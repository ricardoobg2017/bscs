RENAME MF_ENVIOS TO MF_ENVIOS_19042017;
RENAME MF_ENVIOS_IVR TO MF_ENVIOS_IVR_19042017;
RENAME MF_ENVIOS_GES TO MF_ENVIOS_GES_19042017;

ALTER INDEX MF_ENVIOS_PK_IDENVIO RENAME TO MF_ENVIOS_PK_IDENVIO_19042017;
ALTER INDEX MF_ENVIOS_IVR_PK_IDENVIO RENAME TO MF_ENVIOS_IVR_PK_IDENVIO1904;
ALTER INDEX MF_ENVIOS_GES_PK_IDENVIO RENAME TO MF_ENVIOS_GES_PK_IDENVIO_1904;

ALTER TABLE MF_ENVIOS_19042017 RENAME CONSTRAINT MF_ENVIOS_PK_IDENVIO TO MF_ENVIOS_PK_IDENVIO_19042017;
ALTER TABLE MF_ENVIOS_IVR_19042017 RENAME CONSTRAINT MF_ENVIOS_IVR_PK_IDENVIO TO MF_ENVIOS_IVR_PK_IDENVIO1904;
ALTER TABLE MF_ENVIOS_GES_19042017 RENAME CONSTRAINT MF_ENVIOS_GES_PK_IDENVIO TO MF_ENVIOS_GES_PK_IDENVIO_1904;

ALTER TABLE MF_ENVIOS_19042017 RENAME CONSTRAINT MF_ENVIOS_CH_ESTADO TO MF_ENVIOS_CH_ESTADO_19042017;
ALTER TABLE MF_ENVIOS_IVR_19042017 RENAME CONSTRAINT MF_ENVIOS_IVR_CH_ESTADO TO MF_ENVIOS_IVR_CH_ESTADO_1904;
ALTER TABLE MF_ENVIOS_GES_19042017 RENAME CONSTRAINT MF_ENVIOS_GES_CH_ESTADO TO MF_ENVIOS_GES_CH_ESTADO_1904;
