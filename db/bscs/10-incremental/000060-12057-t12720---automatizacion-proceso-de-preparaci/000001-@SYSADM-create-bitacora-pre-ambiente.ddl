-- Create table
create table BITACORA_PRE_AMBIENTE
(
  FECHA_CORTE   DATE not null,
  USUARIO       VARCHAR2(20) not null,
  FECHA_INGRESO DATE not null,
  TIPO          VARCHAR2(20) not null,
  TIPO_VISTA    VARCHAR2(10) not null,
  VALOR         VARCHAR2(4000),
  ESTADO        VARCHAR2(2) not null
)
tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BITACORA_PRE_AMBIENTE.TIPO
  is 'Commit y Control Group';
comment on column BITACORA_PRE_AMBIENTE.TIPO_VISTA
  is 'Tipo de vista Commit (Union, bulk, pymes, asterisco) y control Group (Aut, Tar)';
comment on column BITACORA_PRE_AMBIENTE.VALOR
  is 'Encabezado del html ';
comment on column BITACORA_PRE_AMBIENTE.ESTADO
  is 'Estado de verificacion de la ultima ejecucion';
