-- Create table
create table BITACORA_PRE_AMBIENTE_DE
(
  FECHA_CORTE      DATE not null,
  USUARIO          VARCHAR2(20) not null,
  FECHA_INICIO     DATE not null,
  FECHA_FIN        DATE,
  EJECUCION        VARCHAR2(1000) not null,
  OBSERVACION      VARCHAR2(1000),
  ESTADO_EJECUCION VARCHAR2(2) not null,
  TIPO             VARCHAR2(10) not null,
  ESTADO           VARCHAR2(3) not null
)
tablespace DATA12
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 120K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BITACORA_PRE_AMBIENTE_DE.FECHA_CORTE
  is 'Fecha de corte de la ejecucion';
comment on column BITACORA_PRE_AMBIENTE_DE.USUARIO
  is 'Ususario que ejecuta el proceso de preparacion de ambiente';
comment on column BITACORA_PRE_AMBIENTE_DE.FECHA_INICIO
  is 'Fecha de inicio de cada una de las etapas';
comment on column BITACORA_PRE_AMBIENTE_DE.FECHA_FIN
  is 'Fecha fin de cada una de las etapas';
comment on column BITACORA_PRE_AMBIENTE_DE.EJECUCION
  is 'Nombre de las etapas del proceso';
comment on column BITACORA_PRE_AMBIENTE_DE.OBSERVACION
  is 'Ejecucion detalla del proceso';
comment on column BITACORA_PRE_AMBIENTE_DE.ESTADO_EJECUCION
  is '(VI) Vistas, (DC) Tablas de Control, (TP) Promociones Activas/Inactivas, (TS) Tabla Billcycle, (TH) Tabla History_table, (SI) Sinonimos, (TM) Tabla Mpulktmb, (VM) Tabla View_mater';
comment on column BITACORA_PRE_AMBIENTE_DE.TIPO
  is '(C) Commit, (CG) Control Group, (SEG-C) Ejecucion por segmento Commit, (SEG-CG) Ejecucion por segmento Control Group, (PRB-C) Ejecucion modo prueba Commit, (PRB-CG) Ejecucion modo prueba Control Group';
comment on column BITACORA_PRE_AMBIENTE_DE.ESTADO
  is '(I) Inicio de le etapa, (T) Etapa Terminada correctamente,(E) Error';
