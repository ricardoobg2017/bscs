-- Create table
create table FN_PAGOS_FINAN
(
  id_pago    VARCHAR2(20),
  no_fac     VARCHAR2(20),
  no_detfac  VARCHAR2(20),
  valor_pago NUMBER(20),
  fecha_pago DATE
)
tablespace ORDER_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_PAGO_FINAN_1 on FN_PAGOS_FINAN (NO_FAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PAGO_FINAN_2 on FN_PAGOS_FINAN (NO_DETFAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
