-- Create table
create table FN_FINANCIAMIENTOS
(
  id_finan_crm         VARCHAR2(20),
  id_finan_cbs         VARCHAR2(20),
  no_orden             NUMBER(20),
  no_cuotas            NUMBER(2),
  total_financiamiento NUMBER(20),
  estado               VARCHAR2(32),
  producto             VARCHAR2(100),
  codigo_prod          VARCHAR2(32),
  fecha_creacion       DATE
)
tablespace ORDER_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_FINAN_MES_1 on FN_FINANCIAMIENTOS (ID_FINAN_CRM)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FINAN_MES_2 on FN_FINANCIAMIENTOS (ID_FINAN_CBS)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FINAN_MES_3 on FN_FINANCIAMIENTOS (NO_ORDEN)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FINAN_MES_4 on FN_FINANCIAMIENTOS (FECHA_CREACION)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
