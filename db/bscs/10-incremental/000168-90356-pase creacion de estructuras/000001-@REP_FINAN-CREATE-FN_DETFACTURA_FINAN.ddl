-- Create table
create table FN_DETFACTURACION_FINAN
(
  no_fac       NUMBER(20),
  no_detfac    NUMBER(20),
  id_finan_cbs VARCHAR2(20),
  total        NUMBER(20),
  saldo        NUMBER(20),
  fecha_fac    DATE
)
tablespace ORDER_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_FACT_FINAN_1 on FN_DETFACTURACION_FINAN (ID_FINAN_CBS)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FACT_FINAN_2 on FN_DETFACTURACION_FINAN (NO_FAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FACT_FINAN_3 on FN_DETFACTURACION_FINAN (NO_DETFAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
