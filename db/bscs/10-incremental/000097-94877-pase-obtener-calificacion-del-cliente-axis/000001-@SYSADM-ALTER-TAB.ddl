-- Add/modify columns 
alter table CO_REPCARCLI2_08042016 add calificacion number;
comment on column CO_REPCARCLI2_08042016.calificacion
  is 'Indica la calificacion del clente';
alter table CO_REPCARCLI2_08042016 add categoria varchar2(100);
comment on column CO_REPCARCLI2_08042016.categoria
  is 'Indica la categoria del clente';
alter table CO_REPCARCLI2_08042016 add fecha_proceso date;
comment on column CO_REPCARCLI2_08042016.fecha_proceso
  is 'Indica la fecha en que se consulta la calificacion';
