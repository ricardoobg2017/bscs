-- Create table
create table FN_GESTION_SHORTCODE
(
  id_mensaje          NUMBER,
  shortcode           NUMBER,
  mensaje             VARCHAR2(1000),
  ciclo               VARCHAR2(3),
  customer_id         NUMBER,
  custcode            VARCHAR2(30),
  co_id               NUMBER,
  id_servicio         VARCHAR2(20),
  financiamiento      VARCHAR2(3),
  deuda_total         NUMBER,
  respuesta           VARCHAR2(1000),
  id_negociacion      NUMBER,
  fecha_max_pago      DATE,
  fecha_respuesta     DATE,
  estado              VARCHAR2(3),
  fecha_ingreso       DATE,
  usuario_ingreso     varchar2(100),  
  observacion         VARCHAR2(4000),  
  fecha_actualizacion DATE
)

partition by range (FECHA_INGRESO)
(
  partition FN_GESTION_SHORTCODE_20170822 values less than (TO_DATE(' 2017-08-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )  
);
  
-- Add comments to the table 
comment on table FN_GESTION_SHORTCODE
  is 'Tabla de trabajo para alamacenar el resultado de las transacciones de shortcode o USSD de cobranzas.';  
-- Add comments to the columns 
comment on column FN_GESTION_SHORTCODE.id_mensaje
  is 'id del mensaje de cobranzas.';
comment on column FN_GESTION_SHORTCODE.shortcode
  is 'Numero de shortcode o USSD utilizado.';
comment on column FN_GESTION_SHORTCODE.mensaje
  is 'Mensaje inicial que le llegar� al cliente.';
comment on column FN_GESTION_SHORTCODE.ciclo
  is 'Ciclo de facturaci�n del cliente.';
comment on column FN_GESTION_SHORTCODE.customer_id
  is 'Identificador del cliente en BSCS.';
comment on column FN_GESTION_SHORTCODE.custcode
  is 'Numero de la cuenta del cliente.';
comment on column FN_GESTION_SHORTCODE.co_id
  is 'Contrato BSCS del n�mero de servicio.';
comment on column FN_GESTION_SHORTCODE.id_servicio
  is 'Numero del servicio del cliente.';
comment on column FN_GESTION_SHORTCODE.financiamiento
  is 'Indica el criterio utilizado para determinar financiamiento del cliente; S=cliente con financiamiento; N=cliente sin financiamiento; T=indistinto.';
comment on column FN_GESTION_SHORTCODE.deuda_total
  is 'Monto total de la deuda del cliente.';
comment on column FN_GESTION_SHORTCODE.respuesta
  is 'Registra la respuesta del cliente al mensaje enviado.';
comment on column FN_GESTION_SHORTCODE.id_negociacion
  is 'Codigo de la negociacion en caso de aceptaci�n por parte del cliente.';
comment on column FN_GESTION_SHORTCODE.fecha_max_pago
  is 'Fecha maximna de pago del cliente.';
comment on column FN_GESTION_SHORTCODE.fecha_respuesta
  is 'Fecha en la cual se registr� la respuesta del cliente.';
comment on column FN_GESTION_SHORTCODE.estado
  is 'Estado del registro.';  
comment on column FN_GESTION_SHORTCODE.usuario_ingreso
  is 'Usuario que ingreso el registro';    
comment on column FN_GESTION_SHORTCODE.fecha_ingreso
  is 'Fecha en la cual se ingres� el registro';
comment on column FN_GESTION_SHORTCODE.Observacion
  is 'Comentario adicional acerca de la transacci�n.';  
comment on column FN_GESTION_SHORTCODE.fecha_actualizacion
  is 'Fecha en la cual se actualiz� el registro';


-- Create/Recreate foreign key constraints 
alter table FN_GESTION_SHORTCODE
  add constraint FK_FN_GEST_SHORT_IDMEN foreign key (ID_MENSAJE)
  references FN_CONF_CAB_SHORTCODE (ID_MENSAJE);

alter table FN_GESTION_SHORTCODE
  add constraint FK_FN_GEST_SHORT_SHORT foreign key (SHORTCODE)
  references FN_SHORTCODE_SMS_CONFIG (SHORTCODE);
  
  
-- Create/Recreate indexes 
create index IDX_FN_GEST_SHORT_CUST on FN_GESTION_SHORTCODE (CUSTCODE)LOCAL
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FN_GEST_SHORT_ESTADO on FN_GESTION_SHORTCODE (ESTADO)LOCAL
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FN_GEST_SHORT_FECHING on FN_GESTION_SHORTCODE (FECHA_INGRESO)LOCAL
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  create index IDX_FN_GEST_SHORT_IDMEN on FN_GESTION_SHORTCODE (ID_MENSAJE)LOCAL
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  create index IDX_FN_GEST_SHORT_SHORT on FN_GESTION_SHORTCODE (SHORTCODE)LOCAL
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

  
  
