-- Creación de tabla de configuración de mensaje spara ussd o shortcodes
create table FN_SHORTCODE_SMS_CONFIG
(
  shortcode           NUMBER not null,
  mensaje             VARCHAR2(1000),
  estado              VARCHAR2(3),
  usuario             VARCHAR2(30),
  fecha_ingreso       DATE,
  fecha_actualizacion DATE
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the table 
comment on table FN_SHORTCODE_SMS_CONFIG
  is 'Tabla que contiene los mensajes de texto de los shortcodes o USSD de cobranzas';
  
-- Add comments to the columns 
comment on column FN_SHORTCODE_SMS_CONFIG.shortcode
  is 'Numero de shortcode o USSD a configurar.';
comment on column FN_SHORTCODE_SMS_CONFIG.mensaje
  is 'Texto del mensaje inicial que dispara el evento.';
comment on column FN_SHORTCODE_SMS_CONFIG.estado
  is 'Estado del Registro.';
comment on column FN_SHORTCODE_SMS_CONFIG.usuario
  is 'Usuario que ingresa o actualiza el registro.';
comment on column FN_SHORTCODE_SMS_CONFIG.fecha_ingreso
  is 'Fecha en la que se ingresa el registro.';
comment on column FN_SHORTCODE_SMS_CONFIG.fecha_actualizacion
  is 'Fecha de modificación del registro.';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table FN_SHORTCODE_SMS_CONFIG
  add constraint PK_FN_SHORT_SHORT primary key (SHORTCODE)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
