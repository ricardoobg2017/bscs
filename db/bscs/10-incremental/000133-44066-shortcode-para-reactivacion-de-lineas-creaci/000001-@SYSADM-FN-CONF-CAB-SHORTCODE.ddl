-- Create table
create table FN_CONF_CAB_SHORTCODE
(
  id_mensaje      NUMBER not null,
  shortcode       NUMBER,
  mensaje         VARCHAR2(160),
  status_susp     VARCHAR2(50),
  monto_minimo    NUMBER,
  monto_maximo    NUMBER,
  financiamiento  VARCHAR2(1),
  ciclo           VARCHAR2(50),
  estado          VARCHAR2(1),
  usuario_ingreso VARCHAR2(100),
  fecha_ingreso   DATE,
  fecha_actualizacion DATE
)
tablespace  DATA2
  pctfree 10
  initrans 1
  maxtrans 255;
  
-- Add comments to the table 
comment on table FN_CONF_CAB_SHORTCODE
  is 'Tabla que contiene la configuración general de un shortcode o USSD de cobranzas.';
  
    
-- Add comments to the columns 
comment on column FN_CONF_CAB_SHORTCODE.id_mensaje
  is 'id del mensaje de cobranzas.';
comment on column FN_CONF_CAB_SHORTCODE.shortcode
  is 'Numero de shortcode o USSD a configurar.';
comment on column FN_CONF_CAB_SHORTCODE.mensaje
  is 'Texto del mensaje inicial que dispara el evento.';
comment on column FN_CONF_CAB_SHORTCODE.status_susp
  is 'Estados de suspension de cobranzas para la extracción.';
comment on column FN_CONF_CAB_SHORTCODE.monto_minimo
  is 'Valor mínimo de deuda en porcentajes.';
comment on column FN_CONF_CAB_SHORTCODE.monto_maximo
  is 'Valor máximo de deuda en porcentajes.';
comment on column FN_CONF_CAB_SHORTCODE.financiamiento
  is 'Indica el criterio utilizado para determinar financiamiento del cliente; S=cliente con financiamiento; N=cliente sin financiamiento; T=indistinto.';
comment on column FN_CONF_CAB_SHORTCODE.ciclo
  is 'Ciclo de facturacion del cliente.';
comment on column FN_CONF_CAB_SHORTCODE.estado
  is 'Estado del registro A(Activo), I(Inactivo).';
comment on column FN_CONF_CAB_SHORTCODE.usuario_ingreso
  is 'Usuario creador de la configuración.';
comment on column FN_CONF_CAB_SHORTCODE.fecha_ingreso
  is 'Fecha de creación de la configuración.';
  comment on column FN_CONF_CAB_SHORTCODE.fecha_actualizacion
  is 'Fecha de actualización de la configuración.';
  
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table FN_CONF_CAB_SHORTCODE
  add constraint PK_FN_CAB_SHORT_IDMEN primary key (ID_MENSAJE)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create/Recreate indexes 
create index IDX_FN_CAB_SHORT_SHORT on FN_CONF_CAB_SHORTCODE (SHORTCODE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
      
-- Create/Recreate indexes 
create index IDX_FN_CAB_SHORT_ESTADO on FN_CONF_CAB_SHORTCODE (ESTADO)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );

