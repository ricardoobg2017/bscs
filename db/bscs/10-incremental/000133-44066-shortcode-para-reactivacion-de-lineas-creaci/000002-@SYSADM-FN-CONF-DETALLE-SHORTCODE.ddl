-- Create table
create table FN_CONF_DETALLE_SHORTCODE
(
  id_mensaje      NUMBER,
  shortcode       NUMBER,
  codigo          VARCHAR2(10),
  descripcion     VARCHAR2(500),
  tipo            VARCHAR2(1),
  usuario_ingreso VARCHAR2(100),
  fecha_ingreso   DATE,
  fecha_actualizacion DATE
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the table 
comment on table FN_CONF_DETALLE_SHORTCODE
  is 'Tabla que contiene el detalle de la configuración de un shortcode o USSD de cobranzas.';
    
-- Add comments to the columns 
comment on column FN_CONF_DETALLE_SHORTCODE.id_mensaje
  is 'Id mensaje de la tabla FN_CONF_CAB_SHORTCODE.';
comment on column FN_CONF_DETALLE_SHORTCODE.shortcode
  is 'Numero de shortcode o USSD a configurar.';
comment on column FN_CONF_DETALLE_SHORTCODE.codigo
  is 'Codigo del tipo o categoria del cliente (prgcode de la tabla customer_all de Bscs o campo código de la tabla mf_categorias_cliente de Bscs).';
comment on column FN_CONF_DETALLE_SHORTCODE.descripcion
  is 'Descripcion del tipo o categoria del cliente.';
comment on column FN_CONF_DETALLE_SHORTCODE.tipo
  is 'Identificador del cliente T= por tipo de cliente; C=por categoría de cliente.';
comment on column FN_CONF_DETALLE_SHORTCODE.usuario_ingreso
  is 'Usuario creador de la configuración.';
comment on column FN_CONF_DETALLE_SHORTCODE.fecha_ingreso
  is 'Fecha de creación de la configuración.';
  comment on column FN_CONF_DETALLE_SHORTCODE.fecha_actualizacion
  is 'Fecha de actualización de la configuración.';  
  
-- Create/Recreate foreign key constraints 
alter table FN_CONF_DETALLE_SHORTCODE
  add constraint FK_FN_DET_SHORT_IDMEN foreign key (ID_MENSAJE)
  references FN_CONF_CAB_SHORTCODE (ID_MENSAJE);

alter table FN_CONF_DETALLE_SHORTCODE
  add constraint FK_FN_DET_SHORT_SHORT foreign key (SHORTCODE)
  references FN_SHORTCODE_SMS_CONFIG (SHORTCODE);
  
-- Create index    
create index IDX_FN_DET_SHORT_IDMEN on FN_CONF_DETALLE_SHORTCODE (ID_MENSAJE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  create index IDX_CONF_DETALLE_SHORTCODE on FN_CONF_DETALLE_SHORTCODE (SHORTCODE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

