--Ejecutar en el usuario sysadm   08/03/2018


------------------------------------------------------------------------
--------------- CREA COLUMNA EN LA TABLA (NC_DATOS_CAJA) ---------------
---USUARIO
 ALTER TABLE SYSADM.NC_DATOS_CAJA
  ADD USUARIO VARCHAR2(100);    
 comment on column NC_DATOS_CAJA.USUARIO
  is 'Nombre del usuario';

---CICLO
 ALTER TABLE SYSADM.NC_DATOS_CAJA
  ADD CICLO VARCHAR2(5);    
 comment on column NC_DATOS_CAJA.CICLO
  is 'Valor del ciclo';  

---CIA
 ALTER TABLE SYSADM.NC_DATOS_CAJA
  ADD CIA VARCHAR2(10);    
 comment on column NC_DATOS_CAJA.CIA
  is 'Valor del cia';    

---ESTADO
 ALTER TABLE SYSADM.NC_DATOS_CAJA
  ADD ESTADO VARCHAR2(1) DEFAULT 'P';    
 comment on column NC_DATOS_CAJA.ESTADO
  is 'Estado : P(endiente), F(inalizado)'; 
  
  
  ------------------------------------------------------------------------
------------------------- CREACION DE LOS INDEX ------------------------
--- (FECHA_CREDITO)
-- Create/Recreate indexes 
create index IDX_NC_CAJA_CRED on NC_DATOS_CAJA (FECHA_CREDITO);
  
--- (ESTADO)  
-- Create/Recreate indexes 
create index IDX_NC_CAJA_CRED_ES on NC_DATOS_CAJA (ESTADO);
