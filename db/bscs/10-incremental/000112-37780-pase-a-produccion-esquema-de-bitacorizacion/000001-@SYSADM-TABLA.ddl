-- Create table
create table MF_BITACORA_CALIF
(
  id_envio       NUMBER not null,
  secuencia      NUMBER,
  identificacion VARCHAR2(20),
  calificacion   NUMBER,
  categoria_cli  VARCHAR2(10),
  destinatario   VARCHAR2(20),
  fecha_registro DATE
)
partition by range (fecha_registro)
(
 partition MF_BITA_CALIFIC_20160301 values less than (TO_DATE(' 2016-10-01 00:00:00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
 tablespace DATA8
);
-- Add comments to the columns 
comment on column MF_BITACORA_CALIF.id_envio
  is 'Numero de Envio';
comment on column MF_BITACORA_CALIF.secuencia
  is 'Numero de secuencia';
comment on column MF_BITACORA_CALIF.identificacion
  is 'Identificacion del cliente';
comment on column MF_BITACORA_CALIF.calificacion
  is 'Calificacion del cliente';
comment on column MF_BITACORA_CALIF.categoria_cli
  is 'Categoria segun la calificacion del cliente';
comment on column MF_BITACORA_CALIF.destinatario
  is 'Numero de servicio del cliente';
comment on column MF_BITACORA_CALIF.fecha_registro
  is 'Fecha de registro';
-- Create/Recreate indexes Local
CREATE INDEX IDENTIFIDX ON MF_BITACORA_CALIF (IDENTIFICACION) LOCAL;
CREATE INDEX IDENVIOIDX ON MF_BITACORA_CALIF (ID_ENVIO) LOCAL;
