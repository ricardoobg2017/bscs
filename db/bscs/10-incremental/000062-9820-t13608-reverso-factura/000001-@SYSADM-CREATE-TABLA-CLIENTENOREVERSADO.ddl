-- Create table
create table CLIENTE_NOREVERSADO
(
  CUSTOMER_ID        NUMBER,
  PROCESADO          VARCHAR2(1),
  DESCRIPCION        VARCHAR2(200),
  CIERRE             DATE
  
)

tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the table 
comment on table CLIENTE_NOREVERSADO
  is 'Tabla de cuentas no reversadas';
-- Add comments to the columns 
comment on column CLIENTE_NOREVERSADO.CUSTOMER_ID
  is 'El customer_id de las cuentas que no fueron reversadas';
comment on column CLIENTE_NOREVERSADO.PROCESADO
  is 'La marca de las cuentas que no fueron reversadas';
comment on column CLIENTE_NOREVERSADO.DESCRIPCION
  is 'La descripcion de las cuentas con error, que no fueron reversadas';
comment on column CLIENTE_NOREVERSADO.CIERRE
  is 'Fecha de cierre de facturiacion que se va a reversar';
   
