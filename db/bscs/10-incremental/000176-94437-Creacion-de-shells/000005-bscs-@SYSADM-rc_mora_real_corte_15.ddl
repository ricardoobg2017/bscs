-- CREATE TABLE
CREATE TABLE RC_MORA_REAL_CORTE_15
(
  CUENTA      VARCHAR2(100),
  HILO        NUMBER,
  MORA_REAL   NUMBER,
  CASTIGADA   VARCHAR2(1),
  FECHA       DATE,
  USUARIO     VARCHAR2(100),
  FECHA_MOD   DATE,
  USUARIO_MOD VARCHAR2(100)
)
TABLESPACE REP_DATA
  PCTFREE 10
  PCTUSED 40
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
-- ADD COMMENTS TO THE COLUMNS 
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.CUENTA
  IS 'CUENTA DEL CLIENTE';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.HILO
  IS 'HILO POR DE LA CO_REPORT_ADIC';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.MORA_REAL
  IS 'CANTIDAD EN DIAS DE MORA QUE TIENE LA CUENTA';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.CASTIGADA
  IS 'S SI LA CUENTA ES CASTIGADA O N SI LA CUENTA NO ESTA CASTIGADA ';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.FECHA
  IS 'FECHA DE INSERT';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.USUARIO
  IS 'USARIO DE INSERT ';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.FECHA_MOD
  IS 'FECHA MODIFICACION ';
COMMENT ON COLUMN RC_MORA_REAL_CORTE_15.USUARIO_MOD
  IS 'USUARIO MODIFICACION ';
-- CREATE/RECREATE INDEXES 
CREATE INDEX IDX2_CAST_15 ON RC_MORA_REAL_CORTE_15 (CASTIGADA)
  TABLESPACE IND2
  PCTFREE 10
  INITRANS 2
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
CREATE INDEX IDX2_CUENTA_15 ON RC_MORA_REAL_CORTE_15 (CUENTA)
  TABLESPACE IND2
  PCTFREE 10
  INITRANS 2
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
CREATE INDEX IDX2_HILO_15 ON RC_MORA_REAL_CORTE_15 (HILO)
  TABLESPACE IND2
  PCTFREE 10
  INITRANS 2
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );

grant select on RC_MORA_REAL_CORTE_15 to ADM_ATF;
grant select on RC_MORA_REAL_CORTE_15 to ADM_AUD;
grant select on RC_MORA_REAL_CORTE_15 to ADM_AXIS;
grant select on RC_MORA_REAL_CORTE_15 to ADM_BILLING;
grant select on RC_MORA_REAL_CORTE_15 to ADM_EXTRAFONDO;
grant select on RC_MORA_REAL_CORTE_15 to ADM_MDE;
grant select on RC_MORA_REAL_CORTE_15 to ADM_REP_SAC;
grant select on RC_MORA_REAL_CORTE_15 to ADM_TXT;
grant select on RC_MORA_REAL_CORTE_15 to ADM_USUARIO;
grant select on RC_MORA_REAL_CORTE_15 to ADM_USUARIOS;
grant select on RC_MORA_REAL_CORTE_15 to AIC;
grant select on RC_MORA_REAL_CORTE_15 to AQ_ADMINISTRATOR_ROLE;
grant select on RC_MORA_REAL_CORTE_15 to AQ_USER_ROLE;
grant select on RC_MORA_REAL_CORTE_15 to ASE_TXT;
grant select on RC_MORA_REAL_CORTE_15 to ASIST_CONTRATOS_CEL;
grant select on RC_MORA_REAL_CORTE_15 to ASIST_CVP_FINAN;
grant select on RC_MORA_REAL_CORTE_15 to ASIST_VENT_SLU;
grant select on RC_MORA_REAL_CORTE_15 to ASIS_REP_SAC;
grant select on RC_MORA_REAL_CORTE_15 to ASI_MDE;
grant select on RC_MORA_REAL_CORTE_15 to ATF3C;
grant select on RC_MORA_REAL_CORTE_15 to AUD;
grant select on RC_MORA_REAL_CORTE_15 to AUD_ADMINISTRADOR;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_ANALISTA_SEC;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_CONSULT_LLAMADAS;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_CONSULT_SMS;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_SUP_SEC;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_SUSPENSION_LOCUTORIO;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_TARJETA_CAC;
grant select on RC_MORA_REAL_CORTE_15 to CBILL_TARJETA_CONTACT;
grant select on RC_MORA_REAL_CORTE_15 to CEQ_TXT;
grant select on RC_MORA_REAL_CORTE_15 to COB_ADMINISTRADOR;
grant select on RC_MORA_REAL_CORTE_15 to CONAXIS;
grant select on RC_MORA_REAL_CORTE_15 to CPL_TXT;
grant select on RC_MORA_REAL_CORTE_15 to CSI_TXT;
grant select on RC_MORA_REAL_CORTE_15 to CUR1;
grant select on RC_MORA_REAL_CORTE_15 to EXTRAFONDO_TOTAL;
grant select on RC_MORA_REAL_CORTE_15 to FA_ADMIN;
grant select on RC_MORA_REAL_CORTE_15 to FIN;
grant select on RC_MORA_REAL_CORTE_15 to GSI_GESTOR_PROC;
grant select on RC_MORA_REAL_CORTE_15 to GV_ADMIN;
grant select on RC_MORA_REAL_CORTE_15 to GV_ADM_DAT;
grant select on RC_MORA_REAL_CORTE_15 to GV_ADM_FIN;
grant select on RC_MORA_REAL_CORTE_15 to GV_ADM_SCO;
grant select on RC_MORA_REAL_CORTE_15 to GV_GER_JEF;
grant select on RC_MORA_REAL_CORTE_15 to HS_ADMIN_ROLE;
grant select on RC_MORA_REAL_CORTE_15 to JEFE_CAC;
grant select on RC_MORA_REAL_CORTE_15 to JEFE_OPE;
grant select on RC_MORA_REAL_CORTE_15 to NET;
grant select on RC_MORA_REAL_CORTE_15 to NET_GEST_VENTAS;
grant select on RC_MORA_REAL_CORTE_15 to NP_ADM;
grant select on RC_MORA_REAL_CORTE_15 to NP_APR;
grant select on RC_MORA_REAL_CORTE_15 to NP_ASI;
grant select on RC_MORA_REAL_CORTE_15 to OFI_TXT;
grant select on RC_MORA_REAL_CORTE_15 to OPE;
grant select on RC_MORA_REAL_CORTE_15 to OPE_ACT_MASIV_RPTA;
grant select on RC_MORA_REAL_CORTE_15 to OPE_AXIS_2CAPAS;
grant select on RC_MORA_REAL_CORTE_15 to OPE_REP_AUT_DEBITO;
grant select on RC_MORA_REAL_CORTE_15 to PRO_SACVIP_ASIS;
grant select on RC_MORA_REAL_CORTE_15 to PRO_SACVIP_SUP;
grant select on RC_MORA_REAL_CORTE_15 to RCC_ADMINISTRACION;
grant select on RC_MORA_REAL_CORTE_15 to RCC_CONFIGURACION;
grant select on RC_MORA_REAL_CORTE_15 to REPORTE_PUNTOS;
grant select on RC_MORA_REAL_CORTE_15 to REP_CONSULTA_TARJETAS;
grant select on RC_MORA_REAL_CORTE_15 to REP_PRODUCCION;
grant select on RC_MORA_REAL_CORTE_15 to REP_SACVIP_ASIS;
grant select on RC_MORA_REAL_CORTE_15 to REP_SACVIP_SUP;
grant select on RC_MORA_REAL_CORTE_15 to REP_SDA_PRINCIPAL;
grant select on RC_MORA_REAL_CORTE_15 to RET_NAV_50_2006;
grant select on RC_MORA_REAL_CORTE_15 to SAC;
grant select on RC_MORA_REAL_CORTE_15 to SCO_GEST_RECP_LOTES;
grant select on RC_MORA_REAL_CORTE_15 to SEC_GEST_TRAMI_SDA;
grant select on RC_MORA_REAL_CORTE_15 to SEC_MANT_USUAR_SDA;
grant select on RC_MORA_REAL_CORTE_15 to SEC_MANT_USU_SDA_WEB;
grant select on RC_MORA_REAL_CORTE_15 to SELECT_CATALOG_ROLE;
grant select on RC_MORA_REAL_CORTE_15 to ST_ASIS_DOM;
grant select on RC_MORA_REAL_CORTE_15 to TEL_TXT;
grant select on RC_MORA_REAL_CORTE_15 to WFCONSULTA;
grant select on RC_MORA_REAL_CORTE_15 to WFL_SAC_REASG;
grant select on RC_MORA_REAL_CORTE_15 to WM_ADMIN_ROLE;
grant select on RC_MORA_REAL_CORTE_15 to WORKFLOW;
grant select on RC_MORA_REAL_CORTE_15 to WPP_FINANCIERO;
grant select on RC_MORA_REAL_CORTE_15 to WPP_INVENTARIO;
grant select on RC_MORA_REAL_CORTE_15 to WPP_PORTA_RENT;
grant select on RC_MORA_REAL_CORTE_15 to WPP_SAC_SUP;
grant select on RC_MORA_REAL_CORTE_15 to WPP_VENTAS_DTLL;
grant select on RC_MORA_REAL_CORTE_15 to YOV;
