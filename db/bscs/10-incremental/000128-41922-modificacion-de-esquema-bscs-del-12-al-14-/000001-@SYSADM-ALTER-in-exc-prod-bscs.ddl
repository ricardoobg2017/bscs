-- Add/modify columns 
alter table IN_EXCL_PROD_BSCS add FECHA_INI date;
alter table IN_EXCL_PROD_BSCS add FECHA_FIN date;
alter table IN_EXCL_PROD_BSCS add Estado varchar2(2);
-- Add comments to the columns 
comment on column IN_EXCL_PROD_BSCS.FECHA_INI
  is 'Fecha de Inicio';
comment on column IN_EXCL_PROD_BSCS.FECHA_FIN
  is 'Fecha Fin de inactivación';
comment on column IN_EXCL_PROD_BSCS.Estado
  is 'Estado';
