-- Create/Recreate indexes 
create unique index BSCS_IN_IMPU_HIST_PK on BSCS_IN_IMPU_HIST (CODI_HIST, FECH_FIN_HIST, FECH_INI_HIST)
  tablespace IND12
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );