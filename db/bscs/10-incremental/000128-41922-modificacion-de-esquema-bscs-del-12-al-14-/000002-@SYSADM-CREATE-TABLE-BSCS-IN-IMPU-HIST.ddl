-- Create table
create table BSCS_IN_IMPU_HIST
(
  codi_hist     VARCHAR2(2),
  desc_hist     VARCHAR2(30),
  fech_ini_hist DATE,
  fech_fin_hist DATE,
  porc_hist     NUMBER(4,2),
  fech_prorateo varchar2(10),
  estado        varchar2(1),
  fech_varia VARCHAR2(10)
)
tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    minextents 1
    maxextents unlimited
  );


