
create table GSI_BITACORA_TARIFAS
(
  id_proceso   NUMBER,
  proceso      VARCHAR2(500),
  fecha_inicio DATE,
  fecha_fin    DATE,
  observacion  VARCHAR2(1000)
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_BITACORA_TARIFAS.id_proceso
  is 'Identificador del proceso en ejecucion';
comment on column GSI_BITACORA_TARIFAS.proceso
  is 'Descripcion del proceso que se esta ejecutando';
comment on column GSI_BITACORA_TARIFAS.fecha_inicio
  is 'Fecha de inicio de ejecucion del proceso';
comment on column GSI_BITACORA_TARIFAS.fecha_fin
  is 'Fecha de finalizacion de ejecucion del proceso';
comment on column GSI_BITACORA_TARIFAS.observacion
  is 'Observacion del proceso';
