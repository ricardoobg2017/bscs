create table GSI_QC_CONFIG_TARIFAS_BSCS
(
  id_parametro VARCHAR2(30),
  descripcion  VARCHAR2(1000),
  valor        LONG
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table GSI_QC_CONFIG_TARIFAS_BSCS
  is 'Tabla de configuracion de las tarifas BSCS para verificar el versionamiento de las mismas';
-- Add comments to the columns 
comment on column GSI_QC_CONFIG_TARIFAS_BSCS.id_parametro
  is 'Identificador del Parametro';
comment on column GSI_QC_CONFIG_TARIFAS_BSCS.descripcion
  is 'Descripcion del parametro';
comment on column GSI_QC_CONFIG_TARIFAS_BSCS.valor
  is 'Valor parametrizado';
-- Create/Recreate indexes 
create index PK_GSI_QC_CONFIG_TARIFAS_BSCS on GSI_QC_CONFIG_TARIFAS_BSCS (ID_PARAMETRO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );