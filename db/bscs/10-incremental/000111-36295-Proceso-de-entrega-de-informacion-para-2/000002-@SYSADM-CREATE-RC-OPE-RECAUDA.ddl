-- Creacion de tabla RC_OPE_RECAUDA
CREATE TABLE RC_OPE_RECAUDA 
(
CUENTA                VARCHAR2(24),
ID_CLIENTE            NUMBER,
PRODUCTO              VARCHAR2(30),
CANTON                VARCHAR2(40),
PROVINCIA             VARCHAR2(25),
APELLIDOS             VARCHAR2(40),
NOMBRES               VARCHAR2(40),
RUC                   VARCHAR2(20),
ID_FORMA_PAGO         VARCHAR2(5),
FORMA_PAGO            VARCHAR2(58),
FECH_APER_CUENTA      DATE,
TELEFONO1             VARCHAR2(25),
TELEFONO2             VARCHAR2(25),
TELEFONO              VARCHAR2(63),
DIRECCION             VARCHAR2(70),
DIRECCION2            VARCHAR2(200),
ID_SOLICITUD          VARCHAR2(10),
LINEAS_ACT            VARCHAR2(5),
LINEAS_INAC           VARCHAR2(5),
LINEAS_SUSP           VARCHAR2(5),
COD_VENDEDOR          VARCHAR2(10),
VENDEDOR              VARCHAR2(2000),
NUM_FACTURA           VARCHAR2(30),
BALANCE_1             NUMBER,
BALANCE_2             NUMBER,
BALANCE_3             NUMBER,
BALANCE_4             NUMBER,
BALANCE_5             NUMBER,
BALANCE_6             NUMBER,
BALANCE_7             NUMBER,
BALANCE_8             NUMBER,
BALANCE_9             NUMBER,
BALANCE_10            NUMBER,
BALANCE_11            NUMBER,
BALANCE_12            NUMBER,
TOTALVENCIDA          NUMBER,
TOTAL_DEUDA           NUMBER,
SALDO_PENDIENTE       NUMBER,
MAYORVENCIDO          VARCHAR2(10),
COMPANIA              NUMBER,
FECH_MAX_PAGO         DATE,
ID_PLAN               VARCHAR2(10),
DETALLE_PLAN          VARCHAR2(4000),
DESC_APROBACION       VARCHAR2(4000),
ESTADO                VARCHAR2(2),
FECHA_CORTE           DATE,
USUARIO_INGRESO       VARCHAR2(100) DEFAULT USER,
FECHA_INGRESO         DATE DEFAULT SYSDATE
)
tablespace DATA9
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create/Recreate indexes 
CREATE INDEX IDX_RC_OPE_RECAUD_01 ON RC_OPE_RECAUDA (FECHA_CORTE)
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
CREATE INDEX IDX_RC_OPE_RECAUD_02 ON RC_OPE_RECAUDA (ID_FORMA_PAGO)
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
CREATE INDEX IDX_RC_OPE_RECAUD_03 ON RC_OPE_RECAUDA (MAYORVENCIDO)
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
CREATE INDEX IDX_RC_OPE_RECAUD_04 ON RC_OPE_RECAUDA (FORMA_PAGO)
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
CREATE INDEX IDX_RC_OPE_RECAUD_05 ON RC_OPE_RECAUDA (CUENTA)
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
COMMENT ON TABLE RC_OPE_RECAUDA
  IS 'Tabla de las cuentas que se enviaran a los recaudadores DAS.';
-- Add comments to the columns 
COMMENT ON COLUMN RC_OPE_RECAUDA.CUENTA
  IS 'Cuenta del servicio.';
COMMENT ON COLUMN RC_OPE_RECAUDA.ID_CLIENTE
  IS 'Customer_Id del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.PRODUCTO
  IS 'Subproducto del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.CANTON
  IS 'Canton que esta registrado del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.PROVINCIA
  IS 'Provincia con la que se encuentra registrado el cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.APELLIDOS
  IS 'Apellidos completos del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.NOMBRES
  IS 'Nombres completos del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.RUC
  IS 'Identificacion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.ID_FORMA_PAGO
  IS 'El Id forma de pago que tiene registrado el cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.FORMA_PAGO
  IS 'Descripcion de la forma de pago del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.FECH_APER_CUENTA
  IS 'Fecha de creacion de la cuenta del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.TELEFONO1
  IS 'Medio de comunicacion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.TELEFONO2
  IS 'Medio de comunicacion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.TELEFONO
  IS 'Servicio del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.DIRECCION
  IS 'Direccion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.DIRECCION2
  IS 'Segunda direccion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.ID_SOLICITUD
  IS 'Id solicitud de la aprobacion del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.LINEAS_ACT
  IS 'Cantidad de lineas activas del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.LINEAS_INAC
  IS 'Cantidad de lineas inactivas del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.LINEAS_SUSP
  IS 'Cantidad de lineas suspendidas del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.COD_VENDEDOR
  IS 'Codigo del vendedor del servicio del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.VENDEDOR
  IS 'Descripcion del vendedor del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.NUM_FACTURA
  IS 'Numero de la ultima factura del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_1
  IS 'Deuda de la factura de once meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_2
  IS 'Deuda de la factura de diez meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_3
  IS 'Deuda de la factura de nueve meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_4
  IS 'Deuda de la factura de ocho meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_5
  IS 'Deuda de la factura de siete meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_6
  IS 'Deuda de la factura de seis meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_7
  IS 'Deuda de la factura de cinco meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_8
  IS 'Deuda de la factura de cuatro meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_9
  IS 'Deuda de la factura de tres meses anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_10
  IS 'Deuda de la factura de dos mes anteriores del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_11
  IS 'Deuda de la factura del mes anterior del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.BALANCE_12
  IS 'Valor de la ultima factura del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.TOTALVENCIDA
  IS 'Total de la deuda vencida del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.TOTAL_DEUDA
  IS 'Total de la deuda vencia + la ultima factura generada del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.SALDO_PENDIENTE
  IS 'Saldo del total de la deuda menos los pagos realizados por el cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.MAYORVENCIDO
  IS 'Dias de mora que posee el cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.COMPANIA
  IS 'Id de la compania del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.FECH_MAX_PAGO
  IS 'Fecha maxima de pago del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.ID_PLAN
  IS 'Id del plan del cliente que posee en Axis.';
COMMENT ON COLUMN RC_OPE_RECAUDA.DETALLE_PLAN
  IS 'Descripcion del plan del cliente que posee en Axis.';
COMMENT ON COLUMN RC_OPE_RECAUDA.DESC_APROBACION
  IS 'Descripcion del id solicitud del cliente.';
COMMENT ON COLUMN RC_OPE_RECAUDA.ESTADO
  IS 'Estado del registro I ingresado A procesado T terminado.';
COMMENT ON COLUMN RC_OPE_RECAUDA.FECHA_CORTE
  IS 'Fecha de corte en la que se genero la data.';
COMMENT ON COLUMN RC_OPE_RECAUDA.USUARIO_INGRESO
  IS 'Usuario que ingreso el registro.';
COMMENT ON COLUMN RC_OPE_RECAUDA.FECHA_INGRESO
  IS 'Fecha de ingreso del registro.';
