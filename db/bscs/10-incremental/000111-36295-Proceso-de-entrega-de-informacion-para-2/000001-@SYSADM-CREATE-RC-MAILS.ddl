-- Create table 
create table RC_MAILS
(
  nombre       VARCHAR2(255),
  mail         VARCHAR2(255) not null,
  destinatario VARCHAR2(3) not null,
  estado       VARCHAR2(1) not null
)
tablespace DATA9
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table RC_MAILS
  add constraint PK_RC_MAILS primary key (MAIL, DESTINATARIO, ESTADO)
  using index 
tablespace Teh_idx
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
COMMENT ON TABLE RC_MAILS
  IS 'Tabla de configuracion de direcciones de correo para procesos de Reloj de Cobranzas';
-- Add comments to the columns 
COMMENT ON COLUMN RC_MAILS.nombre
  IS 'Nombres del usuario de correo.';
  -- Add comments to the columns 
COMMENT ON COLUMN RC_MAILS.mail
  IS 'Dirección de correo del usuario de correo.';
  -- Add comments to the columns 
COMMENT ON COLUMN RC_MAILS.destinatario
  IS 'Código que define el proceso para el cual se configura el correo.';
  -- Add comments to the columns 
COMMENT ON COLUMN RC_MAILS.estado
  IS 'Estado de registro A=activo; I=Inactivo.';
