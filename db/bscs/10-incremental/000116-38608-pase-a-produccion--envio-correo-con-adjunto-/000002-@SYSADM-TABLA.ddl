-- Create table
create table MF_MENSAJES_IVR
(
  idenvio           NUMBER not null,
  secuencia_envio   NUMBER not null,
  secuencia_mensaje NUMBER not null,
  mensaje           VARCHAR2(160),
  remitente         VARCHAR2(100),
  destinatario      VARCHAR2(500),
  copia             VARCHAR2(500),
  copiaoculta       VARCHAR2(500),
  asunto            VARCHAR2(100),
  estado            VARCHAR2(1),
  hilo              NUMBER,
  estado_archivo    VARCHAR2(1),
  identificacion    VARCHAR2(20),
  cuenta_bscs       VARCHAR2(24),
  observacion       VARCHAR2(500),
  calificacion      NUMBER,
  categoria_cli     VARCHAR2(10),
  fecha_registro    DATE
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 30M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_MENSAJES_IVR.idenvio
  is 'Identificador del Envio';
comment on column MF_MENSAJES_IVR.secuencia_envio
  is 'Identificador de Secuencia de envio';
comment on column MF_MENSAJES_IVR.secuencia_mensaje
  is 'Identificador de Secuencia de mensaje';
comment on column MF_MENSAJES_IVR.mensaje
  is 'Descripcion del mensaje';
comment on column MF_MENSAJES_IVR.remitente
  is 'Remitente';
comment on column MF_MENSAJES_IVR.destinatario
  is 'Destinatario';
comment on column MF_MENSAJES_IVR.copia
  is 'Copias para mensaje';
comment on column MF_MENSAJES_IVR.copiaoculta
  is 'Copias ocultas del mensaje';
comment on column MF_MENSAJES_IVR.asunto
  is 'Asunto del mensaje';
comment on column MF_MENSAJES_IVR.estado
  is 'Estado del envio del mensaje';
comment on column MF_MENSAJES_IVR.hilo
  is 'Hilo a ejecutar';
comment on column MF_MENSAJES_IVR.estado_archivo
  is 'Estado en que se encuentra para envio de archivo';
comment on column MF_MENSAJES_IVR.identificacion
  is 'Identificacion del cliente';  
comment on column MF_MENSAJES_IVR.cuenta_bscs
  is 'Codigo de cuenta BSCS relacionado';
comment on column MF_MENSAJES_IVR.observacion
  is 'Observacion del registro';
comment on column MF_MENSAJES_IVR.calificacion
  is 'Calificacion del cliente';
comment on column MF_MENSAJES_IVR.categoria_cli
  is 'Categoria segun la calificacion del cliente';      
comment on column MF_MENSAJES_IVR.fecha_registro
  is 'Fecha de insercion del registro';  
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_MENSAJES_IVR
  add constraint MF_MENSAJES_IVR_PK_CLAVE primary key (IDENVIO, SECUENCIA_ENVIO, SECUENCIA_MENSAJE)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 18M
    next 1M
    minextents 1
    maxextents unlimited
  );
