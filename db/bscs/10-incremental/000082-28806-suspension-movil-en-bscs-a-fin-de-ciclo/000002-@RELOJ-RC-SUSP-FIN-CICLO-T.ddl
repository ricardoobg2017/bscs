  --=====================================================================================--
  -- MODIFICADO POR: Jordan Rodr�guez.
  -- FECHA MOD:      23/02/2016
  -- PROYECTO:       [10695] Mejoras al proceso de Reloj de Cobranzas
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Se agrega atributo BSCS_POSTERGADO para se pueda ingresar X en la tabla 
  --                 reloj.suspendidos_fin_ciclo
  --=====================================================================================-- 
CREATE OR REPLACE TYPE reloj.RC_SUSP_FIN_CICLO_T AS OBJECT
(
  CUENTA            VARCHAR2(20),
  CUSTOMER_ID       NUMBER,
  ID_SERVICIO       VARCHAR2(12),
  CO_ID             NUMBER,
  STATUS_SUSP_BSCS  VARCHAR2(5),
  CED_IDENTIDAD     VARCHAR2(15),
  CICLO             VARCHAR2(5),
  BP_PLAN           VARCHAR2(8),
  DETALLE_PLAN      VARCHAR2(1000),
  DEUDA             NUMBER,
  CALIFICACION      NUMBER,
  TIPO_CLIENTE      VARCHAR2(20),
  FORMA_PAGO        VARCHAR2(10),
  CATEGORIA         VARCHAR2(10),
  TIPO_APROBACION   VARCHAR2(5),
  TIPO_CUENTA       VARCHAR2(10),
  ID_FINANCIERA     VARCHAR2(20),
  COMPA�IA          VARCHAR2(3),
  STATUS_LINEA_AXIS VARCHAR2(4),
  FECHA_SUSPENSION  DATE,
  BSCS_POSTERGADO   VARCHAR2(3)--10695 IRO JRO
 );
/
