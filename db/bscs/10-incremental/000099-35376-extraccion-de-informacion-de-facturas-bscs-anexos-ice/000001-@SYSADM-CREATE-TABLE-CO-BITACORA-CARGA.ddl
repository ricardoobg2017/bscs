-- Create table
create table CO_BITACORA_CARGA
(
  fecha_ejecucion_real DATE,
  fecha_corte          DATE,
  fecha_ejecucion      DATE,
  estado_ejecucion     VARCHAR2(1),
  detalle_error        VARCHAR2(2000)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns  
comment on column CO_BITACORA_CARGA.fecha_ejecucion_real
  is 'FECHA DE EJCUCION REAL';
comment on column CO_BITACORA_CARGA.fecha_corte
  is 'FECHA DE CORTE';
comment on column CO_BITACORA_CARGA.fecha_ejecucion
  is 'FECHA DE EJECUCION';
comment on column CO_BITACORA_CARGA.estado_ejecucion
  is 'ESTADO DE EJECUCION';
comment on column CO_BITACORA_CARGA.detalle_error
  is 'DETALLE DEL ERROR';