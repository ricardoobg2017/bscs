-- Add/modify columns 
alter table CON_HIST_MPULKFXO add DESTINATION_NEW varchar2(63) default null;
-- Add comments to the columns 
comment on column CON_HIST_MPULKFXO.DESTINATION_NEW
  is 'Numero nuevo por el que se modifica la tabla mpulkfxo';
