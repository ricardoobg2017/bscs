--10503
--SIS Diana Chonillo
--RGT Josue Rocafuerte
--16/12/2015
--Ajustes a la wf_complain_temp

-- AUMENTO DE COLUMNAS EN TABLA TEMPORAL BSCS
alter table WF_COMPLAIN_TEMPORAL add service_id NUMBER;
alter table WF_COMPLAIN_TEMPORAL add product_id VARCHAR2(20);
alter table WF_COMPLAIN_TEMPORAL add account_id VARCHAR2(30);
alter table WF_COMPLAIN_TEMPORAL add balance VARCHAR2(2);

comment on column WF_COMPLAIN_TEMPORAL.product_id
  is 'guardar id_producto de cob_servicios.';
comment on column WF_COMPLAIN_TEMPORAL.service_id
  is 'guardar servicio de cob_servicios.';
comment on column WF_COMPLAIN_TEMPORAL.account_id
  is 'guardar ctactble de cob_servicios.';
comment on column WF_COMPLAIN_TEMPORAL.balance
  is 'guardar balance de cob_servicios.';  
