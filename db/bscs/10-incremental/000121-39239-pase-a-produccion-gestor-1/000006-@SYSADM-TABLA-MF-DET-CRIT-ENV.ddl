-- Create table
create table MF_DET_CRITERIOS_ENVIOS_GES
(
  id_envio     NUMBER(10),
  idcriterio   NUMBER(10),
  subcategoria VARCHAR2(10),
  estado       VARCHAR2(1)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_DET_CRITERIOS_ENVIOS_GES.id_envio
  is 'Campo identificador de envio relacionado.';
comment on column MF_DET_CRITERIOS_ENVIOS_GES.idcriterio
  is 'Campo identificador del criterio relacionado.';
comment on column MF_DET_CRITERIOS_ENVIOS_GES.subcategoria
  is 'Detalle da la subcategoria relacionada con el Criterio.';
comment on column MF_DET_CRITERIOS_ENVIOS_GES.estado
  is 'Estado del los criterios ''A''ctivo, ''I''nactivo.';
