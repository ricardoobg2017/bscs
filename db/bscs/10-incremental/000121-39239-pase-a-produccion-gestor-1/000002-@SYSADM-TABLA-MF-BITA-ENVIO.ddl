-- Create table
create table MF_BITACORA_ENVIO_GES
(
  num_cuenta_bscs       VARCHAR2(24),
  num_celular           VARCHAR2(25),
  nombre_cliente        VARCHAR2(150),
  cedula                VARCHAR2(20),
  monto_deuda           NUMBER(8,2),
  fecha_registro        DATE,
  id_envio              NUMBER,
  secuencia             NUMBER,
  mensaje               VARCHAR2(160), 
  hilo                  NUMBER,
  estado_archivo        VARCHAR2(1),
  observacion           VARCHAR2(500),
  calificacion          NUMBER,
  categoria_cli         VARCHAR2(10),
  id_cliente            NUMBER,
  producto              VARCHAR2(30),
  canton                VARCHAR2(40),
  provincia             VARCHAR2(25),
  forma_pago            VARCHAR2(58),
  forma_pago2           VARCHAR2(5),
  tarjeta_cuenta        VARCHAR2(25),
  fech_expir_tarjeta    VARCHAR2(20),
  tipo_cuenta           VARCHAR2(40),
  fech_aper_cuenta      DATE,
  telefono1             VARCHAR2(25),
  telefono2             VARCHAR2(25),
  direccion             VARCHAR2(70),
  direccion2            VARCHAR2(200),
  grupo                 VARCHAR2(10),
  id_solicitud          VARCHAR2(10) default '0',
  carta_responsabilidad VARCHAR2(1) default 'N',
  verificacion          VARCHAR2(1) default 'N',
  verificacion_c        VARCHAR2(1) default 'N',
  verificacion_i        VARCHAR2(1) default 'N',
  lineas_act            VARCHAR2(5) default '0',
  lineas_inac           VARCHAR2(5) default '0',
  lineas_susp           VARCHAR2(5) default '0',
  regularizada          VARCHAR2(1) default 'N',
  factura               VARCHAR2(1) default 'N',
  vendedor              VARCHAR2(2000) default 'N',
  num_factura           VARCHAR2(30),
  balance_1             NUMBER default 0,
  balance_2             NUMBER default 0,
  balance_3             NUMBER default 0,
  balance_4             NUMBER default 0,
  balance_5             NUMBER default 0,
  balance_6             NUMBER default 0,
  balance_7             NUMBER default 0,
  balance_8             NUMBER default 0,
  balance_9             NUMBER default 0,
  balance_10            NUMBER default 0,
  balance_11            NUMBER default 0,
  balance_12            NUMBER default 0,
  totalvencida          NUMBER default 0,
  totaladeuda           NUMBER default 0,
  mayorvencido          VARCHAR2(10),
  total_fact            NUMBER default 0,
  saldoant              NUMBER default 0,
  pagosper              NUMBER default 0,
  credtper              NUMBER default 0,
  cmper                 NUMBER default 0,
  consmper              NUMBER default 0,
  compania              NUMBER default 0,
  burocredito           VARCHAR2(1),
  fech_max_pago         DATE,
  mora_real             NUMBER default 0,
  mora_real_mig         NUMBER default 0,
  tipo_ingreso          NUMBER,
  id_plan               VARCHAR2(10),
  detalle_plan          VARCHAR2(4000),
  gestor                VARCHAR2(100)
)
partition by range (FECHA_REGISTRO)
(
  partition MF_BITA_ENV_GES_201611 values less than (TO_DATE(' 2016-11-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    tablespace DATA8
    pctfree 10
    initrans 1
    maxtrans 255
);
-- Add comments to the columns 
comment on column MF_BITACORA_ENVIO_GES.num_cuenta_bscs
  is 'Codigo de cuenta BSCS relacionado';
comment on column MF_BITACORA_ENVIO_GES.num_celular
  is 'Numero de telefono celular';
comment on column MF_BITACORA_ENVIO_GES.nombre_cliente
  is 'Datos del Cliente';
comment on column MF_BITACORA_ENVIO_GES.cedula
  is 'Numero de Cedula o Ruc del cliente';
comment on column MF_BITACORA_ENVIO_GES.monto_deuda
  is 'Valor Adeudado del Cliente';
comment on column MF_BITACORA_ENVIO_GES.fecha_registro
  is 'Fecha de insercion del registro';
comment on column MF_BITACORA_ENVIO_GES.id_envio
  is 'Identificador de envio relacionado';
comment on column MF_BITACORA_ENVIO_GES.secuencia
  is 'Secuebcia de envio';
comment on column MF_BITACORA_ENVIO_GES.mensaje
  is 'Mensaje de envio al cliente';
comment on column MF_BITACORA_ENVIO_GES.hilo
  is 'Numero de hilo';
comment on column MF_BITACORA_ENVIO_GES.estado_archivo
  is 'Estado en que se encuentra para envio de archivo';
comment on column MF_BITACORA_ENVIO_GES.observacion
  is 'Observacion del registro';
comment on column MF_BITACORA_ENVIO_GES.calificacion
  is 'Calificacion del cliente';
comment on column MF_BITACORA_ENVIO_GES.categoria_cli
  is 'Categoria segun la calificacion del cliente';
comment on column MF_BITACORA_ENVIO_GES.id_cliente
  is 'Id del cliente';
comment on column MF_BITACORA_ENVIO_GES.producto
  is 'Producto';
comment on column MF_BITACORA_ENVIO_GES.canton
  is 'Canton';
comment on column MF_BITACORA_ENVIO_GES.provincia
  is 'Provincia';
comment on column MF_BITACORA_ENVIO_GES.forma_pago
  is 'Forma de pago bscs';
comment on column MF_BITACORA_ENVIO_GES.forma_pago2
  is 'Forma de pago axis';
comment on column MF_BITACORA_ENVIO_GES.tarjeta_cuenta
  is 'Codigo de tarjeta';
comment on column MF_BITACORA_ENVIO_GES.fech_expir_tarjeta
  is 'Fecha de expiracion de tarjeta';
comment on column MF_BITACORA_ENVIO_GES.tipo_cuenta
  is 'Tipo de cuenta';
comment on column MF_BITACORA_ENVIO_GES.fech_aper_cuenta
  is 'Fecha de apertura de cuenta';
comment on column MF_BITACORA_ENVIO_GES.telefono1
  is 'Telefono 1';
comment on column MF_BITACORA_ENVIO_GES.telefono2
  is 'Telefono 2';
comment on column MF_BITACORA_ENVIO_GES.direccion
  is 'Direccion del cliente';
comment on column MF_BITACORA_ENVIO_GES.direccion2
  is 'Direccion 2 del cliente';
comment on column MF_BITACORA_ENVIO_GES.grupo
  is 'Indica el grupo';
comment on column MF_BITACORA_ENVIO_GES.id_solicitud
  is 'Id solicitud de credito';
comment on column MF_BITACORA_ENVIO_GES.carta_responsabilidad
  is 'Posee carta de responsabilidad';
comment on column MF_BITACORA_ENVIO_GES.verificacion
  is 'Verificacion';
comment on column MF_BITACORA_ENVIO_GES.verificacion_c
  is 'Verificacion c';
comment on column MF_BITACORA_ENVIO_GES.verificacion_i
  is 'Verificacion i';
comment on column MF_BITACORA_ENVIO_GES.lineas_act
  is 'Lineas activas';
comment on column MF_BITACORA_ENVIO_GES.lineas_inac
  is 'Lineas inactivas';
comment on column MF_BITACORA_ENVIO_GES.lineas_susp
  is 'Lineas suspendidas';
comment on column MF_BITACORA_ENVIO_GES.regularizada
  is 'Regularizada';
comment on column MF_BITACORA_ENVIO_GES.factura
  is 'Indica la factura';
comment on column MF_BITACORA_ENVIO_GES.vendedor
  is 'Indica el vendedor';
comment on column MF_BITACORA_ENVIO_GES.num_factura
  is 'Indica numero de factura';
comment on column MF_BITACORA_ENVIO_GES.balance_1
  is 'Valor del mes 1';
comment on column MF_BITACORA_ENVIO_GES.balance_2
  is 'Valor del mes 2';
comment on column MF_BITACORA_ENVIO_GES.balance_3
  is 'Valor del mes 3';
comment on column MF_BITACORA_ENVIO_GES.balance_4
  is 'Valor del mes 4';
comment on column MF_BITACORA_ENVIO_GES.balance_5
  is 'Valor del mes 5';
comment on column MF_BITACORA_ENVIO_GES.balance_6
  is 'Valor del mes 6';
comment on column MF_BITACORA_ENVIO_GES.balance_7
  is 'Valor del mes 7';
comment on column MF_BITACORA_ENVIO_GES.balance_8
  is 'Valor del mes 8';
comment on column MF_BITACORA_ENVIO_GES.balance_9
  is 'Valor del mes 9';
comment on column MF_BITACORA_ENVIO_GES.balance_10
  is 'Valor del mes 10';
comment on column MF_BITACORA_ENVIO_GES.balance_11
  is 'Valor del mes 11';
comment on column MF_BITACORA_ENVIO_GES.balance_12
  is 'Valor del mes 12';
comment on column MF_BITACORA_ENVIO_GES.totalvencida
  is 'Valor vencido';
comment on column MF_BITACORA_ENVIO_GES.totaladeuda
  is 'Valor deuda';
comment on column MF_BITACORA_ENVIO_GES.mayorvencido
  is 'Indica el mayor vencido';
comment on column MF_BITACORA_ENVIO_GES.total_fact
  is 'Valor total factura';
comment on column MF_BITACORA_ENVIO_GES.saldoant
  is 'Saldo anterior';
comment on column MF_BITACORA_ENVIO_GES.pagosper
  is 'Pagos en periodo';
comment on column MF_BITACORA_ENVIO_GES.credtper
  is 'Credito en periodo';
comment on column MF_BITACORA_ENVIO_GES.cmper
  is 'Consumo periodo';
comment on column MF_BITACORA_ENVIO_GES.consmper
  is 'Consumo en periodo actual';
comment on column MF_BITACORA_ENVIO_GES.compania
  is 'Codigo de compania';
comment on column MF_BITACORA_ENVIO_GES.burocredito
  is 'Buro de credito';
comment on column MF_BITACORA_ENVIO_GES.fech_max_pago
  is 'Fecha maxima de pago';
comment on column MF_BITACORA_ENVIO_GES.mora_real
  is 'Mora real';
comment on column MF_BITACORA_ENVIO_GES.mora_real_mig
  is 'Mora real mig';
comment on column MF_BITACORA_ENVIO_GES.tipo_ingreso
  is 'Indica el tipo de ingreso';
comment on column MF_BITACORA_ENVIO_GES.id_plan
  is 'Codigo de plan';
comment on column MF_BITACORA_ENVIO_GES.detalle_plan
  is 'Detalle del plan';
comment on column MF_BITACORA_ENVIO_GES.gestor
  is 'Indica el numero de gestor asignado';
-- Create/Recreate indexes
create index MF_BITCELULARGES_IDXP on MF_BITACORA_ENVIO_GES (NUM_CELULAR) LOCAL;
create index MF_BITCUENTAGES_IDXP on MF_BITACORA_ENVIO_GES (NUM_CUENTA_BSCS) LOCAL;
create index MF_BITENVIOGES_IDXP on MF_BITACORA_ENVIO_GES (ID_ENVIO, SECUENCIA) LOCAL;
create index MF_ENV_IDX_GES on MF_BITACORA_ENVIO_GES (ID_ENVIO, SECUENCIA, NUM_CELULAR) LOCAL;
