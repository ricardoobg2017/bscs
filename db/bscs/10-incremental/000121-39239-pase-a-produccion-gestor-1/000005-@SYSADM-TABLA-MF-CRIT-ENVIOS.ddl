-- Create table
create table MF_CRITERIOS_X_ENVIOS_GES
(
  idenvio    NUMBER not null,
  idcriterio NUMBER not null,
  secuencia  NUMBER not null,
  valor      VARCHAR2(50) not null,
  efecto     VARCHAR2(1) not null,
  estado     VARCHAR2(1) not null
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_CRITERIOS_X_ENVIOS_GES.idenvio
  is 'ID Envio referenciado';
comment on column MF_CRITERIOS_X_ENVIOS_GES.idcriterio
  is 'Id Criterio referenciado';
comment on column MF_CRITERIOS_X_ENVIOS_GES.secuencia
  is 'Id Secuencia de envio';
comment on column MF_CRITERIOS_X_ENVIOS_GES.valor
  is 'Valor de ese envio';
comment on column MF_CRITERIOS_X_ENVIOS_GES.efecto
  is 'Codigo Efecto para ese envio';
comment on column MF_CRITERIOS_X_ENVIOS_GES.estado
  is 'Estado del envio';  
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_CRITERIOS_X_ENVIOS_GES
  add constraint MF_CRIT_X_ENVIO_GES_PK_CLAVE primary key (IDENVIO, IDCRITERIO, SECUENCIA)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
