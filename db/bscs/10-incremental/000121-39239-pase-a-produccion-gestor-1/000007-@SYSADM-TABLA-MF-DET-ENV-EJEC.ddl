-- Create table
create table MF_DET_ENVIO_EJECUCIONES_GES
(
  idenvio         NUMBER,
  secuencia       NUMBER,
  hilo            NUMBER,
  estado          VARCHAR2(1),
  fecha_inicio    DATE,
  fecha_fin       DATE,
  mensaje_proceso VARCHAR2(1000)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_DET_ENVIO_EJECUCIONES_GES.idenvio
  is 'Identificador del Envio';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.secuencia
  is 'Identificador de la Secuencia';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.hilo
  is 'Numero de hilo a ejecutarse';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.estado
  is 'Estado de la tabla';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.fecha_inicio
  is 'Fecha inicio de proceso';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.fecha_fin
  is 'Fecha fin de proceso';
comment on column MF_DET_ENVIO_EJECUCIONES_GES.mensaje_proceso
  is 'Mensaje del proceso';
-- Create/Recreate indexes 
create index MF_DETENVIO_EJE_GES_IDX on MF_DET_ENVIO_EJECUCIONES_GES (IDENVIO, SECUENCIA)
  tablespace MF_COB_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
