-- Create table
create table MF_DETALLES_ENVIOS_GES
(
  idenvio   NUMBER not null,
  idcliente VARCHAR2(2) not null,
  tipoenvio VARCHAR2(1) not null
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_DETALLES_ENVIOS_GES.idenvio
  is 'Identificador del Envio';
comment on column MF_DETALLES_ENVIOS_GES.idcliente
  is 'Identificador para el Tipo Cliente y Categorizacion Cliente';
comment on column MF_DETALLES_ENVIOS_GES.tipoenvio
  is 'Identifica el Tipo de Envio. Posibles valores ''T'' Tipo Cliente y ''C'' Categorizacion Cliente';
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_DETALLES_ENVIOS_GES
  add constraint MF_DETSENVIOSGES_PK_IDENVIO primary key (IDENVIO, IDCLIENTE, TIPOENVIO)
  using index 
  tablespace MF_COB_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
