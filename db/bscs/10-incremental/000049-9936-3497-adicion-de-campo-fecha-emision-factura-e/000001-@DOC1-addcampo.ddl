-- A�adir nuevo campo para tabla de billing
ALTER TABLE CUSTOMER_NOTIFIES ADD FECHA_EMISION_2 DATE;
-- Comentario del nuevo campo para tabla de billing
comment on column CUSTOMER_NOTIFIES.FECHA_EMISION_2
  is 'Fecha_Emision de la Factura del XML, para aplicar notas de credito';
