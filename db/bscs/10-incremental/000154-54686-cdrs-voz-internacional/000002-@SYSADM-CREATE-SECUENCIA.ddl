-- Create sequence 
create sequence SEQ_ARCHIVO_JASPER
minvalue 0
maxvalue 99999
start with 0
increment by 1
cache 20
cycle;