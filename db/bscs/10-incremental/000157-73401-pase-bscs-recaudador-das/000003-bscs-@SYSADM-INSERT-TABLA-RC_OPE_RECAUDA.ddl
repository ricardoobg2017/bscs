INSERT INTO RC_OPE_RECAUDA 
(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, id_forma_pago, forma_pago, tipo_cliente, fech_aper_cuenta,
telefono1, telefono2, telefono, direccion, direccion2, id_solicitud, lineas_act, lineas_inac, lineas_susp, cod_vendedor, vendedor,
num_factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11,
balance_12, totalvencida, total_deuda, saldo_pendiente, mayorvencido, compania, fech_max_pago, id_plan, detalle_plan, desc_aprobacion,
estado, fecha_corte, hilo, financiamiento, recaudador, fecha_asignacion, fecha_fin_ciclo, pagos, usuario_ingreso, fecha_ingreso,
fecha_actualizacion)
SELECT
cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, id_forma_pago, forma_pago, tipo_cliente, fech_aper_cuenta,
telefono1, telefono2, telefono, direccion, direccion2, id_solicitud, lineas_act, lineas_inac, lineas_susp, cod_vendedor, vendedor,
num_factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11,
balance_12, totalvencida, total_deuda, saldo_pendiente, mayorvencido, compania, fech_max_pago, id_plan, detalle_plan, desc_aprobacion,
estado, fecha_corte, hilo, financiamiento, recaudador, fecha_asignacion, fecha_fin_ciclo, pagos, usuario_ingreso, fecha_ingreso,
fecha_actualizacion
FROM RC_OPE_RECAUDA_201910;

COMMIT;
