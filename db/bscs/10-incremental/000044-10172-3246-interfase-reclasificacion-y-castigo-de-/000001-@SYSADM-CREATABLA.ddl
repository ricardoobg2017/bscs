-- Create table
create table TB_SAP_TRAMAS_REC_CAS
(
  id_trama         NUMBER not null,
  proceso          VARCHAR2(1000),
  trama            CLOB,
  estado_despacho  VARCHAR2(10),
  estado_reproceso VARCHAR2(10),
  fecha_ingreso    DATE,
  fecha_despacho   DATE,
  fecha_reproceso  DATE,
  comentario       VARCHAR2(3000)
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the columns 
comment on column TB_SAP_TRAMAS_REC_CAS.id_trama
  is 'identificador de la trama';
comment on column TB_SAP_TRAMAS_REC_CAS.proceso
  is 'reclasificacion o castigo';
comment on column TB_SAP_TRAMAS_REC_CAS.trama
  is 'trama generada';
comment on column TB_SAP_TRAMAS_REC_CAS.estado_despacho
  is 'estados del despacho I-ingresado,D-despachado,R-reproceso';
comment on column TB_SAP_TRAMAS_REC_CAS.estado_reproceso
  is 'estados del reproceso D-despachado,R-reproceso';
    
-- Create/Recreate primary, unique and foreign key constraints 
alter table TB_SAP_TRAMAS_REC_CAS
  add constraint ID_TRAMA primary key (ID_TRAMA)
  using index 
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
