create sequence SEQ_ID_POLIZA_CASTIGO_SAP
minvalue 0
maxvalue 99
start with 98
increment by -1
nocache;


create sequence SEQ_ID_POLIZA_RECLA_SAP
minvalue 0
maxvalue 99
start with 1
increment by 1
cache 20;
