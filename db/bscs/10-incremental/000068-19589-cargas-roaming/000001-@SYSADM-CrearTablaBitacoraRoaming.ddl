-- Create table
create table GSI_BITA_ROAMING_TMP 
(
  co_id              VARCHAR2(50),
  s_p_number_address VARCHAR2(20),
  id_servicio        VARCHAR2(20),
  sesiones           NUMBER,
  id_subproducto     VARCHAR2(20),
  monto              NUMBER(13,5),
  a_cargar           VARCHAR2(2000),
  sn_code            VARCHAR2(10),
  tipo_prod          VARCHAR2(20),
  elimina_reg        VARCHAR2(2),
  observacion        VARCHAR2(2000)
)
tablespace BILLING_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_BITA_ROAMING_TMP.co_id
  is 'cod_id de la cuenta roaming';
comment on column GSI_BITA_ROAMING_TMP.s_p_number_address
  is 'numero de la cuenta roaming';
comment on column GSI_BITA_ROAMING_TMP.id_servicio
  is 'servicio que identifica a la cuenta roaming';
comment on column GSI_BITA_ROAMING_TMP.sesiones
  is 'conteo de sesiones que a tenido la cuenta';
comment on column GSI_BITA_ROAMING_TMP.id_subproducto
  is 'id_subproducto que identifica la cuenta';
comment on column GSI_BITA_ROAMING_TMP.monto
  is 'valor que genero la cuenta';
comment on column GSI_BITA_ROAMING_TMP.a_cargar
  is 'valores finales a procesar';
comment on column GSI_BITA_ROAMING_TMP.sn_code
  is 'identifica sncode para el tipo de roaming ';
comment on column GSI_BITA_ROAMING_TMP.tipo_prod
  is 'tipo de producto roaming';
comment on column GSI_BITA_ROAMING_TMP.elimina_reg
  is '''S'' no aplica a la carga, ''N'' si aplica a la carga';
comment on column GSI_BITA_ROAMING_TMP.observacion
  is 'comentario para identificar porque no procede';
