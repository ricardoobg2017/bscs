-- Create table
create table GSI_RESU_ROAMING_TMP 
(
  tipo_prod  VARCHAR2(20),
  sn_code    INTEGER,
  reg_proce  NUMBER,
  monto_resu NUMBER(13,5),
  monto_fees NUMBER(13,5),
  proceso    NUMBER,
  estado     VARCHAR2(2),
  detalle    VARCHAR2(2000),
  fec_corte  DATE
)
tablespace BILLING_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_RESU_ROAMING_TMP.tipo_prod
  is 'tipo de producto roaming';
comment on column GSI_RESU_ROAMING_TMP.sn_code
  is 'identifica sncode para el tipo de roaming ';
comment on column GSI_RESU_ROAMING_TMP.reg_proce
  is 'cantidad de registros procesados';
comment on column GSI_RESU_ROAMING_TMP.monto_resu
  is 'monto total que deben procesar';
comment on column GSI_RESU_ROAMING_TMP.monto_fees
  is 'monto total que se cargo en la tabla fees';
comment on column GSI_RESU_ROAMING_TMP.proceso
  is 'numero de procesos que ejecutara';
comment on column GSI_RESU_ROAMING_TMP.estado
  is '''I'' ingresado, ''P'' procesado';
comment on column GSI_RESU_ROAMING_TMP.detalle
  is 'identifica el Remark que sera enviado a la fees.';
comment on column GSI_RESU_ROAMING_TMP.fec_corte
  is 'Fecha del corte del periodo';
