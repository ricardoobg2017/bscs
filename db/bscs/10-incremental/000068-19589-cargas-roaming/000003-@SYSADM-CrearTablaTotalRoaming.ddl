-- Create table
create table GSI_TOTAL_ROAMING_TMP 
(
  co_id     VARCHAR2(50),
  sn_code   VARCHAR2(10),
  valor     NUMBER(13,5),
  tipo_prod VARCHAR2(20)
)
tablespace BILLING_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_TOTAL_ROAMING_TMP.co_id
  is 'identifica el co_id de la cuenta roaming';
comment on column GSI_TOTAL_ROAMING_TMP.sn_code
  is 'identifica sncode para el tipo de roaming ';
comment on column GSI_TOTAL_ROAMING_TMP.valor
  is 'valor que genero la cuenta';
comment on column GSI_TOTAL_ROAMING_TMP.tipo_prod
  is 'tipo de producto roaming';
