-- Create table
create table CO_JOURNAL_PROCESA_FECHAS
(
  SECUENCIA     NUMBER not null,
  REGION        VARCHAR2(10),
  FECHA_INGRESO DATE not null,
  FECHA_DESDE   DATE not null,
  FECHA_HASTA   DATE,
  FECHA_PROCESO DATE,
  ESTADO        VARCHAR2(1)
)
tablespace DATA2
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CO_JOURNAL_PROCESA_FECHAS
  add constraint PK_JOURNAL_1 primary key (SECUENCIA)
  using index 
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_FECHAS_JOURNAL on CO_JOURNAL_PROCESA_FECHAS (FECHA_DESDE, FECHA_HASTA)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IDX_FEC_ING_JOU on CO_JOURNAL_PROCESA_FECHAS (FECHA_INGRESO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );