-- Create table
create table CO_JOURNAL_PROCESA_FECHAS
(
  secuencia     NUMBER not null,
  region        VARCHAR2(10),
  fecha_ingreso DATE not null,
  fecha_desde   DATE not null,
  fecha_hasta   DATE,
  fecha_proceso DATE,
  estado        VARCHAR2(1),
  email         VARCHAR2(100)
)
tablespace DATA2
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CO_JOURNAL_PROCESA_FECHAS.estado
  is 'P:Pendiente F:Finalizado';
-- Create/Recreate primary, unique and foreign key constraints 
alter table CO_JOURNAL_PROCESA_FECHAS
  add constraint PK_JOURNAL_1 primary key (SECUENCIA)
  using index 
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_FECHAS_JOURNAL on CO_JOURNAL_PROCESA_FECHAS (FECHA_DESDE, FECHA_HASTA)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FEC_ING_JOU on CO_JOURNAL_PROCESA_FECHAS (FECHA_INGRESO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
