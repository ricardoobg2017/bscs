-- Create table
create table OCC_PARAMETROS_GRALES
(
  CODIGO      VARCHAR2(30),
  DESCRIPCION VARCHAR2(300),
  VALOR1      VARCHAR2(1000),
  VALOR2      VARCHAR2(1000),
  ESTADO      VARCHAR2(2)
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column OCC_PARAMETROS_GRALES.CODIGO
  is 'C�digo de par�metro';
comment on column OCC_PARAMETROS_GRALES.DESCRIPCION
  is 'Descripci�n del par�metro';
comment on column OCC_PARAMETROS_GRALES.VALOR1
  is 'Valor 1 parametrizado';
comment on column OCC_PARAMETROS_GRALES.VALOR2
  is 'Valor 2 parametrizado';
comment on column OCC_PARAMETROS_GRALES.ESTADO
  is 'Estado A = Activo, I = Inactivo';
-- Create/Recreate indexes 
create index IDX_PARAM_CARGA_OCC on OCC_PARAMETROS_GRALES (CODIGO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
