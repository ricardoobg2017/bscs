-- Create table
create table BL_CARGA_OCC_TMP_V2
(
  ID_REQUERIMIENTO NUMBER,
  CO_ID            NUMBER,
  AMOUNT           VARCHAR2(250),
  SNCODE           NUMBER,
  STATUS           VARCHAR2(1),
  ERROR            VARCHAR2(200),
  HILO             INTEGER,
  CUSTOMER_ID      INTEGER,
  CUSTCODE         VARCHAR2(24),
  OBSERVACION      VARCHAR2(100),
  SERVICIO         VARCHAR2(24)
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BL_CARGA_OCC_TMP_V2.ID_REQUERIMIENTO
  is 'Id requerimiento del SCP de AXIS';
comment on column BL_CARGA_OCC_TMP_V2.CO_ID
  is 'Cod_Id viaja desde AXIS';
comment on column BL_CARGA_OCC_TMP_V2.AMOUNT
  is 'Valor de cada registro';
comment on column BL_CARGA_OCC_TMP_V2.SNCODE
  is 'Creditos (-) = 46 y Cargos (+) = 38';
comment on column BL_CARGA_OCC_TMP_V2.STATUS
  is 'A = Activo - I = INactivo';
comment on column BL_CARGA_OCC_TMP_V2.ERROR
  is 'Descripcion del error generado';
comment on column BL_CARGA_OCC_TMP_V2.HILO
  is 'Hilo para ejecucion mediante Shell';
comment on column BL_CARGA_OCC_TMP_V2.CUSTOMER_ID
  is 'Valor de BSCS';
comment on column BL_CARGA_OCC_TMP_V2.CUSTCODE
  is 'Viaja desde AXIS';
comment on column BL_CARGA_OCC_TMP_V2.OBSERVACION
  is 'Observacion adicional';
comment on column BL_CARGA_OCC_TMP_V2.SERVICIO
  is 'Servicio del archivo';
-- Create/Recreate indexes 
create index IDX_COID_OCC_V2 on BL_CARGA_OCC_TMP_V2 (CO_ID)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CUSTCODE_OCC_V2 on BL_CARGA_OCC_TMP_V2 (CUSTCODE)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CUSTOMER_OCC_V2 on BL_CARGA_OCC_TMP_V2 (CUSTOMER_ID)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_REQ_OCC_V2 on BL_CARGA_OCC_TMP_V2 (ID_REQUERIMIENTO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_SERV_OCC_V2 on BL_CARGA_OCC_TMP_V2 (SERVICIO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
