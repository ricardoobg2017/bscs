alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_inpool; 
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_claro; 
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_movistar;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_alegro;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_fijas;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_771;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_776;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_777;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_778;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_779;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_780;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column fact_781;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column cant_cuentas;
alter table GSI_TARIFAS_BSCS_HISTORICA drop column cant_clientes;

alter table GSI_TARIFAS_BSCS_HISTORICA
add tarifa_basica           VARCHAR2(15)
add features_fijos          VARCHAR2(1000)
add vpn_aprovisionador      VARCHAR2(1000)
add vpn_aprovisionador_tipo VARCHAR2(1000)
add vpn_holding             VARCHAR2(1000)
add vpn_holding_tipo        VARCHAR2(1000)
add vpn_holding_ilim        VARCHAR2(1000)
add vpn_holding_ilim_tipo   VARCHAR2(1000)
add id_detalle_plan         NUMBER
add id_subproducto          VARCHAR2(30)
add fu_pack_id              VARCHAR2(1500);


comment on column GSI_TARIFAS_BSCS_HISTORICA.tarifa_basica
  is 'Tarifa basica del plan';
comment on column GSI_TARIFAS_BSCS_HISTORICA.features_fijos
  is 'Features fijos asociados al plan concatenado de la siguiente forma ID_TIPO_DETALLE_SERV | NOMBRE_FEATURE | MENSAJES | COSTO | ADICIONAL';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_aprovisionador
  is 'Contiene el feature aprovisionador de la tarifa vpn';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_aprovisionador_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_holding
  is 'Contiene el feature para holding';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_holding_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_holding_ilim
  is 'Contiene el feature holding ilimitados';
comment on column GSI_TARIFAS_BSCS_HISTORICA.vpn_holding_ilim_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_HISTORICA.id_detalle_plan
  is 'Id detalle del plan';
comment on column GSI_TARIFAS_BSCS_HISTORICA.id_subproducto
  is 'Id del subproducto del plan';
comment on column GSI_TARIFAS_BSCS_HISTORICA.fu_pack_id
  is 'Contiene los siguientes datos concatenados <des | accessfee | fu_pack_id | long_name | free_units_volume>';
  
