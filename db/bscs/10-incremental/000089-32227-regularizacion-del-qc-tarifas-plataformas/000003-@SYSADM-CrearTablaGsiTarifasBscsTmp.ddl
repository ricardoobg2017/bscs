-- Create table
create table GSI_TARIFAS_BSCS_TMP
(
  id_plan                 VARCHAR2(60),
  descripcion             VARCHAR2(60) not null,
  tmcode                  INTEGER not null,
  "1700"                  NUMBER,
  "1800"                  NUMBER,
  claro                   NUMBER,
  claro_inpool            NUMBER,
  claro_776               NUMBER,
  claro_777               NUMBER,
  claro_778               NUMBER,
  claro_779               NUMBER,
  claro_780               NUMBER,
  claro_781               NUMBER,
  movistar                NUMBER,
  alegro                  NUMBER,
  fija                    NUMBER,
  chile                   NUMBER,
  cuba                    NUMBER,
  espana                  NUMBER,
  italia                  NUMBER,
  canada                  NUMBER,
  usa                     NUMBER,
  europa                  NUMBER,
  japon                   NUMBER,
  maritima                NUMBER,
  mexico                  NUMBER,
  mexico_cel              NUMBER,
  pacto_andino            NUMBER,
  restoamerica            NUMBER,
  restodelmundo           NUMBER,
  zona_especial           NUMBER,
  alemania                NUMBER,
  argentina               NUMBER,
  bolivia                 NUMBER,
  brasil                  NUMBER,
  china                   NUMBER,
  colombia                NUMBER,
  costa_rica              NUMBER,
  francia                 NUMBER,
  com_771                 NUMBER,
  com_776                 NUMBER,
  com_777                 NUMBER,
  com_778                 NUMBER,
  com_779                 NUMBER,
  com_780                 NUMBER,
  com_781                 NUMBER,
  toll                    VARCHAR2(20),
  toll_com                VARCHAR2(20),
  tipo                    VARCHAR2(20),
  cant_cuentas            NUMBER,
  cant_clientes           NUMBER,
  fecha_creacion          DATE,
  recurso                 VARCHAR2(100),
  id_plan_pareja          VARCHAR2(10),
  estado                  VARCHAR2(1),
  com_inpool              NUMBER,
  com_claro               NUMBER,
  com_movistar            NUMBER,
  com_alegro              NUMBER,
  com_fijas               NUMBER,
  fact_inpool             NUMBER,
  fact_claro              NUMBER,
  fact_movistar           NUMBER,
  fact_alegro             NUMBER,
  fact_fijas              NUMBER,
  categoria_amx           VARCHAR2(20),
  clase_amx               VARCHAR2(20),
  tipo_cliente            VARCHAR2(40),
  tipo_plan               VARCHAR2(40),
  segmento                VARCHAR2(40),
  plan_suptel             VARCHAR2(40),
  fact_771                NUMBER,
  fact_776                NUMBER,
  fact_777                NUMBER,
  fact_778                NUMBER,
  fact_779                NUMBER,
  fact_780                NUMBER,
  fact_781                NUMBER,
  id_promocion            NUMBER,
  desc_promocion          VARCHAR2(300),
  bgh_porta_porta         VARCHAR2(100),
  bgh_porta_fijas         VARCHAR2(100),
  bgh_porta_movistar      VARCHAR2(100),
  bgh_porta_alegro        VARCHAR2(100),
  tarifa_basica           VARCHAR2(15),
  features_fijos          VARCHAR2(1000),
  vpn_aprovisionador      VARCHAR2(1000),
  vpn_aprovisionador_tipo VARCHAR2(1000),
  vpn_holding             VARCHAR2(1000),
  vpn_holding_tipo        VARCHAR2(1000),
  vpn_holding_ilim        VARCHAR2(1000),
  vpn_holding_ilim_tipo   VARCHAR2(1000),
  id_detalle_plan         NUMBER,
  id_subproducto          VARCHAR2(30),
  fu_pack_id              VARCHAR2(1500),
  fecha_act_clientes      DATE
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_TARIFAS_BSCS_TMP.id_plan
  is 'Identificador del plan';
comment on column GSI_TARIFAS_BSCS_TMP.descripcion
  is 'Descripcion del plan';
comment on column GSI_TARIFAS_BSCS_TMP.tmcode
  is 'TMCODE del plan';
comment on column GSI_TARIFAS_BSCS_TMP."1700"
  is 'Costo al marcar 1700';
comment on column GSI_TARIFAS_BSCS_TMP."1800"
  is 'Costo al marcar 1800';
comment on column GSI_TARIFAS_BSCS_TMP.claro
  is 'Costo de la tarifa a claro';
comment on column GSI_TARIFAS_BSCS_TMP.claro_inpool
  is 'Costo de la tarifa claro que viene en la factura inpool';
comment on column GSI_TARIFAS_BSCS_TMP.claro_776
  is 'Costo de la tarifa vpn claro_776';
comment on column GSI_TARIFAS_BSCS_TMP.claro_777
  is 'Costo de la tarifa vpn claro_777';
comment on column GSI_TARIFAS_BSCS_TMP.claro_778
  is 'Costo de la tarifa vpn claro_778';
comment on column GSI_TARIFAS_BSCS_TMP.claro_779
  is 'Costo de la tarifa vpn claro_779';
comment on column GSI_TARIFAS_BSCS_TMP.claro_780
  is 'Costo de la tarifa vpn claro_780';
comment on column GSI_TARIFAS_BSCS_TMP.claro_781
  is 'Costo de la tarifa vpn claro_781';
comment on column GSI_TARIFAS_BSCS_TMP.movistar
  is 'Costo de la tarifa a movistar';
comment on column GSI_TARIFAS_BSCS_TMP.alegro
  is 'Costo de la tarifa a alegro';
comment on column GSI_TARIFAS_BSCS_TMP.fija
  is 'Costo de la tarifa a fijo';
comment on column GSI_TARIFAS_BSCS_TMP.chile
  is 'Costo de la tarifa a Chile';
comment on column GSI_TARIFAS_BSCS_TMP.cuba
  is 'Costo de la tarifa a Cuba';
comment on column GSI_TARIFAS_BSCS_TMP.espana
  is 'Costo de la tarifa a Espana';
comment on column GSI_TARIFAS_BSCS_TMP.italia
  is 'Costo de la tarifa a Italia';
comment on column GSI_TARIFAS_BSCS_TMP.canada
  is 'Costo de la tarifa a Canada';
comment on column GSI_TARIFAS_BSCS_TMP.usa
  is 'Costo de la tarifa a Usa';
comment on column GSI_TARIFAS_BSCS_TMP.europa
  is 'Costo de la tarifa a Europa';
comment on column GSI_TARIFAS_BSCS_TMP.japon
  is 'Costo de la tarifa a Japon';
comment on column GSI_TARIFAS_BSCS_TMP.maritima
  is 'Costo de la tarifa a maritima';
comment on column GSI_TARIFAS_BSCS_TMP.mexico
  is 'Costo de la tarifa a Mexico';
comment on column GSI_TARIFAS_BSCS_TMP.mexico_cel
  is 'Costo de la tarifa a cel de Mexico';
comment on column GSI_TARIFAS_BSCS_TMP.pacto_andino
  is 'Costo de la tarifa a pacto andino';
comment on column GSI_TARIFAS_BSCS_TMP.restoamerica
  is 'Costo de la tarifa a resto America';
comment on column GSI_TARIFAS_BSCS_TMP.restodelmundo
  is 'Costo de la tarifa a resto de mundo';
comment on column GSI_TARIFAS_BSCS_TMP.zona_especial
  is 'Costo de la tarifa a zona especial';
comment on column GSI_TARIFAS_BSCS_TMP.alemania
  is 'Costo de la tarifa a Alemania';
comment on column GSI_TARIFAS_BSCS_TMP.argentina
  is 'Costo de la tarifa a Argentina';
comment on column GSI_TARIFAS_BSCS_TMP.bolivia
  is 'Costo de la tarifa a Bolivia';
comment on column GSI_TARIFAS_BSCS_TMP.brasil
  is 'Costo de la tarifa a Brasil';
comment on column GSI_TARIFAS_BSCS_TMP.china
  is 'Costo de la tarifa a China';
comment on column GSI_TARIFAS_BSCS_TMP.colombia
  is 'Costo de la tarifa a Colombia';
comment on column GSI_TARIFAS_BSCS_TMP.costa_rica
  is 'Costo de la tarifa a Costa Rica';
comment on column GSI_TARIFAS_BSCS_TMP.francia
  is 'Costo de la tarifa a Francia';
comment on column GSI_TARIFAS_BSCS_TMP.com_771
  is 'Costo de la tarifa que viene de comercial por vpn 771';
comment on column GSI_TARIFAS_BSCS_TMP.com_776
  is 'Costo de la tarifa que viene de comercial por vpn 776';
comment on column GSI_TARIFAS_BSCS_TMP.com_777
  is 'Costo de la tarifa que viene de comercial por vpn 777';
comment on column GSI_TARIFAS_BSCS_TMP.com_778
  is 'Costo de la tarifa que viene de comercial por vpn 778';
comment on column GSI_TARIFAS_BSCS_TMP.com_779
  is 'Costo de la tarifa que viene de comercial por vpn 779';
comment on column GSI_TARIFAS_BSCS_TMP.com_780
  is 'Costo de la tarifa que viene de comercial por vpn 780';
comment on column GSI_TARIFAS_BSCS_TMP.com_781
  is 'Costo de la tarifa que viene de comercial por vpn 781';
comment on column GSI_TARIFAS_BSCS_TMP.toll
  is 'TOLL de la tarifa';
comment on column GSI_TARIFAS_BSCS_TMP.toll_com
  is 'TOLL configurado en comercial';
comment on column GSI_TARIFAS_BSCS_TMP.tipo
  is 'Identifica el tipo de tarifa si es vpn - aut_puro - aut_puro_vpn - bulk';
comment on column GSI_TARIFAS_BSCS_TMP.cant_cuentas
  is 'Cantidad de cuentas';
comment on column GSI_TARIFAS_BSCS_TMP.cant_clientes
  is 'Cantidad de clientes';
comment on column GSI_TARIFAS_BSCS_TMP.fecha_creacion
  is 'Fecha en la que se creo el plan';
comment on column GSI_TARIFAS_BSCS_TMP.recurso
  is 'Recurso que creo el plan (Siempre y cuando exista en la tabla parametro_planes de axis)';
comment on column GSI_TARIFAS_BSCS_TMP.id_plan_pareja
  is 'El plan pareja asociado';
comment on column GSI_TARIFAS_BSCS_TMP.estado
  is 'Estado del plan ubicado en la tabla ge_planes de axis';
comment on column GSI_TARIFAS_BSCS_TMP.com_inpool
  is 'Valor de la tarifa inpool que viene de comercial. Ubicado en la tabla GE_TARIFAS_OPERADORAS de axis';
comment on column GSI_TARIFAS_BSCS_TMP.com_claro
  is 'Valor de la tarifa claro que viene de comercial. Ubicado en la tabla GE_TARIFAS_OPERADORAS de axis';
comment on column GSI_TARIFAS_BSCS_TMP.com_movistar
  is 'Valor de la tarifa movistar que viene de comercial. Ubicado en la tabla GE_TARIFAS_OPERADORAS de axis';
comment on column GSI_TARIFAS_BSCS_TMP.com_alegro
  is 'Valor de la tarifa alegro que viene de comercial. Ubicado en la tabla GE_TARIFAS_OPERADORAS de axis';
comment on column GSI_TARIFAS_BSCS_TMP.com_fijas
  is 'Valor de la tarifa fija local que viene de comercial. Ubicado en la tabla GE_TARIFAS_OPERADORAS de axis';
comment on column GSI_TARIFAS_BSCS_TMP.fact_inpool
  is 'Campo usado para validaciones internas usando la formula trunc(COM_inpool/60,5)*60 para el tipo VPN - BULK :: FACT_INPOOL=COM_inpool para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_claro
  is 'Campo usado para validaciones internas usando la formula trunc(COM_CLARO/60,5)*60 para el tipo VPN - BULK :: FACT_CLARO=COM_CLARO para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_movistar
  is 'Campo usado para validaciones internas usando la formula trunc(0.0639/60,5)*60 + trunc((COM_movistar- 0.0639)/60,5)*60 :: tipo VPN - BULK :: FACT_MOVISTAR=COM_movistar para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_alegro
  is 'Campo usado para validaciones internas usando la formula trunc(0.0915/60,5)*60 + trunc((COM_alegro- 0.0915)/60,5)*60 :: tipo VPN - BULK :: FACT_ALEGRO=COM_alegro para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_fijas
  is 'Campo usado para validaciones internas usando la formula trunc(0.0162/60,5)*60 + trunc((COM_FIJAS- 0.0162)/60,5)*60 para el tipo VPN - BULK :: FACT_FIJAS=COM_FIJAS para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.categoria_amx
  is 'Categoria AMX del plan sacado de la tabla Ge_Categorias_Amx de axis';
comment on column GSI_TARIFAS_BSCS_TMP.clase_amx
  is 'Clase AMX del plan sacado de la tabla ge_clases_amx de axis';
comment on column GSI_TARIFAS_BSCS_TMP.tipo_cliente
  is 'Tipo de cliente que aplica al plan sacado de la tabla ge_tipo_clientes de axis';
comment on column GSI_TARIFAS_BSCS_TMP.tipo_plan
  is 'Tipo de plan sacado de la tabla ge_tipos_planes de axis';
comment on column GSI_TARIFAS_BSCS_TMP.segmento
  is 'Segmento del plan sacado de la tabla ge_segmentos_planes de axis';
comment on column GSI_TARIFAS_BSCS_TMP.plan_suptel
  is 'Plan Suptel sacado de la tabla GE_TIPO_PLAN_SUPTEL de axis';
comment on column GSI_TARIFAS_BSCS_TMP.fact_771
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_771/60,5)*60 para el tipo VPN - BULK :: FACT_771=CLARO_771 para el tipo AUT_PURO_VPN - AUT-PURO (No existe el campo CLARO_771)';
comment on column GSI_TARIFAS_BSCS_TMP.fact_776
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_776/60,5)*60 para el tipo VPN - BULK :: FACT_776=CLARO_776 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_777
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_777/60,5)*60 para el tipo VPN - BULK :: FACT_777=CLARO_777 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_778
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_778/60,5)*60 para el tipo VPN - BULK :: FACT_778=CLARO_778 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_779
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_779/60,5)*60 para el tipo VPN - BULK :: FACT_779=CLARO_779 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_780
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_780/60,5)*60 para el tipo VPN - BULK :: FACT_780=CLARO_780 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.fact_781
  is 'Campo usado para validaciones internas usando la formula trunc(b.CLARO_781/60,5)*60 para el tipo VPN - BULK :: FACT_781=CLARO_781 para el tipo AUT_PURO_VPN - AUT-PURO';
comment on column GSI_TARIFAS_BSCS_TMP.id_promocion
  is 'Id de la promocion';
comment on column GSI_TARIFAS_BSCS_TMP.desc_promocion
  is 'Descripcion de la promocion (Doble de minutos y Llamadas ilimitadas)';
comment on column GSI_TARIFAS_BSCS_TMP.bgh_porta_porta
  is 'Costo de la tarifa del plan BGH de claro a claro';
comment on column GSI_TARIFAS_BSCS_TMP.bgh_porta_fijas
  is 'Costo de la tarifa del plan BGH de claro a fijas';
comment on column GSI_TARIFAS_BSCS_TMP.bgh_porta_movistar
  is 'Costo de la tarifa del plan BGH de claro a movistar';
comment on column GSI_TARIFAS_BSCS_TMP.bgh_porta_alegro
  is 'Costo de la tarifa del plan BGH de claro a alegro';
comment on column GSI_TARIFAS_BSCS_TMP.tarifa_basica
  is 'Tarifa basica del plan';
comment on column GSI_TARIFAS_BSCS_TMP.features_fijos
  is 'Features fijos asociados al plan concatenado de la siguiente forma ID_TIPO_DETALLE_SERV | NOMBRE_FEATURE | MENSAJES | COSTO | ADICIONAL';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_aprovisionador
  is 'Contiene el feature aprovisionador de la tarifa vpn';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_aprovisionador_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_holding
  is 'Contiene el feature para holding';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_holding_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_holding_ilim
  is 'Contiene el feature holding ilimitados';
comment on column GSI_TARIFAS_BSCS_TMP.vpn_holding_ilim_tipo
  is 'Contiene el tipo del feature aprovisionador de la tarifa vpn - ADICIONAL = no obligatorio y actualizable - FIJO = obligatorio y no actualizable - FIJO VARIABLE = obligatorio y actualizable - MAL_CONFIGURADO  = no tiene asignado y obligacion (origen cl_servicios_planes de axis)';
comment on column GSI_TARIFAS_BSCS_TMP.id_detalle_plan
  is 'Id detalle del plan';
comment on column GSI_TARIFAS_BSCS_TMP.id_subproducto
  is 'Id del subproducto del plan';
comment on column GSI_TARIFAS_BSCS_TMP.fu_pack_id
  is 'Contiene los siguientes datos concatenados <des | accessfee | fu_pack_id | long_name | free_units_volume>';
comment on column GSI_TARIFAS_BSCS_TMP.fecha_act_clientes
  is 'Fecha de actualizacion de los clientes y las cuentas de AXIS';
