
create table PB_COMPLAIN_TEMPORAL
(
  NOMBRE2     VARCHAR2(1000),
  VALOR       NUMBER,
  CUSTOMER_ID VARCHAR2(50),
  CUSTCODE    VARCHAR2(50),
  OHREFNUM    VARCHAR2(100),
  TIPO        VARCHAR2(50),
  NOMBRE      VARCHAR2(1000),
  SERVICIO    VARCHAR2(100),
  DESCUENTO   NUMBER,
  COBRADO     NUMBER,
  CTACLBLEP   VARCHAR2(30),
  CTA_DEVOL   VARCHAR2(30),
  TIPO_IVA    VARCHAR2(30)
)

tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
