-- Creacion de tabla similar a la tabla Fees para los registros de NC


create table FEES_CARGAS_NC
(
  CUSTOMER_ID       INTEGER not null,
  SEQNO             INTEGER not null,
  FEE_TYPE          VARCHAR2(1) not null,
  AMOUNT            FLOAT,
  REMARK            VARCHAR2(2000),
  GLCODE            VARCHAR2(30),
  ENTDATE           DATE,
  PERIOD            INTEGER,
  USERNAME          VARCHAR2(16),
  VALID_FROM        DATE,
  JOBCOST           INTEGER,
  BILL_FMT          VARCHAR2(30),
  SERVCAT_CODE      VARCHAR2(10),
  SERV_CODE         VARCHAR2(10),
  SERV_TYPE         VARCHAR2(3),
  CO_ID             INTEGER,
  AMOUNT_GROSS      FLOAT,
  CURRENCY          INTEGER not null,
  GLCODE_DISC       VARCHAR2(30),
  JOBCOST_ID_DISC   INTEGER,
  GLCODE_MINCOM     VARCHAR2(30),
  JOBCOST_ID_MINCOM INTEGER,
  REC_VERSION       INTEGER default (0) not null,
  CDR_ID            INTEGER,
  CDR_SUB_ID        INTEGER,
  UDR_BASEPART_ID   INTEGER,
  UDR_CHARGEPART_ID INTEGER,
  TMCODE            INTEGER,
  VSCODE            INTEGER,
  SPCODE            INTEGER,
  SNCODE            INTEGER,
  EVCODE            INTEGER,
  FEE_CLASS         INTEGER not null,
  FU_PACK_ID        INTEGER,
  FUP_VERSION       INTEGER,
  FUP_SEQ           INTEGER,
  VERSION           INTEGER,
  FREE_UNITS_NUMBER FLOAT,
  FECHA_REGISTRO    DATE default SYSDATE not null,
  LASTMODDATE       DATE,
  ESTADO            VARCHAR2(1),
  GENERA_NC         VARCHAR2(1),
  COMPANIA          VARCHAR2(1),
  PRODUCTO          VARCHAR2(100)
)
tablespace DATA5
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 120K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table FEES_CARGAS_NC
  is 'Business Scenario Charged Parties Table
The fees_cargas_nc table stores the administrative and the financial fees_cargas_nc, which have to be charged to a customer.';
-- Add comments to the columns 
comment on column FEES_CARGAS_NC.CUSTOMER_ID
  is 'internal pointer to customer';
comment on column FEES_CARGAS_NC.SEQNO
  is 'seq. no. for this customer';
comment on column FEES_CARGAS_NC.FEE_TYPE
  is 'type of fee: R=recurring, N=non-recurring';
comment on column FEES_CARGAS_NC.AMOUNT
  is 'fee amount';
comment on column FEES_CARGAS_NC.REMARK
  is 'remark';
comment on column FEES_CARGAS_NC.GLCODE
  is 'link to gl-code';
comment on column FEES_CARGAS_NC.ENTDATE
  is 'entry date';
comment on column FEES_CARGAS_NC.PERIOD
  is 'Number of billing runs.
The number of times that the customer is credited/charged with the stated amount.
Domain:
>= 1: The amount will be credited/charged with the next billing run. After the billing run the period will be reduced by 1. This proceeding will be repeated with every billing run, until PERIOD has the value "0".
=  0: The amount will not be credited/charged anymore. The fifth flag in APP_MODULES.PARAMS for entry "BCH" configures if these entries will be removed or not.
= -1: The amount will be credited/charged with every billing run.';
comment on column FEES_CARGAS_NC.USERNAME
  is 'User Name
The fees_cargas_nc.USERNAME attribute specifies the name of the BSCS user entering the fee.
Further Information:
- USERS table comment';
comment on column FEES_CARGAS_NC.VALID_FROM
  is 'date, when the fee should be billed';
comment on column FEES_CARGAS_NC.JOBCOST
  is 'foreign key to jobcost';
comment on column FEES_CARGAS_NC.BILL_FMT
  is 'Bill format string for this fee.';
comment on column FEES_CARGAS_NC.SERVCAT_CODE
  is 'Service Category, FK SERVICECATCODE';
comment on column FEES_CARGAS_NC.SERV_CODE
  is 'Service Code, FK SERVICECATCODE';
comment on column FEES_CARGAS_NC.SERV_TYPE
  is 'Service Type, FK to SERVICECATCODE';
comment on column FEES_CARGAS_NC.CO_ID
  is 'Contract ID, FK to CONTRACT_ALL';
comment on column FEES_CARGAS_NC.AMOUNT_GROSS
  is 'Fee Gross Amount';
comment on column FEES_CARGAS_NC.CURRENCY
  is 'Defines currency of column AMOUNT, AMOUNT_GROSS.
FK: FORCURR';
comment on column FEES_CARGAS_NC.GLCODE_DISC
  is 'General Ledger Account for Discounts.';
comment on column FEES_CARGAS_NC.JOBCOST_ID_DISC
  is 'Jobcost Identifier for Discounts.';
comment on column FEES_CARGAS_NC.GLCODE_MINCOM
  is 'General Ledger Account for Minimal Commitments.';
comment on column FEES_CARGAS_NC.JOBCOST_ID_MINCOM
  is 'Jobcost Identifier for Minimal Commitments.';
comment on column FEES_CARGAS_NC.REC_VERSION
  is 'Counter for multiuser access';
comment on column FEES_CARGAS_NC.CDR_ID
  is 'Call Data Record Identifier.';
comment on column FEES_CARGAS_NC.CDR_SUB_ID
  is 'Call Data Record Sub Identifier.';
comment on column FEES_CARGAS_NC.UDR_BASEPART_ID
  is 'UDR Base Part Identifier.';
comment on column FEES_CARGAS_NC.UDR_CHARGEPART_ID
  is 'UDR Charge Part Identifier.';
comment on column FEES_CARGAS_NC.TMCODE
  is 'Rate plan';
comment on column FEES_CARGAS_NC.VSCODE
  is 'Rate plan version';
comment on column FEES_CARGAS_NC.SPCODE
  is 'Service package';
comment on column FEES_CARGAS_NC.SNCODE
  is 'Service';
comment on column FEES_CARGAS_NC.EVCODE
  is 'Event code';
comment on column FEES_CARGAS_NC.FEE_CLASS
  is 'Fee class.
Gives the information about the reason for the fee generation.
Domain:
1 - the fee is a credit on the invoice
2 - the fee is a credit on the call
3 - the fee is a credit/charge, based on an event service, generated by a user (OCC)
4 - the fee is a charge, based on an event service, generated by the system
5 - the fee is a free unit credit';
comment on column FEES_CARGAS_NC.FU_PACK_ID
  is 'Free Units Package Identifier.';
comment on column FEES_CARGAS_NC.FUP_VERSION
  is 'Free Units Package Version.';
comment on column FEES_CARGAS_NC.FUP_SEQ
  is 'FUP Element Sequence number.';
comment on column FEES_CARGAS_NC.VERSION
  is 'FUP Element Version.';
comment on column FEES_CARGAS_NC.FREE_UNITS_NUMBER
  is 'Number of Free Units.';
comment on column FEES_CARGAS_NC.FECHA_REGISTRO
  is 'Insertion date';
comment on column FEES_CARGAS_NC.LASTMODDATE
  is 'Last modification date';
comment on column FEES_CARGAS_NC.ESTADO
  is 'Estado';
-- Create/Recreate primary, unique and foreign key constraints 
alter table FEES_CARGAS_NC
  add constraint PKFEES_CARGAS_NC primary key (CUSTOMER_ID, SEQNO)
  using index 
  tablespace IND5
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table FEES_CARGAS_NC
  add constraint FE_CUID_NC foreign key (CUSTOMER_ID)
  references CUSTOMER_ALL (CUSTOMER_ID);
/*alter table FEES_CARGAS_NC
  add constraint FE_USNM_NC foreign key (USERNAME)
  references USERS_20141109 (USERNAME)
  disable;*/
-- Create/Recreate indexes 
create index F_CARGAS_NC_CO_ID on FEES_CARGAS_NC (CO_ID)
  tablespace IND5
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index F_CARGAS_NC_CO_INSDATE on FEES_CARGAS_NC (FECHA_REGISTRO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index F_CARGAS_NC_LASTMODDATE on FEES_CARGAS_NC (LASTMODDATE)
  tablespace IND5
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index F_CARGAS_NC_SERVS on FEES_CARGAS_NC (SERVCAT_CODE, SERV_CODE, SERV_TYPE)
  tablespace IND5
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index F_CARGAS_NC_USERNAME on FEES_CARGAS_NC (USERNAME)
  tablespace IND5
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
