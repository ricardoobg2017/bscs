-- Create table
create table CL_CLI_NEGOCIACIONES
(
  codigo_doc     VARCHAR2(20),
  customer_id    NUMBER,
  id_negociacion NUMBER,
  identificacion VARCHAR2(20),
  fecha_registro DATE
)
tablespace DATA12
  pctfree 10
  initrans 40
  maxtrans 255
  storage
  (
    initial 896K
    next 1M
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the columns 
comment on column CL_CLI_NEGOCIACIONES.codigo_doc
  is 'CUENTA DEL CLIENTE CODIGO_DOC';
comment on column CL_CLI_NEGOCIACIONES.customer_id
  is 'CUSTOMER_ID';
comment on column CL_CLI_NEGOCIACIONES.id_negociacion
  is 'NUMERO DE LA NEGOCIACION ';
comment on column CL_CLI_NEGOCIACIONES.identificacion
  is 'IDENTIFICACION DEL CLIENTE';
comment on column CL_CLI_NEGOCIACIONES.fecha_registro
  is 'FECHA QUE SE CARGA LA INFORMACION ';
