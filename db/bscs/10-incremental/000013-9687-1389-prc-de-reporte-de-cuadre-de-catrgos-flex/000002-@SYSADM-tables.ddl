-- Create table
create table RPT_REPORTE_CARGO_FLEX
(
  id_servicio          VARCHAR2(20),
  sncode               VARCHAR2(10),
  feature              VARCHAR2(15),
  fecha_occ            DATE,
  fecha_navegacion     DATE,
  costo                VARCHAR2(10),
  estado               VARCHAR2(1),
  observacion_actual   VARCHAR2(200),
  observacion_anterior VARCHAR2(200)
)
tablespace co_rep_dat 
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column RPT_REPORTE_CARGO_FLEX.id_servicio
  is 'NUMERO QUE REALIZA LA NEVACION';
comment on column RPT_REPORTE_CARGO_FLEX.sncode
  is 'CODIGO SN_CODE PARA EL COBRO OCC ';
comment on column RPT_REPORTE_CARGO_FLEX.feature
  is 'NOMBRE DEL FEATURE EN AXIS';
comment on column RPT_REPORTE_CARGO_FLEX.fecha_occ
  is 'FECHA EN QUE SE GENERO EL COBRO ';
comment on column RPT_REPORTE_CARGO_FLEX.fecha_navegacion
  is 'FECHA DE SU PRIMERA NAVEGACION';
comment on column RPT_REPORTE_CARGO_FLEX.costo
  is 'VALOR EN USD SIN IMPUESTPOS PARA OCC X CONTRATO';
comment on column RPT_REPORTE_CARGO_FLEX.estado
  is 'ESTADO; C = NO SE COBRO; F = FINALIZADO O COBRADO';
comment on column RPT_REPORTE_CARGO_FLEX.observacion_actual
  is 'OBSERAVACION ESTADO ATCUAL DE COMO SE ENCONTRABA LA LINEA EN AXIS';
comment on column RPT_REPORTE_CARGO_FLEX.observacion_anterior
  is 'OBSERAVACION ESTADO ATCUAL DE COMO SE ENCONTRABA LA LINEA EN TEC_DETAIL';
comment on table RPT_REPORTE_CARGO_FLEX
  is 'Esta es una tabla temporal para generar reportes para GSIB por cargos FLEX';  
