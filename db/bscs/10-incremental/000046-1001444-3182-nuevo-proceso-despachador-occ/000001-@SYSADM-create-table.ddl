-- Create table
create table CL_PROCESA_OCC_HIST
(
  co_id          INTEGER,
  id_servicio    VARCHAR2(15),
  custcode       VARCHAR2(20),
  customer_id    INTEGER,
  fecha_registro DATE,
  reintentos     NUMBER(1),
  estado_proceso NUMBER(1),
  fecha_proceso  DATE,
  tmcode         INTEGER,
  sncode         INTEGER,
  period         INTEGER,
  valid_from     DATE,
  remark         VARCHAR2(2000),
  amount         FLOAT,
  fee_type       VARCHAR2(1),
  fee_class      INTEGER,
  username       VARCHAR2(16),
  origen         VARCHAR2(100),
  observacion    VARCHAR2(1000)
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  -- Create/Recreate indexes 
create index IDX_PROCESA_CUSTOMER_ID on CL_PROCESA_OCC_HIST (customer_id)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROCESA_FECHA_REG on CL_PROCESA_OCC_HIST (fecha_registro)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  ); 
  
------------------------------------------
create table CL_PROCESA_OCC_TMP
(
  co_id                INTEGER,
  id_servicio          VARCHAR2(15),
  custcode             VARCHAR2(20),
  customer_id          INTEGER,
  tmcode               INTEGER,
  sncode               INTEGER,
  period               INTEGER,
  valid_from           DATE,
  remark               VARCHAR2(2000),
  amount               FLOAT,
  fee_type             VARCHAR2(1),
  fee_class            INTEGER,
  username             VARCHAR2(16),
  fecha_registro       DATE,
  reintentos           NUMBER(1),
  estado_proceso       NUMBER(1),
  origen               VARCHAR2(100),
  observacion          VARCHAR2(1000),
  fecha_prox_reintento DATE,
  tipo_reintento       VARCHAR2(1)
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CL_PROCESA_OCC_TMP.tipo_reintento
  is 'C=CONTROLADO, I= ILIMITADO, T=TERMINADO';
    -- Create/Recreate indexes 
create index IDX_CUST_ID on CL_PROCESA_OCC_TMP(customer_id)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FECHA_REG on CL_PROCESA_OCC_TMP(fecha_registro)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-----------------------
create table GV_PARAMETROS_ERRORES
(
  id_parametro VARCHAR2(30) not null,
  mensaje      VARCHAR2(2000) not null,
  tipo         VARCHAR2(1) not null,
  estado       VARCHAR2(1) not null
)
tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 104K
    next 1M
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the columns 
comment on column GV_PARAMETROS_ERRORES.mensaje
  is 'Mensaje de error';
comment on column GV_PARAMETROS_ERRORES.tipo
  is 'TIPO PARA REINTENTOS=(C=CONTROLADO , I=ILIMITADO, T=TERMINADO) ';
comment on column GV_PARAMETROS_ERRORES.estado
  is 'ESTADO DEL MENSAJE DE ERROR A: Activo, I: Inactivo';
