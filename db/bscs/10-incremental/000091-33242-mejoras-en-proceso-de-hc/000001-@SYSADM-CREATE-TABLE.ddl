-- Create table
create table WF_PARAMETROS
(
  id_tipo_parametro NUMBER not null,
  id_parametro      VARCHAR2(30) not null,
  nombre            VARCHAR2(30),
  descripcion       VARCHAR2(1000),
  valor             VARCHAR2(4000),
  clase             VARCHAR2(3)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255;
