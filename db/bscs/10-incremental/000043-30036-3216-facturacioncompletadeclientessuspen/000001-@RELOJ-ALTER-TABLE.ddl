alter table RELOJ.RC_SUSPENDIDOS_FIN_CICLO
add
   (
   CO_ID             NUMBER,
   STATUS_SUSP_BSCS  VARCHAR2(5),
   ESTADO            VARCHAR2(4)
   );
   
--add comments
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.ID_SECUENCIA
  is 'Numero de acuerdo a la secuencia SCORE_CLIENTE_SEQ';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CUENTA
  is 'Cuenta de acuerdo a la CL_CONTRATOS';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CUSTOMER_ID
  is 'Customer_id de acuerdo a la CUSTOMAR_ALL';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.ID_SERVICIO
  is 'Numero de telefono de 8 digitos';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CO_ID
  is 'Co_id de acuerdo a la tabla CONTRACT_ALL';

comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.STATUS_SUSP_BSCS
  is 'Numero que indica en que estado de reloj se encuentra el co_id.
      1:Mensajeria
      2:Suspension 34
      7:Suspension 80
      4:Suspension 33
      5:Suspension 35';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CED_IDENTIDAD
  is 'Documento de identificacion del cliente';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CICLO
  is 'Ciclo de la cuenta segun axis';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.BP_PLAN
  is 'Codigo del plan de acuerdo a la tabla GE_DETALLES_PLANES';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.DETALLE_PLAN
  is 'Descripcion del plan de acuerdo a la tabla GE_DETALLES_PLANES';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.DEUDA
  is 'Deuda del cliente';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.DIAS_MORA
  is 'Dias de mora del cliente';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CALIFICACION
  is 'Score del cliente de acuerdo a la tabla MMAG_RESUL_SCORE_CUENTA';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.TIPO_CLIENTE
  is 'Nombre del tipo de cliente de acuerdo a la tabla RC_TIPO_CLIENTE';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.FORMA_PAGO
  is 'Forma de pago de la cuenta de acuerdo a la tabla CL_CONTRATOS';
    
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.CATEGORIA
  is 'Categoria del cliente de acuerdo a la tabla CL_PERSONAS_EMPRESA';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.TIPO_APROBACION
  is 'Tipo de aprobacion del cliente segun la tabla CUSTOMER_ALL';
  
comment on column RC_SUSPENDIDOS_FIN_CICLO.TIPO_CUENTA
  is 'Tipo de cuenta bancaria: 
     A:ahorro 
     C:credito';
     
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.ID_FINANCIERA
  is 'Codigo de la entidad financiera de acuerdo a la tabla CL_AUTORIZACIONES_DEBITOS';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.COMPA�IA
  is 'Compa�ia asociada al contrato de acuerdo a la tabla CL_CONTRATOS';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.PAGO_ACUMULADOS
  is 'Valor de total de pagos realizados por el cliente segun la tabla CASHRECEIPTS_ALL de BSCS';
 
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.STATUS_LINEA_AXIS
  is 'Estatus de la linea de acuerdo a la tabla CL_DETALLES_SERVICIOS';
  
comment on column RC_SUSPENDIDOS_FIN_CICLO.FECHA_SUSPENSION
  is 'Fecha en la cual se debe suspender al servicio';

comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.FECHA_INGRESO
  is 'Fecha en la cual ingreso el registro';
  
comment on column RELOJ.RC_SUSPENDIDOS_FIN_CICLO.ESTADO
  is 'Estado del registro referente al proceso de actualizacion de planificacion en ttc_contractplannigdetails:
     P:pendiente de que se actualice su planificacion
     R:planificacion actualizada
     NE:no se actualizo la planificacion porque no se encontro informacion en la tabla o porque el registro se de suspension parcial se encuentra con CO_STATUSTRX in (E,F)';
  
 --INDICES
   
 create index RELOJ.IDX6_FECHA_SUSP on RELOJ.RC_SUSPENDIDOS_FIN_CICLO(ESTADO)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  
  create index RELOJ.IDX7_FECHA_SUSP on RELOJ.RC_SUSPENDIDOS_FIN_CICLO(CUSTOMER_ID,CO_ID,STATUS_SUSP_BSCS)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
