--- Create table
create table WF_COMPLAIN_TEMPORAL_CF
(
  NOMBRE2     VARCHAR2(1000),
  VALOR       NUMBER,
  CUSTOMER_ID VARCHAR2(50),
  CUSTCODE    VARCHAR2(50),
  OHREFNUM    VARCHAR2(100),
  TIPO        VARCHAR2(50),
  NOMBRE      VARCHAR2(1000),
  SERVICIO    VARCHAR2(100),
  DESCUENTO   NUMBER,
  COBRADO     NUMBER,
  CTACLBLEP   VARCHAR2(30),
  CTA_DEVOL   VARCHAR2(30),
  TIPO_IVA    VARCHAR2(30),
  ID_PRODUCTO VARCHAR2(5)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column WF_COMPLAIN_TEMPORAL_CF.NOMBRE2
  is 'Nombre del producto';
comment on column WF_COMPLAIN_TEMPORAL_CF.VALOR
  is 'Valor del producto';
comment on column WF_COMPLAIN_TEMPORAL_CF.CUSTOMER_ID
  is 'Id ';
comment on column WF_COMPLAIN_TEMPORAL_CF.CUSTCODE
  is 'Numero de cuenta';
comment on column WF_COMPLAIN_TEMPORAL_CF.OHREFNUM
  is 'Numero de documento';
comment on column WF_COMPLAIN_TEMPORAL_CF.TIPO
  is 'Tipo de Producto';
comment on column WF_COMPLAIN_TEMPORAL_CF.NOMBRE
  is 'Nombre del producto';
comment on column WF_COMPLAIN_TEMPORAL_CF.SERVICIO
  is 'Servicio ';
comment on column WF_COMPLAIN_TEMPORAL_CF.DESCUENTO
  is 'Valor descuento';
comment on column WF_COMPLAIN_TEMPORAL_CF.COBRADO
  is 'Valor cobrado';
comment on column WF_COMPLAIN_TEMPORAL_CF.CTACLBLEP
  is 'Cuenta Contable';
comment on column WF_COMPLAIN_TEMPORAL_CF.CTA_DEVOL
  is 'Cuenta devol';
comment on column WF_COMPLAIN_TEMPORAL_CF.TIPO_IVA
  is 'Tipo de iva segun la tabla cob_servicios';
comment on column WF_COMPLAIN_TEMPORAL_CF.ID_PRODUCTO
  is 'Cod. de producto de BSCS';

