--- Create table
create table BIT_FACTURA_BSCS_CF
(
  factura_bscs     VARCHAR2(30) not null,
  factura_sri_caja VARCHAR2(30),
  fecha_factura    DATE,
  id_hist          number,
  secuencia        number
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BIT_FACTURA_BSCS_CF.factura_bscs
  is 'Numero de factura BSCS';
comment on column BIT_FACTURA_BSCS_CF.factura_sri_caja
  is 'Numero de factura SRI de caja';
comment on column BIT_FACTURA_BSCS_CF.fecha_factura
  is 'Fecha de la factura';
comment on column BIT_FACTURA_BSCS_CF.id_hist
  is 'Id Historico de la factura bscs';
comment on column BIT_FACTURA_BSCS_CF.secuencia
  is 'secuencia de los cambios de fecha para la factura de bscs';  
-- Create/Recreate primary, unique and foreign key constraints 
alter table BIT_FACTURA_BSCS_CF
  add constraint PK_FACTURA_BSCS primary key (FACTURA_BSCS)
  using index 
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

