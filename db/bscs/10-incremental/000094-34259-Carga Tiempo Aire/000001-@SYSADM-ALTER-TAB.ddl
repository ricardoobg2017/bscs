--CAMPO QUE INDICA QUIEN COMPRO EL TIEMPO AIRE
alter table GSI_TMP_TIEMPO_AIRE add CO_ID INTEGER;

comment on column GSI_TMP_TIEMPO_AIRE.co_id
  is 'CO_ID del servicio que realizo la recarga';
