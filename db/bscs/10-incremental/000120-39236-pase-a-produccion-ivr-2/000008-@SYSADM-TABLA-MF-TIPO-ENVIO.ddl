-- Create table
create table MF_TIPO_ENVIO_IVR
(
  id_tipo         NUMBER,
  tipo_envio      VARCHAR2(1),
  descripcion     VARCHAR2(50),
  estado          VARCHAR2(1),
  envio_novedad   VARCHAR2(1) default 'N',
  id_tipo_novedad VARCHAR2(7)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_TIPO_ENVIO_IVR.id_tipo
  is 'Identificador de tipo de envio.';
comment on column MF_TIPO_ENVIO_IVR.tipo_envio
  is 'Abreviatura del tipo.';
comment on column MF_TIPO_ENVIO_IVR.descripcion
  is 'Contiene el detalle de los motivos de envio de mensaje.';
comment on column MF_TIPO_ENVIO_IVR.estado
  is 'Campo que indica si esta activo el tipo de envio.';
comment on column MF_TIPO_ENVIO_IVR.envio_novedad
  is 'Campo que decide si envia a cl_novedades en Axis si se envia el mensaje S-> SI N->NO';
comment on column MF_TIPO_ENVIO_IVR.id_tipo_novedad
  is 'Campo de id_tipo_novedad que se registrara en cl_novedades axis';
