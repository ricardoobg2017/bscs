-- Create table
create table MF_LINEA_PRINCIPAL_IVR
(
  id_servicio VARCHAR2(20),
  codigo_doc  VARCHAR2(25),
  id_plan     VARCHAR2(20)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_LINEA_PRINCIPAL_IVR.id_servicio
  is 'Servicio del cliente';
comment on column MF_LINEA_PRINCIPAL_IVR.codigo_doc
  is 'Cuenta del cliente';
comment on column MF_LINEA_PRINCIPAL_IVR.id_plan
  is 'ID de plan';
