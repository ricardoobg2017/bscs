-- Create table
create table MF_DIAS_SEMANA_IVR
(
  idenvio NUMBER,
  dia     NUMBER
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_DIAS_SEMANA_IVR.idenvio
  is 'ID de envio referenciado.';
comment on column MF_DIAS_SEMANA_IVR.dia
  is 'Campo para validacion para envios Diario y Semanal.';
