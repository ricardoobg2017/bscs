-- Create table
create table MF_FORMAS_PAGOS_IVR
(
  idbanco      NUMBER not null,
  nombre_banco VARCHAR2(70),
  idgrupo      VARCHAR2(4),
  nombre_grupo VARCHAR2(60)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_FORMAS_PAGOS_IVR.idbanco
  is 'Id referenciado al banco';
comment on column MF_FORMAS_PAGOS_IVR.nombre_banco
  is 'Nombre de la entidad Bancaria';
comment on column MF_FORMAS_PAGOS_IVR.idgrupo
  is 'Id del grupo que pertenece';
comment on column MF_FORMAS_PAGOS_IVR.nombre_grupo
  is 'Nombre referenciado al rupo que pertenece';
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_FORMAS_PAGOS_IVR
  add constraint PKBANKIDIVR primary key (IDBANCO)
  using index 
  tablespace MF_COB_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
