-- Create table
create table MF_ENVIO_EJECUCIONES_IVR
(
  idenvio               NUMBER not null,
  secuencia             NUMBER not null,
  estado_envio          VARCHAR2(1) not null,
  fecha_ejecucion       DATE not null,
  fecha_inicio          DATE,
  fecha_fin             DATE,
  tot_mensajes_enviados NUMBER,
  mensaje_proceso       VARCHAR2(250),
  estado                VARCHAR2(1),
  tot_mensajes_no_enviados NUMBER
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_ENVIO_EJECUCIONES_IVR.idenvio
  is 'Identificador del Envio';
comment on column MF_ENVIO_EJECUCIONES_IVR.secuencia
  is 'Identificador de la Secuencia';
comment on column MF_ENVIO_EJECUCIONES_IVR.estado_envio
  is 'Estado en que se encuentra el envio';
comment on column MF_ENVIO_EJECUCIONES_IVR.fecha_ejecucion
  is 'Fecha de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES_IVR.fecha_inicio
  is 'Fecha de inicio de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES_IVR.fecha_fin
  is 'Fecha fin de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES_IVR.tot_mensajes_enviados
  is 'Total de mensajes enviados';
comment on column MF_ENVIO_EJECUCIONES_IVR.mensaje_proceso
  is 'Indica mensaje de ejecucion del proceso';
comment on column MF_ENVIO_EJECUCIONES_IVR.estado
  is '(A)CTIVO, (I)NACTIVO campo utilizado en la eliminacion.';
comment on column MF_ENVIO_EJECUCIONES_IVR.tot_mensajes_no_enviados
  is 'Indica el total de mensajes no enviados';  
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_ENVIO_EJECUCIONES_IVR
  add constraint MF_ENVIO_EJEC_IVR_PK_CLAVE primary key (IDENVIO, SECUENCIA)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate check constraints 
alter table MF_ENVIO_EJECUCIONES_IVR
  add constraint ENVIO_IVR_EJEC_CH_ESTADOEN
  check (estado_envio in ('P','E','F','C','R','T'));
