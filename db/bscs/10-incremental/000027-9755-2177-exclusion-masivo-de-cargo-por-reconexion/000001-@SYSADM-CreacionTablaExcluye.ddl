-- Create table
create table CL_EXCLUYE_COBROS_AUD
(
  codigo_doc          VARCHAR2(12),
  fecha_procesamiento DATE,
  estado              VARCHAR2(1),
  observacion         VARCHAR2(200),
  cod_registro        NUMBER
)
tablespace COBROS_AUD_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CL_EXCLUYE_COBROS_AUD.codigo_doc
  is 'CODIGO DE LA CUENTA';
comment on column CL_EXCLUYE_COBROS_AUD.fecha_procesamiento
  is 'FECHA EN QUE SE PROCESO LA ELIMINACION DEL CARGO';
comment on column CL_EXCLUYE_COBROS_AUD.estado
  is 'P --> PENDIENTE ::: F --> FINALIZADO';
-- Create/Recreate indexes 
create index COD_REG_ESTADO_IDX on CL_EXCLUYE_COBROS_AUD (CODIGO_DOC, ESTADO)
  tablespace COBROS_AUD_IDX
  pctfree 10
  initrans 2
  maxtrans 255;
create index INDX_COD_REG on CL_EXCLUYE_COBROS_AUD (COD_REGISTRO)
  tablespace COBROS_AUD_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index INDX_CUENTA_COBRO on CL_EXCLUYE_COBROS_AUD (CODIGO_DOC)
  tablespace COBROS_AUD_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index INDX_ESTADO_CORO on CL_EXCLUYE_COBROS_AUD (ESTADO)
  tablespace COBROS_AUD_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CL_EXCLUYE_COBROS_AUD
  add constraint COD_REGISTRO_PK primary key (COD_REGISTRO)
  using index 
  tablespace COBROS_AUD_IDX
  pctfree 10
  initrans 2
  maxtrans 255;
