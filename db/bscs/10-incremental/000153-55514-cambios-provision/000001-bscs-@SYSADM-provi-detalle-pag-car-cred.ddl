-- Create table
create table PROVI_DETALLE_PAG_CAR_CRED
(
  provi_cuenta       VARCHAR2(30),
  provi_id_cliente   NUMBER,
  provi_fecha_regis  DATE,
  provi_clase        NUMBER,
  provi_descripcion  VARCHAR2(4000),
  provi_valor        NUMBER,
  provi_fees_codigo  NUMBER,
  provi_fees_detalle VARCHAR2(3000),
  provi_ruc          VARCHAR2(30),
  provi_nombres      VARCHAR2(80),
  provi_apellidos    VARCHAR2(80),
  provi_id_cia       NUMBER,
  provi_producto     VARCHAR2(40),
  provi_id_ciclo     VARCHAR2(2),
  provi_dia_ciclo    VARCHAR2(2),
  provi_nro_hilo     NUMBER,
  provi_fecha_insert DATE
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_cuenta
  is 'N�mero de Cuenta del Cliente';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_id_cliente
  is 'Id del Cliente';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_fecha_regis
  is 'Fecha en que se proces� este asiento o transacci�n dentro de la facturaci�n';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_clase
  is 'Clase del Registro: 1=PAGO, 2=CREDITO, 3=CARGO';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_descripcion
  is 'Descripci�n general tomada de las Tabla Principales de Facturaci�n';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_valor
  is 'Valor del Asiento o Transacci�n';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_fees_codigo
  is 'Codigo tomado de la Tabla FEES';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_fees_detalle
  is 'Descripci�n detallada tomada de la Tabla FEES';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_ruc
  is 'N�mero de RUC o C.I. del Cliente';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_nombres
  is 'Descripci�n de los Nombre del Cliente';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_apellidos
  is 'Descripci�n de los Apellidos del Cliente';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_id_cia
  is 'ID de la Regi�n a la que pertenece la Cuenta; 1=GYE o 2=UIO';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_producto
  is 'Descripci�n del Producto';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_id_ciclo
  is 'ID del Ciclo al que pertenece la Cuenta';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_dia_ciclo
  is 'D�a de Inicio del Ciclo al que pertenece la Cuenta';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_nro_hilo
  is 'N�mero de Hilo por el que es Procesada la Cuenta';
comment on column PROVI_DETALLE_PAG_CAR_CRED.provi_fecha_insert
  is 'Fecha de Inserci�n del Registro';
-- Create/Recreate indexes 
create index IDX_PROVI_DET_PCC_TEMP_1 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_CUENTA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_10 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_ID_CICLO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_11 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_DIA_CICLO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_12 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_NRO_HILO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_13 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_FECHA_INSERT)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_2 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_ID_CLIENTE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_3 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_FECHA_REGIS)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_4 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_CLASE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_5 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_FEES_CODIGO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_6 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_FEES_DETALLE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_7 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_RUC)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_8 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_ID_CIA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_DET_PCC_TEMP_9 on PROVI_DETALLE_PAG_CAR_CRED (PROVI_PRODUCTO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

  -- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on provi_detalle_pag_car_cred to DBJZAMBRANO;