-- Create table
CREATE TABLE WF_COMPLAIN_TEMPORAL_CF
(
  nombre2     VARCHAR2(1000),
  valor       NUMBER,
  customer_id VARCHAR2(50),
  custcode    VARCHAR2(50),
  ohrefnum    VARCHAR2(100),
  tipo        VARCHAR2(50),
  nombre      VARCHAR2(1000),
  servicio    VARCHAR2(100),
  descuento   NUMBER,
  cobrado     NUMBER,
  ctaclblep   VARCHAR2(30),
  cta_devol   VARCHAR2(30),
  tipo_iva    VARCHAR2(30),
  id_producto VARCHAR2(5),
  fecha       DATE DEFAULT SYSDATE
)
  
PARTITION BY RANGE (fecha)
(
  PARTITION WF_COMPLAIN_TEM_20160407 VALUES LESS THAN (TO_DATE('2016-04-08 00:00:00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
 TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  )
);
-- Add comments to the columns 
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.nombre2
  IS 'Nombre del producto';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.valor
  IS 'Valor del producto';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.customer_id
  IS 'Id ';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.custcode
  IS 'Numero de cuenta';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.ohrefnum
  IS 'Numero de documento';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.tipo
  IS 'Tipo de Producto';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.nombre
  IS 'Nombre del producto';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.servicio
  IS 'Servicio ';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.descuento
  IS 'Valor descuento';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.cobrado
  IS 'Valor cobrado';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.ctaclblep
  IS 'Cuenta Contable';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.cta_devol
  IS 'Cuenta devol';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.tipo_iva
  IS 'Tipo de iva segun la tabla cob_servicios';
COMMENT ON COLUMN WF_COMPLAIN_TEMPORAL_CF.id_producto
  IS 'Cod. de producto de BSCS';

