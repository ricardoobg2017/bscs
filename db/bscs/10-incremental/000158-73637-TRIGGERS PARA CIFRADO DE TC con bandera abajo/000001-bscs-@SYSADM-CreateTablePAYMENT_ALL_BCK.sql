/*ALTERS TABLAS*/

-- Create table
create table PAYMENT_ALL_BCK
(
  customer_id     INTEGER not null,
  seq_id          INTEGER not null,
  bank_id         INTEGER,
  accountowner    VARCHAR2(40),
  bankaccno       VARCHAR2(25),
  banksubaccount  VARCHAR2(20),
  bankname        VARCHAR2(70),
  bankzip         VARCHAR2(15),
  bankcity        VARCHAR2(40),
  bankstreet      VARCHAR2(40),
  valid_thru_date VARCHAR2(4),
  auth_ok         VARCHAR2(1),
  auth_date       DATE,
  auth_no         VARCHAR2(30),
  auth_credit     FLOAT,
  auth_tn         VARCHAR2(25),
  auth_remark     VARCHAR2(2000),
  ceilingamt      FLOAT,
  bankstate       VARCHAR2(25),
  bankcounty      VARCHAR2(40),
  bankstreetno    VARCHAR2(15),
  bankcountry     INTEGER,
  ordernumber     VARCHAR2(20),
  act_used        VARCHAR2(1),
  payment_type    INTEGER not null,
  entdate         DATE,
  moddate         DATE,
  userlastmod     VARCHAR2(16),
  pmod            VARCHAR2(1),
  swiftcode       VARCHAR2(11),
  bank_controlkey VARCHAR2(2),
  currency        INTEGER not null,
  rec_version     INTEGER not null,
  insertiondate   DATE not null,
  lastmoddate     DATE,
  fecha_respaldo  DATE
);
-- Add comments to the table 
comment on table PAYMENT_ALL_BCK
  is 'data of payment screen';
-- Add comments to the columns 
comment on column PAYMENT_ALL_BCK.customer_id
  is 'internal key (link to CUSTOMER_ALL)';
comment on column PAYMENT_ALL_BCK.seq_id
  is 'consecutive number per customer_id';
comment on column PAYMENT_ALL_BCK.bank_id
  is 'link to bank_all';
comment on column PAYMENT_ALL_BCK.accountowner
  is 'Owner of Account';
comment on column PAYMENT_ALL_BCK.bankaccno
  is 'bank accountnumber';
comment on column PAYMENT_ALL_BCK.banksubaccount
  is 'bank subaccount';
comment on column PAYMENT_ALL_BCK.bankname
  is 'bank name';
comment on column PAYMENT_ALL_BCK.bankzip
  is 'bank zip-code';
comment on column PAYMENT_ALL_BCK.bankcity
  is 'bank city';
comment on column PAYMENT_ALL_BCK.bankstreet
  is 'bank street';
comment on column PAYMENT_ALL_BCK.valid_thru_date
  is 'Valid Thru Date on Credit Card (Format: YYMM)';
comment on column PAYMENT_ALL_BCK.auth_ok
  is 'X=authorization is ok';
comment on column PAYMENT_ALL_BCK.auth_date
  is 'Date of authorization';
comment on column PAYMENT_ALL_BCK.auth_no
  is 'credit card authorization code';
comment on column PAYMENT_ALL_BCK.auth_credit
  is 'credit';
comment on column PAYMENT_ALL_BCK.auth_tn
  is 'Phone number of authorization -contact';
comment on column PAYMENT_ALL_BCK.auth_remark
  is 'autorization remark';
comment on column PAYMENT_ALL_BCK.ceilingamt
  is 'Ceiling amount for credit card payments';
comment on column PAYMENT_ALL_BCK.bankstate
  is 'State of bankaddress';
comment on column PAYMENT_ALL_BCK.bankcounty
  is 'County of bankaddress';
comment on column PAYMENT_ALL_BCK.bankstreetno
  is 'Street number of bank address';
comment on column PAYMENT_ALL_BCK.bankcountry
  is 'Country of bank address. Pointer to table COUNTRY.';
comment on column PAYMENT_ALL_BCK.ordernumber
  is 'individual order number per customer and payment type.';
comment on column PAYMENT_ALL_BCK.act_used
  is 'X=The payment connection actually used by the customer';
comment on column PAYMENT_ALL_BCK.payment_type
  is 'Payment Type, FK to PAYMENTTYPE_ALL';
comment on column PAYMENT_ALL_BCK.entdate
  is 'entry date';
comment on column PAYMENT_ALL_BCK.moddate
  is 'date of last modification';
comment on column PAYMENT_ALL_BCK.userlastmod
  is 'user, who made last modification of this record.';
comment on column PAYMENT_ALL_BCK.pmod
  is 'record modified flag:
NULL= not modified,
X= modified,
D= deleted.';
comment on column PAYMENT_ALL_BCK.swiftcode
  is 'Swift code, used as international bank identifier.';
comment on column PAYMENT_ALL_BCK.bank_controlkey
  is 'Bank control key ("Bankenkontrollschluessel"), used for foreign banks if the bankcode is not unique.';
comment on column PAYMENT_ALL_BCK.currency
  is 'Currency.
FK: FORCURR';
comment on column PAYMENT_ALL_BCK.rec_version
  is 'Counter for multiuser access';
comment on column PAYMENT_ALL_BCK.insertiondate
  is 'Insertion date';
comment on column PAYMENT_ALL_BCK.lastmoddate
  is 'Last modification date';
-- Create/Recreate indexes 
create index FKIPAYBNK_BCK on PAYMENT_ALL_BCK (BANK_ID)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIPAYCOUNT_BCK on PAYMENT_ALL_BCK (BANKCOUNTRY)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIPAYMENT_PTYPE_BCK on PAYMENT_ALL_BCK (PAYMENT_TYPE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IX_PAYMENT_ALL_LMD_BCK on PAYMENT_ALL_BCK (LASTMODDATE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PAYMNTBNKACC_BCK on PAYMENT_ALL_BCK (BANKACCNO)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PAYMSBBNKACC_BCK on PAYMENT_ALL_BCK (BANKSUBACCOUNT)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table PAYMENT_ALL_BCK
  add constraint PKPAYMENT_BCK primary key (CUSTOMER_ID, SEQ_ID)
  using index 
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table PAYMENT_ALL_BCK
  add constraint FK_PAYMENT_ALL_FORCURR_BCK foreign key (CURRENCY)
  references FORCURR (FC_ID);
alter table PAYMENT_ALL_BCK
  add constraint PAYBNK_BCK foreign key (BANK_ID)
  references BANK_ALL (BANK_ID);
alter table PAYMENT_ALL_BCK
  add constraint PAYCOUNT_BCK foreign key (BANKCOUNTRY)
  references COUNTRY (COUNTRY_ID);
alter table PAYMENT_ALL_BCK
  add constraint PAYMENT_PTYPE_BCK foreign key (PAYMENT_TYPE)
  references PAYMENTTYPE_ALL (PAYMENT_ID);
