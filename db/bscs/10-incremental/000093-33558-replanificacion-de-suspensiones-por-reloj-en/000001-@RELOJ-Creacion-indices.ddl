-- Creacion de indices para la tabla historica bck_process
create index reloj.idx_bck_proc_h_process on RELOJ.TTC_BCK_PROCESS_HIST(process);
create index reloj.idx_bck_proc_h_fecha on RELOJ.TTC_BCK_PROCESS_HIST(fisrtrundate);
