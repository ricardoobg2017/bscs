-- Create table
create table RC_OPE_RECAUDA_201910
(
  cuenta              VARCHAR2(24),
  id_cliente          NUMBER,
  producto            VARCHAR2(30),
  canton              VARCHAR2(40),
  provincia           VARCHAR2(25),
  apellidos           VARCHAR2(40),
  nombres             VARCHAR2(40),
  ruc                 VARCHAR2(20),
  id_forma_pago       VARCHAR2(5),
  forma_pago          VARCHAR2(58),
  tipo_cliente        VARCHAR2(60),
  fech_aper_cuenta    DATE,
  telefono1           VARCHAR2(25),
  telefono2           VARCHAR2(25),
  telefono            VARCHAR2(63),
  direccion           VARCHAR2(70),
  direccion2          VARCHAR2(200),
  id_solicitud        VARCHAR2(10),
  lineas_act          VARCHAR2(5),
  lineas_inac         VARCHAR2(5),
  lineas_susp         VARCHAR2(5),
  cod_vendedor        VARCHAR2(10),
  vendedor            VARCHAR2(2000),
  num_factura         VARCHAR2(30),
  balance_1           NUMBER,
  balance_2           NUMBER,
  balance_3           NUMBER,
  balance_4           NUMBER,
  balance_5           NUMBER,
  balance_6           NUMBER,
  balance_7           NUMBER,
  balance_8           NUMBER,
  balance_9           NUMBER,
  balance_10          NUMBER,
  balance_11          NUMBER,
  balance_12          NUMBER,
  totalvencida        NUMBER,
  total_deuda         NUMBER,
  saldo_pendiente     NUMBER,
  mayorvencido        VARCHAR2(10),
  compania            NUMBER,
  fech_max_pago       DATE,
  id_plan             VARCHAR2(10),
  detalle_plan        VARCHAR2(4000),
  desc_aprobacion     VARCHAR2(4000),
  estado              VARCHAR2(2),
  fecha_corte         DATE,
  hilo                NUMBER,
  financiamiento      VARCHAR2(50),
  recaudador          VARCHAR2(100),
  fecha_asignacion    DATE,
  fecha_fin_ciclo     DATE,
  pagos               NUMBER,
  usuario_ingreso     VARCHAR2(100) default USER,
  fecha_ingreso       DATE default SYSDATE,
  fecha_actualizacion DATE default SYSDATE
)
tablespace DATA2  --PRODUCCION
--tablespace DBX_DAT  --DESARROLLO
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
