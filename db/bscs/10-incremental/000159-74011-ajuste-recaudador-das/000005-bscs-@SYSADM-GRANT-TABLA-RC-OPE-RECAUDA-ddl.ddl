  -- Grant/Revoke object privileges 
  grant select, insert, update, delete, references, alter, index on RC_OPE_RECAUDA to DBAELIAS;
  grant select, insert, update, delete, references, alter, index on RC_OPE_RECAUDA to DBJZAMBRANO;
  grant select, insert, update, delete, references, alter, index on RC_OPE_RECAUDA to READ;
