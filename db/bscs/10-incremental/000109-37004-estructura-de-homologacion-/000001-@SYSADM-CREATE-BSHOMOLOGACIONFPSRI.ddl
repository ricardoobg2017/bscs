create table BS_HOMOLOGACION_FPSRI
(
  id_forma_pago         NUMBER(10) not null,
  id_fp_sri             NUMBER(10) not null,
  id_sistema            NUMBER(3) not null,
  fecha_creacion        DATE,
  usuario_creacion      VARCHAR2(50),
  fecha_modificacion    DATE,
  usuario_modificacion  VARCHAR2(50),
  estado                VARCHAR2(1)
)
tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BS_HOMOLOGACION_FPSRI.id_forma_pago is 'codigo de la forma de pago';
comment on column BS_HOMOLOGACION_FPSRI.id_fp_sri is 'codigo de la forma de pago del sri';
comment on column BS_HOMOLOGACION_FPSRI.id_sistema is 'codigo del sistema';
comment on column BS_HOMOLOGACION_FPSRI.fecha_creacion is 'fecha de creacion';
comment on column BS_HOMOLOGACION_FPSRI.usuario_creacion is 'usuario de creacion';
comment on column BS_HOMOLOGACION_FPSRI.fecha_modificacion is 'fecha de modificacion';
comment on column BS_HOMOLOGACION_FPSRI.usuario_modificacion is 'usuario de modificacion';
comment on column BS_HOMOLOGACION_FPSRI.estado is 'A:Activo, I:Inactivo';

-- Create/Recreate primary, unique and foreign key constraints 

create index BS_HOMOLOGACION_FPSRI_IDX on BS_HOMOLOGACION_FPSRI (id_forma_pago, id_sistema)
  tablespace IND12
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 8504K
    minextents 1
    maxextents unlimited
  );
  


