--DROP TABLE RELOJ.TTC_CONTRACTPLANNIG_2
-- Create table 
create table SYSADM.TTC_CONTRACTPLANNIG_2
  of RELOJ.TTC_CONTRACTPLANNIG_T
tablespace TEH_IDX
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Modify columns 
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CUSTOMER_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CO_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CO_PERIOD not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CO_STATUS_A not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CH_STATUS_B not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CO_REASON not null;
alter table SYSADM.TTC_CONTRACTPLANNIG_2 modify CH_VALIDFROM not null;
-- Add comments to the columns 
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CUSTOMER_ID
  is 'Customer_Id de la cuenta de la tabla Contract_All de BSCS.';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CO_ID
  is 'Co_id de la cuenta de la tabla Contract_All de BSCS.';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.DN_NUM
  is 'Dn_Num o N�mero de tel�fono del contrato de la tabla Directory_Number de BSCS, asociado al Co_id.';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CO_PERIOD
  is 'co_ext_csuin o etapa del contrato de la tabla Contract_All de BSCS, asociado al Co_id.';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CO_STATUS_A
  is 'Estatus actual de AXIS(23,29,32,34,33,80...etc) de la tabla de configuracion de los estados.';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CH_STATUS_B
  is 'Estatus actual de BSCS(a,s,d) de la tabla Contract_history';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CO_REASON
  is 'Reason actual de BSCS(37,41,49...etc) de la tabla Contract_history';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CO_TIMEPERIOD
  is 'Tiempo Transcurrido de permanencia del contrato en el reloj de cobranzas';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.CH_VALIDFROM
  is 'Fecha v�lida desde que el registro de cambio de estatus es vigente para BSCS-BCH de la tabla Contract_history';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.TMCODE
  is 'Tmcode del contrato de la tabla Contract_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.PRGCODE
  is 'Prgcode de la cuenta de la tabla Customer_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.ADMINCYCLE
  is 'Ciclo Administrativo de la cuenta y sus contratos de la tabla fa_ciclos_axis_bscs de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.BILLCYCLE
  is 'Billcycle del contrato de la tabla Contract_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.NOVERIFY
  is 'Numero de verificaciones que tiene la transaccion que pasar antes que se haga efectivo su cambio de estado o su procesamiento.
  Campo �til para la clasificaci�n o distribuci�n de las cuentas para su r�pida ejecuci�n,
  El proceso _SEND internamente, verifica la informacion y clasifa las cuentas en las tablas o colas
  respectivamente, de acuerdo a la tabla TTC_Relation_Object';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.INSERTIONDATE
  is 'Fecha de inserci�n del registro en la tabla';
comment on column SYSADM.TTC_CONTRACTPLANNIG_2.LASTMODDATE
  is 'Fecha de la �ltima modificaci�n del registro en la tabla';
-- Create/Recreate primary, unique and foreign key constraints 
alter table SYSADM.TTC_CONTRACTPLANNIG_2
  add constraint PK_TTC_CONTRACTPLANNIG_2 primary key (CO_ID)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IX_CONTRPLANNIG_CUST_2 on SYSADM.TTC_CONTRACTPLANNIG_2 (CUSTOMER_ID)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_CONTRPLANNIG_INSERTIONDA_2 on SYSADM.TTC_CONTRACTPLANNIG_2 (INSERTIONDATE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1400K
    minextents 1
    maxextents unlimited
  );
create index IX_CONTRPLANNIG_PRGCO_2 on SYSADM.TTC_CONTRACTPLANNIG_2 (PRGCODE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );

