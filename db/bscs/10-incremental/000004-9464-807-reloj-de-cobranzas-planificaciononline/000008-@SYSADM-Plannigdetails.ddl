--DROP TABLE SYSADM.TTC_CONTRACTPLANNIGDETAILS_2
-- Create table
create table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2
  of RELOJ.TTC_CONTRACTPLANNIGDETAILS_T
tablespace TEH_IDX
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Modify columns 
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CUSTOMER_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CO_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify PK_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify ST_ID not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify ST_SEQNO not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CO_STATUS_A not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CH_STATUS_B not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CO_REASON not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CH_TIMEACCIONDATE not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify TMCODE not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify PRGCODE not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify ADMINCYCLE not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify BILLCYCLE not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify ORDERBYTRX not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CO_USERTRX_1 not null;
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 modify CO_STATUSTRX not null;
-- Add comments to the columns 
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CUSTOMER_ID
  is 'Customer_Id de la cuenta de la tabla Contract_All de BSCS.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_ID
  is 'Co_id del contrato de la tabla Contract_All de BSCS.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.PK_ID
  is 'C�digo asignado para la configuraci�n del Package, relacionando a la tabla TTC_WorkFlow_Package';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.ST_ID
  is 'C�digo asignado para la configuraci�n del estatus, relacionando a la tabla TTC_WorkFlow_Status_all';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.ST_SEQNO
  is 'C�digo asignado para la configuraci�n del estatus, relacionando a la tabla TTC_WorkFlow_Status_all';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.RE_ORDEN
  is 'orden de ejecuci�n de los estatus, relacionando a la tabla ttc_workflow_relation';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CUSTCODE
  is 'Custcode N�mero de la cuenta de la tabla Customer_all.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.DN_NUM
  is 'Dn_Num o N�mero de tel�fono del contrato de la tabla Directory_Number de BSCS, asociado al Co_id.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_PERIOD
  is 'co_ext_csuin o etapa del contrato de la tabla Contract_All de BSCS, asociado al Co_id.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_STATUS_A
  is 'Estatus actual de AXIS(23,29,32,34,33,80...etc) de la tabla de configuracion de los estados.';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CH_STATUS_B
  is 'Estatus actual de BSCS(a,s,d) de la tabla Contract_history';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_REASON
  is 'Reason actual de BSCS(37,41,49...etc) de la tabla Contract_history';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_TIMEPERIOD
  is 'Tiempo Transcurrido de permanencia del contrato en el reloj de cobranzas. Este campo se conoce en cuantos dias
  se ejecutara la transaccion';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CH_TIMEACCIONDATE
  is 'N�mero de dias en que se ejecutar� el cambio de estatus, a partir de la fecha del campo InsertionDate.
  Si la fecha del campo Ch_TimeAccionDate=Sysdate de la ejecucion del proceso y el estatus de la transaccion
  en el campo Co_StatusTrx=P, se procede a ejecutar el registro';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.TMCODE
  is 'Tmcode del contrato de la tabla Contract_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.PRGCODE
  is 'Prgcode de la cuenta de la tabla Customer_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.ADMINCYCLE
  is 'Ciclo Administrativo de la cuenta y sus contratos de la tabla fa_ciclos_axis_bscs de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.BILLCYCLE
  is 'Billcycle del contrato de la tabla Contract_All de BSCS';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.VALUETRX
  is 'Valor de la transaccion esto es determinado por el Numero de Servicios Configurados en el plan multiplicado
  por el numero de contratos de la cuenta';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.ORDERBYTRX
  is 'Orden de ejecucion de los contratos de la cuenta, esto es para identificar validaciones como si es la linea principal u otros escenarios';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_IDTRAMITE
  is 'Tramite o numero de la transaccion que identifique que es una ejecucion del Reloj Cobranzas';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_REMARK
  is 'Comentario de la transaccion del Reloj, ejemplo:RELOJNEW, SUSPENDIDO desde El Reloj';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_USERTRX_1
  is 'Usuario configurado que tiene permiso para ejecutar la transaccion en AXIS y BSCS, esto sirve para el campo Co_CommandTrx_1,
  actualmente es: RELOJ';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_USERTRX_2
  is 'Usuario configurado que tiene permiso para ejecutar la transaccion en AXIS, esto sirve para el campo Co_CommandTrx_2,
  actualmente es: RELOJ';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_USERTRX_3
  is 'Usuario configurado que tiene permiso para ejecutar la transaccion en Espejo, esto sirve para el campo Co_CommandTrx_3,
  actualmente es: RELOJNEW';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_NOINTENTO
  is 'N�mero de intento de ejecuci�n de la transacci�n, esto quiere decir si es mayor >1,
  la transacci�n se procesco con reintentos y si gener� error hasta que reintento qued�';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_STATUSTRX
  is 'Estatus actual de la transacci�n:
  P-->Pendiente de procesar o de ejecutar el Reloj,
  T-->Transferida a la colas de procesamiento,
  F-->Finalizada,
  R-->Reintendo,
  L-->Pendiente por limite de credito,
  A-->Cuenta con saldo 0, pendiente que el proceso OUT, las quite del reloj,
  E-->Error
  V-->Trx pasa a Verificacion, luego de que ha sido reprocesada por 15 dias o mas
  )';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.CO_REMARKTRX
  is 'Comentario de la transacci�n si al momento de la ejecuci�n retorna error';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.INSERTIONDATE
  is 'Fecha de inserci�n del registro en la tabla';
comment on column SYSADM.TTC_CONTRACTPLANNIGDETAILS_2.LASTMODDATE
  is 'Fecha de la �ltima modificaci�n del registro en la tabla';
-- Create/Recreate primary, unique and foreign key constraints 
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2
  add constraint PK_TTC_CONTRACTPLANNIGDETAI_2 primary key (CO_ID, PK_ID, ST_ID, ST_SEQNO)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 59416K
    minextents 1
    maxextents unlimited
  );
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2
  add constraint FK_TTC_CONTRACTPLANNIGW_2_RE foreign key (PK_ID, ST_ID, ST_SEQNO, RE_ORDEN)
  references SYSADM.TTC_WORKFLOW_RELATION (PK_ID, ST_ID, ST_SEQNO, RE_ORDEN);
alter table SYSADM.TTC_CONTRACTPLANNIGDETAILS_2
  add constraint FK_TTC_CONTRACTPLANNING_2 foreign key (CO_ID)
  references SYSADM.TTC_CONTRACTPLANNIG_2 (CO_ID)
  disable;
-- Create/Recreate indexes 
create index IX_PDETAILS_CH_TIMEACCIONDA_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CH_TIMEACCIOND1_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, TMCODE, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CH_TIMEACCIOND2_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, PRGCODE, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CH_TIMEACCIOND3_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, ADMINCYCLE, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CH_TIMEACCION4_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, BILLCYCLE, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CH_TIMEACCION5_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CH_TIMEACCIONDATE, VALUETRX, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_C_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CO_ID)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CO_PER_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CO_PERIOD, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1400K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILSCO_TIMEPER_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CO_TIMEPERIOD, CO_STATUSTRX)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 102608K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CUS_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CUSTOMER_ID)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IX_PDETAILS_CUSTOME_2 on SYSADM.TTC_CONTRACTPLANNIGDETAILS_2 (CUSTOMER_ID, CH_TIMEACCIONDATE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 105248K
    minextents 1
    maxextents unlimited
  );


