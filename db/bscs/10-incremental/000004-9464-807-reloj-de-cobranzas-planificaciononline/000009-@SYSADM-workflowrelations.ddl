--DROP TABLE SYSADM.TTC_WORKFLOW_RELATION
--Creacion de tabla SYSADM.TTC_WORKFLOW_RELATION
CREATE TABLE SYSADM.TTC_WORKFLOW_RELATION AS 
       SELECT * FROM RELOJ.TTC_WORKFLOW_RELATION;


-- Create/Recreate primary key constraints 
alter table SYSADM.TTC_WORKFLOW_RELATION
  add constraint PK_TTC_WORKFLOW_RELATION primary key (PK_ID, ST_ID, ST_SEQNO, RE_ORDEN)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 80K
    minextents 1
    maxextents unlimited
  );
