-- Create table
create table MF_ENVIOS_GES
(
  idenvio              NUMBER not null,
  descripcion          VARCHAR2(60) not null,
  estado               VARCHAR2(1) not null,
  cantidad_mensajes    NUMBER default 1 not null,
  horas_separacion_msn NUMBER default 1 not null,
  forma_envio          VARCHAR2(5) not null,
  mensaje              NUMBER not null,
  monto_minimo         NUMBER default 0 not null,
  monto_maximo         NUMBER default 0 not null,
  region               NUMBER not null,
  tipo_cliente         NUMBER not null,
  requiere_edad_mora   VARCHAR2(1) not null,
  usuario_ingreso      VARCHAR2(20),
  fecha_ingreso        DATE default SYSDATE,
  usuario_modificacion VARCHAR2(20),
  fecha_modificacion   DATE,
  id_ciclo             VARCHAR2(2),
  tipo_ingreso         NUMBER(2),
  tipo_envio           VARCHAR2(1),
  score                VARCHAR2(1) default 'N'
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_ENVIOS_GES.idenvio
  is 'Identificador del Envio';
comment on column MF_ENVIOS_GES.descripcion
  is 'Descripcion del Envio';
comment on column MF_ENVIOS_GES.estado
  is 'Estado Actual del Envio. Posibles valores ''A'' Activo y ''I'' Inactivo';
comment on column MF_ENVIOS_GES.cantidad_mensajes
  is 'Cantidad de Mensajes que seran enviados por Dia';
comment on column MF_ENVIOS_GES.horas_separacion_msn
  is 'Horas de separacion entre los mensajes';
comment on column MF_ENVIOS_GES.forma_envio
  is 'Forma de Envio de Mensaje, SMS o EMAIL';
comment on column MF_ENVIOS_GES.mensaje
  is 'Formato del Mensaje, SMS o EMAIL';
comment on column MF_ENVIOS_GES.monto_minimo
  is 'Monto minimo';
comment on column MF_ENVIOS_GES.monto_maximo
  is 'Monto maximo';
comment on column MF_ENVIOS_GES.region
  is 'Region a la que pertenece la cuenta';
comment on column MF_ENVIOS_GES.tipo_cliente
  is 'Tipo de Cliente';
comment on column MF_ENVIOS_GES.requiere_edad_mora
  is 'Indica si se requiere edad mora';
comment on column MF_ENVIOS_GES.usuario_ingreso
  is 'Usuario que realiza el ingreso';
comment on column MF_ENVIOS_GES.fecha_ingreso
  is 'Fecha de ingreso del registro';
comment on column MF_ENVIOS_GES.usuario_modificacion
  is 'Usuario que realiza modificacion a la tabla';
comment on column MF_ENVIOS_GES.fecha_modificacion
  is 'Fecha de modificacion del registro';
comment on column MF_ENVIOS_GES.id_ciclo
  is 'Indica el ciclo a ejecutarse';
comment on column MF_ENVIOS_GES.tipo_ingreso
  is 'Tipo de Ingreso';
comment on column MF_ENVIOS_GES.tipo_envio
  is 'Tipo de Envio';
comment on column MF_ENVIOS_GES.score
  is 'Aplica Scoring  S->SI o N->NO';
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_ENVIOS_GES
  add constraint MF_ENVIOS_GES_PK_IDENVIO primary key (IDENVIO)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate check constraints 
alter table MF_ENVIOS_GES
  add constraint MF_ENVIOS_GES_CH_ESTADO
  check (estado in ('A','I'));
