-- Create table
create table MF_PLANES_GES
(
  id_plan     VARCHAR2(10),
  descripcion VARCHAR2(60),
  tmcode      NUMBER
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_PLANES_GES.id_plan
  is 'ID de Plan';
comment on column MF_PLANES_GES.descripcion
  is 'Descripcion de Planes';
comment on column MF_PLANES_IVR.tmcode
  is 'Codigo de Descripcion';
