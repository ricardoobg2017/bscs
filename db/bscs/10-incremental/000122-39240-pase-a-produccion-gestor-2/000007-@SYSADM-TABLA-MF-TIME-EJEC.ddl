-- Create table
create table MF_TIME_EJECUCION_GES
(
  idenvio             NUMBER,
  horamaxejecucion    NUMBER(10),
  periodoejecucion    VARCHAR2(20),
  minutosmaxejecucion NUMBER(2),
  estado              VARCHAR2(1),
  numerosmsmaxenvio   NUMBER(5),
  diaejecucion        DATE
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_TIME_EJECUCION_GES.idenvio
  is 'IDENTIFICADOR DE ENVIO';
comment on column MF_TIME_EJECUCION_GES.horamaxejecucion
  is 'CAMPO QUE CONTIENE LOS DIFERENTES PERIODOS: DIARIO, SEMANAL , QUINCENAL, MENSUAL.';
comment on column MF_TIME_EJECUCION_GES.periodoejecucion
  is 'SERVIRA PARA DETERMINAR LA HORA MAXIMO PARA LA EJECUCION DEL ENVIO DEL SMS.';
comment on column MF_TIME_EJECUCION_GES.minutosmaxejecucion
  is 'SERVIRA PARA DETERMINAR EL MINUTO MAXIMO PARA LA EJECUCION DEL ENVIO DEL SMS.';
comment on column MF_TIME_EJECUCION_GES.estado
  is 'ESTADO DEL CAMPO';
comment on column MF_TIME_EJECUCION_GES.numerosmsmaxenvio
  is 'CAMPO QUE DETERMINA EL NUMERO MAX DE ENVIOS';
comment on column MF_TIME_EJECUCION_GES.diaejecucion
  is 'DIAS DE EJECUCION DEL ENVIO';
