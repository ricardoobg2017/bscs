--Creacion de la tabla externa para verificar los Co_Ids de CMS
create table SYSADM.TB_EXT_COID_CMS
    (
     Nombre_coId varchar2(100),
     Reason     varchar2(100)
     )
     organization external
     (
                  default directory CMS_MASIVO
                  access parameters
                    (
                    records delimited by newline   
                    nologfile  
                    nobadfile 
                    nodiscardfile     
                    fields terminated by ','
                    )
    location ('')
    );
