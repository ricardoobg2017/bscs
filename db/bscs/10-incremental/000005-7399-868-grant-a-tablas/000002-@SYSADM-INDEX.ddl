-- Create/Recreate indexes 
create index IDX_EVENTOS_CUENTAS_01 on CO_EVENTOS_CUENTAS (CUENTA_BSCS)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IDX_EVENTOS_CUENTAS_02 on CO_EVENTOS_CUENTAS (ESTADO_CARTER)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IDX_EVENTOS_CUENTAS_03 on CO_EVENTOS_CUENTAS (FECHA_PROCESO)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
create index IDX_COLA_CARTERA_01 on CO_COLA_CARTERA (TIPO_CARTERA)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IDX_COLA_CARTERA_02 on CO_COLA_CARTERA (CIA, CICLO, TIPO_CARTERA)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index IDX_COLA_CARTERA_03 on CO_COLA_CARTERA (ESTADO_EJECUCION)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
