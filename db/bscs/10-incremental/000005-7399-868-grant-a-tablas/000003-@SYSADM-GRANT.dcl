-- Grant/Revoke object privileges 
grant select, insert on CO_EVENTOS_CUENTAS to DBJZAMBRANO;
grant select, insert on CO_EVENTOS_CUENTAS to DBAELIAS;
grant select, insert on CO_EVENTOS_CUENTAS to DBCPENA;
grant select, insert on CO_EVENTOS_CUENTAS to DBJROMERO;

-- Grant/Revoke object privileges 
grant select, insert, update, delete on CO_COLA_CARTERA to DBJZAMBRANO;
grant select, insert, update, delete on CO_COLA_CARTERA to DBAELIAS;
grant select, insert, update, delete on CO_COLA_CARTERA to DBCPENA;
grant select, insert, update, delete on CO_COLA_CARTERA to DBJROMERO;

grant select, insert, update, delete on CO_cartera_clientes to DBJZAMBRANO;
grant select, insert, update, delete on CO_cartera_clientes to DBAELIAS;
grant select, insert, update, delete on CO_cartera_clientes to DBCPENA;
grant select, insert, update, delete on CO_cartera_clientes to DBJROMERO;