-- Create table
create table CO_EVENTOS_CUENTAS
(
  id_transaccion    NUMBER,
  cuenta_bscs       VARCHAR2(2000),
  observacion       VARCHAR2(2000),
  fecha_proceso     DATE,
  estado_carter     VARCHAR2(100),
  estado_forma_pago VARCHAR2(100)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table CO_COLA_CARTERA
(
  cia              VARCHAR2(2),
  ciclo            VARCHAR2(10),
  estado           VARCHAR2(100),
  tipo_cartera     VARCHAR2(1),
  usuario          VARCHAR2(2000),
  fecha            DATE,
  estado_ejecucion VARCHAR2(20),
  observacion      VARCHAR2(2000)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
