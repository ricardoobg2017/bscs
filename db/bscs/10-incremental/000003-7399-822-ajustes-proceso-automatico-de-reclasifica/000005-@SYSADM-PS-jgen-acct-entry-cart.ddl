-- Create table
create table PS_JGEN_ACCT_ENTRY_CART
(
  business_unit     VARCHAR2(5) not null,
  transaction_id    VARCHAR2(10) not null,
  transaction_line  NUMBER(38) not null,
  ledger_group      VARCHAR2(10) not null,
  ledger            VARCHAR2(10) not null,
  accounting_dt     DATE,
  appl_jrnl_id      VARCHAR2(10) not null,
  business_unit_gl  VARCHAR2(5) not null,
  fiscal_year       NUMBER(38) not null,
  accounting_period NUMBER(38) not null,
  journal_id        VARCHAR2(10) not null,
  journal_date      DATE,
  journal_line      NUMBER(38) not null,
  account           VARCHAR2(10) not null,
  altacct           VARCHAR2(10) not null,
  deptid            VARCHAR2(10) not null,
  operating_unit    VARCHAR2(8) not null,
  product           VARCHAR2(6) not null,
  fund_code         VARCHAR2(5) not null,
  class_fld         VARCHAR2(5) not null,
  program_code      VARCHAR2(5) not null,
  budget_ref        VARCHAR2(8) not null,
  affiliate         VARCHAR2(5) not null,
  affiliate_intra1  VARCHAR2(10) not null,
  affiliate_intra2  VARCHAR2(10) not null,
  chartfield1       VARCHAR2(10) not null,
  chartfield2       VARCHAR2(10) not null,
  chartfield3       VARCHAR2(10) not null,
  project_id        VARCHAR2(15) not null,
  currency_cd       VARCHAR2(3) not null,
  statistics_code   VARCHAR2(3) not null,
  foreign_currency  VARCHAR2(3) not null,
  rt_type           VARCHAR2(5) not null,
  rate_mult         NUMBER(15,8) not null,
  rate_div          NUMBER(15,8) not null,
  monetary_amount   NUMBER(26,3) not null,
  foreign_amount    NUMBER(26,3) not null,
  statistic_amount  NUMBER(15,2) not null,
  movement_flag     VARCHAR2(1) not null,
  doc_type          VARCHAR2(8) not null,
  doc_seq_nbr       VARCHAR2(12) not null,
  doc_seq_date      DATE,
  jrnl_ln_ref       VARCHAR2(10) not null,
  line_descr        VARCHAR2(100) not null,
  iu_sys_tran_cd    VARCHAR2(8) not null,
  iu_tran_cd        VARCHAR2(8) not null,
  iu_anchor_flg     VARCHAR2(1) not null,
  gl_distrib_status VARCHAR2(1) not null,
  process_instance  NUMBER(10) not null
)
tablespace DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    minextents 1
    maxextents unlimited
  );
