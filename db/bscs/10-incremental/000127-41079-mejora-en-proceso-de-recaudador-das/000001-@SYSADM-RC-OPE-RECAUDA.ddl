--DROP TABLE
DROP TABLE RC_OPE_RECAUDA;
-- Create table
create table RC_OPE_RECAUDA
(
  CUENTA              VARCHAR2(24),
  ID_CLIENTE          NUMBER,
  PRODUCTO            VARCHAR2(30),
  CANTON              VARCHAR2(40),
  PROVINCIA           VARCHAR2(25),
  APELLIDOS           VARCHAR2(40),
  NOMBRES             VARCHAR2(40),
  RUC                 VARCHAR2(20),
  ID_FORMA_PAGO       VARCHAR2(5),
  FORMA_PAGO          VARCHAR2(58),
  FECH_APER_CUENTA    DATE,
  TELEFONO1           VARCHAR2(25),
  TELEFONO2           VARCHAR2(25),
  TELEFONO            VARCHAR2(63),
  DIRECCION           VARCHAR2(70),
  DIRECCION2          VARCHAR2(200),
  ID_SOLICITUD        VARCHAR2(10),
  LINEAS_ACT          VARCHAR2(5),
  LINEAS_INAC         VARCHAR2(5),
  LINEAS_SUSP         VARCHAR2(5),
  COD_VENDEDOR        VARCHAR2(10),
  VENDEDOR            VARCHAR2(2000),
  NUM_FACTURA         VARCHAR2(30),
  BALANCE_1           NUMBER,
  BALANCE_2           NUMBER,
  BALANCE_3           NUMBER,
  BALANCE_4           NUMBER,
  BALANCE_5           NUMBER,
  BALANCE_6           NUMBER,
  BALANCE_7           NUMBER,
  BALANCE_8           NUMBER,
  BALANCE_9           NUMBER,
  BALANCE_10          NUMBER,
  BALANCE_11          NUMBER,
  BALANCE_12          NUMBER,
  TOTALVENCIDA        NUMBER,
  TOTAL_DEUDA         NUMBER,
  SALDO_PENDIENTE     NUMBER,
  MAYORVENCIDO        VARCHAR2(10),
  COMPANIA            NUMBER,
  FECH_MAX_PAGO       DATE,
  ID_PLAN             VARCHAR2(10),
  DETALLE_PLAN        VARCHAR2(4000),
  DESC_APROBACION     VARCHAR2(4000),
  ESTADO              VARCHAR2(2),
  FECHA_CORTE         DATE,
  HILO                NUMBER,
  FINANCIAMIENTO      VARCHAR2(50),
  RECAUDADOR          VARCHAR2(100),
  FECHA_ASIGNACION    DATE,
  FECHA_FIN_CICLO     DATE,
  PAGOS               NUMBER,
  USUARIO_INGRESO     VARCHAR2(100) default USER,
  FECHA_INGRESO       DATE default SYSDATE,
  FECHA_ACTUALIZACION DATE default SYSDATE
)
tablespace DATA2
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table RC_OPE_RECAUDA
  is 'Tabla que contiene la base de cuentas que se enviaran a los recaudadores DAS para la gestion de cobro.';
-- Add comments to the columns 
comment on column RC_OPE_RECAUDA.CUENTA
  is 'Cuenta del servicio.';
comment on column RC_OPE_RECAUDA.ID_CLIENTE
  is 'Customer_Id del cliente.';
comment on column RC_OPE_RECAUDA.PRODUCTO
  is 'Subproducto del cliente.';
comment on column RC_OPE_RECAUDA.CANTON
  is 'Canton que esta registrado del cliente.';
comment on column RC_OPE_RECAUDA.PROVINCIA
  is 'Provincia con la que se encuentra registrado el cliente.';
comment on column RC_OPE_RECAUDA.APELLIDOS
  is 'Apellidos completos del cliente.';
comment on column RC_OPE_RECAUDA.NOMBRES
  is 'Nombres completos del cliente.';
comment on column RC_OPE_RECAUDA.RUC
  is 'Identificacion del cliente.';
comment on column RC_OPE_RECAUDA.ID_FORMA_PAGO
  is 'El Id forma de pago que tiene registrado el cliente.';
comment on column RC_OPE_RECAUDA.FORMA_PAGO
  is 'Descripcion de la forma de pago del cliente.';
comment on column RC_OPE_RECAUDA.FECH_APER_CUENTA
  is 'Fecha de creacion de la cuenta del cliente.';
comment on column RC_OPE_RECAUDA.TELEFONO1
  is 'Medio de comunicacion del cliente.';
comment on column RC_OPE_RECAUDA.TELEFONO2
  is 'Medio de comunicacion del cliente.';
comment on column RC_OPE_RECAUDA.TELEFONO
  is 'Servicio del cliente.';
comment on column RC_OPE_RECAUDA.DIRECCION
  is 'Direccion del cliente.';
comment on column RC_OPE_RECAUDA.DIRECCION2
  is 'Segunda direccion del cliente.';
comment on column RC_OPE_RECAUDA.ID_SOLICITUD
  is 'Id solicitud de la aprobacion del cliente.';
comment on column RC_OPE_RECAUDA.LINEAS_ACT
  is 'Cantidad de lineas activas del cliente.';
comment on column RC_OPE_RECAUDA.LINEAS_INAC
  is 'Cantidad de lineas inactivas del cliente.';
comment on column RC_OPE_RECAUDA.LINEAS_SUSP
  is 'Cantidad de lineas suspendidas del cliente.';
comment on column RC_OPE_RECAUDA.COD_VENDEDOR
  is 'Codigo del vendedor del servicio del cliente.';
comment on column RC_OPE_RECAUDA.VENDEDOR
  is 'Descripcion del vendedor del cliente.';
comment on column RC_OPE_RECAUDA.NUM_FACTURA
  is 'Numero de la ultima factura del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_1
  is 'Deuda de la factura de once meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_2
  is 'Deuda de la factura de diez meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_3
  is 'Deuda de la factura de nueve meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_4
  is 'Deuda de la factura de ocho meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_5
  is 'Deuda de la factura de siete meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_6
  is 'Deuda de la factura de seis meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_7
  is 'Deuda de la factura de cinco meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_8
  is 'Deuda de la factura de cuatro meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_9
  is 'Deuda de la factura de tres meses anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_10
  is 'Deuda de la factura de dos mes anteriores del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_11
  is 'Deuda de la factura del mes anterior del cliente.';
comment on column RC_OPE_RECAUDA.BALANCE_12
  is 'Valor de la ultima factura del cliente.';
comment on column RC_OPE_RECAUDA.TOTALVENCIDA
  is 'Total de la deuda vencida del cliente.';
comment on column RC_OPE_RECAUDA.TOTAL_DEUDA
  is 'Total de la deuda vencia + la ultima factura generada del cliente.';
comment on column RC_OPE_RECAUDA.SALDO_PENDIENTE
  is 'Saldo del total de la deuda menos los pagos realizados por el cliente.';
comment on column RC_OPE_RECAUDA.MAYORVENCIDO
  is 'Dias de mora que posee el cliente.';
comment on column RC_OPE_RECAUDA.COMPANIA
  is 'Id de la compania del cliente.';
comment on column RC_OPE_RECAUDA.FECH_MAX_PAGO
  is 'Fecha maxima de pago del cliente.';
comment on column RC_OPE_RECAUDA.ID_PLAN
  is 'Id del plan del cliente que posee en Axis.';
comment on column RC_OPE_RECAUDA.DETALLE_PLAN
  is 'Descripcion del plan del cliente que posee en Axis.';
comment on column RC_OPE_RECAUDA.DESC_APROBACION
  is 'Descripcion del id solicitud del cliente.';
comment on column RC_OPE_RECAUDA.ESTADO
  is 'Estado del registro I ingresado, G generado, A asignado, T terminado.';
comment on column RC_OPE_RECAUDA.FECHA_CORTE
  is 'Fecha de corte en la que se genero la data.';
comment on column RC_OPE_RECAUDA.HILO
  is 'Numero que corresponde al hilo que realizo la carga.';
comment on column RC_OPE_RECAUDA.FINANCIAMIENTO
  is 'Indica si el cliente posee financiamiento "SI" o "NO".';
comment on column RC_OPE_RECAUDA.RECAUDADOR
  is 'Nombre del recaudador al que fue asignado el cliente.';
comment on column RC_OPE_RECAUDA.FECHA_ASIGNACION
  is 'Fecha en la que se asigno el cliente al recaudador.';
comment on column RC_OPE_RECAUDA.FECHA_FIN_CICLO
  is 'Fecha limite para actualizar los pagos del cliente asignado al recaudador.';
comment on column RC_OPE_RECAUDA.PAGOS
  is 'Pagos del cliente que realizo mientras estaba asignado al recaudador.';
comment on column RC_OPE_RECAUDA.USUARIO_INGRESO
  is 'Usuario que ingreso el registro.';
comment on column RC_OPE_RECAUDA.FECHA_INGRESO
  is 'Fecha de ingreso del registro.';
comment on column RC_OPE_RECAUDA.FECHA_ACTUALIZACION
  is 'Fecha de actualizacion del registro.';
-- Create/Recreate indexes 
create index IDX_RC_OPE_RECAUD_01 on RC_OPE_RECAUDA (FECHA_CORTE)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_02 on RC_OPE_RECAUDA (ID_FORMA_PAGO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_03 on RC_OPE_RECAUDA (MAYORVENCIDO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_04 on RC_OPE_RECAUDA (FORMA_PAGO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_05 on RC_OPE_RECAUDA (CUENTA)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_06 on RC_OPE_RECAUDA (HILO, ESTADO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_07 on RC_OPE_RECAUDA (FINANCIAMIENTO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_RC_OPE_RECAUD_08 on RC_OPE_RECAUDA (FECHA_FIN_CICLO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
