create index IDX_SERV_IVR_4 on CONTR_SERVICES_CAP_IVR (CO_ID, DN_ID, CS_ACTIV_DATE, CS_DEACTIV_DATE)
  tablespace BILLING_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

