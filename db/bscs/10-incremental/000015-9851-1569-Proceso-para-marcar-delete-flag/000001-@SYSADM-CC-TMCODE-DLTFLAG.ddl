--drop table CC_TMCODE_DLTFLAG
create table CC_TMCODE_DLTFLAG
(
  id_registro   NUMBER not null,
  tmcode        NUMBER,
  tipo          varchar2(25),
  estado        varchar2(1),
  fecha_inicio  DATE,
  fecha_fin     DATE,
  observaciones VARCHAR2(250)
);
CREATE INDEX CC_TMCODE_DLTFLAG_IDX ON CC_TMCODE_DLTFLAG (tmcode);
