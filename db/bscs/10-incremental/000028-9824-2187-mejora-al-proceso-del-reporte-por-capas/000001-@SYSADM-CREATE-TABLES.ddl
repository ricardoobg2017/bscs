--CREACION DE TABLAS
-- Create table RPC_CAPAS_BITACORA
create table RPC_CAPAS_BITACORA
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(2),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_BITACORA_M
create table RPC_CAPAS_BITACORA_M
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(10),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_FECHAS
create table RPC_CAPAS_FECHAS
(
  ID_FECHAS             NUMBER(10) not null,
  FECHA_INICIO          DATE,
  FECHA_FIN             DATE,
  DIA                   VARCHAR2(2),
  ESTADO_EJECUCION_LLB  VARCHAR2(2),
  FECHA_INI_PRO_LLB     DATE,
  FECHA_FIN_PRO_LLB     DATE,
  CONTADOR_LLB          NUMBER(2),
  ESTADO_EJECUCION_MAIN VARCHAR2(2),
  FECHA_INI_PRO_MAIN    DATE,
  FECHA_FIN_PRO_MAIN    DATE,
  CONTADOR_MAIN         NUMBER(2),
  ESTADO                NUMBER(2),
  ESTADO_MAIN           NUMBER(2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_DETALLE
create table RPC_CAPAS_DETALLE
(
  ID_CAPAS    NUMBER,
  DESCRIPCION VARCHAR2(60),
  ESTADO      VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
 -- Create table RPC_CAPAS_DETALLE_MAIN
create table RPC_CAPAS_DETALLE_MAIN
(
  ID_CAPAS    NUMBER,
  DESCRIPCION VARCHAR2(60),
  ESTADO      VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  

-- Create table RPC_CAPAS_PARAMETROS
create table RPC_CAPAS_PARAMETROS
(
  CODIGO      VARCHAR2(30),
  DESCRIPCION VARCHAR2(300),
  VALOR1      VARCHAR2(1000),
  VALOR2      VARCHAR2(1000),
  ESTADO      VARCHAR2(2),
  HILO        NUMBER
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_VALIDA_POR
create table RPC_CAPAS_VALIDA_POR
(
  FECHA_CORTE     DATE,
  PERIODOS        DATE,
  REGIONES        VARCHAR2(4),
  USUARIO_INGRESO VARCHAR2(20),
  FECHA_INGRESO   DATE,
  POR_RECUPERADO  NUMBER(8,2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  


-- Create table RPC_CAPAS_PARAMETROS_ALARMA
create table RPC_CAPAS_PARAMETROS_ALARMA
(
  ID_PARAMETRO NUMBER(10),
  FROM_NAME    VARCHAR2(100),
  TO_NAME      VARCHAR2(100),
  CC           VARCHAR2(100),
  CCO          VARCHAR2(100),
  ESTADO       VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table RPC_CAPAS_PARAMETROS_ALARMA
  add constraint PK_RPC_ALARMA primary key (ID_PARAMETRO)
  deferrable
  using index 
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_ALARMA
create table RPC_CAPAS_ALARMA
(
  ID_ALARMA    NUMBER not null,
  ID_PARAMETRO NUMBER(10),
  SUBJECT      VARCHAR2(100),
  DESCRIPCION  VARCHAR2(1000),
  ESTADO       VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table RPC_CAPAS_ALARMA
  add constraint PK_RPC_ALARMA1 primary key (ID_ALARMA)
  using index 
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table RPC_CAPAS_ALARMA
  add constraint FK_RPC_ALARMA1 foreign key (ID_PARAMETRO)
  references RPC_CAPAS_PARAMETROS_ALARMA (ID_PARAMETRO);
  
  
-- Create table RPC_PERIODOS_VAL_FACT
create table RPC_PERIODOS_VAL_FACT
(
  cierre_periodo DATE not null,
  revisado       VARCHAR2(1),
  hilo           NUMBER
)
tablespace RECUPERA_CCH
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
