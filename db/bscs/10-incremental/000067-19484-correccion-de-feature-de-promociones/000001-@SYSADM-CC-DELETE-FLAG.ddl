CREATE TABLE CC_DELETE_FLAG
(
CO_ID            INTEGER,
SNCODE           INTEGER,
LASTMODDATE      DATE,
STATUS           VARCHAR2(1),
VALID_FROM_DATE  DATE,
OBSERVACION      VARCHAR2(4000),
FECHA_REGISTRO   DATE)

tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255;

COMMENT ON TABLE CC_DELETE_FLAG
  IS 'Tabla de los CO_ID  y SNCODE conciliados por Delete_Flag';
-- Add comments to the columns 
COMMENT ON COLUMN CC_DELETE_FLAG.CO_ID
  IS 'Contrato del cliente';
COMMENT ON COLUMN CC_DELETE_FLAG.SNCODE
  IS 'Id del feature en BSCS';
COMMENT ON COLUMN CC_DELETE_FLAG.LASTMODDATE
  IS 'Fecha de la ultima modificacion del feature en la tabla: PROFILE_SERVICE.';
COMMENT ON COLUMN CC_DELETE_FLAG.STATUS
  IS 'Estado del feature';
COMMENT ON COLUMN CC_DELETE_FLAG.VALID_FROM_DATE
  IS 'Fecha de ingreso del feature en la tabla: PR_SERV_STATUS_HIST';
COMMENT ON COLUMN CC_DELETE_FLAG.OBSERVACION
  IS 'Observacion de la conciliacion del Delete_Flag "Conciliado" o "Error"';
COMMENT ON COLUMN CC_DELETE_FLAG.FECHA_REGISTRO
  IS 'Fecha de conciliacion del Delete_Flag';
  
  create index CC_DF_IDX_FECH on CC_DELETE_FLAG (FECHA_REGISTRO)
  tablespace ind2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
    create index CC_DF_IDX_CODSN on CC_DELETE_FLAG (CO_ID,SNCODE)
  tablespace ind2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
