alter table MF_ENVIO_EJECUCIONES
  add constraint MF_ENVIO_EJEC_PK_CLAVE primary key (IDENVIO, SECUENCIA)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate check constraints 
alter table MF_ENVIO_EJECUCIONES
  add constraint ENVIO_EJEC_CH_ESTADOENVIO
  check (estado_envio in ('P','E','F','C','R','T'));
  