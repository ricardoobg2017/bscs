-- Create table
create table MF_ENVIO_EJECUCIONES
(
  IDENVIO               NUMBER not null,
  SECUENCIA             NUMBER not null,
  ESTADO_ENVIO          VARCHAR2(1) not null,
  FECHA_EJECUCION       DATE not null,
  FECHA_INICIO          DATE,
  FECHA_FIN             DATE,
  TOT_MENSAJES_ENVIADOS NUMBER,
  MENSAJE_PROCESO       VARCHAR2(250),
  ESTADO                VARCHAR2(1),
  TOT_MENSAJES_NO_ENVIADOS NUMBER
)
tablespace DATA8
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 6M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns
comment on column MF_ENVIO_EJECUCIONES.idenvio
  is 'Identificador del Envio';
comment on column MF_ENVIO_EJECUCIONES.secuencia
  is 'Identificador de la Secuencia';
comment on column MF_ENVIO_EJECUCIONES.estado_envio
  is 'Estado en que se encuentra el envio';
comment on column MF_ENVIO_EJECUCIONES.fecha_ejecucion
  is 'Fecha de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES.fecha_inicio
  is 'Fecha de inicio de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES.fecha_fin
  is 'Fecha fin de ejecucion del envio';
comment on column MF_ENVIO_EJECUCIONES.tot_mensajes_enviados
  is 'Total de mensajes enviados';
comment on column MF_ENVIO_EJECUCIONES.mensaje_proceso
  is 'Indica mensaje de ejecucion del proceso'; 
comment on column MF_ENVIO_EJECUCIONES.estado
  is '(A)CTIVO, (I)NACTIVO campo utilizado en la eliminacion.';
comment on column MF_ENVIO_EJECUCIONES.tot_mensajes_no_enviados
  is 'Indica el total de mensajes no enviados';  
