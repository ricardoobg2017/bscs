-- Add comments to the table 
comment on table MK_DESCUENTO_BSCS_TMP
  is 'Se guarda informacion de las promociones que aplican descuento';
-- Add comments to the columns 
comment on column MK_DESCUENTO_BSCS_TMP.id_servicio
  is 'Codigo de servicio';
comment on column MK_DESCUENTO_BSCS_TMP.co_id
  is 'Codigo de servicio asociado';
comment on column MK_DESCUENTO_BSCS_TMP.monto_dolares
  is 'Valor a acreditar al servicio';
comment on column MK_DESCUENTO_BSCS_TMP.expiracion
  is 'Fecha de expiracion de la promocion';
comment on column MK_DESCUENTO_BSCS_TMP.estado
  is 'Estado del registro';
comment on column MK_DESCUENTO_BSCS_TMP.id_promocion
  is 'Codigo de la promocion';
comment on column MK_DESCUENTO_BSCS_TMP.id_ciclo
  is 'Ciclo del servicio';
comment on column MK_DESCUENTO_BSCS_TMP.fecha_acredita
  is 'Fecha de acreditacion de la promocion';
comment on column MK_DESCUENTO_BSCS_TMP.fecha_ingreso
  is 'Fecha de ingreso de la promocion';
comment on column MK_DESCUENTO_BSCS_TMP.despachador
  is 'Despachador';
comment on column MK_DESCUENTO_BSCS_TMP.observacion
  is 'Observacion de la trx';
comment on column MK_DESCUENTO_BSCS_TMP.codigo_doc
  is 'Codigo de la cuenta bscs';
comment on column MK_DESCUENTO_BSCS_TMP.id_contrato
  is 'Codigo del contrato de la cuenta';
comment on column MK_DESCUENTO_BSCS_TMP.id_subproducto
  is 'Tipo de producto';
comment on column MK_DESCUENTO_BSCS_TMP.customer_id
  is 'Codigo de la cuenta interno';
comment on column MK_DESCUENTO_BSCS_TMP.pack_id
  is 'Codigo asociado a la promocion';
comment on column MK_DESCUENTO_BSCS_TMP.secuencia_bscs
  is 'Secuencia del descuento';
comment on column MK_DESCUENTO_BSCS_TMP.tipo_promo
  is 'Tipo de promocion asociado';
comment on column MK_DESCUENTO_BSCS_TMP.fecha_registro
  is 'Fecha del registro';