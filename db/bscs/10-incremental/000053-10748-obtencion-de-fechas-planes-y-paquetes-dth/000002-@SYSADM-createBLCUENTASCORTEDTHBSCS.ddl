-- Create table
create table BL_CUENTAS_CORTE_DTH
(
  cuenta         VARCHAR2(20),
  ciclo          VARCHAR2(2),
  fecha_corte    VARCHAR2(12),
  fecha_ingreso  DATE default sysdate,
  hilo           NUMBER,
  id_ciclo_admin VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 9360K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column BL_CUENTAS_CORTE_DTH.cuenta
  is 'cuenta bscs del cliente claro';
comment on column BL_CUENTAS_CORTE_DTH.ciclo
  is 'ciclo que se este facturando';
comment on column BL_CUENTAS_CORTE_DTH.fecha_corte
  is 'fecha ciclo que se registre en BSCS ';
comment on column BL_CUENTAS_CORTE_DTH.fecha_ingreso
  is 'fecha en que fue lanzado el proceso ';
comment on column BL_CUENTAS_CORTE_DTH.hilo
  is 'hilos que van Desde 1.....20';
comment on column BL_CUENTAS_CORTE_DTH.id_ciclo_admin
  is 'subciclos que existen en BSCS para el ciclo que procese'; 

-- Create/Recreate indexes 
create index IDX_CUENTA_DTH1 on BL_CUENTAS_CORTE_DTH (CUENTA)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index IDX_FECHA_CORTE_DTH1 on BL_CUENTAS_CORTE_DTH (FECHA_CORTE)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

 
