-- Create table
create table BL_CUENTAS_FACTURAR_DTH
(
  fecha_corte    VARCHAR2(20),
  cuenta_bscs    VARCHAR2(20),
  campo1         DATE,
  campo2         VARCHAR2(100),
  campo3         VARCHAR2(100),
  fecha_ingreso  DATE,
  ciclo          VARCHAR2(2),
  hilo           NUMBER,
  estado         VARCHAR2(1),
  telefono       VARCHAR2(20),
  tipo_proceso   VARCHAR2(50),
  id_ciclo_admin VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 9360K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column BL_CUENTAS_FACTURAR_DTH.fecha_corte
  is 'fecha ciclo que se registre en BSCS ';
comment on column BL_CUENTAS_FACTURAR_DTH.cuenta_bscs
  is 'cuenta bscs del cliente claro';
comment on column BL_CUENTAS_FACTURAR_DTH.campo1
  is 'campo fecha (fecha adendum,fecha contratacion feacrure)';
comment on column BL_CUENTAS_FACTURAR_DTH.campo2
  is 'campo varchar (fecha de contratacion,descripcion del feature contratado)';
comment on column BL_CUENTAS_FACTURAR_DTH.campo3
  is 'campo varchar (fecha de activacion,descripcion del feature contratado)';
comment on column BL_CUENTAS_FACTURAR_DTH.fecha_ingreso
  is 'fecha en que fue lanzado el proceso ';
comment on column BL_CUENTAS_FACTURAR_DTH.ciclo
  is 'ciclo que se este facturando';
comment on column BL_CUENTAS_FACTURAR_DTH.hilo
  is 'hilos que van Desde 1.....20';
comment on column BL_CUENTAS_FACTURAR_DTH.estado
  is 'estado  A activo';
comment on column BL_CUENTAS_FACTURAR_DTH.telefono
  is 'servicio que registre el Cliente';
comment on column BL_CUENTAS_FACTURAR_DTH.tipo_proceso
  is 'registra el modulo de ejecucion (CTA_PLAN,CTA_FEAT)';
comment on column BL_CUENTAS_FACTURAR_DTH.id_ciclo_admin
  is 'subciclos que existen en BSCS para el ciclo que procese';

  -- Create/Recreate indexes 
create index IDX_CTAS_FACT_TIPO_DTH on BL_CUENTAS_FACTURAR_DTH (TIPO_PROCESO,id_ciclo_admin);
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );