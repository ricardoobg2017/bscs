-- Create table  BL_GSI_RELOJ - MRECILLO
create table BL_GSI_RELOJ
(
  CUENTA      VARCHAR2(24) not null,
  DEUDA       NUMBER,
  SALDO       NUMBER,
  CREDITOS    NUMBER,
  MENSAJE     VARCHAR2(20),
  CONTRATO    NUMBER,
  DESPACHADOR NUMBER
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2360K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BL_GSI_RELOJ.CUENTA
  is 'Cuenta por servicio';
comment on column BL_GSI_RELOJ.DEUDA
  is 'Valor de deuda acumulado del servicio';
comment on column BL_GSI_RELOJ.MENSAJE
  is '''OK'' O ''CON DEUDA''';
comment on column BL_GSI_RELOJ.CONTRATO
  is 'N� DEL CONTRATO ';
comment on column BL_GSI_RELOJ.DESPACHADOR
  is 'Numero del despachador por hilo';
-- Create/Recreate primary, unique and foreign key constraints 
alter table BL_GSI_RELOJ
  add constraint PK_GSI_RELOJ_CTA primary key (CUENTA)
  using index 
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_C_DESP_GSI_RELOJ on BL_GSI_RELOJ(DESPACHADOR)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_C_MSJ_GSI_RELOJ on BL_GSI_RELOJ(MENSAJE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
