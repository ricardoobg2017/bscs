--CREAR TABLA
create table PROVI_CTAS_X_COBRAR
(
  Asiento_contable    NUMBER,
  id_cia         NUMBER,
  id_cuenta      VARCHAR2(15),
  nombre_cta     VARCHAR2(50),
  tipo           NUMBER,
  producto       VARCHAR2(50),
  provi_mes_ant  NUMBER,
  sub_casti_cart NUMBER,
  baja           NUMBER,
  baja_castigo   NUMBER,
  acu_antes      NUMBER,
  cart_mas_90    NUMBER,
  cart_exclu     NUMBER,
  total_provi    NUMBER,
  provi_mes      NUMBER,
  acum_mes_act   NUMBER,
  usuario        VARCHAR2(20),
  fecha_gen      DATE,
  corte_conta    DATE,
  mes            VARCHAR2(2),
  anio           VARCHAR2(4),
  estado_asiento VARCHAR2(2) 
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
  -- Add comments to the columns 
  
comment on column PROVI_CTAS_X_COBRAR.Asiento_contable
  is 'Secuencial referencial de lo contabilizado a SAP';
comment on column PROVI_CTAS_X_COBRAR.id_cia
  is 'Regi�n de la exclusi�n adicional; 1=''GYE'', 2=''UIO'' ';
comment on column PROVI_CTAS_X_COBRAR.id_cuenta
  is 'Tipo de cuenta referencial de la tabla PROVI_CTAS_CONT';
comment on column PROVI_CTAS_X_COBRAR.nombre_cta
  is 'Descripci�n de la cuenta; ''CONSUMO CELULAR POR COBRAR'',''CARTERA VENCIDA''';
comment on column PROVI_CTAS_X_COBRAR.tipo
  is 'tipo de descripci�n; 1=''CONSUMO CELULAR POR COBRAR'', 2=''CARTERA VENCIDA''';
comment on column PROVI_CTAS_X_COBRAR.provi_mes_ant
  is 'Valor de Provisi�n acumulada del mes anterior ';
comment on column PROVI_CTAS_X_COBRAR.sub_casti_cart
  is 'Valor de cartera castigada ';    
comment on column PROVI_CTAS_X_COBRAR.baja
  is 'Valor de condonaciones ';
comment on column PROVI_CTAS_X_COBRAR.baja_castigo
  is 'Valor de castigo_cart (+) baja ' ;  
comment on column PROVI_CTAS_X_COBRAR.acu_antes
  is 'Valor de provisi�n Mes Ant  (-) Baja Castigo ';    
comment on column PROVI_CTAS_X_COBRAR.cart_mas_90
  is 'Valor de Cartera con mora m�s de 90 d�as ';
comment on column PROVI_CTAS_X_COBRAR.cart_exclu
  is 'Valor de exclusiones. (AUTOMATICAS Y MANUALES)';
comment on column PROVI_CTAS_X_COBRAR.total_provi
  is 'Valor de Cart Mas 90  (-) las exclusiones.';
comment on column PROVI_CTAS_X_COBRAR.provi_mes
  is 'Valor de Total Provi   (-)  Acu Antes';
comment on column PROVI_CTAS_X_COBRAR.acum_mes_act
  is 'Valor de Acu Antes  (+) Provi Mes ';
comment on column PROVI_CTAS_X_COBRAR.usuario
  is 'Usuario que realiz� la provisi�n ';   
comment on column PROVI_CTAS_X_COBRAR.fecha_gen
  is 'Fecha en la cual se realiz� la provisi�n ';
comment on column PROVI_CTAS_X_COBRAR.corte_conta
  is 'Fecha corte generada por el proceso ';
comment on column PROVI_CTAS_X_COBRAR.mes
  is 'Mes a provisar ';
comment on column PROVI_CTAS_X_COBRAR.anio
  is 'Anio de la provisi�n generada';
comment on column PROVI_CTAS_X_COBRAR.estado_asiento
  is 'Estado del asiento C=(Contabilizado) o P=(Pendiente)'; 
comment on column PROVI_CTAS_X_COBRAR.producto
  is 'Tipo de producto (DTH) O (SMA)';     
  
             
-- Create/Recreate primary, unique and foreign key constraints 
alter table PROVI_CTAS_X_COBRAR
  add constraint PK_PROVI_CTAS_X_COBRAR primary key (id_cia,tipo,mes,anio,producto)
  using index 
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  --Creaci�n de indices
create index IDX_PROVI_CTAS_X_COBRAR_1 on PROVI_CTAS_X_COBRAR (estado_asiento)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index IDX_PROVI_CTAS_X_COBRAR_2 on PROVI_CTAS_X_COBRAR (Asiento_contable)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

