--crear tabla 
create table provi_conta_hist
(
id_cia number,
desc_cia varchar2(30),
mes number ,
anio number ,
valor number ,
producto varchar2(30),
usuario varchar2(30) ,
fecha_conta  DATE
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
 -- Add comments to the columns  
comment on column provi_conta_hist.id_cia
  is 'N�mero de compa��a ingresada';
comment on column provi_conta_hist.DESC_CIA
  is 'Regi�n de la PROVISION ; 1=''GUAYAQUIL'', 2=''QUITO'' ';
comment on column provi_conta_hist.MES
  is 'Mes de la provisi�n realizada';
comment on column provi_conta_hist.ANIO
  is 'Anio de la provisi�n realizada';
comment on column provi_conta_hist.VALOR
  is 'Valor de la provisi�n';
comment on column provi_conta_hist.FECHA_CONTA
  is 'Fecha de la generaci�n de la provsi�n';
comment on column provi_conta_hist.PRODUCTO
  is 'Tipo de producto';
  
  --Creacion de PrimaryKey
alter table provi_conta_hist
  add constraint PK_provi_conta_hist primary key (id_cia,MES,ANIO,PRODUCTO)
  using index 
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );    
  
