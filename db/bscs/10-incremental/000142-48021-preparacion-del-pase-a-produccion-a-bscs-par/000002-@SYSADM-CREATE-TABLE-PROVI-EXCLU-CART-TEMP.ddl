-- Create table
create table SYSADM.PROVI_EXCLU_CART_TEMP
(
  custcode       VARCHAR2(24),
  customer_id    NUMBER not null,
  compania       NUMBER,
  categoria      VARCHAR2(30),
  provincia      VARCHAR2(40),
  canton         VARCHAR2(40),
  producto       VARCHAR2(30),
  nombres        VARCHAR2(40),
  apellidos      VARCHAR2(40),
  ruc            VARCHAR2(50),
  id_forma       NUMBER,
  des_forma_pago VARCHAR2(58),
  total_deuda    NUMBER,
  id_ciclo       VARCHAR2(2),
  mora           NUMBER,
  estado_exclu   VARCHAR2(2),
  estado_provi   VARCHAR2(2),
  fecha_exclu    DATE,
  fecha_conta    DATE not null,
  mes_exclusion  VARCHAR2(2),
  anio_exclusion VARCHAR2(4),
  motivo         VARCHAR2(58)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );


-- Add comments to the columns 
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.custcode
  is 'N�mero de Cuenta del Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.customer_id
  is 'ID del Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.compania
  is 'ID de la Regi�n; 1=''GYE'', 2=''UIO'' ';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.provincia
  is 'Descripci�n de la Provincia';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.canton
  is 'Descripci�n del Cant�n';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.producto
  is 'Descripci�n del Producto';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.nombres
  is 'Descripci�n de los Nombre del Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.apellidos
  is 'Descripci�n de los Apellidos del Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.ruc
  is 'N�mero de RUC o C.I. del Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.id_forma
  is 'ID del Motivo de la Exclusi�n';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.des_forma_pago
  is 'Descripci�n de la Forma de Pago';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.total_deuda
  is 'Valor que adeuda el Cliente';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.id_ciclo
  is 'ID del Ciclo al que pertenece la Cuenta';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.mora
  is 'N�mero de meses en que el Cliente cay� en MORA';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.estado_exclu
  is 'Indica el Estado de la Exclusi�n de la Cuenta; ''I''=''Inactivo'', ''A''=''Activo''';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.estado_provi
  is 'Indica el Estado de la Cuenta en el Proceso de Provisi�n; ''P''=''Procesada'', ''C''=''Contabilizada''';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.fecha_exclu
  is 'Fecha de Registro en que la Cuenta es Excluida';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.fecha_conta
  is 'Fecha de Contabilizaci�n en que se procesa la Excluisi�n de la Cuenta (generalmente cada fin de mes)';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.mes_exclusion
  is 'Contiene el n�mero del Mes en que se procesa la Exclusi�n de la Cuenta';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.anio_exclusion
  is 'Contiene el A�o? en que se procesa la Exclusi�n de la Cuenta';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.motivo
  is 'Descripci�n de la Forma de Pago';
comment on column SYSADM.PROVI_EXCLU_CART_TEMP.categoria
  is 'Descripci�n del Tipo de Cliente; ''INSIGNIA'', ''PREMIUN'', ''VIP'', ''MASIVOS''';


-- Create/Recreate primary, unique and foreign key constraints 
alter table SYSADM.PROVI_EXCLU_CART_TEMP
  add constraint PK_PROVI_EXCLU_CART_TEMP primary key (CUSTOMER_ID, FECHA_CONTA)
  using index 
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
