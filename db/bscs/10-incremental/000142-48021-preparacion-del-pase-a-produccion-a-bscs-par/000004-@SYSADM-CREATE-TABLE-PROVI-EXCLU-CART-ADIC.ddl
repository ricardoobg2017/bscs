-- Create table
create table PROVI_EXCLU_CART_ADIC
(
  mes_excluision        VARCHAR2(20) not null,
  anio_exclusion        VARCHAR2(10) not null,
  id_cia_exclusion      NUMBER not null,
  producto_exclusion    VARCHAR2(30) not null,
  valor_exclusion       NUMBER(10,3),
  observacion_exclusion VARCHAR2(4000),
  estado_exclusion      VARCHAR2(3),  
  fecha_registro        DATE,
  fecha_exclusion       DATE,  
  usuario_exclusion     VARCHAR2(50),
  fecha_modificacion    DATE,
  usuario_modificacion  VARCHAR2(50),
  estado                VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  pctused 40  
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  
-- Add comments to the columns 
comment on column PROVI_EXCLU_CART_ADIC.mes_excluision
  is 'Mes de la exclusion adicional';
comment on column PROVI_EXCLU_CART_ADIC.anio_exclusion
  is 'Annio de la exclusion adicional';
comment on column PROVI_EXCLU_CART_ADIC.id_cia_exclusion
  is 'Regi�n de la exclusi�n adicional; 1=''GYE'', 2=''UIO'' ';
comment on column PROVI_EXCLU_CART_ADIC.producto_exclusion
  is 'Producto de la exclusion adicional, DTH o SMA';
comment on column PROVI_EXCLU_CART_ADIC.valor_exclusion
  is 'Valor de la excelusi�n adicional';
comment on column PROVI_EXCLU_CART_ADIC.observacion_exclusion
  is 'Descripci�n para la exclusi�n adicional';
comment on column PROVI_EXCLU_CART_ADIC.estado
  is 'Indica el Estado de la Exclusi�n del registro; ''I''=''Inactivo'', ''A''=''Activo''';
comment on column PROVI_EXCLU_CART_ADIC.estado_exclusion
  is 'Indica el Estado del registro en el Proceso de Provisi�n; ''P''=''Procesada'', ''C''=''Contabilizada''';
comment on column PROVI_EXCLU_CART_ADIC.fecha_registro
  is 'Fecha en que se inserta el registro de exclusi�n';
comment on column PROVI_EXCLU_CART_ADIC.fecha_exclusion
  is 'Fecha de la exclusi�n adicional (generalmente fin de mes)';
comment on column PROVI_EXCLU_CART_ADIC.usuario_exclusion
  is 'Usuario que registra la exclusion adicional';
comment on column PROVI_EXCLU_CART_ADIC.fecha_modificacion
  is 'Fecha en que se modifica la exclusi�n adicional ya registrada';
comment on column PROVI_EXCLU_CART_ADIC.usuario_modificacion
  is 'Usuario que realiza la modificaci�n de la exclusi�n adicional ya registrada';
  
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table PROVI_EXCLU_CART_ADIC
  add constraint PK_EXCLU_CART_ADIC primary key (ID_CIA_EXCLUSION, MES_EXCLUISION, ANIO_EXCLUSION, PRODUCTO_EXCLUSION)
  using index 
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
