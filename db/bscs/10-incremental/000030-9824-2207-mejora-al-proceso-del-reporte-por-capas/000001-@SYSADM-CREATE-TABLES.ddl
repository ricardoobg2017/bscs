-- Create table
create table RPC_CAPAS_BITACORA
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(2),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_BITACORA.ID_BITACORA
  is 'Pk de la tabla';
comment on column RPC_CAPAS_BITACORA.IDENTIFICADOR
  is 'C�digo de la tabla RPC_CAPAS_DETALLE';
comment on column RPC_CAPAS_BITACORA.CONTADOR
  is 'N�mero de  veces en que se ejecuta el paso';
comment on column RPC_CAPAS_BITACORA.FECHA_FIN
  is 'Fecha fin del paso';
comment on column RPC_CAPAS_BITACORA.NOMBRE_DEL_SH
  is 'Nombre del shell sh_llena_balance.sh';
comment on column RPC_CAPAS_BITACORA.DESCRIPCION
  is 'Detalle del paso';
comment on column RPC_CAPAS_BITACORA.PERIODO
  is 'Fecha que se procesa';
comment on column RPC_CAPAS_BITACORA.CUSTOMER
  is 'Id Cliente que utiliza uno de los pasos';
comment on column RPC_CAPAS_BITACORA.FECHA
  is 'Fecha de inserci�n';
comment on column RPC_CAPAS_BITACORA.ESTADO
  is 'E = Error   D = No Data    R = Revisado';

  
-- Create table RPC_CAPAS_BITACORA_M
create table RPC_CAPAS_BITACORA_M
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(10),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_BITACORA_M.ID_BITACORA
  is 'Pk de la tabla';
comment on column RPC_CAPAS_BITACORA_M.IDENTIFICADOR
  is 'C�digo de la tabla RPC_CAPAS_DETALLE';
comment on column RPC_CAPAS_BITACORA_M.CONTADOR
  is 'N�mero de  veces en que se ejecuta el paso';
comment on column RPC_CAPAS_BITACORA_M.FECHA_FIN
  is 'Fecha fin del paso';
comment on column RPC_CAPAS_BITACORA_M.NOMBRE_DEL_SH
  is 'Nombre del shell sh_main.sh';
comment on column RPC_CAPAS_BITACORA_M.DESCRIPCION
  is 'Detalle del paso';
comment on column RPC_CAPAS_BITACORA_M.PERIODO
  is 'Fecha que se procesa';
comment on column RPC_CAPAS_BITACORA_M.CUSTOMER
  is 'Id Cliente que utiliza uno de los pasos';
comment on column RPC_CAPAS_BITACORA_M.FECHA
  is 'Fecha de inserci�n';
comment on column RPC_CAPAS_BITACORA_M.ESTADO
  is 'E = Error   D = No Data    R = Revisado';
  
-- Create table RPC_CAPAS_FECHAS
create table RPC_CAPAS_FECHAS
(
  ID_FECHAS             NUMBER(10) not null,
  FECHA_INICIO          DATE,
  FECHA_FIN             DATE,
  DIA                   VARCHAR2(2),
  ESTADO_EJECUCION_LLB  VARCHAR2(2),
  FECHA_INI_PRO_LLB     DATE,
  FECHA_FIN_PRO_LLB     DATE,
  CONTADOR_LLB          NUMBER(2),
  ESTADO_EJECUCION_MAIN VARCHAR2(2),
  FECHA_INI_PRO_MAIN    DATE,
  FECHA_FIN_PRO_MAIN    DATE,
  CONTADOR_MAIN         NUMBER(2),
  ESTADO                NUMBER(2),
  ESTADO_MAIN           NUMBER(2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_FECHAS.ID_FECHAS
  is 'Pk de la tabla';
comment on column RPC_CAPAS_FECHAS.FECHA_INICIO
  is 'Fecha inicio del proceso';
comment on column RPC_CAPAS_FECHAS.FECHA_FIN
  is 'Fecha fin del proceso';
comment on column RPC_CAPAS_FECHAS.DIA
  is 'D�a de corte';
comment on column RPC_CAPAS_FECHAS.ESTADO_EJECUCION_LLB
  is 'S = En Ejecuci�n     P = Procesado';
comment on column RPC_CAPAS_FECHAS.FECHA_INI_PRO_LLB
  is 'Fecha inicio de ejecuci�n';
comment on column RPC_CAPAS_FECHAS.FECHA_FIN_PRO_LLB
  is 'Fecha fin de ejecuci�n';
comment on column RPC_CAPAS_FECHAS.CONTADOR_LLB
  is 'N�mero de veces de ejecuci�n del shell sh_llena_balance.sh';
comment on column RPC_CAPAS_FECHAS.ESTADO_EJECUCION_MAIN
  is 'S = En Ejecuci�n     P = Procesado';
comment on column RPC_CAPAS_FECHAS.FECHA_INI_PRO_MAIN
  is 'Fecha inicio de ejecuci�n';
comment on column RPC_CAPAS_FECHAS.FECHA_FIN_PRO_MAIN
  is 'Fecha fin de ejecuci�n';
comment on column RPC_CAPAS_FECHAS.CONTADOR_MAIN
  is 'N�mero de veces de ejecuci�n del shell sh_main.sh';
comment on column RPC_CAPAS_FECHAS.ESTADO
  is 'Paso de ejecuci�n sh_llena_balance.sh';
comment on column RPC_CAPAS_FECHAS.ESTADO_MAIN
  is 'Paso de ejecuci�n sh_main.sh';

-- Create table RPC_CAPAS_DETALLE
create table RPC_CAPAS_DETALLE
(
  ID_CAPAS    NUMBER,
  DESCRIPCION VARCHAR2(60),
  ESTADO      VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_DETALLE.ID_CAPAS
  is 'Id de la tabla';
comment on column RPC_CAPAS_DETALLE.DESCRIPCION
  is 'Descripci�n del paso';
comment on column RPC_CAPAS_DETALLE.ESTADO
  is 'A = Activo    I = Inactivo';
  
 -- Create table RPC_CAPAS_DETALLE_MAIN
create table RPC_CAPAS_DETALLE_MAIN
(
  ID_CAPAS    NUMBER,
  DESCRIPCION VARCHAR2(60),
  ESTADO      VARCHAR2(2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_DETALLE_MAIN.ID_CAPAS
  is 'Id de la tabla';
comment on column RPC_CAPAS_DETALLE_MAIN.DESCRIPCION
  is 'Descripci�n del paso';
comment on column RPC_CAPAS_DETALLE_MAIN.ESTADO
  is 'A = Activo     I = Inactivo';

-- Create table RPC_CAPAS_PARAMETROS
create table RPC_CAPAS_PARAMETROS
(
  CODIGO      VARCHAR2(30),
  DESCRIPCION VARCHAR2(300),
  VALOR1      VARCHAR2(1000),
  VALOR2      VARCHAR2(1000),
  ESTADO      VARCHAR2(2),
  HILO        NUMBER
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_PARAMETROS.CODIGO
  is 'Id de la tabla';
comment on column RPC_CAPAS_PARAMETROS.DESCRIPCION
  is 'Descripci�n del par�metro';
comment on column RPC_CAPAS_PARAMETROS.VALOR1
  is 'Valor 1';
comment on column RPC_CAPAS_PARAMETROS.VALOR2
  is 'Valor 2';
comment on column RPC_CAPAS_PARAMETROS.ESTADO
  is 'A = Activo     I = Inactivo';
comment on column RPC_CAPAS_PARAMETROS.HILO
  is 'Hilos a ejecutar';
  
  
-- Create table RPC_CAPAS_VALIDA_POR
create table RPC_CAPAS_VALIDA_POR
(
  FECHA_CORTE     DATE,
  PERIODOS        DATE,
  REGIONES        VARCHAR2(4),
  USUARIO_INGRESO VARCHAR2(20),
  FECHA_INGRESO   DATE,
  POR_RECUPERADO  NUMBER(8,2)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_VALIDA_POR.FECHA_CORTE
  is 'Fecha de corte del proceso';
comment on column RPC_CAPAS_VALIDA_POR.PERIODOS
  is 'Fechas incluidas en el rango de las fechas de corte';
comment on column RPC_CAPAS_VALIDA_POR.REGIONES
  is 'UIO = QUITO     GYE = GUAYAQUIL';
comment on column RPC_CAPAS_VALIDA_POR.USUARIO_INGRESO
  is 'Usuario que ingresa ';
comment on column RPC_CAPAS_VALIDA_POR.FECHA_INGRESO
  is 'Fecha de ingreso';
comment on column RPC_CAPAS_VALIDA_POR.POR_RECUPERADO
  is 'Porcentaje calculado por el proceso';


-- Create table RPC_CAPAS_PARAMETROS_ALARMA
create table RPC_CAPAS_PARAMETROS_ALARMA
(
  ID_PARAMETRO NUMBER(10),
  FROM_NAME    VARCHAR2(100),
  TO_NAME      VARCHAR2(100),
  CC           VARCHAR2(100),
  CCO          VARCHAR2(100),
  ESTADO       VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_PARAMETROS_ALARMA.ID_PARAMETRO
  is 'Id de la tabla';
comment on column RPC_CAPAS_PARAMETROS_ALARMA.FROM_NAME
  is 'Campo origen';
comment on column RPC_CAPAS_PARAMETROS_ALARMA.TO_NAME
  is 'Campo para';
comment on column RPC_CAPAS_PARAMETROS_ALARMA.CC
  is 'Campo Copia';
comment on column RPC_CAPAS_PARAMETROS_ALARMA.CCO
  is 'Campo Copia oculta';
comment on column RPC_CAPAS_PARAMETROS_ALARMA.ESTADO
  is 'A = Activo     I = Inactivo';
-- Create/Recreate primary, unique and foreign key constraints 
alter table RPC_CAPAS_PARAMETROS_ALARMA
  add constraint PK_RPC_ALARMA primary key (ID_PARAMETRO)
  deferrable
  using index 
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
-- Create table RPC_CAPAS_ALARMA
create table RPC_CAPAS_ALARMA
(
  ID_ALARMA    NUMBER not null,
  ID_PARAMETRO NUMBER(10),
  SUBJECT      VARCHAR2(100),
  DESCRIPCION  VARCHAR2(1000),
  ESTADO       VARCHAR2(1)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_ALARMA.ID_ALARMA
  is 'Id de la tabla';
comment on column RPC_CAPAS_ALARMA.ID_PARAMETRO
  is 'Campo de la tabla RPC_CAPAS_PARAMETROS_ALARMA';
comment on column RPC_CAPAS_ALARMA.SUBJECT
  is 'Asunto';
comment on column RPC_CAPAS_ALARMA.DESCRIPCION
  is 'Descripci�n del mensaje';
comment on column RPC_CAPAS_ALARMA.ESTADO
  is 'A = Activo     I = Inactivo';
-- Create/Recreate primary, unique and foreign key constraints 
alter table RPC_CAPAS_ALARMA
  add constraint PK_RPC_ALARMA1 primary key (ID_ALARMA)
  using index 
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table RPC_CAPAS_ALARMA
  add constraint FK_RPC_ALARMA1 foreign key (ID_PARAMETRO)
  references RPC_CAPAS_PARAMETROS_ALARMA (ID_PARAMETRO);
  
  
-- Create table RPC_PERIODOS_VAL_FACT
create table RPC_PERIODOS_VAL_FACT
(
  CIERRE_PERIODO DATE not null,
  REVISADO       VARCHAR2(1),
  HILO           NUMBER
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_PERIODOS_VAL_FACT.CIERRE_PERIODO
  is 'Fecha de los periodos a ejecutar ';
comment on column RPC_PERIODOS_VAL_FACT.REVISADO
  is 'S = Revisado';
comment on column RPC_PERIODOS_VAL_FACT.HILO
  is 'Hilo a ejecutar';
  
--
CREATE OR REPLACE TYPE "COT_NUMBER_POR"  is table of number;
CREATE OR REPLACE TYPE "COT_VARCHAR_PO"  is table of varchar2(5);
--
-- Create sequence 
create sequence SEQ_REPORTE_CAPAS
minvalue 1
maxvalue 999999999999999999999999999
start with 281
increment by 1
cache 20;
--
-- Create sequence 
create sequence SEQ_BITACORA
minvalue 1
maxvalue 999999999999999999999999999
start with 52801
increment by 1
cache 20;
--
-- Create sequence 
create sequence SEQ_BITACORA_MAIN
minvalue 1
maxvalue 999999999999999999999999999
start with 701
increment by 1
cache 20;
/
