

-- Create/Recreate indexes 
create index SYSADM.TB_SAP_TRAMAS_REC_CASIDX01 on SYSADM.TB_SAP_TRAMAS_REC_CAS (PROCESO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1200K
    minextents 1
    maxextents unlimited
  );
  
create index SYSADM.TB_SAP_TRAMAS_REC_CASIDX02 on SYSADM.TB_SAP_TRAMAS_REC_CAS (FECHA_INGRESO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1200K
    minextents 1
    maxextents unlimited
  );  
  

create index SYSADM.TB_SAP_TRAMAS_REC_CASIDX03 on SYSADM.TB_SAP_TRAMAS_REC_CAS (ESTADO_DESPACHO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1200K
    minextents 1
    maxextents unlimited
  );    
  
 
create index SYSADM.TB_SAP_TRAMAS_REC_CASIDX04 on SYSADM.TB_SAP_TRAMAS_REC_CAS (ESTADO_REPROCESO)
  tablespace DATA2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1200K
    minextents 1
    maxextents unlimited
  );     
  