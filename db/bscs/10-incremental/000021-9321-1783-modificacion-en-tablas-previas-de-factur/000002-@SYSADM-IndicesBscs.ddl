/******************** CREACION DE INDICES ***********************/
create index IDX_CICLOS  on FA_CICLOS_AXIS_BSCS(id_ciclo) 
tablespace IND 
Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
unlimited pctincrease 0);
---
create index IDX_SUBCICLOS  on FA_CICLOS_AXIS_BSCS(ID_CICLO_ADMIN) 
tablespace IND 
Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
unlimited pctincrease 0);
---
create index IDX_SUBCICLO_FACTURA  on sysadm.BL_CUENTAS_FACTURAR(ID_CICLO_ADMIN) 
tablespace IND 
Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
unlimited pctincrease 0);
