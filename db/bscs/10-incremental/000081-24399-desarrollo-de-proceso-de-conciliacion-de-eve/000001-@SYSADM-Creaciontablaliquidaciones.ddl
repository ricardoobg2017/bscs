
-- Create table
create table PC_ORIGENES_DATOS
(
  NOMBRE_FUENTE VARCHAR2(50),
  ENLACE        VARCHAR2(50),
  ESTADO        VARCHAR2(1),
  DESCRIPCION   VARCHAR2(100),
  USUARIO       VARCHAR2(30)
)

tablespace REP_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column PC_ORIGENES_DATOS.NOMBRE_FUENTE
  is 'Nombre de la BD donde se conectara';
comment on column PC_ORIGENES_DATOS.ENLACE
  is 'Nombre del dblink que se usara para la conexion';
comment on column PC_ORIGENES_DATOS.ESTADO
  is 'Estado del dblink a la base A="Activo" I="Inactivo"';
comment on column PC_ORIGENES_DATOS.DESCRIPCION
  is 'Descripcion del dblink';
comment on column PC_ORIGENES_DATOS.USUARIO
  is 'Usuario de creacion de la tabla a la que se quiere acceder';
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Create table
create table PC_CONTROL_TABLAS_TMP
(
  NOMBRE_TABLA  VARCHAR2(50),
  TIPO_TABLA    VARCHAR2(3),
  ESTADO        VARCHAR2(1),
  TIPO_SUB_PROD VARCHAR2(10),
  USUARIO       VARCHAR2(20),
  FECHA         DATE,
  FUENTE        VARCHAR2(40)
)

tablespace REP_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column PC_CONTROL_TABLAS_TMP.NOMBRE_TABLA
  is 'Nombre de tabla temporal usada en la ejecucion';
comment on column PC_CONTROL_TABLAS_TMP.TIPO_TABLA
  is 'Dos tipos de tabla REC="Recaudacion" REG="Registro"';
comment on column PC_CONTROL_TABLAS_TMP.ESTADO
  is 'Dos Estados A="Activo", R="Recaudado"';
comment on column PC_CONTROL_TABLAS_TMP.TIPO_SUB_PROD
  is 'Se almacenan 3 tipos de Sub_Producto AUT_CF,TAR_N,TAR_R';
comment on column PC_CONTROL_TABLAS_TMP.USUARIO
  is 'Usuario de Base de Datos que creara el registro';
comment on column PC_CONTROL_TABLAS_TMP.FECHA
  is 'Fecha de Creacion de la tabla';
comment on column PC_CONTROL_TABLAS_TMP.FUENTE
  is 'Origen de la tabla (BD donde fue creada o modificada)';
  
------------------------------------------------------------------------------
