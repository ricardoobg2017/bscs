-- Create table
create table CUSTOMER_ALL_ACT
(
  customer_id            INTEGER not null,
  customer_id_high       INTEGER,
  custcode               VARCHAR2(24) not null,
  csst                   VARCHAR2(2),
  cstype                 VARCHAR2(1),
  csactivated            DATE,
  csdeactivated          DATE,
  customer_dealer        VARCHAR2(1),
  cstype_date            DATE,
  cslevel                VARCHAR2(2),
  cscusttype             VARCHAR2(1),
  cslvlname              VARCHAR2(10),
  tmcode                 INTEGER,
  prgcode                VARCHAR2(10),
  termcode               NUMBER(4),
  csclimit               FLOAT,
  cscurbalance           FLOAT,
  csdepdate              DATE,
  billcycle              VARCHAR2(2),
  cstestbillrun          VARCHAR2(1),
  bill_layout            INTEGER,
  paymntresp             VARCHAR2(1),
  target_reached         INTEGER,
  pcsmethpaymnt          VARCHAR2(1),
  passportno             VARCHAR2(30),
  birthdate              DATE,
  dunning_flag           VARCHAR2(1),
  comm_no                VARCHAR2(20),
  pos_comm_type          INTEGER,
  btx_password           VARCHAR2(8),
  btx_user               VARCHAR2(14),
  settles_p_month        VARCHAR2(12),
  cashretour             INTEGER,
  cstradecode            VARCHAR2(10),
  cspassword             VARCHAR2(20),
  cspromotion            VARCHAR2(1),
  cscompregno            VARCHAR2(20),
  cscomptaxno            VARCHAR2(20),
  csreason               INTEGER,
  cscollector            VARCHAR2(16),
  cscontresp             VARCHAR2(1),
  csdeposit              FLOAT,
  cscredit_date          DATE,
  cscredit_remark        VARCHAR2(2000),
  suspended              DATE,
  reactivated            DATE,
  prev_balance           FLOAT,
  lbc_date               DATE,
  employee               VARCHAR2(1),
  company_type           VARCHAR2(1),
  crlimit_exc            VARCHAR2(1),
  area_id                INTEGER,
  costcenter_id          INTEGER,
  csfedtaxid             VARCHAR2(5),
  credit_rating          INTEGER,
  cscredit_status        VARCHAR2(2),
  deact_create_date      DATE,
  deact_receip_date      DATE,
  edifact_addr           VARCHAR2(32),
  edifact_user_flag      VARCHAR2(1),
  edifact_flag           VARCHAR2(1),
  csdeposit_due_date     DATE,
  calculate_deposit      VARCHAR2(1),
  tmcode_date            DATE,
  cslanguage             INTEGER,
  csrentalbc             VARCHAR2(2),
  id_type                INTEGER,
  user_lastmod           VARCHAR2(16),
  csentdate              DATE default (sysdate) not null,
  csmoddate              DATE,
  csmod                  VARCHAR2(1),
  csnationality          INTEGER,
  csbillmedium           INTEGER,
  csitembillmedium       INTEGER,
  customer_id_ext        VARCHAR2(10),
  csreseller             VARCHAR2(1),
  csclimit_o_tr1         INTEGER,
  csclimit_o_tr2         INTEGER,
  csclimit_o_tr3         INTEGER,
  cscredit_score         VARCHAR2(40),
  cstraderef             VARCHAR2(2000),
  cssocialsecno          VARCHAR2(20),
  csdrivelicence         VARCHAR2(20),
  cssex                  VARCHAR2(1),
  csemployer             VARCHAR2(40),
  wpid                   INTEGER,
  csprepayment           VARCHAR2(1),
  csremark_1             VARCHAR2(40),
  csremark_2             VARCHAR2(40),
  ma_id                  INTEGER,
  cssumaddr              VARCHAR2(1),
  bill_information       VARCHAR2(1),
  dealer_id              INTEGER,
  dunning_mode           VARCHAR2(4),
  not_valid              VARCHAR2(1),
  cscrdcheck_agreed      VARCHAR2(1),
  marital_status         INTEGER,
  expect_pay_curr_id     INTEGER,
  convratetype_payment   INTEGER,
  refund_curr_id         INTEGER,
  convratetype_refund    INTEGER,
  srcode                 INTEGER,
  currency               INTEGER not null,
  primary_doc_currency   INTEGER,
  secondary_doc_currency INTEGER,
  prim_convratetype_doc  INTEGER,
  sec_convratetype_doc   INTEGER,
  rec_version            INTEGER default (0) not null,
  anonymous_customer     VARCHAR2(1),
  dummy_customer         VARCHAR2(1),
  dummy_owner_id         INTEGER,
  insertiondate          DATE default SYSDATE not null,
  lastmoddate            DATE
)
tablespace DATA3
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table CUSTOMER_ALL_ACT
  is 'customer/dealer names and other information';
-- Add comments to the columns 
comment on column CUSTOMER_ALL_ACT.customer_id
  is 'Customer Identifier.
DAB constraint CUSTOMER_ID !=0,
Logical foreign key to CUSTOMER_BASE.
Dealer do not reference to CUSTOMER_BASE.';
comment on column CUSTOMER_ALL_ACT.customer_id_high
  is 'pointer to customer of upper hierarchy level';
comment on column CUSTOMER_ALL_ACT.custcode
  is 'customer code in form Kd.AK.KSt.TN
Eg.: 1.20398400, 2.10, 2.45.100.100.100067, 3.123.120.150.190023, 7.2398764.130.170.100968';
comment on column CUSTOMER_ALL_ACT.csst
  is 'state';
comment on column CUSTOMER_ALL_ACT.cstype
  is 'type:
a=active
d=deactive
i=interested
s=suspended (g=gesperrt)
(b=beantragt)';
comment on column CUSTOMER_ALL_ACT.csactivated
  is 'date of activation';
comment on column CUSTOMER_ALL_ACT.csdeactivated
  is 'date of deactivation';
comment on column CUSTOMER_ALL_ACT.customer_dealer
  is 'C-customer, D-dealer';
comment on column CUSTOMER_ALL_ACT.cstype_date
  is 'date when CSTYPE was changed';
comment on column CUSTOMER_ALL_ACT.cslevel
  is 'Customer Level
The domain splits in the following parts:
Value for business partner:
0- External Carrier or Roaming Partner
Values for Customers and Service providers:
10-
20-
30-
40-
Values for Dealers:
All values defined for Sales Agents.
FK: CUSTOMER_LEVEL';
comment on column CUSTOMER_ALL_ACT.cscusttype
  is 'Customer Type: C = Consumer, B = Business';
comment on column CUSTOMER_ALL_ACT.cslvlname
  is 'name of Kd, AK, or KSt on this level';
comment on column CUSTOMER_ALL_ACT.tmcode
  is 'pointer to active tariff model';
comment on column CUSTOMER_ALL_ACT.prgcode
  is 'price group code';
comment on column CUSTOMER_ALL_ACT.termcode
  is 'payment terms';
comment on column CUSTOMER_ALL_ACT.csclimit
  is 'credit limit';
comment on column CUSTOMER_ALL_ACT.cscurbalance
  is 'Current balance of the customers account. Not including deposits and interests for deposits';
comment on column CUSTOMER_ALL_ACT.csdepdate
  is 'Date Deposit Paid';
comment on column CUSTOMER_ALL_ACT.billcycle
  is 'bill cycle in form 01 - 10';
comment on column CUSTOMER_ALL_ACT.cstestbillrun
  is 'entry for special control group, range A to Z';
comment on column CUSTOMER_ALL_ACT.bill_layout
  is 'Bill-layout for Customer (link to FORM_LAYOUT)';
comment on column CUSTOMER_ALL_ACT.paymntresp
  is 'payment responsibility if not space';
comment on column CUSTOMER_ALL_ACT.target_reached
  is 'Target reached value';
comment on column CUSTOMER_ALL_ACT.pcsmethpaymnt
  is 'method of payment; S = Scheck, B = BAR';
comment on column CUSTOMER_ALL_ACT.passportno
  is 'Passport number';
comment on column CUSTOMER_ALL_ACT.birthdate
  is 'birthdate';
comment on column CUSTOMER_ALL_ACT.dunning_flag
  is 'X = no dunning for this customer';
comment on column CUSTOMER_ALL_ACT.comm_no
  is 'number of the communication port';
comment on column CUSTOMER_ALL_ACT.pos_comm_type
  is 'Point of Sales (PoS) Communication Type
The CUSTOMER_ALL_ACT.POS_COMM_TYPE attribute specifies the Point of Sales (PoS) communication types.
The PoS connects are defined in the POSCONNECT table.
Further Information:
- POSCONNECT table comment';
comment on column CUSTOMER_ALL_ACT.btx_password
  is 'password for BTX';
comment on column CUSTOMER_ALL_ACT.btx_user
  is 'BTX-user';
comment on column CUSTOMER_ALL_ACT.settles_p_month
  is 'this field contains one character for each month of the year.  values for each character: 0=payment within 1 month, 1=payment one month too late, 2=payment two months too late, 3=payment three months too late.';
comment on column CUSTOMER_ALL_ACT.cashretour
  is 'Number of bounced checks and retoured direct debits';
comment on column CUSTOMER_ALL_ACT.cstradecode
  is 'fkey to table TRADE';
comment on column CUSTOMER_ALL_ACT.cspassword
  is 'password for customer info';
comment on column CUSTOMER_ALL_ACT.cspromotion
  is 'Y=responded to promotion';
comment on column CUSTOMER_ALL_ACT.cscompregno
  is 'registration number';
comment on column CUSTOMER_ALL_ACT.cscomptaxno
  is 'tax number';
comment on column CUSTOMER_ALL_ACT.csreason
  is 'Reason for status-change (link to REASONSTATUS_ALL)';
comment on column CUSTOMER_ALL_ACT.cscollector
  is 'cashcollector';
comment on column CUSTOMER_ALL_ACT.cscontresp
  is 'responsible for contract';
comment on column CUSTOMER_ALL_ACT.csdeposit
  is 'initial deposit amount';
comment on column CUSTOMER_ALL_ACT.cscredit_date
  is 'date of last credit scoring.';
comment on column CUSTOMER_ALL_ACT.cscredit_remark
  is 'remark for last credit-check';
comment on column CUSTOMER_ALL_ACT.suspended
  is 'date of suspension';
comment on column CUSTOMER_ALL_ACT.reactivated
  is 'date of reactivation';
comment on column CUSTOMER_ALL_ACT.prev_balance
  is 'Previous Balance
The CUSTOMER_ALL_ACT.PREV_BALANCE attribute specifies the previous balance, what means the financial balance of the customer after the last billing run (monetary value).';
comment on column CUSTOMER_ALL_ACT.lbc_date
  is 'last billcycle date';
comment on column CUSTOMER_ALL_ACT.employee
  is 'X=customer is an employee';
comment on column CUSTOMER_ALL_ACT.company_type
  is 'Type of Company:- F Foreign, R Related, I Internal';
comment on column CUSTOMER_ALL_ACT.crlimit_exc
  is 'X = Credit Limit Exceeded';
comment on column CUSTOMER_ALL_ACT.area_id
  is 'region which is attached to this Customer (fk to AREA)';
comment on column CUSTOMER_ALL_ACT.costcenter_id
  is 'costcenter which is attached to this customer (fk to COSTCENTER)';
comment on column CUSTOMER_ALL_ACT.csfedtaxid
  is 'Federal Tax ID';
comment on column CUSTOMER_ALL_ACT.credit_rating
  is 'Credit Rating 0 to 100';
comment on column CUSTOMER_ALL_ACT.cscredit_status
  is 'Credit Status';
comment on column CUSTOMER_ALL_ACT.deact_create_date
  is 'Date of Deactivation (Date on Customer Letter)';
comment on column CUSTOMER_ALL_ACT.deact_receip_date
  is 'Receipt of Written Deactivation Notification';
comment on column CUSTOMER_ALL_ACT.edifact_addr
  is 'EDIFACT Address';
comment on column CUSTOMER_ALL_ACT.edifact_user_flag
  is 'X = Edifact User;';
comment on column CUSTOMER_ALL_ACT.edifact_flag
  is 'X indicates if an EDIFACT user';
comment on column CUSTOMER_ALL_ACT.csdeposit_due_date
  is 'date when the deposit have to be paid';
comment on column CUSTOMER_ALL_ACT.calculate_deposit
  is 'flag for calculate the deposit by billing';
comment on column CUSTOMER_ALL_ACT.tmcode_date
  is 'date since when the tmcode is valid';
comment on column CUSTOMER_ALL_ACT.cslanguage
  is 'customers language. fkey to LANGUAGE.LNG_ID';
comment on column CUSTOMER_ALL_ACT.csrentalbc
  is 'the rental-bill bill-cycle';
comment on column CUSTOMER_ALL_ACT.id_type
  is 'fkey to table ID_TYPE, type of entered ID number in field PASSPORTNO';
comment on column CUSTOMER_ALL_ACT.user_lastmod
  is 'name of user who made the last modification of this record';
comment on column CUSTOMER_ALL_ACT.csentdate
  is 'start date';
comment on column CUSTOMER_ALL_ACT.csmoddate
  is 'date last file maint change';
comment on column CUSTOMER_ALL_ACT.csmod
  is 'record modified flag';
comment on column CUSTOMER_ALL_ACT.csnationality
  is 'nationality of the customer (fkey to table country)';
comment on column CUSTOMER_ALL_ACT.csbillmedium
  is 'Medium for the monthly-bill of the customer (fkey to table BILL_MEDIUM)';
comment on column CUSTOMER_ALL_ACT.csitembillmedium
  is 'Medium for the itemized-bill of the customer (fkey to table BILL_MEDIUM)';
comment on column CUSTOMER_ALL_ACT.customer_id_ext
  is 'convert the PPC-file dealer code to BSCS dealer_id';
comment on column CUSTOMER_ALL_ACT.csreseller
  is 'X = customer is reseller';
comment on column CUSTOMER_ALL_ACT.csclimit_o_tr1
  is 'Open Amount Threshold 1, FK to MPUTRTAB';
comment on column CUSTOMER_ALL_ACT.csclimit_o_tr2
  is 'Open Amount Threshold 2,FK to MPUTRTAB';
comment on column CUSTOMER_ALL_ACT.csclimit_o_tr3
  is 'Open Amount Threshold 3,FK to MPUTRTAB';
comment on column CUSTOMER_ALL_ACT.cscredit_score
  is 'Credit score';
comment on column CUSTOMER_ALL_ACT.cstraderef
  is 'Trade reference for credit scoring';
comment on column CUSTOMER_ALL_ACT.cssocialsecno
  is 'Social security number.';
comment on column CUSTOMER_ALL_ACT.csdrivelicence
  is 'Driving licence number.';
comment on column CUSTOMER_ALL_ACT.cssex
  is 'Gender (M=male,F=female).';
comment on column CUSTOMER_ALL_ACT.csemployer
  is 'Name of the employer.';
comment on column CUSTOMER_ALL_ACT.wpid
  is 'Welcome Procedure / FK to WELCPME_PROC.WPID';
comment on column CUSTOMER_ALL_ACT.csprepayment
  is 'X = this customer should prepay.';
comment on column CUSTOMER_ALL_ACT.csremark_1
  is 'Remark 1 about the customer.';
comment on column CUSTOMER_ALL_ACT.csremark_2
  is 'Remark 2 about the customer.';
comment on column CUSTOMER_ALL_ACT.ma_id
  is 'Register a Marketing-Action-ID for a Sales-Force-Member, FK to MARKETING_ACTION.MA_ID';
comment on column CUSTOMER_ALL_ACT.cssumaddr
  is 'C=the contract address will be used for sum sheet, B=the bill address will';
comment on column CUSTOMER_ALL_ACT.bill_information
  is 'X=the respective customer has to be processed in bill information runs. Default is NULL.';
comment on column CUSTOMER_ALL_ACT.dealer_id
  is 'Dealer who the data is assigned to-. Logical FK: CUSTOMER_ALL_ACT.CUSTOMER_ID.';
comment on column CUSTOMER_ALL_ACT.dunning_mode
  is 'Dunning mode as proposed by ext. Risk Management System.';
comment on column CUSTOMER_ALL_ACT.not_valid
  is 'Indicates, that data have not been validated by the application (=X).
An X is only allowed if CUSTOMER_ALL_ACT.CSTYPE="i".';
comment on column CUSTOMER_ALL_ACT.cscrdcheck_agreed
  is 'X=agreement to external credit check.';
comment on column CUSTOMER_ALL_ACT.marital_status
  is 'Marital Status. FK: MARITAL_STATUS.MAS_ID.';
comment on column CUSTOMER_ALL_ACT.expect_pay_curr_id
  is 'The currency this customer is supposed to pay his invoice in.
Will not be used in case this customer is not payment responsible.
Will be set to NULL in case this customer`s expected payment currency is configured to be the home currency or in case no expected payment currency is specified for this customer.
FK: FORCURR.FC_ID.';
comment on column CUSTOMER_ALL_ACT.convratetype_payment
  is 'The conversion rate type to be used when payments coming in from this customer are registered.
Note that this conversion rate type must be set if an expected payment currency
is specified for this customer, but in any case a payment comes in from this customer.
FK: CONVRATETYPES.CONVRATETYPE_ID.';
comment on column CUSTOMER_ALL_ACT.refund_curr_id
  is 'The currency this customer is supposed to receive refunds in.
Will not be used in case this customer is not payment responsible.
Will be set to NULL in case customer`s refunding currency is configured to be the home currency.
FK: FORCURR.FC_ID.';
comment on column CUSTOMER_ALL_ACT.convratetype_refund
  is 'The conversion rate type to be used when this customer is refunded.
Mandatory , if REFUND_CURR_ID is not NULL.
FK: CONVRATETYPES.CONVRATETYPE_ID.';
comment on column CUSTOMER_ALL_ACT.srcode
  is 'Identifier for a subscription fee reduction action.
FK to MPUSRTAB.SRCODE.';
comment on column CUSTOMER_ALL_ACT.currency
  is 'Currency
The CUSTOMER_ALL_ACT.CURRENCY attribute specifies the currency, which is used for the customer.
The currencies are defined in the FORCURR table.
Further Information:
- FORCURR table comment';
comment on column CUSTOMER_ALL_ACT.primary_doc_currency
  is 'Primary Document Currency.
Not NULL for payment responsible customers.
FK: FORCURR';
comment on column CUSTOMER_ALL_ACT.secondary_doc_currency
  is 'Secondary Document Currency.
FK: FORCURR';
comment on column CUSTOMER_ALL_ACT.prim_convratetype_doc
  is 'Conversion Rate Type to be used for the Primary Document Currency.
 Mandatory, if PRIMARY_DOC_CURRENCY is not NULL.
FK: CONVRATETYPES';
comment on column CUSTOMER_ALL_ACT.sec_convratetype_doc
  is 'Conversion Rate Type to be used for the Secondary Document Currency.
 Mandatory, if SEC_DOC_CURRENCY is not NULL.
FK: CONVRATETYPES';
comment on column CUSTOMER_ALL_ACT.rec_version
  is 'Counter for multiuser access';
comment on column CUSTOMER_ALL_ACT.anonymous_customer
  is 'X - anonymous customer. Used by prepaid processes.';
comment on column CUSTOMER_ALL_ACT.dummy_customer
  is 'X - dummy customer. Used by prepaid processes.';
comment on column CUSTOMER_ALL_ACT.dummy_owner_id
  is 'Customer Id of dummy owner. A dummy owner is either a service provider or a network operator. Only dummy customers have a dummy owner.';
comment on column CUSTOMER_ALL_ACT.insertiondate
  is 'Insertion date';
comment on column CUSTOMER_ALL_ACT.lastmoddate
  is 'Last modification date';
/*-- Create/Recreate primary, unique and foreign key constraints 
alter table CUSTOMER_ALL_ACT
  add constraint PKCUSTOMER_ALL primary key (CUSTOMER_ID)
  using index 
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_CONVRATETYPE_PAYMENT foreign key (CONVRATETYPE_PAYMENT)
  references CONVRATETYPES (CONVRATETYPE_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_CONVRATETYPE_REFUND foreign key (CONVRATETYPE_REFUND)
  references CONVRATETYPES (CONVRATETYPE_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_EXPECT_PAY_CURR_ID foreign key (EXPECT_PAY_CURR_ID)
  references FORCURR (FC_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_MA_ID foreign key (MA_ID)
  references MARKETING_ACTION (MA_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_MARITAL_STATUS foreign key (MARITAL_STATUS)
  references MARITAL_STATUS (MAS_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_MPUSRTAB foreign key (SRCODE)
  references MPUSRTAB (SRCODE);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTOMER_REFUND_CURR_ID foreign key (REFUND_CURR_ID)
  references FORCURR (FC_ID);
alter table CUSTOMER_ALL_ACT
  add constraint CUSTWELP foreign key (WPID)
  references WELCOME_PROC (WPID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTALL_CUSTBASE_CSL foreign key (CSLEVEL)
  references CUSTOMER_LEVEL (CUSTOMER_LEVEL_CODE);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTALL_RATEPL foreign key (TMCODE)
  references RATEPLAN (TMCODE);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOMER_ALL_DUMMY_OWNER foreign key (DUMMY_OWNER_ID)
  references CUSTOMER_ALL_ACT (CUSTOMER_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOMER_FORCURR foreign key (CURRENCY)
  references FORCURR (FC_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOM_PCONVRTYPE_DOC foreign key (PRIM_CONVRATETYPE_DOC)
  references CONVRATETYPES (CONVRATETYPE_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOM_PDOC_CURR foreign key (PRIMARY_DOC_CURRENCY)
  references FORCURR (FC_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOM_SCONVRTYPE_DOC foreign key (SEC_CONVRATETYPE_DOC)
  references CONVRATETYPES (CONVRATETYPE_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FK_CUSTOM_SDOC_CURR foreign key (SECONDARY_DOC_CURRENCY)
  references FORCURR (FC_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKIBCCYL foreign key (BILLCYCLE)
  references BILLCYCLES (BILLCYCLE);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSARE foreign key (AREA_ID)
  references AREA (AREA_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSBMI foreign key (CSITEMBILLMEDIUM)
  references BILL_MEDIUM (BM_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSBMM foreign key (CSBILLMEDIUM)
  references BILL_MEDIUM (BM_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSCOL foreign key (CSCOLLECTOR)
  references USERS (USERNAME);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSCOS foreign key (COSTCENTER_ID)
  references COSTCENTER (COST_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSLNG foreign key (CSLANGUAGE)
  references LANGUAGE (LNG_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSNAT foreign key (CSNATIONALITY)
  references COUNTRY (COUNTRY_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSOT1 foreign key (CSCLIMIT_O_TR1)
  references MPUTRTAB (TR_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSOT2 foreign key (CSCLIMIT_O_TR2)
  references MPUTRTAB (TR_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSOT3 foreign key (CSCLIMIT_O_TR3)
  references MPUTRTAB (TR_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSPSC foreign key (POS_COMM_TYPE)
  references POSCONNECT (POS_COMM_TYPE);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSRBC foreign key (CSRENTALBC)
  references BILLCYCLES (BILLCYCLE);
alter table CUSTOMER_ALL_ACT
  add constraint FKICSRST foreign key (CSREASON)
  references REASONSTATUS_ALL (RS_ID);
alter table CUSTOMER_ALL_ACT
  add constraint FKIFKCSY foreign key (ID_TYPE)
  references ID_TYPE (IDTYPE_CODE);
alter table CUSTOMER_ALL_ACT
  add constraint FKIPGPGC foreign key (PRGCODE)
  references PRICEGROUP_ALL (PRGCODE);
alter table CUSTOMER_ALL_ACT
  add constraint FKITDCDE foreign key (CSTRADECODE)
  references TRADE_ALL (TRADECODE);
alter table CUSTOMER_ALL_ACT
  add constraint FKITETEC foreign key (TERMCODE)
  references TERMS_ALL (TERMCODE);
-- Create/Recreate check constraints 
alter table CUSTOMER_ALL_ACT
  add constraint CHK_CUSTALL_CUSTOMER_ID
  check (CUSTOMER_ID != 0);
alter table CUSTOMER_ALL_ACT
  add constraint CHK_CUSTOMER_ALL_CSSUMADDR
  check (CSSUMADDR IN ('B','C'));*/
-- Create/Recreate indexes 
create unique index CS_CUSTCODE1 on CUSTOMER_ALL_ACT (CUSTCODE, CSLEVEL)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CS_CUSTID_UP1 on CUSTOMER_ALL_ACT (CUSTOMER_ID_HIGH)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CS_RESELLER1 on CUSTOMER_ALL_ACT (CSRESELLER)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CUST_CUSTCODE1 on CUSTOMER_ALL_ACT (CUSTCODE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKICUSTWELP1 on CUSTOMER_ALL_ACT (WPID)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKIBCCYL1 on CUSTOMER_ALL_ACT (BILLCYCLE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSBMI1 on CUSTOMER_ALL_ACT (CSITEMBILLMEDIUM)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSBMM1 on CUSTOMER_ALL_ACT (CSBILLMEDIUM)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSCOL1 on CUSTOMER_ALL_ACT (CSCOLLECTOR)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT11 on CUSTOMER_ALL_ACT (CSCLIMIT_O_TR1)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT21 on CUSTOMER_ALL_ACT (CSCLIMIT_O_TR2)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT31 on CUSTOMER_ALL_ACT (CSCLIMIT_O_TR3)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSPSC1 on CUSTOMER_ALL_ACT (POS_COMM_TYPE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSRBC1 on CUSTOMER_ALL_ACT (CSRENTALBC)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKIPGPGC1 on CUSTOMER_ALL_ACT (PRGCODE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CSSOCIALSECNO1 on CUSTOMER_ALL_ACT (CSSOCIALSECNO)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IX_CUSTOMER_ALL_LMD1 on CUSTOMER_ALL_ACT (LASTMODDATE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on CUSTOMER_ALL_ACT to BSCSDB_ROLE;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to BSCS_ROLE;
grant select on CUSTOMER_ALL_ACT to CARGA_ECUASISTENCIA_ROL;
grant select on CUSTOMER_ALL_ACT to DOC1;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to EIS_DAT;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to EIS_MIR;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to EIS_PRI;
grant select, update on CUSTOMER_ALL_ACT to GSIDEPURA;
grant select on CUSTOMER_ALL_ACT to JPATINOV;
grant select on CUSTOMER_ALL_ACT to LNK_AIC10_FMS;
grant select on CUSTOMER_ALL_ACT to LNK_BSCS_IDEAS;
grant select on CUSTOMER_ALL_ACT to LNK_DWCOM_GPROCESOS;
grant select on CUSTOMER_ALL_ACT to LNK_FINREPO_BSCS;
grant select on CUSTOMER_ALL_ACT to LNK_GYE_CBL;
grant select on CUSTOMER_ALL_ACT to LNK_IVR13015_OPER;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to LNK_MOVILPOS_USSD;
grant select on CUSTOMER_ALL_ACT to LNK_PREFAC_BULK;
grant select on CUSTOMER_ALL_ACT to LNK_REPCDR_SYS;
grant update on CUSTOMER_ALL_ACT to POSTPAGO_DAT;
grant select, update on CUSTOMER_ALL_ACT to POSTPAGO_MIR;
grant select, update on CUSTOMER_ALL_ACT to POSTPAGO_PRI;
grant select, insert, update, delete, references, alter, index on CUSTOMER_ALL_ACT to PUBLIC;
grant select on CUSTOMER_ALL_ACT to RCC;
grant select on CUSTOMER_ALL_ACT to RELOJ;
grant select on CUSTOMER_ALL_ACT to REPORTE;
grant select on CUSTOMER_ALL_ACT to SCP_DAT;
grant select on CUSTOMER_ALL_ACT to SCP_MIR;
grant select on CUSTOMER_ALL_ACT to SCP_PRI;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to SRE_DAT;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to SRE_MIR;
grant select, insert, update, delete on CUSTOMER_ALL_ACT to SRE_PRI;
grant select on CUSTOMER_ALL_ACT to WLS_JEIS_HUB;
