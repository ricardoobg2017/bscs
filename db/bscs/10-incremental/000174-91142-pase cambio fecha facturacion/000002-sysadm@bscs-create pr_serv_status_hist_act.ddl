-- Create table
create table PR_SERV_STATUS_HIST_ACT
(
  profile_id      INTEGER not null,
  co_id           INTEGER not null,
  sncode          INTEGER not null,
  histno          INTEGER not null,
  status          VARCHAR2(1) not null,
  reason          INTEGER not null,
  transactionno   INTEGER not null,
  valid_from_date DATE,
  entry_date      DATE default (SYSDATE) not null,
  request_id      INTEGER,
  rec_version     INTEGER default (0) not null,
  initiator_type  CHAR(1) default ('C') not null,
  insertiondate   DATE default SYSDATE not null,
  lastmoddate     DATE,
  estado          VARCHAR2(1)
)
tablespace DATA6
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table PR_SERV_STATUS_HIST_ACT
  is 'This table contains the history of service status changes within profiles. For further information see description of the parent table PROFILE_SERVICE.';
-- Add comments to the columns 
comment on column PR_SERV_STATUS_HIST_ACT.profile_id
  is 'Profile identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_ACT.co_id
  is 'Contract identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_ACT.sncode
  is 'Service identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_ACT.histno
  is 'History number beginning with 1.
Sequence for building of the HISTNO is used.';
comment on column PR_SERV_STATUS_HIST_ACT.status
  is 'Status of the service within a profile. Domain:  A (active), D (deactivate), S (suspended in a contract), O (initial value, on-hold status).';
comment on column PR_SERV_STATUS_HIST_ACT.reason
  is 'Reason of status change, possible values are:
0 initial activation
1 following state value of the service within a profile during its life cycle without any external reasons (reasons 2 till 5),
2 Service package change, external reason,
3 Tariff model change, external reason,
4 Profile change (s. PROFILE_CHANGE_HIST), external reason,
5 Contract suspension, external reason.
9 Service is invisible. Reason could not be determined during data migration. Valid only in combination with status D.
For further information see Main HLD for the 82630 feature.
Note for developers with process description: The database cannot guaranty that the value of this attribute corresponds to a reason caused in the reality the status change. For example, if the attribute value is 3, the database cannot guaranty that the RATEPLAN_HIST table obtains the corresponding entry with the new rate plan. For this purpose the attribute TRANSACTIONNO serves (s. example below).
This attribute is used only for performance goals. And the BSCS-program has to guarantee the consistency of data.
Example: a new entry of the PR_SERV_STATUS_HIST_ACT table with the following values has to be inserted (The status change within entries is not allowed, a new entry with the new status has to be inserted.): STATUS = `D`, TRANSACTIONNO=5 and the REASON=3. It means the RATEPLAN_HIST table has to obtain the new entry containing some new rate plan with the same TRANSACTIONNO value as the mentioned above entry has. The TRANSACTIONNO value shows which entries from different tables were simultaneously into the database inserted.';
comment on column PR_SERV_STATUS_HIST_ACT.transactionno
  is 'Transaction number. This attribute is used for reproducing which history entries were simultaneously stored (stored during one transaction with this transaction number). The sequence PR_SERV_TRANS_NO produces attribute values.';
comment on column PR_SERV_STATUS_HIST_ACT.valid_from_date
  is 'From this date on the value of STATUS is valid. At this date the request has been processed. Or from this date the switch has set this status. Note: assume that the service is active. It means, that this table contains the corresponding entry with the STATUS=A. If a new status e.g. D is requested, then the new entry with D-status and VALID_FROM_DATE = NULL has to be inserted into this table. The old profile service status (old entry) has to be used during request pending phase. If the change request is processed the attribute VALID_FROM_DATE gets a not null value and then the old value is not more active. Since VALID_FROM_DATE the new entry is active.';
comment on column PR_SERV_STATUS_HIST_ACT.entry_date
  is 'Date of insertion of an entry.';
comment on column PR_SERV_STATUS_HIST_ACT.request_id
  is 'With this request the value of STATUS is ordered. FK pointing to the GMD_REQUEST_BASE table containing the requested date.';
comment on column PR_SERV_STATUS_HIST_ACT.rec_version
  is 'Counter for multi-user access.';
comment on column PR_SERV_STATUS_HIST_ACT.initiator_type
  is 'Specifies who initiated the status change: C = Customer Administration, D = Dunning (which will only revert its own status changes).';
comment on column PR_SERV_STATUS_HIST_ACT.insertiondate
  is 'Insertion date';
comment on column PR_SERV_STATUS_HIST_ACT.lastmoddate
  is 'Last modification date';
  /*
-- Create/Recreate primary, unique and foreign key constraints 
alter table PR_SERV_STATUS_HIST_ACT
  add constraint PK_PR_SERV_STATUS_HIST primary key (CO_ID, SNCODE, PROFILE_ID, HISTNO)
  using index 
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
 );
alter table PR_SERV_STATUS_HIST_ACT
  add constraint FK_PR_SERV_STATUS_HIST_PR_SRV foreign key (CO_ID, SNCODE, PROFILE_ID)
  references PROFILE_SERVICE (CO_ID, SNCODE, PROFILE_ID);
alter table PR_SERV_STATUS_HIST_ACT
  add constraint FK_PR_SERV_STATUS_HIST_REQUEST foreign key (REQUEST_ID)
  references GMD_REQUEST_BASE (REQUEST_ID);
-- Create/Recreate check constraints 
alter table PR_SERV_STATUS_HIST_ACT
  add constraint CHECK_SERV_STATUS_HIST_REASON
  check (REASON BETWEEN 0 AND 5 OR REASON = 9 AND STATUS = 'D')
  disable;
alter table PR_SERV_STATUS_HIST_ACT
  add constraint CHECK_SERV_STATUS_HIST_STATUS
  check (STATUS in ('A', 'D', 'S', 'O', 'P'));
alter table PR_SERV_STATUS_HIST_ACT
  add constraint CK_PR_SERV_STATUS_HIST_INIT
  check (INITIATOR_TYPE IN ('C','D'));
*/-- Create/Recreate indexes 
create index IX_PR_SERV_STATUS_HIST_LMD1 on PR_SERV_STATUS_HIST_ACT (LASTMODDATE)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IX_PR_SERV_STATUS_STATUS1 on PR_SERV_STATUS_HIST_ACT (STATUS)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 8M
    next 1M
    minextents 1
    maxextents unlimited
  )
  compress;
create index IX_PR_SERV_VALIDFROMDATE1 on PR_SERV_STATUS_HIST_ACT (VALID_FROM_DATE)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 8M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete on PR_SERV_STATUS_HIST_ACT to BSCS_ROLE;
grant select on PR_SERV_STATUS_HIST_ACT to GSIDEPURA;
grant select on PR_SERV_STATUS_HIST_ACT to TEH;
