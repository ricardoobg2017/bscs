-- Create table
create table PROFILE_SERVICE_ACT
(
  profile_id     INTEGER not null,
  co_id          INTEGER not null,
  sncode         INTEGER not null,
  spcode_histno  INTEGER not null,
  status_histno  INTEGER not null,
  entry_date     DATE default (SYSDATE) not null,
  channel_num    INTEGER,
  ovw_acc_first  VARCHAR2(1),
  on_cbb         VARCHAR2(1),
  date_billed    DATE,
  sn_class       INTEGER,
  ovw_subscr     VARCHAR2(1),
  subscript      FLOAT,
  ovw_access     VARCHAR2(1),
  ovw_acc_prd    INTEGER not null,
  accessfee      FLOAT,
  channel_excl   VARCHAR2(1),
  dis_subscr     FLOAT,
  install_date   DATE,
  trial_end_date DATE,
  prm_value_id   INTEGER,
  currency       INTEGER not null,
  srv_type       VARCHAR2(1),
  srv_subtype    VARCHAR2(1),
  ovw_adv_charge VARCHAR2(1) not null,
  adv_charge     FLOAT default (0.0) not null,
  adv_charge_prd INTEGER not null,
  delete_flag    VARCHAR2(1),
  rec_version    INTEGER default (0) not null,
  insertiondate  DATE default SYSDATE not null,
  lastmoddate    DATE
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table PROFILE_SERVICE_ACT
  is 'This table contains actual values of all attributes within a profile (s. PROFILE table for further information).
For some service attributes the history is stored and for some not. The values of attributes having a history are saved in the history tables and the numbers of actual histories are saved in this table.';
-- Add comments to the columns 
comment on column PROFILE_SERVICE_ACT.profile_id
  is 'Profile identifier, FK pointing to the PROFILE table';
comment on column PROFILE_SERVICE_ACT.co_id
  is 'Contract identifier pointing with service identifier to the CONTRACT_SERVICE table';
comment on column PROFILE_SERVICE_ACT.sncode
  is 'Service identifier pointing with contract identifier to the CONTRACT_SERVICE table';
comment on column PROFILE_SERVICE_ACT.spcode_histno
  is 'Valid history number entry in the PR_SERV_SPCODE_HIST saving actual service package.';
comment on column PROFILE_SERVICE_ACT.status_histno
  is 'Valid history number entry in the PR_SERV_STATUS_HIST saving actual status of a profile service.';
comment on column PROFILE_SERVICE_ACT.entry_date
  is 'Date of the insertion of this record into this table (sysdate). The information is used for the tax calculation.';
comment on column PROFILE_SERVICE_ACT.channel_num
  is 'Associated channel (subaddress) for services of ERMES market. Subscribers of the European Radio Message System (ERMES) are identified by a 35 Bit Radio Identity Code (RIC) that is the port in the ERMES market. ERMES Directory Number is Address Code (AdC) that consists of 9 digits and can be assigned to individual services (for example: Infobox). Each RIC can obtain several directory numbers (so-called channels). Each ERMES network service gets a channel, which is stored in this attribute. Remark: two different services can get the same channel.';
comment on column PROFILE_SERVICE_ACT.ovw_acc_first
  is 'X = indicates that overwriting of access charge for this service within the profile has never been processed by BCH.';
comment on column PROFILE_SERVICE_ACT.on_cbb
  is 'C= charged on contract based bill, otherwise null. This flag means that the service has been already charged on the contract based bill. The attribute can be filled only once.';
comment on column PROFILE_SERVICE_ACT.date_billed
  is 'Date when the service has been billed the last time.
NULL if the service has not been billed yet. Date of last bill cycle run. A bill cycle runs per contract or per group of contracts (Several customers can be involved in the billing process.). We save this information per service, because for new services that are assigned to the contract after several bill runs it is not possible to control when the last bill cycle has been run.';
comment on column PROFILE_SERVICE_ACT.sn_class
  is 'Service code of the service, which is the representative for the equivalence class.
No history is needed, because this entry is never updated.';
comment on column PROFILE_SERVICE_ACT.ovw_subscr
  is 'Overwrite flag for subscription charges, A = SUBSCRIPT is an amount, R = SUBSCRIPT is a percentage relative to the tariff model price, see MPULKTMB table, attribute SUBSCRIPT. NULL means that tariff model price has to be taken. This value can be changed.';
comment on column PROFILE_SERVICE_ACT.subscript
  is 'Overwrite amount for subscription charges. For further information see description of the OVW_SUBSCRIPT attribute.';
comment on column PROFILE_SERVICE_ACT.ovw_access
  is 'Overwrite flag for access charges, A = ACCESS is an amount, R = ACCESS is a percentage relative to the tariff model price, see MPULKTMB table, attribute ACCESSFEE. NULL means that the ACCESSFEE value has to be taken from the MPULKTMB table.';
comment on column PROFILE_SERVICE_ACT.ovw_acc_prd
  is 'The number of times of the overwritten amount (value of the ACCESSFEE attribute) has to be applied by BCH.
Domain:
>1 : the overwritten charge has to be used and the value is reduced by 1
-1 : the overwritten charge has to be used and the value is not changed
1  : the overwritten charge has to be used and the value is set to -2
-2 : the charge from the tariff model is used and the value is set to 0
0  : the overwritten charge has not to be considered by BCH any more but
the value from the tariff model has to be used by BCH';
comment on column PROFILE_SERVICE_ACT.accessfee
  is 'Overwrite amount for access fee. For further information see descriptions of the OVW_ACCESS and OVW_ACC_PRD attributes.';
comment on column PROFILE_SERVICE_ACT.channel_excl
  is 'Indicates whether the channel used for this service has to be used exclusive or not. For ERMES market the only `Tone-Only-Paging` service is exclusive.
Allowed values: "X" = exclusive, otherwise null (none exclusive). The channel assigned to this service can be assigned to other services. CA copies the value of this attribute from some other place. The attribute cannot be manually updated. See also description of the CHANNEL_NUM attribute.';
comment on column PROFILE_SERVICE_ACT.dis_subscr
  is 'Amount discounted on the subscription fee (depending on the number of contracts).
Null means no discount is given.
It is a discount that is used per service, if the number of contracts is greater as a defined threshold value. The service discount grows with each new contract. It is a function of number of contracts. This function delivers with each new contract a new value for its service discount. CA sets the attribute and then calculates each time for every new contract new values for its service discounts.';
comment on column PROFILE_SERVICE_ACT.install_date
  is 'Date of service installation by subscriber at home, e.g. ISDN installation. Some markets use this information. The saving of history is not required, because an information will be delivered from an external system. It is typical provisioning situation.
The same service in different profiles can be considered as different services. The subscriber could use two profiles simultaneously with the same services.  Therefore some installations can be profile dependent.
Remark: GSM services do not need any installation.';
comment on column PROFILE_SERVICE_ACT.trial_end_date
  is 'The last day the profile service will be `on trail`. It deals with time limited contract services. For example a customer wants to test some provider services. In this case he makes a trial contract with provider. NULL means the saved profile service is not on trial. The saved information depends on a service. It can be a non-trial contract with trial services.';
comment on column PROFILE_SERVICE_ACT.prm_value_id
  is 'Service parameter value identifier. Points to a set of parameter values. FK: PARAMETER_VALUE_BASE. PRM_VALUE_ID. It is possible to assign to a bscs-service a list of parameters with their values. The PARAMETER_VALUE_BASE table contains those lists. The PARAMETER_VALUE table, containing linked parameters and their values, reference the PARAMETER_VALUE_BASE table. NULL-value means, that for the service within a profile no set of parameter values is required.';
comment on column PROFILE_SERVICE_ACT.currency
  is 'Currency. FK: FORCURR. In this currency all amounts of money are calculated and saved in this table. This column was introduced with the Euro feature 52818. It contains the rateplan currency to the assignment time. The attribute is needed, because the entry is not changed if the currency in the rateplan is changed. The same service can have different currency information in different profiles (p.4, ch.2.1)';
comment on column PROFILE_SERVICE_ACT.srv_type
  is 'Service type (flag for BCH to avoid large joins): V=service is VAS (network VAS) (used for selling goods), NULL=all other services. The column SRV_TYPE of CONTR_SERVICES is set automatically by CA as a copy of the MPULKNXV. SRV_TYPE attribute.';
comment on column PROFILE_SERVICE_ACT.srv_subtype
  is 'A subscriber can get free units for some services. But he can get them in different levels, e. g. in the contract level. In this case the subscriber can get free units for each contract (COFU: contract individual Free Units). The subscriber can get free units for some selected contracts (POFU: pooled Free Units at subscriber level). Free units can be assigned at the payment responsible level of a large account (POFUL). Via the POFUL service free units can be pooled over all contracts of a flat subscriber. Possible values are: NULL means the service is no free-units service, `C` for COFU (contract individual Free Units), `P` for POFU (pooled Free Units at subscriber level), `L` for POFUL (pooled Free Units at payment responsible level). It is the copy of the MPULKNXV. SRV_SUBTYPE attribute.';
comment on column PROFILE_SERVICE_ACT.ovw_adv_charge
  is 'This flag determines algorithm how access charge in advance has to be calculated. Possible values are: N means no changes relative to MPULKTMB.ACCESSFEE value have to be done, A = ADV_CHARGE is an amount, R = ADV_CHARGE is a factor relative to the tariff model price, see MPULKTMB table, attribute ACCESSFEE. The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_ACT.adv_charge
  is 'Access charge in advance (see description of the OVW_ADV_CHARGE attribute). The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_ACT.adv_charge_prd
  is 'How often the ADV_CHARGE amount, saved in this table, has to be applied by BCH, value -1 means unlimited. The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_ACT.delete_flag
  is 'Value `X` means that the entry is marked for deletion, otherwise null.';
comment on column PROFILE_SERVICE_ACT.rec_version
  is 'Counter for multi-user access.';
comment on column PROFILE_SERVICE_ACT.insertiondate
  is 'Insertion date';
comment on column PROFILE_SERVICE_ACT.lastmoddate
  is 'Last modification date';
/*-- Create/Recreate primary, unique and foreign key constraints 
alter table PROFILE_SERVICE_ACT
  add constraint PK_PROFILE_SERVICE primary key (CO_ID, SNCODE, PROFILE_ID)
  using index 
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table PROFILE_SERVICE_ACT
  add constraint FK_PROFILE_SERVICE_CONTR_SERV foreign key (CO_ID, SNCODE)
  references CONTRACT_SERVICE (CO_ID, SNCODE);
alter table PROFILE_SERVICE_ACT
  add constraint FK_PROFILE_SERVICE_FORCURR foreign key (CURRENCY)
  references FORCURR (FC_ID);
alter table PROFILE_SERVICE_ACT
  add constraint FK_PROFILE_SERVICE_PRM_VALUE foreign key (PRM_VALUE_ID)
  references PARAMETER_VALUE_BASE (PRM_VALUE_ID);
alter table PROFILE_SERVICE_ACT
  add constraint FK_PROFILE_SERVICE_PROFILE foreign key (PROFILE_ID)
  references PROFILE (PROFILE_ID);
-- Create/Recreate check constraints 
alter table PROFILE_SERVICE_ACT
  add constraint CHECK_ADV_CHARGE_ATTRIBUTES
  check (OVW_ADV_CHARGE='N' and         ADV_CHARGE=0.0 and         ADV_CHARGE_PRD=0 or         OVW_ADV_CHARGE in ('A','R'));
alter table PROFILE_SERVICE_ACT
  add constraint CHECK_CHANNEL_EXCL
  check (CHANNEL_NUM IS NULL AND         CHANNEL_EXCL IS NULL OR         CHANNEL_NUM IS NOT NULL);
alter table PROFILE_SERVICE_ACT
  add constraint CHECK_OVW_ACCESS
  check (OVW_ACCESS IS NULL AND         OVW_ACC_PRD =0 AND         ACCESSFEE IS NULL OR         OVW_ACCESS IN ('A', 'R') AND         ACCESSFEE > =0);
alter table PROFILE_SERVICE_ACT
  add constraint CHECK_OVW_SUBSCR
  check (OVW_SUBSCR IS NULL AND         SUBSCRIPT IS NULL OR         OVW_SUBSCR IN ('A', 'R') AND         SUBSCRIPT > =0);
*/-- Create/Recreate indexes 
create index IX_PROFILE_SERVICE_LMD1 on PROFILE_SERVICE_ACT (LASTMODDATE)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PROFILE_SERVICE_DELETE_FLAG2 on PROFILE_SERVICE_ACT (DELETE_FLAG)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PROFILE_SERVICE_PRM_VAL_ID3 on PROFILE_SERVICE_ACT (PRM_VALUE_ID)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 7M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete on PROFILE_SERVICE_ACT to BSCS_ROLE;
grant select on PROFILE_SERVICE_ACT to CARGA_ECUASISTENCIA_ROL;
grant select on PROFILE_SERVICE_ACT to EIS_DAT;
grant select on PROFILE_SERVICE_ACT to EIS_MIR;
grant select on PROFILE_SERVICE_ACT to EIS_PRI;
grant select, update on PROFILE_SERVICE_ACT to GSIDEPURA;
grant select on PROFILE_SERVICE_ACT to RELOJ;
grant select on PROFILE_SERVICE_ACT to REPORTE;
