-- Create table
create table CO_REP_SPOOL_EXCEL
(
  campo_secuencia    NUMBER not null,
  nombre_archivo     VARCHAR2(1000) not null,
  fecha              DATE,
  linea              LONG,
  estado             VARCHAR2(2),
  archivo_referencia VARCHAR2(1000),
  directorio         VARCHAR2(2000)
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CO_REP_SPOOL_EXCEL.campo_secuencia
  is 'Secuencial de la tabla';
comment on column CO_REP_SPOOL_EXCEL.nombre_archivo
  is 'Nombre archivo a generar el spool';
comment on column CO_REP_SPOOL_EXCEL.fecha
  is 'Fecha de Ingreso de registro';
comment on column CO_REP_SPOOL_EXCEL.linea
  is 'Query para generar el Spool';
comment on column CO_REP_SPOOL_EXCEL.estado
  is 'Tipo del QC';
comment on column CO_REP_SPOOL_EXCEL.archivo_referencia
  is 'Archivo de referencia';
comment on column CO_REP_SPOOL_EXCEL.directorio
  is 'Directorio del SO para guardar el Archivo';
-- Create/Recreate indexes 
create index IDX_ARC_REF_SP on CO_REP_SPOOL_EXCEL (ARCHIVO_REFERENCIA)
  tablespace IND12
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 11M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CAMP_SEC_SP on CO_REP_SPOOL_EXCEL (CAMPO_SECUENCIA)
  tablespace IND12
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 11M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_NOM_ARC_SP on CO_REP_SPOOL_EXCEL (NOMBRE_ARCHIVO)
  tablespace IND12
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 11M
    next 1M
    minextents 1
    maxextents unlimited
  );