-- Create table
create table CLIENTE_REVERSA_TMP_REV
(
  customer_id        NUMBER,
  procesado          VARCHAR2(1),
  factura            VARCHAR2(50),
  custcode           VARCHAR2(22),
  pr                 VARCHAR2(1),
  id_proceso         NUMBER,
  cierre             DATE,
  bandera_reversa    VARCHAR2(1),
  motivo_bandera_rev VARCHAR2(100)
)
tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table CLIENTE_REVERSA_TMP_REV
  is 'Tabla de cuentas reversadas TMP';
-- Add comments to the columns 
comment on column CLIENTE_REVERSA_TMP_REV.customer_id
  is 'El customer_id de las cuentas que van reversadas';
comment on column CLIENTE_REVERSA_TMP_REV.PROCESADO
  is 'La marca final despues que termina el proceso de reverso';
comment on column CLIENTE_REVERSA_TMP_REV.factura
  is 'La factura de la cuenta del cliente a reversar';
comment on column CLIENTE_REVERSA_TMP_REV.custcode
  is 'La cuenta del cliente';
 comment on column CLIENTE_REVERSA_TMP_REV.pr
  is 'La marca del proceso de reverso';
comment on column CLIENTE_REVERSA_TMP_REV.id_proceso
  is 'El identificador del proceso de reverso';
comment on column CLIENTE_REVERSA_TMP_REV.cierre
  is 'Fecha del cierre del ciclo de facturacion';
comment on column CLIENTE_REVERSA_TMP_REV.bandera_reversa
  is 'EL tipo de la bandera a reversar';  
 comment on column CLIENTE_REVERSA_TMP_REV.motivo_bandera_rev
 is 'Motivo de la Bandera';  