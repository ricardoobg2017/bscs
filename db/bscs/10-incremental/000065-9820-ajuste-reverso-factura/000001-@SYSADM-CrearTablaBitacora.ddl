CREATE TABLE BITACORA_REVERSO (
ID_BITACORA          NUMBER,
CUSTOMER_ID          NUMBER,
DESCRIPCION          VARCHAR2(500),
FECHA_EVENTO         DATE,
USUARIO              VARCHAR2(50))

tablespace DATA12
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the table 
comment on table BITACORA_REVERSO
  is 'Tabla de cuentas reversadas TMP';
-- Add comments to the columns 
comment on column BITACORA_REVERSO.ID_BITACORA
  is 'El id_bitacora de la tabla BITACORA_REVERSO';
comment on column BITACORA_REVERSO.CUSTOMER_ID
  is 'El customer_id de la cuenta para el proceso de reverso';
comment on column BITACORA_REVERSO.DESCRIPCION
  is 'La descripcion de los errores del proceso de reverso';
comment on column BITACORA_REVERSO.FECHA_EVENTO
  is 'La fecha cuando finaliza el proceso de reversa';
 comment on column BITACORA_REVERSO.USUARIO
  is 'El usuario que realiza la reversa';