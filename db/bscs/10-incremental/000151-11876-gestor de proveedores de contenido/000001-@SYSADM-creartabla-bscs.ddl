create table PC_CONTROL_TABLA_IDD
(
  nombre_tabla  VARCHAR2(50),
  tipo_tabla    VARCHAR2(3),
  estado        VARCHAR2(1),
  tipo_sub_prod VARCHAR2(10),
  observacion   VARCHAR2(1000),
  usuario       VARCHAR2(20),
  fecha         DATE,
  fuente        VARCHAR2(40)
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

  --Comentarios de las columnas
comment on column PC_CONTROL_TABLA_IDD.nombre_tabla
  is 'nombre de la tabla asociada a la recaudacion';
comment on column PC_CONTROL_TABLA_IDD.tipo_tabla
  is 'REC o REG';
comment on column PC_CONTROL_TABLA_IDD.estado
  is 'estado A o I';
comment on column PC_CONTROL_TABLA_IDD.tipo_sub_prod
  is 'tipo de subproducto a recaudar';
comment on column PC_CONTROL_TABLA_IDD.observacion
  is 'RECAUDADO cuando termina';
comment on column PC_CONTROL_TABLA_IDD.usuario
  is 'usuario que realiza la recaudacion';
comment on column PC_CONTROL_TABLA_IDD.fecha
  is 'fecha de ejecucion de la recaudacion';
comment on column PC_CONTROL_TABLA_IDD.fuente
  is 'BCSS o Colector';
