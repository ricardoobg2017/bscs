-- Create table
create table GSI_REPORTES_CREDITOS_NC
(
  CUSTCODE          VARCHAR2(24),
  REMARK            VARCHAR2(2000),
  MONTO             NUMBER,
  CUSTOMER_ID       INTEGER,
  CCLINE2           VARCHAR2(80),
  CCLINE3           VARCHAR2(110),
  CSSOCIALSECNO     VARCHAR2(20),
  REGION            VARCHAR2(20),
  GRUPO             VARCHAR2(50),
  FECHA_FACTURACION DATE,
  VAL_SIN_IMPUESTOS FLOAT,
  IVA               FLOAT,
  OHREFNUM          VARCHAR2(30),
  SEQNO             NUMBER,
  CO_ID             NUMBER,
  CIUDAD            VARCHAR2(50),
  SNCODE            INTEGER,
  ENTDATE           DATE
)
tablespace ORDER_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
 -- Create/Recreate indexes 
create index IDX_CUST_NC on GSI_REPORTES_CREDITOS_NC (CUSTCODE)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  ); 
