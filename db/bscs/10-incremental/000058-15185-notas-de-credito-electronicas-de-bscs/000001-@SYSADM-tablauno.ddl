-- Create table
create table BL_CARGA_OCC_TMP_NC
(
  CO_ID            NUMBER,
  AMOUNT           NUMBER,
  SNCODE           NUMBER,
  STATUS           VARCHAR2(1),
  ERROR            VARCHAR2(200),
  HILO             INTEGER,
  CUSTOMER_ID      INTEGER,
  CUSTCODE         VARCHAR2(24),
  ID_REQUERIMIENTO NUMBER
)
tablespace BLP_CAR_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index CARGA_OCC_NC_COID_IDX on BL_CARGA_OCC_TMP_NC (CO_ID)
  tablespace BLP_CAR_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CARGA_OCC_NC_CUSTID_IDX on BL_CARGA_OCC_TMP_NC (CUSTOMER_ID)
  tablespace BLP_CAR_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CARGA_OCC_NC_HILO_IDX on BL_CARGA_OCC_TMP_NC (HILO)
  tablespace BLP_CAR_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CARGA_OCC_NC_STATUS_IDX on BL_CARGA_OCC_TMP_NC (STATUS)
  tablespace BLP_CAR_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
); 