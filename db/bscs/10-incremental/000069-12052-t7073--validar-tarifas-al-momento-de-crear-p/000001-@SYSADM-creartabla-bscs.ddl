
-------Creacion de tablas para BSCS--------

-- Create table 
create table GSI_TABLA_TARIFAS_BSCS
(
  ID_REQUERIMIENTO NUMBER(10),
  PARAMETRO_ITEM   VARCHAR2(20),
  ID_PLAN          VARCHAR2(10),
  COD_BSCS         NUMBER(10),
  ID_DETALLE_PLAN  NUMBER(10),
  SNCODE           INTEGER,
  SERVICIO         VARCHAR2(100),
  DESCRIPCION      VARCHAR2(50),
  RI_CODE          INTEGER,
  DESTINO          VARCHAR2(100),
  ZNCODE           INTEGER,
  ETIQUETA         VARCHAR2(100),
  AIRE             FLOAT,
  INTERCONEXION    FLOAT,
  VALOR            FLOAT,
  USUARIO          VARCHAR2(20),
  FECHA_REGISTRO   DATE
)
tablespace PLANES_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
---Crea la particion con la fecha 
    PARTITION BY RANGE (FECHA_REGISTRO)
       (
         PARTITION GSI_TARIFAS_BSCS_201507 VALUES LESS THAN (TO_DATE('
            2015-08-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
         TABLESPACE PLANES_DAT
         PCTFREE 10
         PCTUSED 40
         INITRANS 1
         MAXTRANS 255
         STORAGE
       (
         INITIAL 64K
         MINEXTENTS 1
         MAXEXTENTS UNLIMITED
       ), 
       PARTITION GSI_TARIFAS_BSCS_201508 VALUES LESS THAN (TO_DATE('
            2015-09-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
         TABLESPACE PLANES_DAT
         PCTFREE 10
         PCTUSED 40
         INITRANS 1
         MAXTRANS 255
         STORAGE
       (
         INITIAL 64K
         MINEXTENTS 1
         MAXEXTENTS UNLIMITED
       ));    
  
-- Add comments to the columns 
comment on column GSI_TABLA_TARIFAS_BSCS.PARAMETRO_ITEM
  is 'Codigo Item';
comment on column GSI_TABLA_TARIFAS_BSCS.DESTINO
  is 'Tipo destino';
comment on column GSI_TABLA_TARIFAS_BSCS.ETIQUETA
  is 'Tipo de etiqueta';
comment on column GSI_TABLA_TARIFAS_BSCS.AIRE
  is 'Costo aire';
comment on column GSI_TABLA_TARIFAS_BSCS.INTERCONEXION
  is 'Costo interconexion';
comment on column GSI_TABLA_TARIFAS_BSCS.VALOR
  is 'Valor de BSCS';  
  


-- Create table 
CREATE TABLE GSI_AUD_TARIFAS_RTX
(
  ID_REQUERIMIENTO             NUMBER,       
  ITEM                         VARCHAR2(20),
  ID_PLAN                      VARCHAR2(20), 
  NOMBRE                       VARCHAR2(120),
  TMCODE                       INTEGER,      
  TMVERSION                    INTEGER,      
  RICODE                       INTEGER,      
  RI_VERSION                   INTEGER,      
  SNCODE                       INTEGER,      
  ZNCODE                       INTEGER,      
  ZONA                         VARCHAR2(120),
  PRECIO_SEGUNDO_AIRE          FLOAT,        
  PRECIO_SEGUNDO_INTERCONEXION FLOAT,        
  PRECIO_FINAL                 FLOAT,        
  BANDERA                      VARCHAR2(2),  
  FECHA_INGRESO                DATE,         
  FECHA_REVISION               DATE,         
  USUARIO                      VARCHAR2(100) 
)
tablespace PLANES_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
---Crea la particion con la fecha 
PARTITION BY RANGE (FECHA_INGRESO)
   (
     PARTITION GSI_AUD_RTX_201507 VALUES LESS THAN (TO_DATE('
        2015-08-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
     TABLESPACE PLANES_DAT
     PCTFREE 10
     PCTUSED 40
     INITRANS 1
     MAXTRANS 255
     STORAGE
   (
     INITIAL 64K
     MINEXTENTS 1
     MAXEXTENTS UNLIMITED
   ), 
   PARTITION GSI_AUD_RTX_201508 VALUES LESS THAN (TO_DATE('
        2015-09-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
     TABLESPACE PLANES_DAT
     PCTFREE 10
     PCTUSED 40
     INITRANS 1
     MAXTRANS 255
     STORAGE
   (
     INITIAL 64K
     MINEXTENTS 1
     MAXEXTENTS UNLIMITED
   ));    
      
-- Add comments to the columns 
comment on column GSI_AUD_TARIFAS_RTX.ITEM
  is 'Codigo Item';
comment on column GSI_AUD_TARIFAS_RTX.ZONA
  is 'Tipo de zona';
comment on column GSI_AUD_TARIFAS_RTX.PRECIO_SEGUNDO_AIRE
  is 'Valor aire';
comment on column GSI_AUD_TARIFAS_RTX.PRECIO_SEGUNDO_INTERCONEXION
  is 'Valor interconexion';
comment on column GSI_AUD_TARIFAS_RTX.PRECIO_FINAL
  is 'Valor total';
comment on column GSI_AUD_TARIFAS_RTX.BANDERA
  is '1 = Activo ,  0 = Inactivo';
comment on column GSI_AUD_TARIFAS_RTX.FECHA_INGRESO
  is 'Fecha de ingreso del registro';
comment on column GSI_AUD_TARIFAS_RTX.FECHA_REVISION
  is 'Fecha de revision del registro';
  
  
  
  
-- Create table 
create table GSI_TMP_TARIFAS_BSCS
(
  DESCRIPCION      VARCHAR2(100),
  SNCODE           INTEGER,
  SERVICIO         VARCHAR2(100),
  TMCODE           NUMBER,
  RI_CODE          INTEGER,
  ZNCODE           INTEGER,
  DESTINO          VARCHAR2(100),
  TIPO             VARCHAR2(50),
  COSTO            FLOAT,
  USUARIO          VARCHAR2(40),
  FECHA_REGISTRO   DATE
)
tablespace PLANES_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
---Crea la particion con la fecha 
PARTITION BY RANGE (FECHA_REGISTRO)
   (
     PARTITION GSI_TMP_BSCS_201507 VALUES LESS THAN (TO_DATE('
        2015-08-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
     TABLESPACE PLANES_DAT
     PCTFREE 10
     PCTUSED 40
     INITRANS 1
     MAXTRANS 255
     STORAGE
   (
     INITIAL 64K
     MINEXTENTS 1
     MAXEXTENTS UNLIMITED
   ), 
   PARTITION GSI_TMP_BSCS_201508 VALUES LESS THAN (TO_DATE('
        2015-09-01 00 :00 :00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
     TABLESPACE PLANES_DAT
     PCTFREE 10
     PCTUSED 40
     INITRANS 1
     MAXTRANS 255
     STORAGE
   (
     INITIAL 64K
     MINEXTENTS 1
     MAXEXTENTS UNLIMITED
   ));    
          
-- Add comments to the columns 
comment on column GSI_TMP_TARIFAS_BSCS.DESTINO
  is 'Tipo de destino';
comment on column GSI_TMP_TARIFAS_BSCS.TIPO
  is 'Existe: Aire, Interconexion';
comment on column GSI_TMP_TARIFAS_BSCS.COSTO
  is 'Costo dependiendo del tipo';
  
