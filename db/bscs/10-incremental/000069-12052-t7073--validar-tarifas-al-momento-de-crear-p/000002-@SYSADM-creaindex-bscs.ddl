
---Creacion del index para BSCS

create index IND_BSCS on GSI_TABLA_TARIFAS_BSCS (ID_PLAN)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
    
create index IND_USR_BSCS on GSI_TABLA_TARIFAS_BSCS (USUARIO)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
  
-------TABLA DE BSCS DE TODOS 
create index IND_TARIFAS on GSI_AUD_TARIFAS_RTX (ID_PLAN)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
create index IND_TAR_USR on GSI_AUD_TARIFAS_RTX (USUARIO)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );  
 

-------TABLA TMP DE BSCS
create index IND_TMP_SNC on GSI_TMP_TARIFAS_BSCS (SNCODE)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
  
  
create index IND_TMP_TMC on GSI_TMP_TARIFAS_BSCS (TMCODE)
tablespace PLANES_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );  
  
  
  
     
  
  
