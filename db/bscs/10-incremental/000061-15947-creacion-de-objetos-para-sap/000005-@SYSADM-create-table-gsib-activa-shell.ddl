--10189
--API BSCS SAP
--SIS Luis Flores
--PDS Fernando Ortega
--RT Ney Miranda
--Fecha: 28-07-2015

CREATE TABLE GSIB_ACTIVA_SHELL
(
  FECHA_INGRESO DATE,
  FECHA_CORTE   DATE,
  ESCENARIO     VARCHAR2(25),
  PRODUCTO      VARCHAR2(25),
  ESTADO        VARCHAR2(15),
  REPORTE_SAP   VARCHAR2(2)
)
TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
  
-- Comentarios
comment on column GSIB_ACTIVA_SHELL.fecha_ingreso
  is 'Fecha Ingreso Ejecucion';
comment on column GSIB_ACTIVA_SHELL.fecha_corte
  is 'Fecha de Corte Proceso';
comment on column GSIB_ACTIVA_SHELL.escenario
  is 'Escenario Ejecutado';
comment on column GSIB_ACTIVA_SHELL.producto
  is 'Producto (VOZ, DTH, NULL)';
comment on column GSIB_ACTIVA_SHELL.estado
  is 'Estado (EJECUTADO)';
comment on column GSIB_ACTIVA_SHELL.reporte_sap
  is 'Reportes Generados (SI, NO)';
