--10189 
--API BSCS SAP
--SIS Luis Flores
--PDS Fernando Ortega
--RT Ney Miranda
--Fecha: 28-07-2015

CREATE TABLE HISTORICO_FIN_CEBES
(
  ID_SEQUENCIA    NUMBER,
  ID_USUARIO      VARCHAR2(50),
  IPADDRESS       VARCHAR2(30),
  HOSTNAME        VARCHAR2(100),
  FECHA           DATE,
  ACCION          VARCHAR2(15),
  ESTADO          VARCHAR2(15),
  CTA_PEOPLE      VARCHAR2(40),
  DETALLE_PEOPLE  VARCHAR2(60),
  CTA_SAP         VARCHAR2(40),
  DETALLE_SAP     VARCHAR2(60),
  CEBE_UIO        VARCHAR2(40),
  CEBE_GYE        VARCHAR2(40),
  TIPO_IVA        VARCHAR2(40)
)
TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );

-- Comentarios
comment on column HISTORICO_FIN_CEBES.id_sequencia
  is 'Secuencial';
comment on column HISTORICO_FIN_CEBES.id_usuario
  is 'Usuario que realiza la accion';
comment on column HISTORICO_FIN_CEBES.ipaddress
  is 'Direccion IP de la maquina del usuario';
comment on column HISTORICO_FIN_CEBES.hostname
  is 'HostName de la maquina del usuario';
comment on column HISTORICO_FIN_CEBES.fecha
  is 'Fecha de la accion';
comment on column HISTORICO_FIN_CEBES.accion
  is 'Tipo de Accion (AFTER=Antes; BEFORE=Despues)';
comment on column HISTORICO_FIN_CEBES.estado
  is 'Estado (A=Activo, I=Inactivo)';
comment on column HISTORICO_FIN_CEBES.cta_people
  is 'Cuenta People';
comment on column HISTORICO_FIN_CEBES.detalle_people
  is 'Detalle cuenta People';
comment on column HISTORICO_FIN_CEBES.cta_sap
  is 'Cuenta Sap';
comment on column HISTORICO_FIN_CEBES.detalle_sap
  is 'Detalle cuenta Sap';
comment on column HISTORICO_FIN_CEBES.cebe_uio
  is 'Cebe UIO';
comment on column HISTORICO_FIN_CEBES.cebe_gye
  is 'Cebe GYE';
comment on column HISTORICO_FIN_CEBES.tipo_iva
  is 'Tipo Iva';
    
-- A�adir Primary Key  
ALTER TABLE HISTORICO_FIN_CEBES ADD CONSTRAINT PK_HIST_FIN_CEBES PRIMARY KEY (ID_SEQUENCIA);  
  
