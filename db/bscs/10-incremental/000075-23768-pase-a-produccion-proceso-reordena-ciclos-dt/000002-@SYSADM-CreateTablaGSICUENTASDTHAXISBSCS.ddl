-- Create table
create table GSI_CUENTAS_DTH_AXIS_BSCS
(
  custcode          VARCHAR2(24) not null,
  customer_id       INTEGER not null,
  csactivated       DATE,
  lbc_date          DATE,
  prgcode           VARCHAR2(10) not null,
  billcycle         VARCHAR2(2) not null,
  ciclo_axis        VARCHAR2(2) not null,
  id_contrato       NUMBER,
  id_servicio       VARCHAR2(20),
  co_id             NUMBER,
  fecha_inicio      DATE,
  fecha_instalacion DATE,
  observacion_axis  VARCHAR2(2000),
  fecha_registro    DATE default (sysdate) not null,
  observacion_reg   VARCHAR2(100)
)
tablespace BILLING_DAT
  pctfree 10
  initrans 1
  maxtrans 255;
-- Add comments to the table 
comment on table GSI_CUENTAS_DTH_AXIS_BSCS
  is 'Se registra la informacion de las activaciones dth en axis y su respectivo ciclo en bscs';
-- Add comments to the columns 
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.custcode
  is 'Cuenta bscs del cliente';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.customer_id
  is 'Codigo de la cuenta interna';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.csactivated
  is 'Fecha de activacion en bscs';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.lbc_date
  is 'Codigo del ciclo administrativo de bscs';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.prgcode
  is 'Codigo de grupo';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.billcycle
  is 'Codigo del ciclo administrativo de bscs';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.ciclo_axis
  is 'Codigo del ciclo de axis';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.id_contrato
  is 'Codigo del contrato asociado a la cuenta en axis';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.id_servicio
  is 'Codigo del servicio creado en axis';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.co_id
  is 'Codigo del servicio asociado en bscs';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.fecha_inicio
  is 'Fecha de inicio del servicio a partir de la activacion';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.fecha_instalacion
  is 'Fecha de instalacion del servicio';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.observacion_axis
  is 'Observacion generada en axis para el servicio';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.fecha_registro
  is 'Fecha de registro';
comment on column GSI_CUENTAS_DTH_AXIS_BSCS.observacion_reg
  is 'Observacion del registro';
--
create or replace public synonym GSI_CUENTAS_DTH_AXIS_BSCS for sysadm.GSI_CUENTAS_DTH_AXIS_BSCS;
