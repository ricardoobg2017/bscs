-- Create table
create table GSI_ACT_CICLO_DTH_REORDENA
(
  custcode       VARCHAR2(24) not null,
  customer_id    INTEGER not null,
  csactivated    DATE,
  prgcode        VARCHAR2(10),
  billcycle      VARCHAR2(2),
  lbc_date       DATE,
  ciclo_axis     VARCHAR2(2),
  fecha_registro DATE default (sysdate) not null,
  observacion    VARCHAR2(100)
)
tablespace BILLING_DAT
  pctfree 10
  initrans 1
  maxtrans 255;
-- Add comments to the table 
comment on table GSI_ACT_CICLO_DTH_REORDENA
  is 'Se registran las cuentas DTH que son afectadas por el proceso Reordena Ciclos';
-- Add comments to the columns 
comment on column GSI_ACT_CICLO_DTH_REORDENA.custcode
  is 'Cuenta bscs del cliente';
comment on column GSI_ACT_CICLO_DTH_REORDENA.customer_id
  is 'Codigo de la cuenta interna';
comment on column GSI_ACT_CICLO_DTH_REORDENA.csactivated
  is 'Fecha de activacion';
comment on column GSI_ACT_CICLO_DTH_REORDENA.prgcode
  is 'Codigo de grupo';
comment on column GSI_ACT_CICLO_DTH_REORDENA.billcycle
  is 'Codigo del ciclo administrativo de bscs';
comment on column GSI_ACT_CICLO_DTH_REORDENA.lbc_date
  is 'Fecha de la ultima facturacion';
comment on column GSI_ACT_CICLO_DTH_REORDENA.ciclo_axis
  is 'Codigo del ciclo de axis';
comment on column GSI_ACT_CICLO_DTH_REORDENA.fecha_registro
  is 'Fecha de registro';
comment on column GSI_ACT_CICLO_DTH_REORDENA.observacion
  is 'Observacion del registro';
--
create or replace public synonym GSI_ACT_CICLO_DTH_REORDENA for sysadm.GSI_ACT_CICLO_DTH_REORDENA;
