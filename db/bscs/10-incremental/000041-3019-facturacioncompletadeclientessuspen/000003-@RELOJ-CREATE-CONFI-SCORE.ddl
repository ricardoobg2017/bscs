-- Create table
create table RELOJ.RC_CONFIGURACION_SCORE
(
  ID_SCORE              NUMBER not null,
  ID_TIPO_CLIENTE       NUMBER not null,
  ID_FORMA_PAGO         VARCHAR2(5) not null,
  TIPO_CUENTA           VARCHAR2(2),
  ID_FINANCIERA         VARCHAR2(6),
  CONDICION             VARCHAR2(5) not null,
  SCORE                 NUMBER not null,
  DIAS                  NUMBER not null,
  APL_VAL_SCORE         VARCHAR2(2) not null,
  APL_DIREC_NEW_ESQUEMA VARCHAR2(2) not null,
  ESTADO                VARCHAR2(2) default 'A'
)
tablespace DATA13
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint PK_CFG_SCORE primary key (ID_SCORE)
  using index 
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 120K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint FK1_CFG_SCORE foreign key (ID_TIPO_CLIENTE)
  references RELOJ.RC_TIPO_CLIENTE (ID_TIPO_CLIENTE);
-- Create/Recreate check constraints 
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint CHK1_CFG_SCORE
  check (CONDICION IN ('>','<','=','>=','<='));
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint CHK2_CFG_SCORE
  check (SCORE >=0 AND SCORE <=100);
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint CHK3_CFG_SCORE
  check (APL_VAL_SCORE IN ('S','N'));
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint CHK4_CFG_SCORE
  check ((APL_DIREC_NEW_ESQUEMA='S' AND APL_VAL_SCORE='N') OR (APL_DIREC_NEW_ESQUEMA='N'));
alter table RELOJ.RC_CONFIGURACION_SCORE
  add constraint CHK5_CFG_SCORE
  check (ESTADO IN ('A','I'));
-- Create/Recreate indexes 
create index RELOJ.IDX1_CFG_SCORE on RELOJ.RC_CONFIGURACION_SCORE (ID_TIPO_CLIENTE, ID_FORMA_PAGO, TIPO_CUENTA, ID_FINANCIERA, ESTADO)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 120K
    next 1M
    minextents 1
    maxextents unlimited
  );
