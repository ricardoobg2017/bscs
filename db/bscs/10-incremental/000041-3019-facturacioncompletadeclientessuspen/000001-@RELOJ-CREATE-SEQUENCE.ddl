-- Create sequence 
create sequence SCORE_CLIENTE_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
cache 20;
