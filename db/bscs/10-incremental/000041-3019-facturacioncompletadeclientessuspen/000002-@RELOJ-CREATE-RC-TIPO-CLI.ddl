-- Create table
create table RELOJ.RC_TIPO_CLIENTE
(
  ID_TIPO_CLIENTE NUMBER not null,
  NOMBRE          VARCHAR2(100) not null,
  SENTENCIA_SQL   VARCHAR2(1000) not null,
  ESTADO          VARCHAR2(2) default 'A'
)
tablespace DATA13
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table RELOJ.RC_TIPO_CLIENTE
  add constraint PK_TIPO_CLIENTE primary key (ID_TIPO_CLIENTE)
  using index 
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 120K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate check constraints 
alter table RELOJ.RC_TIPO_CLIENTE
  add constraint CHK1_TIPO_CLI
  check (ESTADO IN ('A','I'));
-- Create/Recreate indexes 
create index RELOJ.IDX1_TIPO_CLI on RELOJ.RC_TIPO_CLIENTE (ESTADO)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
