-- Create table
create table RELOJ.RC_SUSPENDIDOS_FIN_CICLO
(
  ID_SECUENCIA      NUMBER not null,
  CUENTA            VARCHAR2(20),
  CUSTOMER_ID       NUMBER,
  ID_SERVICIO       VARCHAR2(12),
  CED_IDENTIDAD     VARCHAR2(15),
  CICLO             VARCHAR2(5),
  BP_PLAN           VARCHAR2(8),
  DETALLE_PLAN      VARCHAR2(1000),
  DEUDA             NUMBER,
  DIAS_MORA         NUMBER,
  CALIFICACION      NUMBER,
  TIPO_CLIENTE      VARCHAR2(20),
  FORMA_PAGO        VARCHAR2(10),
  CATEGORIA         VARCHAR2(10),
  TIPO_APROBACION   VARCHAR2(5),
  TIPO_CUENTA       VARCHAR2(10),
  ID_FINANCIERA     VARCHAR2(20),
  COMPA�IA          VARCHAR2(3),
  PAGO_ACUMULADOS   NUMBER,
  STATUS_LINEA_AXIS VARCHAR2(4),
  FECHA_SUSPENSION  DATE,
  FECHA_INGRESO     DATE
)
partition by range (FECHA_INGRESO)
(
  partition RC_SUSP_FIN_CICLO_201501 values less than (TO_DATE(' 2014-04-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    tablespace DATA13
    pctfree 10
    initrans 1
    maxtrans 255
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table RELOJ.RC_SUSPENDIDOS_FIN_CICLO
  add constraint RC_SUSPENDIDOS_FIN_CICLO_PK primary key (ID_SECUENCIA)
  using index 
  tablespace IND13 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX1_CUENTA1 on RELOJ.RC_SUSPENDIDOS_FIN_CICLO (CUENTA)
  tablespace IND13 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX2_CUSTOMER_ID1 on RELOJ.RC_SUSPENDIDOS_FIN_CICLO (CUSTOMER_ID)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX3_ID_SERVICIO1 on RELOJ.RC_SUSPENDIDOS_FIN_CICLO (ID_SERVICIO)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  create index IDX4_FECHA_INGRESO on RELOJ.RC_SUSPENDIDOS_FIN_CICLO (FECHA_INGRESO)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  create index IDX5_FECHA_SUSP on RELOJ.RC_SUSPENDIDOS_FIN_CICLO (FECHA_SUSPENSION)
  tablespace IND13
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
