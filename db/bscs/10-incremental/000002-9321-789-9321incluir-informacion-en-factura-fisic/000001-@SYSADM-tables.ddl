create table BL_CUENTAS_CORTE
(
  CUENTA        VARCHAR2(20),
  CICLO         VARCHAR2(2),
  FECHA_CORTE   VARCHAR2(12),
  FECHA_INGRESO DATE,
  HILO          NUMBER
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 9360K
    next 1040K
    minextents 1
    maxextents unlimited
  );

  
  
  
-- Create table
create table BL_CUENTAS_FACTURAR
(
  FECHA_CORTE         VARCHAR2(20),
  CUENTA_BSCS         VARCHAR2(20),
  CAMPO1              DATE,
  CAMPO2              VARCHAR2(100),
  CAMPO3              NUMBER(20),
  CAMPO4              NUMBER(20),
  CAMPO5              NUMBER(20),
  FECHA_INGRESO       DATE,
  CICLO               VARCHAR2(2),
  HILO                NUMBER,
  FECHA_ACTUALIZACION DATE,
  ESTADO              VARCHAR2(1),
  TELEFONO            VARCHAR2(20),
  TIPO_PROCESO        VARCHAR2(50)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 9360K
    next    1040K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BL_CUENTAS_FACTURAR.CUENTA_BSCS
  is 'cuenta bscs del cliente claro';
comment on column BL_CUENTAS_FACTURAR.CAMPO1
  is 'campo fecha (fecha adendum,fecha contratacion feacrure)';
comment on column BL_CUENTAS_FACTURAR.CAMPO2
  is 'campo varchar (estadistica de cuentas pagadas,descripcion del feature contratado)';
comment on column BL_CUENTAS_FACTURAR.CAMPO3
  is 'campo numerico 1 (Total de cuotas pagadas)';
comment on column BL_CUENTAS_FACTURAR.CAMPO4
  is 'campo numerico 2 (Cuotas Pendientes)';
comment on column BL_CUENTAS_FACTURAR.CAMPO5
  is 'campo numerico 3 (Total de Cuotas Pendientes)';
comment on column BL_CUENTAS_FACTURAR.TIPO_PROCESO
  is 'registra el modulo de ejecucion (CTA_FEAT,CTA_ADEN)';  
  
create index IDX_CUENTAS_FACTURAR_TIPO on BL_CUENTAS_FACTURAR (TIPO_PROCESO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
  );  

