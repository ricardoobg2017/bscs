-- Create table
create table IN_EXCL_PROD_BSCS 
(
  sncode               INTEGER not null,
  des                  VARCHAR2(35) not null,
  shdes                VARCHAR2(5) not null
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on column IN_EXCL_PROD_BSCS.sncode
  is 'Internal Key ';
comment on column IN_EXCL_PROD_BSCS.des
  is 'Descripcion del sncode';
comment on column IN_EXCL_PROD_BSCS.shdes
  is 'Visual Description to be printed on Bill ';
