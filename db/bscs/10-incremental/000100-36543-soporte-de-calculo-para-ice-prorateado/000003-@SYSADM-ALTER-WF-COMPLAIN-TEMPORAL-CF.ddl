-- Add/modify columns    
alter table WF_COMPLAIN_TEMPORAL_CF add excl_ice number; 
-- Add comments to the columns 
comment on column WF_COMPLAIN_TEMPORAL_CF.excl_ice  
  is 'Campo para excluir valor ice '; 

 -- Add/modify columns 
alter table WF_COMPLAIN_TEMPORAL_CF add id_producto2 VARCHAR2(50);
-- Add comments to the columns 
comment on column WF_COMPLAIN_TEMPORAL_CF.id_producto2
  is 'Cod. de producto de BSCS de mpusntab';
  
  -- Add/modify columns 
alter table WF_COMPLAIN_TEMPORAL_CF add ID_SNCODE VARCHAR2(50);
-- Add comments to the columns 
comment on column WF_COMPLAIN_TEMPORAL_CF.ID_SNCODE
  is 'Codigo prod sncode';