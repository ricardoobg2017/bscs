-- Create table
create table GSI_TMP_FREE_UNITS
(
  tmcode      INTEGER,
  fu_pack_id  INTEGER,
  long_name   VARCHAR2(30),
  valid_from  DATE,
  tipo_unidad VARCHAR2(30),
  valor       FLOAT,
  usuario     VARCHAR2(20)
)
tablespace PLANES_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the columns 
comment on column GSI_TMP_FREE_UNITS.tmcode
  is 'Codigo de tmcode';
comment on column GSI_TMP_FREE_UNITS.fu_pack_id
  is 'Codigo de Free Units';
comment on column GSI_TMP_FREE_UNITS.long_name
  is 'Nombre de paquete';
comment on column GSI_TMP_FREE_UNITS.valid_from
  is 'La fecha';
comment on column GSI_TMP_FREE_UNITS.tipo_unidad
  is 'Tipo de unidad: DOLARES, MINUTOS';
comment on column GSI_TMP_FREE_UNITS.valor
  is 'El valor';    
comment on column GSI_TMP_FREE_UNITS.usuario
  is 'Nombre del usuario';    
  

  
