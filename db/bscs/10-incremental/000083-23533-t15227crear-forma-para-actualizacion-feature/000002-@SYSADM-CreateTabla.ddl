/**************************Planes*************************/
-- Create table 
create table GSI_ASOCIA_PLANES
(
  id_plan         VARCHAR2(24),
  id_detalle_plan NUMBER,
  tmcode          NUMBER,
  fecha_max       DATE,
  version_max     NUMBER,
  procesados      VARCHAR2(1),
  usuario         VARCHAR2(30),
  fecha_registro  DATE
);
-- Add comments to the columns 
comment on column GSI_ASOCIA_PLANES.id_plan
  is 'Campo ID_PLAN de tabla GE_DETALLES_PLANES de base AXIS';
comment on column GSI_ASOCIA_PLANES.id_detalle_plan
  is 'Campo ID_DETALLE_PLAN de tabla GE_DETALLES_PLANES de base AXIS';
comment on column GSI_ASOCIA_PLANES.tmcode
  is 'Campo cod_bscs de tabla bs_planes de base AXIS';
comment on column GSI_ASOCIA_PLANES.fecha_max
  is 'Campo vsdate de tabla MPULKTMB de base BSCS';
comment on column GSI_ASOCIA_PLANES.version_max
  is 'Campo vscode de tabla MPULKTMB de base BSCS';
comment on column GSI_ASOCIA_PLANES.procesados
  is '"T" cuando todos los features fueron procesados';
comment on column GSI_ASOCIA_PLANES.usuario
  is 'Usuario de S.O. que ejecuta el Proceso de adicion';
comment on column GSI_ASOCIA_PLANES.fecha_registro
  is 'FECHA DE REGISTRO DEL PLAN';
  
  
  
/*******************************Tabla de adicion de Log***********************************/
-- Create table
create table GSI_MENSAJE_ADICION_BSCS_MK
(
  sncode         VARCHAR2(50),
  tmcode         VARCHAR2(50),
  observacion    VARCHAR2(4000),
  usuario        VARCHAR2(50),
  fecha_registro DATE default SYSDATE,
  tipo_feature   VARCHAR2(10),
  estado         VARCHAR2(1)
);
-- Add comments to the columns 
comment on column GSI_MENSAJE_ADICION_BSCS_MK.sncode
  is 'campo sncode de tabla bs_servicios_paquete de base axis';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.tmcode
  is 'campo cod_bscs de tabla bs_planes de base axis';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.observacion
  is 'Mensaje de adicion del features con el plan';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.usuario
  is 'usuario de s.o. que ejecuta el proceso de adicion';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.fecha_registro
  is 'Fecha de ejecucion de la adicion';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.tipo_feature
  is '''OCC'' o ''1M''';
comment on column GSI_MENSAJE_ADICION_BSCS_MK.estado
  is '''P'' Exitoso ,''W'' con Advertencias ,''E'' con Errores';
  
  

/**********************************************Tabla cuadre de Axis************************************************/

-- Create table
create table GSI_CUADRE_BSCS
(
  sncode       VARCHAR2(100),
  tipo         VARCHAR2(60),
  nombre_tabla VARCHAR2(60),
  n_ok         NUMBER,
  n_warning    NUMBER,
  n_error      NUMBER,
  total        NUMBER,
  estado       VARCHAR2(60),
  usuario      VARCHAR2(50)
);
-- Add comments to the columns 
comment on column GSI_CUADRE_BSCS.sncode
  is 'c�digo de features en bscs';
comment on column GSI_CUADRE_BSCS.tipo
  is 'Tipo del feature en BSCS';
comment on column GSI_CUADRE_BSCS.nombre_tabla
  is 'nombre de tabla donde se adiciona el feature';
comment on column GSI_CUADRE_BSCS.n_ok
  is 'numero de features procesados con �xito';
comment on column GSI_CUADRE_BSCS.n_warning
  is 'numero de features procesados con advertencias';
comment on column GSI_CUADRE_BSCS.n_error
  is 'numero de features no procesados';
comment on column GSI_CUADRE_BSCS.total
  is 'suma de los featuers ok mas los features warning';
comment on column GSI_CUADRE_BSCS.estado
  is '''ok'' cuando en e proceso los features no tengan errores ,''error'' cuando tengan errores, ''no aplica''';
comment on column GSI_CUADRE_BSCS.usuario
  is 'usuario de s.o. que ejecuta el proceso';
