/**********************1M**********************************/
alter table SYSADM.GSI_SNCODES_AGREGAR add usuario VARCHAR2(50);
alter table SYSADM.GSI_SNCODES_AGREGAR add observacion      VARCHAR2(3000);
alter table SYSADM.GSI_SNCODES_AGREGAR add prorrateable     VARCHAR2(5);
alter table SYSADM.GSI_SNCODES_AGREGAR add costo_ing        VARCHAR2(3000);
alter table SYSADM.GSI_SNCODES_AGREGAR add prorrateable_ing VARCHAR2(5);
alter table SYSADM.GSI_SNCODES_AGREGAR add tmcode_plantilla INTEGER;
alter table SYSADM.GSI_SNCODES_AGREGAR add valor_omision    VARCHAR2(100);
alter table SYSADM.GSI_SNCODES_AGREGAR add  cuenta_contable     VARCHAR2(30);
alter table SYSADM.GSI_SNCODES_AGREGAR add  impuesto            VARCHAR2(10);
alter table SYSADM.GSI_SNCODES_AGREGAR add  cuenta_contable_ing VARCHAR2(30);
alter table SYSADM.GSI_SNCODES_AGREGAR add  impuesto_ing        VARCHAR2(10);
alter table SYSADM.GSI_SNCODES_AGREGAR add  fecha_registro      DATE;

-- Add comments to the columns 
comment on column GSI_SNCODES_AGREGAR.sncode
  is 'Campo Sncode de tabla bs_servicios_paquete de base Axis';
comment on column GSI_SNCODES_AGREGAR.accessfee
  is 'Campo ACCESSFEE de tabla mpulktm1 de base BSCS';
comment on column GSI_SNCODES_AGREGAR.usuario
  is 'Usuario de S.O. que ejecuta el proceso';
comment on column GSI_SNCODES_AGREGAR.observacion
  is 'Campo donde se guardaran las observaciones o errores de proceso de adicion.';
comment on column GSI_SNCODES_AGREGAR.prorrateable
  is '''Y'' O ''N''';
comment on column GSI_SNCODES_AGREGAR.costo_ing
  is 'COSTOS INGRESADO POR EL USUARIO';
comment on column GSI_SNCODES_AGREGAR.prorrateable_ing
  is 'PRORRATEABLE INGRESADO POR EL USUARIO';
comment on column GSI_SNCODES_AGREGAR.tmcode_plantilla
  is 'SNCODE CLON O PLANTILLA QUE SE ENCUENTRA CONFIGURAD EN MKPULTM1';
comment on column GSI_SNCODES_AGREGAR.valor_omision
  is 'Campo valor_omision de tabla CL_TIPOS_DETALLES_SERVICIOS de base Axis';

comment on column GSI_SNCODES_AGREGAR.cuenta_contable
  is 'Campo glacode de tabla GLACCOUNT_ALL de base BSCS';
comment on column GSI_SNCODES_AGREGAR.impuesto
  is 'Campo accserv_catcode de tabla mpulktm1 de base BSCS';
comment on column GSI_SNCODES_AGREGAR.cuenta_contable_ing
  is 'Cuenta contable ingresada por el usuario';
comment on column GSI_SNCODES_AGREGAR.impuesto_ing
  is 'Impuesto ingresado por el usuario';
comment on column GSI_SNCODES_AGREGAR.fecha_registro
  is 'FECHA DE REGISTRO DEL FEATURE';


/*********************OCC*********************/
  alter table SYSADM.GSI_OCCS_A_AGREGAR add usuario VARCHAR2(50);
  alter table SYSADM.GSI_OCCS_A_AGREGAR add observacion VARCHAR2(3000);
  alter table SYSADM.GSI_OCCS_A_AGREGAR add cuenta_contable_ing VARCHAR2(30);
  alter table SYSADM.GSI_OCCS_A_AGREGAR add impuesto_ing VARCHAR2(10);
  alter table SYSADM.GSI_OCCS_A_AGREGAR add valor_omision VARCHAR2(100);
  alter table SYSADM.GSI_OCCS_A_AGREGAR add fecha_registro DATE;

-- Add comments to the columns 
comment on column GSI_OCCS_A_AGREGAR.sncode
  is 'Campo Sncode de tabla bs_servicios_paquete de base Axis';
comment on column GSI_OCCS_A_AGREGAR.cuenta_contable
  is 'Campo glacode de tabla GLACCOUNT_ALL de base BSCS';
comment on column GSI_OCCS_A_AGREGAR.impuesto
  is 'Campo accserv_catcode de tabla mpulktm1 de base BSCS';
comment on column GSI_OCCS_A_AGREGAR.usuario
  is 'Usuario de S.O. que ejecuta el proceso';
comment on column GSI_OCCS_A_AGREGAR.observacion
  is 'Campo donde se guardaran las observaciones o errores de proceso de adicion.';
comment on column GSI_OCCS_A_AGREGAR.cuenta_contable_ing
  is 'Cuenta contable ingresada por el usuario';
comment on column GSI_OCCS_A_AGREGAR.impuesto_ing
  is 'Impuesto ingresado por el usuario';
comment on column GSI_OCCS_A_AGREGAR.valor_omision
  is 'Campo valor_omision de tabla CL_TIPOS_DETALLES_SERVICIOS de base Axis';
comment on column GSI_OCCS_A_AGREGAR.fecha_registro
  is 'fecha de registro feature';
