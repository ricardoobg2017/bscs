﻿create table BL_OCC_FACTURAR
(
  sncode           NUMBER,
  cliente          VARCHAR2(200),
  fecha_activacion VARCHAR2(200),
  contador         NUMBER,
  co_id            NUMBER,
  fecha_corte      VARCHAR2(500),
  estado           VARCHAR2(5),
  shdes            VARCHAR2(200),
  hilo             NUMBER,
  cuenta           VARCHAR2(200),
  ciclo            VARCHAR2(5),
  tipo_proceso     VARCHAR2(200),
  id_ciclo_admin   VARCHAR2(200)
);
-- Add comments to the columns 
comment on column BL_OCC_FACTURAR.sncode
  is 'Sncode en Axis';
comment on column BL_OCC_FACTURAR.cliente
  is 'Servicio que registra en las tablas  que  registra OCC';
comment on column BL_OCC_FACTURAR.fecha_activacion
  is 'Fecha en que realiza la ultima peticion';
comment on column BL_OCC_FACTURAR.contador
  is 'Numero de veces que consume el servicio';
comment on column BL_OCC_FACTURAR.fecha_corte
  is 'Fecha de Corte  segun tabla Fa_axis_bscs_ciclos en BSCS';
comment on column BL_OCC_FACTURAR.estado
  is 'I ( ingresado)';
comment on column BL_OCC_FACTURAR.shdes
  is 'Descripcion del feture en BSCS';
comment on column BL_OCC_FACTURAR.hilo
  is 'Hilos del 1... hasta 20';
comment on column BL_OCC_FACTURAR.cuenta
  is 'Cuenta que Registra en cliente Bs_fact_"MES"';
comment on column BL_OCC_FACTURAR.ciclo
  is 'Ciclo que proceso 01 hasta 05';
comment on column BL_OCC_FACTURAR.tipo_proceso
  is 'CTA_FEAT para extracion de Feature';
comment on column BL_OCC_FACTURAR.id_ciclo_admin
  is 'Billcycle que se extrae desde BSCS';
-- Create/Recreate indexes 
create index IDX_BL_OCC_FACT_CLI on BL_OCC_FACTURAR (CLIENTE, ESTADO);


