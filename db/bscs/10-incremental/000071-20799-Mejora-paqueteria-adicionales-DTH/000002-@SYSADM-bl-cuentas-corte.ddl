
drop table BL_CUENTAS_CORTE_DTH;
-- Create table
create table SYSADM.BL_CUENTAS_CORTE_DTH
(
  cuenta         VARCHAR2(20),
  ciclo          VARCHAR2(2),
  fecha_corte    date,
  fecha_ingreso  DATE default SYSDATE,
  hilo           NUMBER,
  id_ciclo_admin VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index SYSADM.IDX_CUENTA_DTH1 on SYSADM.BL_CUENTAS_CORTE_DTH (CUENTA)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index SYSADM.IDX_FECHA_CORTE_DTH1 on SYSADM.BL_CUENTAS_CORTE_DTH (FECHA_CORTE)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index SYSADM.IDX_HILO_CUENTA_DTH on SYSADM.BL_CUENTAS_CORTE_DTH (HILO,CUENTA)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
