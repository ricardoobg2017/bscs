-- Create table


DROP TABLE SYSADM.BL_CUENTAS_FACTURAR_DTH;

create table SYSADM.BL_CUENTAS_FACTURAR_DTH
(
  fecha_corte            DATE,
  cuenta_bscs            VARCHAR2(20),
  campo1                 DATE,
  campo2                 VARCHAR2(100),
  campo3                 VARCHAR2(100),
  fecha_ingreso          DATE,
  ciclo                  VARCHAR2(2),
  hilo                   NUMBER,
  fecha_actualizacion    DATE,
  estado                 VARCHAR2(1),
  telefono               VARCHAR2(20),
  tipo_proceso           VARCHAR2(50),
  id_ciclo_admin         VARCHAR2(3),
  feature_axis           VARCHAR2(20),
  feature_bscs           VARCHAR2(10),
  billcycle_ref          VARCHAR2(3),
  billcycle_final        VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.fecha_corte
  is 'fecha ciclo que se registre en BSCS ';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.cuenta_bscs
  is 'cuenta bscs del cliente claro';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.campo1
  is 'campo fecha (fecha contratacion feature)';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.campo2
  is 'campo varchar (fecha de contratacion,descripcion del feature contratado)';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.campo3
  is 'campo varchar (fecha de activacion,descripcion del feature contratado)';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.fecha_ingreso
  is 'fecha en que fue lanzado el proceso ';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.ciclo
  is 'ciclo que se este facturando';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.hilo
  is 'hilos que van Desde 1.....20';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.estado
  is 'estado  A activo';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.telefono
  is 'servicio que registre el Cliente';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.tipo_proceso
  is 'registra el modulo de ejecucion (CTA_PLAN,CTA_FEAT)';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.id_ciclo_admin
  is 'subciclos que existen en BSCS para el ciclo que procese';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.feature_axis
IS 'registra la informacion del feature en axis';
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.feature_bscs
IS 'codigo_de feature de bscs -mpusntab@bscs'; 
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.billcycle_ref
IS 'subciclo a cual el cliente se lo categorizo en el corte  document_reference@bscs'; 
comment on column SYSADM.BL_CUENTAS_FACTURAR_DTH.billcycle_final
IS 'subciclo que indica finalmente el cliente como cerro'; 


-- Create/Recreate indexes 
create index IDX_CTA_FACT_DTH1 on SYSADM.BL_CUENTAS_FACTURAR_DTH (CUENTA_BSCS)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index IDX_CTA_FACT_DTH2 on SYSADM.BL_CUENTAS_FACTURAR_DTH (CUENTA_BSCS,FECHA_CORTE)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  ); 

create index IDX_CTA_FACT_DTH3 on SYSADM.BL_CUENTAS_FACTURAR_DTH (HILO, TIPO_PROCESO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index IDX_CTA_FACT_DTH4 on SYSADM.BL_CUENTAS_FACTURAR_DTH (TIPO_PROCESO, BILLCYCLE_FINAL)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
