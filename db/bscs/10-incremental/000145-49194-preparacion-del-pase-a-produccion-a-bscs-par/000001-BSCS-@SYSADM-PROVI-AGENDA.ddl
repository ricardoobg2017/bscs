-- CREATE TABLE
CREATE TABLE PROVI_AGENDA
(
  AGEN_MES             VARCHAR2(20)   NOT NULL,
  AGEN_ANIO            VARCHAR2(10)   NOT NULL,
  AGEN_FECHA_REGIS     DATE,
  AGEN_USUARIO_REGIS   VARCHAR2(50),
  AGEN_FECHA_PROC      DATE,
  AGEN_ESTADO_PROC     VARCHAR2(3),
  AGEN_OBSERVACION     VARCHAR2(4000),
  AGEN_FECHA_MODIF     DATE,
  AGEN_USUARIO_MODIF   VARCHAR2(50),
  AGEN_ESTADO          VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
  

-- Add comments to the columns 
comment on column PROVI_AGENDA.agen_mes
  is 'Mes de la provisión agendada';
comment on column PROVI_AGENDA.agen_anio
  is 'Annio de la provisión agendada';
comment on column PROVI_AGENDA.agen_fecha_regis
  is 'Fecha de registro del agendamiento de la Provisión';
comment on column PROVI_AGENDA.agen_usuario_regis
  is 'Usuario que registra el agendamiento de la Provisión';
comment on column PROVI_AGENDA.agen_fecha_proc
  is 'Fecha de ejecución del Proceso (generalmente fin de mes)';
comment on column PROVI_AGENDA.agen_estado_proc
  is 'Estado de la provisión agendada: ''I''=Ingresado, ''P''=Procesado, ''C''=Contabilizado o ''E''=Error';
comment on column PROVI_AGENDA.agen_observacion
  is 'Resultado del procesamiento de la Provisión';
comment on column PROVI_AGENDA.agen_fecha_modif
  is 'Fecha de modificación del agendamiento de la Provisión';
comment on column PROVI_AGENDA.agen_usuario_modif
  is 'Usuario que modifica el agendamiento de la Provisión';
comment on column PROVI_AGENDA.agen_estado
  is 'Estado del registro: ''A''=Activo o ''I''=Inactivo';


-- CREATE/RECREATE PRIMARY, UNIQUE AND FOREIGN KEY CONSTRAINTS 
ALTER TABLE PROVI_AGENDA
  ADD CONSTRAINT PK_PROVI_AGENDA PRIMARY KEY (AGEN_MES, AGEN_ANIO, AGEN_ESTADO)
  using index 
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
