-- Create table
create table PROVI_CUENTAS_DETALLES
(
  provi_cuenta           VARCHAR2(30) not null,
  provi_id_cliente       NUMBER       not null,
  provi_id_cia           NUMBER       not null,
  provi_id_ciclo         VARCHAR2(2)  not null,
  provi_dia_ciclo        VARCHAR2(2),
  provi_provincia        VARCHAR2(30),
  provi_canton           VARCHAR2(50),
  provi_ruc              VARCHAR2(20),
  provi_nombres          VARCHAR2(80),
  provi_apellidos        VARCHAR2(80),
  provi_telefono         VARCHAR2(63),
  provi_direccion        VARCHAR2(70),
  provi_fech_aper_cuenta DATE,
  provi_producto         VARCHAR2(40) not null,
  provi_categoria        VARCHAR2(40) not null,
  provi_id_forma_pago    VARCHAR2(5),
  provi_forma_pago       VARCHAR2(60),
  provi_num_factura      VARCHAR2(30),
  provi_fech_max_pago    DATE,
  provi_balance_12       NUMBER,
  provi_totalvencida     NUMBER,
  provi_totaladeuda      NUMBER,
  provi_mayorvencido     VARCHAR2(10),
  provi_mora_real        NUMBER,
  provi_pagos            NUMBER,
  provi_creditos         NUMBER,
  provi_cargos           NUMBER,
  provi_fecha_conta      DATE         not null,
  provi_fecha_regis      DATE,
  provi_origen           VARCHAR2(100),
  provi_observacion      VARCHAR2(1000)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column PROVI_CUENTAS_DETALLES.provi_cuenta
  is 'N�mero de Cuenta del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_id_cliente
  is 'Id del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_id_cia
  is 'ID de la Regi�n a la que pertenece la Cuenta; 1=GYE o 2=UIO';
comment on column PROVI_CUENTAS_DETALLES.provi_id_ciclo
  is 'ID del Ciclo al que pertenece la Cuenta';
comment on column PROVI_CUENTAS_DETALLES.provi_dia_ciclo
  is 'D�a de Inicio del Ciclo al que pertenece la Cuenta';
comment on column PROVI_CUENTAS_DETALLES.provi_provincia
  is 'Descripci�n de la Provincia';
comment on column PROVI_CUENTAS_DETALLES.provi_canton
  is 'Descripci�n del Cant�n';
comment on column PROVI_CUENTAS_DETALLES.provi_ruc
  is 'N�mero de RUC o C.I. del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_nombres
  is 'Descripci�n de los Nombre del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_apellidos
  is 'Descripci�n de los Apellidos del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_telefono
  is 'N�mero Telef�nico de Contacto del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_direccion
  is 'Direcci�n del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_fech_aper_cuenta
  is 'Fecha de Apertura de la Cuenta del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_producto
  is 'Descripci�n del Producto';
comment on column PROVI_CUENTAS_DETALLES.provi_categoria
  is 'Descripci�n del Tipo de Cliente; ''INSIGNIA'', ''PREMIUN'', ''VIP'', ''MASIVOS'' � ''ND''';
comment on column PROVI_CUENTAS_DETALLES.provi_id_forma_pago
  is 'Id de la Forma de Pago';
comment on column PROVI_CUENTAS_DETALLES.provi_forma_pago
  is 'Descripci�n de la Forma de Pago';
comment on column PROVI_CUENTAS_DETALLES.provi_num_factura
  is 'N�mero de Factura que adeuda el Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_fech_max_pago
  is 'Fecha M�xima de Pago del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_balance_12
  is 'Valor adeudado por el Cliente en el �ltimo Mes';
comment on column PROVI_CUENTAS_DETALLES.provi_totalvencida
  is 'Valor vencido que adeuda el Cliente ';
comment on column PROVI_CUENTAS_DETALLES.provi_totaladeuda
  is 'Total deuda del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_mayorvencido
  is 'Descripci�n del Vencimiento del Cliente; ''V'' = CLIENTE VIGENTE, ''-V''=VALORES A FAVOR DEL CLIENTE � ''N�MEROS''=TIEMPO DE VENCIMIENTO DEL PAGO';
comment on column PROVI_CUENTAS_DETALLES.provi_mora_real
  is 'N�mero de d�as de Mora del Cliente';
comment on column PROVI_CUENTAS_DETALLES.provi_pagos
  is 'Valor que el Cliente tiene en Pagos';
comment on column PROVI_CUENTAS_DETALLES.provi_creditos
  is 'Valor que el Cliente tiene en Cr�ditos';
comment on column PROVI_CUENTAS_DETALLES.provi_cargos
  is 'Valor que el Cliente tiene en Cargos';
comment on column PROVI_CUENTAS_DETALLES.provi_fecha_conta
  is 'Fecha Contable en que se procesa la Cuenta dentro de la Provisi�n (generalmente fin de mes)';
comment on column PROVI_CUENTAS_DETALLES.provi_fecha_regis
  is 'Fecha de Registro del Procesamiento de la Cuenta';
comment on column PROVI_CUENTAS_DETALLES.provi_origen
  is 'Tabla de facturaci�n, origen de los datos';
comment on column PROVI_CUENTAS_DETALLES.provi_observacion
  is 'Resultado del procesamiento de la Cuenta';
-- Create/Recreate indexes 
create index IDX_PROVI_CUENTAS_DETALLES_1 on PROVI_CUENTAS_DETALLES (PROVI_ID_CIA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_CUENTAS_DETALLES_2 on PROVI_CUENTAS_DETALLES (PROVI_PRODUCTO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_CUENTAS_DETALLES_3 on PROVI_CUENTAS_DETALLES (PROVI_FECHA_REGIS)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index IDX_PROVI_CUENTAS_DETALLES_4 on PROVI_CUENTAS_DETALLES (PROVI_CUENTA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

