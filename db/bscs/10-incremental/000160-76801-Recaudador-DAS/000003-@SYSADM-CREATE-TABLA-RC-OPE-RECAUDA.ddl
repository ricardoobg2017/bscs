-- Create table
create table  RC_OPE_RECAUDA
(
  cuenta              VARCHAR2(24),
  id_cliente          NUMBER,
  producto            VARCHAR2(30),
  canton              VARCHAR2(40),
  provincia           VARCHAR2(25),
  apellidos           VARCHAR2(40),
  nombres             VARCHAR2(40),
  ruc                 VARCHAR2(20),
  id_forma_pago       VARCHAR2(5),
  forma_pago          VARCHAR2(58),
  tipo_cliente        VARCHAR2(60),
  fech_aper_cuenta    DATE,
  telefono1           VARCHAR2(25),
  telefono2           VARCHAR2(25),
  telefono            VARCHAR2(63),
  direccion           VARCHAR2(70),
  direccion2          VARCHAR2(200),
  id_solicitud        VARCHAR2(10),
  lineas_act          VARCHAR2(10),
  lineas_inac         VARCHAR2(10),
  lineas_susp         VARCHAR2(10),
  cod_vendedor        VARCHAR2(10),
  vendedor            VARCHAR2(2000),
  num_factura         VARCHAR2(30),
  balance_1           NUMBER,
  balance_2           NUMBER,
  balance_3           NUMBER,
  balance_4           NUMBER,
  balance_5           NUMBER,
  balance_6           NUMBER,
  balance_7           NUMBER,
  balance_8           NUMBER,
  balance_9           NUMBER,
  balance_10          NUMBER,
  balance_11          NUMBER,
  balance_12          NUMBER,
  totalvencida        NUMBER,
  total_deuda         NUMBER,
  saldo_pendiente     NUMBER,
  mayorvencido        VARCHAR2(10),
  compania            NUMBER,
  fech_max_pago       DATE,
  id_plan             VARCHAR2(10),
  detalle_plan        VARCHAR2(4000),
  desc_aprobacion     VARCHAR2(4000),
  estado              VARCHAR2(2),
  fecha_corte         DATE,
  hilo                NUMBER,
  financiamiento      VARCHAR2(50),
  recaudador          VARCHAR2(100),
  fecha_asignacion    DATE,
  fecha_fin_ciclo     DATE,
  pagos               NUMBER,
  usuario_ingreso     VARCHAR2(100) default USER,
  fecha_ingreso       DATE default SYSDATE,
  fecha_actualizacion DATE default SYSDATE
)
tablespace DATA2
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table  RC_OPE_RECAUDA
  is 'Tabla que contiene la base de cuentas que se enviaran a los recaudadores DAS para la gestion de cobro.';
-- Add comments to the columns 
comment on column  RC_OPE_RECAUDA.cuenta
  is 'Cuenta del servicio.';
comment on column  RC_OPE_RECAUDA.id_cliente
  is 'Customer_Id del cliente.';
comment on column  RC_OPE_RECAUDA.producto
  is 'Subproducto del cliente.';
comment on column  RC_OPE_RECAUDA.canton
  is 'Canton que esta registrado del cliente.';
comment on column  RC_OPE_RECAUDA.provincia
  is 'Provincia con la que se encuentra registrado el cliente.';
comment on column  RC_OPE_RECAUDA.apellidos
  is 'Apellidos completos del cliente.';
comment on column  RC_OPE_RECAUDA.nombres
  is 'Nombres completos del cliente.';
comment on column  RC_OPE_RECAUDA.ruc
  is 'Identificacion del cliente.';
comment on column  RC_OPE_RECAUDA.id_forma_pago
  is 'El Id forma de pago que tiene registrado el cliente.';
comment on column  RC_OPE_RECAUDA.forma_pago
  is 'Descripcion de la forma de pago del cliente.';
comment on column  RC_OPE_RECAUDA.tipo_cliente
  is 'Indica el tipo de cliente de la cuenta';
comment on column  RC_OPE_RECAUDA.fech_aper_cuenta
  is 'Fecha de creacion de la cuenta del cliente.';
comment on column  RC_OPE_RECAUDA.telefono1
  is 'Medio de comunicacion del cliente.';
comment on column  RC_OPE_RECAUDA.telefono2
  is 'Medio de comunicacion del cliente.';
comment on column  RC_OPE_RECAUDA.telefono
  is 'Servicio del cliente.';
comment on column  RC_OPE_RECAUDA.direccion
  is 'Direccion del cliente.';
comment on column  RC_OPE_RECAUDA.direccion2
  is 'Segunda direccion del cliente.';
comment on column  RC_OPE_RECAUDA.id_solicitud
  is 'Id solicitud de la aprobacion del cliente.';
comment on column  RC_OPE_RECAUDA.lineas_act
  is 'Cantidad de lineas activas del cliente.';
comment on column  RC_OPE_RECAUDA.lineas_inac
  is 'Cantidad de lineas inactivas del cliente.';
comment on column  RC_OPE_RECAUDA.lineas_susp
  is 'Cantidad de lineas suspendidas del cliente.';
comment on column  RC_OPE_RECAUDA.cod_vendedor
  is 'Codigo del vendedor del servicio del cliente.';
comment on column  RC_OPE_RECAUDA.vendedor
  is 'Descripcion del vendedor del cliente.';
comment on column  RC_OPE_RECAUDA.num_factura
  is 'Numero de la ultima factura del cliente.';
comment on column  RC_OPE_RECAUDA.balance_1
  is 'Deuda de la factura de once meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_2
  is 'Deuda de la factura de diez meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_3
  is 'Deuda de la factura de nueve meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_4
  is 'Deuda de la factura de ocho meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_5
  is 'Deuda de la factura de siete meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_6
  is 'Deuda de la factura de seis meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_7
  is 'Deuda de la factura de cinco meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_8
  is 'Deuda de la factura de cuatro meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_9
  is 'Deuda de la factura de tres meses anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_10
  is 'Deuda de la factura de dos mes anteriores del cliente.';
comment on column  RC_OPE_RECAUDA.balance_11
  is 'Deuda de la factura del mes anterior del cliente.';
comment on column  RC_OPE_RECAUDA.balance_12
  is 'Valor de la ultima factura del cliente.';
comment on column  RC_OPE_RECAUDA.totalvencida
  is 'Total de la deuda vencida del cliente.';
comment on column  RC_OPE_RECAUDA.total_deuda
  is 'Total de la deuda vencia + la ultima factura generada del cliente.';
comment on column  RC_OPE_RECAUDA.saldo_pendiente
  is 'Saldo del total de la deuda menos los pagos realizados por el cliente.';
comment on column  RC_OPE_RECAUDA.mayorvencido
  is 'Dias de mora que posee el cliente.';
comment on column  RC_OPE_RECAUDA.compania
  is 'Id de la compania del cliente.';
comment on column  RC_OPE_RECAUDA.fech_max_pago
  is 'Fecha maxima de pago del cliente.';
comment on column  RC_OPE_RECAUDA.id_plan
  is 'Id del plan del cliente que posee en Axis.';
comment on column  RC_OPE_RECAUDA.detalle_plan
  is 'Descripcion del plan del cliente que posee en Axis.';
comment on column  RC_OPE_RECAUDA.desc_aprobacion
  is 'Descripcion del id solicitud del cliente.';
comment on column  RC_OPE_RECAUDA.estado
  is 'Estado del registro I ingresado, G generado, A asignado, T terminado.';
comment on column  RC_OPE_RECAUDA.fecha_corte
  is 'Fecha de corte en la que se genero la data.';
comment on column  RC_OPE_RECAUDA.hilo
  is 'Numero que corresponde al hilo que realizo la carga.';
comment on column  RC_OPE_RECAUDA.financiamiento
  is 'Indica si el cliente posee financiamiento "SI" o "NO".';
comment on column  RC_OPE_RECAUDA.recaudador
  is 'Nombre del recaudador al que fue asignado el cliente.';
comment on column  RC_OPE_RECAUDA.fecha_asignacion
  is 'Fecha en la que se asigno el cliente al recaudador.';
comment on column  RC_OPE_RECAUDA.fecha_fin_ciclo
  is 'Fecha limite para actualizar los pagos del cliente asignado al recaudador.';
comment on column  RC_OPE_RECAUDA.pagos
  is 'Pagos del cliente que realizo mientras estaba asignado al recaudador.';
comment on column  RC_OPE_RECAUDA.usuario_ingreso
  is 'Usuario que ingreso el registro.';
comment on column  RC_OPE_RECAUDA.fecha_ingreso
  is 'Fecha de ingreso del registro.';
comment on column  RC_OPE_RECAUDA.fecha_actualizacion
  is 'Fecha de actualizacion del registro.';
-- Create/Recreate indexes 
create index  IDX_RC_OPE_RECAUD_01 on  RC_OPE_RECAUDA (FECHA_CORTE)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_02 on  RC_OPE_RECAUDA (ID_FORMA_PAGO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_03 on  RC_OPE_RECAUDA (MAYORVENCIDO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_04 on  RC_OPE_RECAUDA (FORMA_PAGO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_05 on  RC_OPE_RECAUDA (CUENTA)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_06 on  RC_OPE_RECAUDA (HILO, ESTADO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_07 on  RC_OPE_RECAUDA (FINANCIAMIENTO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_08 on  RC_OPE_RECAUDA (FECHA_FIN_CICLO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index  IDX_RC_OPE_RECAUD_09 on  RC_OPE_RECAUDA (TIPO_CLIENTE)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 2048M
    next 1M
    minextents 1
    maxextents unlimited
  );
