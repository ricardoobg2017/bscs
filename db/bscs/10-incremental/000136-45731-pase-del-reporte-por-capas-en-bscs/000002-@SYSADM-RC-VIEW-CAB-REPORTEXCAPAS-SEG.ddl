CREATE OR REPLACE VIEW RC_VIEW_CAB_REPORTEXCAPAS_SEG AS
SELECT "FECHA_CORTE",
               "REGION",
               "PRODUCTO",
               "SEGMENTO",
               "TIPO_SEGMENTO",
               "CANT_FACT",
               "TOTAL_FACT",
               "TOTAL_CREDIT",
               "USUARIO_INGRESO",
               "FECHA_INGRESO",
               "CARTERA",
               TO_CHAR(FECHA_CORTE, 'MM/RRRR') FECHA_CORTE_MES
          FROM PORTA.RC_OPE_CAB_FACT_SEGMENTADA@AXIS;
