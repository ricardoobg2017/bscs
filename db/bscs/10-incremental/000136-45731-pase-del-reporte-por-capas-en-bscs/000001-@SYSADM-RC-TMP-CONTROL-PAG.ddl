
-- DROP TABLE 
DROP TABLE RC_TMP_CONTROL_PAG;
-- Create table
CREATE TABLE RC_TMP_CONTROL_PAG
(
  FECHA_FACT      DATE,
  CUSTOMER_ID     NUMBER,
  NUM_FACT        NUMBER,
  FECHA_PAGO      DATE,
  FORMA_PAG       VARCHAR2(50),
  TIP_CLIENT      VARCHAR2(50),
  CANAL_VENT      VARCHAR2(50),
  FINACIAMIENTO   VARCHAR2(100),
  REGION          VARCHAR2(50),
  PRODUCTO        VARCHAR2(50),
  VALOR_PAGO      NUMBER,
  USUARIO_INGRESO VARCHAR2(50),
  FECHA_INGRESO   DATE
)
TABLESPACE DATA9
  PCTFREE 10
  PCTUSED 40
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
-- Add comments to the table 
COMMENT ON TABLE RC_TMP_CONTROL_PAG
  IS 'Tabla temporal del cruse de los pagos del dia anterior con la cuentas facturadas para el reporte segmentado por capas.';
-- Add comments to the columns 
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.FECHA_FACT
  IS 'Fecha del corte de la facturacion.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.CUSTOMER_ID
  IS 'Custmer_Id del cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.NUM_FACT
  IS 'Codigo de la factura del cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.FECHA_PAGO
  IS 'Fecha del pago del cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.FORMA_PAG
  IS 'El tipo de forma de pago que posee el cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.TIP_CLIENT
  IS 'El tipo de calificacion que posee el cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.CANAL_VENT
  IS 'El canal de venta que le vendio el servicio al cliente.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.FINACIAMIENTO
  IS 'Si el cliente posee un equipo financiado o no.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.REGION
  IS 'Los tipos de regiones que son "TODOS", "COSTA" y "SIERRA".';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.PRODUCTO
  IS 'Los tipos de carteras que son "TODOS", "SMA" y "DTH".';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.VALOR_PAGO
  IS 'Valor del pago que el cliente realizo.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.USUARIO_INGRESO
  IS 'Usuario que ingreso el registro.';
COMMENT ON COLUMN RC_TMP_CONTROL_PAG.FECHA_INGRESO
  IS 'Fecha de ingreso del registro.';
-- Create/Recreate indexes 
CREATE INDEX IDX_RC_TMP_CTR_PAG_01 ON RC_TMP_CONTROL_PAG (FECHA_FACT)
  TABLESPACE TEH_IDX
  PCTFREE 10
  INITRANS 2
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
