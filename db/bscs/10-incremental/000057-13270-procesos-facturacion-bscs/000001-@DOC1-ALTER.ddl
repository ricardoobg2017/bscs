-- Add/modify columns 
alter table DOC1.DOC1_CUENTAS add tipo_producto VARCHAR2(20) default NULL;
-- Add comments to the columns
comment on column DOC1.DOC1_CUENTAS.tipo_producto
  is 'Identifica el tipo de producto asociado a la cuenta VOZ/DTH';
