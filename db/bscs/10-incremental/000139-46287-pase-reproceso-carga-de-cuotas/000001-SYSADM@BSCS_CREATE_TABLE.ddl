-- Create table
create table BIT_REPROCESO
(
  fecha_reproceso       DATE,
  descripcion_reproceso VARCHAR2(30),
  temporal_bscs         NUMBER,
  temporal_axis         NUMBER,
  temporal_ejecutado    NUMBER
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column BIT_REPROCESO.fecha_reproceso
  is 'Fecha que se ejecuto el reproceso';
comment on column BIT_REPROCESO.descripcion_reproceso
  is 'Descripción del tipo de diferencia por la que se realiza el reproceso';
comment on column BIT_REPROCESO.temporal_bscs
  is 'Cantidad de registros en tabla GC_CARGA_OCC_TMP';
comment on column BIT_REPROCESO.temporal_axis
  is 'Cantidad de registros en tabla CO_FINAN_CARGAS_TMP';
comment on column BIT_REPROCESO.temporal_ejecutado
  is 'Cantidad de registros en tabla GC_CARGA_EJECUCION';
-- Grant/Revoke object privileges 
grant select, insert, update, delete on BIT_REPROCESO to PUBLIC;
