

-- Create table 
create table RELOJ.TTC_SUSPENSIONES_PENDIENTES
(
  CUENTA            VARCHAR2(40),
  CO_ID             NUMBER,
  SERVICIO          VARCHAR2(40),
  CH_TIMEACCIONDATE DATE,
  CUSTOMER_ID       VARCHAR2(30),
  STATUS_A          VARCHAR2(10),
  REASON            VARCHAR2(10),
  ADMINCYCLE        VARCHAR2(10),
  STATUSTRX         VARCHAR2(10),
  VERIFICADO        VARCHAR2(20),
  TIPO_SERVICIO     VARCHAR2(50),
  OBSERVACION       VARCHAR2(400)
)
tablespace DATA10
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  -- Add comments to the table 
comment on table RELOJ.TTC_SUSPENSIONES_PENDIENTES
  is 'Tabla de trabajo para el registro de las suspensiones que se encuentran pendientes de procesar';
  -- Add comments to the columns 
comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.CUENTA
  is 'N�mero de cuenta del cliente';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.CO_ID
  is 'N�mero de contrato del cliente en Bscs';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.SERVICIO
  is 'N�mero del servicio del cliente';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.CH_TIMEACCIONDATE
  is 'Fecha en la cual debi� ejecutarse la suspensi�n';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.CUSTOMER_ID
  is 'Identificaci�n del cliente';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.STATUS_A
  is 'Estatus de la suspensi�n en Axis';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.ADMINCYCLE
  is 'ADMINCYCLE de la cuenta';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.STATUSTRX
  is 'Estatus de la transacci�n';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.VERIFICADO
  is 'Estatus de la verificaci�n';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.TIPO_SERVICIO
  is 'Movil o DTH';
  comment on column RELOJ.TTC_SUSPENSIONES_PENDIENTES.OBSERVACION
  is 'Descripci�n del status de verificaci�n';
  
-- Create/Recreate indexes 
create index IDX_TTC_CO_ID_PEND on RELOJ.TTC_SUSPENSIONES_PENDIENTES (CO_ID)
  tablespace IND10
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_TTC_CUENTA_PEND on RELOJ.TTC_SUSPENSIONES_PENDIENTES (CUENTA)
  tablespace IND10
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_TTC_CUST_PEND on RELOJ.TTC_SUSPENSIONES_PENDIENTES (CUSTOMER_ID)
  tablespace IND10
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_TTC_SERV_PEND on RELOJ.TTC_SUSPENSIONES_PENDIENTES (SERVICIO)
  tablespace IND10
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_TTC_STATUS_PEND on RELOJ.TTC_SUSPENSIONES_PENDIENTES (STATUSTRX)
  tablespace IND10
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
