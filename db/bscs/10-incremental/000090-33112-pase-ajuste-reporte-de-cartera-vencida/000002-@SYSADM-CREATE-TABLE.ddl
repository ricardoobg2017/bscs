-- Create table
create table CO_REP_BITACORA_TABLAS
(
  periodo      DATE,
  orden        NUMBER,
  nombre_tabla VARCHAR2(250),
  alias        VARCHAR2(250)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_CO_REP_TAB on CO_REP_BITACORA_TABLAS (PERIODO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CO_REP_BITACORA_TABLAS.periodo
  is 'periodo de facturacion que se ejecuto';
-- Add comments to the columns 
comment on column CO_REP_BITACORA_TABLAS.orden
  is 'orden de creacion de las tablas de servicio';
-- Add comments to the columns 
comment on column CO_REP_BITACORA_TABLAS.nombre_tabla
  is 'nombre de las tablas de servicios creadas';
-- Add comments to the columns 
comment on column CO_REP_BITACORA_TABLAS.alias
  is 'alias de las tablas creadas por servicio';
