-- Add/modify columns table
alter table CON_HIST_MPULKFXO add FECHA date default null;
alter table CON_HIST_MPULKFXO add ESTADO varchar2(5) default null;
-- Add comments to the columns 
comment on column CON_HIST_MPULKFXO.FECHA
  is 'Fecha de insercion del registro';
comment on column CON_HIST_MPULKFXO.ESTADO
  is 'Motivo de insercion del registro A:Adicion , M:Modificacion, E:Eliminacion';