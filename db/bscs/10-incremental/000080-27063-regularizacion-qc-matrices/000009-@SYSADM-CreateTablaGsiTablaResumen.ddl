-- EJECUTAR EN BSCS
-- CREATE TABLE
CREATE TABLE GSI_TABLA_RESUMEN
(
  ID_PLAN	        VARCHAR2(100),
  COSTO	            FLOAT,
  TIPO	            VARCHAR2(100),
  CODIGO	        VARCHAR2(100),
  OCULTO_ROL	    VARCHAR2(1),
  FEATURE_PCRF	    VARCHAR2(20),
  ID_DETALLE_PLAN	NUMBER(30),
  OBSERVACION	    VARCHAR2(1000),
  OBSERVACION_PCRF  VARCHAR2(1000)
 )
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the table 
comment on table GSI_TABLA_RESUMEN
  is 'Tabla donde se carga el valor de omision de los features a eliminar';
-- Add comments to the columns 
comment on column GSI_TABLA_RESUMEN.ID_PLAN
  is 'El id_plan de Axis de matrix que se carga en la tabla principal';
  
comment on column GSI_TABLA_RESUMEN.COSTO
  is 'El costo del plan de Axis de matrix que se carga en la tabla principal';

comment on column GSI_TABLA_RESUMEN.TIPO
  is 'El Tipo del Plan de los feature Adicional - Fijo - PCRF';

comment on column GSI_TABLA_RESUMEN.CODIGO
  is 'El codigo del plan de Axis de matrix que se carga en la tabla principal';

comment on column GSI_TABLA_RESUMEN.OCULTO_ROL
  is 'Maneja el esta S= oculto rol o NULO';

comment on column GSI_TABLA_RESUMEN.FEATURE_PCRF
  is 'Indica si un feature es PCRF S o N';  
  
comment on column GSI_TABLA_RESUMEN.ID_DETALLE_PLAN
  is 'El id_detalle_plan que se carga a la tabla';  
comment on column GSI_TABLA_RESUMEN.OBSERVACION
  is 'La observacion de los feauture que estan mal configurados en Axis';
comment on column GSI_TABLA_RESUMEN.OBSERVACION_PCRF
  is 'La observacion de los feauture PCRF en Axis';  
  
  
 
 create index INDXRES_ID_PLAN on GSI_TABLA_RESUMEN (ID_PLAN)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  

 create index INDXRES_ID_DET_PLAN on GSI_TABLA_RESUMEN (ID_DETALLE_PLAN)
   tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  

   
