--
alter table GSI_BITACORA_PLANESQC
add PRECIO_INPOOL VARCHAR2(300);
--
alter table GSI_BITACORA_PLANESQC
add CLARO_INPOOL VARCHAR2(300);
--
alter table GSI_BITACORA_PLANESQC
add DIFERENCIA_INPOOL VARCHAR2(300);
--
alter table GSI_BITACORA_PLANESQC
add CUPO_MENSUAL_COM VARCHAR2(300);
--
alter table GSI_BITACORA_PLANESQC
add TIPO_FEATURE VARCHAR2(300);
--
alter table GSI_BITACORA_PLANESQC
add OCULTO_ROL VARCHAR2(10);
--
alter table GSI_BITACORA_PLANESQC
add ID_TIPO_PLAN_SUPTEL_COM VARCHAR2(20);
--
alter table GSI_BITACORA_PLANESQC
add ID_TIPO_PLAN_SUPTEL_SIS VARCHAR2(20);
--
alter table GSI_BITACORA_PLANESQC
add ID_PROMO NUMBER;
--
alter table GSI_BITACORA_PLANESQC
add TIPO_PLAN_COM VARCHAR2(10);
--
alter table GSI_BITACORA_PLANESQC
add TIPO_PLAN_SIS VARCHAR2(10);
--


comment on column GSI_BITACORA_PLANESQC.PRECIO_INPOOL
  is 'El precio inpool para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.CLARO_INPOOL
  is 'El costo  de claro_inpool  para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.DIFERENCIA_INPOOL
  is 'La diferencia inpool para el  proceso de planes QC';  
 comment on column GSI_BITACORA_PLANESQC.CUPO_MENSUAL_COM
  is 'El cupo mensual de Comercial para el  proceso de planes QC';   
  comment on column GSI_BITACORA_PLANESQC.TIPO_FEATURE
  is 'El tipo de feature para el  proceso de planes QC';  
  comment on column GSI_BITACORA_PLANESQC.OCULTO_ROL
  is 'El Oculto Rol para el  proceso de planes QC'; 
    comment on column GSI_BITACORA_PLANESQC.ID_PROMO
  is 'El id_promo para el  proceso de planes QC'; 
  comment on column GSI_BITACORA_PLANESQC.ID_TIPO_PLAN_SUPTEL_COM
  is 'El id_tipo_plan_suptel de COM de Comercial para el  proceso de planes QC';   
comment on column GSI_BITACORA_PLANESQC.ID_TIPO_PLAN_SUPTEL_SIS  
  is 'El id_tipo_plan_suptel de SIS para el  proceso de planes QC';  
comment on column GSI_BITACORA_PLANESQC.TIPO_PLAN_COM  
  is 'El id_tipo_plan de COM para el  proceso de planes QC';     
comment on column GSI_BITACORA_PLANESQC.TIPO_PLAN_SIS
  is 'El id_tipo_plan de SIS para el  proceso de planes QC';     
