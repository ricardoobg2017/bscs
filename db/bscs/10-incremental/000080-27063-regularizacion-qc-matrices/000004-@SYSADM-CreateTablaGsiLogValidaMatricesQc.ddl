--EJECUTAR EN BSCS
-- CREATE TABLE
CREATE TABLE GSI_LOG_VALIDA_MATRICESQC
(
  COD_SEQ                   VARCHAR2(10),
  TRANSACCION               VARCHAR2(1000),
  FECHA_EVENTO              DATE,
  ESTADO                    CHAR(1), 
  OBSERVACION               VARCHAR2(1000),
  USUARIO                   VARCHAR2(30) 
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table GSI_LOG_VALIDA_MATRICESQC
  is 'Tabla de Logs para el proceso de configuración Matrices - QC';
-- Add comments to the columns 
comment on column GSI_LOG_VALIDA_MATRICESQC.COD_SEQ
  is 'El codigo de secuencia para el proceso de configuración Matrices - QC';
comment on column GSI_LOG_VALIDA_MATRICESQC.TRANSACCION
  is 'El nombre de los pasos del proceso de configuración Matrices - QC que se ejecutan en el proceso';
comment on column GSI_LOG_VALIDA_MATRICESQC.FECHA_EVENTO
  is 'La fecha evento del proceso de configuración Matrices - QC';
comment on column GSI_LOG_VALIDA_MATRICESQC.ESTADO
  is 'Estado del proceso de configuración de Matrices - QC E= Finalizado con error F=Finalizado';
comment on column GSI_LOG_VALIDA_MATRICESQC.OBSERVACION
  is 'Observacion de la ejecucion de los proceso configuración de Matrices - QC';
comment on column GSI_LOG_VALIDA_MATRICESQC.USUARIO
  is 'Usuario de Base Datos que ejecuta el proceso de configuración de Matrices - QC';
