-- EJECUTAR EN BSCS
-- CREATE TABLE
-- CREATE TABLE
CREATE TABLE GSI_PLANES_CARGA_INICIAL
(
  ID_PLAN          VARCHAR2(100),
  CODIGO_PAQ1      VARCHAR2(100),
  COSTO_PAQ1       FLOAT,
  TIPO1            VARCHAR2(100),
  OCULTO_ROL1      VARCHAR2(1),
  PCRF_1           VARCHAR2(1),
  CODIGO_PAQ2      VARCHAR2(100),
  COSTO_PAQ2       FLOAT,
  TIPO2            VARCHAR2(100),
  OCULTO_ROL2      VARCHAR2(1),
  PCRF_2           VARCHAR2(1),
  CODIGO_PAQ3      VARCHAR2(100),
  COSTO_PAQ3       FLOAT,
  TIPO3            VARCHAR2(100),
  OCULTO_ROL3      VARCHAR2(1),
  PCRF_3           VARCHAR2(1),
  CODIGO_PAQ4      VARCHAR2(100),
  COSTO_PAQ4       FLOAT,
  TIPO4            VARCHAR2(100),
  OCULTO_ROL4      VARCHAR2(1),
  PCRF_4           VARCHAR2(1),
  CODIGO_PAQ5      VARCHAR2(100),
  COSTO_PAQ5       FLOAT,
  TIPO5            VARCHAR2(100),
  OCULTO_ROL5      VARCHAR2(1),
  PCRF_5           VARCHAR2(1),
  CODIGO_PAQ6      VARCHAR2(100),
  COSTO_PAQ6       FLOAT,
  TIPO6            VARCHAR2(100),
  OCULTO_ROL6      VARCHAR2(1),
  PCRF_6           VARCHAR2(100),
  CODIGO_PAQ7      VARCHAR2(100),
  COSTO_PAQ7       FLOAT,
  TIPO7            VARCHAR2(100),
  OCULTO_ROL7      VARCHAR2(1),
  PCRF_7           VARCHAR2(100),
  CODIGO_PAQ8      VARCHAR2(100),
  COSTO_PAQ8       FLOAT,
  TIPO8            VARCHAR2(100),
  OCULTO_ROL8      VARCHAR2(1),
  PCRF_8           VARCHAR2(100),
  CODIGO_PAQ9      VARCHAR2(100),
  COSTO_PAQ9       FLOAT,
  TIPO9            VARCHAR2(100),
  OCULTO_ROL9      VARCHAR2(1),
  PCRF_9           VARCHAR2(100),
  CODIGO_PAQ10     VARCHAR2(100),
  COSTO_PAQ10      FLOAT,
  TIPO10           VARCHAR2(100),
  OCULTO_ROL10     VARCHAR2(1),
  PCRF_10          VARCHAR2(100),
  PRODUCTO         VARCHAR2(100),
  TIPO_PLAN        VARCHAR2(2000),
  CATEGORIA_PLAN   VARCHAR2(2000),
  CUOTA_MENSUAL    FLOAT,
  TB_VOZ           FLOAT,
  PRECIO_INPOOL    FLOAT,
  PRECIO_CLARO     FLOAT,
  PRECIO_MOVISTAR  FLOAT,
  PRECIO_ALEGRO    FLOAT,
  PRECIO_FIJAS     FLOAT,
  TOLL             VARCHAR2(1000),
  VIGENCIA_ADENDUM NUMBER
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );

 -- Add comments to the table 
comment on table GSI_PLANES_CARGA_INICIAL
  is 'Tabla donde se carga la matriz para evaluar los planes mal configurados';
-- Add comments to the columns 
comment on column GSI_PLANES_CARGA_INICIAL.ID_PLAN
  is 'El id_plan de la matrix de configuracion';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ1
  is 'El costo del paquete 1 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ1
  is 'El codigo del paquete 1 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.TIPO1
  is 'El Tipo del paquete 1 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL1
  is 'Maneja el esta S= oculto rol o NULO del paquete 1';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_1
  is 'Indica si un feature del paquete 1 es PCRF S o N';  
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ2
  is 'El costo del paquete 2 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ2
  is 'El codigo del paquete 2 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO2
  is 'El Tipo del paquete 2 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL2
  is 'Maneja el esta S= oculto rol o NULO del paquete 2';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_2 
  is 'Indica si un feature del paquete 2 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ3
  is 'El costo del paquete 3 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ3
  is 'El codigo del paquete 3 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO3
  is 'El Tipo del paquete 3 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL3
  is 'Maneja el esta S= oculto rol o NULO del paquete 3';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_3
  is 'Indica si un feature del paquete 3 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ4
  is 'El costo del paquete 4 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ4
  is 'El codigo del paquete 4 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO4
  is 'El Tipo del paquete 4 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL4
  is 'Maneja el esta S= oculto rol o NULO del paquete 4';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_4
  is 'Indica si un feature del paquete 4 es PCRF S o N';
 comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ5
  is 'El costo del paquete 5 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ5
  is 'El codigo del paquete 5 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO5
  is 'El Tipo del paquete 5 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL5
  is 'Maneja el esta S= oculto rol o NULO del paquete 5';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_5
  is 'Indica si un feature del paquete 5 es PCRF S o N';
 comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ6
  is 'El costo del paquete 6 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ6
  is 'El codigo del paquete 6 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO6
  is 'El tipo del paquete 6 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL6
  is 'Maneja el esta S= oculto rol o NULO del paquete 6';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_6
  is 'Indica si un feature del paquete 6 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ7
  is 'El costo del paquete 7 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ7
  is 'El codigo del paquete 7 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO7
  is 'El tipo del paquete 7 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL7
  is 'Maneja el esta S= oculto rol o NULO del paquete 7';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_7
  is 'Indica si un feature del paquete 7 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ8
  is 'El costo del paquete 8 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ8
  is 'El codigo del paquete 8 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO8
  is 'El tipo del paquete 8 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL8
  is 'Maneja el esta S= oculto rol o NULO del paquete 8';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_8
  is 'Indica si un feature del paquete 8 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ9
  is 'El costo del paquete 9 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ9
  is 'El codigo del paquete 9 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO9
  is 'El tipo del paquete 9 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL9
  is 'Maneja el esta S= oculto rol o NULO del paquete 9';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_9
  is 'Indica si un feature del paquete 9 es PCRF S o N';  
comment on column GSI_PLANES_CARGA_INICIAL.COSTO_PAQ10
  is 'El costo del paquete 10 del plan de Axis de matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CODIGO_PAQ10
  is 'El codigo del paquete 10 del plan de Axis de matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.TIPO10
  is 'El tipo del paquete 10 del plan de Axis de matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.OCULTO_ROL10
  is 'Maneja el esta S= oculto rol o NULO del paquete 10';
comment on column GSI_PLANES_CARGA_INICIAL.PCRF_10
  is 'Indica si un feature del paquete 10 es PCRF S o N';
comment on column GSI_PLANES_CARGA_INICIAL.PRODUCTO
  is 'El producto ya sea Autocontrol o Tarifario de Axis de la matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.CATEGORIA_PLAN
  is 'La categoria del plan  de Axis de la matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.CUOTA_MENSUAL
  is 'El cuota mensual del plan de Axis de la matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.TB_VOZ
  is 'La tarifa de voz basica de Axis de la matrix que se carga en la tabla principal';
 comment on column GSI_PLANES_CARGA_INICIAL.PRECIO_INPOOL
  is 'El precio impool de de la matrix que se carga en la tabla principal'; 
comment on column GSI_PLANES_CARGA_INICIAL.PRECIO_CLARO
  is 'El precio de claro  de la matrix que se carga en la tabla principal';
   comment on column GSI_PLANES_CARGA_INICIAL.PRECIO_MOVISTAR
  is 'El precio de movistar de la matrix que se carga en la tabla principal';  
comment on column GSI_PLANES_CARGA_INICIAL.PRECIO_ALEGRO
  is 'El precio de Alegro de la matrix que se carga en la tabla principal';   
comment on column GSI_PLANES_CARGA_INICIAL.PRECIO_FIJAS
  is 'EL precio fijo de la matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.TOLL
  is 'El tool de la matrix que se carga en la tabla principal';
comment on column GSI_PLANES_CARGA_INICIAL.VIGENCIA_ADENDUM
  is 'La vigencia adendum de Axis de la matrix que se carga en la tabla principal';
  
  
 create index INDX_ID_PLAN on GSI_PLANES_CARGA_INICIAL (ID_PLAN)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  

 
