-- CREATE TABLE

CREATE TABLE GSI_BITACORA_MATRICESQC
(
  COD_SEQ                  VARCHAR2(50),
  ID_PLAN                  VARCHAR2(100),
  NOMBRE_PLAN              VARCHAR2(100),
  VALOR_OMISION            VARCHAR2(100),
  NOMBRE_FEATURE           VARCHAR2(100),
  COSTO                    FLOAT,
  TIPO                     VARCHAR2(100),
  OBSERVACION              VARCHAR2(1000),
  OBSERVACION_PCRF         VARCHAR2(1000),
  ID_DETALLE_PLAN          NUMBER(30),
  CANTIDAD                 NUMBER,
  ID_TIPO_DETALLE_SERV     VARCHAR2(10),
  DESCRIPCION              VARCHAR2(100),
  TARIFA_BASICA_AXIS       FLOAT,
  TARIFA_BASICA_COM        FLOAT,
  ADENDUM_AXIS             FLOAT,
  ADENDUM_COM              FLOAT,
  CUOTA_MENSUAL_AXIS       FLOAT,
  CUOTA_MENSUAL_COM        FLOAT,
  TOLL_AXIS                VARCHAR2(10),
  TOLL_COM                 VARCHAR2(10),
  SUBPRODUCTO_AXIS         VARCHAR2(100),
  SUBPRODUCTO_COM          VARCHAR2(100),
  TIPO_PLAN                VARCHAR2(4),
  PRECIO_CLARO             FLOAT,
  CLARO                    FLOAT,
  DIFERENCIA_CLARO         FLOAT,
  PRECIO_MOVISTAR          FLOAT,
  MOVISTAR                 FLOAT,
  DIFERENCIA_MOVI          FLOAT,
  PRECIO_ALEGRO            FLOAT,
  ALEGRO                   FLOAT,
  DIFERENCIA_ALEGRO        FLOAT,
  PRECIO_FIJAS             FLOAT,
  FIJA                     FLOAT,
  DIFERENCIA_FIJA          FLOAT,
  TMCODE                   NUMBER,
  ACCESSFEE                FLOAT,
  FU_PACK_ID               NUMBER,
  LONG_NAME                VARCHAR2(100),
  FREE_UNITS_VOLUME        FLOAT,
  ID_PROMO                 NUMBER,
  TIPO_PROMO               VARCHAR2(50),
  TIPO_PAQUETE             VARCHAR2(50),
  FEATURE_SMS              VARCHAR2(500),
  NAME                     VARCHAR2(50),
  MENSAJE_TECNICO          VARCHAR2(1000),
  USUARIO                  VARCHAR2(30),
  ESTADO                   CHAR(1),
  FECHA_EVENTO             DATE,
  CATEGORIA_PLAN_COM       VARCHAR2(500),
  CATEGORIA_AMX_AXIS       VARCHAR2(500),
  FEATURE_MEGAS_NO_FIJO    NUMBER,
  CUPO_MEGAS_NO_FIJO       NUMBER,
  PROMO_LLAMADAS_ILIM_AXIS NUMBER,
  PROMO_LLAMADAS_ILIM_COM  NUMBER,
  PRECIO_INPOOL            FLOAT,
  CLARO_INPOOL             FLOAT,
  DIFERENCIA_INPOOL        FLOAT,
  TIPO_PLAN_COM            VARCHAR2(500),
  TIPO_PLAN_AXIS           VARCHAR2(500)
)
TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 1M
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );
  
-- Add comments to the table 
comment on table GSI_BITACORA_MATRICESQC
  is 'Tabla de bitacoras para el proceso de Validacion de Matrices y Qc';
-- Add comments to the columns 
comment on column GSI_BITACORA_MATRICESQC.COD_SEQ
  is 'El codigo de secuencia para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.ID_PLAN
  is 'El ID_PLAN de la base Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.NOMBRE_PLAN
  is 'El nombre del plan de la base Axis';  
comment on column GSI_BITACORA_MATRICESQC.VALOR_OMISION
  is 'El valor omision de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.NOMBRE_FEATURE
  is 'El nombre del feaure de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.COSTO
  is 'El costo del plan de Axis para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.TIPO
  is 'El Tipo del Plan de los feature Adicional - Fijo - PCRF';
comment on column GSI_BITACORA_MATRICESQC.OBSERVACION
  is 'La observacion de los feauture que estan mal configurados';
comment on column GSI_BITACORA_MATRICESQC.OBSERVACION_PCRF
  is 'La observacion de los feauture PCRF';  
comment on column GSI_BITACORA_MATRICESQC.ID_DETALLE_PLAN
  is 'El id_detalle_plan de la tabala gsi_feature_plan';  
comment on column GSI_BITACORA_MATRICESQC.CANTIDAD
  is 'La cantidad de los features repetidos en Axis';
comment on column GSI_BITACORA_MATRICESQC.ID_TIPO_DETALLE_SERV
  is 'El id_tipo_detalle_serv en Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.DESCRIPCION
  is 'La descripcion del plan de Axis para el proceso de Validacion de Matrices y Qc';   
comment on column GSI_BITACORA_MATRICESQC.TARIFA_BASICA_AXIS
  is 'La tarifa basica de Axis para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.TARIFA_BASICA_COM
  is 'La tarifa basica de Comercial para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.ADENDUM_AXIS
  is 'El adendum de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.ADENDUM_COM
  is 'E adendum de Comercial para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.CUOTA_MENSUAL_AXIS
  is 'La cuota mensual de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.CUOTA_MENSUAL_COM
  is 'La cuota mensual de Comercial para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.TOLL_AXIS
  is 'El tipo de Toll de Axis para el proceso de Validacion de Matrices y Qc';     
comment on column GSI_BITACORA_MATRICESQC.TOLL_COM
  is 'El tipo de Toll de Comercial para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.SUBPRODUCTO_AXIS
  is 'El subproducto de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.SUBPRODUCTO_COM
  is 'El subproducto de Comercial para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.TIPO_PLAN
  is 'El tipo de plan para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.PRECIO_CLARO
  is 'El precio de claro a claro de Axis para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.CLARO
  is 'El costo de claro con valor de TN3 - BSCS';
comment on column GSI_BITACORA_MATRICESQC.DIFERENCIA_CLARO
  is 'La diferencia de claro para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.PRECIO_MOVISTAR
  is 'El precio de movistar para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.MOVISTAR
  is 'El costo de movistar con valor de TN3 - BSCS';
comment on column GSI_BITACORA_MATRICESQC.DIFERENCIA_MOVI
  is 'La diferencia de movistar para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.PRECIO_ALEGRO
  is 'El precio de alegro para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.ALEGRO
  is 'El costo de alegro con valor de TN3 - BSCS';
comment on column GSI_BITACORA_MATRICESQC.DIFERENCIA_ALEGRO
  is 'La diferencia de alegro para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.PRECIO_FIJAS
  is 'El precio de fijas para el proceso de Validacion de Matrices y Qc';   
comment on column GSI_BITACORA_MATRICESQC.FIJA
  is 'El costo de alegro con valor de TN3 - BSCS de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.DIFERENCIA_FIJA
  is 'La diferencia fija para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.TMCODE
  is 'Internal Key of Tariff Model para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.ACCESSFEE
  is 'El valor de accessfee para el proceso de Validacion de Matrices y Qc';  
comment on column GSI_BITACORA_MATRICESQC.FU_PACK_ID
  is 'El Free Units Package Identifier para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.LONG_NAME
  is 'Free Units Package Description para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.FREE_UNITS_VOLUME
  is 'Number of Free Units para el proceso de Validacion de Matrices y Qc'; 
comment on column GSI_BITACORA_MATRICESQC.ID_PROMO
  is 'La promocion para el proceso de Validacion de Matrices y Qc'; 
 comment on column GSI_BITACORA_MATRICESQC.TIPO_PROMO 
  is 'El tipo de promocion el proceso de Validacion de Matrices y Qc'; 
comment on column GSI_BITACORA_MATRICESQC.TIPO_PAQUETE   
  is 'El tipo del paquete para el proceso de Validacion de Matrices y Qc'; 
comment on column GSI_BITACORA_MATRICESQC.FEATURE_SMS 
  is 'El features SMS para el proceso de Validacion de Matrices y Qc'; 
comment on column GSI_BITACORA_MATRICESQC.NAME         
  is 'El name para el proceso de Validacion de Matrices y Qc'; 
comment on column GSI_BITACORA_MATRICESQC.MENSAJE_TECNICO
 is 'EL mesanje tecnico de Error para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.USUARIO
  is 'El usario que ejecuta el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.ESTADO
  is 'El estado  A=Activo I=Inactivo para el proceso de planes QC';
comment on column GSI_BITACORA_MATRICESQC.FECHA_EVENTO
  is 'La fecha de inicio del proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.CATEGORIA_PLAN_COM
  is 'La categoria del Plan de Comercial';
comment on column GSI_BITACORA_MATRICESQC.CATEGORIA_AMX_AXIS
  is 'La categoria del Plan configurada en Axis'; 
 comment on column GSI_BITACORA_MATRICESQC.FEATURE_MEGAS_NO_FIJO
  is 'El feature megas no fijo de COM';
comment on column GSI_BITACORA_MATRICESQC.CUPO_MEGAS_NO_FIJO
  is 'El cupo de Megas no FIjo de COM'; 
 comment on column GSI_BITACORA_MATRICESQC.PROMO_LLAMADAS_ILIM_AXIS
 is 'El valor de llamadas ilimitadas configuradas en Axis'; 
  comment on column GSI_BITACORA_MATRICESQC.PROMO_LLAMADAS_ILIM_COM
  is 'El valor de llamadas ilimitadas configuradas en Comercial'; 
comment on column GSI_BITACORA_MATRICESQC.PRECIO_INPOOL
  is 'El precio inpool para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.CLARO_INPOOL
  is 'El costo  de claro_inpool con valor de TN3 - BSCS';
comment on column GSI_BITACORA_MATRICESQC.DIFERENCIA_INPOOL
  is 'La diferencia inpool para el proceso de Validacion de Matrices y Qc';
comment on column GSI_BITACORA_MATRICESQC.TIPO_PLAN_COM
  is 'El tipo de Plan configurado por Comercial';
comment on column GSI_BITACORA_MATRICESQC.TIPO_PLAN_AXIS
  is 'El tipo de Plan configurado por AXIS';  
  
  
  

  
  
