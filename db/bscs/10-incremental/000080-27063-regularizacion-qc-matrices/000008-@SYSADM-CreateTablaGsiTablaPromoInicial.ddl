CREATE TABLE GSI_TABLA_PROMO_INICIAL
(
ID_PLAN        			VARCHAR2(100),
ID_PROMO1      			NUMBER,
TIPO_PROMO1    			VARCHAR2(50),  
ID_PROMO2      			NUMBER,
TIPO_PROMO2    			VARCHAR2(50),
ID_PROMO3      			NUMBER,
TIPO_PROMO3    			VARCHAR2(50),
ID_PROMO4      			NUMBER,
TIPO_PROMO4    			VARCHAR2(50),
ID_PROMO5      			NUMBER,
TIPO_PROMO5    			VARCHAR2(50), 
ID_PROMO6      			NUMBER,
ID_PROMO7      			NUMBER,
ID_PROMO8      			NUMBER,
ID_PROMO9      			NUMBER,
ID_PROMO10      		NUMBER,
ID_PROMO11      		NUMBER,
ID_PROMO12      		NUMBER,
ID_PROMO13      		NUMBER,
ID_PROMO14      		NUMBER,
ID_PROMO15      		NUMBER,
ID_PROMO16      		NUMBER,
ID_PROMO17      		NUMBER,
ID_PROMO18      		NUMBER,
ID_PROMO19      		NUMBER,
ID_PROMO20      		NUMBER,
ID_PROMO21              NUMBER,
ID_PROMO22              NUMBER,
ID_PROMO23            	NUMBER,
ID_PROMO24            	NUMBER,
ID_PROMO25            	NUMBER,
ID_PROMO26            	NUMBER,
ID_PROMO27            	NUMBER,
ID_PROMO28            	NUMBER,
ID_PROMO29            	NUMBER,
ID_PROMO30            	NUMBER,
FEATURE_SMS     		VARCHAR2(500) ,
FEATURE_MEGAS_NO_FIJO 	VARCHAR2(500),
CUPO_MEGAS_NO_FIJO    	FLOAT,
PROMO_LLAMADAS_ILIM     NUMBER
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the table 
comment on table GSI_TABLA_PROMO_INICIAL
  is 'Tabla donde se carga las promociones a la matriz';
-- Add comments to the columns 
comment on column GSI_TABLA_PROMO_INICIAL.ID_PLAN
  is 'El id_plan de Axis para la matrix de las promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO1
  is 'La promocion 1 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.TIPO_PROMO1
  is 'El tipo de la promocion 1 del plan de Axis de matrix que se carga en la tabla principal de promociones';  
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO2
  is 'La promocion 2 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.TIPO_PROMO2
  is 'El tipo de la promocion 2 del plan de Axis de matrix que se carga en la tabla principal de promociones'; 
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO3
  is 'La promocion 3 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.TIPO_PROMO3
  is 'El tipo de la promocion 3 del plan de Axis de matrix que se carga en la tabla principal de promociones'; 
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO4
  is 'La promocion 4 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.TIPO_PROMO4
  is 'El tipo de la promocion 4 del plan de Axis de matrix que se carga en la tabla principal de promociones'; 
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO5
  is 'La promocion 5 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.TIPO_PROMO5
  is 'El tipo de la promocion 5 del plan de Axis de matrix que se carga en la tabla principal de promociones'; 
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO6
  is 'La promocion 6 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO7
  is 'La promocion 7 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO8
  is 'La promocion 8 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO9
  is 'La promocion 9 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO10
  is 'La promocion 10 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO11
  is 'La promocion 11 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO12
  is 'La promocion 12 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO13
  is 'La promocion 13 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO14
  is 'La promocion 14 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO15
  is 'La promocion 15 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO16
  is 'La promocion 16 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO17
  is 'La promocion 17 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO18
  is 'La promocion 18 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO19
  is 'La promocion 19 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO20
  is 'La promocion 20 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO21
  is 'La promocion 10 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO22
  is 'La promocion 11 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO23
  is 'La promocion 12 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO24
  is 'La promocion 13 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO25
  is 'La promocion 14 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO26
  is 'La promocion 15 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO27
  is 'La promocion 16 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO28
  is 'La promocion 17 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO29
  is 'La promocion 18 del plan de Axis de matrix que se carga en la tabla principal de promociones';
comment on column GSI_TABLA_PROMO_INICIAL.ID_PROMO30
  is 'La promocion 19 del plan de Axis de matrix que se carga en la tabla principal de promociones';
 comment on column GSI_TABLA_PROMO_INICIAL.FEATURE_SMS
  is 'El Feature para promociones de SMS';   
comment on column GSI_TABLA_PROMO_INICIAL.FEATURE_MEGAS_NO_FIJO
  is 'El valor de megas del Feature NO fijo';   
comment on column GSI_TABLA_PROMO_INICIAL.CUPO_MEGAS_NO_FIJO
  is 'El cupo de Megas del feature NO fijo';   
comment on column GSI_TABLA_PROMO_INICIAL.PROMO_LLAMADAS_ILIM
  is 'El valor de las Promo LLamadas Ilimitadas';     
