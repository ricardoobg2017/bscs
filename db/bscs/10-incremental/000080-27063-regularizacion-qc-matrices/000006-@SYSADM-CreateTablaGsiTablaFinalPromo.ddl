-- EJECUTAR EN BSCS
-- CREATE TABLE
CREATE TABLE GSI_TABLA_FINAL_PROMO
(
  ID_PLAN			VARCHAR2(100),  
  ID_PROMO			NUMBER, 
  TIPO_PROMO		VARCHAR2(50),  
  FEATURE_SMS   	VARCHAR2(500),
  ID_DETALLE_PLAN   NUMBER(30),
  FEATURE_MEGAS_NO_FIJO  VARCHAR2(500),
CUPO_MEGAS_NO_FIJO       FLOAT
 )
 tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
  
 -- Add comments to the table 
comment on table GSI_TABLA_FINAL_PROMO
  is 'Tabla final donde se visualiza las promociones de la matriz de carga';
-- Add comments to the columns 
comment on column GSI_TABLA_FINAL_PROMO.ID_PLAN
  is 'El id_plan de las promociones de la matriz de carga';
comment on column GSI_TABLA_FINAL_PROMO.ID_PROMO
  is 'La id_prom de las promociones de la matriz de carga';
comment on column GSI_TABLA_FINAL_PROMO.TIPO_PROMO
  is 'El tipo de las promociones de la matriz de carga';  
comment on column GSI_TABLA_FINAL_PROMO.FEATURE_SMS
  is 'El Feaure SMS de las promociones de la matriz de carga'; 
comment on column GSI_TABLA_FINAL_PROMO.FEATURE_MEGAS_NO_FIJO
  is 'El valor de megas del Feature NO fijo';   
comment on column GSI_TABLA_FINAL_PROMO.CUPO_MEGAS_NO_FIJO
  is 'El cupo de Megas del feature NO fijo';    
  
 create index INDEX_PLAN_PR on GSI_TABLA_FINAL_PROMO (ID_PLAN)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  

 create index INDEX_DETPLAN_PR on GSI_TABLA_FINAL_PROMO (ID_DETALLE_PLAN)
   tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
    