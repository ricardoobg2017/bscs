-- Create table
create table MF_CRITERIOS_X_ENVIOS_IVR
(
  idenvio    NUMBER not null,
  idcriterio NUMBER not null,
  secuencia  NUMBER not null,
  valor      VARCHAR2(50) not null,
  efecto     VARCHAR2(1) not null,
  estado     VARCHAR2(1) not null
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_CRITERIOS_X_ENVIOS_IVR.idenvio
  is 'ID Envio referenciado';
comment on column MF_CRITERIOS_X_ENVIOS_IVR.idcriterio
  is 'Id Criterio referenciado';
comment on column MF_CRITERIOS_X_ENVIOS_IVR.secuencia
  is 'Id Secuencia de envio';
comment on column MF_CRITERIOS_X_ENVIOS_IVR.valor
  is 'Valor de ese envio';
comment on column MF_CRITERIOS_X_ENVIOS_IVR.efecto
  is 'Codigo Efecto para ese envio';
comment on column MF_CRITERIOS_X_ENVIOS_IVR.estado
  is 'Estado del envio';
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_CRITERIOS_X_ENVIOS_IVR
  add constraint MF_CRIT_X_ENVIO_IVR_PK_CLAVE primary key (IDENVIO, IDCRITERIO, SECUENCIA)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
