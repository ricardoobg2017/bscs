-- Create table
create table MF_CRITERIOS_IVR
(
  idcriterio  NUMBER not null,
  descripcion VARCHAR2(60) not null,
  contenido   VARCHAR2(1000) not null,
  funcion     VARCHAR2(250)
)
tablespace DATA8
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_CRITERIOS_IVR.idcriterio
  is 'Identificacion del Criterio para el Envio';
comment on column MF_CRITERIOS_IVR.descripcion
  is 'Descripcion para el Criterio';
comment on column MF_CRITERIOS_IVR.contenido
  is 'Listado de Posibles valores que puede tomar el Criterio para efectuar filtros en el Envio';
comment on column MF_CRITERIOS_IVR.funcion
  is 'Nombre de la funcion a ejecutar por el criterio';
-- Create/Recreate primary, unique and foreign key constraints 
alter table MF_CRITERIOS_IVR
  add constraint MF_CRITERIOS_IVR_PK_IDCRITERIO primary key (IDCRITERIO)
  using index 
  tablespace IND8
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
