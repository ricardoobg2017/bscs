-- Create table
create table MF_BITACORA_CALIF_IVR
(
  id_envio       NUMBER not null,
  secuencia      NUMBER,
  identificacion VARCHAR2(20),
  calificacion   NUMBER,
  categoria_cli  VARCHAR2(10),
  destinatario   VARCHAR2(20),
  fecha_registro DATE
)
partition by range (fecha_registro)
(
 partition MF_BITA_CALI_IVR_201611 values less than (TO_DATE(' 2016-11-01 00:00:00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
 tablespace DATA8
);
-- Add comments to the columns 
comment on column MF_BITACORA_CALIF_IVR.id_envio
  is 'Numero de Envio';
comment on column MF_BITACORA_CALIF_IVR.secuencia
  is 'Numero de secuencia';
comment on column MF_BITACORA_CALIF_IVR.identificacion
  is 'Identificacion del cliente';
comment on column MF_BITACORA_CALIF_IVR.calificacion
  is 'Calificacion del cliente';
comment on column MF_BITACORA_CALIF_IVR.categoria_cli
  is 'Categoria segun la calificacion del cliente';
comment on column MF_BITACORA_CALIF_IVR.destinatario
  is 'Numero de servicio del cliente';
comment on column MF_BITACORA_CALIF_IVR.fecha_registro
  is 'Fecha de registro';
-- Create/Recreate indexes Local
CREATE INDEX IDENTIFIVRIDX ON MF_BITACORA_CALIF_IVR (IDENTIFICACION) LOCAL;
CREATE INDEX IDENVIOIVRIDX ON MF_BITACORA_CALIF_IVR (ID_ENVIO) LOCAL;
