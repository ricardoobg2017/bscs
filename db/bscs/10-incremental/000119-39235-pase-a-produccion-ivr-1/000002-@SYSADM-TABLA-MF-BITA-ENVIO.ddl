-- Create table
create table MF_BITACORA_ENVIO_IVR
(
  num_cuenta_bscs VARCHAR2(24),
  num_celular     VARCHAR2(25),
  nombre_cliente  VARCHAR2(100),
  cedula          VARCHAR2(20),
  monto_deuda     NUMBER(8,2),
  fecha_envio     DATE,
  fecha_registro  DATE,
  id_envio        NUMBER,
  secuencia       NUMBER,
  mensaje         VARCHAR2(160),
  hilo            NUMBER,
  novedad         VARCHAR2(1),
  estado_archivo  VARCHAR2(1),
  observacion     VARCHAR2(500),
  calificacion    NUMBER,
  categoria_cli   VARCHAR2(10)
)
partition by range (fecha_registro)
(
 partition MF_BITA_ENV_IVR_201611 values less than (TO_DATE(' 2016-11-01 00:00:00','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
 tablespace DATA8
);
-- Add comments to the columns 
comment on column MF_BITACORA_ENVIO_IVR.num_cuenta_bscs
  is 'Codigo de cuenta BSCS relacionado';
comment on column MF_BITACORA_ENVIO_IVR.num_celular
  is 'Numero de telefono celular';
comment on column MF_BITACORA_ENVIO_IVR.nombre_cliente
  is 'Datos del Cliente';
comment on column MF_BITACORA_ENVIO_IVR.cedula
  is 'Numero de Cedula o Ruc del cliente';
comment on column MF_BITACORA_ENVIO_IVR.monto_deuda
  is 'Valor Adeudado del Cliente';
comment on column MF_BITACORA_ENVIO_IVR.fecha_envio
  is 'Fecha de envio del mensaje';
comment on column MF_BITACORA_ENVIO_IVR.fecha_registro
  is 'Fecha de insercion del registro';
comment on column MF_BITACORA_ENVIO_IVR.id_envio
  is 'Identificador de envio relacionado';
comment on column MF_BITACORA_ENVIO_IVR.secuencia
  is 'Secuebcia de envio';
comment on column MF_BITACORA_ENVIO_IVR.mensaje
  is 'Mensaje de envio al cliente';
comment on column MF_BITACORA_ENVIO_IVR.hilo
  is 'Numero de hilo';
comment on column MF_BITACORA_ENVIO_IVR.novedad
  is 'Novedades del registro';
comment on column MF_BITACORA_ENVIO_IVR.estado_archivo
  is 'Estado en que se encuentra para envio de archivo';
comment on column MF_BITACORA_ENVIO_IVR.observacion
  is 'Observacion del registro';
comment on column MF_BITACORA_ENVIO_IVR.calificacion
  is 'Calificacion del cliente';
comment on column MF_BITACORA_ENVIO_IVR.categoria_cli
  is 'Categoria segun la calificacion del cliente';        
-- Create/Recreate indexes 
create index MF_BITCELULARIVR_IDXP on MF_BITACORA_ENVIO_IVR (NUM_CELULAR) LOCAL;
create index MF_BITCUENTAIVR_IDXP on MF_BITACORA_ENVIO_IVR (NUM_CUENTA_BSCS) LOCAL;
create index MF_BITENVIOIVR_IDXP on MF_BITACORA_ENVIO_IVR (ID_ENVIO, SECUENCIA) LOCAL;
create index MF_ENV_IDX_IVR on MF_BITACORA_ENVIO_IVR (ID_ENVIO, SECUENCIA, NUM_CELULAR) LOCAL;
