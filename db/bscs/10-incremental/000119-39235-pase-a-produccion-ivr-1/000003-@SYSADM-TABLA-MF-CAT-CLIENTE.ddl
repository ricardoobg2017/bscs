-- Create table
create table MF_CATEGORIAS_CLIENTE_IVR
(
  codigo      VARCHAR2(10),
  descripcion VARCHAR2(30),
  segmento    VARCHAR2(8)
)
tablespace MF_COB_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column MF_CATEGORIAS_CLIENTE_IVR.codigo
  is 'ID de Categorias';
comment on column MF_CATEGORIAS_CLIENTE_IVR.descripcion
  is 'Descripcion de Categorias';
comment on column MF_CATEGORIAS_CLIENTE_IVR.segmento
  is 'Segmento de Categorias';
