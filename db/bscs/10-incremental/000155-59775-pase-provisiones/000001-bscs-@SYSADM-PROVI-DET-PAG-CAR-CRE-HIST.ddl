-- Create table
create table PROVI_DET_PAG_CAR_CRE_HIST
(
  pcch_cuenta       VARCHAR2(30),
  pcch_id_cliente   NUMBER,
  pcch_fecha_regis  DATE,
  pcch_clase        NUMBER,
  pcch_descripcion  VARCHAR2(4000),
  pcch_valor        NUMBER,
  pcch_fees_codigo  NUMBER,
  pcch_fees_detalle VARCHAR2(3000),
  pcch_ruc          VARCHAR2(30),
  pcch_nombres      VARCHAR2(80),
  pcch_apellidos    VARCHAR2(80),
  pcch_id_cia       NUMBER,
  pcch_producto     VARCHAR2(40),
  pcch_id_ciclo     VARCHAR2(2),
  pcch_dia_ciclo    VARCHAR2(2),
  pcch_nro_hilo     NUMBER,
  pcch_fecha_insert DATE
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 16K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_cuenta
  is 'N�mero de Cuenta del Cliente';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_id_cliente
  is 'Id del Cliente';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_fecha_regis
  is 'Fecha en que se proces� este asiento o transacci�n dentro de la facturaci�n';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_clase
  is 'Clase del Registro: 1=PAGO, 2=CREDITO, 3=CARGO';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_descripcion
  is 'Descripci�n general tomada de las Tabla Principales de Facturaci�n';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_valor
  is 'Valor del Asiento o Transacci�n';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_fees_codigo
  is 'Codigo tomado de la Tabla FEES';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_fees_detalle
  is 'Descripci�n detallada tomada de la Tabla FEES';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_ruc
  is 'N�mero de RUC o C.I. del Cliente';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_nombres
  is 'Descripci�n de los Nombre del Cliente';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_apellidos
  is 'Descripci�n de los Apellidos del Cliente';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_id_cia
  is 'ID de la Regi�n a la que pertenece la Cuenta; 1=GYE o 2=UIO';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_producto
  is 'Descripci�n del Producto';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_id_ciclo
  is 'ID del Ciclo al que pertenece la Cuenta';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_dia_ciclo
  is 'D�a de Inicio del Ciclo al que pertenece la Cuenta';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_nro_hilo
  is 'N�mero de Hilo por el que es Procesada la Cuenta';
comment on column PROVI_DET_PAG_CAR_CRE_HIST.pcch_fecha_insert
  is 'Fecha de Inserci�n del Registro';
-- Create/Recreate indexes 
create index IDX_PROVI_PCC_HIST_1 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_CUENTA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_2 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_ID_CLIENTE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_3 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_FECHA_REGIS)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_4 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_CLASE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_5 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_FEES_CODIGO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_6 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_FEES_DETALLE)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_7 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_RUC)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_8 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_ID_CIA)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_9 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_PRODUCTO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_10 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_ID_CICLO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_11 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_DIA_CICLO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_PROVI_PCC_HIST_12 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_NRO_HILO)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_pcc_DET_PCC_TEMP_13 on PROVI_DET_PAG_CAR_CRE_HIST (PCCH_FECHA_INSERT)
  tablespace REP_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on PROVI_DET_PAG_CAR_CRE_HIST to DBJZAMBRANO;
