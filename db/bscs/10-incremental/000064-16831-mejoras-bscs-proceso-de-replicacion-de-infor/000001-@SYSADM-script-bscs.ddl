--- Create table BL_CONCILIA_ABONADOS_CICLOS
drop table BL_CONCILIA_ABONADOS_CICLOS;


create global temporary table BL_CONCILIA_ABONADOS_CICLOS
(
  customer_id INTEGER not null,
  custcode    VARCHAR2(24) not null,
  billcycle   VARCHAR2(2),
  hilo        VARCHAR2(1)
)
ON COMMIT PRESERVE ROWS; 
  
comment on column BL_CONCILIA_ABONADOS_CICLOS.customer_id
  is 'Registra el customer_id';
comment on column BL_CONCILIA_ABONADOS_CICLOS.custcode
  is 'Registra el custcode';
comment on column BL_CONCILIA_ABONADOS_CICLOS.billcycle
  is 'Registra el valor del billcycle para reproceso';
comment on column BL_CONCILIA_ABONADOS_CICLOS.hilo
  is 'Representa el valor del hilo';

-- Create table
create table BL_EJECUTA_CORTE_BULK
(
  fecha_corte         DATE,
  estado              VARCHAR2(1),
  fecha_ingreso       DATE,
  fecha_actualizacion DATE
)
tablespace REP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add comments to the columns 
comment on column BL_EJECUTA_CORTE_BULK.fecha_corte
  is 'FECHA DE CORTE A PROCESAR';
comment on column BL_EJECUTA_CORTE_BULK.estado
  is 'ESTADO DEL REGISTRO';
comment on column BL_EJECUTA_CORTE_BULK.fecha_ingreso
  is 'FECHA_INGRESO DEL REGISTRO';
comment on column BL_EJECUTA_CORTE_BULK.fecha_actualizacion
  is 'FECHA ACTUALIZACION  DEL REGISTRO';

-- Add/modify columns 
alter table BL_OCC_FACTURAR add observacion VARCHAR2(500);
-- Add comments to the columns 
comment on column BL_OCC_FACTURAR.observacion
  is 'Descripcion de error presentado';

drop table  BL_CUENTAS_FACTURAR; 

-- Create table
create table BL_CUENTAS_FACTURAR
(
  fecha_corte            DATE,
  cuenta_bscs            VARCHAR2(20),
  campo1                 DATE,
  campo2                 VARCHAR2(100),
  campo3                 NUMBER(20),
  campo4                 NUMBER(20),
  campo5                 NUMBER(20),
  fecha_ingreso          DATE,
  ciclo                  VARCHAR2(2),
  hilo                   NUMBER,
  fecha_actualizacion    DATE,
  estado                 VARCHAR2(1),
  telefono               VARCHAR2(20),
  tipo_proceso           VARCHAR2(50),
  id_ciclo_admin         VARCHAR2(3),
  feature_descripcion    VARCHAR2(100),
  servicio_administrador VARCHAR2(20),
  feature_axis           VARCHAR2(20),
  cod_serv               VARCHAR2(10),
  billcycle_ref          VARCHAR2(3),
  billcycle_final        VARCHAR2(3)
)
tablespace REP_DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 9360K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 

comment on column BL_CUENTAS_FACTURAR.fecha_corte
  is 'fecha ciclo que se registre en BSCS ';
comment on column BL_CUENTAS_FACTURAR.cuenta_bscs
  is 'cuenta bscs del cliente claro';
comment on column BL_CUENTAS_FACTURAR.campo1
  is 'campo fecha (fecha adendum,fecha contratacion feacrure)';
comment on column BL_CUENTAS_FACTURAR.campo2
  is 'campo varchar (estadistica de cuentas pagadas,descripcion del feature contratado)';
comment on column BL_CUENTAS_FACTURAR.campo3
  is 'campo numerico 1 (Total de cuotas pagadas)';
comment on column BL_CUENTAS_FACTURAR.campo4
  is 'campo numerico 2 (Cuotas Pendientes)';
comment on column BL_CUENTAS_FACTURAR.campo5
  is 'campo numerico 3 (Total de Cuotas Pendientes)';
comment on column BL_CUENTAS_FACTURAR.fecha_ingreso
  is 'fecha en que fue lanzado el proceso ';
comment on column BL_CUENTAS_FACTURAR.ciclo
  is 'ciclo que se este facturando';
comment on column BL_CUENTAS_FACTURAR.hilo
  is 'hilos que van Desde 1.....20';
comment on column BL_CUENTAS_FACTURAR.estado
  is 'estado  A activo';
comment on column BL_CUENTAS_FACTURAR.telefono
  is 'servicio que registre el Cliente';
comment on column BL_CUENTAS_FACTURAR.tipo_proceso
  is 'registra el modulo de ejecucion (CTA_FEAT, CTA_ADEN, CTA_ADMIN, CTA_PLAN)';
comment on column BL_CUENTAS_FACTURAR.id_ciclo_admin
  is 'subciclos que existen en BSCS para el ciclo que procese';
comment on column BL_CUENTAS_FACTURAR.feature_descripcion
  is 'Cantidad de fetures contratados por el Cliente en el corte EJ: 1,2....."Ilimitado"';
comment on column BL_CUENTAS_FACTURAR.servicio_administrador
  is 'registra el servicio adminitrador del contrato';
comment on column BL_CUENTAS_FACTURAR.feature_axis
  is 'registra la informacion del feature en axis';
comment on column BL_CUENTAS_FACTURAR.cod_serv
  is 'Codigo_de feature de bscs -mpusntab@bscs';
comment on column BL_CUENTAS_FACTURAR.billcycle_ref
  is 'subciclo a cual el cliente se lo categorizo en el corte  document_reference@bscs';
comment on column BL_CUENTAS_FACTURAR.billcycle_final
  is 'subciclo a cual el cliente se lo categorizo en el corte';
  -------------------
-- Create/Recreate indexes 
create index IDX_CUENTAS_FACT_TIPO_CICL on BL_CUENTAS_FACTURAR (TIPO_PROCESO, billcycle_final)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CUENTAS_FACTURAR_TIPO on BL_CUENTAS_FACTURAR (TIPO_PROCESO)
  tablespace REP_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_SUBCICLO_FACTURA on BL_CUENTAS_FACTURAR (ID_CICLO_ADMIN)
  tablespace IND
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 100M
    next 2M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
  