-- Create table
create table CUSTOMER_ALL_TMR
(
  customer_id            INTEGER not null,
  customer_id_high       INTEGER,
  custcode               VARCHAR2(24) not null,
  csst                   VARCHAR2(2),
  cstype                 VARCHAR2(1),
  csactivated            DATE,
  csdeactivated          DATE,
  customer_dealer        VARCHAR2(1),
  cstype_date            DATE,
  cslevel                VARCHAR2(2),
  cscusttype             VARCHAR2(1),
  cslvlname              VARCHAR2(10),
  tmcode                 INTEGER,
  prgcode                VARCHAR2(10),
  termcode               NUMBER(4),
  csclimit               FLOAT,
  cscurbalance           FLOAT,
  csdepdate              DATE,
  billcycle              VARCHAR2(2),
  cstestbillrun          VARCHAR2(1),
  bill_layout            INTEGER,
  paymntresp             VARCHAR2(1),
  target_reached         INTEGER,
  pcsmethpaymnt          VARCHAR2(1),
  passportno             VARCHAR2(30),
  birthdate              DATE,
  dunning_flag           VARCHAR2(1),
  comm_no                VARCHAR2(20),
  pos_comm_type          INTEGER,
  btx_password           VARCHAR2(8),
  btx_user               VARCHAR2(14),
  settles_p_month        VARCHAR2(12),
  cashretour             INTEGER,
  cstradecode            VARCHAR2(10),
  cspassword             VARCHAR2(20),
  cspromotion            VARCHAR2(1),
  cscompregno            VARCHAR2(20),
  cscomptaxno            VARCHAR2(20),
  csreason               INTEGER,
  cscollector            VARCHAR2(16),
  cscontresp             VARCHAR2(1),
  csdeposit              FLOAT,
  cscredit_date          DATE,
  cscredit_remark        VARCHAR2(2000),
  suspended              DATE,
  reactivated            DATE,
  prev_balance           FLOAT,
  lbc_date               DATE,
  employee               VARCHAR2(1),
  company_type           VARCHAR2(1),
  crlimit_exc            VARCHAR2(1),
  area_id                INTEGER,
  costcenter_id          INTEGER,
  csfedtaxid             VARCHAR2(5),
  credit_rating          INTEGER,
  cscredit_status        VARCHAR2(2),
  deact_create_date      DATE,
  deact_receip_date      DATE,
  edifact_addr           VARCHAR2(32),
  edifact_user_flag      VARCHAR2(1),
  edifact_flag           VARCHAR2(1),
  csdeposit_due_date     DATE,
  calculate_deposit      VARCHAR2(1),
  tmcode_date            DATE,
  cslanguage             INTEGER,
  csrentalbc             VARCHAR2(2),
  id_type                INTEGER,
  user_lastmod           VARCHAR2(16),
  csentdate              DATE default (sysdate) not null,
  csmoddate              DATE,
  csmod                  VARCHAR2(1),
  csnationality          INTEGER,
  csbillmedium           INTEGER,
  csitembillmedium       INTEGER,
  customer_id_ext        VARCHAR2(10),
  csreseller             VARCHAR2(1),
  csclimit_o_tr1         INTEGER,
  csclimit_o_tr2         INTEGER,
  csclimit_o_tr3         INTEGER,
  cscredit_score         VARCHAR2(40),
  cstraderef             VARCHAR2(2000),
  cssocialsecno          VARCHAR2(20),
  csdrivelicence         VARCHAR2(20),
  cssex                  VARCHAR2(1),
  csemployer             VARCHAR2(40),
  wpid                   INTEGER,
  csprepayment           VARCHAR2(1),
  csremark_1             VARCHAR2(40),
  csremark_2             VARCHAR2(40),
  ma_id                  INTEGER,
  cssumaddr              VARCHAR2(1),
  bill_information       VARCHAR2(1),
  dealer_id              INTEGER,
  dunning_mode           VARCHAR2(4),
  not_valid              VARCHAR2(1),
  cscrdcheck_agreed      VARCHAR2(1),
  marital_status         INTEGER,
  expect_pay_curr_id     INTEGER,
  convratetype_payment   INTEGER,
  refund_curr_id         INTEGER,
  convratetype_refund    INTEGER,
  srcode                 INTEGER,
  currency               INTEGER not null,
  primary_doc_currency   INTEGER,
  secondary_doc_currency INTEGER,
  prim_convratetype_doc  INTEGER,
  sec_convratetype_doc   INTEGER,
  rec_version            INTEGER default (0) not null,
  anonymous_customer     VARCHAR2(1),
  dummy_customer         VARCHAR2(1),
  dummy_owner_id         INTEGER,
  insertiondate          DATE default SYSDATE not null,
  lastmoddate            DATE
)
tablespace DATA3
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table CUSTOMER_ALL_TMR
  is 'customer/dealer names and other information';
-- Add comments to the columns 
comment on column CUSTOMER_ALL_TMR.customer_id
  is 'Customer Identifier.
DAB constraint CUSTOMER_ID !=0,
Logical foreign key to CUSTOMER_BASE.
Dealer do not reference to CUSTOMER_BASE.';
comment on column CUSTOMER_ALL_TMR.customer_id_high
  is 'pointer to customer of upper hierarchy level';
comment on column CUSTOMER_ALL_TMR.custcode
  is 'customer code in form Kd.AK.KSt.TN
Eg.: 1.20398400, 2.10, 2.45.100.100.100067, 3.123.120.150.190023, 7.2398764.130.170.100968';
comment on column CUSTOMER_ALL_TMR.csst
  is 'state';
comment on column CUSTOMER_ALL_TMR.cstype
  is 'type:
a=active
d=deactive
i=interested
s=suspended (g=gesperrt)
(b=beantragt)';
comment on column CUSTOMER_ALL_TMR.csactivated
  is 'date of activation';
comment on column CUSTOMER_ALL_TMR.csdeactivated
  is 'date of deactivation';
comment on column CUSTOMER_ALL_TMR.customer_dealer
  is 'C-customer, D-dealer';
comment on column CUSTOMER_ALL_TMR.cstype_date
  is 'date when CSTYPE was changed';
comment on column CUSTOMER_ALL_TMR.cslevel
  is 'Customer Level
The domain splits in the following parts:
Value for business partner:
0- External Carrier or Roaming Partner
Values for Customers and Service providers:
10-
20-
30-
40-
Values for Dealers:
All values defined for Sales Agents.
FK: CUSTOMER_LEVEL';
comment on column CUSTOMER_ALL_TMR.cscusttype
  is 'Customer Type: C = Consumer, B = Business';
comment on column CUSTOMER_ALL_TMR.cslvlname
  is 'name of Kd, AK, or KSt on this level';
comment on column CUSTOMER_ALL_TMR.tmcode
  is 'pointer to active tariff model';
comment on column CUSTOMER_ALL_TMR.prgcode
  is 'price group code';
comment on column CUSTOMER_ALL_TMR.termcode
  is 'payment terms';
comment on column CUSTOMER_ALL_TMR.csclimit
  is 'credit limit';
comment on column CUSTOMER_ALL_TMR.cscurbalance
  is 'Current balance of the customers account. Not including deposits and interests for deposits';
comment on column CUSTOMER_ALL_TMR.csdepdate
  is 'Date Deposit Paid';
comment on column CUSTOMER_ALL_TMR.billcycle
  is 'bill cycle in form 01 - 10';
comment on column CUSTOMER_ALL_TMR.cstestbillrun
  is 'entry for special control group, range A to Z';
comment on column CUSTOMER_ALL_TMR.bill_layout
  is 'Bill-layout for Customer (link to FORM_LAYOUT)';
comment on column CUSTOMER_ALL_TMR.paymntresp
  is 'payment responsibility if not space';
comment on column CUSTOMER_ALL_TMR.target_reached
  is 'Target reached value';
comment on column CUSTOMER_ALL_TMR.pcsmethpaymnt
  is 'method of payment; S = Scheck, B = BAR';
comment on column CUSTOMER_ALL_TMR.passportno
  is 'Passport number';
comment on column CUSTOMER_ALL_TMR.birthdate
  is 'birthdate';
comment on column CUSTOMER_ALL_TMR.dunning_flag
  is 'X = no dunning for this customer';
comment on column CUSTOMER_ALL_TMR.comm_no
  is 'number of the communication port';
comment on column CUSTOMER_ALL_TMR.pos_comm_type
  is 'Point of Sales (PoS) Communication Type
The CUSTOMER_ALL_TMR.POS_COMM_TYPE attribute specifies the Point of Sales (PoS) communication types.
The PoS connects are defined in the POSCONNECT table.
Further Information:
- POSCONNECT table comment';
comment on column CUSTOMER_ALL_TMR.btx_password
  is 'password for BTX';
comment on column CUSTOMER_ALL_TMR.btx_user
  is 'BTX-user';
comment on column CUSTOMER_ALL_TMR.settles_p_month
  is 'this field contains one character for each month of the year.  values for each character: 0=payment within 1 month, 1=payment one month too late, 2=payment two months too late, 3=payment three months too late.';
comment on column CUSTOMER_ALL_TMR.cashretour
  is 'Number of bounced checks and retoured direct debits';
comment on column CUSTOMER_ALL_TMR.cstradecode
  is 'fkey to table TRADE';
comment on column CUSTOMER_ALL_TMR.cspassword
  is 'password for customer info';
comment on column CUSTOMER_ALL_TMR.cspromotion
  is 'Y=responded to promotion';
comment on column CUSTOMER_ALL_TMR.cscompregno
  is 'registration number';
comment on column CUSTOMER_ALL_TMR.cscomptaxno
  is 'tax number';
comment on column CUSTOMER_ALL_TMR.csreason
  is 'Reason for status-change (link to REASONSTATUS_ALL)';
comment on column CUSTOMER_ALL_TMR.cscollector
  is 'cashcollector';
comment on column CUSTOMER_ALL_TMR.cscontresp
  is 'responsible for contract';
comment on column CUSTOMER_ALL_TMR.csdeposit
  is 'initial deposit amount';
comment on column CUSTOMER_ALL_TMR.cscredit_date
  is 'date of last credit scoring.';
comment on column CUSTOMER_ALL_TMR.cscredit_remark
  is 'remark for last credit-check';
comment on column CUSTOMER_ALL_TMR.suspended
  is 'date of suspension';
comment on column CUSTOMER_ALL_TMR.reactivated
  is 'date of reactivation';
comment on column CUSTOMER_ALL_TMR.prev_balance
  is 'Previous Balance
The CUSTOMER_ALL_TMR.PREV_BALANCE attribute specifies the previous balance, what means the financial balance of the customer after the last billing run (monetary value).';
comment on column CUSTOMER_ALL_TMR.lbc_date
  is 'last billcycle date';
comment on column CUSTOMER_ALL_TMR.employee
  is 'X=customer is an employee';
comment on column CUSTOMER_ALL_TMR.company_type
  is 'Type of Company:- F Foreign, R Related, I Internal';
comment on column CUSTOMER_ALL_TMR.crlimit_exc
  is 'X = Credit Limit Exceeded';
comment on column CUSTOMER_ALL_TMR.area_id
  is 'region which is attached to this Customer (fk to AREA)';
comment on column CUSTOMER_ALL_TMR.costcenter_id
  is 'costcenter which is attached to this customer (fk to COSTCENTER)';
comment on column CUSTOMER_ALL_TMR.csfedtaxid
  is 'Federal Tax ID';
comment on column CUSTOMER_ALL_TMR.credit_rating
  is 'Credit Rating 0 to 100';
comment on column CUSTOMER_ALL_TMR.cscredit_status
  is 'Credit Status';
comment on column CUSTOMER_ALL_TMR.deact_create_date
  is 'Date of Deactivation (Date on Customer Letter)';
comment on column CUSTOMER_ALL_TMR.deact_receip_date
  is 'Receipt of Written Deactivation Notification';
comment on column CUSTOMER_ALL_TMR.edifact_addr
  is 'EDIFACT Address';
comment on column CUSTOMER_ALL_TMR.edifact_user_flag
  is 'X = Edifact User;';
comment on column CUSTOMER_ALL_TMR.edifact_flag
  is 'X indicates if an EDIFACT user';
comment on column CUSTOMER_ALL_TMR.csdeposit_due_date
  is 'date when the deposit have to be paid';
comment on column CUSTOMER_ALL_TMR.calculate_deposit
  is 'flag for calculate the deposit by billing';
comment on column CUSTOMER_ALL_TMR.tmcode_date
  is 'date since when the tmcode is valid';
comment on column CUSTOMER_ALL_TMR.cslanguage
  is 'customers language. fkey to LANGUAGE.LNG_ID';
comment on column CUSTOMER_ALL_TMR.csrentalbc
  is 'the rental-bill bill-cycle';
comment on column CUSTOMER_ALL_TMR.id_type
  is 'fkey to table ID_TYPE, type of entered ID number in field PASSPORTNO';
comment on column CUSTOMER_ALL_TMR.user_lastmod
  is 'name of user who made the last modification of this record';
comment on column CUSTOMER_ALL_TMR.csentdate
  is 'start date';
comment on column CUSTOMER_ALL_TMR.csmoddate
  is 'date last file maint change';
comment on column CUSTOMER_ALL_TMR.csmod
  is 'record modified flag';
comment on column CUSTOMER_ALL_TMR.csnationality
  is 'nationality of the customer (fkey to table country)';
comment on column CUSTOMER_ALL_TMR.csbillmedium
  is 'Medium for the monthly-bill of the customer (fkey to table BILL_MEDIUM)';
comment on column CUSTOMER_ALL_TMR.csitembillmedium
  is 'Medium for the itemized-bill of the customer (fkey to table BILL_MEDIUM)';
comment on column CUSTOMER_ALL_TMR.customer_id_ext
  is 'convert the PPC-file dealer code to BSCS dealer_id';
comment on column CUSTOMER_ALL_TMR.csreseller
  is 'X = customer is reseller';
comment on column CUSTOMER_ALL_TMR.csclimit_o_tr1
  is 'Open Amount Threshold 1, FK to MPUTRTAB';
comment on column CUSTOMER_ALL_TMR.csclimit_o_tr2
  is 'Open Amount Threshold 2,FK to MPUTRTAB';
comment on column CUSTOMER_ALL_TMR.csclimit_o_tr3
  is 'Open Amount Threshold 3,FK to MPUTRTAB';
comment on column CUSTOMER_ALL_TMR.cscredit_score
  is 'Credit score';
comment on column CUSTOMER_ALL_TMR.cstraderef
  is 'Trade reference for credit scoring';
comment on column CUSTOMER_ALL_TMR.cssocialsecno
  is 'Social security number.';
comment on column CUSTOMER_ALL_TMR.csdrivelicence
  is 'Driving licence number.';
comment on column CUSTOMER_ALL_TMR.cssex
  is 'Gender (M=male,F=female).';
comment on column CUSTOMER_ALL_TMR.csemployer
  is 'Name of the employer.';
comment on column CUSTOMER_ALL_TMR.wpid
  is 'Welcome Procedure / FK to WELCPME_PROC.WPID';
comment on column CUSTOMER_ALL_TMR.csprepayment
  is 'X = this customer should prepay.';
comment on column CUSTOMER_ALL_TMR.csremark_1
  is 'Remark 1 about the customer.';
comment on column CUSTOMER_ALL_TMR.csremark_2
  is 'Remark 2 about the customer.';
comment on column CUSTOMER_ALL_TMR.ma_id
  is 'Register a Marketing-Action-ID for a Sales-Force-Member, FK to MARKETING_ACTION.MA_ID';
comment on column CUSTOMER_ALL_TMR.cssumaddr
  is 'C=the contract address will be used for sum sheet, B=the bill address will';
comment on column CUSTOMER_ALL_TMR.bill_information
  is 'X=the respective customer has to be processed in bill information runs. Default is NULL.';
comment on column CUSTOMER_ALL_TMR.dealer_id
  is 'Dealer who the data is assigned to-. Logical FK: CUSTOMER_ALL_TMR.CUSTOMER_ID.';
comment on column CUSTOMER_ALL_TMR.dunning_mode
  is 'Dunning mode as proposed by ext. Risk Management System.';
comment on column CUSTOMER_ALL_TMR.not_valid
  is 'Indicates, that data have not been validated by the application (=X).
An X is only allowed if CUSTOMER_ALL_TMR.CSTYPE="i".';
comment on column CUSTOMER_ALL_TMR.cscrdcheck_agreed
  is 'X=agreement to external credit check.';
comment on column CUSTOMER_ALL_TMR.marital_status
  is 'Marital Status. FK: MARITAL_STATUS.MAS_ID.';
comment on column CUSTOMER_ALL_TMR.expect_pay_curr_id
  is 'The currency this customer is supposed to pay his invoice in.
Will not be used in case this customer is not payment responsible.
Will be set to NULL in case this customer`s expected payment currency is configured to be the home currency or in case no expected payment currency is specified for this customer.
FK: FORCURR.FC_ID.';
comment on column CUSTOMER_ALL_TMR.convratetype_payment
  is 'The conversion rate type to be used when payments coming in from this customer are registered.
Note that this conversion rate type must be set if an expected payment currency
is specified for this customer, but in any case a payment comes in from this customer.
FK: CONVRATETYPES.CONVRATETYPE_ID.';
comment on column CUSTOMER_ALL_TMR.refund_curr_id
  is 'The currency this customer is supposed to receive refunds in.
Will not be used in case this customer is not payment responsible.
Will be set to NULL in case customer`s refunding currency is configured to be the home currency.
FK: FORCURR.FC_ID.';
comment on column CUSTOMER_ALL_TMR.convratetype_refund
  is 'The conversion rate type to be used when this customer is refunded.
Mandatory , if REFUND_CURR_ID is not NULL.
FK: CONVRATETYPES.CONVRATETYPE_ID.';
comment on column CUSTOMER_ALL_TMR.srcode
  is 'Identifier for a subscription fee reduction action.
FK to MPUSRTAB.SRCODE.';
comment on column CUSTOMER_ALL_TMR.currency
  is 'Currency
The CUSTOMER_ALL_TMR.CURRENCY attribute specifies the currency, which is used for the customer.
The currencies are defined in the FORCURR table.
Further Information:
- FORCURR table comment';
comment on column CUSTOMER_ALL_TMR.primary_doc_currency
  is 'Primary Document Currency.
Not NULL for payment responsible customers.
FK: FORCURR';
comment on column CUSTOMER_ALL_TMR.secondary_doc_currency
  is 'Secondary Document Currency.
FK: FORCURR';
comment on column CUSTOMER_ALL_TMR.prim_convratetype_doc
  is 'Conversion Rate Type to be used for the Primary Document Currency.
 Mandatory, if PRIMARY_DOC_CURRENCY is not NULL.
FK: CONVRATETYPES';
comment on column CUSTOMER_ALL_TMR.sec_convratetype_doc
  is 'Conversion Rate Type to be used for the Secondary Document Currency.
 Mandatory, if SEC_DOC_CURRENCY is not NULL.
FK: CONVRATETYPES';
comment on column CUSTOMER_ALL_TMR.rec_version
  is 'Counter for multiuser access';
comment on column CUSTOMER_ALL_TMR.anonymous_customer
  is 'X - anonymous customer. Used by prepaid processes.';
comment on column CUSTOMER_ALL_TMR.dummy_customer
  is 'X - dummy customer. Used by prepaid processes.';
comment on column CUSTOMER_ALL_TMR.dummy_owner_id
  is 'Customer Id of dummy owner. A dummy owner is either a service provider or a network operator. Only dummy customers have a dummy owner.';
comment on column CUSTOMER_ALL_TMR.insertiondate
  is 'Insertion date';
comment on column CUSTOMER_ALL_TMR.lastmoddate
  is 'Last modification date';
-- Create/Recreate indexes 
create unique index CS_CUSTCODE1 on CUSTOMER_ALL_TMR (CUSTCODE, CSLEVEL)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CS_CUSTID_UP1 on CUSTOMER_ALL_TMR (CUSTOMER_ID_HIGH)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CS_RESELLER1 on CUSTOMER_ALL_TMR (CSRESELLER)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CUST_CUSTCODE1 on CUSTOMER_ALL_TMR (CUSTCODE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKICUSTWELP1 on CUSTOMER_ALL_TMR (WPID)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKIBCCYL1 on CUSTOMER_ALL_TMR (BILLCYCLE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSBMI1 on CUSTOMER_ALL_TMR (CSITEMBILLMEDIUM)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSBMM1 on CUSTOMER_ALL_TMR (CSBILLMEDIUM)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSCOL1 on CUSTOMER_ALL_TMR (CSCOLLECTOR)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT11 on CUSTOMER_ALL_TMR (CSCLIMIT_O_TR1)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT21 on CUSTOMER_ALL_TMR (CSCLIMIT_O_TR2)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSOT31 on CUSTOMER_ALL_TMR (CSCLIMIT_O_TR3)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSPSC1 on CUSTOMER_ALL_TMR (POS_COMM_TYPE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKICSRBC1 on CUSTOMER_ALL_TMR (CSRENTALBC)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index FKIFKIPGPGC1 on CUSTOMER_ALL_TMR (PRGCODE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_CSSOCIALSECNO1 on CUSTOMER_ALL_TMR (CSSOCIALSECNO)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IX_CUSTOMER_ALL_LMD1 on CUSTOMER_ALL_TMR (LASTMODDATE)
  tablespace IND3
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete on CUSTOMER_ALL_TMR to SRE_DAT;
grant select, insert, update, delete on CUSTOMER_ALL_TMR to SRE_MIR;
grant select, insert, update, delete on CUSTOMER_ALL_TMR to SRE_PRI;
grant select on CUSTOMER_ALL_TMR to WLS_JEIS_HUB;



-- Create table
create table PR_SERV_STATUS_HIST_TMR
(
  profile_id      INTEGER not null,
  co_id           INTEGER not null,
  sncode          INTEGER not null,
  histno          INTEGER not null,
  status          VARCHAR2(1) not null,
  reason          INTEGER not null,
  transactionno   INTEGER not null,
  valid_from_date DATE,
  entry_date      DATE default (SYSDATE) not null,
  request_id      INTEGER,
  rec_version     INTEGER default (0) not null,
  initiator_type  CHAR(1) default ('C') not null,
  insertiondate   DATE default SYSDATE not null,
  lastmoddate     DATE,
  estado          VARCHAR2(1)
)
tablespace DATA6
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table PR_SERV_STATUS_HIST_TMR
  is 'This table contains the history of service status changes within profiles. For further information see description of the parent table PROFILE_SERVICE.';
-- Add comments to the columns 
comment on column PR_SERV_STATUS_HIST_TMR.profile_id
  is 'Profile identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_TMR.co_id
  is 'Contract identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_TMR.sncode
  is 'Service identifier, a part of FK pointing to the PROFILE_SERVICE table';
comment on column PR_SERV_STATUS_HIST_TMR.histno
  is 'History number beginning with 1.
Sequence for building of the HISTNO is used.';
comment on column PR_SERV_STATUS_HIST_TMR.status
  is 'Status of the service within a profile. Domain:  A (active), D (deactivate), S (suspended in a contract), O (initial value, on-hold status).';
comment on column PR_SERV_STATUS_HIST_TMR.reason
  is 'Reason of status change, possible values are:
0 initial activation
1 following state value of the service within a profile during its life cycle without any external reasons (reasons 2 till 5),
2 Service package change, external reason,
3 Tariff model change, external reason,
4 Profile change (s. PROFILE_CHANGE_HIST), external reason,
5 Contract suspension, external reason.
9 Service is invisible. Reason could not be determined during data migration. Valid only in combination with status D.
For further information see Main HLD for the 82630 feature.
Note for developers with process description: The database cannot guaranty that the value of this attribute corresponds to a reason caused in the reality the status change. For example, if the attribute value is 3, the database cannot guaranty that the RATEPLAN_HIST table obtains the corresponding entry with the new rate plan. For this purpose the attribute TRANSACTIONNO serves (s. example below).
This attribute is used only for performance goals. And the BSCS-program has to guarantee the consistency of data.
Example: a new entry of the PR_SERV_STATUS_HIST_TMR table with the following values has to be inserted (The status change within entries is not allowed, a new entry with the new status has to be inserted.): STATUS = `D`, TRANSACTIONNO=5 and the REASON=3. It means the RATEPLAN_HIST table has to obtain the new entry containing some new rate plan with the same TRANSACTIONNO value as the mentioned above entry has. The TRANSACTIONNO value shows which entries from different tables were simultaneously into the database inserted.';
comment on column PR_SERV_STATUS_HIST_TMR.transactionno
  is 'Transaction number. This attribute is used for reproducing which history entries were simultaneously stored (stored during one transaction with this transaction number). The sequence PR_SERV_TRANS_NO produces attribute values.';
comment on column PR_SERV_STATUS_HIST_TMR.valid_from_date
  is 'From this date on the value of STATUS is valid. At this date the request has been processed. Or from this date the switch has set this status. Note: assume that the service is active. It means, that this table contains the corresponding entry with the STATUS=A. If a new status e.g. D is requested, then the new entry with D-status and VALID_FROM_DATE = NULL has to be inserted into this table. The old profile service status (old entry) has to be used during request pending phase. If the change request is processed the attribute VALID_FROM_DATE gets a not null value and then the old value is not more active. Since VALID_FROM_DATE the new entry is active.';
comment on column PR_SERV_STATUS_HIST_TMR.entry_date
  is 'Date of insertion of an entry.';
comment on column PR_SERV_STATUS_HIST_TMR.request_id
  is 'With this request the value of STATUS is ordered. FK pointing to the GMD_REQUEST_BASE table containing the requested date.';
comment on column PR_SERV_STATUS_HIST_TMR.rec_version
  is 'Counter for multi-user access.';
comment on column PR_SERV_STATUS_HIST_TMR.initiator_type
  is 'Specifies who initiated the status change: C = Customer Administration, D = Dunning (which will only revert its own status changes).';
comment on column PR_SERV_STATUS_HIST_TMR.insertiondate
  is 'Insertion date';
comment on column PR_SERV_STATUS_HIST_TMR.lastmoddate
  is 'Last modification date';
  /*
-- Create/Recreate primary, unique and foreign key constraints 
alter table PR_SERV_STATUS_HIST_TMR
  add constraint PK_PR_SERV_STATUS_HIST primary key (CO_ID, SNCODE, PROFILE_ID, HISTNO)
  using index 
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
 );
alter table PR_SERV_STATUS_HIST_TMR
  add constraint FK_PR_SERV_STATUS_HIST_PR_SRV foreign key (CO_ID, SNCODE, PROFILE_ID)
  references PROFILE_SERVICE (CO_ID, SNCODE, PROFILE_ID);
alter table PR_SERV_STATUS_HIST_TMR
  add constraint FK_PR_SERV_STATUS_HIST_REQUEST foreign key (REQUEST_ID)
  references GMD_REQUEST_BASE (REQUEST_ID);
-- Create/Recreate check constraints 
alter table PR_SERV_STATUS_HIST_TMR
  add constraint CHECK_SERV_STATUS_HIST_REASON
  check (REASON BETWEEN 0 AND 5 OR REASON = 9 AND STATUS = 'D')
  disable;
alter table PR_SERV_STATUS_HIST_TMR
  add constraint CHECK_SERV_STATUS_HIST_STATUS
  check (STATUS in ('A', 'D', 'S', 'O', 'P'));
alter table PR_SERV_STATUS_HIST_TMR
  add constraint CK_PR_SERV_STATUS_HIST_INIT
  check (INITIATOR_TYPE IN ('C','D'));
*/-- Create/Recreate indexes 
create index IX_PR_SERV_STATUS_HIST_LMD1 on PR_SERV_STATUS_HIST_TMR (LASTMODDATE)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IX_PR_SERV_STATUS_STATUS1 on PR_SERV_STATUS_HIST_TMR (STATUS)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 8M
    next 1M
    minextents 1
    maxextents unlimited
  )
  compress;
create index IX_PR_SERV_VALIDFROMDATE1 on PR_SERV_STATUS_HIST_TMR (VALID_FROM_DATE)
  tablespace IND6
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 8M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete on PR_SERV_STATUS_HIST_TMR to SRE_DAT;
grant select, insert, update, delete on PR_SERV_STATUS_HIST_TMR to SRE_MIR;
grant select, insert, update, delete on PR_SERV_STATUS_HIST_TMR to SRE_PRI;
grant select on CUSTOMER_ALL_TMR to WLS_JEIS_HUB;



-- Create table
create table PROFILE_SERVICE_TMR
(
  profile_id     INTEGER not null,
  co_id          INTEGER not null,
  sncode         INTEGER not null,
  spcode_histno  INTEGER not null,
  status_histno  INTEGER not null,
  entry_date     DATE default (SYSDATE) not null,
  channel_num    INTEGER,
  ovw_acc_first  VARCHAR2(1),
  on_cbb         VARCHAR2(1),
  date_billed    DATE,
  sn_class       INTEGER,
  ovw_subscr     VARCHAR2(1),
  subscript      FLOAT,
  ovw_access     VARCHAR2(1),
  ovw_acc_prd    INTEGER not null,
  accessfee      FLOAT,
  channel_excl   VARCHAR2(1),
  dis_subscr     FLOAT,
  install_date   DATE,
  trial_end_date DATE,
  prm_value_id   INTEGER,
  currency       INTEGER not null,
  srv_type       VARCHAR2(1),
  srv_subtype    VARCHAR2(1),
  ovw_adv_charge VARCHAR2(1) not null,
  adv_charge     FLOAT default (0.0) not null,
  adv_charge_prd INTEGER not null,
  delete_flag    VARCHAR2(1),
  rec_version    INTEGER default (0) not null,
  insertiondate  DATE default SYSDATE not null,
  lastmoddate    DATE
)
tablespace DATA2
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table PROFILE_SERVICE_TMR
  is 'This table contains actual values of all attributes within a profile (s. PROFILE table for further information).
For some service attributes the history is stored and for some not. The values of attributes having a history are saved in the history tables and the numbers of actual histories are saved in this table.';
-- Add comments to the columns 
comment on column PROFILE_SERVICE_TMR.profile_id
  is 'Profile identifier, FK pointing to the PROFILE table';
comment on column PROFILE_SERVICE_TMR.co_id
  is 'Contract identifier pointing with service identifier to the CONTRACT_SERVICE table';
comment on column PROFILE_SERVICE_TMR.sncode
  is 'Service identifier pointing with contract identifier to the CONTRACT_SERVICE table';
comment on column PROFILE_SERVICE_TMR.spcode_histno
  is 'Valid history number entry in the PR_SERV_SPCODE_HIST saving actual service package.';
comment on column PROFILE_SERVICE_TMR.status_histno
  is 'Valid history number entry in the PR_SERV_STATUS_HIST saving actual status of a profile service.';
comment on column PROFILE_SERVICE_TMR.entry_date
  is 'Date of the insertion of this record into this table (sysdate). The information is used for the tax calculation.';
comment on column PROFILE_SERVICE_TMR.channel_num
  is 'Associated channel (subaddress) for services of ERMES market. Subscribers of the European Radio Message System (ERMES) are identified by a 35 Bit Radio Identity Code (RIC) that is the port in the ERMES market. ERMES Directory Number is Address Code (AdC) that consists of 9 digits and can be assigned to individual services (for example: Infobox). Each RIC can obtain several directory numbers (so-called channels). Each ERMES network service gets a channel, which is stored in this attribute. Remark: two different services can get the same channel.';
comment on column PROFILE_SERVICE_TMR.ovw_acc_first
  is 'X = indicates that overwriting of access charge for this service within the profile has never been processed by BCH.';
comment on column PROFILE_SERVICE_TMR.on_cbb
  is 'C= charged on contract based bill, otherwise null. This flag means that the service has been already charged on the contract based bill. The attribute can be filled only once.';
comment on column PROFILE_SERVICE_TMR.date_billed
  is 'Date when the service has been billed the last time.
NULL if the service has not been billed yet. Date of last bill cycle run. A bill cycle runs per contract or per group of contracts (Several customers can be involved in the billing process.). We save this information per service, because for new services that are assigned to the contract after several bill runs it is not possible to control when the last bill cycle has been run.';
comment on column PROFILE_SERVICE_TMR.sn_class
  is 'Service code of the service, which is the representative for the equivalence class.
No history is needed, because this entry is never updated.';
comment on column PROFILE_SERVICE_TMR.ovw_subscr
  is 'Overwrite flag for subscription charges, A = SUBSCRIPT is an amount, R = SUBSCRIPT is a percentage relative to the tariff model price, see MPULKTMB table, attribute SUBSCRIPT. NULL means that tariff model price has to be taken. This value can be changed.';
comment on column PROFILE_SERVICE_TMR.subscript
  is 'Overwrite amount for subscription charges. For further information see description of the OVW_SUBSCRIPT attribute.';
comment on column PROFILE_SERVICE_TMR.ovw_access
  is 'Overwrite flag for access charges, A = ACCESS is an amount, R = ACCESS is a percentage relative to the tariff model price, see MPULKTMB table, attribute ACCESSFEE. NULL means that the ACCESSFEE value has to be taken from the MPULKTMB table.';
comment on column PROFILE_SERVICE_TMR.ovw_acc_prd
  is 'The number of times of the overwritten amount (value of the ACCESSFEE attribute) has to be applied by BCH.
Domain:
>1 : the overwritten charge has to be used and the value is reduced by 1
-1 : the overwritten charge has to be used and the value is not changed
1  : the overwritten charge has to be used and the value is set to -2
-2 : the charge from the tariff model is used and the value is set to 0
0  : the overwritten charge has not to be considered by BCH any more but
the value from the tariff model has to be used by BCH';
comment on column PROFILE_SERVICE_TMR.accessfee
  is 'Overwrite amount for access fee. For further information see descriptions of the OVW_ACCESS and OVW_ACC_PRD attributes.';
comment on column PROFILE_SERVICE_TMR.channel_excl
  is 'Indicates whether the channel used for this service has to be used exclusive or not. For ERMES market the only `Tone-Only-Paging` service is exclusive.
Allowed values: "X" = exclusive, otherwise null (none exclusive). The channel assigned to this service can be assigned to other services. CA copies the value of this attribute from some other place. The attribute cannot be manually updated. See also description of the CHANNEL_NUM attribute.';
comment on column PROFILE_SERVICE_TMR.dis_subscr
  is 'Amount discounted on the subscription fee (depending on the number of contracts).
Null means no discount is given.
It is a discount that is used per service, if the number of contracts is greater as a defined threshold value. The service discount grows with each new contract. It is a function of number of contracts. This function delivers with each new contract a new value for its service discount. CA sets the attribute and then calculates each time for every new contract new values for its service discounts.';
comment on column PROFILE_SERVICE_TMR.install_date
  is 'Date of service installation by subscriber at home, e.g. ISDN installation. Some markets use this information. The saving of history is not required, because an information will be delivered from an external system. It is typical provisioning situation.
The same service in different profiles can be considered as different services. The subscriber could use two profiles simultaneously with the same services.  Therefore some installations can be profile dependent.
Remark: GSM services do not need any installation.';
comment on column PROFILE_SERVICE_TMR.trial_end_date
  is 'The last day the profile service will be `on trail`. It deals with time limited contract services. For example a customer wants to test some provider services. In this case he makes a trial contract with provider. NULL means the saved profile service is not on trial. The saved information depends on a service. It can be a non-trial contract with trial services.';
comment on column PROFILE_SERVICE_TMR.prm_value_id
  is 'Service parameter value identifier. Points to a set of parameter values. FK: PARAMETER_VALUE_BASE. PRM_VALUE_ID. It is possible to assign to a bscs-service a list of parameters with their values. The PARAMETER_VALUE_BASE table contains those lists. The PARAMETER_VALUE table, containing linked parameters and their values, reference the PARAMETER_VALUE_BASE table. NULL-value means, that for the service within a profile no set of parameter values is required.';
comment on column PROFILE_SERVICE_TMR.currency
  is 'Currency. FK: FORCURR. In this currency all amounts of money are calculated and saved in this table. This column was introduced with the Euro feature 52818. It contains the rateplan currency to the assignment time. The attribute is needed, because the entry is not changed if the currency in the rateplan is changed. The same service can have different currency information in different profiles (p.4, ch.2.1)';
comment on column PROFILE_SERVICE_TMR.srv_type
  is 'Service type (flag for BCH to avoid large joins): V=service is VAS (network VAS) (used for selling goods), NULL=all other services. The column SRV_TYPE of CONTR_SERVICES is set automatically by CA as a copy of the MPULKNXV. SRV_TYPE attribute.';
comment on column PROFILE_SERVICE_TMR.srv_subtype
  is 'A subscriber can get free units for some services. But he can get them in different levels, e. g. in the contract level. In this case the subscriber can get free units for each contract (COFU: contract individual Free Units). The subscriber can get free units for some selected contracts (POFU: pooled Free Units at subscriber level). Free units can be assigned at the payment responsible level of a large account (POFUL). Via the POFUL service free units can be pooled over all contracts of a flat subscriber. Possible values are: NULL means the service is no free-units service, `C` for COFU (contract individual Free Units), `P` for POFU (pooled Free Units at subscriber level), `L` for POFUL (pooled Free Units at payment responsible level). It is the copy of the MPULKNXV. SRV_SUBTYPE attribute.';
comment on column PROFILE_SERVICE_TMR.ovw_adv_charge
  is 'This flag determines algorithm how access charge in advance has to be calculated. Possible values are: N means no changes relative to MPULKTMB.ACCESSFEE value have to be done, A = ADV_CHARGE is an amount, R = ADV_CHARGE is a factor relative to the tariff model price, see MPULKTMB table, attribute ACCESSFEE. The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_TMR.adv_charge
  is 'Access charge in advance (see description of the OVW_ADV_CHARGE attribute). The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_TMR.adv_charge_prd
  is 'How often the ADV_CHARGE amount, saved in this table, has to be applied by BCH, value -1 means unlimited. The attribute is set by CA or CAS module.';
comment on column PROFILE_SERVICE_TMR.delete_flag
  is 'Value `X` means that the entry is marked for deletion, otherwise null.';
comment on column PROFILE_SERVICE_TMR.rec_version
  is 'Counter for multi-user access.';
comment on column PROFILE_SERVICE_TMR.insertiondate
  is 'Insertion date';
comment on column PROFILE_SERVICE_TMR.lastmoddate
  is 'Last modification date';
-- Create/Recreate indexes 
create index IX_PROFILE_SERVICE_LMD1 on PROFILE_SERVICE_TMR (LASTMODDATE)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PROFILE_SERVICE_DELETE_FLAG2 on PROFILE_SERVICE_TMR (DELETE_FLAG)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PROFILE_SERVICE_PRM_VAL_ID3 on PROFILE_SERVICE_TMR (PRM_VALUE_ID)
  tablespace IND2
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 7M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges
grant select, insert, update, delete on PROFILE_SERVICE_TMR to SRE_DAT;
grant select, insert, update, delete on PROFILE_SERVICE_TMR to SRE_MIR;
grant select, insert, update, delete on PROFILE_SERVICE_TMR to SRE_PRI;
grant select on PROFILE_SERVICE_TMR to WLS_JEIS_HUB;