-- Create table
create table RC_TMP_CONTROL_PAG
(
  fecha_fact      DATE,
  customer_id     NUMBER,
  num_fact        NUMBER,
  fecha_pago      DATE,
  forma_pag       VARCHAR2(50),
  tip_client      VARCHAR2(50),
  canal_vent      VARCHAR2(50),
  finaciamiento   VARCHAR2(100),
  region          VARCHAR2(50),
  producto        VARCHAR2(50),
  valor_pago      NUMBER,
  codigo_gestor    NUMBER,
  ejecutivo    VARCHAR2(300),
  ejecutivo_postventa  VARCHAR2(300),
  usuario_ingreso VARCHAR2(50),
  fecha_ingreso   DATE
  
)
tablespace DATA12
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the table 
comment on table RC_TMP_CONTROL_PAG
  is 'Tabla temporal del cruse de los pagos del dia anterior con la cuentas facturadas para el reporte segmentado por capas.';
-- Add comments to the columns 
comment on column RC_TMP_CONTROL_PAG.fecha_fact
  is 'Fecha del corte de la facturacion.';
comment on column RC_TMP_CONTROL_PAG.customer_id
  is 'Custmer_Id del cliente.';
comment on column RC_TMP_CONTROL_PAG.num_fact
  is 'Codigo de la factura del cliente.';
comment on column RC_TMP_CONTROL_PAG.fecha_pago
  is 'Fecha del pago del cliente.';
comment on column RC_TMP_CONTROL_PAG.forma_pag
  is 'El tipo de forma de pago que posee el cliente.';
comment on column RC_TMP_CONTROL_PAG.tip_client
  is 'El tipo de calificacion que posee el cliente.';
comment on column RC_TMP_CONTROL_PAG.canal_vent
  is 'El canal de venta que le vendio el servicio al cliente.';
comment on column RC_TMP_CONTROL_PAG.finaciamiento
  is 'Si el cliente posee un equipo financiado o no.';
comment on column RC_TMP_CONTROL_PAG.region
  is 'Los tipos de regiones que son "TODOS", "COSTA" y "SIERRA".';
comment on column RC_TMP_CONTROL_PAG.producto
  is 'Los tipos de carteras que son "TODOS", "SMA" y "DTH".';
comment on column RC_TMP_CONTROL_PAG.valor_pago
  is 'Valor del pago que el cliente realizo.';
comment on column RC_TMP_CONTROL_PAG.codigo_gestor
  is 'Ingreso del codigo de gestor.';
comment on column RC_TMP_CONTROL_PAG.ejecutivo
  is 'Si el cliente tiene asignado un ejecutivo.';
comment on column RC_TMP_CONTROL_PAG.ejecutivo_postventa
  is 'Si el cliente tiene asignado un ejecutivo postventa.';
comment on column RC_TMP_CONTROL_PAG.usuario_ingreso
  is 'Usuario que ingreso el registro.';
comment on column RC_TMP_CONTROL_PAG.fecha_ingreso
  is 'Fecha de ingreso del registro.';

-- Create/Recreate indexes 
create index IDX_RC_TMP_CTR_PAG_01 on RC_TMP_CONTROL_PAG (FECHA_FACT)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
  nologging;
