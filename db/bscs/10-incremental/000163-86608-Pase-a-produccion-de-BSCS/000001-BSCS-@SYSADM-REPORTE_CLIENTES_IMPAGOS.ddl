-- Create table
create table REPORTE_CLIENTES_IMPAGOS
(
  ruc               VARCHAR2(20) not null,
  nombre_cliente    VARCHAR2(300),
  numero_cta        VARCHAR2(20),
  region            VARCHAR2(10),
  customer_id       NUMBER,
  total_deuda       NUMBER,
  ciclo_facturacion VARCHAR2(2),
  capa1             VARCHAR2(3),
  valores_capa1     NUMBER,
  capa2             VARCHAR2(3),
  valores_capa2     NUMBER,
  capa3             VARCHAR2(3),
  valores_capa3     NUMBER,
  capa4             VARCHAR2(3),
  valores_capa4     NUMBER,
  usuario_ingreso   VARCHAR2(100),
  fecha_ingreso     DATE
)
tablespace DATA12
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the table 
comment on table REPORTE_CLIENTES_IMPAGOS
  is 'Tabla donde se registran los clientes impagos.';
-- Add comments to the columns 
comment on column REPORTE_CLIENTES_IMPAGOS.ruc
  is 'Contiene el Ruc del cliente.';
comment on column REPORTE_CLIENTES_IMPAGOS.nombre_cliente
  is 'Contiene nombre del cliente.';
comment on column REPORTE_CLIENTES_IMPAGOS.numero_cta
  is 'Contiene el numero de cuenta del cliente.';
comment on column REPORTE_CLIENTES_IMPAGOS.region
  is 'Contiene la region del cliente.';
COMMENT ON COLUMN REPORTE_CLIENTES_IMPAGOS.customer_id
  IS 'Contiene la identificacion del cliente.';  
comment on column REPORTE_CLIENTES_IMPAGOS.total_deuda
  is 'Contiene la deuda total del cliente.';
comment on column REPORTE_CLIENTES_IMPAGOS.ciclo_facturacion
  is 'Contiene el ciclo de facturacion del cliente.';
comment on column REPORTE_CLIENTES_IMPAGOS.capa1
  is 'Contiene la capa, numero de dias 30.';
comment on column REPORTE_CLIENTES_IMPAGOS.valores_capa1
  is 'Contiene la capa, numero de dias 30.';
comment on column REPORTE_CLIENTES_IMPAGOS.capa2
  is 'Contiene la capa, numero de dias 60.';
comment on column REPORTE_CLIENTES_IMPAGOS.valores_capa2
  is 'Contiene la capa, numero de dias 60.';
comment on column REPORTE_CLIENTES_IMPAGOS.capa3
  is 'Contiene la capa, numero de dias 90.';
comment on column REPORTE_CLIENTES_IMPAGOS.valores_capa3
  is 'Contiene la capa, numero de dias 90.';
comment on column REPORTE_CLIENTES_IMPAGOS.capa4
  is 'Contiene la capa, numero de dias 120.';
comment on column REPORTE_CLIENTES_IMPAGOS.valores_capa4
  is 'Contiene la capa, numero de dias 120.';
 comment on column REPORTE_CLIENTES_IMPAGOS.usuario_ingreso
  is 'Contiene el usuario de ingreso.';
comment on column REPORTE_CLIENTES_IMPAGOS.fecha_ingreso
  is 'Contiene la fecha de ingreso.'; 

create index IDX_FIN_REP_CLI_IMPAGOS_01 on REPORTE_CLIENTES_IMPAGOS (RUC)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

grant select, insert, update, delete on REPORTE_CLIENTES_IMPAGOS to DBJZAMBRANO;


