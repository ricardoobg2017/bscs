-- Create table
create table FN_DETFACTURACION_FINAN
(
  no_fac       NUMBER(20),
  no_detfac    NUMBER(20),
  id_finan_cbs VARCHAR2(20),
  total        NUMBER(20),
  saldo        NUMBER(20),
  fecha_fac    DATE
)
tablespace ORDER_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-----add coments 
comment on column FN_DETFACTURACION_FINAN.no_fac
  is 'no de factura del financimaiento';
comment on column FN_DETFACTURACION_FINAN.no_detfac
  is 'no del detalle del financiamiento ';
comment on column FN_DETFACTURACION_FINAN.id_finan_cbs
  is 'id financiamineto cbs';
comment on column FN_DETFACTURACION_FINAN.total
  is 'total facturado  ';
comment on column FN_DETFACTURACION_FINAN.saldo
  is 'saldo';
comment on column FN_DETFACTURACION_FINAN.fecha_fac
  is 'fecha de la factura ';    

-- Create/Recreate indexes 
create index IDX_FACT_FINAN_1 on FN_DETFACTURACION_FINAN (ID_FINAN_CBS)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FACT_FINAN_2 on FN_DETFACTURACION_FINAN (NO_FAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_FACT_FINAN_3 on FN_DETFACTURACION_FINAN (NO_DETFAC)
  tablespace ORDER_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
