CREATE INDEX OCC_CARGAHIST_IDX on OCC_CARGA_HIST
(DN_NUM, CO_ID, CUSTOMER_ID, SNCODE, VALID_FROM)            
tablespace BLP_CAR_IDX
pctfree 10
maxtrans 255
storage
(
initial 100M
minextents 1
maxextents unlimited
)
