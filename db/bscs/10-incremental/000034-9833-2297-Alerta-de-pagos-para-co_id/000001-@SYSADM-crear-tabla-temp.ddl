-- Create table
create table CONTRACT_HISTORY_TEMP
(
  co_id          INTEGER not null,
  ch_seqno       INTEGER not null,
  ch_status      VARCHAR2(1) not null,
  ch_reason      INTEGER not null,
  ch_validfrom   DATE,
  ch_pending     VARCHAR2(1),
  entdate        DATE,
  userlastmod    VARCHAR2(16),
  request        INTEGER,
  rec_version    INTEGER default (0) not null,
  initiator_type CHAR(1) default ('C') not null,
  inserdate      DATE,
  lastmoddate    DATE
)
tablespace TEH_IDX
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 120K
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the table 
comment on table CONTRACT_HISTORY_TEMP
  is 'History of status_changes for a contract - This table is filled by the stored procedure CONTRACT.ChangeStateContract().';
-- Add comments to the columns 
comment on column CONTRACT_HISTORY_TEMP.co_id
  is 'the contract. fkey to contract_all.co_id';
comment on column CONTRACT_HISTORY_TEMP.ch_seqno
  is 'consecutive number. incremented by 1 for each status change';
comment on column CONTRACT_HISTORY_TEMP.ch_status
  is 'possible status values are:
o: on hold (must be the first status)
a: activated
s: suspended
d: deactivated';
comment on column CONTRACT_HISTORY_TEMP.ch_reason
  is 'reason for the status-change';
comment on column CONTRACT_HISTORY_TEMP.ch_validfrom
  is 'date, when this status change will be valid';
comment on column CONTRACT_HISTORY_TEMP.ch_pending
  is 'Request is pending for this contract. If pending then "X" else NULL. Only one pending entry is allowed for a contract!';
comment on column CONTRACT_HISTORY_TEMP.entdate
  is 'date, when this change has been entered';
comment on column CONTRACT_HISTORY_TEMP.userlastmod
  is 'user, who made this status change';
comment on column CONTRACT_HISTORY_TEMP.request
  is 'request_id, logical fkey to table MDSRRTAB.REQUEST';
comment on column CONTRACT_HISTORY_TEMP.rec_version
  is 'Counter for multiuser access';
comment on column CONTRACT_HISTORY_TEMP.initiator_type
  is 'Specifies who initiated the status change: C = Customer Administration, D = Dunning (which will only revert its own status changes).';
comment on column CONTRACT_HISTORY_TEMP.inserdate
  is 'Insertion date';
comment on column CONTRACT_HISTORY_TEMP.lastmoddate
  is 'Last modification date';
-- Create/Recreate indexes 
create index IX_CONTRACT_HISTORY_TEMP_LMD on CONTRACT_HISTORY_TEMP (LASTMODDATE)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CONTRACT_HISTORY_TEMP
  add constraint PKCONTRACT_HISTORY_TEMP primary key (CO_ID, CH_SEQNO)
  using index 
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 120K
    minextents 1
    maxextents unlimited
  );
