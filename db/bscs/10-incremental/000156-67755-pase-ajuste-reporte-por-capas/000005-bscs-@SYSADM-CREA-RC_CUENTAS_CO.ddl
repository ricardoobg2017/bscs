-- Create table
create table RC_CUENTAS_CO
(
  cuenta          VARCHAR2(24),
  customer_id     NUMBER,
  saldo           NUMBER,
  fecha_fact_co   DATE,
  usuario_ingreso VARCHAR2(100),
  fecha_ingreso   DATE
)
tablespace DATA12
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table RC_CUENTAS_CO
  is 'Tabla temporal de los pagos anticipados para el reporte segmentado por capas.';
-- Add comments to the columns 
comment on column RC_CUENTAS_CO.cuenta
  is 'Numero de la cuenta del cliente.';
comment on column RC_CUENTAS_CO.customer_id
  is 'Custmer_Id del cliente.';
comment on column RC_CUENTAS_CO.saldo
  is 'Saldo a favor que posee el cliente.';
comment on column RC_CUENTAS_CO.fecha_fact_co
  is 'Fecha del saldo a favor del cliente.';
comment on column RC_CUENTAS_CO.usuario_ingreso
  is 'Usuario que ingreso el registro.';
comment on column RC_CUENTAS_CO.fecha_ingreso
  is 'Fecha de ingreso del registro.';
-- Create/Recreate indexes 
create index IDX_RC_CTA_CO_01 on RC_CUENTAS_CO (CUENTA)
  tablespace TEH_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
