--sysadm@bscs
grant select on mf_formas_pagos to LNK_DWCOMDA_CUBO; 
grant select on cashreceipts_all to LNK_DWCOMDA_CUBO;
grant select on cashdetail to LNK_DWCOMDA_CUBO;
grant select on orderhdr_all to LNK_DWCOMDA_CUBO;
grant select on PAYMENT_ALL to LNK_DWCOMDA_CUBO;
grant select on costcenter to LNK_DWCOMDA_CUBO;
grant select on contract_all to LNK_DWCOMDA_CUBO;
grant select on ttc_contractplannigdetails_vw to LNK_DWCOMDA_CUBO;
--
