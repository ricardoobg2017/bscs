--CREADO POR JOYAGUE Y PPADILLA
-- Create table
  create table RPC_CAPAS_BITACORA
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(2),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(2),
  OBSERVACION   varchar2(1000)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table RPC_CAPAS_BITACORA
  add constraint PK_CAPAS_BITACORA1 primary key (ID_BITACORA);
  
-- Add comments to the columns 
comment on column RPC_CAPAS_BITACORA.ID_BITACORA
  is 'Pk de la tabla';
comment on column RPC_CAPAS_BITACORA.IDENTIFICADOR
  is 'C�digo de la tabla RPC_CAPAS_DETALLE';
comment on column RPC_CAPAS_BITACORA.CONTADOR
  is 'N�mero de  veces en que se ejecuta el paso';
comment on column RPC_CAPAS_BITACORA.FECHA_FIN
  is 'Fecha fin del paso';
comment on column RPC_CAPAS_BITACORA.NOMBRE_DEL_SH
  is 'Nombre del shell sh_llena_balance.sh';
comment on column RPC_CAPAS_BITACORA.DESCRIPCION
  is 'Detalle del paso';
comment on column RPC_CAPAS_BITACORA.PERIODO
  is 'Fecha que se procesa';
comment on column RPC_CAPAS_BITACORA.CUSTOMER
  is 'Id Cliente que utiliza uno de los pasos';
comment on column RPC_CAPAS_BITACORA.FECHA
  is 'Fecha de inserci�n';
comment on column RPC_CAPAS_BITACORA.ESTADO
  is 'E = Error   D = No Data    R = Revisado';

-- RPC_CAPAS_BITACORA_M
  create table RPC_CAPAS_BITACORA_M
(
  ID_BITACORA   NUMBER(10),
  IDENTIFICADOR NUMBER(2),
  CONTADOR      NUMBER(2),
  FECHA_FIN     DATE,
  NOMBRE_DEL_SH VARCHAR2(100),
  DESCRIPCION   VARCHAR2(100),
  PERIODO       DATE,
  CUSTOMER      NUMBER(10),
  FECHA         DATE,
  ESTADO        VARCHAR2(2),
  OBSERVACION   varchar2(1000)
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column RPC_CAPAS_BITACORA_M.ID_BITACORA
  is 'Pk de la tabla';
comment on column RPC_CAPAS_BITACORA_M.IDENTIFICADOR
  is 'C�digo de la tabla RPC_CAPAS_DETALLE';
comment on column RPC_CAPAS_BITACORA_M.CONTADOR
  is 'N�mero de  veces en que se ejecuta el paso';
comment on column RPC_CAPAS_BITACORA_M.FECHA_FIN
  is 'Fecha fin del paso';
comment on column RPC_CAPAS_BITACORA_M.NOMBRE_DEL_SH
  is 'Nombre del shell sh_main.sh';
comment on column RPC_CAPAS_BITACORA_M.DESCRIPCION
  is 'Detalle del paso';
comment on column RPC_CAPAS_BITACORA_M.PERIODO
  is 'Fecha que se procesa';
comment on column RPC_CAPAS_BITACORA_M.CUSTOMER
  is 'Id Cliente que utiliza uno de los pasos';
comment on column RPC_CAPAS_BITACORA_M.FECHA
  is 'Fecha de inserci�n';
comment on column RPC_CAPAS_BITACORA_M.ESTADO
  is 'E = Error   D = No Data    R = Revisado';
 
-- Create sequence 
create sequence SEQ_BITACORA
minvalue 1
maxvalue 999999999999999999999999999
start with 52801
increment by 1
cache 20;
--
-- Create sequence 
create sequence SEQ_BITACORA_MAIN
minvalue 1
maxvalue 999999999999999999999999999
start with 701
increment by 1
cache 20;
