create table CP_CUENTAS_HW_BSCS
(
  cuenta_hw   VARCHAR2(20),
  cuenta_bscs VARCHAR2(20)
)
tablespace CALC_CTA_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CP_CUENTAS_HW_BSCS.cuenta_hw
  is 'Cuenta HW';
comment on column CP_CUENTAS_HW_BSCS.cuenta_bscs
  is 'Cuenta de legado(BSCS)';
-- Create/Recreate indexes 
create index IDX_HWCP_CUENTA on CP_CUENTAS_HW_BSCS (CUENTA_HW)
  tablespace CALC_CTA_IDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
