--EJECUTAR EN BSCS
-- CREATE TABLE
CREATE TABLE GSI_LOG_PLANESQC
(
  COD_SEQ                   VARCHAR2(10),
  TRANSACCION               VARCHAR2(500),
  FECHA_EVENTO              DATE,
  ESTADO                    CHAR(1), 
  OBSERVACION               VARCHAR2(500),
  USUARIO                   VARCHAR2(30) 
)
tablespace BILLING_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table GSI_LOG_PLANESQC
  is 'Tabla de Logs para el proceso de planes QC';
-- Add comments to the columns 
comment on column GSI_LOG_PLANESQC.COD_SEQ
  is 'El codigo de secuencia para el proceso de planes QC';
comment on column GSI_LOG_PLANESQC.TRANSACCION
  is 'El nombre de los pasos de los planes QC que se ejecutan en el proceso';
comment on column GSI_LOG_PLANESQC.FECHA_EVENTO
  is 'La fecha evento del proceso de planes QC';
comment on column GSI_LOG_PLANESQC.ESTADO
  is 'Estado del proceso E= Finalizado con error F=Finalizado';
comment on column GSI_LOG_PLANESQC.OBSERVACION
  is 'Observacion de la ejecucion de los proceso';
comment on column GSI_LOG_PLANESQC.USUARIO
  is 'Usuario de Base Datos que ejecuta el proceso';
