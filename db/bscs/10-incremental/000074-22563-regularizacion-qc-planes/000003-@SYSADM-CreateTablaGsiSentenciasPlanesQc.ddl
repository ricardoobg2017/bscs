-- Create table
create table GSI_SENTENCIAS_PLANESQC
(
  COD_SEQ               VARCHAR2(10),
  ESTADO                CHAR(1),
  DESCRIPCION           VARCHAR2(500),
  EXPRESION_SQL         LONG,
  ETIQUETAS             VARCHAR2(500),
  FECHA_INGRESO         DATE
)
tablespace BILLING_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Add comments to the table 
comment on table GSI_SENTENCIAS_PLANESQC
  is 'Tabla de sentencias para el proceso de Planes QC';
-- Add comments to the columns 
comment on column GSI_SENTENCIAS_PLANESQC.COD_SEQ
  is 'El codigo de secuencia para el proceso de Planes QC';
comment on column GSI_SENTENCIAS_PLANESQC.ESTADO
  is 'El estado  A=Activo I=Inactivo para el proceso de Planes QC';
comment on column GSI_SENTENCIAS_PLANESQC.DESCRIPCION
  is 'La Descripcion de las sentencias para el proceso de Planes QC';
comment on column GSI_SENTENCIAS_PLANESQC.EXPRESION_SQL
  is 'Expresion SQL para el proceso de Planes QC'; 
comment on column GSI_SENTENCIAS_PLANESQC.ETIQUETAS
  is 'Etiquetas de los pasos del proceso de Planes QC';   
comment on column GSI_SENTENCIAS_PLANESQC.FECHA_INGRESO
  is 'La fecha registro de las sentencias de los Planes QC';    
