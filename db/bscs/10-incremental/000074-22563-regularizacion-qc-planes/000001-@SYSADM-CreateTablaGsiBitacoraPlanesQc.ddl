-- CREATE TABLE
CREATE TABLE GSI_BITACORA_PLANESQC
(
  COD_SEQ                   VARCHAR2(10),
  ID_PLAN                   VARCHAR2(10),
  DESCRIPCION               VARCHAR2(350),
  TARIFA_BASICA_COM         VARCHAR2(300),
  TARIFA_BASICA_AXIS        VARCHAR2(300),
  TARIFA_BSCS               VARCHAR2(300),
  VALOR_OMISION             VARCHAR2(100),
  ID_TIPO_DETALLE_SERVICIOS VARCHAR2(10),
  ID_CLASIFICACION_DIF1     VARCHAR2(10),
  ID_CLASIFICACION_DIF2     VARCHAR2(10),
  TIPO                      VARCHAR2(8),
  TIPO_INTERCONEXION        VARCHAR2(100),
  SUBPRODUCTO_PLAN          VARCHAR2(10),
  SUBPRODUCTO_FEATURE       VARCHAR2(10),
  CODIGO                    VARCHAR2(30),
  COSTO_MIN_CLAROACLARO     VARCHAR2(300),
  CLARO_CON_VALOR           VARCHAR2(300),
  DIFERENCIA_CLARO          VARCHAR2(300),
  COSTO_MIN_CLAROAMOVI      VARCHAR2(300),
  MOVI_CON_VALOR            VARCHAR2(300),
  DIFERENCIA_MOVISTAR       VARCHAR2(300),
  COSTO_MIN_CLAROAALEGRO    VARCHAR2(300),
  ALEGRO_CON_VALOR          VARCHAR2(300),
  DIFERENCIA_ALEGRO         VARCHAR2(300),
  COSTO_MIN_CLAROAFIJO      VARCHAR2(300),
  LOCAL_CON_VALOR           VARCHAR2(300),
  DIFERENCIA_FIJAS          VARCHAR2(300),
  CUOTA_MENSUAL_BSCS        VARCHAR2(300),
  CUOTA_MENSUAL_AXIS        VARCHAR2(300),
  ID_DETALLE_PLAN           VARCHAR2(100),
  SNCODE                    NUMBER,
  TMCODE                    NUMBER,
  QC_VERSION                NUMBER,
  ASOCIADO                  VARCHAR2(1),
  CODI_PLAN                 VARCHAR2(10),
  CODIGO_TN3_OPER           VARCHAR2(10),
  CODIGO_TN3_AXIS           VARCHAR2(10),
  SHDES                     VARCHAR2(5),
  VSCODE                    NUMBER,
  VALOR                     VARCHAR2(30),
  ID_FEATURE                VARCHAR2(100),
  PLAN_AXIS                 VARCHAR2(10),
  PLAN_CRE                  VARCHAR2(10),
  TIPO_SVA                  VARCHAR2(10),
  MENSAJE_TECNICO           VARCHAR2(500),
  USUARIO                   VARCHAR2(30),
  ESTADO                    CHAR(1),
  FECHA_EVENTO              DATE,
  PLAN_TECNOMEN             VARCHAR2(10),
  DIFERENCIA_TB_BSCS        VARCHAR2(300),
  DIFERENCIA_TB_AXIS        VARCHAR2(300)
)
tablespace BILLING_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table GSI_BITACORA_PLANESQC
  is 'Tabla de bitacoras para el proceso de planes QC';
-- Add comments to the columns 
comment on column GSI_BITACORA_PLANESQC.cod_seq
  is 'El codigo de secuencia para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.id_plan
  is 'El ID_PLAN de la base Axis para el proeso de planes QC';
comment on column GSI_BITACORA_PLANESQC.descripcion
  is 'La Descripcion de cada uno de los planes';
comment on column GSI_BITACORA_PLANESQC.tarifa_basica_com
  is 'La tarifa basica de comercial para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.tarifa_basica_axis
  is 'La tarifa basica de Axis para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.tarifa_bscs
  is 'La tarifa basica de BSCS para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.valor_omision
  is 'Secuencial que debe coincidir con el ingresado en el campo "id_tipo_detalle_serv" de la misma tabla para los features de servicio';
comment on column GSI_BITACORA_PLANESQC.id_tipo_detalle_servicios
  is 'Codigo secuencial TAR-(n) (Tarifario) AUT-(n) (Autocontrol) PPA-(n) (Prepago) Donde: n es un secuencial
 que debe coincidir con el valor ingresado en el campo "valor_omisi�n" de la misma tabla para el caso de features
 administrativos la (n) se reemplaza por una descripci�n';
comment on column GSI_BITACORA_PLANESQC.id_clasificacion_dif1
  is 'Valor en USD sin impuestos para OCC x contrato diferencia1';
comment on column GSI_BITACORA_PLANESQC.id_clasificacion_dif2
  is 'Valor en USD sin impuestos para OCC x contrato diferencia2';
comment on column GSI_BITACORA_PLANESQC.tipo
  is 'El tipo de plan para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.tipo_interconexion
  is 'El tipo de interconexion para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.subproducto_plan
  is 'EL id_subproducto para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.subproducto_feature
  is 'El Subproducto_feautre para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.codigo
  is 'El codigo del plan para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.costo_min_claroaclaro
  is 'El costo minimo de claro a claro';
comment on column GSI_BITACORA_PLANESQC.claro_con_valor
  is 'El costo de claro con valor de TN3';
comment on column GSI_BITACORA_PLANESQC.diferencia_claro
  is 'La diferencia de claro para el proceso de planes QC ';
comment on column GSI_BITACORA_PLANESQC.costo_min_claroamovi
  is 'El costo minimo de claro a movi';
comment on column GSI_BITACORA_PLANESQC.movi_con_valor
  is 'El costo de movi con el valor de TN3';
comment on column GSI_BITACORA_PLANESQC.diferencia_movistar
  is 'La diferencia de movistar para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.costo_min_claroaalegro
  is 'El costo minimo de claro a alegro';
comment on column GSI_BITACORA_PLANESQC.alegro_con_valor
  is 'El costo de alegro con valor';
comment on column GSI_BITACORA_PLANESQC.diferencia_alegro
  is 'La diferencia de claro para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.costo_min_claroafijo
  is 'El costo minimo de claro a fijo';
comment on column GSI_BITACORA_PLANESQC.local_con_valor
  is 'El costo local con valor de TN3';
comment on column GSI_BITACORA_PLANESQC.diferencia_fijas
  is 'La difencia fija para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.cuota_mensual_bscs
  is 'La cuota mensual de BSCS';
comment on column GSI_BITACORA_PLANESQC.cuota_mensual_axis
  is 'La cuota mensual de AXIS';
comment on column GSI_BITACORA_PLANESQC.id_detalle_plan
  is 'El id_detalle_plan de la tabala gsi_feature_plan';
comment on column GSI_BITACORA_PLANESQC.sncode
  is 'Internal Key of Service Name para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.tmcode
  is 'Internal Key of Tariff Model';
comment on column GSI_BITACORA_PLANESQC.qc_version
  is 'El valor de la version para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.asociado
  is 'El asociado para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.codi_plan
  is 'El codigo del plan para el proeso de planes QC';
comment on column GSI_BITACORA_PLANESQC.codigo_tn3_oper
  is 'El codigo de TN3 a OPER para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.codigo_tn3_axis
  is 'El codigo de TN3 a AXIS para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.shdes
  is 'Visual Description to be printed on Bill para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.vscode
  is 'Internal Key of Version para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.valor
  is 'El valor de NAME para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.id_feature
  is 'Feature en Axis para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.plan_axis
  is 'Nombre del plan segun Axis para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.plan_cre
  is 'Nombre del Plan segun CRE para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.mensaje_tecnico
  is 'EL mesanje tecnico de Error para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.usuario
  is 'El usario que ejecuta el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.estado
  is 'El estado  A=Activo I=Inactivo para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.fecha_evento
  is 'La fecha de inicio del proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.PLAN_TECNOMEN
  is 'El Nombre del plan segun Tecnomen para el proceso de planes QC';
comment on column GSI_BITACORA_PLANESQC.DIFERENCIA_TB_BSCS
  is 'La diferencia de la Tarifa_Basica de BSCS';
comment on column GSI_BITACORA_PLANESQC.DIFERENCIA_TB_AXIS
  is 'La diferencia de la Tarifa_Basica de AXIS'; 
