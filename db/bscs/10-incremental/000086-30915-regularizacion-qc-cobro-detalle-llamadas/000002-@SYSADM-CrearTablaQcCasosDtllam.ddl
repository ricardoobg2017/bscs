-- Create table
create table GSI_QC_CASOS_DTLLAM
(
  codigo_doc       VARCHAR2(20),
  id_servicio      VARCHAR2(20),
  id_contrato      NUMBER(10),
  customer_id      NUMBER,
  id_subproducto   VARCHAR2(20),
  cliente_especial VARCHAR2(1),
  valor_det        NUMBER,
  tipo_fact        VARCHAR2(15),
  escenario        VARCHAR2(150),
  fecha_peri       VARCHAR2(20),
  fecha_desde      DATE,
  fecha_hasta      DATE,
  dias_feat        NUMBER,
  camb_num         VARCHAR2(2),
  est_valid        VARCHAR2(15),
  est_axis         VARCHAR2(1)
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 576K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_QC_CASOS_DTLLAM.codigo_doc
  is 'numero de la cuenta';
comment on column GSI_QC_CASOS_DTLLAM.id_servicio
  is 'id servicio que esta asociado a la cuenta';
comment on column GSI_QC_CASOS_DTLLAM.id_contrato
  is 'contrato que esta asociado a la cuenta y el servicio';
comment on column GSI_QC_CASOS_DTLLAM.customer_id
  is 'codigo con el cual se visualiza en bscs';
comment on column GSI_QC_CASOS_DTLLAM.id_subproducto
  is 'subproducto que tiene el servicio';
comment on column GSI_QC_CASOS_DTLLAM.cliente_especial
  is 'S cuando es cliente especial, N cuando no es cliente especial';
comment on column GSI_QC_CASOS_DTLLAM.valor_det
  is 'valor cargado en la fees';
comment on column GSI_QC_CASOS_DTLLAM.tipo_fact
  is 'CG o CO';
comment on column GSI_QC_CASOS_DTLLAM.escenario
  is 'Casos que se identifican en el proceso';
comment on column GSI_QC_CASOS_DTLLAM.fecha_peri
  is 'Corte de Facturacion en formato yyyymmdd';
comment on column GSI_QC_CASOS_DTLLAM.fecha_desde
  is 'Fecha desde la vigencia de la linea';
comment on column GSI_QC_CASOS_DTLLAM.fecha_hasta
  is 'Fecha hasta la vigencia de la linea';
comment on column GSI_QC_CASOS_DTLLAM.dias_feat
  is 'Dias que la linea tuvo el feature activo';
comment on column GSI_QC_CASOS_DTLLAM.camb_num
  is 'Indica si la linea tiene cambio numero';
comment on column GSI_QC_CASOS_DTLLAM.est_valid
  is 'estado obtenido antes de la carga';
comment on column GSI_QC_CASOS_DTLLAM.est_axis
  is 'estado obtenido des axis';
-- Create/Recreate indexes 
create index IDX_CODIGO_DOC on GSI_QC_CASOS_DTLLAM (CODIGO_DOC)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 28M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_IDSERVICIO on GSI_QC_CASOS_DTLLAM (ID_SERVICIO, ID_CONTRATO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 28M
    next 1M
    minextents 1
    maxextents unlimited
  );
