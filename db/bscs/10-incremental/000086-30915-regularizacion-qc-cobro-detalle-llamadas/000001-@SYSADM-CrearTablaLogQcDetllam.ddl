-- Create table
create table GSI_LOG_QC_COBROSDETLLAM
(
  id_secuencia NUMBER,
  id_proceso   NUMBER,
  transaccion  VARCHAR2(500),
  fecha_ini    DATE,
  fecha_fin    DATE,
  estado       VARCHAR2(2),
  observacion  VARCHAR2(500),
  usuario      VARCHAR2(30)
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table GSI_LOG_QC_COBROSDETLLAM
  is 'Tabla de Logs para el proceso de cobros detalles llamadas QC';
-- Add comments to the columns 
comment on column GSI_LOG_QC_COBROSDETLLAM.id_secuencia
  is 'Id secuencia de los log de cobros detalles llamadas QC';
comment on column GSI_LOG_QC_COBROSDETLLAM.id_proceso
  is 'El codigo que identifica el proceso de cobros detalles llamadas QC';
comment on column GSI_LOG_QC_COBROSDETLLAM.transaccion
  is 'El nombre de los pasos de los cobros detalles llamadas QC que se ejecutan en el proceso';
comment on column GSI_LOG_QC_COBROSDETLLAM.fecha_ini
  is 'La fecha inicio del proceso de cobros detalles llamadas QC';
comment on column GSI_LOG_QC_COBROSDETLLAM.fecha_fin
  is 'La fecha fin del proceso de cobros detalles llamadas QC';
comment on column GSI_LOG_QC_COBROSDETLLAM.estado
  is 'Estado del proceso E= Finalizado con error F=Finalizado RI=Regularizado Incompleto';
comment on column GSI_LOG_QC_COBROSDETLLAM.observacion
  is 'Observacion de la ejecucion de los proceso';
comment on column GSI_LOG_QC_COBROSDETLLAM.usuario
  is 'Usuario de Base Datos que ejecuta el proceso';
