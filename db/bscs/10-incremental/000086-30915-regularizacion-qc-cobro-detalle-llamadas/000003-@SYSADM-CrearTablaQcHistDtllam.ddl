-- Create table
create table GSI_QC_HIST_DTLLAM
(
  id_proceso       NUMBER,
  customer_id      NUMBER,
  codigo_doc       VARCHAR2(20),
  fecha_peri       VARCHAR2(20),
  tipo_fact        VARCHAR2(15),
  escenario        VARCHAR2(150),
  cliente_especial VARCHAR2(1),
  estado           VARCHAR2(1),
  id_contrato      NUMBER,
  observacion      VARCHAR2(300)
)
tablespace DBX_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 576K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column GSI_QC_HIST_DTLLAM.id_proceso
  is 'Id_proceso que identifica la secuencia de ejecucion';
comment on column GSI_QC_HIST_DTLLAM.customer_id
  is 'codigo con el cual se visualiza en bscs';
comment on column GSI_QC_HIST_DTLLAM.codigo_doc
  is 'numero de la cuenta';
comment on column GSI_QC_HIST_DTLLAM.fecha_peri
  is 'Corte de Facturacion en formato yyyymmdd';
comment on column GSI_QC_HIST_DTLLAM.tipo_fact
  is 'CG o CO';
comment on column GSI_QC_HIST_DTLLAM.escenario
  is 'Casos que se identifican en el proceso';
comment on column GSI_QC_HIST_DTLLAM.cliente_especial
  is 'S cuando es cliente especial, N cuando no es cliente especial';
comment on column GSI_QC_HIST_DTLLAM.estado
  is 'P procesada (Estado inicial) / R regularizado (cuando se corrige en la fees)';
comment on column GSI_QC_HIST_DTLLAM.id_contrato
  is 'codigo de la cuenta que se puede ver en axis y bscs';
comment on column GSI_QC_HIST_DTLLAM.observacion
  is 'Indica lo que paso con las cuentas si el escenario es nulo';
-- Create/Recreate indexes 
create index IDX_ESCENARIO on GSI_QC_HIST_DTLLAM (ESCENARIO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 28M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_ESTADO on GSI_QC_HIST_DTLLAM (ESTADO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 28M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IDX_IDPROCESO on GSI_QC_HIST_DTLLAM (ID_PROCESO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 28M
    next 1M
    minextents 1
    maxextents unlimited
  );
