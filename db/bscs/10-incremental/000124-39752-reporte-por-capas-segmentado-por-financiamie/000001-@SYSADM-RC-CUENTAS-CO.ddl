CREATE TABLE RC_CUENTAS_CO(
CUENTA              VARCHAR2(24),
CUSTOMER_ID         NUMBER,
SALDO               NUMBER,
FECHA_FACT_CO       DATE,
USUARIO_INGRESO     VARCHAR2(100),
FECHA_INGRESO       DATE
)
TABLESPACE DATA9
  PCTFREE 10
  PCTUSED 40
  INITRANS 1
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );

CREATE INDEX IDX_RC_CTA_CO_01 ON RC_CUENTAS_CO (CUENTA)
 TABLESPACE Teh_idx
  PCTFREE 10
  INITRANS 2
  MAXTRANS 255
  STORAGE
  (
    INITIAL 64K
    NEXT 1M
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
  );

-- Add comments to the table 
COMMENT ON TABLE RC_CUENTAS_CO
  IS 'Tabla temporal de los pagos anticipados para el reporte segmentado por capas.';
-- Add comments to the columns 
COMMENT ON COLUMN RC_CUENTAS_CO.CUENTA
  IS 'Numero de la cuenta del cliente.';
COMMENT ON COLUMN RC_CUENTAS_CO.CUSTOMER_ID
  IS 'Custmer_Id del cliente.';
COMMENT ON COLUMN RC_CUENTAS_CO.SALDO
  IS 'Saldo a favor que posee el cliente.';
COMMENT ON COLUMN RC_CUENTAS_CO.FECHA_FACT_CO
  IS 'Fecha del saldo a favor del cliente.';
COMMENT ON COLUMN RC_CUENTAS_CO.USUARIO_INGRESO
  IS 'Usuario que ingreso el registro.';
COMMENT ON COLUMN RC_CUENTAS_CO.FECHA_INGRESO
  IS 'Fecha de ingreso del registro.';
