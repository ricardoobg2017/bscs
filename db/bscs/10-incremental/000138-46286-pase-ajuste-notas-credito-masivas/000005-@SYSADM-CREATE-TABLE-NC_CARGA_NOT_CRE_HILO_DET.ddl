-- Create table
create table NC_CARGA_NOT_CRE_HILO_DET
(
  id_ejecucion   NUMBER,
  hilo           NUMBER,
  estado         VARCHAR2(2),
  reg_procesados NUMBER,
  seq            NUMBER,
  fecha_inicio   DATE,
  fecha_fin      DATE,
  observacion    VARCHAR2(200)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
