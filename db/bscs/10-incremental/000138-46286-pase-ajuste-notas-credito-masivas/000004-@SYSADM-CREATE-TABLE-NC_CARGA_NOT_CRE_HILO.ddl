-- Create table
create table NC_CARGA_NOT_CRE_HILO
(
  id_ejecucion   NUMBER,
  hilo           NUMBER,
  estado         VARCHAR2(2),
  reg_procesados NUMBER
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255;
