-- Create table
create table NC_PARAMETROS_GRALES
(
  codigo      VARCHAR2(30),
  descripcion VARCHAR2(300),
  valor1      VARCHAR2(1000),
  valor2      VARCHAR2(1000),
  estado      VARCHAR2(2)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
