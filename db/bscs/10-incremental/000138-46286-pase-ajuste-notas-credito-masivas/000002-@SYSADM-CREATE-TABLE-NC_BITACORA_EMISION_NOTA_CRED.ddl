-- Create table
create table NC_BITACORA_EMISION_NOTA_CRED
(
  id_proceso       NUMBER,
  id_requerimiento NUMBER,
  transaccion      VARCHAR2(500),
  fecha_inicio     DATE,
  fecha_fin        DATE,
  estado           VARCHAR2(2),
  observacion      VARCHAR2(500),
  usuario          VARCHAR2(30),
  observacion2     VARCHAR2(30)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  )
nologging;
-- Add comments to the table 
comment on table NC_BITACORA_EMISION_NOTA_CRED
  is 'Tabla de bitacora Mejoras en el Proceso Emision Notas Credito';
-- Add comments to the columns 
comment on column NC_BITACORA_EMISION_NOTA_CRED.id_proceso
  is 'El id_proceso - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.id_requerimiento
  is 'El id_requerimiento - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.transaccion
  is 'El nombre de las fases - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.fecha_inicio
  is 'La fecha inicio - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.fecha_fin
  is 'La fecha fin - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.estado
  is 'Estado del proceso F= Finalizado con error E=ERROR P=Procesando Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.observacion
  is 'Observacion en caso de Error - Mejoras en el Proceso Emision Notas Credito';
comment on column NC_BITACORA_EMISION_NOTA_CRED.usuario
  is 'Observacion de los registros cargados con error';
