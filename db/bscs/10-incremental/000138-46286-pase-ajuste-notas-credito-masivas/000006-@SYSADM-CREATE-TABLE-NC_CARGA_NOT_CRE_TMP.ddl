-- Create table
create table NC_CARGA_NOT_CRE_TMP
(
  id_requerimiento NUMBER,
  amount           VARCHAR2(250),
  sncode           NUMBER,
  status           VARCHAR2(1),
  error            VARCHAR2(200),
  hilo             INTEGER,
  customer_id      INTEGER,
  custcode         VARCHAR2(24),
  observacion      VARCHAR2(100)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PK_INX_ID_CUST_ID on NC_CARGA_NOT_CRE_TMP (CUSTOMER_ID)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index PK_INX_ID_REQ on NC_CARGA_NOT_CRE_TMP (ID_REQUERIMIENTO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
