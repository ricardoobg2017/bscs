-- Create table
create table NC_CARGA_NOTA_CREDITO
(
  id_ejecucion    NUMBER,
  usuario         VARCHAR2(20),
  fecha_inicio    DATE,
  fecha_fin       DATE,
  estado          VARCHAR2(2),
  reg_procesados  NUMBER,
  id_notificacion NUMBER,
  tiempo_notif    NUMBER,
  cant_hilos      NUMBER,
  cant_reg_mem    NUMBER,
  recurrencia     VARCHAR2(1),
  remark          VARCHAR2(2000),
  entdate         DATE,
  respaldar       VARCHAR2(1),
  nombre_respaldo VARCHAR2(80),
  observacion     VARCHAR2(600)
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on NC_CARGA_NOTA_CREDITO to EIS_DAT;
grant select on NC_CARGA_NOTA_CREDITO to EIS_MIR;
grant select on NC_CARGA_NOTA_CREDITO to EIS_PRI;
