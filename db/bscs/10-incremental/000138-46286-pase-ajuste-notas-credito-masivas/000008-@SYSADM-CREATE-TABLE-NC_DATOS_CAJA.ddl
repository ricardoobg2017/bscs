-- Create table
create table NC_DATOS_CAJA
(
  id_requerimiento    NUMBER,
  cuenta              VARCHAR2(30),
  fecha_credito       DATE,
  nombre_cliente      VARCHAR2(200),
  direccion_cliente   VARCHAR2(200),
  descripcion_credito VARCHAR2(300),
  factura             VARCHAR2(100),
  id_cliente          VARCHAR2(20),
  fecha_factura       DATE,
  codigo_carga        VARCHAR2(20),
  ciudad              VARCHAR2(100),
  producto_caja       VARCHAR2(100),
  valor_con_impuesto  NUMBER,
  valor_sin_impuesto  NUMBER,
  iva                 NUMBER,
  ice                 NUMBER
)
tablespace DBX_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_NC_CAJA on NC_DATOS_CAJA (ID_REQUERIMIENTO)
  tablespace DBX_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
