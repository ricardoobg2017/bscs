-- Modificacion de la Vista RELOJ.TTC_VIEW_CUSTOMERS_D (9833) - 10/09/2014
CREATE OR REPLACE VIEW RELOJ.TTC_VIEW_CUSTOMERS_D AS
SELECT /*+ rule +*/
            cp.customer_id,
            cp.co_id
 FROM   reloj.ttc_contractplannig  cp,
             sysadm.curr_co_status cur
WHERE cur.co_id=cp.co_id  --'d': estado de Inactividad
     AND cur.ch_status='d'
UNION ALL
select /*+ rule +*/
 ca.customer_id, ca.co_id
  FROM reloj.ttc_contractplannig cp, reloj.ttc_contract_all ca
 WHERE ca.ttc_insertiondate =
       (SELECT max(ca2.ttc_insertiondate)
          FROM reloj.ttc_contract_all ca2
         WHERE ca.co_id = ca2.co_id)
   and ca.co_id = cp.co_id
   AND ca.co_ext_csuin = '5';
