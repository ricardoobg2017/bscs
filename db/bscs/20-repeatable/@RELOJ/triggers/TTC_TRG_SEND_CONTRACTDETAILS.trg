CREATE OR REPLACE TRIGGER TTC_TRG_SEND_CONTRACTDETAILS
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      IRO JUAN ROMERO 
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       10324 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          20/05/2015
  PROPOSITO:      Se modifica el TRIGGER para que considere los CO_STATUSTRX ('L','A') y el nuevo estado 
                  'D'.
  *********************************************************************************************************/
    AFTER INSERT
     ON reloj.TTC_PROCESS_CONTRACTHISTORY_S
     FOR EACH ROW
 
when (NEW.CO_STATUSTRX = 'F' OR NEW.CO_STATUSTRX = 'R' Or NEW.CO_STATUSTRX = 'E' OR NEW.CO_STATUSTRX = 'D' OR NEW.CO_STATUSTRX = 'L' OR NEW.CO_STATUSTRX = 'A')
BEGIN
   IF :NEW.CO_STATUSTRX = 'F' THEN
         --Actualiza el ultimo estado en la cabecera
         UPDATE reloj.ttc_contractplannig_s cpl
                  SET cpl.lastmoddate =:NEW.LASTMODDATE,
                           cpl.co_period=:NEW.CO_PERIOD,
                           cpl.co_status_a=:NEW.CO_STATUS_A,
                           cpl.ch_status_b=:NEW.CH_STATUS_B,
                           cpl.co_reason=:NEW.CO_REASON,
                           cpl.ch_validfrom=:NEW.LASTMODDATE
           WHERE cpl.customer_id = :NEW.CUSTOMER_ID
                  AND cpl.co_id              = :NEW.CO_ID;

         --Actualiza el estado procesado en la tabla de detalles
         UPDATE reloj.TTC_CONTRACTPLANNIGDETAILS_S CDT
                  SET CDT.Co_Statustrx =:NEW.CO_STATUSTRX,
                      CDT.LASTMODDATE  =:NEW.LASTMODDATE,
                    --  CDT.ORDERBYTRX= :NEW.ORDERBYTRX,
                      CDT.CO_REMARKTRX =:NEW.CO_REMARKTRX
           WHERE CDT.CUSTOMER_ID = :NEW.CUSTOMER_ID
                  AND CDT.CO_ID  =  :NEW.CO_ID
                  AND CDT.ST_SEQNO = :NEW.ST_SEQNO;

   ELSIF (:NEW.CO_STATUSTRX = 'R' Or :NEW.CO_STATUSTRX = 'E') THEN
         --Actualiza el estado procesado en la tabla de detalles
         UPDATE reloj.TTC_CONTRACTPLANNIGDETAILS CDT
                  SET CDT.Co_Statustrx ='E',
                      CDT.LASTMODDATE  =:NEW.LASTMODDATE,
                      CDT.CO_REMARKTRX =:NEW.CO_REMARKTRX,
                      CDT.CO_NOINTENTO =:NEW.CO_NOINTENTO
           WHERE CDT.CUSTOMER_ID = :NEW.CUSTOMER_ID
                  AND CDT.CO_ID  =  :NEW.CO_ID
                  AND CDT.ST_SEQNO  = :NEW.ST_SEQNO;

   ELSIF (:NEW.CO_STATUSTRX = 'D' OR :NEW.CO_STATUSTRX = 'L' OR :NEW.CO_STATUSTRX = 'A') THEN
         --Actualiza el estado correspondiente en la tabla de detalles
         UPDATE reloj.TTC_CONTRACTPLANNIGDETAILS CDT
                  SET CDT.Co_Statustrx =:NEW.CO_STATUSTRX,
                      CDT.LASTMODDATE  =:NEW.LASTMODDATE,
                      CDT.CO_REMARKTRX =:NEW.CO_REMARKTRX,
                      CDT.CO_NOINTENTO =:NEW.CO_NOINTENTO
           WHERE CDT.CUSTOMER_ID = :NEW.CUSTOMER_ID
                  AND CDT.CO_ID  = :NEW.CO_ID
                  AND CDT.ST_SEQNO = :NEW.ST_SEQNO;
   END IF;
   
  --[8649]- IRO JJ 
  exception
    when others then
      rc_bitacoriza_trigger_scp('TTC_TRG_SEND_CONTRACTDETAILS',
                                'TTC_TRG_SEND_CONTRACTDETAILS',
                                'Error al actualizar co_statustrx en la tabla TTC_CONTRACTPLANNIG y TTC_CONTRACTPLANNIGDETAILS',
                                substr(sqlerrm,1,500),
                                :NEW.customer_id,
                                :NEW.co_id);
  --[8649]- IRO JJ
 END TTC_TRG_SEND_CONTRACTDETAILS;
/
