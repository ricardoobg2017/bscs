create or replace procedure RC_TRX_UPDATE_PLANNING(PV_FECHA_INICIO IN VARCHAR2,
                                                         PV_FECHA_FIN    IN VARCHAR2,
                                                         PV_ERROR        OUT VARCHAR2) is

  /*******************************************************************************************************
    LIDER SIS:      KARLA AVENDANO
    LIDER IRO:      IRO LAURA CAMPOVERDE 
    CREADO POR:     IRO JOHANNA JIMENEZ 
    PROYECTO:       10036 FACTURACION COMPLETA DE CLIENTES SUSPENDIDOS
    FECHA:          13/02/2015
    Proosito:       Actualizar en la tabla ttc_contractplannigdetails la fecha de ejecucion de las 
                    suspensiones futuras para los servicios insertados en la tabla 
                    rc_suspendidos_fin_ciclo de axis
  ********************************************************************************************************/
  --=====================================================================================--
  -- MODIFICADO POR: Jordan Rodr�guez.
  -- FECHA MOD:      23/02/2016
  -- PROYECTO:       [10695] Mejoras al proceso de Reloj de Cobranzas
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Suspension en Bscs a fecha de fin de ciclo de cuentas con mal score
  --=====================================================================================-- 
  --=====================================================================================--
  -- MODIFICADO POR: Jordan Rodr�guez.
  -- FECHA MOD:      19/04/2016
  -- PROYECTO:       [10695] Mejoras al proceso de Reloj de Cobranzas
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Se modifico el cursor C_REG_PLANNING_X para que tome todas las cuentas
  --                 que se encuentran en la planificacion con CO_STATUSTRX diferente de (D,A)
  --=====================================================================================-- 
/*********************************************************************************************************
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Modificado por: Iro Anabell Amayquema
  Fecha         : 23/11/2017
  Proyecto      : 11603 Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  Motivo        : Cambiar la planificacion del reloj de tal manera que las inactivaciones de los 
  		  servicios (status 35) sean planificadas para el �ltimo d�a del mes que corresponda
  *********************************************************************************************************/
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      05/06/2018
-- PROYECTO:       [11922] Equipo �gil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Ajuste de Reloj para replanificacion de clientes con status de suspension incorrecto (34 u 80)
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      30/07/2018
-- PROYECTO:       [12018] Equipo �gil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Notificaciones del proceso de inactivacion de reloj y cambio en la fecha de inactivacion.
--=====================================================================================--

  type T_SERVICIO IS RECORD(
    ID_SECUENCIA NUMBER,
    CUSTOMER_ID  NUMBER,
    CO_ID        NUMBER,
    CSUIN        VARCHAR2(4),
    FECHA_SUSP   DATE,
    TIPO_SCORE    VARCHAR2(3));--10695 IRO JRO

  TYPE T_SERVICIOS IS TABLE OF T_SERVICIO;
  TYPE T_REGISTROS IS TABLE OF RC_SUSP_FIN_CICLO_T;

  lt_servicios T_SERVICIOS;
  lt_registros T_REGISTROS;

  CURSOR C_SUSP_FIN_CICLO(CD_FECHA_INICIO DATE, CD_FECHA_FIN DATE) IS
    SELECT RC_SUSP_FIN_CICLO_T(A.CUENTA,
                               A.CUSTOMER_ID,
                               A.ID_SERVICIO,
                               A.CO_ID,
                               A.STATUS_SUSP_BSCS,
                               A.CED_IDENTIDAD,
                               A.CICLO,
                               A.BP_PLAN,
                               A.DETALLE_PLAN,
                               A.DEUDA,
                               A.CALIFICACION,
                               A.TIPO_CLIENTE,
                               A.FORMA_PAGO,
                               A.CATEGORIA,
                               A.TIPO_APROBACION,
                               A.TIPO_CUENTA,
                               A.ID_FINANCIERA,
                               A.COMPA�IA,
                               A.STATUS_LINEA_AXIS,
                               A.FECHA_SUSPENSION,
                               A.BSCS_POSTERGADO)--10695 IRO JRO
      FROM PORTA.RC_SUSPENDIDOS_FIN_CICLO@AXIS A
     WHERE A.FECHA_INGRESO >= CD_FECHA_INICIO
       AND A.FECHA_INGRESO <= CD_FECHA_FIN
       AND NOT EXISTS (SELECT CO_ID
                        FROM RC_SUSPENDIDOS_FIN_CICLO B
                       WHERE B.CUSTOMER_ID = A.CUSTOMER_ID
                         AND B.CO_ID = A.CO_ID
                         AND B.STATUS_SUSP_BSCS = A.STATUS_SUSP_BSCS
                         AND B.FECHA_INGRESO >= A.FECHA_INGRESO
                         AND (B.ESTADO = 'P' OR B.ESTADO = 'R' OR B.ESTADO = 'NE'));

  CURSOR C_TOTAL_SERVICIOS IS
    SELECT COUNT(CO_ID)
      FROM RC_SUSPENDIDOS_FIN_CICLO
     WHERE FECHA_SUSPENSION > TRUNC(SYSDATE)
       AND ESTADO = 'P';

  CURSOR C_SERVICIOS IS
    SELECT ID_SECUENCIA,
           CUSTOMER_ID,
           CO_ID,
           STATUS_SUSP_BSCS,
           FECHA_SUSPENSION,
           BSCS_POSTERGADO
      FROM RC_SUSPENDIDOS_FIN_CICLO 
     WHERE FECHA_SUSPENSION > TRUNC(SYSDATE)
       AND ESTADO = 'P';

  CURSOR C_REGS_PLANNING(CN_CUSTOMER_ID NUMBER,
                         CN_CO_ID       NUMBER,
                         CN_ORDEN       NUMBER) IS
    SELECT A.PK_ID, A.ST_ID, A.ST_SEQNO, A.CO_TIMEPERIOD, A.CO_STATUS_A
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE CUSTOMER_ID = CN_CUSTOMER_ID
       AND CO_ID = CN_CO_ID
       AND RE_ORDEN >= CN_ORDEN
     ORDER BY RE_ORDEN ASC;

  CURSOR C_REG_PLANNING(CN_CUSTOMER_ID NUMBER,
                        CN_CO_ID       NUMBER,
                        CV_CSUIN       VARCHAR2) IS
    SELECT A.RE_ORDEN
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE CUSTOMER_ID = CN_CUSTOMER_ID
       AND CO_ID = CN_CO_ID
       AND CO_PERIOD = CV_CSUIN
       AND CO_STATUSTRX NOT IN ('F', 'E');

  CURSOR C_REG_PLANNING_X(CN_CUSTOMER_ID NUMBER,
                        CN_CO_ID       NUMBER,
                        CV_CSUIN       VARCHAR2) IS
    SELECT A.RE_ORDEN
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE CUSTOMER_ID = CN_CUSTOMER_ID
       AND CO_ID = CN_CO_ID
       AND CO_PERIOD = CV_CSUIN
       AND CO_STATUSTRX NOT IN ('D', 'A');
  --INICIO [11922] 31/05/2018 IRO ABA
    CURSOR C_REG_PLANNING2(CN_CUSTOMER_ID NUMBER,
                           CN_CO_ID       NUMBER) IS
    SELECT A.RE_ORDEN
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE CUSTOMER_ID = CN_CUSTOMER_ID
       AND CO_ID = CN_CO_ID
       AND CO_PERIOD IN ('2','7')
       AND CO_STATUSTRX NOT IN ('F', 'E');

  CURSOR C_REG_PLANNING_X2(CN_CUSTOMER_ID NUMBER,
                           CN_CO_ID       NUMBER) IS
  SELECT A.RE_ORDEN
    FROM TTC_CONTRACTPLANNIGDETAILS A
   WHERE CUSTOMER_ID = CN_CUSTOMER_ID
     AND CO_ID = CN_CO_ID
     AND CO_PERIOD IN ('2','7')
     AND CO_STATUSTRX NOT IN ('D', 'A');

  CURSOR c_parametro(cn_id_tipo_parametro NUMBER,
                     cv_id_parametro      VARCHAR2) IS
      SELECT nvl(valor,'N')
        FROM sysadm.gv_parametros
       WHERE id_tipo_parametro = cn_id_tipo_parametro
         AND id_parametro = cv_id_parametro;
  --FIN [11922] 31/05/2018 IRO ABA

  lv_error       VARCHAR2(2000);
  ln_total       NUMBER;
  ln_count       NUMBER := 0;
  ln_customer_id NUMBER;
  ln_co_id       NUMBER;
  lv_csuin       VARCHAR2(5);
  ld_fecha_susp  DATE;
  lv_tipo_score  VARCHAR2(3);--10695 IRO JRO 
  ln_found       NUMBER;
  ln_orden       NUMBER;
  ln_registro    NUMBER;
  ln_secuencia   NUMBER;
  ld_fecha_ini   DATE;
  ld_fecha_fin   DATE;
  ld_fecha_base  DATE;
  ld_fecha       DATE;
  ln_ejecucion   NUMBER;
  ln_max_ejecucion NUMBER;

  le_error EXCEPTION;
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  lv_valor_scp                Varchar2(4000) := '0';
  lv_descripcion_scp          Varchar2(500);
  LN_ID_BITACORA_SCP          NUMBER := 0;
  LN_TOTAL_REGISTROS_SCP      NUMBER := 0;
  LV_ID_PROCESO_SCP           VARCHAR2(100) := 'API_SCORE_RELOJ';
  LV_REFERENCIA_SCP           VARCHAR2(200) := 'RC_TRX_UPDATE_PLANNING';
  LV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'CO_ID';
  LN_ERROR_SCP                NUMBER := 0;
  LV_ERROR_SCP                VARCHAR2(500);
  LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
  LN_REGISTROS_ERROR_SCP      NUMBER := 0;
  LV_MENSAJE_APL_SCP          VARCHAR2(4000);
  LV_MENSAJE_TEC_SCP          VARCHAR2(4000);
  --LV_MENSAJE_ACC_SCP          VARCHAR2(4000);
  LN_NIVEL_ERROR_SCP          NUMBER := 0;
  LN_DIA_FIN_MES              NUMBER:=0;
  LD_FECHA_FIN_MES            date;
  ---------------------------------------------------------------
  Lv_BanderaEstSusp           VARCHAR2(1);
  --INI [12018] IRO ABA 
  CURSOR C_PARAMETROS(Cn_IdTipoParametro NUMBER, Cv_IdParametro VARCHAR2) IS
  SELECT G.VALOR
    FROM SYSADM.GV_PARAMETROS G
   WHERE G.ID_TIPO_PARAMETRO = Cn_IdTipoParametro
     AND G.ID_PARAMETRO = Cv_IdParametro;
  
  CURSOR C_PKID_DIAS_INACT IS
  SELECT Y.PK_ID, Y.RE_ORDEN, Y.ST_DURATION_TOTAL
    FROM RELOJ.TTC_VIEW_PLANNIGSTATUS_IN Y
   WHERE Y.RE_ORDEN>2;
  
  TYPE T_INACTIVACION_RELOJ IS RECORD(
    PK_ID              NUMBER,
    RE_ORDEN           NUMBER,
    ST_DURATION_TOTAL  NUMBER);
  
  TYPE T_DIAS_INACT IS TABLE OF T_INACTIVACION_RELOJ;
  Lt_DiasInact           T_DIAS_INACT;
  Ln_DiasInactivarServ   NUMBER;
  Ld_FechaInacTemp       DATE;
  --FIN [12018] IRO ABA 

begin

  --Consulto valor de bandera para validacion de score
  scp.sck_api.scp_parametros_procesos_lee('MAX_EJECUCION_UPD_PLANNING',
                                          lv_id_proceso_scp,
                                          lv_valor_scp,
                                          lv_descripcion_scp,
                                          ln_error_scp,
                                          lv_error_scp);

  ln_max_ejecucion := to_number(nvl(lv_valor_scp, 3));

  --------------------------------SCP - mensaje ---------------------------------
  ln_total_registros_scp := NVL(LN_TOTAL, 0);
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        ln_total_registros_scp,
                                        lv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  IF ln_error_scp <> 0 THEN
    lv_error := 'Error al iniciar bitacora >> ' || lv_error_scp;
    RAISE le_error;
  ELSE
    lv_mensaje_apl_scp := 'Inicio Extraccion de Informacion de AXIS';
    lv_mensaje_tec_scp := '   Parametros de Entrada:';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          NULL,
                                          NULL,
                                          0,
                                          NULL,
                                          PV_FECHA_INICIO,
                                          PV_FECHA_FIN,
                                          NULL,
                                          NULL,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
  END IF;
  -------------------------------------------------------------------------------
  ld_fecha_ini := to_Date(PV_FECHA_INICIO || ' 00:00:00',
                          'dd/mm/rrrr hh24:mi:ss');
  ld_fecha_fin := to_Date(PV_FECHA_FIN || ' 23:59:59',
                          'dd/mm/rrrr hh24:mi:ss');

  --Se extrae de AXIS los servicios a los cuales se les debe actualizar su planificacion
  OPEN C_SUSP_FIN_CICLO(ld_fecha_ini, ld_fecha_fin);
  LOOP
    FETCH C_SUSP_FIN_CICLO BULK COLLECT
      INTO lt_registros LIMIT 5000;
  
    FOR rc IN 1 .. lt_registros.COUNT LOOP
      BEGIN
 
        ln_count := ln_count + 1;
        
        --Inserto servicios a los cuales se les actualizara la planificacion con estado P (PENDIENTE)      
        INSERT INTO RC_SUSPENDIDOS_FIN_CICLO
          (ID_SECUENCIA,
           CUENTA,
           CUSTOMER_ID,
           ID_SERVICIO,
           CO_ID,
           STATUS_SUSP_BSCS,
           CED_IDENTIDAD,
           CICLO,
           BP_PLAN,
           DETALLE_PLAN,
           DEUDA,
           CALIFICACION,
           TIPO_CLIENTE,
           FORMA_PAGO,
           CATEGORIA,
           TIPO_APROBACION,
           TIPO_CUENTA,
           ID_FINANCIERA,
           COMPA�IA,
           STATUS_LINEA_AXIS,
           FECHA_SUSPENSION,
           FECHA_INGRESO,
           ESTADO,
           BSCS_POSTERGADO)--10695 IRO JRO
        VALUES
          (SCORE_CLIENTE_SEQ.nextval,
           lt_registros(rc).CUENTA,
           lt_registros(rc).CUSTOMER_ID,
           lt_registros(rc).ID_SERVICIO,
           lt_registros(rc).CO_ID,
           lt_registros(rc).STATUS_SUSP_BSCS,
           lt_registros(rc).CED_IDENTIDAD,
           lt_registros(rc).CICLO,
           lt_registros(rc).BP_PLAN,
           lt_registros(rc).DETALLE_PLAN,
           lt_registros(rc).DEUDA,
           lt_registros(rc).CALIFICACION,
           lt_registros(rc).TIPO_CLIENTE,
           lt_registros(rc).FORMA_PAGO,
           lt_registros(rc).CATEGORIA,
           lt_registros(rc).TIPO_APROBACION,
           lt_registros(rc).TIPO_CUENTA,
           lt_registros(rc).ID_FINANCIERA,
           lt_registros(rc).COMPA�IA,
           lt_registros(rc).STATUS_LINEA_AXIS,
           lt_registros(rc).FECHA_SUSPENSION,
           SYSDATE,
           'P',
           lt_registros(rc).BSCS_POSTERGADO);--10695 IRO JRO
      
        ln_registros_procesados_scp := ln_registros_procesados_scp + 1;
      
        IF ln_count >= 500 THEN
          COMMIT;
          ln_count := 0;
        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
         
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp := ln_registros_error_scp + 1;
          lv_mensaje_apl_scp     := '   Error en Extraccion';
          lv_mensaje_tec_scp     := SUBSTR(SQLERRM, 1, 500);
          ln_nivel_error_scp     := 3;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                NULL,
                                                ln_nivel_error_scp,
                                                lt_registros(rc)
                                                .STATUS_SUSP_BSCS,
                                                to_char(lt_registros(rc)
                                                        .FECHA_SUSPENSION,
                                                        'dd/mm/rrrr'),
                                                lt_registros(rc).CUSTOMER_ID,
                                                lt_registros(rc).CO_ID,
                                                NULL,
                                                'S',
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------------
          NULL;
      END;
    END LOOP;
  
    COMMIT;
  
    EXIT WHEN C_SUSP_FIN_CICLO%NOTFOUND;
  END LOOP;
  CLOSE C_SUSP_FIN_CICLO;

  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := '   Total Registros Extraidos:';
  ln_nivel_error_scp := 0;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        NULL,
                                        NULL,
                                        ln_nivel_error_scp,
                                        NULL,
                                        'Exito',
                                        'Error',
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        'S',
                                        ln_error_scp,
                                        lv_error_scp);

  lv_mensaje_apl_scp := 'Fin Extraccion de Informacion de AXIS';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        NULL,
                                        NULL,
                                        0,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  -------------------------------------------------------------------------------

  COMMIT;

  --Obtengo el total de co_id a procesar
  OPEN C_TOTAL_SERVICIOS;
  FETCH C_TOTAL_SERVICIOS
    INTO LN_TOTAL;
  CLOSE C_TOTAL_SERVICIOS;

  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'Inicio Actualizacion de la Planificacion';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        NULL,
                                        NULL,
                                        0,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  -------------------------------------------------------------------------------

  ln_ejecucion := 0;
  
  --Verifico si existen registros para ser procesados y si aun no se cumple el maximo de intentos
  WHILE (LN_TOTAL > 0 AND ln_ejecucion <= ln_max_ejecucion) LOOP
  
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje
    ----------------------------------------------------------------------                                      
    lv_mensaje_apl_scp := '   Registos por procesar:';
    ln_nivel_error_scp := 0;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          NULL,
                                          NULL,
                                          ln_nivel_error_scp,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL,
                                          NULL,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    -------------------------------------------------------------------------------
  
    ln_count                    := 0;
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
    ln_ejecucion                := ln_ejecucion + 1;
    --INICIO [11922] 31/05/2018 IRO ABA
    OPEN c_parametro(11922, 'BAND_CSUIN_SUSPENSION');
    FETCH c_parametro
      INTO Lv_BanderaEstSusp;
    CLOSE c_parametro;
    --INI [12018] IRO ABA 
    OPEN C_PARAMETROS(12018, 'DIAS_INACT_RELOJ_MOVIL');
    FETCH C_PARAMETROS INTO Ln_DiasInactivarServ;
    CLOSE C_PARAMETROS;
    
    OPEN C_PKID_DIAS_INACT;
    FETCH C_PKID_DIAS_INACT BULK COLLECT INTO Lt_DiasInact;
    CLOSE C_PKID_DIAS_INACT;
    --FIN [12018] IRO ABA 
    OPEN C_SERVICIOS;
    LOOP
      FETCH C_SERVICIOS BULK COLLECT
        INTO lt_servicios LIMIT 5000;
    
      FOR i IN 1 .. lt_servicios.count LOOP
        BEGIN
          SAVEPOINT A;
          ln_count := ln_count + 1;
        
          ln_secuencia   := lt_servicios(i).ID_SECUENCIA;
          ln_customer_id := lt_servicios(i).CUSTOMER_ID;
          ln_co_id       := lt_servicios(i).CO_ID;
          lv_csuin       := lt_servicios(i).CSUIN;
          lv_tipo_score   := nvl(lt_servicios(i).TIPO_SCORE,'N');
          ld_fecha_susp  := lt_servicios(i).FECHA_SUSP;
          ln_found       :=0;--10695 IRO JRO
          IF NVL(Lv_BanderaEstSusp,'N') = 'S' THEN
             IF lv_tipo_score='X' THEN
                OPEN C_REG_PLANNING_X2(ln_customer_id, ln_co_id);
                FETCH C_REG_PLANNING_X2  INTO ln_orden;
                ln_found := C_REG_PLANNING_X2%ROWCOUNT;
                CLOSE C_REG_PLANNING_X2;
             ELSE
                OPEN C_REG_PLANNING2(ln_customer_id, ln_co_id);
                FETCH C_REG_PLANNING2  INTO ln_orden;
                ln_found := C_REG_PLANNING2%ROWCOUNT;
                CLOSE C_REG_PLANNING2;
             END IF;
          ELSE
          --INICIO 10695 IRO JRO
          IF lv_tipo_score='X' then
          OPEN C_REG_PLANNING_X(ln_customer_id, ln_co_id, lv_csuin);
          FETCH C_REG_PLANNING_X  INTO ln_orden;
          ln_found := C_REG_PLANNING_X%ROWCOUNT;
          CLOSE C_REG_PLANNING_X;
          else
          OPEN C_REG_PLANNING(ln_customer_id, ln_co_id, lv_csuin);
          FETCH C_REG_PLANNING  INTO ln_orden;
          ln_found := C_REG_PLANNING%ROWCOUNT;
          CLOSE C_REG_PLANNING;
          end if;
          --FIN 10695 IRO JRO
          END IF;
          ln_registro := 0;
        
          IF ln_found > 0 THEN
            --Obtengo planifacion del co_id en ttc_contractplannigdetails
            FOR j IN C_REGS_PLANNING(ln_customer_id, ln_co_id, ln_orden) LOOP
              Ln_dia_fin_mes:=0;
              ld_fecha_fin_mes:=null;
              ln_registro := ln_registro + 1;
              ld_fecha    := to_date(to_char(SYSDATE,
                                             'DD/MM/RRRR HH24:MI:SS'),
                                     'DD/MM/RRRR HH24:MI:SS');
              IF ln_registro = 1 THEN
                ld_fecha_base := ld_fecha_susp;
                EXECUTE IMMEDIATE 'UPDATE TTC_CONTRACTPLANNIGDETAILS A SET CH_TIMEACCIONDATE=:1,CO_STATUSTRX=''P'',LASTMODDATE=:2' ||
                                  ' WHERE CO_ID=:3 AND A.PK_ID=:4 AND A.ST_ID=:5 AND A.ST_SEQNO=:6'
                  USING ld_fecha_base, ld_fecha, ln_co_id, j.pk_id, j.st_id, j.st_seqno;
              ELSE
                

                --11603 INI AAM
                if j.co_status_a = '35' THEN
                   FOR X IN Lt_DiasInact.FIRST .. Lt_DiasInact.LAST LOOP
                       IF J.PK_ID = Lt_DiasInact(X).PK_ID THEN
                          Ld_FechaInacTemp := ld_fecha_base + Lt_DiasInact(X).ST_DURATION_TOTAL;
                       END IF;
                   END LOOP;
                   ld_fecha_fin_mes:=last_day(Ld_FechaInacTemp)- NVL(Ln_DiasInactivarServ,2);
                   Ln_dia_fin_mes:= ld_fecha_fin_mes - ld_fecha_base;
                   ld_fecha_base:=ld_fecha_fin_mes;
                   EXECUTE IMMEDIATE 'UPDATE TTC_CONTRACTPLANNIGDETAILS A SET CH_TIMEACCIONDATE=:1,LASTMODDATE=:2, ' ||
                                  ' A.CO_TIMEPERIOD= :3 WHERE CO_ID=:4 AND A.PK_ID=:5 AND A.ST_ID=:6 AND A.ST_SEQNO=:7'
                  USING ld_fecha_base, ld_fecha, Ln_dia_fin_mes, ln_co_id, j.pk_id, j.st_id, j.st_seqno;
                   
                ELSE
                
                --11603 FIN AAM
                ld_fecha_base := ld_fecha_base + j.co_timeperiod;
                EXECUTE IMMEDIATE 'UPDATE TTC_CONTRACTPLANNIGDETAILS A SET CH_TIMEACCIONDATE=:1,LASTMODDATE=:2, ' ||
                                  ' A.CO_TIMEPERIOD=A.CO_TIMEPERIOD + :3 WHERE CO_ID=:4 AND A.PK_ID=:5 AND A.ST_ID=:6 AND A.ST_SEQNO=:7'
                  USING ld_fecha_base, ld_fecha, Ln_dia_fin_mes, ln_co_id, j.pk_id, j.st_id, j.st_seqno;
                End if;
              END IF;
            
            END LOOP;
          
            EXECUTE IMMEDIATE 'UPDATE RC_SUSPENDIDOS_FIN_CICLO SET ESTADO=''R'' WHERE ID_SECUENCIA=:1'
              USING ln_secuencia;
          
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje
            ----------------------------------------------------------------------
            lv_mensaje_apl_scp := '   Registro Procesado con Exito';
            lv_mensaje_tec_scp := '';
            ln_nivel_error_scp := 0;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  NULL,
                                                  ln_nivel_error_scp,
                                                  lv_csuin,
                                                  to_char(ld_fecha_susp,
                                                          'dd/mm/rrrr'),
                                                  ln_customer_id,
                                                  ln_co_id,
                                                  NULL,
                                                  'S',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
          
          ELSE
            EXECUTE IMMEDIATE 'UPDATE RC_SUSPENDIDOS_FIN_CICLO SET ESTADO=''NE'' WHERE ID_SECUENCIA=:1'
              USING ln_secuencia;
          
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje
            ----------------------------------------------------------------------
            --ln_registros_error_scp:=ln_registros_error_scp+1;
            lv_mensaje_apl_scp := '   Registro No Encontrado';
            lv_mensaje_tec_scp := 'No se encontro planificado el co_id en la tabla TTC_CONTRACTPLANNNIGDETAILS o se encuentra con CO_STATUSTRX IN (F,E)';
            ln_nivel_error_scp := 0;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  NULL,
                                                  ln_nivel_error_scp,
                                                  lv_csuin,
                                                  to_char(ld_fecha_susp,
                                                          'dd/mm/rrrr'),
                                                  ln_customer_id,
                                                  ln_co_id,
                                                  NULL,
                                                  'S',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
          END IF;
        
          ln_registros_procesados_scp := ln_registros_procesados_scp + 1;
        
          IF ln_count >= 500 THEN
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                  ln_registros_procesados_scp,
                                                  ln_registros_error_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
            COMMIT;
            ln_count := 0;
          
          END IF;
        
        EXCEPTION
          WHEN OTHERS THEN
            ROLLBACK TO A;
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '   Error en Procesamiento';
            lv_mensaje_tec_scp     := SUBSTR(SQLERRM, 1, 500);
            ln_nivel_error_scp     := 3;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  NULL,
                                                  ln_nivel_error_scp,
                                                  lv_csuin,
                                                  to_char(ld_fecha_susp,
                                                          'dd/mm/rrrr'),
                                                  ln_customer_id,
                                                  ln_co_id,
                                                  NULL,
                                                  'S',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
        END;
      END LOOP;
    
      COMMIT;
    
      EXIT WHEN C_SERVICIOS%NOTFOUND;
    END LOOP;
    CLOSE C_SERVICIOS;
  
    COMMIT;
  
    --Obtengo el total de co_id a procesar
    OPEN C_TOTAL_SERVICIOS;
    FETCH C_TOTAL_SERVICIOS
      INTO LN_TOTAL;
    CLOSE C_TOTAL_SERVICIOS;
  
  END LOOP;

  COMMIT;

  --------------------------------SCP - mensaje ---------------------------------

  lv_mensaje_apl_scp := 'Fin Actualizacion de la Planificacion';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        NULL,
                                        NULL,
                                        0,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);

  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);

  -------------------------------------------------------------------------------
  
  IF LN_TOTAL > 0 THEN
    PV_ERROR:='Error - Existen servicios a los cuales no se les ha actualizado la planificacion.'||
              ' Ejecutar de nuevo el proceso con los mismos parametros de entrada';
  END IF;

  commit;
  Begin
    dbms_session.close_database_link('AXIS');
  Exception
    When others Then
      null;
  End;
  commit;

exception
  when le_error then
    PV_ERROR := lv_error;
  when others then
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);
  
    commit;
    Begin
      dbms_session.close_database_link('AXIS');
    Exception
      When others Then
        null;
    End;
    commit;
end RC_TRX_UPDATE_PLANNING;
/
