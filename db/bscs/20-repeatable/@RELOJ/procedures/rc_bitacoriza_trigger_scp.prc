create or replace procedure reloj.rc_bitacoriza_trigger_scp(PV_ID_PROCESO   in VARCHAR2,
                                                      PV_REFERENCIA   in VARCHAR2,
                                                      PV_MENSAJE_APL  in VARCHAR2,
                                                      PV_MENSAJE_TEC  in VARCHAR2,
                                                      PN_CUSTOMERID   in NUMBER,
                                                      PN_COID         in NUMBER) is
PRAGMA AUTONOMOUS_TRANSACTION;

    --SCP:VARIABLES
    ------------------------------------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ------------------------------------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=1;
    lv_id_proceso_scp varchar2(100):=PV_ID_PROCESO;
    lv_referencia_scp varchar2(100):=PV_REFERENCIA;
    lv_unidad_registro_scp varchar2(30):='CO_ID';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_procesados_scp number:=1;
    ln_registros_error_scp number:=0;
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    lv_mensaje_acc_scp     varchar2(4000);
    ---------------------------------------------------------------
    
    Le_Error               EXCEPTION;
begin

   ----------------------------------------------------------------------------
   -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
   ----------------------------------------------------------------------------
   scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                         lv_referencia_scp,
                                         null,null,null,null,
                                         ln_total_registros_scp,
                                         lv_unidad_registro_scp,
                                         ln_id_bitacora_scp,
                                         ln_error_scp,
                                         lv_error_scp);
   if ln_error_scp <>0 then
      RAISE Le_Error;
   end if;
   ----------------------------------------------------------------------
   -- SCP: Código generado automáticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   lv_mensaje_apl_scp:=PV_MENSAJE_APL;
   lv_mensaje_tec_scp:=PV_MENSAJE_TEC;
   lv_mensaje_acc_scp:=null;
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                         lv_mensaje_apl_scp,
                                         lv_mensaje_tec_scp,
                                         lv_mensaje_acc_scp,
                                         3,
                                         PN_CUSTOMERID,
                                         PN_COID,
                                         Null,
                                         Null,
                                         Null,
                                         'S',
                                         ln_error_scp,
                                         lv_error_scp);
   if ln_error_scp <>0 then
      RAISE Le_Error;
   end if;
   ---------------------------------------------------------------------------------
   -- SCP: Código generado automáticamente. Registro de Actualizacion y Finalizacion
   ---------------------------------------------------------------------------------
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                         ln_registros_procesados_scp,
                                         ln_registros_error_scp,
                                         ln_error_scp,
                                         lv_error_scp);
                                                         
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                         ln_error_scp,
                                         lv_error_scp);
   
   commit;

exception
  when Le_Error then
    ROLLBACK;

  when others then
    ROLLBACK;
    
end rc_bitacoriza_trigger_scp;
/
