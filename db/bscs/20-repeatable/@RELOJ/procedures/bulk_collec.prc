create or replace procedure reloj.bulk_collec is

TYPE ttc_react IS REF CURSOR;
react ttc_react;
i_react    TTC_Customers_Count_React_tbk;

begin  

OPEN react FOR 'SELECT TTC_Customers_Count_React(Customer_id ,Custcode,bank_id) FROM ttc_view_cust_reactive_all where ROWNUM<= 10';
LOOP   
FETCH react BULK COLLECT INTO i_react LIMIT 5;
    FOR rc_in IN (select * from table(cast( i_react as TTC_Customers_Count_React_tbk)))  LOOP  
    BEGIN 
       insert into ttc_customers_check (CUSTOMER_ID, CUSTCODE, BANK_ID, INSERTIONDATE, STATUSTRX)
        values (rc_in.Customer_id, rc_in.Custcode, rc_in.bank_id, SYSDATE, 'V');
    EXCEPTION
      When Others Then
            Null;
    end;      
    end loop; 
     --***********
     COMMIT;
     --***********   
END LOOP;         
CLOSE react;
end bulk_collec;
/
