CREATE OR REPLACE PROCEDURE reloj.ADMIP_ENVIAR_CORREO_k(Pv_Servidor IN VARCHAR2,
                                Pv_De       IN VARCHAR2,
                                Pv_Para     IN VARCHAR2,
                                Pv_CC       IN VARCHAR2,
                                Pv_BCC      IN VARCHAR2,
                                Pv_Asunto   IN VARCHAR2,
                                Pv_Mensaje  IN VARCHAR2
                                --Pv_TipoCorreo    IN VARCHAR2 DEFAULT 'text/html',
                         --       Pv_CodigoError  OUT VARCHAR2,
                             --   Pv_MensajeError OUT VARCHAR2
                                ) IS

   Pv_Para        VARCHAR2(32767) := NULL;
   -- Pv_De          VARCHAR2(32767) := NULL;
  --  Pv_CC          VARCHAR2(32767) := NULL;
    Pv_BCC         VARCHAR2(32767) := NULL;
    Ln_Port        NUMBER := 25; /*  Numero de Puerto por el cual se envia correo */
    Lv_Cabecera    VARCHAR2(4000) := NULL;
    Lv_Conexion    UTL_SMTP.CONNECTION;
    --Lr_DatosCorreo ADMI_CORREOS%ROWTYPE := NULL;
    Lv_Crlf        VARCHAR2(2) := UTL_TCP.CRLF;

  BEGIN

    /* Abre Conexion */
    Lv_Conexion :=UTL_SMTP.OPEN_CONNECTION('130.2.18.61', Ln_Port);

    Lv_Cabecera := 'MIME-Version: 1.0' || Lv_Crlf || --
                   'Content-type: text/html' || Lv_Crlf || --
                   'From:' || Lv_De || Lv_Crlf || --
                   'Subject: ' || Pv_Asunto || Lv_Crlf || --
                   'To: ' || Lv_Para || Lv_Crlf || --
                   'CC: ' || Lv_CC || Lv_Crlf || --
                   'CCO: ' || Lv_BCC || Lv_Crlf;

    UTL_SMTP.HELO(Lv_Conexion, Pv_Servidor);

    /* Configura Envia y Recibe con UTL_STMP*/
--    WHILE (Lv_De IS NOT NULL) LOOP
      UTL_SMTP.MAIL(Lv_Conexion, Pv_De);
--    END LOOP;

--    WHILE (Lv_Para IS NOT NULL) LOOP
--      UTL_SMTP.RCPT(Lv_Conexion, Pv_Para);
      UTL_SMTP.RCPT(Lv_Conexion, 'kmite@cimaconsulting.com.ec');
      UTL_SMTP.RCPT(Lv_Conexion, 'kmite@cimaconsulting.com.ec');
--    END LOOP;

--    WHILE (Lv_CC IS NOT NULL) LOOP
      UTL_SMTP.RCPT(Lv_Conexion, 'kmite@cimaconsulting.com.ec');
--    END LOOP;

--    WHILE (Lv_BCC IS NOT NULL) LOOP
      UTL_SMTP.RCPT(Lv_Conexion, 'kmite@cimaconsulting.com.ec');
--    END LOOP;

    UTL_SMTP.DATA(Lv_Conexion, Lv_Cabecera || Pv_Mensaje);
    DBMS_OUTPUT.PUT_LINE('Envi� correo');

    /* Cierra Conexion */
    UTL_SMTP.QUIT(Lv_Conexion);
    --Lr_DatosCorreo.ESTADO := 'E';
    --ADMIK_TRX.ADMIP_INSERTA_CORREO(Lr_DatosCorreo, SUBSTR(SQLCODE || '-' || SQLERRM, 1, 4000), Pv_MensajeError, Pv_CodigoError);

  /*EXCEPTION
  WHEN OTHERS THEN
  NULL;
    /*WHEN UTL_SMTP.INVALID_OPERATION THEN
      Lr_DatosCorreo.ESTADO := 'N';
      ADMIK_TRX.ADMIP_INSERTA_CORREO(Lr_DatosCorreo, SUBSTR(SQLCODE || '-1-' || SQLERRM, 1, 4000), Pv_MensajeError, Pv_CodigoError);
      UTL_SMTP.QUIT(Lv_Conexion);
    WHEN UTL_SMTP.TRANSIENT_ERROR THEN
      Lr_DatosCorreo.ESTADO := 'N';
      ADMIK_TRX.ADMIP_INSERTA_CORREO(Lr_DatosCorreo, SUBSTR(SQLCODE || '-2-' || SQLERRM, 1, 4000), Pv_MensajeError, Pv_CodigoError);
      UTL_SMTP.QUIT(Lv_Conexion);
    WHEN UTL_SMTP.PERMANENT_ERROR THEN
      Lr_DatosCorreo.ESTADO := 'N';
      ADMIK_TRX.ADMIP_INSERTA_CORREO(Lr_DatosCorreo, SUBSTR(SQLCODE || '-3-' || SQLERRM, 1, 4000), Pv_MensajeError, Pv_CodigoError);
      UTL_SMTP.QUIT(Lv_Conexion);
    WHEN OTHERS THEN
      Lr_DatosCorreo.ESTADO := 'N';
      ADMIK_TRX.ADMIP_INSERTA_CORREO(Lr_DatosCorreo, SUBSTR(SQLCODE || '-4-' || SQLERRM, 1, 4000), Pv_MensajeError, Pv_CodigoError);
      UTL_SMTP.QUIT(Lv_Conexion);*/
  END ADMIP_ENVIAR_CORREO_k;
/
