create or replace procedure reloj.ADMIN_ENVIAR_CORREO_PRUEBA(Pv_Servidor IN VARCHAR2,
                                Pv_De       IN VARCHAR2,
                                Pv_Para     IN VARCHAR2,
                                Pv_CC       IN VARCHAR2,
                                --Pv_BCC      IN VARCHAR2,
                                Pv_Asunto   IN VARCHAR2,
                                Pv_Mensaje  IN VARCHAR2,
                                --Pv_TipoCorreo    IN VARCHAR2 DEFAULT 'text/html',
                                Pv_CodigoError  OUT VARCHAR2,
                                Pv_MensajeError OUT VARCHAR2) is


    --Lv_Para        VARCHAR2(32767) := NULL;
    Lv_De          VARCHAR2(32767) := NULL;
    --Lv_CC          VARCHAR2(32767) := NULL;
    Lv_BCC         VARCHAR2(32767) := NULL;
    Ln_Port        NUMBER := 25; /*  Numero de Puerto por el cual se envia correo */
    Lv_Cabecera    VARCHAR2(4000) := NULL;
    Lv_Conexion    UTL_SMTP.CONNECTION;
    --Lr_DatosCorreo ADMI_CORREOS%ROWTYPE := NULL;
    Lv_Crlf        VARCHAR2(2) := UTL_TCP.CRLF;

  BEGIN

    /* Abre Conexion */
    Lv_Conexion :=UTL_SMTP.OPEN_CONNECTION('130.2.18.61', Ln_Port);

    Lv_Cabecera := 'MIME-Version: 1.0' || Lv_Crlf || --
                   'Content-type: text/html' || Lv_Crlf || --
                   'From:' || Pv_De || Lv_Crlf || --
                   'Subject: ' || Pv_Asunto || Lv_Crlf || --
                   'To: ' || Pv_Para || Lv_Crlf || --
                   'CC: ' || Pv_CC || Lv_Crlf || --
                   'CCO: ' || Lv_BCC || Lv_Crlf;

    UTL_SMTP.HELO(Lv_Conexion, Pv_Servidor);
    Lv_De:='sistema_archivo@conecel.com';
      UTL_SMTP.MAIL(Lv_Conexion, Lv_De);

    DECLARE
      CADENA        VARCHAR2(500) := PV_PARA;
      MAIL          VARCHAR2(500);
      NUM_CADENA    NUMBER;
      INICIO_CADENA NUMBER := 1;
    BEGIN
      LOOP
        INICIO_CADENA := 1;
        SELECT INSTR(CADENA, ';') - 1 INTO NUM_CADENA FROM DUAL;
        IF NUM_CADENA != -1 THEN
          SELECT SUBSTR(CADENA, INICIO_CADENA, NUM_CADENA)
            INTO MAIL
            FROM DUAL;
        ELSE
          SELECT SUBSTR(CADENA, INICIO_CADENA, LENGTH(CADENA))
            INTO MAIL
            FROM DUAL;
        END IF;
        UTL_SMTP.RCPT(LV_CONEXION, MAIL);
        INICIO_CADENA := NUM_CADENA + 2;
        SELECT SUBSTR(CADENA, INICIO_CADENA, LENGTH(CADENA))
          INTO CADENA
          FROM DUAL;
        EXIT WHEN NUM_CADENA = -1;
      END LOOP;
    END;
    DECLARE
      CADENA        VARCHAR2(500) := PV_CC;
      MAIL          VARCHAR2(500);
      NUM_CADENA    NUMBER;
      INICIO_CADENA NUMBER := 1;
    BEGIN
      LOOP
        INICIO_CADENA := 1;
        SELECT INSTR(CADENA, ';') - 1 INTO NUM_CADENA FROM DUAL;
        IF NUM_CADENA != -1 THEN
          SELECT SUBSTR(CADENA, INICIO_CADENA, NUM_CADENA) INTO MAIL FROM DUAL;
        ELSE
          SELECT SUBSTR(CADENA, INICIO_CADENA, LENGTH(CADENA))
            INTO MAIL
            FROM DUAL;
        END IF;
        UTL_SMTP.RCPT(LV_CONEXION, MAIL);
      
        INICIO_CADENA := NUM_CADENA + 2;
        SELECT SUBSTR(CADENA, INICIO_CADENA, LENGTH(CADENA))
          INTO CADENA
          FROM DUAL;
        EXIT WHEN NUM_CADENA = -1;
      END LOOP;

    END;


    UTL_SMTP.DATA(Lv_Conexion, Lv_Cabecera || Pv_Mensaje);
    DBMS_OUTPUT.PUT_LINE('Envi� correo');

    /* Cierra Conexion */
    UTL_SMTP.QUIT(Lv_Conexion);

  EXCEPTION
  WHEN OTHERS THEN
    Pv_MensajeError:=SQLERRM;
    Pv_CodigoError:=SQLCODE;


  

  
end ADMIN_ENVIAR_CORREO_PRUEBA;
/
