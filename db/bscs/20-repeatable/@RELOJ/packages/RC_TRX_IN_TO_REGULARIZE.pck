CREATE OR REPLACE PACKAGE RC_TRX_IN_TO_REGULARIZE IS

      GN_PROCESO_IN   NUMBER:=11;
      GN_DESSHORT_IN VARCHAR2(15):='TTC_IN_TO_REGU';
      --[8693] JJI - INICIO
      GN_PROCESO_REGU_ESTAT  NUMBER := 28;
      GV_DESSHORT_REGU_ESTAT VARCHAR2(16) := 'TTC_REGU_ESTAT';
      GN_PROCESO_DTH  NUMBER := 29;
      GV_DESSHORT_DTH VARCHAR2(20) := 'TTC_IN_REGU_DTH';
      --[8693] JJI - FIN
      GN_DATE_MAX_TO_REGULARIZE  VARCHAR2(45):='TTC_DATE_MAX_TO_REGULARIZE';
      GN_DES_FLAG_IN_SUSP_X_OPER  VARCHAR2(45):='TTC_IN_FLAG_ANALISIS_CONTR_SUSP_X_OPER';
      GN_STATUS_F   VARCHAR2(1):='F';
       CURSOR C_SUSP_X_OPER_IN IS
         SELECT /*+ ALL_ROWS */ TTC_Contr_SuspXOper_In_type(
                          customer_id,
                          co_id,
                          co_period,
                          product_history_date,
                          re_orden,
                          co_userlastmod,
                          co_status_a,
                          ch_status_b,
                          co_reason
                          )
           FROM TTC_View_Contr_X_Oper_In; 

         CURSOR C_SUSP_X_OPER_IN2(CN_GRUPOHILO1 IN NUMBER, CN_GRUPOHILO2 IN NUMBER)IS
         SELECT /*+ ALL_ROWS */ TTC_Contr_SuspXOper_In_type(
                          customer_id,
                          co_id,
                          co_period,
                          product_history_date,
                          re_orden,
                          co_userlastmod,
                          co_status_a,
                          ch_status_b,
                          co_reason
                          )
           FROM TTC_View_Contr_X_Oper_In 
           where SUBSTR(CUSTOMER_ID,-1,1) between CN_GRUPOHILO1 AND CN_GRUPOHILO2;  

       CURSOR C_REGULARIZE_IN IS
          SELECT /*+ ALL_ROWS */ TTC_View_Customers_In_type(
                          customer_id, 
                          co_id, 
                          dn_num,
                          billcycle, 
                          custcode,
                          tmcode, 
                          prgcode, 
                          cstradecode, 
                          product_history_date,
                          CO_EXT_CSUIN, 
                          co_userlastmod,
                          ttc_insertiondate,
                          ttc_lastmoddate 
                          )
             FROM TTC_VIEW_CUST_TO_REGULARIZE_IN;             

         CURSOR C_REGULARIZE_IN2(CN_GRUPOHILO1 IN NUMBER, CN_GRUPOHILO2 IN NUMBER) IS
          SELECT /*+ ALL_ROWS */ TTC_View_Customers_In_type(
                          customer_id, 
                          co_id, 
                          dn_num,
                          billcycle, 
                          custcode,
                          tmcode, 
                          prgcode, 
                          cstradecode, 
                          product_history_date,
                          CO_EXT_CSUIN, 
                          co_userlastmod,
                          ttc_insertiondate,
                          ttc_lastmoddate 
                          )
          FROM TTC_VIEW_CUST_TO_REGULARIZE_IN
          where SUBSTR(CUSTOMER_ID,-1,1) between CN_GRUPOHILO1 AND CN_GRUPOHILO2;     
          
          CURSOR C_CANTIDAD_TOTAL_ALL(CN_GRUPOHILO1 IN NUMBER, CN_GRUPOHILO2 IN NUMBER) IS 
          SELECT /*+ ALL_ROWS */ COUNT(CUSTOMER_ID) FROM TTC_VIEW_CUST_TO_REGULARIZE_IN 
          where SUBSTR(CUSTOMER_ID,-1,1) between CN_GRUPOHILO1 AND CN_GRUPOHILO2;  
          
          CURSOR C_CANTIDAD_TOTAL IS 
          SELECT /*+ ALL_ROWS */ COUNT(CUSTOMER_ID) FROM TTC_VIEW_CUST_TO_REGULARIZE_IN;
          
          
          --[8693]JJI - INICIO
          CURSOR C_REGULARIZE_DTH IS
            SELECT /*+ ALL_ROWS */
             TTC_View_Customers_In_type(customer_id,
                                        co_id,
                                        dn_num,
                                        billcycle,
                                        custcode,
                                        tmcode,
                                        prgcode,
                                        cstradecode,
                                        product_history_date,
                                        CO_EXT_CSUIN,
                                        co_userlastmod,
                                        ttc_insertiondate,
                                        ttc_lastmoddate)
              FROM TTC_VIEW_CUST_TO_REGU_DTH_IN;

          CURSOR C_REGULARIZE_DTH_2(CN_GRUPOHILO1 IN NUMBER, CN_GRUPOHILO2 IN NUMBER) IS
            SELECT /*+ ALL_ROWS */
             TTC_View_Customers_In_type(customer_id,
                                        co_id,
                                        dn_num,
                                        billcycle,
                                        custcode,
                                        tmcode,
                                        prgcode,
                                        cstradecode,
                                        product_history_date,
                                        CO_EXT_CSUIN,
                                        co_userlastmod,
                                        ttc_insertiondate,
                                        ttc_lastmoddate)
              FROM TTC_VIEW_CUST_TO_REGU_DTH_IN
             where SUBSTR(CUSTOMER_ID, -1, 1) between CN_GRUPOHILO1 AND CN_GRUPOHILO2;
             
          CURSOR C_REG_TOTAL(CN_GRUPOHILO1 IN NUMBER,
                             CN_GRUPOHILO2 IN NUMBER) IS
            SELECT /*+ ALL_ROWS */COUNT(CUSTOMER_ID)
              FROM TTC_VIEW_CUST_TO_REGU_DTH_IN
             where SUBSTR(CUSTOMER_ID, -1, 1) between CN_GRUPOHILO1 AND CN_GRUPOHILO2;

          CURSOR C_REG_TOTAL_ALL IS
            SELECT /*+ ALL_ROWS */COUNT(CUSTOMER_ID)
              FROM TTC_VIEW_CUST_TO_REGU_DTH_IN;    
         --[8693]JJI - FIN
      
              
      PROCEDURE Prc_Header(PV_FILTRO IN VARCHAR2,
                           PV_BANDERA IN VARCHAR2,
                           PN_HILO IN NUMBER,
                           PN_GRUPOHILO IN VARCHAR2,
                           Pn_SosPid IN NUMBER,
                           PV_Error  OUT VARCHAR2);
                           
      PROCEDURE Prc_To_Regularize_Oper(/*PV_FILTRO IN VARCHAR2,PN_HILO IN NUMBER, PN_GRUPOHILO in varchar2, PV_BANDERA IN VARCHAR2,*/--8693 jji
                                       PV_Error OUT VARCHAR2);                                               
      PROCEDURE Prc_To_Regularize_All(PV_FILTRO IN VARCHAR2,PN_HILO IN NUMBER, PN_GRUPOHILO in varchar2, PV_BANDERA IN VARCHAR2,PV_Error OUT VARCHAR2);                                               
      PROCEDURE Prc_Insert_Programmer(PV_DESSHORT     IN VARCHAR2,--8693 JJI
                                      PN_PROCESS      IN NUMBER,--8693 JJI
                                      PV_ERROR   OUT VARCHAR2); 
                                      
      --8693 JJI - INICIO
      PROCEDURE Prc_Header_Dth(PV_BANDERA   IN VARCHAR2,
                              PN_HILO      IN NUMBER,
                              PN_GRUPOHILO IN VARCHAR2,
                              Pn_SosPid    IN NUMBER,
                              PV_Error     OUT VARCHAR2);
                              
      PROCEDURE Prc_Header_Regu_Estat(Pn_SosPid IN NUMBER,
                                    PV_Error  OUT VARCHAR2);
                                    
      PROCEDURE Prc_To_Regularize_All_Dth(PN_HILO      IN NUMBER,
                                        PN_GRUPOHILO in varchar2,
                                        PV_BANDERA   IN VARCHAR2,
                                        PV_Error     OUT VARCHAR2);
                                                                   
      --8693 JJI - FIN
      --9102 MRO - INICIO
      PROCEDURE Prc_Insert_Programmer_hasta(Ld_SysdateRun  IN varchar2,
                                            PV_ERROR   OUT VARCHAR2);
      --9102 MRO - FIN                                             
       --10536 HRO INICIO
      PROCEDURE PRC_RE_PLANNIG (PN_CUSTOMER IN NUMBER,
                                PV_DN_NUM   IN VARCHAR2,
                                PN_CO_ID    NUMBER,
                                PN_PK_ID    NUMBER,
                                PN_ERROR    OUT NUMBER,
                                PV_ERROR    OUT VARCHAR2);   
      --10536 HRO FIN  

End RC_TRX_IN_TO_REGULARIZE;
/
CREATE OR REPLACE package BODY RC_TRX_IN_TO_REGULARIZE is
/*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO MIGUEL MUÑOZ 
  PROYECTO:       8250 MEJORAS EN RELOJ DE COBRANZA
  FECHA:          16/10/2012
  PROPOSITO:      OPTIMIZAR EL PROCESO TTC_IN_TO_REGU para regularizacion de las cuentas con inconsistencias en la planificacion 
                  utilizando un esquema de hilos para mejorar el rendimiento en reloj de cobranza
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO MIGUEL MUÑOZ 
  PROYECTO:       8250 MEJORAS EN RELOJ DE COBRANZA
  FECHA:          12/12/2012
  PROPOSITO:      Optimizar el Proceso TTC_IN_TO_REGU validando que cuando se de un error en el proceso no se 
                  vuelva a procesar las cuentas que ya estan en la cola de regularize.
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO MIGUEL MUÑOZ 
  PROYECTO:       8649 MEJORAS EN RELOJ DE COBRANZA
  FECHA:          08/02/2013
  PROPOSITO:      Bandera para determinar si se realiza el conteo de las cuentas a procesar.
                  
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8693 RELOJ DE COBRANZA DTH
  FECHA:          03/05/2013
  PROPOSITO:      Creacion del procedimiento PRC_HEADER_DTH para identificar las cuentas dth que deben estar 
                  planificadas en reloj y PRC_HEADER_REGU_ESTAT para regularizar en la planificacion el estado
                  de las cuentas planificadas.
                  
  *********************************************************************************************************/
/*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      JEFFERSON MATEO 
  MODIFICADO POR: IRO MIGUEL ROSADO 
  PROYECTO:        [9102] Nuevo Reloj de cobranzas ¿ vistas regularize y in_to_work
  FECHA:          03/07/2013
  PROPOSITO:      Se realiza un procedimiento PRC_INSERT_PROGRAMMER_HASTA, cuando el Regularize finalice OK
                  se inserte la fecha de ayer para que el proceso de IN_TO_WORK gestiones las cuentas desde 
                  esa fecha para cubrir las cuentas que no fueron analizadas.
                  
  *********************************************************************************************************/
/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO HUGO ROMERO
  PROYECTO:       10536 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          14/12/2015
  PROPOSITO:      Se realiza un procedimiento PRC_RE_PLANNIG el cual se encarga de actualizar correctamente
                  la planificación de los servicios TARIFARIOS en la tabla RELOJ.TTC_CONTRACTPLANNIGDETAILS  
  *********************************************************************************************************/
/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       10995 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          27/07/2016
  PROPOSITO:      Se modifica el cursor C_PLANNIG_DETAILS para que se replanifiquen los siguientes estatus
                  de RELOJ (servicios que tienen inconsistencias de subproducto en BSCS).
  *********************************************************************************************************/  
/*********************************************************************************************************
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Modificado por: Iro Anabell Amayquema
  Fecha         : 05/12/2017
  Proyecto      : 11603 Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  Motivo        : Cambiar la planificacion del reloj de tal manera que las inactivaciones de los servicios 
                  (status 35) sean planificadas para el último dia del mes que corresponda
  *********************************************************************************************************/
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      30/07/2018
-- PROYECTO:       [12018] Equipo Ágil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Notificaciones del proceso de inactivacion de reloj y cambio en la fecha de inactivacion.
--=====================================================================================--
  -- Private type declarations ,constant declarations,variable declarations
        i_process                                   ttc_process_t;
        i_rule_element                          ttc_Rule_Element_t;
        i_Bck_Process_t                        ttc_Bck_Process_t;
        i_Log_InOut_t                            ttc_Log_InOut_t; 
        i_Process_Programmer_t       ttc_Process_Programmer_t;    
        i_Rule_Values_t                        ttc_Rule_Values_t;   
        i_Contr_SuspXOper_In             ttc_Contr_SuspXOper_In_t;
        i_Customers_In_t                      ttc_View_Customers_In_t;
        i_Cust_to_Save_In_t                  ttc_View_Customers_In_type;
        
     --SCP:VARIABLES
    ------------------------------------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ------------------------------------------------------------------------------------------
        ln_id_bitacora_scp number:=0; 
        ln_total_registros_scp number:=0;
        ln_total_registros_scp_all  number := 0;
        lv_id_proceso_scp varchar2(100):='TTC_IN';
        lv_referencia_scp varchar2(100):='RC_TRX_IN_TO_REGULARIZE.Prc_Header';
        lv_unidad_registro_scp varchar2(30):='Regu CO_ID';
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
    ---------------------------------------------------------------

--**********************************************************************************
FUNCTION Fct_Save_ToWk_Regularize  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;        
                  INSERT INTO TTC_CONTRACT_TO_REGULARIZE_S NOLOGGING
                  VALUES (i_Cust_to_Save_In_t) ;                          
        
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk_Regularize-Error General al intentar grabar en la tabla TTC_CONTRACT_ALL_TO_REGULARIZE_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
END;

/************************************************************************************
 8693 JJI
 Funcion encargada de insertar un registro en la tabla TTC_CONTRACT_TO_REGU_DTH_S
*************************************************************************************/ 
FUNCTION Fct_Save_ToWk_Regu_Dth  RETURN VARCHAR2 IS
         Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
 BEGIN
   Lv_Error:=NULL;        
   INSERT INTO TTC_CONTRACT_TO_REGU_DTH_S NOLOGGING
   VALUES (i_Cust_to_Save_In_t) ;                          
          
   RETURN(Lv_Error);
 EXCEPTION        
 WHEN OTHERS THEN  
      Lv_Error:='Fct_Save_ToWk_Regu_Dth-Error General al intentar grabar en la tabla TTC_CONTRACT_TO_REGU_DTH, '||substr(SQLERRM,1,300);
      RETURN (Lv_Error) ;
END;

--**********************************************************************************
PROCEDURE Prc_Header(PV_FILTRO IN VARCHAR2,
                       PV_BANDERA IN VARCHAR2,
                       PN_HILO IN NUMBER,
                       PN_GRUPOHILO IN VARCHAR2,
                       Pn_SosPid IN NUMBER, 
                       PV_Error OUT VARCHAR2) IS
                                               
       CURSOR C_Trx_In IS
       SELECT  TTC_TRX_IN.NEXTVAL FROM dual;            
       Cursor C_PARAMETROS (CN_TIPOPARAMETRO Number, CV_PARAMETRO Varchar2) Is
       Select Valor
       From sysadm.Gv_Parametros g
       Where g.Id_Tipo_Parametro = Cn_Tipoparametro
       And g.Id_Parametro = Cv_Parametro;            
                                
      Lv_Des_Error        VARCHAR2(800);
      Le_Error_Valid     EXCEPTION;
      Le_Error_Process EXCEPTION;
      Ln_ConTrx            NUMBER;
      Ld_SysdateRun    DATE;
      Ln_Seqno              NUMBER;
      LV_FILTRO VARCHAR(200);
      lv_hilo number;
      LN_R1 NUMBER:=0;
      LN_R2 NUMBER:=0;
      lv_parametro varchar2(4000);
BEGIN
           Ld_SysdateRun:=SYSDATE;

----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
/* 8250 MMC - INICIO */ 
  IF nvl(PV_BANDERA, 'N')='S' THEN
    lv_referencia_scp:='Grupo_Hilo: '||PN_HILO||' Subhilo: '||PN_GRUPOHILO||' >> '||PV_FILTRO;
END IF;

   Open C_PARAMETROS(753,'CANT_REGU_RELOJ');
   Fetch C_PARAMETROS Into lv_parametro;
   Close C_PARAMETROS;

IF NVL(lv_parametro,'N')='S'THEN
  IF nvl(PV_BANDERA, 'N')='S'THEN
     LN_R1 :=SUBSTR(PN_GRUPOHILO,1,1);
     LN_R2 :=SUBSTR(PN_GRUPOHILO,-1,1);
        
     OPEN C_CANTIDAD_TOTAL_ALL(LN_R1,LN_R2); 
     FETCH C_CANTIDAD_TOTAL_ALL INTO ln_total_registros_scp_all;
     close C_CANTIDAD_TOTAL_ALL;
    
     ln_total_registros_scp:= ln_total_registros_scp_all;
    
   ELSE
       
      OPEN C_CANTIDAD_TOTAL; 
      FETCH C_CANTIDAD_TOTAL INTO ln_total_registros_scp_all;
      close C_CANTIDAD_TOTAL;
    
      ln_total_registros_scp:= ln_total_registros_scp_all;
    
   END IF;
END IF; 
/* 8250 MMC - FIN */ 
           scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                                 lv_referencia_scp,
                                                 null,null,null,null,
                                                 ln_total_registros_scp,
                                                 lv_unidad_registro_scp,
                                                 ln_id_bitacora_scp,
                                                 ln_error_scp,lv_error_scp);
           if ln_error_scp <>0 then
              Lv_Des_Error:='Error en plataforma SCP, No se pudo iniciar la Bitacora';
              RAISE Le_Error_Valid;
           end if;
           ----------------------------------------------------------------------
         --Cargar los datos del Proceso TTC_IN -->TTC_Process_s                  
          i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GN_DESSHORT_IN);      
          IF i_process.Process IS NULL THEN
                Lv_Des_Error:='No existen datos configurados en la tabla TTC_Process_s';
                RAISE Le_Error_Valid;
          END IF;

          --Ingreso de Inicio del proceso 
            i_Log_InOut_t:= ttc_Log_InOut_t(Process => nvl(i_process.Process,0),
                                                                      RunDate => SYSDATE,
                                                                      LastDate => NULL,
                                                                      StatusProcess => 'OKI',
                                                                      Comment_Run => NULL                                                                           
                                                                      );                  
            --Verifica si el proceso se encuentra en ejecución
/* 8250 MMC - INICIO */ 
            IF nvl(PV_BANDERA, 'N') <> 'S' THEN
    
            IF  RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GN_DESSHORT_IN,i_process.Duration) THEN
                  Lv_Des_Error:='El proceso ya se encuentra en ejecución, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
                  RAISE Le_Error_process;
            END IF;
    
             LV_FILTRO := i_process.FilterField;
             lv_hilo := 0;
            ELSE  
    
             LV_FILTRO := PV_FILTRO;
             lv_hilo := PN_HILO;     
            END IF;            
/* 8250 MMC - FIN */ 
--Graba el proceso para controlar el bloqueo de los procesos
             i_Bck_Process_t := ttc_Bck_Process_t(Process       => i_process.Process,
                                         DesShort      => i_process.DesShort,
                                         GroupTread    => 0,
                                         Tread_NO      => lv_hilo,      --0,
                                         FilterField   => LV_FILTRO,--i_process.FilterField,
                                         LastRunDate   => SYSDATE,
                                         MaxDelay      => i_process.MaxDelay,
                                         Refresh_Cycle => i_process.Refresh_Cycle,
                                         SosPid        => Pn_SosPid);
             Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
             IF Lv_Des_Error IS NOT NULL THEN
                  Lv_Des_Error:='Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesión levantada';
                  RAISE Le_Error_process;
             END IF;                                                                                                                                                            

            --Cargar los datos del Proceso TTC_IN_TO_REGU --> TTC_rule_element
            i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GN_DESSHORT_IN);
           IF i_rule_element.Process IS NULL THEN
                  Lv_Des_Error:='No existen datos configurados en la tabla TTC_rule_element';
                  RAISE Le_Error_Valid;
            END IF;                        
                                                 
           ----------------------------------------------------------------------
           -- Ejecución del proceso :Prc_Regulariza_Operador
           ----------------------------------------------------------------------
 --8693 jji
 /*Prc_To_Regularize_Oper(PV_FILTRO,PN_HILO, PN_GRUPOHILO, PV_BANDERA, PV_Error);
 IF PV_ERROR IS NOT NULL THEN
        Lv_Des_Error:=PV_ERROR;
        RAISE Le_Error_Valid;
 END IF;            */     
 --8693 jji      
----------------------------------------------------------------------
-- Ejecución del proceso :Prc_Regulariza_All
----------------------------------------------------------------------
Prc_To_Regularize_All(PV_FILTRO,PN_HILO, PN_GRUPOHILO, PV_BANDERA, PV_Error);
           IF PV_ERROR IS NOT NULL THEN
                  Lv_Des_Error:=PV_ERROR;
                  RAISE Le_Error_Valid;
           END IF;                          
           
           --Graba en la tabla programmer la fecha de ejecucion del proceso
            OPEN C_Trx_In ;
           FETCH C_Trx_In INTO Ln_Seqno;
           CLOSE C_Trx_In;                                                                                                          
/* 8250 MMC - INICIO */ 
          IF nvl(PV_BANDERA, 'N')<>'S'THEN                                                                                                          
           i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                                                                              Seqno => Ln_Seqno,
                                                                                                              RunDate => Ld_SysdateRun,
                                                                                                              LastModDate => Ld_SysdateRun
                                                                                                             );
           Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
           IF Lv_Des_Error IS NOT NULL THEN
                RAISE Le_Error_Valid;
           END IF;                                                                                                                                                            
          END IF;
/* 8250 MMC - FIN */ 
          ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='FIN '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 'RC_TRX_IN_TO_REGULARIZE-Prc_Header Ejecutado con Exito'
                                                 ,Null,
                                                 0,
                                                 lv_unidad_registro_scp,
                                                 Null,Null,null,null,
                                                 'N',ln_error_scp,lv_error_scp);
           ----------------------------------------------------------------------
          --SCP:FIN                  
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);                  
          ----------------------------------------------------------------------------
           Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);          
           PV_ERROR:='RC_TRX_IN_TO_REGULARIZE-Prc_Header-SUCESSFUL';                                       
           --Registro del Fin del proceso 
           i_Log_InOut_t.LastDate:=SYSDATE;
           i_Log_InOut_t.StatusProcess:='OKI';
           i_Log_InOut_t.Comment_Run:=PV_ERROR;
           Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
      
     EXCEPTION            
           WHEN Le_Error_process THEN
                PV_ERROR:='Proceso: RC_TRX_IN_TO_REGULARIZE- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:=PV_ERROR;
                 ----------------------------------------------------------------------
                 -- SCP: Código generado automáticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                 
           WHEN Le_Error_Valid THEN                      
                PV_ERROR:='Proceso: RC_TRX_IN_TO_REGULARIZE- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:=PV_ERROR;
                 ----------------------------------------------------------------------
                 -- SCP: Código generado automáticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                 
           WHEN OTHERS THEN                                        
                 PV_ERROR:='     RC_TRX_IN_TO_REGULARIZE-Prc_Head-Error al ejecutar el proceso-'||substr(SQLERRM,1,500);
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                  ----------------------------------------------------------------------
                   -- SCP: Código generado automáticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Proceso: RC_TRX_IN_TO_REGULARIZE-Error general';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         PV_ERROR,
                                                         Null,
                                                         2,
                                                         lv_unidad_registro_scp,
                                                         Null,Null,
                                                         null,null,'N',ln_error_scp,lv_error_scp);
                  ----------------------------------------------------------------------------
                  i_Log_InOut_t.LastDate:=SYSDATE;
                  i_Log_InOut_t.StatusProcess:='ERR';
                  i_Log_InOut_t.Comment_Run:=  PV_ERROR;                      
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                  
   END Prc_Header;
   
/************************************************************************************
 8693 JJI
 Procedimiento que ejecuta el proceso encargado de identificar las cuentas que deberian
 estar planificadas en ttc_contractplannigdetails
*************************************************************************************/ 
   
PROCEDURE Prc_Header_Dth(PV_BANDERA   IN VARCHAR2,
                          PN_HILO      IN NUMBER,
                          PN_GRUPOHILO IN VARCHAR2,
                          Pn_SosPid    IN NUMBER,
                          PV_Error     OUT VARCHAR2) IS

  CURSOR C_Trx_In_Dth IS
    SELECT TTC_TRX_IN_DTH.NEXTVAL FROM dual;
    
  CURSOR C_RULE_VALUES (CV_PARAM VARCHAR2) IS
  SELECT A.VALUE_RETURN_SHORTSTRING
    FROM TTC_RULE_VALUES A
   WHERE A.PROCESS = GN_PROCESO_DTH
     AND A.VALUE_RULE_STRING = CV_PARAM;

  Lv_Des_Error VARCHAR2(800);
  Le_Error_Valid EXCEPTION;
  Le_Error_Process EXCEPTION;
  Ld_SysdateRun DATE;
  Ln_Seqno      NUMBER;
  lv_hilo       number;
  LN_R1         NUMBER := 0;
  LN_R2         NUMBER := 0;
  lv_parametro  varchar2(4000);
  LB_NOTFOUND   BOOLEAN;
BEGIN
  Ld_SysdateRun := SYSDATE;

  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_id_proceso_scp:=GV_DESSHORT_DTH;
  lv_referencia_scp:='RC_TRX_IN_TO_REGULARIZE.Prc_Header_DTH';
  IF nvl(PV_BANDERA, 'N') = 'S' THEN
    lv_referencia_scp := lv_referencia_scp|| ' >> Hilo: ' ||PN_GRUPOHILO||' Grupo_Hilo: ' || PN_HILO  ;
  END IF;
  
  OPEN C_RULE_VALUES('TTC_FLAG_CANTIDAD_REGISTROS');
  FETCH C_RULE_VALUES INTO lv_parametro;
  LB_NOTFOUND:=C_RULE_VALUES%NOTFOUND;
  IF LB_NOTFOUND THEN
    lv_parametro:='N';
  END IF;
  CLOSE C_RULE_VALUES;

  IF NVL(lv_parametro, 'N') = 'S' THEN
    IF nvl(PV_BANDERA, 'N') = 'S' THEN
      LN_R1 := SUBSTR(PN_GRUPOHILO, 1, 1);
      LN_R2 := SUBSTR(PN_GRUPOHILO, -1, 1);
    
      OPEN C_REG_TOTAL(LN_R1, LN_R2);
      FETCH C_REG_TOTAL INTO ln_total_registros_scp;
      close C_REG_TOTAL;

    ELSE
    
      OPEN C_REG_TOTAL_ALL;
      FETCH C_REG_TOTAL_ALL INTO ln_total_registros_scp;
      close C_REG_TOTAL_ALL;

    END IF;
  END IF;
  
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        lv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 then
    Lv_Des_Error := 'Error en plataforma SCP, No se pudo iniciar la Bitacora';
    RAISE Le_Error_Valid;
  end if;
  ----------------------------------------------------------------------
  --Cargar los datos del Proceso TTC_IN_REGU_DTH -->TTC_Process_s                  
  i_process := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_DTH);
  IF i_process.Process IS NULL THEN
    Lv_Des_Error := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE Le_Error_Valid;
  END IF;

  --Ingreso de Inicio del proceso 
  i_Log_InOut_t := ttc_Log_InOut_t(Process       => nvl(i_process.Process, 0),
                                   RunDate       => SYSDATE,
                                   LastDate      => NULL,
                                   StatusProcess => 'OKI',
                                   Comment_Run   => NULL);
  
  --Verifica si el proceso se encuentra en ejecución
  IF nvl(PV_BANDERA, 'N') <> 'S' THEN
  
    IF RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GN_DESSHORT_IN,
                                                           i_process.Duration) THEN
      Lv_Des_Error := 'El proceso ya se encuentra en ejecución, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
      RAISE Le_Error_process;
    END IF;
 
    lv_hilo   := 0;
  ELSE
    lv_hilo   := PN_HILO;
  END IF;
 
  --Graba el proceso para controlar el bloqueo de los procesos
  i_Bck_Process_t := ttc_Bck_Process_t(Process       => i_process.Process,
                                       DesShort      => i_process.DesShort,
                                       GroupTread    => 0,
                                       Tread_NO      => lv_hilo,
                                       FilterField   => i_process.FilterField,
                                       LastRunDate   => SYSDATE,
                                       MaxDelay      => i_process.MaxDelay,
                                       Refresh_Cycle => i_process.Refresh_Cycle,
                                       SosPid        => Pn_SosPid);
  Lv_Des_Error    := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
  IF Lv_Des_Error IS NOT NULL THEN
    Lv_Des_Error := 'Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesión levantada';
    RAISE Le_Error_process;
  END IF;

  --Cargar los datos del Proceso TTC_IN_REGU_DTH --> TTC_rule_element
  i_rule_element := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_DTH);
  IF i_rule_element.Process IS NULL THEN
    Lv_Des_Error := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE Le_Error_Valid;
  END IF;
  
  ----------------------------------------------------------------------
  -- Ejecución del proceso :Prc_Regulariza_All_Dth
  ----------------------------------------------------------------------
  Prc_To_Regularize_All_DTH(PN_HILO,
                            PN_GRUPOHILO,
                            PV_BANDERA,
                            PV_Error);
  IF PV_ERROR IS NOT NULL THEN
    Lv_Des_Error := PV_ERROR;
    RAISE Le_Error_Valid;
  END IF;

  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_In_Dth;
  FETCH C_Trx_In_Dth
    INTO Ln_Seqno;
  CLOSE C_Trx_In_Dth;

  IF nvl(PV_BANDERA, 'N') <> 'S' THEN
    i_Process_Programmer_t := ttc_Process_Programmer_t(Process     => i_process.Process,
                                                       Seqno       => Ln_Seqno,
                                                       RunDate     => Ld_SysdateRun,
                                                       LastModDate => Ld_SysdateRun);
    Lv_Des_Error           := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
    IF Lv_Des_Error IS NOT NULL THEN
      RAISE Le_Error_Valid;
    END IF;
  END IF;

  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN ' || lv_id_proceso_scp;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        'RC_TRX_IN_TO_REGULARIZE-Prc_Header_DTH Ejecutado con Exito',
                                        Null,
                                        0,
                                        lv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------
  --SCP:FIN                  
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------
  Lv_Des_Error := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
  PV_ERROR     := 'RC_TRX_IN_TO_REGULARIZE-Prc_Header_DTH-SUCESSFUL';
  --Registro del Fin del proceso 
  i_Log_InOut_t.LastDate      := SYSDATE;
  i_Log_InOut_t.StatusProcess := 'OKI';
  i_Log_InOut_t.Comment_Run   := PV_ERROR;
  Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);

EXCEPTION
  WHEN Le_Error_process THEN
    PV_ERROR                    := 'Proceso: RC_TRX_IN_TO_REGULARIZE- Ejecutado con ERROR-->' ||
                                   'Prc_Header_DTH-' || Lv_Des_Error;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          3,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
  WHEN Le_Error_Valid THEN
    PV_ERROR                    := 'Proceso: RC_TRX_IN_TO_REGULARIZE- Ejecutado con ERROR-->' ||
                                   'Prc_Header_DTH-' || Lv_Des_Error;
    i_Log_InOut_t.Comment_Run   := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
    i_Log_InOut_t.process       := GN_PROCESO_DTH;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;

    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          3,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
  WHEN OTHERS THEN
    PV_ERROR                  := '     RC_TRX_IN_TO_REGULARIZE-Prc_Header_Dth-Error al ejecutar el proceso-' ||
                                 substr(SQLERRM, 1, 500);
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error general';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          2,
                                          lv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    i_Log_InOut_t.process       := GN_PROCESO_DTH;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    i_Log_InOut_t.Comment_Run   := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
 END Prc_Header_Dth;

/************************************************************************************
 8693 JJI
 Procedimiento que ejecuta el proceso encargado de Regulariza todos los co_id o contratos
 modificados por el operador a partir de la última fecha procesada.
*************************************************************************************/ 
PROCEDURE Prc_Header_Regu_Estat(Pn_SosPid IN NUMBER,
                                PV_Error  OUT VARCHAR2) IS

  CURSOR C_Trx_Regu_Estat IS
    SELECT TTC_TRX_REGU_ESTAT.NEXTVAL FROM dual;
              
  Lv_Des_Error            VARCHAR2(800);
  Le_Error_Valid          EXCEPTION;
  Le_Error_Process        EXCEPTION;
  Ld_SysdateRun           DATE;
  Ln_Seqno                NUMBER;

BEGIN
  Ld_SysdateRun := SYSDATE;

  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_id_proceso_scp:=GV_DESSHORT_REGU_ESTAT; 
  lv_referencia_scp:='RC_TRX_IN_TO_REGULARIZE.Prc_Header_Regu_Estat';
  lv_unidad_registro_scp:='CO_ID';
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        lv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 then
    Lv_Des_Error := 'Error en plataforma SCP, No se pudo iniciar la Bitacora';
    RAISE Le_Error_Valid;
  end if;
  ----------------------------------------------------------------------
  --Cargar los datos del Proceso TTC_IN -->TTC_Process_s                  
  i_process := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_REGU_ESTAT);
  IF i_process.Process IS NULL THEN
    Lv_Des_Error := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE Le_Error_Valid;
  END IF;

  --Ingreso de Inicio del proceso 
  i_Log_InOut_t := ttc_Log_InOut_t(Process       => nvl(i_process.Process, 0),
                                   RunDate       => SYSDATE,
                                   LastDate      => NULL,
                                   StatusProcess => 'OKI',
                                   Comment_Run   => NULL);
  --Verifica si el proceso se encuentra en ejecución

  IF RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GV_DESSHORT_REGU_ESTAT,
                                                         i_process.Duration) THEN
    Lv_Des_Error := 'El proceso ya se encuentra en ejecución, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
    RAISE Le_Error_process;
  END IF;

  --Graba el proceso para controlar el bloqueo de los procesos
  i_Bck_Process_t := ttc_Bck_Process_t(Process       => i_process.Process,
                                       DesShort      => i_process.DesShort,
                                       GroupTread    => 0,
                                       Tread_NO      => 0,
                                       FilterField   => i_process.FilterField,
                                       LastRunDate   => SYSDATE,
                                       MaxDelay      => i_process.MaxDelay,
                                       Refresh_Cycle => i_process.Refresh_Cycle,
                                       SosPid        => Pn_SosPid);
  Lv_Des_Error    := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
  IF Lv_Des_Error IS NOT NULL THEN
    Lv_Des_Error := 'Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesión levantada';
    RAISE Le_Error_process;
  END IF;

  --Cargar los datos del Proceso TTC_REGU_ESTAT --> TTC_rule_element
  i_rule_element := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_REGU_ESTAT);
  IF i_rule_element.Process IS NULL THEN
    Lv_Des_Error := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE Le_Error_Valid;
  END IF;

  ----------------------------------------------------------------------
  -- Ejecución del proceso :Prc_Regulariza_Operador
  ----------------------------------------------------------------------
  Prc_To_Regularize_Oper(PV_Error);
  IF PV_ERROR IS NOT NULL THEN
    Lv_Des_Error := PV_ERROR;
    RAISE Le_Error_Valid;
  END IF;

  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_Regu_Estat ;
  FETCH C_Trx_Regu_Estat INTO Ln_Seqno;
  CLOSE C_Trx_Regu_Estat;                                                                                                          

                                                                                                        
  i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                  Seqno => Ln_Seqno,
                                                  RunDate => Ld_SysdateRun,
                                                  LastModDate => Ld_SysdateRun
                                                  );
  Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
  IF Lv_Des_Error IS NOT NULL THEN
      RAISE Le_Error_Valid;
  END IF;                                                                                                                                                            

  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN ' || lv_id_proceso_scp;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        'RC_TRX_IN_TO_REGULARIZE-Prc_Header_Regu_Estat Ejecutado con Exito',
                                        Null,
                                        0,
                                        lv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------
  --SCP:FIN                  
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------
  Lv_Des_Error := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
  PV_ERROR     := 'RC_TRX_IN_TO_REGULARIZE-Prc_Header_Regu_Estat-SUCESSFUL';
  --Registro del Fin del proceso 
  i_Log_InOut_t.LastDate      := SYSDATE;
  i_Log_InOut_t.StatusProcess := 'OKI';
  i_Log_InOut_t.Comment_Run   := PV_ERROR;
  Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);

EXCEPTION
  WHEN Le_Error_process THEN
    PV_ERROR                    := 'Proceso: RC_TRX_IN_TO_REGULARIZE.Prc_Header_Regu_Estat- Ejecutado con ERROR--> ' ||Lv_Des_Error;
    i_Log_InOut_t.Process       := GN_PROCESO_REGU_ESTAT;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
 
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          3,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
  WHEN Le_Error_Valid THEN
    PV_ERROR                    := 'Proceso: RC_TRX_IN_TO_REGULARIZE.Prc_Header_Regu_Estat- Ejecutado con ERROR-->' ||
                                   Lv_Des_Error;
    i_Log_InOut_t.Comment_Run   := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
    i_Log_InOut_t.Process       := GN_PROCESO_REGU_ESTAT;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error en Validación de Datos';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
   
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          3,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
  WHEN OTHERS THEN
    PV_ERROR                  := '     RC_TRX_IN_TO_REGULARIZE.Prc_Header_Regu_Estat - Error al ejecutar el proceso-' ||
                                 substr(SQLERRM, 1, 500);
    i_Log_InOut_t.Comment_Run := RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso: RC_TRX_IN_TO_REGULARIZE-Error general';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          PV_ERROR,
                                          Null,
                                          2,
                                          lv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp); 
    ----------------------------------------------------------------------------
    i_Log_InOut_t.Process       := GN_PROCESO_REGU_ESTAT;
    i_Log_InOut_t.LastDate      := SYSDATE;
    i_Log_InOut_t.StatusProcess := 'ERR';
    i_Log_InOut_t.Comment_Run   := PV_ERROR;
    i_Log_InOut_t.Comment_Run   := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
END Prc_Header_Regu_Estat;

   /************************************************************************************
     Regulariza todos los co_id o contratos modificados por el operador a partir
     de la última fecha procesada.
   *************************************************************************************/     
   PROCEDURE Prc_To_Regularize_Oper(/*PV_FILTRO IN VARCHAR2,PN_HILO IN NUMBER, PN_GRUPOHILO in varchar2, PV_BANDERA IN VARCHAR2,*/--8693 jji
                                    PV_Error OUT VARCHAR2) IS
      Lv_Des_Error        VARCHAR2(800);            
      Le_Error_Valid     EXCEPTION;         
      Ln_ConTrx            NUMBER:=0;
      /*LN_R1 NUMBER:=0;
      LN_R2 NUMBER:=0;*/--8693 jji
      lv_mensaje_tec_scp varchar2(4000);
      le_error EXCEPTION;                                                      
   BEGIN              
            ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje 
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='INICIO Prc_To_Regularize_Oper: '||lv_id_proceso_scp;
--8693 jji
/*\* 8250 MMC - INICIO *\ 
     IF nvl(PV_BANDERA, 'N')='S'THEN
       lv_mensaje_tec_scp:='Grupo_Hilo: '||PN_HILO||' Subhilo: '||PN_GRUPOHILO||' >> '||PV_FILTRO;
     else
       lv_mensaje_tec_scp:=NULL;
     end if;
\* 8250 MMC - FIN*\ */
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                 lv_mensaje_apl_scp,
                                                                                 lv_mensaje_tec_scp,
                                                                                 Null,
                                                                                 0,
                                                                                 lv_unidad_registro_scp,
                                                                                 Null,Null,
                                                                                 null,null,'N',ln_error_scp,lv_error_scp
                                                                                 );

            --Bloque para procesar los contratos que fueron suspendidos por el operador 
            i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(GN_PROCESO_REGU_ESTAT/*GN_PROCESO_IN*/,
                                                                             GN_DES_FLAG_IN_SUSP_X_OPER);  

            IF nvl(i_Rule_Values_t.Value_Return_ShortString,'N')<>'S' THEN
                RAISE Le_Error_Valid;
            END IF;
--8693 jji
/*\* 8250 MMC - INICIO *\  
   IF nvl(PV_BANDERA, 'N')='S' THEN
    
    LN_R1 :=SUBSTR(PN_GRUPOHILO,1,1);
     
    LN_R2 :=SUBSTR(PN_GRUPOHILO,-1,1);
    
             
    OPEN C_SUSP_X_OPER_IN2(LN_R1,LN_R2);
    
    LOOP
      FETCH C_SUSP_X_OPER_IN2 BULK COLLECT INTO i_Contr_SuspXOper_In LIMIT i_rule_element.Value_Max_Trx_By_Execution;
      exit when i_Contr_SuspXOper_In.count=0;
      Ln_ConTrx := 0;
      FOR rc_in IN (select *
                      from table(cast(i_Contr_SuspXOper_In as
                                      ttc_Contr_SuspXOper_In_t))) LOOP
        BEGIN
          Ln_ConTrx := Ln_ConTrx + 1;
          UPDATE \*+ all_rows *\ttc_contractplannigdetails cp
             SET cp.co_statustrx = GN_STATUS_F,
                 cp.co_remark    = 'RELOJ-QC',
                 cp.co_remarktrx = 'Registro modificado por transacciones directas del Operador-->' ||
                                   rc_in.co_userlastmod,
                 cp.lastmoddate  = rc_in.product_history_date
           WHERE cp.customer_id = rc_in.customer_id
             AND cp.co_id = rc_in.co_id
                --AND  cp.co_period    =rc_in.co_period
             AND cp.re_orden <= rc_in.re_orden
             AND cp.co_statustrx <> GN_STATUS_F;
        
          UPDATE \*+ all_rows *\ttc_contractplannig ct
             SET ct.co_period    = rc_in.co_period,
                 ct.co_status_a  = rc_in.co_status_a,
                 ct.ch_status_b  = rc_in.ch_status_b,
                 ct.co_reason    = rc_in.co_reason,
                 ct.ch_validfrom = rc_in.product_history_date,
                 ct.lastmoddate  = rc_in.product_history_date
           WHERE ct.customer_id = rc_in.customer_id
             AND ct.co_id = rc_in.co_id;
        
          IF Ln_ConTrx >= i_rule_element.Value_Max_TrxCommit THEN
            COMMIT;
            Ln_ConTrx := 0;
          END IF;
          
          
        EXCEPTION
          WHEN OTHERS THEN
            PV_Error                    := 'Prc_To_Regularize_Oper-Error al momento de Actualizar los Estados modificados por el Operador,' ||
                                           substr(SQLERRM, 1, 300);
            i_Log_InOut_t.LastDate      := SYSDATE;
            i_Log_InOut_t.StatusProcess := 'ERR';
            i_Log_InOut_t.Comment_Run   := PV_Error;
            Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            lv_mensaje_apl_scp := '          Error general: Prc_To_Regularize_Oper-Procesando Cuenta';
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  PV_Error,
                                                  Null,
                                                  2,
                                                  Null,
                                                  Null,
                                                  Null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
        END;
      END LOOP;
      --***********--  
      COMMIT;
      --***********--     
      EXIT WHEN C_SUSP_X_OPER_IN2%NOTFOUND;
    END LOOP;
    CLOSE C_SUSP_X_OPER_IN2; 
\* 8250 MMC - FIN *\           
ELSE                           */
    OPEN C_SUSP_X_OPER_IN;
    LOOP   
    FETCH C_SUSP_X_OPER_IN BULK COLLECT INTO i_Contr_SuspXOper_In LIMIT i_rule_element.Value_Max_Trx_By_Execution;                                                         
    exit when i_Contr_SuspXOper_In.count=0;
    Ln_ConTrx:=0;
    FOR rc_in IN (select * from table(cast( i_Contr_SuspXOper_In as reloj.ttc_Contr_SuspXOper_In_t)))  LOOP
           BEGIN
                  Ln_ConTrx:=Ln_ConTrx+1;
                   UPDATE /*+ all_rows */ reloj.ttc_contractplannigdetails cp
                            SET cp.co_statustrx=GN_STATUS_F,
                                     cp.co_remark='RELOJ-QC',
                                     cp.co_remarktrx='Registro modificado por transacciones directas del Operador-->'||rc_in.co_userlastmod,
                                     cp.lastmoddate=rc_in.product_history_date
                     WHERE  cp.customer_id=rc_in.customer_id
                            AND  cp.co_id             =rc_in.co_id
                           -- AND  cp.co_period    =rc_in.co_period
                             AND cp.re_orden    <=rc_in.re_orden
                             AND cp.co_statustrx <> GN_STATUS_F;

                   UPDATE /*+ all_rows */ reloj.ttc_contractplannig ct
                            SET ct.co_period =rc_in.co_period,
                                     ct.co_status_a =rc_in.co_status_a,
                                     ct.ch_status_b =rc_in.ch_status_b,
                                     ct.co_reason =rc_in.co_reason,
                                     ct.ch_validfrom=rc_in.product_history_date,
                                     ct.lastmoddate=rc_in.product_history_date
                     WHERE  ct.customer_id=rc_in.customer_id
                            AND  ct.co_id             =rc_in.co_id;
                                                                                                                            
                ln_registros_procesados_scp:=ln_registros_procesados_scp + 1; --8693 JJI
                IF Ln_ConTrx>=i_rule_element.Value_Max_TrxCommit THEN
                  --[8693] JJI - INICIO
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                         ln_registros_procesados_scp,
                                                         ln_registros_error_scp,
                                                         ln_error_scp,
                                                         lv_error_scp);
                  --[8693] JJI - FIN
                   COMMIT;
                   Ln_ConTrx:=0;
                END IF;                       
           EXCEPTION            
                 WHEN OTHERS THEN 
                 ln_registros_error_scp:=ln_registros_error_scp + 1;                                            
                 PV_Error:='Error al momento de Actualizar el estado de la cuenta modificada por el Operador >> co_id:'||rc_in.co_id||', co_status_a:'||rc_in.co_status_a||' - '||substr(SQLERRM,1,300);                                                                   
                  i_Log_InOut_t.LastDate:=SYSDATE;
                  i_Log_InOut_t.StatusProcess:='ERR';
                  i_Log_InOut_t.Comment_Run:=  PV_Error;                      
                  Lv_Des_Error:= reloj.RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                  --[8693] JJI - INICIO
                  ----------------------------------------------------------------------
                  -- SCP: Código generado automáticamente. Registro de mensaje de error
                  ----------------------------------------------------------------------
                  lv_mensaje_apl_scp := '          Error general: Prc_To_Regularize_Oper-Procesando Cuenta';
                  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                        lv_mensaje_apl_scp,
                                                        PV_Error,
                                                        Null,
                                                        2,
                                                        Null,
                                                        Null,
                                                        Null,
                                                        null,
                                                        null,
                                                        'N',
                                                        ln_error_scp,
                                                        lv_error_scp);
                  --[8693] JJI - FIN
                        
                  END;
      END LOOP;
      COMMIT;
      --***********--     
      EXIT WHEN C_SUSP_X_OPER_IN%NOTFOUND;
    END LOOP;
    CLOSE C_SUSP_X_OPER_IN;
		              ----------------------------------------------------------------------
                   -- SCP: Código generado automáticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                    --[8693] JJI - INICIO
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                         ln_registros_procesados_scp,
                                                         ln_registros_error_scp,
                                                         ln_error_scp,
                                                         lv_error_scp);
                  --[8693] JJI - INICIO
                   
                   lv_mensaje_apl_scp:='FIN Prc_To_Regularize_Oper '||lv_id_proceso_scp;
                        
 -- END IF;
 /* IF nvl(PV_BANDERA, 'N')='S'THEN
   lv_mensaje_tec_scp:='Grupo_Hilo: '||PN_HILO||' Subhilo: '||PN_GRUPOHILO||' >> '||PV_FILTRO;
  else*/
   lv_mensaje_tec_scp:='Prc_To_Regularize_Oper- Ejecutado con Exito';
 -- end if;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                          lv_mensaje_tec_scp,
                                                         Null,
                                                         0,
                                                         lv_unidad_registro_scp,
                                                         Null,Null,null,null,
                                                         'N',ln_error_scp,lv_error_scp);
                   ----------------------------------------------------------------------
                  --SCP:FIN

                          
               PV_Error:=NULL;
    EXCEPTION            
           WHEN Le_Error_Valid THEN
               PV_Error:='Prc_To_Regularize_Oper-La bandera de ejecución con Valor N'||substr(SQLERRM,1,500);
           WHEN OTHERS THEN        
               ROLLBACK;                                
              PV_Error:='Prc_To_Regularize_Oper-Error en la ejecución del proceso'||substr(SQLERRM,1,500);                  
   END Prc_To_Regularize_Oper;
   
/************************************************************************************
 Regulariza todos los co_id o contratos inconsistente a partir de una fecha
 segun el valor configurado por "TTC_DATE_MAX_TO_REGULARIZE"
*************************************************************************************/    
PROCEDURE Prc_To_Regularize_All(PV_FILTRO IN VARCHAR2,PN_HILO IN NUMBER, PN_GRUPOHILO in varchar2, PV_BANDERA IN VARCHAR2,PV_Error OUT VARCHAR2) IS
  Lv_Des_Error        VARCHAR2(800);            
  Le_Error_Valid     EXCEPTION;         
  Ln_ConTrx            NUMBER;              
  Le_Error_Trx        EXCEPTION;      
  Ln_Customer_id   NUMBER;
  Ln_Co_id                NUMBER;
  LN_R1 NUMBER:=0;
  LN_R2 NUMBER:=0;
  lv_mensaje_tec_scp varchar2(4000);
  le_error exception;                                                                                      

   BEGIN              
            ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje de error
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='INICIO Prc_To_Regularize_All: '||lv_id_proceso_scp;
/* 8250 MMC - INICIO */ 
IF nvl(PV_BANDERA, 'N')='S'THEN
   lv_mensaje_tec_scp:='Grupo_Hilo: '||PN_HILO||' Subhilo: '||PN_GRUPOHILO||' >> '||PV_FILTRO;
            
 else
   lv_mensaje_tec_scp:=NULL;
end if;
/* 8250 MMC - FIN */  
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                 lv_mensaje_apl_scp,
                                      lv_mensaje_tec_scp,
                                      Null,
                                      0,
                                      lv_unidad_registro_scp,
                                      Null,
                                      Null,
                                      NULL,
                                      null,
                                      'N',
                                      ln_error_scp,
                                      lv_error_scp);
  
/* 8250 MMC - INICIO */ 
IF nvl(PV_BANDERA, 'N')='S' THEN
     
    
LN_R1 :=SUBSTR(PN_GRUPOHILO,1,1); 
LN_R2 :=SUBSTR(PN_GRUPOHILO,-1,1);
    
OPEN C_REGULARIZE_IN2(LN_R1,LN_R2);
    
LOOP
      
  FETCH C_REGULARIZE_IN2 BULK COLLECT INTO i_Customers_In_t LIMIT i_rule_element.Value_Max_Trx_By_Execution;  
  exit when i_Customers_In_t.count=0;
  Ln_ConTrx := 0;
  FOR rc_in IN (select *
                  from table(cast(i_Customers_In_t as
                                  ttc_View_Customers_In_t))) LOOP
    BEGIN
      --Setea la bandera de validación de la configuración de los parámetros                                           
      Ln_Customer_id := rc_in.Customer_id;
      Ln_Co_id       := rc_in.Co_id;
      Ln_ConTrx      := Ln_ConTrx + 1;
      --Graba en la tabla ttc_contract_to_regularize 
      i_Cust_to_Save_In_t := ttc_View_Customers_In_type(customer_id          => rc_in.customer_id,
                                                        co_id                => rc_in.co_id,
                                                        dn_num               => rc_in.dn_num,
                                                        billcycle            => rc_in.billcycle,
                                                        custcode             => rc_in.custcode,
                                                        tmcode               => rc_in.tmcode,
                                                        prgcode              => rc_in.prgcode,
                                                        cstradecode          => rc_in.cstradecode,
                                                        PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                        CO_EXT_CSUIN         => rc_in.CO_EXT_CSUIN,
                                                        co_userlastmod       => nvl(rc_in.co_userlastmod,
                                                                                    'RELOJ-QC'),
                                                        ttc_insertiondate    => nvl(rc_in.ttc_insertiondate,
                                                                                    SYSDATE),
                                                      ttc_lastmoddate      => NULL);
                   
      Lv_Des_Error        := Fct_Save_ToWk_Regularize;
      IF Lv_Des_Error IS NOT NULL THEN
        RAISE Le_Error_Trx;
      END IF;
        
      IF Ln_ConTrx >= i_rule_element.Value_Max_TrxCommit THEN
        --***********--     
        COMMIT;
        --***********--
        -- SCP:AVANCE
        -----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de avance de proceso
        -----------------------------------------------------------------------
        ln_registros_procesados_scp := ln_registros_procesados_scp +
                                       i_rule_element.Value_Max_TrxCommit;
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              null,
                                              ln_error_scp,
                                              lv_error_scp);
                                                 
        -----------------------------------------------------------------------
        Ln_ConTrx := 0;
      END IF;
    EXCEPTION
      WHEN Le_Error_Trx THEN
        --*************
       --ROLLBACK;
        --*************
        PV_Error                    := 'Prc_To_Regularize_All-' ||
                                       Lv_Des_Error || ',' ||
                                       'customer_id:' || Ln_Customer_id ||
                                       ',Co_id:' || Ln_Co_id;
        i_Log_InOut_t.LastDate      := SYSDATE;
        i_Log_InOut_t.StatusProcess := 'ERR';
        i_Log_InOut_t.Comment_Run   := PV_Error;
        ----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp := ln_registros_error_scp + 1;
        lv_mensaje_apl_scp     := '          Error Validación: Prc_To_Regularize_All-Procesando Cuenta';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              PV_Error,
                                              Null,
                                              2,
                                              Null,
                                              'Customer_id',
                                              'Co_id',
                                              Ln_Customer_id,
                                              Ln_Co_id,
                                              'N',
                                              ln_error_scp,
                                              lv_error_scp);
          
        ----------------------------------------------------------------------------
        Lv_Des_Error := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
      WHEN OTHERS THEN
        PV_Error                    := 'Prc_To_Regularize_All-Error al momento de ingresar los registros a la tabla de regularización';
        i_Log_InOut_t.LastDate      := SYSDATE;
        i_Log_InOut_t.StatusProcess := 'ERR';
        i_Log_InOut_t.Comment_Run   := PV_Error;
        Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
        ----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := '          Error general: Prc_To_Regularize_All-Procesando Cuenta';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              PV_Error,
                                              Null,
                                              2,
                                              Null,
                                              Null,
                                              Null,
                                              null,
                                              null,
                                              'N',
                                              ln_error_scp,
                                              lv_error_scp);
          
    END;
  END LOOP;
  --***********--     
  COMMIT; 
  --***********--                     
  ---------------------------------------------------------------------
  -- SCP:AVANCE
  IF Ln_ConTrx > 0 THEN
    ln_registros_procesados_scp := ln_registros_procesados_scp +
                                   Ln_ConTrx;
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  END IF;
  ----------------------------------------------------------------------------
   --exit when i_Customers_In_t.count=0; 
  EXIT WHEN C_REGULARIZE_IN2%NOTFOUND;
END LOOP;
CLOSE C_REGULARIZE_IN2;
/* 8250 MMC - FIN */       
ELSE
                                   
   OPEN C_REGULARIZE_IN;
   LOOP   
   FETCH C_REGULARIZE_IN
   BULK COLLECT INTO i_Customers_In_t LIMIT i_rule_element.Value_Max_Trx_By_Execution; 
   exit when i_Customers_In_t.count=0;                                                        
   Ln_ConTrx:=0;
             FOR rc_in IN (select * from table(cast( i_Customers_In_t as reloj.ttc_View_Customers_In_t)))  LOOP
                 BEGIN
                       --Setea la bandera de validación de la configuración de los parámetros                                           
                        Ln_Customer_id:=rc_in.Customer_id;
                        Ln_Co_id:=rc_in.Co_id;                     
                        Ln_ConTrx:=Ln_ConTrx+1;
                         --Graba en la tabla ttc_contract_to_regularize 
                          i_Cust_to_Save_In_t:=reloj.ttc_View_Customers_In_type(customer_id => rc_in.customer_id, 
                                                                                co_id => rc_in.co_id,
                                                                                dn_num => rc_in.dn_num,
                                                                                billcycle => rc_in.billcycle,
                                                                                custcode => rc_in.custcode,
                                                                                tmcode => rc_in.tmcode,
                                                                                prgcode => rc_in.prgcode,
                                                                                cstradecode => rc_in.cstradecode,
                                                                                PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                                                CO_EXT_CSUIN => rc_in.CO_EXT_CSUIN,
                                                                                co_userlastmod => nvl(rc_in.co_userlastmod,'RELOJ-QC'),
                                                                                ttc_insertiondate => nvl(rc_in.ttc_insertiondate,SYSDATE),
                                                                                ttc_lastmoddate => NULL
                                                                                );
                           Lv_Des_Error:=Fct_Save_ToWk_Regularize;                  
                           IF Lv_Des_Error IS NOT NULL THEN
                                    RAISE Le_Error_Trx;
                           END IF;                                                                                                                           
                                                                                            
                         IF Ln_ConTrx>=i_rule_element.Value_Max_TrxCommit THEN
                               --***********--     
                                COMMIT;
                                --***********--
                                 -- SCP:AVANCE
                                 -----------------------------------------------------------------------
                                 -- SCP: Código generado automáticamente. Registro de avance de proceso
                                 -----------------------------------------------------------------------
                                 ln_registros_procesados_scp:=ln_registros_procesados_scp + i_rule_element.Value_Max_TrxCommit;
                                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                       ln_registros_procesados_scp,
                                                                       null,
                                                                       ln_error_scp,
                                                                       lv_error_scp);
                                 -----------------------------------------------------------------------
                                 Ln_ConTrx:=0;
                         END IF;                       
 EXCEPTION            
     WHEN Le_Error_Trx THEN
          --*************
          --ROLLBACK;  
          --*************
          PV_Error:='Prc_To_Regularize_All-'||Lv_Des_Error||','||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id;                                                                   
          i_Log_InOut_t.LastDate:=SYSDATE;
          i_Log_InOut_t.StatusProcess:='ERR';
          i_Log_InOut_t.Comment_Run:=  PV_Error;                      
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
           ----------------------------------------------------------------------
           ln_registros_error_scp:=ln_registros_error_scp+1;
           lv_mensaje_apl_scp:='          Error Validación: Prc_To_Regularize_All-Procesando Cuenta';
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 PV_Error,
                                                 Null,
                                                 2,
                                                 Null,'Customer_id','Co_id',
                                                 Ln_Customer_id,Ln_Co_id,'N',ln_error_scp,lv_error_scp);
                                                        
           ----------------------------------------------------------------------------
          Lv_Des_Error:= reloj.RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
     WHEN OTHERS THEN                                               
           PV_Error:='Prc_To_Regularize_All-Error al momento de ingresar los registros a la tabla de regularización';                                                                   
            i_Log_InOut_t.LastDate:=SYSDATE;
            i_Log_InOut_t.StatusProcess:='ERR';
            i_Log_InOut_t.Comment_Run:=  PV_Error;                      
            Lv_Des_Error:= reloj.RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
             ----------------------------------------------------------------------
             lv_mensaje_apl_scp:='          Error general: Prc_To_Regularize_All-Procesando Cuenta';
             scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                   lv_mensaje_apl_scp,
                                                                                   PV_Error,
                                                                                   Null,
                                                                                   2,
                                                                                   Null,Null,Null,
                                                                                   null,null,'N',ln_error_scp,lv_error_scp
                                                                                   );                                                       
             ----------------------------------------------------------------------------
                 END;      
             END LOOP;     
               --***********--     
                 COMMIT;
               --***********--                     
                 ---------------------------------------------------------------------
                 -- SCP:AVANCE
                 IF Ln_ConTrx>0 THEN
                       ln_registros_procesados_scp:=ln_registros_procesados_scp + Ln_ConTrx;
                       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,
                                                     ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 END IF;                              
                 ----------------------------------------------------------------------------
                EXIT WHEN C_REGULARIZE_IN%NOTFOUND;                                    
           END LOOP;        
           CLOSE C_REGULARIZE_IN;
END IF;

/* 8250 MMC - FIN */       
           ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje de error
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='FIN Prc_To_Regularize_All '||lv_id_proceso_scp;
           IF nvl(PV_BANDERA, 'N')='S'THEN
             lv_mensaje_tec_scp:='Grupo_Hilo: '||PN_HILO||' Subhilo: '||PN_GRUPOHILO||' >> '||PV_FILTRO;
       
           else
             lv_mensaje_tec_scp:='Prc_To_Regularize_All- Ejecutado con Exito';
           end if;
                  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                        lv_mensaje_apl_scp,
                                                        lv_mensaje_tec_scp,
                                                        Null,
                                                        0,
                                                        lv_unidad_registro_scp,
                                                        Null,
                                                        Null,
                                                        null,
                                                        null,
                                                        'N',
                                                        ln_error_scp,
                                                        lv_error_scp);	
----------------------------------------------------------------------
--SCP:FIN                          
           PV_Error:=NULL;
EXCEPTION            
       WHEN OTHERS THEN        
           ROLLBACK;                                
          PV_Error:='Prc_To_Regularize_All-Error en la ejecución del proceso'||substr(SQLERRM,1,500);                     
                                             
END Prc_To_Regularize_All;

/************************************************************************************
 8693 JJI
 Procedimiento encargado de identificar las cuentas dth que deben estar planificadas en
 ttc_contractplannigdetails
*************************************************************************************/ 

PROCEDURE Prc_To_Regularize_All_Dth(PN_HILO      IN NUMBER,
                                    PN_GRUPOHILO in varchar2,
                                    PV_BANDERA   IN VARCHAR2,
                                    PV_Error     OUT VARCHAR2) IS
  
  Lv_Des_Error                      VARCHAR2(800);
  Le_Error_Valid                    EXCEPTION;
  Ln_ConTrx                         NUMBER;
  Le_Error_Trx                      EXCEPTION;
  Ln_Customer_id                    NUMBER;
  Ln_Co_id                          NUMBER;
  LN_R1                             NUMBER := 0;
  LN_R2                             NUMBER := 0;
  lv_mensaje_tec_scp                varchar2(4000);
  le_error                          exception;
  
  NUMERO                            NUMBER;

BEGIN
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO Prc_To_Regularize_All_Dth: ' ||lv_id_proceso_scp;

  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        lv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        NULL,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
                                        
 
  IF nvl(PV_BANDERA, 'N') = 'S' THEN
  
    LN_R1 := SUBSTR(PN_GRUPOHILO, 1, 1);
    LN_R2 := SUBSTR(PN_GRUPOHILO, -1, 1);
  
    OPEN C_REGULARIZE_DTH_2(LN_R1, LN_R2);
    LOOP
      FETCH C_REGULARIZE_DTH_2 BULK COLLECT INTO i_Customers_In_t LIMIT i_rule_element.Value_Max_Trx_By_Execution;
      exit when i_Customers_In_t.count = 0;
      Ln_ConTrx := 0;
      FOR rc_in IN (select * from table(cast(i_Customers_In_t as ttc_View_Customers_In_t))) LOOP
        BEGIN
          --Setea la bandera de validación de la configuración de los parámetros                                           
          Ln_Customer_id := rc_in.Customer_id;
          Ln_Co_id       := rc_in.Co_id;
          Ln_ConTrx      := Ln_ConTrx + 1;
          --Graba en la tabla ttc_contract_to_regularize 
          i_Cust_to_Save_In_t := ttc_View_Customers_In_type(customer_id          => rc_in.customer_id,
                                                            co_id                => rc_in.co_id,
                                                            dn_num               => rc_in.dn_num,
                                                            billcycle            => rc_in.billcycle,
                                                            custcode             => rc_in.custcode,
                                                            tmcode               => rc_in.tmcode,
                                                            prgcode              => rc_in.prgcode,
                                                            cstradecode          => rc_in.cstradecode,
                                                            PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                            CO_EXT_CSUIN         => rc_in.CO_EXT_CSUIN,
                                                            co_userlastmod       => nvl(rc_in.co_userlastmod,'RELOJ-QC'),
                                                            ttc_insertiondate    => nvl(rc_in.ttc_insertiondate,SYSDATE),
                                                            ttc_lastmoddate      => NULL);
        
          Lv_Des_Error := Fct_Save_ToWk_Regu_Dth;
          IF Lv_Des_Error IS NOT NULL THEN
            RAISE Le_Error_Trx;
          END IF;
          
          ln_registros_procesados_scp := ln_registros_procesados_scp  + 1;
          IF Ln_ConTrx >= i_rule_element.Value_Max_TrxCommit THEN
            -- SCP:AVANCE
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                  ln_registros_procesados_scp,
                                                  ln_registros_error_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
          
            -----------------------------------------------------------------------
            --***********--     
            COMMIT;
            --***********--
            Ln_ConTrx := 0;
          END IF;
        EXCEPTION
          WHEN Le_Error_Trx THEN
       
            PV_Error := Lv_Des_Error || ',' ||'customer_id:' || Ln_Customer_id ||',Co_id:' || Ln_Co_id;
            i_Log_InOut_t.LastDate      := SYSDATE;
            i_Log_InOut_t.StatusProcess := 'ERR';
            i_Log_InOut_t.Comment_Run   := PV_Error;
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '          Error Validación: Prc_To_Regularize_All_Dth-Procesando Cuenta';
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  PV_Error,
                                                  Null,
                                                  2,
                                                  Null,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
          
            ----------------------------------------------------------------------------
            Lv_Des_Error := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
          WHEN OTHERS THEN
            PV_Error                    := 'Error al momento de ingresar la cuenta a la tabla TTC_CONTRACT_TO_REGU_DTH >> customer_id:' || Ln_Customer_id ||',Co_id:' || Ln_Co_id ||', csuin:'||rc_in.CO_EXT_CSUIN ;
            i_Log_InOut_t.LastDate      := SYSDATE;
            i_Log_InOut_t.StatusProcess := 'ERR';
            i_Log_InOut_t.Comment_Run   := PV_Error;
            Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp := '          Error general: Prc_To_Regularize_All_Dth-Procesando Cuenta';
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  PV_Error,
                                                  Null,
                                                  2,
                                                  Null,
                                                  Null,
                                                  Null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
          
        END;
      END LOOP;
      --***********--     
      COMMIT;
      --***********--
      EXIT WHEN C_REGULARIZE_DTH_2%NOTFOUND;
    END LOOP;
    CLOSE C_REGULARIZE_DTH_2;

  ELSE
  
    OPEN C_REGULARIZE_DTH;
    LOOP
      FETCH C_REGULARIZE_DTH BULK COLLECT INTO i_Customers_In_t LIMIT i_rule_element.Value_Max_Trx_By_Execution;
      exit when i_Customers_In_t.count = 0;
      Ln_ConTrx := 0;
      FOR rc_in IN (select * from table(cast(i_Customers_In_t as ttc_View_Customers_In_t))) LOOP
        BEGIN
          --Setea la bandera de validación de la configuración de los parámetros                                           
          Ln_Customer_id := rc_in.Customer_id;
          Ln_Co_id       := rc_in.Co_id;
          Ln_ConTrx      := Ln_ConTrx + 1;
          --Graba en la tabla ttc_contract_to_regu_dth 
          i_Cust_to_Save_In_t := ttc_View_Customers_In_type(customer_id          => rc_in.customer_id,
                                                            co_id                => rc_in.co_id,
                                                            dn_num               => rc_in.dn_num,
                                                            billcycle            => rc_in.billcycle,
                                                            custcode             => rc_in.custcode,
                                                            tmcode               => rc_in.tmcode,
                                                            prgcode              => rc_in.prgcode,
                                                            cstradecode          => rc_in.cstradecode,
                                                            PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                            CO_EXT_CSUIN         => rc_in.CO_EXT_CSUIN,
                                                            co_userlastmod       => nvl(rc_in.co_userlastmod,'RELOJ-QC'),
                                                            ttc_insertiondate    => nvl(rc_in.ttc_insertiondate,SYSDATE),
                                                            ttc_lastmoddate      => NULL);
        
          Lv_Des_Error := Fct_Save_ToWk_Regu_Dth;
          IF Lv_Des_Error IS NOT NULL THEN
            RAISE Le_Error_Trx;
          END IF;
          
          ln_registros_procesados_scp := ln_registros_procesados_scp  + 1;
          IF Ln_ConTrx >= i_rule_element.Value_Max_TrxCommit THEN
            -- SCP:AVANCE
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                  ln_registros_procesados_scp,
                                                  ln_registros_error_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
          
            -----------------------------------------------------------------------
            --***********--     
            COMMIT;
            --***********--
            Ln_ConTrx := 0;
          END IF;
        EXCEPTION
          WHEN Le_Error_Trx THEN
            --*************
            --ROLLBACK;
            --*************
            PV_Error := Lv_Des_Error || ',' ||'customer_id:' || Ln_Customer_id ||',Co_id:' || Ln_Co_id;
            i_Log_InOut_t.LastDate      := SYSDATE;
            i_Log_InOut_t.StatusProcess := 'ERR';
            i_Log_InOut_t.Comment_Run   := PV_Error;
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '          Error Validación: Prc_To_Regularize_All-Procesando Cuenta';
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  PV_Error,
                                                  Null,
                                                  2,
                                                  Null,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
          
            ----------------------------------------------------------------------------
            Lv_Des_Error := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
          WHEN OTHERS THEN
            PV_Error                    := 'Error al momento de ingresar la cuenta a la tabla TTC_CONTRACT_TO_REGU_DTH >> customer_id:' || Ln_Customer_id ||',Co_id:' || Ln_Co_id ||', csuin:'||rc_in.CO_EXT_CSUIN ;
            i_Log_InOut_t.LastDate      := SYSDATE;
            i_Log_InOut_t.StatusProcess := 'ERR';
            i_Log_InOut_t.Comment_Run   := PV_Error;
            Lv_Des_Error                := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp := '          Error general: Prc_To_Regularize_All-Procesando Cuenta';
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  PV_Error,
                                                  Null,
                                                  2,
                                                  Null,
                                                  Null,
                                                  Null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
          
        END;
      END LOOP;
     --***********--     
      COMMIT;
      --***********--
      EXIT WHEN C_REGULARIZE_DTH%NOTFOUND;
    END LOOP;
    CLOSE C_REGULARIZE_DTH;
  END IF;
 --***********--     
  COMMIT;
  --***********--                     

  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN Prc_To_Regularize_All: ' || lv_id_proceso_scp;
  lv_mensaje_tec_scp := 'Prc_To_Regularize_All_Dth- Ejecutado con Exito';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        lv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------
  --SCP:FIN                          
  PV_Error := NULL;
EXCEPTION
  WHEN OTHERS THEN
    PV_Error := 'Prc_To_Regularize_All_Dth-Error en la ejecución del proceso, ' ||substr(SQLERRM, 1, 500);
  
END Prc_To_Regularize_All_Dth;

--**********************************************************************************
/* 8250 MMC - INICIO
  Procemiento encargado de insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha de  
  finalizacion del proceso
  8250 FIN 
*/ 
PROCEDURE Prc_Insert_Programmer(PV_DESSHORT     IN VARCHAR2,--8693 JJI
                                PN_PROCESS      IN NUMBER,--8693 JJI
                                PV_ERROR        OUT VARCHAR2) IS   
   
CURSOR C_Trx_In IS
 SELECT  TTC_TRX_IN.NEXTVAL FROM dual;  
 
--8693 jji
CURSOR C_Trx_In_Dth IS
  SELECT TTC_TRX_IN_DTH.NEXTVAL FROM dual;
--8693 jji
	
i_Process_Programmer_t		ttc_Process_Programmer_t;
Le_Error            EXCEPTION;
id_process					number;
Ld_SysdateRun    		DATE;
Ln_Seqno					  NUMBER;
Lv_Des_Error				VARCHAR2(800);                      
   
BEGIN
     
--8693 jji
IF PV_DESSHORT='TTC_IN_TO_REGU' THEN
     
  OPEN C_Trx_In ;
  FETCH C_Trx_In INTO Ln_Seqno;
  CLOSE C_Trx_In; 
--8693 jji   
ELSE
  OPEN C_Trx_In_Dth ;
  FETCH C_Trx_In_Dth INTO Ln_Seqno;
  CLOSE C_Trx_In_Dth; 
END IF;    
 

Ld_SysdateRun:=SYSDATE;
	
id_process:=rc_api_timetocash_rule_bscs.rc_retorna_id_process(PV_DESSHORT);
    
i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => nvl(id_process,PN_PROCESS),
                                                       Seqno => Ln_Seqno,
                                                       RunDate => Ld_SysdateRun,
                                                       LastModDate => Ld_SysdateRun
                                                       );
													
Lv_Des_Error:=RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
IF Lv_Des_Error IS NOT NULL THEN
   RAISE Le_Error;
END IF;    
     
PV_ERROR:=NULL;
      
EXCEPTION
 WHEN Le_Error THEN
   PV_ERROR:=Lv_Des_Error;
 WHEN OTHERS THEN
   PV_ERROR:='Prc_Insert_Programmer-Error en la ejecución : '||substr(SQLERRM,1,500);        
                         
END Prc_Insert_Programmer;

PROCEDURE Prc_Insert_Programmer_hasta(Ld_SysdateRun  IN varchar2,
                                       PV_ERROR   OUT VARCHAR2) IS   
   
CURSOR C_Trx_In IS
 SELECT  reloj.TTC_TRX_IN.NEXTVAL FROM dual;

CURSOR C_max_programmer IS 
select max(lastmoddate) from reloj.ttc_process_programmer where process =13;
	
i_Process_Programmer_t		reloj.ttc_Process_Programmer_t;
Le_Error            EXCEPTION;
id_process					number;
Ln_Seqno					  NUMBER;
Ld_programmer       DATE;
Lv_Des_Error				VARCHAR2(800);                      
   
BEGIN
     
OPEN C_Trx_In ;
FETCH C_Trx_In INTO Ln_Seqno;
CLOSE C_Trx_In;     

OPEN C_max_programmer ;
FETCH C_max_programmer INTO Ld_programmer;
CLOSE C_max_programmer;      

if (nvl(Ld_programmer,to_date('01/01/1990 00:00:00','dd/mm/yyyy hh24:mi:ss') ) <= to_date(Ld_SysdateRun,'dd/mm/yyyy hh24:mi:ss') -1 ) then
    
i_Process_Programmer_t:=reloj.ttc_Process_Programmer_t(Process => 13,
                                                       Seqno => Ln_Seqno,
                                                       RunDate => to_date(Ld_SysdateRun,'dd/mm/yyyy hh24:mi:ss') -1,
                                                       LastModDate => to_date(Ld_SysdateRun,'dd/mm/yyyy hh24:mi:ss') -1
                                                       );
													
Lv_Des_Error:=reloj.RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
IF Lv_Des_Error IS NOT NULL THEN
   RAISE Le_Error;
END IF;  
commit;  
END IF;     
PV_ERROR:=NULL;
      
EXCEPTION
 WHEN Le_Error THEN
   PV_ERROR:=Lv_Des_Error;
 WHEN OTHERS THEN
   PV_ERROR:='Prc_Insert_Programmer_hasta-Error en la ejecución : '||substr(SQLERRM,1,500);        
                         
END Prc_Insert_Programmer_hasta;

--10536 INI HRO
PROCEDURE PRC_RE_PLANNIG(PN_CUSTOMER IN NUMBER,
                         PV_DN_NUM   IN VARCHAR2,
                         PN_CO_ID    NUMBER,
                         PN_PK_ID    NUMBER,
                         PN_ERROR    OUT NUMBER,
                         PV_ERROR    OUT VARCHAR2) IS

  TYPE T_PLANNIG_STATUS is record(
    PK_ID             number,
    ST_SEQNO          number,
    ST_STATUS_NEXT    number,
    ST_DURATION_TOTAL number,
    ST_REASON         varchar2(5),
    RE_ORDEN          number,
    STATUS_A          number);

  TYPE T_V_PLANNIG is table of T_PLANNIG_STATUS;
  LC_PLANNIG_STATUS T_V_PLANNIG;

  CURSOR C_PLANNIG_STATUS(CN_PK_ID NUMBER) IS
    SELECT PK_ID,
           ST_SEQNO,
           ST_STATUS_NEXT,
           ST_DURATION_TOTAL,
           ST_REASON,
           RE_ORDEN,
           ST_STATUS_A
      FROM RELOJ.TTC_VIEW_PLANNIGSTATUS_IN a
     WHERE PK_ID = CN_PK_ID 
      AND RE_ORDEN >1
     ORDER BY A.st_duration_total ASC;

  CURSOR C_PLANNIG_DETAILS IS
    SELECT PK_ID,
           ST_SEQNO,
           CO_PERIOD,
           CO_STATUS_A,
           CO_REASON,
           a.customer_id,
           a.dn_num,
           a.co_id,
           A.CH_TIMEACCIONDATE
      FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS a
     WHERE /*CO_STATUSTRX <> 'F'
       AND */CO_ID=PN_CO_ID --AAM
       AND CUSTOMER_ID = PN_CUSTOMER
       AND DN_NUM like('%'||PV_DN_NUM)
      and a.re_orden >0
     ORDER BY CH_TIMEACCIONDATE ASC;
  
      
                     
  LN_CONTADOR NUMBER := 0;
  LD_FECHA    DATE;
  ln_status_pla  number;
  ln_status_vis  number;
  LV_ERROR    VARCHAR2(500);
  lv_Numero   varchar2(20);
  ln_dia_fin_mes   number:=0;
  ld_fecha_fin_mes date;
  lv_band_inact    varchar2(2):= 'S';
  --INI [12018] IRO ABA 
  CURSOR C_PARAMETROS(Cn_IdTipoParametro NUMBER, Cv_IdParametro VARCHAR2) IS
  SELECT G.VALOR
    FROM SYSADM.GV_PARAMETROS G
   WHERE G.ID_TIPO_PARAMETRO = Cn_IdTipoParametro
     AND G.ID_PARAMETRO = Cv_IdParametro;
  
  Ln_DiasInactivarServ   NUMBER;
  --FIN [12018] IRO ABA 

BEGIN

  OPEN C_PLANNIG_STATUS(PN_PK_ID);
  FETCH C_PLANNIG_STATUS BULK COLLECT
    INTO LC_PLANNIG_STATUS;
  CLOSE C_PLANNIG_STATUS;
  
  SELECT R.VALOR into lv_band_inact
    FROM GV_PARAMETROS R
   WHERE ID_TIPO_PARAMETRO = 11603
     AND ID_PARAMETRO = 'REP_BAND_ESTAT_35';
  
lv_Numero:='''%'||PV_DN_NUM||'''';
  --INI [12018] IRO ABA 
  OPEN C_PARAMETROS(12018, 'DIAS_INACT_RELOJ_MOVIL');
  FETCH C_PARAMETROS INTO Ln_DiasInactivarServ;
  CLOSE C_PARAMETROS;
  --FIN [12018] IRO ABA 
  for p in C_PLANNIG_DETAILS loop
    IF LN_CONTADOR = 0 THEN
      LD_FECHA := P.CH_TIMEACCIONDATE;
      --10995 ini AAM
        update RELOJ.TTC_CONTRACTPLANNIGDETAILS a
         set a.co_period = '2', a.co_status_a='34'
         where a.customer_id =p.customer_id 
         and a.dn_num=p.dn_num 
         and a.co_id = p.co_id
         and a.co_status_a = '80';
      --10995 fin AAM
    END IF;
    if LN_CONTADOR > 0 then
      for v in 1 .. LC_PLANNIG_STATUS.count loop
        ln_status_pla := p.co_status_a;
        ln_status_vis := LC_PLANNIG_STATUS(v).status_A;
      
        if p.co_status_a = LC_PLANNIG_STATUS(v).status_A then

          LD_FECHA := TO_DATE(LD_FECHA, 'DD/MM/RRRR') + LC_PLANNIG_STATUS(v).ST_DURATION_TOTAL;
          --INI 11603 AAM
           if NVL(lv_band_inact,'N') = 'S' then
              if p.co_status_a = '35' then
                 ld_fecha_fin_mes:=last_day(ld_fecha)-NVL(Ln_DiasInactivarServ,2);
                 Ln_dia_fin_mes:= ld_fecha_fin_mes - ld_fecha;
                 LD_FECHA:=ld_fecha_fin_mes;
              End if;
           end if;
          --FIN 11603 AAM

          update RELOJ.TTC_CONTRACTPLANNIGDETAILS a
             set a.pk_id=LC_PLANNIG_STATUS(v).pk_id,
                 a.st_seqno=LC_PLANNIG_STATUS(v).ST_SEQNO,
                 a.co_period = LC_PLANNIG_STATUS(v).ST_STATUS_NEXT,
                 a.co_timeperiod = (LC_PLANNIG_STATUS(v).ST_DURATION_TOTAL)+Ln_dia_fin_mes,
                 a.co_reason     = LC_PLANNIG_STATUS(v).ST_REASON,
                 a.re_orden=LC_PLANNIG_STATUS(v).RE_ORDEN,
                 A.CH_TIMEACCIONDATE = LD_FECHA
             where p.pk_id = a.pk_id
             and  a.customer_id =p.customer_id 
             and a.dn_num=p.dn_num 
             and a.co_id = p.co_id
             and a.co_status_a=p.co_status_a;
        end if;
      -- EXIT WHEN LN_CONTADOR=0;                                                                                                                                                                                                                                                          
      end loop;
    end if;
    LN_CONTADOR := LN_CONTADOR + 1;
    COMMIT;
  end loop;
  PV_ERROR:='OK';
  PN_ERROR:=0;
 EXCEPTION
  WHEN OTHERS THEN
   LV_ERROR:='PRC_RE_PLANNIG: '||substr(SQLERRM,1,500);
   PV_ERROR:=LV_ERROR;
   PN_ERROR:=1;  
   Rollback;
END PRC_RE_PLANNIG;
--10536 FIN HRO
END RC_TRX_IN_TO_REGULARIZE;
/
