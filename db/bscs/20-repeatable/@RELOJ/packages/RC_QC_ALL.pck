create or replace package reloj.RC_QC_ALL is

  -- Author  : Katty Mite
  -- Created : 06/12/2010 12:13:53
  -- Purpose : Procesos de Mensajeria
  --VARIABLES GLOBALES
GV_DESSHORT VARCHAR2(14) := 'TTC_QC_SUSP_1';
GV_DESSHORT_P4 VARCHAR2(12) := 'TTC_DISPTC';
GV_DESSHORT_P5 VARCHAR2(12) := 'TTC_SENDRETR';
Gv_unidad_registro_scp varchar2(30):='Resumenes';
gv_value_rule_string_cash varchar2(40):='TTC_FLAG_CUSTOMER_CASH_DIA';
gv_value_rule_string_out varchar2(30):='TTC_FLAG_CUSTOMER_OUT_RELOJ';
GV_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_CURSOR_CUSTCODE';
GV_STATUS_P   VARCHAR2(1) := 'P';

GV_TTC_RANGE_HOUR_FULL     VARCHAR2(30) := 'TTC_RANGE_HOUR_FULL';
GV_TTC_RANGE_HOUR_SEMIFULL VARCHAR2(30) := 'TTC_RANGE_HOUR_SEMIFULL';
GV_TTC_RANGE_HOUR_LOW      VARCHAR2(30) := 'TTC_RANGE_HOUR_LOW';
GV_TTC_MAX_RESTRICT_TIME   VARCHAR2(30) := 'TTC_MAX_RESTRICT_TIME';

gv_hora_ini VARCHAR2(30);
gv_hora_fin VARCHAR2(30);

t_RangeTime_Restrict               ttc_Rule_Values_t;
t_RangeTime_Full_p4                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p4            ttc_Rule_Values_t;
t_RangeTime_Low_p4                 ttc_Rule_Values_t;
t_RangeTime_Select                 ttc_Rule_Values_t;

t_RangeTime_Full_p5                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p5            ttc_Rule_Values_t;
t_RangeTime_Low_p5                 ttc_Rule_Values_t;

i_Process_Programmer_t       ttc_Process_Programmer_t;
GV_DESSHORT_LOW             VARCHAR2(15) := 'TTC_REAC_LOW';
GV_DESSHORT_RETRY_SEMI_FULL VARCHAR2(16) := 'TTC_REAC_SFUL';

gn_secuencia                NUMBER;

procedure prc_qc_notify_1 (pv_error out varchar2);
end RC_QC_ALL;
/
create or replace package body reloj.RC_QC_ALL is


FUNCTION Fct_Verifica_Resumen_QC_Susp(Pn_id_qc NUMBER,Pv_desshort VARCHAR2, Pv_estado VARCHAR2) RETURN TTC_Suspension_Resumen_t as
     CURSOR cur_in IS
           SELECT VALUE(h)
             FROM TTC_Suspension_Resumen_s  h
          WHERE id_qc  = Pn_id_qc
                AND desshort = Pv_desshort
                AND estado = Pv_estado;
      doc_in   TTC_Suspension_Resumen_t;          
BEGIN
      OPEN cur_in;
      FETCH cur_in INTO doc_in;
      CLOSE cur_in;
      RETURN doc_in;
END;



  procedure prc_qc_notify_1 (pv_error out varchar2)is
 
---------------------------------------------------------------
--ln_tope1                            NUMBER;
--ln_tope2                            NUMBER;
--lv_recomendacion1                   VARCHAR2(5000);
--lv_recomendacion2                   VARCHAR2(5000);
ln_error                           NUMBER;
---------------------------------------------------------------
  cursor c_mensaje is
   select DESCRIPCION,
          TOTAL 
          from axis_view_rc_qc_all;
   lv_mensaje_mail  varchar2(15000):='';
   LV_ASUNTO_MAIL   varchar2(500);
   --ln_error         number;
  
   
   rg_registro      c_mensaje%rowtype;
     
   
  begin
    LV_ASUNTO_MAIL:='mail de prueba';
    lv_mensaje_mail:='<html>';
    --lv_mensaje_mail_1:=lv_mensaje_mail||'</html>';
    
    open c_mensaje;
    loop
      fetch c_mensaje into rg_registro;      
      exit when c_mensaje%notfound;
     lv_mensaje_mail:=lv_mensaje_mail||rg_registro.descripcion||' '||rg_registro.total||'<br>';
    end loop;
    close c_mensaje;
    lv_mensaje_mail:=lv_mensaje_mail||'    Recomendaciones:<br>
<dir>- No Bajar los procesos del Reloj.<br></dir>
<dir>- No realizar pases que intervengan con el proceso.<br></dir>
<dir>- Verificar que las IDL de tecnotree se encuentren levantadas.<br></dir>
<dir>- Monitorear el avance del proceso, antes y despu�s del Reinicio de Axis. Ejecutar el Query en BSCSPROD:<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>select</b> descripcion_estado,valor,estado <b>from</b> reloj.TTC_VIEW_QC_COLAS_AXIS;</table bgcolor></dir></dir>
<dir>- Verificar la CMS_MASIVO, que se encuentre levantada y procesando. IP: 130.2.120.10 los directorios C:\CMS_MASIVO\SERVER\RELOJ\1,.. \2,.. \3,.. \4,.. \5<br></dir>
<dir> Si es preciso reiniciarla antes de la ejecuci�n del proceso del reloj.<br></dir>
<dir>- Si se mantiene la cola de pendientes en la vista "reloj.ttc_view_qc_colas_axis", es preciso bajar los procesos del reloj en bscsprod "delete reloj.ttc_bck_process where process in (4,5)", detener los jobs Dispatch y Retry y luego reiniciar los procesos de AXIS. Subir los procesos de AXIS y luego ejecutar los Jobs Dispatch y Retry.</dir>
<dir>- Si las colas en la CMS_MASIVO, no procesa, es decir de las colas C:\CMS_MASIVO\SERVER\RELOJ\ ..\1,..\2,..\10,...no disminuyen los archivos ".xml", es preciso reiniciar las CMS  incluyendo la CMS_MASIVO, y luego levantar primero la CMS masivo, y eliminar los archivos de control  "C:\CMS_MASIVO\SERVER\CTRL\*.ctl"</dir>
 
  Recomendaciones:<br>
<dir>- No Bajar los procesos del Reloj.<br></dir>
<dir>- No realizar pases que intervengan con el proceso RC_RELOJ_COBRANZAS_NEW, GCK_TRX_ESTADOS_SERVCIOS.<br></dir>
<dir>- - Verificar las trx en el espejo.<br></dir>
<dir>- Monitorear el avance del proceso en AXIS, con los siguientes Query::<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>select</b> decode(rc.rc_reg_procesado,'''||'P'''||','''||'TRX Procesadas-->'''||','''||'E'''||','''||'TRX con Error-->'''||','''||'TRX Pendientes o para Revisi�n-->'''||'),COUNT(*) FROM rc_comentarios_cuenta rc WHERE rc.rc_cuenta>'''||'0'''||' AND rc.rc_fecha_mensaje = trunc(SYSDATE)-3 AND rc.rc_envio_mensaje='''||'M'''||' GROUP BY rc.rc_reg_procesado;</table bgcolor></dir></dir><br>
<dir><dir><table bgcolor="##FFFF00"><b>select</b> decode(dt.dt_estatus_bscs,'''||'P'''||','''||'Ok en BSCS'''||', '''||'E'''||','''||'Error en BSCS'''||','''||'Pendiente en BSCS'''||'),   decode(dt.dt_estatus_axis,'''||'P'''||','''||'Ok en AXIS'''||', '''||'E'''||','''||'Error en AXIS'''||','''||'Pendiente en AXIS'''||'),   decode(dt.dt_estatus_espejo,'''||'P'''||','''||'Ok en ESPEJO'''||', '''||'E'''||','''||'Error en ESPEJO'''||','''||'Pendiente en ESPEJO'''||'),COUNT(*)   FROM rc_detalle_transacciones dt  WHERE dt.dt_red='''||'1'''||' AND trunc(dt.dt_fecha)=trunc(SYSDATE)  AND dt.dt_usuario='''||'RELOJ'''||' GROUP BY dt.dt_estatus_bscs,dt.dt_estatus_axis,dt.dt_estatus_espejo;</table bgcolor></dir></dir>
<dir>- Las cuentas OK en Bscs, Axis, Espejo en la tabla rc_detalle_transacciones son las que se procesa con mensajer�a<br></dir>
<dir>- Al final del proceso si quedan cuentas pendientes de la tabla rc_comentarios_cuenta, en el campo rc_reg_procesado=null, verificar si las cuentas se encuentran con l�neas inactivas.<br></dir>';


    /********** invoca al procedimiento de envio de correo   *****************************/
    BEGIN
            reloj.RC_ENVIAR_CORREO(pv_servidor => 'conecel.com',--sisago.amp_enviar_correo@axis
                                                pv_de => 'reloj_cobranzas@conecel.com',
                                              --  pv_para => 'kmite@cimaconsulting.com.ec',--jronquillo@conecel.com',
                                               pv_para =>'kmite@cimaconsulting.com.ec', --'gsiprocesos@conecel.com',
                                                pv_cc =>'kmite@cimaconsulting.com.ec',----wcruz@cimaconsulting.com.ec --'vsoria@conecel.com;kcanizares@conecel.com;jronquillo@conecel.com',
                                                pv_asunto => LV_ASUNTO_MAIL,
                                                pv_mensaje => lv_mensaje_mail,
                                               pv_codigoerror => ln_error,
                                                pv_mensajeerror => pv_error);
     EXCEPTION
         WHEN OTHERS THEN
           NULL;                               
     END; 
     /****************************************************************************************/
  end prc_qc_notify_1;
end RC_QC_ALL;
/
