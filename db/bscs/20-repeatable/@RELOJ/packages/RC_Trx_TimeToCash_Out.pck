create or replace package RC_Trx_TimeToCash_Out is

  /*
   [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   23/11/2009
  * Modificado: 04/01/2010  -- Richard Tigrero
  */
  -----------------------------------------------------------------------
  GV_DESSHORT VARCHAR2(9) := 'TTC_OUT';

  --------------------------------------------------------------------------
  PROCEDURE RC_PROCESS_PRINCIPAL(PV_TIPO      VARCHAR2,
                                 PV_RULER     VARCHAR2,
                                 PN_SOSPID    IN NUMBER,
                                 PV_OUT_ERROR OUT VARCHAR2);

  PROCEDURE RC_DELETE_RULER_A(PN_ID_PROCESS      IN NUMBER,
                              PN_MAX_EXECUTE     IN NUMBER,
                              PN_MAX_COMMIT      IN NUMBER,
                              PN_ID_BITACORA_SCP IN NUMBER,
                              PN_SOSPID          IN NUMBER,
                              PV_OUT_ERROR       OUT VARCHAR2);
  PROCEDURE RC_DELETE_RULER_D(PN_ID_PROCESS      IN NUMBER,
                              PN_MAX_EXECUTE     IN NUMBER,
                              PN_MAX_COMMIT      IN NUMBER,
                              PN_ID_BITACORA_SCP IN NUMBER,
                              PN_SOSPID          IN NUMBER,
                              PV_OUT_ERROR       OUT VARCHAR2);
  PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_ID_PROCESS  IN NUMBER,
                                    PD_LASTMONDATE IN DATE,
                                    PV_OUT_ERROR   OUT VARCHAR2);

end RC_Trx_TimeToCash_Out;
/
create or replace package body RC_Trx_TimeToCash_Out is

  /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   23/11/2009
  * Modificado: 
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
  *       1.Ingreso Registro a la Programación del Reloj,
  ********2.Eliminar Registro de la Programación del Reloj*************   
  *       3.Envio de Transacciones a la cola de despacho
  *       4.Envio de Transacciones a procesar Axis desde la cola de despacho
  *********************************************************************************************************/

  /********************************************************************************************************/
  --====================================================================================--
  -- MODIFICADO POR: IRO MIGUEL ROSADO MRO(9833)
  -- FECHA MOD:      10/09/2014
  -- PROYECTO:       [9833] Optimización del proceso OUT_D
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         SE AGREGA VALIDACIONES PARA QUE BORRE LOS CO_IDs QUE SE ENCUENTRAN CON
  --                 CO_EXT_CSUIN = '5' EN LA TABLA TTC_CONTRACT_ALL Y LA VALIDACION DE LA
  --                 DEPURACION DE LA CABECERA EN LA TABLA TTC_CONTRACTPLANNIG             
  --=====================================================================================--
  --====================================================================================--
  -- MODIFICADO POR: IRO MIGUEL ROSADO MRO(9833)
  -- FECHA MOD:      17/12/2014
  -- PROYECTO:       [9833] Optimización del proceso OUT_A
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         SE AGREGA VALIDACIONES PARA QUE BORRE LOS CO_IDs QUE SE ENCUENTRAN CON
  --                 CO_EXT_CSUIN = '0' EN LA TABLA TTC_CONTRACT_ALL Y LA VALIDACION DE LA
  --                 DEPURACION DE LA CABECERA EN LA TABLA TTC_CONTRACTPLANNIG             
  --=====================================================================================--
  --====================================================================================--
  -- MODIFICADO POR: IRO ANABELL AMAYQUEMA AAM(10695)
  -- FECHA MOD:      10/02/2016
  -- PROYECTO:       [10695] Optimización del procesos OUTs
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Se reemplazan con nuevos cursores las consultas realizadas a las vistas 
  --                 TTC_VIEW_CUSTOMERS_D, TTC_VIEW_CUSTOMERS_A; porque no estan filtrando 
  --                 por un rango de fecha a pesar de que se ejecuten todos los días.
  --                 Se optimiza de depuracion de planificaciones.
  --=====================================================================================--
  --====================================================================================--
  -- MODIFICADO POR: IRO ANABELL AMAYQUEMA AAM(10695)
  -- FECHA MOD:      06/06/2016
  -- PROYECTO:       [10695] Planificaciones de serv inactivos no eliminados por OUTS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         El proceso Rc_Trx_Timetocash_Out.Rc_Delete_Ruler_D debe eliminar todas
  --                 las planificaciones de aquellos servicios que se encuentran con estatus 
  --                 de inactividad.
  --=====================================================================================--
  --====================================================================================--
  -- MODIFICADO POR: IRO ANABELL AMAYQUEMA
  -- FECHA MOD:      11/11/2016
  -- PROYECTO:       [10995] 10995 Mejoras al Proceso de Reloj de Cobranza
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Depuración de líneas que no presentan deuda.
  --=====================================================================================--
    --====================================================================================--
  -- MODIFICADO POR: IRO DIANA ZAMBRANO
  -- FECHA MOD:      16/06/2017
  -- PROYECTO:       [11346] ONE AMX Reloj de Estados postpago Reactivacion Renuncia
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Optimizacion de costos en el query que obtiene datos de reactivación
  --                 e inactivación.                             
  --=====================================================================================--

  PROCEDURE RC_PROCESS_PRINCIPAL(PV_TIPO      VARCHAR2,
                                 PV_RULER     VARCHAR2,
                                 PN_SOSPID    IN NUMBER,
                                 PV_OUT_ERROR OUT VARCHAR2) IS
  
    LV_MENSAJE VARCHAR2(2000);
    LV_STATUS  VARCHAR2(3);
    LE_ERROR          EXCEPTION;
    LE_ERROR_ABORTADO EXCEPTION;
    LD_FECHA_ACTUAL DATE;
    LD_FECHA_CONFIG DATE;
    LN_ID_PROCESS   NUMBER;
    LV_TIPO_PROCESS VARCHAR2(30);
  
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp     number := 0;
    ln_total_registros_scp number := 0;
    lv_id_proceso_scp      varchar2(100);
    lv_referencia_scp      varchar2(100) := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_';
    lv_unidad_registro_scp varchar2(30) := 'Cuentas';
    ln_error_scp           number := 0;
    lv_error_scp           varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    ---------------------------------------------------------------
    LN_MAX_EXECUTE            NUMBER;
    LN_MAXRETENTRX            NUMBER;
    LN_MAX_COMMIT             NUMBER;
    LN_value_max_nointento    NUMBER;
    LN_value_max_trx_encolada NUMBER;
  
  BEGIN
    LV_STATUS         := 'OK';
    LD_FECHA_ACTUAL   := SYSDATE;
    GV_DESSHORT       := GV_DESSHORT || PV_RULER;
    lv_id_proceso_scp := GV_DESSHORT;
    LN_ID_PROCESS     := RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_ID_PROCESS(GV_DESSHORT);
  
    IF PV_TIPO IS NULL THEN
      LD_FECHA_CONFIG := RC_Api_TimeToCash_Rule_ALL.Fct_Planning_Hour_DateValid(LN_ID_PROCESS);
      IF LD_FECHA_CONFIG IS NULL THEN
        LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Planning_Hour, para ejecutar el día de Hoy';
        RAISE Le_Error;
      END IF;
    END IF;
  
    RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_RULE_ELEMENT(GV_DESSHORT,
                                                        LN_value_max_nointento,
                                                        LN_value_max_trx_encolada,
                                                        LN_MAX_EXECUTE,
                                                        LN_MAX_COMMIT,
                                                        LN_MAXRETENTRX,
                                                        PV_OUT_ERROR);
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp || PV_RULER,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      Raise LE_ERROR;
    Else
      lv_mensaje_apl_scp := 'INICIO ' || GV_DESSHORT;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            Null,
                                            Null,
                                            0,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
    end if;
  
    ----------------------------------------------------------------------    
    IF PV_RULER = 'D' THEN
      --Regla 1 para eliminar Usando View D
      RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D(LN_ID_PROCESS,
                                              LN_MAX_EXECUTE,
                                              LN_MAX_COMMIT,
                                              ln_id_bitacora_scp,
                                              PN_SOSPID,
                                              LV_MENSAJE);
      IF LV_MENSAJE IS NOT NULL THEN
        IF LV_MENSAJE = 'ABORTADO' THEN
          RAISE LE_ERROR_ABORTADO;
        END IF;
        RAISE LE_ERROR;
      END IF;
      LV_TIPO_PROCESS := '_VIEW_D';
    ELSE
      --Regla 2 para eliminar Usando View A
      RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A(LN_ID_PROCESS,
                                              LN_MAX_EXECUTE,
                                              LN_MAX_COMMIT,
                                              ln_id_bitacora_scp,
                                              PN_SOSPID,
                                              LV_MENSAJE);
      IF LV_MENSAJE IS NOT NULL THEN
        IF LV_MENSAJE = 'ABORTADO' THEN
          RAISE LE_ERROR_ABORTADO;
        END IF;
        RAISE LE_ERROR;
      END IF;
      LV_TIPO_PROCESS := '_VIEW_A';
    END IF;
    --Actualizar LastMonDate Fecha Real en q EJECUTO
    RC_Trx_TimeToCash_Out.RC_REGISTRAR_PROGRAMMER(LN_ID_PROCESS,
                                                  LD_FECHA_ACTUAL,
                                                  LV_MENSAJE);
    IF LV_MENSAJE IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_STATUS  := 'OKI';
    LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL' ||
                  LV_TIPO_PROCESS || '-SUCESSFUL';
  
    --SCP:FIN  
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    lv_mensaje_apl_scp := 'FIN ' || GV_DESSHORT;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          0,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
    ----------------------------------------------------------------------------
  
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                  LD_FECHA_ACTUAL,
                                                  SYSDATE,
                                                  LV_STATUS,
                                                  LV_MENSAJE);
    PV_OUT_ERROR := LV_MENSAJE;
  
  EXCEPTION
  
    WHEN LE_ERROR_ABORTADO THEN
      LV_STATUS    := 'ERR';
      LV_MENSAJE   := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL' ||
                      LV_TIPO_PROCESS || '- PROCESO ' || LV_MENSAJE ||
                      ' MANUALMENTE';
      PV_OUT_ERROR := LV_MENSAJE;
      ---------------------------------------------------------------------------
      lv_mensaje_apl_scp := '    Proceso Abortado Manualmente por el Usuario';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            LV_MENSAJE,
                                            Null,
                                            2,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      -------------------------------------------------------------------------- 
      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                    LD_FECHA_ACTUAL,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LV_MENSAJE);
    
    WHEN LE_ERROR THEN
      LV_STATUS    := 'ERR';
      LV_MENSAJE   := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL' ||
                      LV_TIPO_PROCESS || '- ' || LV_MENSAJE;
      PV_OUT_ERROR := LV_MENSAJE;
      ---------------------------------------------------------------------------
    
      lv_mensaje_apl_scp := '    Error en el Pocesamineto';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            LV_MENSAJE,
                                            Null,
                                            3,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      --------------------------------------------------------------------------                
      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                    LD_FECHA_ACTUAL,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LV_MENSAJE);
      RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,
                                                  GV_DESSHORT,
                                                  'OUT' || PV_RULER,
                                                  0,
                                                  LV_MENSAJE);
    
    WHEN OTHERS THEN
      LV_STATUS    := 'ERR';
      LV_MENSAJE   := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL' ||
                      LV_TIPO_PROCESS || '- ' || ' ERROR:' || SQLCODE || ' ' ||
                      SUBSTR(SQLERRM, 1, 80);
      PV_OUT_ERROR := LV_MENSAJE;
      ---------------------------------------------------------------------------
      lv_mensaje_apl_scp := '    Error en el Procesamiento';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            LV_MENSAJE,
                                            Null,
                                            3,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            Null,
                                            -1,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      -------------------------------------------------------------------------- 
      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                    LD_FECHA_ACTUAL,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LV_MENSAJE);
      RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,
                                                  GV_DESSHORT,
                                                  'OUT' || PV_RULER,
                                                  0,
                                                  LV_MENSAJE);
    
  END RC_PROCESS_PRINCIPAL;

  /***************************************************************************
  *
  ****************************************************************************/
  PROCEDURE RC_DELETE_RULER_A(PN_ID_PROCESS      IN NUMBER,
                              PN_MAX_EXECUTE     IN NUMBER,
                              PN_MAX_COMMIT      IN NUMBER,
                              PN_ID_BITACORA_SCP IN NUMBER,
                              PN_SOSPID          IN NUMBER,
                              PV_OUT_ERROR       OUT VARCHAR2) IS

  --INI AAM(10695)  
    CURSOR C_PLANNIG_DETAILS(CN_CO_ID NUMBER) IS
      select /*+ index(a PK_TTC_CONTRACTPLANNIGDETAILS) +*/ count(a.co_id)
        from ttc_contractplannigdetails_s a
       where a.co_id = cn_co_id;

    ---INI IRO DZ 16/06/2017  
    CURSOR C_DIAS_PLANIFICA2 IS 
     SELECT NVL(VALOR,210)
       FROM GV_PARAMETROS T
      WHERE ID_TIPO_PARAMETRO = 11346
        AND ID_PARAMETRO = 'MAX_DIAS_PLANIFICADO';
     ---FIN IRO DZ 16/06/2017  

    CURSOR C_OUT_A (CN_DIAS NUMBER, CN_DIAS_PLANIFICA NUMBER) IS
    select ttc_view_customer_out_type(customer_id, co_id) from(
 -----ANTES    
     /*
     select /*+ index(cp pk_ttc_contractplannig) +*/
    /*        cp.customer_id, cp.co_id
       from ttc_contractplannig cp
      where exists
       (select co.co_id
                from sysadm.contract_all co
               where co.co_id = cp.co_id
                 and co.lastmoddate between sysdate - cn_dias and sysdate
                 and co.co_ext_csuin='0'));
   */              
 -----ANTES                  
 ---INI IRO DZ 16/06/2017      
      select /*+ index(IX_CONTRPLANNIG_INSERTIONDATE) +*/
       cp.customer_id, cp.co_id
        from reloj.ttc_contractplannig cp
       where cp.insertiondate between sysdate - cn_dias_planifica and
             sysdate
         and exists (select /*+ index(IX_IN_TTC_INSERTIONDATE) +*/
               co.co_id
                from reloj.ttc_contract_all co
               where co.co_id = cp.co_id
                 and co.ttc_insertiondate =
                     (select /*+ index(FK_CO_ID_TTC_CONTRACT_ALL) +*/
                       max(co2.ttc_insertiondate)
                        from reloj.ttc_contract_all co2
                       where co2.co_id = co.co_id
                         and co2.ttc_insertiondate between
                             sysdate - cn_dias and sysdate)
                 and co.co_ext_csuin = '0')
      union
      select /*+ index(IX_CONTRPLANNIG_INSERTIONDATE) +*/
       cp.customer_id, cp.co_id
        from reloj.ttc_contractplannig cp
       where cp.insertiondate between sysdate - cn_dias_planifica and
             sysdate
         and exists (select /*+ index(IX_PDETAILS_CH_TIMEACCIONDATE) +*/
               cpd.co_id
                from reloj.ttc_contractplannigdetails cpd
               where cpd.ch_timeacciondate between
                     sysdate - cn_dias and sysdate
                 and cpd.co_id = cp.co_id
                 and cpd.customer_id = cp.customer_id
                 and cpd.co_statustrx = 'A'));
   
  ---FIN IRO DZ 16/06/2017               

    CURSOR C_OUT_A_NO_DEPURADAS (CN_DIAS NUMBER) IS
    select /*+ index(cp IX_PDETAILS_CH_TIMEACCIONDATE) +*/ cp.customer_id, cp.co_id
      from reloj.ttc_contractplannigdetails cp
     where cp.ch_timeacciondate between sysdate - cn_dias and sysdate
       and cp.dn_num <> 'DTH'
       and cp.co_statustrx='A';

    CURSOR C_DIAS_DESPLANIFICA(CN_PROCESS NUMBER, CV_VALUE_STRING VARCHAR2) IS
    select to_number(t.value_return_longstring)
      from ttc_rule_values t
     where process = cn_process 
       and value_rule_string = cv_value_string;
  --FIN AAM(10695)

    CURSOR C_DEPURA_PLANNING_A IS 
     SELECT NVL(VALOR,'S')
       FROM GV_PARAMETROS T
      WHERE ID_TIPO_PARAMETRO = 10995
        AND ID_PARAMETRO = 'DEPURA_PLANING_A';

    T_OBJ_A TTC_VIEW_CUSTOMER_OUT_T;
  
    LV_MENSAJE      VARCHAR2(500);
    LV_STATUS       VARCHAR(3);
    LN_NUM_REGISTRO NUMBER;
    LN_MAXTRX       NUMBER;
    LN_COUNT        NUMBER;
    LN_COUNT_COMMIT NUMBER;
    LN_MAX_COMMIT   NUMBER;
    LB_EXISTE_BCK   BOOLEAN;
    LD_FECHA_ACTUAL DATE;
    LN_DIAS         NUMBER:=1;
    LE_ERROR_MANUAL EXCEPTION;
    LV_BAND_DEPURA_A VARCHAR2(1);
    LN_MAX_DIAS_PLANIFICA NUMBER:=0;--IRO DZ 16/06/2017

    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    ln_registros_procesados_scp Number;
    ln_registros_error_scp      Number;
    ln_error_scp                number := 0;
    lv_error_scp                varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    --------------------------------------------------------------        
    TYPE R_cursor is record(
    customer_id number,
    co_id       number);

    TYPE Tc_co_id IS TABLE OF R_cursor;
    Lc_Co_id    Tc_co_id;

  BEGIN
  
    LD_FECHA_ACTUAL := SYSDATE;
    RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(PN_ID_PROCESS, LN_MAXTRX);

    RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                        GV_DESSHORT,
                                                        'OUTA',
                                                        0,
                                                        PN_SOSPID,
                                                        LB_EXISTE_BCK);
    LN_COUNT               := 1;
    LN_COUNT_COMMIT        := 0;
    ln_registros_error_scp := 0;

     OPEN C_DIAS_DESPLANIFICA (PN_ID_PROCESS, 'TTC_REACTIVATION_DAYS');
    FETCH C_DIAS_DESPLANIFICA INTO LN_DIAS;
    CLOSE C_DIAS_DESPLANIFICA;
 ---INI IRO DZ 16/06/2017   
    OPEN C_DIAS_PLANIFICA2 ;
    FETCH C_DIAS_PLANIFICA2 INTO LN_MAX_DIAS_PLANIFICA;
    CLOSE C_DIAS_PLANIFICA2;
 ---FIN IRO DZ 16/06/2017 
    OPEN C_OUT_A (LN_DIAS, LN_MAX_DIAS_PLANIFICA);
    LOOP
      FETCH C_OUT_A BULK COLLECT INTO T_OBJ_A LIMIT PN_MAX_EXECUTE;

       FOR rc_in IN (select * from table(cast(t_obj_a as TTC_VIEW_CUSTOMER_OUT_T)))  loop
      
         OPEN C_PLANNIG_DETAILS(rc_in.co_id);
        FETCH C_PLANNIG_DETAILS
         INTO LN_NUM_REGISTRO;
        CLOSE C_PLANNIG_DETAILS;
      
        IF LN_NUM_REGISTRO > 0 THEN
          BEGIN

            --AAM(10695)
            DELETE /*+ index(A PK_TTC_CONTRACTPLANNIGDETAILS) +*/
              FROM TTC_CONTRACTPLANNIGDETAILS_S A
             WHERE A.CO_ID = rc_in.co_id
               AND A.CUSTOMER_ID = rc_in.customer_id;
          
            LN_COUNT_COMMIT := LN_COUNT_COMMIT + SQL%ROWCOUNT;

            IF LN_NUM_REGISTRO = SQL%ROWCOUNT THEN
              DELETE FROM TTC_CONTRACTPLANNIG_S A
               WHERE A.CO_ID = rc_in.co_id
                 AND A.CUSTOMER_ID = rc_in.customer_id;
            END IF;

            IF LN_COUNT >= LN_MAXTRX THEN
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                  GV_DESSHORT,
                                                                  'OUTA',
                                                                  0,
                                                                  PN_SOSPID,
                                                                  LB_EXISTE_BCK);
              LN_COUNT := 1;
            END IF;
          
            IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
              --SCP:AVANCE
              -----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de avance de proceso
              -----------------------------------------------------------------------
              ln_registros_procesados_scp := ln_registros_procesados_scp +
                                             LN_MAX_COMMIT;
              scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                                    ln_registros_procesados_scp,
                                                    null,
                                                    ln_error_scp,
                                                    lv_error_scp);
              -----------------------------------------------------------------------
              COMMIT;
              LN_COUNT_COMMIT := 0;
            END IF;
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_STATUS  := 'ERR';
              LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A- ' ||
                            ' ERROR:' || SQLCODE || ' ' ||
                            SUBSTR(SQLERRM, 1, 80);
              ROLLBACK;
              PV_OUT_ERROR           := LV_MENSAJE;
              ln_registros_error_scp := ln_registros_error_scp + 1;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp := '    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,
                                                    Null,
                                                    Null,
                                                    Null,
                                                    null,
                                                    null,
                                                    'N',
                                                    ln_error_scp,
                                                    lv_error_scp);
            
              -------------------------------------------------------------------------- 
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                            LD_FECHA_ACTUAL,
                                                            SYSDATE,
                                                            LV_STATUS,
                                                            LV_MENSAJE);
          END;
        ELSE
        --INICIO MRO(9833) SE AGREGA VALIDACION
          BEGIN
            LN_COUNT_COMMIT := LN_COUNT_COMMIT + SQL%ROWCOUNT;
                      
              DELETE FROM TTC_CONTRACTPLANNIG_S A
               WHERE A.CO_ID = rc_in.co_id
                 AND A.CUSTOMER_ID = rc_in.customer_id;
           
            IF LN_COUNT >= LN_MAXTRX THEN
              LB_EXISTE_BCK := TRUE;
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                  GV_DESSHORT,
                                                                  'OUTA',
                                                                  0,
                                                                  PN_SOSPID,
                                                                  LB_EXISTE_BCK);
              LN_COUNT := 1;
            END IF;
          
            IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
              --SCP:AVANCE
              -----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de avance de proceso
              -----------------------------------------------------------------------
              ln_registros_procesados_scp := ln_registros_procesados_scp +
                                             LN_MAX_COMMIT;
              scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                                    ln_registros_procesados_scp,
                                                    null,
                                                    ln_error_scp,
                                                    lv_error_scp);
              -----------------------------------------------------------------------
              COMMIT;
              LN_COUNT_COMMIT := 0;
            END IF;
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_STATUS  := 'ERR';
              LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A- ' ||
                            ' ERROR:' || SQLCODE || ' ' ||
                            SUBSTR(SQLERRM, 1, 80);
              ROLLBACK;
              PV_OUT_ERROR           := LV_MENSAJE;
              ln_registros_error_scp := ln_registros_error_scp + 1;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp := '    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,
                                                    Null,
                                                    Null,
                                                    Null,
                                                    null,
                                                    null,
                                                    'N',
                                                    ln_error_scp,
                                                    lv_error_scp);
            
              -------------------------------------------------------------------------- 
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                            LD_FECHA_ACTUAL,
                                                            SYSDATE,
                                                            LV_STATUS,
                                                            LV_MENSAJE);
          END;     
        --FIN MRO(9833) DE VALIDACION
        END IF;
        IF LB_EXISTE_BCK = FALSE THEN
         RAISE LE_ERROR_MANUAL;
        END IF;
        LN_COUNT := LN_COUNT + 1;
      END LOOP;
      EXIT WHEN C_OUT_A%NOTFOUND;
    END LOOP;
    CLOSE C_OUT_A;

   --10995 INI AAM
    OPEN C_DEPURA_PLANNING_A;
    FETCH C_DEPURA_PLANNING_A INTO LV_BAND_DEPURA_A;
    CLOSE C_DEPURA_PLANNING_A;

    if LV_BAND_DEPURA_A = 'S' THEN

       LN_COUNT_COMMIT:=0;

       OPEN C_OUT_A_NO_DEPURADAS(LN_DIAS);
       FETCH C_OUT_A_NO_DEPURADAS BULK COLLECT INTO Lc_Co_id;
       CLOSE C_OUT_A_NO_DEPURADAS;


     IF Lc_Co_id.COUNT > 0 THEN
        FORALL i IN Lc_Co_id.FIRST .. Lc_Co_id.LAST
           DELETE FROM TTC_CONTRACTPLANNIGDETAILS_S A
            WHERE  A.CO_ID = Lc_Co_id(i).co_id
            AND A.CUSTOMER_ID = Lc_Co_id(i).customer_id;
         COMMIT;
        FORALL i IN Lc_Co_id.FIRST .. Lc_Co_id.LAST
           DELETE FROM TTC_CONTRACTPLANNIG_S A
            WHERE A.CO_ID = Lc_Co_id(i).co_id
              AND A.CUSTOMER_ID = Lc_Co_id(i).customer_id;
         COMMIT;
     END IF;
   --10995 FIN AAM
    END IF;
    -------------------------------------------------------------------
    ln_registros_procesados_scp := ln_registros_procesados_scp +
                                   LN_COUNT_COMMIT;
    scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ---------------------------------------------------------------------------
    COMMIT;
    PV_OUT_ERROR := NULL;
  
  EXCEPTION
    WHEN LE_ERROR_MANUAL THEN
      PV_OUT_ERROR := 'ABORTADO';
      ROLLBACK;
      If ln_registros_error_scp = 0 Then
        ln_registros_error_scp := -1;
      End If;
      scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                            Null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
    
    WHEN OTHERS THEN
      LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A- ' ||
                    ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 80);
      ROLLBACK;
      PV_OUT_ERROR := LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
        ln_registros_error_scp := -1;
      End If;
      scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                            Null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
    
  END RC_DELETE_RULER_A;
  /************************************************************************************
  * PROCESO QUE ELIMINA DE   Ttc_Contractplannig  -- TTC_ContractPlannigDetails
  * DE ACUERDO A LA REGLA QUE SE QUIERA APLICAR
  * Y A SU VEZ SE ACTIVA LOS TRIGGERS  TTC_Trg_ContractPlannig---TTC_Trg_ContractPlannigHist
  *************************************************************************************/

  PROCEDURE RC_DELETE_RULER_D(PN_ID_PROCESS      IN NUMBER,
                              PN_MAX_EXECUTE     IN NUMBER,
                              PN_MAX_COMMIT      IN NUMBER,
                              PN_ID_BITACORA_SCP IN NUMBER,
                              PN_SOSPID          IN NUMBER,
                              PV_OUT_ERROR       OUT VARCHAR2) IS
  --INI AAM(10695)
    CURSOR C_PLANNIG_DETAILS(CN_CO_ID NUMBER) IS
      select /*+ index(a PK_TTC_CONTRACTPLANNIGDETAILS) +*/ count(a.co_id)
        from ttc_contractplannigdetails_s a
       where a.co_id = cn_co_id;
       
    ---INI IRO DZ 16/06/2017  
    CURSOR C_DIAS_PLANIFICA2 IS 
     SELECT NVL(VALOR,210)
       FROM GV_PARAMETROS T
      WHERE ID_TIPO_PARAMETRO = 11346
        AND ID_PARAMETRO = 'MAX_DIAS_PLANIFICADO';
     ---FIN IRO DZ 16/06/2017         
  
    CURSOR C_OUT_D (CN_DIAS NUMBER,  CN_DIAS_PLANIFICA NUMBER)IS
      select ttc_view_customer_out_type(customer_id, co_id)
        FROM (
    ----ANTES        
        /*select cp.customer_id, cp.co_id
                from reloj.ttc_contractplannig cp, sysadm.contract_history cur
               where cur.co_id = cp.co_id
                 and cur.ch_seqno =
                     (select max(z.ch_seqno)
                        from sysadm.contract_history z
                       where z.co_id = cur.co_id
                         and z.co_id = cp.co_id
                         and (z.ch_pending is null or z.ch_pending != 'X'))
                 and cur.ch_status = 'd'
                 and cur.lastmoddate between sysdate - cn_dias and sysdate
              union all
              select ca.customer_id, ca.co_id
                from reloj.ttc_contractplannig cp, reloj.ttc_contract_all ca
               where ca.ttc_insertiondate =
                     (select max(ca2.ttc_insertiondate)
                        from reloj.ttc_contract_all ca2
                       where ca.co_id = ca2.co_id)
                 and ca.co_id = cp.co_id
                 and ca.co_ext_csuin = '5'
                 and ca.ttc_insertiondate between sysdate - cn_dias and sysdate); */--10695 AAM
    ----ANTES
      ---INI IRO DZ 16/06/2017               
      select /*+ index(IX_CONTRPLANNIG_INSERTIONDATE) +*/
       cp.customer_id, cp.co_id
        from reloj.ttc_contractplannig cp
       where cp.insertiondate between sysdate - cn_dias_planifica and sysdate
         and exists (select /*+ index(IX_PDETAILS_CH_TIMEACCIONDATE) +*/
               cpd.co_id
                from reloj.ttc_contractplannigdetails cpd
               where cpd.ch_timeacciondate between
                     (sysdate - cn_dias) and sysdate
                 and cpd.co_id = cp.co_id
                 and cpd.customer_id = cp.customer_id
                 and cpd.co_status_a = '35'
                 and cpd.co_statustrx = 'F')
      
      union
      select /*+ index(IX_CONTRPLANNIG_INSERTIONDATE) +*/
       cp.customer_id, cp.co_id
        from reloj.ttc_contractplannig cp
       where cp.insertiondate between sysdate - cn_dias_planifica and sysdate
         and exists (select /*+ index(IX_IN_TTC_INSERTIONDATE) +*/
               co.co_id
                from reloj.ttc_contract_all co
               where co.co_id = cp.co_id
                 and co.ttc_insertiondate =
                     (select /*+ index(FK_CO_ID_TTC_CONTRACT_ALL) +*/
                       max(co2.ttc_insertiondate)
                        from reloj.ttc_contract_all co2
                       where co2.co_id = co.co_id
                         and co2.ttc_insertiondate between
                             sysdate - cn_dias and sysdate)
                 and co.co_ext_csuin = '5'));
                     
                     
     ---FIN IRO DZ 16/06/2017   
  
    CURSOR C_DIAS_DESPLANIFICA(CN_PROCESS NUMBER, CV_VALUE_STRING VARCHAR2) IS
    select to_number(t.value_return_longstring)
      from ttc_rule_values t
     where process = cn_process 
       and value_rule_string = cv_value_string;

  --FIN AAM(10695)

    t_obj_d         TTC_VIEW_CUSTOMER_OUT_T;
    LV_MENSAJE      VARCHAR2(500);
    LV_STATUS       VARCHAR(3);
    LN_NUM_REGISTRO NUMBER;
    LN_MAXTRX       NUMBER;
    LN_COUNT        NUMBER;
    LN_COUNT_COMMIT NUMBER;
    LN_MAX_COMMIT   NUMBER;
    LB_EXISTE_BCK   BOOLEAN;
    LD_FECHA_ACTUAL DATE;
    LN_DIAS         NUMBER:=1;
    LE_ERROR_MANUAL EXCEPTION;
    LN_MAX_DIAS_PLANIFICA NUMBER:=0;--IRO DZ 16/06/2017    
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    ln_error_scp                number := 0;
    lv_error_scp                varchar2(500);
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      Number := 0;
    lv_mensaje_apl_scp          varchar2(4000);
    ---------------------------------------------------------------
  
  BEGIN
  
    LD_FECHA_ACTUAL := SYSDATE;
    RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(PN_ID_PROCESS,
                                                          LN_MAXTRX);
    RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                        GV_DESSHORT,
                                                        'OUTD',
                                                        0,
                                                        PN_SOSPID,
                                                        LB_EXISTE_BCK);
    LN_COUNT        := 1;
    LN_COUNT_COMMIT := 0;

     OPEN C_DIAS_DESPLANIFICA (PN_ID_PROCESS, 'TTC_REACTIVATION_DAYS');
    FETCH C_DIAS_DESPLANIFICA INTO LN_DIAS;
    CLOSE C_DIAS_DESPLANIFICA;
    
 ---INI IRO DZ 16/06/2017   
    OPEN C_DIAS_PLANIFICA2 ;
    FETCH C_DIAS_PLANIFICA2 INTO LN_MAX_DIAS_PLANIFICA;
    CLOSE C_DIAS_PLANIFICA2;
 ---FIN IRO DZ 16/06/2017 

    OPEN C_OUT_D(LN_DIAS,LN_MAX_DIAS_PLANIFICA);
    LOOP
      FETCH C_OUT_D BULK COLLECT INTO t_obj_d LIMIT PN_MAX_EXECUTE;
    
      FOR rc_in IN (select * from table(cast(t_obj_d as TTC_VIEW_CUSTOMER_OUT_T))) loop
      
         OPEN C_PLANNIG_DETAILS(rc_in.co_id);
        FETCH C_PLANNIG_DETAILS
         INTO LN_NUM_REGISTRO;
        CLOSE C_PLANNIG_DETAILS;
      
        IF LN_NUM_REGISTRO > 0 THEN
          BEGIN

            --AAM(10695)
            DELETE /*+ index(A PK_TTC_CONTRACTPLANNIGDETAILS) +*/
              FROM TTC_CONTRACTPLANNIGDETAILS_S A
             WHERE  A.CO_ID = rc_in.co_id;
          
            LN_COUNT_COMMIT := LN_COUNT_COMMIT + SQL%ROWCOUNT;

            IF LN_NUM_REGISTRO = SQL%ROWCOUNT THEN
              DELETE FROM TTC_CONTRACTPLANNIG_S A
               WHERE A.CO_ID = rc_in.CO_ID
                 AND A.CUSTOMER_ID = rc_in.CUSTOMER_ID;
            END IF;

            IF LN_COUNT >= LN_MAXTRX THEN
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                  GV_DESSHORT,
                                                                  'OUTD',
                                                                  0,
                                                                  PN_SOSPID,
                                                                  LB_EXISTE_BCK);
              LN_COUNT := 1;
            END IF;
          
            IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
              --SCP:AVANCE
              -----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de avance de proceso
              -----------------------------------------------------------------------
              ln_registros_procesados_scp := ln_registros_procesados_scp +
                                             LN_MAX_COMMIT;
              scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                    ln_registros_procesados_scp,
                                                    null,
                                                    ln_error_scp,
                                                    lv_error_scp);
              -----------------------------------------------------------------------
              COMMIT;
              LN_COUNT_COMMIT := 0;
            END IF;
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_STATUS  := 'ERR';
              LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D- NO SE PUDO ELIMINAR REGISTROS POR ' ||
                            ' ERROR:' || SQLCODE || ' ' ||
                            SUBSTR(SQLERRM, 1, 80);
              ROLLBACK;
              PV_OUT_ERROR           := LV_MENSAJE;
              ln_registros_error_scp := ln_registros_error_scp + 1;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp := '    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,
                                                    Null,
                                                    Null,
                                                    Null,
                                                    null,
                                                    null,
                                                    'N',
                                                    ln_error_scp,
                                                    lv_error_scp);
            
              -------------------------------------------------------------------------- 
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                            LD_FECHA_ACTUAL,
                                                            SYSDATE,
                                                            LV_STATUS,
                                                            LV_MENSAJE);
          END;
          --IRO MRO(9833) Inicio 10/09/2014
        ELSE
          BEGIN
            LN_COUNT_COMMIT := LN_COUNT_COMMIT + SQL%ROWCOUNT;
          
            DELETE FROM TTC_CONTRACTPLANNIG_S A
             WHERE A.CO_ID = rc_in.co_id
               AND A.CUSTOMER_ID = rc_in.customer_id;
          
            IF LN_COUNT >= LN_MAXTRX THEN
              LB_EXISTE_BCK := TRUE;
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                  GV_DESSHORT,
                                                                  'OUTD',
                                                                  0,
                                                                  PN_SOSPID,
                                                                  LB_EXISTE_BCK);
              LN_COUNT := 1;
            END IF;
          
            IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
              --SCP:AVANCE
              -----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de avance de proceso
              -----------------------------------------------------------------------
              ln_registros_procesados_scp := ln_registros_procesados_scp +
                                             LN_MAX_COMMIT;
              scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                    ln_registros_procesados_scp,
                                                    null,
                                                    ln_error_scp,
                                                    lv_error_scp);
              -----------------------------------------------------------------------
              COMMIT;
              LN_COUNT_COMMIT := 0;
            END IF;
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_STATUS  := 'ERR';
              LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D- NO SE PUDO ELIMINAR REGISTROS POR ' ||
                            ' ERROR:' || SQLCODE || ' ' ||
                            SUBSTR(SQLERRM, 1, 80);
              ROLLBACK;
              PV_OUT_ERROR           := LV_MENSAJE;
              ln_registros_error_scp := ln_registros_error_scp + 1;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp := '    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,
                                                    Null,
                                                    Null,
                                                    Null,
                                                    null,
                                                    null,
                                                    'N',
                                                    ln_error_scp,
                                                    lv_error_scp);
            
              -------------------------------------------------------------------------- 
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                            LD_FECHA_ACTUAL,
                                                            SYSDATE,
                                                            LV_STATUS,
                                                            LV_MENSAJE);
          END;
          --IRO MRO(9833) Fin 10/09/2014
        END IF;
        IF LB_EXISTE_BCK = FALSE THEN
          RAISE LE_ERROR_MANUAL;
        END IF;
        LN_COUNT := LN_COUNT + 1;
      END LOOP;
      EXIT WHEN C_OUT_D%NOTFOUND;
    END LOOP;
    CLOSE C_OUT_D;
    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    ln_registros_procesados_scp := ln_registros_procesados_scp +
                                   LN_COUNT_COMMIT;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    PV_OUT_ERROR := NULL;
  
  EXCEPTION
    WHEN LE_ERROR_MANUAL THEN
      PV_OUT_ERROR := 'ABORTADO';
      ROLLBACK;
      If ln_registros_error_scp = 0 Then
        ln_registros_error_scp := -1;
      End If;
      scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                            Null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
    
    WHEN OTHERS THEN
      LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D- ' ||
                    ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 80);
      ROLLBACK;
      PV_OUT_ERROR := LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
        ln_registros_error_scp := -1;
      End If;
      scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                            Null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
    
  END RC_DELETE_RULER_D;

  /*******************************************************************************************
  *
  ********************************************************************************************/

  PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_ID_PROCESS  IN NUMBER,
                                    PD_LASTMONDATE IN DATE,
                                    PV_OUT_ERROR   OUT VARCHAR2) IS
    LV_STATUS  VARCHAR2(3);
    LV_MENSAJE VARCHAR2(200);
    LN_SEQNO   NUMBER;
  BEGIN
  
    SELECT TTC_TRX_OUT.NEXTVAL INTO LN_SEQNO FROM DUAL;
  
    INSERT INTO ttc_process_programmer_s
    VALUES
      (PN_ID_PROCESS, LN_SEQNO, PD_LASTMONDATE, sysdate);
    COMMIT;
    PV_OUT_ERROR := NULL;
  EXCEPTION
    WHEN OTHERS THEN
      LV_STATUS    := 'ER';
      LV_MENSAJE   := 'RC_REGISTRAR_PROGRAMMER- ' || ' ERROR:' || SQLCODE || ' ' ||
                      SUBSTR(SQLERRM, 1, 80);
      PV_OUT_ERROR := LV_MENSAJE;
      ROLLBACK;
      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                    SYSDATE,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LV_MENSAJE);
    
  END RC_REGISTRAR_PROGRAMMER;

end RC_Trx_TimeToCash_Out;
/
