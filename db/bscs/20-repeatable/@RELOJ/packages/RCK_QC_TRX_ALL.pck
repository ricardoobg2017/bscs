create or replace package reloj.RCK_QC_TRX_ALL is

  -- Author  : USER
  -- Created : 29/10/2010 10:55:39
  -- Purpose : 
  
 
  procedure Prc_Qc_Suspension(PV_ERROR OUT VARCHAR2);

end RCK_QC_TRX_ALL;
/
create or replace package body reloj.RCK_QC_TRX_ALL is

   procedure Prc_Qc_Suspension(PV_ERROR OUT VARCHAR2) is
   -- Variables
   --Transacciones de BSCS, que se encuentran pendientes de ejecuci�n
   cursor c_bscs_pendientes is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_TRX_BSCS_NEW;
   
   lc_bscs_pendientes                     c_bscs_pendientes%rowtype;
   
   --Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
   cursor c_axis_cola is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_COLAS_AXIS_NEW;
   
   lc_axis_cola                           c_axis_cola%rowtype;
   
   --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
   cursor c_axis_cola_reintentos is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_COLA_REIN_AXIS_NEW;
   
   lc_axis_cola_reintentos                 c_axis_cola_reintentos%rowtype;
   
   --
   cursor c_axisTMP_sendBSCS is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_HIST_AXIS;
   
   lc_axisTMP_sendBSCS                    c_axisTMP_sendBSCS%rowtype;
   
   --extracion de datos de
   cursor c_trx_bscs is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_TRX_2_BSCS;
   
   lc_trx_bscs                            c_trx_bscs%rowtype;
   
   --cursor para extraer los adatos de la tabla de resumen, segun la ejecucion del QC
   CURSOR c_ttc_suspension(cn_secuencia NUMBER) IS
   select id_qc,
          name_qc,
          descripcion_qc,
          estado,
          descripcion_estado,
          num_trans,
          fecha_trans,
          recomendacion
     from ttc_suspension_resumen
    WHERE id_qc = cn_secuencia;
   
   lv_mensaje        varchar2(500);
   lv_recomendacion  varchar2(500);
   ln_secuencia      NUMBER;
   lv_programa       VARCHAR2(200):='RCK_QC_TRX_ALL.Prc_Qc_Suspension'; 
   LV_ASUNTO_MAIL    VARCHAR2(100):='QC - SUSPENSION'; 
   lv_mensaje_mail   varchar2(2000); 
   
   lb_hay_datos      boolean:=false;
   
  
   BEGIN
   
       --EXTRAIGO LA SECUENCIA DE LA EJECUCION
       SELECT TTC_S_SUSPENSION_RESUMEN.Nextval INTO ln_secuencia FROM dual;
          
       -- Procesamiento BSCS
       begin  
            OPEN c_bscs_pendientes;  
            loop  
                FETCH c_bscs_pendientes into lc_bscs_pendientes; 
                
                exit when c_bscs_pendientes%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_bscs_pendientes.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                     ,NAME_QC
                                                     ,DESCRIPCION_QC
                                                     ,ESTADO
                                                     ,DESCRIPCION_ESTADO
                                                     ,NUM_TRANS
                                                     ,FECHA_TRANS
                                                     ,RECOMENDACION)     
                                                     VALUES
                                                     (ln_secuencia
                                                     ,'QC_SUSPENSION'
                                                     ,lv_mensaje
                                                     ,lc_bscs_pendientes.ESTADO
                                                     ,lc_bscs_pendientes.DESCRIPCION_ESTADO
                                                     ,lc_bscs_pendientes.VALOR
                                                     ,sysdate
                                                     ,lv_recomendacion);
                lb_hay_datos:=true;
            end loop;
            close c_bscs_pendientes;   
       end;
       
       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';
                   insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                       ,NAME_QC
                                                       ,DESCRIPCION_QC
                                                       ,ESTADO
                                                       ,DESCRIPCION_ESTADO
                                                       ,NUM_TRANS
                                                       ,FECHA_TRANS
                                                       ,RECOMENDACION)     
                                                       VALUES
                                                       (ln_secuencia
                                                       ,'QC_SUSPENSION'
                                                       ,lv_mensaje
                                                       ,NULL
                                                       ,NULL
                                                       ,NULL
                                                       ,sysdate
                                                       ,NULL);
       end if;
          
       --AXIS EN PROCESAMIENTO
       --Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola;  
            loop  
                FETCH c_axis_cola into lc_axis_cola; 
                
                exit when c_axis_cola%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                     ,NAME_QC
                                                     ,DESCRIPCION_QC
                                                     ,ESTADO
                                                     ,DESCRIPCION_ESTADO
                                                     ,NUM_TRANS
                                                     ,FECHA_TRANS
                                                     ,RECOMENDACION)     
                                                     VALUES
                                                     (ln_secuencia
                                                     ,'QC_SUSPENSION'
                                                     ,lv_mensaje
                                                     ,lc_axis_cola.ESTADO
                                                     ,lc_axis_cola.DESCRIPCION_ESTADO
                                                     ,lc_axis_cola.VALOR
                                                     ,sysdate
                                                     ,lv_recomendacion);
                lb_hay_datos:=true;
            end loop;
            close c_axis_cola;   
       end;
       
       if not lb_hay_datos then  
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';
                   insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                       ,NAME_QC
                                                       ,DESCRIPCION_QC
                                                       ,ESTADO
                                                       ,DESCRIPCION_ESTADO
                                                       ,NUM_TRANS
                                                       ,FECHA_TRANS
                                                       ,RECOMENDACION)     
                                                       VALUES
                                                       (ln_secuencia
                                                       ,'QC_SUSPENSION'
                                                       ,lv_mensaje
                                                       ,NULL
                                                       ,NULL
                                                       ,NULL
                                                       ,sysdate
                                                       ,NULL); 
       end if;
      
       --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola_reintentos;  
            loop  
                FETCH c_axis_cola_reintentos into lc_axis_cola_reintentos; 
                
                exit when c_axis_cola_reintentos%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola_reintentos.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                     ,NAME_QC
                                                     ,DESCRIPCION_QC
                                                     ,ESTADO
                                                     ,DESCRIPCION_ESTADO
                                                     ,NUM_TRANS
                                                     ,FECHA_TRANS
                                                     ,RECOMENDACION)     
                                                     VALUES
                                                     (ln_secuencia
                                                     ,'QC_SUSPENSION'
                                                     ,lv_mensaje
                                                     ,lc_axis_cola_reintentos.ESTADO
                                                     ,lc_axis_cola_reintentos.DESCRIPCION_ESTADO
                                                     ,lc_axis_cola_reintentos.VALOR
                                                     ,sysdate
                                                     ,lv_recomendacion);
                lb_hay_datos:=true;
            end loop;
            close c_axis_cola_reintentos;   
       end;   
       
       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';
                   insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                       ,NAME_QC
                                                       ,DESCRIPCION_QC
                                                       ,ESTADO
                                                       ,DESCRIPCION_ESTADO
                                                       ,NUM_TRANS
                                                       ,FECHA_TRANS
                                                       ,RECOMENDACION)     
                                                       VALUES
                                                       (ln_secuencia
                                                       ,'QC_SUSPENSION'
                                                       ,lv_mensaje
                                                       ,NULL
                                                       ,NULL
                                                       ,NULL
                                                       ,sysdate
                                                       ,NULL);
       end if;
       --Obtengo Tabla de axis temporal para enviar la informaci�n hacia BSCS del proceso Job Retry
       lb_hay_datos:=false;
       begin  
            OPEN c_axisTMP_sendBSCS;  
            loop  
                FETCH c_axisTMP_sendBSCS into lc_axisTMP_sendBSCS; 
                                      
                exit when c_axisTMP_sendBSCS%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axisTMP_sendBSCS.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                     ,NAME_QC
                                                     ,DESCRIPCION_QC
                                                     ,ESTADO
                                                     ,DESCRIPCION_ESTADO
                                                     ,NUM_TRANS
                                                     ,FECHA_TRANS
                                                     ,RECOMENDACION)     
                                                     VALUES
                                                     (ln_secuencia
                                                     ,'QC_SUSPENSION'
                                                     ,lv_mensaje
                                                     ,lc_axisTMP_sendBSCS.ESTADO
                                                     ,lc_axisTMP_sendBSCS.DESCRIPCION_ESTADO
                                                     ,lc_axisTMP_sendBSCS.VALOR
                                                     ,sysdate
                                                     ,lv_recomendacion);
                 lb_hay_datos:=true;
            end loop;
            close c_axisTMP_sendBSCS;   
       end; 
       
       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';
                   insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                       ,NAME_QC
                                                       ,DESCRIPCION_QC
                                                       ,ESTADO
                                                       ,DESCRIPCION_ESTADO
                                                       ,NUM_TRANS
                                                       ,FECHA_TRANS
                                                       ,RECOMENDACION)     
                                                       VALUES
                                                       (ln_secuencia
                                                       ,'QC_SUSPENSION'
                                                       ,lv_mensaje
                                                       ,NULL
                                                       ,NULL
                                                       ,NULL
                                                       ,sysdate
                                                       ,NULL);
       end if;
       
       lb_hay_datos:=false;
        --Obtengo Transacciones de BSCS, que se encuentran Finalizadas en la ejecuci�n de hoy    
        begin  
            OPEN c_trx_bscs;  
            loop  
                FETCH c_trx_bscs into lc_trx_bscs; 
                               
                exit when c_trx_bscs%notfound;
                  
                lv_recomendacion:=NULL;
                IF lc_trx_bscs.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                     ,NAME_QC
                                                     ,DESCRIPCION_QC
                                                     ,ESTADO
                                                     ,DESCRIPCION_ESTADO
                                                     ,NUM_TRANS
                                                     ,FECHA_TRANS
                                                     ,RECOMENDACION)     
                                                     VALUES
                                                     (ln_secuencia
                                                     ,'QC_SUSPENSION'
                                                     ,lv_mensaje
                                                     ,lc_trx_bscs.ESTADO
                                                     ,lc_trx_bscs.DESCRIPCION_ESTADO
                                                     ,lc_trx_bscs.VALOR
                                                     ,sysdate
                                                     ,lv_recomendacion);
                lb_hay_datos:=true;
            end loop;
            close c_trx_bscs;   
       end; 
       if not lb_hay_datos then
          insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                                       ,NAME_QC
                                                       ,DESCRIPCION_QC
                                                       ,ESTADO
                                                       ,DESCRIPCION_ESTADO
                                                       ,NUM_TRANS
                                                       ,FECHA_TRANS
                                                       ,RECOMENDACION)     
                                                       VALUES
                                                       (ln_secuencia
                                                       ,'QC_SUSPENSION'
                                                       ,lv_mensaje
                                                       ,NULL
                                                       ,NULL
                                                       ,NULL
                                                       ,sysdate
                                                       ,NULL); 
       end if;
       
       COMMIT;
       
       lv_mensaje_mail := 'ID_QC - NAME_QC - DESCRIPCION_QC - ESTADO - DESCRIPCION_ESTADO - NUM_TRANS - FECHA_TRANS - RECOMENDACION'||chr(13)||chr(10);
       FOR i IN c_ttc_suspension(ln_secuencia) LOOP
           lv_mensaje_mail:= lv_mensaje_mail ||chr(13)||chr(10)||i.id_qc||' - '||i.name_qc||' - '||i.descripcion_qc||' - '||i.estado||' - '||i.num_trans||' - '||i.fecha_trans||' - '||i.recomendacion;      
       END LOOP;   
       --ENVIO DE MAIL
    /*   BEGIN
               sysadm.send_mail2.mail(from_name => 'sistema_archivo@conecel.com',
                                       to_name   => 'jronquillo@conecel.com',
                                       cc        => NULL,
                                       cco       => NULL,
                                       subject   => LV_ASUNTO_MAIL,
                                       message   => lv_mensaje_mail);
       EXCEPTION
           WHEN OTHERS THEN
             NULL;                               
       END;*/
   
   
 EXCEPTION
  WHEN OTHERS THEN
        Pv_Error := lv_programa || SubStr(sqlerrm,1,200);
        ROLLBACK;
 end Prc_Qc_Suspension;

end RCK_QC_TRX_ALL;
/
