create or replace package Rc_Trx_Free_Ambiente IS

      GN_PROCESO_IN   NUMBER:=12;
      GN_PROCESO_REAC_ALL   NUMBER:=17;
      GN_DESSHORT_IN VARCHAR2(15):='TTC_AMBIENTE';
      GN_DES_TRUN_TTC_CONTRACT_IN VARCHAR2(45):='TTC_IN_SQL_TRUNCATE_TTC_CONTRACT_ALL';
      GN_DES_TRUN_TTC_CONTRACT_DTH VARCHAR2(45):='TTC_IN_SQL_TRUNCATE_TTC_CONTRACT_ALL_DTH_IN';     
      GN_DES_FLAG_SET_A_TO_P VARCHAR2(30) := 'TTC_FLAG_SET_A_TO_P';
      GN_DES_FLAG_SET_E_TO_P VARCHAR2(30) := 'TTC_FLAG_SET_E_TO_P';
      GN_DES_FLAG_SET_T_TO_P VARCHAR2(30) := 'TTC_FLAG_SET_T_TO_P';
      GN_DES_FLAG_SET_L_TO_P VARCHAR2(30) := 'TTC_FLAG_SET_L_TO_P';      
      GN_DES_FLAG_SET_E_TO_V VARCHAR2(30) := 'TTC_FLAG_SET_E_TO_V';
      GN_DES_FLAG_DEL_REACTIVE VARCHAR2(30) := 'TTC_FLAG_DELETE_REACTIVE';
      GN_DES_FLAG_DEL_REACT_RELOJ VARCHAR2(30) := 'TTC_FLAG_DELETE_REACT_RELOJ';
      GN_TTC_FLAG_PROCESS_LOCAL VARCHAR2(25):='TTC_FLAG_PROCESS_LOCAL';
      GV_STATUS_P   VARCHAR2(1) := 'P';
      GV_STATUS_T   VARCHAR2(1) := 'T';
      GV_STATUS_A   VARCHAR2(1) := 'A';
      GV_STATUS_L   VARCHAR2(1) := 'L';
      GV_STATUS_E   VARCHAR2(1) := 'E';
      GV_STATUS_V   VARCHAR2(1) := 'V';      
      GN_MAX_DELETE   NUMBER;      
      TYPE T_TableName   IS TABLE OF ttc_relation_objects_s.table_valuestring%TYPE  INDEX BY BINARY_INTEGER;
       V_TableName              T_TableName;                 
      
      PROCEDURE Prc_Header(Pn_SosPid          IN NUMBER,
                                                       PV_Error  OUT VARCHAR2
                                                      );
      PROCEDURE Prc_To_Delete_Tables(PV_Error  OUT VARCHAR2);                                               
      PROCEDURE Prc_To_Ckeck_Process(PV_Error  OUT VARCHAR2);            
/*       PROCEDURE Prc_Delete_CONTRHIST_TMP(PV_OUT_ERROR   OUT VARCHAR2);*/
                                         
End Rc_Trx_Free_Ambiente;
/
create or replace package body Rc_Trx_Free_Ambiente  IS

/*===========================================================================================
[8693] RELOJ DE COBRANZAS DTH
LIDER SIS :               JUAN DAVID P�REZ
LIDER IRO :               JEFFERSON MATEO
Modificado por:           Iro Juan Romero Aguilar
Proyecto  :               [8250] Modificacion de ambiente Bscs para soporte DTH
OBJETO    :               Depuraci�n de tablas DTH para correcto soporte de cuentas DTH, 
                          Ademas correccion de bitacorizacion.
               
               
=============================================================================================*/
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          17/12/2014
  PROPOSITO:      Eliminaci�n de cuentas insertadas en la tabla RC_CTAS_REAC_RELOJ de hace 180 d�as 
  *********************************************************************************************************/

/*=============================================================================================*/

/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO NORMA PAZMI�O
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          20/04/2015
  PROPOSITO:      *Comentar bloque de c�digo en el procedimiento Prc_To_Delete_Tables que actualiza cuentas que tienen estatus 'A' 
                  *Mejoras sobre las sentencias que hacen las actualizaciones en las tablas de planificaci�n.                        
  *********************************************************************************************************/
/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO Jordan Rodriguez
  PROYECTO:       10695 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          15/06/2016
  PROPOSITO:      Relizar el llamado al procedimiento RELOJ.RC_TRX_UPDATE_PLANNING siempre y cuando se haya ejecuato
                  el proceso RC_TRX_TIMETOCASH_DISPATCH.RC_PROCESS_DISPATCH en horas despues de las 20:00:00 horas.                        
  *********************************************************************************************************/
/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO Diana Zambrano
  PROYECTO:       [11346] ONE AMX Reloj de Estados postpago Reactivacion Renuncia
  FECHA:          16/06/2017
  PROPOSITO:     Realizar ajuste al proceso PRC_TO_CKECK_PROCESS para que elimine los registros antiguos
  		 en la tabla TTC_BCK_PROCESS_S, y se realice correctamente la ejecuci�n del SEND.                 
  *********************************************************************************************************/


       CURSOR C_NameTables IS
         SELECT DISTINCT table_valuestring 
            FROM ttc_relation_objects_s;
            
     -- Private type declarations ,constant declarations,variable declarations
        i_process                                   ttc_process_t;
        i_process_Ckeck                      ttc_process_t;
        i_rule_element                          ttc_Rule_Element_t;
        i_Bck_Process_t                        ttc_Bck_Process_t;
        i_Log_InOut_t                            ttc_Log_InOut_t; 
        i_Process_Programmer_t       ttc_Process_Programmer_t;    
        i_Rule_Values_t                        ttc_Rule_Values_t;   

     --SCP:VARIABLES
    ------------------------------------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ------------------------------------------------------------------------------------------
        ln_id_bitacora_scp number:=0; 
        ln_total_registros_scp number:=0;
        lv_id_proceso_scp varchar2(100):='TTC_IN';
        lv_referencia_scp varchar2(100):='RC_TRX_IN_TO_REGULARIZE.Prc_Header';
        lv_unidad_registro_scp varchar2(30):='Regulariza CO_ID';
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
    ---------------------------------------------------------------


--**********************************************************************************
    PROCEDURE Prc_Header(Pn_SosPid          IN NUMBER,
                                                    PV_Error  OUT VARCHAR2
                                                    ) IS                                               
       CURSOR C_Trx_In IS
          SELECT  TTC_TRX_IN.NEXTVAL FROM dual;            
                                
      Lv_Des_Error        VARCHAR2(800);
      Le_Error_Valid      EXCEPTION;
      Le_Error_process EXCEPTION;
      Ld_SysdateRun    DATE;
      Ln_Seqno              NUMBER;
      --Inicio 10695 Iro_Jro
      lv_bandera            varchar2(3);
      ln_cant_procees       number;
      CURSOR C_PROCESS_HIST IS 
      SELECT COUNT(*) TOTAL
        FROM RELOJ.TTC_BCK_PROCESS_HIST A
       WHERE A.PROCESS = 4
         AND A.FISRTRUNDATE >=
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 20:00:00',
                     'DD/MM/RRRR HH24:MI:SS') - 1;
     LC_PROCESS_HIST C_PROCESS_HIST%ROWTYPE;
     CURSOR C_PARAMETROS_UPDATE(CN_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
     SELECT A.VALOR
       FROM GV_PARAMETROS A
      WHERE A.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
        AND A.ID_PARAMETRO = CV_ID_PARAMETRO;
     LC_PARAMETROS_UPDATE C_PARAMETROS_UPDATE%ROWTYPE;
    --Fin 10695 Iro_Jro
   BEGIN
           Ld_SysdateRun:=SYSDATE;

           ----------------------------------------------------------------------------
           -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
           ----------------------------------------------------------------------------
           scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                                 lv_referencia_scp,
                                                 null,null,null,null,
                                                 ln_total_registros_scp,
                                                 lv_unidad_registro_scp,
                                                 ln_id_bitacora_scp,
                                                 ln_error_scp,lv_error_scp);
           if ln_error_scp <>0 then
              Lv_Des_Error:='Error en plataforma SCP, No se pudo iniciar la Bitacora';
              RAISE Le_Error_Valid;
           end if;
           ----------------------------------------------------------------------

         --Cargar los datos del Proceso TTC_IN -->TTC_Process_s                  
          i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GN_DESSHORT_IN);      
          IF i_process.Process IS NULL THEN
                Lv_Des_Error:='No existen datos configurados en la tabla TTC_Process_s';
                RAISE Le_Error_Valid;
          END IF;

          --Ingreso de Inicio del proceso 
            i_Log_InOut_t:= ttc_Log_InOut_t(Process => nvl(i_process.Process,0),
                                                                      RunDate => SYSDATE,
                                                                      LastDate => NULL,
                                                                      StatusProcess => 'OKI',
                                                                      Comment_Run => NULL                                                                           
                                                                      );                  
            --Verifica si el proceso se encuentra en ejecuci�n
            IF  RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GN_DESSHORT_IN,i_process.Duration) THEN
                  Lv_Des_Error:='El proceso ya se encuentra en ejecuci�n, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
                  RAISE Le_Error_process;
            END IF;            

          --Graba el proceso para controlar el bloqueo de los procesos
             i_Bck_Process_t:= ttc_Bck_Process_t(Process => i_process.Process,
                                                                                DesShort => i_process.DesShort,
                                                                                GroupTread => 0,
                                                                                Tread_NO => 0,
                                                                                FilterField => i_process.FilterField,
                                                                                LastRunDate => SYSDATE,
                                                                                MaxDelay => i_process.MaxDelay,
                                                                                Refresh_Cycle => i_process.Refresh_Cycle,
                                                                                SosPid => Pn_SosPid
                                                                               );
             Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
             IF Lv_Des_Error IS NOT NULL THEN
                  Lv_Des_Error:='Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesi�n levantada';
                  RAISE Le_Error_process;
             END IF;                                                                                                                                                            

            --Cargar los datos del Proceso TTC_IN --> TTC_rule_element
            i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GN_DESSHORT_IN);
            GN_MAX_DELETE:=i_rule_element.MaxRetenTrx;
           IF i_rule_element.Process IS NULL THEN
                  Lv_Des_Error:='No existen datos configurados en la tabla TTC_rule_element';
                  RAISE Le_Error_Valid;
            END IF;                        
                                                 
           ----------------------------------------------------------------------
           -- Ejecuci�n del proceso :Para depurar tablas
           ----------------------------------------------------------------------
           Prc_To_Delete_Tables (PV_ERROR);
           IF PV_ERROR IS NOT NULL THEN
                  Lv_Des_Error:=PV_ERROR;
                  RAISE Le_Error_Valid;
           END IF;                       
           ----------------------------------------------------------------------
           -- Ejecuci�n del proceso :Chekear procesos colgados
           ----------------------------------------------------------------------
           Prc_To_Ckeck_Process (PV_ERROR);
           IF PV_ERROR IS NOT NULL THEN
                  Lv_Des_Error:=PV_ERROR;
                  RAISE Le_Error_Valid;
           END IF;
           --Inicio 10695 Iro_Jro                         
           OPEN C_PARAMETROS_UPDATE(10695,'UPDATE_PLANING');
           FETCH C_PARAMETROS_UPDATE INTO LV_BANDERA;
           CLOSE C_PARAMETROS_UPDATE;
           IF LV_BANDERA = 'S' THEN
             OPEN C_PROCESS_HIST;
             FETCH C_PROCESS_HIST INTO LN_CANT_PROCEES;
             CLOSE C_PROCESS_HIST;
             IF LN_CANT_PROCEES > 0 THEN
               RELOJ.RC_TRX_UPDATE_PLANNING(PV_FECHA_INICIO => TO_CHAR(SYSDATE-1,'DD/MM/RRRR'),
                                            PV_FECHA_FIN    => TO_CHAR(SYSDATE-1,'DD/MM/RRRR'),
                                            PV_ERROR        => PV_ERROR);
              IF PV_ERROR IS NOT NULL THEN
                PV_ERROR:=NULL;
              END IF;
             END IF;
           END IF;
           --Fin 10695 Iro_Jro
           --Graba en la tabla programmer la fecha de ejecucion del proceso
            OPEN C_Trx_In ;
           FETCH C_Trx_In INTO Ln_Seqno;
           CLOSE C_Trx_In;                                                                                                          
           i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                                                                              Seqno => Ln_Seqno,
                                                                                                              RunDate => Ld_SysdateRun,
                                                                                                              LastModDate => Ld_SysdateRun
                                                                                                             );
           Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
           IF Lv_Des_Error IS NOT NULL THEN
                RAISE Le_Error_Valid;
           END IF;                                                                                                                                                            
          ----------------------------------------------------------------------
           -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='FIN '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 'Rc_Trx_Free_Ambiente-Prc_Header Ejecutado con Exito',
                                                 lv_unidad_registro_scp,--13/05/2013 Iro Juan Romero
                                                 0,
                                                 NULL,--13/05/2013 Iro Juan Romero
                                                 Null,Null,null,null,
                                                 'N',ln_error_scp,lv_error_scp);
           ----------------------------------------------------------------------
          --SCP:FIN                  
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);                  
          ----------------------------------------------------------------------------
          Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
          PV_ERROR:='Rc_Trx_Free_Ambiente-Prc_Header-SUCESSFUL';                                    
          --Registro del Fin del proceso 
          i_Log_InOut_t.LastDate:=SYSDATE;
          i_Log_InOut_t.StatusProcess:='OKI';
          i_Log_InOut_t.Comment_Run:= PV_ERROR;
          Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
          
     EXCEPTION            
           WHEN Le_Error_process THEN     
                PV_ERROR :=  'Proceso: Rc_Trx_Free_Ambiente- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;  
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:= PV_ERROR;                                                             
                 ----------------------------------------------------------------------
                 -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: Rc_Trx_Free_Ambiente-Error en Validaci�n';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 ln_registros_procesados_scp:=ln_registros_procesados_scp+0;
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
           WHEN Le_Error_Valid THEN                   
                PV_ERROR :=  'Proceso: Rc_Trx_Free_Ambiente- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;  
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:= PV_ERROR;                                                             
                 ----------------------------------------------------------------------
                 -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: Rc_Trx_Free_Ambiente-Error en Validaci�n';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 ln_registros_procesados_scp:=ln_registros_procesados_scp+0;
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
           WHEN OTHERS THEN                                        
                 PV_ERROR:='     Rc_Trx_Free_Ambiente-Prc_Head-Error al ejecutar el proceso-'||substr(SQLERRM,1,500);
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                  i_Log_InOut_t.LastDate:=SYSDATE;
                  i_Log_InOut_t.StatusProcess:='ERR';
                  i_Log_InOut_t.Comment_Run:=  PV_ERROR;                      
                  ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='   Proceso: Rc_Trx_Free_Ambiente-Error general';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         PV_ERROR,
                                                         Null,
                                                         2,
                                                         lv_unidad_registro_scp,
                                                         Null,Null,
                                                         null,null,'N',ln_error_scp,lv_error_scp);
                  ----------------------------------------------------------------------------
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                  
   END Prc_Header;

   /************************************************************************************
     Regulariza todos los co_id o contratos modificados por el operador a partir
     de la �ltima fecha procesada.
   *************************************************************************************/     
   PROCEDURE Prc_To_Delete_Tables(PV_Error                 OUT VARCHAR2) IS                                                     

     BEGIN              
      ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     lv_mensaje_apl_scp:='INICIO Prc_To_Delete_Tables: '||lv_id_proceso_scp;
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                           lv_mensaje_apl_scp,
                                                                           Null,
                                                                           Null,
                                                                           0,
                                                                           lv_unidad_registro_scp,
                                                                           Null,Null,
                                                                           null,null,'N',ln_error_scp,lv_error_scp
                                                                           );

       --Elimina los registros del dia anterior de la tabla ttc_customers_check_s para el proceso de reactivaci�n
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(GN_PROCESO_REAC_ALL,GN_DES_FLAG_DEL_REACTIVE);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString;
                 COMMIT;                       
          END IF;

     --9833 INI IRO AAM
         --Delete a la tabla RC_CTAS_REAC_RELOJ :: cuentas consideradas por reactivaci�n
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(GN_PROCESO_REAC_ALL,GN_DES_FLAG_DEL_REACT_RELOJ);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString;
                 COMMIT;                       
          END IF;
     --9833 FIN IRO AAM

         --Truncate a la tabla ttc_contract_all_in          
         i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(GN_PROCESO_IN,GN_DES_TRUN_TTC_CONTRACT_IN);                      
         IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                EXECUTE IMMEDIATE i_Rule_Values_t.Value_Return_LongString;                
         END IF;

         --13/05/2013 Iro Juan Romero - Inicio
         --Truncate a la tabla ttc_contract_all_DTH_in          
         i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(GN_PROCESO_IN,GN_DES_TRUN_TTC_CONTRACT_DTH);                      
         IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                EXECUTE IMMEDIATE i_Rule_Values_t.Value_Return_LongString;                
         END IF;
         --12/05/2013 Iro Juan Romero - Fin
    
         --09/03/2015 IRO Norma Pazmino - Inicio
         --Update a la tabla ttc_contractplanningDetails paso de estatus A-->Reactivada a P de pendiente
         /* i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_SET_A_TO_P);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString USING GV_STATUS_P;
                 COMMIT;                       
          END IF;*/
          --09/03/2015 IRO Norma Pazmino - Fin

       --Update a la tabla ttc_contractplanningDetails paso de estatus E-->Con Error a P de pendiente
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_SET_E_TO_P);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString USING GV_STATUS_P;
                 COMMIT;                       
          END IF;

       --Update a la tabla ttc_contractplanningDetails paso de estatus T-->Transferida y que luego pago a P de pendiente
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_SET_T_TO_P);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString USING GV_STATUS_P;
                 COMMIT;                       
          END IF;

       --Update a la tabla ttc_contractplanningDetails paso de estatus L-->Reactivada con limite de credito a P de pendiente
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_SET_L_TO_P);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString USING GV_STATUS_P;
                 COMMIT;                       
          END IF;

       --Update a la tabla ttc_contractplanningDetails paso de estatus E-->Trx con Error  a V  de verificaci�n luego que dentro del periodo de 15 d�as o mas ha sido reprocesada,
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_SET_E_TO_V);
          IF i_Rule_Values_t.Value_Return_ShortString='S' THEN
                 EXECUTE IMMEDIATE  i_Rule_Values_t.Value_Return_LongString USING GV_STATUS_V;
                 COMMIT;                       
          END IF;

       --Valor del parametro, para identificar si la eliminacion la realiza desde el remoto o local.
          i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_TTC_FLAG_PROCESS_LOCAL);
          IF  i_Rule_Values_t.Value_Return_ShortString='BSCS' THEN
               --Delete a todas las tablas TTC_PROCESS_CONTRACT de la tabla ttc_relation_Objects
                 OPEN C_NameTables;
                FETCH C_NameTables  BULK COLLECT INTO  V_TableName;
                CLOSE C_NameTables;
                FOR J IN  V_TableName.FIRST .. V_TableName.LAST LOOP                              
                     EXECUTE IMMEDIATE 'Delete RELOJ.'||V_TableName(J);
                     COMMIT;
                END LOOP;                          
               --Delete a la tabla ttc_process_contrtoretry_s de los registros para reintentos
                 EXECUTE IMMEDIATE 'Delete RELOJ.ttc_process_contrtoretry_s';
                 COMMIT;

               --Delete a la tabla temporal de las transacciones procesadas en AXIS  
                 EXECUTE IMMEDIATE 'DELETE  RELOJ.TTC_PROCESS_CONTRHIST_TMP_S';
                 COMMIT;          
                 --       
          END IF;
                          
          ----------------------------------------------------------------------
           -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='FIN Prc_To_Delete_Tables '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 'Prc_To_Delete_Tables- Ejecutado con Exito'
                                                 ,Null,
                                                 0,
                                                 lv_unidad_registro_scp,
                                                 Null,Null,null,null,
                                                 'N',ln_error_scp,lv_error_scp);
           ----------------------------------------------------------------------
          --SCP:FIN                         

           PV_Error:=NULL;
    EXCEPTION            
           WHEN OTHERS THEN        
               ROLLBACK;                                
              PV_Error:='Prc_To_Delete_Tables-Error en la ejecuci�n del proceso al eliminar las tablas de trabajo'||substr(SQLERRM,1,500);                  
   END Prc_To_Delete_Tables;
   
   --*************************************************************************************
   PROCEDURE Prc_To_Ckeck_Process(PV_Error  OUT VARCHAR2) IS                                                     
       CURSOR C_Process IS
       SELECT  process,DESSHORT,round(SYSDATE-lastrundate) tiempo
          FROM  TTC_BCK_PROCESS_S; 
   BEGIN              
              ----------------------------------------------------------------------------------------------------
           -- SCP: C�digo generado autom�ticamente. Registro de mensaje de Inicio
              ----------------------------------------------------------------------------------------------------
           lv_mensaje_apl_scp:='INICIO Prc_To_Ckeck_Process: '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                 lv_mensaje_apl_scp,
                                                                                 Null,
                                                                                 Null,
                                                                                 0,
                                                                                 lv_unidad_registro_scp,
                                                                                 Null,Null,
                                                                                 null,null,'N',ln_error_scp,lv_error_scp
                                                                                 );

          --Verifica el tiempo de ejecuci�n de los procesos, si existen procesos colgadas los elimina
          --para empezar el proceso sin problemas.
           FOR C_P IN C_Process LOOP
                   --Cargar los datos del Proceso TTC_IN -->TTC_Process_s                  
                   i_process_Ckeck:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(c_p.desshort);      
                   IF i_process_Ckeck.Process IS NOT NULL THEN
                         IF nvl(C_P.TIEMPO,1)>=i_process_Ckeck.Duration THEN -- IRO DZ 16/06/2017
                              DELETE RELOJ.TTC_BCK_PROCESS_S WHERE desshort =c_p.desshort;
                              COMMIT;                              
                         END IF;                                                                      
                    END IF;
           END LOOP;

              ----------------------------------------------------------------------------------------------------
               -- SCP: C�digo generado autom�ticamente. Registro de mensaje de Fin
              ----------------------------------------------------------------------------------------------------
               lv_mensaje_apl_scp:='FIN Prc_To_Ckeck_Process '||lv_id_proceso_scp;
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                     lv_mensaje_apl_scp,
                                                     'Prc_To_Ckeck_Process- Ejecutado con Exito'
                                                     ,Null,
                                                     0,
                                                     lv_unidad_registro_scp,
                                                     Null,Null,null,null,
                                                     'N',ln_error_scp,lv_error_scp);
              ----------------------------------------------------------------------------------------------------
              --SCP:FIN                         
                                        
               PV_Error:=NULL;
    EXCEPTION            
           WHEN OTHERS THEN        
               ROLLBACK;                                
              PV_Error:='Prc_To_Ckeck_Process-Error en la ejecuci�n del proceso'||substr(SQLERRM,1,500);                     
                                             
   END Prc_To_Ckeck_Process;



END Rc_Trx_Free_Ambiente ;
/
