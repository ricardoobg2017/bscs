create or replace package reloj.RC_API_SCORE is

/*******************************************************************************************************
    LIDER SIS:      KARLA AVENDANO
    LIDER IRO:      IRO LAURA CAMPOVERDE 
    CREADO POR:     IRO JOHANNA JIMENEZ 
    PROYECTO:       10036 FACTURACION COMPLETA DE CLIENTES SUSPENDIDOS
    FECHA:          21/01/2015
    Proosito:       Paquete creado con la finalidad de manejar logica
                    de validacion de SCORE de una cuenta
  ********************************************************************************************************/

  type TIPO_CLIENTE IS RECORD(
    ID_TIPO_CLIENTE NUMBER,
    NOMBRE          VARCHAR2(100),
    SENTENCIA_SQL   VARCHAR2(1000));

  TYPE TIPO_CLIENTES IS TABLE OF TIPO_CLIENTE;

  type PERIODO IS RECORD(
    FECHA_INICIO DATE,
    FECHA_FIN    DATE);

  TYPE PERIODOS IS TABLE OF PERIODO INDEX BY VARCHAR2(2);

  -- Private variable declarations
  lt_tipo_clientes TIPO_CLIENTES;
  lt_periodo       PERIODOS;

  -- Public function and procedure declarations
  Procedure PRC_CARGA_SQL_X_CLIENTE(PV_ERROR OUT VARCHAR2);

  Procedure PRC_GENERA_FECHAS(PV_ERROR OUT VARCHAR2);

  Procedure PRC_GET_TIPO_CLIENTE(PV_FORMA_PAGO   IN VARCHAR2,
                                 PV_CSTRADECODE  IN VARCHAR2,
                                 PV_CATEGORIA    IN VARCHAR2,
                                 PN_TIPO_CLIENTE OUT NUMBER,
                                 PV_TIPO_CLIENTE OUT VARCHAR2,
                                 PV_ERROR        OUT VARCHAR2);

  Procedure PRC_VALIDA_SCORE(PV_CUENTA        IN VARCHAR2,
                             PV_BILLCYCLE     IN VARCHAR2,
                             PV_CSTRADECODE   IN VARCHAR2,
                             PV_APL_NEW_ESQU  OUT VARCHAR2,
                             PN_TIPO_CLIENTE  OUT NUMBER,
                             PV_TIPO_CLIENTE  OUT VARCHAR2,
                             PV_FORMA_PAGO    OUT VARCHAR2,
                             PV_CATEGORIA     OUT VARCHAR2,
                             PV_TIPO_CUENTA   OUT VARCHAR2,
                             PV_ID_FINANCIERA OUT VARCHAR2,
                             PV_CICLO         OUT VARCHAR2,
                             PN_SCORE         OUT NUMBER,
                             PD_FIN_CICLO     OUT DATE,
                             PD_INI_CICLO     OUT DATE,
                             PD_FECHA_SUSP    OUT DATE,
                             PV_ERROR         OUT VARCHAR2);

  Procedure PRC_INSERTA_DATOS_REPORTE(PV_CUENTA          IN VARCHAR2,
                                      PV_SERVICIO        IN VARCHAR2,
                                      PN_CUSTOMER        IN NUMBER,
                                      PV_TIPO_CUENTA     IN VARCHAR2,
                                      PV_TIPO_CLIENTE    IN VARCHAR2,
                                      PV_FORMA_PAGO      IN VARCHAR2,
                                      PV_CATEGORIA       IN VARCHAR2,
                                      PV_TIPO_APROBACION IN VARCHAR2,
                                      PV_FINANCIERA      IN VARCHAR2,
                                      PV_CICLO           IN VARCHAR2,
                                      PN_SCORE           IN NUMBER,
                                      PD_FECHA_SUSP      IN DATE,
                                      PD_INI_CICLO       IN DATE,
                                      PD_FIN_CICLO       IN DATE,
                                      PV_ERROR           OUT VARCHAR2);

end RC_API_SCORE;
/
create or replace package body reloj.RC_API_SCORE is

  -- Function and procedure implementations
  Procedure PRC_CARGA_SQL_X_CLIENTE(PV_ERROR OUT VARCHAR2) is
  
    CURSOR C_TIPO_CLIENTE IS
      SELECT ID_TIPO_CLIENTE, NOMBRE, SENTENCIA_SQL
        FROM RC_TIPO_CLIENTE
       WHERE ESTADO = 'A';
  
  begin
  
    OPEN C_TIPO_CLIENTE;
    FETCH C_TIPO_CLIENTE BULK COLLECT
      INTO lt_tipo_clientes;
    CLOSE C_TIPO_CLIENTE;
  
  exception
    when others then
      PV_ERROR := 'RC_API_SCORE.PRC_CARGA_SQL_X_CLIENTE --> Error: ' ||
                  substr(sqlerrm, 1, 500);
  end;

  Procedure PRC_GENERA_FECHAS(PV_ERROR OUT VARCHAR2) is
  
    lv_dia_ini  varchar2(2);
    lv_dia_fin  varchar2(2);
    lv_mes_ini  varchar2(2);
    lv_anio_ini varchar2(4);
    lv_mes_fin  varchar2(2);
    lv_anio_fin varchar2(4);
  
  BEGIN
  
    FOR I IN (select b.id_ciclo, b.dia_ini_ciclo, b.dia_fin_ciclo
                From sysadm.fa_ciclos_bscs b)
    
     LOOP
    
      lv_dia_ini := I.Dia_Ini_Ciclo;
      lv_dia_fin := I.Dia_fin_ciclo;
      --
      If to_number(to_char(sysdate, 'dd')) < to_number(lv_dia_ini) Then
      
        If to_number(to_char(sysdate, 'mm')) = 1 Then
          lv_mes_ini  := '12';
          lv_anio_ini := to_char(sysdate, 'yyyy') - 1;
        Else
          lv_mes_ini  := lpad(to_char(sysdate, 'mm') - 1, 2, '0');
          lv_anio_ini := to_char(sysdate, 'yyyy');
        End If;
        lv_mes_fin  := to_char(sysdate, 'mm');
        lv_anio_fin := to_char(sysdate, 'yyyy');
      
      Else
      
        If to_number(to_char(sysdate, 'mm')) = 12 Then
          lv_mes_fin  := '01';
          lv_anio_fin := to_char(sysdate, 'yyyy') + 1;
        Else
          lv_mes_fin  := lpad(to_char(sysdate, 'mm') + 1, 2, '0');
          lv_anio_fin := to_char(sysdate, 'yyyy');
        End If;
        lv_mes_ini  := to_char(sysdate, 'mm');
        lv_anio_ini := to_char(sysdate, 'yyyy');
      
      End If;
    
      lt_periodo(I.id_ciclo).fecha_inicio := to_date(lv_dia_ini || '/' ||lv_mes_ini || '/' ||lv_anio_ini,'dd/mm/yyyy');
      lt_periodo(I.id_ciclo).fecha_fin := to_date(lv_dia_fin || '/' ||lv_mes_fin || '/' ||lv_anio_fin,'dd/mm/yyyy');
    
    END LOOP;
  
  exception
    when others then
      PV_ERROR := 'RC_API_SCORE.PRC_GENERA_FECHAS --> Error: ' ||
                  substr(sqlerrm, 1, 500);
  end;

  Procedure PRC_GET_TIPO_CLIENTE(PV_FORMA_PAGO   IN VARCHAR2,
                                 PV_CSTRADECODE  IN VARCHAR2,
                                 PV_CATEGORIA    IN VARCHAR2,
                                 PN_TIPO_CLIENTE OUT NUMBER,
                                 PV_TIPO_CLIENTE OUT VARCHAR2,
                                 PV_ERROR        OUT VARCHAR2) IS
  
    lv_sentencia VARCHAR2(1000);
    lv_sql       VARCHAR2(2000);
    ln_salida    NUMBER;
    lb_existe    BOOLEAN := FALSE;
    ln_posicion  NUMBER;
  
  begin
  
    FOR i IN 1 .. lt_tipo_clientes.count LOOP
      ln_posicion  := i;
      lv_sentencia := lt_tipo_clientes(i).sentencia_sql;
    
      --Se arma sql final
      lv_sql := 'SELECT COUNT(CATEGORIA) FROM ( SELECT ''' || PV_CATEGORIA ||
                ''' CATEGORIA,''' || PV_FORMA_PAGO || ''' FORMA_PAGO,''' ||
                PV_CSTRADECODE || ''' TIPO_CREDITO' ||
                ' FROM DUAL ) A WHERE ' || lv_sentencia;
    
      EXECUTE IMMEDIATE lv_sql
        INTO ln_salida;
    
      IF ln_salida = 1 THEN
        lb_existe := TRUE;
      END IF;
    
      EXIT WHEN lb_existe = TRUE;
    END LOOP;
  
    IF lb_existe THEN
      PN_TIPO_CLIENTE := lt_tipo_clientes(ln_posicion).ID_TIPO_CLIENTE;
      PV_TIPO_CLIENTE := lt_tipo_clientes(ln_posicion).NOMBRE;
    ELSE
      PN_TIPO_CLIENTE := -1;
    END IF;
  
  exception
    when others then
      PV_ERROR := 'RC_API_SCORE.PRC_GET_TIPO_CLIENTE --> Error: ' ||substr(sqlerrm, 1, 500);
  end;

  Procedure PRC_VALIDA_SCORE(PV_CUENTA        IN VARCHAR2,
                             PV_BILLCYCLE     IN VARCHAR2,
                             PV_CSTRADECODE   IN VARCHAR2,
                             PV_APL_NEW_ESQU  OUT VARCHAR2,
                             PN_TIPO_CLIENTE  OUT NUMBER,
                             PV_TIPO_CLIENTE  OUT VARCHAR2,
                             PV_FORMA_PAGO    OUT VARCHAR2,
                             PV_CATEGORIA     OUT VARCHAR2,
                             PV_TIPO_CUENTA   OUT VARCHAR2,
                             PV_ID_FINANCIERA OUT VARCHAR2,
                             PV_CICLO         OUT VARCHAR2,
                             PN_SCORE         OUT NUMBER,
                             PD_FIN_CICLO     OUT DATE,
                             PD_INI_CICLO     OUT DATE,
                             PD_FECHA_SUSP    OUT DATE,
                             PV_ERROR         OUT VARCHAR2) IS
  
    lv_error         VARCHAR2(600);
    lv_forma_pago    VARCHAR2(5);
    lv_tipo_cuenta   VARCHAR2(2);
    lv_categoria     VARCHAR2(5);
    lv_id_financiera VARCHAR2(5);
    lv_ciclo         VARCHAR2(5);
    ln_tipo_cliente  NUMBER;
    lv_tipo_cliente  VARCHAR2(100);
    ln_score_actual  NUMBER;
    lv_sql           VARCHAR2(500);
    lv_result        VARCHAR2(5);
  
    lb_found BOOLEAN;
    le_error EXCEPTION;
    le_difer_val EXCEPTION;
  
    CURSOR C_CICLO(CV_BILLCYCLE VARCHAR2) IS
      SELECT ID_CICLO
        FROM sysadm.FA_CICLOS_AXIS_BSCS
       WHERE ID_CICLO_ADMIN = CV_BILLCYCLE;
  
    CURSOR C_SCORE_ACTUAL(CV_CUENTA VARCHAR2) IS
      SELECT A.PROMEDIO
        FROM SYSADM.MMAG_RESUL_SCORE_CUENTA A
       WHERE A.CUENTA = CV_CUENTA
         AND A.FECHA_CORTE = (SELECT MAX(B.FECHA_CORTE)
                                FROM SYSADM.MMAG_RESUL_SCORE_CUENTA B
                               WHERE B.CUENTA = CV_CUENTA);
  
    CURSOR C_CONFG_SCORE(CN_TIPO_CLIENTE  NUMBER,
                         CV_FORMA_PAGO    VARCHAR2,
                         CV_TIPO_CUENTA   VARCHAR2,
                         CV_ID_FINANCIERA VARCHAR2) IS
      SELECT A.CONDICION,
             A.SCORE,
             A.DIAS,
             A.APL_VAL_SCORE,
             A.APL_DIREC_NEW_ESQUEMA
        FROM RC_CONFIGURACION_SCORE A
       WHERE A.ID_TIPO_CLIENTE = CN_TIPO_CLIENTE
         AND A.ID_FORMA_PAGO = CV_FORMA_PAGO
         AND NVL(A.TIPO_CUENTA, 'X') = CV_TIPO_CUENTA
         AND NVL(A.ID_FINANCIERA, 'X') = CV_ID_FINANCIERA;
  
    CURSOR C_INFO_CTA(CV_CUENTA VARCHAR2) IS
      SELECT a.id_forma_pago FORMA_PAGO,
             b.id_clase_cliente CATEGORIA,
             DECODE(c.tipo_cuenta, 'T', NULL, c.tipo_cuenta) TIPO_CUENTA,
             c.id_financiera ID_FINANCIERA
        from PORTA.cl_contratos@AXIS              a,
             PORTA.cl_personas_empresa@AXIS       b,
             PORTA.cl_autorizaciones_debitos@AXIS c
       where a.codigo_doc = CV_CUENTA
         and a.estado = 'A'
         and b.id_persona = a.id_persona
         and c.id_contrato(+) = a.id_contrato
         and c.estado(+) = 'A';
  
    lc_confg_score C_CONFG_SCORE%ROWTYPE;
  
  begin
    --Obtengo informacion de la cuenta desde AXIS
    OPEN C_INFO_CTA(PV_CUENTA);
    FETCH C_INFO_CTA
      INTO LV_FORMA_PAGO, LV_CATEGORIA, LV_TIPO_CUENTA, LV_ID_FINANCIERA;
    lb_found := C_INFO_CTA%FOUND;
    CLOSE C_INFO_CTA;
  
    IF NOT lb_found THEN
      lv_error := 'No se encontro informacion de la cuenta en Axis';
      RAISE le_error;
    END IF;
  
    --Obtengo informacion del customer en BSCS
    OPEN C_CICLO(pv_billcycle);
    FETCH C_CICLO
      INTO lv_ciclo;
    lb_found := C_CICLO%FOUND;
    CLOSE C_CICLO;
  
    IF NOT lb_found THEN
      lv_error := 'No se encontro informacion en la FA_CICLOS_AXIS_BSCS para el BILLCYCLE ' ||
                  pv_billcycle;
      RAISE le_error;
    END IF;
  
    --Obtengo el tipo de cliente
    RC_API_SCORE.prc_get_tipo_cliente(pv_forma_pago   => lv_forma_pago,
                                      pv_cstradecode  => pv_cstradecode,
                                      pv_categoria    => lv_categoria,
                                      pn_tipo_cliente => ln_tipo_cliente,
                                      pv_tipo_cliente => lv_tipo_cliente,
                                      pv_error        => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
    IF ln_tipo_cliente = -1 THEN
      lv_error := 'El cliente no corresponde a ninguno de los tipos de clientes configurados';
      RAISE le_error;
    END IF;
  
    --Consulto configuracion de SCORE   
    OPEN C_CONFG_SCORE(ln_tipo_cliente,
                       lv_forma_pago,
                       NVL(lv_tipo_cuenta, 'X'),
                       NVL(lv_id_financiera, 'X'));
    FETCH C_CONFG_SCORE
      INTO lc_confg_score;
    lb_found := C_CONFG_SCORE%FOUND;
    CLOSE C_CONFG_SCORE;
  
    IF NOT lb_found THEN
      lv_error         := 'No se encontra configuracion de SCORE para ID_TIPO_CLIENTE:' ||
                          ln_tipo_cliente || ' ID_FORMA_PAGO:' ||
                          lv_forma_pago || ' TIPO_CUENTA:' ||
                          lv_tipo_cuenta || ' ID_FINANCIERA:' ||
                          lv_id_financiera;
      PD_FIN_CICLO     := lt_periodo(lv_ciclo).fecha_fin;
      PD_INI_CICLO     := lt_periodo(lv_ciclo).fecha_inicio;
      PD_FECHA_SUSP    := NULL;
      PN_TIPO_CLIENTE  := ln_tipo_cliente;
      PV_TIPO_CLIENTE  := lv_tipo_cliente;
      PV_FORMA_PAGO    := lv_forma_pago;
      PV_CATEGORIA     := lv_categoria;
      PV_CICLO         := lv_ciclo;
      PN_SCORE         := NULL;
      PV_TIPO_CUENTA   := lv_tipo_cuenta;
      PV_ID_FINANCIERA := lv_id_financiera;
      RAISE le_error;
    END IF;
  
    --Consulto el Valor de Score Actual
    OPEN C_SCORE_ACTUAL(PV_CUENTA);
    FETCH C_SCORE_ACTUAL
      INTO ln_score_actual;
    lb_found := C_SCORE_ACTUAL%FOUND;
    CLOSE C_SCORE_ACTUAL;
    
    IF NOT lb_found THEN
      ln_score_actual:=NVL(ln_score_actual,0);
    END IF;
  
    --Valido informacion de la configuracion de SCORE
    --Se valida si la cuenta posee un valor de SCORE calculado o
    --si la configuracion no aplica validacion de SCORE y no aplica
    --directamente el nuevo esquema
    IF (lc_confg_score.apl_val_score = 'N' and
       lc_confg_score.apl_direc_new_esquema = 'N') THEN
      PD_FIN_CICLO     := lt_periodo(lv_ciclo).fecha_fin;
      PD_INI_CICLO     := lt_periodo(lv_ciclo).fecha_inicio;
      PD_FECHA_SUSP    := NULL;
      PV_APL_NEW_ESQU  := 'N';
      PN_TIPO_CLIENTE  := ln_tipo_cliente;
      PV_TIPO_CLIENTE  := lv_tipo_cliente;
      PV_FORMA_PAGO    := lv_forma_pago;
      PV_CATEGORIA     := lv_categoria;
      PV_CICLO         := lv_ciclo;
      PN_SCORE         := ln_score_actual;
      PV_TIPO_CUENTA   := lv_tipo_cuenta;
      PV_ID_FINANCIERA := lv_id_financiera;
      RAISE le_difer_val;
    END IF;
  
    --Se valida si la configuracion aplica el nuevo directamente
    IF lc_confg_score.apl_direc_new_esquema = 'S' THEN
      PD_FIN_CICLO     := lt_periodo(lv_ciclo).fecha_fin;
      PD_INI_CICLO     := lt_periodo(lv_ciclo).fecha_inicio;
      PD_FECHA_SUSP    := lt_periodo(lv_ciclo)
                          .fecha_fin + lc_confg_score.dias;
      PV_APL_NEW_ESQU  := 'S';
      PN_TIPO_CLIENTE  := ln_tipo_cliente;
      PV_TIPO_CLIENTE  := lv_tipo_cliente;
      PV_FORMA_PAGO    := lv_forma_pago;
      PV_CATEGORIA     := lv_categoria;
      PV_CICLO         := lv_ciclo;
      PN_SCORE         := NVL(ln_score_actual, -1);
      PV_TIPO_CUENTA   := lv_tipo_cuenta;
      PV_ID_FINANCIERA := lv_id_financiera;
      RAISE le_difer_val;
    END IF;
  
    --Valido el SCORE ACTUAL contra el SCORE CONFIGURADO
    lv_sql := 'SELECT CASE WHEN ' || ln_score_actual ||
              lc_confg_score.condicion || lc_confg_score.score ||
              ' THEN ''S'' ELSE ''N'' END FROM DUAL';
  
    EXECUTE IMMEDIATE lv_sql
      INTO lv_result;
  
    IF lv_result = 'S' THEN
      PD_FECHA_SUSP   := lt_periodo(lv_ciclo)
                         .fecha_fin + lc_confg_score.dias;
      PV_APL_NEW_ESQU := 'S';
    ELSE
      PD_FECHA_SUSP   := NULL;
      PV_APL_NEW_ESQU := 'N';
    END IF;
  
    PD_FIN_CICLO     := lt_periodo(lv_ciclo).fecha_fin;
    PD_INI_CICLO     := lt_periodo(lv_ciclo).fecha_inicio;
    PN_TIPO_CLIENTE  := ln_tipo_cliente;
    PV_TIPO_CLIENTE  := lv_tipo_cliente;
    PV_FORMA_PAGO    := lv_forma_pago;
    PV_CATEGORIA     := lv_categoria;
    PV_CICLO         := lv_ciclo;
    PN_SCORE         := ln_score_actual;
    PV_TIPO_CUENTA   := lv_tipo_cuenta;
    PV_ID_FINANCIERA := lv_id_financiera;
  
  exception
    when le_difer_val then
      null;
    when le_error then
      PV_APL_NEW_ESQU := 'N';
      PV_ERROR        := 'RC_API_SCORE.PRC_VALIDA_SCORE - ' || lv_error;
    when others then
      PV_APL_NEW_ESQU := 'N';
      PV_ERROR        := 'RC_API_SCORE.PRC_VALIDA_SCORE --> Error: ' || substr(sqlerrm, 1, 500);
  end;

  Procedure PRC_INSERTA_DATOS_REPORTE(PV_CUENTA          IN VARCHAR2,
                                      PV_SERVICIO        IN VARCHAR2,
                                      PN_CUSTOMER        IN NUMBER,
                                      PV_TIPO_CUENTA     IN VARCHAR2,
                                      PV_TIPO_CLIENTE    IN VARCHAR2,
                                      PV_FORMA_PAGO      IN VARCHAR2,
                                      PV_CATEGORIA       IN VARCHAR2,
                                      PV_TIPO_APROBACION IN VARCHAR2,
                                      PV_FINANCIERA      IN VARCHAR2,
                                      PV_CICLO           IN VARCHAR2,
                                      PN_SCORE           IN NUMBER,
                                      PD_FECHA_SUSP      IN DATE,
                                      PD_INI_CICLO       IN DATE,
                                      PD_FIN_CICLO       IN DATE,
                                      PV_ERROR           OUT VARCHAR2) IS
  
    LV_CEDULA          VARCHAR2(15);
    LV_PAGOS           NUMBER;
    LN_SALDO           NUMBER;
    LN_CREDITO         NUMBER;
    LV_COMPANIA        VARCHAR2(3);
    LV_SERVICIO        VARCHAR2(25);
    LV_BAND_MORA       VARCHAR2(5);
    LN_DEUDA_SERVICIO  NUMBER;
    LN_DIAS_MORA       NUMBER;
    LN_SECUENCIA       NUMBER;
    LV_ERROR           VARCHAR2(4000);
    LV_SQL             VARCHAR2(2000);
  
    LE_ERROR           EXCEPTION;
  
    CURSOR C_CEDULA(CN_CUSTOMER VARCHAR2) IS
      select cssocialsecno
        from sysadm.ccontact_all
       where customer_id = CN_CUSTOMER
         and ccbill = 'X';
  
    CURSOR C_DEUDA(CN_CUSTOMER VARCHAR2) IS
      SELECT /*+ rule +*/
       nvl(SUM(b.ohopnamt_doc), 0) saldo --lo que queda despues de pagos
        FROM sysadm.orderhdr_all b
       WHERE b.customer_id = CN_CUSTOMER
         AND b.ohstatus IN ('IN', 'CM', 'CO')
         AND b.ohopnamt_doc <> 0
         AND b.ohduedate < trunc(sysdate);
  
    CURSOR C_CREDITO(CN_CUSTOMER VARCHAR2, CD_FECHA_INI_CICLO DATE) IS
      SELECT /*+ rule +*/
       nvl(SUM(amount), 0) creditos
        FROM sysadm.fees b
       WHERE b.customer_id = CN_CUSTOMER
         AND b.period > 0
         and b.sncode = 46
         AND trunc(b.entdate) >= to_date(CD_FECHA_INI_CICLO, 'dd/mm/yyyy');
  
    CURSOR C_COMPANIA(CN_CUSTOMER VARCHAR2) is
      select cost_code
        from sysadm.customer_all, sysadm.costcenter
       where custcode = CN_CUSTOMER
         and costcenter_id = cost_id;
  
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    LV_ID_PROCESO_SCP  VARCHAR2(100) := 'API_SCORE_RELOJ';
    LN_ERROR_SCP       NUMBER := 0;
    LV_ERROR_SCP       VARCHAR2(500);
    LV_VALOR_SCP       VARCHAR2(4000) := '0';
    LV_DESCRIPCION_SCP VARCHAR2(500);
    ---------------------------------------------------------------
  BEGIN
  
    lv_servicio := sysadm.OBTIENE_TELEFONO_DNC(PV_SERVICIO);
  
    --Se consulta la cedula del cliente
    OPEN C_CEDULA(PN_CUSTOMER);
    FETCH C_CEDULA
      INTO LV_CEDULA;
    CLOSE C_CEDULA;
  
    --se Obtine la deuda del Servicio
    OPEN C_DEUDA(PN_CUSTOMER);
    FETCH C_DEUDA
      INTO Ln_SALDO;
    CLOSE C_DEUDA;
  
    OPEN C_CREDITO(PN_CUSTOMER, PD_INI_CICLO);
    FETCH C_CREDITO
      INTO Ln_CREDITO;
    CLOSE C_CREDITO;
  
    --Calculo de deuda
    ln_deuda_servicio := Ln_SALDO + Ln_CREDITO;
  
    -- Obtener los dias de mora para un servicio
    Scp.Sck_Api.Scp_Parametros_Procesos_Lee(Pv_Id_Parametro => 'DIAS_MORA_SCORE',
                                            Pv_Id_Proceso   => lv_id_proceso_scp,
                                            Pv_Valor        => lv_valor_scp,
                                            Pv_Descripcion  => lv_descripcion_scp,
                                            Pn_Error        => ln_error_scp,
                                            Pv_Error        => lv_error_scp);
    
    ln_dias_mora := to_number(nvl(lv_valor_scp, '-1'));
    
    
    OPEN C_COMPANIA(PV_CUENTA);
    FETCH C_COMPANIA
      INTO LV_COMPANIA;
    CLOSE C_COMPANIA;

    select SCORE_CLIENTE_SEQ.nextval into LN_SECUENCIA from dual;
    
    EXECUTE IMMEDIATE 'insert into RC_SUSPENDIDOS_FIN_CICLO
      (ID_SECUENCIA,CUENTA,CUSTOMER_ID,ID_SERVICIO,CED_IDENTIDAD,
       CICLO,DEUDA,DIAS_MORA,CALIFICACION,TIPO_CLIENTE,
       FORMA_PAGO,CATEGORIA,TIPO_APROBACION,TIPO_CUENTA,
       ID_FINANCIERA,COMPA�IA,PAGO_ACUMULADOS,FECHA_SUSPENSION,
       FECHA_INGRESO)
    values
      (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,
       :14,:15,:16,:17,:18,:19)'
      USING 
       LN_SECUENCIA,
       PV_CUENTA,
       PN_CUSTOMER,
       LV_SERVICIO,
       LV_CEDULA,
       PV_CICLO,
       LN_DEUDA_SERVICIO,
       LN_DIAS_MORA,
       PN_SCORE,
       PV_TIPO_CLIENTE,
       PV_FORMA_PAGO,
       PV_CATEGORIA,
       PV_TIPO_APROBACION,
       PV_TIPO_CUENTA,
       PV_FINANCIERA,
       LV_COMPANIA,
       0,
       PD_FECHA_SUSP,
       SYSDATE;
  
  exception
    when le_error then
      PV_ERROR := 'RC_API_SCORE.PRC_INSERTA_DATOS_REPORTE - ' || lv_error;
    when others then
      PV_ERROR := 'RC_API_SCORE.PRC_INSERTA_DATOS_REPORTE --> Error: ' ||substr(sqlerrm, 1, 500);
  END;

end RC_API_SCORE;
/
