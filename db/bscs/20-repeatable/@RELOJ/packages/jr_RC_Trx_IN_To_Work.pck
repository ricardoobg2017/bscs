create or replace package reloj.jr_RC_Trx_IN_To_Work IS

      GN_PROCESO_IN   NUMBER:=13;
      GN_DESSHORT_IN VARCHAR2(15):='TTC_IN_CUSTOMER';
    
      CURSOR C_IN IS
	        SELECT /*+ ALL_ROWS */
              TTC_View_Customers_In_type(
                          customer_id, 
                          co_id, 
                          dn_num,
                          billcycle, 
                          custcode,
                          tmcode, 
                          prgcode, 
                          cstradecode, 
                          product_history_date,
                          CO_EXT_CSUIN, 
                          co_userlastmod,
                          ttc_insertiondate,
                          ttc_lastmoddate)
           FROM TTC_View_Customers_In;   
           
      CURSOR C_DTH_IN IS
	        SELECT /*+ ALL_ROWS */
              TTC_View_Customers_In_type(
                          customer_id, 
                          co_id, 
                          dn_num,
                          billcycle, 
                          custcode,
                          tmcode, 
                          prgcode, 
                          cstradecode, 
                          product_history_date,
                          CO_EXT_CSUIN, 
                          co_userlastmod,
                          ttc_insertiondate,
                          ttc_lastmoddate)
           FROM TTC_View_Customers_dth_In;       
      
      PROCEDURE Prc_Header(Pn_SosPid  IN NUMBER,PV_TIPO IN NUMBER, PV_Error  OUT VARCHAR2); 
                                                   
      PROCEDURE Prc_To_Save_IN(PV_Error  OUT VARCHAR2);
      
      PROCEDURE Prc_To_Save_DTH_IN(PV_Error  OUT VARCHAR2);
      
      /*[8250] JJO */
      PROCEDURE Prc_Insert_Programmer(PV_TIPO IN VARCHAR2, PV_ERROR   OUT VARCHAR2);                                               

END jr_RC_Trx_IN_To_Work;
/
create or replace package body reloj.jr_RC_Trx_IN_To_Work  is


  /*********************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8250 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
  FECHA:          07/12/2012
  PROPOSITO:      insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha  
                  de la ultima cuenta insertada en TTC_CONTRACT_ALL_IN
                  y mejoras en la vista TTC_View_Customers_In
  *********************************************************************/
  
  -- Private type declarations ,constant declarations,variable declarations
        i_process                    ttc_process_t;
        i_rule_element               ttc_Rule_Element_t;
        i_Bck_Process_t              ttc_Bck_Process_t;
        i_Log_InOut_t                ttc_Log_InOut_t; 
        i_Process_Programmer_t       ttc_Process_Programmer_t;       
        i_Customers_In_t             ttc_View_Customers_In_t;
        i_Cust_to_Save_In_t          ttc_View_Customers_In_type;
        
     --SCP:VARIABLES
    ------------------------------------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ------------------------------------------------------------------------------------------
        ln_id_bitacora_scp number:=0; 
        ln_total_registros_scp number:=0;
        lv_id_proceso_scp varchar2(100):='TTC_IN';
        lv_referencia_scp varchar2(100):='RC_Trx_IN_To_Work.Prc_Header';
        lv_unidad_registro_scp varchar2(30):='IN CO_ID';
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
    ---------------------------------------------------------------
    --8250 JJO - Inicio
    CURSOR C_Trx_In IS
          SELECT  TTC_TRX_IN.NEXTVAL FROM dual;     
    --8250 JJO - Fin

--**********************************************************************************
         FUNCTION Fct_Save_ToWk  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;        
                  INSERT INTO  TTC_CONTRACT_ALL_IN_S NOLOGGING
                  VALUES (i_Cust_to_Save_In_t) ;                          
        
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_Save_ToWk-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla TTC_CONTRACT_ALL_IN_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;        
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk-Error General al intentar grabar en la tabla TTC_CONTRACT_ALL_IN_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
        END;

--**********************************************************************************

--**********************************************************************************
         FUNCTION Fct_Save_dth_ToWk  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;        
                  INSERT INTO TTC_CONTRACT_ALL_DTH_IN_S NOLOGGING
                  VALUES (i_Cust_to_Save_In_t) ;                          
        
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_Save_ToWk_dth-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla TTC_CONTRACT_ALL_DTH_IN, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;        
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk_dth-Error General al intentar grabar en la tabla TTC_CONTRACT_ALL_DTH_IN, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
        END;

--**********************************************************************************

    PROCEDURE Prc_Header(Pn_SosPid   IN NUMBER,PV_TIPO IN VARCHAR2 , PV_Error  OUT VARCHAR2) IS                                               
       
       /* 8250 JJO
       	CURSOR C_Trx_In IS
          SELECT  TTC_TRX_IN.NEXTVAL FROM dual; */           
                                
      Lv_Des_Error        VARCHAR2(800);
      Le_Error_Valid     EXCEPTION;
      Le_Error_Process EXCEPTION;
      Ln_ConTrx            NUMBER;
      Ld_SysdateRun    DATE;
      Ln_Seqno              NUMBER;
   BEGIN
           Ld_SysdateRun:=SYSDATE;
           
--[22/04/2013]Iro Juan Romero...Actualizacion de variables en caso de ser ejecutado para DTH
 
           IF pv_tipo = 'D' THEN
             lv_id_proceso_scp:=  'TTC_IN_DTH';
             GN_DESSHORT_IN:=     'TTC_IN_DTH';
             GN_PROCESO_IN:=      27;            
           END IF;
-------------------------------------------------------------------------------------------
           
           ----------------------------------------------------------------------------
           -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
           ----------------------------------------------------------------------------
           scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                                 lv_referencia_scp,
                                                 null,null,null,null,
                                                 ln_total_registros_scp,
                                                 lv_unidad_registro_scp,
                                                 ln_id_bitacora_scp,
                                                 ln_error_scp,lv_error_scp);
           if ln_error_scp <>0 then
              Lv_Des_Error:='Error en plataforma SCP, No se pudo iniciar la Bitacora';
              RAISE Le_Error_Valid;
           end if;
           ----------------------------------------------------------------------

         --Cargar los datos del Proceso TTC_IN_WORK -->TTC_Process_s                  
          i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GN_DESSHORT_IN);      
          IF i_process.Process IS NULL THEN
                Lv_Des_Error:='No existen datos configurados en la tabla TTC_Process_s';
                RAISE Le_Error_Valid;
          END IF;

          --Ingreso de Inicio del proceso 
            i_Log_InOut_t:= ttc_Log_InOut_t(Process => nvl(i_process.Process,0),
                                            RunDate => SYSDATE,
                                            LastDate => NULL,
                                            StatusProcess => 'OKI',
                                            Comment_Run => NULL);                  
            --Verifica si el proceso se encuentra en ejecución
            IF  RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GN_DESSHORT_IN,i_process.Duration) THEN
                  Lv_Des_Error:='El proceso ya se encuentra en ejecución, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
                  RAISE Le_Error_process;
            END IF;            

          --Graba el proceso para controlar el bloqueo de los procesos
             i_Bck_Process_t:= ttc_Bck_Process_t(Process => i_process.Process,
                                                  DesShort => i_process.DesShort,
                                                  GroupTread => 0,
                                                  Tread_NO => 0,
                                                  FilterField => i_process.FilterField,
                                                  LastRunDate => SYSDATE,
                                                  MaxDelay => i_process.MaxDelay,
                                                  Refresh_Cycle => i_process.Refresh_Cycle,
                                                  SosPid => Pn_SosPid);
                                                  
             Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
             IF Lv_Des_Error IS NOT NULL THEN
                  Lv_Des_Error:='Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesión levantada';
                  RAISE Le_Error_process;
             END IF;                                                                                                                                                            

            --Cargar los datos del Proceso TTC_IN --> TTC_rule_element
            i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GN_DESSHORT_IN);
           IF i_rule_element.Process IS NULL THEN
                  Lv_Des_Error:='No existen datos configurados en la tabla TTC_rule_element';
                  RAISE Le_Error_Valid;
            END IF;                        
                                                 
           ----------------------------------------------------------------------
           -- Ejecución del proceso :Prc_To_Save_IN
           ----------------------------------------------------------------------

--[22/04/2013]Iro Juan Romero: Llamado al proceso Prc_To_Save_IN_DTH para el caso que se trate de DTH           
           IF pv_tipo = 'D' THEN
              Prc_To_Save_IN_DTH (PV_ERROR);
              IF PV_ERROR IS NOT NULL THEN
                  Lv_Des_Error:=PV_ERROR;
                  RAISE Le_Error_Valid;
              END IF; 
---------------------------------------------------------------------------------------                                       
           ELSE
              Prc_To_Save_IN (PV_ERROR);
              IF PV_ERROR IS NOT NULL THEN
                  Lv_Des_Error:=PV_ERROR;
                  RAISE Le_Error_Valid;
              END IF;            
           END IF;
           --Graba en la tabla programmer la fecha de ejecucion del proceso
            OPEN C_Trx_In ;
           FETCH C_Trx_In INTO Ln_Seqno;
           CLOSE C_Trx_In;                                                                                                          
           i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                            Seqno => Ln_Seqno,
                                                            RunDate => Ld_SysdateRun,
                                                            LastModDate => Ld_SysdateRun);
           Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
           IF Lv_Des_Error IS NOT NULL THEN
                RAISE Le_Error_Valid;
           END IF;                                                                                                                                                            

          ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje 
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='FIN '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 'RC_Trx_IN_To_Work-Prc_Header Ejecutado con Exito'
                                                 ,Null,
                                                 0,
                                                 lv_unidad_registro_scp,
                                                 Null,Null,null,null,
                                                 'N',ln_error_scp,lv_error_scp);
           ----------------------------------------------------------------------
          --SCP:FIN                  
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);                  
          ----------------------------------------------------------------------------
          Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
          PV_ERROR:='RC_Trx_IN_To_Work-Prc_Header-SUCESSFUL';                                     
          --Registro del Fin del proceso 
          i_Log_InOut_t.LastDate:=SYSDATE;
          i_Log_InOut_t.StatusProcess:='OKI';
          i_Log_InOut_t.Comment_Run:=PV_ERROR;
          Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);

     EXCEPTION            
           WHEN Le_Error_process THEN
                PV_ERROR:=  'Proceso: RC_Trx_IN_To_Work- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:= PV_ERROR;                      

                                                        
                 ----------------------------------------------------------------------
                 -- SCP: Código generado automáticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: RC_Trx_IN_To_Work-Error en Validación';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 --ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx; 8250 JJO
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                 
           WHEN Le_Error_Valid THEN                      
                PV_ERROR:=  'Proceso: RC_Trx_IN_To_Work- Ejecutado con ERROR-->'||'Prc_Head-'||Lv_Des_Error;
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                 i_Log_InOut_t.LastDate:=SYSDATE;
                 i_Log_InOut_t.StatusProcess:='ERR';
                 i_Log_InOut_t.Comment_Run:= PV_ERROR;                      

                                                        
                 ----------------------------------------------------------------------
                 -- SCP: Código generado automáticamente. Registro de mensaje de error
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Proceso: RC_Trx_IN_To_Work-Error en Validación';
                  If ln_registros_error_scp = 0 Then
                     ln_registros_error_scp:=-1;
                  End If;
                 --ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx; 8250 JJO
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                       lv_mensaje_apl_scp,
                                                                                       PV_ERROR,
                                                                                       Null,
                                                                                       3,Null,
                                                                                       Null,
                                                                                       Null,null,null,
                                                                                       'S',ln_error_scp,lv_error_scp
                                                                                       );
                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                 ----------------------------------------------------------------------------
                 i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                 
           WHEN OTHERS THEN                                        
                 PV_ERROR:='     RC_Trx_IN_To_Work-Prc_Head-Error al ejecutar el proceso-'||substr(SQLERRM,1,500);
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);                 
                  i_Log_InOut_t.LastDate:=SYSDATE;
                  i_Log_InOut_t.StatusProcess:='ERR';
                  i_Log_InOut_t.Comment_Run:=  PV_ERROR;                      
                  ----------------------------------------------------------------------
                   -- SCP: Código generado automáticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Proceso: RC_TRX_IN_TO_Work-Error general';
		               If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                   End If;
                    
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                         lv_mensaje_apl_scp,
                                                                                         PV_ERROR,Null,
                                                                                         2,lv_unidad_registro_scp,
                                                                                         Null,Null, null,null,'N',ln_error_scp,lv_error_scp
                                                                                         );
                  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                  ----------------------------------------------------------------------------
                  i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);                  
   END Prc_Header;

   
   /************************************************************************************
     Regulariza todos los co_id o contratos inconsistente a partir de una fecha
     segun el valor configurado por "TTC_DATE_MAX_TO_REGULARIZE"
   *************************************************************************************/    
   PROCEDURE Prc_To_Save_IN(PV_Error  OUT VARCHAR2) IS                                         
      Lv_Des_Error        VARCHAR2(800);            
      Le_Error_Valid     EXCEPTION;         
      Ln_ConTrx            NUMBER;              
      Le_Error_Trx        EXCEPTION;      
      Ln_Customer_id   NUMBER;
      Ln_Co_id                NUMBER;                                                                                      

   BEGIN              
            ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje 
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='INICIO Prc_To_Save_IN: '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 Null,
                                                 Null,
                                                 0,
                                                 lv_unidad_registro_scp,
                                                 Null,Null,
                                                 null,null,'N',ln_error_scp,lv_error_scp);
                                   
           OPEN C_IN;
            LOOP   
            FETCH C_IN
            BULK COLLECT INTO    i_Customers_In_t 
            LIMIT i_rule_element.Value_Max_Trx_By_Execution;                                                         
                 Ln_ConTrx:=0;
                 FOR rc_in IN (select * from table(cast( i_Customers_In_t as ttc_View_Customers_In_t)))  LOOP
                     BEGIN
                           --Setea la bandera de validación de la configuración de los parámetros                                           
                            Ln_Customer_id:=rc_in.Customer_id;
                            Ln_Co_id:=rc_in.Co_id;                     
                            Ln_ConTrx:=Ln_ConTrx+1;
                             --Graba en la tabla ttc_contract_in 
                              i_Cust_to_Save_In_t:=ttc_View_Customers_In_type(customer_id => rc_in.customer_id, 
                                                                              co_id => rc_in.co_id,
                                                                              dn_num => rc_in.dn_num,
                                                                              billcycle => rc_in.billcycle,
                                                                              custcode => rc_in.custcode,
                                                                              tmcode => rc_in.tmcode,
                                                                              prgcode => rc_in.prgcode,
                                                                              cstradecode => rc_in.cstradecode,
                                                                              PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                                              CO_EXT_CSUIN => rc_in.CO_EXT_CSUIN,
                                                                              co_userlastmod => nvl(rc_in.co_userlastmod,'RELOJ-QC'),
                                                                              ttc_insertiondate => nvl(rc_in.ttc_insertiondate,SYSDATE),
                                                                              ttc_lastmoddate => NULL);
                               Lv_Des_Error:=Fct_Save_ToWk;                  
                               IF Lv_Des_Error IS NOT NULL THEN
                                        RAISE Le_Error_Trx;
                               END IF;
                               
                               ln_registros_procesados_scp:=ln_registros_procesados_scp+1; --8250 JJO;                                                                                                                           
                                                                                            
                             IF Ln_ConTrx>=i_rule_element.Value_Max_TrxCommit THEN
                                   --***********-- 
                                    COMMIT;
                                    --***********--
                                     -- SCP:AVANCE
                                     -----------------------------------------------------------------------
                                     -- SCP: Código generado automáticamente. Registro de avance de proceso
                                     -----------------------------------------------------------------------
                                     --ln_registros_procesados_scp:=ln_registros_procesados_scp+i_rule_element.Value_Max_TrxCommit; 8250 JJO
                                     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                                                             ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                                     -----------------------------------------------------------------------
                                     Ln_ConTrx:=0;
                             END IF;                       
                     EXCEPTION            
                           WHEN Le_Error_Trx THEN                               
                                PV_Error:='Prc_To_Save_IN-'||Lv_Des_Error||','||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id;
                                --8250 JJO - Inicio
                                ----------------------------------------------------------------------
                                -- SCP: Código generado automáticamente. Registro de mensaje de error
                                 ----------------------------------------------------------------------
                                 ln_registros_error_scp:=ln_registros_error_scp+1;
                                 lv_mensaje_apl_scp:='    Error Validacion: Prc_To_Save_IN-Procesando Cuenta';
                                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                       lv_mensaje_apl_scp,
                                                                       PV_Error,
                                                                       Null,
                                                                       2,
                                                                       Null,'Customer_id','Co_id',
                                                                       Ln_Customer_id,Ln_Co_id,'N',ln_error_scp,lv_error_scp);
                                                        
                                 ----------------------------------------------------------------------------
				                         --8250 JJO - Fin                                                                   
                           WHEN OTHERS THEN                                               
                                 PV_Error:='Prc_To_Save_IN-Error al momento de ingresar los registros a la tabla de trabajo ttc_contract_all_in,'||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id||','||substr(SQLERRM,1,300);                                                                   
                                  i_Log_InOut_t.LastDate:=SYSDATE;
                                  i_Log_InOut_t.StatusProcess:='ERR';
                                  i_Log_InOut_t.Comment_Run:=  PV_Error;                      
                                  Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                                  ----------------------------------------------------------------------
                                  -- SCP: Código generado automáticamente. Registro de mensaje de error
                                   ----------------------------------------------------------------------
				                           ln_registros_error_scp:=ln_registros_error_scp+1;--8250 JJO
                                   lv_mensaje_apl_scp:='    Error general: Prc_To_Save_IN-Procesando Cuenta';
                                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                                         lv_mensaje_apl_scp,
                                                                                                         PV_Error,
                                                                                                         Null,
                                                                                                         2,
                                                                                                         Null,Null,Null,
                                                                                                         null,null,'N',ln_error_scp,lv_error_scp
                                                                                                         );                                                       
                                   ----------------------------------------------------------------------------
                     END;      
                 END LOOP;     
                   --***********--     
                     COMMIT;
                   --***********--                     
                     ---------------------------------------------------------------------
                     -- SCP:AVANCE
                     IF Ln_ConTrx>0 THEN
                           --ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx; 8250 JJO
                           scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,
                                                         ln_registros_error_scp,ln_error_scp,lv_error_scp);
                     END IF;                              
                     ----------------------------------------------------------------------------
                    EXIT WHEN C_IN%NOTFOUND;                                    
               END LOOP;        
               CLOSE C_IN;
               
               UPDATE /*+ index(cr IX_CONTRACT_REG_LASTMODDATE) */  TTC_CONTRACT_TO_REGULARIZE_S cr
                        SET TTC_LASTMODDATE=SYSDATE
                WHERE TTC_LASTMODDATE IS NULL;
               --***********--     
                 COMMIT;
               --***********--                     

              ----------------------------------------------------------------------
               -- SCP: Código generado automáticamente. Registro de mensaje 
               ----------------------------------------------------------------------
               lv_mensaje_apl_scp:='FIN Prc_To_Save_IN '||lv_id_proceso_scp;
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                     lv_mensaje_apl_scp,
                                                     'Prc_To_Save_IN- Ejecutado con Exito'
                                                     ,Null,
                                                     0,
                                                     lv_unidad_registro_scp,
                                                     Null,Null,null,null,
                                                     'N',ln_error_scp,lv_error_scp);
               ----------------------------------------------------------------------
              --SCP:FIN                          
               PV_Error:=NULL;
    EXCEPTION            
           WHEN OTHERS THEN        
               --ROLLBACK;                                
              PV_Error:='Prc_To_Save_IN-Error en la ejecución del proceso'||substr(SQLERRM,1,500);                     
                                             
   END Prc_To_Save_IN;
   
  /************************************************************************************
  [8250] JJO - INICIO
  Procemiento encargado de insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha  
  de la ultima cuenta insertada en TTC_CONTRACT_ALL_IN
  [8250] FIN 
  *************************************************************************************/ 
  
 PROCEDURE Prc_Insert_Programmer(PV_TIPO IN VARCHAR2, PV_ERROR OUT VARCHAR2) IS   

--Iro Juan Romero Aguilar: Cursor para utilizar procedimiento con Dth y movil     
     TYPE C_CUENTAS_LAST IS REF CURSOR;
     LC_CUENTAS_LAST C_CUENTAS_LAST;
--------------------------------------------------------------------------     
           
     Le_Error           EXCEPTION;
     id_process					NUMBER;
     Ld_SysdateRun    	DATE;
     Ln_Seqno					  NUMBER;
     Lv_Des_Error				VARCHAR2(800); 
     Ln_count           Number;
     Lv_sql_cuentas     VARCHAR2(1000);
     Lv_sql_last_date   VARCHAR2(1000);
     /*CURSOR C_EXISTE_CUENTA IS 
       SELECT COUNT(CO_ID) FROM TTC_CONTRACT_ALL_IN_S WHERE ROWNUM <= 1;
       
     CURSOR C_LAST_DATE IS
       SELECT MAX(TTC_INSERTIONDATE) FROM TTC_CONTRACT_ALL_IN_S;*/
   
   BEGIN
     IF PV_TIPO = 'D'  THEN 
         
        GN_DESSHORT_IN:=     'TTC_IN_DTH';
        GN_PROCESO_IN:=      27; 
        Lv_sql_cuentas:='SELECT COUNT(CO_ID) FROM TTC_CONTRACT_ALL_DTH_IN_S WHERE ROWNUM <= 1';
        Lv_sql_last_date:='SELECT MAX(TTC_INSERTIONDATE) FROM TTC_CONTRACT_ALL_DTH_IN_S';
     
     ELSE
       
        Lv_sql_cuentas:='SELECT COUNT(CO_ID) FROM TTC_CONTRACT_ALL_IN_S WHERE ROWNUM <= 1';
        Lv_sql_last_date:='SELECT MAX(TTC_INSERTIONDATE) FROM TTC_CONTRACT_ALL_IN_S';
     
     END IF;
     
     OPEN LC_CUENTAS_LAST for Lv_sql_cuentas;
      FETCH LC_CUENTAS_LAST
        INTO Ln_count;
     CLOSE LC_CUENTAS_LAST;
     /*OPEN C_EXISTE_CUENTA ;
     FETCH C_EXISTE_CUENTA INTO Ln_count;
     CLOSE C_EXISTE_CUENTA;*/
     
     IF (Ln_count>0) then
       
       OPEN C_Trx_In ;
       FETCH C_Trx_In INTO Ln_Seqno;
       CLOSE C_Trx_In; 
       
       
       OPEN LC_CUENTAS_LAST for Lv_sql_last_date;
         FETCH LC_CUENTAS_LAST
           INTO Ld_SysdateRun;
       CLOSE LC_CUENTAS_LAST;
       /*OPEN C_LAST_DATE ;
       FETCH C_LAST_DATE INTO Ld_SysdateRun;
       CLOSE C_LAST_DATE;*/       
       
       id_process:=reloj.rc_api_timetocash_rule_bscs.rc_retorna_id_process(GN_DESSHORT_IN);
       
       i_Process_Programmer_t:=reloj.ttc_Process_Programmer_t(Process => nvl(id_process,GN_PROCESO_IN),
                                                           Seqno => Ln_Seqno,
                                                           RunDate => Ld_SysdateRun,
                                                           LastModDate => Ld_SysdateRun
                                                           );
                                                           
       Lv_Des_Error:=reloj.RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
       
       IF Lv_Des_Error IS NOT NULL THEN
         RAISE Le_Error;
       END IF;
       
     END IF;    
     
     PV_ERROR:=NULL;
      
   EXCEPTION
     WHEN Le_Error THEN
       PV_ERROR:=Lv_Des_Error;
     WHEN OTHERS THEN
       PV_ERROR:='Prc_Insert_Programmer-Error en la ejecución : '||substr(SQLERRM,1,500);       
   END Prc_Insert_Programmer;
   
   
    /************************************************************************************
     Regulariza todos los co_id o contratos inconsistente a partir de una fecha
     segun el valor configurado por "TTC_DATE_MAX_TO_REGULARIZE"
   *************************************************************************************/    
   PROCEDURE Prc_To_Save_DTH_IN(PV_Error  OUT VARCHAR2) IS                                         
      Lv_Des_Error        VARCHAR2(800);            
      Le_Error_Valid     EXCEPTION;         
      Ln_ConTrx            NUMBER;              
      Le_Error_Trx        EXCEPTION;      
      Ln_Customer_id   NUMBER;
      Ln_Co_id                NUMBER;                                                                                      

   BEGIN              
            ----------------------------------------------------------------------
           -- SCP: Código generado automáticamente. Registro de mensaje 
           ----------------------------------------------------------------------
           lv_mensaje_apl_scp:='INICIO Prc_To_Save_IN: '||lv_id_proceso_scp;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 Null,
                                                 Null,
                                                 0,
                                                 lv_unidad_registro_scp,
                                                 Null,Null,
                                                 null,null,'N',ln_error_scp,lv_error_scp);
                                   
           OPEN C_DTH_IN;
            LOOP   
            FETCH C_IN
            BULK COLLECT INTO    i_Customers_In_t 
            LIMIT i_rule_element.Value_Max_Trx_By_Execution;                                                         
                 Ln_ConTrx:=0;
                 FOR rc_in IN (select * from table(cast( i_Customers_In_t as ttc_View_Customers_In_t)))  LOOP
                     BEGIN
                           --Setea la bandera de validación de la configuración de los parámetros                                           
                            Ln_Customer_id:=rc_in.Customer_id;
                            Ln_Co_id:=rc_in.Co_id;                     
                            Ln_ConTrx:=Ln_ConTrx+1;
                             --Graba en la tabla ttc_contract_in 
                              i_Cust_to_Save_In_t:=ttc_View_Customers_In_type(customer_id => rc_in.customer_id, 
                                                                              co_id => rc_in.co_id,
                                                                              dn_num => rc_in.dn_num,
                                                                              billcycle => rc_in.billcycle,
                                                                              custcode => rc_in.custcode,
                                                                              tmcode => rc_in.tmcode,
                                                                              prgcode => rc_in.prgcode,
                                                                              cstradecode => rc_in.cstradecode,
                                                                              PRODUCT_HISTORY_DATE => rc_in.PRODUCT_HISTORY_DATE,
                                                                              CO_EXT_CSUIN => rc_in.CO_EXT_CSUIN,
                                                                              co_userlastmod => nvl(rc_in.co_userlastmod,'RELOJ-QC'),
                                                                              ttc_insertiondate => nvl(rc_in.ttc_insertiondate,SYSDATE),
                                                                              ttc_lastmoddate => NULL);
                               Lv_Des_Error:=Fct_Save_dth_ToWk;                  
                               IF Lv_Des_Error IS NOT NULL THEN
                                        RAISE Le_Error_Trx;
                               END IF;
                               
                               ln_registros_procesados_scp:=ln_registros_procesados_scp+1; --8250 JJO;                                                                                                                           
                                                                                            
                             IF Ln_ConTrx>=i_rule_element.Value_Max_TrxCommit THEN
                                   --***********-- 
                                    COMMIT;
                                    --***********--
                                     -- SCP:AVANCE
                                     -----------------------------------------------------------------------
                                     -- SCP: Código generado automáticamente. Registro de avance de proceso
                                     -----------------------------------------------------------------------
                                     --ln_registros_procesados_scp:=ln_registros_procesados_scp+i_rule_element.Value_Max_TrxCommit; 8250 JJO
                                     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                                                             ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                                     -----------------------------------------------------------------------
                                     Ln_ConTrx:=0;
                             END IF;                       
                     EXCEPTION            
                           WHEN Le_Error_Trx THEN                               
                                PV_Error:='Prc_To_Save_dth_IN-'||Lv_Des_Error||','||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id;
                                --8250 JJO - Inicio
                                ----------------------------------------------------------------------
                                -- SCP: Código generado automáticamente. Registro de mensaje de error
                                 ----------------------------------------------------------------------
                                 ln_registros_error_scp:=ln_registros_error_scp+1;
                                 lv_mensaje_apl_scp:='    Error Validacion: Prc_To_Save_dth_IN-Procesando Cuenta';
                                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                       lv_mensaje_apl_scp,
                                                                       PV_Error,
                                                                       Null,
                                                                       2,
                                                                       Null,'Customer_id','Co_id',
                                                                       Ln_Customer_id,Ln_Co_id,'N',ln_error_scp,lv_error_scp);
                                                        
                                 ----------------------------------------------------------------------------
				                         --8250 JJO - Fin                                                                   
                           WHEN OTHERS THEN                                               
                                 PV_Error:='Prc_To_Save_dth_IN-Error al momento de ingresar los registros a la tabla de trabajo ttc_contract_all_in,'||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id||','||substr(SQLERRM,1,300);                                                                   
                                  i_Log_InOut_t.LastDate:=SYSDATE;
                                  i_Log_InOut_t.StatusProcess:='ERR';
                                  i_Log_InOut_t.Comment_Run:=  PV_Error;                      
                                  Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                                  ----------------------------------------------------------------------
                                  -- SCP: Código generado automáticamente. Registro de mensaje de error
                                   ----------------------------------------------------------------------
				                           ln_registros_error_scp:=ln_registros_error_scp+1;--8250 JJO
                                   lv_mensaje_apl_scp:='    Error general: Prc_To_Save_dth_IN-Procesando Cuenta';
                                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                                         lv_mensaje_apl_scp,
                                                                                                         PV_Error,
                                                                                                         Null,
                                                                                                         2,
                                                                                                         Null,Null,Null,
                                                                                                         null,null,'N',ln_error_scp,lv_error_scp
                                                                                                         );                                                       
                                   ----------------------------------------------------------------------------
                     END;      
                 END LOOP;     
                   --***********--     
                     COMMIT;
                   --***********--                     
                     ---------------------------------------------------------------------
                     -- SCP:AVANCE
                     IF Ln_ConTrx>0 THEN
                           --ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx; 8250 JJO
                           scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,
                                                         ln_registros_error_scp,ln_error_scp,lv_error_scp);
                     END IF;                              
                     ----------------------------------------------------------------------------
                    EXIT WHEN C_IN%NOTFOUND;                                    
               END LOOP;        
               CLOSE C_IN;
               
               UPDATE /*+ index(cr IX_CONTRACT_REG_LASTMODDATE) */  TTC_CONTRACT_TO_REGULARIZE_S cr
                        SET TTC_LASTMODDATE=SYSDATE
                WHERE TTC_LASTMODDATE IS NULL;
               --***********--     
                 COMMIT;
               --***********--                     

              ----------------------------------------------------------------------
               -- SCP: Código generado automáticamente. Registro de mensaje 
               ----------------------------------------------------------------------
               lv_mensaje_apl_scp:='FIN Prc_To_Save_dth_IN '||lv_id_proceso_scp;
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                     lv_mensaje_apl_scp,
                                                     'Prc_To_Save_dth_IN- Ejecutado con Exito'
                                                     ,Null,
                                                     0,
                                                     lv_unidad_registro_scp,
                                                     Null,Null,null,null,
                                                     'N',ln_error_scp,lv_error_scp);
               ----------------------------------------------------------------------
              --SCP:FIN                          
               PV_Error:=NULL;
    EXCEPTION            
           WHEN OTHERS THEN        
               --ROLLBACK;                                
              PV_Error:='Prc_To_Save_dth_IN-Error en la ejecución del proceso'||substr(SQLERRM,1,500);                     
                                             
   END Prc_To_Save_DTH_IN;
 
END jr_RC_Trx_IN_To_Work;
/
