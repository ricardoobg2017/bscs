create or replace package reloj.RC_Api_TimeToCash_Rule_ALL_A IS
--Cursor
             ---Poner en el paquete de AXIS
              ------------------------------------------------------------------------
              -- XTR 25/05/2009
              -- ECARFO [4351] Caso 316
              -- Cambios para que la linea Principal en los control empresarial se Inactive al final              
              ------------------------------------------------------------------------
              CURSOR C_Telefonos_Procesar (cv_cuenta VARCHAR2  )IS
                SELECT /*+ rule +*/ r.*
                   FROM rc_detalle_transacciones@axis r,
                                cl_servicios_contratados@axis       sc,
                                cl_cupos_servicios_contratados@axis cs
                WHERE r.dt_telefono = sc.id_servicio(+)
                      AND sc.id_servicio = cs.id_servicio(+)
                      AND sc.estado(+) = 'A'
                      AND r.dt_cuenta = cv_cuenta
                ORDER BY cs.tipo_cupo DESC;           

--Procedures and Functions                   
             FUNCTION Fct_Fa_ciclos_bscs_t(Pv_DesCiclo VARCHAR2)  RETURN TTC_Fa_ciclos_bscs_t;
             FUNCTION Fct_ttc_processDesShort_t(Pv_DesShort VARCHAR2) RETURN ttc_process_t;
             FUNCTION Fct_ttc_process_t(Pv_Process NUMBER) RETURN ttc_process_t;
             FUNCTION Fct_ttc_Rule_ElementProcess_t(Pv_Process NUMBER) RETURN ttc_Rule_Element_t;
             FUNCTION Fct_ttc_rule_values_t(Pv_Process NUMBER, Pv_Value_Rule VARCHAR2) RETURN ttc_Rule_Values_t;
             FUNCTION Fct_ttc_Rule_ElementDesShort_t(Pv_DesShort VARCHAR2) RETURN ttc_Rule_Element_t;
             FUNCTION Fct_ttc_Rule_Ele_ProGroupTread(Pv_Process NUMBER, 
                                                                                                Pv_GroupTread NUMBER) RETURN ttc_Rule_Element_t;
             FUNCTION Fct_ttc_Rule_Ele_DesGroupTread(Pv_DesShort VARCHAR2, 
                                                                                                Pv_GroupTread NUMBER) RETURN ttc_Rule_Element_t;
             FUNCTION Fct_ttc_WorkFlow_User_t(Pv_Us_ID VARCHAR2) RETURN ttc_WorkFlow_User_t;                     
             FUNCTION Fct_ttc_WorkFlow_User_Name(Pv_Us_ID   IN VARCHAR2) RETURN VARCHAR2;
             FUNCTION Fct_Planning_Hour_DateValid(Pn_process NUMBER) RETURN Date;
             FUNCTION Fct_Programmer_DateValid(Pn_process NUMBER) RETURN Date;           
             FUNCTION  Fct_Process_DesShort_Run(Pv_DesShort VARCHAR2, Pn_Time_Max NUMBER) RETURN BOOLEAN;
             FUNCTION Fct_Save_To_Bck_Process(i_Bck_Process_t IN TTC_Bck_Process_t)  RETURN VARCHAR2;
             FUNCTION Fct_Del_To_Bck_Process(i_Bck_Process_t IN TTC_Bck_Process_t)  RETURN VARCHAR2;
             FUNCTION Fct_Save_To_Log_InOut(i_Log_InOut_t IN TTC_Log_InOut_t)  RETURN VARCHAR2;
             FUNCTION Fct_Save_To_Process_Programmer(i_Process_Programmer_t IN TTC_Process_Programmer_t)  RETURN VARCHAR2;
             FUNCTION Fct_ttc_Rule_Value_t(Pn_process NUMBER,Pv_Des VARCHAR2) RETURN TTC_Rule_Values_t;
             FUNCTION Fct_ttc_Contract_in(Pn_co_id NUMBER) RETURN NUMBER;
             FUNCTION Fct_ttc_ContractPlanDetails_in(Pn_customer_id       IN NUMBER,
                                                                                                     Pn_co_id                   IN NUMBER,
                                                                                                      Pn_dn_num              IN VARCHAR2,
                                                                                                      Pn_pk_id                   IN NUMBER, 
                                                                                                      Pn_st_id                    IN NUMBER,
                                                                                                      Pn_st_seqno             IN NUMBER,
                                                                                                      Pn_re_orden             IN NUMBER
                                                                                                       ) RETURN NUMBER;

             
End RC_Api_TimeToCash_Rule_ALL_A;
/
create or replace package body reloj.RC_Api_TimeToCash_Rule_ALL_A IS
  -- Private type declarations  
  -- Private constant declarations
  -- Private variable declarations

 --Procedures and Functions
   --**********************************************************************************
        FUNCTION Fct_Fa_ciclos_bscs_t(Pv_DesCiclo VARCHAR2)  RETURN TTC_Fa_ciclos_bscs_t as
             CURSOR cur_in IS
                   SELECT TTC_Fa_ciclos_bscs_t(id_ciclo,id_ciclo_admin,ciclo_default)
                     FROM Fa_Ciclos_Axis_Bscs h
                  WHERE id_ciclo_admin = Pv_DesCiclo;
              doc_in   TTC_Fa_ciclos_bscs_t; 
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
        FUNCTION Fct_ttc_process_t(Pv_Process NUMBER) RETURN ttc_process_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM TTC_Process_s h
                  WHERE process = Pv_Process;
              doc_in   ttc_process_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

    --***************************************************************************************
        FUNCTION Fct_ttc_processDesShort_t(Pv_DesShort VARCHAR2) RETURN ttc_process_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM TTC_Process_s h
                  WHERE desshort = Pv_DesShort;
              doc_in   ttc_process_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
        FUNCTION Fct_ttc_rule_values_t(Pv_Process NUMBER, Pv_Value_Rule VARCHAR2) RETURN ttc_Rule_Values_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM ttc_rule_values_s h
                  WHERE  h.process                 = Pv_Process
                         AND h.value_rule_string = Pv_Value_Rule;
              doc_in   ttc_Rule_Values_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
        FUNCTION Fct_ttc_Rule_ElementProcess_t(Pv_Process NUMBER) RETURN ttc_Rule_Element_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM ttc_rule_element_s h
                  WHERE  process =  Pv_Process;
              doc_in   ttc_Rule_Element_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
        FUNCTION Fct_ttc_Rule_ElementDesShort_t(Pv_DesShort VARCHAR2) RETURN ttc_Rule_Element_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM ttc_rule_element_s  h
                  WHERE desshort = Pv_DesShort;
              doc_in   ttc_Rule_Element_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
        FUNCTION Fct_ttc_Rule_Ele_ProGroupTread(Pv_Process NUMBER, 
                                                                                       Pv_GroupTread NUMBER) RETURN ttc_Rule_Element_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM ttc_rule_element_s h
                 WHERE  process =Pv_Process
                        AND grouptread =Pv_GroupTread;
              doc_in   ttc_Rule_Element_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

   --***************************************************************************************
           FUNCTION Fct_ttc_Rule_Ele_DesGroupTread(Pv_DesShort VARCHAR2, 
                                                                                          Pv_GroupTread NUMBER) RETURN ttc_Rule_Element_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                      FROM ttc_rule_element_s h
                   WHERE desshort = Pv_DesShort
                         AND grouptread=  Pv_GroupTread;
              doc_in   ttc_Rule_Element_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;
 
   --***************************************************************************************
           FUNCTION Fct_ttc_WorkFlow_User_t(Pv_Us_ID VARCHAR2) RETURN ttc_WorkFlow_User_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM  TTC_WORKFLOW_USER_S h
                  WHERE  Us_ID=Pv_Us_ID;
              doc_in   ttc_WorkFlow_User_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;
        
   --***************************************************************************************
         FUNCTION Fct_ttc_WorkFlow_User_Name(Pv_Us_ID   IN VARCHAR2) RETURN VARCHAR2 IS
          LV_USERS TTC_WorkFlow_User_s.Us_Name%TYPE;
         BEGIN         
             SELECT US_NAME 
                  INTO LV_USERS 
                FROM TTC_WORKFLOW_USER_S 
             WHERE Us_ID=Pv_Us_ID;            
             RETURN(LV_USERS);                              
         END Fct_ttc_WorkFlow_User_Name;
         
    --***************************************************************************************
           FUNCTION  Fct_Planning_Hour_DateValid(Pn_process NUMBER) RETURN DATE IS
             CURSOR cur_in IS
                SELECT  
                          MIN(
                          To_date(to_char(SYSDATE,'dd/mm/yyyy')||
                           decode(to_char(SYSDATE,'d'),
                                        '1',Lu_hour,
                                        '2',Ma_hour,
                                        '3',Mi_hour,
                                        '4',Ju_hour,
                                        '5',Vi_hour,
                                        '6',Sa_hour,
                                        '7',Do_hour 
                                        ) 
                                        ,'dd/mm/yyyy hh24:mi:ss') 
                                        ) Fecha_Disponible
                FROM  TTC_Planning_Hour 
             WHERE   process  =Pn_process                                                    
                  AND To_date(to_char(SYSDATE,'dd/mm/yyyy')||
                           decode(to_char(SYSDATE,'d'), '1',Lu_hour,'2',Ma_hour,'3',Mi_hour,'4',Ju_hour,'5',Vi_hour,'6',Sa_hour,'7',Do_hour) ,'dd/mm/yyyy hh24:mi:ss') 
                           >=
                          (SELECT  to_date(nvl(max( to_char(lastmoddate)), to_char(SYSDATE,'dd/mm/yyyy')||' 00:00:01'),'dd/mm/yyyy hh24:mi:ss') fecha
                                FROM Ttc_Process_Programmer_s a 
                             WHERE process=Pn_process                  
                          )
                          AND SYSDATE
                          <=
                          To_date(to_char(SYSDATE,'dd/mm/yyyy')||
                           decode(to_char(SYSDATE,'d'), '1',Lu_hour,'2',Ma_hour,'3',Mi_hour,'4',Ju_hour,'5',Vi_hour,'6',Sa_hour,'7',Do_hour) ,'dd/mm/yyyy hh24:mi:ss') ;
             doc_in   DATE;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

    --***************************************************************************************
           FUNCTION  Fct_Programmer_DateValid(Pn_process NUMBER) RETURN DATE IS
             CURSOR cur_in IS
                  SELECT nvl(max(lastmoddate),(to_date(to_char(SYSDATE ,'dd/mm/yyyy')||'00:00:01','dd/mm/yyyy hh24:mi:ss'))) fecha
                    FROM Ttc_Process_Programmer_s a 
                 WHERE process=Pn_process ;
             doc_in   DATE;          
            BEGIN
                  OPEN cur_in;
                  FETCH cur_in INTO doc_in;
                  CLOSE cur_in;
                  RETURN doc_in;
            END;

    --***************************************************************************************
           FUNCTION  Fct_Process_DesShort_Run(Pv_DesShort VARCHAR2, Pn_Time_Max NUMBER) RETURN BOOLEAN IS
             CURSOR cur_in IS
                   SELECT  process,round((SYSDATE-lastrundate)*24,1) tiempo
                      FROM  TTC_BCK_PROCESS_S
                   WHERE desshort =Pv_DesShort; 
             doc_in          NUMBER;          
             Ln_tiempo   NUMBER;
             Lb_Found   BOOLEAN;
            BEGIN
                  Lb_Found:=TRUE;
                  OPEN cur_in;
                  FETCH cur_in INTO doc_in,Ln_tiempo;
                  Lb_Found:=cur_in%Found;
                  CLOSE cur_in;
                  IF Lb_Found THEN
                       IF Ln_tiempo>Pn_Time_Max THEN
                            DELETE TTC_BCK_PROCESS_S WHERE desshort =Pv_DesShort;
                            COMMIT;
                            Lb_Found:=FALSE;
                       END IF;
                  END IF;
                 RETURN Lb_Found;
            EXCEPTION    
                    WHEN OTHERS THEN
                           ROLLBACK;                 
                           RETURN Lb_Found;
            END;

    --**********************************************************************************
         FUNCTION Fct_Save_To_Bck_Process(i_Bck_Process_t IN TTC_Bck_Process_t)  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Cabecera
                      INSERT INTO TTC_BCK_PROCESS_S
                      VALUES (i_Bck_Process_t) ;                          
                      COMMIT;
                  RETURN(Lv_Error); 
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   ROLLBACK;
                   Lv_Error:='Fct_Save_To_Bck_Process-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla TTC_BCK_PROCESS_S, '||substr(SQLERRM,1,300);                  
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN
                   ROLLBACK;
                  Lv_Error:='Fct_Save_To_Bck_Process-Error General al intentar grabar en la tabla TTC_BCK_PROCESS_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;           

--**********************************************************************************
         FUNCTION Fct_Del_To_Bck_Process(i_Bck_Process_t IN TTC_Bck_Process_t)  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Cabecera
                  DELETE TTC_BCK_PROCESS_S 
                   WHERE process= i_Bck_Process_t.Process 
                         AND DesShort=i_Bck_Process_t.DesShort;
                  COMMIT;
                  
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   ROLLBACK;
                   Lv_Error:='Fct_Del_To_Bck_Process-DUP_VAL_ON_INDEX, Error al intentar Eliminar en la tabla TTC_BCK_PROCESS_S, '||substr(SQLERRM,1,300);      
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                   ROLLBACK;
                  Lv_Error:='Fct_Del_To_Bck_Process-Error General al intentar Eliminar en la tabla TTC_BCK_PROCESS_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;           

    --**********************************************************************************
         FUNCTION Fct_Save_To_Log_InOut(i_Log_InOut_t IN TTC_Log_InOut_t)  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Cabecera
                  INSERT INTO TTC_LOG_INOUT_S
                  VALUES (i_Log_InOut_t) ;                          
                  COMMIT;
                  
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   ROLLBACK;
                   Lv_Error:='Fct_Save_To_Log_InOut-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla TTC_LOG_INOUT_S, '||substr(SQLERRM,1,300);      
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                   ROLLBACK;
                  Lv_Error:='Fct_Save_To_Log_InOut-Error General al intentar grabar en la tabla TTC_LOG_INOUT_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;           

    --**********************************************************************************
         FUNCTION Fct_Save_To_Process_Programmer(i_Process_Programmer_t IN TTC_Process_Programmer_t)  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Cabecera
                  INSERT INTO TTC_Process_Programmer_S
                  VALUES (i_Process_Programmer_t) ;                          
                  COMMIT;
                  
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   ROLLBACK;
                   Lv_Error:='Fct_Save_To_Process_Programmer-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla TTC_Process_Programmer_S, '||substr(SQLERRM,1,300);      
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                   ROLLBACK;
                  Lv_Error:='Fct_Save_To_Process_Programmer-Error General al intentar grabar en la tabla TTC_Process_Programmer_S, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;           

    --**********************************************************************************
        FUNCTION Fct_ttc_Rule_Value_t(Pn_process NUMBER,Pv_Des VARCHAR2) RETURN TTC_Rule_Values_t as
             CURSOR cur_in IS
                   SELECT VALUE(h)
                     FROM TTC_Rule_Values_s  h
                  WHERE process  = Pn_process
                        AND value_rule_string= Pv_Des;
              doc_in   TTC_Rule_Values_t;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
        END;

    --**********************************************************************************
        FUNCTION Fct_ttc_Contract_in(Pn_co_id NUMBER) RETURN NUMBER as
             CURSOR cur_in IS
                   SELECT 1
                     FROM Ttc_Contractplannig_s  h
                  WHERE h.co_id  = Pn_co_id;
              doc_in   NUMBER;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
              
        EXCEPTION              
                WHEN OTHERS THEN              
                        RETURN -1;
        END;            

    --**********************************************************************************
        FUNCTION Fct_ttc_ContractPlanDetails_in(Pn_customer_id       IN NUMBER,
                                                                                        Pn_co_id                   IN NUMBER,
                                                                                        Pn_dn_num              IN VARCHAR2,
                                                                                        Pn_pk_id                   IN NUMBER, 
                                                                                        Pn_st_id                    IN NUMBER,
                                                                                        Pn_st_seqno             IN NUMBER,
                                                                                        Pn_re_orden             IN NUMBER
                                                                                         ) RETURN NUMBER as
             CURSOR cur_in IS
                   SELECT 1
                     FROM Ttc_Contractplannigdetails_s  h
                  WHERE h.customer_id = Pn_customer_id
                         AND h.co_id              = Pn_co_id
                         AND h.dn_num         =  Pn_dn_num
                         AND h.pk_id              =  Pn_pk_id  
                         AND h.st_id                =  Pn_st_id
                         AND h.st_seqno         = Pn_st_seqno
                         AND h.re_orden         = Pn_re_orden;

              doc_in   NUMBER;          
        BEGIN
              OPEN cur_in;
              FETCH cur_in INTO doc_in;
              CLOSE cur_in;
              RETURN doc_in;
              
        EXCEPTION              
                WHEN OTHERS THEN              
                        RETURN -1;
        END;            

end RC_Api_TimeToCash_Rule_ALL_A;
/
