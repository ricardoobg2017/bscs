create or replace package reloj.RC_Trx_Qc_Suspension_All IS
/*Creador CLS WMENDOZA 27/10/2010
  Reactivacion De Reloj De Cobranza*/

--VARIABLES GLOBALES
GV_DESSHORT VARCHAR2(14) := 'TTC_QC_SUSP_1';
GV_DESSHORT_P4 VARCHAR2(12) := 'TTC_DISPTC';
GV_DESSHORT_P5 VARCHAR2(12) := 'TTC_SENDRETR';
Gv_unidad_registro_scp varchar2(30):='Resumenes';
gv_value_rule_string_cash varchar2(40):='TTC_FLAG_CUSTOMER_CASH_DIA';
gv_value_rule_string_out varchar2(30):='TTC_FLAG_CUSTOMER_OUT_RELOJ';
GV_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_CURSOR_CUSTCODE';
GV_STATUS_P   VARCHAR2(1) := 'P';

GV_TTC_RANGE_HOUR_FULL     VARCHAR2(30) := 'TTC_RANGE_HOUR_FULL';
GV_TTC_RANGE_HOUR_SEMIFULL VARCHAR2(30) := 'TTC_RANGE_HOUR_SEMIFULL';
GV_TTC_RANGE_HOUR_LOW      VARCHAR2(30) := 'TTC_RANGE_HOUR_LOW';
GV_TTC_MAX_RESTRICT_TIME   VARCHAR2(30) := 'TTC_MAX_RESTRICT_TIME';

gv_hora_ini VARCHAR2(30);
gv_hora_fin VARCHAR2(30);

t_RangeTime_Restrict               ttc_Rule_Values_t;
t_RangeTime_Full_p4                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p4            ttc_Rule_Values_t;
t_RangeTime_Low_p4                 ttc_Rule_Values_t;
t_RangeTime_Select                 ttc_Rule_Values_t;

t_RangeTime_Full_p5                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p5            ttc_Rule_Values_t;
t_RangeTime_Low_p5                 ttc_Rule_Values_t;

i_Process_Programmer_t       ttc_Process_Programmer_t;
GV_DESSHORT_LOW             VARCHAR2(15) := 'TTC_REAC_LOW';
GV_DESSHORT_RETRY_SEMI_FULL VARCHAR2(16) := 'TTC_REAC_SFUL';

gn_secuencia                NUMBER;

Function RC_CANT_TOTAL_REGISTRO(pv_table_name in varchar2) Return Number;

FUNCTION Fct_TTC_Suspension_Resumen_t(Pn_id_qc NUMBER,Pv_Desshort VARCHAR2, Pv_estado VARCHAR2) RETURN TTC_Suspension_Resumen_t;


procedure Prc_Head (pv_error        OUT VARCHAR2,
                                          pn_cant_proc out NUMBER
                                          );

procedure Prc_dispatch(Pv_Desshort        IN  varchar2,
                       pn_id_bitacora_scp   IN  NUMBER,
                       PD_Fecha             in  date,
                       pn_Max_Trx_encolada     in number,                                                                        
                       PV_OUT_ERROR         OUT VARCHAR2);    
                       
procedure Prc_retry(Pv_Desshort        IN  varchar2,
                       pn_id_bitacora_scp   IN  NUMBER,
                       PD_Fecha             in  date,
                       pn_Max_Trx_encolada     in number,                                                                        
                       PV_OUT_ERROR         OUT VARCHAR2);                                                     


END RC_Trx_Qc_Suspension_All;
/
create or replace package body reloj.RC_Trx_Qc_Suspension_All is


Function RC_CANT_TOTAL_REGISTRO(pv_table_name in varchar2) Return Number Is
    lv_sql       Varchar2(4000);
    ln_cantidad  Number:=0;
    
    Begin
      lv_sql:='SELECT COUNT(*) FROM '||pv_table_name;
      Execute Immediate lv_sql Into ln_cantidad;
     
      Return ln_cantidad;
      
      Exception
        When NO_DATA_FOUND Then
           Return 0;
        When Others Then
          Return 0;
End RC_CANT_TOTAL_REGISTRO;

FUNCTION Fct_ttc_bank_all_t RETURN  ttc_bank_all_t as
    doc_in  ttc_bank_all_t;
  BEGIN
   SELECT ttc_bank_all_type(bank_Id,bankname) 
                   BULK COLLECT  INTO  doc_in FROM  reloj.ttc_view_bank_all_bscs;
    RETURN doc_in;
END;

--**********************************************************************************
FUNCTION Fct_TTC_Suspension_Resumen_t(Pn_id_qc NUMBER,Pv_desshort VARCHAR2, Pv_estado VARCHAR2) RETURN TTC_Suspension_Resumen_t as
     CURSOR cur_in IS
           SELECT VALUE(h)
             FROM TTC_Suspension_Resumen_s  h
          WHERE id_qc  = Pn_id_qc
                AND desshort = Pv_desshort
                AND estado = Pv_estado;
      doc_in   TTC_Suspension_Resumen_t;          
BEGIN
      OPEN cur_in;
      FETCH cur_in INTO doc_in;
      CLOSE cur_in;
      RETURN doc_in;
END;

    --**********************************************************************************

 
FUNCTION Fct_inserta_resumen (pn_id_qc in number,
                              PV_DESSHORT in varchar2,                            
                              pv_name_qc in varchar2, 
                              pv_descripcion_qc in varchar2,
                              pv_estado in varchar2,
                              pv_descripcion_estado in varchar2,
                              pv_num_trans number,
                              pv_recomendacion in varchar2)return number is
      
  BEGIN
  
      insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                           ,DESSHORT
                                           ,NAME_QC
                                           ,DESCRIPCION_QC
                                           ,ESTADO
                                           ,DESCRIPCION_ESTADO
                                           ,NUM_TRANS
                                           ,FECHA_TRANS
                                           ,RECOMENDACION)     
                                           VALUES
                                           (pn_id_qc
                                           ,PV_DESSHORT
                                           ,pv_name_qc
                                           ,pv_descripcion_qc
                                           ,pv_estado
                                           ,pv_descripcion_estado
                                           ,pv_num_trans
                                           ,sysdate
                                           ,pv_recomendacion);
       Return 1;
    Exception
        When NO_DATA_FOUND Then
          Return 0;
        When Others Then
          Return 0;
END;

procedure Prc_Head (pv_error         OUT VARCHAR2,
                    pn_cant_proc out number) is

CURSOR C_Trx_In IS
              SELECT  TTC_TRX_IN.NEXTVAL FROM dual;
--VARIABLES
i_process                          ttc_process_t;
i_process_p4                       ttc_process_t;
i_process_p5                       ttc_process_t;
i_rule_element                     ttc_Rule_Element_t;
i_rule_element_p4                  ttc_Rule_Element_t;
i_rule_element_p5                  ttc_Rule_Element_t;
i_rule_values                      TTC_Rule_Values_t;
i_rule_values_p4                   TTC_Rule_Values_t;
i_rule_values_p5                   TTC_Rule_Values_t;

Ln_Seqno                           NUMBER;
Ld_Fecha_Actual                    DATE;
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR(3);
LV_HourNow                         VARCHAR2(20);
LV_fecha_ejecucion                 date;
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp                 number:=0; 
ln_total_registros_scp             number;
lv_id_proceso_scp                  varchar2(100):=GV_DESSHORT;
lv_referencia_scp                  varchar2(200):='RC_Trx_Qc_Suspension_All.RC_HEAD';
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
---------------------------------------------------------------
   
begin

  --EXTRAIGO LA SECUENCIA DE LA EJECUCION
  SELECT TTC_S_SUSPENSION_RESUMEN.Nextval INTO gn_secuencia FROM dual;
  
  pn_cant_proc:=0;
  Ld_Fecha_Actual:= SYSDATE;
  SELECT to_date(SYSDATE, 'dd/mm/rrrr') INTO LV_fecha_ejecucion FROM dual;
  SELECT to_char(SYSDATE, 'hh24:mi:ss') INTO LV_HourNow FROM dual;
  
  -- SCP:INICIO 
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_referencia_scp:='Inicio del proceso de Qc Supension: '||lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,null,null,null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  if ln_error_scp <>0 Then
     LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
     Raise LE_ERROR;
  end if;
  
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
  lv_mensaje_tec_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  
  --Datos Configuracion Proceso TTC_QC_SUSP_1 - QC Suspension
  i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
  IF i_process.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   /*i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT);
   IF i_rule_element.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;*/
   --Fin Datos Configuracion Suspension  
   
   -- 
  
  --Datos Configuracion Proceso 4: TTC_DISPTC 
  i_process_p4:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P4);
  IF i_process_p4.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   i_rule_element_p4:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P4);
   IF i_rule_element_p4.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;
   --Fin Datos Configuracion P4  
     
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 4--------------------------
  ---------------------------------------------------------------------------- 
  /*--Obtiene los rangos de horas permitidas, para la condiguración de los parámetros  
    t_RangeTime_Restrict:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_MAX_RESTRICT_TIME);
    
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Restrict.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Restrict.Value_Return_LongString, 'hh24:mi:ss') THEN
      LV_MENSAJE := 'ADVERTENCIA >> Horario no permitido de ejecución del reloj, consulte con VSO o el administrador y cambie los parametros... ';
      RAISE LE_ERROR;
    END IF;
    */
    
    --Rangos del proceso 4:TTC_DISPTC
    t_RangeTime_Full_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_FULL);
    t_RangeTime_SemiFull_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_SEMIFULL);
    t_RangeTime_Low_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_LOW);
    
    --Obtiene los rangos de horas permitidas, para la condiguración de los parámetros      
    --Verifica la hora actual y asigna los parámetros de acuerdo al rango permitido
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Full_p4.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Full_p4.Value_Return_LongString, 'hh24:mi:ss') THEN
       i_rule_element_p4:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P4);      
       
       gv_hora_ini:=t_RangeTime_Full_p4.Value_Return_ShortString;
       gv_hora_fin :=t_RangeTime_Full_p4.Value_Return_LongString;
    ELSIF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_SemiFull_p4.Value_Return_ShortString, 'hh24:mi:ss') AND
          To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_SemiFull_p4.Value_Return_LongString, 'hh24:mi:ss') THEN
          i_rule_element_p4:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_RETRY_SEMI_FULL);  
          
          gv_hora_ini:=t_RangeTime_SemiFull_p4.Value_Return_ShortString;
          gv_hora_fin :=t_RangeTime_SemiFull_p4.Value_Return_LongString;   
    ELSE
      i_rule_element_p4:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_LOW);      
      
      gv_hora_ini:=t_RangeTime_Low_p4.Value_Return_ShortString;
      gv_hora_fin:=t_RangeTime_Low_p4.Value_Return_LongString;
    END IF;
    
    --LLamada al proceso del Job_Dispatch
    
    RC_Trx_Qc_Suspension_All.Prc_dispatch(PV_DESSHORT => i_process_p4.DesShort,
                                           pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                           PD_Fecha => Ld_Fecha_Actual,
                                           pn_Max_Trx_encolada => i_rule_element_p4.Value_Max_Trx_encolada,
                                           PV_OUT_ERROR => LV_MENSAJE);
             
  
  IF  LV_MENSAJE IS NOT NULL THEN
      Raise LE_ERROR;
  END IF;  
  
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 4--------------------------------- 
  --------------------------------------------------------------------------------
  
  --Datos Configuracion Proceso 5: TTC_SENDRETR 
  i_process_p5:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P5);
  IF i_process_p5.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   i_rule_element_p5:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P5);
   IF i_rule_element_p5.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;
   
   ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 5--------------------------
  ---------------------------------------------------------------------------- 
    --Rangos del proceso 5
    t_RangeTime_Full_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_FULL);
    t_RangeTime_SemiFull_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_SEMIFULL);
    t_RangeTime_Low_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_LOW);
    
    --Obtiene los rangos de horas permitidas, para la condiguración de los parámetros      
    --Verifica la hora actual y asigna los parámetros de acuerdo al rango permitido
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Full_p5.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Full_p5.Value_Return_LongString, 'hh24:mi:ss') THEN
       i_rule_element_p5:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_p5);      
       
       gv_hora_ini:=t_RangeTime_Full_p5.Value_Return_ShortString;
       gv_hora_fin :=t_RangeTime_Full_p5.Value_Return_LongString;
    ELSIF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_SemiFull_p5.Value_Return_ShortString, 'hh24:mi:ss') AND
          To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_SemiFull_p5.Value_Return_LongString, 'hh24:mi:ss') THEN
          i_rule_element_p5:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_RETRY_SEMI_FULL);  
          
          gv_hora_ini:=t_RangeTime_SemiFull_p5.Value_Return_ShortString;
          gv_hora_fin :=t_RangeTime_SemiFull_p5.Value_Return_LongString;   
    ELSE
      i_rule_element_p5:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_LOW);      
      
      gv_hora_ini:=t_RangeTime_Low_p5.Value_Return_ShortString;
      gv_hora_fin:=t_RangeTime_Low_p5.Value_Return_LongString;
    END IF;
    
    RC_Trx_Qc_Suspension_All.Prc_retry(PV_DESSHORT => i_process_p5.DesShort,
                                                       pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                                       PD_Fecha => Ld_Fecha_Actual,
                                                       pn_Max_Trx_encolada => i_rule_element_p5.Value_Max_Trx_encolada,
                                                       PV_OUT_ERROR => LV_MENSAJE);
             
  
  IF  LV_MENSAJE IS NOT NULL THEN
      Raise LE_ERROR;
  END IF;      
  
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 5--------------------------------- 
  --------------------------------------------------------------------------------
  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_In ;
  FETCH C_Trx_In INTO Ln_Seqno;
  CLOSE C_Trx_In;                                                                                                          
  i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                   Seqno => Ln_Seqno,
                                                   RunDate => LV_fecha_ejecucion,
                                                   LastModDate => LV_fecha_ejecucion
                                                   );
  LV_MENSAJE:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);  
  LV_STATUS:='OKI';
  LV_MENSAJE:='RC_HEAD-SUCESSFUL';                 
  pv_error:=LV_MENSAJE;
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);
   
  EXCEPTION
  WHEN LE_ERROR THEN
       LV_STATUS    := 'ERR';
      LV_MENSAJE   :='RC_Trx_Qc_Suspension_All.Prc_Head:->'|| LV_MENSAJE;
      pv_error:=LV_MENSAJE;      
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------      
      lv_mensaje_apl_scp := '   Error en el Procesamiento';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            pv_error,
                                            Null,
                                            3,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);      
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp, ln_error_scp, lv_error_scp);
      ----------------------------------------------------------------------------
      /*RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                   LD_FECHA_ACTUAL,
                                                   SYSDATE,
                                                   LV_STATUS,
                                                   pv_error);
      RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,GV_DESSHORT,0,LV_MENSAJE);*/
  WHEN OTHERS THEN
             LV_STATUS:='ERR';
             LV_MENSAJE:=LV_MENSAJE|| ' RC_Trx_Qc_Suspension_All.Prc_Head:'  || 'ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 200);
             pv_error:=LV_MENSAJE;             
             /*RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                          LD_FECHA_ACTUAL,
                                                          SYSDATE,
                                                          LV_STATUS,
                                                          pv_error);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,GV_DESSHORT,0,LV_MENSAJE);*/
end;

procedure Prc_dispatch(PV_DESSHORT        IN  Varchar2,
                       pn_id_bitacora_scp   IN  NUMBER,
                       PD_Fecha             in  date,
                       pn_Max_Trx_encolada     in number,                                                                        
                       PV_OUT_ERROR         OUT VARCHAR2) is
------------------------------------

--VARIABLES
i_suspension_resumen               ttc_suspension_resumen_t;         
i_react                            TTC_Customers_Count_React_tbk;
i_process                          ttc_process_t;
i_Rule_Values_t                    ttc_Rule_Values_t;
i_ttc_bank_all_t                   ttc_bank_all_t;
lv_cuscode                         varchar2(24);
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR2(3);
LB_EXISTE_BCK                      boolean;
LN_COUNT_COMMIT                    NUMBER:=0;
LN_COUNT_MAXTRX                    NUMBER:=0;
LN_Customer_id                     NUMBER;
Ln_Secuence                        NUMBER;
Le_Error_Trx                       EXCEPTION;
LB_Check_Hour                      BOOLEAN;
Ln_bank_id                         INTEGER;
ln_check                           number:=0;
lv_status_trx                      varchar2(2);
--variables para reactivacion
LV_REMARK                          VARCHAR2(400);
LV_Sql_Planing                     VARCHAR2(400);
LV_Sql_PlanDet                     VARCHAR2(400);
LV_Tipo                            VARCHAR2(1);
LV_STATUS_OUT                      VARCHAR(1);
LN_SALDO_MIN                       NUMBER;
LN_SALDO_MIN_POR                   NUMBER;
LV_USER_AXIS                       TTC_WORKFLOW_USER_S.US_NAME%TYPE;
LV_Plan_Telefonia                  VARCHAR2(10);
lb_Reactiva                        BOOLEAN;
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

--Obtencion de la información de las colas de Axis lo que se está procesando el Job Dispatch
   cursor c_axis_cola is
   select descripcion_estado,valor,estado from TTC_VIEW_QC_COLAS_AXIS_NEW;
   
   lc_axis_cola                            c_axis_cola%rowtype;
   
   lv_recomendacion  varchar2(500); 
   lb_hay_datos      boolean:=false;
   LV_ASUNTO_MAIL    VARCHAR2(100):='QC - SUSPENSION'; 
   lv_mensaje_mail   varchar2(2000); 

begin  

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_dispatch:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_dispatch';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);

   
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ---------------------------------------------------------------------------- 
       --Obtencion de la información de las colas de Axis lo que se está procesando el Job Dispatch
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola;  
            loop  
                FETCH c_axis_cola into lc_axis_cola; 
                
                exit when c_axis_cola%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecución Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la información de Transacciones de BSCS finalizadas en la ejecución de hoy';
                
                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION'
                                             ,lv_mensaje
                                             ,lc_axis_cola.ESTADO
                                             ,lc_axis_cola.DESCRIPCION_ESTADO
                                             ,lc_axis_cola.VALOR                                             
                                             ,lv_recomendacion);    
            
               if ln_check=0 then
                  --hubo error;
                  null;
               end if;           
                
               lb_hay_datos:=true;
            end loop;
            close c_axis_cola;   
       end;
       
       if not lb_hay_datos then  
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';          
          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL                                                       
                                        ,NULL); 
          if ln_check=0 then
             --hubo error;
             null;
          end if;                                             
                  
       end if;
       
         
         
       i_suspension_resumen:=  Rc_Trx_Qc_Suspension_All.Fct_TTC_Suspension_Resumen_t(gn_secuencia,GV_DESSHORT_P4,'P');
       
       IF i_suspension_resumen.NUM_TRANS IS NOT NULL AND i_suspension_resumen.NUM_TRANS >  pn_Max_Trx_encolada THEN
          lv_mensaje_mail:= 'Alerta, existen '||i_suspension_resumen.NUM_TRANS||' transacciones encoladas en proceso '||i_suspension_resumen.DESSHORT;
          BEGIN
           sysadm.send_mail2.mail(from_name => 'sistema_archivo@conecel.com',
                                   to_name   => 'wcruz@cimaconsulting.com.ec',
                                   cc        => NULL,
                                   cco       => NULL,
                                   subject   => LV_ASUNTO_MAIL,
                                   message   => lv_mensaje_mail || ' - ' || i_suspension_resumen.RECOMENDACION);
           EXCEPTION
               WHEN OTHERS THEN
                 NULL;                               
           END;
       END IF;
       
     
 PV_OUT_ERROR:=NULL;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de QC--------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_Qc_Suspension_All.Prc_dispatch-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_dispatch '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);   
  
  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_Qc_Suspension_All.Prc_dispatch- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;


procedure Prc_retry(PV_DESSHORT        IN  Varchar2,
                       pn_id_bitacora_scp   IN  NUMBER,
                       PD_Fecha             in  date,
                       pn_Max_Trx_encolada     in number,                                                                        
                       PV_OUT_ERROR         OUT VARCHAR2) is
------------------------------------

--VARIABLES
i_suspension_resumen               ttc_suspension_resumen_t;         
i_react                            TTC_Customers_Count_React_tbk;
i_process                          ttc_process_t;
i_Rule_Values_t                    ttc_Rule_Values_t;
i_ttc_bank_all_t                   ttc_bank_all_t;
lv_cuscode                         varchar2(24);
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR2(3);
LB_EXISTE_BCK                      boolean;
LN_COUNT_COMMIT                    NUMBER:=0;
LN_COUNT_MAXTRX                    NUMBER:=0;
LN_Customer_id                     NUMBER;
Ln_Secuence                        NUMBER;
Le_Error_Trx                       EXCEPTION;
LB_Check_Hour                      BOOLEAN;
Ln_bank_id                         INTEGER;
ln_check                           number:=0;
lv_status_trx                      varchar2(2);
--variables para reactivacion
LV_REMARK                          VARCHAR2(400);
LV_Sql_Planing                     VARCHAR2(400);
LV_Sql_PlanDet                     VARCHAR2(400);
LV_Tipo                            VARCHAR2(1);
LV_STATUS_OUT                      VARCHAR(1);
LN_SALDO_MIN                       NUMBER;
LN_SALDO_MIN_POR                   NUMBER;
LV_USER_AXIS                       TTC_WORKFLOW_USER_S.US_NAME%TYPE;
LV_Plan_Telefonia                  VARCHAR2(10);
lb_Reactiva                        BOOLEAN;
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

 --Obtengo la información de la cola de reintentos, del proceso Job Retry
   cursor c_axis_cola_reintentos is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_COLA_REIN_AXIS_NEW;
   
   lc_axis_cola_reintentos                 c_axis_cola_reintentos%rowtype;
   
   lv_recomendacion  varchar2(500); 
   lb_hay_datos      boolean:=false;
   LV_ASUNTO_MAIL    VARCHAR2(100):='QC - SUSPENSION'; 
   lv_mensaje_mail   varchar2(2000); 

begin  

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_retry:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_retry';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);

   
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ----------------------------------------------------------------------------   
  --Obtengo la información de la cola de reintentos, del proceso Job Retry
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola_reintentos;  
            loop  
                FETCH c_axis_cola_reintentos into lc_axis_cola_reintentos; 
                
                exit when c_axis_cola_reintentos%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola_reintentos.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecución Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la información de Transacciones de BSCS finalizadas en la ejecución de hoy';
                
                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION'
                                             ,lv_mensaje
                                             ,lc_axis_cola_reintentos.ESTADO
                                             ,lc_axis_cola_reintentos.DESCRIPCION_ESTADO
                                             ,lc_axis_cola_reintentos.VALOR                                         
                                             ,lv_recomendacion); 
                                             
                if ln_check=0 then
                  --hubo error;
                  null;
                end if;                                                           
                
                lb_hay_datos:=true;
            end loop;
            close c_axis_cola_reintentos;   
       end;   
       
       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS EN EL PROCESO DE BSCS';
          
          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL                                                       
                                        ,NULL); 
          if ln_check=0 then
             --hubo error;
             null;
          end if;
                   
       end if;        
         
       i_suspension_resumen:=  Rc_Trx_Qc_Suspension_All.Fct_TTC_Suspension_Resumen_t(gn_secuencia,GV_DESSHORT_P5,'R');
       
       IF i_suspension_resumen.NUM_TRANS IS NOT NULL AND i_suspension_resumen.NUM_TRANS >  pn_Max_Trx_encolada THEN
          lv_mensaje_mail:= 'Alerta, existen '||i_suspension_resumen.NUM_TRANS||' transacciones encoladas en proceso '||i_suspension_resumen.DESSHORT;
          BEGIN
           sysadm.send_mail2.mail(from_name => 'sistema_archivo@conecel.com',
                                   to_name   => 'wcruz@cimaconsulting.com.ec',
                                   cc        => NULL,
                                   cco       => NULL,
                                   subject   => LV_ASUNTO_MAIL,
                                   message   => lv_mensaje_mail || ' - ' || i_suspension_resumen.RECOMENDACION);
           EXCEPTION
               WHEN OTHERS THEN
                 NULL;                               
           END;
       END IF;
       
     
 PV_OUT_ERROR:=NULL;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de --------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_Qc_Suspension_All.Prc_retry-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_retry '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);   
  
  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_Qc_Suspension_All.Prc_retry- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;


end RC_Trx_Qc_Suspension_All;
/
