create or replace package reloj.RC_Trx_TimeToCash_Customize is
--
FUNCTION fct_Return_Count_Contract (PN_CUSTOMER_ID  IN INTEGER) RETURN NUMBER;

FUNCTION fct_Return_Count_services (PN_CO_ID  IN INTEGER) RETURN NUMBER;
--
End RC_Trx_TimeToCash_Customize;
/
create or replace package body reloj.RC_Trx_TimeToCash_Customize is
  /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   26/11/2009
  * Modificado:
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
  *       1. Para efectos de mejorar el rendimiento en la distribución de transascciones se
  *           estima la cantidad de servicios a inactivar.
  ********************************************************************************************************/
FUNCTION fct_Return_Count_Contract (PN_CUSTOMER_ID  IN INTEGER) RETURN NUMBER IS
  Ln_count_contract  NUMBER;

BEGIN
      SELECT COUNT(a.co_id)
          INTO Ln_count_contract
        FROM ttc_contractplannig_s a
     WHERE a.customer_id=PN_CUSTOMER_ID;
      
      RETURN(Ln_count_contract);
  
  EXCEPTION
        WHEN NO_DATA_FOUND THEN
         RETURN 0; 
        WHEN OTHERS THEN
         RETURN 0;   
END fct_Return_Count_Contract;

/*************************************************************************************************************/
FUNCTION fct_Return_Count_services (PN_CO_ID  IN INTEGER) RETURN NUMBER IS
  Ln_count_serv  NUMBER;
BEGIN

      SELECT /*+ RULE +*/ COUNT(SNCODE)
          INTO Ln_count_serv
        FROM sysadm.profile_service
     WHERE CO_ID = PN_CO_ID;
      
      RETURN(Ln_count_serv);

  EXCEPTION
      WHEN NO_DATA_FOUND THEN
             RETURN(0);   
      WHEN OTHERS THEN
             RETURN 0;
   
END fct_Return_Count_services;


/********************************************************************************************/
  
END RC_Trx_TimeToCash_Customize;
/
