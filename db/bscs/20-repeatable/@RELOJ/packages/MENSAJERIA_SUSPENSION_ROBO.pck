create or replace package reloj.MENSAJERIA_SUSPENSION_ROBO is
  -- Author  : Katty Mite
  -- Created : 24/01/2011 09:27:43
  -- Purpose : Procesos de Mensajeria
  --VARIABLES GLOBALES

procedure PAQ_MENSAJERIA_SUSPENSION_ROBO (pv_error out varchar2);

end MENSAJERIA_SUSPENSION_ROBO;
/
create or replace package body reloj.MENSAJERIA_SUSPENSION_ROBO is

procedure PAQ_MENSAJERIA_SUSPENSION_ROBO (pv_error out varchar2)is
 
---------------------------------------------------------------
--ln_tope1                            NUMBER;
--ln_tope2                            NUMBER;
--lv_recomendacion1                   VARCHAR2(5000);
--lv_recomendacion2                   VARCHAR2(5000);
ln_error                           NUMBER;
---------------------------------------------------------------
  cursor c_mensaje is
  
SELECT us_id,us_name FROM ttc_workflow_user;
 --  select DESCRIPCION,    TOTAL       from axis_view_rc_qc_all;
          
          
          --SELECT DESCRIPCION,PROCESOS FROM TTC_VIEW_QC_MENSAJERIA;
   lv_mensaje_mail  varchar2(15000):='';
   LV_ASUNTO_MAIL   varchar2(500);
   --ln_error         number;
  
   
   rg_registro      c_mensaje%rowtype;
     
   
  begin
    LV_ASUNTO_MAIL:='mail de prueba';
    lv_mensaje_mail:='<html>';
    --lv_mensaje_mail_1:=lv_mensaje_mail||'</html>';
    
    open c_mensaje;
    loop
      fetch c_mensaje into rg_registro;      
      exit when c_mensaje%notfound;
     lv_mensaje_mail:=lv_mensaje_mail||rg_registro.us_id||' '|| rg_registro.us_name||'<br>';
    end loop;
    close c_mensaje;
    lv_mensaje_mail:=lv_mensaje_mail||'    Recomendaciones:<br>
<dir>- No Bajar los procesos del Reloj.<br></dir>
<dir>- No realizar pases que intervengan con el proceso.<br></dir>
<dir>- Verificar que las IDL de tecnotree se encuentren levantadas.<br></dir>
<dir>- Monitorear el avance del proceso, antes y despu�s del Reinicio de Axis. Ejecutar el Query en BSCSPROD:<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>select</b> descripcion_estado,valor,estado <b>from</b> reloj.TTC_VIEW_QC_COLAS_AXIS;</table bgcolor></dir></dir>';




    /********** invoca al procedimiento de envio de correo   *****************************/
    BEGIN
             
           reloj.admin_enviar_correo_prueba (pv_servidor => 'conecel.com',--sisago.amp_enviar_correo@axis
                                                pv_de => 'sistema_archivo@conecel.com',
                                            --  pv_para => 'kmite@cimaconsulting.com.ec',--jronquillo@conecel.com',
                                                Pv_Para =>'kmite@cimaconsulting.com.ec', --'gsiprocesos@conecel.com',
                                                Pv_CC =>'kmite@cimaconsulting.com.ec',----wcruz@cimaconsulting.com.ec --'vsoria@conecel.com;kcanizares@conecel.com;jronquillo@conecel.com',
                                                Pv_Asunto => LV_ASUNTO_MAIL,
                                                Pv_mensaje => lv_mensaje_mail,
                                                Pv_CodigoError => ln_error,
                                                Pv_MensajEerror => pv_error);
     EXCEPTION
         WHEN OTHERS THEN
           NULL;                               
     END; 
     /****************************************************************************************/
  end PAQ_MENSAJERIA_SUSPENSION_ROBO;
end MENSAJERIA_SUSPENSION_ROBO;
/
