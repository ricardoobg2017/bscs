create or replace package reloj.RC_Trx_Qc_Suspension_Jobs_W IS
 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     CIMA Jose Luis Reinoso
  * Creacion:   24/11/2010
  * Modificado: 24/11/2010
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
   * Pro�sito:
  * 1.- Qc del proceso del reloj de Cobranzas Suspension colas de procesamiento en AXIS.    
  * 2.- Qc de las Transacciones por enviar a procesar. De 3 d�as posteriores.
  *       Ejemplo: 
              25/11/2010 
                Paso a estatus 34 --> 999
                Paso a estatus 80 --> 999
                Paso a estatus 33 --> 999
                Paso a estatus 35 --> 999
             26/11/2010
               Paso a estatus 34 --> 999
                Paso a estatus 80 --> 999
                Paso a estatus 33 --> 999
                Paso a estatus 35 --> 999
             27/11/2010
  ********************************************************************************************************/


--VARIABLES GLOBALES
GV_DESSHORT VARCHAR2(14) := 'TTC_QC_SUSP_1';
GV_DESSHORT_P4 VARCHAR2(12) := 'TTC_DISPTC';
GV_DESSHORT_P5 VARCHAR2(12) := 'TTC_SENDRETR';
Gv_unidad_registro_scp varchar2(30):='Resumenes';
gv_value_rule_string_cash varchar2(40):='TTC_FLAG_CUSTOMER_CASH_DIA';
gv_value_rule_string_out varchar2(30):='TTC_FLAG_CUSTOMER_OUT_RELOJ';
GV_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_CURSOR_CUSTCODE';
GV_STATUS_P   VARCHAR2(1) := 'P';

GV_TTC_RANGE_HOUR_FULL     VARCHAR2(30) := 'TTC_RANGE_HOUR_FULL';
GV_TTC_RANGE_HOUR_SEMIFULL VARCHAR2(30) := 'TTC_RANGE_HOUR_SEMIFULL';
GV_TTC_RANGE_HOUR_LOW      VARCHAR2(30) := 'TTC_RANGE_HOUR_LOW';
GV_TTC_MAX_RESTRICT_TIME   VARCHAR2(30) := 'TTC_MAX_RESTRICT_TIME';

gv_hora_ini VARCHAR2(30);
gv_hora_fin VARCHAR2(30);

t_RangeTime_Restrict               ttc_Rule_Values_t;
t_RangeTime_Full_p4                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p4            ttc_Rule_Values_t;
t_RangeTime_Low_p4                 ttc_Rule_Values_t;
t_RangeTime_Select                 ttc_Rule_Values_t;

t_RangeTime_Full_p5                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p5            ttc_Rule_Values_t;
t_RangeTime_Low_p5                 ttc_Rule_Values_t;

i_Process_Programmer_t       ttc_Process_Programmer_t;
GV_DESSHORT_LOW             VARCHAR2(15) := 'TTC_REAC_LOW';
GV_DESSHORT_RETRY_SEMI_FULL VARCHAR2(16) := 'TTC_REAC_SFUL';

gn_secuencia                NUMBER;

/*Function RC_CANT_TOTAL_REGISTRO(pv_table_name in varchar2) Return Number;*/

FUNCTION Fct_Verifica_Resumen_QC_Susp(Pn_id_qc NUMBER,
                                      Pv_Desshort VARCHAR2,
                                      Pv_estado VARCHAR2) RETURN TTC_Suspension_Resumen_t;


procedure Prc_Head (pv_error        OUT VARCHAR2,
                    pn_cant_proc    out NUMBER);

procedure Prc_dispatch(Pv_Desshort             IN  varchar2,
                       pn_id_bitacora_scp      IN  NUMBER,
                       PV_OUT_ERROR            OUT VARCHAR2);    
                       
procedure Prc_retry(Pv_Desshort                 IN varchar2,
                       pn_id_bitacora_scp       IN NUMBER,
                       PV_OUT_ERROR             OUT VARCHAR2); 
                       
procedure Prc_Notify (pv_error OUT VARCHAR2);   

                    
 
END RC_Trx_Qc_Suspension_Jobs_W;
/
create or replace package body reloj.RC_Trx_Qc_Suspension_Jobs_W is

--**********************************************************************************
FUNCTION Fct_Verifica_Resumen_QC_Susp(Pn_id_qc NUMBER,Pv_desshort VARCHAR2, Pv_estado VARCHAR2) RETURN TTC_Suspension_Resumen_t as
     CURSOR cur_in IS
           SELECT VALUE(h)
             FROM TTC_Suspension_Resumen_s  h
          WHERE id_qc  = Pn_id_qc
                AND desshort = Pv_desshort
                AND estado = Pv_estado;
      doc_in   TTC_Suspension_Resumen_t;          
BEGIN
      OPEN cur_in;
      FETCH cur_in INTO doc_in;
      CLOSE cur_in;
      RETURN doc_in;
END;

    --**********************************************************************************

 
FUNCTION Fct_inserta_resumen (pn_id_qc in number,
                              PV_DESSHORT in varchar2,                            
                              pv_name_qc in varchar2, 
                              pv_descripcion_qc in varchar2,
                              pv_estado in varchar2,
                              pv_descripcion_estado in varchar2,
                              pv_num_trans number,
                              pv_recomendacion in varchar2)return number is
      
  BEGIN
  
      insert into TTC_SUSPENSION_RESUMEN  (ID_QC
                                           ,DESSHORT
                                           ,NAME_QC
                                           ,DESCRIPCION_QC
                                           ,ESTADO
                                           ,DESCRIPCION_ESTADO
                                           ,NUM_TRANS
                                           ,FECHA_TRANS
                                           ,RECOMENDACION)     
                                           VALUES
                                           (pn_id_qc
                                           ,PV_DESSHORT
                                           ,pv_name_qc
                                           ,pv_descripcion_qc
                                           ,pv_estado
                                           ,pv_descripcion_estado
                                           ,pv_num_trans
                                           ,sysdate
                                           ,pv_recomendacion);
       Return 1;
    Exception
        When NO_DATA_FOUND Then
          Return 0;
        When Others Then
          Return 0;
END;

procedure Prc_Head (pv_error         OUT VARCHAR2,
                    pn_cant_proc     out number) is

CURSOR C_Trx_In IS
              SELECT  TTC_TRX_IN.NEXTVAL FROM dual;
--VARIABLES
i_process                          ttc_process_t;
i_process_p4                       ttc_process_t;
i_process_p5                       ttc_process_t;
i_rule_element_p4                  ttc_Rule_Element_t;
i_rule_element_p5                  ttc_Rule_Element_t;

Ln_Seqno                           NUMBER;
Ld_Fecha_Actual                    DATE;
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR(3);
LV_HourNow                         VARCHAR2(20);
LV_fecha_ejecucion                 date;
LN_NUM_PROCESOS_EJEC               NUMBER;
lv_titulo                          VARCHAR2(500);
lv_proceso_tmp                     VARCHAR2(100):='X';
LN_TOPE                            NUMBER:=0;
ln_error                           NUMBER;
--lv_error                           VARCHAR2(500);
LV_RECOMENDACION                   VARCHAR2(5000);
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_id_bitacora_scp                 number:=0; 
ln_total_registros_scp             number;
lv_id_proceso_scp                  varchar2(100):=GV_DESSHORT;
lv_referencia_scp                  varchar2(200):='RC_Trx_Qc_Suspension_Jobs_W.RC_HEAD';
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
---------------------------------------------------------------
--cursor para extraer los adatos de la tabla de resumen, segun la ejecucion del QC
   CURSOR c_ttc_suspension(cn_secuencia NUMBER) IS
   select id_qc,
          name_qc,
          descripcion_qc,
          estado,
          descripcion_estado,
          num_trans,
          fecha_trans,
          recomendacion
     from ttc_suspension_resumen
    WHERE id_qc = cn_secuencia;
    
    LV_ASUNTO_MAIL    VARCHAR2(100):='QC - SUSPENSION'; 

   lv_mensaje_mail   varchar2(2000);
   lv_estado         VARCHAR2(50);
   
   i c_ttc_suspension%ROWTYPE;
 
CURSOR C_TTC_PROCESADA IS
SELECT PROCESOS FROM TTC_VIEW_QC_CONTRHISTORY;
begin
   lv_titulo:='<html><b><u>Resumen de la Ejecuci�n hasta ';
   LN_TOPE:= 5000;
   LV_RECOMENDACION:='    <b>Recomendaciones:</b><br><br>
<dir>- Verificar la ejecuci�n del proceso Retry Job en control M: RELOJ_NEW_RC_RETRY y RELOJ_NEW_RC_DISPATCH.<br></dir>
<dir>- Verificar los logs en la tabla reloj.ttc_log_dispactch<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>Select</b> * <b>from</b> reloj.ttc_log_dispactch where lastdate>=sysdate -1.</table bgcolor></dir></dir>
<dir>- Verificar las idls de tecnotree.<br></dir>
<dir>- Aumentar la capacidad de procesamiento, en la tabla ttc_rule_element.<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>Update</b> ttc_rule_element <b>set</b> SET value_max_trx_by_execution=value_max_trx_by_execution+100,  maxretentrx=maxretentrx + 100 WHERE process =4 AND desshort IN ('||''''||'TTC_DISPTC'||''''||','||''''||'TTC_DISPTC_SFUL'||''''||')'||'</table bgcolor></dir></dir>
<dir>- Verificar la CMS_MASIVO, que se encuentre levantada y procesando. IP:130.2.120.10.<br></dir>';
  --EXTRAIGO LA SECUENCIA DE LA EJECUCION
  SELECT TTC_S_SUSPENSION_RESUMEN.Nextval INTO gn_secuencia FROM dual;
  
  pn_cant_proc:=0;
  Ld_Fecha_Actual:= SYSDATE;
  SELECT to_date(SYSDATE, 'dd/mm/rrrr') INTO LV_fecha_ejecucion FROM dual;
  SELECT to_char(SYSDATE, 'hh24:mi:ss') INTO LV_HourNow FROM dual;
  
  -- SCP:INICIO 
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
  ----------------------------------------------------------------------------
  lv_referencia_scp:='Inicio del proceso de Qc Supension: '||lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,null,null,null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  if ln_error_scp <>0 Then
     LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
     Raise LE_ERROR;
  end if;
  
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
  lv_mensaje_tec_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  
  --Datos Configuracion Proceso TTC_QC_SUSP_1 - QC Suspension
  i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
  IF i_process.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   --Fin Datos Configuracion Suspension  
   
   -- 
  
  --Datos Configuracion Proceso 4: TTC_DISPTC 
  i_process_p4:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P4);
  IF i_process_p4.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   i_rule_element_p4:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P4);
   IF i_rule_element_p4.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;
   --Fin Datos Configuracion P4  
     
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 4--------------------------
  ---------------------------------------------------------------------------- 
    
    --Rangos del proceso 4:TTC_DISPTC
    t_RangeTime_Full_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_FULL);
    t_RangeTime_SemiFull_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_SEMIFULL);
    t_RangeTime_Low_p4:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,GV_TTC_RANGE_HOUR_LOW);
    

    --LLamada al proceso del Job_Dispatch
    
    RC_Trx_Qc_Suspension_Jobs_W.Prc_dispatch(PV_DESSHORT => i_process_p4.DesShort,
                                           pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                           PV_OUT_ERROR => LV_MENSAJE);
             
  
  IF  LV_MENSAJE IS NOT NULL THEN
      Raise LE_ERROR;
  END IF;  
  
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 4--------------------------------- 
  --------------------------------------------------------------------------------
  
  --Datos Configuracion Proceso 5: TTC_SENDRETR 
  i_process_p5:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P5);
  IF i_process_p5.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   i_rule_element_p5:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P5);
   IF i_rule_element_p5.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;
   
   ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 5--------------------------
  ---------------------------------------------------------------------------- 
    --Rangos del proceso 5
    t_RangeTime_Full_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_FULL);
    t_RangeTime_SemiFull_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_SEMIFULL);
    t_RangeTime_Low_p5:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,GV_TTC_RANGE_HOUR_LOW);
    
    
    RC_Trx_Qc_Suspension_Jobs_W.Prc_retry(PV_DESSHORT => i_process_p5.DesShort,
                                                       pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                                       PV_OUT_ERROR => LV_MENSAJE);
             
  
  IF  LV_MENSAJE IS NOT NULL THEN
      Raise LE_ERROR;
  END IF;      
  lv_mensaje_mail:=chr(13)||chr(10)||lv_titulo||LV_fecha_ejecucion||' '||LV_HourNow||'</u></b><br><br>';
  --FOR i IN c_ttc_suspension(gn_secuencia) LOOP
       open c_ttc_suspension(gn_secuencia);
     loop
       fetch c_ttc_suspension into i;
       exit when c_ttc_suspension%notfound;
       IF lv_proceso_tmp <> i.name_qc THEN
          lv_proceso_tmp := i.name_qc;
          lv_mensaje_mail:= lv_mensaje_mail||'<b>'||i.name_qc||'</b><br><br>';
       END IF;
       IF i.estado IS NOT NULL AND I.NUM_TRANS IS NOT NULL THEN          
           SELECT decode(i.estado,'F','Finalizadas','P','Pendientes','R','Con error de reintento','E','Error','Otros')INTO lv_estado FROM dual;
           lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'Transacciones ' ||lv_estado||'--> '||i.num_trans||'<br><br>';      
           IF i.num_trans > ln_tope AND i.estado = 'P' THEN
             lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'    '||lv_recomendacion||'<br><br>';
           END IF;
       ELSE
           lv_mensaje_mail:= lv_mensaje_mail||'<dir>'||'    '||I.DESCRIPCION_QC||'</dir><br><br>';
       END IF;
       
     END LOOP;

       CLOSE c_ttc_suspension;
  OPEN C_TTC_PROCESADA;
  FETCH C_TTC_PROCESADA INTO LN_NUM_PROCESOS_EJEC;
  CLOSE C_TTC_PROCESADA;
  lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'<b>ENVIO TRX A TABLA MAESTRA BSCS:</b><br> <dir>Trx pendientes --> '||LN_NUM_PROCESOS_EJEC||'</dir><br>';
   IF LN_NUM_PROCESOS_EJEC > 0 THEN
    lv_mensaje_mail:= lv_mensaje_mail||'    <dir><b>Recomendaciones:</b></dir><br><br>
    <dir><dir>- Verificar la ejecuci�n del proceso Retry Job en control M: RELOJ_NEW_RC_RETRY</dir></dir>';
   END IF;
  lv_mensaje_mail:=lv_mensaje_mail||'</html>';
       --ENVIO DE MAIL
     BEGIN
           sisago.amp_enviar_correo@axis(pv_servidor => 'conecel.com',
                                                pv_de =>'reloj_cobranzas@conecel.com',
                                                pv_para =>'kmite@cimaconsulting.com.ec',-- 'gsiprocesos@conecel.com',
                                                pv_cc =>'kmite@cimaconsulting.com.ec',--  'vsoria@conecel.com;kcanizares@conecel.com;jronquillo@conecel.com',
                                                pv_asunto => LV_ASUNTO_MAIL,
                                                pv_mensaje => lv_mensaje_mail,
                                                pv_codigoerror => ln_error,
                                                pv_mensajeerror => pv_error);
     EXCEPTION
         WHEN OTHERS THEN
           NULL;                               
     END;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 5--------------------------------- 
  --------------------------------------------------------------------------------
  
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_In ;
  FETCH C_Trx_In INTO Ln_Seqno;
  CLOSE C_Trx_In;                                                                                                          
  i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                   Seqno => Ln_Seqno,
                                                   RunDate => LV_fecha_ejecucion,
                                                   LastModDate => LV_fecha_ejecucion
                                                   );
  LV_MENSAJE:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);  
  LV_STATUS:='OKI';
  LV_MENSAJE:='RC_HEAD-SUCESSFUL';                 
  pv_error:=LV_MENSAJE;
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);
   
  EXCEPTION
  WHEN LE_ERROR THEN
      --LV_STATUS    := 'ERR';
      LV_MENSAJE   :='RC_Trx_Qc_Suspension_Jobs_W.Prc_Head:->'|| LV_MENSAJE;
      pv_error:=LV_MENSAJE;      
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------      
      lv_mensaje_apl_scp := '   Error en el Procesamiento';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            pv_error,
                                            Null,
                                            3,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);      
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp, ln_error_scp, lv_error_scp);
      ----------------------------------------------------------------------------
      
  WHEN OTHERS THEN
             --LV_STATUS:='ERR';
             LV_MENSAJE:=LV_MENSAJE|| ' RC_Trx_Qc_Suspension_Jobs_W.Prc_Head:'  || 'ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 200);
             pv_error:=LV_MENSAJE;             
             
end;

procedure Prc_dispatch(PV_DESSHORT           IN  Varchar2,
                       pn_id_bitacora_scp    IN  NUMBER,
                       PV_OUT_ERROR          OUT VARCHAR2) is
------------------------------------

--VARIABLES
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
Le_Error_Trx                       EXCEPTION;
ln_check                           number:=0;

--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

--Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
   cursor c_axis_cola is
   select descripcion_estado,valor,estado from reloj.TTC_VIEW_QC_COLAS_AXIS;
   
   lc_axis_cola                            c_axis_cola%rowtype;
   
   lv_recomendacion  varchar2(500); 
   lb_hay_datos      boolean:=false;


begin  

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_dispatch:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_dispatch';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);

   
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ---------------------------------------------------------------------------- 
       --Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola;  
            loop  
                FETCH c_axis_cola into lc_axis_cola; 
                
                exit when c_axis_cola%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                
                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION.PRC_DISPATCH'
                                             ,lv_mensaje
                                             ,lc_axis_cola.ESTADO
                                             ,lc_axis_cola.DESCRIPCION_ESTADO
                                             ,lc_axis_cola.VALOR                                             
                                             ,lv_recomendacion);
                                             
              lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
              lv_mensaje_tec_scp:='Insercion en TTC_SUSPENSION_RESUMEN Prc_dispatch';
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    Null,
                                                    0,
                                                    Gv_unidad_registro_scp,
                                                    Null,
                                                    Null,
                                                    null,null,
                                                    'N',ln_error_scp,lv_error_scp);                                                 
            
               if ln_check=0 then
                  --hubo error;
                 lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
                 lv_mensaje_tec_scp:='Error al insert en TTC_SUSPENSION_RESUMEN Prc_dispatch';
                 scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp); 
                  null;
               end if;           
                
               lb_hay_datos:=true;
            end loop;
            close c_axis_cola;   
       end;
       
       if not lb_hay_datos then  
          lv_mensaje:='NO EXISTE DATOS PARA EL PROCESO JOBDISPATCH';          
          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION.PRC_DISPATCH'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL                                                       
                                        ,NULL); 
         lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
          lv_mensaje_tec_scp:='Insercion: NO EXISTE DATOS PARA EL PROCESO JOBDISPATCH  en TTC_SUSPENSION_RESUMEN Prc_dispatch';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                2,
                                                Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'N',ln_error_scp,lv_error_scp); 
          if ln_check=0 then
             --hubo error;
               lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
                 lv_mensaje_tec_scp:='Error al insert: NO EXISTE DATOS PAARA EL PROCESO JOBDISPATCH en TTC_SUSPENSION_RESUMEN Prc_dispatch';
                 scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp); 
             null;
          end if;                                             
                  
       end if;
       
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de QC--------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_Qc_Suspension_Jobs_W.Prc_dispatch-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_dispatch '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);   
  
  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_Qc_Suspension_Jobs_W.Prc_dispatch- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;


procedure Prc_retry(PV_DESSHORT               IN  Varchar2,
                       pn_id_bitacora_scp     IN  NUMBER,
                       PV_OUT_ERROR           OUT VARCHAR2) is
------------------------------------

--VARIABLES
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
Le_Error_Trx                       EXCEPTION;
ln_check                           number:=0;

--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

 --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
   cursor c_axis_cola_reintentos is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_COLA_REIN_AXIS;
   
   lc_axis_cola_reintentos                 c_axis_cola_reintentos%rowtype;
   
   lv_recomendacion  varchar2(500); 
   lb_hay_datos      boolean:=false;


begin  

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_retry:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_retry';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);

   
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ----------------------------------------------------------------------------   
  --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
       lb_hay_datos:=false;
       begin  
            OPEN c_axis_cola_reintentos;  
            loop  
                FETCH c_axis_cola_reintentos into lc_axis_cola_reintentos; 
                
                exit when c_axis_cola_reintentos%notfound;
                                
                lv_recomendacion:=NULL;
                IF lc_axis_cola_reintentos.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF; 
               
                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';
                
                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION.PRC_RETRY'
                                             ,lv_mensaje
                                             ,lc_axis_cola_reintentos.ESTADO
                                             ,lc_axis_cola_reintentos.DESCRIPCION_ESTADO
                                             ,lc_axis_cola_reintentos.VALOR                                         
                                             ,lv_recomendacion); 
               lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
               lv_mensaje_tec_scp:='Insercion en TTC_SUSPENSION_RESUMEN Prc_retry';
               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    Null,
                                                    0,
                                                    Gv_unidad_registro_scp,
                                                    Null,
                                                    Null,
                                                    null,null,
                                                    'N',ln_error_scp,lv_error_scp); 
                                             
                if ln_check=0 then
                  --hubo error;
                  lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
                  lv_mensaje_tec_scp:='Error al insert en TTC_SUSPENSION_RESUMEN Prc_retry';
                  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp); 
                  null;
                end if;                                                           
                
                lb_hay_datos:=true;
            end loop;
            close c_axis_cola_reintentos;   
       end;   
       
       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS PARA EL PROCESO JOBRETRY';
          
          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION.PRC_RETRY'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL                                                       
                                        ,NULL);
          lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
          lv_mensaje_tec_scp:='Insercion: NO EXISTE DATOS PARA EL PROCESO JOBRETRY  en TTC_SUSPENSION_RESUMEN Prc_retry';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                2,
                                                Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'N',ln_error_scp,lv_error_scp);
          if ln_check=0 then
             --hubo error;
             lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
             lv_mensaje_tec_scp:='Error al insert: NO EXISTE DATOS PARA EL PROCESO JOBRETRY en TTC_SUSPENSION_RESUMEN Prc_retry';
             scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  Null,
                                                  3,
                                                  Gv_unidad_registro_scp,
                                                  Null,
                                                  Null,
                                                  null,null,
                                                  'N',ln_error_scp,lv_error_scp); 
             null;
          end if;
                   
       end if;        
         
       
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de --------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_Qc_Suspension_Jobs_W.Prc_retry-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_retry '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);   
  
  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_Qc_Suspension_Jobs_W.Prc_retry- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;

procedure Prc_Notify (pv_error OUT VARCHAR2)IS
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_id_bitacora_scp                 number:=0; 
ln_total_registros_scp             number;
lv_id_proceso_scp                  varchar2(100):=GV_DESSHORT;
lv_referencia_scp                  varchar2(200):='RC_Trx_Qc_Suspension_Jobs_W.RC_NOTIFY';
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
--ln_registros_procesados_scp        number := 0;
ln_registros_error_scp             number:=0;
---------------------------------------------------------------
ln_tope                            NUMBER;
lv_recomendacion                   VARCHAR2(5000);
ln_error                           NUMBER;
CURSOR C_PROCESOS IS
select fecha,
descripcion_estado,
procesos 
from ttc_view_qc_status_procesos;

RG_PROCESOS C_PROCESOS%ROWTYPE;
LV_MENSAJE  VARCHAR2(500);
LE_ERROR    EXCEPTION;
LV_ASUNTO_MAIL    VARCHAR2(3000);
lv_mensaje_mail   VARCHAR2(9000);
lv_fecha_temp     DATE:=to_date('01/01/1900','dd/mm/yyyy');
lv_titulo         VARCHAR2(500):='<html><u><b>Resumen de las transacciones por Procesar de los Proximos 5 d�as</b></u><br><br>';
BEGIN
LV_ASUNTO_MAIL:='Proceso Suspensi�n: Transacciones por Procesar';
ln_tope:=10000;
lv_recomendacion:='    Recomendaciones:<br>
<dir>- No Bajar los procesos del Reloj.<br></dir>
<dir>- No realizar pases que intervengan con el proceso.<br></dir>
<dir>- Verificar que las IDL de tecnotree se encuentren levantadas.<br></dir>
<dir>- Monitorear el avance del proceso, antes y despu�s del Reinicio de Axis. Ejecutar el Query en BSCSPROD:<br></dir>
<dir><dir><table bgcolor="##FFFF00"><b>select</b> descripcion_estado,valor,estado <b>from</b> reloj.TTC_VIEW_QC_COLAS_AXIS;</table bgcolor></dir></dir>
<dir>- Verificar la CMS_MASIVO, que se encuentre levantada y procesando. IP: 130.2.120.10 los directorios C:\CMS_MASIVO\SERVER\RELOJ\1,.. \2,.. \3,.. \4,.. \5<br></dir>
<dir> Si es preciso reiniciarla antes de la ejecuci�n del proceso del reloj.<br></dir>';

  -- SCP:INICIO 
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
  ----------------------------------------------------------------------------
  lv_referencia_scp:='Inicio del proceso de Qc Supension: '||lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,null,null,null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  if ln_error_scp <>0 Then
     LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
     Raise LE_ERROR;
  end if;
  
  -----------------------------------------------
    -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_Notify:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_Notify';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);

   
  ----------------------------------------------------------------------------                                      
lv_mensaje_mail:=chr(13)||chr(10)||lv_titulo||chr(13)||chr(10);
     open C_PROCESOS;
     loop
       fetch C_PROCESOS into RG_PROCESOS;
       exit when C_PROCESOS%notfound;
            IF lv_fecha_temp <> RG_PROCESOS.FECHA THEN
              lv_fecha_temp := RG_PROCESOS.FECHA;
              lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'<u>FECHA:'||RG_PROCESOS.FECHA||'</u><br><br>';
             END IF;
            
            IF RG_PROCESOS.PROCESOS > ln_tope THEN
             lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'    <font color="#DF0101">'||RG_PROCESOS.DESCRIPCION_ESTADO||' '||'-->'||RG_PROCESOS.PROCESOS||'</font color><br><br>';
             lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'    <font color="#0404B4">'||lv_recomendacion||'</font color><br>';
             ELSE
             lv_mensaje_mail:= lv_mensaje_mail||chr(13)||chr(10)||'    '||RG_PROCESOS.DESCRIPCION_ESTADO||' '||'-->'||RG_PROCESOS.PROCESOS||'<br><br>';
            END IF;
     end loop;
     lv_mensaje_mail:=lv_mensaje_mail||'</html>';
     close C_PROCESOS;  
     
         BEGIN
             sisago.amp_enviar_correo@axis(pv_servidor => 'conecel.com',
                                                pv_de => 'reloj_cobranzas@conecel.com',
                                                pv_para =>'kmite@cimaconsulting.com.ec', --'gsiprocesos@conecel.com',
                                                pv_cc =>'jreinoso@cimaconsulting.com.ec',  --'vsoria@conecel.com;kcanizares@conecel.com;jronquillo@conecel.com',
                                                pv_asunto => LV_ASUNTO_MAIL,
                                                pv_mensaje => lv_mensaje_mail,
                                                pv_codigoerror => ln_error,
                                                pv_mensajeerror => pv_error);
     EXCEPTION
         WHEN OTHERS THEN
           NULL;                               
     END; 
     COMMIT;
     EXCEPTION
       WHEN OTHERS THEN
          ROLLBACK;
          LV_MENSAJE := 'RC_Trx_Qc_Suspension_Jobs_W.Prc_retry- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
          pv_error:=LV_MENSAJE;
          If ln_registros_error_scp = 0 Then
             ln_registros_error_scp:=-1;
         End If;
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                               
  
END Prc_Notify;

  

end RC_Trx_Qc_Suspension_Jobs_W;
/
