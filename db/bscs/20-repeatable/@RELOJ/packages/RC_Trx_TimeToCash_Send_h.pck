create or replace package reloj.RC_Trx_TimeToCash_Send_h is

  GV_DESSHORT VARCHAR2(8) := 'TTC_SEND';
  GV_DESSHORT_DTH VARCHAR2(15) := 'TTC_SEND_DTH';--8693 JJI
  
  GV_PK_ID           NUMBER:=1; --Package de Reactivación
  GV_TTC_REAC_COMMENT VARCHAR2(30) := 'TTC_REAC_COMMENT';
  GV_STATUS_P   VARCHAR2(1) := 'P';
  GV_STATUS_T   VARCHAR2(1) := 'T';
  GV_STATUS_A   VARCHAR2(1) := 'A';
  GV_STATUS_L   VARCHAR2(1) := 'L';
  GV_STATUS_E   VARCHAR2(1) := 'E';
  GN_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_VALUE_TRX';
  Gv_unidad_registro_scp varchar2(30):='Cuentas';
  TYPE T_TableName                 IS TABLE OF ttc_relation_objects_s.table_valuestring%TYPE  INDEX BY BINARY_INTEGER;
  TYPE T_DesShort                     IS TABLE OF VARCHAR2(100)  INDEX BY BINARY_INTEGER;
  TYPE T_DesLong                      IS TABLE OF VARCHAR2(500)  INDEX BY BINARY_INTEGER;
  TYPE T_SumCountTread         IS TABLE OF NUMBER  INDEX BY BINARY_INTEGER;
   V_TableName              T_TableName;                 
   V_SumCountTread      T_SumCountTread;                 

   V_Customers_tb           T_DesShort;                 
   V_Customers_id           T_SumCountTread;       

   
  PROCEDURE RC_HEAD(PV_FILTER     IN VARCHAR2,
                    PN_HILO           In Number,
                    PN_GROUPTREAD IN NUMBER,
                    PN_CANTIDAD   In Number,
                    PN_SOSPID        IN NUMBER,
                    PV_OUT_ERROR  OUT VARCHAR2);

  PROCEDURE Prc_Distribuir_Table_ALL(PN_ID_PROCESS        IN NUMBER,
                                                                              PV_FILTER                 IN VARCHAR2,
                                                                              PN_HILO                     IN NUMBER,
                                                                              PD_FECHA                  DATE,
                                                                              PN_MAXTRX_EXE      NUMBER,
                                                                              PN_MAX_COMMIT    NUMBER,          
                                                                              pn_id_bitacora_scp    NUMBER,
                                                                              PN_SOSPID        IN NUMBER,
                                                                              PV_OUT_ERROR        OUT VARCHAR2
                                                                              );

  PROCEDURE Prc_Distribuir_Table_Group(PN_ID_PROCESS     IN NUMBER,
                                                                                 PV_FILTER              IN VARCHAR2,
                                                                                 PN_HILO                  In Number,
                                                                                 PN_GROUPTREAD IN NUMBER,
                                                                                 PD_FECHA                DATE,
                                                                                 PN_MAXTRX_EXE    NUMBER,
                                                                                 PN_MAX_COMMIT  NUMBER,          
                                                                                 pn_id_bitacora_scp    NUMBER,
                                                                                 PN_SOSPID        IN NUMBER,
                                                                                 PV_OUT_ERROR      OUT VARCHAR2
                                                                                 );
                                                                                 
  
  
  --[8693] JJI - INICIO
  PROCEDURE RC_HEAD_DTH(PV_FILTER     IN VARCHAR2,
                      PN_HILO       IN NUMBER,
                      PN_GROUPTREAD IN NUMBER,
                      PN_CANTIDAD   IN NUMBER,
                      PN_SOSPID     IN NUMBER,
                      PV_OUT_ERROR  OUT VARCHAR2);
                      
  PROCEDURE Prc_Distribuir_Table_ALL_Dth(PN_ID_PROCESS        IN NUMBER, 
                                        PV_FILTER             IN VARCHAR2,
                                        PN_HILO               IN NUMBER,
                                        PD_FECHA              DATE,
                                        PN_MAXTRX_EXE         NUMBER,
                                        PN_MAX_COMMIT         NUMBER,   
                                        pn_id_bitacora_scp    NUMBER,  
                                        PN_SOSPID             IN NUMBER,                                                                                                                                              
                                        PV_OUT_ERROR          OUT VARCHAR2
                                        );
                                        
  PROCEDURE Prc_Distribuir_Table_Group_Dth(PN_ID_PROCESS        IN NUMBER, 
                                          PV_FILTER             IN VARCHAR2,
                                          PN_HILO               IN NUMBER,
                                          PN_GROUPTREAD         IN NUMBER,
                                          PD_FECHA              DATE,
                                          PN_MAXTRX_EXE         NUMBER,
                                          PN_MAX_COMMIT         NUMBER,   
                                          pn_id_bitacora_scp    NUMBER,  
                                          PN_SOSPID             IN NUMBER,                                                                                                                                              
                                          PV_OUT_ERROR          OUT VARCHAR2
                                          );
  --[8693] JJI - FIN
                                                                                  
     
end RC_Trx_TimeToCash_Send_h;
/
create or replace package body reloj.RC_Trx_TimeToCash_Send_h IS

 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   26/11/2009
  * Modificado: 07/01/2010
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
   * Proósito:
  *     Envio de Transacciones a las colas de despacho. que correspondan a la fecha de procesamiento
  *     verificando el criterio para la distribución equitativa a las tablas, según los parámetros de
  *     entrada y de la configuración en la tabla TTC_RELATION_OBJECTS_S     
  ********************************************************************************************************/  
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8693 RELOJ DE COBRANZA DTH
  FECHA:          03/05/2013
  PROPOSITO:      Creacion del procedimiento RC_HEAD_DTH encargado de distribuir las cuentas dth de 
		              ttc_contractplannigdetails a las tablas DTH_PROCESS_CONTRACT0..4
  *********************************************************************************************************/
  
  CURSOR C_TTC_VIEW_FILTER_ALL(PV_filterfield VARCHAR2) IS
     SELECT FILTER_VALUE Value_Group_Tread
        FROM TTC_VIEW_FILTER_ALL A
     WHERE A.filter  = decode(UPPER(PV_filterfield),'CYCLE','CYCLES',UPPER(PV_filterfield));        

  CURSOR C_Contracts(CN_grouptread NUMBER, PV_filterfield VARCHAR2, PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT SUM(decode(CV_Tipo,'S',1,cpl.valuetrx)) Tot_Contracts
         FROM Ttc_Contractplannigdetails_s cpl 
      WHERE cpl.ch_timeacciondate<=PD_Fecha
             AND  DECODE(PV_filterfield,'PRGCODE',Prgcode, 
                                         DECODE(PV_filterfield, 'CYCLE',TO_NUMBER(AdminCycle), 
                                         TO_NUMBER(Billcycle))) = CN_grouptread
             AND cpl.co_statustrx = GV_STATUS_P;                                        

    CURSOR C_NameTables(CN_Id_Process NUMBER,PV_filterfield VARCHAR2) IS
     SELECT DISTINCT table_valuestring , 0  SumContract 
        FROM ttc_relation_objects_s  
     WHERE Process     = CN_Id_Process
            AND filterfield  = PV_filterfield;

    CURSOR C_GroupThread(CN_Id_Process NUMBER,PV_filterfield VARCHAR2) IS
      SELECT COUNT(DISTINCT TABLE_VALUESTRING) tot_groupThread
        FROM ttc_relation_objects_s  
     WHERE Process       = CN_Id_Process
            AND filterfield    = PV_filterfield;
            
     --[8693]JJI - INICIO       
    --Cursor para la codición de distribuir por contrato
    CURSOR C_CustomerDth_x_Tipo(PN_grouptread NUMBER, PV_filterfield VARCHAR2,  PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT /*+ rule +*/
       TTC_Customers_Count_t(cpl.Customer_id,
                             cpl.Custcode,
                             pay.bank_id,
                             MIN(cpl.co_idtramite),
                             MIN(cpl.co_period),
                             SUM(decode(CV_Tipo, 'S', 1, cpl.valuetrx)))
        FROM Ttc_Contractplannigdetails_s cpl, sysadm.payment_all pay
       WHERE cpl.ch_timeacciondate <= PD_Fecha
         AND cpl.customer_id = pay.customer_id
         AND DECODE(PV_filterfield,'CYCLE',TO_NUMBER(AdminCycle),'BILLCYCLE',
                           TO_NUMBER(Billcycle)) = PN_grouptread
         AND PRGCODE = (select valor from scp.scp_parametros_procesos 
                         where id_parametro='PRGCODE_DTH'
                           and id_proceso='RELOJ_DTH') -- para que cosidere solo las cuentas DTH
         AND pay.act_used = 'X' --La forma de pago relacionada
         AND cpl.co_statustrx = GV_STATUS_P
       GROUP BY cpl.Customer_id, cpl.Custcode, pay.bank_id;
            
     --[8693]JJI - FIN          

--Cursor para la codición de distribuir por contrato
    CURSOR C_Customer_x_Tipo(PN_grouptread NUMBER, PV_filterfield VARCHAR2,  PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT /*+ rule +*/ TTC_Customers_Count_t(cpl.Customer_id,cpl.Custcode,pay.bank_id,MIN(cpl.co_idtramite),MIN(cpl.co_period),SUM(decode(CV_Tipo,'S',1,cpl.valuetrx)))  
         FROM Ttc_Contractplannigdetails_s cpl ,
                      sysadm.payment_all pay
      WHERE cpl.ch_timeacciondate<=PD_Fecha
            AND cpl.customer_id=pay.customer_id
            AND  DECODE(PV_filterfield,'PRGCODE',Prgcode, 
                                         DECODE(PV_filterfield, 'CYCLE',TO_NUMBER(AdminCycle), 
                                         TO_NUMBER(Billcycle))) = PN_grouptread
            AND PRGCODE <> (select valor from scp.scp_parametros_procesos 
                             where id_parametro='PRGCODE_DTH'
                               and id_proceso='RELOJ_DTH') --[8693] JJI - para que no cosidere las cuentas DTH
            AND pay.act_used    = 'X' --La forma de pago relacionada
            AND cpl.co_statustrx = GV_STATUS_P
      GROUP BY cpl.Customer_id,cpl.Custcode,pay.bank_id;


--Cursor obtiene la tabla default configurada
        CURSOR C_Table_Default(CN_Id_Process NUMBER,PN_grouptread NUMBER, PV_filterfield VARCHAR2) IS
            SELECT Table_valueString
               FROM TTC_RELATION_OBJECTS_S A
            WHERE A.PROCESS          = CN_Id_Process
                   AND A.FILTERFIELD  = PV_filterfield
                   AND A.grouptread       = PN_grouptread;

  -- Private type declarations ,constant declarations,variable declarations                       
   i_Customers_Count    ttc_Customers_Count_t_tbk;   
   i_process                      ttc_process_t;
   i_rule_element             ttc_Rule_Element_t;   
   i_ttc_bank_all_t            ttc_bank_all_t;   
   i_Rule_Values_t           ttc_Rule_Values_t;
   i_Plannigstatus_In       ttc_Plannigstatus_In_t;     
   
    PN_SumCountTread        NUMBER;    
    PN_SumCountContracts  NUMBER;
    PV_react_Comment           VARCHAR2(100);
                
--Procedures and Functions  
--**********************************************************************************
        FUNCTION Fct_Plannigstatus_In_t RETURN  TTC_Plannigstatus_In_t as
          doc_in  TTC_Plannigstatus_In_t;
        BEGIN
         SELECT  TTC_Plannigstatus_In_type(pk_id,
                                                                           st_id,
                                                                           st_seqno, 
                                                                           re_orden,
                                                                           st_sdes,
                                                                           st_status_i,
                                                                           st_status_next,
                                                                           st_duration,
                                                                           st_ext_duration ,
                                                                           st_duration_total,
                                                                           st_status_a,
                                                                           st_status_b,
                                                                           st_reason,
                                                                           st_alert,
                                                                           st_nodayalert,
                                                                           st_valid,
                                                                           co_Remark,
                                                                           co_lastmodDate,
                                                                           st_CkeckStatus,
                                                                           st_valid_cond_1, 
                                                                           st_valid_reac, 
                                                                           st_valid_suspe, 
                                                                           st_valid_eval_1, 
                                                                           st_valid_eval_2                                                             
                                                                           )  BULK COLLECT  INTO  doc_in 
              FROM  TTC_VIEW_PLANNIGSTATUS_IN;
          RETURN doc_in;
        END;


--**********************************************************************************
  FUNCTION Fct_ttc_bank_all_t RETURN  ttc_bank_all_t as
    doc_in  ttc_bank_all_t;
  BEGIN
   SELECT ttc_bank_all_type(bank_Id,bankname) 
                   BULK COLLECT  INTO  doc_in FROM  reloj.ttc_view_bank_all_bscs;
    RETURN doc_in;
  END;

--********************************************************************************** 
  FUNCTION Fct_Save_ToWk_Contract(PN_Customer_ID    NUMBER,
                                                                       PV_TABLA               VARCHAR2,
                                                                       PV_co_valid_chr_1 VARCHAR2 DEFAULT NULL,
                                                                       PD_FECHA                DATE
                                                                       )  RETURN VARCHAR2 IS   
     CURSOR C_PLANING_DETAILS(CN_Customer_id NUMBER) IS
         SELECT CUSTOMER_ID,CO_ID,PK_ID,ST_ID,ST_SEQNO,RE_Orden,CUSTCODE,DN_NUM,
                          CO_PERIOD,CO_STATUS_A,CH_STATUS_B,CO_REASON,PRGCODE,ADMINCYCLE,
                          BILLCYCLE,VALUETRX,ORDERBYTRX,CO_IDTRAMITE,CO_REMARK,
                          CO_USERTRX_1,CO_USERTRX_2,CO_USERTRX_3,CO_COMMANDTRX_1,
                          CO_COMMANDTRX_2,CO_COMMANDTRX_3,CO_NOINTENTO,CO_STATUSTRX,
                          CO_REMARKTRX,INSERTIONDATE,LASTMODDATE
         FROM TTC_VIEW_contractplannigdetail A
       WHERE Customer_id=PN_Customer_ID;
          Lv_Error   TTC_Log_InOut.Comment_Run%TYPE; 
          LV_SQL      VARCHAR2(400);
  BEGIN
          Lv_Error:=NULL;
          FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP           
                     LV_SQL:='INSERT /*+ APPEND */ INTO ' ||PV_TABLA||' NOLOGGING  '||' VALUES (
                                       :D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                                       :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
                      EXECUTE IMMEDIATE LV_SQL   
                           USING rc_send.Customer_id,rc_send.Co_id,rc_send.PK_Id,rc_send.ST_Id,rc_send.ST_Seqno,rc_send.RE_Orden,
                                         rc_send.Custcode,0,rc_send.Dn_Num,rc_send.Co_Period,rc_send.Co_Status_A,rc_send.Ch_Status_B,
                                         rc_send.Co_Reason,rc_send.AdminCycle,rc_send.Billcycle,rc_send.ValueTrx,rc_send.OrderByTrx,
                                         rc_send.Co_IdTramite,PV_TABLA,rc_send.Co_UserTrx_1,rc_send.Co_UserTrx_2,rc_send.Co_UserTrx_3,
                                         rc_send.Co_CommandTrx_1,rc_send.Co_CommandTrx_2,rc_send.Co_CommandTrx_3,rc_send.Co_NoIntento,
                                         rc_send.Co_StatusTrx,rc_send.Co_RemarkTrx, PV_co_valid_chr_1,
                                         'X',0, SYSDATE,rc_send.LastModDate; 
                                               
                     UPDATE TTC_ContractPlannigDetails_s A 
                             SET lastmoddate  = SYSDATE, 
                                     Dn_Num         = rc_send.Dn_Num,
                                     co_statustrx   = GV_STATUS_T
                     WHERE A.CH_TIMEACCIONDATE <=PD_FECHA
                           AND  A.CO_STATUSTRX               = GV_STATUS_P
                           AND  A.ST_SEQNO                        =rc_send.ST_SEQNO
                           AND  A.CO_ID                                = rc_send.Co_id
                           AND  A.CUSTOMER_ID                = rc_send.Customer_id;                                     
          END LOOP; --FOR 
          
          RETURN(Lv_Error);          
  EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_Save_ToWk_Contract-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk_Contract-Error General al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
             
  END;
  
  --********************************************************************************** 
  FUNCTION Fct_Save_ToWk_Contract_Dth(PN_Customer_ID    NUMBER,
                                      PV_TABLA          VARCHAR2,
                                      PV_co_valid_chr_1 VARCHAR2 DEFAULT NULL,
                                      PD_FECHA          DATE) RETURN VARCHAR2 IS
  CURSOR C_PLANING_DETAILS(CN_Customer_id NUMBER) IS
    SELECT CUSTOMER_ID,CO_ID,PK_ID,ST_ID,ST_SEQNO,RE_Orden,CUSTCODE,DN_NUM,CO_PERIOD,CO_STATUS_A,
           CH_STATUS_B,CO_REASON,PRGCODE,ADMINCYCLE,BILLCYCLE,VALUETRX,ORDERBYTRX,CO_IDTRAMITE,
           CO_REMARK,CO_USERTRX_1,CO_USERTRX_2,CO_USERTRX_3,CO_COMMANDTRX_1,CO_COMMANDTRX_2,
           CO_COMMANDTRX_3,CO_NOINTENTO,CO_STATUSTRX,CO_REMARKTRX,INSERTIONDATE,LASTMODDATE
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE Customer_id = PN_Customer_ID
     AND ch_timeacciondate <= trunc(SYSDATE)
     AND co_statustrx = 'P';

  Lv_Error TTC_Log_InOut.Comment_Run%TYPE;
  LV_SQL   VARCHAR2(400);
BEGIN
  Lv_Error := NULL;
  FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP
    LV_SQL := 'INSERT /*+ APPEND */ INTO ' || PV_TABLA || ' NOLOGGING  ' ||
              ' VALUES (:D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                        :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
    EXECUTE IMMEDIATE LV_SQL
      USING rc_send.Customer_id, rc_send.Co_id, rc_send.PK_Id, rc_send.ST_Id, rc_send.ST_Seqno, rc_send.RE_Orden, rc_send.Custcode, 0, rc_send.Dn_Num, rc_send.Co_Period, rc_send.Co_Status_A, rc_send.Ch_Status_B, rc_send.Co_Reason, rc_send.AdminCycle, rc_send.Billcycle, rc_send.ValueTrx, rc_send.OrderByTrx, rc_send.Co_IdTramite, PV_TABLA, rc_send.Co_UserTrx_1, rc_send.Co_UserTrx_2, rc_send.Co_UserTrx_3, rc_send.Co_CommandTrx_1, rc_send.Co_CommandTrx_2, rc_send.Co_CommandTrx_3, rc_send.Co_NoIntento, rc_send.Co_StatusTrx, rc_send.Co_RemarkTrx, PV_co_valid_chr_1, 'X', 0, SYSDATE, rc_send.LastModDate;
  
    UPDATE TTC_ContractPlannigDetails_s A
       SET lastmoddate  = SYSDATE,
           Dn_Num       = rc_send.Dn_Num,
           co_statustrx = GV_STATUS_T
     WHERE A.CH_TIMEACCIONDATE <= PD_FECHA
       AND A.CO_STATUSTRX = GV_STATUS_P
       AND A.ST_SEQNO = rc_send.ST_SEQNO
       AND A.CO_ID = rc_send.Co_id
       AND A.CUSTOMER_ID = rc_send.Customer_id;
  END LOOP; --FOR 

  RETURN(Lv_Error);
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    Lv_Error := 'Fct_Save_ToWk_Contract_Dth-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla Dth_Process_Contract#, ' ||substr(SQLERRM, 1, 300);
    RETURN(Lv_Error);
  WHEN OTHERS THEN
    Lv_Error := 'Fct_Save_ToWk_Contract_Dth-Error General al intentar grabar en la tabla Dth_Process_Contract_#, ' ||substr(SQLERRM, 1, 300);
    RETURN(Lv_Error);
  
END;


/****************************************************************************/
 PROCEDURE RC_HEAD (PV_FILTER                IN VARCHAR2,
                                                PN_HILO                   IN NUMBER,
                                                PN_GROUPTREAD  IN NUMBER,
                                                PN_CANTIDAD        IN  NUMBER,
                                                PN_SOSPID            IN NUMBER,
                                                PV_OUT_ERROR      OUT VARCHAR2)    IS 
                  
     LV_MENSAJE                            VARCHAR2(1000);
     LV_STATUS                              VARCHAR(3);
     LE_ERROR_ABORTADO        EXCEPTION;
     LE_ERROR                                EXCEPTION;
     LE_ERROR_OUT                      EXCEPTION;  
     LE_ERROR_PROC_ANT          EXCEPTION;
     Ld_Fecha_Actual                      DATE;     
    -- LB_CHECK                           BOOLEAN;
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=PN_CANTIDAD;
    lv_id_proceso_scp varchar2(100):=GV_DESSHORT;
    lv_referencia_scp varchar2(200):='RC_Trx_TimeToCash_Send.RC_HEAD';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    ---------------------------------------------------------------
     
   BEGIN
   
        Ld_Fecha_Actual:= SYSDATE;
       --Carga Configuración inicial
        i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
        IF i_process.Process IS NULL THEN
              LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
              RAISE LE_ERROR;
        END IF;
        
        --Cargar los datos del Proceso TTC_SEND --> TTC_rule_element
          i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT);
         IF i_rule_element.Process IS NULL THEN
                LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
                RAISE LE_ERROR;
          END IF;

          
/*         LB_CHECK:=RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_PROCESO_CORRECTO(Ld_Fecha_Actual);
         IF NOT LB_CHECK  THEN
              RAISE LE_ERROR_OUT;
         END IF;
*/         
         -- SCP:INICIO
         ----------------------------------------------------------------------------
         -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
         ----------------------------------------------------------------------------
         lv_referencia_scp:='Hilo: '||PN_HILO||' Grouptheard: '||PN_GROUPTREAD||' Filtro: '||PV_FILTER||' '||lv_referencia_scp;
         scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                               lv_referencia_scp,
                                               null,null,null,null,
                                               ln_total_registros_scp,
                                               Gv_unidad_registro_scp,
                                               ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         if ln_error_scp <>0 Then
            LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
            Raise LE_ERROR;
         end if;
         ----------------------------------------------------------------------
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||' Grouptheard: '||PN_GROUPTREAD;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);
         
         ----------------------------------------------------------------------------
         IF nvl(PN_GROUPTREAD,0)=0 THEN             
                    Prc_Distribuir_Table_ALL(i_process.Process,
                                                                     PV_FILTER,
                                                                     PN_HILO,
                                                                     trunc(Ld_Fecha_Actual),
                                                                      i_rule_element.Value_Max_Trx_By_Execution,
                                                                      i_rule_element.Value_Max_TrxCommit,
                                                                      ln_id_bitacora_scp,    
                                                                      PN_SOSPID,                                                                  
                                                                      LV_MENSAJE);
                    IF LV_MENSAJE IS NOT NULL THEN
                             IF LV_MENSAJE ='ABORTADO' THEN
                                    RAISE LE_ERROR_ABORTADO;
                             END IF;
                             RAISE LE_ERROR;
                   END IF;
         ELSE
                    Prc_Distribuir_Table_Group(i_process.Process,
                                                                         PV_FILTER,
                                                                         PN_HILO,
                                                                         PN_GROUPTREAD,
                                                                         LD_FECHA_ACTUAL,
                                                                          i_rule_element.Value_Max_Trx_By_Execution,
                                                                          i_rule_element.Value_Max_TrxCommit,
                                                                          ln_id_bitacora_scp,
                                                                          PN_SOSPID,                                                                          
                                                                          LV_MENSAJE);
                    IF LV_MENSAJE IS NOT NULL THEN
                             IF LV_MENSAJE ='ABORTADO' THEN
                                   RAISE LE_ERROR_ABORTADO;
                             END IF;
                             RAISE LE_ERROR;
                    END IF;
         END IF;      
       RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(GV_DESSHORT,i_process.Process,LD_FECHA_ACTUAL,LV_MENSAJE); 
        IF LV_MENSAJE IS NOT NULL THEN
               RAISE LE_ERROR;
        END IF;
        
       LV_STATUS:='OKI';
       LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD-SUCESSFUL';
       PV_OUT_ERROR:=LV_MENSAJE;
       ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);
         
        
       --SCP:FIN
       ----------------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de finalización de proceso
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       
       ----------------------------------------------------------------------------
       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                         LD_FECHA_ACTUAL,
                                                                                                         SYSDATE,
                                                                                                         LV_STATUS,
                                                                                                         LV_MENSAJE);       
       EXCEPTION 
            WHEN  LE_ERROR_OUT THEN
                LV_STATUS:='ERR';
                LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- SE DEBE EJECUTAR PREVIAMENTE EL PROCESO OUT, ANTES QUE EL PROCESO SEND';
                PV_OUT_ERROR:=LV_MENSAJE;
                ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error de Inicio';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       1,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp); 
                -----------------------------------------------------------------------
                RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                   LD_FECHA_ACTUAL,
                                                                                                                   SYSDATE,
                                                                                                                   LV_STATUS,
                                                                                                                   LV_MENSAJE);

              WHEN  LE_ERROR_PROC_ANT THEN
                 LV_STATUS:='ERR';
                 LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD NO EXISTE PROCESO OUT REQUERIDO';
                 PV_OUT_ERROR:=LV_MENSAJE;
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error de Inicio';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       'Ejecute Primero los 2 procesos OUT(A,D) ',
                                                       2,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                -----------------------------------------------------------------------
                 RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                    LD_FECHA_ACTUAL,
                                                                    SYSDATE,
                                                                    LV_STATUS,
                                                                    LV_MENSAJE);
                   
              WHEN LE_ERROR_ABORTADO THEN
                   LV_STATUS:='ERR';
                   LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '|| LV_MENSAJE;
                   PV_OUT_ERROR:=LV_MENSAJE;
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Proceso Abortado Manualmente por el Usuario';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       2,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                -----------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                      LD_FECHA_ACTUAL,
                                                                                                                      SYSDATE,
                                                                                                                      LV_STATUS,
                                                                                                                      LV_MENSAJE);
              WHEN LE_ERROR THEN
                   LV_STATUS:='ERR';
                  LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '||LV_MENSAJE;
                   PV_OUT_ERROR:=LV_MENSAJE;
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Error en el Procesamiento';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       3,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                   -----------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                      LD_FECHA_ACTUAL,
                                                                                                                      SYSDATE,
                                                                                                                      LV_STATUS,
                                                                                                                      LV_MENSAJE);
                   RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,PN_HILO,PN_GROUPTREAD,LV_MENSAJE);
       
        WHEN OTHERS THEN
             LV_STATUS:='ERR';
             LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 200);
             PV_OUT_ERROR:=LV_MENSAJE;
             ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error en el Procesamiento';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       3,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
              -----------------------------------------------------------------------
             RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                LD_FECHA_ACTUAL,
                                                                                                                SYSDATE,
                                                                                                                LV_STATUS,
                                                                                                                LV_MENSAJE);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,PN_HILO,PN_GROUPTREAD,LV_MENSAJE);
     END RC_HEAD;
     
/**************************************************************************************
8693 JJI
Proceso encargado de distribuir a las tablas DTH_PROCESS_CONTRACT0..4 las cuentas dth
de ttc_contractplannigdetails que tienen que ser suspendidas
**************************************************************************************/ 
   PROCEDURE RC_HEAD_DTH(PV_FILTER     IN VARCHAR2,
                      PN_HILO       IN NUMBER,
                      PN_GROUPTREAD IN NUMBER,
                      PN_CANTIDAD   IN NUMBER,
                      PN_SOSPID     IN NUMBER,
                      PV_OUT_ERROR  OUT VARCHAR2) IS

  LV_MENSAJE VARCHAR2(1000);
  LV_STATUS  VARCHAR(3);
  LE_ERROR_ABORTADO EXCEPTION;
  LE_ERROR EXCEPTION;
  Ld_Fecha_Actual DATE;
  -- LB_CHECK                           BOOLEAN;

  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: Código generado automaticamente. Definición de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp     number := 0;
  ln_total_registros_scp number := PN_CANTIDAD;
  lv_id_proceso_scp      varchar2(100) := GV_DESSHORT_DTH;
  lv_referencia_scp      varchar2(200) := 'RC_Trx_TimeToCash_Send.RC_HEAD_DTH';
  ln_error_scp           number := 0;
  lv_error_scp           varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  ---------------------------------------------------------------

BEGIN

  Ld_Fecha_Actual := SYSDATE;
  --Carga Configuración inicial
  i_process := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_DTH);
  IF i_process.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE LE_ERROR;
  END IF;

  --Cargar los datos del Proceso TTC_SEND_DTH --> TTC_rule_element
  i_rule_element := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_DTH);
  IF i_rule_element.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE LE_ERROR;
  END IF;

  -- SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_referencia_scp := 'Hilo: ' || PN_HILO || ' Grouptheard: ' ||PN_GROUPTREAD || ' Filtro: ' || PV_FILTER || ' ' || lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 Then
    LV_MENSAJE := 'Error en Plataforma SCP, No se pudo iniciar la Bitacora';
    Raise LE_ERROR;
  end if;
  ----------------------------------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO ' || GV_DESSHORT_DTH;
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||' Grouptheard: ' || PN_GROUPTREAD;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  ----------------------------------------------------------------------------
  IF nvl(PN_GROUPTREAD, 0) = 0 THEN
    Prc_Distribuir_Table_ALL_DTH(i_process.Process,
                                 PV_FILTER,
                                 PN_HILO,
                                 trunc(Ld_Fecha_Actual),
                                 i_rule_element.Value_Max_Trx_By_Execution,
                                 i_rule_element.Value_Max_TrxCommit,
                                 ln_id_bitacora_scp,
                                 PN_SOSPID,
                                 LV_MENSAJE);
    IF LV_MENSAJE IS NOT NULL THEN
      IF LV_MENSAJE = 'ABORTADO' THEN
        RAISE LE_ERROR_ABORTADO;
      END IF;
      RAISE LE_ERROR;
    END IF;
  ELSE
    Prc_Distribuir_Table_Group_DTH(i_process.Process,
                                   PV_FILTER,
                                   PN_HILO,
                                   PN_GROUPTREAD,
                                   LD_FECHA_ACTUAL,
                                   i_rule_element.Value_Max_Trx_By_Execution,
                                   i_rule_element.Value_Max_TrxCommit,
                                   ln_id_bitacora_scp,
                                   PN_SOSPID,
                                   LV_MENSAJE);
    IF LV_MENSAJE IS NOT NULL THEN
      IF LV_MENSAJE = 'ABORTADO' THEN
        RAISE LE_ERROR_ABORTADO;
      END IF;
      RAISE LE_ERROR;
    END IF;
  END IF;
  RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(GV_DESSHORT_DTH,
                                                      i_process.Process,
                                                      LD_FECHA_ACTUAL,
                                                      LV_MENSAJE);
  IF LV_MENSAJE IS NOT NULL THEN
    RAISE LE_ERROR;
  END IF;

  LV_STATUS    := 'OKI';
  LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD ||' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH-SUCESSFUL';
  PV_OUT_ERROR := LV_MENSAJE;
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN ' || GV_DESSHORT_DTH;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  --SCP:FIN
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);

  ----------------------------------------------------------------------------
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);
EXCEPTION
  WHEN LE_ERROR_ABORTADO THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH- ' ||
                    LV_MENSAJE;
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso Abortado Manualmente por el Usuario';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          2,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
  WHEN LE_ERROR THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD-Hilo:' || PN_HILO || ' Grouptheard:' ||
                    PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD- ' ||
                    LV_MENSAJE;
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Error en el Procesamiento';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          3,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
    RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,
                                                GV_DESSHORT_DTH,
                                                PN_HILO,
                                                PN_GROUPTREAD,
                                                LV_MENSAJE);
  
  WHEN OTHERS THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH- ' ||
                    ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 200);
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Error en el Procesamiento';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          3,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
    RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,
                                                GV_DESSHORT_DTH,
                                                PN_HILO,
                                                PN_GROUPTREAD,
                                                LV_MENSAJE);
END RC_HEAD_DTH;

     
     /**************************************************************************************
     **************************************************************************************/ 
     
   PROCEDURE Prc_Distribuir_Table_ALL(PN_ID_PROCESS        IN NUMBER, 
                                                                              PV_FILTER                 IN VARCHAR2,
                                                                              PN_HILO                     IN NUMBER,
                                                                              PD_FECHA                   DATE,
                                                                              PN_MAXTRX_EXE      NUMBER,
                                                                              PN_MAX_COMMIT    NUMBER,   
                                                                              pn_id_bitacora_scp   NUMBER,  
                                                                              PN_SOSPID             IN NUMBER,                                                                                                                                              
                                                                              PV_OUT_ERROR        OUT VARCHAR2
                                                                              )   IS
       LV_STATUS                    VARCHAR2(3);
       LV_MENSAJE                  VARCHAR2(300);
       LV_TABLA_DEFAULT   VARCHAR2(100);
       LN_COUNT_COMMIT    NUMBER;
       LB_EXISTE_BCK              BOOLEAN;
       LE_ERROR_MANUAL     EXCEPTION;
       LE_ERROR_VALIDATE   EXCEPTION;
       --------------------------------------------------------
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
        lv_mensaje_tec_scp     varchar2(4000);
        -------------------------------------------------------
       Le_Error_Trx                   EXCEPTION;
       LV_Tipo                           VARCHAR2(1);       
       LN_SALDO_MIN             NUMBER;
       LN_SALDO_MIN_POR   NUMBER;
       Ln_bank_id                      INTEGER;
       Ln_Status_A                     VARCHAR2(5);
       Lv_Status_B                     VARCHAR2(5);
       Lv_valid_suspe               VARCHAR2(5);       
       lb_Reactiva                      BOOLEAN;
       LV_USER_AXIS                 TTC_WORKFLOW_USER_S.US_NAME%TYPE;
       LN_Customer_id             NUMBER;              
       LN_Count_Name             NUMBER;       
       LV_Plan_Telefonia         VARCHAR2(10);
       LV_TABLA                       VARCHAR2(100);    
       LV_STATUS_OUT           VARCHAR2(1);
       LV_Sql_Planing               VARCHAR2(400);
       LV_Sql_PlanDet              VARCHAR2(400);
       LV_REMARK                    VARCHAR2(400);
       
       
    BEGIN
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='   -Inicio procedimiento Prc_Distribuir_Table_ALL';
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||'Grouptheard: ALL';
         scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);         
         ----------------------------------------------------------------------------   
       --Inicializa variables
       LN_COUNT_COMMIT:=0;
       LN_Count_Name:=0;       
       --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por la suma del campo valuetrx
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);          
       LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);          
       PV_react_Comment:=nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');


       --1- Datos requeridos para el calculo del saldo
        LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
        LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
        LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
       
      --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
          OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
        FETCH C_GroupThread INTO PN_SumCountTread;
        CLOSE C_GroupThread;
        IF NVL(PN_SumCountTread,0)<=0  THEN
             PN_SumCountTread:=1;
        END IF;
                  
        --Obtiene la tabla default para asignar los valores
          OPEN C_Table_Default(PN_ID_PROCESS, 0,PV_FILTER);
        FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
        CLOSE C_Table_Default;
        IF LV_TABLA_DEFAULT IS NULL THEN
                PV_OUT_ERROR:='Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract0';
                RAISE LE_ERROR_VALIDATE;
        END IF;   

        --Cargar los datos de la configuración 
        i_ttc_bank_all_t:=Fct_ttc_bank_all_t; 
        i_Plannigstatus_In:=Fct_Plannigstatus_In_t;

        --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
       LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;


        --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
         OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
        FETCH C_NameTables  BULK COLLECT INTO  V_TableName,V_SumCountTread;
        CLOSE C_NameTables;
         --Filtro utilizado para la selección de los customers    
        FOR C_FILTER_ALL IN C_TTC_VIEW_FILTER_ALL(PV_FILTER) LOOP
                --[8693]JJI - Se comenta porque no es necesario
                --Suma los contracts en total para la carga de hoy
                /*OPEN C_Contracts(C_FILTER_ALL.Value_Group_Tread, PV_FILTER, PD_Fecha,LV_Tipo);
                FETCH C_Contracts INTO PN_SumCountContracts;
                CLOSE C_Contracts;*/
                  
                OPEN  C_Customer_x_Tipo(C_FILTER_ALL.Value_Group_Tread, PV_FILTER, PD_FECHA,LV_Tipo);
                LOOP
                FETCH C_Customer_x_Tipo  BULK COLLECT INTO  i_Customers_Count LIMIT PN_MAXTRX_EXE;                                                
                         FOR rc_cust IN (select * from table(cast( i_Customers_Count as ttc_Customers_Count_t_tbk)))  LOOP                                  
                             BEGIN                 
                                   LN_Customer_id:=rc_cust.Customer_id;
                                   --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
                                   --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
                                     BEGIN
                                            SELECT BANK_ID INTO Ln_bank_id   
                                               FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                                            WHERE wkf_cust.BANK_ID=rc_cust.bank_id;                                              
                                     EXCEPTION            
                                           WHEN OTHERS THEN 
                                              Ln_bank_id:=0;
                                     END;     
                                   --
                                   lb_Reactiva:=FALSE;
                                   IF nvl(Ln_bank_id,0)=0 THEN
                                             --***************************************************************
                                             --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
                                             --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
                                             Ln_Status_A:=NULL; Lv_Status_B:=NULL;Lv_valid_suspe:=NULL;
                                             BEGIN
                                                       SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B,Pla.st_valid_suspe   INTO Ln_Status_A,Lv_Status_B,Lv_valid_suspe 
                                                         FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                                       WHERE Pla.pk_id=GV_PK_ID
                                                             AND Pla.st_status_i= rc_cust.co_period;
                                             EXCEPTION            
                                                   WHEN OTHERS THEN 
                                                      Lv_valid_suspe:='N';
                                             END;     
                                             --
                                             --***************************************************************
                                            IF Lv_valid_suspe='Y' THEN  
                                                  --Verifica si la cuenta se encuentra con los pagos para ser reactivada
                                                  lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => rc_cust.custcode,
                                                                                                                                                                           PN_Customer_id =>rc_cust.Customer_id ,
                                                                                                                                                                           Pd_Date => PD_Fecha,                                                                                                                                                           
                                                                                                                                                                           PN_CO_IDTRAMITE => rc_cust.Co_idtramite,                                                                                                                                                           
                                                                                                                                                                           PV_STATUS_P => GV_STATUS_P,
                                                                                                                                                                           PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                                                                                                                                           PV_STATUS_OUT  => LV_STATUS_OUT,
                                                                                                                                                                           PV_Sql_Planing =>LV_Sql_Planing,
                                                                                                                                                                           PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                                                                                                                                           PV_REMARK =>  LV_REMARK,
                                                                                                                                                                           PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                                                                                                                                           PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                                                                                                                                           PV_USER_AXIS => LV_USER_AXIS, 
                                                                                                                                                                           PV_CO_STATUS_A =>  Ln_Status_A, 
                                                                                                                                                                           PV_CO_STATUS_B =>  Lv_Status_B,
                                                                                                                                                                           PV_PERIOD             =>   rc_cust.co_period,
                                                                                                                                                                           PV_REMARKTRX   =>  PV_react_Comment,                                                                                                                                                                     
                                                                                                                                                                           PV_OUT_ERROR =>PV_OUT_ERROR 
                                                                                                                                                                           );
                                            END IF;                                                                                                                         
                                   END IF;
                                   IF NOT lb_Reactiva THEN                                  
                                                  --Distribuye la carga  
                                                  LN_Count_Name:=LN_Count_Name+1;           
                                                  IF LN_Count_Name>PN_SumCountTread  THEN
                                                         LN_Count_Name:=1;
                                                  END IF;
                                                  LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);         
                                                  PV_OUT_ERROR:=Fct_Save_ToWk_Contract(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA);
                                                  IF PV_OUT_ERROR IS NOT NULL THEN     
                                                         RAISE Le_Error_Trx;
                                                  END IF;                                                  
                                    ELSE
                                                 IF LV_Sql_PlanDet IS NOT NULL THEN 
                                                        EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT,LV_REMARK,SYSDATE,rc_cust.Customer_id,GV_STATUS_P;
                                                 END IF;
                                     END IF;  
                                     LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;
                                     IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                                               COMMIT; 
                                              --SCP:AVANCE
                                                -----------------------------------------------------------------------
                                                -- SCP: Código generado automáticamente. Registro de avance de proceso
                                                -----------------------------------------------------------------------
                                                ln_registros_procesados_scp:=ln_registros_procesados_scp+PN_MAX_COMMIT;
                                                scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                                                ----------------------------------------------------------------------- 
                                                RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,PV_FILTER,C_FILTER_ALL.Value_Group_Tread,PN_SOSPID,LB_EXISTE_BCK);
                                                LN_COUNT_COMMIT:=0;
                                       END IF;
                                       IF LB_EXISTE_BCK=FALSE THEN
                                             EXIT;
                                       END IF;
                                       
                            EXCEPTION
                                  WHEN Le_Error_Trx THEN    
                                        --************
                                       --ROLLBACK;
                                        --***********
                                       LV_STATUS:='ERR';
                                       LV_MENSAJE :='Hilo:'||PN_HILO||' Grouptheard:'||C_FILTER_ALL.Value_Group_Tread||' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL- '  || ' ERROR:'||PV_OUT_ERROR||
                                                                   ' para la cuenta -> Customer_id:'||LN_Customer_id;
                                       --SCP:MENSAJE
                                       ----------------------------------------------------------------------
                                       -- SCP: Código generado automáticamente. Registro de mensaje de error
                                       ----------------------------------------------------------------------
                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                       lv_mensaje_apl_scp:='     Prc_Distribuir_Table_ALL-Error en la Distribucion en Customer_id';
                                       scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                             lv_mensaje_apl_scp,
                                                                                                             LV_MENSAJE,
                                                                                                             Null,
                                                                                                             2,Null,
                                                                                                             'Customer_id',
                                                                                                             'Co_id',
                                                                                                             LN_Customer_id,
                                                                                                             0,
                                                                                                             'N',ln_error_scp,lv_error_scp);                                   
                                       ----------------------------------------------------------------------------
                                       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                          PD_FECHA,
                                                                                                                                          SYSDATE,
                                                                                                                                          LV_STATUS,
                                                                                                                                          LV_MENSAJE);
                                  WHEN OTHERS THEN 
                                         --************
                                        --ROLLBACK;
                                         --***********
                                       LV_STATUS:='ERR';
                                       LV_MENSAJE :='Error General en Customer_id--> Hilo:'||PN_HILO||' Grouptheard:'||C_FILTER_ALL.Value_Group_Tread||' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                                                 ' para la cuenta -> Customer_id:'||LN_Customer_id;
                                       --SCP:MENSAJE
                                       ----------------------------------------------------------------------
                                       -- SCP: Código generado automáticamente. Registro de mensaje de error
                                       ----------------------------------------------------------------------
                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                       lv_mensaje_apl_scp:='     Prc_Distribuir_Table_ALL-Error en la Distribucion x Customer_id';
                                       scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                             lv_mensaje_apl_scp,
                                                                                                             LV_MENSAJE,
                                                                                                             Null,
                                                                                                             2,Null,
                                                                                                             'Customer_id',
                                                                                                             'Co_id',
                                                                                                             Ln_Customer_id,
                                                                                                             0,
                                                                                                             'N',ln_error_scp,lv_error_scp);                                   
                                       ----------------------------------------------------------------------------
                                       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                          PD_FECHA,
                                                                                                                                          SYSDATE,
                                                                                                                                          LV_STATUS,
                                                                                                                                          LV_MENSAJE);

                            END;
                         END LOOP; --FOR Customers
                  EXIT WHEN LB_EXISTE_BCK = FALSE;       
                  EXIT WHEN C_Customer_x_Tipo%notfound; 
                END LOOP; --LOOP  Customers
                CLOSE C_Customer_x_Tipo;
                --***********--  
                COMMIT;       
                --***********--
                IF LB_EXISTE_BCK=FALSE THEN
                    EXIT;
               END IF;
 
          END LOOP; --LOOP  Filter_ALL
          PV_OUT_ERROR:=NULL;
          IF LB_EXISTE_BCK=FALSE THEN
              RAISE LE_ERROR_MANUAL;
         END IF;

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:='   -Fin Prc_Distribuir_Table_ALL';
          ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                0,Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------      
       EXCEPTION                    
             WHEN NO_DATA_FOUND THEN
                  PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL >>ADVERTENCIA No existen datos para la consulta:';
                  If ln_registros_error_scp = 0 Then
                    ln_registros_error_scp:=-1;
                 End If;
                 scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

              WHEN LE_ERROR_VALIDATE THEN
                   ROLLBACK;
                   PV_OUT_ERROR:='Prc_Distribuir_Table_ALL-'||PV_OUT_ERROR;
                   If ln_registros_error_scp = 0 Then
                   ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       
              WHEN LE_ERROR_MANUAL THEN                   
                   PV_OUT_ERROR:='ABORTADO';
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                   End If;
                   scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

               WHEN OTHERS THEN
                   ROLLBACK;
                   LV_MENSAJE := 'Prc_Distribuir_Table_ALL- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
                   PV_OUT_ERROR:=LV_MENSAJE;
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                   
    END Prc_Distribuir_Table_ALL;

/**************************************************************************************
**************************************************************************************/ 

   PROCEDURE Prc_Distribuir_Table_ALL_Dth(PN_ID_PROCESS      IN NUMBER,
                                       PV_FILTER          IN VARCHAR2,
                                       PN_HILO            IN NUMBER,
                                       PD_FECHA           DATE,
                                       PN_MAXTRX_EXE      NUMBER,
                                       PN_MAX_COMMIT      NUMBER,
                                       pn_id_bitacora_scp NUMBER,
                                       PN_SOSPID          IN NUMBER,
                                       PV_OUT_ERROR       OUT VARCHAR2) IS
  LV_STATUS        VARCHAR2(3);
  LV_MENSAJE       VARCHAR2(300);
  LV_TABLA_DEFAULT VARCHAR2(100);
  LN_COUNT_COMMIT  NUMBER;
  LB_EXISTE_BCK    BOOLEAN;
  LE_ERROR_MANUAL EXCEPTION;
  LE_ERROR_VALIDATE EXCEPTION;
  --------------------------------------------------------
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  -------------------------------------------------------
  Le_Error_Trx EXCEPTION;
  LV_Tipo           VARCHAR2(1);
  LN_SALDO_MIN      NUMBER;
  LN_SALDO_MIN_POR  NUMBER;
  Ln_bank_id        INTEGER;
  Ln_Status_A       VARCHAR2(5);
  Lv_Status_B       VARCHAR2(5);
  Lv_valid_suspe    VARCHAR2(5);
  lb_Reactiva       BOOLEAN;
  LV_USER_AXIS      TTC_WORKFLOW_USER_S.US_NAME%TYPE;
  LN_Customer_id    NUMBER;
  LN_Count_Name     NUMBER;
  LV_Plan_Telefonia VARCHAR2(10);
  LV_TABLA          VARCHAR2(100);
  LV_STATUS_OUT     VARCHAR2(1);
  LV_Sql_Planing    VARCHAR2(400);
  LV_Sql_PlanDet    VARCHAR2(400);
  LV_REMARK         VARCHAR2(400);

BEGIN
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := '   -Inicio procedimiento Prc_Distribuir_Table_ALL_DTH';
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||'Grouptheard: ALL';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------   
  --Inicializa variables
  LN_COUNT_COMMIT := 0;
  LN_Count_Name   := 0;
  --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por la suma del campo valuetrx
  i_Rule_Values_t := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process, GN_DES_FLAG_ValueTrx);
  LV_Tipo         := nvl(i_Rule_Values_t.Value_Return_ShortString, 'N');

  i_Rule_Values_t  := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);
  PV_react_Comment := nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');

  --1- Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;
  LV_USER_AXIS     := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');

  --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
  OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
  FETCH C_GroupThread INTO PN_SumCountTread;
  CLOSE C_GroupThread;
  IF NVL(PN_SumCountTread, 0) <= 0 THEN
    PN_SumCountTread := 1;
  END IF;

  --Obtiene la tabla default para asignar los valores
  OPEN C_Table_Default(PN_ID_PROCESS, 0, PV_FILTER);
  FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
  CLOSE C_Table_Default;
  IF LV_TABLA_DEFAULT IS NULL THEN
    PV_OUT_ERROR := 'Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract_Dth0';
    RAISE LE_ERROR_VALIDATE;
  END IF;

  --Cargar los datos de la configuración 
  i_ttc_bank_all_t   := Fct_ttc_bank_all_t;
  i_Plannigstatus_In := Fct_Plannigstatus_In_t;

  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia := RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;

  --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
  OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
  FETCH C_NameTables BULK COLLECT INTO V_TableName, V_SumCountTread;
  CLOSE C_NameTables;
  --Filtro utilizado para la selección de los customers    
  FOR C_FILTER_ALL IN C_TTC_VIEW_FILTER_ALL(PV_FILTER) LOOP
  
    OPEN C_CustomerDth_x_Tipo(C_FILTER_ALL.Value_Group_Tread,
                              PV_FILTER,
                              PD_FECHA,
                              LV_Tipo);
    LOOP
      FETCH C_CustomerDth_x_Tipo BULK COLLECT INTO i_Customers_Count LIMIT PN_MAXTRX_EXE;
      FOR rc_cust IN (select * from table(cast(i_Customers_Count as ttc_Customers_Count_t_tbk))) LOOP
        BEGIN
          LN_Customer_id := rc_cust.Customer_id;
          --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
          --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
          BEGIN
            SELECT BANK_ID INTO Ln_bank_id
              FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t)) wkf_cust
             WHERE wkf_cust.BANK_ID = rc_cust.bank_id;
          EXCEPTION
            WHEN OTHERS THEN
              Ln_bank_id := 0;
          END;
          --
          lb_Reactiva := FALSE;
          IF nvl(Ln_bank_id, 0) = 0 THEN
            --***************************************************************
            --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
            --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
            Ln_Status_A    := NULL;
            Lv_Status_B    := NULL;
            Lv_valid_suspe := NULL;
            BEGIN
              SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B, Pla.st_valid_suspe INTO Ln_Status_A, Lv_Status_B, Lv_valid_suspe
                FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
               WHERE Pla.pk_id = GV_PK_ID
                 AND Pla.st_status_i = rc_cust.co_period;
            EXCEPTION
              WHEN OTHERS THEN
                Lv_valid_suspe := 'N';
            END;
            --
            --***************************************************************
            IF Lv_valid_suspe = 'Y' THEN
              --Verifica si la cuenta se encuentra con los pagos para ser reactivada
              lb_Reactiva := RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode       => rc_cust.custcode,
                                                                         PN_Customer_id    => rc_cust.Customer_id,
                                                                         Pd_Date           => PD_Fecha,
                                                                         PN_CO_IDTRAMITE   => rc_cust.Co_idtramite,
                                                                         PV_STATUS_P       => GV_STATUS_P,
                                                                         PV_Plan_Telefonia => LV_Plan_Telefonia,
                                                                         PV_STATUS_OUT     => LV_STATUS_OUT,
                                                                         PV_Sql_Planing    => LV_Sql_Planing,
                                                                         PV_Sql_PlanDet    => LV_Sql_PlanDet,
                                                                         PV_REMARK         => LV_REMARK,
                                                                         PN_SALDO_MIN      => LN_SALDO_MIN,
                                                                         PN_SALDO_MIN_POR  => LN_SALDO_MIN_POR,
                                                                         PV_USER_AXIS      => LV_USER_AXIS,
                                                                         PV_CO_STATUS_A    => Ln_Status_A,
                                                                         PV_CO_STATUS_B    => Lv_Status_B,
                                                                         PV_PERIOD         => rc_cust.co_period,
                                                                         PV_REMARKTRX      => PV_react_Comment,
                                                                         PV_OUT_ERROR      => PV_OUT_ERROR);
            END IF;
          END IF;
          IF NOT lb_Reactiva THEN
            --Distribuye la carga  
            LN_Count_Name := LN_Count_Name + 1;
            IF LN_Count_Name > PN_SumCountTread THEN
              LN_Count_Name := 1;
            END IF;
            
            LV_TABLA     := nvl(V_TableName(LN_Count_Name), LV_TABLA_DEFAULT);
            PV_OUT_ERROR := Fct_Save_ToWk_Contract_Dth(LN_Customer_id,
                                                       LV_TABLA,
                                                       nvl(Lv_valid_suspe,'N'),
                                                       PD_FECHA);
            IF PV_OUT_ERROR IS NOT NULL THEN
              RAISE Le_Error_Trx;
            END IF;
          ELSE
            IF LV_Sql_PlanDet IS NOT NULL THEN
              EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT, LV_REMARK, SYSDATE, rc_cust.Customer_id, GV_STATUS_P;
            END IF;
          END IF;
          LN_COUNT_COMMIT := LN_COUNT_COMMIT + 1;
          IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
            COMMIT;
            --SCP:AVANCE
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            ln_registros_procesados_scp := ln_registros_procesados_scp + PN_MAX_COMMIT;
            scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                  ln_registros_procesados_scp,
                                                  null,
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------- 
            RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                GV_DESSHORT,
                                                                PV_FILTER,
                                                                C_FILTER_ALL.Value_Group_Tread,
                                                                PN_SOSPID,
                                                                LB_EXISTE_BCK);
            LN_COUNT_COMMIT := 0;
          END IF;
          IF LB_EXISTE_BCK = FALSE THEN
            EXIT;
          END IF;
        
        EXCEPTION
          WHEN Le_Error_Trx THEN
            --************
            --ROLLBACK;
            --***********
            LV_STATUS  := 'ERR';
            LV_MENSAJE := 'Hilo:' || PN_HILO || ' Grouptheard:' || C_FILTER_ALL.Value_Group_Tread ||
                          ' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL_Dth- ' ||' ERROR:' || PV_OUT_ERROR ||
                          ' para la cuenta -> Customer_id:' || LN_Customer_id;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '     Prc_Distribuir_Table_ALL_Dth-Error en la Distribucion en Customer_id';
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  LV_MENSAJE,
                                                  Null,
                                                  2,
                                                  Null,
                                                  'Customer_id',
                                                  'Co_id',
                                                  LN_Customer_id,
                                                  0,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
            RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                         PD_FECHA,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
          WHEN OTHERS THEN
            --************
            --ROLLBACK;
            --***********
            LV_STATUS  := 'ERR';
            LV_MENSAJE := 'Error General en Customer_id--> Hilo:' ||PN_HILO || ' Grouptheard:' ||
                          C_FILTER_ALL.Value_Group_Tread || ' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL_Dth- ' ||
                          ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 100) ||' para la cuenta -> Customer_id:' ||
                          LN_Customer_id;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '     Prc_Distribuir_Table_ALL_Dth-Error en la Distribucion x Customer_id';
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  LV_MENSAJE,
                                                  Null,
                                                  2,
                                                  Null,
                                                  'Customer_id',
                                                  'Co_id',
                                                  Ln_Customer_id,
                                                  0,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
            RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                         PD_FECHA,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
          
        END;
      END LOOP; --FOR Customers
      EXIT WHEN LB_EXISTE_BCK = FALSE;
      EXIT WHEN C_CustomerDth_x_Tipo%notfound;
    END LOOP; --LOOP  Customers
    CLOSE C_CustomerDth_x_Tipo;
    --***********--  
    COMMIT;
    --***********--
    IF LB_EXISTE_BCK = FALSE THEN
      EXIT;
    END IF;
  
  END LOOP; --LOOP  Filter_ALL
  PV_OUT_ERROR := NULL;
  IF LB_EXISTE_BCK = FALSE THEN
    RAISE LE_ERROR_MANUAL;
  END IF;

  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp          := '   -Fin Prc_Distribuir_Table_ALL_Dth';
  ln_registros_procesados_scp := ln_registros_procesados_scp + LN_COUNT_COMMIT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'S',
                                        ln_error_scp,
                                        lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------      
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL_Dth >>ADVERTENCIA No existen datos para la consulta:';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_VALIDATE THEN
    ROLLBACK;
    PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL_Dth-' || PV_OUT_ERROR;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_MANUAL THEN
    PV_OUT_ERROR := 'ABORTADO';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN OTHERS THEN
    ROLLBACK;
    LV_MENSAJE   := 'Prc_Distribuir_Table_ALL_Dth- ' || ' ERROR:' ||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 100);
    PV_OUT_ERROR := LV_MENSAJE;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
END Prc_Distribuir_Table_ALL_Dth;

   /**************************************************************************************
**************************************************************************************/ 
   PROCEDURE Prc_Distribuir_Table_Group(PN_ID_PROCESS        IN NUMBER, 
                                                                                  PV_FILTER                 IN VARCHAR2,
                                                                                  PN_HILO                     IN NUMBER,
                                                                                  PN_GROUPTREAD    IN NUMBER,
                                                                                  PD_FECHA                   DATE,
                                                                                  PN_MAXTRX_EXE      NUMBER,
                                                                                  PN_MAX_COMMIT    NUMBER,   
                                                                                  pn_id_bitacora_scp   NUMBER,  
                                                                                  PN_SOSPID        IN NUMBER,                                                                                                                                              
                                                                                  PV_OUT_ERROR        OUT VARCHAR2
                                                                                  )   IS
       LV_STATUS                    VARCHAR2(3);
       LV_MENSAJE                  VARCHAR2(300);
       LV_TABLA_DEFAULT   VARCHAR2(100);
       LN_COUNT_COMMIT    NUMBER;
       LB_EXISTE_BCK              BOOLEAN;
       LE_ERROR_MANUAL     EXCEPTION;
       LE_ERROR_VALIDATE   EXCEPTION;
       --------------------------------------------------------
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
        lv_mensaje_tec_scp     varchar2(4000);
        -------------------------------------------------------
       Le_Error_Trx                   EXCEPTION;
       LV_Tipo                           VARCHAR2(1);
       LN_SALDO_MIN             NUMBER;
       LN_SALDO_MIN_POR   NUMBER;
       Ln_bank_id                      INTEGER;
       Ln_Status_A                     VARCHAR2(5);
       Lv_Status_B                     VARCHAR2(5);   
       Lv_valid_suspe               VARCHAR2(5);
       lb_Reactiva                      BOOLEAN;
       LV_USER_AXIS                 TTC_WORKFLOW_USER_S.US_NAME%TYPE;
       LN_Customer_id             NUMBER;              
       LN_Count_Name             NUMBER;       
       LV_Plan_Telefonia         VARCHAR2(10);
       LV_TABLA                       VARCHAR2(100);    
       LV_STATUS_OUT           VARCHAR2(1);
       LV_Sql_Planing               VARCHAR2(400);
       LV_Sql_PlanDet              VARCHAR2(400);
       LV_REMARK                    VARCHAR2(400);
       
    BEGIN
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='   -Inicio procedimiento Prc_Distribuir_Table_Group';
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||' Grouptheard: '||PN_GROUPTREAD;
         scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);         
         ----------------------------------------------------------------------------   
       --Inicializa variables
       LN_COUNT_COMMIT:=0;
       LN_Count_Name:=0;       
       --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por ña suma del campo valuetrx
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);          
       LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);          
       PV_react_Comment:=nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');

       --1- Datos requeridos para el calculo del saldo
        LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
        LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
        LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
       
      --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
          OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
        FETCH C_GroupThread INTO PN_SumCountTread;
        CLOSE C_GroupThread;
        IF NVL(PN_SumCountTread,0)<=0  THEN
             PN_SumCountTread:=1;
        END IF;
                  
        --Obtiene la tabla default para asignar los valores
          OPEN C_Table_Default(PN_ID_PROCESS, 0,PV_FILTER);
        FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
        CLOSE C_Table_Default;
        IF LV_TABLA_DEFAULT IS NULL THEN
                PV_OUT_ERROR:='Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract0';
                RAISE LE_ERROR_VALIDATE;
        END IF;   

        --Cargar los datos de la configuración 
        i_ttc_bank_all_t:=Fct_ttc_bank_all_t;
        i_Plannigstatus_In:=Fct_Plannigstatus_In_t;
        
        --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
       LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;
       
       --[8693]JJI - Se comenta porque no se usa
       --Suma los contracts en total para la carga de hoy
         /* OPEN C_Contracts(PN_GROUPTREAD, PV_FILTER, PD_Fecha,LV_Tipo);
        FETCH C_Contracts INTO PN_SumCountContracts;
        CLOSE C_Contracts;*/

        --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
         OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
        FETCH C_NameTables  BULK COLLECT INTO  V_TableName,V_SumCountTread;
        CLOSE C_NameTables;

        OPEN  C_Customer_x_Tipo(PN_GROUPTREAD, PV_FILTER, PD_FECHA,LV_Tipo);
        LOOP
        FETCH C_Customer_x_Tipo  BULK COLLECT INTO  i_Customers_Count LIMIT PN_MAXTRX_EXE;                                                
                 FOR rc_cust IN (select * from table(cast( i_Customers_Count as ttc_Customers_Count_t_tbk)))  LOOP                                  
                     BEGIN                 
                           LN_Customer_id:=rc_cust.Customer_id;
                           --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
                           --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
                             BEGIN
                                    SELECT BANK_ID INTO Ln_bank_id   
                                       FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                                    WHERE wkf_cust.BANK_ID=rc_cust.bank_id;                                              
                             EXCEPTION            
                                   WHEN OTHERS THEN 
                                      Ln_bank_id:=0;
                             END;     
                           --
                           lb_Reactiva:=FALSE;
                           IF nvl(Ln_bank_id,0)=0 THEN
                                   --***************************************************************
                                   --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
                                   --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
                                   Ln_Status_A:=NULL; Lv_Status_B:=NULL;Lv_valid_suspe:=NULL;
                                     BEGIN
                                           SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B,Pla.st_valid_suspe   INTO Ln_Status_A,Lv_Status_B,Lv_valid_suspe 
                                             FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                           WHERE Pla.pk_id=GV_PK_ID
                                                 AND Pla.st_status_i= rc_cust.co_period; 
                                     EXCEPTION            
                                           WHEN OTHERS THEN 
                                              Lv_valid_suspe:='N';
                                     END;     
                                     --                                           
                                   --***************************************************************
                                  IF Lv_valid_suspe='Y' THEN  
                                        --Verifica si la cuenta se encuentra con los pagos para ser reactivada                                  
                                        lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => rc_cust.custcode,
                                                                                                                                                                 PN_Customer_id =>rc_cust.Customer_id ,
                                                                                                                                                                 Pd_Date => PD_Fecha,                                                                                                                                                           
                                                                                                                                                                 PN_CO_IDTRAMITE => rc_cust.Co_idtramite,                                                                                                                                                           
                                                                                                                                                                 PV_STATUS_P => GV_STATUS_P,
                                                                                                                                                                 PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                                                                                                                                 PV_STATUS_OUT  => LV_STATUS_OUT,
                                                                                                                                                                 PV_Sql_Planing =>LV_Sql_Planing,
                                                                                                                                                                 PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                                                                                                                                 PV_REMARK =>  LV_REMARK,
                                                                                                                                                                 PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                                                                                                                                 PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                                                                                                                                 PV_USER_AXIS => LV_USER_AXIS, 
                                                                                                                                                                 PV_CO_STATUS_A =>  Ln_Status_A, 
                                                                                                                                                                 PV_CO_STATUS_B =>  Lv_Status_B,
                                                                                                                                                                 PV_PERIOD             =>   rc_cust.co_period,
                                                                                                                                                                 PV_REMARKTRX   =>  PV_react_Comment,                                                                                                                                                                     
                                                                                                                                                                 PV_OUT_ERROR =>PV_OUT_ERROR 
                                                                                                                                                                 );
                                  END IF;                                                                                                                                                                 
                           END IF;
                           IF NOT lb_Reactiva THEN                                  
                                          --Distribuye la carga  
                                          LN_Count_Name:=LN_Count_Name+1;           
                                          IF LN_Count_Name>PN_SumCountTread  THEN
                                                 LN_Count_Name:=1;
                                          END IF;
                                          LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);         
                                          PV_OUT_ERROR:=Fct_Save_ToWk_Contract(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA);
                                          IF PV_OUT_ERROR IS NOT NULL THEN     
                                                 RAISE Le_Error_Trx;
                                          END IF;                                                  
                            ELSE
                                         IF LV_Sql_PlanDet IS NOT NULL THEN 
                                                EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT,LV_REMARK,SYSDATE,rc_cust.Customer_id,GV_STATUS_P;
                                         END IF;
                             END IF;  
                             LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;
                             IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                                       COMMIT; 
                                      --SCP:AVANCE
                                        -----------------------------------------------------------------------
                                        -- SCP: Código generado automáticamente. Registro de avance de proceso
                                        -----------------------------------------------------------------------
                                        ln_registros_procesados_scp:=ln_registros_procesados_scp+PN_MAX_COMMIT;
                                        scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                                        ----------------------------------------------------------------------- 
                                        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,PV_FILTER,PN_GROUPTREAD,PN_SOSPID,LB_EXISTE_BCK);
                                        LN_COUNT_COMMIT:=0;
                               END IF;
                               IF LB_EXISTE_BCK=FALSE THEN
                                     EXIT;
                               END IF;
                               
                    EXCEPTION
                          WHEN Le_Error_Trx THEN    
                               --*************
                               --ROLLBACK;
                                --************ 
                               LV_STATUS:='ERR';
                               LV_MENSAJE :='Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'||PV_OUT_ERROR||
                                                           ' para la cuenta -> Customer_id:'||LN_Customer_id;
                               --SCP:MENSAJE
                               ----------------------------------------------------------------------
                               -- SCP: Código generado automáticamente. Registro de mensaje de error
                               ----------------------------------------------------------------------
                               ln_registros_error_scp:=ln_registros_error_scp+1;
                               lv_mensaje_apl_scp:='     PRC_DISTRIBUIR_TABLE_GROUP-Error en la Distribucion en Customer_id';
                               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                     lv_mensaje_apl_scp,
                                                                                                     LV_MENSAJE,
                                                                                                     Null,
                                                                                                     2,Null,
                                                                                                     'Customer_id',
                                                                                                     'Co_id',
                                                                                                     LN_Customer_id,
                                                                                                     0,
                                                                                                     'N',ln_error_scp,lv_error_scp);                                   
                               ----------------------------------------------------------------------------
                               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                  PD_FECHA,
                                                                                                                                  SYSDATE,
                                                                                                                                  LV_STATUS,
                                                                                                                                  LV_MENSAJE);
                          WHEN OTHERS THEN
                                --************* 
                                --ROLLBACK;
                                 --************
                               LV_STATUS:='ERR';
                               LV_MENSAJE :='Error General en Customer_id--> Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                                         ' para la cuenta -> Customer_id:'||LN_Customer_id;
                               --SCP:MENSAJE
                               ----------------------------------------------------------------------
                               -- SCP: Código generado automáticamente. Registro de mensaje de error
                               ----------------------------------------------------------------------
                               ln_registros_error_scp:=ln_registros_error_scp+1;
                               lv_mensaje_apl_scp:='     PRC_DISTRIBUIR_TABLE_GROUP-Error en la Distribucion x Customer_id';
                               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                     lv_mensaje_apl_scp,
                                                                                                     LV_MENSAJE,
                                                                                                     Null,
                                                                                                     2,Null,
                                                                                                     'Customer_id',
                                                                                                     'Co_id',
                                                                                                     Ln_Customer_id,
                                                                                                     0,
                                                                                                     'N',ln_error_scp,lv_error_scp);                                   
                               ----------------------------------------------------------------------------
                               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                  PD_FECHA,
                                                                                                                                  SYSDATE,
                                                                                                                                  LV_STATUS,
                                                                                                                                  LV_MENSAJE);

                    END;
                 END LOOP; --FOR Customers
                 --***********--
                   COMMIT;
                --***********--     
                 EXIT WHEN LB_EXISTE_BCK = FALSE;       
                 EXIT WHEN C_Customer_x_Tipo%notfound; 
        END LOOP; --LOOP  Customers
        CLOSE C_Customer_x_Tipo;
         IF LB_EXISTE_BCK=FALSE THEN
               RAISE LE_ERROR_MANUAL;
         END IF;
         
          PV_OUT_ERROR:=NULL;

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:='   -Fin Prc_Distribuir_Table_Group';
          ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                0,Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------      
       EXCEPTION                    
             WHEN NO_DATA_FOUND THEN
                  PV_OUT_ERROR := 'Prc_Distribuir_Table_Group >>ADVERTENCIA No existen datos para la consulta:';
                  If ln_registros_error_scp = 0 Then
                    ln_registros_error_scp:=-1;
                 End If;
                 scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

              WHEN LE_ERROR_VALIDATE THEN
                   ROLLBACK;
                   PV_OUT_ERROR:='Prc_Distribuir_Table_Group-'||PV_OUT_ERROR;
                   If ln_registros_error_scp = 0 Then
                   ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       
              WHEN LE_ERROR_MANUAL THEN                   
                   PV_OUT_ERROR:='ABORTADO';
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                   End If;
                   scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

               WHEN OTHERS THEN
                   ROLLBACK;
                   LV_MENSAJE := 'PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
                   PV_OUT_ERROR:=LV_MENSAJE;
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                   
    END Prc_Distribuir_Table_Group;
    
     /**************************************************************************************
**************************************************************************************/ 
    
 PROCEDURE Prc_Distribuir_Table_Group_Dth(PN_ID_PROCESS      IN NUMBER,
                                         PV_FILTER          IN VARCHAR2,
                                         PN_HILO            IN NUMBER,
                                         PN_GROUPTREAD      IN NUMBER,
                                         PD_FECHA           DATE,
                                         PN_MAXTRX_EXE      NUMBER,
                                         PN_MAX_COMMIT      NUMBER,
                                         pn_id_bitacora_scp NUMBER,
                                         PN_SOSPID          IN NUMBER,
                                         PV_OUT_ERROR       OUT VARCHAR2) IS
  LV_STATUS        VARCHAR2(3);
  LV_MENSAJE       VARCHAR2(300);
  LV_TABLA_DEFAULT VARCHAR2(100);
  LN_COUNT_COMMIT  NUMBER;
  LB_EXISTE_BCK    BOOLEAN;
  LE_ERROR_MANUAL EXCEPTION;
  LE_ERROR_VALIDATE EXCEPTION;
  --------------------------------------------------------
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  -------------------------------------------------------
  Le_Error_Trx EXCEPTION;
  LV_Tipo           VARCHAR2(1);
  LN_SALDO_MIN      NUMBER;
  LN_SALDO_MIN_POR  NUMBER;
  Ln_bank_id        INTEGER;
  Ln_Status_A       VARCHAR2(5);
  Lv_Status_B       VARCHAR2(5);
  Lv_valid_suspe    VARCHAR2(5);
  lb_Reactiva       BOOLEAN;
  LV_USER_AXIS      TTC_WORKFLOW_USER_S.US_NAME%TYPE;
  LN_Customer_id    NUMBER;
  LN_Count_Name     NUMBER;
  LV_Plan_Telefonia VARCHAR2(10);
  LV_TABLA          VARCHAR2(100);
  LV_STATUS_OUT     VARCHAR2(1);
  LV_Sql_Planing    VARCHAR2(400);
  LV_Sql_PlanDet    VARCHAR2(400);
  LV_REMARK         VARCHAR2(400);

BEGIN
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := '   -Inicio procedimiento Prc_Distribuir_Table_Group_Dth';
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||' Grouptheard: ' || PN_GROUPTREAD;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------   
  --Inicializa variables
  LN_COUNT_COMMIT := 0;
  LN_Count_Name   := 0;
  --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por ña suma del campo valuetrx
  i_Rule_Values_t := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);
  LV_Tipo         := nvl(i_Rule_Values_t.Value_Return_ShortString, 'N');

  i_Rule_Values_t  := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);
  PV_react_Comment := nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');

  --1- Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;
  LV_USER_AXIS     := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');

  --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
  OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
  FETCH C_GroupThread INTO PN_SumCountTread;
  CLOSE C_GroupThread;
  IF NVL(PN_SumCountTread, 0) <= 0 THEN
    PN_SumCountTread := 1;
  END IF;

  --Obtiene la tabla default para asignar los valores
  OPEN C_Table_Default(PN_ID_PROCESS, 0, PV_FILTER);
  FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
  CLOSE C_Table_Default;
  IF LV_TABLA_DEFAULT IS NULL THEN
    PV_OUT_ERROR := 'Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract_Dth0';
    RAISE LE_ERROR_VALIDATE;
  END IF;

  --Cargar los datos de la configuración 
  i_ttc_bank_all_t   := Fct_ttc_bank_all_t;
  i_Plannigstatus_In := Fct_Plannigstatus_In_t;

  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia := RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;

  --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
  OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
  FETCH C_NameTables BULK COLLECT INTO V_TableName, V_SumCountTread;
  CLOSE C_NameTables;

  OPEN C_CustomerDth_x_Tipo(PN_GROUPTREAD, PV_FILTER, PD_FECHA, LV_Tipo);
  LOOP
    FETCH C_CustomerDth_x_Tipo BULK COLLECT INTO i_Customers_Count LIMIT PN_MAXTRX_EXE;
    FOR rc_cust IN (select * from table(cast(i_Customers_Count as ttc_Customers_Count_t_tbk))) LOOP
      BEGIN
        LN_Customer_id := rc_cust.Customer_id;
        --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
        --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
        BEGIN
          SELECT BANK_ID INTO Ln_bank_id
            FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t)) wkf_cust
           WHERE wkf_cust.BANK_ID = rc_cust.bank_id;
        EXCEPTION
          WHEN OTHERS THEN
            Ln_bank_id := 0;
        END;
        --
        lb_Reactiva := FALSE;
        IF nvl(Ln_bank_id, 0) = 0 THEN
          --***************************************************************
          --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
          --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
          Ln_Status_A    := NULL;
          Lv_Status_B    := NULL;
          Lv_valid_suspe := NULL;
          BEGIN
            SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B, Pla.st_valid_suspe INTO Ln_Status_A, Lv_Status_B, Lv_valid_suspe
              FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
             WHERE Pla.pk_id = GV_PK_ID
               AND Pla.st_status_i = rc_cust.co_period;
          EXCEPTION
            WHEN OTHERS THEN
              Lv_valid_suspe := 'N';
          END;
          --                                           
          --***************************************************************
          IF Lv_valid_suspe = 'Y' THEN
            --Verifica si la cuenta se encuentra con los pagos para ser reactivada                                  
            lb_Reactiva := RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode       => rc_cust.custcode,
                                                                       PN_Customer_id    => rc_cust.Customer_id,
                                                                       Pd_Date           => PD_Fecha,
                                                                       PN_CO_IDTRAMITE   => rc_cust.Co_idtramite,
                                                                       PV_STATUS_P       => GV_STATUS_P,
                                                                       PV_Plan_Telefonia => LV_Plan_Telefonia,
                                                                       PV_STATUS_OUT     => LV_STATUS_OUT,
                                                                       PV_Sql_Planing    => LV_Sql_Planing,
                                                                       PV_Sql_PlanDet    => LV_Sql_PlanDet,
                                                                       PV_REMARK         => LV_REMARK,
                                                                       PN_SALDO_MIN      => LN_SALDO_MIN,
                                                                       PN_SALDO_MIN_POR  => LN_SALDO_MIN_POR,
                                                                       PV_USER_AXIS      => LV_USER_AXIS,
                                                                       PV_CO_STATUS_A    => Ln_Status_A,
                                                                       PV_CO_STATUS_B    => Lv_Status_B,
                                                                       PV_PERIOD         => rc_cust.co_period,
                                                                       PV_REMARKTRX      => PV_react_Comment,
                                                                       PV_OUT_ERROR      => PV_OUT_ERROR);
          END IF;
        END IF;
        IF NOT lb_Reactiva THEN
          --Distribuye la carga  
          LN_Count_Name := LN_Count_Name + 1;
          IF LN_Count_Name > PN_SumCountTread THEN
            LN_Count_Name := 1;
          END IF;
          LV_TABLA     := nvl(V_TableName(LN_Count_Name), LV_TABLA_DEFAULT);
          PV_OUT_ERROR := Fct_Save_ToWk_Contract_Dth(LN_Customer_id,
                                                     LV_TABLA,
                                                     nvl(Lv_valid_suspe, 'N'),
                                                     PD_FECHA);
          IF PV_OUT_ERROR IS NOT NULL THEN
            RAISE Le_Error_Trx;
          END IF;
        ELSE
          IF LV_Sql_PlanDet IS NOT NULL THEN
            EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT, LV_REMARK, SYSDATE, rc_cust.Customer_id, GV_STATUS_P;
          END IF;
        END IF;
        LN_COUNT_COMMIT := LN_COUNT_COMMIT + 1;
        IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
          COMMIT;
          --SCP:AVANCE
          -----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de avance de proceso
          -----------------------------------------------------------------------
          ln_registros_procesados_scp := ln_registros_procesados_scp + PN_MAX_COMMIT;
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                null,
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------- 
          RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                              GV_DESSHORT,
                                                              PV_FILTER,
                                                              PN_GROUPTREAD,
                                                              PN_SOSPID,
                                                              LB_EXISTE_BCK);
          LN_COUNT_COMMIT := 0;
        END IF;
        IF LB_EXISTE_BCK = FALSE THEN
          EXIT;
        END IF;
      
      EXCEPTION
        WHEN Le_Error_Trx THEN
          --*************
          --ROLLBACK;
          --************ 
          LV_STATUS  := 'ERR';
          LV_MENSAJE := 'Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD ||
                        ' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' ||
                        ' ERROR:' || PV_OUT_ERROR ||' para la cuenta -> Customer_id:' || LN_Customer_id;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp := ln_registros_error_scp + 1;
          lv_mensaje_apl_scp     := '     PRC_DISTRIBUIR_TABLE_GROUP_DTH-Error en la Distribucion en Customer_id';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                LV_MENSAJE,
                                                Null,
                                                2,
                                                Null,
                                                'Customer_id',
                                                'Co_id',
                                                LN_Customer_id,
                                                0,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------------
          RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                       PD_FECHA,
                                                       SYSDATE,
                                                       LV_STATUS,
                                                       LV_MENSAJE);
        WHEN OTHERS THEN
          --************* 
          --ROLLBACK;
          --************
          LV_STATUS  := 'ERR';
          LV_MENSAJE := 'Error General en Customer_id--> Hilo:' || PN_HILO ||
                        ' Grouptheard:' || PN_GROUPTREAD ||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' ||
                        ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 100) ||' para la cuenta -> Customer_id:' || LN_Customer_id;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp := ln_registros_error_scp + 1;
          lv_mensaje_apl_scp     := '     PRC_DISTRIBUIR_TABLE_GROUP_DTH-Error en la Distribucion x Customer_id';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                LV_MENSAJE,
                                                Null,
                                                2,
                                                Null,
                                                'Customer_id',
                                                'Co_id',
                                                Ln_Customer_id,
                                                0,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------------
          RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                       PD_FECHA,
                                                       SYSDATE,
                                                       LV_STATUS,
                                                       LV_MENSAJE);
        
      END;
    END LOOP; --FOR Customers
    --***********--
    COMMIT;
    --***********--     
    EXIT WHEN LB_EXISTE_BCK = FALSE;
    EXIT WHEN C_CustomerDth_x_Tipo%notfound;
  END LOOP; --LOOP  Customers
  CLOSE C_CustomerDth_x_Tipo;
  IF LB_EXISTE_BCK = FALSE THEN
    RAISE LE_ERROR_MANUAL;
  END IF;

  PV_OUT_ERROR := NULL;

  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp          := '   -Fin Prc_Distribuir_Table_Group_Dth';
  ln_registros_procesados_scp := ln_registros_procesados_scp + LN_COUNT_COMMIT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'S',
                                        ln_error_scp,
                                        lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------      
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PV_OUT_ERROR := 'Prc_Distribuir_Table_Group_Dth >>ADVERTENCIA No existen datos para la consulta:';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_VALIDATE THEN
    ROLLBACK;
    PV_OUT_ERROR := 'Prc_Distribuir_Table_Group_Dth-' || PV_OUT_ERROR;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_MANUAL THEN
    PV_OUT_ERROR := 'ABORTADO';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN OTHERS THEN
    ROLLBACK;
    LV_MENSAJE   := 'PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' || ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 100);
    PV_OUT_ERROR := LV_MENSAJE;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
END Prc_Distribuir_Table_Group_Dth;
    
     
end RC_Trx_TimeToCash_Send_h;
/
