create or replace package reloj.RC_TRX_QC_ALL IS
 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     CIMA Jose Luis Reinoso
  * Creacion:   24/11/2010
  * Modificado: 15/12/2010
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
   * Pro�sito:
  * 1.- Qc del proceso del reloj de Cobranzas Suspension colas de procesamiento en AXIS.
  * 2.- Qc de las Transacciones por enviar a procesar. De 3 d�as posteriores.
  *       Ejemplo:
              25/11/2010
                Paso a estatus 34 --> 999
                Paso a estatus 80 --> 999
                Paso a estatus 33 --> 999
                Paso a estatus 35 --> 999
             26/11/2010
               Paso a estatus 34 --> 999
                Paso a estatus 80 --> 999
                Paso a estatus 33 --> 999
                Paso a estatus 35 --> 999
             27/11/2010
  ********************************************************************************************************/
/*****************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     CIMA Vanessa Bohorquez M.
  * Creacion:   24/11/2010
  * Modificado: 13/06/2011
  *-------------------------------------------------------------------------------------------------------*/

--VARIABLES GLOBALES
GV_DESSHORT VARCHAR2(14) := 'TTC_QC_SUSP_1';
GV_DESSHORT_P4 VARCHAR2(12) := 'TTC_DISPTC';
GV_DESSHORT_P5 VARCHAR2(12) := 'TTC_SENDRETR';
Gv_unidad_registro_scp varchar2(30):='Resumenes';
gv_value_rule_string_cash varchar2(40):='TTC_FLAG_CUSTOMER_CASH_DIA';
gv_value_rule_string_out varchar2(30):='TTC_FLAG_CUSTOMER_OUT_RELOJ';
GV_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_CURSOR_CUSTCODE';
GV_STATUS_P   VARCHAR2(1) := 'P';

GV_TTC_RANGE_HOUR_FULL     VARCHAR2(30) := 'TTC_RANGE_HOUR_FULL';
GV_TTC_RANGE_HOUR_SEMIFULL VARCHAR2(30) := 'TTC_RANGE_HOUR_SEMIFULL';
GV_TTC_RANGE_HOUR_LOW      VARCHAR2(30) := 'TTC_RANGE_HOUR_LOW';
GV_TTC_MAX_RESTRICT_TIME   VARCHAR2(30) := 'TTC_MAX_RESTRICT_TIME';

gv_hora_ini VARCHAR2(30);
gv_hora_fin VARCHAR2(30);

t_RangeTime_Restrict               ttc_Rule_Values_t;
t_RangeTime_Full_p4                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p4            ttc_Rule_Values_t;
t_RangeTime_Low_p4                 ttc_Rule_Values_t;
t_RangeTime_Select                 ttc_Rule_Values_t;

t_RangeTime_Full_p5                ttc_Rule_Values_t;
t_RangeTime_SemiFull_p5            ttc_Rule_Values_t;
t_RangeTime_Low_p5                 ttc_Rule_Values_t;

i_Process_Programmer_t       ttc_Process_Programmer_t;
GV_DESSHORT_LOW             VARCHAR2(15) := 'TTC_REAC_LOW';
GV_DESSHORT_RETRY_SEMI_FULL VARCHAR2(16) := 'TTC_REAC_SFUL';

gn_secuencia                NUMBER;


procedure Prc_Head (pv_error        OUT VARCHAR2,
                    pn_cant_proc    out NUMBER);

procedure Prc_dispatch(Pv_Desshort             IN  varchar2,
                       pn_id_bitacora_scp      IN  NUMBER,
                       PV_OUT_ERROR            OUT VARCHAR2);

procedure Prc_retry(Pv_Desshort                 IN varchar2,
                       pn_id_bitacora_scp       IN NUMBER,
                       PV_OUT_ERROR             OUT VARCHAR2);

procedure Prc_Notify (pv_error OUT VARCHAR2);

 procedure Prc_Masive(pv_error OUT VARCHAR2);

END RC_TRX_QC_ALL;
/
create or replace package body reloj.RC_TRX_QC_ALL is


FUNCTION Fct_inserta_resumen (pn_id_qc in number,
                              PV_DESSHORT in varchar2,
                              pv_name_qc in varchar2,
                              pv_descripcion_qc in varchar2,
                              pv_estado in varchar2,
                              pv_descripcion_estado in varchar2,
                              pv_num_trans number,
                              pv_recomendacion in varchar2)return number is
   ln_ERROR                number;
   lv_error                VARCHAR2(1000):=pv_recomendacion;
  BEGIN

        scp.sck_api.scp_detalles_bitacora_ins(pn_id_qc,--pn_id_bitacora
                                              PV_DESSHORT,--pv_mensaje_aplicacion
                                              pv_name_qc,--pv_mensaje_tecnico
                                              pv_descripcion_qc,--pv_mensaje_accion
                                              0,--pn_nivel_error
                                              NULL,--pv_cod_aux_1
                                              pv_estado,--pv_cod_aux_2
                                              pv_descripcion_estado,--pv_cod_aux_3
                                              pv_num_trans,--pn_cod_aux_4
                                              0,--pn_cod_aux_5
                                              'N',--pv_notifica
                                              ln_error,--pn_error
                                              lv_error--pv_error
                                              );

       Return 1;
    Exception
        When NO_DATA_FOUND Then
          Return 0;
        When Others Then
          Return 0;
END;

procedure Prc_Head(pv_error OUT VARCHAR2, pn_cant_proc out number) is

  CURSOR C_Trx_In IS
    SELECT TTC_TRX_IN.NEXTVAL FROM dual;
  --VARIABLES
  i_process         ttc_process_t;
  i_process_p4      ttc_process_t;
  i_process_p5      ttc_process_t;
  i_rule_element_p4 ttc_Rule_Element_t;
  i_rule_element_p5 ttc_Rule_Element_t;

  Ln_Seqno        NUMBER;
  Ld_Fecha_Actual DATE;
  LV_MENSAJE      VARCHAR2(1000);
  LE_ERROR EXCEPTION;
  LV_STATUS            VARCHAR(3);
  LV_HourNow           VARCHAR2(20);
  LV_fecha_ejecucion   date;
  LN_NUM_PROCESOS_EJEC NUMBER;
  lv_titulo            VARCHAR2(500);
  lv_proceso_tmp       VARCHAR2(100) := 'X';
  LN_TOPE              NUMBER := 0;
  ln_tope_r            NUMBER := 0;
  ln_error             NUMBER;
  --lv_error                           VARCHAR2(500);
  LV_RECOMENDACION   VARCHAR2(5000);
  lv_recomendacion_r VARCHAR2(4000);
  lv_recomendacion_m VARCHAR2(4000);
  ln_trans_mensajeria  number;
  LN_TRANS_REACTIVA   NUMBER;
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp     number := 0;
  ln_total_registros_scp number;
  lv_id_proceso_scp      varchar2(100) := GV_DESSHORT;
  lv_referencia_scp      varchar2(200) := 'rc_trx_qc_all.RC_HEAD';
  ln_error_scp           number := 0;
  lv_error_scp           varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  ----------------------------------------------------------------
  cursor c_notificacion(PROCESO VARCHAR2) is
    select pp.valor
      from scp.scp_parametros_procesos pp
     where pp.id_proceso = PROCESO --'RC_NOTIFY_RECOMENDACION'
     order by pp.id_parametro;
  rg_notificacion c_notificacion%rowtype;
  ---------------------------------------------------------------

  --cursor para extraer los adatos de la tabla de resumen, segun la ejecucion del QC
  CURSOR c_ttc_suspension(cn_secuencia NUMBER) IS
    select ID_BITACORA     id_qc,
           FECHA_EVENTO    fecha_trans,
           COD_AUX_2       estado,
           MENSAJE_ACCION  descripcion_qc,
           COD_AUX_4       num_trans,
           MENSAJE_TECNICO name_qc
      from scp.scp_DETALLES_BITACORA bp
     where bp.ID_BITACORA = cn_secuencia;

  LV_ASUNTO_MAIL VARCHAR2(100) := 'QC Reloj Suspensi�n/Reactivaci�n y Mensajer�a';

  lv_mensaje_mail LONG;
  lv_estado       VARCHAR2(50);
  lv_nom_proceso  varchar(100);
  i               c_ttc_suspension%ROWTYPE;
  ---VBO  
   Cursor C_estados is
    select to_char(ch_timeacciondate,'dd/mm/yyyy')ch_timeacciondate,co_status_a, co_statustrx , to_char(a.lastmoddate,'dd/mm/yyyy')Lastmoddate ,count(*)cuenta
    from reloj.ttc_contractplannigdetails a 
    where ch_timeacciondate>=trunc(sysdate-5) 
    and ch_timeacciondate<=trunc(sysdate)
    group by ch_timeacciondate,co_status_a, co_statustrx,  to_char(a.lastmoddate,'dd/mm/yyyy') ;
    
    lc_estados c_estados%ROWTYPE;
   --- 
    Cursor C_Reactivaciones is 

    Select (DECODE(DT.DT_ESTATUS_BSCS,
               'P',
               'Ok en BSCS',
               'E',
               'Error en BSCS',
               'Pendiente en BSCS'))DT_ESTATUS_BSCS,
       (DECODE(DT.DT_ESTATUS_AXIS,
              'P',
              'Ok en AXIS',
              'E',
              'Error en AXIS',
              'Pendiente en AXIS'))DT_ESTATUS_AXIS,
       (DECODE(DT.DT_ESTATUS_ESPEJO,
              'P',
              'Ok en ESPEJO',
              'E',
              'Error en ESPEJO',
              'Pendiente en ESPEJO'))DT_ESTATUS_ESPEJO,
       COUNT(*)cuenta1
       FROM RC_DETALLE_TRANSACCIONES@axis DT
       WHERE DT.DT_FECHA >= SYSDATE - 8 / 24
        AND DT.DT_FECHA <= SYSDATE
        AND DT.DT_USUARIO = 'PAYMENTS'
       GROUP BY DT.DT_ESTATUS_BSCS, DT.DT_ESTATUS_AXIS, DT.DT_ESTATUS_ESPEJO;
       
       lc_Reactivaciones C_Reactivaciones%ROWTYPE;
       ln_prioridad      number:=0;--Prioridad:=0 Correo Normal, 1=Correo Prioridad alta
 -----
  CURSOR C_TTC_PROCESADA IS
    SELECT PROCESOS FROM TTC_VIEW_QC_CONTRHISTORY;

  CURSOR C_TTC_REACTIVAR IS
    SELECT DESCRIPCION, STATUS, PROCESOS FROM TTC_VIEW_QC_REACTIVA;
  CURSOR C_TTC_REACTIVAR_C IS
    SELECT COUNT(*) FROM TTC_VIEW_QC_REACTIVA;

  RG_REACTIVAR C_TTC_REACTIVAR%ROWTYPE;

  CURSOR C_TTC_MENSAJERIA IS
    SELECT DESCRIPCION, PROCESOS FROM TTC_VIEW_QC_MENSAJERIA;

  CURSOR C_TTC_MENSAJERIA_C IS
    SELECT COUNT(*) FROM TTC_VIEW_QC_MENSAJERIA;

  RG_MENSAJERIA C_TTC_MENSAJERIA%ROWTYPE;
  ln_error_S    NUMBER;
  lv_error_S    VARCHAR2(1000);
  ln_tope_m     number := 0;
  LE_ERROR_C EXCEPTION;
  lv_mail_origen  varchar2(1000);
  lv_mail_destino varchar2(1000);
  lv_mail_copia   varchar2(1000);
begin
  lv_titulo := '<html><b><u>Resumen de la Ejecuci�n hasta ';
  /********************  DATOS PARA SUSPENSION *************************************/
  open c_notificacion('RC_HEAD_TOPE_S');
  FETCH c_notificacion
    INTO LN_TOPE;
  CLOSE c_notificacion;
  open c_notificacion('RC_HEAD_RECOMENDACION_S');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    LV_RECOMENDACION := LV_RECOMENDACION || rg_notificacion.valor;
  end loop;
  close c_notificacion;
  /********************  DATOS PARA REACTIVACION *************************************/
  open c_notificacion('RC_HEAD_TOPE_R');
  FETCH c_notificacion
    INTO ln_tope_r;
  CLOSE c_notificacion;
  --ln_tope_r:= 5000;
  open c_notificacion('RC_HEAD_RECOMENDACION_R');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    lv_recomendacion_r := lv_recomendacion_r || rg_notificacion.valor;
  end loop;
  close c_notificacion;
  /********************  DATOS PARA MENSAJERIA *************************************/
  open c_notificacion('RC_HEAD_TOPE_M');
  FETCH c_notificacion
    INTO ln_tope_m;
  CLOSE c_notificacion;
  --ln_tope_m:= 5000;
  open c_notificacion('RC_HEAD_RECOMENDACION_M');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    lv_recomendacion_m := lv_recomendacion_m || rg_notificacion.valor;
  end loop;
  close c_notificacion;

  /********************  DATOS PARA ENVIO DE CORREO *********************************/
  open c_notificacion('RC_HEAD_MAILS_O');
  FETCH c_notificacion INTO lv_mail_origen;
  CLOSE c_notificacion;
  open c_notificacion('RC_HEAD_MAILS_D');
  FETCH c_notificacion INTO lv_mail_destino;
  CLOSE c_notificacion;
  open c_notificacion('RC_HEAD_MAILS_CC');
  FETCH c_notificacion INTO lv_mail_copia;
  CLOSE c_notificacion;
  /**********************************************************************************/

  /****   aqui va el encabezado para el scp_bitacora procesos  ***********/

  scp.sck_api.scp_bitacora_procesos_ins('SUSPENSION', --lv_id_proceso_scp,
                                        'MENSAJES DE ESTADO DE SUSPENSION', --lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        0, --ln_total_registros_scp,
                                        'REGISTRO',
                                        gn_secuencia,
                                        ln_error_S,
                                        lv_error_S);
  IF lv_error_S IS NOT NULL THEN
    RAISE LE_ERROR_C;
  END IF;
  /***********************************************************************/

  pn_cant_proc    := 0;
  Ld_Fecha_Actual := SYSDATE;
  SELECT to_date(SYSDATE, 'dd/mm/rrrr') INTO LV_fecha_ejecucion FROM dual;
  SELECT to_char(SYSDATE, 'hh24:mi:ss') INTO LV_HourNow FROM dual;

  -- SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
  ----------------------------------------------------------------------------
  lv_referencia_scp := 'Inicio del proceso de Qc Supension: ' ||
                       lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 Then
    LV_MENSAJE := 'Error en Plataforma SCP, No se pudo iniciar la Bitacora';
    Raise LE_ERROR;
  end if;

  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO ' || GV_DESSHORT;
  lv_mensaje_tec_scp := '';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  --Datos Configuracion Proceso TTC_QC_SUSP_1 - QC Suspension
  i_process := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
  IF i_process.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE LE_ERROR;
  END IF;

  --Fin Datos Configuracion Suspension

  --

  --Datos Configuracion Proceso 4: TTC_DISPTC
  i_process_p4 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P4);
  IF i_process_p4.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE LE_ERROR;
  END IF;

  i_rule_element_p4 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P4);
  IF i_rule_element_p4.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE LE_ERROR;
  END IF;
  --Fin Datos Configuracion P4

  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 4--------------------------
  ----------------------------------------------------------------------------

  --Rangos del proceso 4:TTC_DISPTC
  t_RangeTime_Full_p4     := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,
                                                                             GV_TTC_RANGE_HOUR_FULL);
  t_RangeTime_SemiFull_p4 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,
                                                                             GV_TTC_RANGE_HOUR_SEMIFULL);
  t_RangeTime_Low_p4      := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p4.Process,
                                                                             GV_TTC_RANGE_HOUR_LOW);

  --LLamada al proceso del Job_Dispatch

  rc_trx_qc_all.Prc_dispatch(PV_DESSHORT        => i_process_p4.DesShort,
                                         pn_id_bitacora_scp => ln_id_bitacora_scp,
                                         PV_OUT_ERROR       => LV_MENSAJE);

  IF LV_MENSAJE IS NOT NULL THEN
    Raise LE_ERROR;
  END IF;

  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 4---------------------------------
  --------------------------------------------------------------------------------

  --Datos Configuracion Proceso 5: TTC_SENDRETR
  i_process_p5 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_P5);
  IF i_process_p5.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE LE_ERROR;
  END IF;

  i_rule_element_p5 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_P5);
  IF i_rule_element_p5.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE LE_ERROR;
  END IF;

  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica Proceso 5--------------------------
  ----------------------------------------------------------------------------
  --Rangos del proceso 5
  t_RangeTime_Full_p5     := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,
                                                                             GV_TTC_RANGE_HOUR_FULL);
  t_RangeTime_SemiFull_p5 := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,
                                                                             GV_TTC_RANGE_HOUR_SEMIFULL);
  t_RangeTime_Low_p5      := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process_p5.Process,
                                                                             GV_TTC_RANGE_HOUR_LOW);

  rc_trx_qc_all.Prc_retry(PV_DESSHORT        => i_process_p5.DesShort,
                                      pn_id_bitacora_scp => ln_id_bitacora_scp,
                                      PV_OUT_ERROR       => LV_MENSAJE);

  IF LV_MENSAJE IS NOT NULL THEN
    Raise LE_ERROR;
  END IF;
  lv_mensaje_mail := lv_mensaje_mail || lv_titulo || LV_fecha_ejecucion || ' ' ||
                     LV_HourNow || '</u></b><br><br>';
  /********************************* PROCESO DE SUSPENSION  **************************************************/
  lv_nom_proceso  := 'SUSPENSION';
  lv_mensaje_mail := lv_mensaje_mail ||
                     '<font size="5"><font color="#0404B4"><u><b>' ||
                     lv_nom_proceso ||
                     '</b></u></font size></font color><br><br>';
  --FOR i IN c_ttc_suspension(gn_secuencia) LOOP
  open c_ttc_suspension(gn_secuencia);
  loop
    fetch c_ttc_suspension
      into i;
    exit when c_ttc_suspension%notfound;
    IF lv_proceso_tmp <> i.name_qc THEN
      lv_proceso_tmp  := i.name_qc;
      lv_mensaje_mail := lv_mensaje_mail || '<dir><b>' || i.name_qc ||'</b></dir><br>';
    END IF;
    IF i.estado IS NOT NULL AND I.NUM_TRANS IS NOT NULL THEN
      SELECT decode(i.estado,
                    'F',
                    'Finalizadas',
                    'P',
                    'Pendientes',
                    'R',
                    'Con error de reintento',
                    'E',
                    'Error',
                    'Otros')
        INTO lv_estado
        FROM dual;
      lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||'Transacciones ' || lv_estado || '--> ' ||i.num_trans || '</dir></dir><br>';
      IF i.num_trans > ln_tope AND i.estado = 'P' THEN
        lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||lv_recomendacion || '</dir></dir><br>';
      END IF;
    ELSE
      lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' || '    ' ||I.DESCRIPCION_QC || '</dir></dir><br>';
    END IF;

  END LOOP;

  CLOSE c_ttc_suspension;
  OPEN C_TTC_PROCESADA;
  FETCH C_TTC_PROCESADA
    INTO LN_NUM_PROCESOS_EJEC;
  CLOSE C_TTC_PROCESADA;
  lv_mensaje_mail := lv_mensaje_mail || '<dir>' ||'<b>ENVIO TRX A TABLA MAESTRA BSCS:</b></dir> <dir><dir>Trx pendientes --> ' ||
                     LN_NUM_PROCESOS_EJEC || '</dir></dir><br>';
  IF LN_NUM_PROCESOS_EJEC > 0 THEN
    lv_mensaje_mail := lv_mensaje_mail ||'    <dir><b>Recomendaciones:</b></dir><br>
    <dir><dir>- Verificar la ejecuci�n del proceso Retry Job en control M: RELOJ_NEW_RC_RETRY</dir></dir>';
  END IF;
  /********************************************************************************************************************/
  /*****************************************  PROCESO DE REACTIVACION *************************************************/
  lv_nom_proceso  := 'REACTIVACION';
  lv_mensaje_mail := lv_mensaje_mail ||'<font size="5"><font color="#0404B4"><u><b>' ||
                     lv_nom_proceso ||'</b></u></font size></font color><br><br>';

  open C_TTC_REACTIVAR;
  loop
    fetch C_TTC_REACTIVAR
      into RG_REACTIVAR;
    exit when C_TTC_REACTIVAR%notfound;
    IF lv_proceso_tmp <> RG_REACTIVAR.DESCRIPCION THEN
      lv_proceso_tmp  := RG_REACTIVAR.DESCRIPCION;
      lv_mensaje_mail := lv_mensaje_mail || '<dir><b>' ||RG_REACTIVAR.DESCRIPCION || '</b></dir><br>';
    END IF;
    IF RG_REACTIVAR.STATUS IS NOT NULL AND
       RG_REACTIVAR.PROCESOS IS NOT NULL THEN
      SELECT decode(RG_REACTIVAR.STATUS, 'F', 'Enviadas', 'Pendientes')
        INTO lv_estado
        FROM dual;
      lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||'Transacciones ' || lv_estado || '--> ' ||
                         RG_REACTIVAR.PROCESOS || '</dir></dir><br>';
    ELSE
      lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' || '    ' ||RG_REACTIVAR.DESCRIPCION || '</dir></dir><br>';
    END IF;

  END LOOP;
  OPEN C_TTC_REACTIVAR_C;
  FETCH C_TTC_REACTIVAR_C INTO LN_TRANS_REACTIVA;
  CLOSE C_TTC_REACTIVAR_C;
        IF LN_TRANS_REACTIVA > 0 THEN
          lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||lv_recomendacion_r || '</dir></dir><br>';
          --INI VBO
          lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<table border="1"><tr>'||'<th>'||'ESTATUS_BSCS'||'</th>'||'<th>'||'ESTATUS_AXIS'||'</th>'||'<th>'||'ESTATUS_ESPEJO'||'</th></th></th>'||'<th>'||'COUNT(*)'||'</th>'||'</tr>'||'</dir></dir><br>';
          open c_Reactivaciones;
            loop
              fetch c_Reactivaciones
                into lc_Reactivaciones;
              exit when c_Reactivaciones%notfound;
              if lc_Reactivaciones.DT_ESTATUS_BSCS IS NOT NULL and lc_Reactivaciones.dt_estatus_axis IS NOT NULL and lc_Reactivaciones.DT_ESTATUS_ESPEJO IS NOT NULL then 
                lv_mensaje_mail := lv_mensaje_mail||'<tr>'||'<td>'||lc_Reactivaciones.dt_estatus_bscs||'</td>'||'<td>'||lc_Reactivaciones.dt_estatus_axis||'</td>'||'<td>'||lc_Reactivaciones.DT_ESTATUS_ESPEJO||'</td>'||'<td><br>'||lc_Reactivaciones.Cuenta1 ||'</td></br>'||'</tr>';
              end if; 
             end loop;
             lv_mensaje_mail := lv_mensaje_mail || '</table>'|| '</dir></dir>';
             close c_Reactivaciones;
          --FIN VBO
        ELSE
          lv_mensaje_mail:=lv_mensaje_mail||'<dir><dir> NO EXISTEN DATOS A PRESENTAR</dir></dir> <br>';
      END IF;
  CLOSE C_TTC_REACTIVAR;

  /********************************************************************************************************************/
  /*****************************************  PROCESO DE MENSAJERIA ***************************************************/
  lv_nom_proceso  := 'MENSAJERIA';
  lv_mensaje_mail := lv_mensaje_mail ||'<font size="5"><font color="#0404B4"><u><b>' ||lv_nom_proceso ||
                     '</b></u></font size></font color><br><br>';
  lv_mensaje_mail := lv_mensaje_mail ||'<dir><b>ENVIO CUENTAS A MENSAJERIA:</b></dir><br>';
  open C_TTC_MENSAJERIA;
  loop
    fetch C_TTC_MENSAJERIA
      into RG_MENSAJERIA;
    exit when C_TTC_MENSAJERIA%notfound;
    lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||
                       RG_MENSAJERIA.DESCRIPCION || ' ' ||
                       RG_MENSAJERIA.PROCESOS || '</dir></dir>';
  end loop;
  OPEN C_TTC_MENSAJERIA_C;
  FETCH C_TTC_MENSAJERIA_C into ln_trans_mensajeria;
  close C_TTC_MENSAJERIA_C;
      IF ln_trans_mensajeria > 0 THEN
      lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||
                         lv_recomendacion_m || '</dir></dir><br>';
      else
        lv_mensaje_mail:=lv_mensaje_mail||'<dir><dir> NO EXISTEN DATOS A PRESENTAR</dir></dir></dir><br>';
    END IF;
  /*IF C_TTC_MENSAJERIA%notfound THEN
    lv_mensaje_mail:=lv_mensaje_mail||'<dir><dir> NO EXISTEN DATOS A PRESENTAR</dir></dir> <br>';
  END IF;*/

  close C_TTC_MENSAJERIA;
  /********************************************************************************************************************/ 
  
   ------vbo 
   
   lv_nom_proceso  := 'RESUMEN DE TRANSACCIONES';
   lv_mensaje_mail := lv_mensaje_mail ||'<font size="5"><font color="#0404B4"><u><b>' ||
                     lv_nom_proceso ||'</b></u></font size></font color><br><br>';
                     
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<table border="1"><tr>'||'<th>'||'CH_TIMEACCIONDATE'||'</th>'||'<th>'||'CO_ESTATUS_A'||'</th>'||'<th>'||'CO_ESTATUSTRX'||'</th>'||'<th>'||'LASTMODDATE'||'</th>'||'<th>'||'COUNT(*)'||'</th>'||'</tr>';
   open C_estados;
  loop
    fetch C_estados
      into lc_estados;
    exit when C_estados%notfound;
    if lc_estados.co_status_a IS NOT NULL and lc_estados.co_statustrx IS NOT NULL then
      if lc_estados.co_statustrx = 'E' or lc_estados.co_statustrx = 'P' then
         lv_mensaje_mail := lv_mensaje_mail||'<tr style="color: #FF0000">'||'<td>'||lc_estados.ch_timeacciondate||'</td>'||'<td>'||lc_estados.co_status_a||'</td>'||'<td>'||lc_estados.co_statustrx||'</td>'||'<td>'||lc_estados.lastmoddate||'</td>'||'<td>'||lc_estados.cuenta||'</td>'||'</tr>';
         ln_prioridad:=1;
      else
        lv_mensaje_mail := lv_mensaje_mail||'<tr>'||'<td>'||lc_estados.ch_timeacciondate||'</td>'||'<td>'||lc_estados.co_status_a||'</td>'||'<td>'||lc_estados.co_statustrx||'</td>'||'<td>'||lc_estados.lastmoddate||'</td>'||'<td>'||lc_estados.cuenta||'</td>'||'</tr>';
      end if;
    end if; 
   end loop;
   lv_mensaje_mail := lv_mensaje_mail || '</table>'|| '</dir></dir>';
   close C_estados;  
   
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<table border="1"><tr>'||'<th>'||'ESTATUS'||'</th>'||'<th>'||'EQUIVALENCIA'||'</th>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'P'||'</td>'||'<td>'||'Pendiente de procesar o de ejecutar el Reloj'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'T'||'</td>'||'<td>'||'Transferida a la colas de procesamiento'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'F'||'</td>'||'<td>'||'Finalizada'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'R'||'</td>'||'<td>'||'Reintendo'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'L'||'</td>'||'<td>'||'Pendiente por limite de credito'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'A'||'</td>'||'<td>'||'Cuenta con saldo 0, pendiente que el proceso OUT, las quite del reloj'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'E'||'</td>'||'<td>'||'Error'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>'||'<tr>'||'<td>'||'V'||'</td>'||'<td>'||'Trx pasa a Verificacion, luego de que ha sido reprocesada por 15 dias o mas'||'</td>'||'</tr>'||'</dir></dir>';
   lv_mensaje_mail := lv_mensaje_mail || '</table>'|| '</dir></dir>';
   
   lv_mensaje_mail := lv_mensaje_mail || '</html>';
  --ENVIO DE MAIL
  BEGIN
    RELOJ.RC_ENVIAR_CORREO(pv_servidor     => 'conecel.com',
                           pv_de           => lv_mail_origen, --'reloj_cobranzas@conecel.com',
                           pv_para         => lv_mail_destino, --'jreinoso@cimaconsulting.com.ec',
                           pv_cc           => lv_mail_copia, --'jronquillo@conecel.com;wcruz@cimaconsulting.com.ec;kmite@cimaconsulting.com.ec',
                           pv_asunto       => LV_ASUNTO_MAIL,
                           pv_mensaje      => lv_mensaje_mail,
                           pv_codigoerror  => ln_error,
                           pv_mensajeerror => pv_error,
                           pn_prioridad    => ln_prioridad);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica Proceso 5---------------------------------
  --------------------------------------------------------------------------------

  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN ' || GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  --SCP:FIN
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------
  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_In;
  FETCH C_Trx_In
    INTO Ln_Seqno;
  CLOSE C_Trx_In;
  i_Process_Programmer_t := ttc_Process_Programmer_t(Process     => i_process.Process,
                                                     Seqno       => Ln_Seqno,
                                                     RunDate     => LV_fecha_ejecucion,
                                                     LastModDate => LV_fecha_ejecucion);
  LV_MENSAJE             := RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
  LV_STATUS              := 'OKI';
  LV_MENSAJE             := 'RC_HEAD-SUCESSFUL';
  pv_error               := LV_MENSAJE;
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);

EXCEPTION
  WHEN LE_ERROR_C THEN
    pv_error := 'ERROR EN PROCEDIMIENTO DE CABECERA: ' || lv_error_S;
  WHEN LE_ERROR THEN
    --LV_STATUS    := 'ERR';
    LV_MENSAJE := 'rc_trx_qc_all.Prc_Head:->' || LV_MENSAJE;
    pv_error   := LV_MENSAJE;
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '   Error en el Procesamiento';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          pv_error,
                                          Null,
                                          3,
                                          Null,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------

  WHEN OTHERS THEN
    --LV_STATUS:='ERR';
    LV_MENSAJE := LV_MENSAJE || ' rc_trx_qc_all.Prc_Head:' ||
                  'ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 200);
    pv_error   := LV_MENSAJE;

end;

procedure Prc_dispatch(PV_DESSHORT           IN  Varchar2,
                       pn_id_bitacora_scp    IN  NUMBER,
                       PV_OUT_ERROR          OUT VARCHAR2) is
------------------------------------

--VARIABLES
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
Le_Error_Trx                       EXCEPTION;
ln_check                           number:=0;

--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

--Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
   cursor c_axis_cola is
   select descripcion_estado,valor,estado from reloj.TTC_VIEW_QC_COLAS_AXIS;

   lc_axis_cola                            c_axis_cola%rowtype;

   lv_recomendacion  varchar2(500);
   lb_hay_datos      boolean:=false;


begin

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_dispatch:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_dispatch';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);


  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ----------------------------------------------------------------------------
       --Obtencion de la informaci�n de las colas de Axis lo que se est� procesando el Job Dispatch
       lb_hay_datos:=false;
       begin
            OPEN c_axis_cola;
            loop
                FETCH c_axis_cola into lc_axis_cola;

                exit when c_axis_cola%notfound;

                lv_recomendacion:=NULL;
                IF lc_axis_cola.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF;

                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';

                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION.PRC_DISPATCH'
                                             ,lv_mensaje
                                             ,lc_axis_cola.ESTADO
                                             ,lc_axis_cola.DESCRIPCION_ESTADO
                                             ,lc_axis_cola.VALOR
                                             ,lv_recomendacion);

              lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
              lv_mensaje_tec_scp:='Insercion en TTC_SUSPENSION_RESUMEN Prc_dispatch';
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    Null,
                                                    0,
                                                    Gv_unidad_registro_scp,
                                                    Null,
                                                    Null,
                                                    null,null,
                                                    'N',ln_error_scp,lv_error_scp);

               if ln_check=0 then
                  --hubo error;
                 lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
                 lv_mensaje_tec_scp:='Error al insert en TTC_SUSPENSION_RESUMEN Prc_dispatch';
                 scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp);
                  null;
               end if;

               lb_hay_datos:=true;
            end loop;
            close c_axis_cola;
       end;

       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS PARA EL PROCESO JOBDISPATCH';
          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION.PRC_DISPATCH'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,NULL);
         lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
          lv_mensaje_tec_scp:='Insercion: NO EXISTE DATOS PARA EL PROCESO JOBDISPATCH  en TTC_SUSPENSION_RESUMEN Prc_dispatch';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                2,
                                                Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'N',ln_error_scp,lv_error_scp);
          if ln_check=0 then
             --hubo error;
               lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_dispatch:'||GV_DESSHORT;
                 lv_mensaje_tec_scp:='Error al insert: NO EXISTE DATOS PAARA EL PROCESO JOBDISPATCH en TTC_SUSPENSION_RESUMEN Prc_dispatch';
                 scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp);
             null;
          end if;

       end if;

  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de QC---------------------------
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='rc_trx_qc_all.Prc_dispatch-SUCESSFUL';
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_dispatch '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';

    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);

  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'rc_trx_qc_all.Prc_dispatch- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
end;


procedure Prc_retry(PV_DESSHORT               IN  Varchar2,
                       pn_id_bitacora_scp     IN  NUMBER,
                       PV_OUT_ERROR           OUT VARCHAR2) is
------------------------------------

--VARIABLES
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
Le_Error_Trx                       EXCEPTION;
ln_check                           number:=0;

--SCP:VARIABLES
---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------

 --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
   cursor c_axis_cola_reintentos is
   select descripcion_estado,valor,estado from RELOJ.TTC_VIEW_QC_COLA_REIN_AXIS;

   lc_axis_cola_reintentos                 c_axis_cola_reintentos%rowtype;

   lv_recomendacion  varchar2(500);
   lb_hay_datos      boolean:=false;


begin

  --pn_cant_proc:=0;
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_retry:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_retry';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);


  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de QC -----------------------------
  ----------------------------------------------------------------------------
  --Obtengo la informaci�n de la cola de reintentos, del proceso Job Retry
       lb_hay_datos:=false;
       begin
            OPEN c_axis_cola_reintentos;
            loop
                FETCH c_axis_cola_reintentos into lc_axis_cola_reintentos;

                exit when c_axis_cola_reintentos%notfound;

                lv_recomendacion:=NULL;
                IF lc_axis_cola_reintentos.estado <> 'F' THEN
                   lv_recomendacion:='Verficar Ejecuci�n Proceso BSCS';
                END IF;

                lv_mensaje:='Obtencion de la informaci�n de Transacciones de BSCS finalizadas en la ejecuci�n de hoy';

                ln_check:=Fct_inserta_resumen(gn_secuencia
                                             ,PV_DESSHORT
                                             ,'QC_SUSPENSION.PRC_RETRY'
                                             ,lv_mensaje
                                             ,lc_axis_cola_reintentos.ESTADO
                                             ,lc_axis_cola_reintentos.DESCRIPCION_ESTADO
                                             ,lc_axis_cola_reintentos.VALOR
                                             ,lv_recomendacion);
               lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
               lv_mensaje_tec_scp:='Insercion en TTC_SUSPENSION_RESUMEN Prc_retry';
               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    Null,
                                                    0,
                                                    Gv_unidad_registro_scp,
                                                    Null,
                                                    Null,
                                                    null,null,
                                                    'N',ln_error_scp,lv_error_scp);

                if ln_check=0 then
                  --hubo error;
                  lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
                  lv_mensaje_tec_scp:='Error al insert en TTC_SUSPENSION_RESUMEN Prc_retry';
                  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                      lv_mensaje_apl_scp,
                                                      lv_mensaje_tec_scp,
                                                      Null,
                                                      3,
                                                      Gv_unidad_registro_scp,
                                                      Null,
                                                      Null,
                                                      null,null,
                                                      'N',ln_error_scp,lv_error_scp);
                  null;
                end if;

                lb_hay_datos:=true;
            end loop;
            close c_axis_cola_reintentos;
       end;

       if not lb_hay_datos then
          lv_mensaje:='NO EXISTE DATOS PARA EL PROCESO JOBRETRY';

          ln_check:=Fct_inserta_resumen(gn_secuencia
                                        ,PV_DESSHORT
                                        ,'QC_SUSPENSION.PRC_RETRY'
                                        ,lv_mensaje
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,NULL);
          lv_mensaje_apl_scp:='REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
          lv_mensaje_tec_scp:='Insercion: NO EXISTE DATOS PARA EL PROCESO JOBRETRY  en TTC_SUSPENSION_RESUMEN Prc_retry';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                2,
                                                Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'N',ln_error_scp,lv_error_scp);
          if ln_check=0 then
             --hubo error;
             lv_mensaje_apl_scp:='ERROR - REGISTRO TTC_SUSPENSION_RESUMEN Prc_retry:'||GV_DESSHORT;
             lv_mensaje_tec_scp:='Error al insert: NO EXISTE DATOS PARA EL PROCESO JOBRETRY en TTC_SUSPENSION_RESUMEN Prc_retry';
             scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  Null,
                                                  3,
                                                  Gv_unidad_registro_scp,
                                                  Null,
                                                  Null,
                                                  null,null,
                                                  'N',ln_error_scp,lv_error_scp);
             null;
          end if;

       end if;


  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de ---------------------------
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='rc_trx_qc_all.Prc_retry-SUCESSFUL';
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_retry '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';

    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);

  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'rc_trx_qc_all.Prc_retry- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
end;

procedure Prc_Notify(pv_error OUT VARCHAR2) IS
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp     number := 0;
  ln_total_registros_scp number;
  lv_id_proceso_scp      varchar2(100) := GV_DESSHORT;
  lv_referencia_scp      varchar2(200) := 'rc_trx_qc_all.RC_NOTIFY';
  ln_error_scp           number := 0;
  lv_error_scp           varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  --ln_registros_procesados_scp        number := 0;
  ln_registros_error_scp number := 0;
  ---------------------------------------------------------------
  ln_tope1          NUMBER;
  ln_tope2          NUMBER;
  lv_recomendacion1 VARCHAR2(5000);
  lv_recomendacion2 VARCHAR2(5000);
  ln_error          NUMBER;
  ---------------------------------------------------------------
  CURSOR C_PROCESOS IS
    select fecha, descripcion_estado, procesos
      from ttc_view_qc_status_procesos;
  RG_PROCESOS C_PROCESOS%ROWTYPE;

  ----------------------------------------------------------------
  CURSOR C_MENSAJE IS
    SELECT FECHA, DESCRIPCION, TOTAL FROM AXIS_VIEW_RC_QC_ALL;
  RG_MENSAJE C_MENSAJE%ROWTYPE;
  ----------------------------------------------------------------
  cursor c_notificacion(PROCESO VARCHAR2) is
    select pp.valor
      from scp.scp_parametros_procesos pp
     where pp.id_proceso = PROCESO --'RC_NOTIFY_RECOMENDACION'
     order by pp.id_parametro;
  rg_notificacion c_notificacion%rowtype;

  LV_MENSAJE VARCHAR2(500);
  LE_ERROR EXCEPTION;
  LV_ASUNTO_MAIL  VARCHAR2(3000);
  lv_mensaje_mail VARCHAR2(9000);
  lv_fecha_temp   DATE := to_date('01/01/1900', 'dd/mm/yyyy');
  lv_titulo1      VARCHAR2(500) := '<html><font size="6"><font color="#0404B4"><u><b>Proceso Suspensi&oacute;n</u></b></font color></font size><br><br><br><dir><u><b>Resumen de las transacciones por Procesar de los Proximos 5 d�as</b></u></dir><br><br>';
  lv_titulo2      VARCHAR2(500) := '<font size="6"><font color="#0404B4"><u><b>Proceso Mensajer&iacute;a</u></b></font color></font size><br><br><br><dir><u><b>Resumen de las transacciones por Procesar de los Proximos 5 d�as</b></u></dir><br>';
  lv_mail_origen  varchar2(1000);
  lv_mail_destino varchar2(1000);
  lv_mail_copia   varchar2(1000);
  lb_found        BOOLEAN:=FALSE;
BEGIN
  LV_ASUNTO_MAIL := 'Proceso Suspensi�n y Mensajer�a: Transacciones por Procesar';
  /******************** DATOS PARA LA SUSPENSION  ***********************/
  open c_notificacion('RC_NOTIFY_TOPE');
  FETCH c_notificacion
    INTO ln_tope1;
  CLOSE c_notificacion;
  open c_notificacion('RC_NOTIFY_RECOMENDACION');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    lv_recomendacion1 := lv_recomendacion1 || rg_notificacion.valor;
  end loop;
  close c_notificacion;
  /**********************  DATOS PARA MENSAJERIA *************************/
  open c_notificacion('RC_NOTIFY_TOPE_M');
  FETCH c_notificacion
    INTO ln_tope2;
  CLOSE c_notificacion;

  open c_notificacion('RC_NOTIFY_RECOM_MENSAJERIA');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    lv_recomendacion2 := lv_recomendacion2 || rg_notificacion.valor;
  end loop;
  close c_notificacion;
  /********************  DATOS PARA ENVIO DE CORREO *********************************/
  open c_notificacion('RC_NOTIFY_MAILS_O');
  FETCH c_notificacion
    INTO lv_mail_origen;
  CLOSE c_notificacion;
  open c_notificacion('RC_NOTIFY_MAILS_D');
  FETCH c_notificacion
    INTO lv_mail_destino;
  CLOSE c_notificacion;
  open c_notificacion('RC_NOTIFY_MAILS_CC');
  FETCH c_notificacion
    INTO lv_mail_copia;
  CLOSE c_notificacion;
  /**********************************************************************************/

  -- SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
  ----------------------------------------------------------------------------
  lv_referencia_scp := 'Inicio del proceso de Qc Supension: ' ||
                       lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 Then
    LV_MENSAJE := 'Error en Plataforma SCP, No se pudo iniciar la Bitacora';
    Raise LE_ERROR;
  end if;

  -----------------------------------------------
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO Prc_Notify:' || GV_DESSHORT;
  lv_mensaje_tec_scp := 'Proceso Prc_Notify';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  -------------------------------------------------------------------------------
  --   PARA EL PROCESO DE SUSPENSION
  ----------------------------------------------------------------------------
  lv_mensaje_mail := chr(13) || chr(10) || lv_titulo1 || chr(13) || chr(10);
  open C_PROCESOS;
  loop
    fetch C_PROCESOS
      into RG_PROCESOS;
    exit when C_PROCESOS%notfound;
    IF lv_fecha_temp <> RG_PROCESOS.FECHA THEN
      lv_fecha_temp   := RG_PROCESOS.FECHA;
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||
                         '<dir><u>FECHA:' || RG_PROCESOS.FECHA ||
                         '</u></dir><br><br>';
    END IF;

    IF RG_PROCESOS.PROCESOS > ln_tope1 THEN
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||
                         ' <dir>&nbsp<font color="#DF0101">  ' ||
                         RG_PROCESOS.DESCRIPCION_ESTADO || ' ' || '-->' ||
                         RG_PROCESOS.PROCESOS || '</font color></dir>';
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||
                         ' <dir>&nbsp<font color="#0404B4">  ' ||
                         lv_recomendacion1 || '</font color></dir><br>';
    ELSE
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||
                         '  <dir>&nbsp ' || RG_PROCESOS.DESCRIPCION_ESTADO || ' ' ||
                         '-->' || RG_PROCESOS.PROCESOS || '</dir>';
    END IF;
  end loop;
  close C_PROCESOS;
  --------------------------------------------------------------------------------
  --  PARA EL PROCESO DE MENSAJERIA
  --------------------------------------------------------------------------------
  lv_fecha_temp   := to_date('01/01/1900', 'dd/mm/yyyy');
  lv_mensaje_mail := lv_mensaje_mail || lv_titulo2;
  open C_MENSAJE;
  loop
    fetch C_MENSAJE
      into RG_MENSAJE;
    exit when C_MENSAJE%notfound;
    IF lv_fecha_temp <> RG_MENSAJE.FECHA THEN
      lv_fecha_temp   := RG_MENSAJE.FECHA;
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||
                         '<br><dir><u>FECHA:' || RG_MENSAJE.FECHA ||
                         ', se aplica el ' ||
                         to_char(to_date(RG_MENSAJE.FECHA, 'dd/mm/yyyy') + 1) ||
                         '</u></dir><br><br>';
    END IF;
    IF RG_MENSAJE.TOTAL > ln_tope2 THEN
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||' <dir>&nbsp<font color="#DF0101">' ||
                         RG_MENSAJE.DESCRIPCION || ' ' || '-->' ||RG_MENSAJE.TOTAL || '</font color></dir>';
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||' <dir>&nbsp<font color="#0404B4">' ||
                         lv_recomendacion2 || '</font color></dir><br>';
    ELSE
      lv_mensaje_mail := lv_mensaje_mail || chr(13) || chr(10) ||' <dir>&nbsp ' || RG_MENSAJE.DESCRIPCION || ' ' ||
                         '-->' || RG_MENSAJE.TOTAL || '</dir>';
    END IF;
  end loop;
  CLOSE C_MENSAJE;

  OPEN C_MENSAJE;
  FETCH C_MENSAJE INTO RG_MENSAJE;
  lb_found:= C_MENSAJE%FOUND;
  CLOSE C_MENSAJE;
  IF NOT lb_found THEN
    lv_mensaje_mail := lv_mensaje_mail ||
                       '<dir><dir>NO EXISTEN DATOS DE MENSAJERIA</dir></dir>';
  END IF;
  --------------------------------------------------------------------------------
  lv_mensaje_mail := lv_mensaje_mail || '</html>';

  BEGIN
    RELOJ.RC_ENVIAR_CORREO(pv_servidor     => 'conecel.com',
                           pv_de           => lv_mail_origen, --'reloj_cobranzas@conecel.com',
                           pv_para         => lv_mail_destino, --'jreinoso@cimaconsulting.com.ec',
                           pv_cc           => lv_mail_copia, --'jreinoso@cimaconsulting.com.ec',
                           pv_asunto       => LV_ASUNTO_MAIL,
                           pv_mensaje      => lv_mensaje_mail,
                           pv_codigoerror  => ln_error,
                           pv_mensajeerror => pv_error);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    LV_MENSAJE := 'rc_trx_qc_all.Prc_Notify- ' || ' ERROR:' ||
                  SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 100);
    pv_error   := LV_MENSAJE;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);

END Prc_Notify;

/*
Procedimiento que verifica si existen transacciones pendientes en la CM_COLA_MASIVO_BSCS
*/
procedure Prc_Masive(pv_error OUT VARCHAR2) IS
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp     number := 0;
  ln_total_registros_scp number;
  lv_id_proceso_scp      varchar2(100) := 'TTC_QC_CMS_MASIVO';--GV_DESSHORT;
  lv_referencia_scp      varchar2(200) := 'rc_trx_qc_all.RC_MASIVE';
  ln_error_scp           number := 0;
  lv_error_scp           varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  --ln_registros_procesados_scp        number := 0;
  --ln_registros_error_scp number := 0;
  ---------------------------------------------------------------
  ---------------------------------------------------------------
  ln_tope1          NUMBER;
  lv_recomendacion1 VARCHAR2(5000);
  ln_error          NUMBER;
  ---------------------------------------------------------------
  CURSOR C_PROCESOS IS
    select estado, procesos
      from TTC_VIEW_QC_MASIVE;
  RG_PROCESOS C_PROCESOS%ROWTYPE;
  ----------------------------------------------------------------
  ---------------------------------------------------------------
  CURSOR C_PROCESOS_C IS
    select count(*)
      from TTC_VIEW_QC_MASIVE;

  ----------------------------------------------------------------
  cursor c_notificacion(PROCESO VARCHAR2) is
    select pp.valor
      from scp.scp_parametros_procesos pp
     where pp.id_proceso = PROCESO --'RC_NOTIFY_RECOMENDACION'
     order by pp.id_parametro;
  rg_notificacion c_notificacion%rowtype;

  --------SE CUENTA CUANTAS BITACORAS HAY EN EL DIA DE HOT
  CURSOR C_CONT_BIT_MASIVO IS
    SELECT COUNT(BP.ID_BITACORA)
      FROM SCP.SCP_BITACORA_PROCESOS BP
     WHERE ID_PROCESO = 'TTC_QC_CMS_MASIVO'
       AND TRUNC(FECHA_INICIO) = TRUNC(SYSDATE)
       AND ROWNUM < 3
     ORDER BY ID_BITACORA DESC;

  LN_CONT_BIT_MASIVO      NUMBER:=0;

  --------PARA EXTRAER LA ULTIMA EJECUCION DEL SCP DEL DIA ACTUAL-----
  CURSOR C_BIT_MASIVO IS
   SELECT BP.ID_BITACORA --MAX(BP.ID_BITACORA) ID_BITACORA
     FROM SCP.SCP_BITACORA_PROCESOS BP
    WHERE ID_PROCESO = 'TTC_QC_CMS_MASIVO'
      AND TRUNC(FECHA_INICIO) = TRUNC(SYSDATE)
      AND ROWNUM < 3
      ORDER BY id_bitacora DESC;

  --------VALOR DE LA ULTIMA BITACORA
  CURSOR C_TX_PENDIENTES(CN_ID_BITACORA NUMBER) IS
   SELECT DB.COD_AUX_5
     FROM SCP.SCP_DETALLES_BITACORA DB
    WHERE ID_BITACORA = CN_ID_BITACORA
      AND MENSAJE_APLICACION = 'TTC_QC_CMS_MASIVO'
      AND MENSAJE_TECNICO = 'PENDIENTES';

  LN_VALOR_TX_PENDIENTES      NUMBER:=0;


  LE_ERROR EXCEPTION;
  LV_ASUNTO_MAIL  VARCHAR2(3000);
  lv_mensaje_mail VARCHAR2(9000);
  --lv_fecha_temp   DATE := to_date('01/01/1900', 'dd/mm/yyyy');
  lv_titulo1      VARCHAR2(500) ;
  lv_mail_origen  varchar2(1000);
  lv_mail_destino varchar2(1000);
  lv_mail_copia   varchar2(1000);
  lv_estado       varchar2(100);
  ln_num_procesos number:=0;
  ln_total_procesos number :=0;
  lb_found        BOOLEAN:=FALSE;
  lb_permite_correo BOOLEAN:=FALSE;
--:= '<html><font size="6"><font color="#0404B4"><u><b>Proceso Suspensi&oacute;n</u></b></font color></font size><br><br><br><dir><u><b>Resumen de las transacciones por Procesar de los Proximos 5 d�as</b></u></dir><br><br>'
  LF_FILE            Sys.Utl_File.File_Type;
  LV_LINEA           VARCHAR2(2000);
  Lb_Nomaslineas     BOOLEAN := FALSE;
  LV_RUTA            VARCHAR2(200);

  LN_TOTAL_CMS            NUMBER;
  LV_FECHA                VARCHAR2(20);
  LV_FECHA_MIN            VARCHAR2(20);
  LV_FECHA_MAX            VARCHAR2(20);
  LI_CONTADOR             PLS_INTEGER:=0;
  LI_CONTADOR_COLA        PLS_INTEGER;
BEGIN

  --SE CUENTA CUANTAS BITACORAS HAY EN EL DIA DE HOY
  open C_CONT_BIT_MASIVO;
  FETCH C_CONT_BIT_MASIVO
    INTO LN_CONT_BIT_MASIVO;
  CLOSE C_CONT_BIT_MASIVO;

  IF LN_CONT_BIT_MASIVO <> 2 THEN
     lb_permite_correo := TRUE;
  ELSE
     --VERIFICO QUE LAS DOS BITACORAS EN EL DETALLE NO SEAN PENDIENTE 0,
     --SI ES ASI ENTONCES NO DEBE ENVIAR CORREO
     FOR I IN C_BIT_MASIVO LOOP
         open C_TX_PENDIENTES(I.ID_BITACORA);
         FETCH C_TX_PENDIENTES
           INTO LN_VALOR_TX_PENDIENTES;
         lb_found:= C_TX_PENDIENTES%FOUND;
         CLOSE C_TX_PENDIENTES;
         IF LB_FOUND AND LN_VALOR_TX_PENDIENTES > 0 THEN
            lb_permite_correo := TRUE;
            EXIT;
         ELSE
            lb_permite_correo := FALSE;
         END IF;

     END LOOP;
  END IF;

  /******************** DATOS PARA CMS_MASIVE  ***********************/
  open c_notificacion('RC_MASIVE_TOPE');
  FETCH c_notificacion
    INTO ln_tope1;
  CLOSE c_notificacion;

    open c_notificacion('RC_MASIVE_TITULO');
  FETCH c_notificacion
    INTO lv_titulo1;
  CLOSE c_notificacion;

  open c_notificacion('RC_MASIVE_RECOMENDACION');
  loop
    fetch c_notificacion
      into rg_notificacion;
    exit when c_notificacion%notfound;
    lv_recomendacion1 := lv_recomendacion1 || rg_notificacion.valor;
  end loop;
  close c_notificacion;


   /********************  DATOS PARA ENVIO DE CORREO *********************************/
  open c_notificacion('RC_MASIVE_MAILS_O');
  FETCH c_notificacion
    INTO lv_mail_origen;
  CLOSE c_notificacion;
  open c_notificacion('RC_MASIVE_MAILS_D');
  FETCH c_notificacion
    INTO lv_mail_destino;
  CLOSE c_notificacion;
  open c_notificacion('RC_MASIVE_MAILS_CC');
  FETCH c_notificacion
    INTO lv_mail_copia;
  CLOSE c_notificacion;
  /**********************************************************************************/
------------------------------------------------------------------------------
LV_ASUNTO_MAIL := 'Proceso CMS MASIVO: Transacciones por Procesar';
lv_mensaje_mail:= lv_titulo1;
 -- SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
  ----------------------------------------------------------------------------
  lv_referencia_scp := 'Inicio del proceso de Qc CMS MASIVO: ' ||
                       lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 Then
    PV_ERROR := 'Error en Plataforma SCP, No se pudo iniciar la Bitacora';
    Raise LE_ERROR;
  end if;

  -----------------------------------------------
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO Prc_Masive:' || 'TTC_QC_CMS_MASIVO';
  lv_mensaje_tec_scp := 'Proceso Prc_Masive';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  -------------------------------------------------------------------------------
/**************************************************************************************/
/*        proceso de cms_masivo  parte primera                                         */
/**************************************************************************************/
  open C_PROCESOS_C;
  fetch C_PROCESOS_C into ln_total_procesos;
  close C_PROCESOS_C;
  if ln_total_procesos = 0 then
    Lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||'NO EXISTEN TRANSACCIONES POR PROCESAR EN CMS MASIVO</dir></dir>';
    -----------------------------------------------
    -----------------------------------------------
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := 'TTC_QC_CMS_MASIVO';
    lv_mensaje_tec_scp := upper('Pendientes');
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          Null,
                                          0,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          Null,
                                          0,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
    -------------------------------------------------------------------------------
   else
        open C_PROCESOS;
        loop
          fetch C_PROCESOS into RG_PROCESOS;
          exit when C_PROCESOS%notfound;
           SELECT decode(RG_PROCESOS.estado,
                          'F',
                          'Finalizadas',
                          'G',
                          'Pendientes',
                          'E',
                          'Error',
                          'Otros')
              INTO lv_estado
              FROM dual;
              -----------------------------------------------
              -----------------------------------------------
              --SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              lv_mensaje_apl_scp := 'TTC_QC_CMS_MASIVO';
              lv_mensaje_tec_scp := upper(lv_estado);
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    Null,
                                                    0,
                                                    Gv_unidad_registro_scp,
                                                    Null,
                                                    Null,
                                                    Null,
                                                    to_number(RG_PROCESOS.PROCESOS),
                                                    'N',
                                                    ln_error_scp,
                                                    lv_error_scp);
              -------------------------------------------------------------------------------


             if RG_PROCESOS.estado in ('G','E')then

                if RG_PROCESOS.PROCESOS > ln_num_procesos then
                   ln_num_procesos:= RG_PROCESOS.PROCESOS;
                end if;
             end if;
            lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||'Transacciones ' || lv_estado ||'('||RG_PROCESOS.estado||')'|| ' --> ' ||RG_PROCESOS.PROCESOS || '</dir></dir>';
        end loop;
        close C_PROCESOS;
        if ln_num_procesos >= ln_tope1 THEN
            --*********************************************************--
            BEGIN

              LV_RUTA:= '/home/gsioper/procesos/datacredito';--'REP_CONCILIA';
              --LF_FILE := Sys.Utl_File.Fopen(Pv_Dirloc, Pv_Archivo, 'R');
              LF_FILE := Sys.Utl_File.Fopen(LV_RUTA, 'reloj_count_colas.txt', 'R');
              LI_CONTADOR_COLA            := 0;

              Lv_mensaje_mail := lv_mensaje_mail ||'<br>';
                Lv_mensaje_mail := lv_mensaje_mail ||'<dir><dir>Colas de Procesamiento de la CMS_MASIVO</dir></dir>';
                Lv_mensaje_mail := lv_mensaje_mail ||'<dir><dir>C:\CMS_MASIVO\SERVER\RELOJ\</dir></dir>';


              LOOP
                BEGIN
                  sys.utl_file.get_line (LF_FILE, LV_LINEA);
                  Lb_Nomaslineas := false;
                  --LV_PARRAFO := LV_LINEA

                  IF substr(LV_LINEA,-1) <> ';' THEN
                     LI_CONTADOR:= LI_CONTADOR + 1;
                     IF LI_CONTADOR = 1 THEN
                        LN_TOTAL_CMS:= TO_NUMBER(LV_LINEA);
                        LV_FECHA:=NULL;
                        LV_FECHA_MIN:=NULL;
                        LV_FECHA_MAX:=NULL;

                     ELSIF LI_CONTADOR = 2 THEN
                           LV_FECHA:=LV_LINEA;
                           LV_FECHA_MIN:=LV_LINEA;
                           LV_FECHA_MAX:=LV_LINEA;

                     ELSIF LI_CONTADOR > 2 THEN
                           LV_FECHA:=LV_LINEA;
                           
                           BEGIN

                               IF to_date(LV_FECHA,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = ENGLISH') < to_date(LV_FECHA_MIN,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = ENGLISH') THEN
                                  LV_FECHA_MIN := LV_FECHA;
                               END IF;

                               IF to_date(LV_FECHA,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = ENGLISH') > to_date(LV_FECHA_MAX,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = ENGLISH') THEN
                                  LV_FECHA_MAX := LV_FECHA;
                               END IF;
                               
                           EXCEPTION
                           
                           WHEN OTHERS THEN
                               IF to_date(LV_FECHA,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = SPANISH') < to_date(LV_FECHA_MIN,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = SPANISH') THEN
                                  LV_FECHA_MIN := LV_FECHA;
                               END IF;

                               IF to_date(LV_FECHA,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = SPANISH') > to_date(LV_FECHA_MAX,'dd/mm/yyyy hh24:mi:ss','NLS_DATE_LANGUAGE = SPANISH') THEN
                                  LV_FECHA_MAX := LV_FECHA;
                               END IF;    
                           
                           END;

                     END IF;

                  END IF;

                  IF substr(LV_LINEA,-1) = ';' THEN
                     LI_CONTADOR := 0;
                     LI_CONTADOR_COLA:=LI_CONTADOR_COLA+1;

                    IF LN_TOTAL_CMS = 0 THEN
                       Lv_mensaje_mail := lv_mensaje_mail ||'<dir><dir>Total Cola :'||LI_CONTADOR_COLA ||'-->'|| LN_TOTAL_CMS||'</dir></dir>';
                    ELSE
                       Lv_mensaje_mail := lv_mensaje_mail ||'<dir><dir>Total Cola :'||LI_CONTADOR_COLA ||'-->'|| LN_TOTAL_CMS||', Primer Archivo <b>Recibido:</b> ' || LV_FECHA_MIN || ', Ultimo Archivo <b>Recibido:</b> ' ||LV_FECHA_MAX || '</dir></dir>';
                    END IF;
                  END IF;

                EXCEPTION
                  WHEN no_data_found
                  THEN
                    LV_LINEA := null;
                    Lb_Nomaslineas  := true;
                END;

                EXIT WHEN Lb_Nomaslineas;

              END LOOP;
              --COMMIT;
              Sys.Utl_File.Fclose(LF_FILE);

            EXCEPTION

                WHEN UTL_FILE.INVALID_PATH THEN
                  --DBMS_OUTPUT.PUT_LINE('INVALID_PATH');
                  PV_ERROR := 'Error en el directorio';
                  utl_file.fclose_all;

                WHEN UTL_FILE.INVALID_MODE THEN
                  --DBMS_OUTPUT.PUT_LINE('INVALID MODE');
                  PV_ERROR := 'Error INVALID MODE';
                  utl_file.fclose_all;

                WHEN UTL_FILE.INVALID_FILEHANDLE THEN
                  --DBMS_OUTPUT.PUT_LINE('INVALID_FILEHANDLE');
                  PV_ERROR := 'Error INVALID_FILEHANDLE';
                  utl_file.fclose_all;

                WHEN UTL_FILE.INVALID_OPERATION THEN
                  --DBMS_OUTPUT.PUT_LINE('INVALID OPERATION');
                  PV_ERROR := 'Error INVALID OPERATION';
                  utl_file.fclose_all;

                WHEN UTL_FILE.READ_ERROR THEN
                  --DBMS_OUTPUT.PUT_LINE('READ ERROR');
                  PV_ERROR := 'Error de lectura';
                  utl_file.fclose_all;

                WHEN UTL_FILE.INTERNAL_ERROR THEN
                  --DBMS_OUTPUT.PUT_LINE('INTERNAL_ERROR');
                  PV_ERROR := 'Error INTERNAL_ERROR';
                  utl_file.fclose_all;

                WHEN NO_DATA_FOUND -- END OF FILE
                 THEN
                  --DBMS_OUTPUT.PUT_LINE('NO DATA FOUND');
                  PV_ERROR := 'Error de existencia del archivo';

                WHEN OTHERS THEN
                  Pv_Error := SQLERRM;

          END;

            if Pv_Error IS NOT NULL THEN
              Raise LE_ERROR;
            end if;
            Lv_mensaje_mail := lv_mensaje_mail ||'<br>';
            --*********************************************************--
            Lv_mensaje_mail := lv_mensaje_mail || '<dir><dir>' ||
                                   lv_recomendacion1 || '</dir></dir><br>';
      end if;
  end if;

  --VERIFICO SI EN LA BITACORA EN EJECUCION HAY PENDIENTES
  open C_TX_PENDIENTES(ln_id_bitacora_scp);
  FETCH C_TX_PENDIENTES
    INTO LN_VALOR_TX_PENDIENTES;
  lb_found:= C_TX_PENDIENTES%FOUND;
  CLOSE C_TX_PENDIENTES;
  IF LB_FOUND AND LN_VALOR_TX_PENDIENTES > 0 THEN
     lb_permite_correo := TRUE;
  END IF;
  -----------------------------------------------
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN Prc_Masive:' || 'TTC_QC_CMS_MASIVO';
  lv_mensaje_tec_scp := 'Proceso Prc_Masive';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  -------------------------------------------------------------------------------

  --CIERRA PROCESO DE BITACORA
  SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(Ln_Id_Bitacora_Scp,
                                        Ln_Error_Scp,
                                        Lv_Error_Scp);
/**************************************************************************************/
/*        proceso de cms_masivo  parte segunda                                         */
/**************************************************************************************/
 /* Lv_mensaje_mail:=Lv_mensaje_mail||'<dir><b><u>Colas de Procesamiento de la CMS_MASIVO</u></b></dir>
         <dir><b><u>C:\CMS_MASIVO\SERVER\RELOJ\</u></b></dir><br>';*/
/**************************************************************************************/
  Lv_mensaje_mail:=Lv_mensaje_mail||'</html>';
    BEGIN
    IF lb_permite_correo THEN
        RELOJ.RC_ENVIAR_CORREO(pv_servidor     => 'conecel.com',
                               pv_de           => lv_mail_origen, --'reloj_cobranzas@conecel.com',
                               pv_para         => lv_mail_destino, --'jreinoso@cimaconsulting.com.ec',
                               pv_cc           => lv_mail_copia, --'jreinoso@cimaconsulting.com.ec',
                               pv_asunto       => LV_ASUNTO_MAIL,
                               pv_mensaje      => lv_mensaje_mail,
                               pv_codigoerror  => ln_error,
                               pv_mensajeerror => pv_error);
    END IF;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  COMMIT;
  pv_error:=NULL;

  EXCEPTION

    WHEN LE_ERROR THEN
      pv_error:= 'RC_TRX_QC_ALL.Prc_Masive'||pv_error;

    WHEN OTHERS THEN
      pv_error:= 'RC_TRX_QC_ALL.Prc_Masive'||SQLERRM;

 END  Prc_Masive;

end RC_TRX_QC_ALL;
/
