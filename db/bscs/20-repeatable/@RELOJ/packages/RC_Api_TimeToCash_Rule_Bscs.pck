create or replace package reloj.RC_Api_TimeToCash_Rule_Bscs IS

   GV_STATUS_T   VARCHAR2(1) := 'T';

  PROCEDURE RC_RETORNA_SALDO(PN_CUSTOMER_ID IN NUMBER, PN_SALDO OUT FLOAT);
  FUNCTION RC_RETORNA_INICIO_PERIODO(PV_BILLCYCLE IN VARCHAR2)    RETURN VARCHAR2;
  PROCEDURE RC_RETORNA_DEUDA(PN_CUSTOMER_ID IN NUMBER, PN_DEUDA OUT FLOAT);
  PROCEDURE RC_RETORNA_CREDITO(PN_CUSTOMER_ID IN NUMBER, PN_CREDITO OUT FLOAT);
  PROCEDURE RC_RETORNA_SALDO_REAL(PN_CUSTOMER_ID IN NUMBER,
                                                                                  PN_SALDO_MIN_POR   IN FLOAT,
                                                                                  PN_SALDOREAL       OUT FLOAT,
                                                                                  PN_SALDO_IMPAGO    OUT FLOAT);

--9833 INI AAM
  PROCEDURE RC_RETORNA_SALDO_REAL_REACT(PN_CUSTOMER_ID IN NUMBER,
                                                                                  PN_SALDO_MIN_POR   IN FLOAT,
                                                                                  PN_SALDOREAL       OUT FLOAT,
                                                                                  PN_DEUDA           OUT FLOAT,
                                                                                  PN_SALDO_IMPAGO    OUT FLOAT);
--9833 FIN AAM  

  PROCEDURE RC_RETORNA_SALDO_REAL_CTA(PV_CUSTCODE        IN VARCHAR2,
                                                                                            PN_SALDO_MIN_POR   IN FLOAT,
                                                                                            PN_SALDOREAL            OUT FLOAT,
                                                                                            PN_SALDO_IMPAGO     OUT FLOAT
                                                                                            );

  PROCEDURE RC_RETORNA_SALDO_REAL_CTA_MENS(PV_CUSTCODE        IN VARCHAR2,
                                           PN_SALDO_MIN_POR   IN FLOAT,
                                           PN_SALDOREAL       OUT FLOAT,
                                           PN_DEUDA           OUT FLOAT,
                                           PN_SALDO_IMPAGO    OUT FLOAT
                                           );

  PROCEDURE RC_CENSO_PROCESS_IN_BCK(PN_PROCESO        IN NUMBER,
                                                                                    PV_DESSHORT       IN VARCHAR2,
                                                                                    PN_FILTRO         IN VARCHAR2,
                                                                                    PN_GROUPTREAD     IN NUMBER,
                                                                                    PN_SOSPID        IN NUMBER,
                                                                                    PB_EXISTE         OUT BOOLEAN);
   PROCEDURE RC_PROCESS_IN_BCK(PN_PROCESO    IN NUMBER,
    PV_DESSHORT   IN VARCHAR2,
    PN_FILTRO     IN VARCHAR2,
    PN_GROUPTREAD IN NUMBER,
    PB_EXISTE     OUT BOOLEAN);

  PROCEDURE RC_RETORNA_MAXTRX_PROCESS(PN_PROCESS IN NUMBER, PN_MAXTRX  OUT NUMBER);
  PROCEDURE RC_RETORNA_REFRESH_CYCLE(PN_PROCESS       IN NUMBER, PN_REFRESH_CYCLE OUT NUMBER);
    PROCEDURE RC_RETORNA_RULE_ELEMENT(PV_DESSHORT     IN VARCHAR2,
                                                                                        PN_value_max_nointento             OUT NUMBER,
                                                                                        PN_value_max_trx_encolada        OUT NUMBER,
                                                                                        PN_value_max_trx_by_execution OUT NUMBER,
                                                                                        PN_value_max_trxcommit OUT NUMBER,
                                                                                        PN_maxretentrx OUT NUMBER,
                                                                                        PV_OUT_ERROR OUT VARCHAR2
                                                                                        );
  FUNCTION RC_RETORNA_ID_PROCESS(PV_DESSHORT IN VARCHAR2) RETURN NUMBER;
  FUNCTION RC_RETORNA_MAX_DELAY(PV_DESSHORT IN VARCHAR2) RETURN NUMBER;
  FUNCTION RC_RETORNA_CANT_REGIS_DISP(PV_TABLA IN Varchar2,
                                      PV_GRUPO_SUBHILO IN VARCHAR2 DEFAULT NULL) return Number;
  PROCEDURE RC_REGISTRAR_BCK(PN_PROCESS       IN NUMBER,
                             PN_DESSHORT      VARCHAR2,
                             PN_GROUPTREAD    IN NUMBER,
                             PN_TREAD_NO      IN NUMBER,
                             PV_PFILTERFIELD  IN VARCHAR2,
                             PD_LASTRUNDATE   IN DATE,
                             PN_MAXDELAY      IN NUMBER,
                             PN_REFRESH_CYCLE IN NUMBER,
                             PN_SOSPID        IN NUMBER,
                             PV_OUT_ERROR     OUT VARCHAR2);
  PROCEDURE RC_REGISTRAR_BCK_REMOTO(PN_PROCESS       IN NUMBER,
                             PN_DESSHORT      VARCHAR2,
                             PN_GROUPTREAD    IN NUMBER,
                             PN_TREAD_NO      IN NUMBER,
                             PV_PFILTERFIELD  IN VARCHAR2,
                             PD_LASTRUNDATE   IN DATE,
                             PN_MAXDELAY      IN NUMBER,
                             PN_REFRESH_CYCLE IN NUMBER,
                             PN_SOSPID        IN NUMBER,
                             PV_OUT_ERROR     OUT VARCHAR2);
  PROCEDURE RC_ELIMINAR_BCK(PN_PROCESO        IN NUMBER,
                            PV_DESSHORT       IN VARCHAR2,
                            PN_FILTRO         IN VARCHAR2,
                            PN_GROUPTREAD     IN NUMBER,
                            PV_OUT_ERROR OUT VARCHAR2);
 PROCEDURE RC_ELIMINAR_BCK_REMOTO(PN_PROCESO        IN NUMBER,
                                  PV_DESSHORT       IN VARCHAR2,
                                  PN_FILTRO         IN VARCHAR2,
                                  PN_GROUPTREAD     IN NUMBER,
                                  PV_OUT_ERROR OUT VARCHAR2);
  PROCEDURE RC_ACTUALIZAR_PROGRAMMER(PN_PROCESO     IN NUMBER,
                                     PN_SEQNO       IN NUMBER,
                                     PD_RUNDATE     IN DATE,
                                     PD_LASTMONDATE IN DATE,
                                     PV_OUT_ERROR   OUT VARCHAR);
  PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_DESSHORT    IN VARCHAR2,
                                    PN_PROCESO     IN NUMBER,
                                    PD_LASTMONDATE IN DATE,
                                    PV_OUT_ERROR   OUT VARCHAR);
  PROCEDURE RC_REGISTRAR_PROGRAMMER_REMOTO(PN_DESSHORT    IN VARCHAR2,
                                           PN_PROCESO     IN NUMBER,
                                           PD_LASTMONDATE IN DATE,
                                           PV_OUT_ERROR   OUT VARCHAR2);
  FUNCTION RC_RETORNA_CANT_REGIS_SEND(TIPO_SEND IN VARCHAR2,--[8693] JJI - D:para dth M:para movil
                                      TIPO IN VARCHAR2,
                                      FECHA IN VARCHAR2,
                                      CAMPO IN VARCHAR2 DEFAULT NULL,
                                      VALOR IN NUMBER DEFAULT NULL) RETURN NUMBER;
  FUNCTION RC_RETORNA_CANT_REGIS_CUSTOMER(FV_FILTRO IN VARCHAR2,
                                      FV_GROUPTHREAD IN VARCHAR2) RETURN NUMBER;
  PROCEDURE RC_SAVE_LOG_INOUT(PN_RC_PROCESS  IN NUMBER,
                              PD_FECHA_START IN DATE,
                              PD_FECHA_STOP  IN DATE,
                              PV_STATUS      IN VARCHAR2,
                              PV_ERROR       IN VARCHAR2);
  PROCEDURE RC_SAVE_LOG_SEND(PN_RC_PROCESS  IN NUMBER,
                             PD_FECHA_START IN DATE,
                             PD_FECHA_STOP  IN DATE,
                             PV_STATUS      IN VARCHAR2,
                             PV_ERROR       IN VARCHAR2);
  PROCEDURE RC_SAVE_LOG_DISPATCH(PN_RC_PROCESS  IN NUMBER,
                                 PD_FECHA_START IN DATE,
                                 PD_FECHA_STOP  IN DATE,
                                 PV_STATUS      IN VARCHAR2,
                                 PV_ERROR       IN VARCHAR2);
  FUNCTION RC_RETORNA_PROCESO_CORRECTO(PD_FECHA_EJECUCION IN DATE)
    RETURN BOOLEAN;
  FUNCTION RC_RETORNA_PROCESO_CORRECTO_SD(PD_FECHA_EJECUCION IN DATE)
    RETURN BOOLEAN;
    PROCEDURE RC_SAVE_REMOTE_TRX_DISPATCH(LN_ID_PROCESS        IN NUMBER,
                                                                                               PV_NAME_TABLE     IN VARCHAR2,
                                                                                               LV_STATUS_TRX       IN VARCHAR2,
                                                                                               LD_FECHA_ACTUAL IN DATE,
                                                                                               LV_CODIGOERROR  IN VARCHAR2,
                                                                                               LN_CUSTOMER_ID   IN NUMBER,
                                                                                               LN_CO_ID                   IN NUMBER,
                                                                                               LV_DN_NUM              IN VARCHAR2,
                                                                                               LN_ST_SEQNO                  IN NUMBER,
                                                                                               PV_ERROR                 OUT VARCHAR2
                                                                                               ) ;
  FUNCTION FNC_RC_REACTIVA(PV_Custcode               IN VARCHAR2,
                                                            PN_Customer_id          IN NUMBER,
                                                            Pd_Date                         IN DATE,
                                                            PN_CO_IDTRAMITE    IN NUMBER,
                                                            PV_STATUS_P               IN VARCHAR2,
                                                            PV_Plan_Telefonia       IN VARCHAR2,
                                                            PV_STATUS_OUT         OUT VARCHAR2,
                                                            PV_Sql_Planing             OUT VARCHAR2,
                                                            PV_Sql_PlanDet            OUT VARCHAR2,
                                                            PV_REMARK                 OUT VARCHAR2,
                                                            PN_SALDO_MIN          IN NUMBER,
                                                            PN_SALDO_MIN_POR IN NUMBER,
                                                            PV_USER_AXIS              IN VARCHAR2,
                                                            PN_PK_ID                      IN NUMBER DEFAULT 0,
                                                            PN_st_seqno                 IN NUMBER DEFAULT 0,
                                                            PV_CO_STATUS_A        IN VARCHAR2  DEFAULT '29',
                                                            PV_CO_STATUS_B        IN VARCHAR2  DEFAULT 'a',
                                                            PV_PERIOD                   IN NUMBER  DEFAULT 0,
                                                            PV_REMARKTRX         IN VARCHAR2 DEFAULT 'RelojAxis:Reactiva Pagos Linea',
                                                            PV_OUT_ERROR          OUT VARCHAR2
                                                            )  RETURN BOOLEAN;

--9833 INI AAM
  FUNCTION FNC_RC_REACTIVA_RELOJ(PV_Custcode               IN VARCHAR2,
                                                            PN_Customer_id          IN NUMBER,
                                                            Pd_Date                         IN DATE,
                                                            PN_CO_IDTRAMITE    IN NUMBER,
                                                            PV_STATUS_P               IN VARCHAR2,
                                                            PV_Plan_Telefonia       IN VARCHAR2,
                                                            PV_STATUS_OUT         OUT VARCHAR2,
                                                            PV_Sql_Planing             OUT VARCHAR2,
                                                            PV_Sql_PlanDet            OUT VARCHAR2,
                                                            PV_REMARK                 OUT VARCHAR2,
                                                            PN_SALDO_MIN          IN NUMBER,
                                                            PN_SALDO_MIN_POR IN NUMBER,
                                                            PV_USER_AXIS              IN VARCHAR2,
                                                            PN_PK_ID                      IN NUMBER DEFAULT 0,
                                                            PN_st_seqno                 IN NUMBER DEFAULT 0,
                                                            PV_CO_STATUS_A        IN VARCHAR2  DEFAULT '29',
                                                            PV_CO_STATUS_B        IN VARCHAR2  DEFAULT 'a',
                                                            PV_PERIOD                   IN NUMBER  DEFAULT 0,
                                                            PV_REMARKTRX         IN VARCHAR2 DEFAULT 'RelojAxis:Reactiva Pagos Linea',
                                                            PN_DEUDA              OUT FLOAT,
                                                            PN_SALDO              OUT FLOAT,
                                                            PV_OUT_ERROR          OUT VARCHAR2,
                                                            PN_INSERT             IN NUMBER DEFAULT 0
                                                            )  RETURN BOOLEAN;
--9833 FIN AAM

FUNCTION FNC_PLAN_TELEFONIA(PN_Customer_id    IN NUMBER,
                                                                   PV_Plan_Telefonia IN VARCHAR2,
                                                                   PD_Fecha                 IN DATE,
                                                                   PV_STATUS_P         IN VARCHAR2
                                                                   ) RETURN NUMBER;
                                                                   
--10695 IRO-JGA 
FUNCTION FNC_MAX_MIN_HORA(
                                                                   Pv_parametro  IN VARCHAR2,
                                                                   Pv_proceso    IN VARCHAR2
                                                                   ) RETURN VARCHAR2;
                                                                   
FUNCTION FNC_RC_REACTIVA_RELOJ_2(PV_Custcode       IN VARCHAR2,
                                 PN_Customer_id    IN NUMBER,
                                 Pd_Date           IN DATE,
                                 PV_STATUS_P       IN VARCHAR2,
                                 PV_Plan_Telefonia IN VARCHAR2 DEFAULT NULL,
                                 PV_STATUS_OUT     OUT VARCHAR2,
                                 PV_Sql_Planing    OUT VARCHAR2,
                                 PV_Sql_PlanDet    OUT VARCHAR2,
                                 PV_REMARK         OUT VARCHAR2,
                                 PN_SALDO_MIN      IN NUMBER,
                                 PN_SALDO_MIN_POR  IN NUMBER,
                                 PN_PK_ID          IN NUMBER DEFAULT 0,
                                 PN_st_seqno       IN NUMBER DEFAULT 0,
                                 PN_DEUDA          OUT FLOAT,
                                 PN_SALDO          OUT FLOAT,
                                 PV_OUT_ERROR      OUT VARCHAR2,
                                 PD_FECHA_CORTE    OUT DATE) RETURN BOOLEAN;
                                 
 PROCEDURE RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID IN NUMBER,
                               PN_DEUDA_TOTAL OUT NUMBER,
                               PN_ERROR       IN OUT NUMBER,
                               PV_ERROR       IN OUT VARCHAR2);                                

End RC_Api_TimeToCash_Rule_Bscs;
/
create or replace package body reloj.RC_Api_TimeToCash_Rule_Bscs is

  /*******************************************************************************************************
  * [4774] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   23/11/2009
  * Modificado:
  ********************************************************************************************************/
  --=============================================================================--
  -- Modificado Por: IRO Miguel Mu�oz
  -- Proyecto      : [6527] - Mejoras en el proceso de Reloj de cobranzas
  -- Fecha         : 03/05/2012
  -- Lider Proyecto: Juan David P�rez
  -- Lider IRO     : Ver�nica Olivo
  -- Motivo        : Se cre� proceso RC_PROCESS_IN_BCK para determinar si existe un process Retry
  --                 en la tabla TTC_BCK_PROCESS_S
--==============================================================================--
/*********************************************************************************************************
  LIDER SIS:      JUAN DAVID P�REZ
  LIDER IRO:      VER�NICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8250 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
  FECHA:          04/09/2012
  PROPOSITO:      Modificacion del proceso: rc_retorna_cant_regis_disp para recibir el parametro PV_GRUPO_SUBHILO
                  que por default es null.
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID P�REZ
  LIDER IRO:      VER�NICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8250 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
  FECHA:          03/10/2012
  PROPOSITO:      Creacion de� procedimiento RC_RETORNA_CANT_REGIS_CUSTOMER
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID P�REZ
  LIDER IRO:      VER�NICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8693 RELOJ DE COBRANZA DTH
  FECHA:          03/05/2013
  PROPOSITO:      Modificacion del procedimiento :RC_RETORNA_CANT_REGIS_SEND para que reciba el parametro TIPO_SEND
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          17/10/2014
  PROPOSITO:      Creaci�n del procedimiento RC_RETORNA_SALDO_REAL_CTA_MENS para consultar deuda de cuentas 
                  que ingresar�n a Mensajer�a
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          17/12/2014
  PROPOSITO:      Creaci�n de la funcion FNC_RC_REACTIVA_RELOJ y el procedimiento RC_RETORNA_SALDO_REAL_REACT
  *********************************************************************************************************/
  /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      JUAN ROMERO
    MODIFICADO POR: IRO JOHANNA GARC�A MORA
    PROYECTO:       10695 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
    FECHA:          10/03/2016
    PROPOSITO:      Creaci�n de la funci�n FNC_MAX_MIN_HORA para obtener los parametros de hora inicio 00:00:00 y 
                    hora fin 22:00:00 para la Ejecuci�n del proceso de suspensi�n por reloj.                  
    *********************************************************************************************************/
   --=====================================================================================--
   -- MODIFICADO POR: IRO Andres Balladares.
   -- FECHA MOD:      07/06/2016
   -- PROYECTO:       [10695] Mejoras al proceso de Reloj de Cobranzas
   -- LIDER IRO:      IRO Juan Romero
   -- LIDER :         SIS Antonio Mayorga
   -- MOTIVO:         Se agrego insert a la tabla RC_BSCS_CTAS_REACNR por DBLINK para evitar usar el trigger TTC_TRG_SEND_TO_REACTIVAR_AXIS
   --=====================================================================================--
 /*--=======================RELOJ DE COBRANZAS ================================--
  Modificado por:    Kevin Murillo.
  lider PDS     :    IRO Juan Romero.
  lider SIS     :    SIS Antonio Mayorga.
  Fecha         :    14/06/2016         
  COD Proy      :    10824 - Mejoras en el proceso de Reloj de Cobranza.
  Descripci�n   :    Se modificara el procedimiento RC_RETORNA_DEUDA, 
                     el cual es el encargado  de obtener el valor de la deuda total
                     de la factura pendiente que posea una cuenta, con el objetivo 
                     que dicho valor ahora  sea considerado en base al valor de la 
                     �ltima factura vencida que posea la cuenta, el cual es requerido 
                     para el c�lculo del porcentaje permitido para los reintentos de 
                     activaci�n del feature de mensajer�a.
  Siglas de Busq:    IRO KMU.
  --======================================================================--*/
   --=====================================================================================--
   -- MODIFICADO POR: IRO Anabell Amayquema.
   -- FECHA MOD:      02/12/2016
   -- PROYECTO:       [10995] Mejoras al proceso de Reloj de Cobranzas
   -- LIDER IRO:      IRO Juan Romero
   -- LIDER :         SIS Antonio Mayorga
   -- MOTIVO:         Se cre� la funci�n FNC_RC_REACTIVA_RELOJ_2 con el objetivo de obtener
   --                 la fecha de corte asociada a una cuenta.
   --=====================================================================================--


  --=====================================================================================--
  -- MODIFICADO POR: HTS Ingrid Orozco.
  -- FECHA MOD:      16/03/2021
  -- PROYECTO:       [13200] Reloj de Cobranzas Clientes Corporativos voz.
  -- LIDER HTS:      HTS Gloria Rojas
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Categorizaci�n de clientes corporativos.
  --=====================================================================================--
  LB_NOTFOUND BOOLEAN;

  LD_DATE_RUN DATE;
  /*****************************************************************
   *Descripcion: retorna el saldo de una cuenta especifica
  ****************************************************************/

    CURSOR VERIFICA_CUENTA_REAC(LV_CUSTCODE VARCHAR2, FECHA_ACTUAL DATE) IS
    SELECT COUNT(*)
      FROM TTC_CUSTOMERS_REACTIVA_S A
     WHERE A.CUSTCODE = LV_CUSTCODE
       AND TRUNC(A.INSERTIONDATE) = TRUNC(FECHA_ACTUAL);

    --Obtiene el saldo pendiente, considerando la fecha de vigencia de la �ltima factura
    CURSOR C_SALDO_PENDIENTE(CN_Customer_id NUMBER) IS
      SELECT /*+ RULE +*/
                       NVL(SUM(O.OHOPNAMT_DOC), 0) SALDO
         FROM  SYSADM.ORDERHDR_ALL O
      WHERE O.CUSTOMER_ID = CN_Customer_id
             AND O.OHSTATUS IN ('IN', 'CM', 'CO')
             AND O.OHDUEDATE < TRUNC(SYSDATE);

    --Obtiene la deuda total de las facturas pendientes, esto es requerido para el c�lculo
    --del porcentaje permitido para la reactivaci�n de la cuenta
  CURSOR C_DEUDA_TOTAL (CN_Customer_id NUMBER) IS-- IRO KMU INI 14/06/2016 
   /*SELECT \*+ INDEX(O,OH_CUST_ID_1)*\
                   NVL(SUM(OHINVAMT_DOC), 0)   DEUDA
      FROM SYSADM.ORDERHDR_ALL O
    WHERE O.CUSTOMER_ID = CN_Customer_id
           AND O.OHSTATUS IN ('IN','CM','CO')
           AND O.OHOPNAMT_DOC > 0;*/
   SELECT NVL(X.OHINVAMT_DOC, 0)
           FROM (SELECT /*+ INDEX(O,OH_CUST_ID_1)*/
                  A.OHXACT,
                  A.OHINVAMT_DOC
                   FROM  SYSADM.ORDERHDR_ALL A
                  WHERE A.CUSTOMER_ID = CN_CUSTOMER_ID
                    AND A.OHSTATUS = 'IN'
                    AND A.OHDUEDATE < TRUNC(SYSDATE)
                  ORDER BY A.OHXACT DESC) X
                  WHERE ROWNUM=1;--IRO KMU FIN 14/06/2016 

    --Obtiene el total de los valores por cr�ditos a facturaci�n, ingresados para que se
    --apliquen en el pr�ximo corte de facturaci�n
    CURSOR C_CREDITOS(CN_Customer_id NUMBER) IS
      SELECT /*+ RULE +*/
                      NVL(SUM(AMOUNT), 0) CREDITOS
        FROM  FEES B
       WHERE B.CUSTOMER_ID = CN_Customer_id
         AND B.PERIOD > 0
         AND B.SNCODE = 46;


      CURSOR C_Plan_TelefoniaPublica (CN_Customer_id NUMBER, Cn_Planes_TP NUMBER, CD_Fecha DATE, CV_STATUS_P IN VARCHAR2) IS
        SELECT  COUNT(*)
          FROM Ttc_Contractplannigdetails_s cpl
         WHERE cpl.customer_id = CN_Customer_id
                AND cpl.tmcode =Cn_Planes_TP
                AND cpl.ch_timeacciondate<=CD_Fecha
                AND cpl.co_statustrx = CV_STATUS_P;

      --Obtiene el customer_id de la customer_all de BSCS
      CURSOR C_Custcode_Return (CV_Custcode  IN VARCHAR2) IS
        SELECT /*+ RULE +*/
                       CUSTOMER_ID
          FROM SYSADM.CUSTOMER_ALL C
       WHERE C.CUSTCODE = CV_Custcode;


  PROCEDURE RC_RETORNA_SALDO(PN_Customer_id IN NUMBER, PN_SALDO OUT FLOAT) IS
    LB_NOTFOUND_SALDO  BOOLEAN;
    LN_SALDO_PENDIENTE FLOAT;
  BEGIN

    OPEN C_SALDO_PENDIENTE(PN_Customer_id);
    FETCH C_SALDO_PENDIENTE
      INTO LN_SALDO_PENDIENTE;
    LB_NOTFOUND_SALDO := C_SALDO_PENDIENTE%NOTFOUND;
    CLOSE C_SALDO_PENDIENTE;

    IF LB_NOTFOUND_SALDO THEN
      LN_SALDO_PENDIENTE := 0;
    END IF;

    PN_SALDO := LN_SALDO_PENDIENTE;

  EXCEPTION
    WHEN OTHERS THEN
      PN_SALDO := 0;
  END RC_RETORNA_SALDO;

  /*****************************************************************************
   *Descripcion: retorna la Fecha de Inicio de Periodo de un ciclon determinado
  *****************************************************************************/

  FUNCTION RC_RETORNA_INICIO_PERIODO(PV_BILLCYCLE IN VARCHAR2)
    RETURN VARCHAR2 IS

    lv_dia_ini   varchar2(2);
    lv_dia_fin   varchar2(2);
    lv_mes_ini   varchar2(2);
    lv_anio_ini  varchar2(4);
    fecha_ini    VARCHAR(10);
    PV_OUT_ERROR VARCHAR2(100);
    LE_ERROR EXCEPTION;

    CURSOR C_RC_BCYCLE(BCYCLE IN VARCHAR2) IS
      select b.dia_ini_ciclo, b.dia_fin_ciclo
        From fa_ciclos_axis_bscs a, fa_ciclos_bscs b
       where a.id_ciclo = b.id_ciclo
         AND a.id_ciclo_admin = BCYCLE;
  BEGIN

    fecha_ini   := NULL;
    LB_NOTFOUND := NULL;
    LD_DATE_RUN := SYSDATE;
    OPEN C_RC_BCYCLE(PV_BILLCYCLE);
    FETCH C_RC_BCYCLE
      INTO lv_dia_ini, lv_dia_fin;
    LB_NOTFOUND := C_RC_BCYCLE%NotFound;
    CLOSE C_RC_BCYCLE;

    IF LB_NOTFOUND THEN
      PV_OUT_ERROR := 'RC_RETORNA_INICIO_PERIODO- ADVERTENCIA NO EXISTEN FECHA DE INICIO PARA EL CYCLE ' ||
                      PV_BILLCYCLE;
      fecha_ini    := NULL;
      RAISE LE_ERROR;
    END IF;

    IF NOT (lv_dia_ini IS NULL) THEN

      If to_NUMBER(to_char(sysdate, 'dd')) < to_NUMBER(lv_dia_ini) Then

        If to_NUMBER(to_char(sysdate, 'mm')) = 1 Then
          lv_mes_ini  := '12';
          lv_anio_ini := to_char(sysdate, 'yyyy') - 1;
        Else
          lv_mes_ini  := lpad(to_char(sysdate, 'mm') - 1, 2, '0');
          lv_anio_ini := to_char(sysdate, 'yyyy');
        End If;
      Else
        lv_mes_ini  := to_char(sysdate, 'mm');
        lv_anio_ini := to_char(sysdate, 'yyyy');

      End If;
      fecha_ini := lv_dia_ini || '/' || lv_mes_ini || '/' || lv_anio_ini;
    END IF;
    --dbms_output.put_line('fecha-> '||fecha_ini);
    RETURN(fecha_ini);

  EXCEPTION
    WHEN LE_ERROR THEN
          RETURN(PV_OUT_ERROR);
  END RC_RETORNA_INICIO_PERIODO;

  /*****************************************************************************
   *Descripcion: retorna la deuda de una cuenta determinada
  *****************************************************************************/

  PROCEDURE RC_RETORNA_DEUDA(PN_Customer_id IN NUMBER, PN_DEUDA OUT FLOAT) IS
    LN_DEUDA_TOTAL            NUMBER :=0;
  BEGIN

    OPEN C_DEUDA_TOTAL (PN_Customer_id);
    FETCH C_DEUDA_TOTAL
     INTO LN_DEUDA_TOTAL;
    CLOSE C_DEUDA_TOTAL;

    IF LN_DEUDA_TOTAL <= 0 THEN
        PN_DEUDA := 0;
    ELSE
       PN_DEUDA:= LN_DEUDA_TOTAL;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_DEUDA := 0;
  END RC_RETORNA_DEUDA;

  /*****************************************************************************
   *Descripcion: retorna el Credito de una cuenta determinado
  *****************************************************************************/

  PROCEDURE RC_RETORNA_CREDITO(PN_Customer_id IN NUMBER, PN_CREDITO OUT FLOAT) IS

    LB_FOUND_CREDITO BOOLEAN;
    LC_CREDITOS      C_CREDITOS%ROWTYPE;

  BEGIN

    OPEN C_CREDITOS(PN_Customer_id);
    FETCH C_CREDITOS
      INTO LC_CREDITOS;
    LB_FOUND_CREDITO := C_CREDITOS%FOUND;
    CLOSE C_CREDITOS;

    IF LB_FOUND_CREDITO THEN
      PN_CREDITO := LC_CREDITOS.CREDITOS;
    ELSE
      PN_CREDITO := 0;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_CREDITO := 0;
  END RC_RETORNA_CREDITO;

  /*****************************************************************************
   *Descripcion: retorna el Saldo Real de una cuenta determinada se envia
   el customer_id desde BSCS o AXIS
  *****************************************************************************/

  PROCEDURE RC_RETORNA_SALDO_REAL(PN_CUSTOMER_ID        IN NUMBER,
                                                                                  PN_SALDO_MIN_POR   IN FLOAT,
                                                                                  PN_SALDOREAL            OUT FLOAT,
                                                                                  PN_SALDO_IMPAGO     OUT FLOAT) IS


    LN_POR_IMPAGO    FLOAT;
    LN_SALDO         FLOAT;
    LN_CREDITO       FLOAT;
    LN_DEUDA         FLOAT;

  BEGIN
    RC_RETORNA_SALDO(PN_CUSTOMER_ID,LN_SALDO);
    RC_RETORNA_CREDITO(PN_CUSTOMER_ID,LN_CREDITO);
    RC_RETORNA_DEUDA(PN_CUSTOMER_ID,LN_DEUDA);

    LN_POR_IMPAGO   := ((100 - PN_SALDO_MIN_POR) / 100);
    PN_SALDO_IMPAGO := (LN_DEUDA * LN_POR_IMPAGO);
    PN_SALDOREAL    := LN_SALDO + LN_CREDITO;

    EXCEPTION
     WHEN OTHERS THEN
       PN_SALDO_IMPAGO:=0;
       PN_SALDOREAL:=0;
  END RC_RETORNA_SALDO_REAL;

--9833 INI AAM
  /*****************************************************************************
   *Descripcion: retorna el Saldo Real de una cuenta determinada se envia
   el customer_id desde BSCS o AXIS
  *****************************************************************************/

  PROCEDURE RC_RETORNA_SALDO_REAL_REACT(PN_CUSTOMER_ID   IN NUMBER,
                                  PN_SALDO_MIN_POR IN FLOAT,
                                  PN_SALDOREAL     OUT FLOAT,
                                  PN_DEUDA         OUT FLOAT,
                                  PN_SALDO_IMPAGO  OUT FLOAT) IS


    LN_POR_IMPAGO    FLOAT;
    LN_SALDO         FLOAT;
    LN_CREDITO       FLOAT;
    LN_DEUDA         FLOAT;

  BEGIN
    RC_RETORNA_SALDO(PN_CUSTOMER_ID,LN_SALDO);
    RC_RETORNA_CREDITO(PN_CUSTOMER_ID,LN_CREDITO);
    RC_RETORNA_DEUDA(PN_CUSTOMER_ID,LN_DEUDA);

    PN_DEUDA        := LN_DEUDA;
    LN_POR_IMPAGO   := ((100 - PN_SALDO_MIN_POR) / 100);
    PN_SALDO_IMPAGO := (LN_DEUDA * LN_POR_IMPAGO);
    PN_SALDOREAL    := LN_SALDO + LN_CREDITO;

    EXCEPTION
     WHEN OTHERS THEN
       PN_SALDO_IMPAGO:=0;
       PN_SALDOREAL:=0;
  END RC_RETORNA_SALDO_REAL_REACT;
--9833 FIN AAM


  /*****************************************************************************
   *Descripcion: retorna el Saldo Real de una cuenta determinada se envia
   la cuenta desde AXIS
  *****************************************************************************/

  PROCEDURE RC_RETORNA_SALDO_REAL_CTA(PV_CUSTCODE        IN VARCHAR2,
                                                                                            PN_SALDO_MIN_POR   IN FLOAT,
                                                                                            PN_SALDOREAL            OUT FLOAT,
                                                                                            PN_SALDO_IMPAGO     OUT FLOAT
                                                                                            ) IS

    LN_POR_IMPAGO   FLOAT;
    LN_SALDO               FLOAT;
    LN_CREDITO           FLOAT;
    LN_DEUDA               FLOAT;
    LN_CUSTOMER_ID NUMBER;
    LB_FOUND               BOOLEAN;
  BEGIN

      OPEN C_Custcode_Return(PV_CUSTCODE);
    FETCH C_Custcode_Return
      INTO LN_CUSTOMER_ID;
    LB_FOUND := C_Custcode_Return%FOUND;
    CLOSE C_Custcode_Return;
    IF LB_FOUND THEN
        RC_RETORNA_SALDO(LN_CUSTOMER_ID,LN_SALDO);
        RC_RETORNA_CREDITO(LN_CUSTOMER_ID,LN_CREDITO);
        RC_RETORNA_DEUDA(LN_CUSTOMER_ID,LN_DEUDA);

        LN_POR_IMPAGO   := ((100 - PN_SALDO_MIN_POR) / 100);
        PN_SALDO_IMPAGO := (LN_DEUDA * LN_POR_IMPAGO);
        PN_SALDOREAL    := LN_SALDO + LN_CREDITO;
    END IF;

    EXCEPTION
     WHEN OTHERS THEN
       PN_SALDO_IMPAGO:=0;
       PN_SALDOREAL:=0;
  END RC_RETORNA_SALDO_REAL_CTA;


  /*****************************************************************************
   *Descripcion: retorna el Saldo Real de una cuenta determinada se envia
   la cuenta desde AXIS
  *****************************************************************************/

  PROCEDURE RC_RETORNA_SALDO_REAL_CTA_MENS(PV_CUSTCODE        IN VARCHAR2,
                                           PN_SALDO_MIN_POR   IN FLOAT,
                                           PN_SALDOREAL       OUT FLOAT,
                                           PN_DEUDA           OUT FLOAT,
                                           PN_SALDO_IMPAGO    OUT FLOAT
                                           ) IS

    LN_POR_IMPAGO   FLOAT;
    LN_SALDO               FLOAT;
    LN_CREDITO           FLOAT;
    LN_DEUDA               FLOAT;
    LN_CUSTOMER_ID NUMBER;
    LB_FOUND               BOOLEAN;
  BEGIN

      OPEN C_Custcode_Return(PV_CUSTCODE);
    FETCH C_Custcode_Return
      INTO LN_CUSTOMER_ID;
    LB_FOUND := C_Custcode_Return%FOUND;
    CLOSE C_Custcode_Return;
    IF LB_FOUND THEN
        RC_RETORNA_SALDO(LN_CUSTOMER_ID,LN_SALDO);
        RC_RETORNA_CREDITO(LN_CUSTOMER_ID,LN_CREDITO);
        RC_RETORNA_DEUDA(LN_CUSTOMER_ID,LN_DEUDA);

        PN_DEUDA        := LN_DEUDA;
        LN_POR_IMPAGO   := ((100 - PN_SALDO_MIN_POR) / 100);
        PN_SALDO_IMPAGO := (LN_DEUDA * LN_POR_IMPAGO);
        PN_SALDOREAL    := LN_SALDO + LN_CREDITO;
    END IF;

    EXCEPTION
     WHEN OTHERS THEN
       PN_SALDO_IMPAGO:=0;
       PN_SALDOREAL:=0;
  END RC_RETORNA_SALDO_REAL_CTA_MENS;


  /**********************************************************************
  * Retorna el maximo de transacciones permitidas, antes de verificar
  * que el proceso siga en bck
  **********************************************************************/
  PROCEDURE RC_RETORNA_MAXTRX_PROCESS(PN_PROCESS IN NUMBER,
                                      PN_MAXTRX  OUT NUMBER) IS

    LN_MAXTRX NUMBER;
    LE_ERROR EXCEPTION;

  BEGIN
    LD_DATE_RUN := SYSDATE;

    SELECT A.MAXTRX
      INTO LN_MAXTRX
      FROM TTC_PROCESS_S A
     WHERE A.PROCESS = PN_PROCESS;

    PN_MAXTRX := LN_MAXTRX;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
          PN_MAXTRX := 0;
    WHEN OTHERS THEN
          PN_MAXTRX    := 0;

  END RC_RETORNA_MAXTRX_PROCESS;

  /**********************************************************************
  **********************************************************************/
  PROCEDURE RC_RETORNA_REFRESH_CYCLE(PN_PROCESS       IN NUMBER,
                                     PN_REFRESH_CYCLE OUT NUMBER) IS

        LN_REFRESH_CYCLE NUMBER;
  BEGIN

    LD_DATE_RUN := SYSDATE;

    SELECT A.REFRESH_CYCLE
      INTO LN_REFRESH_CYCLE
      FROM TTC_PROCESS_S A
     WHERE A.PROCESS = PN_PROCESS;

    PN_REFRESH_CYCLE := LN_REFRESH_CYCLE;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PN_REFRESH_CYCLE := 0;

    WHEN OTHERS THEN
      PN_REFRESH_CYCLE := 0;

  END RC_RETORNA_REFRESH_CYCLE;

  /**********************************************************************
  * Retorna el cantidad de registros a procesar
  **********************************************************************/
  PROCEDURE RC_RETORNA_RULE_ELEMENT(PV_DESSHORT     IN VARCHAR2,
                                                                                        PN_value_max_nointento             OUT NUMBER,
                                                                                        PN_value_max_trx_encolada        OUT NUMBER,
                                                                                        PN_value_max_trx_by_execution OUT NUMBER,
                                                                                        PN_value_max_trxcommit OUT NUMBER,
                                                                                        PN_maxretentrx OUT NUMBER,
                                                                                        PV_OUT_ERROR OUT VARCHAR2
                                                                                        ) IS

    LE_ERROR EXCEPTION;
  BEGIN
    PV_OUT_ERROR   :=NULL;
    LD_DATE_RUN := SYSDATE;
    PN_value_max_nointento:=0;
    PN_value_max_trx_encolada:=0;
    PN_value_max_trx_by_execution:=0;
    PN_value_max_trxcommit:=0;
    PN_maxretentrx:=0;

    SELECT  A.value_max_nointento,
                      A.value_max_trx_encolada,
                      A.value_max_trx_by_execution,
                      A.value_max_trxcommit,
                      A.maxretentrx
      INTO PN_value_max_nointento,
                  PN_value_max_trx_encolada,
                  PN_value_max_trx_by_execution,
                  PN_value_max_trxcommit,
                  PN_maxretentrx
      FROM TTC_RULE_ELEMENT_S A
     WHERE A.DESSHORT = PV_DESSHORT;

  EXCEPTION
     WHEN OTHERS THEN
          PV_OUT_ERROR   := 'RC_RETORNA_RULE_ELEMENT-' || ' ERROR:' ||SQLCODE || ', PROCESO:' || PV_DESSHORT || ',' ||SUBSTR(SQLERRM, 1, 300);
  END RC_RETORNA_RULE_ELEMENT;

  /******************************************************************************
  * Retorna el identificador del proceso
  ******************************************************************************/

  FUNCTION RC_RETORNA_ID_PROCESS(PV_DESSHORT IN VARCHAR2) RETURN NUMBER IS
    LN_ID_PROCESS NUMBER;
  BEGIN

    SELECT PROCESS
      INTO LN_ID_PROCESS
      FROM TTC_PROCESS_S
     WHERE UPPER(DESSHORT) = UPPER(PV_DESSHORT);

    RETURN(LN_ID_PROCESS);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;

  END RC_RETORNA_ID_PROCESS;


  /******************************************************************************
  * Maximo cantidad en segundos en que el proceso se puede quedar sleep
  ******************************************************************************/

  FUNCTION RC_RETORNA_MAX_DELAY(PV_DESSHORT IN VARCHAR2) RETURN NUMBER IS
    LN_MAX_DELAY NUMBER;
  BEGIN

    SELECT A.MAXDELAY
    INTO LN_MAX_DELAY
    FROM TTC_PROCESS_S A
    WHERE A.DESSHORT=UPPER(PV_DESSHORT);

    RETURN(LN_MAX_DELAY);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;

  END RC_RETORNA_MAX_DELAY;

  /******************************************************************************
  *Retorna la cantidad de registros que estan disponibles para segir procesando
   en el Proceso de Dispatch
  *****************************************************************************/
  function RC_RETORNA_CANT_REGIS_DISP(PV_TABLA IN Varchar2,PV_GRUPO_SUBHILO IN VARCHAR2 DEFAULT NULL) return number is
  LN_COUNT    Number;
begin
  IF PV_GRUPO_SUBHILO IS NULL THEN 
    EXECUTE IMMEDIATE 'SELECT count( DISTINCT custcode) FROM '||PV_TABLA||' WHERE co_statustrx in (''P'',''A'',''L'')' INTO LN_COUNT;
  ELSE
    EXECUTE IMMEDIATE 'SELECT count( DISTINCT custcode) FROM '||PV_TABLA||
  ' WHERE co_statustrx in (''P'',''A'',''L'') and substr(customer_id,-1,1) in ('||PV_GRUPO_SUBHILO||') '
   INTO LN_COUNT;
  END IF;
  
  return(LN_COUNT);

  EXCEPTION
  WHEN OTHERS THEN
    return(-1);
end RC_RETORNA_CANT_REGIS_DISP;

  /******************************************************************************
  * Censa los hilos que se displayan de Send y Dispatch
  *****************************************************************************/

  PROCEDURE RC_CENSO_PROCESS_IN_BCK(PN_PROCESO        IN NUMBER,
                                    PV_DESSHORT       IN VARCHAR2,
                                    PN_FILTRO                IN VARCHAR2,
                                    PN_GROUPTREAD     IN NUMBER,
                                    PN_SOSPID        IN NUMBER,
                                    PB_EXISTE         OUT BOOLEAN) IS

    LN_PROCESS   NUMBER;
    LE_ERROR EXCEPTION;

  BEGIN

    SELECT PROCESS
      INTO LN_PROCESS
      FROM TTC_BCK_PROCESS_S
     WHERE PROCESS = PN_PROCESO
       AND GROUPTREAD = PN_GROUPTREAD
       AND filterfield  = PN_FILTRO
       AND UPPER(DESSHORT) = UPPER(PV_DESSHORT)
       AND SOSPID=PN_SOSPID;

    IF LN_PROCESS IS NOT NULL THEN
      PB_EXISTE := TRUE;
    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
        PB_EXISTE := FALSE;
    WHEN OTHERS THEN
        PB_EXISTE    := FALSE;
  END RC_CENSO_PROCESS_IN_BCK;

  /*****************************************************************************/
 ---Inicio [6527] - Mejoras en el proceso de Reloj de cobranzas

  PROCEDURE RC_PROCESS_IN_BCK(PN_PROCESO    IN NUMBER,
                                    PV_DESSHORT   IN VARCHAR2,
                                    PN_FILTRO     IN VARCHAR2,
                                    PN_GROUPTREAD IN NUMBER,
                                    PB_EXISTE     OUT BOOLEAN) IS

    LN_PROCESS NUMBER;
    LE_ERROR EXCEPTION;

  BEGIN

    SELECT count(*)
      INTO LN_PROCESS
      FROM TTC_BCK_PROCESS_S
     WHERE PROCESS = PN_PROCESO
       AND GROUPTREAD = PN_GROUPTREAD
       AND filterfield = PN_FILTRO
       AND UPPER(DESSHORT) = UPPER(PV_DESSHORT);

    IF LN_PROCESS >0 THEN
      PB_EXISTE := TRUE;
    ELSE
       PB_EXISTE := FALSE;
    END IF;


  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PB_EXISTE := FALSE;
    WHEN OTHERS THEN
      PB_EXISTE := FALSE;
  END RC_PROCESS_IN_BCK;
---FIN [6527] - Mejoras en el proceso de Reloj de cobranzas

  /************************************************************************
  * Registra lo Procesos en la tabla de BCK
  **************************************************************************/

  PROCEDURE RC_REGISTRAR_BCK(PN_PROCESS       IN NUMBER,
                             PN_DESSHORT      VARCHAR2,
                             PN_GROUPTREAD    IN NUMBER,
                             PN_TREAD_NO      IN NUMBER,
                             PV_PFILTERFIELD  IN VARCHAR2,
                             PD_LASTRUNDATE   IN DATE,
                             PN_MAXDELAY      IN NUMBER,
                             PN_REFRESH_CYCLE IN NUMBER,
                             PN_SOSPID        IN NUMBER,
                             PV_OUT_ERROR     OUT VARCHAR2) IS
    LV_MENSAJE VARCHAR2(500);
  BEGIN
    LD_DATE_RUN := SYSDATE;

    INSERT INTO TTC_BCK_PROCESS_S
    VALUES
      (PN_PROCESS,
       PN_DESSHORT,
       PN_GROUPTREAD,
       PN_TREAD_NO,
       PV_PFILTERFIELD,
       PD_LASTRUNDATE,
       PN_MAXDELAY,
       PN_REFRESH_CYCLE,
       PN_SOSPID);
   COMMIT;
    PV_OUT_ERROR := 'OK';

  Exception
    WHEN DUP_VAL_ON_INDEX THEN
      LV_MENSAJE   := 'RC_REGISTRAR_BCK- Existe Otro Proceso del mismo tipo de Ejecucion en TTC_Bck_Process';
      PV_OUT_ERROR := LV_MENSAJE;
    WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE   := 'RC_REGISTRAR_BCK- ' || ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 300);
      PV_OUT_ERROR := LV_MENSAJE;
  END RC_REGISTRAR_BCK;

/************************************************************************
  * Registra lo Procesos remotos en la tabla de BCK
  **************************************************************************/

  PROCEDURE RC_REGISTRAR_BCK_REMOTO(PN_PROCESS       IN NUMBER,
                             PN_DESSHORT      VARCHAR2,
                             PN_GROUPTREAD    IN NUMBER,
                             PN_TREAD_NO      IN NUMBER,
                             PV_PFILTERFIELD  IN VARCHAR2,
                             PD_LASTRUNDATE   IN DATE,
                             PN_MAXDELAY      IN NUMBER,
                             PN_REFRESH_CYCLE IN NUMBER,
                             PN_SOSPID        IN NUMBER,
                             PV_OUT_ERROR     OUT VARCHAR2) IS
    LV_MENSAJE VARCHAR2(500);
  BEGIN
    LD_DATE_RUN := SYSDATE;

    INSERT INTO TTC_BCK_PROCESS_S
    VALUES
      (PN_PROCESS,
       PN_DESSHORT,
       PN_GROUPTREAD,
       PN_TREAD_NO,
       PV_PFILTERFIELD,
       PD_LASTRUNDATE,
       PN_MAXDELAY,
       PN_REFRESH_CYCLE,
       PN_SOSPID);

    PV_OUT_ERROR := 'OK';

  EXCEPTION
    WHEN OTHERS THEN
      LV_MENSAJE   := 'RC_REGISTRAR_BCK- ' || ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 300);
      PV_OUT_ERROR := LV_MENSAJE;
  END RC_REGISTRAR_BCK_REMOTO;

  /*************************************************************************************
  * ELIMINA DE   TTC_BCK_PROCESS Y SE ACTIVA EL TRIGGER  TTC_TRG_BCK_PROCESS
  * PARA REGISTRAR EL HISTORIAL
  **********************************************************************************/
  PROCEDURE RC_ELIMINAR_BCK(PN_PROCESO        IN NUMBER,
                            PV_DESSHORT       IN VARCHAR2,
                            PN_FILTRO         IN VARCHAR2,
                            PN_GROUPTREAD     IN NUMBER,
                            PV_OUT_ERROR OUT VARCHAR2) IS
    LV_MENSAJE VARCHAR2(500);
  BEGIN
    LD_DATE_RUN := SYSDATE;
    DELETE FROM TTC_BCK_PROCESS_S
     WHERE PROCESS = PN_PROCESO
       AND GROUPTREAD = PN_GROUPTREAD
       AND filterfield  = PN_FILTRO
       AND UPPER(DESSHORT) = UPPER(PV_DESSHORT);

    COMMIT;

    PV_OUT_ERROR := NULL;

  EXCEPTION
     WHEN OTHERS THEN
          ROLLBACK;
          LV_MENSAJE   := 'RC_ELIMINAR_BCK- ' || ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 300);
          PV_OUT_ERROR := LV_MENSAJE;
  END RC_ELIMINAR_BCK;

/*************************************************************************************
  * ELIMINA DE   TTC_BCK_PROCESS Y SE ACTIVA EL TRIGGER  TTC_TRG_BCK_PROCESS
  * PARA REGISTRAR EL HISTORIAL
  **********************************************************************************/
  PROCEDURE RC_ELIMINAR_BCK_REMOTO(PN_PROCESO        IN NUMBER,
                                   PV_DESSHORT       IN VARCHAR2,
                                   PN_FILTRO         IN VARCHAR2,
                                   PN_GROUPTREAD     IN NUMBER,
                                   PV_OUT_ERROR OUT VARCHAR2) IS
    LV_MENSAJE VARCHAR2(500);
  BEGIN
    LD_DATE_RUN := SYSDATE;
    DELETE FROM TTC_BCK_PROCESS_S
     WHERE PROCESS = PN_PROCESO
       AND GROUPTREAD = PN_GROUPTREAD
       AND filterfield  = PN_FILTRO
       AND UPPER(DESSHORT) = UPPER(PV_DESSHORT);

    PV_OUT_ERROR := NULL;

  EXCEPTION
    WHEN OTHERS THEN
      LV_MENSAJE   := 'RC_ELIMINAR_BCK- ' || ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR := LV_MENSAJE;
  END RC_ELIMINAR_BCK_REMOTO;



  /*******************************************************************************************
  *
  *******************************************************************************************/

  PROCEDURE RC_ACTUALIZAR_PROGRAMMER(PN_PROCESO     IN NUMBER,
                                     PN_SEQNO       IN NUMBER,
                                     PD_RUNDATE     IN DATE,
                                     PD_LASTMONDATE IN DATE,
                                     PV_OUT_ERROR   OUT VARCHAR) IS
    LV_MENSAJE VARCHAR2(200);
    LE_ERROR EXCEPTION;
  BEGIN

    UPDATE ttc_process_programmer_s
       SET lastmoddate = PD_LASTMONDATE
     WHERE process = PN_PROCESO
       AND rundate = PD_RUNDATE
       AND seqno = PN_SEQNO
       AND lastmoddate IS NULL;
    COMMIT;
    PV_OUT_ERROR := NULL;

  EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         LV_MENSAJE   := 'RC_ACTUALIZAR_PROGRAMMER- ' || ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 300);
         PV_OUT_ERROR := LV_MENSAJE;
  END RC_ACTUALIZAR_PROGRAMMER;

  ---------------------------------------------------------------
  PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_DESSHORT    IN VARCHAR2,
                                    PN_PROCESO     IN NUMBER,
                                    PD_LASTMONDATE IN DATE,
                                    PV_OUT_ERROR   OUT VARCHAR) IS
     LV_MENSAJE     VARCHAR2(500);
     LE_ERROR       EXCEPTION;
     LN_SEQNO       NUMBER;
     LV_SECUEN      VARCHAR2(20);
     LV_SQL         VARCHAR2(200);
   BEGIN

     LV_SECUEN:= Substrb(PN_DESSHORT, 5, Length(PN_DESSHORT));

     if LV_SECUEN like 'Hist%'then
        LV_SECUEN:='Hist';
     end if;

     LV_SQL:= 'SELECT TTC_TRX_'||LV_SECUEN||'.NEXTVAL FROM DUAL';

    EXECUTE IMMEDIATE LV_SQL INTO LN_SEQNO;

    INSERT INTO ttc_process_programmer_s VALUES(PN_PROCESO,
                                                                                                  LN_SEQNO,
                                                                                                  PD_LASTMONDATE,
                                                                                                  sysdate);

    COMMIT;
    PV_OUT_ERROR:= NULL;

    EXCEPTION
         WHEN OTHERS THEN
           ROLLBACK;
           LV_MENSAJE := 'RC_REGISTRAR_PROGRAMMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 300);
           PV_OUT_ERROR:=LV_MENSAJE;
  END RC_REGISTRAR_PROGRAMMER;

   ---------------------------------------------------------------
  PROCEDURE RC_REGISTRAR_PROGRAMMER_REMOTO(PN_DESSHORT    IN VARCHAR2,
                                           PN_PROCESO     IN NUMBER,
                                           PD_LASTMONDATE IN DATE,
                                           PV_OUT_ERROR   OUT VARCHAR2) IS
     LV_MENSAJE     VARCHAR2(500);
     LE_ERROR       EXCEPTION;
     LN_SEQNO       NUMBER;
     LV_SECUEN      VARCHAR2(10);
     LV_SQL         VARCHAR2(200);
   BEGIN

     LV_SECUEN:= Substrb(PN_DESSHORT, 5, Length(PN_DESSHORT));

     if LV_SECUEN like 'Hist%'then
        LV_SECUEN:='Hist';
     end if;

     LV_SQL:= 'SELECT TTC_TRX_'||LV_SECUEN||'.NEXTVAL FROM DUAL';

    EXECUTE IMMEDIATE LV_SQL INTO LN_SEQNO;

    INSERT INTO ttc_process_programmer_s VALUES(PN_PROCESO,
                                                LN_SEQNO,
                                                PD_LASTMONDATE,
                                                sysdate);


    PV_OUT_ERROR:= NULL;

    EXCEPTION
         WHEN OTHERS THEN
           LV_MENSAJE := 'RC_REGISTRAR_PROGRAMMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
           PV_OUT_ERROR:=LV_MENSAJE;
  END RC_REGISTRAR_PROGRAMMER_REMOTO;

  /******************************************************************************
  *Descripcion: Cantidad de Registros para ser procesador por send
  ******************************************************************************/
  FUNCTION RC_RETORNA_CANT_REGIS_SEND(TIPO_SEND IN VARCHAR2,--[8693] JJI - D:para dth M:para movil
                                      TIPO IN VARCHAR2,
                                      FECHA IN VARCHAR2,
                                      CAMPO IN VARCHAR2 DEFAULT NULL,
                                      VALOR IN NUMBER DEFAULT NULL) RETURN NUMBER IS
    LN_COUNT   NUMBER;
    LV_CAMPO   VARCHAR2(20);
    LV_SQL     VARCHAR2(500);
    LV_PRGCODE VARCHAR2(4);
    
    CURSOR C_PARAM_PRGCODE IS
    select valor
      from scp.scp_parametros_procesos
     where id_parametro = 'PRGCODE_DTH'
       and id_proceso = 'RELOJ_DTH';
  BEGIN
    
    OPEN C_PARAM_PRGCODE;
    FETCH C_PARAM_PRGCODE INTO LV_PRGCODE;
    CLOSE C_PARAM_PRGCODE;
    
    IF TIPO = 'T' THEN
      LV_SQL:='select count(*) from ttc_contractplannigdetails_s where co_statustrx =''P'' and ch_timeacciondate <=
               to_date('''||FECHA||''',''dd/mm/yyyy hh24:mi:ss'')';
               
      IF TIPO_SEND='M' THEN
        LV_SQL:=LV_SQL||' and prgcode <> '||LV_PRGCODE;
      ELSE
        LV_SQL:=LV_SQL||' and prgcode = '||LV_PRGCODE;
      END IF;
      
      EXECUTE IMMEDIATE LV_SQL INTO LN_COUNT;
               
      /*select count(*)
      INTO LN_COUNT
      from ttc_contractplannigdetails_s
      where co_statustrx ='P' and ch_timeacciondate <= to_date(FECHA,'dd/mm/yyyy hh24:mi:ss') And
      LASTMODDATE Is Null;*/
    ELSE
      IF CAMPO = 'CYCLE' THEN
        LV_CAMPO:='ADMIN'||CAMPO;
      ELSE
        LV_CAMPO:=CAMPO;
      END IF;
      
      LV_SQL:='SELECT COUNT(*)
                        FROM ttc_contractplannigdetails_s
                        WHERE co_statustrx =''P'' and ch_timeacciondate <= to_date(:C1,''dd/mm/yyyy hh24:mi:ss'') AND '
                        ||LV_CAMPO||' = decode(:C2,''PRGCODE'',:C3,''CYCLE'',:C4,''BILLCYCLE'',:C5)';
                        
      IF TIPO_SEND='M' THEN
        LV_SQL:=LV_SQL||' and prgcode <> '||LV_PRGCODE;
      ELSE
        LV_SQL:=LV_SQL||' and prgcode = '||LV_PRGCODE;
      END IF;
      
      EXECUTE IMMEDIATE LV_SQL INTO LN_COUNT 
                        USING FECHA, CAMPO, VALOR, VALOR, VALOR;

      /*EXECUTE IMMEDIATE 'SELECT COUNT(*)
                        FROM ttc_contractplannigdetails_s
                        WHERE co_statustrx =''P'' and ch_timeacciondate <= to_date(:C1,''dd/mm/yyyy hh24:mi:ss'') AND '
                        ||LV_CAMPO||' = decode(:C2,''PRGCODE'',:C3,''CYCLE'',:C4,''BILLCYCLE'',:C5)' INTO LN_COUNT 
                        USING FECHA, CAMPO, VALOR, VALOR, VALOR;*/
    END IF;

    RETURN(LN_COUNT);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('RC_RETORNA_CANT_REGIS_SEND- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80));
      RETURN 0;
  END RC_RETORNA_CANT_REGIS_SEND;
 /******************************************************************************
  *Descripcion: Cantidad de Registros para ser procesador por IN_TO_WORK
  ******************************************************************************/
  FUNCTION RC_RETORNA_CANT_REGIS_CUSTOMER(FV_FILTRO IN VARCHAR2,
                                      FV_GROUPTHREAD IN VARCHAR2) RETURN NUMBER IS
    LN_COUNT   NUMBER;
    LV_SQL   VARCHAR2(500);
  BEGIN
    
    LV_SQL:='SELECT COUNT(*) FROM reloj.TTC_VIEW_CUSTOMERS_IN';
  
    IF (FV_FILTRO IS NOT NULL AND FV_GROUPTHREAD IS NOT NULL) THEN
      LV_SQL:=LV_SQL||' WHERE '||FV_FILTRO||' = '||FV_GROUPTHREAD;
    END IF;
      
    EXECUTE IMMEDIATE LV_SQL INTO LN_COUNT;
   
    RETURN(LN_COUNT);
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80));
      RETURN 0;
  END RC_RETORNA_CANT_REGIS_CUSTOMER;
  /*****************************************************************************
   *Descripcion: Guarda los eventos en la tabla Log
  *****************************************************************************/
  PROCEDURE RC_SAVE_LOG_INOUT(PN_RC_PROCESS  IN NUMBER,
                              PD_FECHA_START IN DATE,
                              PD_FECHA_STOP  IN DATE,
                              PV_STATUS      IN VARCHAR2,
                              PV_ERROR       IN VARCHAR2) IS
  BEGIN

    INSERT INTO TTC_LOG_INOUT_S
      (PROCESS, RUNDATE, LASTDATE, STATUSPROCESS, COMMENT_RUN)
    VALUES
      (PN_RC_PROCESS, PD_FECHA_START, PD_FECHA_STOP, PV_STATUS, PV_ERROR);
    COMMIT;
  END RC_SAVE_LOG_INOUT;
  /*****************************************************************************
   *Descripcion: Guarda los eventos en la tabla Log
  *****************************************************************************/

  PROCEDURE RC_SAVE_LOG_SEND(PN_RC_PROCESS  IN NUMBER,
                             PD_FECHA_START IN DATE,
                             PD_FECHA_STOP  IN DATE,
                             PV_STATUS      IN VARCHAR2,
                             PV_ERROR       IN VARCHAR2) IS
  BEGIN

    INSERT INTO TTC_LOG_SEND_S
      (PROCESS, RUNDATE, LASTDATE, STATUSPROCESS, COMMENT_RUN)
    VALUES
      (PN_RC_PROCESS, PD_FECHA_START, PD_FECHA_STOP, PV_STATUS, PV_ERROR);
    COMMIT;
  END RC_SAVE_LOG_SEND;

  /*****************************************************************************
   *Descripcion: Guarda los eventos en la tabla Log
  *****************************************************************************/

  PROCEDURE RC_SAVE_LOG_DISPATCH(PN_RC_PROCESS  IN NUMBER,
                                 PD_FECHA_START IN DATE,
                                 PD_FECHA_STOP  IN DATE,
                                 PV_STATUS      IN VARCHAR2,
                                 PV_ERROR       IN VARCHAR2) IS
  BEGIN

    INSERT INTO TTC_LOG_DISPACTCH_S
      (PROCESS, RUNDATE, LASTDATE, STATUSPROCESS, COMMENT_RUN)
    VALUES
      (PN_RC_PROCESS, PD_FECHA_START, PD_FECHA_STOP, PV_STATUS, PV_ERROR);
   -- COMMIT;
  END RC_SAVE_LOG_DISPATCH;

  FUNCTION RC_RETORNA_PROCESO_CORRECTO(PD_FECHA_EJECUCION IN DATE)
    RETURN BOOLEAN IS

    LB_CORRECTO      BOOLEAN;
    LV_FECHA_DESDE   Varchar2(20);
    LV_FECHA_HASTA   Varchar2(20);

    TYPE t_process  IS TABLE OF TTC_PROCESS.PROCESS%TYPE;
    ln_temp  t_process;

    Cursor Verifica_out(ld_dateDesde Varchar2, ld_dateHasta Varchar2) Is
        Select DISTINCT p.PROCESS
        From TTC_PROCESS_PROGRAMMER_s p, TTC_PROCESS_s pc
        Where p.PROCESS = pc.PROCESS
        And pc.DESSHORT in ('TTC_OUTA','TTC_OUTD')
        And p.LASTMODDATE between To_date(ld_dateDesde, 'dd/mm/yyyy hh24:mi:ss') and
        To_date(ld_dateHasta, 'dd/mm/yyyy hh24:mi:ss');

  BEGIN
    LV_FECHA_DESDE:=To_Char(trunc(Sysdate),'dd/mm/yyyy');
    LV_FECHA_HASTA:=TO_Char(PD_FECHA_EJECUCION,'dd/mm/yyyy hh24:mi:ss');
    LV_FECHA_DESDE:=LV_FECHA_DESDE||' 00:00:00';

    Open Verifica_out(LV_FECHA_DESDE,LV_FECHA_HASTA);
    Fetch Verifica_out Bulk COLLECT Into ln_temp;
    Close Verifica_out;

    If ln_temp.count = 2 Then
       LB_CORRECTO:=True;
    Else
       LB_CORRECTO:=False;
    End If;

    Return LB_CORRECTO;

  EXCEPTION
    WHEN OTHERS THEN
      Return FALSE;

  END RC_RETORNA_PROCESO_CORRECTO;

  FUNCTION RC_RETORNA_PROCESO_CORRECTO_SD(PD_FECHA_EJECUCION IN DATE)
    RETURN BOOLEAN IS
    LB_CORRECTO      BOOLEAN;
    LV_FECHA_DESDE   Varchar2(20);
    LV_FECHA_HASTA   Varchar2(20);

    TYPE t_process  IS TABLE OF TTC_PROCESS.PROCESS%TYPE;
    ln_temp  t_process;

    Cursor Verifica_out(ld_dateDesde Varchar2, ld_dateHasta Varchar2) Is
        Select DISTINCT p.PROCESS
        From TTC_PROCESS_PROGRAMMER p, TTC_PROCESS pc
        Where p.PROCESS = pc.PROCESS
        And pc.DESSHORT ='TTC_SEND'
        And p.LASTMODDATE between To_date(ld_dateDesde, 'dd/mm/yyyy hh24:mi:ss') and
        To_date(ld_dateHasta, 'dd/mm/yyyy hh24:mi:ss');

  BEGIN
    LV_FECHA_DESDE:=To_Char(trunc(Sysdate),'dd/mm/yyyy');
    LV_FECHA_HASTA:=TO_Char(PD_FECHA_EJECUCION,'dd/mm/yyyy hh24:mi:ss');
    LV_FECHA_DESDE:=LV_FECHA_DESDE||' 00:00:00';

    Open Verifica_out(LV_FECHA_DESDE,LV_FECHA_HASTA);
    Fetch Verifica_out Bulk COLLECT Into ln_temp;
    Close Verifica_out;

    If ln_temp.count = 1 Then
       LB_CORRECTO:=True;
    Else
       LB_CORRECTO:=False;
    End If;

    Return LB_CORRECTO;

  EXCEPTION
    WHEN OTHERS THEN
      Return FALSE;
  END RC_RETORNA_PROCESO_CORRECTO_SD;


/*****************************************************************************
   *Descripcion: Guarda el estado de la trasaccion enviada desde AXIS
  *****************************************************************************/
  PROCEDURE RC_SAVE_REMOTE_TRX_DISPATCH(LN_ID_PROCESS        IN NUMBER,
                                                                                               PV_NAME_TABLE     IN VARCHAR2,
                                                                                               LV_STATUS_TRX       IN VARCHAR2,
                                                                                               LD_FECHA_ACTUAL IN DATE,
                                                                                               LV_CODIGOERROR  IN VARCHAR2,
                                                                                               LN_CUSTOMER_ID   IN NUMBER,
                                                                                               LN_CO_ID                   IN NUMBER,
                                                                                               LV_DN_NUM              IN VARCHAR2,
                                                                                               LN_ST_SEQNO                  IN NUMBER,
                                                                                               PV_ERROR                 OUT VARCHAR2
                                                                                               ) IS
     LV_SQL_UPDATE       VARCHAR2(500);
     LV_STATUS                 VARCHAR2(1);
  BEGIN
       LV_SQL_UPDATE := 'UPDATE RELOJ.' || PV_NAME_TABLE ;
       LV_SQL_UPDATE :=LV_SQL_UPDATE|| '    SET CO_REMARKTRX = :C1,';
       LV_SQL_UPDATE :=LV_SQL_UPDATE|| '             LASTMODDATE  = :C2,';
       IF LV_STATUS_TRX IS NULL THEN
               LV_STATUS:='1';
              LV_SQL_UPDATE :=LV_SQL_UPDATE|| '  CO_NOINTENTO  = CO_NOINTENTO + :C3';
       ELSE
              LV_STATUS:=LV_STATUS_TRX;
              LV_SQL_UPDATE :=LV_SQL_UPDATE|| '  CO_STATUSTRX   = :C3';
       END IF;
       LV_SQL_UPDATE := LV_SQL_UPDATE||'  WHERE  CUSTOMER_ID     = :C4';
       LV_SQL_UPDATE := LV_SQL_UPDATE||'        AND  CO_ID                     = :C5';
       LV_SQL_UPDATE := LV_SQL_UPDATE||'        AND  DN_NUM               = :C6';
       LV_SQL_UPDATE := LV_SQL_UPDATE||'        AND  ST_SEQNO            = :C7';
       EXECUTE IMMEDIATE LV_SQL_UPDATE
                                      USING LV_CODIGOERROR,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LN_CUSTOMER_ID,
                                                    LN_CO_ID,
                                                    LV_DN_NUM,
                                                    LN_ST_SEQNO;



      EXCEPTION
        WHEN OTHERS THEN
                  PV_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REINTENTOS - ERROR NO SE PUDO ACTUALIZAR CO_NOINTENTO +1 EN LA CUENTA: CUSTOMER->' ||
                                                      LN_CUSTOMER_ID || ' CO_ID-> ' ||
                                                      LN_CO_ID || ' - ' || SQLCODE || ' ' ||
                                                       SUBSTR(SQLERRM, 1, 80);
                  RC_API_TIMETOCASH_RULE_BSCS.RC_SAVE_LOG_DISPATCH(LN_ID_PROCESS,
                                                                                                                                 LD_FECHA_ACTUAL,
                                                                                                                                  SYSDATE,
                                                                                                                                  'ERR',
                                                                                                                                  PV_ERROR
                                                                                                                                  );
  END RC_SAVE_REMOTE_TRX_DISPATCH;


/*****************************************************************************/
  FUNCTION FNC_RC_REACTIVA(PV_Custcode               IN VARCHAR2,
                                                            PN_Customer_id          IN NUMBER,
                                                            Pd_Date                         IN DATE,
                                                            PN_CO_IDTRAMITE    IN NUMBER,
                                                            PV_STATUS_P               IN VARCHAR2,
                                                            PV_Plan_Telefonia       IN VARCHAR2,
                                                            PV_STATUS_OUT         OUT VARCHAR2,
                                                            PV_Sql_Planing             OUT VARCHAR2,
                                                            PV_Sql_PlanDet            OUT VARCHAR2,
                                                            PV_REMARK                 OUT VARCHAR2,
                                                            PN_SALDO_MIN          IN NUMBER,
                                                            PN_SALDO_MIN_POR IN NUMBER,
                                                            PV_USER_AXIS              IN VARCHAR2,
                                                            PN_PK_ID                      IN NUMBER DEFAULT 0,
                                                            PN_st_seqno                 IN NUMBER DEFAULT 0,
                                                            PV_CO_STATUS_A       IN VARCHAR2  DEFAULT '29', --Datos a utilizarse desde Axis
                                                            PV_CO_STATUS_B       IN VARCHAR2  DEFAULT 'a',  --Datos a utilizarse desde Axis
                                                            PV_PERIOD                   IN NUMBER  DEFAULT 0,  --Datos a utilizarse desde Axis
                                                            PV_REMARKTRX         IN VARCHAR2 DEFAULT 'RelojAxis:Reactiva Pagos Linea',  --Datos a utilizarse desde Axis
                                                            PV_OUT_ERROR          OUT VARCHAR2
                                                            )  RETURN BOOLEAN IS

    LV_CUSTCODE              VARCHAR2(24);
    LN_SALDO_IMPAGO    FLOAT;
    LN_SALDO_REAL          FLOAT;
    LN_COUNT_CUENTA    NUMBER;
    LN_TELEFONIA_PUB    NUMBER;
    LB_LIM_CREDITO         BOOLEAN;
    LB_EXISTE                      BOOLEAN;
    LE_LIMITE_CREDITO  EXCEPTION;
    LE_EXISTE_CUENTA    EXCEPTION;
    --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
    CURSOR C_INSERT_CTASREACNR (Cn_TipoParametro NUMBER, Cv_Parametro VARCHAR2)IS
    SELECT P.VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = Cn_TipoParametro
       AND P.ID_PARAMETRO = Cv_Parametro;
    
    Lv_BandCtasReaCNR  VARCHAR2(10);
    --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
    --ini 13171 MTS   
    CURSOR c_obtiene_cuenta(cn_customer_id NUMBER)IS 
    SELECT A.CUSTCODE FROM SYSADM.CUSTOMER_ALL A WHERE A.CUSTOMER_ID =cn_customer_id;
    
    LV_BANDERA_13171 VARCHAR2(1);
    LN_PORCENTAJE_SUSP NUMBER;
    Lv_PoseeNego VARCHAR2(1);
    lv_excluyeRC  varchar2(1);
    lv_cuenta      VARCHAR2(20);
    LN_PORCENTAJE  NUMBER;
    ln_deuda_susp NUMBER;
    ln_deuda_total NUMBER;
    Lv_Error  VARCHAR2(1000);
    Ln_Error NUMBER;
    --fin MST
    
    --13200 Ini Hitss IOR 16032021
    CURSOR C_CONTRATO (CV_ID_CUENTA VARCHAR2) IS
      SELECT * FROM porta.CL_CONTRATOS@axis
      WHERE CODIGO_DOC = CV_ID_CUENTA
      AND ESTADO = 'A';
      
    LC_C_CONTRATO                   C_CONTRATO%ROWTYPE;
     
    
    CURSOR C_TIPO_CLIENTE (CV_ID_CUENTA VARCHAR2) IS
        SELECT c.id_contrato, c.codigo_doc, a.identificacion, c.estado, x.id_clase_cliente, x.descripcion
      FROM porta.CL_CONTRATOS@axis C, porta.CL_PERSONAS_EMPRESA@axis A, porta.CL_CLASES_CLIENTES@axis X
     WHERE C.CODIGO_DOC = CV_ID_CUENTA
       AND A.ID_PERSONA = C.ID_PERSONA
       AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
       AND X.ID_CLASE_CLIENTE IN ('17','23','24','26','27','28')
           /*(SELECT T.ID_CLASE_CLIENTE
              FROM porta.CL_CLASES_CLIENTES@axis T
             WHERE T.DESCRIPCION LIKE '%INS%'
                OR T.DESCRIPCION LIKE '%VIP%'
                OR T.DESCRIPCION LIKE '%PRE%')*/;
                
    LC_C_TIPO_CLIENTE               C_TIPO_CLIENTE%ROWTYPE;
                
           
    LV_BAND_CORP                    VARCHAR2(100);
    LV_CATEGORIA_CORP               VARCHAR2(50) := NULL; 
    LV_MENSAJE                      VARCHAR2(500):= NULL;
    LN_DEUDA_TOTAL_CORP             NUMBER := 0;
    LN_DEUDA_PENDIENTE_CORP         NUMBER := 0;
    LN_PORCENTAJE_DEUDA_CORP        NUMBER := 0;
    LN_RESULT_DEUDA_CORP            NUMBER := 0;
    LN_CUSTOMERID_CORP              NUMBER := 0;
    LV_PORCENTAJE_CORP              VARCHAR2(100) := NULL;
    LN_PORCENTAJE_CORP              NUMBER := 0;
    LN_PORCENTAJE_CORP_SUSP         NUMBER := 0;
    LB_EXISTE_CORP                  BOOLEAN:= FALSE;  
    --13200 Fin Hitss IOR 16032021
    LV_CALCULA_DEUDA VARCHAR2(1);
  BEGIN
    LB_EXISTE :=FALSE;
    PV_Sql_Planing:=NULL;
    PV_Sql_PlanDet:=NULL;
    PV_OUT_ERROR :=NULL;
    PV_STATUS_OUT:='P';
    IF SUBSTR(PV_Custcode, 1, 1) = '1' THEN
          LV_CUSTCODE := PV_Custcode;
    ELSE
           LV_CUSTCODE := NVL(SUBSTR(PV_Custcode,1, INSTR(PV_Custcode,'.',1,2) - 1),PV_Custcode);
    END IF;
    
    --ini 13171 MST
    OPEN C_INSERT_CTASREACNR(13171,'FLG_VAL_DEUDA_SUSP');
    FETCH C_INSERT_CTASREACNR
      INTO LV_BANDERA_13171;
    CLOSE C_INSERT_CTASREACNR;
              
    OPEN C_INSERT_CTASREACNR(13171,'PORCENTAJE_DEUDA_SUSP');
    FETCH C_INSERT_CTASREACNR
    INTO LN_PORCENTAJE_SUSP;
    CLOSE C_INSERT_CTASREACNR;
    --fin 13171
    
    --13200 Ini Hitss IOR 16032021    
    OPEN C_INSERT_CTASREACNR(13200,'FLG_DEUDA_SUSP_CORP');
    FETCH C_INSERT_CTASREACNR
      INTO LV_BAND_CORP;
    CLOSE C_INSERT_CTASREACNR;
              
    OPEN C_INSERT_CTASREACNR(13200,'PORCENTAJE_DEUDA_SUSP');
    FETCH C_INSERT_CTASREACNR
    INTO LV_PORCENTAJE_CORP;
    CLOSE C_INSERT_CTASREACNR;
    LN_PORCENTAJE_CORP_SUSP:= TO_NUMBER(LV_PORCENTAJE_CORP);
  
    --13200 Fin Hitss IOR 16032021
    
    RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_SALDO_REAL(PN_Customer_id,
                                                                                                                          PN_SALDO_MIN_POR,
                                                                                                                          LN_SALDO_REAL,
                                                                                                                          LN_SALDO_IMPAGO);

    --2- Verifica saldo con el minimo configurado por parametro y saldo impago para cuentas especiales
    OPEN c_obtiene_cuenta(PN_Customer_id);
    FETCH c_obtiene_cuenta
    INTO lv_cuenta;
    CLOSE c_obtiene_cuenta;
    
     OPEN C_INSERT_CTASREACNR(21891,'BAN_CALCULA_DEUDA');
    FETCH C_INSERT_CTASREACNR
    INTO LV_CALCULA_DEUDA;
    CLOSE C_INSERT_CTASREACNR;
    
    --13200 Ini Hitss IOR 16032021        

    IF NVL(LV_BAND_CORP,'N') = 'S' THEN --Bandera habilitada -Se aplica c�lculo con matriz de plazo

    OPEN C_CONTRATO (lv_cuenta);
    FETCH C_CONTRATO INTO LC_C_CONTRATO;
    CLOSE C_CONTRATO;
    
    --Validaci�n del tipo de cliente
    OPEN C_TIPO_CLIENTE (lv_cuenta);
    FETCH C_TIPO_CLIENTE INTO LC_C_TIPO_CLIENTE;
    LB_EXISTE_CORP:= C_TIPO_CLIENTE%FOUND;
    CLOSE C_TIPO_CLIENTE;     
                 
     --Se valida si el cliente es corporativo (matriz de plazos)
     BEGIN
       PORTA.rc_reloj_cobranzas_corp_masivo.rc_corp_categoria_cliente@axis(pn_idcontrato => LC_C_CONTRATO.ID_CONTRATO,
                                                                     pv_categoria  => LV_CATEGORIA_CORP,
                                                                     pv_mensaje    => lv_mensaje,
                                                                     pv_error      => LV_ERROR);
     EXCEPTION
         WHEN OTHERS THEN
         --ROLLBACK;
          LV_ERROR := 'rc_reloj_cobranzas_corp_masivo.rc_corp_categoria_cliente' || SQLERRM;
     end;
                 
     IF LV_CATEGORIA_CORP IS NOT NULL THEN --Cliente corporativo (matriz de plazo)
                   
       begin
        PORTA.rc_reloj_cobranzas_corp_masivo.rc_corp_deuda_cliente@axis(pn_idcontrato => LC_C_CONTRATO.ID_CONTRATO,
                                                                  pv_categoria  => LV_CATEGORIA_CORP,
                                                                  pn_deuda      => LN_DEUDA_PENDIENTE_CORP,
                                                                  PN_DEUDA_TOTAL=> LN_DEUDA_TOTAL_CORP, 
                                                                  pn_porcentaje => LN_PORCENTAJE_DEUDA_CORP,
                                                                  PN_CUSTOMER_ID=> LN_CUSTOMERID_CORP,                                                                
                                                                  pn_aplica     => LN_RESULT_DEUDA_CORP,
                                                                  pv_mensaje    => lv_mensaje,
                                                                  pv_error      => lv_error);

                                
       EXCEPTION
        WHEN OTHERS THEN
        --ROLLBACK;
        LV_ERROR := 'rc_reloj_cobranzas_corp_masivo.rc_corp_deuda_cliente' || SQLERRM;
       end;               
                   
       IF LN_DEUDA_TOTAL_CORP = 0 THEN
         LN_PORCENTAJE_CORP := 0;                     
       ELSIF LN_DEUDA_TOTAL_CORP > 0 THEN 
         LN_PORCENTAJE_CORP := (LN_DEUDA_PENDIENTE_CORP / LN_DEUDA_TOTAL_CORP)*100;
       END IF;
       
       IF (LN_PORCENTAJE_CORP <= LN_PORCENTAJE_CORP_SUSP) THEN
         BEGIN
         --Para implementar si se requiere validar el estatus en linea para casos especiales
          IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                NULL;
          END IF;
          
          LB_EXISTE:=TRUE;
          
          --3- Verifica limite de credito
          LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
          IF LB_LIM_CREDITO THEN
                PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                PV_STATUS_OUT:='L';
                RAISE LE_LIMITE_CREDITO;
          END IF;
          
          --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
          OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
          FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
          CLOSE VERIFICA_CUENTA_REAC;
          IF LN_COUNT_CUENTA> 0 THEN
                PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                PV_STATUS_OUT:='A';
                RAISE LE_EXISTE_CUENTA;          
          END IF;
          
          --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
          IF PV_Plan_Telefonia  IS NOT NULL THEN
                LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                       PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                       PD_Fecha => Pd_Date,
                                                                                                       PV_STATUS_P =>PV_STATUS_P
                                                                                                      );
                 IF LN_TELEFONIA_PUB>0 THEN
                      PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                      PV_STATUS_OUT:='A';
                      RAISE LE_EXISTE_CUENTA;
                 END IF;
          END IF;
          PV_STATUS_OUT:='A';
                           
         --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
         OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
         FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
         CLOSE C_INSERT_CTASREACNR;
                           
         IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                           
         --6- Graba los registros en la tabla
          INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
          VALUES
            (LV_CUSTCODE,
             PN_CO_IDTRAMITE,
             PV_CO_STATUS_A,
             PV_CO_STATUS_B,
             PV_PERIOD,
             LN_SALDO_REAL,
             SYSDATE,
             PV_USER_AXIS,
             PV_STATUS_P,
             PV_REMARKTRX
             );
         ELSE  
                             
           IF PV_STATUS_P = 'P' THEN
              INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                              FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                              FECHA,USUARIO,TRANSACCION,
                              REINTENTO)
                      VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                              NULL,1,NULL,
                              SYSDATE,PV_USER_AXIS,200,
                              NULL);
           END IF;
         END IF;
           --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo          
         
         --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
         PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
         PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
         PV_Sql_Planing := NULL;

      EXCEPTION
          WHEN NO_DATA_FOUND THEN
                PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

          WHEN LE_EXISTE_CUENTA THEN
                 --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                  PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                  PV_Sql_Planing :=  NULL;

          WHEN LE_LIMITE_CREDITO THEN
                  --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                  PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

          WHEN OTHERS THEN
                    PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                    PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
      END;
         
       END IF; 
       
       ELSIF LB_EXISTE_CORP = FALSE THEN --Sino aplica a matriz de plazos y no es cliente corporativo voz
         
       IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
          PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                        PV_NEGOCIACION => Lv_PoseeNego,
                                                        PV_EXCLUYERC => Lv_ExcluyeRC,
                                                        PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                       
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                       
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                       
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                         
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
         
              END IF;
           
           ELSE
             --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
             IF (NVL(LV_CALCULA_DEUDA,'N') = 'S') THEN 
                RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID,
                                   ln_deuda_total,
                                   LN_ERROR,
                                   LV_ERROR);
             ELSE   
                OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
               END IF; 
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
               IF ln_deuda_susp IS NULL OR ln_deuda_susp = '' THEN 
                     
                  IF ln_deuda_total = 0 THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (LN_SALDO_REAL/ ln_deuda_total)*100;  
                   END IF; 
                  
               ELSE                
                   IF ln_deuda_total = 0 THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
                   END IF;  
               END IF;                
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                           
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                           
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                           
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                             
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
             
               END IF;
           END IF;   
    
    ELSE--Fin 13171
        ---sigue flujo actual
        IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR
           (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN
              BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                     IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                     END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                     
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                     
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                     
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                       
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                       END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

             EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
             END;
       
       END IF;
        
    END IF;
       
       END IF; --Fin matriz de plazos
       
       ELSE --Bandera inhabilitada - No se aplica c�lculo con matriz de plazos
       
       IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
          PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                        PV_NEGOCIACION => Lv_PoseeNego,
                                                        PV_EXCLUYERC => Lv_ExcluyeRC,
                                                        PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                       
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                       
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                       
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                         
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
         
              END IF;
           
           ELSE
             --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
           IF (NVL(LV_CALCULA_DEUDA ,'N')= 'S') THEN 
                RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID,
                                   ln_deuda_total,
                                   LN_ERROR,
                                   LV_ERROR);
             ELSE    
           
                OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
             END IF;   
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
                              
                IF ln_deuda_susp IS NULL OR ln_deuda_susp = '' THEN 
                     
                  IF ln_deuda_total = 0 THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (LN_SALDO_REAL/ ln_deuda_total)*100;  
                   END IF; 
                  
               ELSE  
                 IF ln_deuda_total = 0 THEN 
                    LN_PORCENTAJE:=0;
                 ELSE    
                    LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
                 END IF;   
               END IF;             
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                           
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                           
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                           
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                             
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
             
               END IF;
           END IF;   
    
    ELSE--Fin 13171
        ---sigue flujo actual
        IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR
           (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN
              BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                     IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                     END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                     
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                     
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                     
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                       
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                       END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

             EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
             END;
       
       END IF;
        
    END IF;       
       
       END IF; --Fin 13200 Hitss IOR 16032021 
       
       
    --Ini 13200 Hitss IOR 16032021
    --Se comenta porque se incluye en l�gica de matriz de plazos
    /*IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
          PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                        PV_NEGOCIACION => Lv_PoseeNego,
                                                        PV_EXCLUYERC => Lv_ExcluyeRC,
                                                        PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                       
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                       
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                       
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                         
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
         
              END IF;
           
           ELSE
             --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
             OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
                              
               IF ln_deuda_total = 0 THEN 
                  LN_PORCENTAJE:=0;
               ELSE    
                  LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
               END IF;              
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                    IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                    END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                           
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                           
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                           
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                             
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                     END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
                    END;
             
               END IF;
           END IF;   
    
    ELSE--Fin 13171
        ---sigue flujo actual
        IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR
           (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN
              BEGIN
                    --Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                          NULL;
                    END IF;
                    --
                    LB_EXISTE:=TRUE;
                     --3- Verifica limite de credito
                    LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                    IF LB_LIM_CREDITO THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por l�mite de cr�dito';
                          PV_STATUS_OUT:='L';
                          RAISE LE_LIMITE_CREDITO;
                    END IF;

                    --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                    OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                    FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                    CLOSE VERIFICA_CUENTA_REAC;
                     IF LN_COUNT_CUENTA> 0 THEN
                          PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                          PV_STATUS_OUT:='A';
                          RAISE LE_EXISTE_CUENTA;
                     END IF;

                     --5- Verifica si es un plan de telefon�a no debe enviarse a reactivar
                      IF PV_Plan_Telefonia  IS NOT NULL THEN
                            LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                                   PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                                   PD_Fecha => Pd_Date,
                                                                                                                   PV_STATUS_P =>PV_STATUS_P
                                                                                                                  );
                             IF LN_TELEFONIA_PUB>0 THEN
                                  PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefon�a, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                                  PV_STATUS_OUT:='A';
                                  RAISE LE_EXISTE_CUENTA;
                             END IF;
                      END IF;
                      PV_STATUS_OUT:='A';
                     
                     --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                     OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                     FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                     CLOSE C_INSERT_CTASREACNR;
                     
                     IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                     
                     --6- Graba los registros en la tabla
                      INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                      VALUES
                        (LV_CUSTCODE,
                         PN_CO_IDTRAMITE,
                         PV_CO_STATUS_A,
                         PV_CO_STATUS_B,
                         PV_PERIOD,
                         LN_SALDO_REAL,
                         SYSDATE,
                         PV_USER_AXIS,
                         PV_STATUS_P,
                         PV_REMARKTRX
                         );
                     ELSE  
                       
                       IF PV_STATUS_P = 'P' THEN
                          INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                          FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                          FECHA,USUARIO,TRANSACCION,
                                          REINTENTO)
                                  VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                          NULL,1,NULL,
                                          SYSDATE,PV_USER_AXIS,200,
                                          NULL);
                       END IF;
                       END IF;
                       --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                       --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                       PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                       PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                       PV_Sql_Planing := NULL;

             EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                              PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                        WHEN LE_EXISTE_CUENTA THEN
                               --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                                PV_Sql_Planing :=  NULL;

                        WHEN LE_LIMITE_CREDITO THEN
                                --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                                PV_Sql_PlanDet := 'UPDATE  \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                        WHEN OTHERS THEN
                                  PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                                  PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
             END;
       
       END IF;
        
    END IF;*/
    --Fin 13200 Hitss IOR 16032021
    RETURN (LB_EXISTE);

  EXCEPTION
          WHEN OTHERS THEN
              PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR : '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
              RETURN FALSE;
  END FNC_RC_REACTIVA;

--9833 INI AAM
  FUNCTION FNC_RC_REACTIVA_RELOJ(PV_Custcode               IN VARCHAR2,
                                                            PN_Customer_id          IN NUMBER,
                                                            Pd_Date                         IN DATE,
                                                            PN_CO_IDTRAMITE    IN NUMBER,
                                                            PV_STATUS_P               IN VARCHAR2,
                                                            PV_Plan_Telefonia       IN VARCHAR2,
                                                            PV_STATUS_OUT         OUT VARCHAR2,
                                                            PV_Sql_Planing             OUT VARCHAR2,
                                                            PV_Sql_PlanDet            OUT VARCHAR2,
                                                            PV_REMARK                 OUT VARCHAR2,
                                                            PN_SALDO_MIN          IN NUMBER,
                                                            PN_SALDO_MIN_POR IN NUMBER,
                                                            PV_USER_AXIS              IN VARCHAR2,
                                                            PN_PK_ID                      IN NUMBER DEFAULT 0,
                                                            PN_st_seqno                 IN NUMBER DEFAULT 0,
                                                            PV_CO_STATUS_A       IN VARCHAR2  DEFAULT '29', --Datos a utilizarse desde Axis
                                                            PV_CO_STATUS_B       IN VARCHAR2  DEFAULT 'a',  --Datos a utilizarse desde Axis
                                                            PV_PERIOD                   IN NUMBER  DEFAULT 0,  --Datos a utilizarse desde Axis
                                                            PV_REMARKTRX         IN VARCHAR2 DEFAULT 'RelojAxis:Reactiva Pagos Linea',  --Datos a utilizarse desde Axis
                                                            PN_DEUDA              OUT FLOAT,
                                                            PN_SALDO              OUT FLOAT,
                                                            PV_OUT_ERROR          OUT VARCHAR2,
                                                            PN_INSERT             IN NUMBER DEFAULT 0
                                                            )  RETURN BOOLEAN IS

    LV_CUSTCODE              VARCHAR2(24);
    LN_SALDO_IMPAGO    FLOAT;
    LN_SALDO_REAL          FLOAT;
    LN_COUNT_CUENTA    NUMBER;
    LN_TELEFONIA_PUB    NUMBER;
    LB_LIM_CREDITO         BOOLEAN;
    LB_EXISTE                      BOOLEAN;
    LN_DEUDA           FLOAT;
    LE_LIMITE_CREDITO  EXCEPTION;
    LE_EXISTE_CUENTA    EXCEPTION;
    --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
    CURSOR C_INSERT_CTASREACNR (Cn_TipoParametro NUMBER, Cv_Parametro VARCHAR2)IS
    SELECT P.VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = Cn_TipoParametro
       AND P.ID_PARAMETRO = Cv_Parametro;
    
    Lv_BandCtasReaCNR  VARCHAR2(10);
    --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

  BEGIN
    LB_EXISTE :=FALSE;
    PV_Sql_Planing:=NULL;
    PV_Sql_PlanDet:=NULL;
    PV_OUT_ERROR :=NULL;
    PV_STATUS_OUT:='P';
    IF SUBSTR(PV_Custcode, 1, 1) = '1' THEN
          LV_CUSTCODE := PV_Custcode;
    ELSE
           LV_CUSTCODE := NVL(SUBSTR(PV_Custcode,1, INSTR(PV_Custcode,'.',1,2) - 1),PV_Custcode);
    END IF;

    RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_SALDO_REAL_REACT(PN_Customer_id,
                                                            PN_SALDO_MIN_POR,
                                                            LN_SALDO_REAL,
                                                            LN_DEUDA,
                                                            LN_SALDO_IMPAGO);

    --2- Verifica saldo con el minimo configurado por parametro y saldo impago para cuentas especiales
    IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR
       (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN
          BEGIN
                --Para implementar si se requiere validar el estatus en linea para casos especiales
                IF PN_PK_ID>0 AND PN_st_seqno>0  THEN
                      NULL;
                END IF;
                --
                LB_EXISTE:=TRUE;
                 --3- Verifica limite de credito
                LB_LIM_CREDITO := RC_API_TIMETOCASH_RULE_AXIS.RC_VERIFICAR_LIM_CREDITO(LV_CUSTCODE);
                IF LB_LIM_CREDITO THEN
                      PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', No es enviada a la cola de reactivacion por límite de crédito';
                      PV_STATUS_OUT:='L';
                      RAISE LE_LIMITE_CREDITO;
                END IF;

                --4- Verifica si la cuenta ya se encuentra ingresada a la tabla
                OPEN VERIFICA_CUENTA_REAC(LV_CUSTCODE, Pd_Date);
                FETCH VERIFICA_CUENTA_REAC INTO LN_COUNT_CUENTA;
                CLOSE VERIFICA_CUENTA_REAC;
                 IF LN_COUNT_CUENTA> 0 THEN
                      PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ',Ya se encuentra registrada en la tabla de reactivaciones TTC_CUSTOMERS_REACTIVA_S';
                      PV_STATUS_OUT:='A';
                      RAISE LE_EXISTE_CUENTA;
                 END IF;

                 --5- Verifica si es un plan de telefonía no debe enviarse a reactivar
                  IF PV_Plan_Telefonia  IS NOT NULL THEN
                        LN_TELEFONIA_PUB:=FNC_PLAN_TELEFONIA(PN_Customer_id => PN_Customer_id,
                                                                                                               PV_Plan_Telefonia =>to_number(PV_Plan_Telefonia) ,
                                                                                                               PD_Fecha => Pd_Date,
                                                                                                               PV_STATUS_P =>PV_STATUS_P
                                                                                                              );
                         IF LN_TELEFONIA_PUB>0 THEN
                              PV_REMARK := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE || ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                              PV_STATUS_OUT:='A';
                              RAISE LE_EXISTE_CUENTA;
                         END IF;
                  END IF;
                  PV_STATUS_OUT:='A';
                  
                 --INI [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo
                 OPEN C_INSERT_CTASREACNR (10695,'BAND_INSERT_CTASREACNR');
                 FETCH C_INSERT_CTASREACNR INTO Lv_BandCtasReaCNR;
                 CLOSE C_INSERT_CTASREACNR;
                 
                 IF NVL(Lv_BandCtasReaCNR,'N') = 'N' THEN
                   
                 --6- Graba los registros en la tabla
                  INSERT INTO TTC_CUSTOMERS_REACTIVA_S  NOLOGGING
                  VALUES
                    (LV_CUSTCODE,
                     PN_CO_IDTRAMITE,
                     PV_CO_STATUS_A,
                     PV_CO_STATUS_B,
                     PV_PERIOD,
                     LN_SALDO_REAL,
                     SYSDATE,
                     PV_USER_AXIS,
                     PV_STATUS_P,
                     PV_REMARKTRX
                     );
                 ELSE  
                   
                   IF PN_INSERT <> 1 AND PV_STATUS_P = 'P' THEN
                      INSERT INTO RC_BSCS_CTAS_REACNR@AXIS(CUENTA,VALOR,SALDO,
                                    FORMA_PAGO,ESTADO_PROCESO,ESTADO_CUENTA,
                                    FECHA,USUARIO,TRANSACCION,
                                    REINTENTO)
                            VALUES (LV_CUSTCODE,0,LN_SALDO_REAL,
                                    NULL,1,NULL,
                                    SYSDATE,PV_USER_AXIS,200,
                                    NULL);
                   END IF;
                 END IF;
                   --FIN [10695] 06/06/2016 IRO ABA Reloj suspende y no verifica saldo

                   --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                   PV_REMARK:='Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                   PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                   PV_Sql_Planing := NULL;

         EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                          PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' || PN_Customer_id || ' - CUSCODE->'||PV_Custcode;

                    WHEN LE_EXISTE_CUENTA THEN
                           --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS y TTC_CONTRACTPLANNIG_S porque se encuentran con pagos
                            PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                            PV_Sql_Planing :=  NULL;

                    WHEN LE_LIMITE_CREDITO THEN
                            --Update a los Detalles TTC_CONTRACTPLANNIGDETAILS porque se encuentran con pagos
                            PV_Sql_PlanDet := 'UPDATE  /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';

                    WHEN OTHERS THEN
                              PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                              PN_Customer_id || ' - CUSCODE->'||PV_Custcode||' ERROR: '||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 150);
         END;
    END IF;
   PN_DEUDA := LN_DEUDA;
   PN_SALDO := LN_SALDO_REAL;

    RETURN (LB_EXISTE);
  END FNC_RC_REACTIVA_RELOJ;
--9833 FIN AAM

FUNCTION FNC_PLAN_TELEFONIA(PN_Customer_id    IN NUMBER,
                                                                   PV_Plan_Telefonia IN VARCHAR2,
                                                                   PD_Fecha                 IN DATE,
                                                                   PV_STATUS_P         IN VARCHAR2
                                                                   ) RETURN NUMBER IS
  LN_Count  NUMBER;
  BEGIN
          OPEN  C_Plan_TelefoniaPublica ( PN_Customer_id,PV_Plan_Telefonia,PD_Fecha, PV_STATUS_P );
        FETCH C_Plan_TelefoniaPublica INTO LN_Count;
        CLOSE C_Plan_TelefoniaPublica;

    RETURN(LN_COUNT);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;
  END FNC_PLAN_TELEFONIA;

/* FUNCION CREADA PARA OBTENER MEDIANTE PARAMETROS LA HORA INICIO Y FIN
   PARA EL PROCESO DE SUSPENSION. 10695 IRO-JGA
*/
FUNCTION FNC_MAX_MIN_HORA  (  Pv_parametro  IN VARCHAR2,
                              Pv_proceso    IN VARCHAR2 ) RETURN VARCHAR2 IS


CURSOR C_HORA_MIN_MAX (cv_parametro VARCHAR2, cv_proceso VARCHAR2 )IS
 SELECT S.VALOR
 FROM scp.scp_parametros_procesos S
 WHERE S.ID_PARAMETRO = cv_parametro
 AND  S.ID_PROCESO = cv_proceso;


LV_VALOR   VARCHAR2 (300):=null;

 BEGIN
 
     open C_HORA_MIN_MAX (Pv_parametro,Pv_proceso) ;
     fetch C_HORA_MIN_MAX into  LV_VALOR;                    
     CLOSE C_HORA_MIN_MAX;
     
     IF pv_parametro = 'RC_INI_AMBT' AND LV_VALOR IS NULL THEN
        LV_VALOR := '24';
        
     ELSIF  pv_parametro = 'RC_FIN_AMBT' AND LV_VALOR IS NULL THEN
            LV_VALOR := '22';  
     END IF;      

 RETURN LV_VALOR;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
       IF pv_parametro = 'RC_INI_AMBT'  THEN
        LV_VALOR := '24';        
       ELSIF  pv_parametro = 'RC_FIN_AMBT'  THEN
            LV_VALOR := '22';  
       END IF;   
      
      RETURN LV_VALOR;
   
   WHEN OTHERS THEN
      IF pv_parametro = 'RC_INI_AMBT'  THEN
        LV_VALOR := '24';        
       ELSIF  pv_parametro = 'RC_FIN_AMBT'  THEN
            LV_VALOR := '22';  
       END IF;   
      
      RETURN LV_VALOR;

END FNC_MAX_MIN_HORA;

--10995 INI AAM
FUNCTION FNC_RC_REACTIVA_RELOJ_2(PV_Custcode       IN VARCHAR2,
                                 PN_Customer_id    IN NUMBER,
                                 Pd_Date           IN DATE,
                                 PV_STATUS_P       IN VARCHAR2,
                                 PV_Plan_Telefonia IN VARCHAR2 DEFAULT NULL,
                                 PV_STATUS_OUT     OUT VARCHAR2,
                                 PV_Sql_Planing    OUT VARCHAR2,
                                 PV_Sql_PlanDet    OUT VARCHAR2,
                                 PV_REMARK         OUT VARCHAR2,
                                 PN_SALDO_MIN      IN NUMBER,
                                 PN_SALDO_MIN_POR  IN NUMBER,
                                 PN_PK_ID          IN NUMBER DEFAULT 0,
                                 PN_st_seqno       IN NUMBER DEFAULT 0,
                                 PN_DEUDA          OUT FLOAT,
                                 PN_SALDO          OUT FLOAT,
                                 PV_OUT_ERROR      OUT VARCHAR2,
                                 PD_FECHA_CORTE    OUT DATE) RETURN BOOLEAN IS

  LV_CUSTCODE      VARCHAR2(24);
  LN_SALDO_IMPAGO  FLOAT;
  LN_SALDO_REAL    FLOAT;
  LN_TELEFONIA_PUB NUMBER;
  LB_EXISTE        BOOLEAN;
  LN_DEUDA         FLOAT;
  LE_LIMITE_CREDITO EXCEPTION;
  LE_EXISTE_CUENTA  EXCEPTION;

  CURSOR C_FECHA_CORTE(CN_Customer_id NUMBER) IS
    SELECT NVL(X.OHDUEDATE, TRUNC(SYSDATE))
      FROM (SELECT /*+ INDEX(O,OH_CUST_ID_1)*/
             A.OHXACT, A.OHDUEDATE
              FROM SYSADM.ORDERHDR_ALL A
             WHERE A.CUSTOMER_ID = CN_CUSTOMER_ID
               AND A.OHSTATUS = 'IN'
               AND A.OHDUEDATE < TRUNC(SYSDATE)
             ORDER BY A.OHXACT DESC) X
     WHERE ROWNUM = 1;
   --ini 13171 MTS
    CURSOR C_OBTIENE_PARAMETRO (cn_id_parametro number, cv_parametro VARCHAR2) IS
    SELECT A.VALOR
      FROM GV_PARAMETROS A
     WHERE A.ID_TIPO_PARAMETRO = cn_id_parametro 
       AND A.ID_PARAMETRO = cv_parametro; 
       
    CURSOR c_obtiene_cuenta(cn_customer_id NUMBER)IS 
    SELECT A.CUSTCODE FROM SYSADM.CUSTOMER_ALL A WHERE A.CUSTOMER_ID =cn_customer_id;
    
    LV_BANDERA_13171 VARCHAR2(1);
    LN_PORCENTAJE_SUSP NUMBER;
    Lv_PoseeNego VARCHAR2(1);
    lv_excluyeRC  varchar2(1);
    lv_cuenta      VARCHAR2(20);
    LN_PORCENTAJE  NUMBER;
    ln_deuda_susp NUMBER;
    ln_deuda_total NUMBER;
    Lv_Error  VARCHAR2(1000);
    Ln_Error NUMBER;
    --fin MST
    
    --13200 Ini Hitss IOR 16032021
    CURSOR C_CONTRATO (CV_ID_CUENTA VARCHAR2) IS
      SELECT * FROM porta.CL_CONTRATOS@axis
      WHERE CODIGO_DOC = CV_ID_CUENTA
      AND ESTADO = 'A';
           
    LC_C_CONTRATO                   C_CONTRATO%ROWTYPE;
    
    CURSOR C_TIPO_CLIENTE (CV_ID_CUENTA VARCHAR2) IS
    SELECT c.id_contrato, c.codigo_doc, a.identificacion, c.estado, x.id_clase_cliente, x.descripcion
  FROM porta.CL_CONTRATOS@axis C, porta.CL_PERSONAS_EMPRESA@axis A, porta.CL_CLASES_CLIENTES@axis X
 WHERE C.CODIGO_DOC = CV_ID_CUENTA
   AND A.ID_PERSONA = C.ID_PERSONA
   AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
   AND X.ID_CLASE_CLIENTE IN ('17','23','24','26','27','28')
       /*(SELECT T.ID_CLASE_CLIENTE
          FROM porta.CL_CLASES_CLIENTES@axis T
         WHERE T.DESCRIPCION LIKE '%INS%'
            OR T.DESCRIPCION LIKE '%VIP%'
            OR T.DESCRIPCION LIKE '%PRE%')*/;
                
    LC_C_TIPO_CLIENTE               C_TIPO_CLIENTE%ROWTYPE;
    
    
    LV_BAND_CORP                    VARCHAR2(100);
    LV_CATEGORIA_CORP               VARCHAR2(50) := NULL; 
    LV_MENSAJE                      VARCHAR2(500):= NULL;
    LN_DEUDA_TOTAL_CORP             NUMBER := 0;
    LN_DEUDA_PENDIENTE_CORP         NUMBER := 0;
    LN_PORCENTAJE_DEUDA_CORP        NUMBER := 0;
    LN_RESULT_DEUDA_CORP            NUMBER := 0;
    LN_CUSTOMERID_CORP              NUMBER := 0;
    LV_PORCENTAJE_CORP              VARCHAR2(100) := NULL;
    LN_PORCENTAJE_CORP              NUMBER := 0;
    LN_PORCENTAJE_CORP_SUSP         NUMBER := 0;
    LB_EXISTE_CORP                  BOOLEAN:= FALSE;  
    --13200 Fin Hitss IOR 16032021
    LV_CALCULA_DEUDA VARCHAR2(1);
    
BEGIN
  LB_EXISTE      := FALSE;
  PV_Sql_Planing := NULL;
  PV_Sql_PlanDet := NULL;
  PV_OUT_ERROR   := NULL;
  PV_STATUS_OUT  := 'P';

  IF SUBSTR(PV_Custcode, 1, 1) = '1' THEN
    LV_CUSTCODE := PV_Custcode;
  ELSE
    LV_CUSTCODE := NVL(SUBSTR(PV_Custcode, 1, INSTR(PV_Custcode, '.', 1, 2) - 1), PV_Custcode);
  END IF;
  
  --ini 13171 MST
    OPEN C_OBTIENE_PARAMETRO(13171,'FLG_VAL_DEUDA_SUSP');
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_BANDERA_13171;
    CLOSE C_OBTIENE_PARAMETRO;
              
    OPEN C_OBTIENE_PARAMETRO(13171,'PORCENTAJE_DEUDA_SUSP');
    FETCH C_OBTIENE_PARAMETRO
    INTO LN_PORCENTAJE_SUSP;
    CLOSE C_OBTIENE_PARAMETRO;
    --fin 13171
        
    --13200 Ini Hitss IOR 16032021    
    OPEN C_OBTIENE_PARAMETRO(13200,'FLG_DEUDA_SUSP_CORP');
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_BAND_CORP;
    CLOSE C_OBTIENE_PARAMETRO;
              
    OPEN C_OBTIENE_PARAMETRO(13200,'PORCENTAJE_DEUDA_SUSP');
    FETCH C_OBTIENE_PARAMETRO
    INTO LV_PORCENTAJE_CORP;
    CLOSE C_OBTIENE_PARAMETRO;
    LN_PORCENTAJE_CORP_SUSP:= TO_NUMBER(LV_PORCENTAJE_CORP);
    --13200 Fin Hitss IOR 16032021    
     OPEN C_OBTIENE_PARAMETRO(21891,'BAN_CALCULA_DEUDA');
    FETCH C_OBTIENE_PARAMETRO
    INTO LV_CALCULA_DEUDA;
    CLOSE C_OBTIENE_PARAMETRO;

    
  RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_SALDO_REAL_REACT(PN_Customer_id,
                                                          PN_SALDO_MIN_POR,
                                                          LN_SALDO_REAL,
                                                          LN_DEUDA,
                                                          LN_SALDO_IMPAGO);
  OPEN C_FECHA_CORTE(PN_Customer_id);
  FETCH C_FECHA_CORTE INTO PD_FECHA_CORTE;
  CLOSE C_FECHA_CORTE;
  
  
  OPEN c_obtiene_cuenta(PN_Customer_id);
  FETCH c_obtiene_cuenta
  INTO lv_cuenta;
  CLOSE c_obtiene_cuenta;
  
   --13200 Ini Hitss IOR 16032021        

    IF NVL(LV_BAND_CORP,'N') = 'S' THEN --Bandera habilitada -Se aplica c�lculo con matriz de plazo

    OPEN C_CONTRATO (lv_cuenta);
    FETCH C_CONTRATO INTO LC_C_CONTRATO;
    CLOSE C_CONTRATO;
    
    
    --Validaci�n del tipo de cliente
    OPEN C_TIPO_CLIENTE (lv_cuenta);
    FETCH C_TIPO_CLIENTE INTO LC_C_TIPO_CLIENTE;
    LB_EXISTE_CORP:= C_TIPO_CLIENTE%FOUND;
    CLOSE C_TIPO_CLIENTE;   
                
     --Se valida si el cliente es corporativo (matriz de plazos)
     BEGIN
       PORTA.rc_reloj_cobranzas_corp_masivo.rc_corp_categoria_cliente@axis(pn_idcontrato => LC_C_CONTRATO.ID_CONTRATO,
                                                                     pv_categoria  => LV_CATEGORIA_CORP,
                                                                     pv_mensaje    => lv_mensaje,
                                                                     pv_error      => LV_ERROR);
     EXCEPTION
         WHEN OTHERS THEN
         --ROLLBACK;
          LV_ERROR := 'rc_reloj_cobranzas_corp_masivo.rc_corp_categoria_cliente' || SQLERRM;
     end;
                 
     IF LV_CATEGORIA_CORP IS NOT NULL THEN --Cliente corporativo (matriz de plazo)
                   
       begin
        PORTA.rc_reloj_cobranzas_corp_masivo.rc_corp_deuda_cliente@axis(pn_idcontrato => LC_C_CONTRATO.ID_CONTRATO,
                                                                  pv_categoria  => LV_CATEGORIA_CORP,
                                                                  pn_deuda      => LN_DEUDA_PENDIENTE_CORP,
                                                                  PN_DEUDA_TOTAL=> LN_DEUDA_TOTAL_CORP, 
                                                                  pn_porcentaje => LN_PORCENTAJE_DEUDA_CORP,
                                                                  PN_CUSTOMER_ID=> LN_CUSTOMERID_CORP,                                                                
                                                                  pn_aplica     => LN_RESULT_DEUDA_CORP,
                                                                  pv_mensaje    => lv_mensaje,
                                                                  pv_error      => lv_error);

                                
       EXCEPTION
        WHEN OTHERS THEN
        --ROLLBACK;
        LV_ERROR := 'rc_reloj_cobranzas_corp_masivo.rc_corp_deuda_cliente' || SQLERRM;
       end;               
                   
       IF LN_DEUDA_TOTAL_CORP = 0 THEN
         LN_PORCENTAJE_CORP := 0;                     
       ELSIF LN_DEUDA_TOTAL_CORP > 0 THEN 
         LN_PORCENTAJE_CORP := (LN_DEUDA_PENDIENTE_CORP / LN_DEUDA_TOTAL_CORP)*100;
       END IF;
       PN_SALDO:= LN_DEUDA_PENDIENTE_CORP;
       
       IF (LN_PORCENTAJE_CORP <= LN_PORCENTAJE_CORP_SUSP) THEN
         
                          BEGIN
                      -- Para implementar si se requiere validar el estatus en linea para casos especiales
                      IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                        NULL;
                      END IF;
                      LB_EXISTE := TRUE;    

                      -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                      IF PV_Plan_Telefonia IS NOT NULL THEN
                        LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                               PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                               PD_Fecha          => Pd_Date,
                                                               PV_STATUS_P       => PV_STATUS_P);
                        IF LN_TELEFONIA_PUB > 0 THEN
                          PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                           ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                          PV_STATUS_OUT := 'A';
                          RAISE LE_EXISTE_CUENTA;
                        END IF;
                      END IF;
                      PV_STATUS_OUT := 'A';
                      PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                      PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;
                    
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                      
                      WHEN LE_EXISTE_CUENTA THEN
                        PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                        PV_Sql_Planing := NULL;

                      WHEN OTHERS THEN
                        PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                        ' ERROR: ' || SQLCODE || ' ' ||
                                        SUBSTR(SQLERRM, 1, 150);
                    END;
                  
               END IF;             
       
              
       ELSIF LB_EXISTE_CORP = FALSE THEN --Sino aplica a matriz de plazos y no es cliente corporativo voz
         
         
           IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
      PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                    PV_NEGOCIACION => Lv_PoseeNego,
                                                    PV_EXCLUYERC => Lv_ExcluyeRC,
                                                    PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    -- Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                      NULL;
                    END IF;
                    LB_EXISTE := TRUE;    

                    -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                    IF PV_Plan_Telefonia IS NOT NULL THEN
                      LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                             PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                             PD_Fecha          => Pd_Date,
                                                             PV_STATUS_P       => PV_STATUS_P);
                      IF LN_TELEFONIA_PUB > 0 THEN
                        PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                         ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                        PV_STATUS_OUT := 'A';
                        RAISE LE_EXISTE_CUENTA;
                      END IF;
                    END IF;
                    PV_STATUS_OUT := 'A';
                    PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                    PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                    PV_Sql_Planing := NULL;
                  
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                    
                    WHEN LE_EXISTE_CUENTA THEN
                      PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;

                    WHEN OTHERS THEN
                      PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                      ' ERROR: ' || SQLCODE || ' ' ||
                                      SUBSTR(SQLERRM, 1, 150);
                  END;
                
              END IF;
          ELSE 
            IF (NVL(LV_CALCULA_DEUDA,'N') = 'S') THEN 
                RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID,
                                   ln_deuda_total,
                                   LN_ERROR,
                                   LV_ERROR);
             ELSE   
            --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
                OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
             END IF;   
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
                              
                IF ln_deuda_susp IS NULL OR ln_deuda_susp = '' THEN 
                     
                  IF ln_deuda_total = 0 THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (LN_SALDO_REAL/ ln_deuda_total)*100;  
                   END IF; 
                   PN_SALDO:= LN_SALDO_REAL; 
               ELSE  
                   IF( ln_deuda_total=0)THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
                   END IF;
                   PN_SALDO:= ln_deuda_susp; 
               END IF;    
               --PN_DEUDA:=ln_deuda_total;
                             
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                      -- Para implementar si se requiere validar el estatus en linea para casos especiales
                      IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                        NULL;
                      END IF;
                      LB_EXISTE := TRUE;    

                      -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                      IF PV_Plan_Telefonia IS NOT NULL THEN
                        LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                               PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                               PD_Fecha          => Pd_Date,
                                                               PV_STATUS_P       => PV_STATUS_P);
                        IF LN_TELEFONIA_PUB > 0 THEN
                          PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                           ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                          PV_STATUS_OUT := 'A';
                          RAISE LE_EXISTE_CUENTA;
                        END IF;
                      END IF;
                      PV_STATUS_OUT := 'A';
                      PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                      PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;
                    
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                      
                      WHEN LE_EXISTE_CUENTA THEN
                        PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                        PV_Sql_Planing := NULL;

                      WHEN OTHERS THEN
                        PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                        ' ERROR: ' || SQLCODE || ' ' ||
                                        SUBSTR(SQLERRM, 1, 150);
                    END;
                  
               END IF;  
                
          END IF;
  ELSE 
    ---continua el flujo actual        
  -- Verifica saldo con el minimo configurado por parametro y saldo impago para cuentas especiales
    IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN

      BEGIN
        -- Para implementar si se requiere validar el estatus en linea para casos especiales
        IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
          NULL;
        END IF;
        LB_EXISTE := TRUE;    

        -- Verifica si es un plan de telefonía no debe enviarse a reactivar
        IF PV_Plan_Telefonia IS NOT NULL THEN
          LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                 PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                 PD_Fecha          => Pd_Date,
                                                 PV_STATUS_P       => PV_STATUS_P);
          IF LN_TELEFONIA_PUB > 0 THEN
            PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                             ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
            PV_STATUS_OUT := 'A';
            RAISE LE_EXISTE_CUENTA;
          END IF;
        END IF;
        PV_STATUS_OUT := 'A';
        PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
        PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
        PV_Sql_Planing := NULL;
            
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
              
        WHEN LE_EXISTE_CUENTA THEN
          PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
          PV_Sql_Planing := NULL;

        WHEN OTHERS THEN
          PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                          ' ERROR: ' || SQLCODE || ' ' ||
                          SUBSTR(SQLERRM, 1, 150);
      END;
          
    END IF;
  
  END IF;
         
       END IF; --Fin matriz de plazos
       
       ELSE --Bandera inhabilitada - No se aplica c�lculo con matriz de plazos
         
           IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
      PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                    PV_NEGOCIACION => Lv_PoseeNego,
                                                    PV_EXCLUYERC => Lv_ExcluyeRC,
                                                    PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    -- Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                      NULL;
                    END IF;
                    LB_EXISTE := TRUE;    

                    -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                    IF PV_Plan_Telefonia IS NOT NULL THEN
                      LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                             PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                             PD_Fecha          => Pd_Date,
                                                             PV_STATUS_P       => PV_STATUS_P);
                      IF LN_TELEFONIA_PUB > 0 THEN
                        PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                         ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                        PV_STATUS_OUT := 'A';
                        RAISE LE_EXISTE_CUENTA;
                      END IF;
                    END IF;
                    PV_STATUS_OUT := 'A';
                    PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                    PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                    PV_Sql_Planing := NULL;
                  
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                    
                    WHEN LE_EXISTE_CUENTA THEN
                      PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;

                    WHEN OTHERS THEN
                      PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                      ' ERROR: ' || SQLCODE || ' ' ||
                                      SUBSTR(SQLERRM, 1, 150);
                  END;
                
              END IF;
          ELSE 
            --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
            IF NVL(LV_CALCULA_DEUDA,'N') = 'S' THEN 
                RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID,
                                   ln_deuda_total,
                                   LN_ERROR,
                                   LV_ERROR);
             ELSE   
                OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
             END IF;   
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
                              
                IF ln_deuda_susp IS NULL OR ln_deuda_susp = '' THEN 
                     
                  IF ln_deuda_total = 0 THEN 
                      LN_PORCENTAJE:=0;
                   ELSE    
                      LN_PORCENTAJE :=   (LN_SALDO_REAL/ ln_deuda_total)*100;  
                   END IF; 
                   PN_SALDO:= LN_SALDO_REAL;
               ELSE  
                 IF( ln_deuda_total=0)THEN 
                    LN_PORCENTAJE:=0;
                 ELSE    
                    LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
                 END IF;
                 PN_SALDO:= ln_deuda_susp;
               END IF;  
               --PN_DEUDA:=ln_deuda_total;
                              
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                      -- Para implementar si se requiere validar el estatus en linea para casos especiales
                      IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                        NULL;
                      END IF;
                      LB_EXISTE := TRUE;    

                      -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                      IF PV_Plan_Telefonia IS NOT NULL THEN
                        LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                               PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                               PD_Fecha          => Pd_Date,
                                                               PV_STATUS_P       => PV_STATUS_P);
                        IF LN_TELEFONIA_PUB > 0 THEN
                          PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                           ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                          PV_STATUS_OUT := 'A';
                          RAISE LE_EXISTE_CUENTA;
                        END IF;
                      END IF;
                      PV_STATUS_OUT := 'A';
                      PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                      PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;
                    
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                      
                      WHEN LE_EXISTE_CUENTA THEN
                        PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                        PV_Sql_Planing := NULL;

                      WHEN OTHERS THEN
                        PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                        ' ERROR: ' || SQLCODE || ' ' ||
                                        SUBSTR(SQLERRM, 1, 150);
                    END;
                  
               END IF;  
                
          END IF;
  ELSE 
    ---continua el flujo actual        
  -- Verifica saldo con el minimo configurado por parametro y saldo impago para cuentas especiales
    IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN

      BEGIN
        -- Para implementar si se requiere validar el estatus en linea para casos especiales
        IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
          NULL;
        END IF;
        LB_EXISTE := TRUE;    

        -- Verifica si es un plan de telefonía no debe enviarse a reactivar
        IF PV_Plan_Telefonia IS NOT NULL THEN
          LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                 PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                 PD_Fecha          => Pd_Date,
                                                 PV_STATUS_P       => PV_STATUS_P);
          IF LN_TELEFONIA_PUB > 0 THEN
            PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                             ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
            PV_STATUS_OUT := 'A';
            RAISE LE_EXISTE_CUENTA;
          END IF;
        END IF;
        PV_STATUS_OUT := 'A';
        PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
        PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
        PV_Sql_Planing := NULL;
            
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
              
        WHEN LE_EXISTE_CUENTA THEN
          PV_Sql_PlanDet := 'UPDATE /*+ RULE +*/  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
          PV_Sql_Planing := NULL;

        WHEN OTHERS THEN
          PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                          ' ERROR: ' || SQLCODE || ' ' ||
                          SUBSTR(SQLERRM, 1, 150);
      END;
          
    END IF;
  
  END IF;
         
       END IF;
       --13200 Fin Hitss IOR 16032021          
  
  --13200 Ini Hitss IOR 16032021
/*  IF(nvl(LV_BANDERA_13171,'N')= 'S')THEN
      PORTA.RCK_TRX_PROCESOS.RCP_CALCULA_NEGOCIACION@axis(PV_CUENTA => lv_cuenta,
                                                    PV_NEGOCIACION => Lv_PoseeNego,
                                                    PV_EXCLUYERC => Lv_ExcluyeRC,
                                                    PV_ERROR => LV_ERROR);
           IF Lv_PoseeNego = 'S' THEN
              IF Lv_ExcluyeRC = 'S' THEN
                 BEGIN
                    -- Para implementar si se requiere validar el estatus en linea para casos especiales
                    IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                      NULL;
                    END IF;
                    LB_EXISTE := TRUE;    

                    -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                    IF PV_Plan_Telefonia IS NOT NULL THEN
                      LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                             PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                             PD_Fecha          => Pd_Date,
                                                             PV_STATUS_P       => PV_STATUS_P);
                      IF LN_TELEFONIA_PUB > 0 THEN
                        PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                         ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                        PV_STATUS_OUT := 'A';
                        RAISE LE_EXISTE_CUENTA;
                      END IF;
                    END IF;
                    PV_STATUS_OUT := 'A';
                    PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                    PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                    PV_Sql_Planing := NULL;
                  
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                    
                    WHEN LE_EXISTE_CUENTA THEN
                      PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;

                    WHEN OTHERS THEN
                      PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                      PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                      ' ERROR: ' || SQLCODE || ' ' ||
                                      SUBSTR(SQLERRM, 1, 150);
                  END;
                
              END IF;
          ELSE 
            --si no tiene negociacion se debe calcular la deuda de susp/deuda total 
             OPEN C_DEUDA_TOTAL(PN_Customer_id);
                FETCH C_DEUDA_TOTAL
                  INTO ln_deuda_total;
                CLOSE C_DEUDA_TOTAL;
               PORTA.PCK_API_SUSPEND_DEBT_LEGADO.PRC_CALCULA_DEUDA_SUSPENSION@AXIS(PV_CUENTA=> lv_cuenta,
                                                                              PV_SERVICIO         => NULL,
                                                                              PN_DEUDA_SUSPENSION => ln_deuda_susp,
                                                                              PN_ERROR            => Ln_Error,
                                                                              PV_ERROR            => Lv_Error);
                              
               IF( ln_deuda_total=0)THEN 
                  LN_PORCENTAJE:=0;
               ELSE    
                  LN_PORCENTAJE :=   (ln_deuda_susp/ ln_deuda_total)*100;  
               END IF;
               --PN_DEUDA:=ln_deuda_total;
               PN_SALDO:= ln_deuda_susp;               
               IF (LN_PORCENTAJE <= LN_PORCENTAJE_SUSP) THEN 
                   BEGIN
                      -- Para implementar si se requiere validar el estatus en linea para casos especiales
                      IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
                        NULL;
                      END IF;
                      LB_EXISTE := TRUE;    

                      -- Verifica si es un plan de telefonía no debe enviarse a reactivar
                      IF PV_Plan_Telefonia IS NOT NULL THEN
                        LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                               PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                               PD_Fecha          => Pd_Date,
                                                               PV_STATUS_P       => PV_STATUS_P);
                        IF LN_TELEFONIA_PUB > 0 THEN
                          PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                                           ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
                          PV_STATUS_OUT := 'A';
                          RAISE LE_EXISTE_CUENTA;
                        END IF;
                      END IF;
                      PV_STATUS_OUT := 'A';
                      PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
                      PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                      PV_Sql_Planing := NULL;
                    
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
                      
                      WHEN LE_EXISTE_CUENTA THEN
                        PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
                        PV_Sql_Planing := NULL;

                      WHEN OTHERS THEN
                        PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                                        PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                                        ' ERROR: ' || SQLCODE || ' ' ||
                                        SUBSTR(SQLERRM, 1, 150);
                    END;
                  
               END IF;  
                
          END IF;
  ELSE 
    ---continua el flujo actual        
  -- Verifica saldo con el minimo configurado por parametro y saldo impago para cuentas especiales
    IF (LN_SALDO_REAL <= PN_SALDO_MIN) OR (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN

      BEGIN
        -- Para implementar si se requiere validar el estatus en linea para casos especiales
        IF PN_PK_ID > 0 AND PN_st_seqno > 0 THEN
          NULL;
        END IF;
        LB_EXISTE := TRUE;    

        -- Verifica si es un plan de telefonía no debe enviarse a reactivar
        IF PV_Plan_Telefonia IS NOT NULL THEN
          LN_TELEFONIA_PUB := FNC_PLAN_TELEFONIA(PN_Customer_id    => PN_Customer_id,
                                                 PV_Plan_Telefonia => to_number(PV_Plan_Telefonia),
                                                 PD_Fecha          => Pd_Date,
                                                 PV_STATUS_P       => PV_STATUS_P);
          IF LN_TELEFONIA_PUB > 0 THEN
            PV_REMARK     := 'REACTIVACION - La cuenta: ' || LV_CUSTCODE ||
                             ', se encuentra con un plan de telefonía, no se envia a reactivar y no se envia a suspender. Se cambia el estatus';
            PV_STATUS_OUT := 'A';
            RAISE LE_EXISTE_CUENTA;
          END IF;
        END IF;
        PV_STATUS_OUT := 'A';
        PV_REMARK      := 'Cuenta por Reactivar>>> TRX cambia el estatus a A, hasta que el proceso OUT  la elimine, luego de que se reactive';
        PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4 AND CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
        PV_Sql_Planing := NULL;
            
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          PV_OUT_ERROR := 'Rc_Trx_TimeToCash_Dispatch.RC_REACTIVA >>TTC_CONTRACTPLANNIGDETAILS_S>> ERROR al consultar los datos de la cuenta:' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode;
              
        WHEN LE_EXISTE_CUENTA THEN
          PV_Sql_PlanDet := 'UPDATE \*+ RULE +*\  TTC_CONTRACTPLANNIGDETAILS_S SET CO_STATUSTRX=:D1, CO_REMARKTRX=:D2, LASTMODDATE=:D3  WHERE customer_id = :D4  AND  CO_STATUSTRX=:D5 AND ch_timeacciondate<=SYSDATE';
          PV_Sql_Planing := NULL;

        WHEN OTHERS THEN
          PV_OUT_ERROR := 'RC_Api_TimeToCash_Rule_Bscs.RC_REACTIVA -> ERROR AL INSERTAR LA CUENTA EN REACTIVACION: CUSTOMER_ID->' ||
                          PN_Customer_id || ' - CUSCODE->' || PV_Custcode ||
                          ' ERROR: ' || SQLCODE || ' ' ||
                          SUBSTR(SQLERRM, 1, 150);
      END;
          
    END IF;
  
  END IF;*/
  --13200 Fin Hitss IOR 16032021
  
  PN_DEUDA := LN_DEUDA;
  PN_SALDO := LN_SALDO_REAL;

  RETURN(LB_EXISTE);
END FNC_RC_REACTIVA_RELOJ_2;
--10995 FIN AAM

  PROCEDURE RCP_CONSULTA_DEUDA(PN_CUSTOMER_ID IN NUMBER,
                               PN_DEUDA_TOTAL OUT NUMBER,
                               PN_ERROR       IN OUT NUMBER,
                               PV_ERROR       IN OUT VARCHAR2) IS


    --* OBTIENE DEUDA TOTAL
    CURSOR C_DEUDA_TOTAL(CN_CUSTOMER_ID NUMBER) IS
      SELECT /*+ RULE */
       NVL(SUM(OHINVAMT_DOC), 0) DEUDA
        FROM CUSTOMER_ALL C, ORDERHDR_ALL O
       WHERE O.CUSTOMER_ID = CN_CUSTOMER_ID
         AND O.CUSTOMER_ID = C.CUSTOMER_ID
         AND C.CUSTOMER_DEALER = 'C'
         AND O.OHSTATUS IN ('IN', 'CM', 'CO')
         AND O.OHOPNAMT_DOC > 0
       GROUP BY C.CUSTCODE;

    --* CREDITOS DE UNA CUENTA
    CURSOR C_CREDITOS(CN_CUSTOMER_ID NUMBER) IS
      SELECT /*+ RULE */
       NVL(SUM(AMOUNT), 0) CREDITOS
        FROM FEES B
       WHERE B.CUSTOMER_ID = CN_CUSTOMER_ID
         AND B.PERIOD > 0
         AND B.SNCODE = 46;

    CURSOR C_CARGOS_CTA(CN_CUSTOMER_ID NUMBER) IS
      SELECT NVL(SUM(OHOPNAMT_DOC), 0) DEUDA
        FROM CUSTOMER_ALL C, ORDERHDR_ALL O
       WHERE O.CUSTOMER_ID = CN_CUSTOMER_ID
         AND C.CUSTOMER_ID = O.CUSTOMER_ID
         AND C.CUSTOMER_DEALER = 'C'
         AND O.OHSTATUS IN ('CM', 'CO')
         AND O.OHOPNAMT_DOC < 0
       GROUP BY C.CUSTCODE;

    CURSOR C_FACTURAS_DEUDA(CN_CUSTOMER_ID NUMBER) IS
      SELECT O.OHINVAMT_DOC, O.OHOPNAMT_DOC
        FROM CUSTOMER_ALL C, ORDERHDR_ALL O
       WHERE O.CUSTOMER_ID = CN_CUSTOMER_ID
         AND C.CUSTOMER_ID = O.CUSTOMER_ID
         AND C.CUSTOMER_DEALER = 'C'
         AND O.OHSTATUS IN ('IN')
         AND O.OHOPNAMT_DOC > 0
       ORDER BY O.OHENTDATE ASC;

    LC_CREDITOS         C_CREDITOS%ROWTYPE;
    LN_DEUDA_TOTAL      NUMBER := 0;
    LE_ERROR EXCEPTION;
    LE_ERROR2 EXCEPTION;
    LE_ERROR3 EXCEPTION;
    LB_FOUND      BOOLEAN;
    LN_CREDITOS   NUMBER := 0;
    LV_APLICACION VARCHAR2(80) := 'RCP_CONSULTA_DEUDA - ';
    LV_CARGOSCTA  NUMBER;

  BEGIN

    --* OBTENER CREDITOS REALIZADOS DURANTE EL MES Y NO APLICADOS
    OPEN C_CREDITOS(PN_CUSTOMER_ID);
    FETCH C_CREDITOS
      INTO LC_CREDITOS;
    LB_FOUND := C_CREDITOS%FOUND;
    CLOSE C_CREDITOS;
    --
    IF LB_FOUND THEN
      LN_CREDITOS := LC_CREDITOS.CREDITOS;
    ELSE
      LN_CREDITOS := 0;
    END IF;

    OPEN C_CARGOS_CTA(PN_CUSTOMER_ID);
    FETCH C_CARGOS_CTA
      INTO LV_CARGOSCTA;
    CLOSE C_CARGOS_CTA;

    LV_CARGOSCTA := LV_CARGOSCTA + LN_CREDITOS;

    IF LV_CARGOSCTA < 0 THEN
      FOR X IN C_FACTURAS_DEUDA(PN_CUSTOMER_ID) LOOP
      
        LV_CARGOSCTA := LV_CARGOSCTA + X.OHOPNAMT_DOC;
      
        IF LV_CARGOSCTA > 0 THEN
          LN_DEUDA_TOTAL := LN_DEUDA_TOTAL + X.OHINVAMT_DOC;
        END IF;
      
      END LOOP;
    ELSE
      OPEN C_DEUDA_TOTAL(PN_CUSTOMER_ID);
      FETCH C_DEUDA_TOTAL
        INTO LN_DEUDA_TOTAL;
      CLOSE C_DEUDA_TOTAL;
    END IF;

    --* CTA. SIN DEUDA
    IF LN_DEUDA_TOTAL <= 0 THEN
      PN_DEUDA_TOTAL := 0;
    ELSE
      PN_DEUDA_TOTAL := LN_DEUDA_TOTAL;
      --PN_DEUDA_PENDIENTE:= LN_DEUDA_PENDIENTE;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_DEUDA_TOTAL := NULL;
    
      PV_ERROR := 'ERROR AL VALIDAR DEUDA - ' || SQLERRM || ' - ' ||
                  LV_APLICACION;
      PN_ERROR := SQLCODE;
  END;

end RC_Api_TimeToCash_Rule_Bscs;
/
