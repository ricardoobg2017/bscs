CREATE OR REPLACE PACKAGE reloj.MMAG_ACTUALIZA_MEDI IS

-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
-- PROPOSITO          :  PROCESA REGITROS DEL ARCHIVO DE ENTRADA             ^
--                       ENVIADO POR LA INSTITUCION FINANCIERA               ^  
--                       ACTUALIZA DESDE EL MEDIO MAGNETICO                  ^
-- FECHA DE CREACION  :  05/11/2001                                          ^
-- SISTEMA            :  MEDIOS MAGNETICOS                                   ^
-- AUTOR              :  SIS MARIA SAN WONG                                  ^           
---------------------------------------------------------------------------- ^
-- ACTUALIZADO POR    :  SIS ERNESTO IDROVO                                  ^
-- FECHA              :  28/09/2004                                          ^
-- SISTEMA            :  MEDIOS MAGNETICOS                                   ^
-- PROYECTO           :  INGRESO AUTOMATICO DE PAGOS Y RECHAZOS              ^
-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
---------------------------------------------------------------------------- ^
-- ACTUALIZADO POR    :  SUD JORGE ESPINOZA                                  ^
-- FECHA              :  04/OCT/2004                                         ^
-- SISTEMA            :  MEDIOS MAGNETICOS                                   ^
-- PROYECTO           :  ACTUALIZACION DE PAGOS Y RECHAZOS PARCIALES         ^
-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

   -- Procedimiento que obtiene infomacion  de un campo determinado, en un registro del archivo de entrada 
   procedure mmag_obtiene_campo_arch(    p_linea      in varchar2      -- registro en donde va a buscar la informacion
                                       , p_campo      in varchar2      -- campo que desea obtener
                                       , p_codi_enti  in varchar2      -- entidad financiera a la que pertenece el archivo
                                       , p_valor      in out varchar2  -- retorna la informacion obtenida del campo en el archivo 
                                       , p_valor_conf in out varchar2  -- retorna valor configurado para el campo a buscar
                                       , p_codi_erro  in out number    -- error si existe un problema
                                       , p_mens_erro  in out varchar2  -- mensaje de error
                                    );   

   --Procedimiento que actualiza pagos y rechazos desde un registro del archivo de entrada enviado por la institucion financiera
   procedure mmag_procesa_arch_entrada (   p_linea         in varchar2 -- Registro que se va ha procesar
                                         , p_codi_enti     in varchar2 -- Codigo de la entidad finaciera a la que pertenece el archivo
                                         , p_codi_prod     in varchar2 -- codigo del producto 
                                         , p_codi_peri     in varchar2 -- periodo para el cual se esta ejecutando el proceso
                                         , p_reproceso     in number   -- 2 si es parcial, 1 si es reproceso, 0 si no lo es 
                                         , p_region        in varchar2 -- codigo de region
                                         , p_pago_rechazo  in varchar2
                                         , p_nume_intento  in varchar2 -- numero de intento a procesar
                                         , p_ciclo         In Varchar2  
                                         , p_estado       out varchar2 -- estado con el que se proceso                                         
                                         , p_codi_erro    out number   -- error si existe un problema
                                         , p_mens_erro    out varchar2 -- mensaje de error
                                         , p_incr_direc    in varchar2 default null -- I = incremental, D = Directo
                                        );

   -- Realiza un commit o rollback en la base de datos
   procedure Mmag_Finaliza_Proceso ( pv_grabar        In Varchar2,
                                     pv_error        Out Varchar2                                  
                                     );
                                     
   -- Procedimiento que obtiene infomacion  de un campo determinado, en un registro del archivo de entrada 
   procedure mmag_obtiene_campo_file(    p_linea      in varchar2      -- registro en donde va a buscar la informacion
                                       , p_campo      in varchar2      -- campo que desea obtener
                                       , p_codi_enti  in varchar2      -- entidad financiera a la que pertenece el archivo
                                       , p_valor      in out varchar2  -- retorna la informacion obtenida del campo en el archivo 
                                       , p_valor_conf in out varchar2  -- retorna valor configurado para el campo a buscar
                                       , p_codi_erro  in out number    -- error si existe un problema
                                       , p_mens_erro  in out varchar2
                                    );                                     

END;
/
CREATE OR REPLACE PACKAGE BODY reloj.MMAG_ACTUALIZA_MEDI IS


-------------------------------------------------------------------------------------------------------------------
-- Procedimiento que obtiene infomacion  de un campo determinado, en un registro del archivo de entrada 
-------------------------------------------------------------------------------------------------------------------
procedure mmag_obtiene_campo_arch(    p_linea      in varchar2      -- registro en donde va a buscar la informacion
                                    , p_campo      in varchar2      -- campo que desea obtener
                                    , p_codi_enti  in varchar2      -- entidad financiera a la que pertenece el archivo
                                    , p_valor      in out varchar2  -- retorna la informacion obtenida del campo en el archivo 
                                    , p_valor_conf in out varchar2  -- retorna valor configurado para el campo a buscar
                                    , p_codi_erro  in out number    -- error si existe un problema
                                    , p_mens_erro  in out varchar2
                                 )  is

salida exception;

ln_ubi  mmag_campos_entidad_financ_dat.posi_camp_enti_fina%type;
ln_lon  mmag_campos_entidad_financ_dat.long_camp_enti_fina%type;
lv_tip  mmag_campos_entidad_financ_dat.tipo_camp_enti_fina%type;
lv_alin mmag_campos_entidad_financ_dat.alin_camp_enti_fina%type;
lv_rell mmag_campos_entidad_financ_dat.cara_rell_camp_enti_fina%type;
lv_valor varchar2(500);
ln_pos  number:=0;
lv_est  varchar2(1):= 'E';
                                                   
begin
  p_codi_erro:=0;
  p_mens_erro:= null;

  -- Devuelve informacion sobre la configuracion de un campo especifico del archivo                                                       
  begin
    select a.posi_camp_enti_fina      
         , a.long_camp_enti_fina        
         , a.valo_cons_camp_enti_fina   
         , a.tipo_camp_enti_fina        
         , a.alin_camp_enti_fina
         , a.cara_rell_camp_enti_fina
    into   ln_ubi
         , ln_lon
         , p_valor_conf
         , lv_tip
         , lv_alin
         , lv_rell 
    from mmag_campos_entidad_financ_dat a
    where a.codi_enti_fina = p_codi_enti
    and a.esta_enti_fina = lv_est
    and a.codi_camp = p_campo;
  exception
    when no_data_found then
      p_codi_erro:= 1;
      p_mens_erro:= 'Revise la configuracion del archivo, No se encontro definido el campo '||p_campo ;
      raise salida;
    when too_many_rows then
      p_codi_erro:= 2;
      p_mens_erro:= 'Revise la configuracion del archivo, Se encontro definido varios registros para el campo '||p_campo ;
      raise salida;
    when others then
      p_codi_erro:= 4;
      p_mens_erro:= 'Ocurrio el siguiente error al obtener campo desde el archivo: '||sqlerrm ;
      raise salida;

  end;   
  if  lv_tip <> 'D' then
      p_codi_erro:= 3;
      p_mens_erro:= 'Revise la configuracion del archivo, El campo '||p_campo||' no es un campo de detalle';
      raise salida;    
  end if;
  -- obtiene la posicion inicial del campo en el registro, dependiendo de su posicion 
  select nvl(sum(a.long_camp_enti_fina),0)+1
  into   ln_pos
  from   mmag_campos_entidad_financ_dat a
  where  a.codi_enti_fina = p_codi_enti
  and    a.esta_enti_fina = lv_est
  and    a.posi_camp_enti_fina < ln_ubi;
  
  -- obtiene la informacion del campo, 
  -- extrae la informacion desde la posicion inicial tantas cantidad de caracteres
  -- segun indique su longitud
  select substr(p_linea,ln_pos,ln_lon)
  into   lv_valor
  from   dual;
  -- elimina los caracteres en blanco
  if lv_alin = 'D' then
        select ltrim(lv_valor,' ')
        into   lv_valor
        from dual;
     else 
        select rtrim(lv_valor,' ')
        into   lv_valor
        from dual;
  end if;   
  -- elimina los caracteres de relleno segun la alineacion del campo
  if lv_rell is not null then
     if lv_alin = 'D' then
        select ltrim(lv_valor,lv_rell)
        into   p_valor
        from dual;
     else 
        select rtrim(lv_valor,lv_rell)
        into   p_valor
        from dual;
     end if;   
  else
     p_valor:= lv_valor;
  end if;   
exception
  when salida then
        null;

end;
---------------------------------------------------------------------------------------------------------------------------------------
--Procedimiento que actualiza pagos y rechazos desde un registro del archivo de entrada enviado por la institucion financiera
---------------------------------------------------------------------------------------------------------------------------------------

Procedure Mmag_Procesa_Arch_Entrada ( p_linea         In Varchar2, 
                                      p_codi_enti     In Varchar2, 
                                      p_codi_prod     In Varchar2, 
                                      p_codi_peri     In Varchar2, 
                                      p_reproceso     In Number, -- 0= PRIMER ENVIO, 1= REPROCESO, 2= PARCIALES
                                      p_region        In Varchar2, 
                                      p_pago_rechazo  In Varchar2, 
                                      p_nume_intento  In Varchar2, 
                                      p_ciclo         In Varchar2,
                                      p_estado        Out Varchar2,                                      
                                      p_codi_erro     Out Number, 
                                      p_mens_erro     Out Varchar2,
                                      p_incr_direc    in varchar2 default null
                                      ) Is
-- ACTUALIZADO POR: ANL. JORGE ESPINOZA ANDALUZ
-- PROYECTO:        ACTUALIZACION DE PAGOS Y RECHAZOS PARCIALES.
-- FECHA:           04-OCT-2005
--
CURSOR LC_MAX_INTENTO IS
  SELECT MAX( I.NUME_INTENTO) MAX_INTENTO
  FROM MMAG_INTENTO_COBRO_DAT I
  WHERE I.CODI_PROD 
  --= P_CODI_PROD
  IN (SELECT X.CODI_PROD
                                           FROM   MMAG_PRODUCTOS_DAT X
                                           START  WITH X.CODI_PROD = P_CODI_PROD
                                           CONNECT BY PRIOR X.CODI_PROD = X.CODI_SUPER_PROD)
        AND I.CODI_PERI = P_CODI_PERI
        AND I.CODI_ENTI_FINA = P_CODI_ENTI
        AND I.CODI_REGI = P_REGION
        AND I.CICLO = P_CICLO;
LB_NOT_MAX_INTENTO BOOLEAN;
LR_MAX_INTENTO LC_MAX_INTENTO%ROWTYPE;


--
Cursor c_valores_pagados (cn_nume_secu Number) Is
  Select nume_secu, nume_intento, valor_en_dolares_cob total_deuda, 
         Sum(valor_en_dolares_act) total_pago
  From   mmag_detalle_intento_cobro_dat
  Where  nume_secu        = cn_nume_secu
  And    tipo_generacion  = 'REP'
  And    estado_posterior = 'P'
  Group  By nume_secu, nume_intento, valor_en_dolares_cob
  Having Sum(valor_en_dolares_act) >= valor_en_dolares_cob;
--

/*Cursor c_primera_ves_p is
 Select a.nume_secu
 From  mmag_detalle_intento_cobro_dat a
 Where a.nume_secu            In ( Select b.nume_secu
                                   From  mmag_datos_medio_dat b
                                   Where b.codi_prod            = p_codi_prod
                                   And   b.codi_peri            = p_codi_peri
                                   And   b.codi_enti_fina       = p_codi_enti
                                   And   b.codigo_region        = p_region
                                   And   (to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
                                         b.numero_tarjeta       = lt_cuenta_tarjeta Or
                                         b.cuenta               = lt_cuenta_tarjeta Or
                                         b.secuencia            = lt_cuenta_tarjeta)
                                   And   b.esta_prev_dato_medi  = 'S' )
 And   a.sec_registro         In ( Select max(x.sec_registro)
                                   From  mmag_detalle_intento_cobro_dat x
                                   Where x.nume_secu            = a.nume_secu
                                   And   x.tipo_generacion      = 'PRI'
                                   And   x.estado_posterior     <> 'P' 
                                   And   x.nume_intento         = ln_nume_intento )
 And   a.tipo_generacion      = 'PRI'
 And   a.estado_posterior     <> 'P' 
 And   a.nume_intento         = ln_nume_intento;*/


salida                Exception;
lv_valor              Varchar2(250);
lv_valor_conf         Varchar2(250);
lv_campo              Varchar2(250);
lv_resto              Varchar2(250);
lv_query              Varchar2(500);
lv_erro               Varchar2(500);
ln_erro               Number;
ln_exist_codi         Number;  
ln_exist_moti         Number;   
ln_cant               Number;
ln_temp               Number:=0;
ln_long               Number:=0;
ln_long_tota_pago     Number:=0;
ln_long_tota_rech     Number:=0;
ln_pos                Number:=0;
Lb_Found              Boolean;
ln_nume_intento       mmag_detalle_intento_cobro_dat.nume_intento%Type;
lt_nume_secu_pri      mmag_datos_medio_dat.nume_secu%Type;
lt_nume_secu_rep      mmag_datos_medio_dat.nume_secu%Type;
lt_cuenta_tarjeta     mmag_datos_medio_dat.cuenta_banco%Type;
lt_valor_cobro        mmag_datos_medio_dat.valor_en_dolares_act%Type;
lt_estado             mmag_datos_medio_dat.esta_post_dato_medi%Type;
lt_estado_new         mmag_datos_medio_dat.esta_post_dato_medi%Type;
lv_estado_par         mmag_datos_medio_dat.esta_post_dato_medi%Type;
lt_codi_rech          mmag_datos_medio_dat.codi_rech%Type;
lt_moti_rech          mmag_datos_medio_dat.moti_rech_dato_medi%Type;
lc_valores_pagados    c_valores_pagados%RowType;

Cursor c_primera_ves_n (cv_cabecera varchar2) is
 Select a.nume_secu
 From  mmag_detalle_intento_cobro_dat a
 Where a.nume_secu            In ( Select b.nume_secu
                                   From  mmag_datos_medio_dat b
                                   Where b.codi_prod           -- = p_codi_prod
                                   IN (SELECT X.CODI_PROD
                                           FROM   MMAG_PRODUCTOS_DAT X
                                           START  WITH X.CODI_PROD = P_CODI_PROD
                                           CONNECT BY PRIOR X.CODI_PROD = X.CODI_SUPER_PROD)
                                   
                                   And   b.ciclo                = p_ciclo
                                   And   b.codi_peri            = p_codi_peri
                                   And   b.codi_enti_fina       = p_codi_enti
                                   And   b.codigo_region        = p_region
                                   And   (--to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
                                         b.cuenta_banco         = lt_cuenta_tarjeta Or 
                                         b.numero_tarjeta       = lt_cuenta_tarjeta Or
                                         b.cuenta               = lt_cuenta_tarjeta Or
                                         b.secuencia            = lt_cuenta_tarjeta)
                                   And   b.esta_prev_dato_medi  = 'S' )
 And   a.sec_registro         In ( Select max(x.sec_registro)
                                   From  mmag_detalle_intento_cobro_dat x
                                   Where x.nume_secu            = a.nume_secu
                                   And   x.valor_en_dolares_act = lt_valor_cobro 
                                   And   x.tipo_generacion      = 'PRI'
                                   And   ( x.estado_posterior   <> 'P' Or cv_cabecera = 'S' )
                                   And   x.nume_intento         = ln_nume_intento )
 And   a.valor_en_dolares_act = lt_valor_cobro
 And   a.tipo_generacion      = 'PRI'
 And   ( a.estado_posterior   <> 'P' Or cv_cabecera = 'S' )
 And   a.nume_intento         = ln_nume_intento;
 
 lc_primera_ves_n             c_primera_ves_n%rowtype;

 -------------------------------------------------------------------------------------------------------
 --------------------------  VARIABLES PARA EL PAGO PARCIAL --------------------------------------------
 -------------------------------------------------------------------------------------------------------
 -- JES
 -- CURSOR PARA DETERMINAR SI YA SE PAGO LA TOTALIDAD DE LA CABECERA
  CURSOR LC_VALORES_PAGADOS_PARCIAL(  CN_NUME_SECU NUMBER
                                     ,CN_NUME_INTENTO NUMBER 
                                     --,CN_SECU_REGI NUMBER
                                     ) IS
  SELECT I.NUME_SECU, 
         I.NUME_INTENTO, 
         I.VALOR_EN_DOLARES_COB TOTAL_DEUDA, 
         SUM( NVL( I.VALOR_EN_DOLARES_PAG, 0)) TOTAL_PAGO
  FROM   MMAG_DETALLE_INTENTO_COBRO_DAT I
  WHERE  I.NUME_SECU        = CN_NUME_SECU
         AND I.NUME_INTENTO = CN_NUME_INTENTO
         --AND I.ESTADO_POSTERIOR IN ('P','R')
         --AND I.SEC_REGISTRO != CN_SECU_REGI
  GROUP  BY NUME_SECU, NUME_INTENTO, VALOR_EN_DOLARES_COB;
  LB_NOT_VALORES_PAGADOS_PARCIAL BOOLEAN;
  LR_VALORES_PAGADOS_PARCIAL LC_VALORES_PAGADOS_PARCIAL%ROWTYPE;

 -- CURSOR QUE PERMITE CONSULTAR LA SECUENCIA DEL DETALLE DE SERVICIO QUE SE VA ACTUALIZAR.
 CURSOR LC_SECU_PARCIAL( CV_CODI_PROD VARCHAR2, 
                         CV_CICLO VARCHAR2, 
                         CV_CODI_PERI VARCHAR2, 
                         CV_CODI_ENTI VARCHAR2,
                         CV_REGION VARCHAR2, 
                         CV_CUENTA_TARJETA VARCHAR2, 
                         CN_NUME_INTENTO NUMBER,
                         CN_VALOR_COBRO NUMBER,
                         CV_INCR_DIRE VARCHAR2
                         ) IS
    SELECT D.NUME_INTENTO, 
           D.NUME_SECU, 
           D.SEC_REGISTRO, 
           D.TIPO_GENERACION, 
           DECODE( CV_INCR_DIRE, 
                   'I', (D.VALOR_EN_DOLARES_ACT - NVL( D.VALOR_EN_DOLARES_PAG, 0)), 
                         D.VALOR_EN_DOLARES_ACT
                 ) TOTAL_DEUDA, 
           NVL( D.VALOR_EN_DOLARES_ACT, 0) VALOR_EN_DOLARES_ACT,
           D.VALOR_EN_DOLARES_COB,
           NVL( d.valor_en_dolares_pag, 0) valor_en_dolares_pag,
           NVL( d.valor_en_dolares_tra, 0) valor_en_dolares_tra
    FROM  MMAG_DATOS_MEDIO_DAT B, MMAG_DETALLE_INTENTO_COBRO_DAT D
    WHERE B.CODI_PROD        --  = CV_CODI_PROD
    IN  (SELECT X.CODI_PROD
                                           FROM   MMAG_PRODUCTOS_DAT X
                                           START  WITH X.CODI_PROD = CV_CODI_PROD
                                           CONNECT BY PRIOR X.CODI_PROD = X.CODI_SUPER_PROD)
    AND B.CICLO                = CV_CICLO
    AND B.CODI_PERI            = CV_CODI_PERI
    AND B.CODI_ENTI_FINA       = CV_CODI_ENTI
    AND B.CODIGO_REGION        = CV_REGION
    AND  (
         --TO_NUMBER(B.CUENTA_BANCO) = TO_NUMBER( CV_CUENTA_TARJETA) OR 
         B.CUENTA_BANCO         = CV_CUENTA_TARJETA OR 
         B.NUMERO_TARJETA       = CV_CUENTA_TARJETA OR
         B.CUENTA               = CV_CUENTA_TARJETA OR
         B.SECUENCIA            = CV_CUENTA_TARJETA
         )
    AND B.ESTA_PREV_DATO_MEDI  = 'S'
    AND D.NUME_SECU = B.NUME_SECU
    AND D.NUME_INTENTO = CN_NUME_INTENTO
    AND D.ESTADO_POSTERIOR != 'P'
    AND
      ( 
      ( D.TIPO_GENERACION = 'REP' AND B.ESTA_REPR_DATO_MEDI = 'S') 
      OR
      ( D.TIPO_GENERACION = 'PRI' AND B.ESTA_REPR_DATO_MEDI = 'I') 
      )
    AND
      (
      ( CV_INCR_DIRE = 'I' AND (D.VALOR_EN_DOLARES_ACT - NVL( D.VALOR_EN_DOLARES_PAG, 0)) >= CN_VALOR_COBRO )
      OR
      ( CV_INCR_DIRE = 'D' AND D.VALOR_EN_DOLARES_ACT >= CN_VALOR_COBRO )
      )
    ORDER BY D.VALOR_EN_DOLARES_ACT;
 LB_NOT_SECU_PARCIAL BOOLEAN;
 LR_SECU_PARCIAL     LC_SECU_PARCIAL%ROWTYPE;
    
    lv_proceso varchar2(4);
    lv_accion  varchar2(4);
    LN_VALOR_REC NUMBER;
    LN_VALOR_PAG NUMBER;
--
LV_TIPO_INTENTO         VARCHAR2(1);
LV_TIPO_GENERACION      VARCHAR2(3);
--
ln_valor                number;
ln_sin_imp              number;
ln_iva                  number;
ln_ice                  number;
Begin

   p_codi_erro:=0;
   p_mens_erro:= Null;

   ln_nume_intento := to_number(p_nume_intento);
   
  -- JES
  -- SOPORTE PARA PAGOS PARCIALES
  MMK_TRX_PROCEDURE.MMP_CONSULTA_INTENTO( ln_nume_intento, LV_TIPO_INTENTO, LV_TIPO_GENERACION, p_mens_erro);
  IF p_mens_erro IS NOT NULL THEN
      p_codi_erro:= 27;
      Raise salida;
  END IF;
  IF p_reproceso = 0 AND LV_TIPO_INTENTO in ('X','N') THEN
      NULL; --OK
  ELSIF p_reproceso = 1 AND LV_TIPO_INTENTO in ('X','N') THEN
      NULL; --OK
  ELSIF p_reproceso = 2 AND LV_TIPO_INTENTO in ('P','N') THEN
      NULL; --OK
  ELSE
      p_codi_erro:= 28;
      p_mens_erro:= 'El proceso no acepta modificar este intento ya que antes fue tomado por otro tipo de proceso. Ingrese por la pantalla correspondiente.';
      Raise salida;
  END IF;
  -- FIN JES


   -- verifica que el registro a procesar tenga la misma longitud 
   -- definida en la configuracion del archivo
   
   -- Longitud del registro de Pagos
   begin
     Select Sum(a.long_camp_enti_fina) Into ln_long_tota_pago
     From mmag_campos_entidad_financ_dat a
     Where a.codi_enti_fina = p_codi_enti
     And a.esta_enti_fina = 'E'
     And a.codi_camp not in ('CODIGO_RECHAZO','MOTIVO_RECHAZO');
   exception
     when others then
       ln_long_tota_pago := 0;
   end;
   -- Longitud del registro de Rechazos
   begin
     Select Sum(a.long_camp_enti_fina) Into ln_long_tota_rech
     From mmag_campos_entidad_financ_dat a
     Where a.codi_enti_fina = p_codi_enti
     And a.esta_enti_fina = 'E';
   exception
     when others then
       ln_long_tota_rech := 0;
   end;

   Select length(p_linea) Into ln_long From dual;
   
--   If ln_long Not Between ln_long_tota_pago And ln_long_tota_rech Then
   If ln_long Not Between 5 And ln_long_tota_rech Then
      Dbms_Output.Put_Line('El tamanio del registro no concuerda con la configuracion del archivo' );
      p_codi_erro:= 1;
      p_mens_erro:= 'El tamanio del registro no concuerda con la configuracion del archivo';
      Raise salida;
   End If;   
   
   -------------------------------------------------------
   -- obtiene los campos a procesar desde la configuracion
   -------------------------------------------------------

   -- obtiene el valor al cobro
/*   Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,'VALOR_COBRO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   */
   MMAG_ACTUALIZA_MEDI.Mmag_Obtiene_Campo_File(p_linea,'VALOR_COBRO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   
   If ln_erro <> 0 Then
      p_codi_erro:= 10;
      p_mens_erro:= lv_erro;
      Raise salida;
   End If;     
   declare 
      ln_aux number;
   begin
     ln_aux := instr(lv_valor,',');
     if ln_aux != 0 then
         raise no_data_found;
     end if;
     lt_valor_cobro := to_number(lv_valor,'999999999999.00');
   exception
     when no_data_found then
      p_codi_erro:= 1;
      p_mens_erro:= 'Error en formato del campo VALOR_COBRO : El separador de decimales es el punto(.), y la cadena contiene comas(,).';
      Raise salida;
     when others then
      p_codi_erro:= 1;
      p_mens_erro:= 'Error en conversion numerica del contenido del campo VALOR_COBRO';
      Raise salida;
   end;
   
   -- obtiene cuenta del banco o n�mero de tarjeta
/*   Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,'CUENTA_TARJETA',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   */
   MMAG_ACTUALIZA_MEDI.Mmag_Obtiene_Campo_File(p_linea,'CUENTA_TARJETA',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   
   If ln_erro <> 0 Then
      p_codi_erro:= 2;
      p_mens_erro:= lv_erro;
      Raise salida;
   End If;
   --lt_cuenta_tarjeta := to_number(lv_valor);
   lt_cuenta_tarjeta := lv_valor;
   
/*      If p_reproceso = 1 Then
         Select 1 Into ln_temp
         From  mmag_datos_medio_dat a
         Where a.codi_prod = p_codi_prod
         And   a.codi_peri = p_codi_peri
         And   a.codi_enti_fina = p_codi_enti
         And   a.codigo_region = p_region
         And   a.esta_repr_dato_medi = 'S'
         And   (a.cuenta_banco = lt_cuenta_tarjeta Or a.numero_tarjeta = lt_cuenta_tarjeta)
         And   a.valor_en_dolares_act = lt_valor_cobro;
      Else
         Select 1 Into ln_temp
         From  mmag_datos_medio_dat a
         Where a.codi_prod = p_codi_prod
         And   a.codi_peri = p_codi_peri
         And   a.codi_enti_fina = p_codi_enti
         And   a.codigo_region = p_region
         And   a.esta_prev_dato_medi = 'S'
         And   (a.cuenta_banco = lt_cuenta_tarjeta Or a.numero_tarjeta = lt_cuenta_tarjeta)
         And   a.valor_en_dolares_act = lt_valor_cobro;
      End If;   */

   -- verifica y obtiene la informacion a actualizar en el detalle
   Begin
      -- JES
      -- 05-OCT-2005
      -- ESTO YA NO VA PORQUE SE DESARROLLA LA FUNCIONALIDAD PARA TODAS LAS ENTIDADES FINANCIERAS.
      -- validacion de pagos parciales para pichincha y pichincha2
      
      --if p_codi_enti = 'PICHINCHA' or p_codi_enti = 'PICHINCHA2' then
      --    If p_reproceso = 1 Then -- Para el reproceso
      --       Select a.nume_secu Into lt_nume_secu_rep
      --       From  mmag_detalle_intento_cobro_dat a
      --       Where a.nume_secu            In ( Select b.nume_secu
      --                                         From  mmag_datos_medio_dat b
      --                                         Where b.codi_prod            = p_codi_prod
      --                                         And   b.ciclo                = p_ciclo
      --                                         And   b.codi_peri            = p_codi_peri                                               
      --                                         And   b.codi_enti_fina       = p_codi_enti
      --                                         And   b.codigo_region        = p_region
      --                                         And   (to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
      --                                               b.numero_tarjeta       = lt_cuenta_tarjeta Or
      --                                               b.cuenta               = lt_cuenta_tarjeta Or
      --                                               b.secuencia            = lt_cuenta_tarjeta)
      --                                         And   b.esta_repr_dato_medi  = 'S' )
      --       And   a.sec_registro         In ( Select max(x.sec_registro)
      --                                         From  mmag_detalle_intento_cobro_dat x
      --                                         Where x.nume_secu            = a.nume_secu
      --                                         And   x.tipo_generacion      = 'REP'
      --                                         And   x.estado_posterior     <> 'P' 
      --                                         And   x.nume_intento         = ln_nume_intento )
      --       And   a.tipo_generacion      = 'REP'
      --       And   a.estado_posterior     <> 'P' 
      --       And   a.nume_intento         = ln_nume_intento
      --       And   Rownum = 1;
                      /*Else -- Para el primer envio
                         Select a.nume_secu Into lt_nume_secu_pri
                         From  mmag_detalle_intento_cobro_dat a
                         Where a.nume_secu            In ( Select b.nume_secu
                                                           From  mmag_datos_medio_dat b
                                                           Where b.codi_prod            = p_codi_prod
                                                           And   b.codi_peri            = p_codi_peri
                                                           And   b.codi_enti_fina       = p_codi_enti
                                                           And   b.codigo_region        = p_region
                                                           And   (to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
                                                                 b.numero_tarjeta       = lt_cuenta_tarjeta Or
                                                                 b.cuenta               = lt_cuenta_tarjeta Or
                                                                 b.secuencia            = lt_cuenta_tarjeta)
                                                           And   b.esta_prev_dato_medi  = 'S' )
                         And   a.sec_registro         In ( Select max(x.sec_registro)
                                                           From  mmag_detalle_intento_cobro_dat x
                                                           Where x.nume_secu            = a.nume_secu
                                                           And   x.tipo_generacion      = 'PRI'
                                                           And   x.estado_posterior     <> 'P' 
                                                           And   x.nume_intento         = ln_nume_intento )
                         And   a.tipo_generacion      = 'PRI'
                         And   a.estado_posterior     <> 'P' 
                         And   a.nume_intento         = ln_nume_intento;
                         -- para primera vez se toman todos los registros que entren
                         --And   Rownum = 1;*/
      --    End If;
      --
      --
      -- JES
      -- AHORA SE DEBERA PREGUNTAR SI ESTA CARGA ES PARA UNA CARGA DE ACTUALIZACION DE PAGOS PARCIALES.
      IF p_reproceso = 2 THEN
          -- VERIFICO QUE SE DEBA ACTUALIZAR ESTE PROCESO.
          OPEN LC_MAX_INTENTO;
          FETCH LC_MAX_INTENTO INTO LR_MAX_INTENTO;
          LB_NOT_MAX_INTENTO := LC_MAX_INTENTO%notfound;
          CLOSE LC_MAX_INTENTO;
          
          IF LB_NOT_MAX_INTENTO THEN
             p_codi_erro:= 24;
             p_mens_erro:= 'No se encontro el intento a procesar. Favor escoja un intento existente.';
             Raise salida;               
          END IF;
          
          IF LR_MAX_INTENTO.MAX_INTEnTO != P_NUME_INTENTO THEN
             p_codi_erro:= 26;
             p_mens_erro:= 'El intento a procesar no es el ultimo. Favor escoja el ultimo intento para procesar.';
             Raise salida;               
          END IF;
         -- debo de verificar si es incremental o directo
         IF P_INCR_DIREC != 'D' AND P_INCR_DIREC != 'I' THEN
             p_codi_erro:= 25;
             p_mens_erro:= 'El parametro de actualizacion incremental o directa esta erroneo.';
             Raise salida;               
         END IF;      
          --DETERNIMO EL REGISTRO QUE SE DEBE DE ACTUALIZAR.
           OPEN LC_SECU_PARCIAL( P_CODI_PROD, P_CICLO, P_CODI_PERI, P_CODI_ENTI, P_REGION,
                                 LT_CUENTA_TARJETA, LN_NUME_INTENTO, LT_VALOR_COBRO, p_incr_direc);
           FETCH LC_SECU_PARCIAL INTO LR_SECU_PARCIAL;
           LB_NOT_SECU_PARCIAL := LC_SECU_PARCIAL%NOTFOUND;
           CLOSE LC_SECU_PARCIAL;
           IF LB_NOT_SECU_PARCIAL THEN
               RAISE no_data_found;
           END IF;
      else    -- casos de archivos normales
      
          If p_reproceso = 1 Then -- Para el reproceso
             Select a.nume_secu Into lt_nume_secu_rep
             From  mmag_detalle_intento_cobro_dat a
             Where a.nume_secu            In ( Select b.nume_secu
                                               From  mmag_datos_medio_dat b
                                               Where b.codi_prod            --= p_codi_prod
                                               in  (SELECT X.CODI_PROD
                                           FROM   MMAG_PRODUCTOS_DAT X
                                           START  WITH X.CODI_PROD = P_CODI_PROD
                                           CONNECT BY PRIOR X.CODI_PROD = X.CODI_SUPER_PROD)
                                               And   b.ciclo                = p_ciclo
                                               And   b.codi_peri            = p_codi_peri
                                               And   b.codi_enti_fina       = p_codi_enti
                                               And   b.codigo_region        = p_region
                                               And   (--to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
                                                     b.cuenta_banco         = lt_cuenta_tarjeta Or 
                                                     b.numero_tarjeta       = lt_cuenta_tarjeta Or
                                                     b.cuenta               = lt_cuenta_tarjeta Or
                                                     b.secuencia            = lt_cuenta_tarjeta)
                                               And   b.esta_repr_dato_medi  = 'S' )
                                               --And   b.esta_post_dato_medi  <> 'P' )
             And   a.sec_registro         In ( Select max(x.sec_registro)
                                               From  mmag_detalle_intento_cobro_dat x
                                               Where x.nume_secu            = a.nume_secu
                                               And   x.valor_en_dolares_act = lt_valor_cobro
                                               And   x.tipo_generacion      = 'REP'
                                               And   x.estado_posterior     <> 'P' 
                                               And   x.nume_intento         = ln_nume_intento )
             And   a.valor_en_dolares_act = lt_valor_cobro
             And   a.tipo_generacion      = 'REP'
             And   a.estado_posterior     <> 'P' 
             And   a.nume_intento         = ln_nume_intento
             And   Rownum = 1;
          /*Else -- Para el primer envio
             Select a.nume_secu Into lt_nume_secu_pri
             From  mmag_detalle_intento_cobro_dat a
             Where a.nume_secu            In ( Select b.nume_secu
                                               From  mmag_datos_medio_dat b
                                               Where b.codi_prod            = p_codi_prod
                                               And   b.codi_peri            = p_codi_peri
                                               And   b.codi_enti_fina       = p_codi_enti
                                               And   b.codigo_region        = p_region
                                               And   (to_number(b.cuenta_banco)        = to_number(lt_cuenta_tarjeta) Or 
                                                     b.numero_tarjeta       = lt_cuenta_tarjeta Or
                                                     b.cuenta               = lt_cuenta_tarjeta Or
                                                     b.secuencia            = lt_cuenta_tarjeta)
                                               And   b.esta_prev_dato_medi  = 'S' )
                                               --And   b.esta_post_dato_medi  <> 'P' )
             And   a.sec_registro         In ( Select max(x.sec_registro)
                                               From  mmag_detalle_intento_cobro_dat x
                                               Where x.nume_secu            = a.nume_secu
                                               And   x.valor_en_dolares_act = lt_valor_cobro 
                                               And   x.tipo_generacion      = 'PRI'
                                               And   x.estado_posterior     <> 'P' 
                                               And   x.nume_intento         = ln_nume_intento )
             And   a.valor_en_dolares_act = lt_valor_cobro
             And   a.tipo_generacion      = 'PRI'
             And   a.estado_posterior     <> 'P' 
             And   a.nume_intento         = ln_nume_intento;
             -- para primera vez se toman todos los registros que entren
             --And   Rownum = 1;*/
          End If;

      end if;
          
   Exception
      When no_data_found Then
         p_codi_erro:= 3;
         If p_reproceso = 1 Then
            p_mens_erro:= 'Reproceso: No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
         ElsIF p_reproceso = 2 THEN
            p_mens_erro:= 'Pago/Rechazo Parcial: No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
         ElsE
            p_mens_erro:= 'No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
         End If;   
         Raise salida;   
      When too_many_rows Then
         p_codi_erro:= 4;
         p_mens_erro:= 'Demasiados registros que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
         raise salida;
      When Others Then
         p_codi_erro:= 4;
         p_mens_erro:= 'Problemas al realizar el proceso: '||sqlerrm;
         Raise salida;  
   End;        
    
   If p_pago_rechazo = 'TODOS' Then
      -- obtiene marca
/*      Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,'MARCA',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);  */
      MMAG_ACTUALIZA_MEDI.Mmag_Obtiene_Campo_File(p_linea,'MARCA',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);  
      If ln_erro <> 0 Then
         p_codi_erro:= 2;
         p_mens_erro:= lv_erro;
         Raise salida;
      End If;
      -- obtiene el campo que va a determinar el valor del estado del pago
      /*Select substr(lv_valor_conf,1,instr(lv_valor_conf,',',1)-1) Into lv_campo From dual;
      Select substr(lv_valor_conf,instr(lv_valor_conf,',',1)) Into lv_resto From dual;
      -- busca la informacion del campo anterior
      Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,lv_campo,p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);
      If ln_erro <> 0 Then
         p_codi_erro:= 5;
         p_mens_erro:= 'Error en la configuracion del valor del campo MARCA';
         Raise salida;
      End If;
      -- le agrega las comillas al valor retornado
      Select chr(39)||lv_valor||chr(39) Into lv_valor From dual;
      -- obtiene el estado del pago
      lv_query:= 'SELECT DECODE('||lv_valor||lv_resto||') from dual';
      generar_medio(lv_query,lt_estado,0,False);*/
      ln_exist_codi:=1;
      lt_estado := lv_valor;
   Elsif p_pago_rechazo = 'NO_PAGADOS' Then
      lt_estado := 'R';
   Elsif p_pago_rechazo = 'PAGADOS' Then
      lt_estado := 'P';
   End If;
   
   If lt_estado = 'R' Then
     -- obtiene el codigo del rechazo
  /*   Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,'CODIGO_RECHAZO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   */
     MMAG_ACTUALIZA_MEDI.Mmag_Obtiene_Campo_File(p_linea,'CODIGO_RECHAZO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   
     -- si hay algun error diferente de 1
     If ln_erro > 1 Then
        p_codi_erro:= 6;
        p_mens_erro:= lv_erro;
        Raise salida;
     End If;     
     -- si no exite el codigo del rechazo (error 1)
     If ln_erro = 1 Then
        ln_exist_codi:=0;  
        lt_codi_rech:= Null;
     Else
        If lv_valor Is Null Then
           ln_exist_codi:=0;
        Else    
           ln_exist_codi:=1;
        End If;   
        lt_codi_rech:= lv_valor;
     End If;
     
     -- obtiene el motivo del rechazo
  /*   Mmag_Actualiza_Medi.Mmag_Obtiene_Campo_Arch(p_linea,'MOTIVO_RECHAZO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   */
     MMAG_ACTUALIZA_MEDI.Mmag_Obtiene_Campo_File(p_linea,'MOTIVO_RECHAZO',p_codi_enti,lv_valor,lv_valor_conf,ln_erro,lv_erro);   
     -- si hay algun error diferente de 1
     If ln_erro > 1 Then
        p_codi_erro:= 7;
        p_mens_erro:= lv_erro;
        Raise salida;
     End If;     
     -- si no exite el motivo del rechazo (error 1)
     If ln_erro = 1 Then
        ln_exist_moti:=0;
        lt_moti_rech:= Null;
     Else    
        If lv_valor Is Null Then
           ln_exist_moti:=0;
        Else    
           ln_exist_moti:=1;
        End If;   
        lt_moti_rech:= Upper(lv_valor);
     End If;        
     -- Obtiene y verifica informacion de rechazos
     -- en el caso que tengo los dos campos
     If ln_exist_moti = 1 And ln_exist_codi = 1 Then
        ln_cant:= 0;
        Select Count(*) Into ln_cant 
        From mmag_codigos_rechazo_dat a
        Where  a.codi_rech = lt_codi_rech
        And    a.desc_rech = lt_moti_rech
        And    a.codi_enti_fina = p_codi_enti;
       
        If ln_cant = 0 Then 
           p_codi_erro:= 8;
           p_mens_erro:= 'La informacion del codigo y motivo del rechazo no concuerda con la definida en el sistema';
           Raise salida;
        End If;
     End If;   
     If ln_exist_moti = 1 And ln_exist_codi = 0 Then
        Begin
           -- busca el codigo del rechazo
           Select a.codi_rech Into lt_codi_rech
           From   mmag_codigos_rechazo_dat a
           Where  a.desc_rech = lt_moti_rech
           And a.codi_enti_fina = p_codi_enti;
        Exception
           When no_data_found Then
              p_codi_erro:= 9;
              p_mens_erro:= 'No existe el motivo del rechazo en el sistema';
              Raise salida;
        End;        
     End If;
     If ln_exist_moti = 0 And ln_exist_codi = 1 Then
        Begin
           -- busca el codigo del rechazo
           Select a.desc_rech Into lt_moti_rech
           From   mmag_codigos_rechazo_dat a
           Where   a.codi_rech = lt_codi_rech
           And a.codi_enti_fina = p_codi_enti;
        Exception
           When no_data_found Then
              p_codi_erro:= 10;
              p_mens_erro:= 'No existe el codigo del rechazo en el sistema';
              Raise salida;
        End;        
     End If;
   ElsIf lt_estado = 'P' Then
     lt_moti_rech := Null;
     lt_codi_rech := Null;
   End If;
      
/*   Begin
      Update mmag_datos_medio_dat a
      Set   a.esta_post_dato_medi = lt_estado
          , a.motivo_rechazo      = lt_moti_rech
          , a.codi_rech           = lt_codi_rech
      Where a.codi_prod = p_codi_prod
      And   a.codi_peri = p_codi_peri
      And   a.codi_enti_fina = p_codi_enti
      And   a.codigo_region = p_region
      And   (a.cuenta_banco = lt_cuenta_tarjeta Or a.numero_tarjeta = lt_cuenta_tarjeta)
      And   a.valor_en_dolares_act = lt_valor_cobro;
      p_estado:= lt_estado;
   Exception
      When Others Then
         p_codi_erro:= 11;
         p_mens_erro:= 'Problemas al actualizar la informacion: '||sqlerrm;
         Raise salida;  
   End;        */
   
   -- Actualiza informacion en el detalle
   BEGIN
      IF p_reproceso = 2 THEN -- jes - esto es para pagos parciales...
         -- deternino el proceso y la accion que se ralizar�.
         IF LR_SECU_PARCIAL.TIPO_GENERACION = 'PRI' THEN
             LV_PROCESO := 'PRIM';
         ELSE
             LV_PROCESO := 'REPR';
         END IF;
         -- VALOR RECIBIDO
         LN_VALOR_REC := lt_valor_cobro;
         IF LN_VALOR_REC < 0 THEN
             p_codi_erro:= 22;
             p_mens_erro:= 'El valor recibido no puede ser menor que cero.';
             Raise salida;  
         END IF;             
         -- VALOR PAGADO
         IF LT_ESTADO = 'P' THEN
             LV_ACCION := 'PAGO';
             LN_VALOR_PAG := lt_valor_cobro; -- EL MISMO EN CASO DE PAGO
         ELSE
             LV_ACCION := 'RECH';           -- EL VALOR PAGADO EN CASO DE RECHAZO ESTA ALGO DIFICIL.
                                            -- PERO EL QUERY DE CONSULTA SE ENCARGA DE TODO.
             LN_VALOR_PAG := LR_SECU_PARCIAL.TOTAL_DEUDA - lt_valor_cobro;
         END IF;
         -- PARA ACTUALIZAR LOS REGISTROS DE DETALLE DEL VALOR PAGADO
         IF P_INCR_DIREC = 'I' THEN
             LN_VALOR_PAG := LR_SECU_PARCIAL.VALOR_EN_DOLARES_PAG + LN_VALOR_PAG;
         END IF;
         -- DETERMINO EL ESTADO PARA LOS REGISTROS. ESTO NO DEPENDE DE LO QUE VIENE EN EL ARCHIVO,
         -- SINO DE CUANTO SE HA PAGADO DEL REGISTRO. SI NO SE CANCELA TOTALMENTE LA DEUDA SE MARCA COMO RECHAZADO
         IF LR_SECU_PARCIAL.VALOR_EN_DOLARES_ACT = LN_VALOR_PAG THEN
             lt_estado := 'P';
         ELSE
             lt_estado := 'R';
         END IF;
         -- verifico si el valor a pagar es mayor a lo tranferido... si no hay error
         IF LN_VALOR_PAG < LR_SECU_PARCIAL.VALOR_EN_DOLARES_TRA THEN
             p_codi_erro:= 27;
             p_mens_erro:= 'El valor pagar no puede ser menor al valor que se ha transferido al facturador';
             Raise salida;  
         END IF;             
         --
         UPDATE MMAG_DETALLE_INTENTO_COBRO_DAT
           SET ESTADO_POSTERIOR = LT_ESTADO
              ,PROCESO_ACT = LV_PROCESO || LV_ACCION
              ,VALOR_EN_DOLARES_REC = LN_VALOR_REC
              ,VALOR_EN_DOLARES_PAG = LN_VALOR_PAG 
              -- JES 31/08/2007
              ,motivo_rechazo   = lt_moti_rech
              ,codi_rech        = lt_codi_rech
              -- PARA ACTUALIZAR LOS CONTADORES.
              ,CONT_INC = NVL(CONT_INC,0) + DECODE( P_INCR_DIREC, 'I', 1, 0)
              ,CONT_DIR = NVL(CONT_DIR,0) + DECODE( P_INCR_DIREC, 'D', 1, 0)
              ,ULTI_ACT = P_INCR_DIREC
              ,FECHA_ACT = SYSDATE
         WHERE NUME_SECU = LR_SECU_PARCIAL.NUME_SECU AND 
               NUME_INTENTO = LR_SECU_PARCIAL.NUME_INTENTO AND 
               SEC_REGISTRO = LR_SECU_PARCIAL.SEC_REGISTRO;
         --
         IF SQL%NOTFOUND THEN
            p_codi_erro:= 51;
            p_mens_erro:= 'Pago Parcial: No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
            RAISE Salida;
         END IF; 
      ELSIF p_reproceso = 1 THEN -- Para el reproceso
         --
         UPDATE mmag_detalle_intento_cobro_dat a
         SET   a.estado_posterior = lt_estado
             , a.motivo_rechazo   = lt_moti_rech
             , a.codi_rech        = lt_codi_rech
         WHERE a.nume_secu        = lt_nume_secu_rep
         AND   a.sec_registro     IN ( SELECT MAX(x.sec_registro)
                                       FROM  mmag_detalle_intento_cobro_dat x
                                       WHERE x.nume_secu            = a.nume_secu
                                       AND   x.valor_en_dolares_act = lt_valor_cobro
                                       AND   x.tipo_generacion      = 'REP'
                                       AND   x.estado_posterior     <> 'P'
                                       AND   x.nume_intento         = ln_nume_intento );
         --
         IF SQL%NOTFOUND THEN
            p_codi_erro:= 41;
            p_mens_erro:= 'Reproceso: No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
            RAISE Salida;
         END IF; 
      ELSE -- Para el primer envio
         OPEN  c_primera_ves_n ('N');
         FETCH c_primera_ves_n Into lc_primera_ves_n;
               lb_found :=c_primera_ves_n%FOUND;
         CLOSE c_primera_ves_n;
         --
         IF LB_FOUND THEN  --si existe algo para actualizar, procesa
            FOR i IN c_primera_ves_n ('N') LOOP
                UPDATE mmag_detalle_intento_cobro_dat a
                SET   a.estado_posterior = lt_estado
                    , a.motivo_rechazo   = lt_moti_rech
                    , a.codi_rech        = lt_codi_rech
                WHERE a.nume_secu        = i.nume_secu
                --Where a.nume_secu        = lt_nume_secu_pri
                AND   a.sec_registro     IN ( SELECT MAX(x.sec_registro)
                                              FROM  mmag_detalle_intento_cobro_dat x
                                              WHERE x.nume_secu            = a.nume_secu
                                              AND   x.valor_en_dolares_act = lt_valor_cobro
                                              AND   x.tipo_generacion      = 'PRI'
                                              AND   x.estado_posterior     <> 'P'
                                              AND   x.nume_intento         = ln_nume_intento );
            END LOOP;
         ELSE  --Si no existe nada para actualizar, retorna error
            p_codi_erro:= 31;
            p_mens_erro:= 'Primer Envio: No se encontro datos que actualizar para la cuenta  '||lt_cuenta_tarjeta||' con el valor a cobrar '||to_char(lt_valor_cobro);
            RAISE Salida;
         END IF;
      END IF;
      p_estado:= lt_estado;
   EXCEPTION
      WHEN Salida Then
         NULL;
      WHEN OTHERS THEN
         IF p_reproceso = 2 THEN
             p_codi_erro:= 21;
             p_mens_erro:= 'Pago/Rechazo Parcial: Problemas al actualizar la informacion en el detalle: '||sqlerrm;
         ELSE
             p_codi_erro:= 11;
             p_mens_erro:= 'Problemas al actualizar la informacion en el detalle: '||sqlerrm;
         END IF;
         RAISE salida;  
   END;
   
   -- Actualiza informacion en la cabecera
   Begin
      IF p_reproceso = 2 THEN
         -- PARA DETERMINAR EL ESTADO DE LA CABECERA.
         OPEN LC_VALORES_PAGADOS_PARCIAL( LR_SECU_PARCIAL.nume_secu , LR_SECU_PARCIAL.nume_intento );
         FETCH LC_VALORES_PAGADOS_PARCIAL INTO LR_VALORES_PAGADOS_PARCIAL;
         LB_NOT_VALORES_PAGADOS_PARCIAL := LC_VALORES_PAGADOS_PARCIAL%NOTFOUND;
         CLOSE LC_VALORES_PAGADOS_PARCIAL;

         If LB_NOT_VALORES_PAGADOS_PARCIAL Then
             p_codi_erro:= 29;
             p_mens_erro:= 'No se encuentran valores en el detalle para actualizar el valor cobrado.';
             Raise salida;  
         elsif (LR_VALORES_PAGADOS_PARCIAL.total_deuda - LR_VALORES_PAGADOS_PARCIAL.total_pago) != 0 then
                lv_estado_par := 'R';
            Else
                lv_estado_par := 'P';
                lt_moti_rech  := Null;
                lt_codi_rech  := Null;
         End If;
         
         -- calculo de valores desglosados.
         ln_valor   := LR_VALORES_PAGADOS_PARCIAL.total_deuda - LR_VALORES_PAGADOS_PARCIAL.total_pago;
         ln_iva     := trunc(ln_valor * 0.12,2);
         ln_ice     := trunc(ln_valor * 0.15,2);
         ln_sin_imp := ln_valor - ln_ice - ln_iva;

         UPDATE MMAG_DATOS_MEDIO_DAT
         SET    ESTA_POST_DATO_MEDI = lv_estado_par
               --,VALOR_EN_DOLARES_ACT = ln_valor
               --,valor_sin_imp_act = ln_sin_imp
               --,iva_act = ln_iva
               --,ice_act = ln_ice
               ,motivo_rechazo      = lt_moti_rech
               ,codi_rech           = lt_codi_rech
               ,fech_actu_dato_medi = sysdate
               -- ACTUALIZO LOS CONTADORES DE ACTUALIZACIONES.
               ,CONT_INC = NVL(CONT_INC,0) + DECODE( P_INCR_DIREC, 'I', 1, 0)
               ,CONT_DIR = NVL(CONT_DIR,0) + DECODE( P_INCR_DIREC, 'D', 1, 0)
               ,ULTI_ACT = P_INCR_DIREC
               ,FECHA_ACT = SYSDATE
         WHERE  NUME_SECU           = LR_SECU_PARCIAL.NUME_SECU;
         
         lt_estado := lv_estado_par;
         
      ELSIf p_reproceso = 1 Then -- Para el reproceso
         -- Verifica si todos los detalles tienen estado P
         Open  c_valores_pagados(lt_nume_secu_rep);
         Fetch c_valores_pagados Into lc_valores_pagados;
               Lb_Found := c_valores_pagados%Found;
         Close c_valores_pagados;
         If Lb_Found Then
            lt_estado_new := 'P';
            lt_moti_rech  := Null;
            lt_codi_rech  := Null;
         Else
            lt_estado_new := 'R';
         End If;
         --
         Update mmag_datos_medio_dat
         Set   esta_post_dato_medi = lt_estado_new
             , motivo_rechazo      = lt_moti_rech
             , codi_rech           = lt_codi_rech
             , fech_actu_dato_medi = sysdate
         Where nume_secu           = lt_nume_secu_rep;
      Else -- Para el primer envio
         for i in c_primera_ves_n ('S') loop
             Update mmag_datos_medio_dat
             Set   esta_post_dato_medi = lt_estado
                 , motivo_rechazo      = lt_moti_rech
                 , codi_rech           = lt_codi_rech
                 , fech_actu_dato_medi = sysdate 
             Where nume_secu           = i.nume_secu;
             --Where nume_secu           = lt_nume_secu_pri;
         end loop;
      End If;
      p_estado:= lt_estado;      
   Exception
      When Others Then
         if p_reproceso = 2 then
             p_codi_erro:= 21;
             p_mens_erro:= 'Pagos/Rechazos Parciales: Problemas al actualizar la informacion en la cabecera: '||sqlerrm;
         else
             p_codi_erro:= 11;
             p_mens_erro:= 'Problemas al actualizar la informacion en la cabecera: '||sqlerrm;
         end if;
         Raise salida;  
   End;

Exception
  When salida Then
     Null;
  when others then
     p_codi_erro:= 99;
     p_mens_erro:= 'Ha ocurrido un inconveniente general al actualizar la informacion: '||sqlerrm;
End Mmag_Procesa_Arch_Entrada;

Procedure Mmag_Finaliza_Proceso ( pv_grabar        In Varchar2,
                                  pv_error        Out Varchar2                                  
                                  ) Is
Begin
   If pv_grabar = 'S' Then
      Commit;
   Else
      Rollback;
   End If;
End Mmag_Finaliza_Proceso;

-------------------------------------------------------------------------------------------------------------------
-- Procedimiento que obtiene infomacion  de un campo determinado, en un registro del archivo de entrada 
-------------------------------------------------------------------------------------------------------------------
procedure mmag_obtiene_campo_file(    p_linea      in varchar2      -- registro en donde va a buscar la informacion
                                    , p_campo      in varchar2      -- campo que desea obtener
                                    , p_codi_enti  in varchar2      -- entidad financiera a la que pertenece el archivo
                                    , p_valor      in out varchar2  -- retorna la informacion obtenida del campo en el archivo 
                                    , p_valor_conf in out varchar2  -- retorna valor configurado para el campo a buscar
                                    , p_codi_erro  in out number    -- error si existe un problema
                                    , p_mens_erro  in out varchar2
                                 )  is

salida exception;

ln_ubi  mmag_campos_entidad_financ_dat.posi_camp_enti_fina%type;
ln_lon  mmag_campos_entidad_financ_dat.long_camp_enti_fina%type;
lv_tip  mmag_campos_entidad_financ_dat.tipo_camp_enti_fina%type;
lv_alin mmag_campos_entidad_financ_dat.alin_camp_enti_fina%type;
lv_rell mmag_campos_entidad_financ_dat.cara_rell_camp_enti_fina%type;
lv_valor varchar2(500);
ln_pos  number:=0;
lv_est  varchar2(1):= 'E';
                                                   
begin
  p_codi_erro:=0;
  p_mens_erro:= null;

  -- Devuelve informacion sobre la configuracion de un campo especifico del archivo                                                       
  begin
    select a.posi_camp_enti_fina      
         , a.long_camp_enti_fina        
         , a.valo_cons_camp_enti_fina   
         , a.tipo_camp_enti_fina        
         , a.alin_camp_enti_fina
         , a.cara_rell_camp_enti_fina
    into   ln_ubi
         , ln_lon
         , p_valor_conf
         , lv_tip
         , lv_alin
         , lv_rell 
    from mmag_campos_entidad_financ_dat a
    where a.codi_enti_fina = p_codi_enti
    and a.esta_enti_fina = lv_est
    and a.codi_camp = p_campo;
  exception
    when no_data_found then
      p_codi_erro:= 1;
      p_mens_erro:= 'Revise la configuracion del archivo, No se encontro definido el campo '||p_campo ;
      raise salida;
    when too_many_rows then
      p_codi_erro:= 2;
      p_mens_erro:= 'Revise la configuracion del archivo, Se encontro definido varios registros para el campo '||p_campo ;
      raise salida;
    when others then
      p_codi_erro:= 4;
      p_mens_erro:= 'Ocurrio el siguiente error al obtener campo desde el archivo: '||sqlerrm ;
      raise salida;

  end;   
  if  lv_tip <> 'D' then
      p_codi_erro:= 3;
      p_mens_erro:= 'Revise la configuracion del archivo, El campo '||p_campo||' no es un campo de detalle';
      raise salida;    
  end if;

/*  -- obtiene la posicion inicial del campo en el registro, dependiendo de su posicion 
  select nvl(sum(a.long_camp_enti_fina),0)+1
  into   ln_pos
  from   mmag_campos_entidad_financ_dat a
  where  a.codi_enti_fina = p_codi_enti
  and    a.esta_enti_fina = lv_est
  and    a.posi_camp_enti_fina < ln_ubi;
  
  -- obtiene la informacion del campo, 
  -- extrae la informacion desde la posicion inicial tantas cantidad de caracteres
  -- segun indique su longitud
  select substr(p_linea,ln_pos,ln_lon)
  into   lv_valor
  from   dual;
  -- elimina los caracteres en blanco
  if lv_alin = 'D' then
        select ltrim(lv_valor,' ')
        into   lv_valor
        from dual;
     else 
        select rtrim(lv_valor,' ')
        into   lv_valor
        from dual;
  end if;   
  -- elimina los caracteres de relleno segun la alineacion del campo
  if lv_rell is not null then
     if lv_alin = 'D' then
        select ltrim(lv_valor,lv_rell)
        into   p_valor
        from dual;
     else 
        select rtrim(lv_valor,lv_rell)
        into   p_valor
        from dual;
     end if;   
  else
     p_valor:= lv_valor;
  end if;   */

  --Obtiene el contenido de un campo para el registro separado por un caracter especifico
  declare
    v_separa varchar2(1);
    v_long   number;
    v_inicio number;
    v_fin    number;
    v_flag   number;
  begin
    v_separa:='|';
    v_long:=length(p_linea);
    v_flag:=instr(p_linea,v_separa,1,ln_ubi);
    if ln_ubi=1 then 
       v_inicio:=1;
    else
       v_inicio:=instr(p_linea,v_separa,1,ln_ubi-1)+1;
    end if;
    if v_flag=0 then
       v_fin:=v_long+1;
    else
       v_fin:=instr(p_linea,v_separa,1,ln_ubi);
    end if;
    p_valor:=substr(p_linea,v_inicio,v_fin-v_inicio);
  exception
    when others then
      p_codi_erro:= 4;
      p_mens_erro:= 'Ocurrio el siguiente error al obtener el campo '||p_campo||' desde el archivo: '||sqlerrm;
      raise salida;  
  end;
  

exception
  when salida then
        null;
end;

--
                             
End MMAG_ACTUALIZA_MEDI;
/
