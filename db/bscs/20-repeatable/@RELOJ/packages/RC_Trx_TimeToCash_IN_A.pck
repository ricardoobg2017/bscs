create or replace package reloj.RC_Trx_TimeToCash_IN_A is
  --Datos configurados para el proceso 1--IN      
       GN_PROCESO_IN   NUMBER:=1;    
       GN_st_CkeckStatus   VARCHAR2(1):='S';    
       GN_STATUS_F   VARCHAR2(1):='F';
       GN_DESSHORT_IN VARCHAR2(6):='TTC_IN';
       GN_DES_FLAG_TTC_CONTRACT_IN VARCHAR2(45):='TTC_IN_FLAG_TABLE_CONTRACT_ALL';
       GN_DES_INS_TTC_CONTRACT_IN VARCHAR2(45):='TTC_IN_SQL_INSERT_TTC_CONTRACT_ALL';
       GN_DES_TRUN_TTC_CONTRACT_IN VARCHAR2(45):='TTC_IN_SQL_TRUNCATE_TTC_CONTRACT_ALL';           
                
--Cursors
       CURSOR C_IN(Cn_GroupTread IN NUMBER)  IS
         SELECT TTC_View_Customers_In_type(
                          customer_id, 
                          co_id, 
                          dn_num,
                          billcycle, 
                          custcode,
                          tmcode, 
                          prgcode, 
                          cstradecode, 
                          product_history_date,
                          CO_EXT_CSUIN, 
                          co_userlastmod,
                          ttc_insertiondate,
                          ttc_lastmoddate 
                          )
           FROM TTC_VIEW_CONTRACT_ALL_IN
         WHERE prgcode = decode(Cn_GroupTread,0,prgcode,Cn_GroupTread);   


--Types
        
--Procedures and Functions       
        PROCEDURE Prc_Header_IN (PV_TIPO             IN VARCHAR2,  --> A:Proceso Automatico, M:Proceso Manual                                                                
                                                                Pn_GroupTread IN NUMBER DEFAULT 0,
                                                                Pn_SosPid          IN NUMBER,
                                                                PV_ERROR         OUT VARCHAR2
                                                                ) ;


End RC_Trx_TimeToCash_IN_A;
/
create or replace package body reloj.RC_Trx_TimeToCash_IN_A is
 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     SIS Juan Ronquillo
  * Creacion:   26/11/2009
  * Modificado: 07/01/2010
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
   * Pro�sito:
  *     Planificacion de los contratos al reloj de cobranzas de acuerdo a su tipo de reloj.
  ********************************************************************************************************/

  -- Private type declarations ,constant declarations,variable declarations
                i_process                                   ttc_process_t;
                i_rule_element                          ttc_Rule_Element_t;
                i_Plannigstatus_In                    ttc_Plannigstatus_In_t;
                i_Fa_ciclos_bscs                       ttc_Fa_ciclos_Axis_bscs_t;                
                i_WorkFlow_User_t                 ttc_WorkFlow_User_t;
                i_Customers_In_t                      ttc_View_Customers_In_t;
                i_WorkFlow_Cust_t                  ttc_WorkFlow_Customer_t; 
                i_ContractPlannig_t                  ttc_ContractPlannig_t;
                i_ContractPlannigDetails_t     ttc_ContractPlannigDetails_t; 
                i_Bck_Process_t                        ttc_Bck_Process_t;
                i_Log_InOut_t                            ttc_Log_InOut_t;
                i_Process_Programmer_t       ttc_Process_Programmer_t;
                
--Procedures and Functions  
--**********************************************************************************
        FUNCTION Fct_Plannigstatus_In_t RETURN  TTC_Plannigstatus_In_t as
          doc_in  TTC_Plannigstatus_In_t;
        BEGIN
         SELECT  TTC_Plannigstatus_In_type(pk_id,
                                                                           st_id,
                                                                           st_seqno, 
                                                                           re_orden,
                                                                           st_sdes,
                                                                           st_status_i,
                                                                           st_status_next,
                                                                           st_duration,
                                                                           st_ext_duration ,
                                                                           st_duration_total,
                                                                           st_status_a,
                                                                           st_status_b,
                                                                           st_reason,
                                                                           st_alert,
                                                                           st_nodayalert,
                                                                           st_valid,
                                                                           co_Remark,
                                                                           co_lastmodDate,
                                                                           st_CkeckStatus,          
                                                                           st_valid_cond_1, 
                                                                           st_valid_reac, 
                                                                           st_valid_suspe, 
                                                                           st_valid_eval_1, 
                                                                           st_valid_eval_2                                                                                                                                                
                                                                           )  BULK COLLECT  INTO  doc_in 
              FROM  TTC_VIEW_PLANNIGSTATUS_IN;
          RETURN doc_in;
        END;

--**********************************************************************************
        FUNCTION Fct_Worflow_Customers_t RETURN  TTC_WorkFlow_Customer_t as
          doc_in  TTC_WorkFlow_Customer_t;
        BEGIN
         SELECT TTC_WorkFlow_Customer_type(PK_Id, CSTRADECODE,PRGCODE) 
                         BULK COLLECT  INTO  doc_in FROM  TTC_WorkFlow_Customer;
          RETURN doc_in;
        END;

--**********************************************************************************
        FUNCTION Fct_Fa_ciclos_bscs_t  RETURN   TTC_Fa_ciclos_Axis_bscs_t as
          doc_in    TTC_Fa_ciclos_Axis_bscs_t;
        BEGIN
                   SELECT   TTC_Fa_ciclos_Axis_bscs_type(id_ciclo,id_ciclo_admin,ciclo_default )
                      BULK COLLECT  INTO  doc_in 
                     FROM Fa_Ciclos_Axis_Bscs;
          RETURN doc_in;
        END;
        
--**********************************************************************************
         FUNCTION Fct_Save_ToWk_Planning_Head  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Cabecera
                  INSERT INTO  TTC_ContractPlannig_S
                  VALUES (i_ContractPlannig_t) ;                          
        
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_Save_ToWk_Planning_Head-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_ContractPlannig, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk_Planning_Head-Error General al intentar grabar en la tabla ttc_ContractPlannig, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;

--**********************************************************************************
         FUNCTION Fct_SaveTo_Wk_Planning_Details  RETURN VARCHAR2 IS
                   Lv_Error   TTC_Log_InOut.Comment_Run%TYPE;
         BEGIN
                   Lv_Error:=NULL;
                  --Detalle
                  INSERT INTO  TTC_ContractPlannigDetails_s
                  VALUES  (i_ContractPlannigDetails_t);                         
        
                  RETURN(Lv_Error);
        EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_SaveTo_Wk_Planning_Details-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_ContractPlannigDetails, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_SaveTo_Wk_Planning_Details-Error General al intentar grabar en la tabla ttc_ContractPlannigDetails, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            END;
        
--**********************************************************************************        
        PROCEDURE Prc_Header_IN (PV_TIPO             IN VARCHAR2,  --> A:Proceso Automatico, M:Proceso Manual
                                                                Pn_GroupTread IN NUMBER DEFAULT 0,
                                                                Pn_SosPid          IN NUMBER,
                                                                PV_ERROR         OUT VARCHAR2
                                                                )   IS 
           CURSOR C_idtramite IS
              SELECT  TTC_idtramite.NEXTVAL FROM dual;

           CURSOR C_Trx_In IS
              SELECT  TTC_TRX_IN.NEXTVAL FROM dual;            
              
                    Lv_User_Axis       TTC_WorkFlow_User.Us_Name%TYPE;
                    Lv_User_Bscs       TTC_WorkFlow_User.Us_Name%TYPE;
                    Lv_User_Espejo    TTC_WorkFlow_User.Us_Name%TYPE;
                    Lv_Id_ciclo           Fa_Ciclos_Axis_Bscs.Id_Ciclo%TYPE;
                    Ln_Secuence         NUMBER;
                    Ln_Seqno              NUMBER;                    
                    Ln_ConTrx            NUMBER;
                    Ln_pk_id                NUMBER;
                    Ln_re_orden          NUMBER;
                    Ln_Customer_id   NUMBER;
                    Ln_Co_id                NUMBER;                                        
                    Ln_Sum_Time      NUMBER;
                    Ln_count_serv      NUMBER;
                    Lv_Des_Error        VARCHAR2(800);
                    doc_Wfk                 TTC_VIEW_PLANNIGSTATUS_IN%ROWTYPE;
                    Ld_Fecha               DATE;
                    Ld_Sysdate            DATE;
                    Ld_SysdateRun    DATE;
                    Ld_DateExec         DATE;                    
                    Lb_Flag_Valida    BOOLEAN;
                    Le_Error_Trx        EXCEPTION;
                    Le_Error_Valid     EXCEPTION;
                    Le_Error_Process EXCEPTION;
      	            --SCP:VARIABLES                    
                    ---------------------------------------------------------------
                    --SCP: C�digo generado automaticamente. Definici�n de variables
                    ---------------------------------------------------------------
                    ln_id_bitacora_scp number:=0; 
                    ln_total_registros_scp number:=0;
                    lv_id_proceso_scp varchar2(100):='TTC_IN';
                    lv_referencia_scp varchar2(100):='RC_Trx_TimeToCash_IN_A.Prc_Header_IN_A';
                    lv_unidad_registro_scp varchar2(30):='Cuentas';
                    ln_error_scp number:=0;
                    lv_error_scp varchar2(500);
                    ln_registros_procesados_scp number:=0;
                    ln_registros_error_scp number:=0;
                    lv_mensaje_apl_scp     varchar2(4000);
                    ---------------------------------------------------------------
                    Ln_CsTradeCode    NUMBER;
                    Ln_PrgCode             NUMBER;
        BEGIN
                   Ld_Sysdate:= to_date(SYSDATE,'dd/mm/rrrr');
                   Ld_SysdateRun:=SYSDATE;
                   PV_ERROR:=NULL;
                   Lv_Des_Error:=NULL;
                   Ln_ConTrx:=0;
                  --Cargar los datos del Proceso TTC_IN -->TTC_Process_s                  
                  i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GN_DESSHORT_IN);      
                  IF i_process.Process IS NULL THEN
                        Lv_Des_Error:='No existen datos configurados en la tabla TTC_Process_s';
                        RAISE Le_Error_Valid;
                  END IF;
                  --Ingreso de Inicio del proceso 
	                 ----------------------------------------------------------------------------
                   -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
                   ----------------------------------------------------------------------------
                   scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                                         lv_referencia_scp,
                                                         null,null,null,null,
                                                         ln_total_registros_scp,
                                                         lv_unidad_registro_scp,
                                                         ln_id_bitacora_scp,
                                                         ln_error_scp,lv_error_scp);
                   if ln_error_scp <>0 then
                      Lv_Des_Error:='Error en plataforma SCP, No se pudo iniciar la Bitacora';
                      RAISE Le_Error_Valid;
                   end if;
                   ----------------------------------------------------------------------
                  i_Log_InOut_t:= ttc_Log_InOut_t(Process => nvl(i_process.Process,0),
                                                                            RunDate => SYSDATE,
                                                                            LastDate => NULL,
                                                                            StatusProcess => 'OKI',
                                                                            Comment_Run => NULL                                                                           
                                                                            );                  
                  --Verifica si el proceso se encuentra en ejecuci�n
                  IF  RC_Api_TimeToCash_Rule_ALL.Fct_Process_DesShort_Run(GN_DESSHORT_IN,i_process.Duration) THEN
                        Lv_Des_Error:='El proceso ya se encuentra en ejecuci�n, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
                        RAISE Le_Error_Process;
                  END IF;            

                  --Cargar los datos del Proceso TTC_IN --> TTC_rule_element
                  i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GN_DESSHORT_IN);
                 IF i_rule_element.Process IS NULL THEN
                        Lv_Des_Error:='No existen datos configurados en la tabla TTC_rule_element';
                        RAISE Le_Error_Valid;
                  END IF;
                        

                  --Graba el proceso para controlar el bloqueo de los procesos
                   i_Bck_Process_t:= ttc_Bck_Process_t(Process => i_process.Process,
                                                                                      DesShort => i_process.DesShort,
                                                                                      GroupTread => Pn_GroupTread,
                                                                                      Tread_NO => 0,
                                                                                      FilterField => i_process.FilterField,
                                                                                      LastRunDate => SYSDATE,
                                                                                      MaxDelay => i_process.MaxDelay,
                                                                                      Refresh_Cycle => i_process.Refresh_Cycle,
                                                                                      SosPid => Pn_SosPid
                                                                                     );
                   Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Bck_Process(i_Bck_Process_t);
                   IF Lv_Des_Error IS NOT NULL THEN
                        Lv_Des_Error:='Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesi�n levantada';
                        RAISE Le_Error_Process;
                   END IF;                                                                                                                                                            
		               ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='INICIO '||lv_id_proceso_scp;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         Null,
                                                         Null,
                                                         0,
                                                         lv_unidad_registro_scp,
                                                         Null,Null,
                                                         null,null,'N',ln_error_scp,lv_error_scp);
                   
                   ----------------------------------------------------------------------------
                  --Verificar si existe fecha programada para la ejecuci�n
                  IF PV_TIPO ='A' THEN --Si el proceso es automatico, caso contrario se ejecuta manual a partir de la �ltima ejecuci�n
                          Ld_DateExec:=  RC_Api_TimeToCash_Rule_ALL.Fct_Planning_Hour_DateValid(i_process.Process);
                          IF Ld_DateExec IS NULL THEN
                                Lv_Des_Error:='No existen datos configurados en la tabla TTC_Planning_Hour, para ejecutar el d�a de Hoy';
                                RAISE Le_Error_Valid;
                          END IF;
                  END IF;                  
                                   
                  --Cargar los datos de la configuraci�n 
                  i_Plannigstatus_In:=Fct_Plannigstatus_In_t;
                  i_WorkFlow_Cust_t:=Fct_Worflow_Customers_t;
                  i_Fa_ciclos_bscs:=Fct_Fa_ciclos_bscs_t;                      
                                   
                  --Cargar los datos del Usuario de Procesamiento           
                  i_WorkFlow_User_t:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_t(i_rule_element.User_A);
                  Lv_User_Axis:=i_WorkFlow_User_t.Us_Name;
                  i_WorkFlow_User_t:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_t(i_rule_element.User_B);
                  Lv_User_Bscs:=i_WorkFlow_User_t.Us_Name;
                  i_WorkFlow_User_t:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_t(i_rule_element.User_E);
                  Lv_User_Espejo:=i_WorkFlow_User_t.Us_Name;
                  
                  OPEN C_IN(Pn_GroupTread);
                      LOOP   
                      FETCH C_IN
                      BULK COLLECT INTO    i_Customers_In_t 
                      LIMIT i_rule_element.Value_Max_Trx_By_Execution;   
                                            
                           FOR rc_in IN (select * from table(cast( i_Customers_In_t as ttc_View_Customers_In_t)))  LOOP
                               --Verifica si existe el contrato antes de ingresarlo     
                               IF nvl(RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Contract_in(rc_in.Co_id),0)=0 THEN
                                     BEGIN
                                        Lb_Flag_Valida:=TRUE;
                                       --Setea la bandera de validaci�n de la configuraci�n de los par�metros                                           
                                        Ln_Customer_id:=rc_in.Customer_id;
                                        Ln_Co_id:=rc_in.Co_id;
                                        Ln_CsTradeCode:=rc_in.cstradecode;
                                        Ln_PrgCode:=rc_in.prgcode;
                                       --***************************************************************
                                         SELECT pk_id INTO Ln_pk_id   
                                           FROM table(cast(i_WorkFlow_Cust_t as ttc_WorkFlow_Customer_t))  wkf_cust 
                                        WHERE wkf_cust.cstradecode=rc_in.cstradecode 
                                               AND wkf_cust.prgcode=rc_in.prgcode;
                                       --***************************************************************
                                         SELECT min(Pla.re_orden) INTO Ln_re_orden 
                                           FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                         WHERE Pla.pk_id=Ln_pk_id
                                                AND Pla.st_status_i= rc_in.CO_EXT_CSUIN;
                                       --***************************************************************
                                         SELECT Id_ciclo INTO Lv_Id_ciclo 
                                           FROM table(cast(i_Fa_ciclos_bscs as ttc_Fa_ciclos_Axis_bscs_t)) Fa
                                         WHERE Fa.id_ciclo_admin=rc_in.Billcycle;
                                      --***************************************************************
                                         SELECT * INTO doc_Wfk
                                           FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                        WHERE Pla.pk_id=Ln_pk_id 
                                               AND Pla.re_orden=Ln_re_orden;
                                      --***************************************************************                                           
                                          Ld_Fecha:=to_date(nvl(rc_in.product_history_date,SYSDATE)+nvl(doc_Wfk.st_duration_total,0),'dd/mm/rrrr');
                                          IF Ld_Fecha<Ld_Sysdate THEN
                                               Ld_Fecha:=Ld_Sysdate;
                                          END IF;  
                                        
                                        --Cargar el valor estimado de la transacci�n el costo de ejecuci�n
                                           Ln_count_serv:=RC_Trx_TimeToCash_Customize.fct_Return_Count_services(rc_in.Co_id);                                          
                                           
                                   EXCEPTION            
                                       WHEN OTHERS THEN 
                                             Lb_Flag_Valida:=FALSE;                        
                                             Lv_Des_Error:='Prc_Header_IN-'||'Los datos de configuraci�n como el PRGCODE y CSTRADECODE para el '||','||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id||',CstradeCode:'||Ln_CsTradeCode||',Prgcode:'||Ln_PrgCode||', No se encuentran configurados en la tabla ttc_workflow_relation_s';                                                                   
                                              i_Log_InOut_t.LastDate:=SYSDATE;
                                              i_Log_InOut_t.StatusProcess:='ERR';
                                              i_Log_InOut_t.Comment_Run:=  Lv_Des_Error;                      
					                                    ----------------------------------------------------------------------
                                              -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                               ----------------------------------------------------------------------
                                               ln_registros_error_scp:=ln_registros_error_scp+1;
                                               lv_mensaje_apl_scp:='    Error de Validaci�n Configuraci�n CstradeCode y Prgcode';
                                               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                     lv_mensaje_apl_scp,
                                                                                     Lv_Des_Error,
                                                                                     Null,
                                                                                     2,
                                                                                     Null,Null,Null,
                                                                                     null,null,'N',ln_error_scp,lv_error_scp);
                                               
                                               ----------------------------------------------------------------------------
                                              Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                                   END;      
                                   --****************************************************************************************
                                   --****************************************************************************************
                                   IF Lb_Flag_Valida THEN
                                       BEGIN             
                                           --Cargar la secuencia del tr�mite
                                            OPEN C_idtramite ;
                                            FETCH C_idtramite INTO Ln_Secuence;
                                            CLOSE C_idtramite;                             
                                          --Cabecera 
                                              i_ContractPlannig_t:=ttc_ContractPlannig_t(Customer_id => rc_in.Customer_id,
                                                                                                                             Co_id => rc_in.Co_id,
                                                                                                                             Dn_Num => rc_in.Dn_Num,
                                                                                                                             Co_Period => rc_in.CO_EXT_CSUIN,
                                                                                                                             Co_Status_A => doc_Wfk.st_status_a,
                                                                                                                             Ch_Status_B => doc_Wfk.st_status_b,
                                                                                                                             Co_Reason => doc_Wfk.st_reason,
                                                                                                                             Co_TimePeriod => doc_Wfk.ST_STATUS_NEXT,
                                                                                                                             Ch_ValidFrom => Ld_Fecha,
                                                                                                                             Tmcode => rc_in.Tmcode,
                                                                                                                             Prgcode => rc_in.Prgcode,
                                                                                                                             AdminCycle =>Lv_Id_ciclo,
                                                                                                                             Billcycle => rc_in.Billcycle,
                                                                                                                             NoVerify => 0,
                                                                                                                             InsertionDate => SYSDATE,
                                                                                                                             LastModDate => NULL
                                                                                                                             );
                                               Lv_Des_Error:=Fct_Save_ToWk_Planning_Head;                  
                                               IF Lv_Des_Error IS NOT NULL THEN
                                                        RAISE Le_Error_Trx;
                                               END IF;                                                                                                                           
                                               Ln_Sum_Time:=0;
                                               FOR rc_wfk in (SELECT * 
                                                                               FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                                                            WHERE Pla.pk_id=Ln_pk_id 
                                                                                   AND Pla.re_orden>=Ln_re_orden
                                                                           ) LOOP
                                                        Ln_Sum_Time:=Ln_Sum_Time+nvl(rc_wfk.st_duration_total,0);                   
                                                        Ld_Fecha:=to_date(rc_in.product_history_date+Ln_Sum_Time,'dd/mm/rrrr');
                                                        IF Ld_Fecha<Ld_Sysdate THEN
                                                             Ld_Fecha:=Ld_Sysdate;
                                                        END IF;
                                                        --Detalle                                              
                                                        i_ContractPlannigDetails_t:=ttc_ContractPlannigDetails_t(Customer_id => rc_in.Customer_id,
                                                                                                                                                                Co_id => rc_in.Co_id,
                                                                                                                                                                PK_Id => rc_wfk.pk_id ,
                                                                                                                                                                ST_Id => rc_wfk.ST_Id,
                                                                                                                                                                ST_Seqno => rc_wfk.ST_Seqno,
                                                                                                                                                                RE_Orden => rc_wfk.RE_Orden,
                                                                                                                                                                Custcode => rc_in.Custcode,
                                                                                                                                                                Dn_Num => rc_in.Dn_Num,
                                                                                                                                                                Co_Period => rc_wfk.ST_STATUS_NEXT,
                                                                                                                                                                Co_Status_A => rc_wfk.st_status_a,
                                                                                                                                                                Ch_Status_B => rc_wfk.st_status_b,
                                                                                                                                                                Co_Reason => rc_wfk.st_reason,
                                                                                                                                                                Co_TimePeriod => rc_wfk.st_duration_total,
                                                                                                                                                                Ch_TimeAccionDate => Ld_Fecha,
                                                                                                                                                                Tmcode => rc_in.Tmcode,
                                                                                                                                                                Prgcode => rc_in.Prgcode,
                                                                                                                                                                AdminCycle => Lv_Id_ciclo,
                                                                                                                                                                Billcycle => rc_in.Billcycle,
                                                                                                                                                                ValueTrx => Ln_count_serv,
                                                                                                                                                                OrderByTrx => 0,
                                                                                                                                                                Co_IdTramite =>Ln_Secuence ,
                                                                                                                                                                Co_Remark =>  rc_wfk.Co_Remark,
                                                                                                                                                                Co_UserTrx_1 => Lv_User_Axis,
                                                                                                                                                                Co_UserTrx_2 => Lv_User_Bscs,
                                                                                                                                                                Co_UserTrx_3 => Lv_User_Espejo,
                                                                                                                                                                Co_CommandTrx_1 => NULL,
                                                                                                                                                                Co_CommandTrx_2 => NULL,
                                                                                                                                                                Co_CommandTrx_3 => NULL,
                                                                                                                                                                Co_NoIntento => 1,
                                                                                                                                                                Co_StatusTrx =>nvl(rc_wfk.ST_VALID, i_rule_element.StatusTrx_Ini),
                                                                                                                                                                Co_RemarkTrx => NULL,
                                                                                                                                                                InsertionDate =>nvl(rc_wfk.co_LastModDate, SYSDATE),
                                                                                                                                                                LastModDate => rc_wfk.co_LastModDate
                                                                                                                                                                ); 
                                                              Lv_Des_Error:=Fct_SaveTo_Wk_Planning_Details;
                                                              IF Lv_Des_Error IS NOT NULL THEN     
                                                                     Ln_Customer_id:=rc_in.Customer_id;
                                                                     Ln_Co_id:=rc_in.Co_id;                                                                                                         
                                                                     RAISE Le_Error_Trx;
                                                              END IF;
                                               END LOOP; 
                                            --***********--
                                            Ln_ConTrx:=Ln_ConTrx+1;
                                            IF Ln_ConTrx>=i_rule_element.Value_Max_TrxCommit THEN
                                                 COMMIT;
                                                 -- SCP:AVANCE
                                                   -----------------------------------------------------------------------
                                                   -- SCP: C�digo generado autom�ticamente. Registro de avance de proceso
                                                   -----------------------------------------------------------------------
                                                   ln_registros_procesados_scp:=ln_registros_procesados_scp+i_rule_element.Value_Max_TrxCommit;
                                                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                                         ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);                                                               
                                                   -----------------------------------------------------------------------
                                                   Ln_ConTrx:=0;
                                            END IF;                       
                                            --***********--
                                        EXCEPTION            
                                           WHEN Le_Error_Trx THEN
                                                     --************
                                                      -- ROLLBACK;  
                                                      --************ 
                                                      Lv_Des_Error:='Prc_Header_IN-'||Lv_Des_Error||','||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id;                                                                   
                                                      i_Log_InOut_t.LastDate:=SYSDATE;
                                                      i_Log_InOut_t.StatusProcess:='ERR';
                                                      i_Log_InOut_t.Comment_Run:=  Lv_Des_Error;                      
						                                          ----------------------------------------------------------------------
                                                      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                                       ----------------------------------------------------------------------
                                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                                       lv_mensaje_apl_scp:='    Error en el procesaminento';
                                                       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                             lv_mensaje_apl_scp,
                                                                                             Lv_Des_Error,
                                                                                             Null,
                                                                                             2,
                                                                                             Null,'Customer_id','Co_id',
                                                                                             Ln_Customer_id,Ln_Co_id,'N',ln_error_scp,lv_error_scp);
                                                        
                                                       ----------------------------------------------------------------------------
                                                      Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                                           WHEN OTHERS THEN
                                                      --************
                                                      --ROLLBACK;
                                                      --************
                                                      Lv_Des_Error:='Prc_Header_IN-Error al intentar grabar registros en la tabla ttc_ContractPlannig y ttc_ContractPlannigDetails-->'||'customer_id:'||Ln_Customer_id||',Co_id:'||Ln_Co_id||','||substr(SQLERRM,1,500);
                                                      i_Log_InOut_t.LastDate:=SYSDATE;
                                                      i_Log_InOut_t.StatusProcess:='ERR';
                                                      i_Log_InOut_t.Comment_Run:=  Lv_Des_Error;                      
						                                          ----------------------------------------------------------------------
                                                      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                                       ----------------------------------------------------------------------
                                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                                       lv_mensaje_apl_scp:='    Error en el procesaminento';
                                                       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                             lv_mensaje_apl_scp,
                                                                                             Lv_Des_Error,
                                                                                             Null,
                                                                                             2,
                                                                                             Null,'Customer_id','Co_id',
                                                                                             Ln_Customer_id,Ln_Co_id,'N',ln_error_scp,lv_error_scp);
                                                        
                                                       ----------------------------------------------------------------------------  
                                                      Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
                                           END;
                                    END IF;
                                END IF;                                                                                                              
                           END LOOP;           
                           --***********--
                           COMMIT;
                           --***********--     
			                     ---------------------------------------------------------------------
                           -- SCP:AVANCE
                           IF Ln_ConTrx>0 THEN
                                 ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                                 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,
                                                               ln_registros_error_scp,ln_error_scp,lv_error_scp);
                           END IF;                              
                           ----------------------------------------------------------------------------
                            EXIT WHEN C_IN%NOTFOUND;                                    
                       END LOOP;        
                       CLOSE C_IN;
                       
                  --Graba en la tabla programmer la fecha de ejecucion del proceso
                    OPEN C_Trx_In ;
                   FETCH C_Trx_In INTO Ln_Seqno;
                   CLOSE C_Trx_In;                                                                                                          
                   i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                                                                                      Seqno => Ln_Seqno,
                                                                                                                      RunDate => Ld_SysdateRun,
                                                                                                                      LastModDate => Ld_SysdateRun
                                                                                                                     );
                   Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
                   IF Lv_Des_Error IS NOT NULL THEN
                        RAISE Le_Error_Valid;
                   END IF;                                                                                                                                                            

		              ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='FIN '||lv_id_proceso_scp;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         'RC_Trx_TimeToCash_IN- Ejecutado con Exito'
                                                         ,Null,
                                                         0,
                                                         lv_unidad_registro_scp,
                                                         Null,Null,null,null,
                                                         'N',ln_error_scp,lv_error_scp);
                   ----------------------------------------------------------------------
                  --SCP:FIN
                  
                  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                  
                  ----------------------------------------------------------------------------
                  Lv_Des_Error:='Prc_Header_IN-SUCESSFUL';
                  PV_ERROR:=Lv_Des_Error;
                  --Registro del Fin del proceso 
                   Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                   i_Log_InOut_t.LastDate:=SYSDATE;
                   i_Log_InOut_t.StatusProcess:='OKI';
                   i_Log_InOut_t.Comment_Run:= 'Proceso: RC_Trx_TimeToCash_IN_A- Ejecutado con Exito';
                   Lv_Des_Error:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);

        EXCEPTION            
               WHEN Le_Error_process THEN
                        PV_ERROR := 'Proceso: RC_Trx_TimeToCash_IN_A- Ejecutado con ERROR-->'||'Prc_Header_IN-'||Lv_Des_Error;
                         i_Log_InOut_t.LastDate:=SYSDATE;
                         i_Log_InOut_t.StatusProcess:='ERR';
                         i_Log_InOut_t.Comment_Run:= PV_ERROR;                      
                         ----------------------------------------------------------------------
                         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                         ----------------------------------------------------------------------
                         lv_mensaje_apl_scp:='   Proceso: RC_Trx_TimeToCash_IN_A-Error en Validaci�n';
                          If ln_registros_error_scp = 0 Then
                             ln_registros_error_scp:=-1;
                          End If;
                         ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                               lv_mensaje_apl_scp,
                                                                                               PV_ERROR,
                                                                                               Null,3,Null,Null,
                                                                                               Null,null,null,'S',ln_error_scp,lv_error_scp
                                                                                               );
                         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                         ----------------------------------------------------------------------------
                         i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
 
              WHEN Le_Error_Valid THEN                    
                  PV_ERROR := 'Proceso: RC_Trx_TimeToCash_IN_A- Ejecutado con ERROR-->'||'Prc_Header_IN-'||Lv_Des_Error;
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                   i_Log_InOut_t.LastDate:=SYSDATE;
                   i_Log_InOut_t.StatusProcess:='ERR';
                   i_Log_InOut_t.Comment_Run:= PV_ERROR;                      
		               ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='   Proceso: RC_Trx_TimeToCash_IN_A-Error en Validaci�n';
                    If ln_registros_error_scp = 0 Then
                       ln_registros_error_scp:=-1;
                    End If;
                   ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                         lv_mensaje_apl_scp,
                                                                                         PV_ERROR,
                                                                                         Null,3,Null,Null,
                                                                                         Null,null,null,'S',ln_error_scp,lv_error_scp
                                                                                         );
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                   ----------------------------------------------------------------------------
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
              WHEN NO_DATA_FOUND THEN                 
                   PV_ERROR:= 'Proceso: RC_Trx_TimeToCash_IN_A- Ejecutado con ERROR-->'||'Prc_Header_IN-'||'No existen datos para la ejecuci�n del d�a de hoy, para el criterio de inserci�n al Reloj de cobranzas, 1 de Mensajeria';
                    i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);                   
                   i_Log_InOut_t.LastDate:=SYSDATE;
                   i_Log_InOut_t.StatusProcess:='ERR';
                   i_Log_InOut_t.Comment_Run:= PV_ERROR;                                         
   
		               --SCP:MENSAJE                                                                                                                                                                                                  
                   ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Proceso: RC_Trx_TimeToCash_IN_A-Error No existen datos-NO_DATA_FOUND';
                   If ln_registros_error_scp = 0 Then
                       ln_registros_error_scp:=-1;
                    End If;
                   ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                                         lv_mensaje_apl_scp,
                                                                                         PV_ERROR,
                                                                                         Null,2,Null,Null,
                                                                                         Null,null,null,
                                                                                         'S',ln_error_scp,lv_error_scp
                                                                                         );
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                   ----------------------------------------------------------------------------
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
              WHEN OTHERS THEN
                   ROLLBACK; 
                   PV_ERROR:='Prc_Header_IN-Error General-'||substr(SQLERRM,1,500);                   
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Del_To_Bck_Process(i_Bck_Process_t);
                   i_Log_InOut_t.LastDate:=SYSDATE;
                   i_Log_InOut_t.StatusProcess:='ERR';
                   i_Log_InOut_t.Comment_Run:=  PV_ERROR;                      
		               ----------------------------------------------------------------------
                   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Prc_Header_IN-Error General';
                   If ln_registros_error_scp = 0 Then
                       ln_registros_error_scp:=-1;
                    End If;
                   ln_registros_procesados_scp:=ln_registros_procesados_scp+Ln_ConTrx;
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         PV_ERROR,
                                                         Null,
                                                         3,Null,
                                                         Null,
                                                         Null,null,null,
                                                         'S',ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,ln_registros_error_scp,ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                   ----------------------------------------------------------------------------
                   i_Log_InOut_t.Comment_Run:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Log_InOut(i_Log_InOut_t);
        END Prc_Header_IN;            

END RC_Trx_TimeToCash_IN_A;
/
