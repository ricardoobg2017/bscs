create or replace package Rc_Trx_TimeToCash_Planning is

  -- Author  : JORDAN.RODRIGUEZ
  -- Created : 06/07/2016 8:33:39
  -- Purpose : Prc_Header
  -- Public type declarations
  PROCEDURE PRC_HEADER(PN_SOSPID     IN NUMBER,
                       PN_GROUPTREAD IN NUMBER DEFAULT 0,
                       PV_TIPO       IN VARCHAR2,
                       PV_ERROR      OUT VARCHAR2);
  GN_HILO NUMBER:=1;
  PROCEDURE PRC_HEADER_IN(PV_TIPO IN VARCHAR2, PV_ERROR OUT VARCHAR2, Pn_DiasInactivarServ IN NUMBER DEFAULT NULL);
  FUNCTION FCT_PLANNIGSTATUS_IN_T RETURN TTC_PLANNIGSTATUS_IN_T;
  FUNCTION FCT_WORFLOW_CUSTOMERS_T RETURN TTC_WORKFLOW_CUSTOMER_T;
  FUNCTION FCT_FA_CICLOS_BSCS_T RETURN TTC_FA_CICLOS_AXIS_BSCS_T;
  FUNCTION FCT_SAVE_TOWK_PLANNING_HEAD RETURN VARCHAR2;
  FUNCTION FCT_SAVETO_WK_PLANNING_DETAILS RETURN VARCHAR2;
  
END Rc_Trx_TimeToCash_Planning;
/
CREATE OR REPLACE PACKAGE BODY Rc_Trx_TimeToCash_Planning IS

  GV_ID_PROCESO_SCP           VARCHAR2(100) := 'TTC_PLANING';
  GV_REFERENCIA_SCP           VARCHAR2(100) := 'RELOJ.RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER';
  GN_TOTAL_REGISTROS_SCP      NUMBER := 0;
  GV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'IN CO_ID';
  GN_REGISTROS_ERROR_SCP      NUMBER := 0;
  GN_ID_BITACORA_SCP          NUMBER := 0;
  GN_DESSHORT_PLANING         VARCHAR2(100) := 'TTC_PLANING';
  GN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
  LN_SEQNO                    NUMBER;
  -- Private type declarations ,constant declarations,variable declarations
  I_PROCESS                  TTC_PROCESS_T;
  I_RULE_ELEMENT             TTC_RULE_ELEMENT_T;
  I_BCK_PROCESS_T            TTC_BCK_PROCESS_T;
  I_PROCESS_PROGRAMMER_T     TTC_PROCESS_PROGRAMMER_T;
  I_CUSTOMERS_IN_T           TTC_VIEW_CUSTOMERS_IN_T;
  I_CUST_TO_SAVE_IN_T        TTC_VIEW_CUSTOMERS_IN_TYPE;
  I_PLANNIGSTATUS_IN         TTC_PLANNIGSTATUS_IN_T;
  I_WORKFLOW_CUST_T          TTC_WORKFLOW_CUSTOMER_T;
  I_FA_CICLOS_BSCS           TTC_FA_CICLOS_AXIS_BSCS_T;
  I_WORKFLOW_USER_T          TTC_WORKFLOW_USER_T;
  I_CONTRACTPLANNIG_T        TTC_CONTRACTPLANNIG_T;
  I_CONTRACTPLANNIGDETAILS_T TTC_CONTRACTPLANNIGDETAILS_T;

  GV_USER_AXIS               TTC_WORKFLOW_USER.US_NAME%TYPE;
  GV_USER_BSCS               TTC_WORKFLOW_USER.US_NAME%TYPE;
  GV_USER_ESPEJO             TTC_WORKFLOW_USER.US_NAME%TYPE;
  GV_BANDERA                 VARCHAR2(10);
  GN_CONTRX                  NUMBER;

  CURSOR C_TRX_IN IS
    SELECT TTC_TRX_IN.NEXTVAL FROM DUAL;
/*--=======================RELOJ DE COBRANZAS ======================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creaci�n del procedimiento principal el cual obtiene de la vista
                  RELOJ.TTC_VIEW_CUSTOMERS_PLANING todos los servicios que deben ser
                  planificados.
  --================================================================================--*/
/*--=======================RELOJ DE COBRANZAS ======================================--
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Anabell Amayquema
  Fecha         : 27/07/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Omitir consultas que se realizaban en el procedimiento
                  PRC_HEADER_IN para ubicarlos en el proceso principal PRC_HEADER de
		  forma que solo se realice una consulta.
  --================================================================================--*/
/*--=======================RELOJ DE COBRANZAS ======================================--
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Modificado por: Iro Anabell Amayquema
  Fecha         : 11/11/2016
  Proyecto      : 10995 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Asignaci�n de hilos para el proceso de suspensi�n de forma equitativa.
  --================================================================================--*/
/*--=======================RELOJ DE COBRANZAS ======================================--
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Modificado por: Iro Anabell Amayquema
  Fecha         : 23/11/2017
  Proyecto      : 11603 Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  Motivo        : Cambiar la planificacion del reloj de tal manera que las inactivaciones de los servicios 
                  (status 35) sean planificadas para el �ltimo dia del mes que corresponda
  --================================================================================--*/
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      30/07/2018
-- PROYECTO:       [12018] Equipo �gil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Notificaciones del proceso de inactivacion de reloj y cambio en la fecha de inactivacion.
--=====================================================================================--

  --=====================================================================================--
  -- MODIFICADO POR: HTS Ingrid Orozco.
  -- FECHA MOD:      29/03/2021
  -- PROYECTO:       [13200] Reloj de Cobranzas Clientes Corporativos voz.
  -- LIDER HTS:      HTS Gloria Rojas
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Categorizaci�n de clientes corporativos.
  --=====================================================================================--
  PROCEDURE PRC_HEADER(PN_SOSPID     IN NUMBER,
                       PN_GROUPTREAD IN NUMBER DEFAULT 0,
                       PV_TIPO       IN VARCHAR2, --> A:Proceso Automatico, M:Proceso Manual
                       PV_ERROR      OUT VARCHAR2) IS
  
    LN_ERROR_SCP  NUMBER := 0;
    LV_DES_ERROR  VARCHAR2(800);
    LD_SYSDATERUN DATE;
    LV_ERROR_SCP VARCHAR2(500);
    LN_CONTRX    NUMBER;
    LE_ERROR_VALID     EXCEPTION;
    LE_ERROR_LOOP_VIST EXCEPTION;
    LN_CUSTOMER_ID     NUMBER;
    LN_CO_ID           NUMBER;
    LV_MENSAJE_APL_SCP VARCHAR2(4000);
    LE_ERROR_PROCESS EXCEPTION;
    LE_ERROR_TRX     EXCEPTION;
    LV_OBSERVACION VARCHAR2(4000);
    LD_DATEEXEC    DATE;
    LN_MAX_HILOS   NUMBER:=50;

    CURSOR C_IN(CN_GROUPTREAD IN NUMBER) IS
      SELECT /*+ ALL_ROWS */
       TTC_VIEW_CUSTOMERS_IN_TYPE(CUSTOMER_ID,
                                  CO_ID,
                                  DN_NUM,
                                  BILLCYCLE,
                                  CUSTCODE,
                                  TMCODE,
                                  PRGCODE,
                                  CSTRADECODE,
                                  PRODUCT_HISTORY_DATE,
                                  CO_EXT_CSUIN,
                                  CO_USERLASTMOD,
                                  TTC_INSERTIONDATE,
                                  TTC_LASTMODDATE)
        FROM TTC_VIEW_CUSTOMERS_PLANNING 
       WHERE PRGCODE = DECODE(CN_GROUPTREAD, 0, PRGCODE, CN_GROUPTREAD)
       ORDER BY CUSTOMER_ID;

    CURSOR C_BANDERA IS
      SELECT VALUE_RETURN_SHORTSTRING
        FROM RELOJ.TTC_RULE_VALUES_S
       WHERE PROCESS = '1'
         AND VALUE_RULE_STRING = 'TTC_IN_FLAG_IN';

    CURSOR C_HILOS IS 
     SELECT NVL(VALOR,50)
       FROM GV_PARAMETROS T
      WHERE ID_TIPO_PARAMETRO = 10995
        AND ID_PARAMETRO = 'NUM_HILOS_SUSP_BSCS';
    
    --INI [12018] IRO ABA 
    CURSOR C_PARAMETROS(Cn_IdTipoParametro NUMBER, Cv_IdParametro VARCHAR2) IS
    SELECT G.VALOR
      FROM SYSADM.GV_PARAMETROS G
     WHERE G.ID_TIPO_PARAMETRO = Cn_IdTipoParametro
       AND G.ID_PARAMETRO = Cv_IdParametro;
    
    Ln_DiasInactivarServ   NUMBER;
    --FIN [12018] IRO ABA 

  BEGIN
    LD_SYSDATERUN := SYSDATE - 5 / 1440; --Fecha para tomar encuenta la proxima Planificacion cuando se realize un cambio de CSUIN
    GN_CONTRX     := 0;

    --Inicio de Bitacora
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(GV_ID_PROCESO_SCP,
                                          GV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          GN_TOTAL_REGISTROS_SCP,
                                          GV_UNIDAD_REGISTRO_SCP,
                                          GN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP <> 0 THEN
      LV_DES_ERROR := 'ERROR EN PLATAFORMA SCP, NO SE PUDO INICIAR LA BITACORA';
      RAISE LE_ERROR_VALID;
    END IF;
    --Carga los datos de la tabla TTC_PROCESS_S cuando el DESSHORT = TTC_PLANING
    I_PROCESS := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_PROCESSDESSHORT_T(GN_DESSHORT_PLANING);
    IF I_PROCESS.PROCESS IS NULL THEN
      LV_DES_ERROR := 'NO EXISTEN DATOS CONFIGURADOS EN LA TABLA TTC_PROCESS_S';
      RAISE LE_ERROR_VALID;
    END IF;

    IF PV_TIPO = 'A' THEN
      --SI EL PROCESO ES AUTOMATICO, CASO CONTRARIO SE EJECUTA MANUAL A PARTIR DE LA �LTIMA EJECUCI�N
      LD_DATEEXEC := RC_API_TIMETOCASH_RULE_ALL.FCT_PLANNING_HOUR_DATEVALID(I_PROCESS.PROCESS);
      IF LD_DATEEXEC IS NULL THEN
        LV_DES_ERROR := 'No existen datos configurados en la tabla TTC_Planning_Hour, para ejecutar el d�a de Hoy';
        RAISE LE_ERROR_VALID;
      END IF;
    END IF;

    OPEN C_BANDERA;
    FETCH C_BANDERA
      INTO GV_BANDERA;
    CLOSE C_BANDERA;

    --10995 INI AAM -> Obtener la cantidad de hilos que deben asignarse
    OPEN C_HILOS;
    FETCH C_HILOS INTO LN_MAX_HILOS;
    CLOSE C_HILOS;
    --10995 INI AAM

    --Verifica si el proceso se encuentra en ejecuci�n
    IF RC_API_TIMETOCASH_RULE_ALL.FCT_PROCESS_DESSHORT_RUN(GN_DESSHORT_PLANING,
                                                           I_PROCESS.DURATION) THEN
      LV_DES_ERROR := 'El proceso ya se encuentra en ejecuci�n, verifique si existe alguna inconsistencia en la tabla TTC_BCK_PROCESS_S  y vuelva a ejecutarlo';
      RAISE LE_ERROR_PROCESS;
    END IF;
    --Graba el proceso para controlar el bloqueo de los procesos
    I_BCK_PROCESS_T := TTC_BCK_PROCESS_T(PROCESS       => I_PROCESS.PROCESS,
                                         DESSHORT      => I_PROCESS.DESSHORT,
                                         GROUPTREAD    => 0,
                                         TREAD_NO      => 0,
                                         FILTERFIELD   => I_PROCESS.FILTERFIELD,
                                         LASTRUNDATE   => SYSDATE,
                                         MAXDELAY      => I_PROCESS.MAXDELAY,
                                         REFRESH_CYCLE => I_PROCESS.REFRESH_CYCLE,
                                         SOSPID        => PN_SOSPID);
    LV_DES_ERROR    := RC_API_TIMETOCASH_RULE_ALL.FCT_SAVE_TO_BCK_PROCESS(I_BCK_PROCESS_T);
    IF LV_DES_ERROR IS NOT NULL THEN
      LV_DES_ERROR := 'Existe otro proceso levantado, verifique la tabla ttc_bck_process, si requiere volver a ejecutar elimine el registro y ejecute nuevamente el proceso. Asegurese que no exista un sesi�n levantada';
      RAISE LE_ERROR_PROCESS;
    END IF;
    --Cargar los datos del Proceso TTC_PLANING --> TTC_rule_element
    I_RULE_ELEMENT := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_RULE_ELEMENTDESSHORT_T(GN_DESSHORT_PLANING);
    IF I_RULE_ELEMENT.PROCESS IS NULL THEN
      LV_DES_ERROR := 'No existen datos configurados en la tabla TTC_rule_element';
      RAISE LE_ERROR_VALID;
    END IF;

    --Cargar los datos de la configuraci�n 
    I_PLANNIGSTATUS_IN := FCT_PLANNIGSTATUS_IN_T;
    I_WORKFLOW_CUST_T  := FCT_WORFLOW_CUSTOMERS_T;
    I_FA_CICLOS_BSCS   := FCT_FA_CICLOS_BSCS_T;
    --Cargar los datos del Usuario de Procesamiento           
    I_WORKFLOW_USER_T := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_T(I_RULE_ELEMENT.USER_A);
    GV_USER_AXIS      := I_WORKFLOW_USER_T.US_NAME;
    I_WORKFLOW_USER_T := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_T(I_RULE_ELEMENT.USER_B);
    GV_USER_BSCS      := I_WORKFLOW_USER_T.US_NAME;
    I_WORKFLOW_USER_T := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_T(I_RULE_ELEMENT.USER_E);
    GV_USER_ESPEJO    := I_WORKFLOW_USER_T.US_NAME;
    --INI [12018] IRO ABA 
    OPEN C_PARAMETROS(12018, 'DIAS_INACT_RELOJ_MOVIL');
    FETCH C_PARAMETROS INTO Ln_DiasInactivarServ;
    CLOSE C_PARAMETROS;
    --FIN [12018] IRO ABA 
    BEGIN
      OPEN C_IN(PN_GROUPTREAD);
      LOOP
        FETCH C_IN BULK COLLECT
          INTO I_CUSTOMERS_IN_T LIMIT I_RULE_ELEMENT.VALUE_MAX_TRX_BY_EXECUTION;
        EXIT WHEN I_CUSTOMERS_IN_T.COUNT = 0;
        LN_CONTRX := 0;
        FOR RC_IN IN I_CUSTOMERS_IN_T.FIRST .. I_CUSTOMERS_IN_T.LAST LOOP
          BEGIN
            --Setea la bandera de validaci�n de la configuraci�n de los par�metros                                           
            LN_CUSTOMER_ID := I_CUSTOMERS_IN_T(RC_IN).CUSTOMER_ID;
            LN_CO_ID       := I_CUSTOMERS_IN_T(RC_IN).CO_ID;
            LN_CONTRX      := LN_CONTRX + 1;
            --Graba en la tabla ttc_contract_in 
            I_CUST_TO_SAVE_IN_T := TTC_VIEW_CUSTOMERS_IN_TYPE(CUSTOMER_ID          => I_CUSTOMERS_IN_T(RC_IN).CUSTOMER_ID,
                                                              CO_ID                => I_CUSTOMERS_IN_T(RC_IN).CO_ID,
                                                              DN_NUM               => I_CUSTOMERS_IN_T(RC_IN).DN_NUM,
                                                              BILLCYCLE            => I_CUSTOMERS_IN_T(RC_IN).BILLCYCLE,
                                                              CUSTCODE             => I_CUSTOMERS_IN_T(RC_IN).CUSTCODE,
                                                              TMCODE               => I_CUSTOMERS_IN_T(RC_IN).TMCODE,
                                                              PRGCODE              => I_CUSTOMERS_IN_T(RC_IN).PRGCODE,
                                                              CSTRADECODE          => I_CUSTOMERS_IN_T(RC_IN).CSTRADECODE,
                                                              PRODUCT_HISTORY_DATE => I_CUSTOMERS_IN_T(RC_IN).PRODUCT_HISTORY_DATE,
                                                              CO_EXT_CSUIN         => I_CUSTOMERS_IN_T(RC_IN).CO_EXT_CSUIN,
                                                              CO_USERLASTMOD       => NVL(I_CUSTOMERS_IN_T(RC_IN).CO_USERLASTMOD,'RELOJ-QC'),
                                                              TTC_INSERTIONDATE    => NVL(I_CUSTOMERS_IN_T(RC_IN).TTC_INSERTIONDATE,SYSDATE),
                                                              TTC_LASTMODDATE      => NULL);
            PRC_HEADER_IN(PV_TIPO  => PV_TIPO, --> A:PROCESO AUTOMATICO, M:PROCESO MANUAL                                                                
                          PV_ERROR => PV_ERROR,
                          Pn_DiasInactivarServ => Ln_DiasInactivarServ);
            IF PV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR_TRX;
            ELSE
              GN_REGISTROS_PROCESADOS_SCP := GN_REGISTROS_PROCESADOS_SCP + 1;
            END IF;
          EXCEPTION
            WHEN LE_ERROR_TRX THEN
              PV_ERROR := 'RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER_IN-' || PV_ERROR || ',' || 'CUSTOMER_ID:' ||
                          LN_CUSTOMER_ID || ',CO_ID:' || LN_CO_ID;
              -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
              GN_REGISTROS_ERROR_SCP := GN_REGISTROS_ERROR_SCP + 1;
              LV_MENSAJE_APL_SCP     := 'Error Planificacion: RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER_IN-Procesando Cuenta';
              SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                    LV_MENSAJE_APL_SCP,
                                                    PV_ERROR,
                                                    NULL,
                                                    2,
                                                    NULL,
                                                    'CUSTOMER_ID',
                                                    'CO_ID',
                                                    LN_CUSTOMER_ID,
                                                    LN_CO_ID,
                                                    'N',
                                                    LN_ERROR_SCP,
                                                    LV_ERROR_SCP);
             INSERT INTO RELOJ.TTC_CONTRACT_ALL_PLANING
               (CUSTCODE,
                CUSTOMER_ID,
                CO_ID,
                DN_NUM,
                BILLCYCLE,
                TMCODE,
                PRGCODE,
                PRODUCT_HISTORY_DATE,
                CO_EXT_CSUIN,
                CO_USERLASTMOD,
                TTC_INSERTIONDATE,
                TTC_LASTMODDATE,
                OBSERVACION)
             VALUES
               (I_CUSTOMERS_IN_T(RC_IN).CUSTCODE,
                I_CUSTOMERS_IN_T(RC_IN).CUSTOMER_ID,
                I_CUSTOMERS_IN_T(RC_IN).CO_ID,
                I_CUSTOMERS_IN_T(RC_IN).DN_NUM,
                I_CUSTOMERS_IN_T(RC_IN).BILLCYCLE,
                I_CUSTOMERS_IN_T(RC_IN).TMCODE,
                I_CUSTOMERS_IN_T(RC_IN).PRGCODE,
                I_CUSTOMERS_IN_T(RC_IN).PRODUCT_HISTORY_DATE,
                I_CUSTOMERS_IN_T(RC_IN).CO_EXT_CSUIN,
                I_CUSTOMERS_IN_T(RC_IN).CO_USERLASTMOD,
                I_CUSTOMERS_IN_T(RC_IN).TTC_INSERTIONDATE,
                SYSDATE,
                'ERROR EN EL PROCEDIMIENTO PRC_HEADER_IN: ' || PV_ERROR);
            WHEN OTHERS THEN
              PV_Error                    := 'RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER al momento de planificar la cuenta,' ||
                                             'customer_id:' ||
                                             LN_CUSTOMER_ID || ',CO_ID:' ||
                                             LN_CO_ID || ',' ||
                                             SUBSTR(SQLERRM, 1, 300);
              LV_OBSERVACION:=SUBSTR(SQLERRM, 1, 300);
              GN_REGISTROS_ERROR_SCP      := GN_REGISTROS_ERROR_SCP + 1;
              LV_MENSAJE_APL_SCP          := ' Error general: Rc_Trx_TimeToCash_Planig.Prc_Header Cuenta';
              SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                    LV_MENSAJE_APL_SCP,
                                                    PV_ERROR,
                                                    NULL,
                                                    2,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    'N',
                                                    LN_ERROR_SCP,
                                                    LV_ERROR_SCP);
              INSERT INTO RELOJ.TTC_CONTRACT_ALL_PLANING
                (CUSTCODE,
                 CUSTOMER_ID,
                 CO_ID,
                 DN_NUM,
                 BILLCYCLE,
                 TMCODE,
                 PRGCODE,
                 PRODUCT_HISTORY_DATE,
                 CO_EXT_CSUIN,
                 CO_USERLASTMOD,
                 TTC_INSERTIONDATE,
                 TTC_LASTMODDATE,
                 OBSERVACION)
              VALUES
                (I_CUSTOMERS_IN_T(RC_IN).CUSTCODE,
                 I_CUSTOMERS_IN_T(RC_IN).CUSTOMER_ID,
                 I_CUSTOMERS_IN_T(RC_IN).CO_ID,
                 I_CUSTOMERS_IN_T(RC_IN).DN_NUM,
                 I_CUSTOMERS_IN_T(RC_IN).BILLCYCLE,
                 I_CUSTOMERS_IN_T(RC_IN).TMCODE,
                 I_CUSTOMERS_IN_T(RC_IN).PRGCODE,
                 I_CUSTOMERS_IN_T(RC_IN).PRODUCT_HISTORY_DATE,
                 I_CUSTOMERS_IN_T(RC_IN).CO_EXT_CSUIN,
                 I_CUSTOMERS_IN_T(RC_IN).CO_USERLASTMOD,
                 I_CUSTOMERS_IN_T(RC_IN).TTC_INSERTIONDATE,
                 SYSDATE,
                 'ERROR EN EL LOOP PRINCIPAL: ' || LV_OBSERVACION);
          END;
         --10955 INI AAM
          GN_HILO := GN_HILO + 1;

          IF GN_HILO > LN_MAX_HILOS THEN
            GN_HILO := 1;
          END IF;
         --10955 FIN AAM
        END LOOP;
      END LOOP;
      CLOSE C_IN;
      OPEN C_TRX_IN;
      FETCH C_TRX_IN
        INTO LN_SEQNO;
      CLOSE C_TRX_IN;
      I_PROCESS_PROGRAMMER_T := TTC_PROCESS_PROGRAMMER_T(PROCESS     => I_PROCESS.PROCESS,
                                                         SEQNO       => LN_SEQNO,
                                                         RUNDATE     => LD_SYSDATERUN,
                                                         LASTMODDATE => LD_SYSDATERUN);
      LV_DES_ERROR           := RC_API_TIMETOCASH_RULE_ALL.FCT_SAVE_TO_PROCESS_PROGRAMMER(I_PROCESS_PROGRAMMER_T);
      IF LV_DES_ERROR IS NOT NULL THEN
        RAISE LE_ERROR_VALID;
      END IF;
      LV_MENSAJE_APL_SCP := 'FIN ' || GV_ID_PROCESO_SCP;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            'Rc_Trx_TimeToCash_Planig-Prc_Header Ejecutado con Exito',
                                            NULL,
                                            0,
                                            GV_UNIDAD_REGISTRO_SCP,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            GN_REGISTROS_PROCESADOS_SCP,
                                            GN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --SCP:FIN                  
      SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      LV_DES_ERROR := RC_API_TIMETOCASH_RULE_ALL.FCT_DEL_TO_BCK_PROCESS(I_BCK_PROCESS_T);
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Reloj.Rc_Trx_TimeToCash_Planig.Prc_Header-Error en la ejecuci�n del proceso' ||
                    SUBSTR(SQLERRM, 1, 500);
    END;
   --**************************************************************************************************
    IF PV_ERROR IS NOT NULL THEN
      LV_DES_ERROR := PV_ERROR;
      RAISE LE_ERROR_VALID;
    END IF;
      PV_ERROR     := 'PRC_HEADER-SUCESSFUL';
  EXCEPTION
    WHEN LE_ERROR_PROCESS THEN
      PV_ERROR                    := 'Proceso: Rc_Trx_TimeToCash_Planig- Ejecutado con ERROR-->' ||
                                     'Prc_Head-' || LV_DES_ERROR;
      LV_MENSAJE_APL_SCP := ' Proceso: Rc_Trx_TimeToCash_Planig-Error en Proceso PRC_HEADER';
      IF GN_REGISTROS_ERROR_SCP = 0 THEN
        GN_REGISTROS_ERROR_SCP := -1;
      END IF;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            GN_REGISTROS_PROCESADOS_SCP,
                                            GN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    WHEN LE_ERROR_VALID THEN
      PV_ERROR                    := 'Proceso: Rc_Trx_TimeToCash_Planig.Prc_Header- Ejecutado con ERROR-->' ||
                                     'Prc_Head-' || LV_DES_ERROR;
      LV_MENSAJE_APL_SCP          := '    Proceso: Rc_Trx_TimeToCash_Planig.Prc_Header-Error en Validaci�n';
      IF GN_REGISTROS_ERROR_SCP = 0 THEN
        GN_REGISTROS_ERROR_SCP := -1;
      END IF;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            GN_REGISTROS_PROCESADOS_SCP,
                                            GN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --Fin de Bitacora
      SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    WHEN OTHERS THEN
      PV_ERROR                    := ' RC_Trx_IN_To_Work-Prc_Head-Error al ejecutar el proceso-' ||
                                     SUBSTR(SQLERRM, 1, 500);
      LV_MENSAJE_APL_SCP          := ' Proceso: Rc_Trx_TimeToCash_Planig.Prc_Header-Error general';
      IF GN_REGISTROS_ERROR_SCP = 0 THEN
        GN_REGISTROS_ERROR_SCP := -1;
      END IF;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            2,
                                            GV_UNIDAD_REGISTRO_SCP,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            GN_REGISTROS_PROCESADOS_SCP,
                                            GN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --Fin de Bitacora
      SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  END PRC_HEADER;
/*--=======================RELOJ DE COBRANZAS ================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion del procedimiento encargado de realizar la planificacion de cada servicio.
  --======================================================================--*/
  PROCEDURE PRC_HEADER_IN(PV_TIPO IN VARCHAR2, PV_ERROR OUT VARCHAR2, Pn_DiasInactivarServ IN NUMBER DEFAULT NULL) IS
    CURSOR C_IDTRAMITE IS
      SELECT TTC_IDTRAMITE.NEXTVAL FROM DUAL;
  
    LV_ID_CICLO    FA_CICLOS_AXIS_BSCS.ID_CICLO%TYPE;
    LN_SECUENCE    NUMBER;
    LN_PK_ID       NUMBER;
    LN_RE_ORDEN    NUMBER;
    LN_CUSTOMER_ID NUMBER;
    LN_CO_ID       NUMBER;
    LN_SUM_TIME    NUMBER;
    LN_COUNT_SERV  NUMBER;
    LV_DES_ERROR   VARCHAR2(800);
    LV_DES_BITA   VARCHAR2(800);
    DOC_WFK        TTC_VIEW_PLANNIGSTATUS_IN%ROWTYPE;
    LD_FECHA       DATE;
    LB_FLAG_VALIDA BOOLEAN;
    LE_ERROR_TRX     EXCEPTION;
    LE_ERROR_VALID   EXCEPTION;
    LE_ERROR_PROCESS EXCEPTION;
    LN_ERROR_SCP                NUMBER := 0;
    LV_ERROR_SCP                VARCHAR2(500);
    LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
    LN_REGISTROS_ERROR_SCP      NUMBER := 0;
    LV_MENSAJE_APL_SCP          VARCHAR2(4000);
    LN_CSTRADECODE              NUMBER;
    LN_PRGCODE                  NUMBER;
    LN_REGISTROS_EXITO_SCP      NUMBER := 0;
    LN_DIA_FIN_MES              NUMBER:=0;
    LD_FECHA_FIN_MES            date;


 --13200 Ini Hitss IOR 29032021    
    LV_BAND_CORP                    VARCHAR2(100);
    LV_CATEGORIA_CORP               VARCHAR2(50);
    ln_cantidad                     NUMBER;
    LV_ADICIONAL                    VARCHAR2(10);
    LN_ADICIONAL                    NUMBER;
    LV_MENSAJE                      VARCHAR2(500);
    LB_EXISTE_CORP                  BOOLEAN:= FALSE;  
    
    --Datos del cliente    
    CURSOR C_TIPO_CLIENTE (CN_CUSTOMER_ID NUMBER) IS
      SELECT R.ID_CONTRATO, R.CUENTA, R.CUSTOMER_ID, r.CATEGORIA, R.COMPLEJIDAD, R.DIAS_COMPLEJIDAD,
             r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual fecha_corte_actual,
             to_date((r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual),'dd/mm/rrrr') + r.dias_complejidad FECHA_MAXIMA_PAGO_ACTUAL 
      FROM
      (SELECT rc.id_contrato, RC.CUENTA, cu.customer_id, RC.CATEGORIA, RC.COMPLEJIDAD, FC.DIA_INI_CICLO, (SELECT extract(MONTH FROM sysdate) FROM dual) mes_actual, 
          (SELECT extract(YEAR FROM sysdate) FROM dual) anio_actual,
          decode(rc.complejidad,'BAJA',cat.complejidad_baja,'MEDIA',cat.complejidad_media,'ALTA',cat.complejidad_alta,RC.COMPLEJIDAD) DIAS_COMPLEJIDAD    
      FROM PORTA.CL_CONTRATOS@AXIS c, CUSTOMER_ALL CU, PORTA.FA_CICLOS_BSCS@AXIS fc, PORTA.FA_CONTRATOS_CICLOS_BSCS@AXIS CC, 
      PORTA.RC_CATEGORIA_CLI_CORP@AXIS rc, porta.rc_categorias@AXIS cat
      WHERE C.CODIGO_DOC = CU.CUSTCODE
      AND fc.id_ciclo = CC.ID_CICLO
      AND rc.id_contrato = cc.id_contrato
      AND cat.codigo_cat = rc.categoria
      AND cu.custcode = RC.CUENTA
      AND cc.fecha_fin IS  NULL
      AND CU.CUSTOMER_ID = CN_CUSTOMER_ID) r;  
            
    LC_C_TIPO_CLIENTE               C_TIPO_CLIENTE%ROWTYPE;
    
    --Cantidad de facturas vencidas
    CURSOR C_FACT_VENCIDAS (CN_CUSTOMER_ID NUMBER) IS
    SELECT COUNT(*)
    FROM
    (SELECT MIN(D.FECHA_MAXIMA_PAGO) FECHA_MAXIMA_PAGO, D.NUM_FACTURA, D.fecha_facturacion, D.DEUDA, D.VALOR_FACTURADO,
       D.ID_CONTRATO, D.CUENTA, D.CUSTOMER_ID, D.CATEGORIA, D.COMPLEJIDAD, D.DIAS_COMPLEJIDAD
    FROM 
    (SELECT R.ID_CONTRATO, R.CUENTA, R.CUSTOMER_ID, r.CATEGORIA, R.COMPLEJIDAD, R.DIAS_COMPLEJIDAD,
             r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual fecha_corte_actual,
             to_date((r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual),'dd/mm/rrrr') + r.dias_complejidad FECHA_MAXIMA_PAGO_ACTUAL
             , O.ohrefnum num_factura, o.ohentdate fecha_facturacion, 
           TO_DATE(R.DIA_INI_CICLO||substr(o.ohentdate,3,length(o.ohentdate)),'dd/mm/rrrr') fecha_corte_facturacion,
           TO_DATE(R.DIA_INI_CICLO||substr(o.ohentdate,3,length(o.ohentdate)),'dd/mm/rrrr') + R.DIAS_COMPLEJIDAD FECHA_MAXIMA_PAGO,
           o.ohopnamt_doc deuda, o.ohinvamt_doc VALOR_FACTURADO
      FROM
      (SELECT rc.id_contrato, RC.CUENTA, cu.customer_id, RC.CATEGORIA, RC.COMPLEJIDAD, FC.DIA_INI_CICLO, (SELECT extract(MONTH FROM sysdate) FROM dual) mes_actual, 
          (SELECT extract(YEAR FROM sysdate) FROM dual) anio_actual,
          decode(rc.complejidad,'BAJA',cat.complejidad_baja,'MEDIA',cat.complejidad_media,'ALTA',cat.complejidad_alta,RC.COMPLEJIDAD) DIAS_COMPLEJIDAD    
      FROM PORTA.CL_CONTRATOS@AXIS c, CUSTOMER_ALL CU, PORTA.FA_CICLOS_BSCS@AXIS fc, PORTA.FA_CONTRATOS_CICLOS_BSCS@AXIS CC, 
      PORTA.RC_CATEGORIA_CLI_CORP@AXIS rc, porta.rc_categorias@AXIS cat
      WHERE C.CODIGO_DOC = CU.CUSTCODE
      AND fc.id_ciclo = CC.ID_CICLO
      AND rc.id_contrato = cc.id_contrato
      AND cat.codigo_cat = rc.categoria
      AND cu.custcode = RC.CUENTA
      AND cc.fecha_fin IS  NULL
      AND CU.CUSTOMER_ID = CN_CUSTOMER_ID) r, ORDERHDR_ALL o
      WHERE r.customer_id = O.CUSTOMER_ID
      AND O.OHSTATUS IN ('IN','CM','CO')) d  
      WHERE d.deuda  > 0
      AND d.fecha_maxima_pago < TRUNC(SYSDATE)
      GROUP BY D.FECHA_MAXIMA_PAGO, D.NUM_FACTURA, D.fecha_facturacion, D.DEUDA, D.VALOR_FACTURADO,
            D.ID_CONTRATO, D.CUENTA, D.CUSTOMER_ID, D.CATEGORIA, D.COMPLEJIDAD, D.DIAS_COMPLEJIDAD) A;
     

    --Factura m�s antigua vencida
    CURSOR C_FACT_VENCIDA_ANT (CN_CUSTOMER_ID NUMBER) IS
    SELECT MIN(D.FECHA_MAXIMA_PAGO) FECHA_MAXIMA_PAGO, D.NUM_FACTURA, D.fecha_facturacion, D.DEUDA, D.VALOR_FACTURADO,
       D.ID_CONTRATO, D.CUENTA, D.CUSTOMER_ID, D.CATEGORIA, D.COMPLEJIDAD, D.DIAS_COMPLEJIDAD
    FROM 
    (SELECT R.ID_CONTRATO, R.CUENTA, R.CUSTOMER_ID, r.CATEGORIA, R.COMPLEJIDAD, R.DIAS_COMPLEJIDAD,
             r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual fecha_corte_actual,
             to_date((r.dia_ini_ciclo||'/'||r.mes_actual||'/'||r.anio_actual),'dd/mm/rrrr') + r.dias_complejidad FECHA_MAXIMA_PAGO_ACTUAL
             , O.ohrefnum num_factura, o.ohentdate fecha_facturacion, 
           TO_DATE(R.DIA_INI_CICLO||substr(o.ohentdate,3,length(o.ohentdate)),'dd/mm/rrrr') fecha_corte_facturacion,
           TO_DATE(R.DIA_INI_CICLO||substr(o.ohentdate,3,length(o.ohentdate)),'dd/mm/rrrr') + R.DIAS_COMPLEJIDAD FECHA_MAXIMA_PAGO,
           o.ohopnamt_doc deuda, o.ohinvamt_doc VALOR_FACTURADO
      FROM
      (SELECT rc.id_contrato, RC.CUENTA, cu.customer_id, RC.CATEGORIA, RC.COMPLEJIDAD, FC.DIA_INI_CICLO, (SELECT extract(MONTH FROM sysdate) FROM dual) mes_actual, 
          (SELECT extract(YEAR FROM sysdate) FROM dual) anio_actual,
          decode(rc.complejidad,'BAJA',cat.complejidad_baja,'MEDIA',cat.complejidad_media,'ALTA',cat.complejidad_alta,RC.COMPLEJIDAD) DIAS_COMPLEJIDAD    
      FROM PORTA.CL_CONTRATOS@AXIS c, CUSTOMER_ALL CU, PORTA.FA_CICLOS_BSCS@AXIS fc, PORTA.FA_CONTRATOS_CICLOS_BSCS@AXIS CC, 
      PORTA.RC_CATEGORIA_CLI_CORP@AXIS rc, porta.rc_categorias@AXIS cat
      WHERE C.CODIGO_DOC = CU.CUSTCODE
      AND fc.id_ciclo = CC.ID_CICLO
      AND rc.id_contrato = cc.id_contrato
      AND cat.codigo_cat = rc.categoria
      AND cu.custcode = RC.CUENTA
      AND cc.fecha_fin IS  NULL
      AND CU.CUSTOMER_ID = CN_CUSTOMER_ID) r, ORDERHDR_ALL o
      WHERE r.customer_id = O.CUSTOMER_ID
      AND O.OHSTATUS IN ('IN','CM','CO')) d  
      WHERE d.deuda  > 0
      AND d.fecha_maxima_pago < TRUNC(SYSDATE)
      GROUP BY D.FECHA_MAXIMA_PAGO, D.NUM_FACTURA, D.fecha_facturacion, D.DEUDA, D.VALOR_FACTURADO,
            D.ID_CONTRATO, D.CUENTA, D.CUSTOMER_ID, D.CATEGORIA, D.COMPLEJIDAD, D.DIAS_COMPLEJIDAD;    
      
       
      LC_C_FACT_VENCIDA_ANT               C_FACT_VENCIDA_ANT%ROWTYPE;                        
    
    --Cursor par�metros    
    CURSOR C_BANDERA (Cn_TipoParametro NUMBER, Cv_Parametro VARCHAR2) IS
        SELECT P.VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = Cn_TipoParametro
       AND P.ID_PARAMETRO = Cv_Parametro;              
 --13200 Fin Hitss IOR 29032021 
    
  BEGIN
    PV_ERROR      := NULL;
    LV_DES_ERROR  := NULL;

      BEGIN
        LB_FLAG_VALIDA := TRUE;
        --Setea la bandera de validaci�n de la configuraci�n de los par�metros                                           
        LN_CUSTOMER_ID := I_CUST_TO_SAVE_IN_T.CUSTOMER_ID;
        LN_CO_ID       := I_CUST_TO_SAVE_IN_T.CO_ID;
        LN_CSTRADECODE := I_CUST_TO_SAVE_IN_T.CSTRADECODE;
        LN_PRGCODE     := I_CUST_TO_SAVE_IN_T.PRGCODE;
        --***************************************************************
        SELECT PK_ID
          INTO LN_PK_ID
          FROM TABLE(CAST(I_WORKFLOW_CUST_T AS TTC_WORKFLOW_CUSTOMER_T)) WKF_CUST
         WHERE WKF_CUST.CSTRADECODE = I_CUST_TO_SAVE_IN_T
              .CSTRADECODE
           AND WKF_CUST.PRGCODE = I_CUST_TO_SAVE_IN_T.PRGCODE;
        --***************************************************************
        SELECT MIN(PLA.RE_ORDEN)
          INTO LN_RE_ORDEN
          FROM TABLE(CAST(I_PLANNIGSTATUS_IN AS TTC_PLANNIGSTATUS_IN_T)) PLA
         WHERE PLA.PK_ID = LN_PK_ID
           AND PLA.ST_STATUS_I = I_CUST_TO_SAVE_IN_T.CO_EXT_CSUIN;
        --***************************************************************
        SELECT ID_CICLO
          INTO LV_ID_CICLO
          FROM TABLE(CAST(I_FA_CICLOS_BSCS AS TTC_FA_CICLOS_AXIS_BSCS_T)) FA
         WHERE FA.ID_CICLO_ADMIN = I_CUST_TO_SAVE_IN_T.BILLCYCLE;
        --***************************************************************
        SELECT *
          INTO DOC_WFK
          FROM TABLE(CAST(I_PLANNIGSTATUS_IN AS TTC_PLANNIGSTATUS_IN_T)) PLA
         WHERE PLA.PK_ID = LN_PK_ID
           AND PLA.RE_ORDEN = LN_RE_ORDEN;
        --***************************************************************                                           
        LD_FECHA := TO_DATE(NVL(I_CUST_TO_SAVE_IN_T.PRODUCT_HISTORY_DATE,SYSDATE) + NVL(DOC_WFK.ST_DURATION_TOTAL, 0),'DD/MM/RRRR');
        --Cargar el valor estimado de la transacci�n el costo de ejecuci�n
        LN_COUNT_SERV := RC_TRX_TIMETOCASH_CUSTOMIZE.FCT_RETURN_COUNT_SERVICES(I_CUST_TO_SAVE_IN_T.CO_ID);
      EXCEPTION
        WHEN OTHERS THEN
          LB_FLAG_VALIDA              := FALSE;
          LV_DES_ERROR                := 'Prc_Header_IN-' ||
                                         'Los datos de configuraci�n como el PRGCODE y CSTRADECODE para el ' || ',' ||
                                         'customer_id:' || LN_CUSTOMER_ID ||
                                         ',Co_id:' || LN_CO_ID ||
                                         ',CstradeCode:' || LN_CSTRADECODE ||
                                         ',Prgcode:' || LN_PRGCODE ||
                                         ', No se encuentran configurados en la tabla ttc_workflow_relation_s';
          LN_REGISTROS_ERROR_SCP := LN_REGISTROS_ERROR_SCP + 1;
          LV_MENSAJE_APL_SCP     := ' Error de Validaci�n Configuraci�n CstradeCode y Prgcode';
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_DES_ERROR,
                                                NULL,
                                                2,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
      END;
      --****************************************************************************************
      IF Lb_Flag_Valida THEN
        BEGIN
          --Cargar la secuencia del tr�mite
          OPEN C_IDTRAMITE;
          FETCH C_IDTRAMITE
            INTO LN_SECUENCE;
          CLOSE C_IDTRAMITE;
          
           --Ini 13200 Hitss IOR 29032021
           
           --Bandera para habilitar matriz de plazos
           OPEN C_BANDERA (13200,'FLG_DEUDA_SUSP_CORP');
           FETCH C_BANDERA INTO LV_BAND_CORP;
           CLOSE C_BANDERA;
           
           OPEN C_BANDERA (13200,'DIAS_ADICIONALES');
           FETCH C_BANDERA INTO LV_ADICIONAL;
           CLOSE C_BANDERA;
           LN_ADICIONAL:= TO_NUMBER(LV_ADICIONAL);
 
          --Fin 13200 Hitss IOR 29032021   
                 
          --Cabecera 
          SAVEPOINT A;
          I_CONTRACTPLANNIG_T := TTC_CONTRACTPLANNIG_T(CUSTOMER_ID   => I_CUST_TO_SAVE_IN_T.CUSTOMER_ID,
                                                       CO_ID         => I_CUST_TO_SAVE_IN_T.CO_ID,
                                                       DN_NUM        => I_CUST_TO_SAVE_IN_T.DN_NUM,
                                                       CO_PERIOD     => I_CUST_TO_SAVE_IN_T.CO_EXT_CSUIN,
                                                       CO_STATUS_A   => DOC_WFK.ST_STATUS_A,
                                                       CH_STATUS_B   => DOC_WFK.ST_STATUS_B,
                                                       CO_REASON     => DOC_WFK.ST_REASON,
                                                       CO_TIMEPERIOD => DOC_WFK.ST_STATUS_NEXT,
                                                       CH_VALIDFROM  => LD_FECHA,
                                                       TMCODE        => I_CUST_TO_SAVE_IN_T.TMCODE,
                                                       PRGCODE       => I_CUST_TO_SAVE_IN_T.PRGCODE,
                                                       ADMINCYCLE    => LV_ID_CICLO,
                                                       BILLCYCLE     => I_CUST_TO_SAVE_IN_T.BILLCYCLE,
                                                       NOVERIFY      => 0,
                                                       INSERTIONDATE => SYSDATE,
                                                       LASTMODDATE   => NULL);
          LV_DES_ERROR        := FCT_SAVE_TOWK_PLANNING_HEAD;
          IF LV_DES_ERROR IS NOT NULL THEN
            RAISE LE_ERROR_TRX;
          END IF;
          LN_SUM_TIME := 0;
          FOR RC_WFK IN (SELECT *
                           FROM TABLE(CAST(I_PLANNIGSTATUS_IN AS
                                           TTC_PLANNIGSTATUS_IN_T)) PLA
                          WHERE PLA.PK_ID = LN_PK_ID
                            AND PLA.RE_ORDEN >= LN_RE_ORDEN) LOOP
            
            
            --13200 Ini Hitss IOR 29032021 
            IF NVL(LV_BAND_CORP,'N') = 'S' THEN
                           
                  --Facturas vencidas
                  OPEN C_FACT_VENCIDAS (I_CUST_TO_SAVE_IN_T.CUSTOMER_ID);
                  FETHC C_FACT_VENCIDAS INTO LN_CANTIDAD;
                  CLOSE LC_C_FACT_VENCIDAS;
                                    
                  --Datos del cliente
                 OPEN C_TIPO_CLIENTE (I_CUST_TO_SAVE_IN_T.CUSTOMER_ID);
                 FETCH C_TIPO_CLIENTE INTO LC_C_TIPO_CLIENTE;
                 LB_EXISTE_CORP:= C_TIPO_CLIENTE%FOUND;
                 CLOSE C_TIPO_CLIENTE; 
            
                 --Se valida si el cliente es corporativo (matriz de plazos)
                 BEGIN
                   PORTA.RC_RELOJ_COBRANZAS_CORP_MASIVO.RC_CORP_CATEGORIA_CLIENTE@AXIS(pn_idcontrato => LC_C_TIPO_CLIENTE.ID_CONTRATO,
                                                                                       pv_categoria  => LV_CATEGORIA_CORP,
                                                                                       pv_mensaje    => lv_mensaje,
                                                                                       pv_error      => LV_ERROR);
                 EXCEPTION
                     WHEN OTHERS THEN
                     --ROLLBACK;
                      LV_ERROR := 'rc_reloj_cobranzas_corp_masivo.rc_corp_categoria_cliente' || SQLERRM;
                 end;
            
                IF LV_CATEGORIA_CORP IS NOT NULL THEN
                  
                Ln_dia_fin_mes:=0;
                ld_fecha_fin_mes:=null;
                LN_SUM_TIME := LN_SUM_TIME + NVL(RC_WFK.ST_DURATION_TOTAL, 0); 
                
                --CH_ACCIONDATE
                IF LN_CANTIDAD = 0 THEN --Sino tiene deudas pendientes         
                   LD_FECHA:= TO_DATE(LC_C_TIPO_CLIENTE.FECHA_MAXIMA_PAGO_ACTUAL, 'DD/MM/RRRR') + LN_ADICIONAL;
                
                ELSIF LN_CANTIDAD > 0 THEN  --Si tiene deudas pendientes
                
                   OPEN C_FACT_VENCIDA_ANT (I_CUST_TO_SAVE_IN_T.CUSTOMER_ID);
                   FETCH C_FACT_VENCIDA_ANT INTO LC_C_FACT_VENCIDA_ANT;
                   CLOSE C_FACT_VENCIDA_ANT;
                   
                   LD_FECHA:= TO_DATE(LC_C_FACT_VENCIDA_ANT.FECHA_MAXIMA_PAGO, 'DD/MM/RRRR') + LN_ADICIONAL;
                
                ELSE --caso adverso
                   LD_FECHA:= TO_DATE(LC_C_TIPO_CLIENTE.FECHA_MAXIMA_PAGO_ACTUAL, 'DD/MM/RRRR') + LN_ADICIONAL;
                END IF;
                
                --11603 INI AAM
                  if RC_WFK.ST_STATUS_A = '35' then
                   ld_fecha_fin_mes:=last_day(LD_FECHA)- NVL(Pn_DiasInactivarServ,2);
                   Ln_dia_fin_mes:= ld_fecha_fin_mes - LD_FECHA;
                   
                    --CH_ACCIONDATE
                    IF LN_CANTIDAD = 0 THEN --Sino tiene deudas pendientes         
                       LD_FECHA:= TO_DATE(LC_C_TIPO_CLIENTE.FECHA_MAXIMA_PAGO_ACTUAL, 'DD/MM/RRRR') + LN_ADICIONAL;
                    
                    ELSIF LN_CANTIDAD > 0 THEN  --Si tiene deudas pendientes
                    
                       OPEN C_FACT_VENCIDA_ANT (I_CUST_TO_SAVE_IN_T.CUSTOMER_ID);
                       FETCH C_FACT_VENCIDA_ANT INTO LC_C_FACT_VENCIDA_ANT;
                       CLOSE C_FACT_VENCIDA_ANT;
                       
                       LD_FECHA:= TO_DATE(LC_C_FACT_VENCIDA_ANT.FECHA_MAXIMA_PAGO, 'DD/MM/RRRR') + LN_ADICIONAL;
                    
                    ELSE --caso adverso
                       LD_FECHA:= TO_DATE(LC_C_TIPO_CLIENTE.FECHA_MAXIMA_PAGO_ACTUAL, 'DD/MM/RRRR') + LN_ADICIONAL;
                    END IF;
                   
                   
                  End if;
                --11603 FIN AAM
                  
                ELSE --Sino aplica a matriz de plazos
                  
                Ln_dia_fin_mes:=0;
                ld_fecha_fin_mes:=null;
                LN_SUM_TIME := LN_SUM_TIME + NVL(RC_WFK.ST_DURATION_TOTAL, 0);            
                LD_FECHA    := TO_DATE(I_CUST_TO_SAVE_IN_T.PRODUCT_HISTORY_DATE + LN_SUM_TIME,'DD/MM/RRRR');
                --11603 INI AAM
                  if RC_WFK.ST_STATUS_A = '35' then
                   ld_fecha_fin_mes:=last_day(LD_FECHA)- NVL(Pn_DiasInactivarServ,2);
                   Ln_dia_fin_mes:= ld_fecha_fin_mes - LD_FECHA;
                   LD_FECHA:=ld_fecha_fin_mes;
                  End if;
                --11603 FIN AAM            
                END IF;
            

            ELSE --Bandera inactiva - Corporativo (Matriz de plazos)
            
              Ln_dia_fin_mes:=0;
                ld_fecha_fin_mes:=null;
                LN_SUM_TIME := LN_SUM_TIME + NVL(RC_WFK.ST_DURATION_TOTAL, 0);            
                LD_FECHA    := TO_DATE(I_CUST_TO_SAVE_IN_T.PRODUCT_HISTORY_DATE + LN_SUM_TIME,'DD/MM/RRRR');
                --11603 INI AAM
                  if RC_WFK.ST_STATUS_A = '35' then
                   ld_fecha_fin_mes:=last_day(LD_FECHA)- NVL(Pn_DiasInactivarServ,2);
                   Ln_dia_fin_mes:= ld_fecha_fin_mes - LD_FECHA;
                   LD_FECHA:=ld_fecha_fin_mes;
                  End if;
                --11603 FIN AAM   
            
            END IF; --Fin bandera corporativo
            --13200 Fin Hitss IOR 29032021 
                    
            
            --Detalle                                              
            I_CONTRACTPLANNIGDETAILS_T := TTC_CONTRACTPLANNIGDETAILS_T(CUSTOMER_ID       => I_CUST_TO_SAVE_IN_T.CUSTOMER_ID,
                                                                       CO_ID             => I_CUST_TO_SAVE_IN_T.CO_ID,
                                                                       PK_ID             => RC_WFK.PK_ID,
                                                                       ST_ID             => RC_WFK.ST_ID,
                                                                       ST_SEQNO          => RC_WFK.ST_SEQNO,
                                                                       RE_ORDEN          => RC_WFK.RE_ORDEN,
                                                                       CUSTCODE          => I_CUST_TO_SAVE_IN_T.CUSTCODE,
                                                                       DN_NUM            => I_CUST_TO_SAVE_IN_T.DN_NUM,
                                                                       CO_PERIOD         => RC_WFK.ST_STATUS_NEXT,
                                                                       CO_STATUS_A       => RC_WFK.ST_STATUS_A,
                                                                       CH_STATUS_B       => RC_WFK.ST_STATUS_B,
                                                                       CO_REASON         => RC_WFK.ST_REASON,
                                                                       CO_TIMEPERIOD     => RC_WFK.ST_DURATION_TOTAL + Ln_dia_fin_mes,
                                                                       CH_TIMEACCIONDATE => LD_FECHA,
                                                                       TMCODE            => I_CUST_TO_SAVE_IN_T.TMCODE,
                                                                       PRGCODE           => I_CUST_TO_SAVE_IN_T.PRGCODE,
                                                                       ADMINCYCLE        => LV_ID_CICLO,
                                                                       BILLCYCLE         => GN_HILO,--AAM
                                                                       VALUETRX          => LN_COUNT_SERV,
                                                                       ORDERBYTRX        => 0,
                                                                       CO_IDTRAMITE      => LN_SECUENCE,
                                                                       CO_REMARK         => RC_WFK.CO_REMARK,
                                                                       CO_USERTRX_1      => GV_USER_AXIS,
                                                                       CO_USERTRX_2      => GV_USER_BSCS,
                                                                       CO_USERTRX_3      => GV_USER_ESPEJO,
                                                                       CO_COMMANDTRX_1   => NULL,
                                                                       CO_COMMANDTRX_2   => NULL,
                                                                       CO_COMMANDTRX_3   => NULL,
                                                                       CO_NOINTENTO      => 1,
                                                                       CO_STATUSTRX      => NVL(RC_WFK.ST_VALID,I_RULE_ELEMENT.STATUSTRX_INI),
                                                                       CO_REMARKTRX      => NULL,
                                                                       INSERTIONDATE     => NVL(RC_WFK.CO_LASTMODDATE,SYSDATE),
                                                                       LASTMODDATE       => RC_WFK.CO_LASTMODDATE);
            LV_DES_ERROR               := FCT_SAVETO_WK_PLANNING_DETAILS;
            if I_CUST_TO_SAVE_IN_T.CO_ID = 528857 then 
              LV_DES_ERROR:='ERROR';
            end iF;
            IF LV_DES_ERROR IS NOT NULL THEN
              LN_CUSTOMER_ID := I_CUST_TO_SAVE_IN_T.CUSTOMER_ID;
              LN_CO_ID       := I_CUST_TO_SAVE_IN_T.CO_ID;
              RAISE LE_ERROR_TRX;
            END IF;
          END LOOP;
          IF NVL(GV_BANDERA, 'N') = 'S' THEN
            LN_REGISTROS_EXITO_SCP := LN_REGISTROS_EXITO_SCP + 1;
            LV_DES_BITA            := 'Prc_Header_IN-Se genero correctamente la cuenta ' || ',' ||
                                      'customer_id:' || LN_CUSTOMER_ID || ',Co_id:' || LN_CO_ID;
            LV_MENSAJE_APL_SCP     := '    Procesaminento Exitoso' || LN_REGISTROS_EXITO_SCP;
            SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_DES_BITA,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  'CUSTOMER_ID',
                                                  'CO_ID',
                                                  LN_CUSTOMER_ID,
                                                  LN_CO_ID,
                                                  'N',
                                                  LN_ERROR_SCP,
                                                  LV_ERROR_SCP);
          END IF;
          GN_CONTRX := GN_CONTRX + 1;
          IF GN_CONTRX >= I_RULE_ELEMENT.VALUE_MAX_TRXCOMMIT THEN
            COMMIT;
            LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP +GN_CONTRX;
            SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                                  LN_REGISTROS_PROCESADOS_SCP,
                                                  LN_REGISTROS_ERROR_SCP,
                                                  LN_ERROR_SCP,
                                                  LV_ERROR_SCP);
            -----------------------------------------------------------------------
            GN_CONTRX := 0;
          END IF;
        EXCEPTION
          WHEN LE_ERROR_TRX THEN
           ROLLBACK TO A;
            LV_DES_ERROR                := 'Prc_Header_IN-' || LV_DES_ERROR || ',' ||
                                           'customer_id:' || LN_CUSTOMER_ID || ',Co_id:' || LN_CO_ID;
            LN_REGISTROS_ERROR_SCP      := LN_REGISTROS_ERROR_SCP + 1;
            LV_MENSAJE_APL_SCP          := ' Error en el procesaminento';
            SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_DES_ERROR,
                                                  NULL,
                                                  2,
                                                  NULL,
                                                  'CUSTOMER_ID',
                                                  'CO_ID',
                                                  LN_CUSTOMER_ID,
                                                  LN_CO_ID,
                                                  'N',
                                                  LN_ERROR_SCP,
                                                  LV_ERROR_SCP);
          WHEN OTHERS THEN
            LV_DES_ERROR                := 'Prc_Header_IN-Error al intentar grabar registros en la tabla ttc_ContractPlannig y ttc_ContractPlannigDetails-->' ||
                                           'customer_id:' || LN_CUSTOMER_ID || ',CO_ID:' || LN_CO_ID || ',' ||
                                           SUBSTR(SQLERRM, 1, 500);
            LN_REGISTROS_ERROR_SCP      := LN_REGISTROS_ERROR_SCP + 1;
            LV_MENSAJE_APL_SCP          := ' Error en el procesaminento';
            SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_DES_ERROR,
                                                  NULL,
                                                  2,
                                                  NULL,
                                                  'CUSTOMER_ID',
                                                  'CO_ID',
                                                  LN_CUSTOMER_ID,
                                                  LN_CO_ID,
                                                  'N',
                                                  LN_ERROR_SCP,
                                                  LV_ERROR_SCP); 
        END;
      END IF;
    IF GN_CONTRX > 0 THEN
      LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP + GN_CONTRX;
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            LN_REGISTROS_PROCESADOS_SCP,
                                            LN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);

    END IF;
    PV_ERROR     := LV_DES_ERROR;
  EXCEPTION
    WHEN LE_ERROR_PROCESS THEN
      PV_ERROR                    := 'Proceso: RC_Trx_TimeToCash_IN- Ejecutado con ERROR-->' ||
                                     'Prc_Header_IN-' || LV_DES_ERROR;
      LV_MENSAJE_APL_SCP          := ' Proceso: RC_Trx_TimeToCash_IN-Error en Validaci�n';
      IF LN_REGISTROS_ERROR_SCP = 0 THEN
        LN_REGISTROS_ERROR_SCP := -1;
      END IF;
      LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP +GN_CONTRX;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            LN_REGISTROS_PROCESADOS_SCP,
                                            LN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    WHEN Le_Error_Valid THEN
      PV_ERROR                    := 'Proceso: RC_Trx_TimeToCash_IN- Ejecutado con ERROR-->' ||
                                     'Prc_Header_IN-' || Lv_Des_Error;
      LV_MENSAJE_APL_SCP          := ' Proceso: RC_Trx_TimeToCash_IN-Error en Validaci�n';
      IF LN_REGISTROS_ERROR_SCP = 0 THEN
        LN_REGISTROS_ERROR_SCP := -1;
      END IF;
      LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP + GN_CONTRX;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            LN_REGISTROS_PROCESADOS_SCP,
                                            LN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    WHEN NO_DATA_FOUND THEN
      PV_ERROR                    := 'Proceso: RC_Trx_TimeToCash_IN- Ejecutado con ERROR-->' ||
                                     'Prc_Header_IN-' ||
                                     'No existen datos para la ejecuci�n del d�a de hoy, para el criterio de inserci�n al Reloj de cobranzas, 1 de Mensajeria';
      LV_MENSAJE_APL_SCP          := ' Proceso: RC_Trx_TimeToCash_IN-Error No existen datos-NO_DATA_FOUND';
      IF LN_REGISTROS_ERROR_SCP = 0 THEN
        LN_REGISTROS_ERROR_SCP := -1;
      END IF;
      LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP +  GN_CONTRX;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            LN_REGISTROS_PROCESADOS_SCP,
                                            LN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);  
    WHEN OTHERS THEN
      --ROLLBACK;
      PV_ERROR                    := 'Prc_Header_IN-Error General-' || substr(SQLERRM, 1, 500);
      LV_MENSAJE_APL_SCP          := ' Prc_Header_IN-Error General';
      If ln_registros_error_scp = 0 Then
        ln_registros_error_scp := -1;
      End If;
      LN_REGISTROS_PROCESADOS_SCP := LN_REGISTROS_PROCESADOS_SCP + GN_CONTRX;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            PV_ERROR,
                                            NULL,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                            LN_REGISTROS_PROCESADOS_SCP,
                                            LN_REGISTROS_ERROR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  END PRC_HEADER_IN;

/*--=======================RELOJ DE COBRANZAS================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion de la funcion que carga en un type los estados que debe de pasara un servicio.
  --======================================================================--*/
  FUNCTION FCT_PLANNIGSTATUS_IN_T RETURN TTC_PLANNIGSTATUS_IN_T AS
    DOC_IN TTC_PLANNIGSTATUS_IN_T;
  BEGIN
    SELECT TTC_PLANNIGSTATUS_IN_TYPE(PK_ID,
                                     ST_ID,
                                     ST_SEQNO,
                                     RE_ORDEN,
                                     ST_SDES,
                                     ST_STATUS_I,
                                     ST_STATUS_NEXT,
                                     ST_DURATION,
                                     ST_EXT_DURATION,
                                     ST_DURATION_TOTAL,
                                     ST_STATUS_A,
                                     ST_STATUS_B,
                                     ST_REASON,
                                     ST_ALERT,
                                     ST_NODAYALERT,
                                     ST_VALID,
                                     CO_REMARK,
                                     CO_LASTMODDATE,
                                     ST_CKECKSTATUS,
                                     ST_VALID_COND_1,
                                     ST_VALID_REAC,
                                     ST_VALID_SUSPE,
                                     ST_VALID_EVAL_1,
                                     ST_VALID_EVAL_2) BULK COLLECT
      INTO DOC_IN
      FROM TTC_VIEW_PLANNIGSTATUS_IN;
    RETURN DOC_IN;
  END;

/*--=======================RELOJ DE COBRANZAS================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion de la funcion que guarda en un type la combinacion de PK_ID,CSTRADECODE,PRGCODE.
  --======================================================================--*/
  FUNCTION FCT_WORFLOW_CUSTOMERS_T RETURN TTC_WORKFLOW_CUSTOMER_T AS
    DOC_IN TTC_WORKFLOW_CUSTOMER_T;
  BEGIN
    SELECT TTC_WORKFLOW_CUSTOMER_TYPE(PK_ID, CSTRADECODE, PRGCODE) BULK COLLECT
      INTO DOC_IN
      FROM TTC_WORKFLOW_CUSTOMER;
    RETURN DOC_IN;
  END;

/*--=======================RELOJ DE COBRANZAS================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion de la funcion que guarda en un type los ciclos.
  --======================================================================--*/
  FUNCTION FCT_FA_CICLOS_BSCS_T RETURN TTC_FA_CICLOS_AXIS_BSCS_T AS
    DOC_IN TTC_FA_CICLOS_AXIS_BSCS_T;
  BEGIN
    SELECT TTC_FA_CICLOS_AXIS_BSCS_TYPE(ID_CICLO,
                                        ID_CICLO_ADMIN,
                                        CICLO_DEFAULT) BULK COLLECT
      INTO DOC_IN
      FROM FA_CICLOS_AXIS_BSCS;
    RETURN DOC_IN;
  END;

/*--=======================RELOJ DE COBRANZAS================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion de la funcion que inserta en la cabecera de la planificacion
  --======================================================================--*/
  FUNCTION FCT_SAVE_TOWK_PLANNING_HEAD RETURN VARCHAR2 IS
    LV_ERROR TTC_LOG_INOUT.COMMENT_RUN%TYPE;
  BEGIN
    LV_ERROR := NULL;
    --Cabecera
    INSERT INTO TTC_CONTRACTPLANNIG_S VALUES (I_CONTRACTPLANNIG_T);
    RETURN(LV_ERROR);
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      LV_ERROR := 'Fct_Save_ToWk_Planning_Head-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_ContractPlannig, ' ||
                  SUBSTR(SQLERRM, 1, 300);
      RETURN(LV_ERROR);
    WHEN OTHERS THEN
      LV_ERROR := 'Fct_Save_ToWk_Planning_Head-Error General al intentar grabar en la tabla ttc_ContractPlannig, ' ||
                  SUBSTR(SQLERRM, 1, 300);
      RETURN(LV_ERROR);
  END;

/*--=======================RELOJ DE COBRANZAS================================--
  Versi�n       : 1.0
  Descripci�n   : Paquete para manejo de cobranza automatica.
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  Creado por    : Iro Jordan Rodriguez
  Fecha         : 11/09/2016
  Proyecto      : 10695 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Creacion de la funcion que inserta en el detalle de la planificacion
  --======================================================================--*/
  FUNCTION FCT_SAVETO_WK_PLANNING_DETAILS RETURN VARCHAR2 IS
    LV_ERROR TTC_LOG_INOUT.COMMENT_RUN%TYPE;
  BEGIN
    LV_ERROR := NULL;
    --Detalle
    INSERT INTO TTC_CONTRACTPLANNIGDETAILS_S
    VALUES
      (I_CONTRACTPLANNIGDETAILS_T);
    RETURN(LV_ERROR);
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      LV_ERROR := 'Fct_SaveTo_Wk_Planning_Details-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_ContractPlannigDetails, ' ||
                  SUBSTR(SQLERRM, 1, 300);
      RETURN(LV_ERROR);
    WHEN OTHERS THEN
      LV_ERROR := 'Fct_SaveTo_Wk_Planning_Details-Error General al intentar grabar en la tabla ttc_ContractPlannigDetails, ' ||
                  sUBSTR(SQLERRM, 1, 300);
      RETURN(LV_ERROR);
  END;

end Rc_Trx_TimeToCash_Planning;
/
