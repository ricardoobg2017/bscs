create or replace package reloj.RC_Trx_TimeToCash_Out_P is

   /*
   [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   23/11/2009
  * Modificado: 04/01/2010  -- Richard Tigrero
  */
-----------------------------------------------------------------------
GV_DESSHORT VARCHAR2(9):='TTC_OUT';

--------------------------------------------------------------------------
PROCEDURE RC_PROCESS_PRINCIPAL(PV_TIPO  VARCHAR2,
                                                      PV_RULER VARCHAR2,
                                                      PN_SOSPID  IN NUMBER,
                                                      PV_OUT_ERROR      OUT VARCHAR2);

                               
PROCEDURE  RC_DELETE_RULER_A(PN_ID_PROCESS              IN NUMBER,
                                                                     PN_MAX_EXECUTE          IN NUMBER,
                                                                     PN_MAX_COMMIT          IN NUMBER,
                                                                     PN_ID_BITACORA_SCP   IN NUMBER,
                                                                     PN_SOSPID  IN NUMBER,
                                                                     PV_OUT_ERROR              OUT VARCHAR2
                                                                     );                             
PROCEDURE  RC_DELETE_RULER_D(PN_ID_PROCESS                IN NUMBER,
                                                                      PN_MAX_EXECUTE           IN NUMBER,
                                                                      PN_MAX_COMMIT          IN NUMBER,
                                                                      PN_ID_BITACORA_SCP    IN NUMBER,
                                                                      PN_SOSPID  IN NUMBER,
                                                                      PV_OUT_ERROR               OUT VARCHAR2
                                                                      );                                 
PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_ID_PROCESS    IN NUMBER,
                                                                                      PD_LASTMONDATE   IN DATE,
                                                                                      PV_OUT_ERROR     OUT VARCHAR2
                                                                                      );                                 

end RC_Trx_TimeToCash_Out_P;
/
create or replace package body reloj.RC_Trx_TimeToCash_Out_P is

  /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   23/11/2009
  * Modificado: 
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
  *       1.Ingreso Registro a la Programación del Reloj,
  ********2.Eliminar Registro de la Programación del Reloj*************   
  *       3.Envio de Transacciones a la cola de despacho
  *       4.Envio de Transacciones a procesar Axis desde la cola de despacho
  *********************************************************************************************************/
 
  /********************************************************************************************************/ 
   
   PROCEDURE RC_PROCESS_PRINCIPAL(PV_TIPO             VARCHAR2, 
                                                          PV_RULER VARCHAR2, 
                                                          PN_SOSPID         IN NUMBER,
                                                          PV_OUT_ERROR  OUT VARCHAR2) IS
    
     LV_MENSAJE              VARCHAR2(500);
     LV_STATUS               VARCHAR2(3);
     LE_ERROR                EXCEPTION;
     LE_ERROR_ABORTADO       EXCEPTION;
     LD_FECHA_ACTUAL         DATE;
     LD_FECHA_CONFIG         DATE;
     LN_ID_PROCESS           NUMBER;
     LV_TIPO_PROCESS         VARCHAR2(30);
     
     --SCP:VARIABLES
        ---------------------------------------------------------------
        --SCP: Código generado automaticamente. Definición de variables
        ---------------------------------------------------------------
        ln_id_bitacora_scp          number:=0; 
        ln_total_registros_scp      number:=0;
        lv_id_proceso_scp           varchar2(100);
        lv_referencia_scp           varchar2(100):='RC_Trx_TimeToCash_Out.RC_DELETE_RULER_';
        lv_unidad_registro_scp      varchar2(30):='Cuentas';
        ln_error_scp                number:=0;
        lv_error_scp                varchar2(500);
        lv_mensaje_apl_scp          varchar2(4000);
        ---------------------------------------------------------------
       LN_MAX_EXECUTE                     NUMBER;
       LN_MAXRETENTRX                   NUMBER;
       LN_MAX_COMMIT                      NUMBER;
       LN_value_max_nointento          NUMBER;
       LN_value_max_trx_encolada     NUMBER;

   BEGIN
     LV_STATUS:='OK';
     LD_FECHA_ACTUAL:= SYSDATE;
     GV_DESSHORT:=GV_DESSHORT||PV_RULER;
     lv_id_proceso_scp:=GV_DESSHORT;
     LN_ID_PROCESS:=RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_ID_PROCESS(GV_DESSHORT);
     
     IF PV_TIPO IS NULL THEN 
            LD_FECHA_CONFIG:= RC_Api_TimeToCash_Rule_ALL.Fct_Planning_Hour_DateValid(LN_ID_PROCESS);
            IF LD_FECHA_CONFIG IS NULL THEN
                  LV_MENSAJE:='No existen datos configurados en la tabla TTC_Planning_Hour, para ejecutar el día de Hoy';
                  RAISE Le_Error;
            END IF;
      END IF;                  

        RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_RULE_ELEMENT(GV_DESSHORT, 
                                                                                                                                   LN_value_max_nointento,
                                                                                                                                   LN_value_max_trx_encolada,
                                                                                                                                   LN_MAX_EXECUTE,                                                                                                                               
                                                                                                                                   LN_MAX_COMMIT,
                                                                                                                                   LN_MAXRETENTRX,
                                                                                                                                   PV_OUT_ERROR
                                                                                                                                   );

         --SCP:INICIO
        ----------------------------------------------------------------------------
        -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
        ----------------------------------------------------------------------------
        scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp||PV_RULER,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
        if ln_error_scp <>0 then
           Raise LE_ERROR;
        Else
           lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, Null, Null,
                                             0,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);    
        end if;
        
        ----------------------------------------------------------------------    
         IF PV_RULER = 'D' THEN
              --Regla 1 para eliminar Usando View D
              RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D(LN_ID_PROCESS,
                                                                                                        LN_MAX_EXECUTE,
                                                                                                        LN_MAX_COMMIT,
                                                                                                        ln_id_bitacora_scp,
                                                                                                        PN_SOSPID,
                                                                                                        LV_MENSAJE
                                                                                                        );
              IF LV_MENSAJE IS NOT NULL THEN
                 IF LV_MENSAJE ='ABORTADO' THEN
                    RAISE LE_ERROR_ABORTADO;
                 END IF;
                 RAISE LE_ERROR;
              END IF;
              LV_TIPO_PROCESS:='_VIEW_D';
          ELSE    
              --Regla 2 para eliminar Usando View A
              RC_Trx_TimeToCash_Out_P.RC_DELETE_RULER_A( LN_ID_PROCESS,
                                                                                                         LN_MAX_EXECUTE,
                                                                                                         LN_MAX_COMMIT,
                                                                                                         ln_id_bitacora_scp,
                                                                                                         PN_SOSPID,
                                                                                                         LV_MENSAJE
                                                                                                         );
              IF LV_MENSAJE IS NOT NULL THEN
                 IF LV_MENSAJE ='ABORTADO' THEN
                    RAISE LE_ERROR_ABORTADO;
                 END IF;
                 RAISE LE_ERROR;
              END IF;
              LV_TIPO_PROCESS:='_VIEW_A';
         END IF;
        --Actualizar LastMonDate Fecha Real en q EJECUTO
        RC_Trx_TimeToCash_Out.RC_REGISTRAR_PROGRAMMER(LN_ID_PROCESS,
                                                      LD_FECHA_ACTUAL,
                                                      LV_MENSAJE);
        IF  LV_MENSAJE IS NOT NULL THEN 
           RAISE LE_ERROR;
        END IF;
  
        LV_STATUS:='OKI';
        LV_MENSAJE:='RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL'||LV_TIPO_PROCESS||'-SUCESSFUL';
        

      --SCP:FIN  
       ----------------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de finalización de proceso
       ----------------------------------------------------------------------------
       lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             0,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);                              
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       
       ----------------------------------------------------------------------------

      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                    LD_FECHA_ACTUAL,
                                                    SYSDATE,
                                                    LV_STATUS,
                                                    LV_MENSAJE);
       PV_OUT_ERROR:=LV_MENSAJE;
       
       EXCEPTION  
        
        WHEN LE_ERROR_ABORTADO THEN
             LV_STATUS:='ERR';
             LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL'||LV_TIPO_PROCESS||'- PROCESO '||LV_MENSAJE||' MANUALMENTE';
             PV_OUT_ERROR:=LV_MENSAJE;
             ---------------------------------------------------------------------------
             lv_mensaje_apl_scp:='    Proceso Abortado Manualmente por el Usuario';
             scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                   lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                                   2,Null, Null, Null,null,
                                                   null,'S',ln_error_scp,lv_error_scp);
             scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
             -------------------------------------------------------------------------- 
             RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                              LD_FECHA_ACTUAL,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);
        
         WHEN LE_ERROR THEN
               LV_STATUS:='ERR';
               LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL'||LV_TIPO_PROCESS||'- '  || LV_MENSAJE;
               PV_OUT_ERROR:=LV_MENSAJE;
             ---------------------------------------------------------------------------
           
             lv_mensaje_apl_scp:='    Error en el Pocesamineto';
             scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                   lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                                   3,Null, Null, Null,null,
                                                   null,'S',ln_error_scp,lv_error_scp);
             scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
             --------------------------------------------------------------------------                
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                              LD_FECHA_ACTUAL,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);
               RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,GV_DESSHORT,'OUT'||PV_RULER,0,LV_MENSAJE);
       
         WHEN OTHERS THEN
           LV_STATUS:='ERR';
           LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL'||LV_TIPO_PROCESS||'- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
           PV_OUT_ERROR:=LV_MENSAJE;
           ---------------------------------------------------------------------------
             lv_mensaje_apl_scp:='    Error en el Procesamiento';
             scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                   lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                                   3,Null, Null, Null,null,
                                                   null,'S',ln_error_scp,lv_error_scp);
             scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,
                                                    -1,ln_error_scp,lv_error_scp);
             scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
           -------------------------------------------------------------------------- 
           RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                              LD_FECHA_ACTUAL,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);
           RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,GV_DESSHORT,'OUT'||PV_RULER,0,LV_MENSAJE);
        
          
       
   END RC_PROCESS_PRINCIPAL;
   
   /***************************************************************************
   *
   ****************************************************************************/
   PROCEDURE  RC_DELETE_RULER_A(PN_ID_PROCESS              IN NUMBER,
                                                                        PN_MAX_EXECUTE          IN NUMBER,
                                                                        PN_MAX_COMMIT          IN NUMBER, 
                                                                        PN_ID_BITACORA_SCP   IN NUMBER,
                                                                        PN_SOSPID                  IN NUMBER,
                                                                        PV_OUT_ERROR         OUT VARCHAR2
                                                                        )IS 
   
      CURSOR C_PLANNIG_DETAILS(LN_CO_ID NUMBER) IS
         SELECT COUNT(*)
         FROM TTC_CONTRACTPLANNIGDETAILSA A
         WHERE  A.CO_ID = LN_CO_ID;
           
       CURSOR C_WIEV_A IS
         SELECT TTC_VIEW_CUSTOMER_OUT_TYPE(CUSTOMER_ID,CO_ID) 
         FROM TTC_VIEW_CUSTOMERS_ANA T; 
             
        
        t_obj_a   TTC_VIEW_CUSTOMER_OUT_T; 
        
        LV_MENSAJE             VARCHAR2(500);
        LV_STATUS              VARCHAR(3);     
        LN_NUM_REGISTRO        NUMBER;
        LN_MAXTRX              NUMBER;
        LN_COUNT               NUMBER;
        LN_COUNT_COMMIT        NUMBER;
        LN_MAX_COMMIT          NUMBER;  
        LB_EXISTE_BCK          BOOLEAN;
        LD_FECHA_ACTUAL        DATE;
        LE_ERROR_MANUAL        EXCEPTION;
        LE_ERROR_SCP           EXCEPTION;
        ---------------------------------------------------------------
        --SCP: Código generado automaticamente. Definición de variables
        ---------------------------------------------------------------
        ln_registros_procesados_scp Number;
        ln_registros_error_scp      Number;
        ln_error_scp                number:=0;
        lv_error_scp                varchar2(500);
        lv_mensaje_apl_scp          varchar2(4000);
        --------------------------------------------------------------        
   BEGIN
   
        LD_FECHA_ACTUAL:=SYSDATE; 
        RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(PN_ID_PROCESS,LN_MAXTRX);
        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,'OUTA',0,PN_SOSPID,LB_EXISTE_BCK);
        LN_COUNT:=1;
        LN_COUNT_COMMIT:=0;
        ln_registros_error_scp:=0;
        
        
   OPEN C_WIEV_A;
         LOOP
          FETCH C_WIEV_A BULK COLLECT  INTO t_obj_a  LIMIT PN_MAX_EXECUTE;
          
               FOR rc_in IN (select * from table(cast( t_obj_a as TTC_VIEW_CUSTOMER_OUT_T))) loop
                   
                    OPEN C_PLANNIG_DETAILS(rc_in.CO_ID);
                    FETCH C_PLANNIG_DETAILS INTO LN_NUM_REGISTRO;
                    CLOSE C_PLANNIG_DETAILS;
                    
                    IF LN_NUM_REGISTRO > 0 THEN
                      BEGIN
                           
                            DELETE FROM TTC_CONTRACTPLANNIGDETAILSA A
                            WHERE A.CO_ID  = rc_in.CO_ID
                            AND A.CUSTOMER_ID = rc_in.CUSTOMER_ID;
                           
                           LN_COUNT_COMMIT:=LN_COUNT_COMMIT+SQL%ROWCOUNT;
                           IF LN_NUM_REGISTRO = SQL%ROWCOUNT THEN
                               
                               DELETE FROM TTC_CONTRACTPLANNIG_A A
                               WHERE A.CO_ID  =rc_in.CO_ID
                               AND A.CUSTOMER_ID =rc_in.CUSTOMER_ID;
                           
                                                          
                           END IF;
                           IF LN_COUNT >= LN_MAXTRX THEN
                                RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,'OUTA',0,PN_SOSPID,LB_EXISTE_BCK);
                                LN_COUNT:=1;
                           END IF;
                           
                           IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
                              --SCP:AVANCE
                              -----------------------------------------------------------------------
                              -- SCP: Código generado automáticamente. Registro de avance de proceso
                              -----------------------------------------------------------------------
                              ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_MAX_COMMIT;
                              scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                                                    ln_registros_procesados_scp,
                                                                    null,ln_error_scp,lv_error_scp);
                              -----------------------------------------------------------------------
                              COMMIT;
                              LN_COUNT_COMMIT:=0;
                           END IF;
                           
                         EXCEPTION
                         WHEN OTHERS THEN
                               LV_STATUS:='ERR';
                               LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
                               ROLLBACK;
                               PV_OUT_ERROR:=LV_MENSAJE;
                               ln_registros_error_scp:=ln_registros_error_scp+1;
                               ---------------------------------------------------------------------------
                               lv_mensaje_apl_scp:='    Error al momento del procesamiento';
                               scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             2,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);
                               
                               -------------------------------------------------------------------------- 
                               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                                                  LD_FECHA_ACTUAL,
                                                                                  SYSDATE,
                                                                                  LV_STATUS,
                                                                                  LV_MENSAJE);
                      END;    
                    END IF;
                    IF LB_EXISTE_BCK=FALSE THEN
                        RAISE LE_ERROR_MANUAL;
                    END IF;
                    LN_COUNT:=LN_COUNT+1;
               END LOOP;
          EXIT WHEN C_WIEV_A%NOTFOUND;   
          END LOOP;
   CLOSE C_WIEV_A;
   -------------------------------------------------------------------
   ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
   scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,
                                         ln_registros_procesados_scp,
                                         ln_registros_error_scp,ln_error_scp,lv_error_scp);     
   ---------------------------------------------------------------------------
   COMMIT;
   PV_OUT_ERROR:=NULL;
        
     EXCEPTION  
        WHEN LE_ERROR_SCP THEN
             PV_OUT_ERROR:='ABORTADO';
             ROLLBACK;
              If ln_registros_error_scp = 0 Then
                 ln_registros_error_scp:=-1;
              End If;
             scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,Null,
                                                    ln_registros_error_scp,ln_error_scp,lv_error_scp);
        
         WHEN OTHERS THEN
           LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_A- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
           ROLLBACK;
           PV_OUT_ERROR:=LV_MENSAJE;
            If ln_registros_error_scp = 0 Then
                 ln_registros_error_scp:=-1;
              End If;
             scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,Null,
                                                    ln_registros_error_scp,ln_error_scp,lv_error_scp);
           
           
   END RC_DELETE_RULER_A;
   /************************************************************************************
   * PROCESO QUE ELIMINA DE   Ttc_Contractplannig  -- TTC_ContractPlannigDetails
   * DE ACUERDO A LA REGLA QUE SE QUIERA APLICAR
   * Y A SU VEZ SE ACTIVA LOS TRIGGERS  TTC_Trg_ContractPlannig---TTC_Trg_ContractPlannigHist
   *************************************************************************************/

  PROCEDURE  RC_DELETE_RULER_D(PN_ID_PROCESS        IN NUMBER,
                                                                        PN_MAX_EXECUTE       IN NUMBER,
                                                                        PN_MAX_COMMIT          IN NUMBER, 
                                                                        PN_ID_BITACORA_SCP   IN NUMBER,
                                                                        PN_SOSPID                  IN NUMBER,
                                                                        PV_OUT_ERROR         OUT VARCHAR2
                                                                        )IS 
   
          CURSOR C_PLANNIG_DETAILS(LN_CO_ID NUMBER) IS
             SELECT COUNT(*)
             FROM TTC_CONTRACTPLANNIGDETAILS_S A
             WHERE  A.CO_ID = LN_CO_ID;
         
         
         CURSOR C_WIEV_D IS
             SELECT TTC_VIEW_CUSTOMER_OUT_TYPE(CUSTOMER_ID,CO_ID) 
             FROM TTC_VIEW_CUSTOMERS_D T;

        t_obj_d                TTC_VIEW_CUSTOMER_OUT_T; 
        LV_MENSAJE             VARCHAR2(500);
        LV_STATUS              VARCHAR(3);     
        LN_NUM_REGISTRO        NUMBER;
        LN_MAXTRX              NUMBER;
        LN_COUNT               NUMBER;
        LN_COUNT_COMMIT        NUMBER;
        LN_MAX_COMMIT          NUMBER;     
        LB_EXISTE_BCK          BOOLEAN;
        LD_FECHA_ACTUAL        DATE;
        LE_ERROR_MANUAL        EXCEPTION;
        --SCP:VARIABLES
        ---------------------------------------------------------------
        --SCP: Código generado automaticamente. Definición de variables
        ---------------------------------------------------------------
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp      Number:=0;
        lv_mensaje_apl_scp          varchar2(4000);
        ---------------------------------------------------------------
                
   BEGIN
   
        LD_FECHA_ACTUAL:=SYSDATE; 
        RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(PN_ID_PROCESS,LN_MAXTRX);
        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,'OUTD',0,PN_SOSPID,LB_EXISTE_BCK);
        LN_COUNT:=1;
        LN_COUNT_COMMIT:=0;
        
        OPEN C_WIEV_D;
        LOOP  
             FETCH C_WIEV_D BULK COLLECT  INTO t_obj_d LIMIT PN_MAX_EXECUTE;

              FOR rc_in IN (select * from table(cast( t_obj_d as TTC_VIEW_CUSTOMER_OUT_T))) loop
                     
                       OPEN C_PLANNIG_DETAILS(rc_in.CO_ID);
                       FETCH C_PLANNIG_DETAILS INTO LN_NUM_REGISTRO;
                       CLOSE C_PLANNIG_DETAILS;
                       
                      IF LN_NUM_REGISTRO > 0 THEN
                           BEGIN  
                                
                                DELETE FROM TTC_CONTRACTPLANNIGDETAILS_S A
                                WHERE A.CO_ID  = rc_in.CO_ID
                                AND A.CUSTOMER_ID = rc_in.CUSTOMER_ID; 
                                                      
                              LN_COUNT_COMMIT:=LN_COUNT_COMMIT+SQL%ROWCOUNT;
                              IF LN_NUM_REGISTRO = SQL%ROWCOUNT THEN
                                  
                                  DELETE FROM TTC_CONTRACTPLANNIG_S A
                                  WHERE A.CO_ID  = rc_in.CO_ID
                                  AND A.CUSTOMER_ID = rc_in.CUSTOMER_ID;

                              END IF;
                              IF LN_COUNT >= LN_MAXTRX THEN
                                 RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,'OUTD',0,PN_SOSPID,LB_EXISTE_BCK);
                                 LN_COUNT:=1;
                              END IF;
                             
                              IF LN_COUNT_COMMIT >= PN_MAX_COMMIT THEN
                                 --SCP:AVANCE
                                 -----------------------------------------------------------------------
                                 -- SCP: Código generado automáticamente. Registro de avance de proceso
                                 -----------------------------------------------------------------------
                                 ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_MAX_COMMIT;
                                 scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                                       ln_registros_procesados_scp,
                                                                       null,ln_error_scp,lv_error_scp);
                                 -----------------------------------------------------------------------
                                 COMMIT;
                                 LN_COUNT_COMMIT:=0;
                              END IF;
                              
                              EXCEPTION 
                              WHEN OTHERS THEN
                                 LV_STATUS:='ERR';
                                 LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D- NO SE PUDO ELIMINAR REGISTROS POR '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
                                 ROLLBACK;
                                 PV_OUT_ERROR:=LV_MENSAJE;
                                 ln_registros_error_scp:=ln_registros_error_scp+1;
                                 ---------------------------------------------------------------------------
                                 lv_mensaje_apl_scp:='    Error al momento del procesamiento';
                                 scp.sck_api.scp_detalles_bitacora_ins(Pn_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             2,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);
                                 
                               -------------------------------------------------------------------------- 
                                 RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                                                    LD_FECHA_ACTUAL,
                                                                                    SYSDATE,
                                                                                    LV_STATUS,
                                                                                    LV_MENSAJE);
                           END;
                      END IF;
                      IF LB_EXISTE_BCK=FALSE THEN
                        RAISE LE_ERROR_MANUAL;
                     END IF;
                     LN_COUNT:=LN_COUNT+1;
             END LOOP;
        EXIT WHEN C_WIEV_D%NOTFOUND;
        END LOOP;
        CLOSE C_WIEV_D;  
        --SCP:FIN
        ----------------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de finalización de proceso
        ----------------------------------------------------------------------------
        ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
        scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
        PV_OUT_ERROR:=NULL;
        
     EXCEPTION  
         WHEN LE_ERROR_MANUAL THEN
             PV_OUT_ERROR:='ABORTADO';
             ROLLBACK;
              If ln_registros_error_scp = 0 Then
                 ln_registros_error_scp:=-1;
              End If;
             scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,Null,
                                                    ln_registros_error_scp,ln_error_scp,lv_error_scp);
     
         WHEN OTHERS THEN
           LV_MENSAJE := 'RC_Trx_TimeToCash_Out.RC_DELETE_RULER_D- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
           ROLLBACK;
           PV_OUT_ERROR:=LV_MENSAJE;
            If ln_registros_error_scp = 0 Then
                 ln_registros_error_scp:=-1;
              End If;
             scp.sck_api.scp_bitacora_procesos_act(Pn_id_bitacora_scp,Null,
                                                    ln_registros_error_scp,ln_error_scp,lv_error_scp);
           
          
   END RC_DELETE_RULER_D;
   
/*******************************************************************************************
*
********************************************************************************************/

PROCEDURE RC_REGISTRAR_PROGRAMMER(PN_ID_PROCESS    IN NUMBER,
                                  PD_LASTMONDATE   IN DATE,
                                  PV_OUT_ERROR     OUT VARCHAR2)   IS
  LV_STATUS      VARCHAR2(3);
  LV_MENSAJE     VARCHAR2(200);
  LN_SEQNO       NUMBER;                          
BEGIN

    SELECT TTC_TRX_OUT.NEXTVAL 
    INTO LN_SEQNO
    FROM DUAL;
    
    INSERT INTO ttc_process_programmer_s VALUES(PN_ID_PROCESS,
                                                LN_SEQNO,
                                                PD_LASTMONDATE,
                                                sysdate);
    COMMIT;
    PV_OUT_ERROR:=NULL;
    EXCEPTION
         WHEN OTHERS THEN
           LV_STATUS:='ER';
           LV_MENSAJE := 'RC_REGISTRAR_PROGRAMMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);
           PV_OUT_ERROR:=LV_MENSAJE;
           ROLLBACK;
           RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(PN_ID_PROCESS,
                                                              SYSDATE,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);

END RC_REGISTRAR_PROGRAMMER;

end RC_Trx_TimeToCash_Out_P;
/
