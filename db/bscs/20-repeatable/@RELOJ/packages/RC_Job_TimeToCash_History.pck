create or replace package reloj.RC_Job_TimeToCash_History Is

     CURSOR C_RULES_VALUES(PV_PROCESS  NUMBER) IS
        SELECT VALUE_RETURN_LONGSTRING 
           FROM TTC_Rule_Values_s 
        WHERE PROCESS=PV_PROCESS 
         ORDER BY ORDERRUNSEQ;
  
   PROCEDURE RC_ELIMINA_TTC_HIST_LOG(PV_DESSHORT       IN  VARCHAR2,
                                                              PN_SOSPID            IN NUMBER,
                                                              PV_OUT_ERROR      OUT VARCHAR2
                                                                                    );   
   PROCEDURE RC_OBTENER_MAXRETEN(PN_PROCESS      IN NUMBER,
                                                                                PN_MAX_RETEN    OUT NUMBER
                                                                                );                                 
   FUNCTION RC_CANTIDAD_REGISTROS(PN_PROCESS  IN NUMBER,
                                                                             PN_FECHA_DEL In Varchar2) Return Number;
                                                                             
   PROCEDURE RC_ANALIZE_TTC_HIST_TABLE(PV_DESSHORT       IN  VARCHAR2,
                                      PN_SOSPID            IN NUMBER,   
                                      PV_OUT_ERROR      OUT VARCHAR2
                                      );
End RC_Job_TimeToCash_History;
/
create or replace package body reloj.RC_Job_TimeToCash_History is

 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   27/11/2009
  * Modificado: 23/12/2009
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
    
  ********************************************************************************************************/
    LD_FECHA_ACTUAL     DATE;
 /************************************************************************************/ 
   
  PROCEDURE RC_ELIMINA_TTC_HIST_LOG(PV_DESSHORT       IN  VARCHAR2,
                                                            PN_SOSPID            IN NUMBER,   
                                                            PV_OUT_ERROR      OUT VARCHAR2
                                                            )  IS
     LN_ID_PROCESS        NUMBER;
     LN_COUNT                 NUMBER;
     LN_COUNT_BCK        NUMBER;
     LB_EXISTE_BCK         BOOLEAN;
     LV_MENSAJE              VARCHAR2(500);
     LV_STATUS                 VARCHAR2(3);
     LV_SQL                        VARCHAR2(2000);
     LV_FECHA_ELIM        VARCHAR(50);
     LD_FECHA_ACTUAL        DATE;
     LE_ERROR_ABORTAR     EXCEPTION;
     LE_ERROR                          EXCEPTION;
     --SCP:VARIABLES
     ---------------------------------------------------------------
     --SCP: Código generado automaticamente. Definición de variables
     ---------------------------------------------------------------
     ln_id_bitacora_scp number:=0; 
     ln_total_registros_scp number:=0;
     ln_error_scp                  number:=0;
     lv_error_scp                  varchar2(500);
     lv_id_proceso_scp varchar2(100):=upper(PV_DESSHORT);
     lv_referencia_scp varchar2(100):='RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG';
     lv_unidad_registro_scp varchar2(30):='Registros Historicos';
     ln_registros_procesados_scp number:=0;
     lv_mensaje_apl_scp     varchar2(4000);
     ---------------------------------------------------------------
     LN_MAX_EXECUTE                     NUMBER;
     LN_MAXRETENTRX                   NUMBER;
     LN_MAX_COMMIT                      NUMBER;
     LN_value_max_nointento          NUMBER;
     LN_value_max_trx_encolada     NUMBER;
     LN_MAXTRX                                 NUMBER;

  BEGIN
  
       LD_FECHA_ACTUAL:=SYSDATE;
       LN_ID_PROCESS:=RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_ID_PROCESS(PV_DESSHORT);
       RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_RULE_ELEMENT(PV_DESSHORT, 
                                                                                                                           LN_value_max_nointento,
                                                                                                                           LN_value_max_trx_encolada,
                                                                                                                           LN_MAX_EXECUTE,                                                                                                                               
                                                                                                                           LN_MAX_COMMIT,
                                                                                                                           LN_MAXRETENTRX,
                                                                                                                           PV_OUT_ERROR
                                                                                                                           );
       LN_COUNT:=0;
       LN_COUNT_BCK:=0;
       IF LN_ID_PROCESS = 0 THEN
           LV_MENSAJE:=' ADVERTENCIA NO EXITE ESTE PROCESO-> '||PV_DESSHORT;
           RAISE LE_ERROR;
       END IF; 
            
        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);
        RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(LN_ID_PROCESS,LN_MAXTRX);
        LV_FECHA_ELIM:= TO_CHAR((LD_FECHA_ACTUAL - LN_MAXRETENTRX),'DD/MM/YYYY');
            
        ln_total_registros_scp:=RC_CANTIDAD_REGISTROS(LN_ID_PROCESS,LV_FECHA_ELIM);
        --SCP:INICIO
        ----------------------------------------------------------------------------
        -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
        ----------------------------------------------------------------------------
        scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,lv_error_scp);
        if ln_error_scp <>0 Then
           LV_MENSAJE:='Error en plataforma SCP >>'||lv_error_scp;
           Raise LE_ERROR;
        end if;
            
        ----------------------------------------------------------------------
       lv_mensaje_apl_scp:='INICIO '||PV_DESSHORT;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            Null,
                                            Null,
                                            0,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'N',ln_error_scp,lv_error_scp);           
       ---------------------------------------------------------------------------------           
       OPEN  C_RULES_VALUES(LN_ID_PROCESS);
       LOOP    
          FETCH C_RULES_VALUES INTO LV_SQL;
          EXIT WHEN C_RULES_VALUES%NOTFOUND;
                    
                  BEGIN  
                          
                         EXECUTE IMMEDIATE LV_SQL USING LV_FECHA_ELIM;
                         LN_COUNT:=LN_COUNT+SQL%ROWCOUNT;
                          IF LN_COUNT >= LN_MAX_COMMIT THEN
                             LN_COUNT:=1;
                             --SCP:AVANCE
                             -----------------------------------------------------------------------
                             -- SCP: Código generado automáticamente. Registro de avance de proceso
                             -----------------------------------------------------------------------
                             ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_MAX_COMMIT;
                             scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                   ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                             -----------------------------------------------------------------------
                             COMMIT;
                           END IF;
                          LN_COUNT_BCK:=LN_COUNT_BCK+SQL%ROWCOUNT;
                          IF LN_COUNT_BCK >= LN_MAXTRX THEN
                            RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);
                            LN_COUNT_BCK:=1;
                          END IF;
                         IF LB_EXISTE_BCK = FALSE THEN
                            LV_MENSAJE:='PROCESO ABORTADO MANUALMENTE';
                            RAISE LE_ERROR_ABORTAR;
                         END IF;
                        
                   EXCEPTION
                   WHEN OTHERS Then
                   Rollback;
                   LV_STATUS:='ERR';
                   LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||' ERROR:'||SQLCODE||' '||SUBSTR(SQLERRM, 1, 100);
                   lv_mensaje_apl_scp:='Error al monento de procesar';
                   -------------------------------------------------------------------------
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                            2,Null, Null, Null,null,
                                            null,'N',ln_error_scp,lv_error_scp);
                       
                   -------------------------------------------------------------------------                         
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                 LD_FECHA_ACTUAL,
                                                                 SYSDATE,
                                                                 LV_STATUS,
                                                                 LV_MENSAJE);
                 END;
       END LOOP;
       CLOSE C_RULES_VALUES;
       COMMIT;
           
       RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(PV_DESSHORT,LN_ID_PROCESS,LD_FECHA_ACTUAL,LV_MENSAJE);
       IF  LV_MENSAJE IS NOT NULL THEN
              RAISE LE_ERROR;
       END IF;
             
       LV_STATUS:='OKI';
       LV_MENSAJE:='RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG-SUCESSFUL';
       ----------------------------------------------------------------------------
       lv_mensaje_apl_scp:='FIN '||PV_DESSHORT;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             0,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------                                     
       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                         LD_FECHA_ACTUAL,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
        --SCP:FIN
        ----------------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de finalización de proceso
        ----------------------------------------------------------------------------
        ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT;
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        
        ----------------------------------------------------------------------------
        PV_OUT_ERROR:=LV_MENSAJE;
       EXCEPTION 
         
          WHEN LE_ERROR_ABORTAR Then
               Rollback;
               LV_STATUS:='ERR';
               LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||LV_MENSAJE;
               PV_OUT_ERROR:=LV_MENSAJE;
               lv_mensaje_apl_scp:='    Proceso Abortado por el usuario';
               -------------------------------------------------------------------
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             2,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               -------------------------------------------------------------------                              
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
          WHEN LE_ERROR Then
               Rollback;
               LV_STATUS:='ERR';
               LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '  || LV_MENSAJE;
               PV_OUT_ERROR:=LV_MENSAJE;
               lv_mensaje_apl_scp:='    Error en procesamiento';
               ------------------------------------------------------------------
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             3,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               -------------------------------------------------------------------                              
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
               RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,LV_MENSAJE);
          
          WHEN NO_DATA_FOUND THEN
               LV_STATUS:='ERR';
               LV_MENSAJE:='PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG-NO SE ENCONTRARON PROCESOS A EJECUTAR';
               PV_OUT_ERROR:=LV_MENSAJE;
               ----------------------------------------------------------------------
               lv_mensaje_apl_scp:='    No se Encontraron Datos para el procesamiento';
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp,LV_MENSAJE,Null,
                                             2,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               ----------------------------------------------------------------------
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
          
          WHEN OTHERS Then
              ROLLBACK;
              LV_STATUS:='ERR';
              LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||' ERROR:'||SQLCODE||' '||SUBSTR(SQLERRM, 1, 100);
              PV_OUT_ERROR:=LV_MENSAJE;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp:='    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             3,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
              scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
              scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
              --------------------------------------------------------------------------                               
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                              LD_FECHA_ACTUAL,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,LV_MENSAJE);
           
  END RC_ELIMINA_TTC_HIST_LOG;
  
  /************************************************************************************************
  *
  ************************************************************************************************/
  PROCEDURE RC_OBTENER_MAXRETEN(PN_PROCESS      IN NUMBER,
                                PN_MAX_RETEN    OUT NUMBER)   IS

    LV_MAX_RETEN        NUMBER;
   
  BEGIN
    
    SELECT A.MAXRETENTRX 
    INTO LV_MAX_RETEN
    FROM TTC_RULE_ELEMENT_S A
    WHERE PROCESS=PN_PROCESS;
    
    PN_MAX_RETEN:= LV_MAX_RETEN;
   
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
        PN_MAX_RETEN:=0;  
   WHEN OTHERS THEN
   PN_MAX_RETEN:=0;
  
  END RC_OBTENER_MAXRETEN;
  
  /************************************************************************************************
  * Obtengo la cantidad de registros total que debe procesar
  ************************************************************************************************/
  
  Function RC_CANTIDAD_REGISTROS(PN_PROCESS  IN Number, PN_FECHA_DEL In Varchar2) Return Number  IS

    LN_CANTIDAD_REG        NUMBER:=0;
    LN_CAN_TOTAL_REG       Number:=0;
    LV_SQL                 VARCHAR2(2000);
   
  BEGIN
    
    OPEN  C_RULES_VALUES(PN_PROCESS);
       LOOP    
          FETCH C_RULES_VALUES INTO LV_SQL;
          EXIT WHEN C_RULES_VALUES%NOTFOUND;
             LV_SQL:=REPLACE (LV_SQL,'DELETE', 'SELECT COUNT(*) FROM');
             Execute Immediate LV_SQL Into LN_CANTIDAD_REG Using PN_FECHA_DEL;
             LN_CAN_TOTAL_REG:=LN_CANTIDAD_REG+LN_CAN_TOTAL_REG;
       End Loop;
    Close C_RULES_VALUES;
    
    Return LN_CAN_TOTAL_REG;
   
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
        Return 0;  
   WHEN OTHERS Then
        DBMS_OUTPUT.put_line(' ERROR:'||SQLCODE||' '||SUBSTR(SQLERRM, 1, 100));
        Return 0;
  
  END RC_CANTIDAD_REGISTROS;
  
  PROCEDURE RC_ANALIZE_TTC_HIST_TABLE(PV_DESSHORT       IN  VARCHAR2,
                                      PN_SOSPID            IN NUMBER,   
                                      PV_OUT_ERROR      OUT VARCHAR2
                                      )  IS
                                      
     CURSOR C_Trx_In IS
     SELECT  TTC_TRX_IN.NEXTVAL FROM dual;
     
     i_Process_Programmer_t       ttc_Process_Programmer_t;         
     LN_ID_PROCESS        NUMBER;
     LN_COUNT                 NUMBER;
     LN_COUNT_BCK        NUMBER;
     Ln_Seqno            number;
     LB_EXISTE_BCK         BOOLEAN;
     LV_MENSAJE              VARCHAR2(500);
     LV_STATUS                 VARCHAR2(3);
     LV_SQL                        VARCHAR2(2000);
     LV_FECHA_analize        date;
     LD_FECHA_ACTUAL        DATE;
     LV_fecha_ejecucion     date;
     fecha_programer        date;
     LE_ERROR_ABORTAR     EXCEPTION;
     LE_ERROR                          EXCEPTION;
     --SCP:VARIABLES
     ---------------------------------------------------------------
     --SCP: Código generado automaticamente. Definición de variables
     ---------------------------------------------------------------
     ln_id_bitacora_scp number:=0; 
     ln_total_registros_scp number:=0;
     ln_error_scp                  number:=0;
     lv_error_scp                  varchar2(500);
     lv_id_proceso_scp varchar2(100):=upper(PV_DESSHORT);
     lv_referencia_scp varchar2(100):='RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG';
     lv_unidad_registro_scp varchar2(30):='Registros Historicos';
     ln_registros_procesados_scp number:=0;
     lv_mensaje_apl_scp     varchar2(4000);
     ---------------------------------------------------------------
     LN_MAX_EXECUTE                     NUMBER;
     LN_MAXRETENTRX                   NUMBER;
     LN_MAX_COMMIT                      NUMBER;
     LN_value_max_nointento          NUMBER;
     LN_value_max_trx_encolada     NUMBER;
     LN_MAXTRX                                 NUMBER;

  BEGIN
  
       LD_FECHA_ACTUAL:=SYSDATE;
       SELECT to_date(SYSDATE, 'dd/mm/rrrr') INTO LV_fecha_ejecucion FROM dual;
       LN_ID_PROCESS:=RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_ID_PROCESS(PV_DESSHORT);
       RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_RULE_ELEMENT(PV_DESSHORT, 
                                                                                                                           LN_value_max_nointento,
                                                                                                                           LN_value_max_trx_encolada,
                                                                                                                           LN_MAX_EXECUTE,                                                                                                                               
                                                                                                                           LN_MAX_COMMIT,
                                                                                                                           LN_MAXRETENTRX,
                                                                                                                           PV_OUT_ERROR
                                                                                                                           );
       LN_COUNT:=0;
       LN_COUNT_BCK:=0;
       IF LN_ID_PROCESS = 0 THEN
           LV_MENSAJE:=' ADVERTENCIA NO EXITE ESTE PROCESO-> '||PV_DESSHORT;
           RAISE LE_ERROR;
       END IF; 
            
        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);
        --OJO esto no va
        RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_MAXTRX_PROCESS(LN_ID_PROCESS,LN_MAXTRX);
        fecha_programer := rc_api_timetocash_rule_all.fct_programmer_datevalid(pn_process => LN_ID_PROCESS);
        LV_FECHA_analize:= LD_FECHA_ACTUAL - LN_MAXRETENTRX;       
        --ln_total_registros_scp:=RC_CANTIDAD_REGISTROS(LN_ID_PROCESS,LV_FECHA_ELIM);
        --
        
        --SCP:INICIO
        ----------------------------------------------------------------------------
        -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
        ----------------------------------------------------------------------------
        if fecha_programer <= LV_FECHA_analize then
          ln_total_registros_scp:=50;
        end if;
        --ln_total_registros_scp:=0;
        scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,lv_error_scp);
        if ln_error_scp <>0 Then
           LV_MENSAJE:='Error en plataforma SCP >>'||lv_error_scp;
           Raise LE_ERROR;
        end if;
            
        ----------------------------------------------------------------------
       lv_mensaje_apl_scp:='INICIO '||PV_DESSHORT;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            Null,
                                            Null,
                                            0,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'N',ln_error_scp,lv_error_scp);           
       ---------------------------------------------------------------------------------           
       OPEN  C_RULES_VALUES(LN_ID_PROCESS);
       LOOP    
          FETCH C_RULES_VALUES INTO LV_SQL;
          EXIT WHEN C_RULES_VALUES%NOTFOUND;
                    
                  BEGIN  
                          
                         EXECUTE IMMEDIATE LV_SQL; --USING LV_FECHA_ELIM;
                         LN_COUNT:=LN_COUNT+SQL%ROWCOUNT;
                          IF LN_COUNT >= LN_MAX_COMMIT THEN
                             LN_COUNT:=1;
                             --SCP:AVANCE
                             -----------------------------------------------------------------------
                             -- SCP: Código generado automáticamente. Registro de avance de proceso
                             -----------------------------------------------------------------------
                             ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_MAX_COMMIT;
                             scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                                   ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                             -----------------------------------------------------------------------
                             COMMIT;
                           END IF;
                          LN_COUNT_BCK:=LN_COUNT_BCK+SQL%ROWCOUNT;
                          IF LN_COUNT_BCK >= LN_MAXTRX THEN
                            RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);
                            LN_COUNT_BCK:=1;
                          END IF;
                         IF LB_EXISTE_BCK = FALSE THEN
                            LV_MENSAJE:='PROCESO ABORTADO MANUALMENTE';
                            RAISE LE_ERROR_ABORTAR;
                         END IF;
                        
                   EXCEPTION
                   WHEN OTHERS Then
                   Rollback;
                   LV_STATUS:='ERR';
                   LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||' ERROR:'||SQLCODE||' '||SUBSTR(SQLERRM, 1, 100);
                   lv_mensaje_apl_scp:='Error al monento de procesar';
                   -------------------------------------------------------------------------
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                            2,Null, Null, Null,null,
                                            null,'N',ln_error_scp,lv_error_scp);
                       
                   -------------------------------------------------------------------------                         
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                 LD_FECHA_ACTUAL,
                                                                 SYSDATE,
                                                                 LV_STATUS,
                                                                 LV_MENSAJE);
                 END;
       END LOOP;
       CLOSE C_RULES_VALUES;
       COMMIT;
        
       OPEN C_Trx_In ;
       FETCH C_Trx_In INTO Ln_Seqno;
       CLOSE C_Trx_In;                                                                                                          
       i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => LN_ID_PROCESS,
                                                       Seqno => Ln_Seqno,
                                                       RunDate => LV_fecha_ejecucion,
                                                       LastModDate => LV_fecha_ejecucion
                                                       );   
                                                       
       LV_MENSAJE:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);
       --RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(PV_DESSHORT,LN_ID_PROCESS,LD_FECHA_ACTUAL,LV_MENSAJE);
       IF  LV_MENSAJE IS NOT NULL THEN
              RAISE LE_ERROR;
       END IF;
             
       LV_STATUS:='OKI';
       LV_MENSAJE:='RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG-SUCESSFUL';
       ----------------------------------------------------------------------------
       lv_mensaje_apl_scp:='FIN '||PV_DESSHORT;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             0,Null, Null, Null,null,
                                             null,'N',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------                                     
       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                         LD_FECHA_ACTUAL,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
        --SCP:FIN
        ----------------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de finalización de proceso
        ----------------------------------------------------------------------------
        ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT;
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        
        ----------------------------------------------------------------------------
        PV_OUT_ERROR:=LV_MENSAJE;
       EXCEPTION 
         
          WHEN LE_ERROR_ABORTAR Then
               Rollback;
               LV_STATUS:='ERR';
               LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||LV_MENSAJE;
               PV_OUT_ERROR:=LV_MENSAJE;
               lv_mensaje_apl_scp:='    Proceso Abortado por el usuario';
               -------------------------------------------------------------------
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             2,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               -------------------------------------------------------------------                              
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
          WHEN LE_ERROR Then
               Rollback;
               LV_STATUS:='ERR';
               LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '  || LV_MENSAJE;
               PV_OUT_ERROR:=LV_MENSAJE;
               lv_mensaje_apl_scp:='    Error en procesamiento';
               ------------------------------------------------------------------
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             3,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               -------------------------------------------------------------------                              
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
               RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,LV_MENSAJE);
          
          WHEN NO_DATA_FOUND THEN
               LV_STATUS:='ERR';
               LV_MENSAJE:='PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG-NO SE ENCONTRARON PROCESOS A EJECUTAR';
               PV_OUT_ERROR:=LV_MENSAJE;
               ----------------------------------------------------------------------
               lv_mensaje_apl_scp:='    No se Encontraron Datos para el procesamiento';
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp,LV_MENSAJE,Null,
                                             2,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               ----------------------------------------------------------------------
               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                                  LD_FECHA_ACTUAL,
                                                                  SYSDATE,
                                                                  LV_STATUS,
                                                                  LV_MENSAJE);
          
          WHEN OTHERS Then
              ROLLBACK;
              LV_STATUS:='ERR';
              LV_MENSAJE := 'PROCESO: '||PV_DESSHORT||' >> RC_Job_TimeToCash_History.RC_ELIMINA_TTC_HIST_LOG- '||' ERROR:'||SQLCODE||' '||SUBSTR(SQLERRM, 1, 100);
              PV_OUT_ERROR:=LV_MENSAJE;
              ---------------------------------------------------------------------------
              lv_mensaje_apl_scp:='    Error al momento del procesamiento';
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                             lv_mensaje_apl_scp, LV_MENSAJE, Null,
                                             3,Null, Null, Null,null,
                                             null,'S',ln_error_scp,lv_error_scp);
              scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,Null,-1,ln_error_scp,lv_error_scp);
              scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
              --------------------------------------------------------------------------                               
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_INOUT(LN_ID_PROCESS,
                                                              LD_FECHA_ACTUAL,
                                                              SYSDATE,
                                                              LV_STATUS,
                                                              LV_MENSAJE);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(LN_ID_PROCESS,PV_DESSHORT,PV_DESSHORT,0,LV_MENSAJE);
           
  END RC_ANALIZE_TTC_HIST_TABLE;
  
 END RC_Job_TimeToCash_History;
/
