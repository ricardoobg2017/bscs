create or replace package reloj.RC_API_CHECK_PROCESS is

  -- Author  : JRONQUILLO
  -- Created : 27/10/2010 15:49:57
  -- Purpose : Vericación de los procesos del Reloj
  --                   QC de Reactivación, Mensajería y Suspensión
  --                   Control de ejecuciones
  --                   Envío de Mails, SMS de verificación
  --                   Resumen Diario, Mensual de las ejecuciones
    
  
  -- Public type declarations 
  -- Public constant declarations
  -- Public variable declarations
  -- Public function and procedure declarations
  /*
 PROCEDURE Prc_Qc_Suspension;
 PROCEDURE Prc_Control_Process;
 PROCEDURE Prc_Qc_Reactivacion;
 PROCEDURE Prc_Qc_Mensajeria;
 PROCEDURE Prc_Send_Message;
 PROCEDURE Prc_Resumen_Process;
 */
end RC_API_CHECK_PROCESS;
/
create or replace package body reloj.RC_API_CHECK_PROCESS is

  -- Private type declarations   
  -- Private constant declarations
  -- Private variable declarations

  -- Function and procedure implementations
end RC_API_CHECK_PROCESS;
/
