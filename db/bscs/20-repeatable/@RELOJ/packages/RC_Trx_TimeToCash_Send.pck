create or replace package reloj.RC_Trx_TimeToCash_Send is

  GV_DESSHORT VARCHAR2(8) := 'TTC_SEND';
  GV_DESSHORT_DTH VARCHAR2(15) := 'TTC_SEND_DTH';--8693 JJI
  
  GV_PK_ID           NUMBER:=1; --Package de Reactivación
  GV_TTC_REAC_COMMENT VARCHAR2(30) := 'TTC_REAC_COMMENT';
  GV_STATUS_P   VARCHAR2(1) := 'P';
  GV_STATUS_T   VARCHAR2(1) := 'T';
  GV_STATUS_A   VARCHAR2(1) := 'A';
  GV_STATUS_L   VARCHAR2(1) := 'L';
  GV_STATUS_E   VARCHAR2(1) := 'E';
  GN_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_VALUE_TRX';
  Gv_unidad_registro_scp varchar2(30):='Cuentas';
  TYPE T_TableName                 IS TABLE OF ttc_relation_objects_s.table_valuestring%TYPE  INDEX BY BINARY_INTEGER;
  TYPE T_DesShort                     IS TABLE OF VARCHAR2(100)  INDEX BY BINARY_INTEGER;
  TYPE T_DesLong                      IS TABLE OF VARCHAR2(500)  INDEX BY BINARY_INTEGER;
  TYPE T_SumCountTread         IS TABLE OF NUMBER  INDEX BY BINARY_INTEGER;
   V_TableName              T_TableName;                 
   V_SumCountTread      T_SumCountTread;                 

   V_Customers_tb           T_DesShort;                 
   V_Customers_id           T_SumCountTread;       

   
  PROCEDURE RC_HEAD(PV_FILTER     IN VARCHAR2,
                    PN_HILO           In Number,
                    PN_GROUPTREAD IN NUMBER,
                    PN_CANTIDAD   In Number,
                    PN_SOSPID        IN NUMBER,
                    PV_OUT_ERROR  OUT VARCHAR2);

  PROCEDURE Prc_Distribuir_Table_ALL(PN_ID_PROCESS        IN NUMBER,
                                                                              PV_FILTER                 IN VARCHAR2,
                                                                              PN_HILO                     IN NUMBER,
                                                                              PD_FECHA                  DATE,
                                                                              PN_MAXTRX_EXE      NUMBER,
                                                                              PN_MAX_COMMIT    NUMBER,          
                                                                              pn_id_bitacora_scp    NUMBER,
                                                                              PN_SOSPID        IN NUMBER,
                                                                              PV_OUT_ERROR        OUT VARCHAR2
                                                                              );

  PROCEDURE Prc_Distribuir_Table_Group(PN_ID_PROCESS     IN NUMBER,
                                                                                 PV_FILTER              IN VARCHAR2,
                                                                                 PN_HILO                  In Number,
                                                                                 PN_GROUPTREAD IN NUMBER,
                                                                                 PD_FECHA                DATE,
                                                                                 PN_MAXTRX_EXE    NUMBER,
                                                                                 PN_MAX_COMMIT  NUMBER,          
                                                                                 pn_id_bitacora_scp    NUMBER,
                                                                                 PN_SOSPID        IN NUMBER,
                                                                                 PV_OUT_ERROR      OUT VARCHAR2
                                                                                 );
                                                                                 
  
  
  --[8693] JJI - INICIO
  PROCEDURE RC_HEAD_DTH(PV_FILTER     IN VARCHAR2,
                      PN_HILO       IN NUMBER,
                      PN_GROUPTREAD IN NUMBER,
                      PN_CANTIDAD   IN NUMBER,
                      PN_SOSPID     IN NUMBER,
                      PV_OUT_ERROR  OUT VARCHAR2);
                      
  PROCEDURE Prc_Distribuir_Table_ALL_Dth(PN_ID_PROCESS        IN NUMBER, 
                                        PV_FILTER             IN VARCHAR2,
                                        PN_HILO               IN NUMBER,
                                        PD_FECHA              DATE,
                                        PN_MAXTRX_EXE         NUMBER,
                                        PN_MAX_COMMIT         NUMBER,   
                                        pn_id_bitacora_scp    NUMBER,  
                                        PN_SOSPID             IN NUMBER,                                                                                                                                              
                                        PV_OUT_ERROR          OUT VARCHAR2
                                        );
                                        
  PROCEDURE Prc_Distribuir_Table_Group_Dth(PN_ID_PROCESS        IN NUMBER, 
                                          PV_FILTER             IN VARCHAR2,
                                          PN_HILO               IN NUMBER,
                                          PN_GROUPTREAD         IN NUMBER,
                                          PD_FECHA              DATE,
                                          PN_MAXTRX_EXE         NUMBER,
                                          PN_MAX_COMMIT         NUMBER,   
                                          pn_id_bitacora_scp    NUMBER,  
                                          PN_SOSPID             IN NUMBER,                                                                                                                                              
                                          PV_OUT_ERROR          OUT VARCHAR2
                                          );
  --[8693] JJI - FIN
                                                                                  
     
end RC_Trx_TimeToCash_Send;
/
create or replace package body reloj.RC_Trx_TimeToCash_Send IS

 /*******************************************************************************************************
  * [4737] REINGENIERIA DE RELOJ DE COBRANZA
  * Autor :     IRO Isael Solis
  * Creacion:   26/11/2009
  * Modificado: 07/01/2010
  *-------------------------------------------------------------------------------------------------------
  *  Procesos del Reloj de Cobranzas:
   * Proósito:
  *     Envio de Transacciones a las colas de despacho. que correspondan a la fecha de procesamiento
  *     verificando el criterio para la distribución equitativa a las tablas, según los parámetros de
  *     entrada y de la configuración en la tabla TTC_RELATION_OBJECTS_S     
  ********************************************************************************************************/  
  /*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8693 RELOJ DE COBRANZA DTH
  FECHA:          03/05/2013
  PROPOSITO:      Creacion del procedimiento RC_HEAD_DTH encargado de distribuir las cuentas dth de 
		              ttc_contractplannigdetails a las tablas DTH_PROCESS_CONTRACT0..4
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO JOHANNA JIMENEZ
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          26/01/2015
  PROPOSITO:      Se modifica el procedimiento Prc_distribuir_table_all y Prc_distribuir_table_group para
                  incluir validacion y llamado a la nueva funcion Fct_Save_ToWk_Contract_2. Se genera nuevo
		              merge
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO JORDAN RODRIGUEZ
  PROYECTO:       10995 Equipo Cambios Continuo Reloj de Cobranzas y Reactivaciones
  FECHA:          18/08/2016
  PROPOSITO:      Se agrega una bandera para determinar en que tabla (TTC_PROCESS_CONTRACT_REPO_s/TTC_PROCESS_CONTRACT0_s..TTC_PROCESS_CONTRACT10_s)
                  deben ser ingresados los servicios que tienen que ser suspendidos
  *********************************************************************************************************/
  /*********************************************************************************************************
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  modificado por: Iro Anabell Amayquema
  Fecha         : 11/11/2016
  Proyecto      : 10995 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Asignación de hilos para el proceso de suspensión de forma equitativa y priorización de la 
                  suspensión - asignación de la categoría
  *********************************************************************************************************/
  /*********************************************************************************************************
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  modificado por: Iro Anabell Amayquema
  Fecha         : 04/05/2017
  Proyecto      : 11334 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Optimización del cursor C_Customer_x_Tipo2 para que considere un rango de tiempo de las 
                  trx pendientes de suspender.
  *********************************************************************************************************/
  /*********************************************************************************************************
  Lider SIS     : SIS Antonio Mayorga
  Lider PDS     : Iro Juan Romero
  modificado por: Iro Anabell Amayquema
  Fecha         : 10/05/2017
  Proyecto      : 11334 Mejoras al Proceso de Reloj de Cobranza
  Motivo        : Optimización del cursor C_Customer_x_Tipo2 para que considere un rango de tiempo mayor  
                  de las trx pendientes de suspender y corrección del Hints.
  *********************************************************************************************************/
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      05/06/2018
-- PROYECTO:       [11922] Equipo Ágil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Ajuste de Reloj para clientes suspendidos con forma de pago banco en trámite.
--=====================================================================================--
  CURSOR C_TTC_VIEW_FILTER_ALL(PV_filterfield VARCHAR2) IS
     SELECT FILTER_VALUE Value_Group_Tread
        FROM TTC_VIEW_FILTER_ALL A
     WHERE A.filter  = decode(UPPER(PV_filterfield),'CYCLE','CYCLES',UPPER(PV_filterfield));        

  CURSOR C_Contracts(CN_grouptread NUMBER, PV_filterfield VARCHAR2, PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT SUM(decode(CV_Tipo,'S',1,cpl.valuetrx)) Tot_Contracts
         FROM Ttc_Contractplannigdetails_s cpl 
      WHERE cpl.ch_timeacciondate<=PD_Fecha
             AND  DECODE(PV_filterfield,'PRGCODE',Prgcode, 
                                         DECODE(PV_filterfield, 'CYCLE',TO_NUMBER(AdminCycle), 
                                         TO_NUMBER(Billcycle))) = CN_grouptread
             AND cpl.co_statustrx = GV_STATUS_P;                                        

    CURSOR C_NameTables(CN_Id_Process NUMBER,PV_filterfield VARCHAR2) IS
     SELECT DISTINCT table_valuestring , 0  SumContract 
        FROM ttc_relation_objects_s  
     WHERE Process     = CN_Id_Process
            AND filterfield  = PV_filterfield;

    CURSOR C_GroupThread(CN_Id_Process NUMBER,PV_filterfield VARCHAR2) IS
      SELECT COUNT(DISTINCT TABLE_VALUESTRING) tot_groupThread
        FROM ttc_relation_objects_s  
     WHERE Process       = CN_Id_Process
            AND filterfield    = PV_filterfield;
            
     --[8693]JJI - INICIO       
    --Cursor para la codición de distribuir por contrato
    CURSOR C_CustomerDth_x_Tipo(PN_grouptread NUMBER, PV_filterfield VARCHAR2,  PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT /*+ rule +*/
       TTC_Customers_Count_t(cpl.Customer_id,
                             cpl.Custcode,
                             pay.bank_id,
                             MIN(cpl.co_idtramite),
                             MIN(cpl.co_period),
                             SUM(decode(CV_Tipo, 'S', 1, cpl.valuetrx)))
        FROM Ttc_Contractplannigdetails_s cpl, sysadm.payment_all pay
       WHERE cpl.ch_timeacciondate <= PD_Fecha
         AND cpl.customer_id = pay.customer_id
         AND DECODE(PV_filterfield,'CYCLE',TO_NUMBER(AdminCycle),'BILLCYCLE',
                           TO_NUMBER(Billcycle)) = PN_grouptread
         AND PRGCODE = (select valor from scp.scp_parametros_procesos 
                         where id_parametro='PRGCODE_DTH'
                           and id_proceso='RELOJ_DTH') -- para que cosidere solo las cuentas DTH
         AND pay.act_used = 'X' --La forma de pago relacionada
         AND cpl.co_statustrx = GV_STATUS_P
       GROUP BY cpl.Customer_id, cpl.Custcode, pay.bank_id;
            
     --[8693]JJI - FIN          

--Cursor para la codición de distribuir por contrato
    CURSOR C_Customer_x_Tipo(PN_grouptread NUMBER, PV_filterfield VARCHAR2,  PD_Fecha DATE,CV_Tipo  VARCHAR2) IS
      SELECT /*+ rule +*/ TTC_Customers_Count_t(cpl.Customer_id,cpl.Custcode,pay.bank_id,MIN(cpl.co_idtramite),MIN(cpl.co_period),SUM(decode(CV_Tipo,'S',1,cpl.valuetrx)))  
         FROM Ttc_Contractplannigdetails_s cpl ,
                      sysadm.payment_all pay
      WHERE cpl.ch_timeacciondate<=PD_Fecha
            AND cpl.customer_id=pay.customer_id
            AND  DECODE(PV_filterfield,'PRGCODE',Prgcode, 
                                         DECODE(PV_filterfield, 'CYCLE',TO_NUMBER(AdminCycle), 
                                         TO_NUMBER(Billcycle))) = PN_grouptread
            AND PRGCODE <> (select valor from scp.scp_parametros_procesos 
                             where id_parametro='PRGCODE_DTH'
                               and id_proceso='RELOJ_DTH') --[8693] JJI - para que no cosidere las cuentas DTH
            AND pay.act_used    = 'X' --La forma de pago relacionada
            AND cpl.co_statustrx = GV_STATUS_P
      GROUP BY cpl.Customer_id,cpl.Custcode,pay.bank_id;

--Cursor para la codición de distribuir por contrato
--[11334] ini AAM
    CURSOR C_Customer_x_Tipo2(PV_filterfield VARCHAR2,  PD_Fecha DATE,CV_Tipo  VARCHAR2, CN_HILO NUMBER) IS
      SELECT /*+ RULE */ TTC_CUSTOMERS_COUNT_T2(CPL.CUSTOMER_ID,CPL.CUSTCODE,PAY.BANK_ID,MIN(CPL.CO_IDTRAMITE),MIN(CPL.CO_PERIOD),
      SUM(DECODE(CV_TIPO, 'S', 1, CPL.VALUETRX)),TO_DATE(TO_CHAR(CPL.INSERTIONDATE, 'DD/MM/RRRR'),'DD/MM/RRRR'))
        FROM TTC_CONTRACTPLANNIGDETAILS_S CPL, SYSADM.PAYMENT_ALL PAY
       WHERE CPL.CH_TIMEACCIONDATE BETWEEN PD_FECHA - (SELECT NVL(TO_NUMBER(R.VALUE_RETURN_SHORTSTRING), 210)
                                                         FROM RELOJ.TTC_RULE_VALUES R
                                                        WHERE PROCESS = 3
                                                          AND R.VALUE_RULE_STRING = 'DIAS_SUSPENSION_PEND')
         AND PD_FECHA
         AND CPL.CUSTOMER_ID = PAY.CUSTOMER_ID
         AND DECODE(PV_FILTERFIELD,'PRGCODE',PRGCODE,DECODE(PV_FILTERFIELD,'CYCLE',TO_NUMBER(ADMINCYCLE),TO_NUMBER(BILLCYCLE))) = CN_HILO
         AND PRGCODE <> (SELECT VALOR FROM SCP.SCP_PARAMETROS_PROCESOS
                          WHERE ID_PARAMETRO = 'PRGCODE_DTH'
                            AND ID_PROCESO = 'RELOJ_DTH') --[8693] JJI - PARA QUE NO COSIDERE LAS CUENTAS DTH
         AND PAY.ACT_USED = 'X'--LA FORMA DE PAGO RELACIONADA
         AND CPL.CO_STATUSTRX = GV_STATUS_P
       GROUP BY CPL.CUSTOMER_ID,CPL.CUSTCODE,PAY.BANK_ID,TO_DATE(TO_CHAR(CPL.INSERTIONDATE, 'DD/MM/RRRR'), 'DD/MM/RRRR');
--[11334] fin AAM

--Cursor obtiene la tabla default configurada
        CURSOR C_Table_Default(CN_Id_Process NUMBER,PN_grouptread NUMBER, PV_filterfield VARCHAR2) IS
            SELECT Table_valueString
               FROM TTC_RELATION_OBJECTS_S A
            WHERE A.PROCESS          = CN_Id_Process
                   AND A.FILTERFIELD  = PV_filterfield
                   AND A.grouptread       = PN_grouptread;

  -- Private type declarations ,constant declarations,variable declarations                       
   i_Customers_Count    ttc_Customers_Count_t_tbk;
   i_Customers_Count2   ttc_Customers_Count_t_tbk2;   
   i_process                      ttc_process_t;
   i_rule_element             ttc_Rule_Element_t;   
   i_ttc_bank_all_t            ttc_bank_all_t;   
   i_Rule_Values_t           ttc_Rule_Values_t;
   i_Plannigstatus_In       ttc_Plannigstatus_In_t;     
   
    PN_SumCountTread        NUMBER;    
    PN_SumCountContracts  NUMBER;
    PV_react_Comment           VARCHAR2(100);
                
--Procedures and Functions  
--**********************************************************************************
        FUNCTION Fct_Plannigstatus_In_t RETURN  TTC_Plannigstatus_In_t as
          doc_in  TTC_Plannigstatus_In_t;
        BEGIN
         SELECT  TTC_Plannigstatus_In_type(pk_id,
                                                                           st_id,
                                                                           st_seqno, 
                                                                           re_orden,
                                                                           st_sdes,
                                                                           st_status_i,
                                                                           st_status_next,
                                                                           st_duration,
                                                                           st_ext_duration ,
                                                                           st_duration_total,
                                                                           st_status_a,
                                                                           st_status_b,
                                                                           st_reason,
                                                                           st_alert,
                                                                           st_nodayalert,
                                                                           st_valid,
                                                                           co_Remark,
                                                                           co_lastmodDate,
                                                                           st_CkeckStatus,
                                                                           st_valid_cond_1, 
                                                                           st_valid_reac, 
                                                                           st_valid_suspe, 
                                                                           st_valid_eval_1, 
                                                                           st_valid_eval_2                                                             
                                                                           )  BULK COLLECT  INTO  doc_in 
              FROM  TTC_VIEW_PLANNIGSTATUS_IN;
          RETURN doc_in;
        END;


--**********************************************************************************
  FUNCTION Fct_ttc_bank_all_t RETURN  ttc_bank_all_t as
    doc_in  ttc_bank_all_t;
  BEGIN
   SELECT ttc_bank_all_type(bank_Id,bankname) 
                   BULK COLLECT  INTO  doc_in FROM  reloj.ttc_view_bank_all_bscs;
    RETURN doc_in;
  END;

--********************************************************************************** 
  FUNCTION Fct_Save_ToWk_Contract(PN_Customer_ID    NUMBER,
                                                                       PV_TABLA               VARCHAR2,
                                                                       PV_co_valid_chr_1 VARCHAR2 DEFAULT NULL,
                                                                       PD_FECHA                DATE
                                                                       )  RETURN VARCHAR2 IS   
     CURSOR C_PLANING_DETAILS(CN_Customer_id NUMBER) IS
         SELECT CUSTOMER_ID,CO_ID,PK_ID,ST_ID,ST_SEQNO,RE_Orden,CUSTCODE,DN_NUM,
                          CO_PERIOD,CO_STATUS_A,CH_STATUS_B,CO_REASON,PRGCODE,ADMINCYCLE,
                          BILLCYCLE,VALUETRX,ORDERBYTRX,CO_IDTRAMITE,CO_REMARK,
                          CO_USERTRX_1,CO_USERTRX_2,CO_USERTRX_3,CO_COMMANDTRX_1,
                          CO_COMMANDTRX_2,CO_COMMANDTRX_3,CO_NOINTENTO,CO_STATUSTRX,
                          CO_REMARKTRX,INSERTIONDATE,LASTMODDATE
         FROM TTC_VIEW_contractplannigdetail A
       WHERE Customer_id=PN_Customer_ID;
          Lv_Error   TTC_Log_InOut.Comment_Run%TYPE; 
          LV_SQL      VARCHAR2(400);
  BEGIN
          Lv_Error:=NULL;
          FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP           
                     LV_SQL:='INSERT /*+ APPEND */ INTO ' ||PV_TABLA||' NOLOGGING  '||' VALUES (
                                       :D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                                       :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
                      EXECUTE IMMEDIATE LV_SQL   
                           USING rc_send.Customer_id,rc_send.Co_id,rc_send.PK_Id,rc_send.ST_Id,rc_send.ST_Seqno,rc_send.RE_Orden,
                                         rc_send.Custcode,0,rc_send.Dn_Num,rc_send.Co_Period,rc_send.Co_Status_A,rc_send.Ch_Status_B,
                                         rc_send.Co_Reason,rc_send.AdminCycle,rc_send.Billcycle,rc_send.ValueTrx,rc_send.OrderByTrx,
                                         rc_send.Co_IdTramite,PV_TABLA,rc_send.Co_UserTrx_1,rc_send.Co_UserTrx_2,rc_send.Co_UserTrx_3,
                                         rc_send.Co_CommandTrx_1,rc_send.Co_CommandTrx_2,rc_send.Co_CommandTrx_3,rc_send.Co_NoIntento,
                                         rc_send.Co_StatusTrx,rc_send.Co_RemarkTrx, PV_co_valid_chr_1,
                                         'X',0, SYSDATE,rc_send.LastModDate; 
                                               
                     UPDATE TTC_ContractPlannigDetails_s A 
                             SET lastmoddate  = SYSDATE, 
                                     Dn_Num         = rc_send.Dn_Num,
                                     co_statustrx   = GV_STATUS_T
                     WHERE A.CH_TIMEACCIONDATE <=PD_FECHA
                           AND  A.CO_STATUSTRX               = GV_STATUS_P
                           AND  A.ST_SEQNO                        =rc_send.ST_SEQNO
                           AND  A.CO_ID                                = rc_send.Co_id
                           AND  A.CUSTOMER_ID                = rc_send.Customer_id;                                     
          END LOOP; --FOR 
          
          RETURN(Lv_Error);          
  EXCEPTION        
            WHEN DUP_VAL_ON_INDEX THEN                     
                   Lv_Error:='Fct_Save_ToWk_Contract-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
            WHEN OTHERS THEN  
                  Lv_Error:='Fct_Save_ToWk_Contract-Error General al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
                   RETURN (Lv_Error) ;
             
  END;
  
  --********************************************************************************** 
  --9833 Johanna Jimenez - SE CREA PROCEDIMIENTO PARA QUE SE DISTRIBYAN TAMBIEN LAS
  --CUENTAS QUE NO POSEEN DEUDA (CO_STATUSTRX=A) Y LAS CUENTAS QUE POSEEN LIMITE DE 
  --CREDITO (CO_STATUSTRX=L) A LAS TABLAS DE PROCESAMIENTO Y ASI EL PROCESO DISPATCH
  --INSERTE ESTAS TRANSACCIONES EN LA TABLA DE QC DE SUSPENSION MOVIL 
  FUNCTION Fct_Save_ToWk_Contract_2(PN_Customer_ID    NUMBER,
                                    PV_TABLA               VARCHAR2,
                                    PV_co_valid_chr_1 VARCHAR2 DEFAULT NULL,
                                    PD_FECHA                DATE,
                                    PB_REACTIVA             IN BOOLEAN DEFAULT FALSE,
                                    PV_STATUS_OUT           IN VARCHAR2 DEFAULT NULL,
                                    PV_REMARK               IN VARCHAR2 DEFAULT NULL,
                                    PV_CO_COMMANDTRX_1      IN VARCHAR2 DEFAULT NULL,--10995 AAM
                                    PN_HILO                 IN NUMBER DEFAULT NULL,   --10995 AAM
                                    Pv_IdFormaPago          IN VARCHAR2 DEFAULT NULL--INICIO [11922] 31/05/2018 IRO ABA
                                    )  RETURN VARCHAR2 IS   
     CURSOR C_PLANING_DETAILS(CN_Customer_id NUMBER) IS
         SELECT CUSTOMER_ID,CO_ID,PK_ID,ST_ID,ST_SEQNO,RE_Orden,CUSTCODE,DN_NUM,
                          CO_PERIOD,CO_STATUS_A,CH_STATUS_B,CO_REASON,PRGCODE,ADMINCYCLE,
                          BILLCYCLE,VALUETRX,ORDERBYTRX,CO_IDTRAMITE,CO_REMARK,
                          CO_USERTRX_1,CO_USERTRX_2,CO_USERTRX_3,CO_COMMANDTRX_1,
                          CO_COMMANDTRX_2,CO_COMMANDTRX_3,CO_NOINTENTO,CO_STATUSTRX,
                          CO_REMARKTRX,INSERTIONDATE,LASTMODDATE
         FROM TTC_VIEW_contractplannigdetail A
        WHERE Customer_id=PN_Customer_ID
          AND TO_NUMBER(A.billcycle)=PN_HILO;
        
    Lv_Error   TTC_Log_InOut.Comment_Run%TYPE; 
    LV_SQL      VARCHAR2(400);
  BEGIN
    Lv_Error:=NULL;
         
    IF PB_REACTIVA THEN
      FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP           
           LV_SQL:='INSERT /*+ APPEND */ INTO ' ||PV_TABLA||' NOLOGGING  '||' VALUES (
                             :D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                             :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
            EXECUTE IMMEDIATE LV_SQL   
                 USING rc_send.Customer_id,rc_send.Co_id,rc_send.PK_Id,rc_send.ST_Id,rc_send.ST_Seqno,rc_send.RE_Orden,
                               rc_send.Custcode,0,rc_send.Dn_Num,rc_send.Co_Period,rc_send.Co_Status_A,rc_send.Ch_Status_B,
                               rc_send.Co_Reason,rc_send.AdminCycle,rc_send.Billcycle,rc_send.ValueTrx,rc_send.OrderByTrx,
                               rc_send.Co_IdTramite,PV_TABLA,rc_send.Co_UserTrx_1,rc_send.Co_UserTrx_2,rc_send.Co_UserTrx_3,
                               PV_CO_COMMANDTRX_1,/*rc_send.Co_CommandTrx_2*/Pv_IdFormaPago,rc_send.Co_CommandTrx_3,rc_send.Co_NoIntento,
                               PV_STATUS_OUT,PV_REMARK,'N',
                               'X',0, SYSDATE,rc_send.LastModDate; 
                                                 
           UPDATE TTC_ContractPlannigDetails_s A 
              SET lastmoddate = SYSDATE, 
                  Dn_Num = rc_send.Dn_Num,
                  co_statustrx = PV_STATUS_OUT,
                  co_remarktrx = PV_REMARK,
                  CO_COMMANDTRX_1 = PV_CO_COMMANDTRX_1
            WHERE A.CH_TIMEACCIONDATE <=PD_FECHA
              AND  A.CO_STATUSTRX = GV_STATUS_P
              AND  A.ST_SEQNO =rc_send.ST_SEQNO
              AND  A.CO_ID = rc_send.Co_id
              AND  A.CUSTOMER_ID = rc_send.Customer_id;                                     
      END LOOP; 
    ELSE
      FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP           
                 LV_SQL:='INSERT /*+ APPEND */ INTO ' ||PV_TABLA||' NOLOGGING  '||' VALUES (
                                   :D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                                   :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
                  EXECUTE IMMEDIATE LV_SQL   
                       USING rc_send.Customer_id,rc_send.Co_id,rc_send.PK_Id,rc_send.ST_Id,rc_send.ST_Seqno,rc_send.RE_Orden,
                                     rc_send.Custcode,0,rc_send.Dn_Num,rc_send.Co_Period,rc_send.Co_Status_A,rc_send.Ch_Status_B,
                                     rc_send.Co_Reason,rc_send.AdminCycle,rc_send.Billcycle,rc_send.ValueTrx,rc_send.OrderByTrx,
                                     rc_send.Co_IdTramite,PV_TABLA,rc_send.Co_UserTrx_1,rc_send.Co_UserTrx_2,rc_send.Co_UserTrx_3,
                                     PV_CO_COMMANDTRX_1,/*rc_send.Co_CommandTrx_2*/Pv_IdFormaPago,rc_send.Co_CommandTrx_3,rc_send.Co_NoIntento,
                                     rc_send.Co_StatusTrx,rc_send.Co_RemarkTrx, PV_co_valid_chr_1,
                                     'X',0, SYSDATE,rc_send.LastModDate; 
                                                 
                 UPDATE TTC_ContractPlannigDetails_s A 
                         SET lastmoddate  = SYSDATE, 
                                 Dn_Num         = rc_send.Dn_Num,
                                 co_statustrx   = GV_STATUS_T,
                                 CO_COMMANDTRX_1 = PV_CO_COMMANDTRX_1
                 WHERE A.CH_TIMEACCIONDATE <=PD_FECHA
                       AND  A.CO_STATUSTRX               = GV_STATUS_P
                       AND  A.ST_SEQNO                        =rc_send.ST_SEQNO
                       AND  A.CO_ID                                = rc_send.Co_id
                       AND  A.CUSTOMER_ID                = rc_send.Customer_id;                                     
      END LOOP; 
    END IF;
          
    RETURN(Lv_Error);          
  EXCEPTION        
    WHEN DUP_VAL_ON_INDEX THEN                     
           Lv_Error:='Fct_Save_ToWk_Contract_2-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
           RETURN (Lv_Error) ;
    WHEN OTHERS THEN  
          Lv_Error:='Fct_Save_ToWk_Contract_2-Error General al intentar grabar en la tabla ttc_Process_Contract#, '||substr(SQLERRM,1,300);
           RETURN (Lv_Error) ;
             
  END;
  
  --********************************************************************************** 
  FUNCTION Fct_Save_ToWk_Contract_Dth(PN_Customer_ID    NUMBER,
                                      PV_TABLA          VARCHAR2,
                                      PV_co_valid_chr_1 VARCHAR2 DEFAULT NULL,
                                      PD_FECHA          DATE,
                                      Pv_IdFormaPago    IN VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 IS--INICIO [11922] 31/05/2018 IRO ABA
  CURSOR C_PLANING_DETAILS(CN_Customer_id NUMBER) IS
    SELECT CUSTOMER_ID,CO_ID,PK_ID,ST_ID,ST_SEQNO,RE_Orden,CUSTCODE,DN_NUM,CO_PERIOD,CO_STATUS_A,
           CH_STATUS_B,CO_REASON,PRGCODE,ADMINCYCLE,BILLCYCLE,VALUETRX,ORDERBYTRX,CO_IDTRAMITE,
           CO_REMARK,CO_USERTRX_1,CO_USERTRX_2,CO_USERTRX_3,CO_COMMANDTRX_1,CO_COMMANDTRX_2,
           CO_COMMANDTRX_3,CO_NOINTENTO,CO_STATUSTRX,CO_REMARKTRX,INSERTIONDATE,LASTMODDATE
      FROM TTC_CONTRACTPLANNIGDETAILS A
     WHERE Customer_id = PN_Customer_ID
     AND ch_timeacciondate <= trunc(SYSDATE)
     AND co_statustrx = 'P';

  Lv_Error TTC_Log_InOut.Comment_Run%TYPE;
  LV_SQL   VARCHAR2(400);
BEGIN
  Lv_Error := NULL;
  FOR rc_send IN C_PLANING_DETAILS(PN_Customer_ID) LOOP
    LV_SQL := 'INSERT /*+ APPEND */ INTO ' || PV_TABLA || ' NOLOGGING  ' ||
              ' VALUES (:D1,:D2,:D3,:D4, :D5,:D6,:D7,:D8,:D9,:D10,:D11,:D12,:D13,:D14,:D15,:D16,:D17,
                        :D18,:D19,:D20,:D21,:D22,:D23,:D24,:D25,:D26,:D27,:D28,:D29,:D30,:D31,:D32,:D33)';
    EXECUTE IMMEDIATE LV_SQL
      USING rc_send.Customer_id, rc_send.Co_id, rc_send.PK_Id, rc_send.ST_Id, rc_send.ST_Seqno, rc_send.RE_Orden, rc_send.Custcode, 0, rc_send.Dn_Num, rc_send.Co_Period, rc_send.Co_Status_A, rc_send.Ch_Status_B, rc_send.Co_Reason, rc_send.AdminCycle, rc_send.Billcycle, rc_send.ValueTrx, rc_send.OrderByTrx, rc_send.Co_IdTramite, PV_TABLA, rc_send.Co_UserTrx_1, rc_send.Co_UserTrx_2, rc_send.Co_UserTrx_3, rc_send.Co_CommandTrx_1, /*rc_send.Co_CommandTrx_2*/Pv_IdFormaPago, rc_send.Co_CommandTrx_3, rc_send.Co_NoIntento, rc_send.Co_StatusTrx, rc_send.Co_RemarkTrx, PV_co_valid_chr_1, 'X', 0, SYSDATE, rc_send.LastModDate;
  
    UPDATE TTC_ContractPlannigDetails_s A
       SET lastmoddate  = SYSDATE,
           Dn_Num       = rc_send.Dn_Num,
           co_statustrx = GV_STATUS_T
     WHERE A.CH_TIMEACCIONDATE <= PD_FECHA
       AND A.CO_STATUSTRX = GV_STATUS_P
       AND A.ST_SEQNO = rc_send.ST_SEQNO
       AND A.CO_ID = rc_send.Co_id
       AND A.CUSTOMER_ID = rc_send.Customer_id;
  END LOOP; --FOR 

  RETURN(Lv_Error);
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    Lv_Error := 'Fct_Save_ToWk_Contract_Dth-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla Dth_Process_Contract#, ' ||substr(SQLERRM, 1, 300);
    RETURN(Lv_Error);
  WHEN OTHERS THEN
    Lv_Error := 'Fct_Save_ToWk_Contract_Dth-Error General al intentar grabar en la tabla Dth_Process_Contract_#, ' ||substr(SQLERRM, 1, 300);
    RETURN(Lv_Error);
  
END;


/****************************************************************************/
 PROCEDURE RC_HEAD (PV_FILTER                IN VARCHAR2,
                                                PN_HILO                   IN NUMBER,
                                                PN_GROUPTREAD  IN NUMBER,
                                                PN_CANTIDAD        IN  NUMBER,
                                                PN_SOSPID            IN NUMBER,
                                                PV_OUT_ERROR      OUT VARCHAR2)    IS 
                  
     LV_MENSAJE                            VARCHAR2(1000);
     LV_STATUS                              VARCHAR(3);
     LE_ERROR_ABORTADO        EXCEPTION;
     LE_ERROR                                EXCEPTION;
     LE_ERROR_OUT                      EXCEPTION;  
     LE_ERROR_PROC_ANT          EXCEPTION;
     Ld_Fecha_Actual                      DATE;     
    -- LB_CHECK                           BOOLEAN;
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=PN_CANTIDAD;
    lv_id_proceso_scp varchar2(100):=GV_DESSHORT;
    lv_referencia_scp varchar2(200):='RC_Trx_TimeToCash_Send.RC_HEAD';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    ---------------------------------------------------------------
     
   BEGIN
   
        Ld_Fecha_Actual:= SYSDATE;
       --Carga Configuración inicial
        i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
        IF i_process.Process IS NULL THEN
              LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
              RAISE LE_ERROR;
        END IF;
        
        --Cargar los datos del Proceso TTC_SEND --> TTC_rule_element
          i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT);
         IF i_rule_element.Process IS NULL THEN
                LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
                RAISE LE_ERROR;
          END IF;

          
/*         LB_CHECK:=RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_PROCESO_CORRECTO(Ld_Fecha_Actual);
         IF NOT LB_CHECK  THEN
              RAISE LE_ERROR_OUT;
         END IF;
*/         
         -- SCP:INICIO
         ----------------------------------------------------------------------------
         -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
         ----------------------------------------------------------------------------
         lv_referencia_scp:='Hilo: '||PN_HILO||' Grouptheard: '||PN_GROUPTREAD||' Filtro: '||PV_FILTER||' '||lv_referencia_scp;
         scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                               lv_referencia_scp,
                                               null,null,null,null,
                                               ln_total_registros_scp,
                                               Gv_unidad_registro_scp,
                                               ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         if ln_error_scp <>0 Then
            LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
            Raise LE_ERROR;
         end if;
         ----------------------------------------------------------------------
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||' Grouptheard: '||PN_GROUPTREAD;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);
         
         ----------------------------------------------------------------------------
         IF nvl(PN_GROUPTREAD,0)=0 THEN             
                    Prc_Distribuir_Table_ALL(i_process.Process,
                                                                     PV_FILTER,
                                                                     PN_HILO,
                                                                     trunc(Ld_Fecha_Actual),
                                                                      i_rule_element.Value_Max_Trx_By_Execution,
                                                                      i_rule_element.Value_Max_TrxCommit,
                                                                      ln_id_bitacora_scp,    
                                                                      PN_SOSPID,                                                                  
                                                                      LV_MENSAJE);
                    IF LV_MENSAJE IS NOT NULL THEN
                             IF LV_MENSAJE ='ABORTADO' THEN
                                    RAISE LE_ERROR_ABORTADO;
                             END IF;
                             RAISE LE_ERROR;
                   END IF;
         ELSE
                    Prc_Distribuir_Table_Group(i_process.Process,
                                                                         PV_FILTER,
                                                                         PN_HILO,
                                                                         PN_GROUPTREAD,
                                                                         LD_FECHA_ACTUAL,
                                                                          i_rule_element.Value_Max_Trx_By_Execution,
                                                                          i_rule_element.Value_Max_TrxCommit,
                                                                          ln_id_bitacora_scp,
                                                                          PN_SOSPID,                                                                          
                                                                          LV_MENSAJE);
                    IF LV_MENSAJE IS NOT NULL THEN
                             IF LV_MENSAJE ='ABORTADO' THEN
                                   RAISE LE_ERROR_ABORTADO;
                             END IF;
                             RAISE LE_ERROR;
                    END IF;
         END IF;      
       RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(GV_DESSHORT,i_process.Process,LD_FECHA_ACTUAL,LV_MENSAJE); 
        IF LV_MENSAJE IS NOT NULL THEN
               RAISE LE_ERROR;
        END IF;
        
       LV_STATUS:='OKI';
       LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD-SUCESSFUL';
       PV_OUT_ERROR:=LV_MENSAJE;
       ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);
         
        
       --SCP:FIN
       ----------------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de finalización de proceso
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       
       ----------------------------------------------------------------------------
       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                         LD_FECHA_ACTUAL,
                                                                                                         SYSDATE,
                                                                                                         LV_STATUS,
                                                                                                         LV_MENSAJE);       
       EXCEPTION 
            WHEN  LE_ERROR_OUT THEN
                LV_STATUS:='ERR';
                LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- SE DEBE EJECUTAR PREVIAMENTE EL PROCESO OUT, ANTES QUE EL PROCESO SEND';
                PV_OUT_ERROR:=LV_MENSAJE;
                ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error de Inicio';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       1,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp); 
                -----------------------------------------------------------------------
                RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                   LD_FECHA_ACTUAL,
                                                                                                                   SYSDATE,
                                                                                                                   LV_STATUS,
                                                                                                                   LV_MENSAJE);

              WHEN  LE_ERROR_PROC_ANT THEN
                 LV_STATUS:='ERR';
                 LV_MENSAJE:='RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD NO EXISTE PROCESO OUT REQUERIDO';
                 PV_OUT_ERROR:=LV_MENSAJE;
                 ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error de Inicio';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       'Ejecute Primero los 2 procesos OUT(A,D) ',
                                                       2,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                -----------------------------------------------------------------------
                 RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                    LD_FECHA_ACTUAL,
                                                                    SYSDATE,
                                                                    LV_STATUS,
                                                                    LV_MENSAJE);
                   
              WHEN LE_ERROR_ABORTADO THEN
                   LV_STATUS:='ERR';
                   LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '|| LV_MENSAJE;
                   PV_OUT_ERROR:=LV_MENSAJE;
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Proceso Abortado Manualmente por el Usuario';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       2,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                -----------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                      LD_FECHA_ACTUAL,
                                                                                                                      SYSDATE,
                                                                                                                      LV_STATUS,
                                                                                                                      LV_MENSAJE);
              WHEN LE_ERROR THEN
                   LV_STATUS:='ERR';
                  LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '||LV_MENSAJE;
                   PV_OUT_ERROR:=LV_MENSAJE;
                   ----------------------------------------------------------------------
                   lv_mensaje_apl_scp:='    Error en el Procesamiento';
                   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       3,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
                   -----------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                      LD_FECHA_ACTUAL,
                                                                                                                      SYSDATE,
                                                                                                                      LV_STATUS,
                                                                                                                      LV_MENSAJE);
                   RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,PN_HILO,PN_GROUPTREAD,LV_MENSAJE);
       
        WHEN OTHERS THEN
             LV_STATUS:='ERR';
             LV_MENSAJE := 'RC_HEAD-Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.RC_HEAD- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 200);
             PV_OUT_ERROR:=LV_MENSAJE;
             ----------------------------------------------------------------------
                 lv_mensaje_apl_scp:='    Error en el Procesamiento';
                 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                       lv_mensaje_apl_scp,
                                                       LV_MENSAJE,
                                                       Null,
                                                       3,
                                                       Gv_unidad_registro_scp,
                                                       Null,
                                                       Null,
                                                       null,null,
                                                       'S',ln_error_scp,lv_error_scp);
                 scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
              -----------------------------------------------------------------------
             RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                                                                                LD_FECHA_ACTUAL,
                                                                                                                SYSDATE,
                                                                                                                LV_STATUS,
                                                                                                                LV_MENSAJE);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,PN_HILO,PN_GROUPTREAD,LV_MENSAJE);
     END RC_HEAD;
     
/**************************************************************************************
8693 JJI
Proceso encargado de distribuir a las tablas DTH_PROCESS_CONTRACT0..4 las cuentas dth
de ttc_contractplannigdetails que tienen que ser suspendidas
**************************************************************************************/ 
   PROCEDURE RC_HEAD_DTH(PV_FILTER     IN VARCHAR2,
                      PN_HILO       IN NUMBER,
                      PN_GROUPTREAD IN NUMBER,
                      PN_CANTIDAD   IN NUMBER,
                      PN_SOSPID     IN NUMBER,
                      PV_OUT_ERROR  OUT VARCHAR2) IS

  LV_MENSAJE VARCHAR2(1000);
  LV_STATUS  VARCHAR(3);
  LE_ERROR_ABORTADO EXCEPTION;
  LE_ERROR EXCEPTION;
  Ld_Fecha_Actual DATE;
  -- LB_CHECK                           BOOLEAN;

  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: Código generado automaticamente. Definición de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp     number := 0;
  ln_total_registros_scp number := PN_CANTIDAD;
  lv_id_proceso_scp      varchar2(100) := GV_DESSHORT_DTH;
  lv_referencia_scp      varchar2(200) := 'RC_Trx_TimeToCash_Send.RC_HEAD_DTH';
  ln_error_scp           number := 0;
  lv_error_scp           varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  ---------------------------------------------------------------

BEGIN

  Ld_Fecha_Actual := SYSDATE;
  --Carga Configuración inicial
  i_process := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT_DTH);
  IF i_process.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_Process_s';
    RAISE LE_ERROR;
  END IF;

  --Cargar los datos del Proceso TTC_SEND_DTH --> TTC_rule_element
  i_rule_element := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_DTH);
  IF i_rule_element.Process IS NULL THEN
    LV_MENSAJE := 'No existen datos configurados en la tabla TTC_rule_element';
    RAISE LE_ERROR;
  END IF;

  -- SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_referencia_scp := 'Hilo: ' || PN_HILO || ' Grouptheard: ' ||PN_GROUPTREAD || ' Filtro: ' || PV_FILTER || ' ' || lv_referencia_scp;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,
                                        null,
                                        null,
                                        null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  if ln_error_scp <> 0 Then
    LV_MENSAJE := 'Error en Plataforma SCP, No se pudo iniciar la Bitacora';
    Raise LE_ERROR;
  end if;
  ----------------------------------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO ' || GV_DESSHORT_DTH;
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||' Grouptheard: ' || PN_GROUPTREAD;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  ----------------------------------------------------------------------------
  IF nvl(PN_GROUPTREAD, 0) = 0 THEN
    Prc_Distribuir_Table_ALL_DTH(i_process.Process,
                                 PV_FILTER,
                                 PN_HILO,
                                 trunc(Ld_Fecha_Actual),
                                 i_rule_element.Value_Max_Trx_By_Execution,
                                 i_rule_element.Value_Max_TrxCommit,
                                 ln_id_bitacora_scp,
                                 PN_SOSPID,
                                 LV_MENSAJE);
    IF LV_MENSAJE IS NOT NULL THEN
      IF LV_MENSAJE = 'ABORTADO' THEN
        RAISE LE_ERROR_ABORTADO;
      END IF;
      RAISE LE_ERROR;
    END IF;
  ELSE
    Prc_Distribuir_Table_Group_DTH(i_process.Process,
                                   PV_FILTER,
                                   PN_HILO,
                                   PN_GROUPTREAD,
                                   LD_FECHA_ACTUAL,
                                   i_rule_element.Value_Max_Trx_By_Execution,
                                   i_rule_element.Value_Max_TrxCommit,
                                   ln_id_bitacora_scp,
                                   PN_SOSPID,
                                   LV_MENSAJE);
    IF LV_MENSAJE IS NOT NULL THEN
      IF LV_MENSAJE = 'ABORTADO' THEN
        RAISE LE_ERROR_ABORTADO;
      END IF;
      RAISE LE_ERROR;
    END IF;
  END IF;
  RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_PROGRAMMER(GV_DESSHORT_DTH,
                                                      i_process.Process,
                                                      LD_FECHA_ACTUAL,
                                                      LV_MENSAJE);
  IF LV_MENSAJE IS NOT NULL THEN
    RAISE LE_ERROR;
  END IF;

  LV_STATUS    := 'OKI';
  LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD ||' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH-SUCESSFUL';
  PV_OUT_ERROR := LV_MENSAJE;
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'FIN ' || GV_DESSHORT_DTH;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);

  --SCP:FIN
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                        ln_error_scp,
                                        lv_error_scp);

  ----------------------------------------------------------------------------
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);
EXCEPTION
  WHEN LE_ERROR_ABORTADO THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH- ' ||
                    LV_MENSAJE;
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Proceso Abortado Manualmente por el Usuario';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          2,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
  WHEN LE_ERROR THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD-Hilo:' || PN_HILO || ' Grouptheard:' ||
                    PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD- ' ||
                    LV_MENSAJE;
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Error en el Procesamiento';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          3,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
    RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,
                                                GV_DESSHORT_DTH,
                                                PN_HILO,
                                                PN_GROUPTREAD,
                                                LV_MENSAJE);
  
  WHEN OTHERS THEN
    LV_STATUS    := 'ERR';
    LV_MENSAJE   := 'RC_HEAD_DTH-Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD || ' >> RC_Trx_TimeToCash_Send.RC_HEAD_DTH- ' ||
                    ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 200);
    PV_OUT_ERROR := LV_MENSAJE;
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := '    Error en el Procesamiento';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          3,
                                          Gv_unidad_registro_scp,
                                          Null,
                                          Null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    -----------------------------------------------------------------------
    RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                 LD_FECHA_ACTUAL,
                                                 SYSDATE,
                                                 LV_STATUS,
                                                 LV_MENSAJE);
    RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,
                                                GV_DESSHORT_DTH,
                                                PN_HILO,
                                                PN_GROUPTREAD,
                                                LV_MENSAJE);
END RC_HEAD_DTH;

     
     /**************************************************************************************
     **************************************************************************************/ 
     
   PROCEDURE Prc_Distribuir_Table_ALL(PN_ID_PROCESS        IN NUMBER, 
                                                                              PV_FILTER                 IN VARCHAR2,
                                                                              PN_HILO                     IN NUMBER,
                                                                              PD_FECHA                   DATE,
                                                                              PN_MAXTRX_EXE      NUMBER,
                                                                              PN_MAX_COMMIT    NUMBER,   
                                                                              pn_id_bitacora_scp   NUMBER,  
                                                                              PN_SOSPID             IN NUMBER,                                                                                                                                              
                                                                              PV_OUT_ERROR        OUT VARCHAR2
                                                                              )   IS
       /*INI 13200 MTST*/
       --* VALIDA SI LA FECHA ES FIN DE SEMANA
        CURSOR C_VALIDA_FINDE (CD_FECHA DATE) IS
          SELECT DECODE(TO_CHAR(CD_FECHA,'DY',
                        'NLS_DATE_LANGUAGE=ENGLISH'),
                        'FRI','S',
                        'SAT','S',
                        'SUN','S',
                        'N') APLICA
          FROM DUAL;
       CURSOR C_VALIDA_CLI_CORP(CN_CUSTOMER NUMBER)IS
        SELECT COUNT(*)
          FROM PORTA.CL_CONTRATOS@AXIS        C,
               PORTA.CL_PERSONAS_EMPRESA@AXIS A,
               PORTA.CL_CLASES_CLIENTES@AXIS  X,
               CUSTOMER_ALL                   B
         WHERE B.CUSTOMER_ID = CN_CUSTOMER
           AND A.ID_PERSONA = C.ID_PERSONA
           AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
           AND C.CODIGO_DOC = B.CUSTCODE
           AND X.ID_CLASE_CLIENTE IN ('17','23','24','26','27','28')
               /*(SELECT T.ID_CLASE_CLIENTE
                  FROM PORTA.CL_CLASES_CLIENTES@AXIS T
                 WHERE T.DESCRIPCION LIKE '%INS%'
                    OR T.DESCRIPCION LIKE '%VIP%'
                    OR T.DESCRIPCION LIKE '%PRE%')*/;
         
       CURSOR C_OBT_FLAG IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 13200
           AND G.ID_PARAMETRO = 'FLG_VAL_FIN_SEMANA';
           
       CURSOR C_VALIDA_FERIADO (CD_FECHA DATE) IS
        SELECT COUNT(*)
          FROM PORTA.RC_FERIADOS@AXIS D
         WHERE TRUNC(CD_FECHA) BETWEEN TRUNC(D.CF_FECH_INICIO) AND TRUNC(D.CF_FECH_FIN)
           AND D.CF_ESTADO = 'A';
           
       -- 22212 - Troncales Moviles - Suspension de Servicio    
        CURSOR C_OBT_FLAG_TRONCALES IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 22212
           AND G.ID_PARAMETRO = 'FLAG_VAL_TRONCALES';
           
         CURSOR C_CUENTAS_TRONCALE(CV_CUENTA VARCHAR2)IS 
         SELECT COUNT(*) 
         FROM PORTA.CL_CONTRATOS@axis C ,
          PORTA.CL_SERVICIOS_CONTRATADOS@axis A , 
          PORTA.GE_DETALLES_PLANES@axis G
          WHERE C.CODIGO_DOC = CV_CUENTA
          AND C.ID_CONTRATO = A.ID_CONTRATO
          AND A.ID_DETALLE_PLAN = G.ID_DETALLE_PLAN
          AND G.ID_TIPO_PLAN= '032';
          
         CURSOR C_VALIDA_SUSP_TRONC(CV_CUENTA VARCHAR2)IS 
         select C.ESTADO, C.CUENTA
          from PORTA.rc_servicios_troncales@AXIS c
         where c.cuenta =CV_CUENTA
         order by c.fecha_registro desc;
       
       CURSOR C_BAND_RELOJ_MEDIOS IS
       SELECT G.VALOR
         FROM GV_PARAMETROS G
        WHERE G.ID_TIPO_PARAMETRO = 22311
          AND G.ID_PARAMETRO = 'BAN_RC_MEDIOS_SUSP';
       
       LN_VAL_TRONCAL      NUMBER;
       LV_FLAG_TRONCALES   VARCHAR2(1):= 'N';
       LC_VALIDA_SUSP_TRONC C_VALIDA_SUSP_TRONC%ROWTYPE; 
       LB_FOUND_SUSP       BOOLEAN:= FALSE;
       --Fin 22212 
       LN_VAL_FERIADO      NUMBER;        
       LV_VAL_FECHA        VARCHAR2(1);
       LN_VAL_CORP         NUMBER;
       LV_FLAG_VAL_FECHA   VARCHAR2(1);
       --FIN 13200
       LV_STATUS                    VARCHAR2(3);
       LV_MENSAJE                  VARCHAR2(300);
       LV_TABLA_DEFAULT   VARCHAR2(100);
       LN_COUNT_COMMIT    NUMBER;
       LB_EXISTE_BCK              BOOLEAN;
       LE_ERROR_MANUAL     EXCEPTION;
       LE_ERROR_VALIDATE   EXCEPTION;
       --------------------------------------------------------
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
        lv_mensaje_tec_scp     varchar2(4000);
        -------------------------------------------------------
       Le_Error_Trx                   EXCEPTION;
       LV_Tipo                           VARCHAR2(1);       
       LN_SALDO_MIN             NUMBER;
       LN_SALDO_MIN_POR   NUMBER;
       Ln_bank_id                      INTEGER;
       Ln_Status_A                     VARCHAR2(5);
       Lv_Status_B                     VARCHAR2(5);
       Lv_valid_suspe               VARCHAR2(5);       
       lb_Reactiva                      BOOLEAN;
       LV_USER_AXIS                 TTC_WORKFLOW_USER_S.US_NAME%TYPE;
       LN_Customer_id             NUMBER;              
       LN_Count_Name             NUMBER;       
       LV_Plan_Telefonia         VARCHAR2(10);
       LV_TABLA                       VARCHAR2(100);    
       LV_STATUS_OUT           VARCHAR2(1);
       LV_Sql_Planing               VARCHAR2(400);
       LV_Sql_PlanDet              VARCHAR2(400);
       LV_REMARK                    VARCHAR2(400);
       
       LV_BAND_DISTRI               VARCHAR2(5);--9833 Johanna Jimenez
       ----------------------------------------
       Lv_BanErrorPag               VARCHAR2(1);
       Ln_ExistCtaErrorPag          NUMBER;
       Lv_ErrorCtasPag              VARCHAR2(4000);
       ----------------------------------------
       
    BEGIN
        --INI 13200 MTST
        OPEN C_OBT_FLAG;
        FETCH C_OBT_FLAG
         INTO LV_FLAG_VAL_FECHA;
        CLOSE C_OBT_FLAG;
        IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
            OPEN C_VALIDA_FINDE(PD_FECHA);
            FETCH C_VALIDA_FINDE
             INTO LV_VAL_FECHA;
            CLOSE C_VALIDA_FINDE;
            
            OPEN C_VALIDA_FERIADO(PD_FECHA);
            FETCH C_VALIDA_FERIADO
             INTO LN_VAL_FERIADO;
            CLOSE C_VALIDA_FERIADO;
            
        END IF;
        --FIN 13200 MTST
        
        ---22212 
         OPEN C_OBT_FLAG_TRONCALES;
        FETCH C_OBT_FLAG_TRONCALES
         INTO LV_FLAG_TRONCALES;
        CLOSE C_OBT_FLAG_TRONCALES;
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='   -Inicio procedimiento Prc_Distribuir_Table_ALL';
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||'Grouptheard: ALL';
         scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);         
         ----------------------------------------------------------------------------   
       --Inicializa variables
       LN_COUNT_COMMIT:=0;
       LN_Count_Name:=0;       
       --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por la suma del campo valuetrx
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);          
       LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);          
       PV_react_Comment:=nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');
       
       --9833 Johanna Jimenez - ini
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,'TTC_FLAG_DISTRIBUYE_ALL');          
       LV_BAND_DISTRI:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');
       --9833 Johanna Jimenez - fin


       --1- Datos requeridos para el calculo del saldo
        LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
        LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
        LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
       
      --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
          OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
        FETCH C_GroupThread INTO PN_SumCountTread;
        CLOSE C_GroupThread;
        IF NVL(PN_SumCountTread,0)<=0  THEN
             PN_SumCountTread:=1;
        END IF;
                  
        --Obtiene la tabla default para asignar los valores
          OPEN C_Table_Default(PN_ID_PROCESS, 0,PV_FILTER);
        FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
        CLOSE C_Table_Default;
        IF LV_TABLA_DEFAULT IS NULL THEN
                PV_OUT_ERROR:='Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract0';
                RAISE LE_ERROR_VALIDATE;
        END IF;   

        --Cargar los datos de la configuración 
        i_ttc_bank_all_t:=Fct_ttc_bank_all_t; 
        i_Plannigstatus_In:=Fct_Plannigstatus_In_t;

        --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
       LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;

       ----------------------------------------------------------------------------------
       OPEN C_BAND_RELOJ_MEDIOS;
       FETCH C_BAND_RELOJ_MEDIOS INTO Lv_BanErrorPag;
       CLOSE C_BAND_RELOJ_MEDIOS;
       ----------------------------------------------------------------------------------
        --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
         OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
        FETCH C_NameTables  BULK COLLECT INTO  V_TableName,V_SumCountTread;
        CLOSE C_NameTables;
         --Filtro utilizado para la selección de los customers    
        FOR C_FILTER_ALL IN C_TTC_VIEW_FILTER_ALL(PV_FILTER) LOOP
                --[8693]JJI - Se comenta porque no es necesario
                --Suma los contracts en total para la carga de hoy
                /*OPEN C_Contracts(C_FILTER_ALL.Value_Group_Tread, PV_FILTER, PD_Fecha,LV_Tipo);
                FETCH C_Contracts INTO PN_SumCountContracts;
                CLOSE C_Contracts;*/
                  
                OPEN  C_Customer_x_Tipo(C_FILTER_ALL.Value_Group_Tread, PV_FILTER, PD_FECHA,LV_Tipo);
                LOOP
                FETCH C_Customer_x_Tipo  BULK COLLECT INTO  i_Customers_Count LIMIT PN_MAXTRX_EXE;                                                
                         FOR rc_cust IN (select * from table(cast( i_Customers_Count as ttc_Customers_Count_t_tbk)))  LOOP                                  
                             BEGIN                 
                                   LN_Customer_id:=rc_cust.Customer_id;
                                   --------------------------------------------
                                   IF NVL(Lv_BanErrorPag,'N') = 'S' THEN
                                      Ln_ExistCtaErrorPag := 0;
                                      Lv_ErrorCtasPag := NULL;
                                      SYSADM.PQ_RELOJ_MEDIOS_MAGNETICOS.P_VALIDAR_CTAS(PV_CUENTA => rc_cust.custcode,
                                                                                       PN_EXISTE => Ln_ExistCtaErrorPag,
                                                                                       PV_ERROR  => Lv_ErrorCtasPag);
                                      IF Ln_ExistCtaErrorPag = 1 THEN
                                         GOTO NOSUSPENDE; -- NO DEBE PROCESARSE
                                      END IF;
                                   END IF;
                                   --------------------------------------------
                                   --INI 13200 MTST 
                                   --valida si es fin de semana o feriado no debe suspender para clientes corp
                                   IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
                                       --valida si el cliente es corp
                                        OPEN C_VALIDA_CLI_CORP(LN_Customer_id);
                                        FETCH C_VALIDA_CLI_CORP
                                         INTO LN_VAL_CORP;
                                        CLOSE C_VALIDA_CLI_CORP;
                                        IF LN_VAL_CORP >0 THEN 
                                          --22212 - Troncales Moviles - Suspension de Servicio
                                           IF LV_FLAG_TRONCALES = 'S' THEN 
                                               IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                                                --no debe procesarse
                                                GOTO NOSUSPENDE; 
                                               ELSE 
                                                 BEGIN
                                                 OPEN C_CUENTAS_TRONCALE(rc_cust.custcode);
                                                  FETCH C_CUENTAS_TRONCALE
                                                   INTO LN_VAL_TRONCAL;
                                                  CLOSE C_CUENTAS_TRONCALE;
                                                  IF LN_VAL_TRONCAL >0 THEN 
                                                      OPEN C_VALIDA_SUSP_TRONC(rc_cust.custcode);
                                                        FETCH C_VALIDA_SUSP_TRONC
                                                         INTO LC_VALIDA_SUSP_TRONC;
                                                         LB_FOUND_SUSP:=C_VALIDA_SUSP_TRONC%FOUND;
                                                        CLOSE C_VALIDA_SUSP_TRONC;
                                                        IF LB_FOUND_SUSP THEN 
                                                           IF LC_VALIDA_SUSP_TRONC.ESTADO<> 'P' THEN
                                                              GOTO NOSUSPENDE;
                                                           END IF;
                                                        END IF; 
                                                  END IF;
                                                  EXCEPTION
                                                    WHEN OTHERS THEN
                                                      NULL;
                                                  END;   
                                               END IF;   
                                          
                                           
                                           ELSE 
                                           --FIN 22212
                                             IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                                                --no debe procesarse
                                                GOTO NOSUSPENDE; 
                                             END IF;
                                        END IF;   
                                        END IF;
                                    END IF;
                                    --FIN 13200
                                   --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
                                   --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
                                     BEGIN
                                            SELECT BANK_ID INTO Ln_bank_id   
                                               FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                                            WHERE wkf_cust.BANK_ID=rc_cust.bank_id;                                              
                                     EXCEPTION            
                                           WHEN OTHERS THEN 
                                              Ln_bank_id:=0;
                                     END;     
                                   --
                                   lb_Reactiva:=FALSE;
                                   IF nvl(Ln_bank_id,0)=0 THEN
                                             --***************************************************************
                                             --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
                                             --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
                                             Ln_Status_A:=NULL; Lv_Status_B:=NULL;Lv_valid_suspe:=NULL;
                                             BEGIN
                                                       SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B,Pla.st_valid_suspe   INTO Ln_Status_A,Lv_Status_B,Lv_valid_suspe 
                                                         FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                                       WHERE Pla.pk_id=GV_PK_ID
                                                             AND Pla.st_status_i= rc_cust.co_period;
                                             EXCEPTION            
                                                   WHEN OTHERS THEN 
                                                      Lv_valid_suspe:='N';
                                             END;     
                                             --
                                             --***************************************************************
                                            IF Lv_valid_suspe='Y' THEN  
                                                  --Verifica si la cuenta se encuentra con los pagos para ser reactivada
                                                  lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => rc_cust.custcode,
                                                                                                                                                                           PN_Customer_id =>rc_cust.Customer_id ,
                                                                                                                                                                           Pd_Date => PD_Fecha,                                                                                                                                                           
                                                                                                                                                                           PN_CO_IDTRAMITE => rc_cust.Co_idtramite,                                                                                                                                                           
                                                                                                                                                                           PV_STATUS_P => GV_STATUS_P,
                                                                                                                                                                           PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                                                                                                                                           PV_STATUS_OUT  => LV_STATUS_OUT,
                                                                                                                                                                           PV_Sql_Planing =>LV_Sql_Planing,
                                                                                                                                                                           PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                                                                                                                                           PV_REMARK =>  LV_REMARK,
                                                                                                                                                                           PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                                                                                                                                           PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                                                                                                                                           PV_USER_AXIS => LV_USER_AXIS, 
                                                                                                                                                                           PV_CO_STATUS_A =>  Ln_Status_A, 
                                                                                                                                                                           PV_CO_STATUS_B =>  Lv_Status_B,
                                                                                                                                                                           PV_PERIOD             =>   rc_cust.co_period,
                                                                                                                                                                           PV_REMARKTRX   =>  PV_react_Comment,                                                                                                                                                                     
                                                                                                                                                                           PV_OUT_ERROR =>PV_OUT_ERROR 
                                                                                                                                                                           );
                                            END IF;                                                                                                                         
                                   END IF;
                                   --9833 Johanna Jimenez - ini
                                   IF LV_BAND_DISTRI='S' THEN
                                     --SE LLAMA A LA FUNCION Fct_Save_ToWk_Contract_2 PARA QUE DISTRIBYA TAMBIEN LAS
                                     --CUENTAS QUE NO POSEEN DEUDA (CO_STATUSTRX=A) Y LAS CUENTAS QUE POSEEN LIMITE DE 
                                     --CREDITO (CO_STATUSTRX=L) A LAS TABLAS DE PROCESAMIENTO Y ASI EL PROCESO DISPATCH
                                     --INSERTE ESTAS TRANSACCIONES EN LA TABLA DE QC DE SUSPENSION MOVIL
                                     --Distribuye la carga  
                                     LN_Count_Name:=LN_Count_Name+1;           
                                     IF LN_Count_Name>PN_SumCountTread  THEN
                                        LN_Count_Name:=1;
                                     END IF;
                                     LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);         
                                     PV_OUT_ERROR:=Fct_Save_ToWk_Contract_2(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA,lb_Reactiva,LV_STATUS_OUT,LV_REMARK);
                                     IF PV_OUT_ERROR IS NOT NULL THEN     
                                        RAISE Le_Error_Trx;
                                     END IF;                            
                                   ELSE
                                   --9833 Johanna Jimenez - fin
                                    IF NOT lb_Reactiva THEN                                  
                                                  --Distribuye la carga  
                                                  LN_Count_Name:=LN_Count_Name+1;           
                                                  IF LN_Count_Name>PN_SumCountTread  THEN
                                                         LN_Count_Name:=1;
                                                  END IF;
                                                  LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);         
                                                  PV_OUT_ERROR:=Fct_Save_ToWk_Contract(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA);
                                                  IF PV_OUT_ERROR IS NOT NULL THEN     
                                                         RAISE Le_Error_Trx;
                                                  END IF;                                                  
                                     ELSE
                                                 IF LV_Sql_PlanDet IS NOT NULL THEN 
                                                        EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT,LV_REMARK,SYSDATE,rc_cust.Customer_id,GV_STATUS_P;
                                                 END IF;
                                     END IF;
                                    END IF;  
                                     LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;
                                     IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                                               COMMIT; 
                                              --SCP:AVANCE
                                                -----------------------------------------------------------------------
                                                -- SCP: Código generado automáticamente. Registro de avance de proceso
                                                -----------------------------------------------------------------------
                                                ln_registros_procesados_scp:=ln_registros_procesados_scp+PN_MAX_COMMIT;
                                                scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                                                ----------------------------------------------------------------------- 
                                                RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,PV_FILTER,C_FILTER_ALL.Value_Group_Tread,PN_SOSPID,LB_EXISTE_BCK);
                                                LN_COUNT_COMMIT:=0;
                                       END IF;
                                       IF LB_EXISTE_BCK=FALSE THEN
                                             EXIT;
                                       END IF;
                                       
                            EXCEPTION
                                  WHEN Le_Error_Trx THEN    
                                        --************
                                       --ROLLBACK;
                                        --***********
                                       LV_STATUS:='ERR';
                                       LV_MENSAJE :='Hilo:'||PN_HILO||' Grouptheard:'||C_FILTER_ALL.Value_Group_Tread||' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL- '  || ' ERROR:'||PV_OUT_ERROR||
                                                                   ' para la cuenta -> Customer_id:'||LN_Customer_id;
                                       --SCP:MENSAJE
                                       ----------------------------------------------------------------------
                                       -- SCP: Código generado automáticamente. Registro de mensaje de error
                                       ----------------------------------------------------------------------
                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                       lv_mensaje_apl_scp:='     Prc_Distribuir_Table_ALL-Error en la Distribucion en Customer_id';
                                       scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                             lv_mensaje_apl_scp,
                                                                                                             LV_MENSAJE,
                                                                                                             Null,
                                                                                                             2,Null,
                                                                                                             'Customer_id',
                                                                                                             'Co_id',
                                                                                                             LN_Customer_id,
                                                                                                             0,
                                                                                                             'N',ln_error_scp,lv_error_scp);                                   
                                       ----------------------------------------------------------------------------
                                       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                          PD_FECHA,
                                                                                                                                          SYSDATE,
                                                                                                                                          LV_STATUS,
                                                                                                                                          LV_MENSAJE);
                                  WHEN OTHERS THEN 
                                         --************
                                        --ROLLBACK;
                                         --***********
                                       LV_STATUS:='ERR';
                                       LV_MENSAJE :='Error General en Customer_id--> Hilo:'||PN_HILO||' Grouptheard:'||C_FILTER_ALL.Value_Group_Tread||' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                                                 ' para la cuenta -> Customer_id:'||LN_Customer_id;
                                       --SCP:MENSAJE
                                       ----------------------------------------------------------------------
                                       -- SCP: Código generado automáticamente. Registro de mensaje de error
                                       ----------------------------------------------------------------------
                                       ln_registros_error_scp:=ln_registros_error_scp+1;
                                       lv_mensaje_apl_scp:='     Prc_Distribuir_Table_ALL-Error en la Distribucion x Customer_id';
                                       scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                             lv_mensaje_apl_scp,
                                                                                                             LV_MENSAJE,
                                                                                                             Null,
                                                                                                             2,Null,
                                                                                                             'Customer_id',
                                                                                                             'Co_id',
                                                                                                             Ln_Customer_id,
                                                                                                             0,
                                                                                                             'N',ln_error_scp,lv_error_scp);                                   
                                       ----------------------------------------------------------------------------
                                       RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                          PD_FECHA,
                                                                                                                                          SYSDATE,
                                                                                                                                          LV_STATUS,
                                                                                                                                          LV_MENSAJE);

                            END;
                            <<NOSUSPENDE>>--13200 MTST
                            NULL;--13200 MTST
                         END LOOP; --FOR Customers
                  EXIT WHEN LB_EXISTE_BCK = FALSE;       
                  EXIT WHEN C_Customer_x_Tipo%notfound; 
                END LOOP; --LOOP  Customers
                CLOSE C_Customer_x_Tipo;
                --***********--  
                COMMIT;       
                --***********--
                IF LB_EXISTE_BCK=FALSE THEN
                    EXIT;
               END IF;
 
          END LOOP; --LOOP  Filter_ALL
          PV_OUT_ERROR:=NULL;
          IF LB_EXISTE_BCK=FALSE THEN
              RAISE LE_ERROR_MANUAL;
         END IF;

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:='   -Fin Prc_Distribuir_Table_ALL';
          ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                0,Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------      
       EXCEPTION                    
             WHEN NO_DATA_FOUND THEN
                  PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL >>ADVERTENCIA No existen datos para la consulta:';
                  If ln_registros_error_scp = 0 Then
                    ln_registros_error_scp:=-1;
                 End If;
                 scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

              WHEN LE_ERROR_VALIDATE THEN
                   ROLLBACK;
                   PV_OUT_ERROR:='Prc_Distribuir_Table_ALL-'||PV_OUT_ERROR;
                   If ln_registros_error_scp = 0 Then
                   ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       
              WHEN LE_ERROR_MANUAL THEN                   
                   PV_OUT_ERROR:='ABORTADO';
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                   End If;
                   scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

               WHEN OTHERS THEN
                   ROLLBACK;
                   LV_MENSAJE := 'Prc_Distribuir_Table_ALL- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
                   PV_OUT_ERROR:=LV_MENSAJE;
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                   
    END Prc_Distribuir_Table_ALL;

/**************************************************************************************
**************************************************************************************/ 

   PROCEDURE Prc_Distribuir_Table_ALL_Dth(PN_ID_PROCESS      IN NUMBER,
                                       PV_FILTER          IN VARCHAR2,
                                       PN_HILO            IN NUMBER,
                                       PD_FECHA           DATE,
                                       PN_MAXTRX_EXE      NUMBER,
                                       PN_MAX_COMMIT      NUMBER,
                                       pn_id_bitacora_scp NUMBER,
                                       PN_SOSPID          IN NUMBER,
                                       PV_OUT_ERROR       OUT VARCHAR2) IS
  
       /*INI 13200 MTST*/
       --* VALIDA SI LA FECHA ES FIN DE SEMANA
        CURSOR C_VALIDA_FINDE (CD_FECHA DATE) IS
          SELECT DECODE(TO_CHAR(CD_FECHA,'DY',
                        'NLS_DATE_LANGUAGE=ENGLISH'),
                        'FRI','S',
                        'SAT','S',
                        'SUN','S',
                        'N') APLICA
          FROM DUAL;
       CURSOR C_VALIDA_CLI_CORP(CN_CUSTOMER NUMBER)IS
        SELECT COUNT(*)
          FROM PORTA.CL_CONTRATOS@AXIS        C,
               PORTA.CL_PERSONAS_EMPRESA@AXIS A,
               PORTA.CL_CLASES_CLIENTES@AXIS  X,
               CUSTOMER_ALL                   B
         WHERE B.CUSTOMER_ID = CN_CUSTOMER
           AND A.ID_PERSONA = C.ID_PERSONA
           AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
           AND C.CODIGO_DOC = B.CUSTCODE
           AND X.ID_CLASE_CLIENTE IN
               (SELECT T.ID_CLASE_CLIENTE
                  FROM PORTA.CL_CLASES_CLIENTES@AXIS T
                 WHERE T.DESCRIPCION LIKE '%INS%'
                    OR T.DESCRIPCION LIKE '%VIP%'
                    OR T.DESCRIPCION LIKE '%PRE%');
         
       CURSOR C_OBT_FLAG IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 13200
           AND G.ID_PARAMETRO = 'FLG_VAL_FIN_SEMANA';
           
       CURSOR C_VALIDA_FERIADO (CD_FECHA DATE) IS
        SELECT COUNT(*)
          FROM PORTA.RC_FERIADOS@AXIS D
         WHERE TRUNC(CD_FECHA) BETWEEN TRUNC(D.CF_FECH_INICIO) AND TRUNC(D.CF_FECH_FIN)
           AND D.CF_ESTADO = 'A';
       LN_VAL_FERIADO      NUMBER;        
       LV_VAL_FECHA        VARCHAR2(1);
       LN_VAL_CORP         NUMBER;
       LV_FLAG_VAL_FECHA   VARCHAR2(1);
       --FIN 13200
  
  CURSOR C_BAND_RELOJ_MEDIOS IS
  SELECT G.VALOR
    FROM GV_PARAMETROS G
   WHERE G.ID_TIPO_PARAMETRO = 22311
     AND G.ID_PARAMETRO = 'BAN_RC_MEDIOS_SUSP';
  
  LV_STATUS        VARCHAR2(3);
  LV_MENSAJE       VARCHAR2(300);
  LV_TABLA_DEFAULT VARCHAR2(100);
  LN_COUNT_COMMIT  NUMBER;
  LB_EXISTE_BCK    BOOLEAN;
  LE_ERROR_MANUAL EXCEPTION;
  LE_ERROR_VALIDATE EXCEPTION;
  --------------------------------------------------------
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  -------------------------------------------------------
  Le_Error_Trx EXCEPTION;
  LV_Tipo           VARCHAR2(1);
  LN_SALDO_MIN      NUMBER;
  LN_SALDO_MIN_POR  NUMBER;
  Ln_bank_id        INTEGER;
  Ln_Status_A       VARCHAR2(5);
  Lv_Status_B       VARCHAR2(5);
  Lv_valid_suspe    VARCHAR2(5);
  lb_Reactiva       BOOLEAN;
  LV_USER_AXIS      TTC_WORKFLOW_USER_S.US_NAME%TYPE;
  LN_Customer_id    NUMBER;
  LN_Count_Name     NUMBER;
  LV_Plan_Telefonia VARCHAR2(10);
  LV_TABLA          VARCHAR2(100);
  LV_STATUS_OUT     VARCHAR2(1);
  LV_Sql_Planing    VARCHAR2(400);
  LV_Sql_PlanDet    VARCHAR2(400);
  LV_REMARK         VARCHAR2(400);
  ----------------------------------------
  Lv_BanErrorPag               VARCHAR2(1);
  Ln_ExistCtaErrorPag          NUMBER;
  Lv_ErrorCtasPag              VARCHAR2(4000);
  ----------------------------------------

BEGIN
  
  --INI 13200 MTST
      OPEN C_OBT_FLAG;
      FETCH C_OBT_FLAG
       INTO LV_FLAG_VAL_FECHA;
      CLOSE C_OBT_FLAG;
      IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
          OPEN C_VALIDA_FINDE(PD_FECHA);
          FETCH C_VALIDA_FINDE
           INTO LV_VAL_FECHA;
          CLOSE C_VALIDA_FINDE;
              
          OPEN C_VALIDA_FERIADO(PD_FECHA);
          FETCH C_VALIDA_FERIADO
           INTO LN_VAL_FERIADO;
          CLOSE C_VALIDA_FERIADO;
              
      END IF;
   --FIN 13200 MTST
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := '   -Inicio procedimiento Prc_Distribuir_Table_ALL_DTH';
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||'Grouptheard: ALL';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------   
  --Inicializa variables
  LN_COUNT_COMMIT := 0;
  LN_Count_Name   := 0;
  --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por la suma del campo valuetrx
  i_Rule_Values_t := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process, GN_DES_FLAG_ValueTrx);
  LV_Tipo         := nvl(i_Rule_Values_t.Value_Return_ShortString, 'N');

  i_Rule_Values_t  := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);
  PV_react_Comment := nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');

  --1- Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;
  LV_USER_AXIS     := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');

  --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
  OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
  FETCH C_GroupThread INTO PN_SumCountTread;
  CLOSE C_GroupThread;
  IF NVL(PN_SumCountTread, 0) <= 0 THEN
    PN_SumCountTread := 1;
  END IF;

  --Obtiene la tabla default para asignar los valores
  OPEN C_Table_Default(PN_ID_PROCESS, 0, PV_FILTER);
  FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
  CLOSE C_Table_Default;
  IF LV_TABLA_DEFAULT IS NULL THEN
    PV_OUT_ERROR := 'Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract_Dth0';
    RAISE LE_ERROR_VALIDATE;
  END IF;

  --Cargar los datos de la configuración 
  i_ttc_bank_all_t   := Fct_ttc_bank_all_t;
  i_Plannigstatus_In := Fct_Plannigstatus_In_t;

  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia := RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;

  --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
  OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
  FETCH C_NameTables BULK COLLECT INTO V_TableName, V_SumCountTread;
  CLOSE C_NameTables;
  ----------------------------------------------------------------------------------
  OPEN C_BAND_RELOJ_MEDIOS;
  FETCH C_BAND_RELOJ_MEDIOS INTO Lv_BanErrorPag;
  CLOSE C_BAND_RELOJ_MEDIOS;
  ----------------------------------------------------------------------------------
  --Filtro utilizado para la selección de los customers    
  FOR C_FILTER_ALL IN C_TTC_VIEW_FILTER_ALL(PV_FILTER) LOOP
  
    OPEN C_CustomerDth_x_Tipo(C_FILTER_ALL.Value_Group_Tread,
                              PV_FILTER,
                              PD_FECHA,
                              LV_Tipo);
    LOOP
      FETCH C_CustomerDth_x_Tipo BULK COLLECT INTO i_Customers_Count LIMIT PN_MAXTRX_EXE;
      FOR rc_cust IN (select * from table(cast(i_Customers_Count as ttc_Customers_Count_t_tbk))) LOOP
        BEGIN
          LN_Customer_id := rc_cust.Customer_id;
          --------------------------------------------
          IF NVL(Lv_BanErrorPag,'N') = 'S' THEN
             Ln_ExistCtaErrorPag := 0;
             Lv_ErrorCtasPag := NULL;
             SYSADM.PQ_RELOJ_MEDIOS_MAGNETICOS.P_VALIDAR_CTAS(PV_CUENTA => rc_cust.custcode,
                                                              PN_EXISTE => Ln_ExistCtaErrorPag,
                                                              PV_ERROR  => Lv_ErrorCtasPag);
             IF Ln_ExistCtaErrorPag = 1 THEN
                GOTO NOSUSPENDE; -- NO DEBE PROCESARSE
             END IF;
          END IF;
          --------------------------------------------
          --INI 13200 MTST 
           --valida si es fin de semana o feriado no debe suspender para clientes corp
           IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
               --valida si el cliente es corp
                OPEN C_VALIDA_CLI_CORP(LN_Customer_id);
                FETCH C_VALIDA_CLI_CORP
                 INTO LN_VAL_CORP;
                CLOSE C_VALIDA_CLI_CORP;
                IF LN_VAL_CORP >0 THEN 
                   IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                      --no debe procesarse
                      GOTO NOSUSPENDE; 
                   END IF;
                END IF;
            END IF;
            --FIN 13200
          --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
          --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
          BEGIN
            SELECT BANK_ID INTO Ln_bank_id
              FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t)) wkf_cust
             WHERE wkf_cust.BANK_ID = rc_cust.bank_id;
          EXCEPTION
            WHEN OTHERS THEN
              Ln_bank_id := 0;
          END;
          --
          lb_Reactiva := FALSE;
          IF nvl(Ln_bank_id, 0) = 0 THEN
            --***************************************************************
            --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
            --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
            Ln_Status_A    := NULL;
            Lv_Status_B    := NULL;
            Lv_valid_suspe := NULL;
            BEGIN
              SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B, Pla.st_valid_suspe INTO Ln_Status_A, Lv_Status_B, Lv_valid_suspe
                FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
               WHERE Pla.pk_id = GV_PK_ID
                 AND Pla.st_status_i = rc_cust.co_period;
            EXCEPTION
              WHEN OTHERS THEN
                Lv_valid_suspe := 'N';
            END;
            --
            --***************************************************************
            IF Lv_valid_suspe = 'Y' THEN
              --Verifica si la cuenta se encuentra con los pagos para ser reactivada
              lb_Reactiva := RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode       => rc_cust.custcode,
                                                                         PN_Customer_id    => rc_cust.Customer_id,
                                                                         Pd_Date           => PD_Fecha,
                                                                         PN_CO_IDTRAMITE   => rc_cust.Co_idtramite,
                                                                         PV_STATUS_P       => GV_STATUS_P,
                                                                         PV_Plan_Telefonia => LV_Plan_Telefonia,
                                                                         PV_STATUS_OUT     => LV_STATUS_OUT,
                                                                         PV_Sql_Planing    => LV_Sql_Planing,
                                                                         PV_Sql_PlanDet    => LV_Sql_PlanDet,
                                                                         PV_REMARK         => LV_REMARK,
                                                                         PN_SALDO_MIN      => LN_SALDO_MIN,
                                                                         PN_SALDO_MIN_POR  => LN_SALDO_MIN_POR,
                                                                         PV_USER_AXIS      => LV_USER_AXIS,
                                                                         PV_CO_STATUS_A    => Ln_Status_A,
                                                                         PV_CO_STATUS_B    => Lv_Status_B,
                                                                         PV_PERIOD         => rc_cust.co_period,
                                                                         PV_REMARKTRX      => PV_react_Comment,
                                                                         PV_OUT_ERROR      => PV_OUT_ERROR);
            END IF;
          END IF;
          IF NOT lb_Reactiva THEN
            --Distribuye la carga  
            LN_Count_Name := LN_Count_Name + 1;
            IF LN_Count_Name > PN_SumCountTread THEN
              LN_Count_Name := 1;
            END IF;
            
            LV_TABLA     := nvl(V_TableName(LN_Count_Name), LV_TABLA_DEFAULT);
            PV_OUT_ERROR := Fct_Save_ToWk_Contract_Dth(LN_Customer_id,
                                                       LV_TABLA,
                                                       nvl(Lv_valid_suspe,'N'),
                                                       PD_FECHA);
            IF PV_OUT_ERROR IS NOT NULL THEN
              RAISE Le_Error_Trx;
            END IF;
          ELSE
            IF LV_Sql_PlanDet IS NOT NULL THEN
              EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT, LV_REMARK, SYSDATE, rc_cust.Customer_id, GV_STATUS_P;
            END IF;
          END IF;
          LN_COUNT_COMMIT := LN_COUNT_COMMIT + 1;
          IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
            COMMIT;
            --SCP:AVANCE
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            ln_registros_procesados_scp := ln_registros_procesados_scp + PN_MAX_COMMIT;
            scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                  ln_registros_procesados_scp,
                                                  null,
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------- 
            RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                                GV_DESSHORT_DTH,
                                                                PV_FILTER,
                                                                C_FILTER_ALL.Value_Group_Tread,
                                                                PN_SOSPID,
                                                                LB_EXISTE_BCK);
            LN_COUNT_COMMIT := 0;
          END IF;
          IF LB_EXISTE_BCK = FALSE THEN
            EXIT;
          END IF;
        
        EXCEPTION
          WHEN Le_Error_Trx THEN
            --************
            --ROLLBACK;
            --***********
            LV_STATUS  := 'ERR';
            LV_MENSAJE := 'Hilo:' || PN_HILO || ' Grouptheard:' || C_FILTER_ALL.Value_Group_Tread ||
                          ' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL_Dth- ' ||' ERROR:' || PV_OUT_ERROR ||
                          ' para la cuenta -> Customer_id:' || LN_Customer_id;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '     Prc_Distribuir_Table_ALL_Dth-Error en la Distribucion en Customer_id';
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  LV_MENSAJE,
                                                  Null,
                                                  2,
                                                  Null,
                                                  'Customer_id',
                                                  'Co_id',
                                                  LN_Customer_id,
                                                  0,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
            RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                         PD_FECHA,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
          WHEN OTHERS THEN
            --************
            --ROLLBACK;
            --***********
            LV_STATUS  := 'ERR';
            LV_MENSAJE := 'Error General en Customer_id--> Hilo:' ||PN_HILO || ' Grouptheard:' ||
                          C_FILTER_ALL.Value_Group_Tread || ' >> RC_Trx_TimeToCash_Send.Prc_Distribuir_Table_ALL_Dth- ' ||
                          ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 100) ||' para la cuenta -> Customer_id:' ||
                          LN_Customer_id;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp := ln_registros_error_scp + 1;
            lv_mensaje_apl_scp     := '     Prc_Distribuir_Table_ALL_Dth-Error en la Distribucion x Customer_id';
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  LV_MENSAJE,
                                                  Null,
                                                  2,
                                                  Null,
                                                  'Customer_id',
                                                  'Co_id',
                                                  Ln_Customer_id,
                                                  0,
                                                  'N',
                                                  ln_error_scp,
                                                  lv_error_scp);
            ----------------------------------------------------------------------------
            RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                         PD_FECHA,
                                                         SYSDATE,
                                                         LV_STATUS,
                                                         LV_MENSAJE);
          
        END;
        <<NOSUSPENDE>>--13200 MTST
        NULL;--13200 MTST
      END LOOP; --FOR Customers
      EXIT WHEN LB_EXISTE_BCK = FALSE;
      EXIT WHEN C_CustomerDth_x_Tipo%notfound;
    END LOOP; --LOOP  Customers
    CLOSE C_CustomerDth_x_Tipo;
    --***********--  
    COMMIT;
    --***********--
    IF LB_EXISTE_BCK = FALSE THEN
      EXIT;
    END IF;
  
  END LOOP; --LOOP  Filter_ALL
  PV_OUT_ERROR := NULL;
  IF LB_EXISTE_BCK = FALSE THEN
    RAISE LE_ERROR_MANUAL;
  END IF;

  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp          := '   -Fin Prc_Distribuir_Table_ALL_Dth';
  ln_registros_procesados_scp := ln_registros_procesados_scp + LN_COUNT_COMMIT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'S',
                                        ln_error_scp,
                                        lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------      
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL_Dth >>ADVERTENCIA No existen datos para la consulta:';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_VALIDATE THEN
    ROLLBACK;
    PV_OUT_ERROR := 'Prc_Distribuir_Table_ALL_Dth-' || PV_OUT_ERROR;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_MANUAL THEN
    PV_OUT_ERROR := 'ABORTADO';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN OTHERS THEN
    ROLLBACK;
    LV_MENSAJE   := 'Prc_Distribuir_Table_ALL_Dth- ' || ' ERROR:' ||SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 100);
    PV_OUT_ERROR := LV_MENSAJE;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
END Prc_Distribuir_Table_ALL_Dth;

   /**************************************************************************************
**************************************************************************************/ 
   PROCEDURE Prc_Distribuir_Table_Group(PN_ID_PROCESS        IN NUMBER, 
                                                                                  PV_FILTER                 IN VARCHAR2,
                                                                                  PN_HILO                     IN NUMBER,
                                                                                  PN_GROUPTREAD    IN NUMBER,
                                                                                  PD_FECHA                   DATE,
                                                                                  PN_MAXTRX_EXE      NUMBER,
                                                                                  PN_MAX_COMMIT    NUMBER,   
                                                                                  pn_id_bitacora_scp   NUMBER,  
                                                                                  PN_SOSPID        IN NUMBER,                                                                                                                                              
                                                                                  PV_OUT_ERROR        OUT VARCHAR2
                                                                                  )   IS
       
       /*INI 13200 MTST*/
       --* VALIDA SI LA FECHA ES FIN DE SEMANA
        CURSOR C_VALIDA_FINDE (CD_FECHA DATE) IS
          SELECT DECODE(TO_CHAR(CD_FECHA,'DY',
                        'NLS_DATE_LANGUAGE=ENGLISH'),
                        'FRI','S',
                        'SAT','S',
                        'SUN','S',
                        'N') APLICA
          FROM DUAL;
       CURSOR C_VALIDA_CLI_CORP(CN_CUSTOMER NUMBER)IS
        SELECT COUNT(*)
          FROM PORTA.CL_CONTRATOS@AXIS        C,
               PORTA.CL_PERSONAS_EMPRESA@AXIS A,
               PORTA.CL_CLASES_CLIENTES@AXIS  X,
               CUSTOMER_ALL                   B
         WHERE B.CUSTOMER_ID = CN_CUSTOMER
           AND A.ID_PERSONA = C.ID_PERSONA
           AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
           AND C.CODIGO_DOC = B.CUSTCODE
           AND X.ID_CLASE_CLIENTE IN('17','23','24','26','27','28')
              /* (SELECT T.ID_CLASE_CLIENTE
                  FROM PORTA.CL_CLASES_CLIENTES@AXIS T
                 WHERE T.DESCRIPCION LIKE '%INS%'
                    OR T.DESCRIPCION LIKE '%VIP%'
                    OR T.DESCRIPCION LIKE '%PRE%')*/;
         
       CURSOR C_OBT_FLAG IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 13200
           AND G.ID_PARAMETRO = 'FLG_VAL_FIN_SEMANA';
           
       CURSOR C_VALIDA_FERIADO (CD_FECHA DATE) IS
        SELECT COUNT(*)
          FROM PORTA.RC_FERIADOS@AXIS D
         WHERE TRUNC(CD_FECHA) BETWEEN TRUNC(D.CF_FECH_INICIO) AND TRUNC(D.CF_FECH_FIN)
           AND D.CF_ESTADO = 'A';
       LN_VAL_FERIADO      NUMBER;        
       LV_VAL_FECHA        VARCHAR2(1);
       LN_VAL_CORP         NUMBER;
       LV_FLAG_VAL_FECHA   VARCHAR2(1);
       --FIN 13200
        -- 22212 - Troncales Moviles - Suspension de Servicio    
        CURSOR C_OBT_FLAG_TRONCALES IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 22212
           AND G.ID_PARAMETRO = 'FLAG_VAL_TRONCALES';
           
         CURSOR C_CUENTAS_TRONCALE(CV_CUENTA VARCHAR2)IS 
         SELECT COUNT(*) 
         FROM PORTA.CL_CONTRATOS@axis C ,
          PORTA.CL_SERVICIOS_CONTRATADOS@axis A , 
          PORTA.GE_DETALLES_PLANES@axis G
          WHERE C.CODIGO_DOC = CV_CUENTA
          AND C.ID_CONTRATO = A.ID_CONTRATO
          AND A.ID_DETALLE_PLAN = G.ID_DETALLE_PLAN
          AND G.ID_TIPO_PLAN= '032';
          
         CURSOR C_VALIDA_SUSP_TRONC(CV_CUENTA VARCHAR2)IS 
         select C.ESTADO, C.CUENTA
          from porta.rc_servicios_troncales@AXIS c
         where c.cuenta =CV_CUENTA
         order by c.fecha_registro desc;
         CURSOR C_OBTIENE_CUNETA( CN_CUSTOMER NUMBER) IS
          SELECT CUSTCODE FROM CUSTOMER_ALL C WHERE C.CUSTOMER_ID= CN_CUSTOMER;
               
       LN_VAL_TRONCAL      NUMBER;
       LV_FLAG_TRONCALES   VARCHAR2(1):= 'N';
       LC_VALIDA_SUSP_TRONC C_VALIDA_SUSP_TRONC%ROWTYPE; 
       LB_FOUND_SUSP       BOOLEAN:= FALSE;
       LV_CUENTA           VARCHAR2(20);
       --Fin 22212 
       LV_STATUS                    VARCHAR2(3);
       LV_MENSAJE                  VARCHAR2(300);
       LV_TABLA_DEFAULT   VARCHAR2(100);
       LN_COUNT_COMMIT    NUMBER;
       LB_EXISTE_BCK              BOOLEAN;
       LE_ERROR_MANUAL     EXCEPTION;
       LE_ERROR_VALIDATE   EXCEPTION;
       --------------------------------------------------------
        ln_error_scp number:=0;
        lv_error_scp varchar2(500);
        ln_registros_procesados_scp number:=0;
        ln_registros_error_scp number:=0;
        lv_mensaje_apl_scp     varchar2(4000);
        lv_mensaje_tec_scp     varchar2(4000);
        -------------------------------------------------------
       Le_Error_Trx                   EXCEPTION;
       LV_Tipo                           VARCHAR2(1);
       LN_SALDO_MIN             NUMBER;
       LN_SALDO_MIN_POR   NUMBER;
       Ln_bank_id                      INTEGER;
       Ln_Status_A                     VARCHAR2(5);
       Lv_Status_B                     VARCHAR2(5);   
       Lv_valid_suspe               VARCHAR2(5);
       lb_Reactiva                      BOOLEAN;
       LV_USER_AXIS                 TTC_WORKFLOW_USER_S.US_NAME%TYPE;
       LN_Customer_id             NUMBER;              
       LN_Count_Name             NUMBER;       
       LV_Plan_Telefonia         VARCHAR2(10);
       LV_TABLA                       VARCHAR2(100);    
       LV_STATUS_OUT           VARCHAR2(1);
       LV_Sql_Planing               VARCHAR2(400);
       LV_Sql_PlanDet              VARCHAR2(400);
       LV_REMARK                    VARCHAR2(400);
       
       LV_BAND_DISTRI               VARCHAR2(5);--9833 Johanna Jimenez
       --INICIO 10995 IRO_JRO
       CURSOR C_BANDERA_HILO IS
       SELECT A.VALOR FROM GV_PARAMETROS A WHERE A.ID_TIPO_PARAMETRO=10995 AND A.ID_PARAMETRO='DISPATCH_NUEVO_ESQUEMA';
       LC_BANDERA_HILO C_BANDERA_HILO%ROWTYPE;
       --FIN 10995 IRO_JRO

      CURSOR C_FORMA_PAGO_PRIN IS 
       SELECT NVL(VALOR,'|94|121|78|1|97|100|99|')
         FROM GV_PARAMETROS T
        WHERE ID_TIPO_PARAMETRO = 10995
          AND ID_PARAMETRO = 'PRIORIZACION_CLIENTE_SUSP';

      CURSOR C_DIA_SUSP IS 
       SELECT NVL(TO_NUMBER(VALOR),2)
         FROM GV_PARAMETROS T
        WHERE ID_TIPO_PARAMETRO = 10995
          AND ID_PARAMETRO = 'DIAS_SUSPENDIDOS';

       CURSOR C_BAND_RELOJ_MEDIOS IS
       SELECT G.VALOR
         FROM GV_PARAMETROS G
        WHERE G.ID_TIPO_PARAMETRO = 22311
          AND G.ID_PARAMETRO = 'BAN_RC_MEDIOS_SUSP';
       
       LV_FORMA_PAGO_PRIN VARCHAR2(2000):='|94|121|78|1|97|100|99|';
       LN_CATEGORIA       NUMBER:=3;
       LN_DIAS_SUSP       NUMBER:=2;
       LD_FECHA_SUSP      DATE:=SYSDATE;
       ----------------------------------------
       Lv_BanErrorPag               VARCHAR2(1);
       Ln_ExistCtaErrorPag          NUMBER;
       Lv_ErrorCtasPag              VARCHAR2(4000);
       ----------------------------------------

    BEGIN
       
      --INI 13200 MTST
      OPEN C_OBT_FLAG;
      FETCH C_OBT_FLAG
       INTO LV_FLAG_VAL_FECHA;
      CLOSE C_OBT_FLAG;
      IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
          OPEN C_VALIDA_FINDE(PD_FECHA);
          FETCH C_VALIDA_FINDE
           INTO LV_VAL_FECHA;
          CLOSE C_VALIDA_FINDE;
              
          OPEN C_VALIDA_FERIADO(PD_FECHA);
          FETCH C_VALIDA_FERIADO
           INTO LN_VAL_FERIADO;
          CLOSE C_VALIDA_FERIADO;
              
      END IF;
      --FIN 13200 MTST
      
       ---22212 
         OPEN C_OBT_FLAG_TRONCALES;
        FETCH C_OBT_FLAG_TRONCALES
         INTO LV_FLAG_TRONCALES;
        CLOSE C_OBT_FLAG_TRONCALES;
        
       --INICIO 10995 IRO_JRO
         OPEN C_BANDERA_HILO;
         FETCH C_BANDERA_HILO INTO LC_BANDERA_HILO;
         CLOSE C_BANDERA_HILO;
       --FIN 10995 IRO_JRO

       OPEN C_FORMA_PAGO_PRIN;
       FETCH C_FORMA_PAGO_PRIN INTO LV_FORMA_PAGO_PRIN;
       CLOSE C_FORMA_PAGO_PRIN;

       OPEN C_DIA_SUSP;
       FETCH C_DIA_SUSP INTO LN_DIAS_SUSP;
       CLOSE C_DIA_SUSP;
       
       LD_FECHA_SUSP:=TRUNC(LD_FECHA_SUSP-LN_DIAS_SUSP);       
         ----------------------------------------------------------------------
         -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         lv_mensaje_apl_scp:='   -Inicio procedimiento Prc_Distribuir_Table_Group';
         lv_mensaje_tec_scp:='Hilo: '||PN_HILO||' Filtro: '||PV_FILTER||' Grouptheard: '||PN_GROUPTREAD;
         scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                               lv_mensaje_apl_scp,
                                               lv_mensaje_tec_scp,
                                               Null,
                                               0,
                                               Gv_unidad_registro_scp,
                                               Null,
                                               Null,
                                               null,null,
                                               'N',ln_error_scp,lv_error_scp);         
         ----------------------------------------------------------------------------   
       --Inicializa variables
       LN_COUNT_COMMIT:=0;
       LN_Count_Name:=0;       
       --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por ña suma del campo valuetrx
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);          
       LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);          
       PV_react_Comment:=nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');
       
       --9833 Johanna Jimenez - ini
       i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,'TTC_FLAG_DISTRIBUYE_ALL');          
       LV_BAND_DISTRI:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');
       --9833 Johanna Jimenez - fin

       --1- Datos requeridos para el calculo del saldo
        LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
        LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
        LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
       
      --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
          OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
        FETCH C_GroupThread INTO PN_SumCountTread;
        CLOSE C_GroupThread;
        IF NVL(PN_SumCountTread,0)<=0  THEN
             PN_SumCountTread:=1;
        END IF;
                  
        --Obtiene la tabla default para asignar los valores
          OPEN C_Table_Default(PN_ID_PROCESS, 0,PV_FILTER);
        FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
        CLOSE C_Table_Default;
        IF LV_TABLA_DEFAULT IS NULL THEN
                PV_OUT_ERROR:='Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract0';
                RAISE LE_ERROR_VALIDATE;
        END IF;   

        --Cargar los datos de la configuración 
        i_ttc_bank_all_t:=Fct_ttc_bank_all_t;
        i_Plannigstatus_In:=Fct_Plannigstatus_In_t;
        
        --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
       LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;
       
       --[8693]JJI - Se comenta porque no se usa
       --Suma los contracts en total para la carga de hoy
         /* OPEN C_Contracts(PN_GROUPTREAD, PV_FILTER, PD_Fecha,LV_Tipo);
        FETCH C_Contracts INTO PN_SumCountContracts;
        CLOSE C_Contracts;*/
        ----------------------------------------------------------------------------------
        OPEN C_BAND_RELOJ_MEDIOS;
        FETCH C_BAND_RELOJ_MEDIOS INTO Lv_BanErrorPag;
        CLOSE C_BAND_RELOJ_MEDIOS;
        ----------------------------------------------------------------------------------
        --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
         OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
        FETCH C_NameTables  BULK COLLECT INTO  V_TableName,V_SumCountTread;
        CLOSE C_NameTables;
        LV_CUENTA := NULL;
        OPEN  C_Customer_x_Tipo2(PV_FILTER, PD_FECHA, LV_Tipo, PN_HILO);
        LOOP
        FETCH C_Customer_x_Tipo2 BULK COLLECT INTO  i_Customers_Count2 LIMIT PN_MAXTRX_EXE;                                                
                 FOR rc_cust IN (select * from table(cast( i_Customers_Count2 as ttc_Customers_Count_t_tbk2)))  LOOP                                  
                     BEGIN                 
                           LN_Customer_id:=rc_cust.Customer_id;
                           --------------------------------------------
                           IF NVL(Lv_BanErrorPag,'N') = 'S' THEN
                              Ln_ExistCtaErrorPag := 0;
                              Lv_ErrorCtasPag := NULL;
                              
                              OPEN C_OBTIENE_CUNETA(LN_Customer_id);
                              FETCH C_OBTIENE_CUNETA INTO LV_CUENTA;
                              CLOSE C_OBTIENE_CUNETA;
                              
                              SYSADM.PQ_RELOJ_MEDIOS_MAGNETICOS.P_VALIDAR_CTAS(PV_CUENTA => LV_CUENTA,
                                                                               PN_EXISTE => Ln_ExistCtaErrorPag,
                                                                               PV_ERROR  => Lv_ErrorCtasPag);
                              IF Ln_ExistCtaErrorPag = 1 THEN
                                 GOTO NOSUSPENDE; -- NO DEBE PROCESARSE
                              END IF;
                           END IF;
                           --------------------------------------------
                           --INI 13200 MTST 
                           --valida si es fin de semana o feriado no debe suspender para clientes corp
                           IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
                               --valida si el cliente es corp
                                OPEN C_VALIDA_CLI_CORP(LN_Customer_id);
                                FETCH C_VALIDA_CLI_CORP
                                 INTO LN_VAL_CORP;
                                CLOSE C_VALIDA_CLI_CORP;
                                IF LN_VAL_CORP >0 THEN 
                                  
                                   --22212 - Troncales Moviles - Suspension de Servicio
                                   IF LV_FLAG_TRONCALES = 'S' THEN 
                                       IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                                        --no debe procesarse
                                        GOTO NOSUSPENDE; 
                                       ELSE 
                                         BEGIN
                                         OPEN C_OBTIENE_CUNETA(LN_Customer_id);
                                          FETCH C_OBTIENE_CUNETA
                                           INTO LV_CUENTA;
                                          CLOSE C_OBTIENE_CUNETA;
                                         IF LV_CUENTA IS NOT NULL THEN  
                                             OPEN C_CUENTAS_TRONCALE(LV_CUENTA);
                                              FETCH C_CUENTAS_TRONCALE
                                               INTO LN_VAL_TRONCAL;
                                              CLOSE C_CUENTAS_TRONCALE;
                                              IF LN_VAL_TRONCAL >0 THEN 
                                                  OPEN C_VALIDA_SUSP_TRONC(LV_CUENTA);
                                                    FETCH C_VALIDA_SUSP_TRONC
                                                     INTO LC_VALIDA_SUSP_TRONC;
                                                     LB_FOUND_SUSP:=C_VALIDA_SUSP_TRONC%FOUND;
                                                    CLOSE C_VALIDA_SUSP_TRONC;
                                                    IF LB_FOUND_SUSP THEN 
                                                       IF LC_VALIDA_SUSP_TRONC.ESTADO<> 'P' THEN
                                                          GOTO NOSUSPENDE;
                                                       END IF;
                                                    END IF; 
                                              END IF;
                                         END IF; 
                                         EXCEPTION 
                                           WHEN OTHERS THEN 
                                             NULL;
                                         END;  
                                       END IF;   
                                          
                                           
                                   ELSE 
                                   --FIN 22212
                                       IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                                          --no debe procesarse
                                          GOTO NOSUSPENDE; 
                                       END IF;
                                   END IF;
                                END IF;
                            END IF;
                            --FIN 13200
                           --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
                           --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
                             BEGIN
                                    SELECT BANK_ID INTO Ln_bank_id
                                       FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                                    WHERE wkf_cust.BANK_ID=rc_cust.bank_id;                                              
                             EXCEPTION            
                                   WHEN OTHERS THEN 
                                      Ln_bank_id:=0;
                             END;

                           lb_Reactiva:=FALSE;
                           IF nvl(Ln_bank_id,0)=0 THEN
                                   --***************************************************************
                                   --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
                                   --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
                                   Ln_Status_A:=NULL; Lv_Status_B:=NULL;Lv_valid_suspe:=NULL;
                                     BEGIN
                                           SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B,Pla.st_valid_suspe   INTO Ln_Status_A,Lv_Status_B,Lv_valid_suspe 
                                             FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                                           WHERE Pla.pk_id=GV_PK_ID
                                                 AND Pla.st_status_i= rc_cust.co_period; 
                                     EXCEPTION            
                                           WHEN OTHERS THEN 
                                              Lv_valid_suspe:='N';
                                     END;     
                                     --                                           
                                   --***************************************************************
                                  IF Lv_valid_suspe='Y' THEN  
                                        --Verifica si la cuenta se encuentra con los pagos para ser reactivada                                  
                                        lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => rc_cust.custcode,
                                                                                                                                                                 PN_Customer_id =>rc_cust.Customer_id ,
                                                                                                                                                                 Pd_Date => PD_Fecha,                                                                                                                                                           
                                                                                                                                                                 PN_CO_IDTRAMITE => rc_cust.Co_idtramite,                                                                                                                                                           
                                                                                                                                                                 PV_STATUS_P => GV_STATUS_P,
                                                                                                                                                                 PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                                                                                                                                 PV_STATUS_OUT  => LV_STATUS_OUT,
                                                                                                                                                                 PV_Sql_Planing =>LV_Sql_Planing,
                                                                                                                                                                 PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                                                                                                                                 PV_REMARK =>  LV_REMARK,
                                                                                                                                                                 PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                                                                                                                                 PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                                                                                                                                 PV_USER_AXIS => LV_USER_AXIS, 
                                                                                                                                                                 PV_CO_STATUS_A =>  Ln_Status_A, 
                                                                                                                                                                 PV_CO_STATUS_B =>  Lv_Status_B,
                                                                                                                                                                 PV_PERIOD             =>   rc_cust.co_period,
                                                                                                                                                                 PV_REMARKTRX   =>  PV_react_Comment,                                                                                                                                                                     
                                                                                                                                                                 PV_OUT_ERROR =>PV_OUT_ERROR 
                                                                                                                                                                 );
                                  END IF;                                                                                                                                                                 
                           END IF;
                           --9833 Johanna Jimenez - ini
                           IF LV_BAND_DISTRI='S' THEN
                             --SE LLAMA A LA FUNCION Fct_Save_ToWk_Contract_2 PARA QUE DISTRIBYA TAMBIEN LAS
                             --CUENTAS QUE NO POSEEN DEUDA (CO_STATUSTRX=A) Y LAS CUENTAS QUE POSEEN LIMITE DE 
                             --CREDITO (CO_STATUSTRX=L) A LAS TABLAS DE PROCESAMIENTO Y ASI EL PROCESO DISPATCH
                             --INSERTE ESTAS TRANSACCIONES EN LA TABLA DE QC DE SUSPENSION MOVIL
                             --Distribuye la carga  
                             LN_Count_Name:=LN_Count_Name+1;           
                             IF LN_Count_Name>PN_SumCountTread  THEN
                                LN_Count_Name:=1;
                             END IF;
                             --INICIO 10995 IRO_JRO
                             IF NVL(LC_BANDERA_HILO.VALOR,'N')='S' THEN
                               LV_TABLA:='TTC_PROCESS_CONTRACT_REPO_S';
                             ELSE
                               LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);
                             END IF;
                             --FIN 10995 IRO_JRO

                             --10995 INI AAM
                             --Priorización de la suspensión - asignación de la categoría
                               IF rc_cust.fecha_plannig <= LD_FECHA_SUSP then
                                LN_CATEGORIA := 1;
                               ELSIF INSTR(LV_FORMA_PAGO_PRIN, '|' || rc_cust.bank_id || '|') > 0 THEN
                                LN_CATEGORIA := 2;
                               ELSE
                                LN_CATEGORIA := 3;
                               END IF;
                               --10995 FIN AAM

                             PV_OUT_ERROR:=Fct_Save_ToWk_Contract_2(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA,lb_Reactiva,LV_STATUS_OUT,LV_REMARK, LN_CATEGORIA, PN_HILO, Ln_bank_id);
                             IF PV_OUT_ERROR IS NOT NULL THEN     
                                RAISE Le_Error_Trx;
                             END IF;                            
                           ELSE
                           --9833 Johanna Jimenez - fin
                            IF NOT lb_Reactiva THEN                                  
                                          --Distribuye la carga  
                                          LN_Count_Name:=LN_Count_Name+1;           
                                          IF LN_Count_Name>PN_SumCountTread  THEN
                                                 LN_Count_Name:=1;
                                          END IF;
                                   --INICIO 10995 IRO_JRO
                                   IF NVL(LC_BANDERA_HILO.VALOR,'N')='S' THEN
                                     LV_TABLA:='TTC_PROCESS_CONTRACT_REPO_S';
                                   ELSE
                                     LV_TABLA:= nvl(V_TableName(LN_Count_Name),LV_TABLA_DEFAULT);
                                   END IF;
                                   --FIN 10995 IRO_JRO          
                                          PV_OUT_ERROR:=Fct_Save_ToWk_Contract(LN_Customer_id,LV_TABLA,nvl(Lv_valid_suspe,'N'),PD_FECHA);
                                          IF PV_OUT_ERROR IS NOT NULL THEN     
                                                 RAISE Le_Error_Trx;
                                          END IF;                                                  
                             ELSE
                                         IF LV_Sql_PlanDet IS NOT NULL THEN 
                                                EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT,LV_REMARK,SYSDATE,rc_cust.Customer_id,GV_STATUS_P;
                                         END IF;
                             END IF;
                            END IF;  
                             LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;
                             IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                                       COMMIT; 
                                      --SCP:AVANCE
                                        -----------------------------------------------------------------------
                                        -- SCP: Código generado automáticamente. Registro de avance de proceso
                                        -----------------------------------------------------------------------
                                        ln_registros_procesados_scp:=ln_registros_procesados_scp+PN_MAX_COMMIT;
                                        scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                                        ----------------------------------------------------------------------- 
                                        RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,PV_FILTER,PN_GROUPTREAD,PN_SOSPID,LB_EXISTE_BCK);
                                        LN_COUNT_COMMIT:=0;
                               END IF;
                               IF LB_EXISTE_BCK=FALSE THEN
                                     EXIT;
                               END IF;
                               
                    EXCEPTION
                          WHEN Le_Error_Trx THEN    
                               --*************
                               --ROLLBACK;
                                --************ 
                               LV_STATUS:='ERR';
                               LV_MENSAJE :='Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'||PV_OUT_ERROR||
                                                           ' para la cuenta -> Customer_id:'||LN_Customer_id;
                               --SCP:MENSAJE
                               ----------------------------------------------------------------------
                               -- SCP: Código generado automáticamente. Registro de mensaje de error
                               ----------------------------------------------------------------------
                               ln_registros_error_scp:=ln_registros_error_scp+1;
                               lv_mensaje_apl_scp:='     PRC_DISTRIBUIR_TABLE_GROUP-Error en la Distribucion en Customer_id';
                               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                     lv_mensaje_apl_scp,
                                                                                                     LV_MENSAJE,
                                                                                                     Null,
                                                                                                     2,Null,
                                                                                                     'Customer_id',
                                                                                                     'Co_id',
                                                                                                     LN_Customer_id,
                                                                                                     0,
                                                                                                     'N',ln_error_scp,lv_error_scp);                                   
                               ----------------------------------------------------------------------------
                               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                  PD_FECHA,
                                                                                                                                  SYSDATE,
                                                                                                                                  LV_STATUS,
                                                                                                                                  LV_MENSAJE);
                          WHEN OTHERS THEN
                                --************* 
                                --ROLLBACK;
                                 --************
                               LV_STATUS:='ERR';
                               LV_MENSAJE :='Error General en Customer_id--> Hilo:'||PN_HILO||' Grouptheard:'||PN_GROUPTREAD||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                                         ' para la cuenta -> Customer_id:'||LN_Customer_id;
                               --SCP:MENSAJE
                               ----------------------------------------------------------------------
                               -- SCP: Código generado automáticamente. Registro de mensaje de error
                               ----------------------------------------------------------------------
                               ln_registros_error_scp:=ln_registros_error_scp+1;
                               lv_mensaje_apl_scp:='     PRC_DISTRIBUIR_TABLE_GROUP-Error en la Distribucion x Customer_id';
                               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                                                                     lv_mensaje_apl_scp,
                                                                                                     LV_MENSAJE,
                                                                                                     Null,
                                                                                                     2,Null,
                                                                                                     'Customer_id',
                                                                                                     'Co_id',
                                                                                                     Ln_Customer_id,
                                                                                                     0,
                                                                                                     'N',ln_error_scp,lv_error_scp);                                   
                               ----------------------------------------------------------------------------
                               RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                                                                                  PD_FECHA,
                                                                                                                                  SYSDATE,
                                                                                                                                  LV_STATUS,
                                                                                                                                  LV_MENSAJE);

                    END;
                    <<NOSUSPENDE>>--13200 MTST
                    NULL;--13200 MTST
                 END LOOP; --FOR Customers
                 --***********--
                   COMMIT;
                --***********--     
                 EXIT WHEN LB_EXISTE_BCK = FALSE;       
                 EXIT WHEN C_Customer_x_Tipo2%notfound; 
        END LOOP; --LOOP  Customers
        CLOSE C_Customer_x_Tipo2;
         IF LB_EXISTE_BCK=FALSE THEN
               RAISE LE_ERROR_MANUAL;
         END IF;
         
          PV_OUT_ERROR:=NULL;

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:='   -Fin Prc_Distribuir_Table_Group';
          ln_registros_procesados_scp:=ln_registros_procesados_scp+LN_COUNT_COMMIT;
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                Null,
                                                0,Gv_unidad_registro_scp,
                                                Null,
                                                Null,
                                                null,null,
                                                'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------      
       EXCEPTION                    
             WHEN NO_DATA_FOUND THEN
                  PV_OUT_ERROR := 'Prc_Distribuir_Table_Group >>ADVERTENCIA No existen datos para la consulta:';
                  If ln_registros_error_scp = 0 Then
                    ln_registros_error_scp:=-1;
                 End If;
                 scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

              WHEN LE_ERROR_VALIDATE THEN
                   ROLLBACK;
                   PV_OUT_ERROR:='Prc_Distribuir_Table_Group-'||PV_OUT_ERROR;
                   If ln_registros_error_scp = 0 Then
                   ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       
              WHEN LE_ERROR_MANUAL THEN                   
                   PV_OUT_ERROR:='ABORTADO';
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                   End If;
                   scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);

               WHEN OTHERS THEN
                   ROLLBACK;
                   LV_MENSAJE := 'PRC_DISTRIBUIR_TABLE_GROUP- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
                   PV_OUT_ERROR:=LV_MENSAJE;
                   If ln_registros_error_scp = 0 Then
                      ln_registros_error_scp:=-1;
                  End If;
                  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                   
    END Prc_Distribuir_Table_Group;
    
     /**************************************************************************************
**************************************************************************************/ 
    
 PROCEDURE Prc_Distribuir_Table_Group_Dth(PN_ID_PROCESS      IN NUMBER,
                                         PV_FILTER          IN VARCHAR2,
                                         PN_HILO            IN NUMBER,
                                         PN_GROUPTREAD      IN NUMBER,
                                         PD_FECHA           DATE,
                                         PN_MAXTRX_EXE      NUMBER,
                                         PN_MAX_COMMIT      NUMBER,
                                         pn_id_bitacora_scp NUMBER,
                                         PN_SOSPID          IN NUMBER,
                                         PV_OUT_ERROR       OUT VARCHAR2) IS
  /*INI 13200 MTST*/
       --* VALIDA SI LA FECHA ES FIN DE SEMANA
        CURSOR C_VALIDA_FINDE (CD_FECHA DATE) IS
          SELECT DECODE(TO_CHAR(CD_FECHA,'DY',
                        'NLS_DATE_LANGUAGE=ENGLISH'),
                        'FRI','S',
                        'SAT','S',
                        'SUN','S',
                        'N') APLICA
          FROM DUAL;
       CURSOR C_VALIDA_CLI_CORP(CN_CUSTOMER NUMBER)IS
        SELECT COUNT(*)
          FROM PORTA.CL_CONTRATOS@AXIS        C,
               PORTA.CL_PERSONAS_EMPRESA@AXIS A,
               PORTA.CL_CLASES_CLIENTES@AXIS  X,
               CUSTOMER_ALL                   B
         WHERE B.CUSTOMER_ID = CN_CUSTOMER
           AND A.ID_PERSONA = C.ID_PERSONA
           AND X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
           AND C.CODIGO_DOC = B.CUSTCODE
           AND X.ID_CLASE_CLIENTE IN
               (SELECT T.ID_CLASE_CLIENTE
                  FROM PORTA.CL_CLASES_CLIENTES@AXIS T
                 WHERE T.DESCRIPCION LIKE '%INS%'
                    OR T.DESCRIPCION LIKE '%VIP%'
                    OR T.DESCRIPCION LIKE '%PRE%');
         
       CURSOR C_OBT_FLAG IS
        SELECT G.VALOR
          FROM GV_PARAMETROS G
         WHERE G.ID_TIPO_PARAMETRO = 13200
           AND G.ID_PARAMETRO = 'FLG_VAL_FIN_SEMANA';
           
       CURSOR C_VALIDA_FERIADO (CD_FECHA DATE) IS
        SELECT COUNT(*)
          FROM PORTA.RC_FERIADOS@AXIS D
         WHERE TRUNC(CD_FECHA) BETWEEN TRUNC(D.CF_FECH_INICIO) AND TRUNC(D.CF_FECH_FIN)
           AND D.CF_ESTADO = 'A';
       LN_VAL_FERIADO      NUMBER;        
       LV_VAL_FECHA        VARCHAR2(1);
       LN_VAL_CORP         NUMBER;
       LV_FLAG_VAL_FECHA   VARCHAR2(1);
       --13200 FIN MTST
  
  CURSOR C_BAND_RELOJ_MEDIOS IS
  SELECT G.VALOR
    FROM GV_PARAMETROS G
   WHERE G.ID_TIPO_PARAMETRO = 22311
     AND G.ID_PARAMETRO = 'BAN_RC_MEDIOS_SUSP';
  
  LV_STATUS        VARCHAR2(3);
  LV_MENSAJE       VARCHAR2(300);
  LV_TABLA_DEFAULT VARCHAR2(100);
  LN_COUNT_COMMIT  NUMBER;
  LB_EXISTE_BCK    BOOLEAN;
  LE_ERROR_MANUAL EXCEPTION;
  LE_ERROR_VALIDATE EXCEPTION;
  --------------------------------------------------------
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  -------------------------------------------------------
  Le_Error_Trx EXCEPTION;
  LV_Tipo           VARCHAR2(1);
  LN_SALDO_MIN      NUMBER;
  LN_SALDO_MIN_POR  NUMBER;
  Ln_bank_id        INTEGER;
  Ln_Status_A       VARCHAR2(5);
  Lv_Status_B       VARCHAR2(5);
  Lv_valid_suspe    VARCHAR2(5);
  lb_Reactiva       BOOLEAN;
  LV_USER_AXIS      TTC_WORKFLOW_USER_S.US_NAME%TYPE;
  LN_Customer_id    NUMBER;
  LN_Count_Name     NUMBER;
  LV_Plan_Telefonia VARCHAR2(10);
  LV_TABLA          VARCHAR2(100);
  LV_STATUS_OUT     VARCHAR2(1);
  LV_Sql_Planing    VARCHAR2(400);
  LV_Sql_PlanDet    VARCHAR2(400);
  LV_REMARK         VARCHAR2(400);
  ----------------------------------------
  Lv_BanErrorPag               VARCHAR2(1);
  Ln_ExistCtaErrorPag          NUMBER;
  Lv_ErrorCtasPag              VARCHAR2(4000);
  ----------------------------------------

BEGIN
  
   --INI 13200 MTST
      OPEN C_OBT_FLAG;
      FETCH C_OBT_FLAG
       INTO LV_FLAG_VAL_FECHA;
      CLOSE C_OBT_FLAG;
      IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
          OPEN C_VALIDA_FINDE(PD_FECHA);
          FETCH C_VALIDA_FINDE
           INTO LV_VAL_FECHA;
          CLOSE C_VALIDA_FINDE;
              
          OPEN C_VALIDA_FERIADO(PD_FECHA);
          FETCH C_VALIDA_FERIADO
           INTO LN_VAL_FERIADO;
          CLOSE C_VALIDA_FERIADO;
              
      END IF;
      --FIN 13200 MTST
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := '   -Inicio procedimiento Prc_Distribuir_Table_Group_Dth';
  lv_mensaje_tec_scp := 'Hilo: ' || PN_HILO || ' Filtro: ' || PV_FILTER ||' Grouptheard: ' || PN_GROUPTREAD;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'N',
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------   
  --Inicializa variables
  LN_COUNT_COMMIT := 0;
  LN_Count_Name   := 0;
  --Bandera si el cursor C_Customer_x_Tipo totaliza por count de co_id o por ña suma del campo valuetrx
  i_Rule_Values_t := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GN_DES_FLAG_ValueTrx);
  LV_Tipo         := nvl(i_Rule_Values_t.Value_Return_ShortString, 'N');

  i_Rule_Values_t  := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_REAC_COMMENT);
  PV_react_Comment := nvl(i_Rule_Values_t.Value_Return_ShortString,'Send:Reactivar Pagos Linea');

  --1- Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;
  LV_USER_AXIS     := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');

  --Cuenta los hilos para cada Filtro:PRGCODE,BILLCYCLE,CYCLE
  OPEN C_GroupThread(PN_Id_Process, PV_FILTER);
  FETCH C_GroupThread INTO PN_SumCountTread;
  CLOSE C_GroupThread;
  IF NVL(PN_SumCountTread, 0) <= 0 THEN
    PN_SumCountTread := 1;
  END IF;

  --Obtiene la tabla default para asignar los valores
  OPEN C_Table_Default(PN_ID_PROCESS, 0, PV_FILTER);
  FETCH C_Table_Default INTO LV_TABLA_DEFAULT;
  CLOSE C_Table_Default;
  IF LV_TABLA_DEFAULT IS NULL THEN
    PV_OUT_ERROR := 'Prc_Customers_Distribution-No existe en la configuración la tabla Default--> Ejemplo: TTC_Process_Contract_Dth0';
    RAISE LE_ERROR_VALIDATE;
  END IF;

  --Cargar los datos de la configuración 
  i_ttc_bank_all_t   := Fct_ttc_bank_all_t;
  i_Plannigstatus_In := Fct_Plannigstatus_In_t;

  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia := RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;

  --Asignar a los arreglos de control el nombre de la tabla y el valor en cero para el control de la distribución
  OPEN C_NameTables(PN_ID_PROCESS, PV_FILTER);
  FETCH C_NameTables BULK COLLECT INTO V_TableName, V_SumCountTread;
  CLOSE C_NameTables;
  ----------------------------------------------------------------------------------
  OPEN C_BAND_RELOJ_MEDIOS;
  FETCH C_BAND_RELOJ_MEDIOS INTO Lv_BanErrorPag;
  CLOSE C_BAND_RELOJ_MEDIOS;
  ----------------------------------------------------------------------------------

  OPEN C_CustomerDth_x_Tipo(PN_GROUPTREAD, PV_FILTER, PD_FECHA, LV_Tipo);
  LOOP
    FETCH C_CustomerDth_x_Tipo BULK COLLECT INTO i_Customers_Count LIMIT PN_MAXTRX_EXE;
    FOR rc_cust IN (select * from table(cast(i_Customers_Count as ttc_Customers_Count_t_tbk))) LOOP
      BEGIN
        LN_Customer_id := rc_cust.Customer_id;
        --------------------------------------------
        IF NVL(Lv_BanErrorPag,'N') = 'S' THEN
           Ln_ExistCtaErrorPag := 0;
           Lv_ErrorCtasPag := NULL;
           
           SYSADM.PQ_RELOJ_MEDIOS_MAGNETICOS.P_VALIDAR_CTAS(PV_CUENTA => rc_cust.custcode,
                                                            PN_EXISTE => Ln_ExistCtaErrorPag,
                                                            PV_ERROR  => Lv_ErrorCtasPag);
           
           IF Ln_ExistCtaErrorPag = 1 THEN
              GOTO NOSUSPENDE; -- NO DEBE PROCESARSE
           END IF;
        END IF;
        --------------------------------------------
        --INI 13200 MTST 
           --valida si es fin de semana o feriado no debe suspender para clientes corp
           IF (nvl(LV_FLAG_VAL_FECHA,'N')='S')THEN 
               --valida si el cliente es corp
                OPEN C_VALIDA_CLI_CORP(LN_Customer_id);
                FETCH C_VALIDA_CLI_CORP
                 INTO LN_VAL_CORP;
                CLOSE C_VALIDA_CLI_CORP;
                IF LN_VAL_CORP >0 THEN 
                   IF(LV_VAL_FECHA = 'S' OR LN_VAL_FERIADO >0 )THEN
                      --no debe procesarse
                      GOTO NOSUSPENDE; 
                   END IF;
                END IF;
            END IF;
            --FIN 13200
        --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
        --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
        BEGIN
          SELECT BANK_ID INTO Ln_bank_id
            FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t)) wkf_cust
           WHERE wkf_cust.BANK_ID = rc_cust.bank_id;
        EXCEPTION
          WHEN OTHERS THEN
            Ln_bank_id := 0;
        END;
        --
        lb_Reactiva := FALSE;
        IF nvl(Ln_bank_id, 0) = 0 THEN
          --***************************************************************
          --Verifica si el estatus se encuentra configurado dentro de los permitidos para la reactivación
          --Si se encuentra configurado y si el campo st_valid_suspe = 'Y', se realiza la verificación en este proceso de suspensión
          Ln_Status_A    := NULL;
          Lv_Status_B    := NULL;
          Lv_valid_suspe := NULL;
          BEGIN
            SELECT Pla.ST_STATUS_A, Pla.ST_STATUS_B, Pla.st_valid_suspe INTO Ln_Status_A, Lv_Status_B, Lv_valid_suspe
              FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
             WHERE Pla.pk_id = GV_PK_ID
               AND Pla.st_status_i = rc_cust.co_period;
          EXCEPTION
            WHEN OTHERS THEN
              Lv_valid_suspe := 'N';
          END;
          --                                           
          --***************************************************************
          IF Lv_valid_suspe = 'Y' THEN
            --Verifica si la cuenta se encuentra con los pagos para ser reactivada                                  
            lb_Reactiva := RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode       => rc_cust.custcode,
                                                                       PN_Customer_id    => rc_cust.Customer_id,
                                                                       Pd_Date           => PD_Fecha,
                                                                       PN_CO_IDTRAMITE   => rc_cust.Co_idtramite,
                                                                       PV_STATUS_P       => GV_STATUS_P,
                                                                       PV_Plan_Telefonia => LV_Plan_Telefonia,
                                                                       PV_STATUS_OUT     => LV_STATUS_OUT,
                                                                       PV_Sql_Planing    => LV_Sql_Planing,
                                                                       PV_Sql_PlanDet    => LV_Sql_PlanDet,
                                                                       PV_REMARK         => LV_REMARK,
                                                                       PN_SALDO_MIN      => LN_SALDO_MIN,
                                                                       PN_SALDO_MIN_POR  => LN_SALDO_MIN_POR,
                                                                       PV_USER_AXIS      => LV_USER_AXIS,
                                                                       PV_CO_STATUS_A    => Ln_Status_A,
                                                                       PV_CO_STATUS_B    => Lv_Status_B,
                                                                       PV_PERIOD         => rc_cust.co_period,
                                                                       PV_REMARKTRX      => PV_react_Comment,
                                                                       PV_OUT_ERROR      => PV_OUT_ERROR);
          END IF;
        END IF;
        IF NOT lb_Reactiva THEN
          --Distribuye la carga  
          LN_Count_Name := LN_Count_Name + 1;
          IF LN_Count_Name > PN_SumCountTread THEN
            LN_Count_Name := 1;
          END IF;
          LV_TABLA     := nvl(V_TableName(LN_Count_Name), LV_TABLA_DEFAULT);
          PV_OUT_ERROR := Fct_Save_ToWk_Contract_Dth(LN_Customer_id,
                                                     LV_TABLA,
                                                     nvl(Lv_valid_suspe, 'N'),
                                                     PD_FECHA,
                                                     Ln_bank_id);--INICIO [11922] 05/06/2018 IRO ABA
          IF PV_OUT_ERROR IS NOT NULL THEN
            RAISE Le_Error_Trx;
          END IF;
        ELSE
          IF LV_Sql_PlanDet IS NOT NULL THEN
            EXECUTE IMMEDIATE LV_Sql_PlanDet USING LV_STATUS_OUT, LV_REMARK, SYSDATE, rc_cust.Customer_id, GV_STATUS_P;
          END IF;
        END IF;
        LN_COUNT_COMMIT := LN_COUNT_COMMIT + 1;
        IF LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
          COMMIT;
          --SCP:AVANCE
          -----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de avance de proceso
          -----------------------------------------------------------------------
          ln_registros_procesados_scp := ln_registros_procesados_scp + PN_MAX_COMMIT;
          scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                null,
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------- 
          RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,
                                                              GV_DESSHORT_DTH,
                                                              PV_FILTER,
                                                              PN_GROUPTREAD,
                                                              PN_SOSPID,
                                                              LB_EXISTE_BCK);
          LN_COUNT_COMMIT := 0;
        END IF;
        IF LB_EXISTE_BCK = FALSE THEN
          EXIT;
        END IF;
      
      EXCEPTION
        WHEN Le_Error_Trx THEN
          --*************
          --ROLLBACK;
          --************ 
          LV_STATUS  := 'ERR';
          LV_MENSAJE := 'Hilo:' || PN_HILO || ' Grouptheard:' ||PN_GROUPTREAD ||
                        ' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' ||
                        ' ERROR:' || PV_OUT_ERROR ||' para la cuenta -> Customer_id:' || LN_Customer_id;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp := ln_registros_error_scp + 1;
          lv_mensaje_apl_scp     := '     PRC_DISTRIBUIR_TABLE_GROUP_DTH-Error en la Distribucion en Customer_id';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                LV_MENSAJE,
                                                Null,
                                                2,
                                                Null,
                                                'Customer_id',
                                                'Co_id',
                                                LN_Customer_id,
                                                0,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------------
          RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                       PD_FECHA,
                                                       SYSDATE,
                                                       LV_STATUS,
                                                       LV_MENSAJE);
        WHEN OTHERS THEN
          --************* 
          --ROLLBACK;
          --************
          LV_STATUS  := 'ERR';
          LV_MENSAJE := 'Error General en Customer_id--> Hilo:' || PN_HILO ||
                        ' Grouptheard:' || PN_GROUPTREAD ||' >> RC_Trx_TimeToCash_Send.PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' ||
                        ' ERROR:' || SQLCODE || ' ' ||SUBSTR(SQLERRM, 1, 100) ||' para la cuenta -> Customer_id:' || LN_Customer_id;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp := ln_registros_error_scp + 1;
          lv_mensaje_apl_scp     := '     PRC_DISTRIBUIR_TABLE_GROUP_DTH-Error en la Distribucion x Customer_id';
          scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                LV_MENSAJE,
                                                Null,
                                                2,
                                                Null,
                                                'Customer_id',
                                                'Co_id',
                                                Ln_Customer_id,
                                                0,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
          ----------------------------------------------------------------------------
          RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                       PD_FECHA,
                                                       SYSDATE,
                                                       LV_STATUS,
                                                       LV_MENSAJE);
        
      END;
       <<NOSUSPENDE>>--13200 MTST
       NULL;--13200 MTST
    END LOOP; --FOR Customers
    --***********--
    COMMIT;
    --***********--     
    EXIT WHEN LB_EXISTE_BCK = FALSE;
    EXIT WHEN C_CustomerDth_x_Tipo%notfound;
  END LOOP; --LOOP  Customers
  CLOSE C_CustomerDth_x_Tipo;
  IF LB_EXISTE_BCK = FALSE THEN
    RAISE LE_ERROR_MANUAL;
  END IF;

  PV_OUT_ERROR := NULL;

  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp          := '   -Fin Prc_Distribuir_Table_Group_Dth';
  ln_registros_procesados_scp := ln_registros_procesados_scp + LN_COUNT_COMMIT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,
                                        null,
                                        'S',
                                        ln_error_scp,
                                        lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_registros_procesados_scp,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  ----------------------------------------------------------------------------      
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PV_OUT_ERROR := 'Prc_Distribuir_Table_Group_Dth >>ADVERTENCIA No existen datos para la consulta:';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_VALIDATE THEN
    ROLLBACK;
    PV_OUT_ERROR := 'Prc_Distribuir_Table_Group_Dth-' || PV_OUT_ERROR;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN LE_ERROR_MANUAL THEN
    PV_OUT_ERROR := 'ABORTADO';
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
  
  WHEN OTHERS THEN
    ROLLBACK;
    LV_MENSAJE   := 'PRC_DISTRIBUIR_TABLE_GROUP_DTH- ' || ' ERROR:' || SQLCODE || ' ' || SUBSTR(SQLERRM, 1, 100);
    PV_OUT_ERROR := LV_MENSAJE;
    If ln_registros_error_scp = 0 Then
      ln_registros_error_scp := -1;
    End If;
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                          Null,
                                          ln_registros_error_scp,
                                          ln_error_scp,
                                          lv_error_scp);
END Prc_Distribuir_Table_Group_Dth;
    
     
end RC_Trx_TimeToCash_Send;
/
