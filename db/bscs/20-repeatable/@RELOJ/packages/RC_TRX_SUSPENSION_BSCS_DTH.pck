CREATE OR REPLACE PACKAGE reloj.RC_TRX_SUSPENSION_BSCS_DTH IS

/*******************************************************************************************************
  * [8693] RELOJ DE COBRANZAS DTH
  * Autor :     IRO JUAN ROMERO
  * Creacion:   13/08/2013
  * OBJETIVO:   Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                de la tabla PROFILE_SERVICE_ADV_CHARGE para cada co_id, 
                evitando errores en la facturacion.
 ******************************************************************************************************/                            
                               
PROCEDURE RC_ACTUALIZA_VALOR_SNCODE (Pv_co_id       IN NUMBER,
                                     Pv_per_ini     IN VARCHAR2,
                                     Pv_per_fin     IN VARCHAR2,
                                     Pv_response    out NUMBER,
                                     Pv_etapa       out varchar2,
                                     Pv_detalle     out varchar2);                              
                                                               
                               
PROCEDURE RC_BITACORIZA_SUSP_BSCS_DTH(Pv_id_proceso_scp   varchar2,
                                      Pv_tipo_error       number,
                                      Pv_notifica         varchar2,
                                      Pv_mensaje_app      varchar2,
                                      Pv_mensaje_tec      varchar2);
                                      

END RC_TRX_SUSPENSION_BSCS_DTH;
/
CREATE OR REPLACE PACKAGE BODY reloj.RC_TRX_SUSPENSION_BSCS_DTH IS

  /*******************************************************************************************************
  * [8693] RELOJ DE COBRANZAS DTH
  * Autor :     IRO JUAN ROMERO
  * Creacion:   13/08/2013
  * OBJETIVO:   Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                de la tabla PROFILE_SERVICE_ADV_CHARGE para cada co_id, 
                evitando errores en la facturacion.
                
  ******************************************************************************************************/
    
/*=====PROCEDIMIENTO PARA ACTUALIZAR VALORES EN LA TABLA PROFILE_SERVICE_ADV_CHARGE EN BSCS=====*/  
  
PROCEDURE RC_ACTUALIZA_VALOR_SNCODE (Pv_co_id       IN NUMBER,
                                     Pv_per_ini     IN VARCHAR2,
                                     Pv_per_fin     IN VARCHAR2,
                                     Pv_response    out NUMBER,
                                     Pv_etapa       out varchar2,
                                     Pv_detalle     out varchar2)IS
                                     
                                                     
-- CURSOR PARA OBTENER EL ESTADO DEL CO_ID EN BSCS                                                     
CURSOR c_status_bscs (Cv_co_id number) IS
   SELECT A.ch_status FROM CONTRACT_HISTORY A
   WHERE A.CO_ID=Cv_co_id
   AND A.CH_SEQNO=(SELECT MAX(B.CH_SEQNO) FROM CONTRACT_HISTORY  B
                   WHERE B.CO_ID=Cv_co_id)
   AND A.CH_STATUS='s'; 


/*==============Variables locales==============*/

Ln_dias_ciclo                          number;
Ln_dias_cobrar                         number; 
Lb_Found                               boolean;
Le_error                               exception;
Ln_valor_ADV_CHARGE                    number(12,5);
Ln_valor_ADV_CHARGE_ITEM_PRICE         number(12,5);
Lv_etapa                               varchar2(100);
Lv_status_bscs                         varchar2(100);
Lv_mensaje_error                       varchar2(4000):=NULL;
Ln_ADV_CHARGE                          number;
Ln_ADV_CHARGE_ITEM_PRICE               number;
Ln_num_reg_act                         number;

--Variables de scp_dat
lv_id_proceso_scp          varchar2(100):='SUSPENSION_DTH';
Ln_parametro_dias          varchar2(4000);
lv_descripcion_scp         varchar2(500);
ln_error_scp               number:=0;
lv_error_scp               varchar2(500);

-------------------------------------------------

BEGIN
  
                           
    -------------------------Recuperar el estatus en bscs del co_id
    Lv_etapa:='Recuperación del estatus actual en bscs del contrato';
    open c_status_bscs(Pv_co_id);
    fetch c_status_bscs
    into Lv_status_bscs;
    Lb_Found := c_status_bscs%FOUND;
    close c_status_bscs;
    
    IF NOT Lb_Found THEN
      Lv_mensaje_error:= 'El co_id esta activo  o  no existe en la tabla contract_history';
      RAISE LE_ERROR;
    END IF;

    Lv_etapa:='Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
    Ln_num_reg_act:=0;
    FOR I IN (select a.ADV_CHARGE, 
                     a.ADV_CHARGE_ITEM_PRICE,
                     a.SNCODE
             from SYSADM.PROFILE_SERVICE_ADV_CHARGE a        
             where a.co_id=Pv_co_id) LOOP
             
          Lv_etapa:='Recuperacion del valor del parametro <DIAS_ANT_FIN_CICLO>';
          scp_dat.sck_api.scp_parametros_procesos_lee('DIAS_ANT_FIN_CICLO',
                                                lv_id_proceso_scp,
                                                Ln_parametro_dias,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
         
         If ln_error_scp <> 0 Then
           Lv_mensaje_error:= lv_error_scp;
           raise Le_error;
         End If;
      
         Ln_ADV_CHARGE:=I.ADV_CHARGE;
         Ln_ADV_CHARGE_ITEM_PRICE:=I.ADV_CHARGE_ITEM_PRICE;
            
         Lv_etapa:='Calculo de la variable <Ln_dias_ciclo>';
         Ln_dias_ciclo:= TO_DATE(Pv_per_fin,'DD/MM/RRRR') - TO_DATE(Pv_per_ini,'DD/MM/RRRR')+1;
                       
         Lv_etapa:='Calculo de valor para las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         Ln_dias_cobrar:= Ln_dias_ciclo + to_number(Ln_parametro_dias);
         Ln_valor_ADV_CHARGE:=round((Ln_ADV_CHARGE/Ln_dias_ciclo)*Ln_dias_cobrar,5);
         Ln_valor_ADV_CHARGE_ITEM_PRICE:=round((Ln_ADV_CHARGE_ITEM_PRICE/Ln_dias_ciclo)*Ln_dias_cobrar,5);           
         Lv_etapa:='Actualizacion de las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
         set A.ADV_CHARGE=Ln_valor_ADV_CHARGE,A.ADV_CHARGE_ITEM_PRICE=Ln_valor_ADV_CHARGE_ITEM_PRICE
         where A.co_id=Pv_co_id
         and A.SNCODE=I.SNCODE;
         Ln_num_reg_act:=Ln_num_reg_act+1;      
    END LOOP;
    if Ln_num_reg_act <= 0 then
      Lv_etapa:='Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
      Lv_mensaje_error:= 'No se encontraron registros para el co_id <'||Pv_co_id||'>';
      raise Le_error;
    end if;       
    commit;
    Pv_response:=0;
    Pv_etapa:='RC_TRX_SUSPENSION_BSCS_DTH.RC_ACTUALIZA_VALOR_SNCODE';
    Pv_detalle:='Procesamiento exitoso';
EXCEPTION
    WHEN  LE_ERROR THEN
      Pv_response:=1;
      Pv_etapa:='RC_ACTUALIZA_VALOR_SNCODE: '||Lv_etapa;
      Pv_detalle:=Lv_mensaje_error;
      --Bitacorizando el error suscitado
      RC_BITACORIZA_SUSP_BSCS_DTH(Lv_id_proceso_scp,2,'S',Lv_etapa,Lv_mensaje_error);
    WHEN OTHERS THEN
      Pv_response:=1;
      Pv_etapa:='RC_ACTUALIZA_VALOR_SNCODE: '||Lv_etapa;
      Pv_detalle:=sqlerrm;
      rollback;
      --Bitacorizando el error suscitado
      RC_BITACORIZA_SUSP_BSCS_DTH(Lv_id_proceso_scp,3,'S',Lv_etapa,sqlerrm);
    
END RC_ACTUALIZA_VALOR_SNCODE;  
  
/*=========PROCEDIMIENTO PARA BITACORIZAR ERRORES EN EL PAQUETE RELOJ.RC_TRX_SUSPENSION_BSCS_DTH=====*/

PROCEDURE RC_BITACORIZA_SUSP_BSCS_DTH(Pv_id_proceso_scp   varchar2,
                                      Pv_tipo_error       number,
                                      Pv_notifica         varchar2,
                                      Pv_mensaje_app      varchar2,
                                      Pv_mensaje_tec      varchar2) IS

-------Variables

  Lv_tipo_error                       number:=Pv_tipo_error;
  ln_total_registros_scp              NUMBER:=1;
  ln_id_bitacora_scp                  number:=0;
  ln_id_detalle_bitacora_scp          number:=0;
  ln_error_scp                        number:=0;
  lv_error_scp                        varchar2(4000);
  lv_mensaje_apl_scp                  varchar2(4000):=Pv_mensaje_app; 
  LV_MENSAJE                          varchar2(4000):=Pv_mensaje_tec;                                   
  Lv_id_proceso_scp                   varchar2(30):=Pv_id_proceso_scp; 
  Lv_notifica                         varchar2(5):=Pv_notifica;
  lv_unidad_registro_scp              varchar2(50):='Decodificador';
  lv_referencia_scp                   varchar2(100):='RELOJ.RC_TRX_SUSPENSION_BSCS_DTH.PCK';  
  Le_excepcion                        exception;                                    
        
  BEGIN
   --Inicia la bitacora
    scp_dat.sck_api.scp_bitacora_procesos_ins(Lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    IF ln_error_scp <> 0 THEN

      raise Le_excepcion;

    END IF;

    --Inserta el mensaje de error en la bitacora
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          Lv_tipo_error,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          Lv_notifica,
                                          ln_id_detalle_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
                                          
     IF ln_error_scp <> 0 THEN

      raise Le_excepcion;

    END IF;                                     

    --Finaliza la bitacora
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
                                          
    IF ln_error_scp <> 0 THEN

      raise Le_excepcion;

    END IF;                                      

  EXCEPTION
    WHEN Le_excepcion then
      null;
    when others then
      null;  

END RC_BITACORIZA_SUSP_BSCS_DTH;


END RC_TRX_SUSPENSION_BSCS_DTH;
/
