create or replace package reloj.RC_Trx_TimeToCash_Reactivev IS
/*Creador CLS WMENDOZA 27/10/2010
  Reactivacion De Reloj De Cobranza*/

--VARIABLES GLOBALES
GV_DESSHORT VARCHAR2(12) := 'TTC_REAC_ALL';
GV_PK_ID           NUMBER:=1;
Gv_unidad_registro_scp varchar2(30):='Cuentas';
gv_value_rule_string_cash varchar2(40):='TTC_FLAG_CUSTOMER_CASH_DIA';
gv_value_rule_string_out varchar2(30):='TTC_FLAG_CUSTOMER_OUT_RELOJ';
GV_DES_FLAG_ValueTrx VARCHAR2(30) := 'TTC_FLAG_CURSOR_CUSTCODE';
gv_value_rule_view varchar2(30):='TTC_FLAG_DIAS_PERMITIDO';
gv_value_rule_cant_estatus varchar2(30):='TTC_FLAG_CANTIDAD_ESTATUS';
gv_value_rule_estatus_update varchar2(30):='TTC_FLAG_ESTATUS_UPDATE';
gv_value_rule_mensaje varchar2(30):='TTC_FLAG_MENSAJE_REACTIVE';
gv_value_rule_estatus varchar2(30):='TTC_FLAG_ESTATUS';
GV_STATUS_P   VARCHAR2(1) := 'P';

GV_TTC_RANGE_HOUR_FULL     VARCHAR2(30) := 'TTC_RANGE_HOUR_FULL';
GV_TTC_RANGE_HOUR_SEMIFULL VARCHAR2(30) := 'TTC_RANGE_HOUR_SEMIFULL';
GV_TTC_RANGE_HOUR_LOW      VARCHAR2(30) := 'TTC_RANGE_HOUR_LOW';
GV_TTC_MAX_RESTRICT_TIME   VARCHAR2(30) := 'TTC_MAX_RESTRICT_TIME';

gv_hora_ini VARCHAR2(30);
gv_hora_fin VARCHAR2(30);

t_RangeTime_Restrict               ttc_Rule_Values_t;
t_RangeTime_Full                   ttc_Rule_Values_t;
t_RangeTime_SemiFull               ttc_Rule_Values_t;
t_RangeTime_Low                    ttc_Rule_Values_t;
t_RangeTime_Select                 ttc_Rule_Values_t;

i_Process_Programmer_t       ttc_Process_Programmer_t;
GV_DESSHORT_LOW             VARCHAR2(15) := 'TTC_REAC_LOW';
GV_DESSHORT_RETRY_SEMI_FULL VARCHAR2(16) := 'TTC_REAC_SFUL';

Function RC_CANT_TOTAL_REGISTRO(pv_table_name in varchar2) Return Number;

procedure Prc_Head (pv_modo       IN VARCHAR2 DEFAULT 'D', 
                                          pv_error        OUT VARCHAR2,
                                          PN_SOSPID     IN NUMBER,
                                          pn_cant_proc out NUMBER
                                          );

procedure Prc_Customers_Cash_Dia(PN_ID_PROCESS        IN  NUMBER,
                                 pn_id_bitacora_scp   IN  NUMBER,
                                 PD_Fecha             in  date,
                                 pn_MaxRetenTrx       in number,
                                 PN_MAXTRX_EXE        in number,
                                 PN_MAX_COMMIT        in  NUMBER,
                                 pn_cant_proc         out number,
                                 PN_SOSPID     IN NUMBER,
                                 PV_OUT_ERROR         OUT VARCHAR2);
                                 
procedure Prc_Customers_Out_Reloj(PN_ID_PROCESS        IN  NUMBER,
                                 pn_id_bitacora_scp   IN  NUMBER,
                                 PD_Fecha             in  date,
                                 pn_MaxRetenTrx       in number,
                                 PN_MAXTRX_EXE        in number,
                                 PN_MAX_COMMIT        in  NUMBER,
                                 pn_cant_proc         out number,
                                 PN_SOSPID     IN NUMBER,
                                 PV_OUT_ERROR         OUT VARCHAR2) ;

END RC_Trx_TimeToCash_Reactivev;
/
create or replace package body reloj.RC_Trx_TimeToCash_Reactivev is
/*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÉREZ
  LIDER IRO:      VERÓNICA OLIVO BACILIO 
  MODIFICADO POR: IRO MIGUEL MUÑOZ 
  PROYECTO:       8649 MEJORAS EN RELOJ DE COBRANZA
  FECHA:         08/02/2012
  PROPOSITO:     Optimización de proceso y verificación de bandera para determinar si se realiza o no el conteo  
                 de las cuentas a procesar
  *********************************************************************************************************/
FUNCTION fnc_Check_Range_Hour(PV_OUT_ERROR OUT VARCHAR2) RETURN BOOLEAN IS
    LV_HourNow     VARCHAR2(20);
    LB_Flag_Return BOOLEAN;
  BEGIN
    PV_OUT_ERROR   := NULL;
    LB_Flag_Return := TRUE;
  
    SELECT to_char(SYSDATE, 'hh24:mi:ss') INTO LV_HourNow FROM dual;
    --Verifica la hora actual  si se encuentra dentro del rango de lo contrario
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(gv_hora_ini, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(gv_hora_fin, 'hh24:mi:ss') THEN
      LB_Flag_Return := TRUE;
    ELSE
      LB_Flag_Return := FALSE;
      PV_OUT_ERROR   := 'ADVERTENCIA >> El proceso cambia de rango de hora, se procede a bajar los proceso para subirlos con la nueva configuración... ';
    END IF;
  
    --Verifica si esta dentro del rango permitido 
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Restrict.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Restrict.Value_Return_LongString, 'hh24:mi:ss') THEN
      PV_OUT_ERROR   := 'ADVERTENCIA >> Horario no permitido de ejecución del reloj, consulte con VSO o el administrador y cambie los parametros... ';
      LB_Flag_Return := FALSE;
    END IF;
  
    RETURN LB_Flag_Return;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN TRUE;
END fnc_Check_Range_Hour;

Function RC_CANT_TOTAL_REGISTRO(pv_table_name in varchar2) Return Number Is
    lv_sql       Varchar2(4000);
    ln_cantidad  Number:=0;
    
    Begin
      lv_sql:='SELECT /*+ ALL_ROWS */ COUNT(CUSTOMER_ID) FROM '||pv_table_name;
      Execute Immediate lv_sql Into ln_cantidad;
     
      Return ln_cantidad;
      
      Exception
        When NO_DATA_FOUND Then
           Return 0;
        When Others Then
          Return 0;
End RC_CANT_TOTAL_REGISTRO;

FUNCTION Fct_ttc_bank_all_t RETURN  ttc_bank_all_t as
    doc_in  ttc_bank_all_t;
  BEGIN
   SELECT /*+ ALL_ROWS */ ttc_bank_all_type(bank_Id,bankname) 
                   BULK COLLECT  INTO  doc_in FROM  reloj.ttc_view_bank_all_bscs;
    RETURN doc_in;
END;
 
FUNCTION Fct_check_cuentas (pn_customer_id IN NUMBER,
                            pv_cuscode        IN VARCHAR2,
                            pn_bank_id        IN NUMBER,
                            pv_status           IN VARCHAR2,
                            pv_co_period in varchar2) Return Number Is
      
  BEGIN
      INSERT INTO reloj.ttc_customers_check  NOLOGGING
                  VALUES
                    (pn_customer_id,
                     pv_cuscode,
                     pn_bank_id,
                     to_date(SYSDATE,'DD/MM/YYYY'),
                     pv_status,
                     pv_co_period 
                    );
      Return 1;
    Exception
        When NO_DATA_FOUND Then
          Return 0;
        When Others Then
          Return 0;
END;

FUNCTION Fct_update_periodo (PN_process in number,pn_co_period IN NUMBER) Return Number Is     
      
      i_rule_values                      TTC_Rule_Values_t;
      lv_rule_status                     varchar2(50);
  BEGIN
      lv_rule_status:=gv_value_rule_estatus||'_'||pn_co_period;
      i_rule_values:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_rule_values_t(Pv_Process => PN_process,Pv_Value_Rule => lv_rule_status);

      update ttc_rule_values_s
         set value_return_shortstring = i_rule_values.Value_Return_ShortString
       where process = 17
         and value_rule_string = gv_value_rule_estatus_update;
      commit;
      Return 1;
    Exception
        When NO_DATA_FOUND Then
          Return 0;
        When Others Then
          Return 0;
END;
FUNCTION Fct_Plannigstatus_In_t RETURN  TTC_Plannigstatus_In_t as
          doc_in  TTC_Plannigstatus_In_t;
        BEGIN
         SELECT /*+ ALL_ROWS */ TTC_Plannigstatus_In_type(pk_id,
                                           st_id,
                                           st_seqno, 
                                           re_orden,
                                           st_sdes,
                                           st_status_i,
                                           st_status_next,
                                           st_duration,
                                           st_ext_duration ,
                                           st_duration_total,
                                           st_status_a,
                                           st_status_b,
                                           st_reason,
                                           st_alert,
                                           st_nodayalert,
                                           st_valid,
                                           co_Remark,
                                           co_lastmodDate,
                                           st_CkeckStatus,
                                           st_valid_cond_1,
                                           st_valid_reac,
                                           st_valid_suspe,
                                           st_valid_eval_1,
                                           st_valid_eval_2
                                           )  BULK COLLECT  INTO  doc_in 
              FROM  TTC_VIEW_PLANNIGSTATUS_IN;
          RETURN doc_in;
END;
procedure Prc_Head (pv_modo       IN VARCHAR2 DEFAULT 'D', --D: Ejecuta el reloj todos los dias, G:Ejecuta el reloj  para todas las trx del reloj, ambos verifican si la cuenta debe reactivarse
                    pv_error         OUT VARCHAR2,
                    PN_SOSPID     IN NUMBER,
                    pn_cant_proc out number) is

CURSOR C_Trx_In IS
              SELECT  TTC_TRX_IN.NEXTVAL FROM dual;
Cursor C_PARAMETROS (CN_TIPOPARAMETRO Number, CV_PARAMETRO Varchar2) Is
  Select Valor
  From Gv_Parametros g
  Where g.Id_Tipo_Parametro = Cn_Tipoparametro
  And g.Id_Parametro = Cv_Parametro;
--VARIABLES
i_process                          ttc_process_t;
i_rule_element                     ttc_Rule_Element_t;
i_rule_values                      TTC_Rule_Values_t;
Ln_Seqno                           NUMBER;
ln_dia                             number;
ln_periodo                         NUMBER;
Ld_Fecha_Actual                    DATE;
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR(3);
LV_HourNow                         VARCHAR2(20);
LV_fecha_ejecucion                 date;
LN_CANT_STATUS                     NUMBER;
lv_rule_view                       varchar2(100);
ln_cant_proc                       number:=0;
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp                 number:=0; 
ln_total_registros_scp             number:=0;
lv_id_proceso_scp                  varchar2(100):=GV_DESSHORT;
lv_referencia_scp                  varchar2(200):='RC_Trx_TimeToCash_Reactive.RC_HEAD';
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
lv_parametro                       varchar2(100);
---------------------------------------------------------------
begin
  pn_cant_proc:=0;
  Ld_Fecha_Actual:= SYSDATE;
  SELECT to_date(SYSDATE, 'dd/mm/rrrr') INTO LV_fecha_ejecucion FROM dual;
  SELECT to_char(SYSDATE, 'hh24:mi:ss') INTO LV_HourNow FROM dual;
  SELECT TO_NUMBER(TO_CHAR(SYSDATE,'D')) INTO ln_dia FROM DUAL;
  
  -- SCP:INICIO 
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  lv_referencia_scp:='Inicio del proceso de reactivacion: '||lv_referencia_scp;
  Open C_PARAMETROS(753,'CANT_REAC_RELOJ');
   Fetch C_PARAMETROS Into lv_parametro;
   Close C_PARAMETROS;

 IF NVL(lv_parametro,'N')='S'THEN
   
 IF nvl(pv_modo, 'D')='D'THEN
    ln_total_registros_scp:=RC_CANT_TOTAL_REGISTRO(pv_table_name => 'ttc_view_customers_reactive');
  ELSE
    ln_total_registros_scp:=RC_CANT_TOTAL_REGISTRO(pv_table_name => 'ttc_view_cust_reactive_all');
  END IF;
 
 END IF;
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                        lv_referencia_scp,
                                        null,null,null,null,
                                        ln_total_registros_scp,
                                        Gv_unidad_registro_scp,
                                        ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  if ln_error_scp <>0 Then
     LV_MENSAJE:='Error en Plataforma SCP, No se pudo iniciar la Bitacora';
     Raise LE_ERROR;
  end if;
  
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO '||GV_DESSHORT;
  lv_mensaje_tec_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  
  i_process:=  RC_Api_TimeToCash_Rule_ALL.Fct_ttc_processDesShort_t(GV_DESSHORT);
  IF i_process.Process IS NULL THEN
        LV_MENSAJE:='No existen datos configurados en la tabla TTC_Process_s';
        RAISE LE_ERROR;
  END IF;
  
   i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT);
   IF i_rule_element.Process IS NULL THEN
          LV_MENSAJE:='No existen datos configurados en la tabla TTC_rule_element';
          RAISE LE_ERROR;
   END IF;   
  
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de reactivacion--------------------
  ---------------------------------------------------------------------------- 
  --Obtiene los rangos de horas permitidas, para la condiguración de los parámetros  
    t_RangeTime_Restrict:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_MAX_RESTRICT_TIME);
    
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Restrict.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Restrict.Value_Return_LongString, 'hh24:mi:ss') THEN
      LV_MENSAJE := 'ADVERTENCIA >> Horario no permitido de ejecución del reloj, consulte con VSO o el administrador y cambie los parametros... ';
      RAISE LE_ERROR;
    END IF;
    t_RangeTime_Full:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_RANGE_HOUR_FULL);
    t_RangeTime_SemiFull:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_RANGE_HOUR_SEMIFULL);
    t_RangeTime_Low:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_TTC_RANGE_HOUR_LOW);
    
    --Obtiene los rangos de horas permitidas, para la condiguración de los parámetros      
    --Verifica la hora actual y asigna los parámetros de acuerdo al rango permitido
    IF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_Full.Value_Return_ShortString, 'hh24:mi:ss') AND
       To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_Full.Value_Return_LongString, 'hh24:mi:ss') THEN
       i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT);      
       
       gv_hora_ini:=t_RangeTime_Full.Value_Return_ShortString;
       gv_hora_fin :=t_RangeTime_Full.Value_Return_LongString;
    ELSIF To_date(LV_HourNow, 'hh24:mi:ss') >= to_date(t_RangeTime_SemiFull.Value_Return_ShortString, 'hh24:mi:ss') AND
          To_date(LV_HourNow, 'hh24:mi:ss') <= to_date(t_RangeTime_SemiFull.Value_Return_LongString, 'hh24:mi:ss') THEN
          i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_RETRY_SEMI_FULL);  
          
          gv_hora_ini:=t_RangeTime_SemiFull.Value_Return_ShortString;
          gv_hora_fin :=t_RangeTime_SemiFull.Value_Return_LongString;   
    ELSE
      i_rule_element:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_ElementDesShort_t(GV_DESSHORT_LOW);      
      
      gv_hora_ini:=t_RangeTime_Low.Value_Return_ShortString;
      gv_hora_fin:=t_RangeTime_Low.Value_Return_LongString;
    END IF;
    
  i_rule_values:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_rule_values_t(Pv_Process => i_process.Process,Pv_Value_Rule => gv_value_rule_string_cash);
  
  if i_rule_values.Value_Return_ShortString='S' AND pv_modo='D' then
     RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia(PN_ID_PROCESS => i_process.Process,
                                                       pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                                       PD_Fecha => Ld_Fecha_Actual,
                                                       pn_MaxRetenTrx => i_rule_element.MaxRetenTrx,
                                                       PN_MAXTRX_EXE => i_rule_element.Value_Max_Trx_By_Execution,                                                       
                                                       PN_MAX_COMMIT => i_rule_element.Value_Max_TrxCommit,
                                                       pn_cant_proc  => pn_cant_proc,
                                                       PN_SOSPID  => PN_SOSPID,
                                                       PV_OUT_ERROR => LV_MENSAJE);                                
                                                  
  end if;

   i_rule_values:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_rule_values_t(Pv_Process => i_process.Process,Pv_Value_Rule => gv_value_rule_string_out);
  if i_rule_values.Value_Return_ShortString='S' AND pv_modo='G' THEN  
     --obtengo la cantidad de estatus a procesar
     i_rule_values:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_rule_values_t(Pv_Process => i_process.Process,Pv_Value_Rule => gv_value_rule_cant_estatus);
     LN_CANT_STATUS:=i_rule_values.Value_Return_ShortString;
     FOR i IN 1.. LN_CANT_STATUS LOOP           
       --Obtengo los dias que pueden ser ejecutados cada estatus  
       lv_rule_view:=gv_value_rule_view||'_'||i;
       i_rule_values:= RC_Api_TimeToCash_Rule_ALL.Fct_ttc_rule_values_t(Pv_Process => i_process.Process,Pv_Value_Rule => lv_rule_view);
       if i_rule_values.Value_Return_ShortString like '%'||ln_dia||'%' then      
          --Envio actualizar el campo value_return_shortstring de la tabla ttc_rule_values
          ln_periodo:=Fct_update_periodo(i_process.Process,i);
          if ln_periodo=1 then          
             RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj(PN_ID_PROCESS => i_process.Process,
                                                                pn_id_bitacora_scp =>ln_id_bitacora_scp,
                                                                PD_Fecha => Ld_Fecha_Actual,
                                                                pn_MaxRetenTrx => i_rule_element.MaxRetenTrx,
                                                                PN_MAXTRX_EXE => i_rule_element.Value_Max_Trx_By_Execution,                                                       
                                                                PN_MAX_COMMIT => i_rule_element.Value_Max_TrxCommit,
                                                                pn_cant_proc  => ln_cant_proc,
                                                                PN_SOSPID  => PN_SOSPID,
                                                                PV_OUT_ERROR => LV_MENSAJE); 
                                                                
             if ln_cant_proc>pn_cant_proc then
                pn_cant_proc:=ln_cant_proc;
             end if;                                                   
             IF  LV_MENSAJE IS NOT NULL THEN
                    Raise LE_ERROR;
             END IF;
          else
            LV_MENSAJE:='ERROR AL ACTUALIZAR EL PERIODO '||i||'DEL PROCESO Prc_Customers_Out_Reloj';
            Raise LE_ERROR;
          end if;                                    
        end if;
     END LOOP;
  end if;  
  IF  LV_MENSAJE IS NOT NULL THEN
              Raise LE_ERROR;
  END IF;  
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de reactivacion--------------------------- 
  --------------------------------------------------------------------------------
  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  --Graba en la tabla programmer la fecha de ejecucion del proceso
  OPEN C_Trx_In ;
  FETCH C_Trx_In INTO Ln_Seqno;
  CLOSE C_Trx_In;                                                                                                          
  i_Process_Programmer_t:=ttc_Process_Programmer_t(Process => i_process.Process,
                                                   Seqno => Ln_Seqno,
                                                   RunDate => LV_fecha_ejecucion,
                                                   LastModDate => LV_fecha_ejecucion
                                                   );
  LV_MENSAJE:= RC_Api_TimeToCash_Rule_ALL.Fct_Save_To_Process_Programmer(i_Process_Programmer_t);  
  LV_STATUS:='OKI';
  LV_MENSAJE:='RC_HEAD-SUCESSFUL';                 
  pv_error:=LV_MENSAJE;
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                               LD_FECHA_ACTUAL,
                                               SYSDATE,
                                               LV_STATUS,
                                               LV_MENSAJE);
   
  EXCEPTION
  WHEN LE_ERROR THEN
       LV_STATUS    := 'ERR';
      LV_MENSAJE   :='RC_Trx_TimeToCash_Reactive.Prc_Head:->'|| LV_MENSAJE;
      pv_error:=LV_MENSAJE;      
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------      
      lv_mensaje_apl_scp := '   Error en el Procesamiento';
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            pv_error,
                                            Null,
                                            3,
                                            Null,
                                            Null,
                                            Null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);      
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp, ln_error_scp, lv_error_scp);
      ----------------------------------------------------------------------------
      RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                   LD_FECHA_ACTUAL,
                                                   SYSDATE,
                                                   LV_STATUS,
                                                   pv_error);
      RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,GV_DESSHORT,0,LV_MENSAJE);
  WHEN OTHERS THEN
             LV_STATUS:='ERR';
             LV_MENSAJE:=LV_MENSAJE|| ' RC_Trx_TimeToCash_Reactive.Prc_Head:'  || 'ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 200);
             pv_error:=LV_MENSAJE;             
             RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(i_process.Process,
                                                          LD_FECHA_ACTUAL,
                                                          SYSDATE,
                                                          LV_STATUS,
                                                          pv_error);
             RC_API_TIMETOCASH_RULE_BSCS.RC_ELIMINAR_BCK(i_process.Process,GV_DESSHORT,GV_DESSHORT,0,LV_MENSAJE);
end;

procedure Prc_Customers_Cash_Dia(PN_ID_PROCESS        IN  NUMBER,
                                 pn_id_bitacora_scp   IN  NUMBER,
                                 PD_Fecha             in  date,
                                 pn_MaxRetenTrx       in number,
                                 PN_MAXTRX_EXE        in  number,
                                 PN_MAX_COMMIT        in  NUMBER,
                                 pn_cant_proc         out number,
                                 PN_SOSPID           IN NUMBER, 
                                 PV_OUT_ERROR         OUT VARCHAR2
                                 ) is
------------------------------------
--trae todas las lineas
cursor c_ttc_view_customers(cn_limit number) is
SELECT /*+ ALL_ROWS */ TTC_Customers_Count_React_Dia(Customer_id ,Custcode,bank_id,co_ext_csuin)
              FROM  ttc_view_customers_reactive
              where ROWNUM<= cn_limit;

cursor c_ttc_verif_custcode(cn_customer_id number)is
select /*+ ALL_ROWS */ custcode from customer_all d where customer_id=cn_customer_id;

CURSOR C_idtramite IS
  SELECT  TTC_idtramite.NEXTVAL FROM dual;

--VARIABLES
i_react                            TTC_Customers_React_Dia_tbk;
i_process                          ttc_process_t;
i_Rule_Values_t                    ttc_Rule_Values_t;
i_Rule_Values_mens                 ttc_Rule_Values_t;
i_ttc_bank_all_t                   ttc_bank_all_t;
i_Plannigstatus_In                 ttc_Plannigstatus_In_t;
lv_cuscode                         varchar2(24);
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR2(3);
LB_EXISTE_BCK                      boolean;
LN_COUNT_COMMIT                    NUMBER:=0;
LN_COUNT_MAXTRX                    NUMBER:=0;
LN_Customer_id                     NUMBER;
Ln_Secuence                        NUMBER;
Le_Error_Trx                       EXCEPTION;
LE_ERROR_MANUAL                    EXCEPTION;
LB_Check_Hour                      BOOLEAN;
Ln_bank_id                         INTEGER;
ln_check                           number:=0;
lv_status_trx                      varchar2(2);
lv_valid_reac                      varchar2(5);                        
ld_fecha                           DATE;
ln_cant_react                      NUMBER:=0;
--variables para reactivacion
LV_REMARK                          VARCHAR2(400);
LV_Sql_Planing                     VARCHAR2(400);
LV_Sql_PlanDet                     VARCHAR2(400);
LV_Tipo                            VARCHAR2(1);
LV_STATUS_OUT                      VARCHAR(1);
LN_SALDO_MIN                       NUMBER;
LN_SALDO_MIN_POR                   NUMBER;
LV_USER_AXIS                       TTC_WORKFLOW_USER_S.US_NAME%TYPE;
LV_Plan_Telefonia                  VARCHAR2(10);
lb_Reactiva                        BOOLEAN;
Ln_Status_A                        VARCHAR2(5);
Ln_Status_B                        VARCHAR2(5);
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------
begin      
  pn_cant_proc:=0;
  ld_fecha:= SYSDATE;
  
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_Customers_Cash_Dia:'||GV_DESSHORT;
  lv_mensaje_tec_scp:='Proceso Prc_Customers_Cash_Dia';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de reactivacion--------------------
  ---------------------------------------------------------------------------- 
  --Obtengo mensaje informativo del proceso de reactivacion
  i_Rule_Values_mens:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(PN_ID_PROCESS,gv_value_rule_mensaje);
  --Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
  LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
  
  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;
  
 --Verifica si la bandera esta en S para proceder a buscar el cuscode del cliente de la custome_all 
 i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(i_process.Process,GV_DES_FLAG_ValueTrx);          
 LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

 --Cargar los datos de la configuración 
 i_ttc_bank_all_t:=Fct_ttc_bank_all_t;
 i_Plannigstatus_In:=Fct_Plannigstatus_In_t;
 --Verifica si hay registros para procesar
 pn_cant_proc:=RC_CANT_TOTAL_REGISTRO(pv_table_name => 'ttc_view_customers_reactive');
 if 0=pn_cant_proc then
    raise NO_DATA_FOUND;
 end if;
 
 OPEN c_ttc_view_customers (pn_MaxRetenTrx);
 LOOP   
 FETCH c_ttc_view_customers
 BULK COLLECT INTO i_react LIMIT PN_MAXTRX_EXE;
     FOR rc_in IN (select /*+ ALL_ROWS */ * from table(cast( i_react as TTC_Customers_React_Dia_tbk)))  LOOP  
        BEGIN      
          lv_valid_reac:=null;
          LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;--Controla los cada cuantas transacciones hacer commit
          LN_COUNT_MAXTRX:=LN_COUNT_MAXTRX+1;--Controla la cuantas transacciones deben ejecutarce para verificar el cambio de hora    
          ln_registros_procesados_scp:=ln_registros_procesados_scp+1;                    
          LN_Customer_id:=rc_in.Customer_id;--para registrar las cuentas que salen con error                   
          lv_cuscode:=rc_in.Custcode;
          lv_status_trx:='V';
          if LV_Tipo = 'S' then
             --busca el cuscode del customer
             open c_ttc_verif_custcode(rc_in.Customer_id);
             fetch c_ttc_verif_custcode into lv_cuscode;             
             close c_ttc_verif_custcode;
             
             if lv_cuscode is null then
               PV_OUT_ERROR:='No se encuentra el cuscode del customer:'||rc_in.Customer_id;
               raise Le_Error_Trx;
             end if;
          end if;
          --Cargar la secuencia del trámite
            OPEN C_idtramite ;
            FETCH C_idtramite INTO Ln_Secuence;
            CLOSE C_idtramite;
                      
          --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
          --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
            BEGIN                   
                   SELECT /*+ ALL_ROWS */ BANK_ID INTO Ln_bank_id   
                      FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                   WHERE wkf_cust.BANK_ID=rc_in.bank_id;                                              
            EXCEPTION            
                  WHEN OTHERS THEN                     
                     Ln_bank_id:=0;
            END;   
            IF nvl(Ln_bank_id,0)=0 THEN    
                    --***************************************************************
                    Ln_Status_A:=NULL; Ln_Status_B:=NULL;
                    SELECT /*+ ALL_ROWS */ Pla.ST_STATUS_A, Pla.ST_STATUS_B , Pla.st_valid_reac INTO Ln_Status_A,Ln_Status_B,lv_valid_reac
                      FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                    WHERE Pla.pk_id=GV_PK_ID
                          AND Pla.st_status_i= rc_in.co_ext_csuin;
                    --***************************************************************
                    if lv_valid_reac='Y' then
                    lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => lv_cuscode,
                                                              PN_Customer_id => rc_in.Customer_id,
                                                              Pd_Date => PD_Fecha,                                                                                                                                                           
                                                              PN_CO_IDTRAMITE => Ln_Secuence,
                                                              PV_STATUS_P => GV_STATUS_P,
                                                              PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                              PV_STATUS_OUT  => LV_STATUS_OUT,
                                                              PV_Sql_Planing =>LV_Sql_Planing,
                                                              PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                              PV_REMARK =>  LV_REMARK,
                                                              PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                              PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                              PV_USER_AXIS => LV_USER_AXIS, 
                                                                  PV_CO_STATUS_A =>  Ln_Status_A, 
                                                                  PV_CO_STATUS_B =>  Ln_Status_B,
                                                                  PV_PERIOD    =>   rc_in.co_ext_csuin,--OJO buscar el correcto
                                                                  PV_REMARKTRX => i_Rule_Values_mens.Value_Return_ShortString,
                                                              PV_OUT_ERROR =>PV_OUT_ERROR );
                                                              
                  if lb_Reactiva and (PV_OUT_ERROR is not null) then                     
                     raise Le_Error_Trx;
                  end if;                  
                  if lb_Reactiva and (LV_STATUS_OUT = 'A') then                     
                     ln_cant_react:=ln_cant_react+1;
                  end if;
                  end if;
            end if;    
            IF INSTR(PV_OUT_ERROR, 'ERROR AL INSERTAR LA CUENTA EN REACTIVACION', 1, 1 )>0THEN
                lv_status_trx:='E';                                          
            END IF;
            ln_check:=Fct_check_cuentas(rc_in.Customer_id,
                                        rc_in.Custcode,
                                        rc_in.bank_id,
                                        lv_status_trx,
                                        rc_in.co_ext_csuin
                                        );    
            
            if ln_check=0 then
               --SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------              
              lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia en Fct_check_cuentas - Customer_id:'||LN_Customer_id;
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,Null,
                                                    'Customer_id',
                                                    'Co_id',
                                                    LN_Customer_id,
                                                    0,
                                                    'N',ln_error_scp,lv_error_scp);                                   
              ----------------------------------------------------------------------------
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                           PD_FECHA,
                                                           SYSDATE,
                                                           LV_STATUS,
                                                           lv_mensaje_apl_scp);
            end if;  
            if LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                     COMMIT; 
                     LN_COUNT_COMMIT:=0;
            END IF;
            ----------------------------------------------------------------------
            --Cada cuantas trx se debe preguntar si el proceso continua arriba o si es que se realizo un bajada de proceso manual                        
            IF LN_COUNT_MAXTRX >= PN_MAXTRX_EXE THEN                            
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                    ln_cant_react,
                                                    ln_registros_error_scp,
                                                    ln_error_scp,
                                                    lv_error_scp);
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,GV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);          
              IF LB_EXISTE_BCK = FALSE THEN
                EXIT;
              END IF;
              ----------------------------------------------------------------------------
              LB_Check_Hour := fnc_Check_Range_Hour(LV_MENSAJE);
              IF NOT LB_Check_Hour THEN
                RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK_REMOTO(PN_PROCESO => PN_ID_PROCESS,
                                                                   PV_DESSHORT => GV_DESSHORT,
                                                                   PN_FILTRO => GV_DESSHORT,
                                                                   PN_GROUPTREAD => 0,
                                                                   PV_OUT_ERROR =>LV_MENSAJE);
                                                                   
                LV_MENSAJE    := '--Por Verificación automatica de la hora de ejecución....';
                LB_EXISTE_BCK := FALSE;                
                EXIT;
              END IF;
              LN_COUNT_MAXTRX := 0;
            END IF;            
           EXCEPTION
              WHEN Le_Error_Trx THEN
              LV_STATUS:='ERR';
              LV_MENSAJE :='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia- '  || ' ERROR:'||PV_OUT_ERROR||
                                          ' para la cuenta -> Customer_id:'||LN_Customer_id;
              --SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              ln_registros_error_scp:=ln_registros_error_scp+1;
              lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia en Customer_id:'||LN_Customer_id;
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,Null,
                                                    'Customer_id',
                                                    'Co_id',
                                                    LN_Customer_id,
                                                    0,
                                                    'N',ln_error_scp,lv_error_scp);                                   
              ----------------------------------------------------------------------------
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                           PD_FECHA,
                                                           SYSDATE,
                                                           LV_STATUS,
                                                           LV_MENSAJE);
              WHEN OTHERS THEN 
                   ROLLBACK;
                   LV_STATUS:='ERR';
                   LV_MENSAJE :='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                             ' para la cuenta -> Customer_id:'||LN_Customer_id;
                   --SCP:MENSAJE
                   ----------------------------------------------------------------------
                   -- SCP: Código generado automáticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   ln_registros_error_scp:=ln_registros_error_scp+1;
                   lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia x customer';
                   scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         LV_MENSAJE,
                                                         Null,
                                                         2,Null,
                                                         'Customer_id',
                                                         'Co_id',
                                                         Ln_Customer_id,
                                                         0,
                                                         'N',ln_error_scp,lv_error_scp);                                   
                   ----------------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                PD_FECHA,
                                                                SYSDATE,
                                                                LV_STATUS,
                                                                LV_MENSAJE);

              
       end;
     end loop; 
     --***********
     COMMIT;
     --*********** 
     EXIT WHEN LB_EXISTE_BCK = FALSE; 
     EXIT WHEN c_ttc_view_customers%NOTFOUND;  
 END LOOP;         
 CLOSE c_ttc_view_customers; 
 IF LB_EXISTE_BCK=FALSE THEN
      RAISE LE_ERROR_MANUAL;
 END IF;  
 PV_OUT_ERROR:=NULL;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de reactivacion--------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_cant_react,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_TimeToCash_Send.Prc_Customers_Cash_Dia-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_Customers_Cash_Dia '||GV_DESSHORT;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                               ld_fecha,
                                               SYSDATE,
                                               'OKI',
                                               'Proceso Prc_Customers_Cash_Dia');
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);  
  
  WHEN LE_ERROR_MANUAL THEN                   
       PV_OUT_ERROR:='ABORTADO';
       If ln_registros_error_scp = 0 Then
          ln_registros_error_scp:=-1;
       End If;
       scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                            ln_cant_react,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;

procedure Prc_Customers_Out_Reloj(PN_ID_PROCESS        IN  NUMBER,
                                  pn_id_bitacora_scp   IN  NUMBER,
                                  PD_Fecha             in  date,
                                  pn_MaxRetenTrx       in number,
                                  PN_MAXTRX_EXE        in  number,
                                  PN_MAX_COMMIT        in  NUMBER,
                                  pn_cant_proc         out number, 
                                  PN_SOSPID     IN NUMBER,
                                  PV_OUT_ERROR         OUT VARCHAR2
                                  ) is
------------------------------------
--trae todas las lineas
cursor c_ttc_view_customers(cn_limit number) is
SELECT /*+ ALL_ROWS */ TTC_Customers_Count_React(Customer_id ,Custcode,bank_id)
              FROM  ttc_view_cust_reactive_all
              where ROWNUM<= cn_limit;

cursor c_ttc_verif_custcode(cn_customer_id number)is
select /*+ ALL_ROWS */ custcode from customer_all d where customer_id=cn_customer_id;

CURSOR C_idtramite IS
  SELECT  TTC_idtramite.NEXTVAL FROM dual;

--VARIABLES
i_react                            TTC_Customers_Count_React_tbk;
i_Rule_Values_t                    ttc_Rule_Values_t;
i_ttc_bank_all_t                   ttc_bank_all_t;
i_Rule_Values_mens                 ttc_Rule_Values_t;
i_Plannigstatus_In                 ttc_Plannigstatus_In_t;
i_Rule_Values_status               ttc_Rule_Values_t;
lv_cuscode                         varchar2(24);
LV_MENSAJE                         VARCHAR2(1000);
LE_ERROR                           EXCEPTION;
LV_STATUS                          VARCHAR2(3);
LB_EXISTE_BCK                      boolean;
LN_COUNT_COMMIT                    NUMBER:=0;
LN_COUNT_MAXTRX                    NUMBER:=0;
LN_Customer_id                     NUMBER;
Ln_Secuence                        NUMBER;
Le_Error_Trx                       EXCEPTION;
LE_ERROR_MANUAL                    EXCEPTION;
LB_Check_Hour                      BOOLEAN;
Ln_bank_id                         INTEGER;
LV_REMARK                          VARCHAR2(400);
LV_Sql_Planing                     VARCHAR2(400);
LV_Sql_PlanDet                     VARCHAR2(400);
LV_Tipo                            VARCHAR2(1);
LV_STATUS_OUT                      VARCHAR(1);
LN_SALDO_MIN                       NUMBER;
LN_SALDO_MIN_POR                   NUMBER;
LV_USER_AXIS                       TTC_WORKFLOW_USER_S.US_NAME%TYPE;
LV_Plan_Telefonia                  VARCHAR2(10);
lb_Reactiva                        BOOLEAN;
ln_check                           NUMBER:=0;
lv_status_trx                      VARCHAR2(2);
lv_valid_reac                      varchar2(5);
Ln_Status_A                        VARCHAR2(5);
Ln_Status_B                        VARCHAR2(5);
ld_fecha                           DATE;
ln_cant_react                      NUMBER:=0;
--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_registros_procesados_scp        number := 0;
ln_error_scp                       number:=0;
lv_error_scp                       varchar2(500);
lv_mensaje_apl_scp                 varchar2(4000);
lv_mensaje_tec_scp                 varchar2(4000);
ln_registros_error_scp             number:=0;
---------------------------------------------------------------
begin      
  pn_cant_proc:=0;  
  ld_fecha:=SYSDATE;
  i_Rule_Values_status:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(PN_ID_PROCESS,gv_value_rule_estatus_update);
  -----------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='INICIO Prc_Customers_Out_Reloj:'||GV_DESSHORT||'  PERIODO: --> '|| i_Rule_Values_status.Value_Return_ShortString;
  lv_mensaje_tec_scp:='Proceso Prc_Customers_Out_Reloj';
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  
  ----------------------------------------------------------------------------
  ---------------------------INICIO Logica de reactivacion--------------------
  ---------------------------------------------------------------------------- 
  --Obtengo mensaje informativo del proceso de reactivacion
  i_Rule_Values_mens:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(PN_ID_PROCESS,gv_value_rule_mensaje);   
  --Datos requeridos para el calculo del saldo
  LN_SALDO_MIN     := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MINIMO;
  LN_SALDO_MIN_POR := RC_API_TIMETOCASH_RULE_AXIS.RC_SALDO_MIN_POR;    
  LV_USER_AXIS  := RC_API_TIMETOCASH_RULE_ALL.FCT_TTC_WORKFLOW_USER_NAME('PAYMENT');
  
  --Obtiene el plan de telefonia publica para validar si el tmcode de la cuenta existe para no enviarla a reactivar
  LV_Plan_Telefonia:=RC_Api_TimeToCash_Rule_Axis.Fnc_Plan_Telefonia;
  
 --Verifica si la bandera esta en S para proceder a buscar el cuscode del cliente de la custome_all 
 i_Rule_Values_t:=RC_Api_TimeToCash_Rule_ALL.Fct_ttc_Rule_Value_t(PN_ID_PROCESS,GV_DES_FLAG_ValueTrx);          
 LV_Tipo:=nvl(i_Rule_Values_t.Value_Return_ShortString,'N');

 --Cargar los datos de la configuración 
 i_ttc_bank_all_t:=Fct_ttc_bank_all_t;
 i_Plannigstatus_In:=Fct_Plannigstatus_In_t;
 
 --Verifica si hay registros para procesar
 pn_cant_proc:=RC_CANT_TOTAL_REGISTRO(pv_table_name => 'ttc_view_cust_reactive_all');
 if 0=pn_cant_proc then
    raise NO_DATA_FOUND;
 end if;
 OPEN c_ttc_view_customers (pn_MaxRetenTrx);
 LOOP   
 FETCH c_ttc_view_customers
 BULK COLLECT INTO i_react LIMIT PN_MAXTRX_EXE;
     FOR rc_in IN (select /*+ ALL_ROWS */ * from table(cast( i_react as TTC_Customers_Count_React_tbk)))  LOOP  
        BEGIN      
          lv_valid_reac:=null;
          LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;--Controla los cada cuantas transacciones hacer commit
          LN_COUNT_MAXTRX:=LN_COUNT_MAXTRX+1;--Controla la cuantas transacciones deben ejecutarce para verificar el cambio de hora    
          ln_registros_procesados_scp:=ln_registros_procesados_scp+1;

          LN_Customer_id:=rc_in.Customer_id;--para registrar las cuentas que salen con error                   
          lv_cuscode:=rc_in.Custcode;
          lv_status_trx:='V';
          if LV_Tipo = 'S' then
             --busca el cuscode del customer
             open c_ttc_verif_custcode(rc_in.Customer_id);
             fetch c_ttc_verif_custcode into lv_cuscode;             
             close c_ttc_verif_custcode;
             
             if lv_cuscode is null then
               PV_OUT_ERROR:='No se encuentra el cuscode del customer:'||rc_in.Customer_id;
               raise Le_Error_Trx;
             end if;
          end if;
          --Cargar la secuencia del trámite
            OPEN C_idtramite ;
            FETCH C_idtramite INTO Ln_Secuence;
            CLOSE C_idtramite;
                      
          --Verifica si la cuenta posee una forma de pago configurada como prelegal, banco en tramite, cobranza, etc, 
          --para que no consulte la deuda sino que siga a la siguiente etapa del reloj
            BEGIN                   
                   SELECT /*+ ALL_ROWS */ BANK_ID INTO Ln_bank_id   
                      FROM table(cast(i_ttc_bank_all_t as ttc_bank_all_t))  wkf_cust 
                   WHERE wkf_cust.BANK_ID=rc_in.bank_id;                                              
            EXCEPTION            
                  WHEN OTHERS THEN                     
                     Ln_bank_id:=0;
            END;   
            IF nvl(Ln_bank_id,0)=0 THEN    
                    --***************************************************************
                    Ln_Status_A:=NULL; Ln_Status_B:=NULL;
                    SELECT /*+ ALL_ROWS */ Pla.ST_STATUS_A, Pla.ST_STATUS_B , Pla.st_valid_reac INTO Ln_Status_A,Ln_Status_B,lv_valid_reac
                      FROM table(cast(i_Plannigstatus_In as ttc_Plannigstatus_In_t)) Pla
                    WHERE Pla.pk_id=GV_PK_ID
                          AND Pla.st_status_i= i_Rule_Values_status.Value_Return_ShortString ;
                    --***************************************************************
                    if lv_valid_reac='Y' then
                       lb_Reactiva:=RC_Api_TimeToCash_Rule_Bscs.FNC_RC_REACTIVA(PV_Custcode => lv_cuscode,
                                                              PN_Customer_id => rc_in.Customer_id,
                                                              Pd_Date => PD_Fecha,                                                                                                                                                           
                                                              PN_CO_IDTRAMITE => Ln_Secuence,
                                                              PV_STATUS_P => GV_STATUS_P,
                                                              PV_Plan_Telefonia => LV_Plan_Telefonia, 
                                                              PV_STATUS_OUT  => LV_STATUS_OUT,
                                                              PV_Sql_Planing =>LV_Sql_Planing,
                                                              PV_Sql_PlanDet =>LV_Sql_PlanDet,
                                                              PV_REMARK =>  LV_REMARK,
                                                              PN_SALDO_MIN =>LN_SALDO_MIN,   
                                                              PN_SALDO_MIN_POR => LN_SALDO_MIN_POR,             
                                                              PV_USER_AXIS => LV_USER_AXIS, 
                                                              PV_CO_STATUS_A =>  Ln_Status_A, 
                                                              PV_CO_STATUS_B =>  Ln_Status_B,
                                                              PV_PERIOD    =>   i_Rule_Values_status.Value_Return_ShortString,
                                                              PV_REMARKTRX => i_Rule_Values_mens.Value_Return_ShortString,
                                                              PV_OUT_ERROR =>PV_OUT_ERROR );
                                                              
                        if lb_Reactiva and (PV_OUT_ERROR is not null) then                     
                          raise Le_Error_Trx;
                        end if;
                        if lb_Reactiva and (LV_STATUS_OUT = 'A') then                     
                           ln_cant_react:=ln_cant_react+1;
                        end if;
                     end if;
            end if;      
             --Inserta las cuentas procesadas en la tabla ttc_customers_check             
            IF INSTR(PV_OUT_ERROR, 'ERROR AL INSERTAR LA CUENTA EN REACTIVACION', 1, 1 )>0THEN
                lv_status_trx:='E';                                          
            END IF;
            ln_check:=Fct_check_cuentas(rc_in.Customer_id,
                                                                   rc_in.Custcode,
                                                                   rc_in.bank_id,
                                        lv_status_trx,
                                        i_Rule_Values_status.Value_Return_ShortString
                                                                   );
                                               
            if ln_check=0 then
               --SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------              
              lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Cash_Dia en Fct_check_cuentas - Customer_id:'||LN_Customer_id;
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,Null,
                                                    'Customer_id',
                                                    'Co_id',
                                                    LN_Customer_id,
                                                    0,
                                                    'N',ln_error_scp,lv_error_scp);                                   
              ----------------------------------------------------------------------------
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                           PD_FECHA,
                                                           SYSDATE,
                                                           LV_STATUS,
                                                           lv_mensaje_apl_scp);
            end if;  
            
            if LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                     COMMIT; 
                     LN_COUNT_COMMIT:=0;
            END IF;
            ----------------------------------------------------------------------
            --Cada cuantas trx se debe preguntar si el proceso continua arriba o si es que se realizo un bajada de proceso manual                        
            IF LN_COUNT_MAXTRX >= PN_MAXTRX_EXE THEN                       
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                                    ln_cant_react,
                                                    ln_registros_error_scp,
                                                    ln_error_scp,
                                                    lv_error_scp);
              RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK(PN_ID_PROCESS,GV_DESSHORT,GV_DESSHORT,0,PN_SOSPID,LB_EXISTE_BCK);          
              IF LB_EXISTE_BCK = FALSE THEN
                EXIT;
              END IF;
              ----------------------------------------------------------------------------
              LB_Check_Hour := fnc_Check_Range_Hour(LV_MENSAJE);
              IF NOT LB_Check_Hour THEN
                RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK_REMOTO(PN_PROCESO => PN_ID_PROCESS,
                                                                   PV_DESSHORT => GV_DESSHORT,
                                                                   PN_FILTRO => GV_DESSHORT,
                                                                   PN_GROUPTREAD => 0,
                                                                   PV_OUT_ERROR =>LV_MENSAJE);
                                                                   
                LV_MENSAJE    := '--Por Verificación automatica de la hora de ejecución....';
                LB_EXISTE_BCK := FALSE;
                EXIT;
              END IF;
              LN_COUNT_MAXTRX := 0;
            END IF;
           EXCEPTION
              WHEN Le_Error_Trx THEN
              LV_STATUS:='ERR';
              LV_MENSAJE :='RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj- '  || ' ERROR:'||PV_OUT_ERROR||
                                          ' para la cuenta -> Customer_id:'||LN_Customer_id;
              --SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              ln_registros_error_scp:=ln_registros_error_scp+1;
              lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj';
              scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    LV_MENSAJE,
                                                    Null,
                                                    2,Null,
                                                    'Customer_id',
                                                    'Co_id',
                                                    LN_Customer_id,
                                                    0,
                                                    'N',ln_error_scp,lv_error_scp);                                   
              ----------------------------------------------------------------------------
              RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                           PD_FECHA,
                                                           SYSDATE,
                                                           LV_STATUS,
                                                           LV_MENSAJE);
              WHEN OTHERS THEN 
                   ROLLBACK;
                   LV_STATUS:='ERR';
                   LV_MENSAJE :='RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100)||
                                             ' para la cuenta -> Customer_id:'||LN_Customer_id;
                   --SCP:MENSAJE
                   ----------------------------------------------------------------------
                   -- SCP: Código generado automáticamente. Registro de mensaje de error
                   ----------------------------------------------------------------------
                   ln_registros_error_scp:=ln_registros_error_scp+1;
                   lv_mensaje_apl_scp:='RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj x customer:'||LN_Customer_id;
                   scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                         lv_mensaje_apl_scp,
                                                         LV_MENSAJE,
                                                         Null,
                                                         2,Null,
                                                         'Customer_id',
                                                         'Co_id',
                                                         Ln_Customer_id,
                                                         0,
                                                         'N',ln_error_scp,lv_error_scp);                                   
                   ----------------------------------------------------------------------------
                   RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                                                PD_FECHA,
                                                                SYSDATE,
                                                                LV_STATUS,
                                                                LV_MENSAJE);

              
       end;
     end loop; 
     --***********
     COMMIT;
     --*********** 
     EXIT WHEN LB_EXISTE_BCK = FALSE; 
     EXIT WHEN c_ttc_view_customers%NOTFOUND;  
 END LOOP;         
 CLOSE c_ttc_view_customers;
 IF LB_EXISTE_BCK=FALSE THEN
      RAISE LE_ERROR_MANUAL;
 END IF;  
 PV_OUT_ERROR:=NULL;
  --------------------------------------------------------------------------------
  ---------------------------FIN Logica de reactivacion--------------------------- 
  --------------------------------------------------------------------------------
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                        ln_cant_react,
                                        ln_registros_error_scp,
                                        ln_error_scp,
                                        lv_error_scp);
  LV_MENSAJE:='RC_Trx_TimeToCash_Send.Prc_Customers_Out_Reloj-SUCESSFUL';  
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='FIN Prc_Customers_Out_Reloj '||GV_DESSHORT||'  PERIODO: --> '|| i_Rule_Values_status.Value_Return_ShortString;
  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
  --SCP:FIN  
  ----------------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de finalización de proceso
  ----------------------------------------------------------------------------
  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,ln_error_scp,lv_error_scp);  
  ----------------------------------------------------------------------------
  ---------------------------------------------------------------
  --Registro el avance del proceso
  ---------------------------------------------------------------
  RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND(PN_ID_PROCESS,
                                               ld_fecha,
                                               SYSDATE,
                                               'OKI',
                                               'Proceso Prc_Customers_Out_Reloj');
  ----------------------------------------------------------------
  PV_OUT_ERROR:=NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     PV_OUT_ERROR := '';
     
    scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,0,ln_error_scp,lv_error_scp);  
   ----------------------------------------------------------------------
   -- SCP: Código generado automáticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   lv_mensaje_apl_scp:='FIN Prc_Customers_Out_Reloj '||GV_DESSHORT||'  PERIODO: --> '|| i_Rule_Values_status.Value_Return_ShortString;
   scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                        lv_mensaje_apl_scp,
                                        lv_mensaje_tec_scp,
                                        Null,
                                        0,
                                        Gv_unidad_registro_scp,
                                        Null,
                                        Null,
                                        null,null,
                                        'N',ln_error_scp,lv_error_scp);
   --SCP:FIN
  WHEN LE_ERROR_MANUAL THEN                   
       PV_OUT_ERROR:='ABORTADO';
       If ln_registros_error_scp = 0 Then
          ln_registros_error_scp:=-1;
       End If;
       scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,
                                            ln_cant_react,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);

  WHEN OTHERS THEN
      ROLLBACK;
      LV_MENSAJE := 'RC_Trx_TimeToCash_Reactive.Prc_Customers_Out_Reloj- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 100);
      PV_OUT_ERROR:=LV_MENSAJE;
      If ln_registros_error_scp = 0 Then
         ln_registros_error_scp:=-1;
     End If;
     scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora_scp,Null,ln_registros_error_scp,ln_error_scp,lv_error_scp);                     
end;


end RC_Trx_TimeToCash_Reactivev;
/
