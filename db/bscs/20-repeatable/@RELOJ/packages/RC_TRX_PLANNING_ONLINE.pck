create or replace package reloj.RC_TRX_PLANNING_ONLINE is

  -- Author  : JJIMENEZ;JROMERO
  -- Created : 27/12/2013 15:32:30
  -- Purpose : 

  FUNCTION FCT_SAVE_PLANNING_HEAD(Fv_table VARCHAR2) RETURN VARCHAR2;

  FUNCTION FCT_SAVE_PLANNING_DETAILS(Fv_table VARCHAR2) RETURN VARCHAR2;

  Procedure PRC_HEADER(PN_COID        IN NUMBER,
                       PD_FECH_ACT    IN DATE,
                       PV_CSUIN       IN VARCHAR2,
                       PV_DN_NUM      IN VARCHAR2,
                       PV_USERLASTMOD IN VARCHAR2,
                       PN_RESULT      OUT NUMBER,
                       PV_ERROR       OUT VARCHAR2);

  Procedure PRC_PLANNING_IN(PV_DN_NUM   IN VARCHAR2,
                            PN_COID     IN NUMBER,
                            PV_CSUIN    IN VARCHAR2,
                            PD_FECH_ACT IN DATE,
                            PN_RESULT   OUT NUMBER,
                            PV_ERROR    OUT VARCHAR2);

  Procedure PRC_UPDATE_PLANNING(PN_COID        IN NUMBER,
                                PV_CSUIN       IN VARCHAR2,
                                PN_PK_ID       IN NUMBER,
                                PN_RE_ORDEN    IN NUMBER,
                                PD_FECH_ACT    IN DATE,
                                PV_USERLASTMOD IN VARCHAR2,
                                PN_RESULT      OUT NUMBER,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE PRC_PLANNING_OUT(PV_CO_ID     IN NUMBER,
                             PV_DN_NUM    IN VARCHAR2,
                             PV_RESPONSE  OUT NUMBER,
                             PV_PROC      OUT VARCHAR2,
                             PV_RESULTADO OUT VARCHAR2);


end RC_TRX_PLANNING_ONLINE;
/
create or replace package body reloj.RC_TRX_PLANNING_ONLINE is

  i_ContractPlannig_t        ttc_ContractPlannig_t;
  i_ContractPlannigDetails_t ttc_ContractPlannigDetails_t;

  Lv_tablas_alternas  VARCHAR2(1);
  Lv_planning_head    VARCHAR2(100);
  Lv_planning_details VARCHAR2(100);

  --Cursores
  Cursor C_COUNT_COID(CN_COID NUMBER) IS
    Select count(co_id) from ttc_contractplannig where co_id = CN_COID;

  Cursor C_REG_PLANNING(CN_COID NUMBER, CV_CSUIN VARCHAR2) IS
    Select pk_id, re_orden, ch_timeacciondate
      from ttc_contractplannigdetails
     where co_id = CN_COID
       and co_period = CV_CSUIN
       and co_statustrx = 'P';

  Cursor C_COUNT_COID_2(CN_COID NUMBER) IS
    Select count(co_id) from ttc_contractplannig_2 where co_id = CN_COID;

  Cursor C_REG_PLANNING_2(CN_COID NUMBER, CV_CSUIN VARCHAR2) IS
    Select pk_id, re_orden, ch_timeacciondate
      from ttc_contractplannigdetails_2
     where co_id = CN_COID
       and co_period = CV_CSUIN
       and co_statustrx = 'P';

  Cursor C_CONTRACT_CUSTOMER(CN_COID NUMBER) IS
    Select /*+ ALL_ROWS */
     decode(cu.customer_id_high, NULL, cu.customer_id, cu.customer_id_high) customer_id,
     cu.billcycle,
     cu.custcode,
     co.tmcode,
     cu.prgcode,
     cu.cstradecode
      from contract_all co, customer_all cu
     where co.co_id = CN_COID
       and cu.customer_id = co.customer_id
       and co.tmcode <> 268;

  -- Function and procedure implementations
  FUNCTION FCT_SAVE_PLANNING_HEAD(Fv_table VARCHAR2) RETURN VARCHAR2 IS
    Lv_Error VARCHAR2(800);
    Lv_sql   VARCHAR2(500);
  BEGIN
    Lv_Error := NULL;
    --Cabecera
    Lv_sql := 'INSERT INTO ' || Fv_table || ' VALUES (:1)';
  
    EXECUTE IMMEDIATE Lv_sql
      USING IN i_ContractPlannig_t;
  
    RETURN(Lv_Error);
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      Lv_Error := 'Fct_Save_Planning_Head-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ' ||
                  Fv_table || ' - ' || substr(SQLERRM, 1, 300);
      RETURN(Lv_Error);
    WHEN OTHERS THEN
      Lv_Error := 'Fct_Save_Planning_Head-Error General al intentar grabar en la tabla ' ||
                  Fv_table || ' - ' || substr(SQLERRM, 1, 300);
      RETURN(Lv_Error);
  END;

  FUNCTION FCT_SAVE_PLANNING_DETAILS(Fv_table VARCHAR2) RETURN VARCHAR2 IS
    Lv_Error VARCHAR2(800);
    Lv_sql   VARCHAR2(500);
  BEGIN
    Lv_Error := NULL;
    --Detalle
    Lv_sql := 'INSERT INTO ' || Fv_table || ' VALUES (:1)';
  
    EXECUTE IMMEDIATE Lv_sql
      USING IN i_ContractPlannigDetails_t;
  
    RETURN(Lv_Error);
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      Lv_Error := 'Fct_Save_Planning_Details-DUP_VAL_ON_INDEX, Error al intentar grabar en la tabla ' ||
                  Fv_table || ' - ' || substr(SQLERRM, 1, 300);
      RETURN(Lv_Error);
    WHEN OTHERS THEN
      Lv_Error := 'Fct_Save_Planning_Details-Error General al intentar grabar en la tabla ' ||
                  Fv_table || ' - ' || substr(SQLERRM, 1, 300);
      RETURN(Lv_Error);
  END;

  Procedure PRC_HEADER(PN_COID        IN NUMBER,
                       PD_FECH_ACT    IN DATE,
                       PV_CSUIN       IN VARCHAR2,
                       PV_DN_NUM      IN VARCHAR2,
                       PV_USERLASTMOD IN VARCHAR2 DEFAULT NULL,
                       PN_RESULT      OUT NUMBER,
                       PV_ERROR       OUT VARCHAR2) is
  
    lc_reg_planning C_REG_PLANNING%ROWTYPE;
  
    --Variables  
    Ln_co_id      number:=PN_COID;
    ln_count_coid NUMBER := 0;
    --lv_dn_num     VARCHAR2(12);
    Le_Error_Scp EXCEPTION;
    Le_Error_Valid EXCEPTION;
  
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_id_proceso_scp  varchar2(100) := 'PLANIFICACION_ON_LINE';
    ln_error_scp       number := 0;
    lv_error_scp       varchar2(500);
    lv_descripcion_scp Varchar2(500);
  
  begin
    --Leer bandera que indica si se deben usar las tablas alternas
    scp.sck_api.scp_parametros_procesos_lee('TABLAS_ALTERNAS',
                                            lv_id_proceso_scp,
                                            Lv_tablas_alternas,
                                            lv_descripcion_scp,
                                            ln_error_scp,
                                            lv_error_scp);
  
    If ln_error_scp <> 0 Then
      PV_ERROR := 'ERROR EN APLICACION SCP, NO SE PUDO CARGAR PARAMETRO TABLAS_ALTERNAS >> ' ||
                  lv_error_scp;
      RAISE Le_Error_Scp;
    End If;
  
    --Verifico los parametros de entrada
    IF PN_COID IS NULL THEN
      SELECT co_id into Ln_co_id FROM contr_services_cap ctr  
         WHERE dn_id IN (SELECT dn_id FROM directory_number 
                         WHERE dn_num=PV_DN_NUM)
         AND ctr.cs_deactiv_date IS NULL;
    END IF;
  
    IF PV_CSUIN IS NULL THEN
      PV_ERROR := 'El csuin no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
  
    --Verifico si el co_id se encuentra planificado
    IF Lv_tablas_alternas = 'S' THEN
      OPEN C_COUNT_COID_2(Ln_co_id);
      FETCH C_COUNT_COID_2
        INTO ln_count_coid;
      CLOSE C_COUNT_COID_2;
    ELSE
      OPEN C_COUNT_COID(Ln_co_id);
      FETCH C_COUNT_COID
        INTO ln_count_coid;
      CLOSE C_COUNT_COID;
    END IF;
  
    IF ln_count_coid > 0 THEN
      --Consulto timeacciondate del registro de la planificacion correspondiente al csuin
      IF Lv_tablas_alternas = 'S' THEN
        OPEN C_REG_PLANNING_2(Ln_co_id, PV_CSUIN);
        FETCH C_REG_PLANNING_2
          INTO lc_reg_planning;
        CLOSE C_REG_PLANNING_2;
      ELSE
        OPEN C_REG_PLANNING(Ln_co_id, PV_CSUIN);
        FETCH C_REG_PLANNING
          INTO lc_reg_planning;
        CLOSE C_REG_PLANNING;
      END IF;
    
      IF lc_reg_planning.ch_timeacciondate > SYSDATE THEN
        PRC_UPDATE_PLANNING(PN_COID        => Ln_co_id,
                            PV_CSUIN       => PV_CSUIN,
                            PN_PK_ID       => lc_reg_planning.PK_ID,
                            PN_RE_ORDEN    => lc_reg_planning.RE_ORDEN,
                            PD_FECH_ACT    => PD_FECH_ACT,
                            PV_USERLASTMOD => PV_USERLASTMOD,
                            PN_RESULT      => PN_RESULT,
                            PV_ERROR       => PV_ERROR);
      ELSE
        PV_ERROR  := 'PRC_HEADER --> El co_id:' || Ln_co_id ||
                     ' ya se encuentra planificado y no se debe actualizar su planificacion';
        PN_RESULT := 0;
      END IF;
    
    ELSE
      /*
      --Transforma numero a formato de 12 digitos 
      lv_dn_num := sysadm.OBTIENE_TELEFONO_DNC_INT(PV_DN_NUM, null, 'N');
    
      PRC_PLANNING_IN(PV_DN_NUM   => lv_dn_num,
                      PN_COID     => PN_COID,
                      PV_CSUIN    => PV_CSUIN,
                      PD_FECH_ACT => PD_FECH_ACT,
                      PN_RESULT   => PN_RESULT,
                      PV_ERROR    => PV_ERROR);
      */
      PRC_PLANNING_IN(PV_DN_NUM   => PV_DN_NUM,
                      PN_COID     => Ln_co_id,
                      PV_CSUIN    => PV_CSUIN,
                      PD_FECH_ACT => PD_FECH_ACT,
                      PN_RESULT   => PN_RESULT,
                      PV_ERROR    => PV_ERROR);
    END IF;
  
    COMMIT;
  
  exception
    when Le_Error_Valid then
      PV_ERROR  := 'PRC_HEADER --> '|| PV_ERROR;
      PN_RESULT := 0;
    when Le_Error_Scp then
      PV_ERROR  := 'PRC_HEADER --> co_id:' || Ln_co_id ||' y el servicio: ' ||
                   PV_DN_NUM || ' - ' || PV_ERROR;
      PN_RESULT := 1;
    when others then
      PV_ERROR  := 'PRC_HEADER --> Error General al procesar el servicio: ' ||
                   PV_DN_NUM ||'y el co_id :'||Ln_co_id|| ' - ' || substr(sqlerrm, 1, 300);
      PN_RESULT := 1;
  end;

  Procedure PRC_PLANNING_IN(PV_DN_NUM   IN VARCHAR2,
                            PN_COID     IN NUMBER,
                            PV_CSUIN    IN VARCHAR2,
                            PD_FECH_ACT IN DATE,
                            PN_RESULT   OUT NUMBER,
                            PV_ERROR    OUT VARCHAR2) is
  
    CURSOR C_idtramite IS
      SELECT TTC_idtramite.NEXTVAL FROM dual;
  
    lc_inf_coid C_CONTRACT_CUSTOMER%ROWTYPE;
    doc_Wfk     TTC_VIEW_PLANNIGSTATUS_IN%ROWTYPE;
  
    lb_found       BOOLEAN := FALSE;
    Lb_Flag_Valida BOOLEAN := FALSE;
    Lv_mensaje     VARCHAR2(300);
    Lv_User_Axis   TTC_WorkFlow_User.Us_Name%TYPE;
    Lv_User_Bscs   TTC_WorkFlow_User.Us_Name%TYPE;
    Lv_User_Espejo TTC_WorkFlow_User.Us_Name%TYPE;
    Lv_Id_ciclo    Fa_Ciclos_Axis_Bscs.Id_Ciclo%TYPE;
    Ln_Secuence    NUMBER;
    Ln_pk_id       NUMBER;
    Ln_re_orden    NUMBER;
    Ln_count_serv  NUMBER;
    Ln_Sum_Time    NUMBER;
    Ld_Fecha       DATE;
    Ld_Sysdate     DATE;
    Lv_Des_Error   VARCHAR2(800);
    Le_Error_Head EXCEPTION;
    Le_Error_Details EXCEPTION;
    Le_Error_Valid EXCEPTION;
  
  begin
    
    --Verifico los parametros de entrada
    IF PN_COID IS NULL THEN
         PV_ERROR := 'El co_id no puede ser nulo';
       RAISE Le_Error_Valid;
    END IF;
  
    IF PV_CSUIN IS NULL THEN
      PV_ERROR := 'El csuin no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
    
    --Obtengo los datos necesarios del co_id
    OPEN C_CONTRACT_CUSTOMER(PN_COID);
    FETCH C_CONTRACT_CUSTOMER
      INTO lc_inf_coid;
    lb_found := C_CONTRACT_CUSTOMER%FOUND;
    CLOSE C_CONTRACT_CUSTOMER;
  
  
    IF lb_found THEN     
       
      Ld_Sysdate := to_date(SYSDATE, 'dd/mm/rrrr');
      
      --Cargar los datos del Usuario de Procesamiento           
      Lv_User_Axis   := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_Name('AXIS');
      Lv_User_Bscs   := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_Name('BSCS');
      Lv_User_Espejo := RC_Api_TimeToCash_Rule_ALL.Fct_ttc_WorkFlow_User_Name('ESPEJO');
    
      IF Lv_tablas_alternas = 'S' THEN
        Lv_planning_head    := 'TTC_CONTRACTPLANNIG_2';
        Lv_planning_details := 'TTC_CONTRACTPLANNIGDETAILS_2';
      ELSE
        Lv_planning_head    := 'TTC_CONTRACTPLANNIG';
        Lv_planning_details := 'TTC_CONTRACTPLANNIGDETAILS';
      END IF;
    
      BEGIN
        --Setea la bandera de validaci�n de la configuraci�n de los par�metros  
        Lb_Flag_Valida := TRUE;
      
        --***************************************************************
        Lv_mensaje := 'cstradecode:' || lc_inf_coid.cstradecode ||
                      ' y prgcode:' || lc_inf_coid.prgcode ||
                      ' en la tabla TTC_WorkFlow_Customer';
      
        SELECT pk_id
          INTO Ln_pk_id
          FROM TTC_WorkFlow_Customer wkf_cust
         WHERE wkf_cust.cstradecode = lc_inf_coid.cstradecode
           AND wkf_cust.prgcode = lc_inf_coid.prgcode;
        --***************************************************************
        Lv_mensaje := 'pk_id:' || Ln_pk_id || ' y st_status_i:' || PV_CSUIN ||
                      ' en la tabla TTC_VIEW_PLANNIGSTATUS_IN';
      
        SELECT min(Pla.re_orden)
          INTO Ln_re_orden
          FROM TTC_VIEW_PLANNIGSTATUS_IN Pla
         WHERE Pla.pk_id = Ln_pk_id
           AND Pla.st_status_i = PV_CSUIN;
        --***************************************************************
        Lv_mensaje := 'id_ciclo_admin:' || lc_inf_coid.Billcycle ||
                      ' en la tabla Fa_Ciclos_Axis_Bscs';
      
        SELECT Id_ciclo
          INTO Lv_Id_ciclo
          FROM Fa_Ciclos_Axis_Bscs Fa
         WHERE Fa.id_ciclo_admin = lc_inf_coid.Billcycle;
        --***************************************************************
        Lv_mensaje := 'pk_id:' || Ln_pk_id || ' y re_orden:' || Ln_re_orden ||
                      ' en la tabla TTC_VIEW_PLANNIGSTATUS_IN';
      
        SELECT *
          INTO doc_Wfk
          FROM TTC_VIEW_PLANNIGSTATUS_IN Pla
         WHERE Pla.pk_id = Ln_pk_id
           AND Pla.re_orden = Ln_re_orden;
        --***************************************************************                                           
        Ld_Fecha := to_date(nvl(PD_FECH_ACT, SYSDATE) +
                            nvl(doc_Wfk.st_duration_total, 0),
                            'dd/mm/rrrr');
        IF Ld_Fecha < Ld_Sysdate THEN
          Ld_Fecha := Ld_Sysdate;
        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
          Lb_Flag_Valida := FALSE;
          PV_ERROR       := 'PRC_PLANNING_IN --> Error General al consultar los regsitros con ' ||
                            lv_mensaje || ' - ' || substr(sqlerrm, 1, 300);
          PN_RESULT      := 1;
      END;
    
      IF Lb_Flag_Valida THEN
        BEGIN
          --Cargar la secuencia del tr�mite
          OPEN C_idtramite;
          FETCH C_idtramite
            INTO Ln_Secuence;
          CLOSE C_idtramite;
        
          --Cabecera 
          SAVEPOINT A;
          i_ContractPlannig_t := ttc_ContractPlannig_t(Customer_id   => lc_inf_coid.Customer_id,
                                                       Co_id         => PN_COID,
                                                       Dn_Num        => PV_DN_NUM,
                                                       Co_Period     => PV_CSUIN,
                                                       Co_Status_A   => doc_Wfk.st_status_a,
                                                       Ch_Status_B   => doc_Wfk.st_status_b,
                                                       Co_Reason     => doc_Wfk.st_reason,
                                                       Co_TimePeriod => doc_Wfk.ST_STATUS_NEXT,
                                                       Ch_ValidFrom  => Ld_Fecha,
                                                       Tmcode        => lc_inf_coid.Tmcode,
                                                       Prgcode       => lc_inf_coid.Prgcode,
                                                       AdminCycle    => Lv_Id_ciclo,
                                                       Billcycle     => lc_inf_coid.Billcycle,
                                                       NoVerify      => 0,
                                                       InsertionDate => SYSDATE,
                                                       LastModDate   => NULL);
          Lv_Des_Error        := Fct_Save_Planning_Head(Lv_planning_head);
          IF Lv_Des_Error IS NOT NULL THEN
            RAISE Le_Error_Head;
          END IF;
        
          Ln_Sum_Time := 0;
          --Cargar el valor estimado de la transacci�n el costo de ejecuci�n
          Ln_count_serv := RC_Trx_TimeToCash_Customize.fct_Return_Count_services(PN_COID);
        
          FOR rc_wfk in (SELECT *
                           FROM TTC_VIEW_PLANNIGSTATUS_IN Pla
                          WHERE Pla.pk_id = Ln_pk_id
                            AND Pla.re_orden >= Ln_re_orden) LOOP
          
            Ln_Sum_Time := Ln_Sum_Time + nvl(rc_wfk.st_duration_total, 0);
            Ld_Fecha    := to_date(PD_FECH_ACT + Ln_Sum_Time, 'dd/mm/rrrr');
            IF Ld_Fecha < Ld_Sysdate THEN
              Ld_Fecha := Ld_Sysdate;
            END IF;
            --Detalle                                              
            i_ContractPlannigDetails_t := ttc_ContractPlannigDetails_t(Customer_id       => lc_inf_coid.Customer_id,
                                                                       Co_id             => PN_COID,
                                                                       PK_Id             => rc_wfk.pk_id,
                                                                       ST_Id             => rc_wfk.ST_Id,
                                                                       ST_Seqno          => rc_wfk.ST_Seqno,
                                                                       RE_Orden          => rc_wfk.RE_Orden,
                                                                       Custcode          => lc_inf_coid.Custcode,
                                                                       Dn_Num            => PV_DN_NUM,
                                                                       Co_Period         => rc_wfk.ST_STATUS_NEXT,
                                                                       Co_Status_A       => rc_wfk.st_status_a,
                                                                       Ch_Status_B       => rc_wfk.st_status_b,
                                                                       Co_Reason         => rc_wfk.st_reason,
                                                                       Co_TimePeriod     => rc_wfk.st_duration_total,
                                                                       Ch_TimeAccionDate => Ld_Fecha,
                                                                       Tmcode            => lc_inf_coid.Tmcode,
                                                                       Prgcode           => lc_inf_coid.Prgcode,
                                                                       AdminCycle        => Lv_Id_ciclo,
                                                                       Billcycle         => lc_inf_coid.Billcycle,
                                                                       ValueTrx          => Ln_count_serv,
                                                                       OrderByTrx        => 0,
                                                                       Co_IdTramite      => Ln_Secuence,
                                                                       Co_Remark         => rc_wfk.Co_Remark,
                                                                       Co_UserTrx_1      => Lv_User_Axis,
                                                                       Co_UserTrx_2      => Lv_User_Bscs,
                                                                       Co_UserTrx_3      => Lv_User_Espejo,
                                                                       Co_CommandTrx_1   => NULL,
                                                                       Co_CommandTrx_2   => NULL,
                                                                       Co_CommandTrx_3   => NULL,
                                                                       Co_NoIntento      => 1,
                                                                       Co_StatusTrx      => nvl(rc_wfk.ST_VALID,
                                                                                                'P'),
                                                                       Co_RemarkTrx      => NULL,
                                                                       InsertionDate     => nvl(rc_wfk.co_LastModDate,
                                                                                                SYSDATE),
                                                                       LastModDate       => rc_wfk.co_LastModDate);
            Lv_Des_Error               := Fct_Save_Planning_Details(Lv_planning_details);
            IF Lv_Des_Error IS NOT NULL THEN
              RAISE Le_Error_Details;
            END IF;
          END LOOP;
          --***********--
          PV_ERROR := 'PRC_PLANNING_IN- Se planificaron las suspensiones para el customer_id:' ||
                      lc_inf_coid.Customer_id || ' y Co_id:' || PN_COID;
        
          PN_RESULT := 0;
        EXCEPTION
          WHEN Le_Error_Head THEN
            PV_ERROR  := 'PRC_PLANNING_IN-' || Lv_Des_Error || ',' ||
                         'customer_id:' || lc_inf_coid.Customer_id ||
                         ',Co_id:' || PN_COID;
            PN_RESULT := 0;
          
          WHEN Le_Error_Details THEN
            ROLLBACK TO A;
            PV_ERROR  := 'PRC_PLANNING_IN-' || Lv_Des_Error || ',' ||
                         'customer_id:' || lc_inf_coid.Customer_id ||
                         ',Co_id:' || PN_COID;
            PN_RESULT := 1;
          
          WHEN OTHERS THEN
            ROLLBACK TO A;
            PV_ERROR  := 'PRC_PLANNING_IN-Error al intentar grabar registros en las tablas ' ||
                         Lv_planning_head || ' y ' || Lv_planning_details ||
                         ' customer_id:' || lc_inf_coid.Customer_id ||
                         ',Co_id:' || PN_COID || ',' ||
                         substr(SQLERRM, 1, 300);
            PN_RESULT := 1;
          
        END;
      END IF;
    
    ELSE
      PV_ERROR  := 'PRC_PLANNING_IN --> No se encontro informacion del co_id:' ||
                   PN_COID || ' en las tablas CONTRACT_ALL y CUSTOMER_ALL';
      PN_RESULT := 0;
    END IF;
  
  exception
    when Le_Error_Valid then
      PV_ERROR  := 'PRC_PLANNING_IN --> '|| PV_ERROR;
      PN_RESULT := 0;
    when others then
      PV_ERROR  := 'PRC_PLANNING_IN --> Error General al planialcar el co_id:' ||
                   PN_COID || ' - ' || substr(sqlerrm, 1, 300);
      PN_RESULT := 1;
  end;

  Procedure PRC_UPDATE_PLANNING(PN_COID        IN NUMBER,
                                PV_CSUIN       IN VARCHAR2,
                                PN_PK_ID       IN NUMBER,
                                PN_RE_ORDEN    IN NUMBER,
                                PD_FECH_ACT    IN DATE,
                                PV_USERLASTMOD IN VARCHAR2,
                                PN_RESULT      OUT NUMBER,
                                PV_ERROR       OUT VARCHAR2) is
  
    Ln_Sum_Time NUMBER;
    Ld_Sysdate  DATE;
    Ld_Fecha    DATE;
    Lv_Sql_1    VARCHAR2(200);
    Lv_Sql_2    VARCHAR2(200);
    Ln_re_orden NUMBER;
    Le_Error_Valid EXCEPTION;
  
    Lb_Flag_Valida BOOLEAN := FALSE;
  
  begin
    Ld_Sysdate     := to_date(SYSDATE, 'dd/mm/rrrr');
    Lb_Flag_Valida := TRUE;
    
    --Verifico los parametros de entrada
    IF PN_COID IS NULL THEN
      PV_ERROR := 'El co_id no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
  
    IF PV_CSUIN IS NULL THEN
      PV_ERROR := 'El csuin no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
    
    IF PN_PK_ID IS NULL THEN
      PV_ERROR := 'El pk_id no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
    
    IF PN_RE_ORDEN IS NULL THEN
      PV_ERROR := 'El re_orden no puede ser nulo';
      RAISE Le_Error_Valid;
    END IF;
  
    IF Lv_tablas_alternas = 'S' THEN
      Lv_planning_details := 'TTC_CONTRACTPLANNIGDETAILS_2';
    ELSE
      Lv_planning_details := 'TTC_CONTRACTPLANNIGDETAILS';
    END IF;
  
    Lv_Sql_1 := 'UPDATE ' || Lv_planning_details ||
                ' SET CO_REMARKTRX=''Registro modificado por transacciones directas del Operador --> ' ||
                PV_USERLASTMOD ||
                ''',CO_STATUSTRX=''F'', LASTMODDATE=:1 WHERE co_id=:2 and re_orden=:3';
  
    Lv_Sql_2 := 'UPDATE ' || Lv_planning_details ||
                ' SET ch_timeacciondate=:1 WHERE co_id=:2 and re_orden=:3';
  
    BEGIN
    
      SELECT min(Pla.re_orden)
        INTO Ln_re_orden
        FROM TTC_VIEW_PLANNIGSTATUS_IN Pla
       WHERE Pla.pk_id = PN_PK_ID
         AND Pla.st_status_i = PV_CSUIN;
    
    EXCEPTION
      WHEN OTHERS THEN
        Lb_Flag_Valida := FALSE;
        PV_ERROR       := 'PRC_UPDATE_PLANNING --> Error General al consultar el re_orden para pk_id:' ||
                          PN_PK_ID || ' y st_status_i:' || PV_CSUIN ||
                          ' en la tabla TTC_VIEW_PLANNIGSTATUS_IN' || ' - ' ||
                          substr(sqlerrm, 1, 300);
        PN_RESULT      := 1;
    END;
  
    IF Lb_Flag_Valida THEN
      BEGIN
        --Actualizo lastmoddate del registro modificado por el operador
        EXECUTE IMMEDIATE Lv_Sql_1
          USING PD_FECH_ACT, PN_COID, PN_RE_ORDEN;
      
        Ln_Sum_Time := 0;
        FOR rc_wfk in (SELECT *
                         FROM TTC_VIEW_PLANNIGSTATUS_IN Pla
                        WHERE Pla.pk_id = PN_PK_ID
                          AND Pla.re_orden >= Ln_re_orden) LOOP
        
          Ln_Sum_Time := Ln_Sum_Time + nvl(rc_wfk.st_duration_total, 0);
          Ld_Fecha    := to_date(PD_FECH_ACT + Ln_Sum_Time, 'dd/mm/rrrr');
          IF Ld_Fecha < Ld_Sysdate THEN
            Ld_Fecha := Ld_Sysdate;
          END IF;
        
          --Actualizo el ch_timeacciondate de las siguientes suspensiones
          EXECUTE IMMEDIATE Lv_Sql_2
            USING Ld_Fecha, PN_COID, rc_wfk.RE_ORDEN;
        END LOOP;
      
        PV_ERROR  := 'PRC_UPDATE_PLANNING --> Se actualizo la planificacion del co_id:' ||
                     PN_COID;
        PN_RESULT := 0;
      
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR  := 'PRC_UPDATE_PLANNING --> Error General al actualizar la planificacion del co_id:' ||
                       PN_COID || ' - ' || substr(sqlerrm, 1, 300);
          PN_RESULT := 1;
      END;
    END IF;
  
  exception
    when Le_Error_Valid then
      PV_ERROR  := 'PRC_UPDATE_PLANNING --> '|| PV_ERROR;
      PN_RESULT := 0;
    when others then
      PV_ERROR  := 'PRC_UPDATE_PLANNING --> Error General al actualizar la planificacion del co_id:' ||
                   PN_COID || ' - ' || substr(sqlerrm, 1, 300);
      PN_RESULT := 1;
  end;

  /*=============================PROCEDIMIENTO DESPLANIFICADOR===============================*/

  PROCEDURE PRC_PLANNING_OUT(PV_CO_ID     IN NUMBER,
                             PV_DN_NUM    IN VARCHAR2,
                             PV_RESPONSE  OUT NUMBER,
                             PV_PROC      OUT VARCHAR2,
                             PV_RESULTADO OUT VARCHAR2) IS
  
    --Variables locales
    lv_id_proceso_scp  varchar2(100) := 'PLANIFICACION_ON_LINE';
    Lv_sql             varchar2(1000);
    lv_error_scp       varchar2(500);
    lv_descripcion_scp Varchar2(500);
    Lv_tablas_alternas VARCHAR2(1);
    Lv_co_id           number:=PV_CO_ID;
    Ln_co_id           number;
    ln_error_scp       number := 0;
    Le_Error_Scp EXCEPTION;
  
  BEGIN
    PV_RESPONSE := 0;
    PV_PROC     := 'RC_TRX_PLANNING_ONLINE.PRC_ PLANNING_OUT';
    --Verifica si se recibio el co_id
    IF PV_CO_ID IS NULL THEN
      SELECT co_id into Lv_co_id FROM contr_services_cap ctr  
         WHERE dn_id IN (SELECT dn_id FROM directory_number 
                         WHERE dn_num=PV_DN_NUM)
         AND ctr.cs_deactiv_date IS NULL;
    END IF;
  
    --Leer bandera que indica si se deben usar las tablas alternas
    scp.sck_api.scp_parametros_procesos_lee('TABLAS_ALTERNAS',
                                            lv_id_proceso_scp,
                                            Lv_tablas_alternas,
                                            lv_descripcion_scp,
                                            ln_error_scp,
                                            lv_error_scp);
  
    If ln_error_scp <> 0 Then
      RAISE Le_Error_Scp;
    End If;
  
    --Verifica si el co_id se encuentra planificado
    IF Lv_tablas_alternas = 'S' THEN
      open C_COUNT_COID_2(Lv_co_id);
      fetch C_COUNT_COID_2
        into Ln_co_id;
      close C_COUNT_COID_2;
    ELSE
      open C_COUNT_COID(Lv_co_id);
      fetch C_COUNT_COID
        into Ln_co_id;
      close C_COUNT_COID;
    END IF;
    if Ln_co_id <= 0 then
      PV_RESULTADO := 'El co_id: ' || Lv_co_id ||
                      ' no se encuentra planificado';
      PV_RESPONSE  := 1;
    else
      begin
        --Determina donde realizar� la desplanificacion
        IF Lv_tablas_alternas = 'S' THEN
          Lv_planning_head    := 'RELOJ.TTC_CONTRACTPLANNIG_2';
          Lv_planning_details := 'RELOJ.TTC_CONTRACTPLANNIGDETAILS_2';
        ELSE
          Lv_planning_head    := 'RELOJ.TTC_CONTRACTPLANNIG';
          Lv_planning_details := 'RELOJ.TTC_CONTRACTPLANNIGDETAILS';
        END IF;
        --Realiza la desplanificacion
        Lv_sql := 'DELETE FROM ' || Lv_planning_head || ' WHERE CO_ID = ' ||
                  Lv_co_id;
        EXECUTE IMMEDIATE Lv_sql;
      
        Lv_sql := 'DELETE FROM ' || Lv_planning_details ||
                  ' WHERE CO_ID = ' || Lv_co_id;
        EXECUTE IMMEDIATE Lv_sql;
      
        commit;
        PV_RESULTADO := 'Desplanificacion exitosa para el co_id: ' ||
                        Lv_co_id;
      exception
        when others then
          PV_RESULTADO := ' Error al desplanificar el co_id: ' || Lv_co_id || ': ' ||
                          sqlerrm;
          PV_RESPONSE  := 1;
          ROLLBACK;
      end;
    end if;
     
  EXCEPTION
    
    WHEN Le_Error_Scp THEN
      PV_RESULTADO := 'ERROR EN APLICACION SCP, NO SE PUDO CARGAR PARAMETRO TABLAS_ALTERNAS >> ' ||
                      lv_error_scp;
      PV_RESPONSE  := 1;
      
    WHEN OTHERS THEN
      PV_RESULTADO := 'Error general: ' || sqlerrm;
      PV_RESPONSE  := 1;
  
  END PRC_PLANNING_OUT;

end RC_TRX_PLANNING_ONLINE;
/
