CREATE OR REPLACE FUNCTION Tessera (Pv_cadena VARCHAR2)RETURN VARCHAR2 is
  lv_resultado VARCHAR2(100);
  lv_sentencia VARCHAR2(200);
begin
  lv_resultado:=iam_bscs.Tessera@iam_oper(Pv_cadena); 
  
  commit; 
  lv_sentencia := 'ALTER SESSION CLOSE DATABASE LINK IAM_OPER';
  execute immediate lv_sentencia;
  
  return(lv_resultado);
end TESSERA;

/
