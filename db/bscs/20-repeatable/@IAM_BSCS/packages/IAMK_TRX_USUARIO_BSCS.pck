CREATE OR REPLACE PACKAGE IAMK_TRX_USUARIO_BSCS IS

  -- valida si existe usuario de base de datos
  CURSOR C_EXISTE_USER_DB(CV_USUARIO VARCHAR2) IS
    SELECT 1 OK FROM DBA_USERS X WHERE X.USERNAME = UPPER(TRIM(CV_USUARIO));

  -- valida si existe usuario de aplicacion --MODIFICADO
  CURSOR C_EXISTE_USER_APP(CV_USUARIO VARCHAR2) IS
    SELECT 1 OK FROM USERS X WHERE UPPER(X.USERNAME) = UPPER(TRIM(CV_USUARIO));

  -- verifica existencia de ip
  CURSOR C_EXISTE_IP(PC_IP IN VARCHAR2, PC_METODO IN VARCHAR2) IS
    SELECT 'X'
      FROM IAM_IP_USUARIOS
     WHERE DIRECCION_IP = TRIM(PC_IP)
       AND UPPER(NOMBRE_METODO) = UPPER(TRIM(PC_METODO))
       AND UPPER(ESTADO) = 'A';

  -- verifica si tablespace existe
  CURSOR C_EXISTE_TABLESPACE(PC_TABLASPACE IN VARCHAR2) IS
    SELECT 'x'
      FROM DBA_TABLESPACES
     WHERE TABLESPACE_NAME = UPPER(TRIM(PC_TABLASPACE))
       AND CONTENTS = UPPER('PERMANENT');

  -- verifica si tablespace temporal existe
  CURSOR C_EXISTE_TABLESPACE_TEMP(PC_TABLASPACE_TEMP IN VARCHAR2) IS
    SELECT 'x'
      FROM DBA_TABLESPACES
     WHERE TABLESPACE_NAME = UPPER(TRIM(PC_TABLASPACE_TEMP))
       AND CONTENTS = UPPER('TEMPORARY');

  -- verifica si profile existe
  CURSOR C_EXISTE_PROFILE(PC_PROFILE IN VARCHAR2) IS
    SELECT 'x' FROM DBA_PROFILES WHERE PROFILE = UPPER(TRIM(PC_PROFILE));

  -- valida si existe rol EN tabla:USERS  campo GROUP_USER
  CURSOR C_EXISTE_ROL_APP(CV_ROL VARCHAR2) IS
    SELECT 1 AS OK
      FROM USERS X
     WHERE UPPER(USERNAME) = UPPER(TRIM(CV_ROL))
       AND UPPER(USER_TYPE) = 'G';

  -- LISTADO DE MODULOS DE ROL :TABLA USERACCESS
  CURSOR C_LISTA_USERACCESS(CV_ROL VARCHAR2) IS
    SELECT X.MODULENAME, X.UAPERM,
    --6608 Norian Pilco
    X.MODIFIED,X.REC_VERSION
    --fin 6608
      FROM USERACCESS X
     WHERE UPPER(USERNAME) = UPPER(TRIM(CV_ROL));
     
    -- LISTADO DE MODULOS DE ROL :TABLA USER_CLASS
    CURSOR C_LISTA_USER_CLASS(CV_ROL VARCHAR2) IS
    SELECT X.CLCODE, X.USERLASTMOD
      FROM USER_CLASS X
     WHERE UPPER(X.USERNAME) = UPPER(TRIM(CV_ROL));

    --6608 Norian Pilco 
   -- LISTADO DE MODULOS DE ROL :TABLA USER_MARKETS
    CURSOR C_LISTA_USER_MARKETS(CV_ROL VARCHAR2) IS
    SELECT X.SCCODE,X.USERLASTMOD,X.REC_VERSION
    FROM USER_MARKETS X
     WHERE UPPER(USERNAME) = UPPER(TRIM(CV_ROL));

  -- LISTADO DE MODULOS DE ROL :TABLA USERS_DEALERS
  CURSOR C_LISTA_USERS_DEALERS(CV_ROL VARCHAR2) IS
    SELECT X.CUSTOMER_ID,X.USERLASTMOD,X.REC_VERSION
      FROM USERS_DEALERS X
     WHERE UPPER(X.USERNAME) = UPPER(TRIM(CV_ROL));
     
   -- LISTADO DE MODULOS DE ROL :TABLA SY_AMOUNTSUSER
    CURSOR C_LISTA_SY_AMOUNTSUSER(CV_ROL VARCHAR2) IS
    SELECT X.AU_AVID,X.REC_VERSION
      FROM SY_AMOUNTSUSER  X
     WHERE UPPER(X.AUNAME) = UPPER(TRIM(CV_ROL));
   --fin 6608
  -- valida si existe rol
  CURSOR C_EXISTE_ROL_DB(CV_ID_ROL VARCHAR2) IS
    SELECT 1 AS OK FROM DBA_ROLES X WHERE UPPER(ROLE) = UPPER(TRIM(CV_ID_ROL));

  -- verifica si rol esta relacionado con usuario de USERS
  CURSOR C_EXISTE_ROL_N_USUARIO_APP(PC_ROL IN VARCHAR2, PC_UUSARIO IN VARCHAR2) IS
    SELECT 'x'
      FROM USERS U
     WHERE UPPER(U.USERNAME) = UPPER(TRIM(PC_UUSARIO))
       AND UPPER(U.GROUP_USER) = UPPER(TRIM(PC_ROL));

  -- verifica si USUARIO TIENE ASIGNADO 1 ROL  USERS
  CURSOR C_EXISTE_USR_ROL_APP(PC_USUARIO IN VARCHAR2) IS
    SELECT 'x'
      FROM USERS U
     WHERE UPPER(U.USERNAME) = UPPER(TRIM(PC_USUARIO))
       AND UPPER(U.GROUP_USER) IS NOT NULL;

  PROCEDURE IAMP_CREA_USUARIO_DB(PV_USUARIO   IN VARCHAR2,
                                 PV_PASSWORD  IN VARCHAR2,
                                 PV_TSDEFAULT IN VARCHAR2,
                                 PV_TSTEMP    IN VARCHAR2,
                                 PV_PROFILE   IN VARCHAR2,
                                 PV_DESCRPTION      IN VARCHAR2,
                                 PV_BATCH_FLAG      IN VARCHAR2,
                                 PN_REC_VERSION     IN NUMBER,
                                 PV_USER_TYPE       IN VARCHAR2,
                                 PN_MAS_DEFAULT_LNG IN NUMBER,
                                 PN_SECURITYUNIT    IN NUMBER,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CREA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                  PV_DESCRPTION      IN VARCHAR2,
                                  PV_BATCH_FLAG      IN VARCHAR2,
                                  PN_REC_VERSION     IN NUMBER,
                                  PV_USER_TYPE       IN VARCHAR2,
                                  PN_MAS_DEFAULT_LNG IN NUMBER,
                                  PN_SECURITYUNIT    IN NUMBER,
                                  PV_ERROR           OUT VARCHAR2,
                                  PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ACTUALIZA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                   PV_PASSWORD        IN VARCHAR2,
                                   PV_TSDEFAULT       IN VARCHAR2,
                                   PV_TSTEMP          IN VARCHAR2,
                                   PV_PROFILE         IN VARCHAR2,
                                   PV_DESCRPTION      IN VARCHAR2,
                                   PV_BATCH_FLAG      IN VARCHAR2,
                                   PN_REC_VERSION     IN NUMBER,
                                   PV_USER_TYPE       IN VARCHAR2,
                                   PN_MAS_DEFAULT_LNG IN NUMBER,
                                   PN_SECURITYUNIT    IN NUMBER,
                                   PV_ERROR           OUT VARCHAR2,
                                   PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ACTUALIZA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                       PV_DESCRPTION      IN VARCHAR2,
                                       PV_BATCH_FLAG      IN VARCHAR2,
                                       PN_REC_VERSION     IN NUMBER,
                                       PV_USER_TYPE       IN VARCHAR2,
                                       PN_MAS_DEFAULT_LNG IN NUMBER,
                                       PN_SECURITYUNIT    IN NUMBER,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ELIMINA_USUARIO_BD(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ELIMINA_USUARIO_APP(PV_USUARIO IN VARCHAR2,
                                     PV_CADENA_ROL     IN VARCHAR2,
                                     PV_ERROR           OUT VARCHAR2,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_BLOQUEA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CREAR_ROL(PV_ID_ROL          IN VARCHAR2,
                           PV_DESCRPTION   IN VARCHAR2,
                           PV_ERROR           OUT VARCHAR2,
                           PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_DESBLOQUEA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_QUITAR_ROL(PV_USUARIO         IN VARCHAR2,
                            PV_CADENA_ROL      IN VARCHAR2,
                            PV_ERROR           OUT VARCHAR2,
                            PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_RESETEO_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ASIGNA_ROL_USUARIO(PV_CADENA_ROL IN VARCHAR2,
                                    PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_RETORNA_MSJ_ERROR(PV_COD_INTERNO IN OUT VARCHAR2,
                                   PV_MSJ_USUARIO OUT VARCHAR2,
                                   PN_COD_RETORNO OUT NUMBER);

  PROCEDURE IAMP_INGRESAR_BITACORA(PV_METODO           IN VARCHAR2,
                                   PV_APLICACION       IN VARCHAR2,
                                   PV_USUARIO          IN VARCHAR2,
                                   PV_USUARIO_OPERADOR IN VARCHAR2,
                                   PD_FECHA            IN DATE,
                                   LV_DIR_IP           IN VARCHAR2,
                                   PV_DESCRIPCION      IN VARCHAR2,
                                   LV_MSJ_SISTEMA      IN VARCHAR2,
                                   LV_MSJ_TECNICO      IN VARCHAR2,
                                   PV_ERROR            OUT VARCHAR2,
                                   PN_ERROR            OUT NUMBER,
                                   PV_MENSAJE_TECNICO  OUT VARCHAR2);

  FUNCTION IAMP_VALIDA_IP(PC_IP IN VARCHAR2, PC_METODO IN VARCHAR2)
    RETURN BOOLEAN;

  PROCEDURE IAMP_DESHACER_CREA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_DATOS_USUARIO(PV_USUARIO         IN VARCHAR2,
                               PV_DESCRIPCION     OUT VARCHAR2,
                               PV_GROUP_USER      OUT VARCHAR2,
                               PV_USER_TYPE       OUT VARCHAR2,
                               PV_BATCH_FLAG      OUT VARCHAR2,
                               PV_REC_VERSION     OUT NUMBER,
                               PV_MAS_DEFAULT_LNG OUT NUMBER,
                               PV_SECURITYUNIT    OUT NUMBER,
                               PV_TS_DEFAULT      OUT VARCHAR2,
                               PV_TS_TEMPORAL     OUT VARCHAR2,
                               PV_PROFILE         OUT VARCHAR2,
                               PV_STATUS          OUT VARCHAR2,
                               PV_ERROR           OUT VARCHAR2,
                               PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CONSULTA_USUARIO_ROLES(PV_USUARIO         IN VARCHAR2,
                                        PV_LISTA_ROLES     OUT CLOB,
                                        PV_ERROR           OUT VARCHAR2,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2 );

  PROCEDURE IAMP_CONSULTA_ROL_USUARIOS(PV_ROL             IN VARCHAR2,
                                       PV_LISTA_USUARIOS  OUT CLOB,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CONSULTA_USUARIO_Y_ROLES(PV_LISTA           OUT CLOB,
                                          PV_ERROR           OUT VARCHAR2,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CONSULTA_LISTA_ROLES(PV_LISTA_ROLES     OUT CLOB,
                                      PV_ERROR           OUT VARCHAR2,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2);

  Function IAMF_VALIDA_CLAVE(Pv_clave           In Varchar2,
                             Pv_usuario         In Varchar2,
                             Pv_error           Out Varchar2,
                             Pv_Mensaje_Tecnico Out Varchar2)Return Boolean;
END IAMK_TRX_USUARIO_BSCS;
/
CREATE OR REPLACE PACKAGE BODY IAMK_TRX_USUARIO_BSCS IS

  GV_PAQUETE VARCHAR2(30) := 'AMK_TRX_USUARIO_BSCS';

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Creacion de usuario DB

  PROCEDURE IAMP_CREA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_TSDEFAULT       IN VARCHAR2,
                                 PV_TSTEMP          IN VARCHAR2,
                                 PV_PROFILE         IN VARCHAR2,
                                 PV_DESCRPTION      IN VARCHAR2,
                                 PV_BATCH_FLAG      IN VARCHAR2,
                                 PN_REC_VERSION     IN NUMBER,
                                 PV_USER_TYPE       IN VARCHAR2,
                                 PN_MAS_DEFAULT_LNG IN NUMBER,
                                 PN_SECURITYUNIT    IN NUMBER,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA               VARCHAR2(61) := GV_PAQUETE ||'.IAMP_CREA_USUARIO_DB';
    LC_EXISTE_USER_DB         C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_TABLESPACE      C_EXISTE_TABLESPACE%ROWTYPE;
    LC_EXISTE_PROFILE         C_EXISTE_PROFILE%ROWTYPE;
    LC_EXISTE_TABLESPACE_TEMP C_EXISTE_TABLESPACE_TEMP%ROWTYPE;
    LV_ERROR                  VARCHAR2(1024);
    LV_MENSAJE_TECNICO        VARCHAR2(1024);
    LV_PASSWORD               VARCHAR2(20);
    Lb_valida_clave           Boolean:=False;
    LB_FOUND                  BOOLEAN;
    LE_ERROR                  EXCEPTION;
    LE_MY_ERROR               EXCEPTION;

  BEGIN

    PV_ERROR           := 'ER_0001'; -- operacion con exito
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      LV_ERROR := 'ER_0023'; -- valida que usuario no sea null
      RAISE LE_ERROR;
    END IF;

    IF (LENGTH(PV_USUARIO) > 30) THEN
      LV_ERROR := 'ER_0029'; -- valida que longitud del nombre no sea mayor 32
      RAISE LE_ERROR;
    END IF;

    -- valida password
    Lb_valida_clave:= iamf_valida_clave (Pv_clave           => PV_PASSWORD,
                                         Pv_usuario         => PV_USUARIO,
                                         Pv_error           => LV_ERROR,
                                         Pv_Mensaje_Tecnico => LV_MENSAJE_TECNICO);
     If Not Lb_valida_clave Then
        RAISE LE_ERROR;
     End If;

    IF PV_DESCRPTION IS NULL Then                 -- parametro obligatorio
      -- valida que PV_DESCRPTION no sea null
      LV_ERROR := 'ER_0040';
      RAISE LE_ERROR;
    END IF;

    LV_PASSWORD := TESSERA(PV_PASSWORD);
    IF (LV_PASSWORD = 'ER_0000') THEN
      LV_ERROR := 'ER_0021'; -- valida que Password
      RAISE LE_ERROR;
    END IF;

    -- verifica que exista el tablespace si no es null
    IF PV_TSDEFAULT IS NOT NULL THEN
      OPEN C_EXISTE_TABLESPACE(PV_TSDEFAULT);
      FETCH C_EXISTE_TABLESPACE
        INTO LC_EXISTE_TABLESPACE;
      LB_FOUND := C_EXISTE_TABLESPACE%FOUND;
      CLOSE C_EXISTE_TABLESPACE;

      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0026';
        RAISE LE_ERROR;
      END IF;
    END IF;

    -- verifica que exista el tablespace temporal si no es null
    IF PV_TSTEMP IS NOT NULL THEN
      OPEN C_EXISTE_TABLESPACE_TEMP(PV_TSTEMP);
      FETCH C_EXISTE_TABLESPACE_TEMP
        INTO LC_EXISTE_TABLESPACE_TEMP;
      LB_FOUND := C_EXISTE_TABLESPACE_TEMP%FOUND;
      CLOSE C_EXISTE_TABLESPACE_TEMP;

      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0027';
        RAISE LE_ERROR;
      END IF;
    END IF;

    -- verifica que exista el profile sino es null
    IF PV_PROFILE IS NOT NULL THEN
      OPEN C_EXISTE_PROFILE(PV_PROFILE);
      FETCH C_EXISTE_PROFILE
        INTO LC_EXISTE_PROFILE;
      LB_FOUND := C_EXISTE_PROFILE%FOUND;
      CLOSE C_EXISTE_PROFILE;

      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0028';
        RAISE LE_ERROR;
      END IF;
    END IF;

    --Verifica que no exista el usuario en la base para poder crearlo
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF NOT LB_FOUND THEN
      -- no existe usuario lo puede crear
      IAMK_OBJ_USUARIO_BSCS.IAMP_CREA_USUARIO_DB(UPPER(PV_USUARIO),
                                                 LV_PASSWORD,
                                                 PV_TSDEFAULT,
                                                 PV_TSTEMP,
                                                 PV_PROFILE,
                                                 LV_ERROR,
                                                 LV_MENSAJE_TECNICO);

      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        PV_ERROR := LV_ERROR;
        RAISE LE_MY_ERROR;
      END IF;
    ELSE

      -- existe usuario lo actualiza  CONFIRMAR  ACTUALIZA USER
      IAMK_TRX_USUARIO_BSCS.IAMP_ACTUALIZA_USUARIO(PV_USUARIO,
                                                   LV_PASSWORD,
                                                   PV_TSDEFAULT,
                                                   PV_TSTEMP,
                                                   PV_PROFILE,
                                                  -- PV_ESTATUS,
                                                   PV_DESCRPTION,
                                                   PV_BATCH_FLAG,
                                                   PN_REC_VERSION,
                                                   PV_USER_TYPE,
                                                   PN_MAS_DEFAULT_LNG,
                                                   PN_SECURITYUNIT,

                                                   PV_ERROR,
                                                   PV_MENSAJE_TECNICO);

      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        PV_ERROR := LV_ERROR;
        RAISE LE_MY_ERROR;
      END IF;

    END IF;

  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CREA_USUARIO_DB;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      :Crear Usuario a nivel de Aplicaci�n.

  PROCEDURE IAMP_CREA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                  PV_DESCRPTION      IN VARCHAR2,
                                  PV_BATCH_FLAG      IN VARCHAR2,
                                  PN_REC_VERSION     IN NUMBER,
                                  PV_USER_TYPE       IN VARCHAR2,
                                  PN_MAS_DEFAULT_LNG IN NUMBER,
                                  PN_SECURITYUNIT    IN NUMBER,
                                  PV_ERROR           OUT VARCHAR2,
                                  PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||
                                       '.IAMP_CREA_USUARIO_AP';
    LC_EXISTE_USER_APP C_EXISTE_USER_APP%ROWTYPE;
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_DESCRPTION      VARCHAR2(50);
    LV_USER_TYPE       VARCHAR2(1);
    LV_BATCH_FLAG      VARCHAR2(1);
    LB_FOUND           BOOLEAN;
    LN_REC_VERSION     NUMBER;
    LN_MAS_DEFAULT_LNG NUMBER;
    LN_SECURITYUNIT    NUMBER;
    LE_ERROR           EXCEPTION;
    LE_MY_ERROR        EXCEPTION;
  BEGIN

    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    IF PV_DESCRPTION IS NULL THEN
      -- valida que PV_DESCRPTION no sea null
      LV_ERROR := 'ER_0040';
      RAISE LE_ERROR;
    END IF;

    IF (LENGTH(PV_USUARIO) > 30) THEN
      LV_ERROR := 'ER_0029'; -- valida que longitud del nombre no sea mayor 32
      RAISE LE_ERROR;
    END IF;

    -- VALIDA EN CASO DE  SER NULL

    IF PV_DESCRPTION IS NULL THEN
      LV_DESCRPTION := ' ';
    ELSE
      LV_DESCRPTION := UPPER(PV_DESCRPTION);
    END IF;

    IF PN_REC_VERSION IS NULL THEN
      LN_REC_VERSION := 0;
    ELSE
      LN_REC_VERSION := PN_REC_VERSION;
    END IF;

    IF PV_USER_TYPE IS NULL THEN
      LV_USER_TYPE := 'U';
    ELSE
      LV_USER_TYPE := UPPER(PV_USER_TYPE);
    END IF;

    IF PN_MAS_DEFAULT_LNG IS NULL THEN
      LN_MAS_DEFAULT_LNG := 2;
    ELSE
      LN_MAS_DEFAULT_LNG := PN_MAS_DEFAULT_LNG;
    END IF;

    IF PN_SECURITYUNIT IS NULL THEN
      LN_SECURITYUNIT := 2;
    ELSE
      LN_SECURITYUNIT := PN_SECURITYUNIT;
    END IF;

    IF PV_BATCH_FLAG IS NULL THEN
      LV_BATCH_FLAG := '';
    ELSE
      LV_BATCH_FLAG := UPPER(PV_BATCH_FLAG);
    END IF;

    ---------VALIDACION  Y/N

    IF NOT (UPPER(LV_USER_TYPE) = 'U' OR UPPER(LV_USER_TYPE) = 'G') THEN
      LV_ERROR := 'ER_0034'; -- valida que LV_USER_TYPE SEA U / G
      RAISE LE_ERROR;
    END IF;

    IF NOT (UPPER(LV_BATCH_FLAG) = 'X') THEN
      LV_ERROR := 'ER_0035'; -- valida que LV_BATCH_FLAG SEA X O VACIO
      RAISE LE_ERROR;
    END IF;

    -------------FIN VALIDACION

    --Verifica existencia de usuario de aplicacion para poder crearlo
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP
      INTO LC_EXISTE_USER_APP;
    LB_FOUND := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    IF NOT LB_FOUND THEN
      IAMK_OBJ_USUARIO_BSCS.IAMP_CREA_USUARIO_APP(UPPER(PV_USUARIO),
                                                  LV_DESCRPTION,
                                                  LV_BATCH_FLAG,
                                                  LN_REC_VERSION,
                                                  LV_USER_TYPE,
                                                  LN_MAS_DEFAULT_LNG,
                                                  LN_SECURITYUNIT,
                                                  PV_ERROR,
                                                  PV_MENSAJE_TECNICO);

      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        PV_ERROR := LV_ERROR;
        RAISE LE_MY_ERROR;
      END IF;

    END IF;

  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  WHEN VALUE_ERROR THEN
      PV_ERROR           := 'ER_0045';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM, 1, 512) || ' en ' ||
                            LV_PROGRAMA;
   WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_CREA_USUARIO_APP;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Actualiza Usuario a nivel de Base de datos.

  PROCEDURE IAMP_ACTUALIZA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                   PV_PASSWORD        IN VARCHAR2,
                                   PV_TSDEFAULT       IN VARCHAR2,
                                   PV_TSTEMP          IN VARCHAR2,
                                   PV_PROFILE         IN VARCHAR2,
                                   PV_DESCRPTION      IN VARCHAR2,
                                   PV_BATCH_FLAG      IN VARCHAR2,
                                   PN_REC_VERSION     IN NUMBER,
                                   PV_USER_TYPE       IN VARCHAR2,
                                   PN_MAS_DEFAULT_LNG IN NUMBER,
                                   PN_SECURITYUNIT    IN NUMBER,
                                   PV_ERROR           OUT VARCHAR2,
                                   PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA               VARCHAR2(61) := GV_PAQUETE ||
                                              '.IAMP_ACTUALIZA_USUARIO';
    LC_EXISTE_USER_DB         C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_USER_APP        C_EXISTE_USER_APP%ROWTYPE;
    LC_EXISTE_TABLESPACE      C_EXISTE_TABLESPACE%ROWTYPE;
    LC_EXISTE_PROFILE         C_EXISTE_PROFILE%ROWTYPE;
    LC_EXISTE_TABLESPACE_TEMP C_EXISTE_TABLESPACE_TEMP%ROWTYPE;
    LV_ERROR                  VARCHAR2(1024);
    LV_SENTENCIA              VARCHAR2(1024);
    LV_MENSAJE_TECNICO        VARCHAR2(1024);
    LB_FOUND_DB               BOOLEAN;
    LB_FOUND_APP              BOOLEAN;
    LB_FOUND                  BOOLEAN;
    LE_ERROR                  EXCEPTION;
    LE_MY_ERROR               EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- validacion solo para evitar warning al no usar este parametro
    -- se incluyo el parametro para mantener estandar
    if pv_password is null then
       null;
    end if;

    IF PV_USUARIO IS NULL THEN
      -- valida que usuario no sea null
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    --Verifica si existe el usuario en la base para poder actualizarlo
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;
    IF NOT LB_FOUND_DB THEN
      LV_ERROR := 'ER_0009';
      RAISE LE_ERROR;
    END IF;

    --Verifica existencia de usuario de aplicacion para poder actualizarlo
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP
      INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;
    IF NOT LB_FOUND_APP THEN
      LV_ERROR := 'ER_0010';
      RAISE LE_ERROR;
    END IF;

    -- verifica que exista el tablespace si no es null
    IF PV_TSDEFAULT IS NOT NULL THEN
      OPEN C_EXISTE_TABLESPACE(PV_TSDEFAULT);
      FETCH C_EXISTE_TABLESPACE
        INTO LC_EXISTE_TABLESPACE;
      LB_FOUND := C_EXISTE_TABLESPACE%FOUND;
      CLOSE C_EXISTE_TABLESPACE;
      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0026';
        RAISE LE_ERROR;
      END IF;
    END IF;

    -- verifica que exista el tablespace temporal si no es null
    IF PV_TSTEMP IS NOT NULL THEN
      OPEN C_EXISTE_TABLESPACE_TEMP(PV_TSTEMP);
      FETCH C_EXISTE_TABLESPACE_TEMP
        INTO LC_EXISTE_TABLESPACE_TEMP;
      LB_FOUND := C_EXISTE_TABLESPACE_TEMP%FOUND;
      CLOSE C_EXISTE_TABLESPACE_TEMP;
      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0027';
        RAISE LE_ERROR;
      END IF;
    END IF;

    -- verifica que exista el profile sino es null
    IF PV_PROFILE IS NOT NULL THEN
      OPEN C_EXISTE_PROFILE(PV_PROFILE);
      FETCH C_EXISTE_PROFILE
        INTO LC_EXISTE_PROFILE;
      LB_FOUND := C_EXISTE_PROFILE%FOUND;
      CLOSE C_EXISTE_PROFILE;
      IF NOT LB_FOUND THEN
        LV_ERROR := 'ER_0028';
        RAISE LE_ERROR;
      END IF;
    END IF;

    IF LB_FOUND_DB AND LB_FOUND_APP THEN

      IF PV_TSDEFAULT IS NOT NULL OR
         PV_TSTEMP IS NOT NULL OR PV_PROFILE IS NOT NULL THEN
        LV_SENTENCIA := 'Alter User  ' || PV_USUARIO || ' ';
      END IF;

      IF PV_TSDEFAULT IS NOT NULL THEN
        LV_SENTENCIA := LV_SENTENCIA || '  default tablespace  ' ||
                        PV_TSDEFAULT || ' ';
      END IF;

      IF PV_TSTEMP IS NOT NULL THEN
        LV_SENTENCIA := LV_SENTENCIA || '  temporary tablespace ' ||
                        PV_TSTEMP || ' ';
      END IF;

      IF PV_PROFILE IS NOT NULL THEN
        LV_SENTENCIA := LV_SENTENCIA || '  profile ' || PV_PROFILE || ' ';
      END IF;

      IF LV_SENTENCIA IS NOT NULL THEN
        EXECUTE IMMEDIATE LV_SENTENCIA;
      END IF;

      ---- actualiza app
      IAMK_TRX_USUARIO_BSCS.IAMP_ACTUALIZA_USUARIO_APP(PV_USUARIO,
                                                       PV_DESCRPTION,
                                                       PV_BATCH_FLAG,
                                                       PN_REC_VERSION,
                                                       PV_USER_TYPE,
                                                       PN_MAS_DEFAULT_LNG,
                                                       PN_SECURITYUNIT,
                                                       PV_ERROR,
                                                       PV_MENSAJE_TECNICO);

    END IF;

  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      :Actualiza Usuario a nivel de Aplicaci�n.

  PROCEDURE IAMP_ACTUALIZA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                       PV_DESCRPTION      IN VARCHAR2,
                                       PV_BATCH_FLAG      IN VARCHAR2,
                                       PN_REC_VERSION     IN NUMBER,
                                       PV_USER_TYPE       IN VARCHAR2,
                                       PN_MAS_DEFAULT_LNG IN NUMBER,
                                       PN_SECURITYUNIT    IN NUMBER,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA             VARCHAR2(61) := GV_PAQUETE || '.IAMP_ACTUALIZA_USUARIO_APP';
    LC_EXISTE_USER_APP      C_EXISTE_USER_APP%ROWTYPE;
    LV_ERROR                VARCHAR2(1024);
    LV_DESCRPTION           VARCHAR2(50);
    LV_USER_TYPE            VARCHAR2(1);
    LV_BATCH_FLAG           VARCHAR2(1);
    LV_MENSAJE_TECNICO      VARCHAR2(1024);
    LN_REC_VERSION          NUMBER;
    LN_MAS_DEFAULT_LNG      NUMBER;
    LN_SECURITYUNIT         NUMBER;
    LN_USER_RIGHTS_SEC_UNIT NUMBER;
    LB_FOUND_APP            BOOLEAN;
    LE_MY_ERROR        EXCEPTION;
    LE_ERROR           EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    --Verifica existencia de usuario de aplicacion para poder actualizarlo
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP
      INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    IF NOT LB_FOUND_APP THEN
      LV_ERROR := 'ER_0010';
      RAISE LE_ERROR;
    END IF;

    -- VALIDA EN CASO DE  SER NULL

     LV_DESCRPTION := UPPER(PV_DESCRPTION);
     LN_REC_VERSION := PN_REC_VERSION;
     LV_USER_TYPE := UPPER(PV_USER_TYPE);
     LN_MAS_DEFAULT_LNG := PN_MAS_DEFAULT_LNG;
     LN_SECURITYUNIT := UPPER(PN_SECURITYUNIT);
     LV_BATCH_FLAG := UPPER(PV_BATCH_FLAG);

    ---------VALIDACION  Y/N
    IF NOT (UPPER(LV_USER_TYPE) = 'U' OR UPPER(LV_USER_TYPE) = 'G') THEN
      LV_ERROR := 'ER_0034'; -- valida que LV_USER_TYPE SEA U / G
      RAISE LE_ERROR;
    END IF;

    IF NOT (UPPER(LV_BATCH_FLAG) = 'X') THEN
      LV_ERROR := 'ER_0035'; -- valida que LV_BATCH_FLAG SEA X O VACIO
      RAISE LE_ERROR;
    END IF;
    -------------FIN VALIDACION

    IF LV_DESCRPTION IS NOT NULL THEN
      UPDATE USERS US
         SET US.DESCRIPTION = LV_DESCRPTION
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LN_REC_VERSION IS NOT NULL THEN
      UPDATE USERS US
         SET US.REC_VERSION = LN_REC_VERSION
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LV_USER_TYPE IS NOT NULL THEN
      UPDATE USERS US
         SET US.USER_TYPE = LV_USER_TYPE
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LN_MAS_DEFAULT_LNG IS NOT NULL THEN
      UPDATE USERS US
         SET US.MAS_DEFAULT_LNG = LN_MAS_DEFAULT_LNG
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LN_SECURITYUNIT IS NOT NULL THEN
      UPDATE USERS US
         SET US.SECURITYUNIT = LN_SECURITYUNIT
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LN_USER_RIGHTS_SEC_UNIT IS NOT NULL THEN
      UPDATE USERS US
         SET US.USER_RIGHTS_SEC_UNIT = LN_USER_RIGHTS_SEC_UNIT
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;

    IF LV_BATCH_FLAG IS NOT NULL THEN
      UPDATE USERS US
         SET US.BATCH_FLAG = UPPER(LV_BATCH_FLAG)
       WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
    END IF;
    -- INSERTA ACTUALIZACION EN AUDITTRAIL
    INSERT INTO SYSADM.AUDITTRAIL
    VALUES
      ('SY',
       SYSDATE,
       'SYSTBE', -- DEFAULT
       'USERS', -- DEFAULT
       'U', -- DEFAULT
       0);

  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN VALUE_ERROR THEN
      PV_ERROR           := 'ER_0045';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM, 1, 512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_ACTUALIZA_USUARIO_APP;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      :Elimina Usuario a nivel de Base de datos.

  PROCEDURE IAMP_ELIMINA_USUARIO_BD(PV_USUARIO         IN VARCHAR2,
                                   -- PC_LISTADO_ROL     IN VARCHAR2, --CLOB,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.' ||
                                'IAMP_ELIMINA_USUARIO_BD';
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LB_FOUND_DB        BOOLEAN;
    LE_ERROR           EXCEPTION;
    LE_MY_ERROR        EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    --Verifica si existe el usuario en la base para poder eliminarlo
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;


    IF LB_FOUND_DB THEN
      --AND LB_FOUND_APP THEN

      IAMK_OBJ_USUARIO_BSCS.IAMP_ELIMINA_USUARIO_BD(PV_USUARIO,
                                                    LV_ERROR,
                                                    LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        -- error al eliminar user db
        LV_ERROR := LV_ERROR;
        RAISE LE_MY_ERROR;
      END IF;

    ELSE
      IF NOT LB_FOUND_DB THEN
        LV_ERROR := 'ER_0009';
        RAISE LE_ERROR;
      END IF;

    END IF;

  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_ELIMINA_USUARIO_BD;

 --========================================================
 --========================================================
  PROCEDURE IAMP_ELIMINA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                     PV_CADENA_ROL      IN VARCHAR2,
                                     PV_ERROR           OUT VARCHAR2,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.' ||
                                'IAMP_ELIMINA_USUARIO_APP';
    LC_EXISTE_USER_APP          C_EXISTE_USER_APP%ROWTYPE;
    LC_EXISTE_ROL_N_USUARIO_APP C_EXISTE_ROL_N_USUARIO_APP%ROWTYPE;
    LC_EXISTE_ROL_APP           C_EXISTE_ROL_APP%ROWTYPE;
    LV_ERROR                    VARCHAR2(1024);
    LV_MENSAJE_TECNICO          VARCHAR2(1024);
    LV_ROL                      VARCHAR2(4000);
    LV_CADENA_ROL               VARCHAR2(4000);
    LV_AUX_ROL                  VARCHAR2(4000);
    LN_POS                      NUMBER;
    LV_OK                       NUMBER;
    LB_FOUND_ROL                BOOLEAN;
    LB_FOUND_RU                 BOOLEAN;
    LB_FOUND_APP                BOOLEAN;
    LE_ERROR                    EXCEPTION;
    LE_MY_ERROR                 EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';
    LV_OK:=1;

   DBMS_OUTPUT.ENABLE(100000);

  --Verifica existencia de usuario de aplicacion
   OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP
      INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    /*IF PV_CADENA_ROL IS NULL THEN
       RAISE;
    END IF; */

    IF LB_FOUND_APP THEN

     ------------- selecciona el ultimo
        IF SUBSTR(PV_CADENA_ROL,
              -1,
              1) = ';' THEN
      LV_CADENA_ROL := PV_CADENA_ROL;
    ELSE
      LV_CADENA_ROL := PV_CADENA_ROL || ';';
    END IF;

        WHILE LV_CADENA_ROL <> ' ' OR LV_CADENA_ROL IS NOT NULL LOOP
      LN_POS        := INSTR(LV_CADENA_ROL,
                             ';',
                             1,
                             1);
      LV_ROL        := SUBSTR(LV_CADENA_ROL,
                              1,
                              LN_POS - 1);
      LV_CADENA_ROL := SUBSTR(LV_CADENA_ROL,
                              LN_POS + 1);


      LV_AUX_ROL:=LV_ROL;

      IF LV_ROL IS NULL THEN
        EXIT;

      END IF;
    END LOOP;

      --Verifica que  exista rol
      OPEN C_EXISTE_ROL_APP(LV_AUX_ROL);
      FETCH C_EXISTE_ROL_APP
        INTO LC_EXISTE_ROL_APP;
      LB_FOUND_ROL := C_EXISTE_ROL_APP%FOUND;
      CLOSE C_EXISTE_ROL_APP;

      -- verifica que el rol este asignado al usuario
      OPEN C_EXISTE_ROL_N_USUARIO_APP(LV_AUX_ROL,
                                      PV_USUARIO);
      FETCH C_EXISTE_ROL_N_USUARIO_APP
        INTO LC_EXISTE_ROL_N_USUARIO_APP;
      LB_FOUND_RU := C_EXISTE_ROL_N_USUARIO_APP%FOUND;
      CLOSE C_EXISTE_ROL_N_USUARIO_APP;


      IF NOT LB_FOUND_RU AND NOT LB_FOUND_ROL THEN
         LV_OK:=0;
      END IF;

      -----------
      IF NOT LB_FOUND_ROL AND LV_OK=1 THEN

        LV_ERROR := 'ER_0025';
        RAISE LE_ERROR;
      END IF;

      IF (PV_USUARIO IS NULL) THEN
        LV_ERROR := 'ER_0008';
        RAISE LE_ERROR;
      END IF;
      ------------
    /* IF (LV_AUX_ROL IS NULL) OR (PV_USUARIO IS NULL) THEN
        LV_ERROR := 'ER_0008';
        RAISE LE_ERROR;
       END IF;
    */

    -----------
       --  if PV_CADENA_ROL IS NOT null then
  IF LV_OK=1 THEN
       -- envia a revocar
      IAMK_TRX_USUARIO_BSCS.IAMP_QUITAR_ROL(PV_USUARIO,
                                            LV_AUX_ROL,
                                            LV_ERROR,
                                            LV_MENSAJE_TECNICO);


      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
   END IF;
----------   FINALIZA BLOQUE DE ROLES


  IAMK_TRX_USUARIO_BSCS.IAMP_ASIGNA_ROL_USUARIO('ACCPROH',PV_USUARIO,LV_ERROR,LV_MENSAJE_TECNICO);


      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;

  ELSE
      IF NOT LB_FOUND_APP THEN
        LV_ERROR := 'ER_0010';
        RAISE LE_ERROR;
      END IF;
  END IF;



  EXCEPTION
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_ELIMINA_USUARIO_APP;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      :Bloquea Usuario.
  PROCEDURE IAMP_BLOQUEA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.IAMP_BLOQUEA_USUARIO';
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_ERROR           VARCHAR2(1024);
    LB_FOUND           BOOLEAN;
    LE_ERROR           EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      -- verifica si usuario no es nulo
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    --Verifica existencia de usuario de base para poder bloquear
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF LB_FOUND THEN
      IAMK_OBJ_USUARIO_BSCS.IAMP_BLOQUEA_USUARIO_DB(PV_USUARIO,
                                                    LV_ERROR,
                                                    LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        LV_ERROR := LV_ERROR;
        RAISE LE_ERROR;
      END IF;
    ELSE
      LV_ERROR := 'ER_0009';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;

      -- obtiene mensaje de error parametrizado
      /*IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
      LV_MENSAJE_TECNICO,
      LN_ERROR);*/

      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_BLOQUEA_USUARIO;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Desbloquea Usuario.
  PROCEDURE IAMP_DESBLOQUEA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA       VARCHAR2(61) := GV_PAQUETE ||
                                      '.IAMP_DESBLOQUEA_USUARIO';
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LB_FOUND           BOOLEAN;
    LE_ERROR           EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';
    IF PV_USUARIO IS NULL THEN
      -- verifica si usuario no es nulo
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    -- Verifica existencia de usuario de base para poder desbloquear
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF LB_FOUND THEN
      IAMK_OBJ_USUARIO_BSCS.IAMP_DESBLOQUEA_USUARIO_DB(PV_USUARIO,
                                                       LV_ERROR,
                                                       LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        LV_ERROR := LV_ERROR;
        RAISE LE_ERROR;
      END IF;
    ELSE
      LV_ERROR := 'ER_0009';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_DESBLOQUEA_USUARIO;

  --========================================================
  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      :Crea Rol a nivel de Base de datos
 --========================================================

  PROCEDURE IAMP_CREAR_ROL(PV_ID_ROL          IN VARCHAR2,
                           PV_DESCRPTION      IN VARCHAR2,
                           PV_ERROR           OUT VARCHAR2,
                           PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA         VARCHAR2(61) := GV_PAQUETE || '.IAMP_CREAR_ROL';
    LV_ERROR            VARCHAR2(1024) := NULL;
    LV_MENSAJE_TECNICO  VARCHAR2(1024);
    LE_ERROR            EXCEPTION;
    LE_ERROR_PARAMETROS EXCEPTION;
    LE_MY_ERROR         EXCEPTION;

  BEGIN

    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';


  IF PV_ID_ROL IS NULL THEN
     LV_ERROR := 'ER_0023';
     RAISE LE_ERROR;
  END IF;

      IF PV_DESCRPTION IS NULL THEN
         LV_ERROR := 'ER_0046';
         RAISE LE_ERROR;
      END IF;

  IF (LENGTH(PV_ID_ROL) > 30) THEN
     LV_ERROR := 'ER_0043'; -- valida que longitud del nombre no sea mayor 32
     RAISE LE_ERROR;
  END IF;

    IAMK_OBJ_USUARIO_BSCS.IAMP_CREAR_ROL(PV_ROL             => PV_ID_ROL,
                                         PV_DESCRPTION      => PV_DESCRPTION,
                                         PV_ERROR           => LV_ERROR,
                                         PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_MY_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR_PARAMETROS THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CREAR_ROL;

 --========================================================
 -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Quita Rol a nivel de Base de datos.
 --========================================================
  PROCEDURE IAMP_QUITAR_ROL(PV_USUARIO         IN VARCHAR2,
                            PV_CADENA_ROL      IN VARCHAR2,
                            PV_ERROR           OUT VARCHAR2,
                            PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA                 VARCHAR2(61) := GV_PAQUETE ||
                                                '.IAMP_QUITAR_ROL';
    LC_EXISTE_ROL_APP           C_EXISTE_ROL_APP%ROWTYPE;
    LC_EXISTE_USER_DB           C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_ROL_N_USUARIO_APP C_EXISTE_ROL_N_USUARIO_APP%ROWTYPE;
    LV_ERROR                    VARCHAR2(600);
    LV_MENSAJE_TECNICO          VARCHAR2(1024);
    LV_AUX_ROL                  varchar2(4000);
    LV_ROL                      VARCHAR2(4000);
    LV_CADENA_ROL               VARCHAR2(4000);
    LN_POS                      NUMBER;
    LB_FOUND_ROL                BOOLEAN;
    LB_FOUND_DB                 BOOLEAN;
    LB_FOUND_RU                 BOOLEAN;
    LE_ERROR                    EXCEPTION;

  BEGIN

    DBMS_OUTPUT.ENABLE(100000);
    PV_ERROR           := 'ER_0001'; -- transaccion con exito
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL OR PV_CADENA_ROL IS NULL THEN
      LV_ERROR:='ER_0008';
      RAISE LE_ERROR;
    END IF;
    ------------- selecciona el ultimo
        IF SUBSTR(PV_CADENA_ROL,
              -1,
              1) = ';' THEN
      LV_CADENA_ROL := PV_CADENA_ROL;
    ELSE
      LV_CADENA_ROL := PV_CADENA_ROL || ';';
    END IF;

        WHILE LV_CADENA_ROL <> ' ' OR LV_CADENA_ROL IS NOT NULL LOOP
      LN_POS        := INSTR(LV_CADENA_ROL,
                             ';',
                             1,
                             1);
      LV_ROL        := SUBSTR(LV_CADENA_ROL,
                              1,
                              LN_POS - 1);
      LV_CADENA_ROL := SUBSTR(LV_CADENA_ROL,
                              LN_POS + 1);


      LV_AUX_ROL:=LV_ROL;

      IF LV_ROL IS NULL THEN
        EXIT;

      END IF;



    END LOOP;

    --Verifica que  exista el usuario en la base
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF NOT LB_FOUND_DB THEN
      LV_ERROR := 'ER_0024';
      RAISE LE_ERROR;
    END IF;

      --Verifica que  exista rol
      OPEN C_EXISTE_ROL_APP(LV_AUX_ROL);
      FETCH C_EXISTE_ROL_APP
        INTO LC_EXISTE_ROL_APP;
      LB_FOUND_ROL := C_EXISTE_ROL_APP%FOUND;
      CLOSE C_EXISTE_ROL_APP;

      IF NOT LB_FOUND_ROL THEN
        LV_ERROR := 'ER_0015';
        RAISE LE_ERROR;
      END IF;

      IF (LV_AUX_ROL IS NULL) OR (PV_USUARIO IS NULL) THEN
        LV_ERROR := 'ER_0008';
        RAISE LE_ERROR;
      END IF;

      -- verifica que el rol este asignado al usuario
     OPEN C_EXISTE_ROL_N_USUARIO_APP(LV_AUX_ROL,
                                     PV_USUARIO);
      FETCH C_EXISTE_ROL_N_USUARIO_APP
        INTO LC_EXISTE_ROL_N_USUARIO_APP;
      LB_FOUND_RU := C_EXISTE_ROL_N_USUARIO_APP%FOUND;
      CLOSE C_EXISTE_ROL_N_USUARIO_APP;

      IF NOT LB_FOUND_RU THEN
        --LV_R:='ERROR';
        LV_ERROR := 'ER_0011';
        RAISE LE_ERROR;
      END IF;

      -------------QUITO ROL
        -- envia a revocar rol
      IAMK_OBJ_USUARIO_BSCS.IAMP_QUITAR_ROL_APP(upper(trim(PV_USUARIO)),
                                                upper(trim(LV_AUX_ROL)),
                                                LV_ERROR,
                                                LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
      ----------------DELETE USER_CLASS, USERACCESS
       
       DELETE USERACCESS US WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
       --6608 Norian Pilco
       DELETE USER_MARKETS X WHERE UPPER(X.USERNAME) = UPPER(TRIM(PV_USUARIO));
       DELETE USERS_DEALERS Y WHERE UPPER(Y.USERNAME) = UPPER(TRIM(PV_USUARIO));
       DELETE SY_AMOUNTSUSER Z WHERE UPPER(Z.AUNAME) = UPPER(TRIM(PV_USUARIO));
       --FIN 6608
       DELETE USER_CLASS UC WHERE UPPER(uc.username) = UPPER(TRIM(PV_USUARIO));


  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN NO_DATA_FOUND THEN
      PV_ERROR           := 'ER_0002';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_QUITAR_ROL;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : reseteo clave

  PROCEDURE IAMP_RESETEO_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA          Varchar2(61) := GV_PAQUETE ||'.IAMP_RESETEO_USUARIO';
    LC_EXISTE_USER_DB    C_EXISTE_USER_DB%Rowtype;
    LV_ERROR             Varchar2(1024);
    LV_MENSAJE_TECNICO   Varchar2(1024);
    LB_FOUND_DB          Boolean;
    Lb_valida_clave      Boolean:=False;
    LE_ERROR             Exception;
    LE_MY_ERROR          Exception;
    LV_PASSWORD          Varchar2(30);

  BEGIN
    PV_ERROR           := 'ER_0001'; -- Operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      LV_ERROR := 'ER_0023'; -- Verifica que usuario no sea null
      RAISE LE_ERROR;
    END IF;

    -- valida password
    Lb_valida_clave:= iamf_valida_clave (Pv_clave           => PV_PASSWORD,
                                         Pv_usuario         => PV_USUARIO,
                                         Pv_error           => LV_ERROR,
                                         Pv_Mensaje_Tecnico => LV_MENSAJE_TECNICO);
     If Not Lb_valida_clave Then
        RAISE LE_ERROR;
     End If;


    --LV_PASSWORD := XXX(PV_PASSWORD);
    LV_PASSWORD := TESSERA(PV_PASSWORD);
    IF (LV_PASSWORD = 'ER_0000') THEN
      LV_ERROR := 'ER_0021'; -- valida que Password
      RAISE LE_ERROR;
    END IF;

    --Verifica si existe el usuario en la base para poder actualizar el password
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF LB_FOUND_DB THEN
      IAMK_OBJ_USUARIO_BSCS.IAMP_RESETEO_USUARIO(PV_USUARIO         => PV_USUARIO,
                                                 PV_PASSWORD        => LV_PASSWORD,
                                                 PV_ERROR           => LV_ERROR,
                                                 PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        LV_ERROR := 'ER_0003';
        RAISE LE_MY_ERROR;
      END IF;
    ELSE
      LV_ERROR := 'ER_0009';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_RESETEO_USUARIO;

  -- Author       : IRO Rosangela Espinoza
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez.
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Asigna 1 rol (o varios roles separados por signo ;) a un usuario a nivel de Base de datos

  PROCEDURE IAMP_ASIGNA_ROL_USUARIO(PV_CADENA_ROL      IN VARCHAR2,
                                    PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA           VARCHAR2(61) := GV_PAQUETE ||
                                      '.IAMP_ASIGNA_ROL_USUARIO';
    LC_EXISTE_USER_DB     C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_ROL_APP     C_EXISTE_ROL_APP%ROWTYPE;
    LC_EXISTE_USR_ROL_APP C_EXISTE_USR_ROL_APP%ROWTYPE;
    LC_MODULENAME         VARCHAR2(20);
    LC_UAPERM             VARCHAR2(20);
    
    --6608 Norian Pilco
    LC_MODIFIED           USERACCESS.MODIFIED%TYPE;
    LC_REC_VERSION        USERACCESS.REC_VERSION%TYPE;
    LC_SCCODEU            USER_MARKETS.SCCODE%TYPE;
    LC_USERLASTMODU       USER_MARKETS.USERLASTMOD%TYPE;
    LC_REC_VERSIONU       USER_MARKETS.REC_VERSION%TYPE;
    LC_CUSTOMER_ID        USERS_DEALERS.CUSTOMER_ID%TYPE;
    LC_USERLASTMODD       USERS_DEALERS.USERLASTMOD%TYPE;
    LC_REC_VERSIOND       USERS_DEALERS.REC_VERSION%TYPE;
    LC_AVIDS              SY_AMOUNTSUSER.AU_AVID%TYPE;
    LC_REC_VERSIONS       SY_AMOUNTSUSER.REC_VERSION%TYPE;
    -- fin 6608
    LC_CLCODE             VARCHAR2(20);
    LC_USERLASTMOD        VARCHAR2(20);
    LV_ERROR              VARCHAR2(1024);
    LV_MENSAJE_TECNICO    VARCHAR2(1024);
    LV_ROL                VARCHAR2(4000);
    LV_CADENA_ROL         VARCHAR2(4000);
    LV_ROL_U1             VARCHAR2(4000);
    LV_AUX_ROL            VARCHAR2(4000);
    LN_POS                NUMBER;
    LB_FOUND_DB           BOOLEAN;
    LB_FOUND_ROL          BOOLEAN;
    LB_FOUND_USR_ROL      BOOLEAN;
    LE_ERROR              EXCEPTION;
    LE_MY_ERROR           EXCEPTION;

  BEGIN

  LV_AUX_ROL:=' ';
  DBMS_OUTPUT.ENABLE(100000);
    PV_ERROR           := 'ER_0001';
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL OR PV_CADENA_ROL IS NULL THEN
      LV_ERROR:='ER_0008';
      RAISE LE_ERROR;
    END IF;

    -- selecciona el ultimo
    IF SUBSTR(PV_CADENA_ROL, -1,
              1) = ';' THEN
      LV_CADENA_ROL := PV_CADENA_ROL;
    ELSE
      LV_CADENA_ROL := PV_CADENA_ROL || ';';
    END IF;

        WHILE LV_CADENA_ROL <> ' ' OR LV_CADENA_ROL IS NOT NULL LOOP
      LN_POS        := INSTR(LV_CADENA_ROL,
                             ';',
                             1,
                             1);
      LV_ROL        := SUBSTR(LV_CADENA_ROL,
                              1,
                              LN_POS - 1);
      LV_CADENA_ROL := SUBSTR(LV_CADENA_ROL,
                              LN_POS + 1);


      LV_AUX_ROL:=LV_ROL;

      IF LV_ROL IS NULL THEN
        EXIT;

      END IF;



    END LOOP;

    --Verifica que  exista el usuario en la base
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF NOT LB_FOUND_DB THEN
      LV_ERROR := 'ER_0024';
      RAISE LE_ERROR;
    END IF;

      --Verifica que  exista rol EN TABLA
      OPEN C_EXISTE_ROL_APP(LV_AUX_ROL);
      FETCH C_EXISTE_ROL_APP
        INTO LC_EXISTE_ROL_APP;
      LB_FOUND_ROL := C_EXISTE_ROL_APP%FOUND;
      CLOSE C_EXISTE_ROL_APP;

      IF NOT LB_FOUND_ROL THEN
        LV_ERROR := 'ER_0015';
        RAISE LE_ERROR;
      END IF;

      IF (LV_AUX_ROL IS NULL) OR (PV_USUARIO IS NULL) THEN
        LV_ERROR := 'ER_0008';
        RAISE LE_ERROR;
      END IF;

      -- verifica que el rol este asignado al usuario
      OPEN C_EXISTE_USR_ROL_APP(PV_USUARIO);
      FETCH C_EXISTE_USR_ROL_APP INTO LC_EXISTE_USR_ROL_APP;
      LB_FOUND_USR_ROL := C_EXISTE_USR_ROL_APP%FOUND;
      CLOSE C_EXISTE_USR_ROL_APP;

      IF LB_FOUND_USR_ROL THEN
         BEGIN
         SELECT U.GROUP_USER INTO LV_ROL_U1
         FROM USERS U
         WHERE UPPER(U.USERNAME)=UPPER(TRIM(PV_USUARIO));
         END;
      -- QUITO ROL
         IAMK_TRX_USUARIO_BSCS.IAMP_QUITAR_ROL(upper(trim(PV_USUARIO)),
                                               upper(trim(LV_ROL_U1)),
                                               LV_ERROR,
                                               LV_MENSAJE_TECNICO );

         IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
      ----------------DELETE USER_CLASS, USERACCESS
       DELETE USERACCESS US WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
       DELETE USER_CLASS UC WHERE UPPER(uc.username) = UPPER(TRIM(PV_USUARIO));


      END IF;


     OPEN C_LISTA_USER_CLASS(LV_AUX_ROL);
      FETCH C_LISTA_USER_CLASS
        INTO LC_CLCODE, LC_USERLASTMOD;
      WHILE C_LISTA_USER_CLASS%FOUND LOOP
        INSERT INTO USER_CLASS
        VALUES
          (PV_USUARIO,
           LC_CLCODE,
           SYSDATE,
           SYSDATE,
           LC_USERLASTMOD,
           1);
        FETCH C_LISTA_USER_CLASS
          INTO LC_CLCODE, LC_USERLASTMOD;
      END LOOP;
      CLOSE C_LISTA_USER_CLASS;
      --FIN TABLA USER_CLASS

      -- TABLA USERACCESS
      OPEN C_LISTA_USERACCESS(LV_AUX_ROL);
      FETCH C_LISTA_USERACCESS
        INTO LC_MODULENAME, LC_UAPERM,
        --6608 Norian Pilco
        LC_MODIFIED,LC_REC_VERSION
        --fin 6608
        ;
      WHILE C_LISTA_USERACCESS%FOUND LOOP
        INSERT INTO USERACCESS
        VALUES
          (PV_USUARIO,
           LC_MODULENAME,
           LC_UAPERM,
           SYSDATE,
           --6608 Norian Pilco
           SYSDATE,
           LC_MODIFIED,
           LC_REC_VERSION);
           --fin 6608
        FETCH C_LISTA_USERACCESS
          INTO LC_MODULENAME, LC_UAPERM,LC_MODIFIED,LC_REC_VERSION;
      END LOOP;
      CLOSE C_LISTA_USERACCESS;
      --FIN TABLA USER_CLASS
    -----------
    
     --6608 Norian Pilco 
     -- TABLA USER_MARKETS
      OPEN C_LISTA_USER_MARKETS(LV_AUX_ROL);
      FETCH C_LISTA_USER_MARKETS
      INTO LC_SCCODEU, LC_USERLASTMODU,LC_REC_VERSIONU;
      
      WHILE C_LISTA_USER_MARKETS%FOUND LOOP
        INSERT INTO USER_MARKETS
        VALUES
          (PV_USUARIO,
           LC_SCCODEU,
           SYSDATE,
           SYSDATE,
           LC_USERLASTMODU,
           LC_REC_VERSIONU);         
        FETCH C_LISTA_USER_MARKETS
          INTO LC_SCCODEU, LC_USERLASTMODU,LC_REC_VERSIONU;
      END LOOP;
      
      CLOSE C_LISTA_USER_MARKETS;
      
      
     -- TABLA USERS_DEALERS
      OPEN C_LISTA_USERS_DEALERS(LV_AUX_ROL);
      FETCH C_LISTA_USERS_DEALERS
      INTO LC_CUSTOMER_ID, LC_USERLASTMODD,LC_REC_VERSIOND;
      
      WHILE C_LISTA_USERS_DEALERS%FOUND LOOP
        INSERT INTO USERS_DEALERS
        VALUES
          (LC_CUSTOMER_ID,
           PV_USUARIO,          
           SYSDATE,
           SYSDATE,
           LC_USERLASTMODD,
           LC_REC_VERSIOND);         
        FETCH C_LISTA_USERS_DEALERS
          INTO LC_CUSTOMER_ID,LC_USERLASTMODD,LC_REC_VERSIOND;
      END LOOP;
      
      CLOSE C_LISTA_USERS_DEALERS;
      

     -- TABLA SY_AMOUNTSUSER
      OPEN C_LISTA_SY_AMOUNTSUSER(LV_AUX_ROL);
      FETCH C_LISTA_SY_AMOUNTSUSER
      INTO LC_AVIDS,LC_REC_VERSIONS;
      
      WHILE C_LISTA_SY_AMOUNTSUSER%FOUND LOOP
        INSERT INTO SY_AMOUNTSUSER
        VALUES
          (PV_USUARIO, 
           LC_AVIDS,
           LC_REC_VERSIONS);         
        FETCH C_LISTA_SY_AMOUNTSUSER
          INTO LC_AVIDS,LC_REC_VERSIONS;
      END LOOP;
      
      CLOSE C_LISTA_SY_AMOUNTSUSER;
     --FIN 6608     
    
    
    
          IAMK_OBJ_USUARIO_BSCS.IAMP_ASIGNA_ROL_USUARIO(upper(trim(LV_AUX_ROL)),
                                                    upper(PV_USUARIO),
                                                    LV_ERROR,
                                                    LV_MENSAJE_TECNICO);

      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        LV_ERROR := LV_ERROR;
        RAISE LE_MY_ERROR;
      END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN LE_MY_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_ASIGNA_ROL_USUARIO;

  --=================================================================
  --=================================================================
  -- Author    :  IRO Fernando Contreras
  -- Proyecto  :  [5406] Integracion de los Sistemas de Informacion
  -- Created   : 09/08/2010
  -- Purpose   : Ingreso de Novedades en Bitacora

  PROCEDURE IAMP_INGRESAR_BITACORA(PV_METODO           IN VARCHAR2,
                                   PV_APLICACION       IN VARCHAR2,
                                   PV_USUARIO          IN VARCHAR2,
                                   PV_USUARIO_OPERADOR IN VARCHAR2,
                                   PD_FECHA            IN DATE,
                                   LV_DIR_IP           IN VARCHAR2,
                                   PV_DESCRIPCION      IN VARCHAR2,
                                   LV_MSJ_SISTEMA      IN VARCHAR2,
                                   LV_MSJ_TECNICO      IN VARCHAR2,
                                   PV_ERROR            OUT VARCHAR2,
                                   PN_ERROR            OUT NUMBER,
                                   PV_MENSAJE_TECNICO  OUT VARCHAR2) IS

    PRAGMA AUTONOMOUS_TRANSACTION;

    LV_NOMBREPROGRAMA VARCHAR2(200) := GV_PAQUETE ||
                                       '.IAMP_INGRESAR_BITACORA';
    LN_ID_BITACORA    NUMBER;
    LE_ERROR EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001';
    PV_MENSAJE_TECNICO := '';
    PN_ERROR           := 1;

    BEGIN
      SELECT IAM_S_BITACORA_USUARIO.NEXTVAL INTO LN_ID_BITACORA FROM DUAL;
    END;

    INSERT INTO IAM_BITACORA_USUARIO_BSCS
      (ID_BITACORA,
       METODO,
       APLICACION,
       FECHA_TRANSACCION,
       USUARIO,
       USUARIO_OPERADOR,
       DIR_IP,
       DESCRIPCION,
       MSJ_SISTEMA,
       MSJ_TECNICO,
       ESTADO)
    VALUES
      (LN_ID_BITACORA,
       UPPER(TRIM(PV_METODO)),
       UPPER(TRIM(PV_APLICACION)),
       PD_FECHA,
       UPPER(TRIM(PV_USUARIO)),
       UPPER(TRIM(PV_USUARIO_OPERADOR)),
       TRIM(LV_DIR_IP),
       UPPER(TRIM(PV_DESCRIPCION)),
       LV_MSJ_SISTEMA,
       LV_MSJ_TECNICO,
       'A');

    COMMIT;
  EXCEPTION
    WHEN VALUE_ERROR THEN
      PV_ERROR := 'ER_0029';
      ROLLBACK;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := SUBSTR(SQLERRM,
                                   1,
                                   200) || '-' || LV_NOMBREPROGRAMA;
      PN_ERROR           := -1;
      ROLLBACK;
  END IAMP_INGRESAR_BITACORA;

  --=================================================================
  --=================================================================
  -- Author    :  IRO Fernando Contreras
  -- Proyecto  :  [5406] Integracion de los Sistemas de Informacion
  -- Created   : 09/08/2010
  -- Purpose   : Consultar mensajes de error

  PROCEDURE IAMP_RETORNA_MSJ_ERROR(PV_COD_INTERNO IN OUT VARCHAR2,
                                   PV_MSJ_USUARIO OUT VARCHAR2,
                                   PN_COD_RETORNO OUT NUMBER) IS
  BEGIN

    SELECT CD.MENSAJE_USUARIO, CD.COD_INTERNO_MSJ, CD.COD_RETORNO_MSJ
      INTO PV_MSJ_USUARIO, PV_COD_INTERNO, PN_COD_RETORNO
      FROM IAM_MENSAJE_ERRORES CD
     WHERE CD.COD_INTERNO_MSJ = UPPER(TRIM(PV_COD_INTERNO));

  EXCEPTION
    WHEN OTHERS THEN
      PV_COD_INTERNO := 'ER_0003';
      PV_MSJ_USUARIO := 'Error Tecnico';
      PN_COD_RETORNO := -1;
  END IAMP_RETORNA_MSJ_ERROR;

  --=================================================================
  --=================================================================
  -- Author   : IRO Fernando Contreras
  -- Proyecto : [5406] Integracion de los Sistemas de Informacion
  -- Created  : 09/08/2010
  -- Purpose  : Valida ip

  FUNCTION IAMP_VALIDA_IP(PC_IP IN VARCHAR2, PC_METODO IN VARCHAR2)
    RETURN BOOLEAN IS

    LC_EXISTE_IP C_EXISTE_IP%ROWTYPE;
    LB_EXISTE_IP BOOLEAN;

  BEGIN

    -- valida acceso desde una ip valida y configurada
    OPEN C_EXISTE_IP(PC_IP,
                     PC_METODO);
    FETCH C_EXISTE_IP
      INTO LC_EXISTE_IP;
    LB_EXISTE_IP := C_EXISTE_IP%FOUND;
    CLOSE C_EXISTE_IP;

    IF NOT LB_EXISTE_IP THEN
      RETURN FALSE; -- Usuario no tiene permiso para realizar la operacion
    END IF;

    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END IAMP_VALIDA_IP;

  --=================================================================
  --=================================================================
  -- Author    : IRO Fernando Contreras
  -- Proyecto  : [5406] Integracion de los Sistemas de Informacion
  -- Created   : 09/08/2010
  -- Purpose   : Deshace crear usuario

  PROCEDURE IAMP_DESHACER_CREA_USUARIO(PV_USUARIO         IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||
                                       '.IAMP_DESHACER_CREA_USUARIO';
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_USER_APP C_EXISTE_USER_APP%ROWTYPE;
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LB_FOUND_APP       BOOLEAN;
    LB_FOUND_DB        BOOLEAN;
  BEGIN
    PV_ERROR           := 'ER_0001';
    PV_MENSAJE_TECNICO := '';

    --Verifica existencia de usuario de aplicacion
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    IF LB_FOUND_APP THEN
       DELETE USER_CLASS UC WHERE UPPER(TRIM(UC.USERNAME)) = UPPER(TRIM(PV_USUARIO));
       DELETE USERACCESS US WHERE UPPER(TRIM(US.USERNAME)) = UPPER(TRIM(PV_USUARIO));

    IAMK_OBJ_USUARIO_BSCS.IAMP_ELIMINA_USUARIO_APP(PV_USUARIO,
                                                      PV_ERROR,
                                                      LV_MENSAJE_TECNICO);

    END IF;

    --Verifica si existe el usuario en la base
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB INTO LC_EXISTE_USER_DB;
    LB_FOUND_DB := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    IF LB_FOUND_DB THEN
      IAMK_OBJ_USUARIO_BSCS.IAMP_ELIMINA_USUARIO_BD(PV_USUARIO,
                                                    PV_ERROR,
                                                    LV_MENSAJE_TECNICO);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_DESHACER_CREA_USUARIO;

 --========================================================
 --========================================================
  PROCEDURE IAMP_DATOS_USUARIO(PV_USUARIO         IN VARCHAR2,
                               PV_DESCRIPCION     OUT VARCHAR2,
                               PV_GROUP_USER      OUT VARCHAR2,
                               PV_USER_TYPE       OUT VARCHAR2,
                               PV_BATCH_FLAG      OUT VARCHAR2,
                               PV_REC_VERSION     OUT NUMBER,
                               PV_MAS_DEFAULT_LNG OUT NUMBER,
                               PV_SECURITYUNIT    OUT NUMBER,
                               PV_TS_DEFAULT      OUT VARCHAR2,
                               PV_TS_TEMPORAL     OUT VARCHAR2,
                               PV_PROFILE         OUT VARCHAR2,
                               PV_STATUS          OUT VARCHAR2,
                               PV_ERROR           OUT VARCHAR2,
                               PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.IAMP_DATOS_USUARIO';
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LC_EXISTE_USER_APP C_EXISTE_USER_APP%ROWTYPE;
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_ERROR           VARCHAR2(1024);
    LB_FOUND_BD        BOOLEAN;
    LB_FOUND_APP       BOOLEAN;
    LE_ERROR EXCEPTION;
  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF PV_USUARIO IS NULL THEN
      -- verifica si usuario no es nulo
      LV_ERROR := 'ER_0023';
      RAISE LE_ERROR;
    END IF;

    --Verifica existencia de usuario de base
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB
      INTO LC_EXISTE_USER_DB;
    LB_FOUND_BD := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    --Verifica existencia de usuario de aplicacion
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP
      INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    IF LB_FOUND_BD AND LB_FOUND_APP THEN

      SELECT U.DESCRIPTION
        INTO PV_DESCRIPCION
        FROM USERS U
       WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

      SELECT U.GROUP_USER
        INTO PV_GROUP_USER
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

        SELECT U.USER_TYPE
        INTO PV_USER_TYPE
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));


        SELECT U.BATCH_FLAG
        INTO PV_BATCH_FLAG
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

        SELECT U.REC_VERSION
        INTO PV_REC_VERSION
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

        SELECT U.MAS_DEFAULT_LNG
        INTO PV_MAS_DEFAULT_LNG
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

        SELECT U.SECURITYUNIT
        INTO PV_SECURITYUNIT
        FROM USERS U
        WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

        SELECT U.DEFAULT_TABLESPACE
        INTO  PV_TS_DEFAULT
        FROM DBA_USERS U
       WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

       SELECT U.TEMPORARY_TABLESPACE
        INTO  PV_TS_TEMPORAL
        FROM DBA_USERS U
       WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

       SELECT U.PROFILE
        INTO  PV_PROFILE
        FROM DBA_USERS U
       WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));

       SELECT U.ACCOUNT_STATUS
        INTO  PV_STATUS
        FROM DBA_USERS U
       WHERE upper(U.USERNAME) = upper(TRIM(PV_USUARIO));



    ELSE
      LV_ERROR := 'ER_0033';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR           := LV_ERROR;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

  END IAMP_DATOS_USUARIO;

 --========================================================
 --========================================================

PROCEDURE IAMP_CONSULTA_USUARIO_ROLES(PV_USUARIO         IN VARCHAR2,
                                      PV_LISTA_ROLES     OUT CLOB,
                                      PV_ERROR           OUT VARCHAR2,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2)IS



    -- variables locales
    LV_PROGRAMA   VARCHAR2(61) := GV_PAQUETE ||
                                  '.IAMP_CONSULTA_USUARIO_ROLES';
    LV_SALIDA     VARCHAR2(200);
    LV_SALIDA_ALL CLOB := EMPTY_CLOB();

        CURSOR C_USUARIO_ROLES IS
      SELECT U.GROUP_USER REG
        FROM USERS U
       WHERE UPPER(U.USERNAME) = UPPER(TRIM(PV_USUARIO))
       AND U.GROUP_USER IS NOT NULL
       ORDER BY UPPER(U.USERNAME);


  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    DBMS_LOB.CREATETEMPORARY(LV_SALIDA_ALL,
                             TRUE);

    FOR I IN C_USUARIO_ROLES LOOP
      LV_SALIDA := I.REG || ';';
      DBMS_LOB.WRITEAPPEND(LV_SALIDA_ALL,
                           LENGTH(LV_SALIDA),
                           LV_SALIDA);
    END LOOP;

    PV_LISTA_ROLES := LV_SALIDA_ALL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PV_ERROR           := 'ER_0002';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CONSULTA_USUARIO_ROLES;

 --========================================================
 --========================================================

PROCEDURE IAMP_CONSULTA_ROL_USUARIOS(PV_ROL         IN VARCHAR2,
                                        PV_LISTA_USUARIOS     OUT CLOB,
                                        PV_ERROR           OUT VARCHAR2,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2)IS



    -- variables locales
    LV_PROGRAMA   VARCHAR2(61) := GV_PAQUETE ||
                                  '.IAMP_CONSULTA_ROL_USUARIOS';
    LV_SALIDA     VARCHAR2(200);
    LV_SALIDA_ALL CLOB := EMPTY_CLOB();

        CURSOR C_ROLE_USUARIOS IS
        SELECT U.GROUP_USER || '|' || U.USERNAME REG
        FROM USERS U
        WHERE UPPER(U.GROUP_USER) = UPPER(TRIM(PV_ROL))
        ORDER BY UPPER(U.GROUP_USER);


  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    DBMS_LOB.CREATETEMPORARY(LV_SALIDA_ALL,
                             TRUE);

    FOR I IN C_ROLE_USUARIOS LOOP
      LV_SALIDA := I.REG || ';';
      DBMS_LOB.WRITEAPPEND(LV_SALIDA_ALL,
                           LENGTH(LV_SALIDA),
                           LV_SALIDA);
    END LOOP;

    PV_LISTA_USUARIOS := LV_SALIDA_ALL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PV_ERROR           := 'ER_0002';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CONSULTA_ROL_USUARIOS;

 --========================================================
 -- Author       : IRO MIGUEL MU�OZ
 -- Lider Porta  : SIS Jorge Chicala
 -- Lider Iroute : IRO Jorge Suarez.
 -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
 -- Created      : 28/02/2011
 -- Purpose      : CONSULTA USUARIOS Y ROLES
 --========================================================

  PROCEDURE IAMP_CONSULTA_USUARIO_Y_ROLES(PV_LISTA           OUT CLOB,
                                          PV_ERROR           OUT VARCHAR2,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    --USUARIOS Y ROLES DE LA APLICACION
       CURSOR C_USUARIOS_Y_ROLES IS
       SELECT U.USERNAME || '|' ||A.GRANTED_ROLE REG
       FROM DBA_ROLE_PRIVS A, USERS U
       WHERE U.USERNAME=A.GRANTEE
       AND A.GRANTED_ROLE='BSCS_ROLE'
       UNION
       SELECT U.USERNAME || '|' ||U.GROUP_USER --REG
       FROM DBA_ROLE_PRIVS A, USERS U
       WHERE U.USERNAME=UPPER(TRIM(A.GRANTEE))
       AND A.GRANTED_ROLE=UPPER('BSCS_ROLE')
       AND A.GRANTED_ROLE <> UPPER('CONNECT')
       AND U.GROUP_USER <> 'ACCPROH'
       AND U.GROUP_USER IS NOT NULL
       ORDER BY REG;

    -- variables locales
    LV_PROGRAMA   VARCHAR2(61) := GV_PAQUETE ||
                                  '.IAMP_CONSULTA_USUARIO_Y_ROLES';
    LV_SALIDA     VARCHAR2(200);
    LV_SALIDA_ALL CLOB := EMPTY_CLOB();

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    DBMS_LOB.CREATETEMPORARY(LV_SALIDA_ALL,
                             TRUE);

    FOR I IN C_USUARIOS_Y_ROLES LOOP
      LV_SALIDA := I.REG || ';';
      DBMS_LOB.WRITEAPPEND(LV_SALIDA_ALL,
                           LENGTH(LV_SALIDA),
                           LV_SALIDA);
    END LOOP;


    PV_LISTA := LV_SALIDA_ALL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PV_ERROR           := 'ER_0002';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CONSULTA_USUARIO_Y_ROLES;

 --========================================================
 --========================================================

  PROCEDURE IAMP_CONSULTA_LISTA_ROLES(PV_LISTA_ROLES     OUT CLOB,
                                      PV_ERROR           OUT VARCHAR2,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA   VARCHAR2(61) := GV_PAQUETE ||
                                  '.IAMP_CONSULTA_LISTA_ROLES';
    LV_SALIDA     VARCHAR2(200);
    LV_SALIDA_ALL CLOB := EMPTY_CLOB();

    CURSOR C_LISTA_ROLES IS
      SELECT DISTINCT U.GROUP_USER  REG
        FROM USERS U where u.group_user not in (' ')
       ORDER BY U.GROUP_USER;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    DBMS_LOB.CREATETEMPORARY(LV_SALIDA_ALL,
                             TRUE);

    FOR I IN C_LISTA_ROLES LOOP
      LV_SALIDA := I.REG || ';';
      DBMS_LOB.WRITEAPPEND(LV_SALIDA_ALL,
                           LENGTH(LV_SALIDA),
                           LV_SALIDA);
    END LOOP;

    PV_LISTA_ROLES := LV_SALIDA_ALL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PV_ERROR           := 'ER_0002';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CONSULTA_LISTA_ROLES;

 --========================================================
 --========================================================
 -- Author       : IRO Fernando Contreras
 -- Lider Porta  : SIS Jorge Chicala
 -- Lider Iroute : IRO Jorge Suarez.
 -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
 -- Created      : 12/10//2010
 -- Purpose      : valida formato de clave minimo 6 letras, 2 numeros
 Function IAMF_VALIDA_CLAVE(Pv_clave           In Varchar2,
                            Pv_usuario         In Varchar2,
                            Pv_error           Out Varchar2,
                            Pv_Mensaje_Tecnico Out Varchar2) Return Boolean Is

Lv_letra          Varchar2(20);
Ln_cuenta_letras  Number;
Ln_cuenta_numeros Number;

Begin
Ln_cuenta_letras:=0;
Ln_cuenta_numeros:=0;


    IF Pv_clave IS NULL THEN
      Pv_error := 'ER_0006';
      Return False;
    END IF;

    IF (LENGTH(Pv_clave) < 8) Or LENGTH(Pv_clave) > 8 THEN
      Pv_error := 'ER_0036';
      Return False;
    END IF;

    IF Pv_clave = PV_USUARIO THEN
      Pv_error := 'ER_0039';
      Return False;
    END IF;

-- recorre cadena clave preguntando si existen minimo 2 letras y 2 numeros
For i In 1..length (Pv_clave) Loop
Lv_letra:=substr(Pv_clave, i, 1);

      If (ascii(Lv_letra)>= ascii('A') And ascii(Lv_letra)<= ascii('Z')) Or
        (ascii(Lv_letra)>= ascii('a') And ascii(Lv_letra)<=ascii('z'))  Then
         Ln_cuenta_letras:=Ln_cuenta_letras+1;
      End If;
                If ascii(Lv_letra)>= ascii('0') And   ascii(Lv_letra)<= ascii('9') Then
                Ln_cuenta_numeros:=Ln_cuenta_numeros+1;
                End If;

     Exit When  Ln_cuenta_letras >=6 And Ln_cuenta_numeros>=2;
End Loop;


If ln_cuenta_letras <6 Then
Pv_error:='ER_0030';  -- La clave debe tener minimo seis letras
Return False;
End If;

If ln_cuenta_numeros <2 Then
Pv_error:='ER_0037';   -- La clave debe tener minimo dos numeros
Return False;
End If;

Pv_Mensaje_Tecnico:=Null;
Return True;

Exception
When Others Then
Pv_error:='ER_0003';
Pv_Mensaje_Tecnico:='Error: '|| SUBSTR(SQLERRM, 1, 512);
Return False;

end IAMF_VALIDA_CLAVE;
END IAMK_TRX_USUARIO_BSCS;
/
