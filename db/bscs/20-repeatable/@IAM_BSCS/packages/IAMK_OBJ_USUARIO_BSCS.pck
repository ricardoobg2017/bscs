CREATE OR REPLACE PACKAGE IAMK_OBJ_USUARIO_BSCS IS

  PROCEDURE IAMP_CREA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_TSDEFAULT       IN VARCHAR2,
                                 PV_TSTEMP          IN VARCHAR2,
                                 PV_PROFILE         IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CREA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                  PV_DESCRPTION      IN VARCHAR2,
                                  PV_BATCH_FLAG      IN VARCHAR2,
                                  PN_REC_VERSION     IN NUMBER,
                                  PV_USER_TYPE       IN VARCHAR2,
                                  PN_MAS_DEFAULT_LNG IN NUMBER,
                                  PN_SECURITYUNIT    IN NUMBER,

                                  PV_ERROR           OUT VARCHAR2,
                                  PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ELIMINA_USUARIO_BD(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ELIMINA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                     PV_ERROR           OUT VARCHAR2,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_BLOQUEA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_DESBLOQUEA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CREAR_ROL(PV_ROL             IN VARCHAR2,
                           PV_DESCRPTION      IN VARCHAR2,
                           PV_ERROR           OUT VARCHAR2,
                           PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_QUITAR_ROL_APP(PV_USUARIO         IN VARCHAR2,
                                PV_ROL             IN VARCHAR2,
                                PV_ERROR           OUT VARCHAR2,
                                PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ASIGNA_ROL_USUARIO(PV_ROL IN VARCHAR2,
                                    PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_RESETEO_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2);
END IAMK_OBJ_USUARIO_BSCS;
/
CREATE OR REPLACE PACKAGE BODY IAMK_OBJ_USUARIO_BSCS IS

  GV_PAQUETE VARCHAR2(30) := 'AMK_OBJ_USUARIO_BSCS';

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Crear Usuario a nivel de Base de Datos
  PROCEDURE IAMP_CREA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_TSDEFAULT       IN VARCHAR2,
                                 PV_TSTEMP          IN VARCHAR2,
                                 PV_PROFILE         IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA            VARCHAR2(61) := GV_PAQUETE || '.' ||
                                           'AMP_CREA_USUARIO_DB';
    LV_SENTENCIA           VARCHAR2(1000);
    LV_SENTENCIA2          VARCHAR2(1000);
    LV_DEFAULTTABLESPACE   VARCHAR2(200);
    LV_TEMPORARYTABLESPACE VARCHAR2(200);
    LV_PROFILE             VARCHAR2(200);
    LE_ERROR               EXCEPTION;

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    IF (PV_PASSWORD IS NULL) THEN
      PV_ERROR := 'ER_0006';
      RAISE LE_ERROR;
    ELSE

      --Asigna por defecto al DEFAULT_TABLESPACE
      IF (PV_TSDEFAULT IS NULL) THEN
        LV_DEFAULTTABLESPACE := 'DATA';
      ELSE
        LV_DEFAULTTABLESPACE := PV_TSDEFAULT;
      END IF;

      --Asigna por defecto al  TEMPORARY_TABLESPACE
      IF (PV_TSTEMP IS NULL) THEN
        LV_TEMPORARYTABLESPACE := 'TEMP_USERS';
      ELSE
        LV_TEMPORARYTABLESPACE := PV_TSTEMP;
      END IF;

      --Asigna por defecto al PROFILE el valor de  'DEFAULT'
      IF (PV_PROFILE IS NULL) THEN
        LV_PROFILE := 'DEFAULT';
      ELSE
        LV_PROFILE := PV_PROFILE;
      END IF;

      -- creación de usuario en BD
      LV_SENTENCIA := 'CREATE USER ' || PV_USUARIO || ' IDENTIFIED BY ' ||
                      PV_PASSWORD || ' DEFAULT TABLESPACE ' ||
                      LV_DEFAULTTABLESPACE || ' TEMPORARY TABLESPACE ' ||
                      LV_TEMPORARYTABLESPACE || ' PROFILE ' || LV_PROFILE;
      EXECUTE IMMEDIATE LV_SENTENCIA;

      -- Asignación de roles al usuario creado
      LV_SENTENCIA2 := 'GRANT CONNECT , BSCS_ROLE  ' || ' TO ' ||
                       PV_USUARIO;
      EXECUTE IMMEDIATE LV_SENTENCIA2;

    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_CREA_USUARIO_DB;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : crea usuario a nivel de APP

  PROCEDURE IAMP_CREA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                  PV_DESCRPTION      IN VARCHAR2,
                                  PV_BATCH_FLAG      IN VARCHAR2,
                                  PN_REC_VERSION     IN NUMBER,
                                  PV_USER_TYPE       IN VARCHAR2,
                                  PN_MAS_DEFAULT_LNG IN NUMBER,
                                  PN_SECURITYUNIT    IN NUMBER,
                                  PV_ERROR           OUT VARCHAR2,
                                  PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.AMP_CREA_USUARIO_APP';
    LE_ERROR    EXCEPTION;

    --Ln_Id_SR_USER_GROUPS NUMBER;
  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    INSERT INTO sysadm.USERS -- CREA USUARIO APP

    VALUES
      (UPPER(PV_USUARIO),
       UPPER(PV_DESCRPTION),
       NULL,
       SYSDATE,
       NULL,
       NULL,
       UPPER(PV_BATCH_FLAG),
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       PN_REC_VERSION,
       UPPER(PV_USER_TYPE),
       PN_MAS_DEFAULT_LNG,
       PN_SECURITYUNIT,
       NULL);

    -- INSERTA EN AUDITTRAIL
    INSERT INTO SYSADM.AUDITTRAIL
    VALUES
      ('SY',
       SYSDATE,
       'SYSTBE', -- DEFAULT
       'USERS', -- DEFAULT
       'I', -- DEFAULT
       0);

    -- INSERTA EN AUDITTRAIL
    INSERT INTO SYSADM.AUDITTRAIL
    VALUES
      ('SY',
       SYSDATE,
       'SYVOZV', -- DEFAULT
       'USERACCESS', -- DEFAULT
       'I', -- DEFAULT
       0);

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_CREA_USUARIO_APP;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Elimina Usuario APP

  PROCEDURE IAMP_ELIMINA_USUARIO_APP(PV_USUARIO         IN VARCHAR2,
                                     PV_ERROR           OUT VARCHAR2,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.AMP_ELIMINA_USUARIO_APP';
    --LV_SENTENCIA VARCHAR2(1000);

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';
     DBMS_OUTPUT.ENABLE(100000);
   /*DELETE USERACCESS US WHERE UPPER(US.USERNAME) = UPPER(TRIM(PV_USUARIO));
   DELETE USER_CLASS UC WHERE UPPER(UC.USERNAME) = UPPER(TRIM(PV_USUARIO));
   */
   DELETE USERS U WHERE UPPER(TRIM(U.USERNAME))= UPPER(TRIM(PV_USUARIO));

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_ELIMINA_USUARIO_APP;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Elimina Usuario a nivel de Base de datos.

  PROCEDURE IAMP_ELIMINA_USUARIO_BD(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA  VARCHAR2(61) := GV_PAQUETE || '.AMP_ELIMINA_USUARIO_BD';
    LV_SENTENCIA VARCHAR2(1000);

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    LV_SENTENCIA := 'DROP USER ' || PV_USUARIO;
    EXECUTE IMMEDIATE LV_SENTENCIA;

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_ELIMINA_USUARIO_BD;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Bloquea Usuario a nivel de Base de datos.
  PROCEDURE IAMP_BLOQUEA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.IAMP_BLOQUEA_USUARIO_DB';

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    EXECUTE IMMEDIATE 'Alter User ' || UPPER(PV_USUARIO) || ' Account Lock';

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || 'en ' ||
                            LV_PROGRAMA;
  END IAMP_BLOQUEA_USUARIO_DB;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Desbloquea Usuario a nivel de Base de datos.

  PROCEDURE IAMP_DESBLOQUEA_USUARIO_DB(PV_USUARIO         IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(61) := GV_PAQUETE || '.IAMP_DESBLOQUEA_USUARIO_DB';

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    EXECUTE IMMEDIATE 'alter user ' || UPPER(PV_USUARIO) ||
                      ' account unlock';

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_DESBLOQUEA_USUARIO_DB;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Crea Rol en la tabla sr_user_groups

  PROCEDURE IAMP_CREAR_ROL(PV_ROL             IN VARCHAR2,
                           PV_DESCRPTION      IN VARCHAR2,
                           PV_ERROR           OUT VARCHAR2,
                           PV_MENSAJE_TECNICO OUT VARCHAR2) IS

   --variables locales
   LV_PROGRAMA VARCHAR2(100) := GV_PAQUETE || '.AMP_CREAR_ROL';

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';


    INSERT INTO USERS -- CREA USUARIO APP
    VALUES
      (UPPER(TRIM(PV_ROL)),
       UPPER(TRIM(PV_DESCRPTION)),
       NULL,
       SYSDATE,
       NULL,
       'X',
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       0,
       'G',
        2,
        2,
       NULL);

 EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_CREAR_ROL;

  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Informacion
  -- Created      : 02/09//2010
  -- Purpose      : Quita Rol a nivel de Base de datos.
  PROCEDURE IAMP_QUITAR_ROL_APP(PV_USUARIO         IN VARCHAR2, -- usuario a quien se le quita rol
                                PV_ROL             IN VARCHAR2, -- rol que se le revoca a usuario
                                PV_ERROR           OUT VARCHAR2,
                                PV_MENSAJE_TECNICO OUT VARCHAR2) IS
    -- variables locales
    LV_PROGRAMA VARCHAR2(100) := GV_PAQUETE || '.IAMP_QUITAR_ROL_APP';
    -- Lv_Sentencia Varchar2(1000);

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- SOLO SETEA EL CAMPO GROUP_USER
    UPDATE USERS U
       SET GROUP_USER = ''
     WHERE UPPER(USERNAME) = UPPER(TRIM(PV_USUARIO))
       AND UPPER(GROUP_USER) = UPPER(TRIM(PV_ROL));

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
  END IAMP_QUITAR_ROL_APP;

  --=================================================================
  --=================================================================
  -- Author       : IRO Rosangela Espinoza
  -- Proyecto     : [5406] Integracion de los Sistemas de Información
  -- Created      : 02/09//2010
  -- Purpose      : Asigna 1 rol (o varios roles separados por signo ;) a un usuario a nivel de Base de datos

  PROCEDURE IAMP_ASIGNA_ROL_USUARIO(PV_ROL             IN VARCHAR2,
                                    PV_USUARIO         IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA VARCHAR2(100) := GV_PAQUETE || '.IAMP_ASIGNA_ROL_USUARIO';

  BEGIN
  DBMS_OUTPUT.ENABLE(100000);
    PV_ERROR           := 'ER_0001';    -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    UPDATE USERS U
       SET GROUP_USER = UPPER(TRIM(PV_ROL))
     WHERE UPPER(USERNAME) = UPPER(TRIM(PV_USUARIO));

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_ASIGNA_ROL_USUARIO;

  PROCEDURE IAMP_RESETEO_USUARIO(PV_USUARIO         IN VARCHAR2,
                                 PV_PASSWORD        IN VARCHAR2,
                                 PV_ERROR           OUT VARCHAR2,
                                 PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA  VARCHAR2(100) := GV_PAQUETE || '.IAMP_RESETEO_USUARIO';
    LV_SENTENCIA VARCHAR2(1000);

  BEGIN
    PV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    LV_SENTENCIA := 'alter user  ' || PV_USUARIO || ' ' ||
                    '  identified by ' || PV_PASSWORD || ' ';
    EXECUTE IMMEDIATE LV_SENTENCIA;

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en el paquete ' ||
                            LV_PROGRAMA;
  END IAMP_RESETEO_USUARIO;

END IAMK_OBJ_USUARIO_BSCS;
/
