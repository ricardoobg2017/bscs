CREATE OR REPLACE Package IAMK_OBJ_USUARIO_ADAM Is

  Procedure IAMP_CREA_USUARIO_DB(Pv_usuario         In Varchar2,
								 Pv_password        In Varchar2,
								 Pv_tsdefault       In Varchar2,
								 Pv_tstemp          In Varchar2,
								 Pv_profile         In Varchar2,
								 Pv_error           Out Varchar2,
								 Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_CREA_USUARIO_APP(Pv_usuario         In Varchar2,
								  Pv_descripcion     In Varchar2,
								  Pv_bloqueo         In Varchar2,
								  Pv_error           Out Varchar2,
								  Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_BLOQUEA_USUARIO_APP(Pv_usuario         In Varchar2,
									 Pv_error           Out Varchar2,
									 Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_BLOQUEA_USUARIO_DB(Pv_usuario         In Varchar2,
									Pv_error           Out Varchar2,
									Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_DESBLOQUEA_USUARIO_APP(Pv_usuario         In Varchar2,
										Pv_error           Out Varchar2,
										Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_DESBLOQUEA_USUARIO_DB(Pv_usuario         In varchar2,
									   Pv_error           Out varchar2,
									   Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_RECREA_USUARIO_APP(Pv_usuario         In Varchar2,
									Pv_error           Out Varchar2,
									Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_ELIMINA_USUARIO_BD(Pv_usuario         In Varchar2,
									Pv_error           Out Varchar2,
									Pv_Mensaje_Tecnico Out Varchar2);

  -- Author       : IRO Rosa Espinoza, IRO Fernando Contreras
  -- Lider Porta  : SIS Jorge Chicala
  -- Lider Iroute : IRO Jorge Suarez
  -- Proyecto     : [5406] Integración de los Sistemas de Información de Conecel con IAM
  -- Created      : 15/08/2010
  -- Purpose      : Para TRX de la API de gestion de usuario

  Procedure IAMP_CREAR_ROL_DB(Pv_Rol             In Varchar2,
							  Pv_Error           Out Varchar2,
							  Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_QUITAR_ROL_DB(Pv_Usuario         In Varchar2,
							   Pv_Rol             In Varchar2,
							   Pv_Error           Out Varchar2,
							   Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_RESETEO_USUARIO(Pv_Usuario         In Varchar2,
								 Pv_password        In Varchar2,
								 Pv_Error           Out Varchar2,
								 Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_ASIGNA_ROL_USUARIO(Pv_Rol             In Varchar2,
									Pv_Usuario         In Varchar2,
									Pv_Error           Out Varchar2,
									Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_UPDATE_SINCRONIZA_TRB(Pn_Secuencial      In Number, -- parametro clave unica
									   Pv_usuario         In Varchar2,
									   Pv_Mail            In Varchar2,
									   Pn_Resultado       In Number,
									   Pv_Observacion     In Varchar2,
									   Pv_error           Out Varchar2,
									   Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_UPDATE_TRABAJADORES(Pv_Id_trabajador   In Varchar2,
									 Pv_usuario         In Varchar2,
									 Pv_Mail            In Varchar2,
									 Pv_error           Out Varchar2,
									 Pv_Mensaje_Tecnico Out Varchar2);

  Procedure IAMP_UPD_EST_IAM_SNCRNZ_TRB(Pn_Secuencial      In Number, -- parametro clave unica
										Pn_Resultado       In Number,
										Pv_Observacion     In Varchar2,
										Pv_error           Out Varchar2,
										Pv_Mensaje_Tecnico Out Varchar2);

End IAMK_OBJ_USUARIO_ADAM;

/
