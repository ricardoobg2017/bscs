CREATE OR REPLACE PACKAGE IAMK_API_USUARIO_BSCS IS

  -- declaracion de metodos
  PROCEDURE IAMP_CREA_USUARIO_BSCS(PV_USUARIO   IN VARCHAR2,
                                   PV_PASSWORD  IN VARCHAR2,
                                   PV_TSDEFAULT IN VARCHAR2,
                                   PV_TSTEMP    IN VARCHAR2,
                                   PV_PROFILE   IN VARCHAR2,
                                   PV_DESCRPTION      IN VARCHAR2,
                                   PV_BATCH_FLAG      IN VARCHAR2,
                                   PN_REC_VERSION     IN NUMBER,
                                   PV_USER_TYPE       IN VARCHAR2,
                                   PN_MAS_DEFAULT_LNG IN NUMBER,
                                   PN_SECURITYUNIT    IN NUMBER,
                                   PV_MSJ_ERROR       OUT VARCHAR2,
                                   PN_ERROR           OUT NUMBER,
                                   PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ACTUALIZA_USUARIO_BSCS(PV_USUARIO   IN VARCHAR2,
                                        PV_PASSWORD  IN VARCHAR2,
                                        PV_TSDEFAULT IN VARCHAR2,
                                        PV_TSTEMP    IN VARCHAR2,
                                        PV_PROFILE   IN VARCHAR2,
                                        PV_DESCRPTION      IN VARCHAR2,
                                        PV_BATCH_FLAG      IN VARCHAR2,
                                        PN_REC_VERSION     IN NUMBER,
                                        PV_USER_TYPE       IN VARCHAR2,
                                        PN_MAS_DEFAULT_LNG IN NUMBER,
                                        PN_SECURITYUNIT    IN NUMBER,
                                        PV_MSJ_ERROR       OUT VARCHAR2,
                                        PN_ERROR           OUT NUMBER,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ELIMINA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PC_LISTADO_ROL     IN VARCHAR2,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_BLOQUEA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ACTIVA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                     PV_MSJ_ERROR       OUT VARCHAR2,
                                     PN_ERROR           OUT NUMBER,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_REVOCA_ROLES_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                           PV_ROL             IN VARCHAR2,
                                           PV_MSJ_ERROR       OUT VARCHAR2,
                                           PN_ERROR           OUT NUMBER,
                                           PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CREA_ROL_BSCS(PV_ID_ROL          IN VARCHAR2,
                               PV_DESCRPTION      IN VARCHAR2,
                               PV_MSJ_ERROR       OUT VARCHAR2,
                               PN_ERROR           OUT NUMBER,
                               PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_RESETEO_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PV_PASSWORD        IN VARCHAR2,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_ASIGNA_ROLES_USUARIO_BSCS(PV_ROL             IN VARCHAR2,
                                           PV_USUARIO         IN VARCHAR2,
                                           PV_MSJ_ERROR       OUT VARCHAR2,
                                           PN_ERROR           OUT NUMBER,
                                           PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_DESHACER_CREA_USUARIO_BS(PV_USUARIO         IN VARCHAR2,
                                          PV_MSJ_ERROR       OUT VARCHAR2,
                                          PN_ERROR           OUT NUMBER,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_DATOS_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                    PV_DESCRIPCION     OUT VARCHAR2,
                                    PV_GROUP_USER      OUT VARCHAR2,
                                    PV_USER_TYPE       OUT VARCHAR2,
                                    PV_BATCH_FLAG      OUT VARCHAR2,
                                    PV_REC_VERSION     OUT NUMBER,
                                    PV_MAS_DEFAULT_LNG OUT NUMBER,
                                    PV_SECURITYUNIT    OUT NUMBER,
                                    PV_TS_DEFAULT      OUT VARCHAR2,
                                    PV_TS_TEMPORAL     OUT VARCHAR2,
                                    PV_PROFILE         OUT VARCHAR2,
                                    PV_STATUS          OUT VARCHAR2,
                                    PV_MSJ_ERROR       OUT VARCHAR2,
                                    PN_ERROR           OUT NUMBER,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CONSULTA_USR_ROLES_BSCS(PV_USUARIO         IN VARCHAR2,
                                         PV_LISTA_ROL       OUT CLOB,
                                         PV_MSJ_ERROR       OUT VARCHAR2,
                                         PN_ERROR           OUT NUMBER,
                                         PV_MENSAJE_TECNICO OUT VARCHAR2);


  PROCEDURE IAMP_CONSULTA_ROL_USRS_BSCS(PV_ROL             IN VARCHAR2,
                                        PV_LISTA_USER      OUT CLOB,
                                        PV_MSJ_ERROR       OUT VARCHAR2,
                                        PN_ERROR           OUT NUMBER,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2);

  PROCEDURE IAMP_CONSULTA_USRS_ROLES_BSCS(PV_RESUL_CONSULTA  OUT CLOB,
                                          PV_MSJ_ERROR       OUT VARCHAR2,
                                          PN_ERROR           OUT NUMBER,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2);


PROCEDURE IAMP_CONSULTA_ROLES_BSCS(PV_RESUL_CONSULTA  OUT CLOB,
                                     PV_MSJ_ERROR       OUT VARCHAR2,
                                     PN_ERROR           OUT NUMBER,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2);

END IAMK_API_USUARIO_BSCS;
/
CREATE OR REPLACE PACKAGE BODY IAMK_API_USUARIO_BSCS IS

  -- variable global que contiene el nombre del paquete
  GV_PAQUETE VARCHAR2(30) := 'IAMK_API_USUARIO_BSCS';

    -- valida si existe usuario de base de datos
  CURSOR C_EXISTE_USER_DB(CV_USUARIO VARCHAR2) IS
    SELECT * FROM DBA_USERS X WHERE UPPER(X.USERNAME) = UPPER(TRIM(CV_USUARIO));

  -- valida si existe usuario de aplicacion
  CURSOR C_EXISTE_USER_APP(CV_USUARIO VARCHAR2) IS
    SELECT * FROM USERS X WHERE UPPER(X.USERNAME) = UPPER(TRIM(CV_USUARIO));

  PROCEDURE IAMP_CREA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                   PV_PASSWORD        IN VARCHAR2,
                                   PV_TSDEFAULT       IN VARCHAR2,
                                   PV_TSTEMP          IN VARCHAR2,
                                   PV_PROFILE         IN VARCHAR2,
                                   PV_DESCRPTION      IN VARCHAR2,
                                   PV_BATCH_FLAG      IN VARCHAR2,
                                   PN_REC_VERSION     IN NUMBER,
                                   PV_USER_TYPE       IN VARCHAR2,
                                   PN_MAS_DEFAULT_LNG IN NUMBER,
                                   PN_SECURITYUNIT    IN NUMBER,
                                   PV_MSJ_ERROR       OUT VARCHAR2,
                                   PN_ERROR           OUT NUMBER, -- codigo de error configurado 1 exito - 0 error controlado - -1 error tecnivo
                                   PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.IAMP_CREA_USUARIO_BSCS';
    LC_EXISTE_USER_APP C_EXISTE_USER_APP%ROWTYPE;
    LC_EXISTE_USER_DB  C_EXISTE_USER_DB%ROWTYPE;
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(500);
    LV_DIR_IP          VARCHAR2(20);
    LV_SENTENCIA       VARCHAR2(200);
    LN_ERROR           NUMBER;
    LB_FOUND           BOOLEAN;
    LB_FOUND_APP       BOOLEAN;
    LE_ERROR           EXCEPTION;
    LE_ERROR_MSJ       EXCEPTION;
    LE_MY_ERROR        EXCEPTION;
    LV_SENT_AUX        VARCHAR2(200);
    LN_BANDERA_AUX     NUMBER:=0;

  BEGIN

    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
    INTO LV_DIR_IP
    FROM DUAL;

    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                UPPER(GV_PAQUETE)) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR_MSJ;
    END IF;

    --Verifica que exista el usuario en DB
    OPEN C_EXISTE_USER_DB(PV_USUARIO);
    FETCH C_EXISTE_USER_DB INTO LC_EXISTE_USER_DB;
    LB_FOUND := C_EXISTE_USER_DB%FOUND;
    CLOSE C_EXISTE_USER_DB;

    --Verifica existencia de usuario de APP
    OPEN C_EXISTE_USER_APP(PV_USUARIO);
    FETCH C_EXISTE_USER_APP INTO LC_EXISTE_USER_APP;
    LB_FOUND_APP := C_EXISTE_USER_APP%FOUND;
    CLOSE C_EXISTE_USER_APP;

    /*IF LB_FOUND AND LB_FOUND_APP THEN
       LV_ERROR:='ER_0044';
       RAISE LE_ERROR_MSJ;*/
    IF LB_FOUND AND LB_FOUND_APP THEN
      IF TRIM(LC_EXISTE_USER_DB.ACCOUNT_STATUS)='LOCKED' THEN
          IF LC_EXISTE_USER_APP.GROUP_USER LIKE 'ACCPROH%' OR LC_EXISTE_USER_APP.GROUP_USER IS NULL  THEN
             LV_SENT_AUX:='DROP USER '||PV_USUARIO;
             EXECUTE IMMEDIATE LV_SENT_AUX;
             LN_BANDERA_AUX:=1; 
          ELSE
             LV_ERROR:='ER_0044';
             RAISE LE_ERROR_MSJ;
          END IF;
          
      ELSE
          LV_ERROR:='ER_0044';
          RAISE LE_ERROR_MSJ;        
      END IF;
      
    END IF;  
        
      IF ((NOT LB_FOUND) AND LB_FOUND_APP) OR LN_BANDERA_AUX=1 THEN
      --LV_ERROR := 'ER_0041';
     -- RAISE LE_ERROR_MSJ;
      IAMK_TRX_USUARIO_BSCS.IAMP_CREA_USUARIO_DB(upper(trim(PV_USUARIO)),
                                                   upper(trim(PV_PASSWORD)),
                                                   upper(trim(PV_TSDEFAULT)),
                                                   upper(trim(PV_TSTEMP)),
                                                   upper(trim(PV_PROFILE)),
                                                   upper(trim(PV_DESCRPTION)),
                                                   upper(trim(PV_BATCH_FLAG)),
                                                   PN_REC_VERSION,
                                                   upper(trim(PV_USER_TYPE)),
                                                   PN_MAS_DEFAULT_LNG,
                                                   PN_SECURITYUNIT,
                                                   LV_ERROR,
                                                   LV_MENSAJE_TECNICO);
              IF LV_MENSAJE_TECNICO IS NOT NULL THEN
                RAISE LE_ERROR;
              END IF;

        IAMK_TRX_USUARIO_BSCS.IAMP_ACTUALIZA_USUARIO(PV_USUARIO         => upper(trim(PV_USUARIO)),
                                                 PV_PASSWORD        => upper(trim(PV_PASSWORD)),
                                                 PV_TSDEFAULT       => upper(trim(PV_TSDEFAULT)),
                                                 PV_TSTEMP          => upper(trim(PV_TSTEMP)),
                                                 PV_PROFILE         => upper(trim(PV_PROFILE)),
                                                 PV_DESCRPTION      => upper(trim(PV_DESCRPTION)),
                                                 PV_BATCH_FLAG      => upper(trim(PV_BATCH_FLAG)),
                                                 PN_REC_VERSION     => PN_REC_VERSION,
                                                 PV_USER_TYPE       => upper(trim(PV_USER_TYPE)),
                                                 PN_MAS_DEFAULT_LNG => PN_MAS_DEFAULT_LNG,
                                                 PN_SECURITYUNIT    => PN_SECURITYUNIT,
                                                 PV_ERROR           => LV_ERROR,
                                                 PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);

    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    
    IF LC_EXISTE_USER_APP.GROUP_USER IS NOT NULL THEN
      
      IAMK_TRX_USUARIO_BSCS.IAMP_QUITAR_ROL(PV_USUARIO         => upper(trim(PV_USUARIO)),
                                            PV_CADENA_ROL      => upper(trim(LC_EXISTE_USER_APP.GROUP_USER)),
                                            PV_ERROR           => LV_ERROR,
                                            PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);
      IF LV_MENSAJE_TECNICO IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
      
    END IF;

    Else
        -- crea usuario de DB
        IAMK_TRX_USUARIO_BSCS.IAMP_CREA_USUARIO_DB(upper(trim(PV_USUARIO)),
                                                   upper(trim(PV_PASSWORD)),
                                                   upper(trim(PV_TSDEFAULT)),
                                                   upper(trim(PV_TSTEMP)),
                                                   upper(trim(PV_PROFILE)),
                                                   upper(trim(PV_DESCRPTION)),
                                                   upper(trim(PV_BATCH_FLAG)),
                                                   PN_REC_VERSION,
                                                   upper(trim(PV_USER_TYPE)),
                                                   PN_MAS_DEFAULT_LNG,
                                                   PN_SECURITYUNIT,
                                                   LV_ERROR,
                                                   LV_MENSAJE_TECNICO);
              IF LV_MENSAJE_TECNICO IS NOT NULL THEN
                RAISE LE_ERROR;
              END IF;

        -- se crea usuario de aplicacion
        IAMK_TRX_USUARIO_BSCS.IAMP_CREA_USUARIO_APP(upper(trim(PV_USUARIO)),
                                                    upper(trim(PV_DESCRPTION)),
                                                    upper(trim(PV_BATCH_FLAG)),
                                                    PN_REC_VERSION,
                                                    upper(trim(PV_USER_TYPE)),
                                                    PN_MAS_DEFAULT_LNG,
                                                    PN_SECURITYUNIT,
                                                    LV_ERROR,
                                                    LV_MENSAJE_TECNICO);
            IF LV_MENSAJE_TECNICO IS NOT NULL THEN
                RAISE LE_MY_ERROR;  -- Error en creacion de usuario de aplicacion
            END IF;
      End If;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => NVL(PV_USUARIO, NULL),
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Creaci�n del usuario ' ||
                                                                          PV_USUARIO ||' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
    When others then
    null;
    End; -- fin ingreso en bitacora

    COMMIT;
  EXCEPTION
    WHEN LE_MY_ERROR THEN
      ROLLBACK;
      BEGIN
        LV_SENTENCIA := 'drop user ' || PV_USUARIO;
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;

      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,   -- obtiene mensaje de error parametrizado
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear Usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    WHEN LE_ERROR THEN
      ROLLBACK;
        BEGIN
             LV_SENTENCIA := 'drop user ' || PV_USUARIO;
             EXECUTE IMMEDIATE LV_SENTENCIA;
        EXCEPTION
        WHEN OTHERS THEN
             NULL;
        END;

      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear Usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    WHEN LE_ERROR_MSJ THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear Usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    WHEN OTHERS THEN
      ROLLBACK;
      BEGIN
        LV_SENTENCIA := 'drop user ' || PV_USUARIO;
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;

      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear Usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  END IAMP_CREA_USUARIO_BSCS;

  --=================================================================
  --=================================================================

  PROCEDURE IAMP_ACTUALIZA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                        PV_PASSWORD        IN VARCHAR2,
                                        PV_TSDEFAULT       IN VARCHAR2,
                                        PV_TSTEMP          IN VARCHAR2,
                                        PV_PROFILE         IN VARCHAR2,
                                        PV_DESCRPTION      IN VARCHAR2,
                                        PV_BATCH_FLAG      IN VARCHAR2,
                                        PN_REC_VERSION     IN NUMBER,
                                        PV_USER_TYPE       IN VARCHAR2,
                                        PN_MAS_DEFAULT_LNG IN NUMBER,
                                        PN_SECURITYUNIT    IN NUMBER,
                                        PV_MSJ_ERROR       OUT VARCHAR2,
                                        PN_ERROR           OUT NUMBER,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.' ||'IAMP_ACTUALIZA_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
  DBMS_OUTPUT.ENABLE(100000);
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv','ip_address')
    INTO LV_DIR_IP
    FROM DUAL;

    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    -- Llamada a los paquetes que actualizan el usuario
    IAMK_TRX_USUARIO_BSCS.IAMP_ACTUALIZA_USUARIO(PV_USUARIO         => upper(trim(PV_USUARIO)),
                                                 PV_PASSWORD        => upper(trim(PV_PASSWORD)),
                                                 PV_TSDEFAULT       => upper(trim(PV_TSDEFAULT)),
                                                 PV_TSTEMP          => upper(trim(PV_TSTEMP)),
                                                 PV_PROFILE         => upper(trim(PV_PROFILE)),
                                                 PV_DESCRPTION      => upper(trim(PV_DESCRPTION)),
                                                 PV_BATCH_FLAG      => upper(trim(PV_BATCH_FLAG)),
                                                 PN_REC_VERSION     => PN_REC_VERSION,
                                                 PV_USER_TYPE       => upper(trim(PV_USER_TYPE)),
                                                 PN_MAS_DEFAULT_LNG => PN_MAS_DEFAULT_LNG,
                                                 PN_SECURITYUNIT    => PN_SECURITYUNIT,
                                                 PV_ERROR           => LV_ERROR,
                                                 PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);

    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => NVL(PV_USUARIO,
                                                                              NULL),
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Actualizaci�n del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al actualizar usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al actualizar usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_ACTUALIZA_USUARIO_BSCS;


  --================================================
  --================================================
  PROCEDURE IAMP_ELIMINA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PC_LISTADO_ROL     IN VARCHAR2, --CLOB,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||'.IAMP_ELIMINA_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';
        DBMS_OUTPUT.ENABLE(100000);

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_ELIMINA_USUARIO_APP(upper(trim(PV_USUARIO)),
                                                   upper(trim(PC_LISTADO_ROL)),
                                                   LV_ERROR,
                                                   LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      -- error al pretender eliminar usuario
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_ELIMINA_USUARIO_BD(upper(trim(PV_USUARIO)),
                                                  --PC_LISTADO_ROL,
                                                  LV_ERROR,
                                                  LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      -- error al pretender eliminar usuario
      RAISE LE_ERROR;
    END IF;



    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => NVL(PV_USUARIO,
                                                                              NULL),
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Eliminaci�n del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    END; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al eliminar usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      END; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al eliminar el usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_ELIMINA_USUARIO_BSCS;

  PROCEDURE IAMP_BLOQUEA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||
                                       '.IAMP_BLOQUEA_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    LV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv', 'ip_address') INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_BLOQUEA_USUARIO(upper(trim(PV_USUARIO)),
                                               LV_ERROR,
                                               LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);
    IF LV_ERROR != 'ER_0001' THEN
      RAISE LE_ERROR;
    END IF;
    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Bloqueo del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al bloquear usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al bloquear usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;

  END IAMP_BLOQUEA_USUARIO_BSCS;

  PROCEDURE IAMP_ACTIVA_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                     PV_MSJ_ERROR       OUT VARCHAR2,
                                     PN_ERROR           OUT NUMBER,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    -- variables locales
    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.IAMP_ACTIVA_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_DESBLOQUEA_USUARIO(upper(trim(PV_USUARIO)),
                                                  LV_ERROR,
                                                  LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    IF LV_ERROR != 'ER_0001' THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Desbloqueo del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          'se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al desbloquear usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      -- =========DESHACER OPERACION=========

    WHEN OTHERS THEN
      ROLLBACK;
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'ADAM',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al desbloquear usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  END IAMP_ACTIVA_USUARIO_BSCS;

  --===========================================================
  --===========================================================

  PROCEDURE IAMP_CREA_ROL_BSCS(PV_ID_ROL          IN VARCHAR2,
                               PV_DESCRPTION     IN VARCHAR2,
                               PV_MSJ_ERROR       OUT VARCHAR2,
                               PN_ERROR           OUT NUMBER,
                               PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA         VARCHAR2(100) := GV_PAQUETE || '.IAMP_CREA_ROL_BSCS';
    LV_DIR_IP           VARCHAR2(20);
    LV_ERROR            VARCHAR2(2000) := NULL;
    LV_MENSAJE_TECNICO  VARCHAR2(1024);
    LN_ERROR            NUMBER;
    LE_ERROR            EXCEPTION;
    LE_MY_ERROR         EXCEPTION;
    LE_ERROR_PARAMETROS EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    --crea rol en db
    IAMK_TRX_USUARIO_BSCS.IAMP_CREAR_ROL(PV_ID_ROL          => upper(trim(PV_ID_ROL)),
                                         PV_DESCRPTION      => upper(trim(PV_DESCRPTION)),
                                         PV_ERROR           => LV_ERROR,
                                         PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      LV_ERROR := LV_ERROR;
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => '',
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'Creaci�n de rol(es) ' ||
                                                                          PV_ID_ROL ||
                                                                          ' Realizado exitosamente ',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_MY_ERROR THEN
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear rol(es) ' ||
                                                                            PV_ID_ROL,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      END; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear el rol(es) ' ||
                                                                            PV_ID_ROL,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al crear rol(es) ' ||
                                                                            PV_ID_ROL,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_CREA_ROL_BSCS;

  --===========================================================
  --===========================================================
  PROCEDURE IAMP_REVOCA_ROLES_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                           PV_ROL             IN VARCHAR2, --CLOB,
                                           PV_MSJ_ERROR       OUT VARCHAR2,
                                           PN_ERROR           OUT NUMBER,
                                           PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_REVOCA_ROLES_USUARIO_BSCS';
    LV_DIR_IP          VARCHAR2(20);
    LV_ERROR           VARCHAR2(600);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;
    --PRUEBA_ROL CLOB;
  BEGIN
  DBMS_OUTPUT.ENABLE(100000);
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    ---------------QUITA ROL
    --PRUEBA_ROL:=  'SCO_CAJERO;BSCS_PRUEBA2';
    IAMK_TRX_USUARIO_BSCS.IAMP_QUITAR_ROL(PV_USUARIO         => upper(trim(PV_USUARIO)),
                                          PV_CADENA_ROL      => upper(trim(PV_ROL)),
                                          PV_ERROR           => LV_ERROR,
                                          PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora YATA100
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'El proceso de quitar/revocar rol(es): '|| PV_ROL ||' al usuario: ' ||PV_USUARIO ||' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al quitar/revocar rol(es): ' ||
                                                                            SUBSTR(PV_ROL,
                                                                                   4000,
                                                                                   1) ||
                                                                            ' del usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN

        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'El proceso de quitar/revocar rol(es): ' ||
                                                                            SUBSTR(PV_ROL,
                                                                                   4000,
                                                                                   1) ||
                                                                            ' al usuario: ' ||
                                                                            PV_USUARIO ||
                                                                            ' se realiz� exitosamente',
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_REVOCA_ROLES_USUARIO_BSCS;

  PROCEDURE IAMP_RESETEO_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                      PV_PASSWORD        IN VARCHAR2,
                                      PV_MSJ_ERROR       OUT VARCHAR2,
                                      PN_ERROR           OUT NUMBER,
                                      PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE || '.' ||
                                       'IAMP_RESETEO_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    PV_DIR_IP          VARCHAR2(20);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    -- Llamada al paquete TRX que resetea el usuario
    IAMK_TRX_USUARIO_BSCS.IAMP_RESETEO_USUARIO(PV_USUARIO         => upper(trim(PV_USUARIO)),
                                               PV_PASSWORD        => upper(trim(PV_PASSWORD)),
                                               PV_ERROR           => LV_ERROR,
                                               PV_MENSAJE_TECNICO => LV_MENSAJE_TECNICO);

    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    
    IAMK_TRX_USUARIO_BSCS.IAMP_DESBLOQUEA_USUARIO(upper(trim(PV_USUARIO)),
                                                  LV_ERROR,
                                                  LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    IF LV_ERROR != 'ER_0001' THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'El reseteo de la contrase�a al usuario: ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      rollback;
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al resetear contrase�a al usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al resetear contrase�a al usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_RESETEO_USUARIO_BSCS;

  --=============================================
  --=============================================
  PROCEDURE IAMP_ASIGNA_ROLES_USUARIO_BSCS(PV_ROL             IN VARCHAR2,
                                           PV_USUARIO         IN VARCHAR2,
                                           PV_MSJ_ERROR       OUT VARCHAR2,
                                           PN_ERROR           OUT NUMBER,
                                           PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||
                                       '.IAMP_ASIGNA_ROLES_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(1024);
    LV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;
    --PRUEBA_ROL CLOB;
  BEGIN
    DBMS_OUTPUT.ENABLE(100000);
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO LV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(LV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_ASIGNA_ROL_USUARIO(upper(trim(PV_ROL)),
                                                  upper(trim(PV_USUARIO)),
                                                  LV_ERROR,
                                                  LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => LV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La asignaci�n del rol(es): ' ||
                                                                          SUBSTR(PV_ROL,
                                                                                 4000,
                                                                                 1) ||
                                                                          ' al usuario: ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al asignar rol(es): ' ||
                                                                            SUBSTR(PV_ROL,
                                                                                   4000,
                                                                                   1) ||
                                                                            ' al usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora YA TA
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => LV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al asignar rol(es): ' ||
                                                                            SUBSTR(PV_ROL,
                                                                                   4000,
                                                                                   1) ||
                                                                            ' al usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_ASIGNA_ROLES_USUARIO_BSCS;

  PROCEDURE IAMP_DESHACER_CREA_USUARIO_BS(PV_USUARIO         IN VARCHAR2,
                                          PV_MSJ_ERROR       OUT VARCHAR2,
                                          PN_ERROR           OUT NUMBER,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(61) := GV_PAQUETE ||
                                       '.IAMP_DESHACER_CREA_USUARIO_BS';
    LV_ERROR           VARCHAR2(1024);
    LV_MENSAJE_TECNICO VARCHAR2(500);
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN

    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_DESHACER_CREA_USUARIO(upper(trim(PV_USUARIO)),
                                                     LV_ERROR,
                                                     LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => NVL(PV_USUARIO, NULL),
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'El reverso de la creaci�n del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al reversar creaci�n del usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al reversar creaci�n del usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;

  END IAMP_DESHACER_CREA_USUARIO_BS;

  PROCEDURE IAMP_DATOS_USUARIO_BSCS(PV_USUARIO         IN VARCHAR2,
                                    PV_DESCRIPCION     OUT VARCHAR2,
                                    PV_GROUP_USER      OUT VARCHAR2,
                                    PV_USER_TYPE       OUT VARCHAR2,
                                    PV_BATCH_FLAG      OUT VARCHAR2,
                                    PV_REC_VERSION     OUT NUMBER,
                                    PV_MAS_DEFAULT_LNG OUT NUMBER,
                                    PV_SECURITYUNIT    OUT NUMBER,
                                    PV_TS_DEFAULT      OUT VARCHAR2,
                                    PV_TS_TEMPORAL     OUT VARCHAR2,
                                    PV_PROFILE         OUT VARCHAR2,
                                    PV_STATUS          OUT VARCHAR2,
                                    PV_MSJ_ERROR       OUT VARCHAR2,
                                    PN_ERROR           OUT NUMBER,
                                    PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_DATOS_USUARIO_BSCS';
    LV_ERROR           VARCHAR2(2000) := NULL;
    LV_MENSAJE_TECNICO VARCHAR2(2000) := NULL;
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_DATOS_USUARIO(upper(trim(PV_USUARIO)),
                                             PV_DESCRIPCION,
                                             PV_GROUP_USER,
                                             PV_USER_TYPE,
                                             PV_BATCH_FLAG,
                                             PV_REC_VERSION,
                                             PV_MAS_DEFAULT_LNG,
                                             PV_SECURITYUNIT,
                                             PV_TS_DEFAULT,
                                             PV_TS_TEMPORAL,
                                             PV_PROFILE,
                                             PV_STATUS,
                                             LV_ERROR,
                                             LV_MENSAJE_TECNICO );

    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora

      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => NVL(PV_USUARIO,
                                                                              NULL),
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La consulta de datos del usuario ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    ROLLBACK;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar datos del usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);

      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora

        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => NVL(PV_USUARIO,
                                                                                NULL),
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar datos del usuario ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;

  END IAMP_DATOS_USUARIO_BSCS;

PROCEDURE IAMP_CONSULTA_USR_ROLES_BSCS(PV_USUARIO         IN VARCHAR2,
                                       PV_LISTA_ROL       OUT CLOB,
                                       PV_MSJ_ERROR       OUT VARCHAR2,
                                       PN_ERROR           OUT NUMBER,
                                       PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_CONSULTA_USR_ROLES_BSCS';
    LV_ERROR           VARCHAR2(1024) := NULL;
    LV_MENSAJE_TECNICO VARCHAR2(1024) := NULL;
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_CONSULTA_USUARIO_ROLES(upper(trim(PV_USUARIO)),
                                                      PV_LISTA_ROL,
                                                      LV_ERROR,
                                                      LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);
    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => '',
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La consulta de el (los) rol (es) del usuario: ' ||
                                                                          PV_USUARIO ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar el (los) rol(es) del usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => PV_USUARIO,
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar el (los) rol(es) del usuario: ' ||
                                                                            PV_USUARIO,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
  END IAMP_CONSULTA_USR_ROLES_BSCS;

  --=========================================
  --=========================================
  PROCEDURE IAMP_CONSULTA_ROL_USRS_BSCS(PV_ROL             IN VARCHAR2,
                                        PV_LISTA_USER      OUT CLOB,
                                        PV_MSJ_ERROR       OUT VARCHAR2,
                                        PN_ERROR           OUT NUMBER,
                                        PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_CONSULTA_ROL_USRS_BSCS';
    LV_ERROR           VARCHAR2(2000) := NULL;
    LV_MENSAJE_TECNICO VARCHAR2(1024) := NULL;
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_CONSULTA_ROL_USUARIOS(upper(trim(PV_ROL)),
                                                     PV_LISTA_USER,
                                                     LV_ERROR,
                                                     LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);
    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => '',
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La consulta de el(los) usuario(s) asignado(s) al rol: ' ||
                                                                          PV_ROL ||
                                                                          ' se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
    COMMIT;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;

      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR, -- mensaje de error parametrizado
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar el(los) usuario(s) asignado al rol: ' ||
                                                                            PV_ROL,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;

      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR, -- mensaje de error parametrizado
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);
      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar el(los) usuario(s) asignado al rol: ' ||
                                                                            PV_ROL,
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora
      ROLLBACK;
  END IAMP_CONSULTA_ROL_USRS_BSCS;

  --===============================================
  --===============================================
  PROCEDURE IAMP_CONSULTA_USRS_ROLES_BSCS(PV_RESUL_CONSULTA  OUT CLOB,
                                          PV_MSJ_ERROR       OUT VARCHAR2,
                                          PN_ERROR           OUT NUMBER,
                                          PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_CONSULTA_USRS_ROLES_BSCS';
    LV_ERROR           VARCHAR2(2000) := NULL;
    LV_MENSAJE_TECNICO VARCHAR2(2000) := NULL;
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_CONSULTA_USUARIO_Y_ROLES(PV_RESUL_CONSULTA,
                                                        LV_ERROR,
                                                        LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => '',
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La consulta de todos los usuario y sus roles s realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar todos los usuario y sus roles',
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar todos los usuario y sus roles',
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
     Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  END IAMP_CONSULTA_USRS_ROLES_BSCS;

  --========================================================
  --========================================================

  PROCEDURE IAMP_CONSULTA_ROLES_BSCS(PV_RESUL_CONSULTA  OUT CLOB,
                                     PV_MSJ_ERROR       OUT VARCHAR2,
                                     PN_ERROR           OUT NUMBER,
                                     PV_MENSAJE_TECNICO OUT VARCHAR2) IS

    LV_PROGRAMA        VARCHAR2(100) := GV_PAQUETE ||
                                        '.IAMP_CONSULTA_ROLES_BSCS';
    LV_ERROR           VARCHAR2(2000) := NULL;
    LV_MENSAJE_TECNICO VARCHAR2(2000) := NULL;
    PV_DIR_IP          VARCHAR2(20);
    LN_ERROR           NUMBER;
    LE_ERROR           EXCEPTION;

  BEGIN
    LV_ERROR           := 'ER_0001'; -- operacion exitosa
    PV_MENSAJE_TECNICO := '';

    -- obtiene la direccion ip de la maquina que ejecuta la operacion
    SELECT SYS_CONTEXT('userenv',
                       'ip_address')
      INTO PV_DIR_IP
      FROM DUAL;
    IF NOT IAMK_TRX_USUARIO_BSCS.IAMP_VALIDA_IP(PV_DIR_IP,
                                                GV_PAQUETE) THEN
      LV_ERROR := 'ER_0017'; -- Usuario no tiene permiso para realizar la operacion
      RAISE LE_ERROR;
    END IF;

    IAMK_TRX_USUARIO_BSCS.IAMP_CONSULTA_LISTA_ROLES(PV_RESUL_CONSULTA,
                                                    LV_ERROR,
                                                    LV_MENSAJE_TECNICO);
    IF LV_MENSAJE_TECNICO IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    -- obtiene mensaje de error parametrizado
    IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                 PV_MSJ_ERROR,
                                                 PN_ERROR);

    --=================B  I  T  A  C  O  R  A==============
    BEGIN
      -- ingreso en bitacora
      IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                   PV_APLICACION       => 'BSCS',
                                                   PV_USUARIO          => '',
                                                   PV_USUARIO_OPERADOR => USER,
                                                   PD_FECHA            => SYSDATE,
                                                   LV_DIR_IP           => PV_DIR_IP,
                                                   PV_DESCRIPCION      => 'La consulta de Roles se realiz� exitosamente',
                                                   LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                   LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                   PV_ERROR            => LV_ERROR,
                                                   PN_ERROR            => LN_ERROR,
                                                   PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
    Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MENSAJE_TECNICO := LV_MENSAJE_TECNICO;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar los Roles',
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

    WHEN OTHERS THEN
      LV_ERROR           := 'ER_0003';
      PV_MENSAJE_TECNICO := 'Error: ' || SUBSTR(SQLERRM,
                                                1,
                                                512) || ' en ' ||
                            LV_PROGRAMA;
      -- obtiene mensaje de error parametrizado
      IAMK_TRX_USUARIO_BSCS.IAMP_RETORNA_MSJ_ERROR(LV_ERROR,
                                                   PV_MSJ_ERROR,
                                                   PN_ERROR);

      --=================B  I  T  A  C  O  R  A==============
      BEGIN
        -- ingreso en bitacora
        IAMK_TRX_USUARIO_BSCS.IAMP_INGRESAR_BITACORA(PV_METODO           => LV_PROGRAMA,
                                                     PV_APLICACION       => 'BSCS',
                                                     PV_USUARIO          => '',
                                                     PV_USUARIO_OPERADOR => USER,
                                                     PD_FECHA            => SYSDATE,
                                                     LV_DIR_IP           => PV_DIR_IP,
                                                     PV_DESCRIPCION      => 'Error al consultar los Roles',
                                                     LV_MSJ_SISTEMA      => PV_MSJ_ERROR,
                                                     LV_MSJ_TECNICO      => LV_MENSAJE_TECNICO,
                                                     PV_ERROR            => LV_ERROR,
                                                     PN_ERROR            => LN_ERROR,
                                                     PV_MENSAJE_TECNICO  => LV_MENSAJE_TECNICO);
      Exception
      When others then
      null;
      End; -- fin ingreso en bitacora

  END IAMP_CONSULTA_ROLES_BSCS;

END IAMK_API_USUARIO_BSCS;
/
