CREATE OR REPLACE TRIGGER SERVICIOS_RUBROS_COB_SERVICIOS
AFTER INSERT OR UPDATE ON COB_SERVICIOS
FOR EACH ROW

DECLARE
  
  CURSOR C_DetallesCuentas(Cv_CtaPS VARCHAR2) IS
    SELECT COUNT(CUENTA)
      FROM wf_detalles_cuentas@axis
     WHERE CUENTA = Cv_CtaPS;
     
  CTAPS_NEW     read.cob_servicios.CTAPSOFT%TYPE;
  CTADEVOL_NEW  read.cob_servicios.CTA_DEVOL%TYPE;
  SERVICIO_NEW  read.cob_servicios.SERVICIO%TYPE;
  CTACTBLE_NEW  read.cob_servicios.CTACTBLE%TYPE;
  
  CTAPS_OLD     read.cob_servicios.CTAPSOFT%TYPE;
  SERVICIO_OLD  read.cob_servicios.SERVICIO%TYPE;
  CTACTBLE_OLD  read.cob_servicios.CTACTBLE%TYPE;
  Ln_DetCtas    NUMBER := 0;

BEGIN

  CTAPS_NEW    := :NEW.CTAPSOFT;
  CTADEVOL_NEW := :NEW.CTA_DEVOL;
  SERVICIO_NEW := :NEW.SERVICIO;
  CTACTBLE_NEW := :NEW.CTACTBLE;
  
  CTAPS_OLD    := :OLD.CTAPSOFT;
  SERVICIO_OLD := :OLD.SERVICIO;
  CTACTBLE_OLD := :OLD.CTACTBLE;
  
  OPEN C_DetallesCuentas(CTAPS_NEW);
  FETCH C_DetallesCuentas
    INTO Ln_DetCtas;
  CLOSE C_DetallesCuentas;

  IF INSERTING THEN
 
    INSERT INTO WF_CONFIGURACION_CUENTAS@AXIS(CUENTA_FACTURA,
                                              CUENTA_DEVOLUCION,
                                              SERVICIO,
                                              CTACTBLE,
                                              TABLA)
                                      VALUES (CTAPS_NEW,
                                              CTADEVOL_NEW,
                                              SERVICIO_NEW,
                                              CTACTBLE_NEW,
                                              'C');                                             
    IF Ln_DetCtas = 0 THEN
      INSERT INTO WF_DETALLES_CUENTAS@AXIS(CUENTA,
                                           TIPO)
                                   VALUES (CTAPS_NEW,
                                           'D');
    END IF;
  
  ELSIF UPDATING THEN
  
    UPDATE WF_CONFIGURACION_CUENTAS@AXIS
       SET CUENTA_FACTURA    = CTAPS_NEW,
           CUENTA_DEVOLUCION = CTADEVOL_NEW,
           SERVICIO          = SERVICIO_NEW,
           CTACTBLE          = CTACTBLE_NEW
     WHERE SERVICIO = SERVICIO_OLD
       AND CTACTBLE = CTACTBLE_OLD;

    IF Ln_DetCtas = 0 THEN
      INSERT INTO WF_DETALLES_CUENTAS@AXIS(CUENTA,
                                           TIPO)
                                   VALUES (CTAPS_NEW,
                                           'D');
    END IF;
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(SQLCODE,
                            SQLERRM);
  
END SERVICIOS_RUBROS_COB_SERVICIOS;
/
