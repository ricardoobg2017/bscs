CREATE OR REPLACE function GSIF_BUSCA_NUM(numero varchar2) return varchar2 is
  cursor busca_num is
    select substr(a.dn_num,-8), b.co_id, e.ch_status, c.product_history_date
    from directory_number a, contr_services_cap b, contract_all c, contract_history e
    where a.dn_id=b.dn_id and b.co_id=c.co_id
    and c.co_id=e.co_id
    and a.dn_status = 'a' --status del contrato
    and b.cs_deactiv_date is null --para ver el contrato que est� activo
    AND  b.main_dirnum = 'X'
    and dn_num=decode(length(numero),8,'593','5939')||numero --n�mero de tel�fono
    and e.ch_status in ('a','s')
    AND e.ch_seqno = (SELECT MAX(chl.ch_seqno) FROM contract_history chl WHERE chl.co_id = e.co_id);
      
  retorna varchar2(10):='NADA';
  reg_num busca_num%rowtype;
begin
  reg_num:=null;
  open busca_num;
  fetch busca_num into reg_num;
  if (busca_num%found) then
     retorna:=upper(reg_num.ch_status);
  end if;
  close busca_num;
  
  return retorna;
end GSIF_BUSCA_NUM;

/
