CREATE OR REPLACE VIEW CONTRATOSSERVPOOL AS
select
    d.custcode,
    d.customer_id,
    e.co_id,
    m.ch_status,
    m.entdate,
    m.ch_validfrom
from
    customer_all d,
    contract_all e,
    curr_co_status m,
    profile_service a,
    pr_serv_status_hist k
where
    d.customer_id = e.customer_id
    and m.co_id = e.co_id
    AND a.co_id = e.co_id
    and m.ch_status = 'a'
    and a.status_histno = k.histno
    and a.sncode = 49;