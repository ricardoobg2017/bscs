CREATE OR REPLACE VIEW PARAMETER_VALUE_ACT AS 
select
    prm_value_id,
    prm_seqno,
    a.prm_value_number
from
    parameter_value a
where
    prm_seqno = (
        select
            max(prm_seqno)
        from
            parameter_value b
        where
            (a.prm_value_id = b.prm_value_id)
    );