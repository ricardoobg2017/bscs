CREATE OR REPLACE VIEW VF_FECHA_ULTIMA_CTA_PREVIA AS 
select
    t.co_rep,
    max(c_all.csactivated) fec_Max_Cta_Previa
from
    sysadm.contract_all t,
    sysadm.customer_all c_all
where
    length(trim(t.co_rep)) > 0
    and t.customer_id = c_all.customer_id
    and t.co_rep in (
        select
            distinct linea
        from
            VF_CambiosPlanes_de_julio t
    )
group by
    t.co_rep;