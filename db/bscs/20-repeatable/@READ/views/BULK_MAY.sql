CREATE OR REPLACE VIEW BULK_MAY AS
select /*+ rule */ 
  d.customer_id,k.co_id,d.custcode,c.ohrefnum,f.cccity,
  f.ccstate,g.bank_id,I.PRODUCTO,
  j.cost_desc,a.otglsale as Cble,
  sum(NVL(otamt_revenue_gl,0)) as Valor,sum(NVL(A.OTAMT_DISC_DOC,0)) AS Descuento,
  h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft CTACLBLEP
  from 
  ordertrailer A, --items de la factura
  mpusntab b,  --maestro de servicios
  orderhdr_all c, --cabecera de facturas
  customer_all d, -- maestro de cliente
  ccontact_all f, -- información demografica de la cuente
  payment_all g,  --Forna de pago
  COB_SERVICIOS h, --formato de reporte
  read.cob_grupos_bulk I, --NOMBRE GRUPO
  COSTCENTER j,
  contract_all k
  where
  a.otxact=c.ohxact and 
  d.customer_id=k.customer_id and
  c.ohentdate=to_date('24/01/2006','DD/MM/YYYY') and
  C.OHSTATUS IN ('IN','CM') AND
  C.OHUSERID IS NULL AND
  c.customer_id=d.customer_id and 
  substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
  instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
  instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
  d.customer_id= f.customer_id and
  f.ccbill='X' and
  g.customer_id=d.customer_id and
  act_used ='X' and
  h.servicio=b.sncode and
  h.ctactble=a.otglsale AND
  D.PRGCODE=I.PRGCODE and
  j.cost_id=d.costcenter_id
    group by d.customer_id,k.co_id,d.custcode,c.ohrefnum,
    f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,
    j.cost_desc, a.otglsale,
    a.otxact, h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft    
 UNION
 SELECT /*+ rule */ 
 d.customer_id,k.co_id,d.custcode,c.ohrefnum,f.cccity,
 f.ccstate,g.bank_id,I.PRODUCTO,
 j.cost_desc,A.glacode as Cble,
 sum(TAXAMT_TAX_CURR) AS VALOR, 0 as Descuento,
 h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft CTACLBLEP
 FROM 
 ORDERTAX_ITEMS A, --items de impuestos relacionados a la factura
 TAX_CATEGORY B ,--maestro de tipos de impuestos
 orderhdr_all C, --cabecera de facturas
 customer_all d,
 ccontact_all f, --datos demograficos
 payment_all g, -- forma de pago
 COB_SERVICIOS h,
 read.cob_grupos_bulk I,
 COSTCENTER J,
 contract_all k
   where 
   c.ohxact=a.otxact and
   d.customer_id=k.customer_id and
   c.ohentdate=to_date('24/01/2006','DD/MM/YYYY') and
   C.OHSTATUS IN ('IN','CM') AND
  C.OHUSERID IS NULL AND
   c.customer_id=d.customer_id and 
   taxamt_tax_curr>0 and
   A.TAXCAT_ID=B.TAXCAT_ID and
   d.customer_id=f.customer_id and
   f.ccbill='X' and 
   g.customer_id=d.customer_id and
   act_used ='X' and
   h.servicio=A.TAXCAT_ID and
  h.ctactble=A.glacode AND
  D.PRGCODE=I.PRGCODE and
  j.cost_id=d.costcenter_id
 group by d.customer_id,k.co_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, A.glacode,
 h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft;

