CREATE OR REPLACE VIEW DATOSCLIENTES  AS
select /*+ rule */
  d.customer_id,d.custcode,f.ccfname,f.cclname,f.cssocialsecno,f.cccity,f.ccstate,
  f.ccname,f.cctn,f.cctn2, f.ccline3||f.ccline4 dir2, h.tradename,
  j.cost_desc, d.CSCURBALANCE,d.PREV_BALANCE
  from 
  customer_all d, -- maestro de cliente
  ccontact_all f, -- información demografica de la cuente
  payment_all g,  --Forna de pago
  COSTCENTER j,
  trade_all h
  where
  d.customer_id  = f.customer_id and
  f.ccbill       = 'X' and
  d.customer_id  = g.customer_id and
  d.costcenter_id= j.cost_id and
  d.cstradecode  = h.tradecode(+);

