CREATE OR REPLACE VIEW VF_DATOS_CTA_PREVIA AS 
select 
distinct 
t.co_rep,           -- Linea
c_all.custcode,      -- cuenta
c_all.csactivated  -- fecha activacion cuenta 
from 
sysadm.contract_all t,
sysadm.customer_all c_all,
vf_fechaultima_cta_previa vs
where 
t.co_rep = vs.co_rep and  -- Enlace a numeros de interes
t.customer_id = c_all.customer_id and  --Enlace a cuenta
c_all.csactivated = vs.fec_max_cta_previa;

