CREATE OR REPLACE VIEW VF_ULTIMOS_PLANES AS 
select
    distinct c_all.csactivated fe_activacion_Cta,
    t.co_rep linea,
    rp_h.tmcode id_plan_linea,
    rp.des nombre_plan_linea,
    max(t.co_activated) fecha_activado_fono,
    max(t.tmcode_date) Fec_Ultimo_Plan
from
    sysadm.contract_all t,
    sysadm.customer_all c_all,
    sysadm.rateplan_hist rp_h,
    sysadm.rateplan rp
where
    length(trim(t.co_rep)) > 0
    and t.customer_id = c_all.customer_id
    and t.co_id = rp_h.co_id
    and rp_h.tmcode = rp.tmcode
    and (t.co_rep, c_all.custcode, c_all.csactivated) in (
        select
            p.co_rep,
            p.custcode,
            p.csactivated
        from
            vf_datos_ultima_cta p
    )
group by
    c_all.custcode,
    c_all.cstype,
    c_all.csactivated,
    t.co_rep,
    rp_h.tmcode,
    rp.des