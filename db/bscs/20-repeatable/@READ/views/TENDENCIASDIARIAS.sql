CREATE OR REPLACE VIEW TENDENCIASDIARIAS AS 
select
    TRUNC (
        new_time(initial_start_time_timestamp, 'GMT', 'EST')
    ) as Fecha,
    to_char (
        new_time(initial_start_time_timestamp, 'GMT', 'EST'),
        'DAY'
    ) as Dia,
    to_char (
        new_time(initial_start_time_timestamp, 'GMT', 'EST'),
        'HH24'
    ) as Hora,
    b.costcenter_id,
    s_p_location_numbering_plan Tecnologia,
    count(*) Llamadas
from
    sysadm.UDR_LT_20050405 @bscs_to_rtx_link a,
    sysadm.tarifarios @bscs_to_rtx_link b
where
    uds_charge_part_id = 1
    and a.cust_info_customer_id = b.customer_id
group by
    TRUNC (
        new_time(initial_start_time_timestamp, 'GMT', 'EST')
    ),
    to_char (
        new_time(initial_start_time_timestamp, 'GMT', 'EST'),
        'DAY'
    ),
    to_char (
        new_time(initial_start_time_timestamp, 'GMT', 'EST'),
        'HH24'
    ),
    b.costcenter_id,
    s_p_location_numbering_plan