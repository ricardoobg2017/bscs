CREATE OR REPLACE  VIEW VF_FONOS_MAX_FECHA4  AS 
select
    fo.co_rep,
    fo.tmcode id_plan_BSCS,
    rp.des desc_Plan,
    fo.tmcode_date fecha_ini_plan,
    tt.ch_validfrom fecha_ini_st_fono,
    tt.ch_status,
    decode(
        tt.ch_status,
        Null,
        Null,
        'o',
        'PreActivado',
        'a',
        'Activo',
        's',
        'Suspendido',
        'd',
        'Inactivo',
        'Otro'
    ) Desc_Status_Linea,
    c_all.custcode cuenta,
    c_all.cstype st_cuenta,
    decode(
        c_all.cstype,
        null,
        'Ninguno',
        'i',
        'Cuenta Cli Interesado',
        'a',
        decode(
            c_all.reactivated,
            null,
            'Cuenta Activa',
            'Cuenta Rectiva'
        ),
        'd',
        'Cuenta Inactiva',
        'i',
        'Cuenta Cli Interesado',
        's',
        'Cuenta Suspendida',
        'g',
        'Cuenta Gestprrt',
        'b',
        'Cuenta Beantragt',
        'Otro'
    ) Desc_Estado_Cuenta,
    decode(
        c_all.cstype_date,
        'i',
        c_all.csentdate,
        'a',
        decode(
            c_all.reactivated,
            null,
            c_all.csactivated,
            c_all.reactivated
        ),
        's',
        c_all.suspended,
        'd',
        c_all.csdeactivated
    ) fec_status_cta,
    c_all.csentdate fec_cta_inter,
    c_all.reactivated fec_cta_reactiva,
    c_all.csactivated fec_cta_activa,
    c_all.suspended fec_cta_suspendida,
    c_all.csdeactivated fec_cta_nactiva,
    c_all.cstype_date fec_cambio_st_cta
from
    sysadm.contract_history tt,
    sysadm.contract_all fo,
    sysadm.rateplan rp,
    sysadm.customer_all c_all
where
    tt.co_id = fo.co_id
    and fo.co_rep is not null
    and fo.tmcode = rp.tmcode
    and fo.customer_id = c_all.customer_id (+);