CREATE OR REPLACE VIEW QC_RUBROS AS 
select
    /*+ rule */
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    I.PRODUCTO,
    j.cost_desc,
    a.otglsale as Cble,
    servcat_code,
    sum(NVL(otamt_revenue_gl, 0)) as Valor,
    sum(NVL(A.OTAMT_DISC_DOC, 0)) AS Descuento,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE
from
    ordertrailer A,    -- items de la factura
     mpusntab b,    -- maestro de servicios 
     orderhdr_all c,    -- cabecera de facturas 
     customer_all d,    -- maestro de cliente 
     COB_SERVICIOS h,    -- formato de reporte 
     COB_GRUPOS I,    -- NOMBRE GRUPO 
     COSTCENTER j
where
    a.otxact = c.ohxact
    and c.ohentdate = to_date('24/02/2005', 'DD/MM/YYYY')
    and C.OHSTATUS IN ('IN', 'CM')
    AND C.OHUSERID IS NULL
    AND c.customer_id = d.customer_id
    and substr(
        otname,
        instr(
            otname,
            '.',
            instr(otname, '.', instr(otname, '.', 1) + 1) + 1
        ) + 1,
        instr(
            otname,
            '.',
            instr(
                otname,
                '.',
                instr(otname, '.', instr(otname, '.', 1) + 1) + 1
            ) + 1
        ) - instr(
            otname,
            '.',
            instr(otname, '.', instr(otname, '.', 1) + 1) + 1
        ) -1
    ) = b.sncode
    and h.servicio = b.sncode
    and h.ctactble = a.otglsale
    AND D.PRGCODE = I.PRGCODE
    and j.cost_id = d.costcenter_id
group by
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    I.PRODUCTO,
    j.cost_desc,
    a.otglsale,
    servcat_code,
    a.otxact,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE;