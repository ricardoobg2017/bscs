/* Formatted on 16/12/2013 18:28:50 (QP5 v5.256.13226.35510) */
CREATE OR REPLACE FORCE VIEW ORDERHDR_CONSULTA_FACT
(
   OHXACT,
   OHSTATUS,
   OHENTDATE,
   OHREFNUM,
   CUSTOMER_ID,
   OHREFDATE,
   OHINVTYPE,
   GL_CURRENCY,
   OHINVAMT_GL,
   OHOPNAMT_GL,
   COMPLAINT
)
AS
   SELECT /*+ index (b ORDERCUSTOMER) */
         b.OHXACT,
          b.OHSTATUS,
          b.OHENTDATE,
          b.OHREFNUM,
          b.CUSTOMER_ID,
          b.OHREFDATE,
          b.OHINVTYPE,
          b.GL_CURRENCY,
          b.OHINVAMT_GL,
          b.ohopnamt_gl,
          b.complaint
     FROM sysadm.orderhdr_all b
    WHERE customer_id > 0
   UNION ALL
   SELECT /*+ index (a IDX_PYMES_AFE) */
         a.OHXACT,
          a.OHSTATUS,
          a.OHENTDATE,
          a.OHREFNUM,
          a.CUSTOMER_ID,
          a.OHREFDATE,
          a.OHINVTYPE,
          a.GL_CURRENCY,
          a.OHINVAMT_GL,
          a.ohopnamt_gl,
          a.complaint
     FROM sysadm.orderhdr_pymesafect_24082013 a
    WHERE customer_id > 0;
