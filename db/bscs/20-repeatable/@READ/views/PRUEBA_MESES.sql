create
or replace view prueba_meses as
select
    a.cuenta,
    a.num_fact,
    a.cedula,
    a.texto1 nomres,
    ltrim(rtrim(a.texto2)) || ' ' || ltrim(rtrim(a.texto3)) direccion,
    ltrim(rtrim(texto4)) ciudad,
    a.totalpagar valorfactura,
    a.iva iva,
    to_number(nvl(a.agua, '0')) + to_number(nvl(a.deporte, '0')) otros_impuesto,
    to_number(nvl(a.iva, '0')) + to_number(nvl(a.agua, '0')) + to_number(nvl(a.deporte, '0')) total_impuesto,
    a.descterminopago formapago,
    a.fechapago fecha_fact,
    'CARGA.CARGA1999' tabla,
    to_number(nvl(a.tarbassus, 0)) tbasica,
    a.cargopaquete,
    'num_fact' fact_field
from
    carga1999 a
union all
select
    cuenta,
    num_fact,
    cedula,
    nombres,
    'S/D' direccion,
    'S/C' ciudad,
    valorfactura,
    iva,
    to_number(otros_impuesto),
    to_number(total_impuesto),
    formapago,
    fecha_fact,
    'CARGA.CARGA2000TARGYE',
    to_number(tbasica),
    to_number(cargopaquete),
    'num_fact' fact_field
from
    carga.carga2000TARGYE
union all
select
    cuenta,
    num_fact,
    cedula,
    nombres,
    'S/D' direccion,
    'S/C' ciudad,
    valorfactura,
    iva,
    to_number(otros_impuesto),
    to_number(total_impuesto),
    formapago,
    fecha_fact,
    'CARGA.CARGA2000AUTGYE',
    to_number(tbasica),
    to_number(cargopaquete),
    'num_fact' fact_field
from
    carga.carga2000AUTGYE
union all
select
    cuenta,
    numfactura num_fact,
    ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva12 iva,
    to_number(nvl(impaguapotable10, '0')) + to_number(nvl(impdeporte5, '0')) otros_impuesto,
    to_number(nvl(iva12, '0')) + to_number(nvl(impaguapotable10, '0')) + to_number(nvl(impdeporte5, '0')) total_impuesto,
    descpago formapago,
    '24/09/2000' fecha_fact,
    'CARGA.CARGA200009AUTG' tabla,
    to_number(nvl(tarifabasicamensual, 0)) + to_number(nvl(tarifabasicamensual2, 0)) + to_number(nvl(tarifabasicasuspendido, 0)) tbasica,
    to_number(nvl(cp129cargoapaquete, 0)) + to_number(nvl(cp139cargoapaquete, 0)) + to_number(nvl(cp1400cargoapaquete, 0)) + to_number(nvl(cp149cargoapaquete, 0)) + to_number(nvl(cp199cargoapaquete, 0)) + to_number(nvl(cp2100cargoapaquete, 0)) + to_number(nvl(cp2400cargoapaquete, 0)) + to_number(nvl(cp2700cargoapaquete, 0)) + to_number(nvl(cp375cargoapaquete, 0)) + to_number(nvl(cp3990cargoapaquete, 0)) + to_number(nvl(cp45cargoapaquete, 0)) + to_number(nvl(cp47cargoapaquete, 0)) + to_number(nvl(cp49cargoapaquete, 0)) + to_number(nvl(cp50cargoapaquete, 0)) + to_number(nvl(cp55cargoapaquete, 0)) + to_number(nvl(cp59cargoapaquete, 0)) + to_number(nvl(cp69cargoapaquete, 0)) + to_number(nvl(cp729cargoapaquete, 0)) + to_number(nvl(cp89cargoapaquete, 0)) + to_number(nvl(cp95cargoapaquete, 0)) + to_number(nvl(cp999cargoapaquete, 0)) + to_number(nvl(cp99cargoapaquete, 0)) + to_number(nvl(cpeq48cargoapaquete, 0)) cargopaquete,
    'numfactura' fact_field
from
    carga.carga200009AUTG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/10/2000' fecha_fact,
    'CARGA.CARGA200010AUTG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200010AUTG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/11/2000' fecha_fact,
    'CARGA.CARGA200011AUTG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200011AUTG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/12/2000' fecha_fact,
    'CARGA.CARGA200012AUTG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200012AUTG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    tot_fact valorfactura,
    iva_10 iva,
    to_number(nvl(imp_agua, '0')) + to_number(nvl(imp_dep, '0')) otros_impuesto,
    to_number(nvl(iva_10, '0')) + to_number(nvl(imp_agua, '0')) + to_number(nvl(imp_dep, '0')) total_impuesto,
    desc_pago formapago,
    '24/02/2000' fecha_fact,
    'CARGA.CARGA200002AUTQ' tabla,
    to_number(nvl(tf_bas, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200002AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    total_fact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/03/2000' fecha_fact,
    'CARGA.CARGA200003AUTQ' tabla,
    to_number(nvl(tf_bas, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200003AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(impaguapotable_1, '0')) + to_number(nvl(impdeporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(impaguapotable_1, '0')) + to_number(nvl(impdeporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/04/2000' fecha_fact,
    'CARGA.CARGA200004AUTQ' tabla,
    to_number(nvl(tarifa_f, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200004AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(agu_10, '0')) + to_number(nvl(dep_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(agu_10, '0')) + to_number(nvl(dep_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/05/2000' fecha_fact,
    'CARGA.CARGA200005AUTQ' tabla,
    to_number(nvl(taf_bas, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200005AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/06/2000' fecha_fact,
    'CARGA.CARGA200006AUTQ' tabla,
    to_number(nvl(tarifa_basica, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200006AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/07/2000' fecha_fact,
    'CARGA.CARGA200007AUTQ' tabla,
    to_number(nvl(tarifa_basica_mensual, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200007AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/08/2000' fecha_fact,
    'CARGA.CARGA200008AUTQ' tabla,
    to_number(nvl(tarifa_basica_mensual, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200008AUTQ
union all
select
    ciudad cuenta,
    apellidos num_fact,
    tipo_cta_cte_aho cedula,
    fechaexp nomres,
    ltrim(rtrim(balance1)) || ' ' || ltrim(rtrim(balance30)) direccion,
    ltrim(rtrim(c_ident_o_ruc)) ciudad,
    '0' valorfactura,
    cargo_diners iva,
    to_number(nvl(total1, '0')) + to_number(nvl(cargo_a_fact, '0')) otros_impuesto,
    to_number(nvl(cargo_diners, '0')) + to_number(nvl(total1, '0')) + to_number(nvl(cargo_a_fact, '0')) total_impuesto,
    '' formapago,
    '24/09/2000' fecha_fact,
    'CARGA.CARGA200009AUTQ' tabla,
    to_number(nvl(iva_12, 0)) tbasica,
    0 cargopaquete,
    'apellidos' fact_field
from
    carga.carga200009AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/10/2000' fecha_fact,
    'CARGA.CARGA200010AUTQ' tabla,
    to_number(nvl(tarifa_basica_mensual, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200010AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/11/2000' fecha_fact,
    'CARGA.CARGA200011AUTQ' tabla,
    to_number(nvl(tarifa_basica_mensual, 0)) tbasica,
    0 cargopaquete,
    'factura' fact_field
from
    carga.carga200011AUTQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/11/2000' fecha_fact,
    'CARGA.CARGA200011TARG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200011TARG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/12/2000' fecha_fact,
    'CARGA.CARGA200012TARG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200012TARG
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/13/2000' fecha_fact,
    'CARGA.CARGA200013TARG' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) + to_number(nvl(tarifa_basica_suspendido, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp1400_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp1660_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp2100_cargo_a_paquete, 0)) + to_number(nvl(cp2200_cargo_a_paquete, 0)) + to_number(nvl(cp2400_cargo_a_paquete, 0)) + to_number(nvl(cp2700_cargo_a_paquete, 0)) + to_number(nvl(cp2760_cargo_a_paquete, 0)) + to_number(nvl(cp3300_cargo_a_paquete, 0)) + to_number(nvl(cp375_cargo_a_paquete, 0)) + to_number(nvl(cp3990_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp590_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp729_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp999_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) + to_number(nvl(cpeq48_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200013TARG
union all
select
    cuenta,
    no_factura num_fact,
    '' cedula,
    nombres nomres,
    'S/D' direccion,
    'S/C' ciudad,
    tot_fact valorfactura,
    iva_10 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_10, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_for_pag formapago,
    '24/01/2000' fecha_fact,
    'CARGA.CARGA200001TARQ' tabla,
    to_number(nvl(t_basica_noches_o_f_sema, 0)) + to_number(nvl(t_basica_noches_y_f_sema, 0)) + to_number(nvl(t_basica_n_o_fs_internos, 0)) + to_number(nvl(t_basica_n_o_f_contingen, 0)) + to_number(nvl(t_basica_n_o_f_milenio, 0)) + to_number(nvl(t_basica_n_y_fs_internos, 0)) + to_number(nvl(t_basica_n_y_f_contingen, 0)) + to_number(nvl(t_basica_n_y_f_milenio, 0)) tbasica,
    to_number(nvl(cargo_paquete_best, 0)) + to_number(nvl(cargo_paquete_ciaem1000, 0)) + to_number(nvl(cargo_paquete_ciaem1700, 0)) + to_number(nvl(cargo_paquete_ciaprodu, 0)) + to_number(nvl(cargo_paquete_corp_plus, 0)) + to_number(nvl(cargo_paquete_delta, 0)) + to_number(nvl(cargo_paquete_em2700, 0)) + to_number(nvl(cargo_paquete_em3400, 0)) + to_number(nvl(cargo_paquete_em4500, 0)) + to_number(nvl(cargo_paquete_empem1000, 0)) + to_number(nvl(cargo_paquete_empem1700, 0)) + to_number(nvl(cargo_paquete_empprodu, 0)) + to_number(nvl(cargo_paquete_empre800, 0)) + to_number(nvl(cargo_paquete_empre800_2, 0)) + to_number(nvl(cargo_paquete_empresa, 0)) + to_number(nvl(cargo_paquete_executive, 0)) + to_number(nvl(cargo_paquete_negocio, 0)) + to_number(nvl(cargo_paquete_nego_plus, 0)) + to_number(nvl(cargo_paquete_omega, 0)) + to_number(nvl(cargo_paquete_premium, 0)) + to_number(nvl(cargo_paquete_pro, 0)) + to_number(nvl(cargo_paquete_savings, 0)) + to_number(nvl(cargo_paquete_sigma, 0)) + to_number(nvl(cargo_paquete_special, 0)) + to_number(nvl(cargo_paquete_vega, 0)) + to_number(nvl(cargo_paquete_zeta, 0)) cargopaquete,
    'no_factura' fact_field
from
    carga.carga200001TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    total_general valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/02/2000' fecha_fact,
    'CARGA.CARGA200002TARQ' tabla,
    to_number(nvl(t_basica_n_o_f_milenio, 0)) + to_number(nvl(t_basica_n_y_f_contingen, 0)) + to_number(nvl(t_basica_n_y_f_milenio, 0)) + to_number(nvl(t_bas_noc_o_f_sema, 0)) + to_number(nvl(t_bas_noc_y_f_sema, 0)) + to_number(nvl(t_bas_n_o_fs_internos, 0)) + to_number(nvl(t_bas_n_o_f_contingen, 0)) + to_number(nvl(t_bas_n_y_fs_internos, 0)) tbasica,
    to_number(nvl(carg_pqt_best, 0)) + to_number(nvl(carg_pqt_ciaem1000, 0)) + to_number(nvl(carg_pqt_ciaem1700, 0)) + to_number(nvl(carg_pqt_ciaem2500, 0)) + to_number(nvl(carg_pqt_ciaprodu, 0)) + to_number(nvl(carg_pqt_corp_plus, 0)) + to_number(nvl(carg_pqt_delta, 0)) + to_number(nvl(carg_pqt_em2700, 0)) + to_number(nvl(carg_pqt_em3400, 0)) + to_number(nvl(carg_pqt_em4500, 0)) + to_number(nvl(carg_pqt_empem1000, 0)) + to_number(nvl(carg_pqt_empem1700, 0)) + to_number(nvl(carg_pqt_empprodu, 0)) + to_number(nvl(carg_pqt_empre500_2, 0)) + to_number(nvl(carg_pqt_empre800, 0)) + to_number(nvl(carg_pqt_empre800_2, 0)) + to_number(nvl(carg_pqt_empresa, 0)) + to_number(nvl(carg_pqt_executive, 0)) + to_number(nvl(carg_pqt_negocio, 0)) + to_number(nvl(carg_pqt_nego_plus, 0)) + to_number(nvl(carg_pqt_omega, 0)) + to_number(nvl(carg_pqt_person500, 0)) + to_number(nvl(carg_pqt_pool1000, 0)) + to_number(nvl(carg_pqt_premium, 0)) + to_number(nvl(carg_pqt_pro, 0)) + to_number(nvl(carg_pqt_savings, 0)) + to_number(nvl(carg_pqt_sigma, 0)) + to_number(nvl(carg_pqt_special, 0)) + to_number(nvl(carg_pqt_vega, 0)) + to_number(nvl(carg_pqt_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200002TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    total_general valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_dep, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_dep, '0')) total_impuesto,
    desc_pago formapago,
    '24/03/2000' fecha_fact,
    'CARGA.CARGA200003TARQ' tabla,
    to_number(nvl(t_basica_n_o_f_milenio, 0)) + to_number(nvl(t_basica_n_y_f_contingen, 0)) + to_number(nvl(t_basica_n_y_f_milenio, 0)) + to_number(nvl(t_bas_noc_o_f_sema, 0)) + to_number(nvl(t_bas_noc_y_f_sema, 0)) + to_number(nvl(t_bas_n_o_fs_internos, 0)) + to_number(nvl(t_bas_n_o_f_contingen, 0)) + to_number(nvl(t_bas_n_y_fs_internos, 0)) tbasica,
    to_number(nvl(carg_pqt_best, 0)) + to_number(nvl(carg_pqt_ciaem1000, 0)) + to_number(nvl(carg_pqt_ciaem1700, 0)) + to_number(nvl(carg_pqt_ciaem2500, 0)) + to_number(nvl(carg_pqt_ciaprodu, 0)) + to_number(nvl(carg_pqt_corp_plus, 0)) + to_number(nvl(carg_pqt_delta, 0)) + to_number(nvl(carg_pqt_em2700, 0)) + to_number(nvl(carg_pqt_em3400, 0)) + to_number(nvl(carg_pqt_em4500, 0)) + to_number(nvl(carg_pqt_empem1000, 0)) + to_number(nvl(carg_pqt_empem1700, 0)) + to_number(nvl(carg_pqt_empprodu, 0)) + to_number(nvl(carg_pqt_empre500_2, 0)) + to_number(nvl(carg_pqt_empre800, 0)) + to_number(nvl(carg_pqt_empre800_2, 0)) + to_number(nvl(carg_pqt_empresa, 0)) + to_number(nvl(carg_pqt_executive, 0)) + to_number(nvl(carg_pqt_negocio, 0)) + to_number(nvl(carg_pqt_nego_plus, 0)) + to_number(nvl(carg_pqt_omega, 0)) + to_number(nvl(carg_pqt_person500, 0)) + to_number(nvl(carg_pqt_pool1000, 0)) + to_number(nvl(carg_pqt_premium, 0)) + to_number(nvl(carg_pqt_pro, 0)) + to_number(nvl(carg_pqt_savings, 0)) + to_number(nvl(carg_pqt_sigma, 0)) + to_number(nvl(carg_pqt_special, 0)) + to_number(nvl(carg_pqt_vega, 0)) + to_number(nvl(carg_pqt_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200003TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/04/2000' fecha_fact,
    'CARGA.CARGA200004TARQ' tabla,
    to_number(nvl(tarifa_basica, 0)) tbasica,
    to_number(nvl(cargo_paquete_best, 0)) + to_number(nvl(cargo_paquete_ciaem1000, 0)) + to_number(nvl(cargo_paquete_ciaem1700, 0)) + to_number(nvl(cargo_paquete_ciaem2500, 0)) + to_number(nvl(cargo_paquete_ciaprodu, 0)) + to_number(nvl(cargo_paquete_corp_plus, 0)) + to_number(nvl(cargo_paquete_delta, 0)) + to_number(nvl(cargo_paquete_em2700, 0)) + to_number(nvl(cargo_paquete_em3400, 0)) + to_number(nvl(cargo_paquete_em4500, 0)) + to_number(nvl(cargo_paquete_empem1000, 0)) + to_number(nvl(cargo_paquete_empem1700, 0)) + to_number(nvl(cargo_paquete_empprodu, 0)) + to_number(nvl(cargo_paquete_empre500_2, 0)) + to_number(nvl(cargo_paquete_empre800, 0)) + to_number(nvl(cargo_paquete_empre800_2, 0)) + to_number(nvl(cargo_paquete_empresa, 0)) + to_number(nvl(cargo_paquete_executive, 0)) + to_number(nvl(cargo_paquete_negocio, 0)) + to_number(nvl(cargo_paquete_nego_plus, 0)) + to_number(nvl(cargo_paquete_omega, 0)) + to_number(nvl(cargo_paquete_person500, 0)) + to_number(nvl(cargo_paquete_pool1700, 0)) + to_number(nvl(cargo_paquete_pool1000, 0)) + to_number(nvl(cargo_paquete_premium, 0)) + to_number(nvl(cargo_paquete_pro, 0)) + to_number(nvl(cargo_paquete_savings, 0)) + to_number(nvl(cargo_paquete_sigma, 0)) + to_number(nvl(cargo_paquete_special, 0)) + to_number(nvl(cargo_paquete_vega, 0)) + to_number(nvl(cargo_paquete_vip, 0)) + to_number(nvl(cargo_paquete_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200004TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/05/2000' fecha_fact,
    'CARGA.CARGA200005TARQ' tabla,
    to_number(nvl(tarifa_basica, 0)) tbasica,
    to_number(nvl(cargo_paquete_best, 0)) + to_number(nvl(cargo_paquete_ciaem1000, 0)) + to_number(nvl(cargo_paquete_ciaem1700, 0)) + to_number(nvl(cargo_paquete_ciaem2500, 0)) + to_number(nvl(cargo_paquete_ciaprodu, 0)) + to_number(nvl(cargo_paquete_corp_plus, 0)) + to_number(nvl(cargo_paquete_delta, 0)) + to_number(nvl(cargo_paquete_em2700, 0)) + to_number(nvl(cargo_paquete_em3400, 0)) + to_number(nvl(cargo_paquete_em4500, 0)) + to_number(nvl(cargo_paquete_empem1000, 0)) + to_number(nvl(cargo_paquete_empem1700, 0)) + to_number(nvl(cargo_paquete_empprodu, 0)) + to_number(nvl(cargo_paquete_empre500_2, 0)) + to_number(nvl(cargo_paquete_empre800, 0)) + to_number(nvl(cargo_paquete_empre800_2, 0)) + to_number(nvl(cargo_paquete_empresa, 0)) + to_number(nvl(cargo_paquete_executive, 0)) + to_number(nvl(cargo_paquete_negocio, 0)) + to_number(nvl(cargo_paquete_nego_plus, 0)) + to_number(nvl(cargo_paquete_omega, 0)) + to_number(nvl(cargo_paquete_person500, 0)) + to_number(nvl(cargo_paquete_pool1700, 0)) + to_number(nvl(cargo_paquete_pool1000, 0)) + to_number(nvl(cargo_paquete_premium, 0)) + to_number(nvl(cargo_paquete_pro, 0)) + to_number(nvl(cargo_paquete_savings, 0)) + to_number(nvl(cargo_paquete_sigma, 0)) + to_number(nvl(cargo_paquete_special, 0)) + to_number(nvl(cargo_paquete_vega, 0)) + to_number(nvl(cargo_paquete_vip, 0)) + to_number(nvl(cargo_paquete_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200005TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    forma_de_pago formapago,
    '24/06/2000' fecha_fact,
    'CARGA.CARGA200006TARQ' tabla,
    to_number(nvl(t_basica_noches_o_f_sema, 0)) + to_number(nvl(t_basica_noches_y_f_sema, 0)) + to_number(nvl(t_basica_n_o_fs_internos, 0)) + to_number(nvl(t_basica_n_o_f_contingen, 0)) + to_number(nvl(t_basica_n_o_f_milenio, 0)) + to_number(nvl(t_basica_n_y_fs_internos, 0)) + to_number(nvl(t_basica_n_y_f_contingen, 0)) + to_number(nvl(t_basica_n_y_f_milenio, 0)) tbasica,
    to_number(nvl(cargo_paquete_best, 0)) + to_number(nvl(cargo_paquete_blitz13, 0)) + to_number(nvl(cargo_paquete_ciaem1000, 0)) + to_number(nvl(cargo_paquete_ciaem1700, 0)) + to_number(nvl(cargo_paquete_ciaem2500, 0)) + to_number(nvl(cargo_paquete_ciaprodu, 0)) + to_number(nvl(cargo_paquete_corp_plus, 0)) + to_number(nvl(cargo_paquete_delta, 0)) + to_number(nvl(cargo_paquete_em2700, 0)) + to_number(nvl(cargo_paquete_em3400, 0)) + to_number(nvl(cargo_paquete_em4500, 0)) + to_number(nvl(cargo_paquete_empem1000, 0)) + to_number(nvl(cargo_paquete_empem1700, 0)) + to_number(nvl(cargo_paquete_empprodu, 0)) + to_number(nvl(cargo_paquete_empre500_2, 0)) + to_number(nvl(cargo_paquete_empre800, 0)) + to_number(nvl(cargo_paquete_empre800_2, 0)) + to_number(nvl(cargo_paquete_empresa, 0)) + to_number(nvl(cargo_paquete_executive, 0)) + to_number(nvl(cargo_paquete_negocio, 0)) + to_number(nvl(cargo_paquete_nego_plus, 0)) + to_number(nvl(cargo_paquete_omega, 0)) + to_number(nvl(cargo_paquete_person500, 0)) + to_number(nvl(cargo_paquete_pool1700, 0)) + to_number(nvl(cargo_paquete_pool1000, 0)) + to_number(nvl(cargo_paquete_pool3500, 0)) + to_number(nvl(cargo_paquete_premium, 0)) + to_number(nvl(cargo_paquete_pro, 0)) + to_number(nvl(cargo_paquete_savings, 0)) + to_number(nvl(cargo_paquete_sigma, 0)) + to_number(nvl(cargo_paquete_special, 0)) + to_number(nvl(cargo_paquete_vega, 0)) + to_number(nvl(cargo_paquete_vip, 0)) + to_number(nvl(cargo_paquete_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200006TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    ltrim(rtrim(saldoanter)) || ' ' || ltrim(rtrim(total2)) nomres,
    ltrim(rtrim(cargo_por_debito_diners)) direccion,
    ltrim(rtrim(campo_nulo)) ciudad,
    total valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/07/2000' fecha_fact,
    'CARGA.CARGA200007TARQ' tabla,
    to_number(nvl(t_basica_noches_o_f_sema, 0)) + to_number(nvl(t_basica_noches_y_f_sema, 0)) + to_number(nvl(t_basica_n_o_fs_internos, 0)) + to_number(nvl(t_basica_n_o_f_contingen, 0)) + to_number(nvl(t_basica_n_o_f_milenio, 0)) + to_number(nvl(t_basica_n_y_fs_internos, 0)) + to_number(nvl(t_basica_n_y_f_contingen, 0)) + to_number(nvl(t_basica_n_y_f_milenio, 0)) tbasica,
    to_number(nvl(cargo_paquete_best, 0)) + to_number(nvl(cargo_paquete_ciaem1000, 0)) + to_number(nvl(cargo_paquete_ciaem1700, 0)) + to_number(nvl(cargo_paquete_ciaem2500, 0)) + to_number(nvl(cargo_paquete_ciaprodu, 0)) + to_number(nvl(cargo_paquete_corp_plus, 0)) + to_number(nvl(cargo_paquete_delta, 0)) + to_number(nvl(cargo_paquete_em2700, 0)) + to_number(nvl(cargo_paquete_em3400, 0)) + to_number(nvl(cargo_paquete_em4500, 0)) + to_number(nvl(cargo_paquete_empem1000, 0)) + to_number(nvl(cargo_paquete_empem1700, 0)) + to_number(nvl(cargo_paquete_empprodu, 0)) + to_number(nvl(cargo_paquete_empre500_2, 0)) + to_number(nvl(cargo_paquete_empre800, 0)) + to_number(nvl(cargo_paquete_empre800_2, 0)) + to_number(nvl(cargo_paquete_empresa, 0)) + to_number(nvl(cargo_paquete_executive, 0)) + to_number(nvl(cargo_paquete_negocio, 0)) + to_number(nvl(cargo_paquete_nego_plus, 0)) + to_number(nvl(cargo_paquete_omega, 0)) + to_number(nvl(cargo_paquete_person500, 0)) + to_number(nvl(cargo_paquete_pool1700, 0)) + to_number(nvl(cargo_paquete_pool1000, 0)) + to_number(nvl(cargo_paquete_pool3500, 0)) + to_number(nvl(cargo_paquete_premium, 0)) + to_number(nvl(cargo_paquete_pro, 0)) + to_number(nvl(cargo_paquete_savings, 0)) + to_number(nvl(cargo_paquete_sigma, 0)) + to_number(nvl(cargo_paquete_special, 0)) + to_number(nvl(cargo_paquete_vega, 0)) + to_number(nvl(cargo_paquete_vip, 0)) + to_number(nvl(cargo_paquete_zeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200007TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(agua, '0')) + to_number(nvl(deporte, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(agua, '0')) + to_number(nvl(deporte, '0')) total_impuesto,
    desc_pago formapago,
    '24/08/2000' fecha_fact,
    'CARGA.CARGA200008TARQ' tabla,
    to_number(nvl(tarifa_b, 0)) tbasica,
    to_number(nvl(cp129, 0)) + to_number(nvl(cp139, 0)) + to_number(nvl(cp149, 0)) + to_number(nvl(cp199, 0)) + to_number(nvl(cp47, 0)) + to_number(nvl(cp49, 0)) + to_number(nvl(cp50, 0)) + to_number(nvl(cp55, 0)) + to_number(nvl(cp59, 0)) + to_number(nvl(cp69, 0)) + to_number(nvl(cp89, 0)) + to_number(nvl(cp95, 0)) + to_number(nvl(cp99, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200008TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/09/2000' fecha_fact,
    'CARGA.CARGA200009TARQ' tabla,
    to_number(nvl(tarifa_basica_mensual, 0)) tbasica,
    to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200009TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/10/2000' fecha_fact,
    'CARGA.CARGA200010TARQ' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) tbasica,
    to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200010TARQ
union all
select
    cuenta,
    factura num_fact,
    c_ident_o_ruc cedula,
    nombres nomres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    totalfact valorfactura,
    iva_12 iva,
    to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) otros_impuesto,
    to_number(nvl(iva_12, '0')) + to_number(nvl(imp_agua_potable_10, '0')) + to_number(nvl(imp_deporte_5, '0')) total_impuesto,
    desc_pago formapago,
    '24/10/2000' fecha_fact,
    'CARGA.CARGA200010TARQ' tabla,
    to_number(nvl(tarifa_basica_mensual_1, 0)) + to_number(nvl(tarifa_basica_mensual_2, 0)) tbasica,
    to_number(nvl(cp1110_cargo_a_paquete, 0)) + to_number(nvl(cp129_cargo_a_paquete, 0)) + to_number(nvl(cp139_cargo_a_paquete, 0)) + to_number(nvl(cp149_cargo_a_paquete, 0)) + to_number(nvl(cp199_cargo_a_paquete, 0)) + to_number(nvl(cp45_cargo_a_paquete, 0)) + to_number(nvl(cp47_cargo_a_paquete, 0)) + to_number(nvl(cp49_cargo_a_paquete, 0)) + to_number(nvl(cp50_cargo_a_paquete, 0)) + to_number(nvl(cp55_cargo_a_paquete, 0)) + to_number(nvl(cp59_cargo_a_paquete, 0)) + to_number(nvl(cp69_cargo_a_paquete, 0)) + to_number(nvl(cp89_cargo_a_paquete, 0)) + to_number(nvl(cp95_cargo_a_paquete, 0)) + to_number(nvl(cp99_cargo_a_paquete, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200011TARQ
union all
select
    cuenta,
    factura num_fact,
    '' cedula,
    nombre nomres,
    ltrim(rtrim(direcc)) direccion,
    'S/C' ciudad,
    total_general valorfactura,
    iva_10 iva,
    to_number(nvl(imp_agua, '0')) + to_number(nvl(imp_dep, '0')) otros_impuesto,
    to_number(nvl(iva_10, '0')) + to_number(nvl(imp_agua, '0')) + to_number(nvl(imp_dep, '0')) total_impuesto,
    forma_de_pago formapago,
    '24/12/2000' fecha_fact,
    'CARGA.CARGA200012TARQ' tabla,
    to_number(nvl(tarifa_basica, 0)) tbasica,
    to_number(nvl(cargocorpplus, 0)) + to_number(nvl(cargodelta, 0)) + to_number(nvl(cargofacuio, 0)) + to_number(nvl(cargonegocio, 0)) + to_number(nvl(cargonegoplus, 0)) + to_number(nvl(cargoomega, 0)) + to_number(nvl(cargoreacti, 0)) + to_number(nvl(cargosigma, 0)) + to_number(nvl(cargovega, 0)) + to_number(nvl(cargozeta, 0)) cargopaquete,
    'factura' fact_field
from
    carga.carga200012TARQ
union all    -- 2001
select
    b.cuenta,
    b.factura num_fact,
    b.cidentoruc cedula,
    b.apellidos || b.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    b.totalfact valor_factura,
    b.iva12 iva,
    to_number(nvl(b.impaguapotable10, '0')) + to_number(nvl(b.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(b.impaguapotable10, '0')) + to_number(nvl(b.impdeporte5, '0')) + to_number(nvl(b.iva12, '0')) total_impuesto,
    b.descpago formapago,
    '23/01/2001' fecha_fact,
    'CARGA.CARGA200101g' tabla,
    to_number(nvl(b.tarifabasicamensual, 0)) + to_number(nvl(b.tarifabasicamensual2, 0)) + to_number(nvl(b.tarifabasicasuspendido, 0)) tbasica,
    b.cargopaquete,
    'factura' fact_field
from
    carga200101g b
union all
select
    c.cuenta,
    c.factura num_fact,
    c.cidentoruc cedula,
    c.apellidos || c.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    c.totalfact valor_factura,
    c.iva12 iva,
    to_number(nvl(c.impaguapotable10, '0')) + to_number(nvl(c.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(c.impaguapotable10, '0')) + to_number(nvl(c.impdeporte5, '0')) + to_number(nvl(c.iva12, '0')) total_impuesto,
    c.descpago formapago,
    '23/01/2001' fecha_fact,
    'CARGA.CARGA200101g' tabla,
    to_number(
        replace(
            replace(
                replace(
                    nvl(ltrim(rtrim(c.tarifabasicamensual)), 0),
                    '-',
                    '0'
                ),
                '"',
                ''
            ),
            'V',
            '0'
        )
    ) + to_number(
        replace(
            replace(
                replace(nvl(c.tarifabasicasuspendido, 0), '-', '0'),
                '"',
                ''
            ),
            'V',
            '0'
        )
    ) tbasica,
    c.cargopaquete,
    'factura' fact_field
from
    carga200101autq c
union all
select
    d.cuenta,
    d.factura num_fact,
    d.cidentoruc cedula,
    d.apellidos || d.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    d.totalfact valor_factura,
    d.iva12 iva,
    to_number(nvl(d.impaguapotable10, '0')) + to_number(nvl(d.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(d.impaguapotable10, '0')) + to_number(nvl(d.impdeporte5, '0')) + to_number(nvl(d.iva12, '0')) total_impuesto,
    d.descpago formapago,
    '23/02/2001' fecha_fact,
    'CARGA.CARGA200102g' tabla,
    to_number(nvl(d.tarifabasicamensual, 0)) + to_number(nvl(d.tarifabasicamensual2, 0)) + to_number(nvl(d.tarifabasicasuspendido, 0)) tbasica,
    d.cargopaquete,
    'factura' fact_field
from
    carga200102g d
union all
select
    da.cuenta,
    da.factura num_fact,
    da.cidentoruc cedula,
    da.apellidos || da.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(ciudad)) ciudad,
    da.totalfact valor_factura,
    da.iva12 iva,
    to_number(nvl(da.impaguapotable10, '0')) + to_number(nvl(da.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(da.impaguapotable10, '0')) + to_number(nvl(da.impdeporte5, '0')) + to_number(nvl(da.iva12, '0')) total_impuesto,
    da.descpago formapago,
    '23/02/2001' fecha_fact,
    'CARGA.CARGA200102g' tabla,
    to_number(nvl(da.tarifabasicamensual, 0)) + to_number(nvl(da.tarifabasicasuspendido, 0)) tbasica,
    da.cargopaquete,
    'factura' fact_field
from
    carga200102autq da
union all
select
    e.cuenta,
    e.factura num_fact,
    e.cidentoruc cedula,
    e.apellidos || e.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    e.totalfact valor_factura,
    e.iva12 iva,
    to_number(nvl(e.impaguapotable10, '0')) + to_number(nvl(e.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(e.impaguapotable10, '0')) + to_number(nvl(e.impdeporte5, '0')) + to_number(nvl(e.iva12, '0')) total_impuesto,
    e.descpago formapago,
    '23/03/2001' fecha_fact,
    'CARGA.CARGA200103g' tabla,
    to_number(nvl(e.tarifabasicamensual, 0)) + to_number(nvl(e.tarifabasicamensual2, 0)) + to_number(nvl(e.tarifabasicasuspendido, 0)) tbasica,
    e.cargopaquete,
    'factura' fact_field
from
    carga200103g e
union all
select
    e.cuenta,
    e.factura num_fact,
    e.cidentoruc cedula,
    e.apellidos || e.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    e.totalfact valor_factura,
    e.iva12 iva,
    to_number(nvl(e.impaguapotable10, '0')) + to_number(nvl(e.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(e.impaguapotable10, '0')) + to_number(nvl(e.impdeporte5, '0')) + to_number(nvl(e.iva12, '0')) total_impuesto,
    e.descpago formapago,
    '23/03/2001' fecha_fact,
    'CARGA.CARGA200103g' tabla,
    to_number(nvl(e.tarifabasicamensual, 0)) + to_number(nvl(e.tarifabasicasuspendido, 0)) tbasica,
    e.cargopaquete,
    'factura' fact_field
from
    carga200103autq e
union all
select
    f.cuenta,
    f.factura num_fact,
    f.cidentoruc cedula,
    f.apellidos || f.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    f.totalfact valor_factura,
    f.iva12 iva,
    to_number(nvl(f.impaguapotable10, '0')) + to_number(nvl(f.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(f.impaguapotable10, '0')) + to_number(nvl(f.impdeporte5, '0')) + to_number(nvl(f.iva12, '0')) total_impuesto,
    f.descpago formapago,
    '23/04/2001' fecha_fact,
    'CARGA.CARGA200104g' tabla,
    to_number(nvl(f.tarifabsicamensual, 0)) + to_number(nvl(f.tarifabsicamensual, 0)) + to_number(nvl(f.tarifabsicamensual2, 0)) + to_number(nvl(f.tarifabsicasupendido, 0)) tbasica,
    f.cargopaquete,
    'factura' fact_field
from
    carga200104 f
union all
select
    h.cuenta,
    h.factura num_fact,
    h.cidentoruc cedula,
    h.apellidos || h.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    h.totalfact valor_factura,
    h.iva12 iva,
    to_number(nvl(h.impaguapotable10, '0')) + to_number(nvl(h.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(h.impaguapotable10, '0')) + to_number(nvl(h.impdeporte5, '0')) + to_number(nvl(h.iva12, '0')) total_impuesto,
    h.descpago formapago,
    '23/05/2001' fecha_fact,
    'CARGA.CARGA200105g' tabla,
    to_number(
        replace(
            replace(
                replace(
                    nvl(ltrim(rtrim(h.tarifabsicamensual)), 0),
                    '-',
                    '0'
                ),
                '"',
                ''
            ),
            'V',
            '0'
        )
    ) + to_number(
        replace(
            nvl(ltrim(rtrim(h.tarifabsicamensual2)), 0),
            '-',
            '0'
        )
    ) + to_number(
        replace(
            nvl(ltrim(rtrim(h.tarifabsicamensual3)), 0),
            '-',
            '0'
        )
    ) + to_number(
        replace(
            nvl(ltrim(rtrim(h.tarifabsicasupendido)), 0),
            '-',
            '0'
        )
    ) tbasica,
    h.cargopaquete,
    'factura' fact_field
from
    carga200105 h
union all
select
    i.cuenta,
    i.factura num_fact,
    i.cidentoruc cedula,
    i.apellidos || i.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    i.totalfact valor_factura,
    i.iva14 iva,
    to_number(nvl(i.impaguapotable10, '0')) + to_number(nvl(i.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(i.impaguapotable10, '0')) + to_number(nvl(i.impdeporte5, '0')) + to_number(nvl(i.iva14, '0')) total_impuesto,
    i.descpago formapago,
    '23/06/2001' fecha_fact,
    'CARGA.CARGA200106g' tabla,
    to_number(nvl(i.tarifabsicamensual, 0)) + to_number(nvl(i.tarifabsicamensual, 0)) + to_number(nvl(i.tarifabsicamensual2, 0)) + to_number(nvl(i.tarifabsicasupendido, 0)) tbasica,
    i.cargopaquete,
    'factura' fact_field
from
    carga200106 i
union all
select
    j.cuenta,
    j.numfactura num_fact,
    j.cidentoruc cedula,
    j.apellidos || j.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    j.totalfact valor_factura,
    j.iva14 iva,
    to_number(nvl(j.impaguapotable10, '0')) + to_number(nvl(j.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(j.impaguapotable10, '0')) + to_number(nvl(j.impdeporte5, '0')) + to_number(nvl(j.iva14, '0')) total_impuesto,
    j.descpago formapago,
    '23/07/2001' fecha_fact,
    'CARGA.CARGA200107' tabla,
    to_number(nvl(j.tarifabsicamensual, 0)) + to_number(nvl(j.tarifabsicamensual, 0)) + to_number(nvl(j.tarifabsicamensual2, 0)) + to_number(nvl(j.tarifabsicasupendido, 0)) tbasica,
    j.cargopaquete,
    'numfactura' fact_field
from
    carga200107 j
union all
select
    k.cuenta,
    k.factura num_fact,
    k.cidentoruc cedula,
    k.apellidos || k.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    k.totalfact valor_factura,
    k.iva14 iva,
    to_number(nvl(k.impaguapotable10, '0')) + to_number(nvl(k.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(k.impaguapotable10, '0')) + to_number(nvl(k.impdeporte5, '0')) + to_number(nvl(k.iva14, '0')) total_impuesto,
    k.descpago formapago,
    '23/08/2001' fecha_fact,
    'CARGA.CARGA200108' tabla,
    to_number(nvl(k.tarifabsicamensual, 0)) + to_number(nvl(k.tarifabsicamensual2, 0)) + to_number(nvl(k.tarifabsicamensual3, 0)) + to_number(nvl(k.tarifabsicasupendido, 0)) tbasica,
    k.cargopaquete,
    'factura' fact_field
from
    carga200108 k
union all
select
    m.cuenta,
    m.factura num_fact,
    m.cidentoruc cedula,
    m.apellidos || m.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    m.totalfact valor_factura,
    m.iva12 iva,
    to_number(nvl(m.impaguapotable10, '0')) + to_number(nvl(m.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(m.impaguapotable10, '0')) + to_number(nvl(m.impdeporte5, '0')) + to_number(nvl(m.iva12, '0')) total_impuesto,
    m.cardnumber formapago,
    '23/09/2001' fecha_fact,
    'CARGA.CARGA200109' tabla,
    to_number(nvl(m.tarifabsicamensual, 0)) + to_number(nvl(m.tarifabsicamensual2, 0)) + to_number(nvl(m.tarifabsicamensual3, 0)) + to_number(nvl(m.tarifabsicasupendido, 0)) tbasica,
    m.cargopaquete,
    'factura' fact_field
from
    carga200109 m
union all
select
    n.cuenta,
    n.factura num_fact,
    n.cidentoruc cedula,
    n.apellidos || n.nombres nombres,
    ltrim(rtrim(balance1)) || ' ' || ltrim(rtrim(balance30)) direccion,
    ltrim(rtrim(canton)) ciudad,
    n.totalfact valor_factura,
    n.iva12 iva,
    to_number(
        replace(
            nvl(rtrim(ltrim(n.impaguapotable10)), '0'),
            '-',
            '0'
        )
    ) + to_number(
        replace(nvl(rtrim(ltrim(n.impdeporte5)), '0'), '-', '0')
    ) otros_impuesto,
    to_number(
        replace(
            nvl(rtrim(ltrim(n.impaguapotable10)), '0'),
            '-',
            '0'
        )
    ) + to_number(
        replace(nvl(rtrim(ltrim(n.impdeporte5)), '0'), '-', '0')
    ) + to_number(replace(nvl(rtrim(ltrim(n.iva12)), '0'), '-', '0')) total_impuesto,
    n.cardnumber formapago,
    '23/01/2001' fecha_fact,
    'CARGA.CARGA200110' tabla,
    to_number(
        replace(
            replace(
                nvl(ltrim(rtrim(n.tarifabsicamensual)), 0),
                '-',
                '0'
            ),
            'V',
            '0'
        )
    ) + to_number(
        replace(
            replace(
                nvl(ltrim(rtrim(n.tarifabsicamensual2)), 0),
                '-',
                '0'
            ),
            'V',
            '0'
        )
    ) + to_number(
        replace(
            replace(
                nvl(rtrim(ltrim(n.tarifabsicamensual3)), 0),
                '-',
                '0'
            ),
            'V',
            '0'
        )
    ) + to_number(
        replace(
            replace(
                nvl(rtrim(ltrim(n.tarifabsicasupendido)), 0),
                '-',
                '0'
            ),
            'V',
            '0'
        )
    ) tbasica,
    n.cargopaquete,
    'factura' fact_field
from
    carga200110 n
union all
select
    o.cuenta,
    o.factura num_fact,
    o.cidentoruc cedula,
    o.apellidos || o.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    o.totalfact valor_factura,
    o.iva12 iva,
    to_number(nvl(o.impaguapotable10, '0')) + to_number(nvl(o.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(o.impaguapotable10, '0')) + to_number(nvl(o.impdeporte5, '0')) + to_number(nvl(o.iva12, '0')) total_impuesto,
    o.cardnumber formapago,
    '23/11/2001' fecha_fact,
    'CARGA.CARGA200111' tabla,
    to_number(nvl(o.tarifabsicamensual, 0)) + to_number(nvl(o.tarifabsicamensual2, 0)) + to_number(nvl(o.tarifabsicamensual3, 0)) + to_number(nvl(o.tarifabsicasupendido, 0)) tbasica,
    o.cargopaquete,
    'factura' fact_field
from
    carga200111 o
union all
select
    p.cuenta,
    p.factura num_fact,
    p.cidentoruc cedula,
    p.apellidos || p.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    p.totalfact valor_factura,
    p.iva12 iva,
    to_number(nvl(p.impaguapotable10, '0')) + to_number(nvl(p.impdeporte5, '0')) otros_impuesto,
    to_number(nvl(p.impaguapotable10, '0')) + to_number(nvl(p.impdeporte5, '0')) + to_number(nvl(p.iva12, '0')) total_impuesto,
    p.cardnumber formapago,
    '23/12/2001' fecha_fact,
    'CARGA.CARGA200112' tabla,
    to_number(nvl(p.tarifabsicamensual, 0)) + to_number(nvl(p.tarifabsicamensual2, 0)) + to_number(nvl(p.tarifabsicamensual3, 0)) + to_number(nvl(p.tarifabsicasupendido, 0)) tbasica,
    p.cargopaquete,
    'factura' fact_field
from
    carga200112 p
union all     -- 2002
select
    q.cuenta,
    q.N_FACTURA num_fact,
    q.ci_ruc cedula,
    q.apellidos || q.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    q.totalfact valor_factura,
    q.iva iva,
    to_number(nvl(q.impto_aguapotable, '0')) + to_number(nvl(q.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(q.impto_aguapotable, '0')) + to_number(nvl(q.impto_deporte, '0')) + to_number(nvl(q.iva, '0')) total_impuesto,
    q.cardnumber formapago,
    '23/01/2002' fecha_fact,
    'CARGA.CARGA200201' tabla,
    to_number(nvl(q.tarifa_basica, 0)) + to_number(nvl(q.tarifa_basica_supendido, 0)) tbasica,
    q.cargopaquete,
    'n_factura' fact_field
from
    carga200201 q
union all
select
    r.cuenta,
    r.N_FACTURA num_fact,
    r.ci_ruc cedula,
    r.apellidos || r.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    r.totalfact valor_factura,
    r.iva iva,
    to_number(nvl(r.impto_aguapotable, '0')) + to_number(nvl(r.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(r.impto_aguapotable, '0')) + to_number(nvl(r.impto_deporte, '0')) + to_number(nvl(r.iva, '0')) total_impuesto,
    r.cardnumber formapago,
    '23/02/2002' fecha_fact,
    'CARGA.CARGA200202' tabla,
    to_number(nvl(r.tarifa_basica, 0)) + to_number(nvl(r.tarifa_basica_supendido, 0)) tbasica,
    r.cargopaquete,
    'n_factura' fact_field
from
    carga200202 r
union all
select
    s.cuenta,
    s.N_FACTURA num_fact,
    s.ci_ruc cedula,
    s.apellidos || s.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    s.totalfact valor_factura,
    s.iva iva,
    to_number(nvl(s.impto_aguapotable, '0')) + to_number(nvl(s.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(s.impto_aguapotable, '0')) + to_number(nvl(s.impto_deporte, '0')) + to_number(nvl(s.iva, '0')) total_impuesto,
    s.cardnumber formapago,
    '23/03/2002' fecha_fact,
    'CARGA.CARGA200203' tabla,
    to_number(nvl(s.tarifa_basica, 0)) + to_number(nvl(s.tarifa_basica_supendido, 0)) tbasica,
    s.cargopaquete,
    'n_factura' fact_field
from
    carga200203 s
union all
select
    t.cuenta,
    t.N_FACTURA num_fact,
    t.ci_ruc cedula,
    t.apellidos || t.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    t.totalfact valor_factura,
    t.iva iva,
    to_number(
        replace(
            nvl(rtrim(ltrim(t.impto_aguapotable)), '0'),
            '-',
            ''
        )
    ) + to_number(
        replace(nvl(rtrim(ltrim(t.impto_deporte)), '0'), '-', '')
    ) otros_impuesto,
    to_number(
        replace(
            nvl(rtrim(ltrim(t.impto_aguapotable)), '0'),
            '-',
            ''
        )
    ) + to_number(
        replace(nvl(rtrim(ltrim(t.impto_deporte)), '0'), '-', '')
    ) + to_number(nvl(t.iva, '0')) total_impuesto,
    t.cardnumber formapago,
    '23/04/2002' fecha_fact,
    'CARGA.CARGA200204' tabla,
    to_number(
        replace(nvl(ltrim(rtrim(t.tarifa_basica)), 0), '-', '')
    ) + to_number(
        replace(
            nvl(rtrim(ltrim(t.tarifa_basica_supendido)), 0),
            '-',
            ''
        )
    ) tbasica,
    t.cargopaquete,
    'n_factura' fact_field
from
    carga200204 t
union all
select
    u.cuenta,
    u.N_FACTURA num_fact,
    u.ci_ruc cedula,
    u.apellidos || u.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    u.totalfact valor_factura,
    u.iva iva,
    to_number(nvl(u.impto_aguapotable, '0')) + to_number(nvl(u.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(u.impto_aguapotable, '0')) + to_number(nvl(u.impto_deporte, '0')) + to_number(nvl(u.iva, '0')) total_impuesto,
    u.cardnumber formapago,
    '23/05/2002' fecha_fact,
    'CARGA.CARGA200205' tabla,
    to_number(nvl(u.tarifa_basica, 0)) + to_number(nvl(u.tarifa_basica_supendido, 0)) tbasica,
    u.cargopaquete,
    'n_factura' fact_field
from
    carga200205 u
union all
select
    v.cuenta,
    v.N_FACTURA num_fact,
    v.ci_ruc cedula,
    v.apellidos || v.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    v.totalfact valor_factura,
    v.iva iva,
    to_number(nvl(v.impto_aguapotable, '0')) + to_number(nvl(v.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(v.impto_aguapotable, '0')) + to_number(nvl(v.impto_deporte, '0')) + to_number(nvl(v.iva, '0')) total_impuesto,
    v.cardnumber formapago,
    '23/06/2002' fecha_fact,
    'CARGA.CARGA200206' tabla,
    to_number(nvl(v.tarifa_basica, 0)) + to_number(nvl(v.tarifa_basica_supendido, 0)) tbasica,
    v.cargopaquete,
    'n_factura' fact_field
from
    carga200206 v
union all
select
    w.cuenta,
    w.N_FACTURA num_fact,
    w.ci_ruc cedula,
    w.apellidos || w.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    w.totalfact valor_factura,
    w.iva iva,
    to_number(nvl(w.impto_aguapotable, '0')) + to_number(nvl(w.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(w.impto_aguapotable, '0')) + to_number(nvl(w.impto_deporte, '0')) + to_number(nvl(w.iva, '0')) total_impuesto,
    w.cardnumber formapago,
    '23/07/2002' fecha_fact,
    'CARGA.CARGA200207' tabla,
    to_number(nvl(w.tarifa_basica, 0)) + to_number(nvl(w.tarifa_basica_supendido, 0)) tbasica,
    w.cargopaquete,
    'n_factura' fact_field
from
    carga200207 w
union all
select
    z.cuenta,
    z.N_FACTURA num_fact,
    z.ci_ruc cedula,
    z.apellidos || z.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    z.totalfact valor_factura,
    z.iva iva,
    to_number(nvl(z.impto_aguapotable, '0')) + to_number(nvl(z.impto_deporte, '0')) otros_impuesto,
    to_number(nvl(z.impto_aguapotable, '0')) + to_number(nvl(z.impto_deporte, '0')) + to_number(nvl(z.iva, '0')) total_impuesto,
    z.cardnumber formapago,
    '23/08/2002' fecha_fact,
    'CARGA.CARGA200208' tabla,
    to_number(nvl(z.tarifa_basica, 0)) + to_number(nvl(z.tarifa_basica_supendido, 0)) tbasica,
    z.cargopaquete,
    'n_factura' fact_field
from
    carga200208 z
union all     -- 2003
select
    aa.cuenta,
    aa.NFACTURA num_fact,
    aa.cidentoruc cedula,
    aa.apellidos || aa.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    aa.totalfact valor_factura,
    aa.iva12 iva,
    to_number(nvl(aa.impaguapotable, '0')) + to_number(nvl(aa.impdeporte, '0')) otros_impuesto,
    to_number(nvl(aa.impaguapotable, '0')) + to_number(nvl(aa.impdeporte, '0')) + to_number(nvl(aa.iva12, '0')) total_impuesto,
    aa.descpago formapago,
    fechaExp fecha_fact,
    'CARGA.CARGA200301' tabla,
    to_number(nvl(aa.tarifabasicamensual, 0)) + to_number(nvl(aa.tarifabasicamensual2, 0)) + to_number(nvl(aa.tarifabasicamensual3, 0)) + to_number(nvl(aa.tarifabasicasuspendido, 0)) tbasica,
    aa.cargopaquete,
    'n_factura' fact_field
from
    carga200301 aa
union all
select
    ab.cuenta,
    ab.NFACTURA num_fact,
    ab.cidentoruc cedula,
    ab.apellidos || ab.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ab.totalfact valor_factura,
    ab.iva12 iva,
    to_number(nvl(ab.impaguapotable, '0')) + to_number(nvl(ab.impdeporte, '0')) otros_impuesto,
    to_number(nvl(ab.impaguapotable, '0')) + to_number(nvl(ab.impdeporte, '0')) + to_number(nvl(ab.iva12, '0')) total_impuesto,
    ab.descpago formapago,
    fechaexp fecha_fact,
    'CARGA.CARGA200302' tabla,
    to_number(nvl(ab.tarifabasicamensual, 0)) + to_number(nvl(ab.tarifabasicamensual2, 0)) + to_number(nvl(ab.tarifabasicamensual3, 0)) + to_number(nvl(ab.tarifabasicasuspendido, 0)) tbasica,
    ab.cargopaquete,
    'n_factura' fact_field
from
    carga200302 ab
union all
select
    ac.cuenta,
    ac.NFACTURA num_fact,
    ac.cidentoruc cedula,
    ac.apellidos || ac.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ac.totalfact valor_factura,
    ac.iva12 iva,
    to_number(nvl(ac.impaguapotable, '0')) + to_number(nvl(ac.impdeporte, '0')) otros_impuesto,
    to_number(nvl(ac.impaguapotable, '0')) + to_number(nvl(ac.impdeporte, '0')) + to_number(nvl(ac.iva12, '0')) total_impuesto,
    ac.descpago formapago,
    fechaExp fecha_fact,
    'CARGA.CARGA200303' tabla,
    to_number(nvl(ac.tarifabasicamensual, 0)) + to_number(nvl(ac.tarifabasicamensual2, 0)) + to_number(nvl(ac.tarifabasicamensual3, 0)) + to_number(nvl(ac.tarifabasicasuspendido, 0)) tbasica,
    ac.cargopaquete,
    'n_factura' fact_field
from
    carga200303 ac
union all
select
    ad.cuenta,
    ad.NFACTURA num_fact,
    ad.cidentoruc cedula,
    ad.apellidos || ad.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ad.totalfact valor_factura,
    ad.iva12 iva,
    to_number(nvl(ad.impaguapotable, '0')) + to_number(nvl(ad.impdeporte, '0')) otros_impuesto,
    to_number(nvl(ad.impaguapotable, '0')) + to_number(nvl(ad.impdeporte, '0')) + to_number(nvl(ad.iva12, '0')) total_impuesto,
    ad.descpago formapago,
    fechaExp fecha_fact,
    'CARGA.CARGA200304' tabla,
    to_number(nvl(ad.tarifabasicamensual, 0)) + to_number(nvl(ad.tarifabasicamensual2, 0)) + to_number(nvl(ad.tarifabasicamensual3, 0)) + to_number(nvl(ad.tarifabasicasuspendido, 0)) tbasica,
    ad.cargopaquete,
    'n_factura' fact_field
from
    carga200304 ad
union all
select
    ae.cuenta,
    ae.NFACTURA num_fact,
    ae.cidentoruc cedula,
    ae.apellidos || ae.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ae.totalfact valor_factura,
    ae.iva12 iva,
    to_number(nvl(ae.impaguapotable, '0')) + to_number(nvl(ae.impdeporte, '0')) otros_impuesto,
    to_number(nvl(ae.impaguapotable, '0')) + to_number(nvl(ae.impdeporte, '0')) + to_number(nvl(ae.iva12, '0')) total_impuesto,
    ae.descpago formapago,
    fechaExp fecha_fact,
    'CARGA.CARGA200305' tabla,
    to_number(nvl(ae.tarifabasicamensual, 0)) + to_number(nvl(ae.tarifabasicamensual2, 0)) + to_number(nvl(ae.tarifabasicamensual3, 0)) + to_number(nvl(ae.tarifabasicasuspendido, 0)) tbasica,
    ae.cargopaquete,
    'n_factura' fact_field
from
    carga200305 ae
union all
select
    af.cuenta,
    af.NFACTURA num_fact,
    af.cidentoruc cedula,
    af.apellidos || af.nombres nombres,
    ltrim(rtrim(direccion1)) || ' ' || ltrim(rtrim(direccion2)) direccion,
    ltrim(rtrim(canton)) ciudad,
    af.totalfact valor_factura,
    af.iva12 iva,
    to_number(nvl(af.impaguapotable, '0')) + to_number(nvl(af.impdeporte, '0')) otros_impuesto,
    to_number(nvl(af.impaguapotable, '0')) + to_number(nvl(af.impdeporte, '0')) + to_number(nvl(af.iva12, '0')) total_impuesto,
    af.descpago formapago,
    fechaExp fecha_fact,
    'CARGA.CARGA200306' tabla,
    to_number(nvl(af.tarifabasicamensual, 0)) + to_number(nvl(af.tarifabasicamensual2, 0)) + to_number(nvl(af.tarifabasicamensual3, 0)) + to_number(nvl(af.tarifabasicasuspendido, 0)) tbasica,
    af.cargopaquete,
    'n_factura' fact_field
from
    carga200306 af
union all
select
    ag.cuenta,
    ag.FACTURA num_fact,
    ag.ruc_ced cedula,
    ag.apellidos || ag.nombres nombres,
    ltrim(rtrim(direccion)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ag.total valor_factura,
    ag.iva12 iva,
    to_number(nvl(ag.icedetelecomunicacion, '0')) otros_impuesto,
    to_number(nvl(ag.icedetelecomunicacion, '0')) + to_number(nvl(ag.iva12, '0')) total_impuesto,
    ag.forma_de_pago formapago,
    contacto1 fecha_fact,
    'CARGA.CARGA200307' tabla,
    to_number(nvl(ag.tarifabasicaamps, 0)) + to_number(nvl(ag.tarifabasicagsm, 0)) + to_number(nvl(ag.tarifabasicasuspendidaamps, 0)) + to_number(nvl(ag.tarifabasicasuspendidagsm, 0)) tbasica,
    ag.cargopaquete,
    'factura' fact_field
from
    carga200307 ag
union all
select
    ah.cuenta,
    ah.FACTURA num_fact,
    ah.ruc_ced cedula,
    ah.apellidos || ah.nombres nombres,
    ltrim(rtrim(direccion)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ah.total valor_factura,
    ah.iva12 iva,
    to_number(nvl(ah.icedetelecomunicacion, '0')) otros_impuesto,
    to_number(nvl(ah.icedetelecomunicacion, '0')) + to_number(nvl(ah.iva12, '0')) total_impuesto,
    ah.forma_de_pago formapago,
    contacto1 fecha_fact,
    'CARGA.CARGA200307b' tabla,
    to_number(nvl(ah.tarifabasicaamps, 0)) + to_number(nvl(ah.tarifabasicagsm, 0)) + to_number(nvl(ah.tarifabasicasuspendidaamps, 0)) + to_number(nvl(ah.tarifabasicasuspendidagsm, 0)) tbasica,
    ah.cargopaquete,
    'factura' fact_field
from
    carga200307b ah
union all
select
    ai.cuenta,
    ai.FACTURA num_fact,
    ai.ruc cedula,
    ai.apellidos || ai.nombres nombres,
    ltrim(rtrim(direccion)) direccion,
    ltrim(rtrim(canton)) ciudad,
    ai.total valor_factura,
    ai.iva12 iva,
    to_number(nvl(ai.icedetelecomunicacion15, '0')) otros_impuesto,
    to_number(nvl(ai.icedetelecomunicacion15, '0')) + to_number(nvl(ai.iva12, '0')) total_impuesto,
    ai.forma_pago formapago,
    cont1 fecha_fact,
    'CARGA.CARGA200308' tabla,
    to_number(nvl(ai.tarifabasicaamps, 0)) + to_number(nvl(ai.tarifabasicagsm, 0)) + to_number(nvl(ai.tarifabasicasuspendidaamps, 0)) + to_number(nvl(ai.tarifabasicasuspendidagsm, 0)) tbasica,
    ai.cargopaquete,
    'factura' fact_field
from
    carga200308 ai
union all
select
    aj.cuenta,
    aj.FACTURA num_fact,
    aj.Ruc cedula,
    aj.apellidos || aj.nombres nombres,
    ltrim(rtrim(direccion)) direccion,
    ltrim(rtrim(canton)) ciudad,
    aj.total valor_factura,
    aj.iva12 iva,
    to_number(nvl(aj.icedetelecomunicacion15, '0')) otros_impuesto,
    to_number(nvl(aj.icedetelecomunicacion15, '0')) + to_number(nvl(aj.iva12, '0')) total_impuesto,
    aj.forma_pago formapago,
    cont1 fecha_fact,
    'CARGA.CARGA200308b' tabla,
    to_number(nvl(aj.tarifabasicaamps, 0)) + to_number(nvl(aj.tarifabasicagsm, 0)) + to_number(nvl(aj.tarifabasicasuspendidaamps, 0)) + to_number(nvl(aj.tarifabasicasuspendidagsm, 0)) tbasica,
    aj.cargopaquete,
    'factura' fact_field
from
    carga200308b aj;