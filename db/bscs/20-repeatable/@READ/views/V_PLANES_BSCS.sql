CREATE OR REPLACE VIEW V_PLANES_BSCS AS 
select
    b.tmcode,
    b.des "PLAN",
    c.des "TIPOTARIFA",
    a.accessfee "TARIFA"
from
    sysadm.mpulktm1 a,
    sysadm.rateplan b,
    sysadm.mpusntab c
where
    a.sncode in (1, 2, 5, 6)
    and a.tmcode = b.tmcode
    and a.sncode = c.sncode;