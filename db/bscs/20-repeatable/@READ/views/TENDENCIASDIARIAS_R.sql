CREATE OR REPLACE VIEW TENDENCIASDIARIAS_R AS 
select
    trunc(a.initial_start_time_timestamp) Fecha,
    count(*) Llamadas
    /*+ FULL(UDR_LT_20040910) PARALLEL(UDR_LT_20040910,,6) */
from
    udr_lt_20041011 @bscs_to_rtx_link a
where
    xfile_ind = 'V'
group by
    trunc(a.initial_start_time_timestamp)