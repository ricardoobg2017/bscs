CREATE OR REPLACE VIEW QCREPORTE AS 
select  /*+ rule */
  d.customer_id, d.custcode, c.ohrefnum, b.sncode,a.otglsale,b.des,
  sum(otamt_revenue_gl) as Valor
from 
  ordertrailer  A, --items de la factura
  mpusntab      b, --maestro de servicios
  orderhdr_all  c, --cabecera de facturas
  customer_all  d--, -- maestro de cliente
  --COB_SERVICIOS h, --formato de reporte
  --COB_GRUPOS    I --NOMBRE GRUPO
where
  a.otxact          = c.ohxact and 
  c.ohentdate=to_date('24/01/2004','DD/MM/YYYY') and
  c.customer_id     = d.customer_id and 
  substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
  instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
  instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode 
group by d.customer_id,  d.custcode,  c.ohrefnum,  b.sncode,a.otglsale,b.des;

