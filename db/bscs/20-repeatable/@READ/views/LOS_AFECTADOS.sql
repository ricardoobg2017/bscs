CREATE
OR REPLACE VIEW LOS_AFECTADOS AS
select
   "CUSTOMER_ID",
   "SEQNO",
   "FEE_TYPE",
   "AMOUNT",
   "REMARK",
   "GLCODE",
   "ENTDATE",
   "PERIOD",
   "USERNAME",
   "VALID_FROM",
   "JOBCOST",
   "BILL_FMT",
   "SERVCAT_CODE",
   "SERV_CODE",
   "SERV_TYPE",
   "CO_ID",
   "AMOUNT_GROSS",
   "CURRENCY",
   "GLCODE_DISC",
   "JOBCOST_ID_DISC",
   "GLCODE_MINCOM",
   "JOBCOST_ID_MINCOM",
   "REC_VERSION",
   "CDR_ID",
   "CDR_SUB_ID",
   "UDR_BASEPART_ID",
   "UDR_CHARGEPART_ID",
   "TMCODE",
   "VSCODE",
   "SPCODE",
   "SNCODE",
   "EVCODE",
   "FEE_CLASS",
   "FU_PACK_ID",
   "FUP_VERSION",
   "FUP_SEQ",
   "VERSION",
   "FREE_UNITS_NUMBER"
from
    fees a
where
    fee_type = 'R'
    and sncode = 103
    and customer_id in (
        select
            customer_id
        from
            fees
        where
            period <> 0
            and fee_type = 'R'
            and sncode = 103
        group by
            customer_id
        having
            count (*) > 1
    )
order by
    customer_id,
    seqno