CREATE OR REPLACE VIEW CONTRATOSPLANPOOL AS
select
    a.custcode,
    a.customer_id,
    b.co_id,
    c.ch_status,
    c.entdate,
    c.ch_validfrom
from
    customer_all a,
    contract_all b,
    curr_co_status c
where
    a.customer_id = b.customer_id
    and b.co_id = c.co_id
    and c.ch_status = 'a'
    and b.tmcode in (
        select
            tmcode
        from
            rateplan
        where
            des like '%MAESTRO%'
    );