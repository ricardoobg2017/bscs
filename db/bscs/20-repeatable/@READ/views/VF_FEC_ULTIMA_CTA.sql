CREATE OR REPLACE VIEW VF_FEC_ULTIMA_CTA AS 
select
    t.co_rep,
    max(c_all.csactivated) fec_Max_Cta_Previa
from
    sysadm.contract_all t,
    sysadm.customer_all c_all
where
    length(trim(t.co_rep)) > 0
    and t.customer_id = c_all.customer_id
group by
    t.co_rep;