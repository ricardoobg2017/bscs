CREATE OR REPLACE VIEW VF_CAMBIOSPLANES_DE_JULIO AS 
select
    c_all.custcode cuenta,
    c_all.cstype st_cuenta,
    decode(
        c_all.cstype,
        null,
        'Ninguno',
        'a',
        'Cuenta Activa',
        'd',
        'Cuenta Inactiva',
        'i',
        'Cuenta Cli Interesado',
        's',
        'Cuenta Suspendida',
        'g',
        'Cuenta Gestprrt',
        'b',
        'Cuenta Beantragt',
        'Otro'
    ) Desc_Estado_Cuenta,
    c_all.cssocialsecno CED_RUC,
    t.co_rep linea,
    rp_h.tmcode id_plan_linea,
    rp.des nombre_plan_linea,
    rp_h.tmcode_date Fec_ini_plan_linea
from
    sysadm.contract_all t,
    sysadm.customer_all c_all,
    sysadm.rateplan_hist rp_h,
    sysadm.rateplan rp
where
    length(trim(t.co_rep)) > 0
    and t.customer_id = c_all.customer_id
    and t.co_id = rp_h.co_id
    and rp_h.tmcode = rp.tmcode
    and rp_h.tmcode_date >= to_date('24/07/2003 00:00:00', 'dd/mm/yyyy hh24:mi:ss');