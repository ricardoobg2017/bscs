CREATE OR REPLACE VIEW VF_ULTIMO_PLAN_DE_LINEA  AS 
select
    a.co_rep,
    a.tmcode,
    b.des,
    d.id_plan,
    a.co_activated Ul_Fe_st_Linea_Activa,
    a.tmcode_date Fec_Ini_plan
from
    contract_all a,
    rateplan b,
    cl_servicios_contratados @axis c,
    ge_detalles_planes @axis d
where
    -- a.co_rep = '59399420007' and 
    b.tmcode = a.tmcode
    and c.id_servicio = substr(a.co_rep, 5, 7)
    and c.estado = 'A'
    and d.id_detalle_plan = c.id_detalle_plan
    and (a.co_rep, a.co_activated) in (
        select
            a.co_rep,
            max(a.co_activated) fecha_ac_max
        from
            contract_all a
        group by
            a.co_rep
    );