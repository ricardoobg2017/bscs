/* Formatted on 16/12/2013 18:28:22 (QP5 v5.256.13226.35510) */
CREATE OR REPLACE FORCE VIEW ENE2008
(
   CUSTOMER_ID,
   CUSTCODE,
   OHREFNUM,
   CCCITY,
   CCSTATE,
   BANK_ID,
   PRODUCTO,
   COST_DESC,
   CBLE,
   VALOR,
   DESCUENTO,
   TIPO,
   NOMBRE,
   NOMBRE2,
   SERVICIO,
   CTACTBLE,
   CTACLBLEP
)
AS
     SELECT /*+ rule */
           d.customer_id,
            d.custcode,
            c.ohrefnum,
            f.cccity,
            f.ccstate,
            g.bank_id,
            I.PRODUCTO,
            j.cost_desc,
            a.otglsale AS Cble,
            SUM (NVL (otamt_revenue_gl, 0)) AS Valor,
            SUM (NVL (A.OTAMT_DISC_DOC, 0)) AS Descuento,
            h.tipo,
            h.nombre,
            h.nombre2,
            H.SERVICIO,
            H.CTACTBLE,
            h.ctapsoft CTACLBLEP
       FROM ordertrailer A,
            mpusntab b,
            orderhdr_all c,
            customer_all d,
            ccontact_all f,
            payment_all g,
            COB_SERVICIOS h,
            COB_GRUPOS I,
            COSTCENTER j
      WHERE     a.otxact = c.ohxact
            AND c.ohentdate = TO_DATE ('24/01/2008', 'DD/MM/YYYY') ----ojo con la fecha
            AND C.OHSTATUS IN ('IN', 'CM')
            AND C.OHUSERID IS NULL
            AND c.customer_id = d.customer_id
            AND SUBSTR (
                   otname,
                     INSTR (
                        otname,
                        '.',
                        INSTR (otname, '.', INSTR (otname, '.', 1) + 1) + 1)
                   + 1,
                     INSTR (
                        otname,
                        '.',
                          INSTR (
                             otname,
                             '.',
                               INSTR (otname, '.', INSTR (otname, '.', 1) + 1)
                             + 1)
                        + 1)
                   - INSTR (
                        otname,
                        '.',
                        INSTR (otname, '.', INSTR (otname, '.', 1) + 1) + 1)
                   - 1) = b.sncode
            AND d.customer_id = f.customer_id
            AND f.ccbill = 'X'
            AND g.customer_id = d.customer_id
            AND act_used = 'X'
            AND h.servicio = b.sncode
            AND h.ctactble = a.otglsale
            AND D.PRGCODE = I.PRGCODE
            AND j.cost_id = d.costcenter_id
            AND A.servcat_code NOT LIKE 'IVA-IC%'
   GROUP BY d.customer_id,
            d.custcode,
            c.ohrefnum,
            f.cccity,
            f.ccstate,
            g.bank_id,
            I.PRODUCTO,
            j.cost_desc,
            a.otglsale,
            a.otxact,
            h.tipo,
            h.nombre,
            h.nombre2,
            H.SERVICIO,
            H.CTACTBLE,
            h.ctapsoft
   UNION
     SELECT /*+ rule */
           d.customer_id,
            d.custcode,
            c.ohrefnum,
            f.cccity,
            f.ccstate,
            g.bank_id,
            I.PRODUCTO,
            j.cost_desc,
            A.glacode AS Cble,
            SUM (TAXAMT_TAX_CURR) AS VALOR,
            0 AS Descuento,
            h.tipo,
            h.nombre,
            h.nombre2,
            H.SERVICIO,
            H.CTACTBLE,
            h.ctapsoft CTACLBLEP
       FROM ORDERTAX_ITEMS A,
            TAX_CATEGORY B,
            orderhdr_all C,
            customer_all d,
            ccontact_all f,
            payment_all g,
            COB_SERVICIOS h,
            COB_GRUPOS I,
            COSTCENTER J
      WHERE     c.ohxact = a.otxact
            AND c.ohentdate = TO_DATE ('24/01/2008', 'DD/MM/YYYY')
            AND C.OHSTATUS IN ('IN', 'CM')
            AND C.OHUSERID IS NULL
            AND c.customer_id = d.customer_id
            AND taxamt_tax_curr > 0
            AND A.TAXCAT_ID = B.TAXCAT_ID
            AND d.customer_id = f.customer_id
            AND f.ccbill = 'X'
            AND g.customer_id = d.customer_id
            AND act_used = 'X'
            AND h.servicio = A.TAXCAT_ID
            AND h.ctactble = A.glacode
            AND D.PRGCODE = I.PRGCODE
            AND j.cost_id = d.costcenter_id
   GROUP BY d.customer_id,
            d.custcode,
            c.ohrefnum,
            f.cccity,
            f.ccstate,
            g.bank_id,
            I.PRODUCTO,
            j.cost_desc,
            A.glacode,
            h.tipo,
            h.nombre,
            h.nombre2,
            H.SERVICIO,
            H.CTACTBLE,
            h.ctapsoft;
