CREATE OR REPLACE VIEW DIC2007 AS select
    /*+ rule */
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    f.cccity,
    f.ccstate,
    g.bank_id,
    I.PRODUCTO,
    j.cost_desc,
    a.otglsale as Cble,
    sum(NVL(otamt_revenue_gl, 0)) as Valor,
    sum(NVL(A.OTAMT_DISC_DOC, 0)) AS Descuento,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE,
    h.ctapsoft CTACLBLEP
from
    ordertrailer A,
    mpusntab b,
    orderhdr_all c,
    customer_all d,
    ccontact_all f,
    payment_all g,
    COB_SERVICIOS h,
    COB_GRUPOS I,
    COSTCENTER j
where
    a.otxact = c.ohxact
    and c.ohentdate = to_date('24/12/2007', 'DD/MM/YYYY') ---- ojo con la fecha
    and C.OHSTATUS IN ('IN', 'CM')
    AND C.OHUSERID IS NULL
    AND c.customer_id = d.customer_id
    and substr(
        otname,
        instr(
            otname,
            '.',
            instr(otname, '.', instr(otname, '.', 1) + 1) + 1
        ) + 1,
        instr(
            otname,
            '.',
            instr(
                otname,
                '.',
                instr(otname, '.', instr(otname, '.', 1) + 1) + 1
            ) + 1
        ) - instr(
            otname,
            '.',
            instr(otname, '.', instr(otname, '.', 1) + 1) + 1
        ) -1
    ) = b.sncode
    and d.customer_id = f.customer_id
    and f.ccbill = 'X'
    and g.customer_id = d.customer_id
    and act_used = 'X'
    and h.servicio = b.sncode
    and h.ctactble = a.otglsale
    AND D.PRGCODE = I.PRGCODE
    and j.cost_id = d.costcenter_id
group by
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    f.cccity,
    f.ccstate,
    g.bank_id,
    I.PRODUCTO,
    j.cost_desc,
    a.otglsale,
    a.otxact,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE,
    h.ctapsoft
UNION
SELECT
    /*+ rule */
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    f.cccity,
    f.ccstate,
    g.bank_id,
    I.PRODUCTO,
    j.cost_desc,
    A.glacode as Cble,
    sum(TAXAMT_TAX_CURR) AS VALOR,
    0 as Descuento,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE,
    h.ctapsoft CTACLBLEP
FROM
    ORDERTAX_ITEMS A,
    TAX_CATEGORY B,
    orderhdr_all C,
    customer_all d,
    ccontact_all f,
    payment_all g,
    COB_SERVICIOS h,
    COB_GRUPOS I,
    COSTCENTER J
where
    c.ohxact = a.otxact
    and c.ohentdate = to_date('24/12/2007', 'DD/MM/YYYY')
    and C.OHSTATUS IN ('IN', 'CM')
    AND C.OHUSERID IS NULL
    AND c.customer_id = d.customer_id
    and taxamt_tax_curr > 0
    and A.TAXCAT_ID = B.TAXCAT_ID
    and d.customer_id = f.customer_id
    and f.ccbill = 'X'
    and g.customer_id = d.customer_id
    and act_used = 'X'
    and h.servicio = A.TAXCAT_ID
    and h.ctactble = A.glacode
    AND D.PRGCODE = I.PRGCODE
    and j.cost_id = d.costcenter_id
group by
    d.customer_id,
    d.custcode,
    c.ohrefnum,
    f.cccity,
    f.ccstate,
    g.bank_id,
    I.PRODUCTO,
    j.cost_desc,
    A.glacode,
    h.tipo,
    h.nombre,
    h.nombre2,
    H.SERVICIO,
    H.CTACTBLE,
    h.ctapsoft;