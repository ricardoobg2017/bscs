CREATE OR REPLACE VIEW VF_PLANES_PREVIOS AS 
select
    c_all.custcode cuenta,
    c_all.cstype st_cuenta,
    decode(
        c_all.cstype,
        null,
        'Ninguno',
        'a',
        'Cuenta Activa',
        'd',
        'Cuenta Inactiva',
        'i',
        'Cuenta Cli Interesado',
        's',
        'Cuenta Suspendida',
        'g',
        'Cuenta Gestprrt',
        'b',
        'Cuenta Beantragt',
        'Otro'
    ) Desc_Estado_Cuenta,
    c_all.cssocialsecno CED_RUC,
    t.co_rep linea,
    rp_h.tmcode id_plan_linea,
    rp.des nombre_plan_linea,
    max(rp_h.tmcode_date) Fec_plan_previo
from
    sysadm.contract_all t,
    sysadm.customer_all c_all,
    sysadm.rateplan_hist rp_h,
    sysadm.rateplan rp
where
    length(trim(t.co_rep)) > 0
    and t.customer_id = c_all.customer_id
    and t.co_id = rp_h.co_id
    and rp_h.tmcode = rp.tmcode
    and rp_h.tmcode_date >= to_date('24/12/2003 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
    and rp_h.tmcode_date < to_date('12/01/2004 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
    and t.co_rep in (
        select
            distinct linea
        from
            VF_CambiosPlanes t
    )
    and (t.co_rep, c_all.custcode, c_all.csactivated) in (
        select
            p.co_rep,
            p.custcode,
            p.csactivated
        from
            vf_datos_cta_previa p
    )
group by
    c_all.custcode,
    c_all.cstype,
    c_all.cssocialsecno,
    t.co_rep,
    rp_h.tmcode,
    rp.des;