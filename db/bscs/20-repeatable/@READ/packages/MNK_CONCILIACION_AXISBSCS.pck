CREATE OR REPLACE package MNK_CONCILIACION_AXISBSCS is
  --===================================================
  -- Proyecto: Conciliaci�n de AXISvsBSCS
  -- Autores:    Alan Pab� R.
  -- Fecha:    21/Ago/2003
  -- Version:  1.0
  --===================================================
-- 1.2.1	Cuentas que tienen fecha de activaci�n diferentes entre AXIS y BSCS
-- 1.3.1	Cuentas activas en AXIS y se encuentran inactivas en BSCS
-- 1.3.2	Cuentas activas en BSCS y se encuentran inactivas en AXIS
-- 1.4.1	Cuentas activas en AXIS y no existen en BSCS
-- 1.4.2	Cuentas activas en BSCS y no existen en AXIS
-- 2.1.1	Tel�fonos que tienen fecha de activaci�n diferentes entre AXIS y BSCS
-- 2.2.1	Tel�fonos activos en AXIS y se encuentran inactivas en BSCS
-- 2.2.2	Tel�fonos activos en BSCS y se encuentran inactivas en AXIS
-- 2.3.1	Tel�fonos activos en AXIS y no existen en BSCS
-- 2.3.2	Tel�fonos activos en BSCS y no existen en AXIS
--
--============================== CUENTAS ================================--
--  
  procedure MNP_CTA_ACT_FECH_DIF(p_fecha in varchar2,
  /*1.2.1.*/                     p_error out varchar2);
--
  procedure MNP_CTA_INACTBS_ACTAX(p_fecha in varchar2,
 /*1.3.1.*/                       p_error out varchar2);
--
 procedure MNP_CTA_ACTBS_INACTAX(p_fecha in varchar2,
 /*1.3.2.*/                      p_error out varchar2);
--
 procedure MNP_CTA_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*1.4.1.*/                       p_error out varchar2);
--
 procedure MNP_CTA_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*1.4.2.*/                       p_error out varchar2);
--
--============================== TELEFONOS ================================--
--
 procedure MNP_TELF_ACT_FECH_DIF(--p_telefono in varchar2,
                                 p_fecha in varchar2,
 /*2.1.1.*/                      p_error out varchar2);
--
 procedure MNP_TELF_INACTBS_ACTAX(p_fecha in varchar2,
 /*2.2.1.*/                       p_error out varchar2);
--
 procedure MNP_TELF_ACTBS_INACTAX(p_fecha in varchar2,
 /*2.2.2.*/                       p_error out varchar2);
--
 procedure MNP_TELF_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*2.3.1.*/                        p_error out varchar2);
--
 procedure MNP_TELF_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*2.3.2.*/                        p_error out varchar2);
--
end MNK_CONCILIACION_AXISBSCS;
/

