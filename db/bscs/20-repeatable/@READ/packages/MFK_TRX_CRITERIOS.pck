CREATE OR REPLACE PACKAGE read.MFK_TRX_CRITERIOS is

  TYPE Gtr_Forma_Pago IS RECORD(CODE varchar2(10),
                                NAMES varchar2(70),
                                grupo varchar2(60));
  TYPE Gr_Forma_Pago IS REF CURSOR RETURN Gtr_Forma_Pago;
  TYPE Gt_Forma_Pago IS TABLE OF Gtr_Forma_Pago INDEX BY BINARY_INTEGER;
  
  TYPE Gtr_Planes IS RECORD(CODE varchar2(10),
                            NAMES varchar2(70),
                            grupo varchar2(60));
  TYPE Gr_Planes IS REF CURSOR RETURN Gtr_Planes;
  TYPE Gt_Planes IS TABLE OF Gtr_Planes INDEX BY BINARY_INTEGER;
  
  
FUNCTION MFF_CRITERIOS (PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                        PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                        PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                        PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                        PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                        PN_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                        PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;

FUNCTION MFF_TIPOCLIENTE (PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                          PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                          PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                          PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                          PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                          PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                          PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                          PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                          PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;

FUNCTION MFF_FORMAPAGO(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                       PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                       PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                       PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                       PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                       PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                       PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                       PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;

FUNCTION MFF_FORMAPAGOESPECIFICO(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                                 PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                                 PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                                 PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                                 PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                                 PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                                 PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                 PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                                 PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;

FUNCTION MFF_TIPOPLAN(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                      PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                      PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                      PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                      PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                      PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                      PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                      PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                      PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;


 FUNCTION MFF_EDADMORA(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                        PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                        PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                        PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                        PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                        PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                        PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                        PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                        PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2;



PROCEDURE MFP_FORMA_PAGO(Pt_Forma_Pago  IN OUT Gt_Forma_Pago); 


PROCEDURE MFP_PLANES(Pt_Planes  IN OUT Gt_Planes);


END MFK_TRX_CRITERIOS;
/
create or replace package body read.MFK_TRX_CRITERIOS is


FUNCTION MFF_CRITERIOS (PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                        PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                        PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                        PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                        PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                        PN_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                        PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS

   CURSOR c_criterios(cn_idEnvio  NUMBER) is
      SELECT DISTINCT ce.idENVIO,ce.idcriterio,c.descripcion,c.contenido,c.funcion,ce.efecto
      FROM mf_criterios_x_envios ce, mf_criterios c
      WHERE ce.idcriterio = c.idcriterio
      AND ce.idenvio = cn_idEnvio
      AND ce.estado = 'A'
      ORDER BY ce.idcriterio;

   lc_criterios           c_criterios%rowtype;
   lb_found               boolean;
   lv_functionStatement   VARCHAR2(500);
   lv_retorno             VARCHAR2(1) := '0';

begin

     --insert_kav_1(PV_CUSTCODE,PV_EDAD_MORA,sysdate);
      --Recupera Criterios para la Consulta
      open c_criterios(pn_idEnvio);
      fetch c_criterios into lc_criterios;
      lb_found := c_criterios%FOUND;
      IF lb_found THEN
         WHILE(c_criterios%FOUND) LOOP
            lv_functionStatement := lc_criterios.funcion ||'('||
                                                           PN_CUSTOMERID            ||','''|| --JRC(')
                                                           PV_CUSTCODE              ||''','|| --JRC(')
                                                           PN_IDENVIO               ||','''|| --JRC(')
                                                           PV_PRGCODE               ||''','|| --JRC(')
                                                           PN_REGION                ||','||
                                                           NVL(PN_CREDIT_RATING,0)  ||','||
                                                           lc_criterios.idcriterio  ||','''||
                                                           lc_criterios.efecto      ||''','''||
                                                           PV_EDAD_MORA             ||''')';
             EXECUTE IMMEDIATE 'BEGIN :1 := '||lv_functionStatement||'; END;'
             USING OUT lv_retorno;

             IF  lv_retorno = '0'  THEN
                EXIT;
             END IF;

             FETCH c_criterios INTO lc_criterios;
         END LOOP;
      ELSE
           --Significa que no tiene Filtros el Envio
--           lv_retorno := '1';
             lv_retorno:='0'; ---si la trx de envio no tiene criterios entonces no es válido.
      END IF;
      CLOSE c_criterios;
      RETURN lv_retorno;

END MFF_CRITERIOS;


FUNCTION MFF_TIPOCLIENTE (PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                          PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                          PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                          PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                          PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                          PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                          PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                          PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                          PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS

  CURSOR C_VALIDA(cn_idEnvio     NUMBER,
                  cv_prgCode     VARCHAR2,
                  cn_idCriterio  NUMBER,
                  cv_efecto      VARCHAR2) IS
     SELECT TX.EFECTO
     FROM mf_criterios_x_envios TX
     WHERE TX.IDENVIO = cn_idEnvio
     AND TX.IDCRITERIO = cn_idCriterio
     AND TX.ESTADO = 'A'
     AND TX.EFECTO = cv_efecto
     AND TX.VALOR  = cv_prgCode;

    LV_RETORNO   VARCHAR2(1) := '0';
    lv_efecto    mf_criterios_x_envios.efecto%TYPE;
    lb_found     BOOLEAN;

BEGIN
      OPEN c_valida(PN_IDENVIO,PV_PRGCODE,PN_IDCRITERIO,PV_EFECTO);
      FETCH c_valida INTO lv_efecto;
      lb_found := c_valida%FOUND;
      CLOSE c_valida;
      IF pv_efecto = 'A' THEN  --'A' significa q es un criterio de aceptacion
         IF lb_found THEN
            lv_retorno := '1';
         ELSE
            lv_retorno := '0';
         END IF;
      ELSIF pv_efecto = 'E'  THEN -- 'E' significa q es un criterio de Exceptuación
         IF lb_found THEN
            lv_retorno := '0';
         ELSE
            lv_retorno := '1';
         END IF;
      END IF;
      RETURN LV_RETORNO;

end MFF_TIPOCLIENTE;


FUNCTION MFF_FORMAPAGO(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                       PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                       PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                       PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                       PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                       PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                       PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                       PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS


   CURSOR C_FORMAPAGO(Cn_customerId NUMBER) IS
       SELECT TP.RPT_PAYMENT_ID
       FROM  CUSTOMER_ALL C,PAYMENT_ALL FP,OPE_RPT_PAYMENT_TYPE TP
       WHERE c.customer_id = Cn_customerId
       AND   C.CUSTOMER_ID = FP.CUSTOMER_ID
       AND   FP.BANK_ID = TP.RPT_COD_BSCS
       AND   FP.act_used = 'X';

   CURSOR C_VALIDA(cn_idEnvio       NUMBER,
                   cn_rptPaymentId  NUMBER,
                   cn_idCriterio    NUMBER,
                   cv_efecto        VARCHAR2) IS
     SELECT TX.EFECTO
     FROM mf_criterios_x_envios TX
     WHERE TX.IDENVIO = cn_idEnvio
     AND TX.IDCRITERIO = cn_idCriterio
     AND TX.ESTADO = 'A'
     AND TX.EFECTO = cv_efecto
     AND TX.VALOR  = cn_rptPaymentId;

       LV_RETORNO   VARCHAR2(1) := '0';
       ln_rptPaymentId   OPE_RPT_PAYMENT_TYPE.RPT_PAYMENT_ID%TYPE;
       lv_efecto    mf_criterios_x_envios.efecto%TYPE;
       lb_found     BOOLEAN;


BEGIN
      OPEN c_formaPago(PN_CUSTOMERID);
      FETCH c_formaPago INTO ln_rptPaymentId;
      CLOSE c_formaPago;

      OPEN c_valida(PN_IDENVIO,ln_rptPaymentId,PN_IDCRITERIO,PV_EFECTO);
      FETCH c_valida INTO lv_efecto;
      lb_found := c_valida%FOUND;
      CLOSE c_valida;
      IF pv_efecto = 'A' THEN  --'A' significa q es un criterio de aceptacion
         IF lb_found THEN
            lv_retorno := '1';
         ELSE
            lv_retorno := '0';
         END IF;
      ELSIF pv_efecto = 'E'  THEN -- 'E' significa q es un criterio de Exceptuación
         IF lb_found THEN
            lv_retorno := '0';
         ELSE
            lv_retorno := '1';
         END IF;
      END IF;
      RETURN LV_RETORNO;

END MFF_FORMAPAGO;



FUNCTION MFF_FORMAPAGOESPECIFICO(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                                 PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                                 PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                                 PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                                 PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                                 PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                                 PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                 PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                                 PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS

   CURSOR C_FORMAPAGO(Cn_customerId NUMBER) IS
       SELECT TP.RPT_COD_BSCS
        FROM  CUSTOMER_ALL C,PAYMENT_ALL FP,OPE_RPT_PAYMENT_TYPE TP
        WHERE c.customer_id = Cn_customerId
        AND   C.CUSTOMER_ID = FP.CUSTOMER_ID
        AND   FP.BANK_ID = TP.RPT_COD_BSCS
        AND   FP.act_used = 'X';

   CURSOR C_VALIDA(cn_idEnvio       NUMBER,
                   cn_rptCodBSCS    NUMBER,
                   cn_idCriterio    NUMBER,
                   cv_efecto        VARCHAR2) IS
     SELECT TX.EFECTO
     FROM mf_criterios_x_envios TX
     WHERE TX.IDENVIO = cn_idEnvio
     AND TX.IDCRITERIO = cn_idCriterio
     AND TX.ESTADO = 'A'
     AND TX.EFECTO = cv_efecto
     AND TX.VALOR  = cn_rptCodBSCS;

        LV_RETORNO     VARCHAR2(1) := '0';
        ln_rptCodBSCS  OPE_RPT_PAYMENT_TYPE.RPT_COD_BSCS%TYPE;
        lv_efecto    mf_criterios_x_envios.efecto%TYPE;
        lb_found     BOOLEAN;


BEGIN
      OPEN c_formaPago(PN_CUSTOMERID);
      FETCH c_formaPago INTO ln_rptCodBSCS;
      CLOSE c_formaPago;

      OPEN c_valida(PN_IDENVIO,ln_rptCodBSCS,PN_IDCRITERIO,PV_EFECTO);
      FETCH c_valida INTO lv_efecto;
      lb_found := c_valida%FOUND;
      CLOSE c_valida;
      IF pv_efecto = 'A' THEN  --'A' significa q es un criterio de aceptacion
         IF lb_found THEN
            lv_retorno := '1';
         ELSE
            lv_retorno := '0';
         END IF;
      ELSIF pv_efecto = 'E'  THEN -- 'E' significa q es un criterio de Exceptuación
         IF lb_found THEN
            lv_retorno := '0';
         ELSE
            lv_retorno := '1';
         END IF;
      END IF;
      RETURN LV_RETORNO;

END MFF_FORMAPAGOESPECIFICO;


FUNCTION MFF_TIPOPLAN(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                      PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                      PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                      PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                      PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                      PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                      PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                      PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                      PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS


   CURSOR c_plan(cn_customerId NUMBER) IS
       SELECT rp.tmcode
       FROM contract_all ca,rateplan rp,curr_co_status ccs
       WHERE ca.customer_id = ANY (SELECT customer_id
                                   FROM customer_all
                                   CONNECT BY PRIOR customer_id = customer_id_high
                                   START WITH customer_id= cn_customerId)
       AND ca.tmcode = rp.tmcode
       AND ca.co_id = ccs.CO_ID
       AND ccs.CH_STATUS = 'a';

   CURSOR C_VALIDA(cn_idEnvio       NUMBER,
                   cn_tmcode        NUMBER,
                   cn_idCriterio    NUMBER,
                   cv_efecto        VARCHAR2) IS
     SELECT TX.EFECTO
     FROM mf_criterios_x_envios TX
     WHERE TX.IDENVIO = cn_idEnvio
     AND TX.IDCRITERIO = cn_idCriterio
     AND TX.ESTADO = 'A'
     AND TX.EFECTO = cv_efecto
     AND TX.VALOR  = cn_tmcode;

   lv_efecto    mf_criterios_x_envios.efecto%TYPE;
   lb_found     BOOLEAN;
   LV_RETORNO   VARCHAR2(1) := '0';
   ln_tmcode  rateplan.tmcode%TYPE;

BEGIN

     OPEN c_plan(PN_CUSTOMERID);   --returning many records
     FETCH c_plan INTO ln_tmcode;
     CLOSE c_plan;

     OPEN c_valida(PN_IDENVIO,ln_tmcode,PN_IDCRITERIO,PV_EFECTO);
     FETCH c_valida INTO lv_efecto;
     lb_found := c_valida%FOUND;
     CLOSE c_valida;
     IF pv_efecto = 'A' THEN  --'A' significa q es un criterio de aceptacion
         IF lb_found THEN
            lv_retorno := '1';
         ELSE
            lv_retorno := '0';
         END IF;
      ELSIF pv_efecto = 'E'  THEN -- 'E' significa q es un criterio de Exceptuación
         IF lb_found THEN
            lv_retorno := '0';
         ELSE
            lv_retorno := '1';
         END IF;
      END IF;
     RETURN LV_RETORNO;

END MFF_TIPOPLAN;


 FUNCTION MFF_EDADMORA(PN_CUSTOMERID       IN CUSTOMER_ALL.CUSTOMER_ID%TYPE,
                        PV_CUSTCODE         IN CUSTOMER_ALL.CUSTCODE%TYPE,
                        PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                        PV_PRGCODE          IN CUSTOMER_ALL.PRGCODE%TYPE,
                        PN_REGION           IN MF_ENVIOS.REGION%TYPE,
                        PV_CREDIT_RATING    IN CUSTOMER_ALL.CREDIT_RATING%TYPE,
                        PN_IDCRITERIO       IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                        PV_EFECTO           IN MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                        PV_EDAD_MORA        IN VARCHAR2) RETURN VARCHAR2 IS

    CURSOR C_VALIDA(cn_idEnvio        NUMBER,
                    cv_edadMora       VARCHAR2,
                    cn_idCriterio     NUMBER,
                    cv_efecto         VARCHAR2) IS
     SELECT TX.EFECTO
     FROM mf_criterios_x_envios TX
     WHERE TX.IDENVIO = cn_idEnvio
     AND TX.IDCRITERIO = cn_idCriterio
     AND TX.ESTADO = 'A'
     AND TX.EFECTO = cv_efecto
     AND TX.VALOR  = cv_edadMora;

    lv_efecto    mf_criterios_x_envios.efecto%TYPE;
    lb_found     BOOLEAN;
    LV_RETORNO   VARCHAR2(1) := '0';

  BEGIN
       IF PV_EDAD_MORA IS NOT NULL THEN
           OPEN c_valida(PN_IDENVIO,PV_EDAD_MORA,PN_IDCRITERIO,PV_EFECTO);
           FETCH c_valida INTO lv_efecto;
           lb_found := c_valida%FOUND;
           CLOSE c_valida;
           IF pv_efecto = 'A' THEN  --'A' significa q es un criterio de aceptacion
              IF lb_found THEN
                    lv_retorno := '1';
              ELSE
                    lv_retorno := '0';
              END IF;
           ELSIF pv_efecto = 'E'  THEN -- 'E' significa q es un criterio de Exceptuación
               IF lb_found THEN
                 lv_retorno := '0';
               ELSE
                 lv_retorno := '1';
               END IF;
           END IF;
       ELSE
           LV_RETORNO := '1';
       END IF;
       RETURN LV_RETORNO;

  END MFF_EDADMORA;
---


----CARGA DE SUBCRITERIO DESDE TABLAS-----
PROCEDURE MFP_FORMA_PAGO(Pt_Forma_Pago  IN OUT Gt_Forma_Pago)AS
CURSOR C_forma_pago IS 
    SELECT idbanco code, nombre_banco names,nombre_grupo grupo 
      FROM sysadm.mf_formas_pagos 
  ORDER BY 3, 1;

  Ln_Contador NUMBER;

BEGIN
   Ln_Contador := 1;
   FOR i IN C_forma_pago LOOP     
    Pt_Forma_Pago(Ln_Contador).CODE := i.Code;
    Pt_Forma_Pago(Ln_Contador).NAMES := i.Names;
    Pt_Forma_Pago(Ln_Contador).grupo := i.grupo;      
    Ln_Contador := Ln_Contador + 1;      
   END LOOP;      
        
  BEGIN
    DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
 EXCEPTION
 WHEN OTHERS THEN
  BEGIN
    DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
  EXCEPTION
    WHEN OTHERS THEN
    NULL;
  END;
  Pt_Forma_Pago(1).CODE := '0';
  Pt_Forma_Pago(1).NAMES := 'SIN DATOS';
END;   
--


PROCEDURE MFP_PLANES(Pt_Planes  IN OUT Gt_Planes)AS
CURSOR C_Planes IS 
  select p.tmcode CODE, 
         p.descripcion NAMES, 
         p.id_plan Grupo
    from sysadm.mf_planes p
    order by 1,3;

  Ln_Contador NUMBER;

BEGIN
   Ln_Contador := 1;
   FOR i IN C_Planes LOOP
      Pt_Planes(Ln_Contador).CODE := i.Code;
      Pt_Planes(Ln_Contador).NAMES := i.Names;
      Pt_Planes(Ln_Contador).Grupo := i.Grupo;
      Ln_Contador := Ln_Contador + 1;      
   END LOOP;      
      
  BEGIN
   DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
   EXCEPTION
   WHEN OTHERS THEN
   NULL;
  END;
 EXCEPTION
  WHEN OTHERS THEN
  BEGIN
   DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
   EXCEPTION
    WHEN OTHERS THEN
     NULL;
  END;
     Pt_Planes(1).CODE := '0';
     Pt_Planes(1).NAMES := 'SIN DATOS';
END;   

---------
END MFK_TRX_CRITERIOS;
/
