create or replace package read.COK_CARTERA_CLIENTES is
    
  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================

  --[2702] ECA
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_CARTERA_CLIENTES';
  lv_referencia_scp varchar2(100):='COK_CARTERA_CLIENTES.MAIN';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_id_bitacora_scp number:=0;
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_error_scp number:=0;
  ln_registros_procesados_scp number:=0;

  -- Sud Andres de la Cuadra
  --4737 Modificacion al reporte de Detalles de Clientes
  gv_ciclo       varchar2(3);

  --
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string);
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date);
  FUNCTION SET_MORA(pdFecha in date) RETURN number;
  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string);
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2;


   /* Sud Andres de la Cuadra
     procedimientos para la extracion de los pagos diarios y la deuda acutual del cliente
  ini
  */
  PROCEDURE set_valor_deuda_pagos_diarios(pn_despachador in number,
                                          pv_ciclo       in varchar2,
                                          pv_fecha       in varchar2,
                                          Pv_Error out varchar2);

  PROCEDURE get_valor_deuda_actual(Pv_cuenta in varchar2,
                                   pn_solicitud in number default NULL,
                                   pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                   pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                   pv_valor  out varchar2,
                                   Pv_Error out varchar2);

  PROCEDURE get_valor_pagos_diarios(Pv_cuenta in varchar2,
                                    pn_valoradeuda in number default NULL,
                                    pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                    pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                    pv_valor  out varchar2,
                                    Pv_Error out varchar2);
  /*fin*/

    --AGC
    --PROCEDIMIENTO QUE ADCIONAN CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    GV_ACCION      VARCHAR2(1):='N';

    PROCEDURE ADICIONALES(PV_FECHACORTE IN DATE,
                        PV_ERROR      OUT VARCHAR2,
                        PV_REPROCESO  IN VARCHAR2 DEFAULT NULL);
  PROCEDURE CREA_TABLA1(PV_FECHACORTE IN VARCHAR2, PV_ERROR OUT VARCHAR2);
  PROCEDURE COP_EXTRAE_CUENTAS(PV_FECHACORTE IN VARCHAR2,
                               PV_CICLO      IN VARCHAR2,
                               PV_ERROR      OUT VARCHAR2);
  PROCEDURE COP_AGENDA_CARTERA(PV_FECHACORTE IN VARCHAR2,
                               PV_CICLO      IN VARCHAR2,
                               PV_ERROR      OUT VARCHAR2);

  PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,
                             PV_CICLO      IN VARCHAR2,
                             PV_ANIO       IN VARCHAR2,
                             PV_MES        IN VARCHAR2,
                             PV_REPROCESO  IN VARCHAR2,
                             PV_ERROR      OUT VARCHAR2);

  PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE  IN VARCHAR2,
                                PV_CICLO       IN VARCHAR2,
                                PV_ANIO        IN VARCHAR2,
                                PV_MES         IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_OBSERVACION IN VARCHAR2,
                                PV_REPROCESO   IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE COP_REPROCESA(PV_FECHACORTE  IN VARCHAR2,
                          PV_CICLO       IN VARCHAR2,
                          PV_ANIO        IN VARCHAR2,
                          PV_MES         IN VARCHAR2,
                          PV_ESTADO      IN VARCHAR2,
                          PV_OBSERVACION IN VARCHAR2,
                          PV_ERROR       OUT VARCHAR2);




end;
/
create or replace package body read.COK_CARTERA_CLIENTES is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    CLS - DARWIN ROSERO
  -- Fecha Modificaci�n: 2006/04/04
  -- Objetivo: Agregaci�n de un nuevo index a la tabla CO_REPCARCLI_ddmmyyyy
  --===================================================
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 20/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Se comenta la creacion de las tablas OPE_CUADRE1
                      y OPE_CUADRE2, esto fue consultado con el usuario,
                      GSI Billing y con Jessica Herrera y confirmaron
                      que nadie usa estas tablas.
  =========================================================*/
  -----------------------------------
  -- FUNCIONES PARA VIGENTE Y VENCIDA
  -----------------------------------
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string) is
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
    lvIdCiclo   VARCHAR2(2);
    leError      EXCEPTION;

  BEGIN

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.INSERTA_OPE_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    select id_ciclo into lvIdCiclo from fa_ciclos_bscs where dia_ini_ciclo = to_char(pdFechCierrePeriodo, 'dd');

    /*if lvIdCiclo = '01' then
       lvSentencia := 'drop table ope_cuadre1';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre1 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    else
       lvSentencia := 'drop table ope_cuadre2';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre2 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    end if;
    commit;*/

    delete from ope_cuadre where id_ciclo = lvIdCiclo;
    commit;

    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre NOLOGGING (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade, plan, id_ciclo)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade, plan, '''||lvIdCiclo||''' from co_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    commit;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.INSERTA_OPE_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;

  EXCEPTION
    when leError then
      pstrError := 'inserta ope_cuadre '||lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar INSERTA_OPE_CUADRE',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
    when others then
      pstrError := 'inserta ope_cuadre '||sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar INSERTA_OPE_CUADRE',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ----------------------------------------
  ----------------------------------------

  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(32767);
    lvServ2            varchar2(10000);
    lvSentencia        varchar2(32767);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;

    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, upper(replace(replace(replace(replace(nombre2, '(', ''), ')', ''), ' ', ''), '.', '_')) nombre2
        from read.COB_SERVICIOS--cob_servicios
       where cargo2 = 0;
    /*
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS--cob_servicios
       where cargo2 = 0;*/

    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS --cob_servicios
       where cargo2 = 1;

  BEGIN

    lvServ1 := '';
    for s in c_servicios1 loop
      lvServ1 := lvServ1 || s.nombre2 || '+';
    end loop;
    lvServ1 := substr(lvServ1, 1, length(lvServ1) - 1);

    lvServ2 := '';
    for t in c_servicios2 loop
      lvServ2 := lvServ2 || t.nombre2 || '+';
    end loop;
    lvServ2 := substr(lvServ2, 1, length(lvServ2) - 1);

    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 || ',' || ' TOTAL2 = ' ||
                   lvServ2 || ',' || ' TOTAL3 = credito_promocion,' ||
                   ' TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

  EXCEPTION
    when others then
      pvError := sqlerrm;
  END;

  -------------------------------------------
  --             SET_SERVICIOS
  -------------------------------------------
  PROCEDURE SET_SERVICIOS(pdFecha in date) IS

    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);

    cursor cur_servicios is
      select /*+ rule */
       d.custcode,
       sum(otamt_revenue_gl) as Valor,
       replace(h.nombre2, '.', '_') nombre2
        from ordertrailer  A, --items de la factura
             mpusntab      b, --maestro de servicios
             orderhdr_all  c, --cabecera de facturas
             customer_all  d, -- maestro de cliente
             ccontact_all  f, -- informaci�n demografica de la cuente
             payment_all   g, --Forna de pago
             read.COB_SERVICIOS  /*COB_SERVICIOS*/ h, --formato de reporte
             COB_GRUPOS    I, --NOMBRE GRUPO
             COSTCENTER    j
       where a.otxact = c.ohxact and c.ohentdate = pdFecha and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and
             substr(otname,
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                    instr(otname,
                          '.',
                          instr(otname,
                                '.',
                                instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
             b.sncode and d.customer_id = f.customer_id and f.ccbill = 'X' and
             g.customer_id = d.customer_id and act_used = 'X' and
             h.servicio = b.sncode and h.ctactble = a.otglsale AND
             D.PRGCODE = I.PRGCODE and j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                a.otglsale,
                a.otxact,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE
      UNION
      SELECT /*+ rule */
       d.custcode, sum(TAXAMT_TAX_CURR) AS VALOR, h.nombre2
        FROM ORDERTAX_ITEMS A, --items de impuestos relacionados a la factura
             TAX_CATEGORY   B, --maestro de tipos de impuestos
             orderhdr_all   C, --cabecera de facturas
             customer_all   d,
             ccontact_all   f, --datos demograficos
             payment_all    g, -- forma de pago
             read.COB_SERVICIOS /*COB_SERVICIOS*/  h,
             COB_GRUPOS     I,
             COSTCENTER     J
       where c.ohxact = a.otxact and c.ohentdate = pdFecha and
            --c.ohentdate=to_date('24/11/2003','DD/MM/YYYY') and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and taxamt_tax_curr > 0 and
             A.TAXCAT_ID = B.TAXCAT_ID and d.customer_id = f.customer_id and
             f.ccbill = 'X' and g.customer_id = d.customer_id and
             act_used = 'X' and h.servicio = A.TAXCAT_ID and
             h.ctactble = A.glacode AND D.PRGCODE = I.PRGCODE and
             j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                A.glacode,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE;

  BEGIN
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT custcode, sum(valor), sum(nvl(descuento,0)), replace(nombre2, ''.'', ''_'') nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  --' FROM cust_3_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY custcode, nombre2';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lvCuenta, 24);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
   dbms_sql.define_column(source_cursor, 4,  lvNombre2, 40);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lvCuenta);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);
      dbms_sql.column_value(source_cursor, 4, lvNombre2);

      lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                     ' set '||lvNombre2||' = '||lvNombre2||'+'||lnValor||'-'||lnDescuento||
                     ' where cuenta = ''' ||lvCuenta|| '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII:=lII+1;
      if lII = 2000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

/*    for i in cur_servicios loop
      lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                     ' set ' || i.nombre2 || ' = ' || i.nombre2 || '+' ||
                     to_char(i.valor) || ' where cuenta = ''' || i.custcode || '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII := lII + 1;
      if lII = 5000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
*/
  END;

  -------------------------------------------------------
  --      SET_MORA
  --      Funci�n que coloca la letra -V en caso
  --      de que el saldo del ultimo periodo sea negativo
  -------------------------------------------------------
  FUNCTION SET_MORA(pdFecha in date) RETURN number is
    lvSentencia varchar2(1000);
    lvMensErr   varchar2(1000);
  BEGIN
    lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                   ' set mayorvencido = ''''''-V''' || ' where balance_12 < 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
    return 1;
  END;

  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN
    --lnMesFinal:= to_number(to_char(pdFecha,'MM'));
    lvSentencia := '';
    --for lII in 1..lnMesFinal-1 loop
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || 'balance_' || lII || ',';
    end loop;
    lvSentencia := lvSentencia || 'balance_12';
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_BALANCES;

  --------------------------------------------
  -- SET_CAMPOS_SERVICIOS
  --------------------------------------------
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia      varchar2(32000);
    lvNombreServicio varchar2(100);

    --SIS RCA: Agregado el "replace", para evitar problemas con nombres de servicio
    -- que contienen el caracter punto "."

    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
        from read.cob_servicios
       where cargo2 = 0;

    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
        from read.cob_servicios
       where cargo2 = 1;

  BEGIN
    lvSentencia := '';
    for s in c_servicios1 loop
      lvSentencia := lvSentencia || s.nombre2 || ' NUMBER default 0, ';
    end loop;
    -- este campo divide los servicios de tipo no cargo
    -- de los tipo occ o cargos segun el reporte de detalle de clientes.
    lvSentencia := lvSentencia || ' TOTAL1 NUMBER default 0, ';
    for t in c_servicios2 loop
      lvSentencia := lvSentencia || t.nombre2 || ' NUMBER default 0, ';
    end loop;

    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_SERVICIOS;

  --------------------------------------------
  -- SET_CAMPOS_MORA
  --------------------------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN

    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_MORA;

  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    --
    lvSentencia1 VARCHAR2(32000);
    lvMensErr   VARCHAR2(1000);
    leError     exception;
    --
  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.CREA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    lvSentencia1 := 'CREATE TABLE CO_REPCARCLI_'|| to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( CUENTA               VARCHAR2(24),' ||
                   '  ID_CLIENTE            NUMBER,' ||
                   '  PRODUCTO              VARCHAR2(30),' ||
                   '  CANTON                VARCHAR2(40),' ||
                   '  PROVINCIA             VARCHAR2(25),' ||
                   '  APELLIDOS             VARCHAR2(40),' ||
                   '  NOMBRES               VARCHAR2(40),' ||
                   '  RUC                   VARCHAR2(20),' ||
                   '  FORMA_PAGO            VARCHAR2(58),' ||
                   '  TARJETA_CUENTA        VARCHAR2(25),' ||
                   '  FECH_EXPIR_TARJETA    VARCHAR2(20),' ||
                   '  TIPO_CUENTA           VARCHAR2(40),' ||
                   '  FECH_APER_CUENTA      DATE,' ||
                   '  TELEFONO1             VARCHAR2(25),' ||
                   '  TELEFONO2             VARCHAR2(25),' ||
                   '  DIRECCION             VARCHAR2(70),' ||
                   '  DIRECCION2            VARCHAR2(200),' ||
                   '  GRUPO                 VARCHAR2(10),' ||
                   cok_cartera_clientes.set_campos_mora(pdFechaPeriodo, lvMensErr) ||
                   '  TOTALVENCIDA          NUMBER default 0,' ||
                   '  TOTALADEUDA           NUMBER default 0,' ||
                   '  MAYORVENCIDO          VARCHAR2(10),' ||
                   cok_cartera_clientes.set_campos_servicios(lvMensErr) ||
                   '  SALDOANTER            NUMBER default 0,' ||
                   '  TOTAL2                NUMBER default 0,' ||
                   '  TOTAL3                NUMBER default 0,' ||
                   '  TOTAL_FACT            NUMBER default 0,' ||
                   '  SALDOANT              NUMBER default 0,' ||
                   '  PAGOSPER              NUMBER default 0,' ||
                   '  CREDTPER              NUMBER default 0,' ||
                   '  CMPER                 NUMBER default 0,' ||
                   '  CONSMPER              NUMBER default 0,' ||
                   '  DESCUENTO             NUMBER default 0,' ||
                   '  NUM_FACTURA           VARCHAR2(30),' ||
                   '  COMPANIA              NUMBER default 0,' ||
                   '  TELEFONO              VARCHAR2(63),' ||
                   '  BUROCREDITO           VARCHAR2(1) DEFAULT NULL,' ||
                   '  FECH_MAX_PAGO         DATE,'||
                   '  MORA_REAL             NUMBER default 0,'||
                   '  MORA_REAL_MIG         NUMBER default 0)'||
                   ' tablespace REP_DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K'||
                   '   next 1040K'||
                   '   minextents 1'||
                   '    maxextents unlimited'||
                   '   pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    lvSentencia1 := 'create index IDX_CARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (CUENTA)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

     ----------index  cliente - CLS - DARWIN ROSERO---------
     lvSentencia1 := 'create index IDX_CARCLI1_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (ID_CLIENTE)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    ----------------------------------------------

    lvSentencia1 := 'create public synonym CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||' for sysadm.CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    Lvsentencia1 := 'grant all on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.CREA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    return 1;

  EXCEPTION
    when leError then
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
    when others then
      pv_error := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
  END CREA_TABLA;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 19/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : No debe haber c�digo quemado.
  =========================================================*/

  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string) IS

    -- variables
    lvSentencia    VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;


    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;



    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;

    --AGC 4735 ADICION DE CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    bandera        VARCHAR2(1) DEFAULT 'S';
    lv_error      varchar2(4000); --AGC


    CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2)IS
           SELECT VALOR
           FROM GV_PARAMETROS
           WHERE ID_PARAMETRO=CV_PARAMETRO;



  BEGIN

    --CONTROL DE EJECUCION
    OPEN C_VERIFICA_CONTROL ('COK_CARTERA_CLIENTES.MAIN');
    FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
    LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
    CLOSE C_VERIFICA_CONTROL;

    IF LB_FOUND_CONTROL THEN
       UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
       WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
    ELSE
       INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
       VALUES ('COK_CARTERA_CLIENTES.MAIN',SYSDATE,'A');
    END IF;
    --

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_CUADRE'''; --''CUADRE_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

    -- si la tabla del cual consultamos existe entonces
    -- se procede a calcular los valores
    if lnExisteTabla = 1 then

      --busco la tabla de detalle de clientes
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_REPCARCLI_' ||
                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

      if lnExisteTabla = 1 then
         lvSentencia := 'truncate table CO_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      else
        -- creamos la tabla de acuerdo a los servicios del periodo
        -- y las diferentes edades de mora
        lnExito := cok_cartera_clientes.crea_tabla(pdFechCierrePeriodo,lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      end if;


      --se insertan los datos generales y los saldos de la tabla
      --de proceso inicial de reportes
      --lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                     ' NOLOGGING ' ||
                     '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                     cok_cartera_clientes.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                     'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, ' ||
                     cok_cartera_clientes.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', total_deuda, total_deuda+balance_12, decode(mora,0,''V'',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      commit;

      -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
      lnExito := cok_cartera_clientes.set_mora(pdFechCierrePeriodo);

      -- luego se insertan los servicios de la vista
      cok_cartera_clientes.set_servicios(Pdfechcierreperiodo);

      -- se calculan las columnas de totales
      cok_cartera_clientes.set_totales(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;

      -- se inserta registro para parametro
      lvSentencia := 'truncate table CO_BILLCYCLES';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      for i in cur_periodos loop
          --[2702] ECA
          --No debe haber ciclos quemados
          /*insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);*/
          insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          GET_CICLO(i.cierre_periodo),
          to_number(GET_CICLO(i.cierre_periodo)),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);
      end loop;
      commit;

      -- se inserta en la ope_cuadre
      cok_cartera_clientes.inserta_ope_cuadre(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;

    end if;

          ---JMO Llamada al reporte datacredito para calificaci�n de clientes
      COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(pdFechCierrePeriodo =>pdfechcierreperiodo,
                                                         pv_error => lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      --JMO

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --CONTROL DE EJECUCION
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
      COMMIT;
      --

        /*
      SUD. Arturo Gamboa Carey
       Adicion de campos al Reporte de Detalle de clientes
      */

      OPEN C_PARAMETROS('ADICIONA_COL') ;
      FETCH C_PARAMETROS INTO bandera;
      CLOSE C_PARAMETROS;

      if bandera= 'S' THEN

         COK_CARTERA_CLIENTES.ADICIONALES(pdFechCierrePeriodo,lv_error);
      if lv_error is not null THEN
            NULL;
      end if;

      END IF;


  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
         --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         ln_registros_error_scp:=ln_registros_error_scp+1;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_CARTERA_CLIENTES.MAIN',pstrError,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------

         --SCP:FIN
         ----------------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
         ----------------------------------------------------------------------------
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END MAIN;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 19/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Funcion para obtener el ciclo de
                      facturacion en base a una fecha
  =========================================================*/

  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2 IS

  CURSOR C_CICLO IS
    SELECT ID_CICLO
      FROM FA_CICLOS_BSCS
     WHERE DIA_INI_CICLO = TO_CHAR(PD_FECHA, 'DD');

  LV_CICLO     VARCHAR2(2);

  BEGIN
       OPEN C_CICLO;
       FETCH C_CICLO INTO LV_CICLO;
       CLOSE C_CICLO;
       RETURN (LV_CICLO);
  END;



/*########################################################################
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : Procedimiento encargado de extraer
                        el valor de los pagos realizados por el cliente hasta la fecha
          4737 Modificacion al reporte de Detalles de Clientes
#######################################################################*/
 FUNCTION get_ciclo_dia(pv_ciclo in varchar2)return varchar2 is
   cursor c_ciclo is
    select dia_ini_ciclo from fa_ciclos_bscs
      where id_ciclo =  pv_ciclo;
   lv_dia_ciclo     varchar2(3);
 BEGIN
    open c_ciclo;
     fetch c_ciclo into lv_dia_ciclo;
    close c_ciclo;
    return (lv_dia_ciclo);
 END get_ciclo_dia;

 /*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : procedimiento encargado de actualizar la tabla ...
                      con los valores diarios de la deuda y los pagos realizados por el cliente
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
PROCEDURE set_valor_deuda_pagos_diarios(pn_despachador in number,
                                        pv_ciclo       in varchar2,
                                        pv_fecha       in varchar2,
                                        Pv_Error out varchar2) is
  cursor c_campos_procesar is
   select nombre_campo,tipo_dato,programa,campo_control from co_report_cli_parametros
    where estado='A'
     --- and adicional='X'
      and  tipo_ejecucion in('T','D')
      AND BASE =  'BSCS'
    order by orden;

   cursor c_valida_tabla(cv_nombre varchar2) is
    select 'X' from all_tables
     where table_name=cv_nombre;

   cursor c_rangodias(cv_ciclo varchar2) is
    select dias_p from co_ciclos_reporte_cli
     where id_ciclo=cv_ciclo;

  /*Parametros SCP ini*/
  lv_Programa          varchar2(1000) := 'CARGA DE PAGOS DIARIOS-- Ciclo :'||pv_ciclo||' Fecha :'||pv_fecha||'Hilo :'||pn_despachador;
  lv_proceso           varchar2(30) := 'COK_CARGAPAGOSDEUDAS';
  lv_parametro_detener varchar2(30) := 'COK_DETENER_CARGAS';
  lv_parametro_Error   varchar2(30) := 'COK_NIVEL_ERROR_PAGOSDEUDASDIA';
  lv_parametro_Commit  varchar2(30) := 'COK_COMMIT_PAGOSDEUDASDIA';
  lv_nivelError        varchar2(50) := '0';
  lv_desc_NivelError   varchar2(5000);
  lv_valor_commit      varchar2(50);
  lv_desc_commit       varchar2(1000);
  lv_valor_detener     varchar2(1);
  lv_desc_detener      varchar2(5000);
  ln_contador          number:=0;
  ln_bitacora          number;
  ln_error             number;
  ln_errores           number;
  lv_MensajeAccion     varchar2(4000);
  lv_MensajeTecnico    varchar2(4000);
  lv_errorAplica     varchar2(100);
  ln_nivelError      varchar2(50) := '0';
  ln_exitos          number := 0;
  /* fin*/
  lv_dia_ciclo    varchar2(15);
  lv_co_repcarcli_adic varchar2(30);
  lv_co_repcarcli varchar2(30);
  lv_cuentas      varchar2(4000);
  lv_extrae_datos varchar2(4000);
  lv_update       varchar2(4000);
  ln_procesados   number:=0;
  lb_found        boolean;
  lb_notfound     boolean;
  ld_fecha        date:=to_date(pv_fecha,'dd/mm/yyyy');
  pd_fecha_ini    date;
  pd_fecha_fin    date;
  pn_valor_entrada  NUMBER:=NULL;
  lt_customer_id  cot_string := cot_string();
  lt_totaladeuda  cot_number:=cot_number();
  lv_valor        varchar2(100);
  le_error_bscs   exception;
  le_error_cuentas   exception;
  lv_ciclo_axis     varchar2(2);
  lv_ciclo_bscs     varchar2(2);
  lv_desc_ciclo     varchar2(100);
  lv_inicio_periodo varchar2(10);
  lv_fin_periodo    varchar2(10);
  begin
       porta.clk_api_politica_ciclo.clp_obtiene_ciclo@axis(ld_fecha,
                                                pv_ciclo,
                                                lv_ciclo_bscs,
                                                lv_inicio_periodo,
                                                lv_fin_periodo,
                                                lv_desc_ciclo,
                                                Pv_Error);
       commit;
       DBMS_SESSION.close_database_link('axis');
       commit;
       pd_fecha_ini:=to_date(lv_inicio_periodo,'dd/mm/yyyy');
       pd_fecha_fin:=to_date(lv_fin_periodo,'dd/mm/yyyy');

       --dia del ciclo a ejecutar
       lv_dia_ciclo:=to_char(to_date(lv_inicio_periodo,'dd/mm/yyyy'),'ddmmyyyy');
       lv_co_repcarcli_adic:='CO_REPORTE_ADIC_'||lv_dia_ciclo;
       lv_co_repcarcli:='CO_REPCARCLI_'||lv_dia_ciclo;
       --validar que la tabla exista
       open c_valida_tabla(lv_co_repcarcli_adic);
        fetch c_valida_tabla into lv_valor;
         lb_found:=c_valida_tabla%notfound;
       close c_valida_tabla;
       --si no encuentra la tabla actualiza
       if lb_found then
          lv_MensajeAccion  := 'La tabla '||lv_co_repcarcli_adic||' no existe';
                Lv_MensajeTecnico := 'Error en cok_cartera_clientes';
          raise le_error_bscs;
       end if;
       lv_valor:=null;
       open c_rangodias(pv_ciclo);
        fetch c_rangodias into lv_valor;
       close c_rangodias;

    /*   \* ini*\
       --cargar las cuentas
     lv_cuentas:='begin '||
                 ' select count(cuenta)'||
                 '  into :1 from '||lv_co_repcarcli_adic||
                 ' a where hilo = :2 '||
                 '   and a.deuda <> 0'||
                 '   and a.id_contrato is not null '||
                 '   and exists ('||
                 ' select ''X'' '||
                 '  from cashreceipts_all d'||
                 ' where d.customer_id = a.cliente '||
                 ' and d.cachkdate between '||
                 ' to_date('''||pv_fecha||''',''dd/mm/yyy'')-:3 and to_date('''||pv_fecha||''',''dd/mm/yyy'')' ||
                 ' ); '||
                 ' exception '||
                 ' when no_data_found then '||
                 ' null; '||
                 'end;';
         execute immediate lv_cuentas using out ln_contador ,in pn_despachador,in lv_valor;
        */

      lv_cuentas :='begin '||
                  ' select a.cuenta ,a.deuda bulk collect into :1,:2 '||
                  ' from '||lv_co_repcarcli_adic||' a '||
                  ' where a.hilo_diario = :3 '||
                  ' and a.deuda <> 0'||
                  ' and a.id_contrato is not null '||
                  ' and exists ('||
                  ' select ''X'' '||
                  '  from cashreceipts_all d'||
                  ' where d.customer_id = a.cliente '||
                  ' and d.cachkdate between '||
                  ' to_date('''||pv_fecha||''',''dd/mm/yyyy'')-:4 and to_date('''||pv_fecha||''',''dd/mm/yyyy'')' ||
                  ' ); '||
                  ' end;';
       execute immediate lv_cuentas using out lt_customer_id,out lt_totaladeuda ,in pn_despachador,in lv_valor;
       ln_contador:=lt_customer_id.last;
           --
        SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso        => lv_proceso,
                                              pv_referencia        => lv_Programa,
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => null,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => ln_contador, --cuantos registros se procesar�n
                                              pv_unidad_registro   => 'registros',
                                              pn_id_bitacora       => ln_bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => Pv_Error);

        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            raise le_error_bscs;
        end if;
        -- Consultar el par�metro de nivel de error
        SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_Error,
                                                pv_id_proceso   => lv_Proceso,
                                                pv_valor        => lv_nivelError,
                                                pv_descripcion  => lv_desc_NivelError,
                                                pn_error        => ln_error,
                                                pv_error        => Pv_Error);
        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            raise le_error_bscs;
        end if;
        -- Consultar el par�metro de nivel de error
        SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_commit,
                                                pv_id_proceso   => lv_Proceso,
                                                pv_valor        => lv_valor_commit,
                                                pv_descripcion  => lv_desc_commit,
                                                pn_error        => ln_error,
                                                pv_error        => Pv_Error);
        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            raise le_error_bscs;
        end if;
        --
        if ln_contador = 0 then
            lv_MensajeAccion  := 'Se espera a la siguiente ejecuci�n';
            Lv_MensajeTecnico := 'No existen registros a procesar.';
            Pv_Error          := 'No existen registros procesar-' ||
                                                     sqlerrm;
            raise le_error_bscs;
        end if;
        --
       /*fin*/
       --recorre las cuentas
       lv_valor:=null;
       if lt_customer_id.count > 0 then
        for ind in lt_customer_id.first .. lt_customer_id.last loop
/*inicio proceso*/
         begin
              SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_detener,
                                                      pv_id_proceso   => lv_proceso,
                                                      pv_valor        => lv_valor_detener,
                                                      pv_descripcion  => lv_desc_detener,
                                                      pn_error        => ln_error,
                                                      pv_error        => Pv_Error);
                      if ln_error = -1 then
                          lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
                          Lv_MensajeTecnico := 'Error en SCP.';
                          --
                          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                pv_mensaje_aplicacion => Pv_Error,
                                                                pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                                pv_mensaje_accion     => lv_MensajeAccion,
                                                                pn_nivel_error        => 3,
                                                                pv_cod_aux_1          => null,
                                                                pv_cod_aux_2          => null,
                                                                pv_cod_aux_3          => null,
                                                                pn_cod_aux_4          => null,
                                                                pn_cod_aux_5          => null,
                                                                pv_notifica           => 'S',
                                                                pn_error              => ln_error,
                                                                pv_error              => Pv_Error);
                          exit;
                      end if;
                      --
                      if lv_valor_detener = 'S' then
                          exit;
                      end if;
                      --
                      Pv_Error := NULL;
             --empieza a arma la sentecia de update
              lv_update:= 'update '||lv_co_repcarcli_adic||' set';
              --se crea la sentencia de extraer los valores de los campos
              for j in c_campos_procesar loop
               lv_extrae_datos := 'begin  '|| j.programa ||'(:1,:2,:3,:4,:5,:6); end;';
               execute immediate lv_extrae_datos using in lt_customer_id(ind),
                                                       in lt_totaladeuda(ind),
                                                       in pd_fecha_ini,
                                                       in pd_fecha_fin,
                                                       out lv_valor,
                                                       out Pv_Error;
               if Pv_Error is not null then
                  lv_errorAplica := 'Al ejecutar la sentencia dinamica de extracion de datos'||Pv_Error;
                              ln_nivelError  := 1;
                              ln_errores     := ln_errores + 1;
                              PV_ERROR:=NULL;
                              raise le_error_cuentas;
               end if;
               IF j.campo_control = 'S'  THEN
                   lv_update:=lv_update||lv_valor||',';
               ELSE
                  lv_valor :=Replace(lv_valor,',','.');--para error de la coma en valor float
              --con el valor del campo continua la sentencia del set para cada campo configurado
                  lv_update:=lv_update||' '||j.nombre_campo||' = '||lv_valor ||',';
               END IF;
              end loop;
              lv_update:=lv_update||' FECHA_ACTUALIZACION = TO_DATE('''||to_char(SYSDATE,'dd/mm/yyyy HH24:MI:SS')||''',''dd/mm/yyyy HH24:MI:SS'')';
             -- lv_update:=Substr(lv_update,1,Length(lv_update)-1);--para quitar la ultima coma
              lv_update:= lv_update||' where cuenta= :1';
              -- se ejecuta la sentencia del update
              execute immediate lv_update using in lt_customer_id(ind);
                      ln_procesados:=ln_procesados+1;
              ln_exitos:=ln_exitos+1;
            EXCEPTION
              when le_error_cuentas then
               ln_nivelError := ln_nivelError+1;
               pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES ';
               SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                     pv_mensaje_aplicacion => Pv_Error,
                                                     pv_mensaje_tecnico    => lv_errorAplica,
                                                     pv_mensaje_accion     => null,
                                                     pn_nivel_error        => ln_nivelError,
                                                     pv_cod_aux_1          => null,
                                                     pv_cod_aux_2          => lt_customer_id(ind),
                                                     pv_cod_aux_3          => null,
                                                     pn_cod_aux_4          => null,
                                                     pn_cod_aux_5          => null,
                                                     pv_notifica           => 'S',
                                                     pn_error              => ln_error,
                                                     pv_error              => Pv_Error);

               SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                     pn_registros_procesados => ln_exitos,
                                                     pn_registros_error      => ln_nivelError,
                                                     pn_error                => ln_error,
                                                     pv_error                => Pv_Error);
               When Others then
                  ln_nivelError := ln_nivelError+1;
                  pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES ';
                  lv_errorAplica:=substr(sqlerrm, 1, 250);
                  SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                        pv_mensaje_aplicacion => Pv_Error,
                                                        pv_mensaje_tecnico    => lv_errorAplica,
                                                        pv_mensaje_accion     => null,
                                                        pn_nivel_error        => ln_nivelError,
                                                        pv_cod_aux_1          => null,
                                                        pv_cod_aux_2          => lt_customer_id(ind),
                                                        pv_cod_aux_3          => null,
                                                        pn_cod_aux_4          => null,
                                                        pn_cod_aux_5          => null,
                                                        pv_notifica           => 'S',
                                                        pn_error              => ln_error,
                                                        pv_error              => Pv_Error);

                  SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                        pn_registros_procesados => ln_exitos,
                                                        pn_registros_error      => ln_nivelError,
                                                        pn_error                => ln_error,
                                                        pv_error                => Pv_Error);

            end;--fin de procedure por cada cuenta
      --contar registros procesados
       if lv_valor_commit = ln_procesados then
          ln_procesados:=0;
          commit;
       end if;
     end loop;--loop de las cuentas
     commit;
    end if;
        SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                              pn_registros_procesados => ln_exitos,
                                              pn_registros_error      => ln_nivelError,
                                              pn_error                => ln_error,
                                              pv_error                => Pv_Error);

           -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
  commit;
  Exception
     When le_error_bscs then
        if Ln_nivelError >= lv_nivelError then
            SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso => lv_proceso,
                                                  pv_referencia        => lv_Programa,
                                                  pv_usuario_so        => null,
                                                  pv_usuario_bd        => null,
                                                  pn_spid              => null,
                                                  pn_sid               => null,
                                                  pn_registros_totales => 0, --cuantos registros se procesar�n
                                                  pv_unidad_registro   => 'registros',
                                                  pn_id_bitacora       => ln_bitacora,
                                                  pn_error             => ln_error,
                                                  pv_error             => Pv_Error);

            if ln_error = -1 then
                lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
                Lv_MensajeTecnico := 'Error en SCP.';
            end if;
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                pv_mensaje_aplicacion => Pv_error,
                                                pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                pv_mensaje_accion     => lv_MensajeAccion,
                                                pn_nivel_error        => ln_nivelError,
                                                pv_cod_aux_1          => null,
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pn_error              => ln_error,
                                                pv_error              => Pv_Error);
              end if;
              -- Termin� el proceso
              SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                                                                           pn_error       => ln_error,
                                                                                           pv_error       => Pv_Error);
     When Others then
          pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES ';
          Lv_MensajeTecnico:=substr(sqlerrm, 1, 250);
          if Ln_nivelError >= lv_nivelError then
            SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso => lv_proceso,
                                                  pv_referencia        => lv_Programa,
                                                  pv_usuario_so        => null,
                                                  pv_usuario_bd        => null,
                                                  pn_spid              => null,
                                                  pn_sid               => null,
                                                  pn_registros_totales => 0, --cuantos registros se procesar�n
                                                  pv_unidad_registro   => 'registros',
                                                  pn_id_bitacora       => ln_bitacora,
                                                  pn_error             => ln_error,
                                                  pv_error             => pv_error);

            if ln_error = -1 then
               lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
               Lv_MensajeTecnico := 'Error en SCP.';
            end if;
            SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                  pv_mensaje_aplicacion => pv_error,
                                                  pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                  pv_mensaje_accion     => lv_MensajeAccion,
                                                  pn_nivel_error        => ln_nivelError,
                                                  pv_cod_aux_1          => null,
                                                  pv_cod_aux_2          => null,
                                                  pv_cod_aux_3          => null,
                                                  pn_cod_aux_4          => null,
                                                  pn_cod_aux_5          => null,
                                                  pv_notifica           => 'S',
                                                  pn_error              => ln_error,
                                                  pv_error              => Pv_Error);
          end if;
    -- Termin� el proceso
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                          pn_error       => ln_error,
                                          pv_error       => Pv_Error);

  END set_valor_deuda_pagos_diarios;


/*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : function encargada de extraer
                        el valor de la deuda actual del cliente
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
PROCEDURE get_valor_deuda_actual(Pv_cuenta in varchar2,
                                 pn_solicitud in number default NULL,
                                 pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                 pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                 pv_valor  out varchar2,
                                 Pv_Error  out varchar2)  is

  --obtiene la forma de pago
  cursor c_cuenta(lv_cuenta varchar2) is
    select b.codigo_doc, id_forma_pago
      from cl_contratos@axis b
     where b.codigo_doc = lv_cuenta;

  cursor c_deuda_pendiente(cv_cuenta varchar2) is
    SELECT /*+ rule +*/
     nvl(sum(ohinvamt_doc), 0) deuda,
     nvl(SUM(o.ohopnamt_doc), 0) saldo,
     o.ohduedate fecha,
     ohrefdate fecha_1
      FROM customer_all c, orderhdr_all o
     WHERE c.custcode = cv_cuenta
       AND c.customer_dealer = 'C'
       AND c.customer_id = o.customer_id
       AND o.ohstatus IN ('IN', 'CM', 'CO')
       AND trunc(o.ohduedate) <
           to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
     GROUP BY c.custcode, o.ohduedate, ohrefdate;

  cursor c_deuda_pendiente_actual(cv_cuenta varchar2) is
    SELECT /*+ rule +*/
     nvl(sum(ohinvamt_doc), 0) deuda, nvl(SUM(o.ohopnamt_doc), 0) saldo
      FROM customer_all c, orderhdr_all o
     WHERE c.custcode = cv_cuenta
       AND c.customer_dealer = 'C'
       AND c.customer_id = o.customer_id
       AND o.ohstatus IN ('IN', 'CM', 'CO')
       AND trunc(o.ohduedate) <
           to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
     GROUP BY c.custcode;

  cursor c_pagos_no_levantados(cv_cuenta varchar2) is
    select nvl(sum(cachkamt), 0) pago
      from pihtab_all
     where carecdate between
           to_date(to_char(sysdate - 1, 'dd/mm/yyyy'), 'dd/mm/yyyy') and
           to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy')
       and custcode = cv_cuenta
       and transx_status in (1, 3);

  cursor c_creditos(cv_cuenta varchar2, cd_inicio_periodo date) is
    SELECT /*+ rule +*/
     nvl(sum(amount), 0) creditos
      FROM customer_all a, fees b
     WHERE a.custcode = cv_cuenta
       AND a.customer_id = b.customer_id
       AND b.period > 0
       AND b.sncode = 46
       AND trunc(b.entdate) >= cd_inicio_periodo;--to_date(cv_inicio_periodo, 'dd/mm/yyyy');

  CURSOR Cur_SaldoMinPor IS
    SELECT /*+ rule +*/
     par_valor
      FROM rc_param_global@axis
     WHERE par_nombre = 'SALDO_MIN_POR';

  CURSOR Cur_SaldoMinimo IS
    SELECT /*+ rule +*/
     par_valor
      FROM rc_param_global@axis
     WHERE par_nombre = 'SALDO_MINIMO';

  CURSOR c_dia_max_pago(cv_ciclo varchar2, lv_forma_pago varchar2) IS
    select valor
      from cl_formas_pago_deudas@axis w
     where w.id_ciclo = cv_ciclo
       and w.id_forma_pago = lv_forma_pago;

--  lc_ciclo_cuenta           Cur_ciclo_cuenta%rowtype;
  lc_deuda_pendiente_actual c_deuda_pendiente_actual%rowtype;
  lc_creditos               c_creditos%rowtype;
  lc_pagos_no_levantados    c_pagos_no_levantados%rowtype;
  Lv_Saldo_Min_Por          varchar2(255);
  lb_notfound               boolean;
  lb_found                  boolean;
  le_error_bscs exception;
  ln_deuda          number(10, 2) := 0;
  ln_creditos       number := 0;
  ln_saldo_real     number := 0;
  lv_mes1           varchar2(2);
  lv_anio           varchar2(4);
  Ln_Saldo_Min      number;
  Lv_Saldo_Min      varchar2(255);
  ln_por_impago     number;
  Ln_Saldo_Min_Por  number;
  ln_saldo_impago   number := 0;
  lv_cuenta         varchar2(20);
  lv_forma_pago     varchar2(3);
  ld_fecha          date;
  ld_fecha_periodo  date;
  fecha_caduca      date;
  lb_encontro       boolean;
  lv_ciclo_axis     varchar2(2):=gv_ciclo;
  Pn_deuda          float:=0;
BEGIN
  --para sacar la forma de pago
  open c_cuenta(Pv_cuenta);
  fetch c_cuenta
    into lv_cuenta, lv_forma_pago;
  lb_notfound := c_cuenta%notfound;
  close c_cuenta;
  commit;
   DBMS_SESSION.close_database_link('axis');
  commit;
  if lb_notfound then
    Pv_Error := 'La cuenta no est� activa';
    raise le_error_bscs;
  end if;
  --obtienen el ciclo de la cuenta
  /* 1.-saca la fecha de inicio del periodo
     2.- fecha actual
     3.-que sea igual al mes del ciclo
  */
  ld_fecha_periodo := pd_fecha_ini;--to_date(lv_inicio_periodo, 'dd/mm/yyyy');
  ld_fecha         := to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy');
  --
  if to_char(ld_fecha_periodo, 'mm') = to_char(ld_fecha, 'mm') then--si esta en el mismo mes
    lv_mes1 := to_char(ld_fecha_periodo, 'mm');--dejo el mismo mes
  else
    lv_mes1 := to_char(add_months(ld_fecha_periodo, 1), 'mm');--si no le sumo 1mes
  end if;

  lv_anio := to_char(sysdate, 'yyyy');
  --gv_ciclo
  for i in c_dia_max_pago(lv_ciclo_axis, lv_forma_pago) loop
    --saca el dia maximo en que puede pagar el cliente en su ciclo
    if i.valor is null then--Si no tienen dia maximo de pago saca la deuda actual
      open c_deuda_pendiente_actual(lv_cuenta);
      fetch c_deuda_pendiente_actual
        into lc_deuda_pendiente_actual;
      lb_notfound := c_deuda_pendiente_actual%notfound;
      close c_deuda_pendiente_actual;
    --ojo
      if lb_notfound then--si no encuentra el cliente no tienen deuda
        pv_valor:=to_char(Pn_deuda);
        return;
      else
        ln_deuda := lc_deuda_pendiente_actual.saldo;--esta es la deuda acutal
      end if;
    elsif i.valor <> '0' then --ojo
      if lv_mes1 = '02' then
        if i.valor > '28' then
          fecha_caduca := to_date('28'||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
          lb_encontro  := false;
        else--que a�o bisiesto ??
          fecha_caduca := to_date(i.valor||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
        end if;
      else
        fecha_caduca := to_date(i.valor||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
        lb_encontro  := false;
      end if;
    end if;
  end loop;
  if not lb_encontro then--
    --solo sumarizo los valores pendientes no deuda actual
    if ld_fecha <= fecha_caduca then--seg�n OPE y Johann
      for i in c_deuda_pendiente(lv_cuenta) loop
        if i.fecha_1 <> ld_fecha_periodo then
          ln_deuda := ln_deuda + i.saldo;
        elsif i.saldo < 0 then--Se valida si tiene un cr�dito memo o menor a cero
          ln_deuda := ln_deuda + i.saldo;
        end if;
      end loop;
      lb_encontro := true;
    else--saca la deuda pendiente + la actual
      open c_deuda_pendiente_actual(lv_cuenta);
      fetch c_deuda_pendiente_actual
        into lc_deuda_pendiente_actual;
      lb_notfound := c_deuda_pendiente_actual%notfound;
      close c_deuda_pendiente_actual;

      if lb_notfound then--si no encuentra el cliente no tienen deuda
        Pn_deuda := 0;
        pv_valor:=to_char(Pn_deuda);
        return;
      else--saca la deuda
        ln_deuda    := lc_deuda_pendiente_actual.saldo;
        lb_encontro := true;
      end if;
    end if;
  end if;
    --Verifico el pago en tabla pihtab_all
    --en el caso de que aun no hayan levantado
    --el pago.
    OPEN  c_pagos_no_levantados(lv_cuenta);
    FETCH c_pagos_no_levantados INTO lc_pagos_no_levantados;
    CLOSE c_pagos_no_levantados;

    if lc_pagos_no_levantados.pago > 0 then
       ln_deuda := ln_deuda - lc_pagos_no_levantados.pago;
       if ln_deuda <= 0  then
          ln_deuda := 0;
       end if;
    end if;

    if not lb_encontro then
       Pn_deuda:= 0;
       pv_valor:=to_char(Pn_deuda);
       return;
    end if;

    OPEN  Cur_SaldoMinimo;
    FETCH Cur_SaldoMinimo INTO Lv_Saldo_Min;
    CLOSE Cur_SaldoMinimo;
    --
    commit;
    DBMS_SESSION.close_database_link('axis');
    commit;
    Ln_Saldo_Min := to_number(Lv_Saldo_Min);

    OPEN  Cur_SaldoMinPor;
    FETCH Cur_SaldoMinPor INTO Lv_Saldo_Min_Por;
    CLOSE Cur_SaldoMinPor;
    commit;
    DBMS_SESSION.close_database_link('axis');
    commit;
    --
    Ln_Saldo_Min_Por := to_number(Lv_Saldo_Min_Por);
    --
    ln_por_impago := ((100 - Ln_Saldo_Min_Por) / 100);

    ln_saldo_impago := (ln_deuda * ln_por_impago);
    --
    -- Obtener creditos realizados durante el mes y no aplicados
    OPEN c_creditos (lv_cuenta,ld_fecha_periodo);
    FETCH c_creditos INTO lc_creditos;
    lb_found := c_creditos%FOUND;
    CLOSE c_creditos ;
    --
    IF lb_found THEN
       ln_creditos := lc_creditos.creditos;
    ELSE
       ln_creditos := 0;
    END IF;
    --
    ln_saldo_real := ln_deuda + ln_creditos;
    --No tiene deuda pendiente y puede continuar con la transaccion
    IF (ln_saldo_real <= Ln_Saldo_Min) OR (ln_saldo_real <= ln_saldo_impago) THEN
       Pn_deuda:= ln_saldo_real;
       pv_valor:=to_char(Pn_deuda);
       return;
    else
    --Tiene deuda
       Pn_deuda:= ln_saldo_real;
       pv_valor:=to_char(Pn_deuda);
       return;
    end if;

    Exception
     When le_error_bscs then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||Pv_Error;
          rollback;
          DBMS_SESSION.close_database_link('axis');
          commit;
          pv_valor:=0;
          return;

     When Others then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||sqlerrm;
          rollback;
          DBMS_SESSION.close_database_link('axis');
          commit;
          pv_valor:=0;
          return;
END get_valor_deuda_actual;

/*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : function encargada de extraer
                        el valor de los pagos realizados por el cliente en el ciclo
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
PROCEDURE get_valor_pagos_diarios(Pv_cuenta in varchar2,
                                  pn_valoradeuda in number default NULL,
                                  pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                  pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                  pv_valor  out varchar2,
                                  Pv_Error out varchar2)is

 cursor c_obtiene_pagos(cv_custcode in varchar2,cd_fecha_ini date, cd_fecha_fin date)is
  select to_char(d.cachkdate, 'DD-MM-YYYY') fecha_pago,
         nvl(d.cachkamt_pay, 0) importe_pagado,
         nvl(d.cacuramt_pay, 0) importe_actual,
         to_char(d.carecdate,'DD-MM-YYYY') fecha_recepcion
  from customer_all    c,
       cashreceipts_all d
  where c.custcode = cv_custcode
    and d.customer_id = c.customer_id
    and d.cachkdate between cd_fecha_ini and cd_fecha_fin
    order by d.cachkdate desc;


  pn_tot_pagos      float:=0;
  pn_total_pagos    float:=0;
  pn_saldo          float:=0;
  le_error_bscs     exception;

 BEGIN
   pv_valor:='0';
    if(Pv_cuenta is null) then
       raise le_error_bscs;
       Pv_Error:= 'la cuenta no puede ser nula';
    end if;
    for pagos in c_obtiene_pagos(Pv_cuenta,pd_fecha_ini,pd_fecha_fin) loop
      pn_tot_pagos:= pn_tot_pagos + pagos.importe_pagado;
    end loop;
    pn_total_pagos:=pn_tot_pagos;
    if(pn_valoradeuda > 0) then
    pn_saldo:=pn_valoradeuda - pn_total_pagos;
    else
    pn_saldo:= pn_valoradeuda;
    end if;
   -- pv_valor:=to_char(pn_total_pagos);
    pv_valor:=' pagos = '||Replace(to_char(pn_total_pagos),',','.')||', SALDO = '||Replace(to_char(pn_saldo),',','.');
   Exception
     When le_error_bscs then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||Pv_Error;
     When Others then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||sqlerrm;
 END get_valor_pagos_diarios;

 /*
  --===================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que permite crear campos adicionales para el reporte de detalles de clientes
  -- y los carga en cada corte de facturacion
  --===================================================
 */

 PROCEDURE ADICIONALES(PV_FECHACORTE IN DATE,PV_ERROR OUT VARCHAR2, PV_REPROCESO IN VARCHAR2 DEFAULT 'N') IS

    LV_ERROR VARCHAR2(5000);
    MI_ERROR EXCEPTION;
    BSCS_ERROR EXCEPTION;
    AXIS_ERROR EXCEPTION;
    LV_CICLO VARCHAR2(2);
    LV_FECHACORTE VARCHAR2(15);

 -----------------
 --scp variables
 -----------------

  Lv_Programa               varchar2(200) :=  'COK_CARTERA_CLIENTES.ADICIONALES';
  lv_proceso                varchar2(50) :=  'ADICIONALES';
  ln_error_scp                NUMBER;
  lv_error_scp                VARCHAR2(1024);
  ln_bitacora               number;
  ln_errores                number:=0;
  ln_exitos                 number := 0;
  ln_contador               number:= 0;

    BEGIN

    GV_ACCION:=PV_REPROCESO;

    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
        SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso      => lv_proceso,
                                                                                    pv_referencia        => lv_Programa,
                                                                                    pv_usuario_so        => null,
                                                                                    pv_usuario_bd        => null,
                                                                                    pn_spid              => null,
                                                                                    pn_sid               => null,
                                                                                    pn_registros_totales => ln_contador, --cuantos registros se procesar�n
                                                                                    pv_unidad_registro   => '',
                                                                                    pn_id_bitacora       => ln_bitacora,
                                                                                    pn_error             => ln_error_scp,
                                                                                    pv_error             => lv_error_scp);

        if ln_error_scp <> 0 THEN

            lv_error := lv_error_scp;
            raise MI_ERROR;
        end if;

              LV_CICLO:=GET_CICLO(PV_FECHACORTE);
              LV_FECHACORTE:=TO_CHAR(PV_FECHACORTE,'DDMMYYYY');

               IF LV_CICLO IS NULL THEN
                  LV_ERROR:='LA FECHA INGRESADA NO PERTENECE A UN CICLO EXISTENTE';
                  ln_errores := ln_errores + 1;
                  RAISE MI_ERROR;
              ELSE

                 COK_CARTERA_CLIENTES.cop_agenda_cartera(LV_FECHACORTE,LV_CICLO,LV_ERROR);
                 IF LV_ERROR IS NOT NULL THEN
                 ln_errores := ln_errores + 1;
                 RAISE MI_ERROR;
                 END IF;

                 ln_exitos:= ln_exitos + 1;

                SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => 'CORTE AGENDADO CON EXITO',
                                                                                                                    pv_mensaje_tecnico    => 'CO_REPORTE_ADIC_'||PV_FECHACORTE,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 0,
                                                                                                                    pv_cod_aux_1          => null,
                                                                                                                    pv_cod_aux_2          => null,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'N',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);

                COMMIT;

                END IF;

                --------------------------------------------------------------------
                -- Termin� el proceso
                --------------------------------------------------------------------
                 SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                                  pn_error       => ln_error_scp,
                                                  pv_error       => lv_error_scp);
               --------------------------------------------------------------------
                COMMIT;
                EXCEPTION
                WHEN MI_ERROR THEN
                    PV_ERROR:= 'COK_CARTERA_CLIENTES.ADICIONALES '||LV_ERROR ;
                    ----------------------------------------------------------------------
                                        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                        ----------------------------------------------------------------------
                                         SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => PV_ERROR,
                                                                                                                    pv_mensaje_tecnico    => PV_ERROR,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 1,
                                                                                                                    pv_cod_aux_1          => NULL,
                                                                                                                    pv_cod_aux_2          => NULL,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'S',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);
                                            commit;



                 WHEN OTHERS THEN
                  PV_ERROR:= SQLERRM;

                   ----------------------------------------------------------------------
                                        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                        ----------------------------------------------------------------------
                                         SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => PV_ERROR,
                                                                                                                    pv_mensaje_tecnico    => PV_ERROR,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 3,
                                                                                                                    pv_cod_aux_1          => NULL,
                                                                                                                    pv_cod_aux_2          => NULL,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'S',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);
                                            commit;


  END ADICIONALES;

  /*
  --===============================================================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC la cual es tomada via shell para procesar el corte
  shell ubicado en exis HilocargaMensual
  --===============================================================================================
 */

 PROCEDURE COP_AGENDA_CARTERA(PV_FECHACORTE IN VARCHAR2,
                                PV_CICLO IN VARCHAR2,
                                PV_ERROR OUT VARCHAR2) IS

    CURSOR BUSCA_EJECUCION(CV_FECHA_CORTE VARCHAR2) IS
    SELECT ESTADO FROM CO_BITACORA_REPORTE_ADIC S
    WHERE S.FECHA_CORTE=CV_FECHA_CORTE
          AND S.FECHA_INGRESO=(SELECT MAX(FECHA_INGRESO)FROM CO_BITACORA_REPORTE_ADIC A
                             WHERE A.FECHA_CORTE=CV_FECHA_CORTE);

     TYPE LR_CUENTAS IS RECORD(
    CUENTA        VARCHAR2(24));
  --*
  TYPE LT_CUENTAS IS TABLE OF LR_CUENTAS INDEX BY BINARY_INTEGER;
  TMP_CUENTAS     LT_CUENTAS;

  LV_ANIO VARCHAR2(4);
  LV_MES  VARCHAR2(2);
  HILOS           NUMBER:=1;
  TOT_HILOS       NUMBER;
  LV_ESTADO       VARCHAR2(1):=NULL;
  LV_QUERY        VARCHAR2(2000);
  LV_QUERY1       VARCHAR2(2000);
  LV_OBSERVACION  VARCHAR2(2000);
  LV_ERROR VARCHAR2(4000);
  CUENTA VARCHAR2(10);
  MI_ERROR EXCEPTION;
  LB_FOUND BOOLEAN;



      BEGIN
                select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'yyyy') INTO LV_ANIO from dual;
                select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'mm') INTO LV_MES from dual;

            -- verifica si existe una ejecucion correspondiente a la fecha de corte
            OPEN BUSCA_EJECUCION (PV_FECHACORTE);
            FETCH BUSCA_EJECUCION INTO LV_ESTADO;
            LB_FOUND:= BUSCA_EJECUCION%FOUND;
            CLOSE BUSCA_EJECUCION;

            IF LV_ESTADO IS NULL THEN



                COK_CARTERA_CLIENTES.CREA_TABLA1(PV_FECHACORTE,LV_ERROR);

                IF LV_ERROR IS NOT NULL  THEN
                    RAISE MI_ERROR;
                END IF;



                  COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                   COK_CARTERA_CLIENTES.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,'N',LV_ERROR );
                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;


            ELSIF  LV_ESTADO IN  ('A','X') THEN

                  LV_OBSERVACION:='REGISTRO INACTIVO, NO FUE PROCESADO.. LISTO PARA REPROCESAR';


                  BEGIN

                          LV_QUERY:='TRUNCATE TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                          EXECUTE IMMEDIATE LV_QUERY;

                 EXCEPTION
                    WHEN OTHERS THEN
                         LV_ERROR:='ERROR AL TRUNCAR LA TABLA CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE|| ''||  SQLERRM;
                         RAISE MI_ERROR;
                 END;


                  COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                  COK_CARTERA_CLIENTES.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,'S',LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;

            ELSIF  LV_ESTADO IN ('P') AND GV_ACCION='S'  THEN
                   LV_OBSERVACION:='REGISTRO INACTIVO, REPROCESO ';

                   COK_CARTERA_CLIENTES.COP_REPROCESA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,LV_ERROR);
                    IF LV_ERROR IS NOT NULL THEN
                     RAISE MI_ERROR;
                    END IF;
            END IF;
           COMMIT;




    EXCEPTION

    WHEN MI_ERROR THEN
            PV_ERROR:=LV_ERROR;


         WHEN OTHERS THEN
          PV_ERROR:= ' ERROR EN EL PROCEDIMIENTO COP_AGENDA_CARTERA '||SQLERRM;


   END COP_AGENDA_CARTERA;

      /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  --  Proceso que inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC
  --===============================================================================================
 */

   PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
   IS

   BEGIN

   INSERT INTO CO_BITACORA_REPORTE_ADIC (fecha_corte,ID_EJECUCION,CICLO,MES,A�O,ESTADO,FECHA_INGRESO,REPROCESO)
   VALUES(PV_FECHACORTE,COP_ADICIONAL_BITA_SEQ.NEXTVAL,PV_CICLO,PV_MES,PV_ANIO,'A',SYSDATE,NVL(PV_REPROCESO,'N'));

   EXCEPTION
   WHEN OTHERS THEN
   PV_ERROR:='ERROR AL AGENDAR LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;


   END COP_AGENDA_FECHA;

         /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  --  Proceso que inactiva el regsitro correspondiente a la fecha de corte inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC  con estado 'A'
  --===============================================================================================
 */

   PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS
     LV_ERROR VARCHAR2(4000);
     MI_ERROR EXCEPTION;
   BEGIN

             BEGIN

             update CO_BITACORA_REPORTE_ADIC A
             set
             A.ESTADO='I',
             A.FECHA_ACTUALIZACION=SYSDATE,
             A.OBSERVACION=PV_OBSERVACION

             where A.FECHA_CORTE=PV_FECHACORTE AND A.ESTADO=PV_ESTADO
             AND A.ID_EJECUCION=(SELECT MAX(ID_EJECUCION) FROM CO_BITACORA_REPORTE_ADIC
                                 WHERE FECHA_CORTE=PV_FECHACORTE AND ESTADO=PV_ESTADO);

             EXCEPTION
                WHEN OTHERS THEN
                   LV_ERROR:=' ERROR AL ACTUALIZAR EL CORTE'||PV_FECHACORTE;
                RAISE MI_ERROR;
             END;
   ---LLAMAR A MI PROCEDIMIENTO DE INGRESO

             COK_CARTERA_CLIENTES.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_REPROCESO,LV_ERROR);
               IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
               END IF;





   EXCEPTION
   WHEN MI_ERROR THEN
   PV_ERROR:= LV_ERROR  || SQLERRM;

   WHEN OTHERS THEN
   PV_ERROR:='ERROR AL Actualizar LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;


   END COP_ACTUALIZA_FECHA;

            /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que trunca las estructuras correspondientes a la fecha de corte y extrae la cuentas nuevamente
  --===============================================================================================
 */

   PROCEDURE COP_REPROCESA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS

   LV_QUERY VARCHAR2(4000);
   LV_ERROR VARCHAR2(4000);
   MI_ERROR EXCEPTION;

   BEGIN

        BEGIN

                LV_QUERY:='TRUNCATE TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                 LV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_REPORTE_ADIC_'||PV_FECHACORTE|| ''||  SQLERRM;
                 RAISE MI_ERROR;
         END;

          BEGIN

                LV_QUERY:='TRUNCATE TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                 LV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE|| ''||  SQLERRM;
                 RAISE MI_ERROR;
         END;

                   COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                  COK_CARTERA_CLIENTES.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_ESTADO,PV_OBSERVACION,'S',LV_ERROR);
                     IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                     END IF;


  EXCEPTION
  WHEN MI_ERROR THEN
    PV_ERROR:= 'ERROR EN PROCEDIMIENTO EXTRACCION PARA REPROCESO'||LV_ERROR;

  WHEN OTHERS THEN
  PV_ERROR:='COK_CARTERA_CLIENTES.COP_REPROCESA'||SQLERRM;


   END COP_REPROCESA;

   /*
  --===============================================================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que permite permite extraer las cuentas pertencientes a la co_repcarcli_ddmmyyyy
      y asignar el hilo correspondiente para su procesamiento
  --===============================================================================================
 */

 PROCEDURE COP_EXTRAE_CUENTAS(PV_FECHACORTE IN VARCHAR2,
                                PV_CICLO IN VARCHAR2,
                                PV_ERROR OUT VARCHAR2) IS

    CURSOR C_HILOS(CV_CICLO VARCHAR2) IS
    SELECT N_HILOS,N_HILOS_CORTE FROM CO_CICLOS_REPORTE_CLI
    WHERE ID_CICLO=CV_CICLO;
    
    CURSOR C_CAMPOS IS
     SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL AND VISIBLE ='S'
    UNION ALL 
    SELECT NOMBRE_CAMPO, TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE EXTRACCION='X' AND ESTADO='A' AND VISIBLE ='S' AND NOMBRE_ALTERNO IS NULL; 
    
    CURSOR C_CAMPOS1 IS 
   SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL AND VISIBLE ='S'
    UNION ALL
    SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS 
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL AND VISIBLE ='S';

       type t_cuenta is table of varchar2(2000) index by binary_integer;
    --lII           number;
    LC_CUENTA     t_cuenta;



  HILOS_D           NUMBER:=1;
  HILOS_C           NUMBER:=1;
  lt_customer_id  cot_string := cot_string();
  lt_totaladeuda  cot_string := cot_string();
  ln_cliente      cot_number := cot_number();
  LN_DEUDA  VARCHAR2(15);
  LV_CONTADOR VARCHAR2(4000):=NULL;
  LV_TIPO_DATO varchar2(4000):=NULL;
  lv_variable  varchar2(4000):=null;
  --lt_totaladeuda  cot_number:=cot_number();
  TOT_HILOS_D       NUMBER;
  TOT_HILOS_C       NUMBER;
  LV_QUERY        VARCHAR2(2000);
  LV_QUERY1       VARCHAR2(2000);
  H NUMBER:=0;
  SQL_ VARCHAR2(4000):=NULL;
  SQL1_ VARCHAR2(4000):=NULL;
  CUENTA VARCHAR2(10);
  MI_ERROR EXCEPTION;


      BEGIN

              OPEN  C_HILOS(PV_CICLO);
              FETCH C_HILOS INTO TOT_HILOS_D,TOT_HILOS_C;
              CLOSE C_HILOS;
              
              FOR I IN C_CAMPOS  LOOP
                  H:= H + 1;
                  IF SQL_ IS NOT NULL THEN
                  
                    SQL_ := SQL_ || ',';
                    LV_CONTADOR := LV_CONTADOR || ',';
                    lv_variable:= lv_variable || ',';
                  END IF;
                    LV_CONTADOR := LV_CONTADOR||':'||H; 
                    SQL_ := SQL_ || I.NOMBRE_CAMPO;
                  begin 
                        IF I.TIPO_DATO <> 'NUMBER' THEN
                           LV_TIPO_DATO:= 'LT'||I.NOMBRE_CAMPO ||'cot_string := cot_string()';
                        ELSE
                           LV_TIPO_DATO:= 'LT'||I.NOMBRE_CAMPO ||'cot_number := cot_number()';
                        END IF;
                      
                  EXECUTE IMMEDIATE LV_TIPO_DATO;  
                        
                  exception 
                    when others then 
                    null;
                  end;
                  lv_variable := lv_variable||' OUT  '||' LT'||I.NOMBRE_CAMPO;
                  
              END LOOP;
              
              FOR J IN C_CAMPOS1  LOOP
                  IF SQL1_ IS NOT NULL THEN
                  
                    SQL1_ := SQL1_ || ',';
                  
                  END IF;
                
                  SQL1_ := SQL1_ || J.NOMBRE_ALTERNO;
                  
              END LOOP;
              
              
              BEGIN   
                        LV_QUERY:=' BEGIN  SELECT '||SQL_||'  BULK COLLECT INTO'||LV_CONTADOR;   
                        LV_QUERY:= LV_QUERY ||' FROM CO_REPCARCLI_'||PV_FECHACORTE||' ;';   
                        LV_QUERY:= LV_QUERY ||'  END; ';   
       
                   EXECUTE IMMEDIATE LV_QUERY USING lv_variable;---OUT lt_customer_id, OUT lt_totaladeuda, OUT ln_cliente;   
               
                 
              EXCEPTION   
                 
                WHEN OTHERS THEN   
                 PV_ERROR:= 'ERROR AL EXTRAER LAS CUENTAS DE BSCS A AXIS- '||SQLERRM;   
                 RAISE MI_ERROR;   
              END;   
                 
               /*FOR I IN lt_customer_id.FIRST .. lt_customer_id.LAST   LOOP   
                   
                LN_DEUDA:=REPLACE(lt_totaladeuda(I),',','.');   
                   
          
                         BEGIN   
                            
                        LV_QUERY1:='INSERT INTO CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;   
                        LV_QUERY1:=LV_QUERY1 || '(HILO,CUENTA,CLIENTE,DEUDA,FECHA_INGRESO )';   
                        LV_QUERY1:=LV_QUERY1 ||' VALUES ( '||HILOS_C||','''||lt_customer_id(I)||''','||ln_cliente(I)||','''||LN_DEUDA||''',sysdate)';   
                                       
                                EXECUTE IMMEDIATE LV_QUERY1;   
                        EXCEPTION   
                          WHEN OTHERS THEN    
                          PV_ERROR:= 'ERROR AL INSERTAR LA INFORMACION'||SQLERRM;   
                          RAISE MI_ERROR;   
                        END;      
                                
                                          
                          IF HILOS_C = TOT_HILOS_C THEN   
                                COMMIT;   
                                HILOS_C := 1;   
                          ELSE    
                                  
                          HILOS_C := HILOS_C + 1;   
                          END IF;   
                  
                 END LOOP;*/   
                 
    
 
 
                /*BEGIN
                    LV_QUERY := ' INSERT \*+ APPEND*\ INTO CO_REPORTE_ADIC_CTAS_' || PV_FECHACORTE;
                    LV_QUERY := LV_QUERY || '( '||SQL1_||')(';
                    LV_QUERY := LV_QUERY || ' SELECT  ' || SQL_ || ' FROM CO_REPCARCLI_' ||PV_FECHACORTE||' ) ';
      
                  EXECUTE IMMEDIATE LV_QUERY;
                  COMMIT;
              EXCEPTION

                WHEN OTHERS THEN
                 PV_ERROR:= 'ERROR AL EXTRAER LAS CUENTAS DE BSCS A AXIS- '||SQLERRM;
                 RAISE MI_ERROR;
              END;*/

              /* BEGIN
                        LV_QUERY:=' BEGIN  SELECT  A.CLIENTE  BULK COLLECT INTO :1 ';
                        LV_QUERY:= LV_QUERY ||' FROM CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE||' A;';
                        LV_QUERY:= LV_QUERY ||'  END; ';

                   EXECUTE IMMEDIATE LV_QUERY USING OUT lt_customer_id;


              EXCEPTION

                WHEN OTHERS THEN
                 PV_ERROR:= 'ERROR AL EXTRAER LAS CUENTAS DE BSCS A AXIS- '||SQLERRM;
                 RAISE MI_ERROR;
              END;*/



                /*FOR I IN lt_customer_id.FIRST .. lt_customer_id.LAST   LOOP


                         BEGIN

                        LV_QUERY1:='UPDATE  CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;
                        LV_QUERY1:=LV_QUERY1 || ' SET HILO ='||HILOS_C;
                        LV_QUERY1:=LV_QUERY1 ||' WHERE CUENTA='''||lt_customer_id(I)||'''';  

                            EXECUTE IMMEDIATE LV_QUERY1;
                            
                        EXCEPTION
                          WHEN OTHERS THEN
                          PV_ERROR:= 'ERROR ASIGNAR EL INDICE '||SQLERRM;
                          RAISE MI_ERROR;
                        END;


                          IF HILOS_C = TOT_HILOS_C THEN
                                COMMIT;
                                HILOS_C := 1;
                          ELSE

                          HILOS_C := HILOS_C + 1;
                          END IF;

                 END LOOP;*/


                   commit;


    EXCEPTION

    WHEN MI_ERROR THEN
            PV_ERROR:=PV_ERROR;


         WHEN OTHERS THEN
          PV_ERROR:= ' ERROR EN EL PROCEDIMIENTO COK_DETALLE_CLIENTES_NEW.COP_EXTRAE_CUENTAS '||SQLERRM;


   END COP_EXTRAE_CUENTAS;

    /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009

      Procedimiento que crea la estructura llamada CO_REPORTE_ADIC_DDMMYY
      la cual se llena dinamicamente mediante la estructura co_report_cli_parametros
      tambien crea la estructura co_reporte_adic_ctas la cual cotendra cuentas de
      la co_repcarcli_ddmmyyyy a procesar
   */

  PROCEDURE CREA_TABLA1(PV_FECHACORTE IN VARCHAR2,
                        PV_ERROR OUT  VARCHAR2) IS

     SQL_SENTECIA VARCHAR2(5000);
     SQL1_ VARCHAR2(5000):=NULL;
     SQL_ VARCHAR2(5000):=NULL;
     
     -- OBTIENE LOS INDICES 
     CURSOR C_INDICES IS 
        SELECT NOMBRE_ALTERNO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;
        
     -- OBTIENE CAMPOS DE SISTEMAS PARA EL PROCESO
     CURSOR C_SISTEMAS IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;
        
     -- OBTINENE CAMPOS A PROCESAR ADIONCIONALES
    CURSOR C_ADICIONALES IS
    SELECT NOMBRE_CAMPO, TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE ADICIONAL='X' AND ESTADO='A';
    
     -- OBTINEE CAMPOS REQUERIDOS PARA LA EXTRACCION
    CURSOR C_EXTRACCION IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;

    -- VEIRIFICA SI LA ESTRUCTURA YA EXISTE EN LA BASE
    CURSOR EXISTE_TABLA (CV_TABLA VARCHAR2) IS
    SELECT 'X' FROM ALL_TABLES A
    WHERE  A.TABLE_NAME= CV_TABLA;
    
 

     LV_QUERY VARCHAR2(5000);
     LV_SEPARADOR VARCHAR2(1):=NULL;
     EXISTE VARCHAR2(1);
     MI_ERROR EXCEPTION;

     BEGIN

               OPEN  EXISTE_TABLA('CO_REPORTE_ADIC_'||PV_FECHACORTE);
               FETCH EXISTE_TABLA INTO EXISTE;
               CLOSE EXISTE_TABLA;

               IF EXISTE='X' THEN
                     BEGIN
                        LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;
                        EXECUTE IMMEDIATE LV_QUERY;
                     EXCEPTION
                     WHEN OTHERS THEN
                       NULL; 
                     END; 
               END IF;


         

         BEGIN
                   SQL_SENTECIA:=' CREATE TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;
                 --  SQL_SENTECIA:=SQL_SENTECIA ||' (HILO NUMBER, HILO_DIARIO NUMBER,CUENTA VARCHAR2(15), TIPO_INGRESO NUMBER ,';        
                 --  SQL_SENTECIA:=SQL_SENTECIA || SQL_ ||' , CLIENTE number ,DEUDA NUMBER default ''0'' ,ESTADO VARCHAR2(1), FECHA_INGRESO DATE,FECHA_ACTUALIZACION DATE, ID_CONTRATO NUMBER, FECHA_ACTUALIZACION_ESTADO DATE ) ';                     

                       FOR I IN C_SISTEMAS
                       LOOP

                       IF SQL1_ IS NOT NULL THEN
                           SQL1_:=SQL1_ ||',';
                       END IF;

                         SQL1_:=SQL1_ || I.NOMBRE_ALTERNO ||' '||I.TIPO_DATO;

                       END LOOP;
                       
                       FOR j IN C_ADICIONALES
                       LOOP

                       IF SQL_ IS NOT NULL THEN
                           SQL_:=SQL_ ||',';
                       END IF;

                         SQL_:=SQL_ || j.NOMBRE_CAMPO ||' '||j.TIPO_DATO;

                       END LOOP;
                       
                        IF   SQL1_ IS NULL THEN
                        LV_SEPARADOR:= NULL;
                       ELSIF   SQL_ IS NULL THEN
                        LV_SEPARADOR:= NULL;
                       ELSE
                        LV_SEPARADOR:=',';
                       END IF;  
                       SQL_SENTECIA:=SQL_SENTECIA ||'('|| SQL1_ ||LV_SEPARADOR|| SQL_ ||' )';
             
                     SQL_SENTECIA:= SQL_SENTECIA || ' tablespace DATA  pctfree 10' || -- SOLO DESARROLLO
               --     SQL_SENTECIA:= SQL_SENTECIA || ' tablespace REP_DATA  pctfree 10' ||
                                 '  pctused 40  initrans 1  maxtrans 255' ||
                                 '   storage (initial 9360K'||
                                 '   next 1040K'||
                                 '   minextents 1'||
                                 '    maxextents unlimited'||
                                 '   pctincrease 0)';

                     EXECUTE IMMEDIATE SQL_SENTECIA;
       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

       FOR H IN C_INDICES
       LOOP

                BEGIN
                     SQL_SENTECIA := 'create index IDX_ADIC'||SUBSTR(H.NOMBRE_ALTERNO,0,6)||PV_FECHACORTE ||
                                 ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' ||
                                    'tablespace DATA '|| -- SOLO DESARROLLO
                                  --    'tablespace REP_IDX '||
                                        'pctfree 10 '||
                                        'initrans 2 '||
                                        'maxtrans 255 '||
                                        'storage '||
                                        '( initial 256K '||
                                        '  next 256K '||
                                        '  minextents 1 '||
                                        '  maxextents unlimited '||
                                        '  pctincrease 0 )';

                 EXECUTE IMMEDIATE SQL_SENTECIA;

                 EXCEPTION
                     WHEN OTHERS THEN
                     PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
                     RAISE MI_ERROR;
                 END;
       END LOOP;

      /*  BEGIN
       SQL_SENTECIA := 'create index IDX_ADICCLI_'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (CLIENTE)' ||
                        'tablespace DATA '|| -- SOLO DESARROLLO
                      --    'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;


       BEGIN
       SQL_SENTECIA := 'create index IDX_ADICHILO_'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (HILO)' ||
                         'tablespace DATA '|| -- SOLO DESARROLLO
                     --      'tablespace REP_IDX '|| -- PRODUCCION TABLE SPACE
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

       BEGIN
       SQL_SENTECIA := 'create index IDX_ADICHILO_DIARI'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (HILO_DIARIO)' ||
                         'tablespace DATA '|| -- SOLO DESARROLLO
                     --  'tablespace REP_IDX '|| -- PRODUCCION TABLE SPACE
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;
*/

        BEGIN
       SQL_SENTECIA := 'create or replace public synonym co_reporte_adic_'||PV_FECHACORTE;
       SQL_SENTECIA := SQL_SENTECIA|| ' for SYSADM.co_reporte_adic_'||PV_FECHACORTE;


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL SINONIMO A LA TABLA CO_REPORTE_ADIC'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

        BEGIN
       SQL_SENTECIA := 'grant select, insert, update, delete, references, alter, index on CO_REPORTE_ADIC_'||PV_FECHACORTE||' to PUBLIC ';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL ASIGNAR PERMISOS A LA TABLA CO_REPORTE_ADIC'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;


           BEGIN


                LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                null;
         END;

                 SQL1_:=NULL;
                   
                 FOR k IN C_EXTRACCION  LOOP
                    
                    IF SQL1_ IS NOT NULL THEN
                    
                      SQL1_ := SQL1_ || ',';
                    
                    END IF;
                  
                    SQL1_ := SQL1_ || k.NOMBRE_ALTERNO ||' '||k.TIPO_DATO;
                    
                  END LOOP;     
              
                   BEGIN
                               SQL_SENTECIA:=' CREATE TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;
                               SQL_SENTECIA:=SQL_SENTECIA ||' (' || SQL1_||')';
                               

                                SQL_SENTECIA:= SQL_SENTECIA || ' tablespace DATA  pctfree 10' || -- SOLO DESARROLLO
                           --     SQL_SENTECIA:= SQL_SENTECIA || ' tablespace REP_DATA  pctfree 10' ||
                                             '  pctused 40  initrans 1  maxtrans 255' ||
                                             '   storage (initial 9360K'||
                                             '   next 1040K'||
                                             '   minextents 1'||
                                             '    maxextents unlimited'||
                                             '   pctincrease 0)';

                                 EXECUTE IMMEDIATE SQL_SENTECIA;
                   EXCEPTION
                       WHEN OTHERS THEN
                       PV_ERROR:= 'ERROR AL CREAR LA TABLA EN BSCS CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE||' ' || SQLERRM;
                       RAISE MI_ERROR;
                   END;


       EXCEPTION
        WHEN MI_ERROR THEN

         NULL;


          WHEN OTHERS THEN
          PV_ERROR:='ERROR EN PROCEDIMIENTO COK_CARTERA_CLIENTES.CREA_TABLA1 ' || SQLERRM;


     END CREA_TABLA1;

end;
/
