CREATE OR REPLACE PACKAGE RC_EXTRAE_DATOS_BSCS is



  TYPE TYPE_VARCHAR_63 IS TABLE OF VARCHAR(63) INDEX BY BINARY_INTEGER;
  TYPE TYPE_VARCHAR_1 IS TABLE OF VARCHAR(1) INDEX BY BINARY_INTEGER;
  TYPE TYPE_VARCHAR_2 IS TABLE OF VARCHAR(2) INDEX BY BINARY_INTEGER;
  TYPE TYPE_NUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
--  TYPE TYPE_FLOAT IS TABLE OF FLOAT INDEX BY BINARY_INTEGER;
  TYPE TYPE_DATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

 PROCEDURE RC_SAVE_STEP (PN_RC_PROCESS NUMBER,
                         PD_FECHA_START  DATE,
                         PD_FECHA_STOP   DATE,
                         PV_STATUS       VARCHAR2,
                         PV_ERROR        VARCHAR2);

 PROCEDURE RC_VALIDATE_CONTROL_PROCESS(LN_RC_PROCESS    IN NUMBER,
                                       LV_ESTADO        IN VARCHAR2,
                                       LV_OUT_ERROR     OUT VARCHAR2);


 PROCEDURE RC_VALIDATE_DAT(PN_RC_PROCESS    IN NUMBER,
                           PV_OUT_ERROR       OUT VARCHAR2);

 PROCEDURE RC_UPDATE_CUSTOMER_1(PN_RC_PROCESS      IN NUMBER,
                                PV_TABLE_CUSTOMER  IN VARCHAR2,
                                PD_PERIODO         DATE,
                                PV_MARCA           IN VARCHAR2 DEFAULT NULL,
                                PN_CANT_DIAS    IN NUMBER,
                                PV_OUT_ERROR       OUT VARCHAR2);


 PROCEDURE RC_UPDATE_CUSTOMER_2(PN_RC_PROCESS      IN NUMBER,
                                 PV_OUT_ERROR      OUT VARCHAR2);

 PROCEDURE RC_DISTRIBUTION_CUSTOMER(PN_RC_PROCESS    IN NUMBER,
                                    PN_CANT_PROCESS_ID IN NUMBER,
                                    PV_OUT_ERROR       OUT VARCHAR2);

 PROCEDURE RC_DISTRIBUTION_CUSTOMER_3(PN_RC_PROCESS    IN NUMBER,
                                      PN_CANT_PROCESS_ID IN NUMBER,
                                      PV_OUT_ERROR       OUT VARCHAR2);

 PROCEDURE RC_SEND_1(PN_RC_PROCESS    IN NUMBER,
                     PN_PROCESS_ID    IN NUMBER,
                     PV_OUT_ERROR     OUT VARCHAR2);

 PROCEDURE RC_SEND_2(PN_RC_PROCESS    IN NUMBER,
                      PN_PROCESS_ID    IN NUMBER,
                      PV_OUT_ERROR     OUT VARCHAR2);

 PROCEDURE RC_SEND_3(PN_RC_PROCESS    IN NUMBER,
                      PN_PROCESS_ID    IN NUMBER,
                      PV_OUT_ERROR     OUT VARCHAR2);

 PROCEDURE RC_HEADER(PN_RC_PROCESS IN NUMBER,
                     PD_PERIODO     IN   VARCHAR2,
                     PV_MARCA      IN VARCHAR2 DEFAULT NULL,
                     PV_OUT_ERROR    OUT VARCHAR2);


 FUNCTION RC_RETORNA_CANT_PROCESS(PV_PROCESS_ID IN NUMBER,
                                  PV_ERROR      OUT VARCHAR2) RETURN NUMBER ;


 -----------------------------------------------------------------
 PROCEDURE RC_GENERA_RCFECHAS(PV_ERROR       OUT VARCHAR2);

END RC_EXTRAE_DATOS_BSCS;
/
CREATE OR REPLACE PACKAGE BODY RC_EXTRAE_DATOS_BSCS is
 
/*********************************************************************************************************
  LIDER SIS:      JUAN DAVID PÿREZ
  LIDER IRO:      VERÿNICA OLIVO BACILIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       8693 RELOJ DE COBRANZA DTH
  FECHA:          06/05/2013
  PROPOSITO:      Modificacion del procedimiento RC_SEND_1 para que diferencie entre cuentas DTH y movil
  *********************************************************************************************************/

/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      PATRICIA ASENCIO 
  MODIFICADO POR: IRO JOHANNA JIMENEZ 
  PROYECTO:       9464 MEJORAS AL PROCESO DE RELOJ DE COBRANZA
  FECHA:          04/02/2014
  PROPOSITO:      Modificacion del procedimiento RC_SEND_1 para obtener de la tabla orderhdr_all el valor 
                  de la ultima factura del cliente
  *********************************************************************************************************/

/*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO ANABELL AMAYQUEMA
  PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          17/10/2014
  PROPOSITO:      Modificación del cursor C_DEUDA para obtener de la tabla orderhdr_all solo la deuda de
                  saldos pendientes(>0)
  *********************************************************************************************************/
  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO Jordan Rodriguez
  PROYECTO:       10324 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          29/05/2015
  PROPOSITO:      Modificacion del procedimiento RC_SEND_1 al cual se le agrego un cursor para que obtenga la
                  ultima fecha de actualizacion de la tabla orderhdr_all.
  *********************************************************************************************************/

  ------------------------------------------------------
  --- VARIABLES GENERALES
  ------------------------------------------------------
  -- RFE
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  GV_CODES_INI TDATE;


  ------------------------------------------------------
  --- PROCEDIMIENTOS
  ------------------------------------------------------


  --1--
 PROCEDURE RC_SAVE_STEP (PN_RC_PROCESS NUMBER,
                         PD_FECHA_START  DATE,
                         PD_FECHA_STOP   DATE,
                         PV_STATUS       VARCHAR2,
                         PV_ERROR        VARCHAR2) IS
  BEGIN

     INSERT INTO READ.RC_PROCESS_STEP_LOG
                 (RC_PROCESS,DES_EVENTO,DATE_EXEC_START,DATE_EXEC_STOP,STATUS)
     VALUES
                 (PN_RC_PROCESS,
                  PV_ERROR,
                  PD_FECHA_START,
                  PD_FECHA_STOP,
                  PV_STATUS
                 );
      COMMIT;

  END RC_SAVE_STEP;

  --2--
  PROCEDURE RC_VALIDATE_CONTROL_PROCESS(LN_RC_PROCESS    IN NUMBER,
                                        LV_ESTADO        IN VARCHAR2,
                                        LV_OUT_ERROR     OUT VARCHAR2) IS

      CURSOR C_RC_CONTROL_PROCESOS IS
            SELECT PROCESS_STATUS
              FROM READ.RC_CONTROL_PROCESOS
             WHERE RC_PROCESS      = LN_RC_PROCESS;


      LB_NOTFOUND              BOOLEAN;
      LC_DAT_CONTROL_PROCESS   C_RC_CONTROL_PROCESOS%ROWTYPE;
      LE_ERROR                 EXCEPTION;

      BEGIN

        OPEN C_RC_CONTROL_PROCESOS;
       FETCH C_RC_CONTROL_PROCESOS INTO LC_DAT_CONTROL_PROCESS;
       LB_NOTFOUND:=C_RC_CONTROL_PROCESOS%NotFound;
       CLOSE C_RC_CONTROL_PROCESOS;
       IF LB_NOTFOUND THEN
          LV_OUT_ERROR:='RC_VALIDATE_CONTROL_PROCESS- ADVERTENCIA NO EXISTEN DATOS C_RC_CONTROL_PROCESOS';
          RAISE LE_ERROR;
       ELSE
          IF LC_DAT_CONTROL_PROCESS.PROCESS_STATUS=LV_ESTADO THEN
             LV_OUT_ERROR:='RC_VALIDATE_CONTROL_PROCESS- ADVERTENCIA EL PROCESO SE ENCUENTRA EN EJECUCION';
             RAISE LE_ERROR;
          END IF;
       END IF;

       LV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN LE_ERROR THEN
           NULL;

      WHEN OTHERS THEN
           LV_OUT_ERROR := 'RC_VALIDATE_CONTROL_PROCESS- ' || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);

  END RC_VALIDATE_CONTROL_PROCESS;


  --3--
  PROCEDURE RC_VALIDATE_DAT(PN_RC_PROCESS      IN NUMBER,
                            PV_OUT_ERROR       OUT VARCHAR2) IS

      CURSOR C_RC_PROCESS IS
             SELECT RC_PROCESS,
                    CANT_PROCESS_ID,
                    CANT_REG_COMMIT,
                    TABLE_ORIGEN,
                    TABLE_DESTINO
               FROM READ.RC_PROCESS
              WHERE RC_PROCESS=PN_RC_PROCESS;

      CURSOR C_RC_CONTROL IS
             SELECT FECHA_REFERENCIA
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS     = PN_RC_PROCESS
                AND PROCESS_STATUS = 'R'
                AND ROWNUM=1
              ORDER BY  PER_START;

      LB_NOTFOUND              BOOLEAN;
      LC_DAT_PROCESS           C_RC_PROCESS%ROWTYPE;
      LC_DAT_CONTROL           C_RC_CONTROL%ROWTYPE;
      LE_ERROR                 EXCEPTION;

      BEGIN

        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO LC_DAT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_VALIDATE_DAT- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_PROCESS';
          RAISE LE_ERROR;
       END IF;

        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_VALIDATE_DAT- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;

       IF LC_DAT_PROCESS.TABLE_ORIGEN IS NULL THEN
          PV_OUT_ERROR:='RC_VALIDATE_DAT: NO EXISTE EL NOMBRE DE LA TABLA ORIGEN DE LOS DATOS EN EL CAMPO "TABLE_ORIGEN" DE LA TABLA "RC_PROCESS"';
          RAISE LE_ERROR;
       END IF;


       IF LC_DAT_PROCESS.TABLE_DESTINO IS NULL THEN
          PV_OUT_ERROR:='RC_VALIDATE_DAT: NO EXISTE EL NOMBRE DE LA TABLA DESTINO DE LOS DATOS EN EL CAMPO "TABLE_DESTINO" DE LA TABLA "RC_PROCESS"';
          RAISE LE_ERROR;
       END IF;

       RC_VALIDATE_CONTROL_PROCESS(LN_RC_PROCESS      => PN_RC_PROCESS,
                                   LV_ESTADO          => 'E',
                                   LV_OUT_ERROR       => PV_OUT_ERROR);
       IF NOT PV_OUT_ERROR IS NULL THEN
           RAISE LE_ERROR;
       END IF;

       UPDATE READ.RC_CONTROL_PROCESOS
          SET PROCESS_STATUS        = 'E',
              PROCCESS_LAST_RUNDATE = SYSDATE
        WHERE RC_PROCESS            = PN_RC_PROCESS
          AND PROCESS_STATUS        = 'R';


       COMMIT;

       PV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN LE_ERROR THEN
           NULL;

      WHEN OTHERS THEN
           PV_OUT_ERROR := SQLCODE || 'RC_VALIDATE_DAT- ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

  END RC_VALIDATE_DAT;



  --5--
  PROCEDURE RC_UPDATE_CUSTOMER_1(PN_RC_PROCESS      IN NUMBER,
                                 PV_TABLE_CUSTOMER  IN VARCHAR2,
                                 PD_PERIODO         DATE,
                                 PV_MARCA           IN VARCHAR2 DEFAULT NULL,
                                 PN_CANT_DIAS    IN NUMBER,
                                 PV_OUT_ERROR       OUT VARCHAR2) IS
      LD_FECHA_START  DATE;
      LV_SQL          VARCHAR2(700);
      LV_CHAR                VARCHAR2(2);

      BEGIN
       LD_FECHA_START := SYSDATE;
       LV_CHAR        := '''';

       LV_SQL:= 'alter session set NLS_DATE_FORMAT =' || LV_CHAR || 'DD/MM/YYYY' || LV_CHAR ;
       EXECUTE IMMEDIATE LV_SQL;

       LV_SQL:= 'DELETE READ.RC_CUSTOMER_WORK WHERE RC_PROCESS=' || PN_RC_PROCESS;
       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       ----------------------------------------------------------------
       ----[3569]--Mejoras en la Optimización del proceso de Mensajería
       ----JRO: 02/07/2008
       ----------------------------------------------------------------
     --1--LLENA LA TABLA DE TRABAJO

      LV_SQL:= 'INSERT INTO READ.RC_CUSTOMER_WORK (RC_PROCESS,CUSTOMER_ID,CUSTCODE,CANT_CONTRATOS)';
      LV_SQL:= LV_SQL || ' SELECT ' || PN_RC_PROCESS ||',0,B.RC_CUENTA,0 ';
      LV_SQL:= LV_SQL || '   FROM ' || PV_TABLE_CUSTOMER || ' B ';
      LV_SQL:= LV_SQL || '   WHERE B.RC_FECHA_MENSAJE >= TO_DATE(' || LV_CHAR || PD_PERIODO|| LV_CHAR ||','|| LV_CHAR ||'DD/MM/YYYY'|| LV_CHAR ||')';
      LV_SQL:= LV_SQL || '    AND B.RC_FECHA_MENSAJE <= TO_DATE(' || LV_CHAR || (trunc(SYSDATE)-PN_CANT_DIAS)|| LV_CHAR ||','|| LV_CHAR ||'DD/MM/YYYY'|| LV_CHAR ||')';
      LV_SQL:= LV_SQL || '    AND B.RC_ENVIO_MENSAJE = '|| LV_CHAR || PV_MARCA || LV_CHAR;
      LV_SQL:= LV_SQL || '    AND B.RC_REG_PROCESADO  IS NULL';

      EXECUTE IMMEDIATE LV_SQL;
      COMMIT;

      --2--ACTUALIZA EL CUSTOMER_ID
      LV_SQL:= 'UPDATE READ.RC_CUSTOMER_WORK CW ';
      LV_SQL:= LV_SQL || ' SET CW.CUSTOMER_ID=(SELECT B.CUSTOMER_ID ';
      LV_SQL:= LV_SQL || '                      FROM SYSADM.CUSTOMER_ALL B ';
      LV_SQL:= LV_SQL || '                     WHERE B.CUSTCODE = CW.CUSTCODE ';
      LV_SQL:= LV_SQL || '                    ) ';
      LV_SQL:= LV_SQL || ' WHERE CW.RC_PROCESS =' || PN_RC_PROCESS;

      EXECUTE IMMEDIATE LV_SQL;
      COMMIT;

      --3--ACTUALIZA LA CANTIDAD DE CONTRATO DE LAS ESTADISTICAS
      LV_SQL:= ' UPDATE READ.RC_CUSTOMER_WORK CW ';
      LV_SQL:= LV_SQL || '  SET CW.CANT_CONTRATOS=(SELECT NVL(B.CANT_CONTRATOS,0) ';
      LV_SQL:= LV_SQL || '                             FROM READ.RC_CUSTOMER_EST B ';
      LV_SQL:= LV_SQL || '                            WHERE B.CUSTOMER_ID=CW.CUSTOMER_ID ';
      LV_SQL:= LV_SQL || '                              AND B.CUSTCODE = CW.CUSTCODE ';
      LV_SQL:= LV_SQL || '                              AND B.RC_PROCESS = ' || PN_RC_PROCESS;
      LV_SQL:= LV_SQL || '                            ) ';
      LV_SQL:= LV_SQL || '   WHERE CW.RC_PROCESS =' || PN_RC_PROCESS;

      EXECUTE IMMEDIATE LV_SQL;
      COMMIT;


      --4--ACTUALIZA A 0 LOS CONTRATOS NO CONSIDERADOS
      LV_SQL:= ' UPDATE READ.RC_CUSTOMER_WORK CW ';
      LV_SQL:= LV_SQL || '     SET CW.CANT_CONTRATOS=0 ';
      LV_SQL:= LV_SQL || '   WHERE CW.RC_PROCESS = ' || PN_RC_PROCESS;
      LV_SQL:= LV_SQL || '     AND CW.CANT_CONTRATOS IS NULL ';

      EXECUTE IMMEDIATE LV_SQL;
      COMMIT;

      --5--ACTUALIZA LAS ESTADISTICAS
      LV_SQL:= ' INSERT INTO READ.RC_CUSTOMER_EST (RC_PROCESS,CUSTOMER_ID,CUSTCODE,CANT_CONTRATOS,DATE_UPDATE,DURATION) ';
      LV_SQL:= LV_SQL || '  SELECT RC_PROCESS,CUSTOMER_ID,CUSTCODE,0,NULL,0 ';
      LV_SQL:= LV_SQL || '   FROM READ.RC_CUSTOMER_WORK  ';
      LV_SQL:= LV_SQL || '  WHERE RC_PROCESS = ' || PN_RC_PROCESS;
      LV_SQL:= LV_SQL || '  MINUS ';
      LV_SQL:= LV_SQL || '  SELECT /*+ INDEX(A,PK_RC_CUSTOMER_EST) */ RC_PROCESS,CUSTOMER_ID,CUSTCODE,0,NULL,0  ';
      LV_SQL:= LV_SQL || '    FROM READ.RC_CUSTOMER_EST A ';
      LV_SQL:= LV_SQL || '   WHERE RC_PROCESS = ' || PN_RC_PROCESS;


       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       LV_SQL:= 'UPDATE READ.RC_CUSTOMER_WORK A ';
       LV_SQL:= LV_SQL || ' SET A.CUSTOMER_ID_HIGH = ( ';
       LV_SQL:= LV_SQL || '                       SELECT B.CUSTOMER_ID ';
       LV_SQL:= LV_SQL || '                         FROM CUSTOMER_ALL B ';
       LV_SQL:= LV_SQL || '                        WHERE B.CUSTOMER_ID_HIGH = A.CUSTOMER_ID ';
       LV_SQL:= LV_SQL || '                          )';

       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       LV_SQL:= 'UPDATE READ.RC_CUSTOMER_WORK A ';
       LV_SQL:= LV_SQL || ' SET A.CUSTOMER_ID_HIGH = ( ';
       LV_SQL:= LV_SQL || '                  SELECT B.CUSTOMER_ID ';
       LV_SQL:= LV_SQL || '                    FROM CUSTOMER_ALL B ';
       LV_SQL:= LV_SQL || '                   WHERE B.CUSTOMER_ID = A.CUSTOMER_ID ';
       LV_SQL:= LV_SQL || '                          ) ';
       LV_SQL:= LV_SQL || '  WHERE A.CUSTOMER_ID_HIGH IS NULL';

       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                     PD_FECHA_START  => LD_FECHA_START,
                     PD_FECHA_STOP   => SYSDATE,
                     PV_STATUS       => 'OK',
                     PV_ERROR        => 'RC_UPDATE_CUSTOMER-SUCESSFUL'
                     );

      PV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN OTHERS THEN           
           PV_OUT_ERROR := 'RC_UPDATE_CUSTOMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

  END RC_UPDATE_CUSTOMER_1;


 --JRO OPCION PARA MEJORAR TIEMPOS EN LA REACTIVACION
 --22/03/08 AUT. EGA Y VSO
  PROCEDURE RC_UPDATE_CUSTOMER_2(PN_RC_PROCESS      IN NUMBER,
                                 PV_OUT_ERROR       OUT VARCHAR2) IS
      LD_FECHA_START  DATE;
      LV_SQL          VARCHAR2(700);
      LV_CHAR                VARCHAR2(2);

      BEGIN
       LD_FECHA_START := SYSDATE;
       LV_CHAR        := '''';

       LV_SQL:= 'alter session set NLS_DATE_FORMAT =' || LV_CHAR || 'DD/MM/YYYY' || LV_CHAR ;
       EXECUTE IMMEDIATE LV_SQL;

       LV_SQL:= 'DELETE READ.RC_CUSTOMER_WORK WHERE RC_PROCESS=' || PN_RC_PROCESS;
       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       LV_SQL:= 'INSERT INTO READ.RC_CUSTOMER_WORK (RC_PROCESS,CUSTOMER_ID,CUSTOMER_ID_HIGH,CUSTCODE,CANT_CONTRATOS)';
       LV_SQL:= LV_SQL || ' SELECT DISTINCT ' || PN_RC_PROCESS ;
       LV_SQL:= LV_SQL || '        ,CU.CUSTOMER_ID,CU.CUSTOMER_ID_HIGH,CU.CUSTCODE,0';
       LV_SQL:= LV_SQL || '  FROM  CUSTOMER_ALL CU, ';
       LV_SQL:= LV_SQL || '        CONTRACT_ALL CO, ';
       LV_SQL:= LV_SQL || '        CURR_CO_STATUS CUR ';
       LV_SQL:= LV_SQL || '  WHERE CU.CUSTOMER_ID = CO.CUSTOMER_ID ';
       LV_SQL:= LV_SQL || '    AND CU.CSTYPE NOT IN (' || LV_CHAR || 'd' || LV_CHAR ||','|| LV_CHAR ||'o'|| LV_CHAR ||') ';
       LV_SQL:= LV_SQL || '    AND CO.CO_EXT_CSUIN IN (1, 2, 3, 4, 7)  ';
       LV_SQL:= LV_SQL || '    AND CO.CO_ID=CUR.CO_ID ';
       LV_SQL:= LV_SQL || '    AND CUR.CH_STATUS NOT IN ('|| LV_CHAR ||'d'|| LV_CHAR ||','|| LV_CHAR ||'o'|| LV_CHAR ||') ';

       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       --INSERT NUEVOS REGISTROS DE LA TABLA CUSTOMERs A LA TABLA LOCAL CUSTOMERS
       LV_SQL:= 'INSERT INTO READ.RC_CUSTOMER_EST (RC_PROCESS,CUSTOMER_ID,CUSTOMER_ID_HIGH,CUSTCODE,CANT_CONTRATOS,DATE_UPDATE,DURATION)';
       LV_SQL:= LV_SQL || ' SELECT ' || PN_RC_PROCESS ;
       LV_SQL:= LV_SQL || '        ,CUSTOMER_ID,CUSTOMER_ID_HIGH,CUSTCODE,0,NULL,0 ';
       LV_SQL:= LV_SQL || '  FROM  READ.RC_CUSTOMER_WORK ';
       LV_SQL:= LV_SQL || '  WHERE RC_PROCESS = ' || PN_RC_PROCESS;
       LV_SQL:= LV_SQL || ' MINUS ';
       LV_SQL:= LV_SQL || ' SELECT /*+ INDEX(A,PK_RC_CUSTOMER_EST) */ RC_PROCESS,CUSTOMER_ID,CUSTOMER_ID_HIGH,CUSTCODE,0,NULL,0 FROM READ.RC_CUSTOMER_EST A';
       LV_SQL:= LV_SQL || '  WHERE RC_PROCESS = ' || PN_RC_PROCESS;

       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;

       LV_SQL:= ' UPDATE READ.RC_CUSTOMER_WORK SET CUSTOMER_ID_HIGH=CUSTOMER_ID WHERE CUSTOMER_ID_HIGH IS NULL AND RC_PROCESS = ' || PN_RC_PROCESS;

       EXECUTE IMMEDIATE LV_SQL;
       COMMIT;


       RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                     PD_FECHA_START  => LD_FECHA_START,
                     PD_FECHA_STOP   => SYSDATE,
                     PV_STATUS       => 'OK',
                     PV_ERROR        => 'RC_UPDATE_CUSTOMER-SUCESSFUL'
                     );

      PV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN OTHERS THEN
           PV_OUT_ERROR := 'RC_UPDATE_CUSTOMER- No existen Datos pendientes...Verifique en la rc_comentarios_cuenta'  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

  END RC_UPDATE_CUSTOMER_2;



  --7--
  PROCEDURE RC_DISTRIBUTION_CUSTOMER(PN_RC_PROCESS      IN NUMBER,
                                     PN_CANT_PROCESS_ID IN NUMBER,
                                     PV_OUT_ERROR       OUT VARCHAR2) IS

      CURSOR C_CUSTOMER_CONT(LN_CANTIDAD NUMBER) IS
             SELECT /*+ INDEX(A,PK_RC_CUSTOMER_EST) */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CANT_CONTRATOS,0
               FROM READ.RC_CUSTOMER_WORK A
              WHERE CANT_CONTRATOS > LN_CANTIDAD
                AND RC_PROCESS     = PN_RC_PROCESS
              ORDER BY  CANT_CONTRATOS ASC;

      CURSOR C_VERIFICA_CUSTOMER IS
             SELECT /*+ INDEX(A,PK_RC_CUSTOMER_EST) */  1
               FROM READ.RC_CUSTOMER_WORK A
            WHERE RC_PROCESS     = PN_RC_PROCESS;
            
      CURSOR C_RC_CONTROL IS
             SELECT SEQNO,FECHA_REFERENCIA,PER_START,PER_STOP
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS     = PN_RC_PROCESS
                AND PROCESS_STATUS = 'R'
                AND ROWNUM=1
              ORDER BY  PER_START;

      LD_FECHA_START        DATE;
      LC_DAT_CONTROL      C_RC_CONTROL%ROWTYPE;
      LE_ERROR                     EXCEPTION;
      LE_NO_DATA               EXCEPTION;
      LB_NOTFOUND           BOOLEAN;
      LN_CONT                      NUMBER;
      LN_CONT_EXCESO       NUMBER;
      LN_SUMA                      NUMBER :=0;


      TYPE T_CUSTOMERS IS TABLE OF READ.RC_CUSTOMER_EST.CUSTOMER_ID%TYPE
                       INDEX BY BINARY_INTEGER;
      V_CUSTOMERS           T_CUSTOMERS;
      V_CUSTOMERS_HIGH      T_CUSTOMERS;
      V_EST_CUST            T_CUSTOMERS;
      V_DISTRI              T_CUSTOMERS;

      LN_ID_SUM_SESIONES    DBMS_SQL.NUMBER_TABLE;

      BEGIN
       LD_FECHA_START := SYSDATE;

       DELETE READ.RC_PROCESS_CUST
        WHERE RC_PROCESS      = PN_RC_PROCESS;
       COMMIT;              
                   
        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_DISTRIBUTION_CUSTOMER- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;

       --VERIFICA SI EXISTEN DATOS POR PROCESAR
         OPEN C_VERIFICA_CUSTOMER;
        FETCH C_VERIFICA_CUSTOMER INTO LN_CONT;
        LB_NOTFOUND:=C_VERIFICA_CUSTOMER%NotFound;
        CLOSE C_VERIFICA_CUSTOMER;
        IF LB_NOTFOUND THEN
           PV_OUT_ERROR:='RC_DISTRIBUTION_CUSTOMER- ADVERTENCIA NO EXISTEN REGISTROS EN LA TABLA DE MENSAJERIA DE AXIS, RC_COMENTARIOS_CUENTA, THREAD: '||PN_CANT_PROCESS_ID;
           RAISE LE_NO_DATA;
        END IF;


       --ASIGNACION DE LOS CUSTOMER CON RELACION A LAS ESTADISTICAS--
         OPEN C_CUSTOMER_CONT(-1);
        FETCH C_CUSTOMER_CONT BULK COLLECT  INTO V_CUSTOMERS,V_CUSTOMERS_HIGH,V_EST_CUST, V_DISTRI;
        CLOSE C_CUSTOMER_CONT;

       IF LN_SUMA IS NULL THEN
          LN_SUMA := 0;
       ELSE
          LN_SUMA := TRUNC(LN_SUMA/(PN_CANT_PROCESS_ID));
       END IF;

       LN_CONT        := PN_CANT_PROCESS_ID;
       LN_CONT_EXCESO := PN_CANT_PROCESS_ID;

       FOR I IN 1..PN_CANT_PROCESS_ID LOOP
           LN_ID_SUM_SESIONES(I):=0;
       END LOOP;

       FOR I IN 1..V_CUSTOMERS.COUNT LOOP
            IF LN_ID_SUM_SESIONES(LN_CONT) <= LN_SUMA THEN
               V_DISTRI(I)                 := LN_CONT;
               LN_ID_SUM_SESIONES(LN_CONT) := LN_ID_SUM_SESIONES(LN_CONT) + V_EST_CUST(I);
            ELSE
               IF LN_CONT_EXCESO=0 THEN
                  LN_CONT_EXCESO:=PN_CANT_PROCESS_ID;
               END IF;
               V_DISTRI(I)       := LN_CONT_EXCESO;
               LN_ID_SUM_SESIONES(LN_CONT_EXCESO) := LN_ID_SUM_SESIONES(LN_CONT_EXCESO) + V_EST_CUST(I);
               LN_CONT_EXCESO    := LN_CONT_EXCESO - 1;
            END IF;
            LN_CONT:=LN_CONT-1;

            IF LN_CONT=0 THEN
               LN_CONT:=PN_CANT_PROCESS_ID;
            END IF;

       END LOOP;



       --INSERCION DE LOS REGISTROS
       FORALL I IN V_DISTRI.FIRST .. V_DISTRI.LAST
            INSERT INTO READ.RC_PROCESS_CUST(RC_PROCESS,
                                             CUSTOMER_ID,
                                             CUSTOMER_ID_HIGH,
                                             SEQNO,
                                             FECHA_REFERENCIA,
                                             PROCESS_ID,
                                             PROCESS_STATUS)
            VALUES(PN_RC_PROCESS,
                   V_CUSTOMERS(I),
                   V_CUSTOMERS_HIGH(I),
                   LC_DAT_CONTROL.SEQNO,
                   LC_DAT_CONTROL.FECHA_REFERENCIA,
                   V_DISTRI(I),
                   'R');


       --FIN--


       COMMIT;

       RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                     PD_FECHA_START  => LD_FECHA_START,
                     PD_FECHA_STOP   => SYSDATE,
                     PV_STATUS       => 'OK',
                     PV_ERROR        => 'RC_DISTRIBUTION_CUSTOMER-SUCESSFUL'
                     );

      PV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN LE_NO_DATA THEN
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );
             PV_OUT_ERROR:='NO_DATA';                        

      WHEN LE_ERROR THEN
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'RC_DISTRIBUTION_CUSTOMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

  END RC_DISTRIBUTION_CUSTOMER;


  PROCEDURE RC_DISTRIBUTION_CUSTOMER_3(PN_RC_PROCESS      IN NUMBER,
                                       PN_CANT_PROCESS_ID IN NUMBER,
                                       PV_OUT_ERROR       OUT VARCHAR2) IS

      CURSOR C_CUSTOMER_CONT(LN_CANTIDAD NUMBER) IS
             SELECT /*+ RULE +*/
                    RC_TIPO_RELOJ,
                    RC_PREDECESOR,
                    RC_DIA_EFECTIVO,
                    RC_DURACION,
                    RC_ETAPA,
                    0,
                    0
               FROM RC_RELOJ_ETAPA@AXIS A
              WHERE RC_PREDECESOR IN (1, 2, 3, 4, 7)
              ORDER BY  RC_TIPO_RELOJ, RC_ETAPA;

      CURSOR C_RC_CONTROL IS
             SELECT SEQNO,FECHA_REFERENCIA,PER_START,PER_STOP
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS     = PN_RC_PROCESS
                AND PROCESS_STATUS = 'R'
                AND ROWNUM=1
              ORDER BY  PER_START;

      LD_FECHA_START        DATE;
      LC_DAT_CONTROL        C_RC_CONTROL%ROWTYPE;
      LE_ERROR              EXCEPTION;
      LB_NOTFOUND           BOOLEAN;
      LN_CONT               NUMBER;
      LN_CONT_EXCESO        NUMBER;
      LN_SUMA               NUMBER :=0;


      TYPE T_CUSTOMERS IS TABLE OF READ.RC_CUSTOMER_EST.CUSTOMER_ID%TYPE
                       INDEX BY BINARY_INTEGER;
      V_RC_TIPO_RELOJ       T_CUSTOMERS;
      V_RC_PREDECESOR       T_CUSTOMERS;
      V_RC_DIA_EFECTIVO     T_CUSTOMERS;
      V_RC_DURACION         T_CUSTOMERS;
      V_RC_ETAPA            T_CUSTOMERS;
      V_EST_CUST            T_CUSTOMERS;
      V_DISTRI              T_CUSTOMERS;

      LN_ID_SUM_SESIONES    DBMS_SQL.NUMBER_TABLE;

      BEGIN
       LD_FECHA_START := SYSDATE;

        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_DISTRIBUTION_CUSTOMER- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;

       --ASIGNACION DE LOS CUSTOMER CON RELACION A LAS ESTADISTICAS--
         OPEN C_CUSTOMER_CONT(-1);
        FETCH C_CUSTOMER_CONT BULK COLLECT  INTO V_RC_TIPO_RELOJ,
                                                 V_RC_PREDECESOR,
                                                 V_RC_DIA_EFECTIVO,
                                                 V_RC_DURACION,
                                                 V_RC_ETAPA,
                                                 V_EST_CUST,
                                                 V_DISTRI;
        CLOSE C_CUSTOMER_CONT;

       IF LN_SUMA IS NULL THEN
          LN_SUMA := 0;
       ELSE
          LN_SUMA := TRUNC(LN_SUMA/(PN_CANT_PROCESS_ID));
       END IF;

       LN_CONT        := PN_CANT_PROCESS_ID;
       LN_CONT_EXCESO := PN_CANT_PROCESS_ID;

       FOR I IN 1..PN_CANT_PROCESS_ID LOOP
           LN_ID_SUM_SESIONES(I):=0;
       END LOOP;

       FOR I IN 1..V_RC_TIPO_RELOJ.COUNT LOOP
            IF LN_ID_SUM_SESIONES(LN_CONT) <= LN_SUMA THEN
               V_DISTRI(I)                 := LN_CONT;
               LN_ID_SUM_SESIONES(LN_CONT) := LN_ID_SUM_SESIONES(LN_CONT) + V_EST_CUST(I);
            ELSE
               IF LN_CONT_EXCESO=0 THEN
                  LN_CONT_EXCESO:=PN_CANT_PROCESS_ID;
               END IF;
               V_DISTRI(I)       := LN_CONT_EXCESO;
               LN_ID_SUM_SESIONES(LN_CONT_EXCESO) := LN_ID_SUM_SESIONES(LN_CONT_EXCESO) + V_EST_CUST(I);
               LN_CONT_EXCESO    := LN_CONT_EXCESO - 1;
            END IF;
            LN_CONT:=LN_CONT-1;

            IF LN_CONT=0 THEN
               LN_CONT:=PN_CANT_PROCESS_ID;
            END IF;

       END LOOP;



       --INSERCION DE LOS REGISTROS
       FORALL I IN V_DISTRI.FIRST .. V_DISTRI.LAST
            INSERT INTO READ.RC_PROCESS_CUST_ETAPA(RC_PROCESS,
                                                   RC_TIPO_RELOJ,
                                                   RC_PREDECESOR,
                                                   RC_DIA_EFECTIVO,
                                                   RC_DURACION,
                                                   RC_ETAPA,
                                                   SEQNO,
                                                   FECHA_REFERENCIA,
                                                   PROCESS_ID,
                                                   PROCESS_STATUS
                                                  )
            VALUES(PN_RC_PROCESS,
                   V_RC_TIPO_RELOJ(I),
                   V_RC_PREDECESOR(I),
                   V_RC_DIA_EFECTIVO(I),
                   V_RC_DURACION(I),
                   V_RC_ETAPA(I),
                   LC_DAT_CONTROL.SEQNO,
                   LC_DAT_CONTROL.FECHA_REFERENCIA,
                   V_DISTRI(I),
                   'R');


       --FIN--


       COMMIT;

       RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                     PD_FECHA_START  => LD_FECHA_START,
                     PD_FECHA_STOP   => SYSDATE,
                     PV_STATUS       => 'OK',
                     PV_ERROR        => 'RC_DISTRIBUTION_CUSTOMER-SUCESSFUL'
                     );

      PV_OUT_ERROR:=NULL;

  EXCEPTION
      WHEN LE_ERROR THEN
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'RC_DISTRIBUTION_CUSTOMER- '  || ' ERROR:'|| SQLCODE|| ' ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

  END RC_DISTRIBUTION_CUSTOMER_3;


  --8--
  PROCEDURE RC_SEND_1(PN_RC_PROCESS    IN NUMBER,
                      PN_PROCESS_ID    IN NUMBER,
                      PV_OUT_ERROR     OUT VARCHAR2) IS

    CURSOR C_CONTRACT(PN_CUSTOMER_ID NUMBER) IS
       SELECT
               CO.CO_ID,
               CO.CO_EXT_CSUIN,
               CO.PRODUCT_HISTORY_DATE,
               CO.TMCODE,
               CO.PLCODE,
               CUR.CH_STATUS,
               CS.MAIN_DIRNUM,
               CS.CS_DEACTIV_DATE,
               DI.DN_NUM,
               DI.DN_STATUS
        FROM  CONTRACT_ALL               CO,
              CURR_CO_STATUS             CUR,
              CONTR_SERVICES_CAP         CS,
              DIRECTORY_NUMBER           DI
       WHERE CO.CUSTOMER_ID   = PN_CUSTOMER_ID
         AND CO.CO_ID         = CUR.CO_ID
         AND CO.TMCODE        <> 268
         AND CUR.CH_STATUS NOT IN ('d', 'o')
         AND CO.CO_ID         = CS.CO_ID
         AND CS.SEQNO         = ( SELECT MAX ( CCL.SEQNO )
                                    FROM CONTR_SERVICES_CAP CCL
                                   WHERE CCL.CO_ID = CS.CO_ID)
        AND CS.DN_ID          = DI.DN_ID;
        
      --[8693]JJI - INICIO
      CURSOR C_CONTRACT_DTH(PN_CUSTOMER_ID NUMBER) IS
       SELECT
               CO.CO_ID,
               CO.CO_EXT_CSUIN,
               CO.PRODUCT_HISTORY_DATE,
               CO.TMCODE,
               CO.PLCODE,
               CUR.CH_STATUS,
               'X',
               NULL,
               'DTH',
               'a'
        FROM  CONTRACT_ALL               CO,
              CURR_CO_STATUS             CUR
       WHERE CO.CUSTOMER_ID   = PN_CUSTOMER_ID
         AND CO.CO_ID         = CUR.CO_ID
         AND CO.TMCODE        <> 268
         AND CUR.CH_STATUS NOT IN ('d', 'o');
         
      CURSOR C_PARAM_PRGCODE IS
      select valor
        from scp.scp_parametros_procesos
       where id_parametro = 'PRGCODE_DTH'
         and id_proceso = 'RELOJ_DTH';
      --[8693]JJI - FIN

      CURSOR C_CUSTOMER_P(PD_FECHA_PER  DATE,
                          VC_PN_SEQNO      NUMBER,
                          VC_PROCESS_ID    NUMBER) IS

             SELECT B.CUSTOMER_ID,
                    B.CUSTOMER_ID_HIGH,
                    A.CUSTCODE,
                    A.BILLCYCLE,
                    A.CSTYPE,
                    A.PRGCODE,
                    A.CSTRADECODE,
                    A.CUSTOMER_DEALER,
                    A.PREV_BALANCE,
                    B.PROCESS_ID
               FROM CUSTOMER_ALL A, READ.RC_PROCESS_CUST B
              WHERE A.CUSTOMER_ID             = B.CUSTOMER_ID
                AND B.RC_PROCESS              = PN_RC_PROCESS
                AND B.PROCESS_ID              = VC_PROCESS_ID
                AND B.SEQNO                   = VC_PN_SEQNO
                AND TRUNC(B.FECHA_REFERENCIA) = TRUNC(PD_FECHA_PER)
                AND B.PROCESS_STATUS          = 'R';


      CURSOR C_CUSTOMER_CTA(PN_CUSTOMER_ID NUMBER) IS
             SELECT A.CUSTCODE
               FROM CUSTOMER_ALL A
              WHERE A.CUSTOMER_ID = PN_CUSTOMER_ID;


        CURSOR C_RC_PROCESS_CUST(LV_SEQNO NUMBER,LV_FECHA DATE ) IS
             SELECT 1
               FROM READ.RC_PROCESS_CUST
              WHERE PROCESS_STATUS   = 'R'
                AND RC_PROCESS       = PN_RC_PROCESS
                AND SEQNO            = LV_SEQNO
                AND FECHA_REFERENCIA = LV_FECHA;

        CURSOR C_RC_PROCESS IS
             SELECT RC_PROCESS,
                    CANT_PROCESS_ID,
                    CANT_REG_COMMIT,
                    TABLE_ORIGEN,
                    TABLE_DESTINO
               FROM READ.RC_PROCESS
              WHERE RC_PROCESS = PN_RC_PROCESS;

      CURSOR C_RC_CONTROL IS
             SELECT CO.SEQNO,CO.FECHA_REFERENCIA,CO.PER_START,CO.PER_STOP
               FROM READ.RC_CONTROL CO
              WHERE CO.RC_PROCESS     = PN_RC_PROCESS
                AND CO.SEQNO=(SELECT MAX(CO2.SEQNO) FROM READ.RC_CONTROL CO2  WHERE CO2.RC_PROCESS=CO.RC_PROCESS)
                AND ROWNUM=1
              ORDER BY  CO.PER_START;


      CURSOR C_DEUDA(PN_CUSTOMER_ID NUMBER) IS
             SELECT /*+ INDEX(O,OH_CUST_ID_1)*/ NVL (SUM (DECODE (O.OHSTATUS, 'CO', 0, OHINVAMT_DOC)), 0)
               FROM SYSADM.ORDERHDR_ALL O
              WHERE O.CUSTOMER_ID = PN_CUSTOMER_ID
                AND O.OHSTATUS IN ('IN', 'CM', 'CO')
                AND O.OHOPNAMT_DOC > 0;--9833 AAM -Obtener solo la deuda de saldos pendientes-

      CURSOR C_SALDO(PN_CUSTOMER_ID NUMBER) IS
              SELECT /*+ INDEX(O,OH_CUST_ID_1)*/ NVL (SUM (O.OHOPNAMT_DOC), 0)
                FROM SYSADM.ORDERHDR_ALL O
               WHERE O.CUSTOMER_ID = PN_CUSTOMER_ID
                 AND O.OHSTATUS IN ('IN', 'CM', 'CO');
    --INICIO 10324 IRO_JRO CURSOR PARA OBTENER LA ULTIMA FECHA EN LA QUE FUE ACTULIZADA LA TABLA ORDERHDR_ALL 
      CURSOR C_FECHA(PN_CUSTOMER_ID NUMBER) IS
             SELECT nvl(y.lastmoddate, y.insertiondate) fecha
             FROM (SELECT/*+ INDEX(O,OH_CUST_ID_1)*/a.ohxact, a.customer_id
                    FROM SYSADM.orderhdr_all a
                    WHERE customer_id = PN_CUSTOMER_ID order by a.ohxact desc) x,
                   SYSADM.orderhdr_all y
              where x.customer_id = y.customer_id
              and x.ohxact = y.ohxact
              and rownum = 1;
    --FIN 10324 IRO_JRO
              
      CURSOR C_CREDITO(PN_CUSTOMER_ID NUMBER, PD_FECHA DATE) IS
              SELECT /*+ RULE +*/ NVL(SUM(AMOUNT), 0) CREDITOS
                FROM FEES B
               WHERE B.CUSTOMER_ID = PN_CUSTOMER_ID
                 AND B.PERIOD      > 0
                 AND B.SNCODE      = 46
                 AND TRUNC(B.ENTDATE) >= PD_FECHA;
                 
                 
     --[9464] INI JJI-Obtener valor de la untima factura
     CURSOR C_DEUDA_MES(PN_CUSTOMER_ID NUMBER) IS
       SELECT NVL(OHINVAMT_DOC, 0) deuda_mes
         FROM SYSADM.ORDERHDR_ALL O
        WHERE O.CUSTOMER_ID = PN_CUSTOMER_ID
          AND O.OHENTDATE = (SELECT MAX(RD.OHENTDATE)
                               FROM SYSADM.ORDERHDR_ALL RD
                              WHERE RD.CUSTOMER_ID = PN_CUSTOMER_ID
                                AND RD.OHSTATUS IN ('IN','CM')
                                AND RD.OHENTDATE <= SYSDATE);
     --[9260] FIN JJI


      CURSOR C_RC_CONTROL_EXEC_PROCESS_ID(LV_SEQNO NUMBER) IS
       SELECT 1
         FROM READ.RC_EXEC_PROCESS_ID
        WHERE RC_PROCESS      = PN_RC_PROCESS
          AND SEQNO           = LV_SEQNO
          AND PROCESS_ID      = PN_PROCESS_ID;

      --SUD Rene Vega. Cursor para obtener el id_contrato de axis a partir del custcode de bscs
/*      CURSOR Cur_ServiciosContratados (cv_cuenta VARCHAR2) IS
       SELECT a.id_contrato
         FROM cl_servicios_contratados@AXIS a,
              cl_contratos@AXIS b
        WHERE b.codigo_doc  = cv_cuenta
          AND a.id_contrato = b.id_contrato
          AND b.estado='A'
          AND a.estado='A';*/




      LD_FECHA_START         DATE;
      LV_INICIO_PERIODO      DATE;
      LV_FECHA               DATE;
      LV_DIA                 VARCHAR2(2);
      LN_COUNT_CUST          NUMBER;
      LN_COUNT_CONTRACT      NUMBER;

      LC_DAT_CONTROL         C_RC_CONTROL%ROWTYPE;
      LC_DAT_PROCESS         C_RC_PROCESS%ROWTYPE;
      LN_SALDO               FLOAT;
      LN_DEUDA               FLOAT;
      LN_CREDITO             FLOAT;
      LN_DEUDA_MES           FLOAT;--[9464] JJI-valor de la ultima factura
      LD_FECHA_OR            DATE;-- 10324 IRO_JRO
      LB_FOUND_FEC           BOOLEAN;-- 10324 IRO_JRO

      LE_ERROR               EXCEPTION;
      LB_NOTFOUND            BOOLEAN;
      LB_FOUND               BOOLEAN;
   /*   LV_STOP_MANUAL         VARCHAR(100);*/

      T_CO_ID                TYPE_NUMBER;
      T_CO_EXT_CSUIN         TYPE_VARCHAR_63;
      T_PRODUCT_HISTORY_DATE TYPE_DATE;
      T_TMCODE               TYPE_NUMBER;
      T_PLCODE               TYPE_NUMBER;
      T_CH_STATUS            TYPE_VARCHAR_1;
      T_MAIN_DIRNUM          TYPE_VARCHAR_1;
      T_CS_DEACTIV_DATE      TYPE_DATE;
      T_DN_NUM               TYPE_VARCHAR_63;
      T_DN_STATUS            TYPE_VARCHAR_1;
      LV_PROCESS_STATUS_F    VARCHAR(1) := 'F';
      LV_PROCESS_STATUS_W    VARCHAR(1) := 'W';
      LV_SQL                 VARCHAR2(100);
      LV_CUST_CODE_HIGH      VARCHAR2(63);
      LV_CHAR                VARCHAR2(2);
      
      LV_PRGCODE             VARCHAR2(4);--8693 jji

      --SUD Rene Vega. Variable para almacenar el contrato, y datos que devuelve el API
/*      LN_CONTRATO            NUMBER;
      LV_CICLO_AXIS          VARCHAR2(3);
      LV_CICLO_BSCS          VARCHAR2(3);
      LV_DESC_CICLO          VARCHAR2(100);
      LV_FIN_PERIODO         VARCHAR2(20);*/      
  /*    lb_Not_found           BOOLEAN;*/

      BEGIN

       LD_FECHA_START:=SYSDATE;
       LV_CHAR        := '''';
       
       --8693 JJI - INICIO
       OPEN C_PARAM_PRGCODE;
       FETCH C_PARAM_PRGCODE INTO LV_PRGCODE;
       CLOSE C_PARAM_PRGCODE;
       --8693 JJI - FIN

       DELETE READ.RC_TMP_DATOS_GENERALES WHERE SPCODE=PN_PROCESS_ID;
       COMMIT;

        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO LC_DAT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_PROCESS';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL_EXEC_PROCESS_ID(LC_DAT_CONTROL.SEQNO);
       FETCH C_RC_CONTROL_EXEC_PROCESS_ID INTO LN_COUNT_CUST;
       LB_NOTFOUND:=C_RC_CONTROL_EXEC_PROCESS_ID%NotFound;
       CLOSE C_RC_CONTROL_EXEC_PROCESS_ID;

       IF LB_NOTFOUND THEN
              INSERT INTO READ.RC_EXEC_PROCESS_ID(RC_PROCESS,
                                                  SEQNO,
                                                  FECHA_REFERENCIA,
                                                  PROCESS_ID,
                                                  DATE_START)
            VALUES (PN_RC_PROCESS,
                    LC_DAT_CONTROL.SEQNO,
                    LC_DAT_CONTROL.FECHA_REFERENCIA,
                    PN_PROCESS_ID,
                    SYSDATE);
       END IF;

       LV_SQL:= 'alter session set NLS_DATE_FORMAT =' || LV_CHAR || 'DD/MM/YYYY' || LV_CHAR ;
       EXECUTE IMMEDIATE LV_SQL;
       ---------------------------------------------------------------------
       -- RFE CASO 441
       RC_EXTRAE_DATOS_BSCS.RC_GENERA_RCFECHAS(PV_OUT_ERROR);
       IF PV_OUT_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
       ---------------------------------------------------------------------
       LN_COUNT_CUST:=0;
       FOR C_CLIENTE IN C_CUSTOMER_P(LC_DAT_CONTROL.FECHA_REFERENCIA,
                                     LC_DAT_CONTROL.SEQNO,
                                     PN_PROCESS_ID) LOOP

            LN_COUNT_CUST:=LN_COUNT_CUST+1;

            --_1.-LOS CONTRATOS SE ENCUENTRAN EN MEMORIA
            T_CO_ID.DELETE;
            T_CO_EXT_CSUIN.DELETE;
            T_PRODUCT_HISTORY_DATE.DELETE;
            T_TMCODE.DELETE;
            T_PLCODE.DELETE;
            T_CH_STATUS.DELETE;
            T_MAIN_DIRNUM.DELETE;
            T_CS_DEACTIV_DATE.DELETE;
            T_DN_NUM.DELETE;
            T_DN_STATUS.DELETE;
            
            --[8693]JJO - INICIO
            --Se verifica si el cliente es DTH
            IF C_CLIENTE.PRGCODE=LV_PRGCODE THEN
             OPEN C_CONTRACT_DTH(C_CLIENTE.CUSTOMER_ID_HIGH);
             FETCH C_CONTRACT_DTH BULK COLLECT INTO T_CO_ID,
                                   T_CO_EXT_CSUIN,
                                   T_PRODUCT_HISTORY_DATE,
                                   T_TMCODE,
                                   T_PLCODE,
                                   T_CH_STATUS,T_MAIN_DIRNUM,
                                   T_CS_DEACTIV_DATE,T_DN_NUM,T_DN_STATUS;
             CLOSE C_CONTRACT_DTH;
            --[8693]JJO - FIN
            ELSE
             OPEN C_CONTRACT(C_CLIENTE.CUSTOMER_ID_HIGH);
             FETCH C_CONTRACT BULK COLLECT INTO T_CO_ID,
                                   T_CO_EXT_CSUIN,
                                   T_PRODUCT_HISTORY_DATE,
                                   T_TMCODE,
                                   T_PLCODE,
                                   T_CH_STATUS,T_MAIN_DIRNUM,
                                   T_CS_DEACTIV_DATE,T_DN_NUM,T_DN_STATUS;
             CLOSE C_CONTRACT;
            END IF;

            LN_COUNT_CONTRACT:=T_CO_ID.LAST;

            IF NOT LN_COUNT_CONTRACT IS NULL THEN

               LV_CUST_CODE_HIGH:='NOTFOUND';
                OPEN C_CUSTOMER_CTA(C_CLIENTE.CUSTOMER_ID_HIGH);
               FETCH C_CUSTOMER_CTA INTO LV_CUST_CODE_HIGH;
               CLOSE C_CUSTOMER_CTA;



               --_2.-DETERMINA LA FECHA DEL CICLO DE FACTURACION
                SELECT SYSDATE INTO LV_FECHA FROM DUAL;
                LV_DIA := TO_CHAR(LV_FECHA,'DD');
                --------------------------------------------------------
                -- RFE CASO 441
                LV_INICIO_PERIODO := GV_CODES_INI(C_CLIENTE.billcycle);
                --------------------------------------------------------

                --[2702] Fin SUD Rene Vega. Bloque para llamar al API que retorna el ciclo

               --_3.-ACTUALIZA LOS SALDOS
               LN_DEUDA:=0; LN_SALDO:=0; LN_CREDITO:=0; LD_FECHA_OR:=null;

                OPEN C_DEUDA(C_CLIENTE.CUSTOMER_ID);
               FETCH C_DEUDA INTO LN_DEUDA;
               LB_FOUND := C_DEUDA%FOUND;
               CLOSE C_DEUDA;
               IF NOT LB_FOUND THEN
                  LN_DEUDA := 0;
               END IF;

                OPEN C_SALDO(C_CLIENTE.CUSTOMER_ID);
               FETCH C_SALDO INTO LN_SALDO;
               LB_FOUND := C_SALDO%FOUND;
               CLOSE C_SALDO;
               IF NOT LB_FOUND THEN
                  LN_SALDO := 0;
               END IF;
            --INICIO 10324 IRO_JRO                
               OPEN C_FECHA(C_CLIENTE.CUSTOMER_ID);
               FETCH C_FECHA INTO LD_FECHA_OR;
               LB_FOUND_FEC:=C_FECHA%FOUND;
               CLOSE C_FECHA;
               IF NOT LB_FOUND_FEC THEN
                 LB_FOUND_FEC:=NULL;
               END IF;  
            --FIN 10324 IRO_JRO  
               --[9464] INI JJI-Obtener valor de la untima factura
               OPEN C_DEUDA_MES(C_CLIENTE.CUSTOMER_ID);
               FETCH C_DEUDA_MES INTO LN_DEUDA_MES;
               LB_FOUND := C_DEUDA_MES%FOUND;
               CLOSE C_DEUDA_MES;
               IF NOT LB_FOUND THEN
                  LN_DEUDA_MES := 0;
               END IF;
               --[9464] FIN JJI

                OPEN C_CREDITO(C_CLIENTE.CUSTOMER_ID,TO_DATE(LV_INICIO_PERIODO,'DD/MM/YYYY'));
               FETCH C_CREDITO INTO LN_CREDITO;
               LB_FOUND := C_CREDITO%FOUND;
               CLOSE C_CREDITO;
               IF NOT LB_FOUND THEN
                  LN_CREDITO := 0;
               END IF;

               --_4.- INSERTAR LOS REGSTROS
               FORALL I IN T_CO_ID.FIRST .. T_CO_ID.LAST
                   INSERT  INTO READ.RC_TMP_DATOS_GENERALES NOLOGGING
                     (CUSTCODE,
                      CSTYPE,
                      PRGCODE,
                      PREV_BALANCE,
                      SALDO,
                      CO_ID,
                      CO_EXT_CSUIN,
                      PRODUCT_HISTORY_DATE,
                      TMCODE,
                      PLCODE,
                      CH_STATUS,
                      SNCODE,
                      PRM_VALUE_ID,
                      STATUS,
                      MAIN_DIRNUM,
                      CS_DEACTIV_DATE,
                      DN_NUM,
                      DN_STATUS,
                      SPCODE,
                      PROCESADA,
                      SM_SERIALNUM,
                      DEUDA,
                      CREDITOS,
                      CSTRADECODE,
                      FECHA_ACT_BSCS 
                 )
              VALUES
                 (
                     LV_CUST_CODE_HIGH,
                     C_CLIENTE.CSTYPE,
                     C_CLIENTE.PRGCODE,
                     C_CLIENTE.PREV_BALANCE,
                     LN_SALDO,
                     T_CO_ID(I),
                     T_CO_EXT_CSUIN(I),
                     T_PRODUCT_HISTORY_DATE(I),
                     T_TMCODE(I),
                     T_PLCODE(I),
                     T_CH_STATUS(I),
                     NULL,
                     /*NULL,*/LN_DEUDA_MES,--[9260] JJI-valor de la ultima factura
                     NULL,
                     T_MAIN_DIRNUM(I),
                     T_CS_DEACTIV_DATE(I),
                     T_DN_NUM(I),
                     T_DN_STATUS(I),
                     C_CLIENTE.PROCESS_ID,
                      'A',
                     NULL,
                     LN_DEUDA,
                     LN_CREDITO,
                     C_CLIENTE.CSTRADECODE,
                     LD_FECHA_OR--10324 IRO_JRO ULTIMA FECHA DE ACTULIZACION DE LA ORDERHDR_ALL
                 );
           ELSE
               LN_COUNT_CONTRACT:=0;
           END IF;

                --ACTUALIZA LA ESTADISTICA DEL CUSTOMER_ID
               UPDATE  /*+ INDEX(A,PK_RC_CUSTOMER_EST) */ READ.RC_CUSTOMER_EST A
                  SET A.CANT_CONTRATOS = CANT_CONTRATOS + LN_COUNT_CONTRACT,
                      A.DATE_UPDATE   = SYSDATE
                WHERE A.CUSTOMER_ID   = C_CLIENTE.CUSTOMER_ID
                  AND A.RC_PROCESS    = PN_PROCESS_ID;

                   /*       OPEN C_RC_CONTROL_PROCESS(LV_TIPODES);
                     FETCH C_RC_CONTROL_PROCESS INTO LC_DAT_CONTROL_PROCESS;
                     LB_NOTFOUND:=C_RC_CONTROL_PROCESS%NotFound;
                     CLOSE C_RC_CONTROL_PROCESS;
                     IF LB_NOTFOUND THEN
                        LV_STOP_MANUAL:='FACT_SEND_CALL- PROCESO DETENIDO MANUALMENTE';
                     END IF;*/

                    <<next_in_loop>>

                    COMMIT;
       END LOOP;

    --FINALIZACION DE LAS INSERCIONES
      IF LN_COUNT_CUST>=0 THEN

          UPDATE READ.RC_CONTROL
             SET REG_PROCESS      = REG_PROCESS + LN_COUNT_CUST,
                 DES_EVENTO       = DES_EVENTO ||'PROCESO:' || TO_CHAR(PN_PROCESS_ID) ||'-OK,',
                 PER_STOP         = SYSDATE,
                 DURATION         = ROUND((SYSDATE - PER_START) * 1440,2)
           WHERE RC_PROCESS       = PN_RC_PROCESS
             AND SEQNO            = LC_DAT_CONTROL.SEQNO
             AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

          UPDATE READ.RC_EXEC_PROCESS_ID
             SET DATE_STOP      = SYSDATE,
                 DURATION       = ROUND((SYSDATE - DATE_START) * 1440,2),
                 TOT_REGISTROS  = LN_COUNT_CUST,
                 PROCESS_STATUS = LV_PROCESS_STATUS_F
           WHERE RC_PROCESS     = PN_RC_PROCESS
             AND SEQNO          = LC_DAT_CONTROL.SEQNO
             AND PROCESS_ID     = PN_PROCESS_ID
             AND PROCESS_STATUS = LV_PROCESS_STATUS_W;
                  
           OPEN C_RC_PROCESS_CUST(LC_DAT_CONTROL.SEQNO,LC_DAT_CONTROL.FECHA_REFERENCIA);
           FETCH C_RC_PROCESS_CUST INTO LN_COUNT_CUST;
           LB_NOTFOUND:=C_RC_PROCESS_CUST%NotFound;
           CLOSE C_RC_PROCESS_CUST;

           IF LB_NOTFOUND THEN
                UPDATE READ.RC_CONTROL
                   SET PROCESS_STATUS   = LV_PROCESS_STATUS_W,
                       PER_STOP         = SYSDATE,
                       DURATION         = ROUND((SYSDATE - PER_START) * 1440,2)
                 WHERE RC_PROCESS       = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;


                 DELETE READ.RC_PROCESS_CUST
                  WHERE RC_PROCESS      = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

                UPDATE READ.RC_CONTROL_PROCESOS
                   SET PROCESS_STATUS = 'R',
                       DES_EVENTO     = 'FACT_SEND-SUCCESFUL'
                 WHERE RC_PROCESS     = PN_RC_PROCESS
                   AND PROCESS_STATUS = 'E';

           END IF;

           COMMIT WORK;

           PV_OUT_ERROR:= 'RC_SEND-SUCESSFUL,PROCESS_ID:'||PN_PROCESS_ID;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'OK',
                         PV_ERROR        => PV_OUT_ERROR
                         );
       END IF;
           
       --SUD Rene Vega. Cierre del DBLINK
/*       COMMIT;
       DBMS_SESSION.close_database_link('AXIS');
       COMMIT;*/
  EXCEPTION
      WHEN LE_ERROR THEN
           ROLLBACK;
           --SUD Rene Vega. Cierre del DBLINK
          /* DBMS_SESSION.close_database_link('AXIS');
           ROLLBACK;*/
		   --
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'FACT_SEND ' || ' ERROR:'|| SQLCODE || ', PROCESS_ID:'||PN_PROCESS_ID||','|| SUBSTR(SQLERRM, 1, 80);
           ROLLBACK;
           --SUD Rene Vega. Cierre del DBLINK
            DBMS_SESSION.close_database_link('AXIS');
            ROLLBACK;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

  END RC_SEND_1;


 --JRO OPCION PARA MEJORAR TIEMPOS EN LA REACTIVACION
 --22/03/08 AUT. EGA Y VSO
  PROCEDURE RC_SEND_2(PN_RC_PROCESS    IN NUMBER,
                      PN_PROCESS_ID    IN NUMBER,
                      PV_OUT_ERROR     OUT VARCHAR2) IS

    CURSOR C_CONTRACT(PN_CUSTOMER_ID NUMBER) IS
       SELECT
               CO.CO_ID,
               CO.CO_EXT_CSUIN,
               CO.PRODUCT_HISTORY_DATE,
               CO.TMCODE,
               CO.PLCODE,
               CUR.CH_STATUS,
               CS.MAIN_DIRNUM,
               CS.CS_DEACTIV_DATE,
               DI.DN_NUM,
               DI.DN_STATUS
        FROM  CONTRACT_ALL               CO,
              CURR_CO_STATUS             CUR,
              CONTR_SERVICES_CAP         CS,
              DIRECTORY_NUMBER           DI
       WHERE CO.CUSTOMER_ID   = PN_CUSTOMER_ID
         AND CO.CO_EXT_CSUIN IN (1, 2, 3, 4, 7)
         AND CO.CO_ID         = CUR.CO_ID
         AND CUR.CH_STATUS NOT IN ('d', 'o')
         AND CO.CO_ID         = CS.CO_ID
         AND CS.SEQNO         = ( SELECT MAX ( CCL.SEQNO )
                                    FROM CONTR_SERVICES_CAP CCL
                                   WHERE CCL.CO_ID = CS.CO_ID)
        AND CS.DN_ID          = DI.DN_ID
        AND DI.DN_STATUS = 'a';

      CURSOR C_CUSTOMER_P(PD_FECHA_PER  DATE,
                          VC_PN_SEQNO      NUMBER,
                          VC_PROCESS_ID    NUMBER) IS

             SELECT B.CUSTOMER_ID,
                    B.CUSTOMER_ID_HIGH,
                    A.CUSTCODE,
                    A.BILLCYCLE,
                    A.CSTYPE,
                    A.PRGCODE,
                    A.CSTRADECODE,
                    A.CUSTOMER_DEALER,
                    A.PREV_BALANCE
               FROM CUSTOMER_ALL A, READ.RC_PROCESS_CUST B
              WHERE A.CUSTOMER_ID             = B.CUSTOMER_ID
                AND B.RC_PROCESS              = PN_RC_PROCESS
                AND B.PROCESS_ID              = VC_PROCESS_ID
                AND B.SEQNO                   = VC_PN_SEQNO
                AND B.PROCESS_STATUS          = 'R';


      CURSOR C_CUSTOMER_CTA(PN_CUSTOMER_ID NUMBER) IS
             SELECT A.CUSTCODE
               FROM CUSTOMER_ALL A
              WHERE A.CUSTOMER_ID = PN_CUSTOMER_ID;


        CURSOR C_RC_PROCESS_CUST(LV_SEQNO NUMBER,LV_FECHA DATE ) IS
             SELECT 1
               FROM READ.RC_PROCESS_CUST
              WHERE PROCESS_STATUS   = 'R'
                AND RC_PROCESS       = PN_RC_PROCESS
                AND SEQNO            = LV_SEQNO
                AND FECHA_REFERENCIA = LV_FECHA;

        CURSOR C_RC_PROCESS IS
             SELECT RC_PROCESS,
                    CANT_PROCESS_ID,
                    CANT_REG_COMMIT,
                    TABLE_ORIGEN,
                    TABLE_DESTINO
               FROM READ.RC_PROCESS
              WHERE RC_PROCESS = PN_RC_PROCESS;

      CURSOR C_RC_CONTROL IS
             SELECT SEQNO,FECHA_REFERENCIA,PER_START,PER_STOP
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS     = PN_RC_PROCESS
                AND PROCESS_STATUS = 'R'
                AND ROWNUM=1
              ORDER BY  PER_START;

      CURSOR C_DEUDA(PN_CUSTOMER_ID NUMBER) IS
             SELECT /*+ INDEX(O,OH_CUST_ID_1)*/ NVL (SUM (DECODE (O.OHSTATUS, 'CO', 0, OHINVAMT_DOC)), 0)
               FROM SYSADM.ORDERHDR_ALL O
              WHERE O.CUSTOMER_ID = PN_CUSTOMER_ID
                AND O.OHSTATUS IN ('IN', 'CM', 'CO')
                AND TRUNC(O.OHDUEDATE) < TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY');


      CURSOR C_SALDO(PN_CUSTOMER_ID NUMBER) IS
            SELECT /*+ RULE +*/  SUM(OH.OHOPNAMT_DOC) SALDO
              FROM
                 SYSADM.ORDERHDR_ALL        OH,
                 SYSADM.PAYMENT_ALL         PA
             WHERE
                   OH.OHDUEDATE   < TRUNC(SYSDATE)
               AND OH.CUSTOMER_ID = PN_CUSTOMER_ID
               AND PA.CUSTOMER_ID = OH.CUSTOMER_ID
               AND PA.ACT_USED    = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZÿ
                AND PA.BANK_ID NOT IN (33, 34, 35, 36, 37, 38, 39, 41, 42,43, 44, 45, 46, 47, 48,49,50, 51, 52, 53, 54, 55,
                                      56, 57, 58, 59, 60,61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 74, 75, 76);


      CURSOR CUR_CREDITOS_MSJ (PV_CUSTOMER_ID NUMBER, CV_INICIO_PERIODO VARCHAR2) IS
          SELECT /*+ RULE +*/ NVL(SUM(AMOUNT), 0) CREDITOS
          FROM  FEES B
          WHERE B.CUSTOMER_ID =PV_CUSTOMER_ID
          AND B.PERIOD > 0
          AND B.SNCODE = 46
          AND TRUNC(B.ENTDATE) >= TO_DATE(CV_INICIO_PERIODO, 'DD/MM/YYYY');


      CURSOR CUR_SALDOMINIMO IS
        SELECT /*+ RULE +*/ PAR_VALOR
        FROM RC_PARAM_GLOBAL@AXIS
        WHERE PAR_NOMBRE = 'SALDO_MINIMO';

       CURSOR CUR_SALDOMINPOR IS
         SELECT /*+ RULE +*/ PAR_VALOR
         FROM RC_PARAM_GLOBAL@AXIS
         WHERE PAR_NOMBRE = 'SALDO_MIN_POR';


      CURSOR C_RC_CONTROL_EXEC_PROCESS_ID(LV_SEQNO NUMBER) IS
       SELECT 1
         FROM READ.RC_EXEC_PROCESS_ID
        WHERE RC_PROCESS      = PN_RC_PROCESS
          AND SEQNO           = LV_SEQNO
          AND PROCESS_ID      = PN_PROCESS_ID;

     CURSOR C_FA_CICLO(PV_BILLCYCLE VARCHAR2) IS
          SELECT /*+ RULE +*/ ID_CICLO
            FROM FA_CICLOS_AXIS_BSCS M
           WHERE M.ID_CICLO_ADMIN = PV_BILLCYCLE;

      LD_FECHA_START         DATE;
      LV_FECHA               DATE;
      LN_COUNT_CUST          NUMBER;
      LN_COUNT_CONTRACT      NUMBER;

      LC_DAT_CONTROL         C_RC_CONTROL%ROWTYPE;
      LC_DAT_PROCESS         C_RC_PROCESS%ROWTYPE;
      LN_SALDO               FLOAT;
      LN_DEUDA               FLOAT;
      LN_CREDITO             FLOAT;

      LE_ERROR               EXCEPTION;
      LB_NOTFOUND            BOOLEAN;
      LV_STOP_MANUAL         VARCHAR(100);

      T_CO_ID                TYPE_NUMBER;
      T_CO_EXT_CSUIN         TYPE_VARCHAR_63;
      T_PRODUCT_HISTORY_DATE TYPE_DATE;
      T_TMCODE               TYPE_NUMBER;
      T_PLCODE               TYPE_NUMBER;
      T_CH_STATUS            TYPE_VARCHAR_1;
      T_MAIN_DIRNUM          TYPE_VARCHAR_1;
      T_CS_DEACTIV_DATE      TYPE_DATE;
      T_DN_NUM               TYPE_VARCHAR_63;
      T_DN_STATUS            TYPE_VARCHAR_1;
      LV_PROCESS_STATUS_F    VARCHAR(1) := 'F';
      LV_PROCESS_STATUS_W    VARCHAR(1) := 'W';
      LV_SQL                 VARCHAR2(100);
      LV_CUST_CODE_HIGH      VARCHAR2(63);
      LV_CHAR                VARCHAR2(2);
      LV_CYCLE               VARCHAR2(2);
      LN_SALDO_MIN           FLOAT;
      LN_SALDO_MIN_POR       FLOAT;
      LN_POR_IMPAGO          FLOAT;
      LN_SALDO_IMPAGO        FLOAT;
      LN_SALDO_REAL          FLOAT;
      LV_MES                 VARCHAR2(3);
      LV_INICIO_PERIODO      VARCHAR2(10); -- variable para sacar la fecha de inicio del periodo actual
      LV_FIN_PERIODO         VARCHAR2(10); -- variable para sacar la fecha en que finaliza el periodo actual
      LV_PERIODO_INICIADO    VARCHAR2(10); -- variable para sacar la fecha en que ya ha iniciado el periodo actual
      LV_NUMERO_DIAS         VARCHAR2(2); -- variable para almacenar el numero de dias del mes anterior al periodo actual
      LB_FOUND               BOOLEAN;

      BEGIN

       LD_FECHA_START:=SYSDATE;
       LV_CHAR        := '''';

        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO LC_DAT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_PROCESS';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL_EXEC_PROCESS_ID(LC_DAT_CONTROL.SEQNO);
       FETCH C_RC_CONTROL_EXEC_PROCESS_ID INTO LN_COUNT_CUST;
       LB_NOTFOUND:=C_RC_CONTROL_EXEC_PROCESS_ID%NotFound;
       CLOSE C_RC_CONTROL_EXEC_PROCESS_ID;

       IF LB_NOTFOUND THEN
              INSERT INTO READ.RC_EXEC_PROCESS_ID(RC_PROCESS,
                                                  SEQNO,
                                                  FECHA_REFERENCIA,
                                                  PROCESS_ID,
                                                  DATE_START)
            VALUES (PN_RC_PROCESS,
                    LC_DAT_CONTROL.SEQNO,
                    LC_DAT_CONTROL.FECHA_REFERENCIA,
                    PN_PROCESS_ID,
                    SYSDATE);
       END IF;

       LV_SQL:= 'alter session set NLS_DATE_FORMAT =' || LV_CHAR || 'DD/MM/YYYY' || LV_CHAR ;
       EXECUTE IMMEDIATE LV_SQL;
       ---------------------------------------------------------------------
       -- RFE CASO 441 - 14 de Noviembre del 2008
       RC_EXTRAE_DATOS_BSCS.RC_GENERA_RCFECHAS(PV_OUT_ERROR);
       IF PV_OUT_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
       ---------------------------------------------------------------------

       LN_COUNT_CUST:=0;
       -- RFE CASO 441 - 14 de Noviembre del 2008
       --_2.-DETERMINA LA FECHA DEL CICLO DE FACTURACION
       OPEN  CUR_SALDOMINIMO;
       FETCH CUR_SALDOMINIMO INTO LN_SALDO_MIN;
       CLOSE CUR_SALDOMINIMO;

       OPEN  CUR_SALDOMINPOR;
       FETCH CUR_SALDOMINPOR INTO LN_SALDO_MIN_POR;
       CLOSE CUR_SALDOMINPOR;
       --
       FOR C_CLIENTE IN C_CUSTOMER_P(LC_DAT_CONTROL.FECHA_REFERENCIA,
                                     LC_DAT_CONTROL.SEQNO,
                                     PN_PROCESS_ID) LOOP

            LN_COUNT_CUST:=LN_COUNT_CUST+1;

            --_1.-LOS CONTRATOS SE ENCUENTRAN EN MEMORIA
            T_CO_ID.DELETE;
            T_CO_EXT_CSUIN.DELETE;
            T_PRODUCT_HISTORY_DATE.DELETE;
            T_TMCODE.DELETE;
            T_PLCODE.DELETE;
            T_CH_STATUS.DELETE;
            T_MAIN_DIRNUM.DELETE;
            T_CS_DEACTIV_DATE.DELETE;
            T_DN_NUM.DELETE;
            T_DN_STATUS.DELETE;

              OPEN C_CONTRACT(C_CLIENTE.CUSTOMER_ID);
             FETCH C_CONTRACT BULK COLLECT INTO T_CO_ID,
                                   T_CO_EXT_CSUIN,
                                   T_PRODUCT_HISTORY_DATE,
                                   T_TMCODE,
                                   T_PLCODE,
                                   T_CH_STATUS,T_MAIN_DIRNUM,
                                   T_CS_DEACTIV_DATE,T_DN_NUM,T_DN_STATUS;
             CLOSE C_CONTRACT;

            LN_COUNT_CONTRACT:=T_CO_ID.LAST;

            IF NOT LN_COUNT_CONTRACT IS NULL THEN

               LV_CUST_CODE_HIGH:='NOTFOUND';
                OPEN C_CUSTOMER_CTA(C_CLIENTE.CUSTOMER_ID);
               FETCH C_CUSTOMER_CTA INTO LV_CUST_CODE_HIGH;
               CLOSE C_CUSTOMER_CTA;

                LN_POR_IMPAGO := ((100 - LN_SALDO_MIN_POR) / 100);

               --_3.-ACTUALIZA LOS SALDOS
               LN_DEUDA:=0; LN_SALDO:=0; LN_CREDITO:=0;

                OPEN C_DEUDA(C_CLIENTE.CUSTOMER_ID_HIGH);
               FETCH C_DEUDA INTO LN_DEUDA;
               LB_FOUND := C_DEUDA%FOUND;
               CLOSE C_DEUDA;
               IF NOT LB_FOUND THEN
                  LN_DEUDA := 0;
               END IF;

                OPEN C_SALDO(C_CLIENTE.CUSTOMER_ID_HIGH);
               FETCH C_SALDO INTO LN_SALDO;
               LB_FOUND := C_SALDO%FOUND;
               CLOSE C_SALDO;
               IF NOT LB_FOUND THEN
                  LN_SALDO := 0;
               END IF;

                OPEN C_FA_CICLO(C_CLIENTE.BILLCYCLE);
               FETCH C_FA_CICLO INTO LV_CYCLE;
               CLOSE C_FA_CICLO;

               LN_SALDO_IMPAGO := (LN_DEUDA * LN_POR_IMPAGO);
               --------------------------------------------------------
               -- RFE CASO 441 - 14 de Noviembre del 2008
               LV_INICIO_PERIODO := GV_CODES_INI(C_CLIENTE.BILLCYCLE);
               --------------------------------------------------------
              -- Obtener creditos realizados durante el mes y no aplicados
              OPEN CUR_CREDITOS_MSJ (C_CLIENTE.CUSTOMER_ID_HIGH , LV_INICIO_PERIODO );
              FETCH CUR_CREDITOS_MSJ INTO LN_CREDITO;
              LB_FOUND := CUR_CREDITOS_MSJ%FOUND;
              CLOSE CUR_CREDITOS_MSJ;
              IF NOT LB_FOUND THEN
                 LN_CREDITO := 0;
              END IF;

              LN_SALDO_REAL := LN_SALDO + LN_CREDITO;

               --_4.- INSERTAR LOS REGSTROS
              IF (LN_SALDO_REAL <= LN_SALDO_MIN) OR (LN_SALDO_REAL <= LN_SALDO_IMPAGO) THEN
                   FORALL I IN T_CO_ID.FIRST .. T_CO_ID.LAST
                       INSERT  INTO READ.RC_TMP_DATOS_GENERALES NOLOGGING
                         (CUSTCODE,
                          CSTYPE,
                          PRGCODE,
                          PREV_BALANCE,
                          SALDO,
                          CO_ID,
                          CO_EXT_CSUIN,
                          PRODUCT_HISTORY_DATE,
                          TMCODE,
                          PLCODE,
                          CH_STATUS,
                          SNCODE,
                          PRM_VALUE_ID,
                          STATUS,
                          MAIN_DIRNUM,
                          CS_DEACTIV_DATE,
                          DN_NUM,
                          DN_STATUS,
                          SPCODE,
                          PROCESADA,
                          SM_SERIALNUM,
                          DEUDA,
                          CREDITOS,
                          CSTRADECODE
                     )
                  VALUES
                     (
                         LV_CUST_CODE_HIGH,
                         C_CLIENTE.CSTYPE,
                         C_CLIENTE.PRGCODE,
                         C_CLIENTE.PREV_BALANCE,
                         LN_SALDO,
                         T_CO_ID(I),
                         T_CO_EXT_CSUIN(I),
                         T_PRODUCT_HISTORY_DATE(I),
                         T_TMCODE(I),
                         T_PLCODE(I),
                         T_CH_STATUS(I),
                         PN_RC_PROCESS,
                         TO_NUMBER(TO_CHAR(SYSDATE+1,'DDMMYYYY')),
                         NULL,
                         T_MAIN_DIRNUM(I),
                         T_CS_DEACTIV_DATE(I),
                         T_DN_NUM(I),
                         T_DN_STATUS(I),
                         NULL,
                          'A',
                         NULL,
                         LN_DEUDA,
                         LN_CREDITO,
                         C_CLIENTE.CSTRADECODE
                     );
              END IF;
           ELSE
               LN_COUNT_CONTRACT:=0;
           END IF;

                --ACTUALIZA EL CUSTOMER_ID PROCESADO
               UPDATE READ.RC_PROCESS_CUST
                   SET PROCESS_STATUS = LV_PROCESS_STATUS_W
                 WHERE RC_PROCESS     = PN_RC_PROCESS
                   AND CUSTOMER_ID    = C_CLIENTE.CUSTOMER_ID
                   AND SEQNO          = LC_DAT_CONTROL.SEQNO
                   AND PROCESS_ID     = PN_PROCESS_ID;

                --ACTUALIZA LA ESTADISTICA DEL CUSTOMER_ID
               UPDATE  /*+ INDEX(A,PK_RC_CUSTOMER_EST) */ READ.RC_CUSTOMER_EST A
                  SET A.CANT_CONTRATOS = CANT_CONTRATOS + LN_COUNT_CONTRACT,
                      A.DATE_UPDATE   = SYSDATE
                WHERE A.CUSTOMER_ID   = C_CLIENTE.CUSTOMER_ID
                  AND A.RC_PROCESS    = PN_PROCESS_ID;

                   /*       OPEN C_RC_CONTROL_PROCESS(LV_TIPODES);
                     FETCH C_RC_CONTROL_PROCESS INTO LC_DAT_CONTROL_PROCESS;
                     LB_NOTFOUND:=C_RC_CONTROL_PROCESS%NotFound;
                     CLOSE C_RC_CONTROL_PROCESS;
                     IF LB_NOTFOUND THEN
                        LV_STOP_MANUAL:='FACT_SEND_CALL- PROCESO DETENIDO MANUALMENTE';
                     END IF;*/

                    COMMIT;

       END LOOP;



    --FINALIZACION DE LAS INSERCIONES
      IF LN_COUNT_CUST>0 THEN

          UPDATE READ.RC_CONTROL
             SET REG_PROCESS      = REG_PROCESS + LN_COUNT_CUST,
                 DES_EVENTO       = DES_EVENTO ||'PROCESO:' || TO_CHAR(PN_PROCESS_ID) ||'-OK,'
           WHERE RC_PROCESS       = PN_RC_PROCESS
             AND SEQNO            = LC_DAT_CONTROL.SEQNO
             AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

          UPDATE READ.RC_EXEC_PROCESS_ID
             SET DATE_STOP      = SYSDATE,
                 DURATION       = ROUND((SYSDATE - DATE_START) * 1440,2),
                 TOT_REGISTROS  = LN_COUNT_CUST,
                 PROCESS_STATUS = LV_PROCESS_STATUS_F
           WHERE RC_PROCESS     = PN_RC_PROCESS
             AND SEQNO          = LC_DAT_CONTROL.SEQNO
             AND PROCESS_ID     = PN_PROCESS_ID
             AND PROCESS_STATUS = LV_PROCESS_STATUS_W;

           OPEN C_RC_PROCESS_CUST(LC_DAT_CONTROL.SEQNO,LC_DAT_CONTROL.FECHA_REFERENCIA);
           FETCH C_RC_PROCESS_CUST INTO LN_COUNT_CUST;
           LB_NOTFOUND:=C_RC_PROCESS_CUST%NotFound;
           CLOSE C_RC_PROCESS_CUST;

           IF LB_NOTFOUND THEN
                UPDATE READ.RC_CONTROL
                   SET PROCESS_STATUS   = LV_PROCESS_STATUS_W,
                       PER_STOP         = SYSDATE,
                       DURATION         = ROUND((SYSDATE - PER_START) * 1440,2)
                 WHERE RC_PROCESS       = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;


                 DELETE READ.RC_PROCESS_CUST
                  WHERE RC_PROCESS      = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

                UPDATE READ.RC_CONTROL_PROCESOS
                   SET PROCESS_STATUS = 'R',
                       DES_EVENTO     = 'FACT_SEND_CALL-SUCCESFUL'
                 WHERE RC_PROCESS     = PN_RC_PROCESS
                   AND PROCESS_STATUS = 'E';

           END IF;

           COMMIT WORK;

           PV_OUT_ERROR:= 'RC_SEND-SUCESSFUL, PROCESS_ID:'||PN_PROCESS_ID||' '||LV_STOP_MANUAL;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'OK',
                         PV_ERROR        => PV_OUT_ERROR
                         );
       END IF;
  EXCEPTION
      WHEN LE_ERROR THEN
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'FACT_SEND ' || ' ERROR:'|| SQLCODE || ', PROCESS_ID:'||PN_PROCESS_ID||','|| SUBSTR(SQLERRM, 1, 80);
           ROLLBACK;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

  END RC_SEND_2;


  PROCEDURE RC_SEND_3(PN_RC_PROCESS    IN NUMBER,
                      PN_PROCESS_ID    IN NUMBER,
                      PV_OUT_ERROR     OUT VARCHAR2) IS

      CURSOR C_RC_PROCESS IS
             SELECT RC_PROCESS,
                    CANT_PROCESS_ID,
                    CANT_REG_COMMIT,
                    TABLE_ORIGEN,
                    TABLE_DESTINO
               FROM READ.RC_PROCESS
              WHERE RC_PROCESS = PN_RC_PROCESS;

      CURSOR C_RC_CONTROL IS
             SELECT SEQNO,FECHA_REFERENCIA,PER_START,PER_STOP
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS     = PN_RC_PROCESS
                AND PROCESS_STATUS = 'R'
                AND ROWNUM=1
              ORDER BY  PER_START;


      CURSOR C_RC_PROCESS_CUST(LV_SEQNO NUMBER,LV_FECHA DATE ) IS
       SELECT 1
         FROM READ.RC_PROCESS_CUST_ETAPA
        WHERE PROCESS_STATUS   = 'R'
          AND RC_PROCESS       = PN_RC_PROCESS
          AND SEQNO            = LV_SEQNO
          AND FECHA_REFERENCIA = LV_FECHA;

      CURSOR C_RC_CUSTOMERS(VC_PN_SEQNO NUMBER) IS
       SELECT RC_PROCESS,
              RC_TIPO_RELOJ,
              RC_PREDECESOR,
              RC_DIA_EFECTIVO,
              RC_DURACION,
              RC_ETAPA
         FROM READ.RC_PROCESS_CUST_ETAPA
        WHERE RC_PROCESS     = PN_RC_PROCESS
          AND SEQNO          = VC_PN_SEQNO
          AND PROCESS_ID     = PN_PROCESS_ID
          AND PROCESS_STATUS = 'R';

      CURSOR C_RC_CONTROL_EXEC_PROCESS_ID(LV_SEQNO NUMBER) IS
       SELECT 1
         FROM READ.RC_EXEC_PROCESS_ID
        WHERE RC_PROCESS      = PN_RC_PROCESS
          AND SEQNO           = LV_SEQNO
          AND PROCESS_ID      = PN_PROCESS_ID;

      LD_FECHA_START         DATE;
      LV_FECHA               DATE;
      LN_COUNT_CUST          NUMBER;

      LC_DAT_CONTROL         C_RC_CONTROL%ROWTYPE;
      LC_DAT_PROCESS         C_RC_PROCESS%ROWTYPE;

      LE_ERROR               EXCEPTION;
      LB_NOTFOUND            BOOLEAN;
      LV_STOP_MANUAL         VARCHAR(100);
      LV_SQL                 VARCHAR(200);
      LV_CHAR                VARCHAR(1);
      LV_PROCESS_STATUS_F    VARCHAR(1) := 'F';
      LV_PROCESS_STATUS_W    VARCHAR(1) := 'W';


      BEGIN

       LV_CHAR        := '''';
       LD_FECHA_START:=SYSDATE;

        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO LC_DAT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS;

       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_PROCESS';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;
       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_SEND- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;


        OPEN C_RC_CONTROL_EXEC_PROCESS_ID(LC_DAT_CONTROL.SEQNO);
       FETCH C_RC_CONTROL_EXEC_PROCESS_ID INTO LN_COUNT_CUST;
       LB_NOTFOUND:=C_RC_CONTROL_EXEC_PROCESS_ID%NotFound;
       CLOSE C_RC_CONTROL_EXEC_PROCESS_ID;

       IF LB_NOTFOUND THEN
              INSERT INTO READ.RC_EXEC_PROCESS_ID(RC_PROCESS,
                                                  SEQNO,
                                                  FECHA_REFERENCIA,
                                                  PROCESS_ID,
                                                  DATE_START)
            VALUES (PN_RC_PROCESS,
                    LC_DAT_CONTROL.SEQNO,
                    LC_DAT_CONTROL.FECHA_REFERENCIA,
                    PN_PROCESS_ID,
                    SYSDATE);
       END IF;

       LV_SQL:= 'alter session set NLS_DATE_FORMAT =' || LV_CHAR || 'DD/MM/YYYY' || LV_CHAR ;
       EXECUTE IMMEDIATE LV_SQL;

       LN_COUNT_CUST:=0;

       FOR C_CLIENTE IN C_RC_CUSTOMERS(LC_DAT_CONTROL.SEQNO) LOOP
            LN_COUNT_CUST:=LN_COUNT_CUST+1;

            IF NVL(C_CLIENTE.RC_DIA_EFECTIVO, 1)=1 THEN
                     INSERT
                        INTO RC_TMP_DATOS_GENERALES_STATUS NOLOGGING
                        (CUSTCODE,
                         CSTYPE,
                         PRGCODE,
                         PREV_BALANCE,
                         SALDO,
                         CO_ID,
                         CO_EXT_CSUIN,
                         PRODUCT_HISTORY_DATE,
                         TMCODE,
                         PLCODE,
                         CH_STATUS,
                         SNCODE,
                         PRM_VALUE_ID,
                         STATUS, MAIN_DIRNUM,
                         CS_DEACTIV_DATE,
                         DN_NUM,
                         DN_STATUS,
                         SPCODE,
                         PROCESADA,
                         SM_SERIALNUM
                        )
                        (SELECT /*+ index(cu,pkcustomer_all),
                                   index(co,pkcontract_all ),
                                   index(cs,pk_contr_services_cap),
                                   index(di,pkdirectory_number),
                                   index(cd,pkcontr_devices),
                                   index(sm,stormed_sepl),
                               +*/
                         DISTINCT CU.CUSTCODE,
                                  CU.CSTYPE,
                                  CU.PRGCODE,
                                  CU.PREV_BALANCE,
                                  CU.CSTRADECODE TRADECODE,
                                  CO.CO_ID,
                                  CO.CO_EXT_CSUIN,
                                  CO.PRODUCT_HISTORY_DATE,
                                  CO.TMCODE,
                                  CO.PLCODE,
                                  CUR.CH_STATUS,
                                  NULL,
                                  PN_RC_PROCESS,
                                  NULL,
                                  CS.MAIN_DIRNUM,
                                  CS.CS_DEACTIV_DATE,
                                  DI.DN_NUM,
                                  DI.DN_STATUS,
                                  NULL,
                                  'A',
                                  SM.SM_SERIALNUM
                           FROM CUSTOMER_ALL        CU,
                                CONTRACT_ALL        CO,
                                CURR_CO_STATUS      CUR,
                                CONTR_SERVICES_CAP  CS,
                                DIRECTORY_NUMBER    DI,
                                CONTR_DEVICES       CD,
                                STORAGE_MEDIUM      SM
                          WHERE CO.CUSTOMER_ID = CU.CUSTOMER_ID
              		      	  AND CUR.CO_ID      = co.co_id
                            AND CO.TMCODE      <> 268
                            AND CU.CSTYPE NOT IN ('d', 'o')
                            AND CO.CO_EXT_CSUIN = C_CLIENTE.RC_PREDECESOR
                            AND CUR.CH_STATUS NOT IN ('d', 'o')
                            AND TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY') -  TRUNC(CO.PRODUCT_HISTORY_DATE) >= C_CLIENTE.RC_DURACION
                            AND CU.CSTRADECODE IN ( SELECT B.RC_TIPO_CREDITO
                                                    FROM RC_TR_TC@AXIS B
                                                    WHERE B.RC_TIPO_RELOJ = C_CLIENTE.RC_TIPO_RELOJ
                                                   )
                            AND CU.PRGCODE   IN ( SELECT C.ST_ID_SUB
                                                    FROM RC_SUB_TIPOS_RELOJ@AXIS C
                                                    WHERE C.ST_TIPO_RELOJ = C_CLIENTE.RC_TIPO_RELOJ
                                                 )
                            AND CS.CO_ID = CO.CO_ID
                            AND CS.SEQNO      = ( SELECT MAX(CCL.SEQNO)
                                                     FROM CONTR_SERVICES_CAP CCL
                                                    WHERE CCL.CO_ID = CS.CO_ID
                                                )
                            AND DI.DN_ID      = CS.DN_ID AND (CS.CS_DEACTIV_DATE IS NULL)
                            AND DI.DN_STATUS  = 'a'
                            AND CD.CO_ID      = CO.CO_ID AND (CD.CD_DEACTIV_DATE IS NULL)
                            AND CD_SEQNO      = ( SELECT MAX(CDD.CD_SEQNO)
                                                    FROM CONTR_DEVICES CDD
                                                   WHERE CDD.CO_ID = CO.CO_ID
                                                 )
                            AND SM.SM_SERIALNUM = CD.CD_SM_NUM
                          GROUP BY CU.CUSTCODE,
                                   CU.CSTYPE,
                                   CU.PREV_BALANCE,
                                   CU.CSTRADECODE,
                                   CU.PRGCODE,
                                   CO.CO_ID,
                                   CUR.CH_STATUS,
                                   CO.CO_EXT_CSUIN,
                                   CO.PRODUCT_HISTORY_DATE,
                                   CO.TMCODE,
                                   CO.PLCODE,
                                   CS.MAIN_DIRNUM,
                                   CS.CS_DEACTIV_DATE,
                                   DI.DN_NUM,
                                   DI.DN_STATUS,
                                   SM.SM_SERIALNUM
                         );

            ELSE
               IF NVL(C_CLIENTE.RC_DIA_EFECTIVO, 1)<>1 THEN
                    INSERT
                       INTO RC_TMP_DATOS_GENERALES_STATUS NOLOGGING
                       (CUSTCODE,
                        CSTYPE,
                        PRGCODE,
                        PREV_BALANCE,
                        SALDO,
                        CO_ID,
                        CO_EXT_CSUIN,
                        PRODUCT_HISTORY_DATE,
                        TMCODE,
                        PLCODE,
                        CH_STATUS,
                        SNCODE,
                        PRM_VALUE_ID,
                        STATUS,
                        MAIN_DIRNUM,
                        CS_DEACTIV_DATE,
                        DN_NUM, DN_STATUS,
                        SPCODE,
                        PROCESADA,
                        SM_SERIALNUM)
                     (SELECT /*+ index(cu,pkcustomer_all),
                               index(co,pkcontract_all ),
                               index(cs,pk_contr_services_cap),
                               index(di,pkdirectory_number),
                               index(cd,pkcontr_devices),
                               index(sm,stormed_sepl),
                           +*/
                        DISTINCT CU.CUSTCODE,
                                 CU.CSTYPE,
                                 CU.PRGCODE,
                                 CU.PREV_BALANCE,
                                 CU.CSTRADECODE TRADECODE,
                                 CO.CO_ID,
                                 CO.CO_EXT_CSUIN,
                                 CO.PRODUCT_HISTORY_DATE,
                                 CO.TMCODE,
                                 CO.PLCODE,
                                 CUR.CH_STATUS,
                                 NULL,
                                 NULL,
                                 NULL,
                                 CS.MAIN_DIRNUM,
                                 CS.CS_DEACTIV_DATE,
                                 DI.DN_NUM,
                                 DI.DN_STATUS,
                                 NULL,
                                 'A',
                                 SM.SM_SERIALNUM
                          FROM CUSTOMER_ALL        CU,
                               CONTRACT_ALL        CO,
                               CURR_CO_STATUS      CUR,
                               CONTR_SERVICES_CAP  CS,
                               DIRECTORY_NUMBER    DI,
                               CONTR_DEVICES       CD,
                               STORAGE_MEDIUM      SM
                         WHERE CO.CUSTOMER_ID = CU.CUSTOMER_ID
          			         	 AND CUR.CO_ID      = CO.CO_ID
                           AND CU.CSTYPE NOT IN ('d', 'o')
                           AND CO.CO_EXT_CSUIN = C_CLIENTE.RC_PREDECESOR
                           AND CUR.CH_STATUS  NOT IN ('d', 'o')
                           AND CU.CSTRADECODE IN ( SELECT B.RC_TIPO_CREDITO
                                                     FROM RC_TR_TC@AXIS B
                                                    WHERE B.RC_TIPO_RELOJ = C_CLIENTE.RC_TIPO_RELOJ
                                                 )
                           AND CU.PRGCODE     IN ( SELECT C.ST_ID_SUB
                                                     FROM RC_SUB_TIPOS_RELOJ@AXIS C
                                                    WHERE C.ST_TIPO_RELOJ = C_CLIENTE.RC_TIPO_RELOJ
                                                 )
                           AND CS.CO_ID   = CO.CO_ID
                           AND CS.SEQNO   = ( SELECT MAX(CCL.SEQNO)
                                                FROM CONTR_SERVICES_CAP CCL
                                               WHERE CCL.CO_ID = CS.CO_ID
                                            )
                           AND DI.DN_ID     = CS.DN_ID AND (CS.CS_DEACTIV_DATE IS NULL)
                           AND DI.DN_STATUS = 'a'
                           AND CD.CO_ID     = CO.CO_ID AND (CD.CD_DEACTIV_DATE IS NULL)
                           AND CD_SEQNO     = ( SELECT MAX(CDD.CD_SEQNO)
                                                  FROM CONTR_DEVICES CDD
                                                 WHERE CDD.CO_ID = CO.CO_ID
                                              )
                           AND SM.SM_SERIALNUM = CD.CD_SM_NUM
                           --AND sm.sm_status = 'a' JHE 20-06-2007
                          GROUP BY CU.CUSTCODE,
                                   CU.CSTYPE,
                                   CU.PREV_BALANCE,
                                   CU.CSTRADECODE,
                                   CU.PRGCODE,
                                   CO.CO_ID,
                                   CUR.CH_STATUS,
                                   CO.CO_EXT_CSUIN,
                                   CO.PRODUCT_HISTORY_DATE,
                                   CO.TMCODE,
                                   CO.PLCODE,
                                   CS.MAIN_DIRNUM,
                                   CS.CS_DEACTIV_DATE,
                                   DI.DN_NUM,
                                   DI.DN_STATUS,
                                   SM.SM_SERIALNUM
                       );

               END IF;
            END IF;

           UPDATE READ.RC_PROCESS_CUST_ETAPA
               SET PROCESS_STATUS = LV_PROCESS_STATUS_W
             WHERE RC_PROCESS     = PN_RC_PROCESS
               AND SEQNO          = LC_DAT_CONTROL.SEQNO
               AND PROCESS_ID     = PN_PROCESS_ID;

              COMMIT;

      END LOOP;

    --FINALIZACION DE LAS INSERCIONES
      IF LN_COUNT_CUST>0 THEN

          UPDATE READ.RC_CONTROL
             SET REG_PROCESS      = REG_PROCESS + LN_COUNT_CUST,
                 DES_EVENTO       = DES_EVENTO ||'PROCESO:' || TO_CHAR(PN_PROCESS_ID) ||'-OK,'
           WHERE RC_PROCESS       = PN_RC_PROCESS
             AND SEQNO            = LC_DAT_CONTROL.SEQNO
             AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

          UPDATE READ.RC_EXEC_PROCESS_ID
             SET DATE_STOP      = SYSDATE,
                 DURATION       = ROUND((SYSDATE - DATE_START) * 1440,2),
                 TOT_REGISTROS  = LN_COUNT_CUST,
                 PROCESS_STATUS = LV_PROCESS_STATUS_F
           WHERE RC_PROCESS     = PN_RC_PROCESS
             AND SEQNO          = LC_DAT_CONTROL.SEQNO
             AND PROCESS_ID     = PN_PROCESS_ID
             AND PROCESS_STATUS = LV_PROCESS_STATUS_W;

           OPEN C_RC_PROCESS_CUST(LC_DAT_CONTROL.SEQNO,LC_DAT_CONTROL.FECHA_REFERENCIA);
           FETCH C_RC_PROCESS_CUST INTO LN_COUNT_CUST;
           LB_NOTFOUND:=C_RC_PROCESS_CUST%NotFound;
           CLOSE C_RC_PROCESS_CUST;

           IF LB_NOTFOUND THEN
                UPDATE READ.RC_CONTROL
                   SET PROCESS_STATUS   = LV_PROCESS_STATUS_W,
                       PER_STOP         = SYSDATE,
                       DURATION         = ROUND((SYSDATE - PER_START) * 1440,2)
                 WHERE RC_PROCESS       = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;


                 DELETE READ.RC_PROCESS_CUST_ETAPA
                  WHERE RC_PROCESS      = PN_RC_PROCESS
                   AND SEQNO            = LC_DAT_CONTROL.SEQNO
                   AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

                UPDATE READ.RC_CONTROL_PROCESOS
                   SET PROCESS_STATUS = 'R',
                       DES_EVENTO     = 'FACT_SEND_CALL-SUCCESFUL'
                 WHERE RC_PROCESS     = PN_RC_PROCESS
                   AND PROCESS_STATUS = 'E';

           END IF;

           COMMIT WORK;

           PV_OUT_ERROR:= 'RC_SEND-SUCESSFUL, PROCESS_ID:'||PN_PROCESS_ID||' '||LV_STOP_MANUAL;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'OK',
                         PV_ERROR        => PV_OUT_ERROR
                         );
       END IF;
  EXCEPTION
      WHEN LE_ERROR THEN
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'FACT_SEND ' || ' ERROR:'|| SQLCODE || ', PROCESS_ID:'||PN_PROCESS_ID||','|| SUBSTR(SQLERRM, 1, 80);
           ROLLBACK;
           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                        );

  END RC_SEND_3;


  --9--
  --JRO OPCION PARA MEJORAR TIEMPOS EN LA REACTIVACION
 --22/03/08 AUT. EGA Y VSO
  PROCEDURE RC_HEADER(PN_RC_PROCESS  IN NUMBER,
                      PD_PERIODO     IN  VARCHAR2,
                      PV_MARCA       VARCHAR2 DEFAULT NULL,
                      PV_OUT_ERROR   OUT VARCHAR2) IS

        CURSOR C_RC_PROCESS IS
             SELECT RC_PROCESS,
                    CANT_PROCESS_ID,
                    CANT_REG_COMMIT,
                    TABLE_ORIGEN,
                    TABLE_DESTINO,
                    CANT_DIAS
               FROM READ.RC_PROCESS
              WHERE RC_PROCESS=PN_RC_PROCESS;

      CURSOR C_RC_CONTROL_MAX_SEQNO IS
             SELECT NVL(MAX(SEQNO),0)
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS=PN_RC_PROCESS;

      CURSOR C_RC_CONTROL IS
             SELECT RC_PROCESS,
                    SEQNO,
                    FECHA_REFERENCIA
               FROM READ.RC_CONTROL
              WHERE RC_PROCESS=PN_RC_PROCESS
                AND PROCESS_STATUS='R'
                AND ROWNUM=1
              ORDER BY  PER_START;

      CURSOR C_RC_PROCESS_CUST(LV_SEQNO NUMBER, LV_PER_REF DATE) IS
             SELECT COUNT(*)
               FROM READ.RC_PROCESS_CUST
              WHERE PROCESS_STATUS   = 'R'
                AND RC_PROCESS       = PN_RC_PROCESS
                AND SEQNO            = LV_SEQNO
                AND FECHA_REFERENCIA = LV_PER_REF;

     --JRO: al 03/05/2010
      CURSOR C_RC_FEHA_MENSAJERIA IS
             SELECT MAX(pr.rundatemensajeria), MAX(pr.rundateAxis)
               FROM READ.RC_Programmer Pr
              WHERE pr.RC_PROCESS=PN_RC_PROCESS;
              
      CURSOR C_SeqMensajeria IS
              SELECT  RC_RUN_MENSAJERIA.NEXTVAL FROM dual;
      --
      LD_FECHA_START             DATE;
      LD_FECHA_MENSAJERIA  DATE;
      LD_FECHA_AXIS                 DATE;      
      LN_SEQNO_MENSAJERIA NUMBER;
      LE_ERROR                           EXCEPTION;
      LB_NOTFOUND                 BOOLEAN;
      LN_CANT_PROCESS_ID    NUMBER;
      LN_COUNT_REG                NUMBER;
      LN_MAX_SEQNO                NUMBER;
      LC_DAT_PROCESS             C_RC_PROCESS%ROWTYPE;
      LC_DAT_CONTROL           C_RC_CONTROL%ROWTYPE;
      LV_OUT_ERROR                 VARCHAR2(300);
      LV_SQL                                 VARCHAR2(100);


      BEGIN
       LN_COUNT_REG   := 0;
       LD_FECHA_START := SYSDATE;
       LV_OUT_ERROR:= '.';
       --
       UPDATE  READ.RC_CONTROL CN 
                SET CN.PER_STOP=SYSDATE,
                         CN.PROCESS_STATUS='W'
        WHERE  CN.RC_PROCESS=PN_RC_PROCESS
               AND  CN.PROCESS_STATUS='R';
        COMMIT;
       --
       UPDATE  READ.RC_CONTROL_PROCESOS CP 
                SET CP.PROCESS_STATUS='R'
        WHERE CP.RC_PROCESS=PN_RC_PROCESS
               AND CP.PROCESS_STATUS='E';
        COMMIT;
        --
        DELETE READ.RC_PROCESS_CUST;
        COMMIT;              
        --                     
        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO LC_DAT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS; 
       IF LB_NOTFOUND THEN
          PV_OUT_ERROR:='RC_HEADER- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_PROCESS';
          RAISE LE_ERROR;
       END IF;

       OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;

       IF LB_NOTFOUND THEN
           OPEN C_RC_CONTROL_MAX_SEQNO;
          FETCH C_RC_CONTROL_MAX_SEQNO INTO LN_MAX_SEQNO;
          CLOSE C_RC_CONTROL_MAX_SEQNO;
          LN_MAX_SEQNO:=LN_MAX_SEQNO+1;

          INSERT INTO READ.RC_CONTROL(RC_PROCESS,
                                      SEQNO,
                                      FECHA_REFERENCIA,
                                      PER_START,
                                      PER_STOP,
                                      DURATION,
                                      REG_PROCESS,
                                      PROCESS_STATUS,
                                      DES_EVENTO)
          VALUES(
                 PN_RC_PROCESS,
                 LN_MAX_SEQNO,
                 TO_DATE(PD_PERIODO,'DD/MM/YYYY'),
                 SYSDATE,
                 NULL,
                 0,
                 0,
                 'R',
                 'PROCESO EN EJECUCION:'
          );
          COMMIT;


       END IF;

        OPEN C_RC_CONTROL;
       FETCH C_RC_CONTROL INTO LC_DAT_CONTROL;
       LB_NOTFOUND:=C_RC_CONTROL%NotFound;
       CLOSE C_RC_CONTROL;
       IF LB_NOTFOUND THEN
           PV_OUT_ERROR:='RC_HEADER- ADVERTENCIA NO EXISTEN REGISTROS EN LA C_RC_CONTROL';
          RAISE LE_ERROR;
       END IF;

       OPEN C_RC_PROCESS_CUST(LC_DAT_CONTROL.SEQNO,LC_DAT_CONTROL.FECHA_REFERENCIA);
       FETCH C_RC_PROCESS_CUST INTO LN_COUNT_REG;
       CLOSE C_RC_PROCESS_CUST;
       IF LN_COUNT_REG>0 THEN
          LV_OUT_ERROR:='RC_HEADER- ADVERTENCIA EXISTEN REGISTROS NO PROCESADOS: '||LN_COUNT_REG||', SE EJECUTA EL/LOS PROCESS_ID PENDIENTES';
          PV_OUT_ERROR:='RC_HEADER-SUCESSFUL';
          RAISE LE_ERROR;
       END IF;

       RC_VALIDATE_DAT(PN_RC_PROCESS      => PN_RC_PROCESS,
                       PV_OUT_ERROR       => PV_OUT_ERROR);

       IF NOT PV_OUT_ERROR IS NULL THEN
           RAISE LE_ERROR;
       END IF;

       --ACTUALIZACION DE NUEVOS CLIENTES
       IF PN_RC_PROCESS=1 THEN
            --JRO: [4737]-Mejoras al proceso de mensajeria para la ejecución desde Control-M
               OPEN C_RC_FEHA_MENSAJERIA;
             FETCH C_RC_FEHA_MENSAJERIA INTO LD_FECHA_MENSAJERIA,LD_FECHA_AXIS;
             LB_NOTFOUND:=C_RC_FEHA_MENSAJERIA%NotFound;
             CLOSE C_RC_FEHA_MENSAJERIA;
             IF LB_NOTFOUND THEN
                 LD_FECHA_MENSAJERIA:=PD_PERIODO;    
             END IF;
             LD_FECHA_MENSAJERIA:=LD_FECHA_MENSAJERIA-20;  --Se cambia a la LD_FECHA_MENSAJERIA -15  para que tenga un margen de 30 días atras, igual la tabla rc_comentarios_cuenta de AXIS sólo tendrá las cuentas pendientes
             LD_FECHA_AXIS:=LD_FECHA_MENSAJERIA;
            --
             RC_UPDATE_CUSTOMER_1(PN_RC_PROCESS,
                                  LC_DAT_PROCESS.TABLE_ORIGEN,
                                  LD_FECHA_MENSAJERIA,
                                  PV_MARCA,
                                  nvl(LC_DAT_PROCESS.CANT_DIAS,0),
                                  PV_OUT_ERROR);
             IF PV_OUT_ERROR IS NOT NULL THEN
                 RAISE LE_ERROR;
             END IF;
       ELSE
             IF PN_RC_PROCESS=2 THEN
                 RC_UPDATE_CUSTOMER_2(PN_RC_PROCESS,
                                      PV_OUT_ERROR);
             END IF;
       END IF;

       --DISTRIBUCION DE LOS CLIENTES PARA LOS PROCESS_ID
       IF NVL(LC_DAT_PROCESS.CANT_PROCESS_ID,1)=1 THEN
              LN_CANT_PROCESS_ID:=1;
       ELSE
              LN_CANT_PROCESS_ID:=LC_DAT_PROCESS.CANT_PROCESS_ID;
       END IF;


       UPDATE READ.RC_CONTROL
          SET PER_START        = SYSDATE
        WHERE RC_PROCESS       = PN_RC_PROCESS
          AND SEQNO            = LC_DAT_CONTROL.SEQNO
          AND FECHA_REFERENCIA = LC_DAT_CONTROL.FECHA_REFERENCIA;

       COMMIT;



       IF PN_RC_PROCESS=3 THEN
          LV_SQL:= 'TRUNCATE TABLE READ.RC_TMP_DATOS_GENERALES_STATUS';
          EXECUTE IMMEDIATE LV_SQL;

           RC_DISTRIBUTION_CUSTOMER_3(PN_RC_PROCESS,
                                      LN_CANT_PROCESS_ID,
                                      PV_OUT_ERROR
                                      );
       ELSE
          LV_SQL:= 'TRUNCATE TABLE READ.RC_TMP_DATOS_GENERALES';
          EXECUTE IMMEDIATE LV_SQL;

           RC_DISTRIBUTION_CUSTOMER(PN_RC_PROCESS,
                                    LN_CANT_PROCESS_ID,
                                    PV_OUT_ERROR
                                    );

       END IF;
       
       IF NOT PV_OUT_ERROR IS NULL THEN
          IF NOT PV_OUT_ERROR='NO_DATA' THEN
                    RAISE LE_ERROR;
          END IF;
       END IF;
       
      --JRO: [4737]-Mejoras al proceso de mensajeria para la ejecución desde Control-M
       IF PN_RC_PROCESS=1 THEN
             OPEN C_SeqMensajeria ;
            FETCH C_SeqMensajeria INTO LN_SEQNO_MENSAJERIA;
            CLOSE C_SeqMensajeria;                             

            INSERT INTO READ.RC_PROGRAMMER(rc_process, 
                                                                                        seqno, 
                                                                                        rundatemensajeria, 
                                                                                        lastmoddate,
                                                                                        rundateAxis
                                                                                        )
                                          VALUES(PN_RC_PROCESS,
                                                           LN_SEQNO_MENSAJERIA,
                                                           trunc(SYSDATE),
                                                           SYSDATE,
                                                           LD_FECHA_AXIS 
                                                          );
            
       END IF;
       --
       PV_OUT_ERROR:='RC_HEADER-SUCESSFUL';

       RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                     PD_FECHA_START  => LD_FECHA_START,
                     PD_FECHA_STOP   => SYSDATE,
                     PV_STATUS       => 'OK',
                     PV_ERROR        => PV_OUT_ERROR
                     );
  EXCEPTION
      WHEN LE_ERROR THEN
           IF LN_COUNT_REG=0 THEN
               RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                             PD_FECHA_START  => LD_FECHA_START,
                             PD_FECHA_STOP   => SYSDATE,
                             PV_STATUS       => 'ER',
                             PV_ERROR        => PV_OUT_ERROR || LV_OUT_ERROR
                            );
           ELSE
               RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                             PD_FECHA_START  => LD_FECHA_START,
                             PD_FECHA_STOP   => SYSDATE,
                             PV_STATUS       => 'OK',
                             PV_ERROR        => PV_OUT_ERROR || LV_OUT_ERROR
                            );
           END IF;

      WHEN OTHERS THEN
           PV_OUT_ERROR := 'RC_HEADER- ' || SUBSTR(SQLERRM, 1, 80);

           ROLLBACK;

           RC_SAVE_STEP (PN_RC_PROCESS   => PN_RC_PROCESS,
                         PD_FECHA_START  => LD_FECHA_START,
                         PD_FECHA_STOP   => SYSDATE,
                         PV_STATUS       => 'ER',
                         PV_ERROR        => PV_OUT_ERROR
                         );

  END RC_HEADER;


  --11--
  FUNCTION RC_RETORNA_CANT_PROCESS(PV_PROCESS_ID IN NUMBER,
                                  PV_ERROR      OUT VARCHAR2) RETURN NUMBER IS

    CURSOR C_RC_PROCESS IS
         SELECT CANT_PROCESS_ID
           FROM READ.RC_PROCESS
          WHERE RC_PROCESS=PV_PROCESS_ID;

   PV_CANT_PROCESS    NUMBER;
   LB_NOTFOUND        BOOLEAN;

  BEGIN

        OPEN C_RC_PROCESS;
       FETCH C_RC_PROCESS INTO PV_CANT_PROCESS;
       LB_NOTFOUND:=C_RC_PROCESS%NotFound;
       CLOSE C_RC_PROCESS;

       IF LB_NOTFOUND THEN
          PV_CANT_PROCESS:=0;
       END IF;

       RETURN(PV_CANT_PROCESS);

   EXCEPTION
      WHEN OTHERS THEN
           PV_ERROR := 'FACT_GENERA_CALLS- ' || SUBSTR(SQLERRM, 1, 80);

  END RC_RETORNA_CANT_PROCESS;

  -----------------------------------------------------------------------
  -- [3934] RFE CASO 441
  -- 10 de Noviembre del 2008
  -----------------------------------------------------------------------
  PROCEDURE RC_GENERA_RCFECHAS(PV_ERROR       OUT VARCHAR2) IS


  lv_dia_ini          varchar2(2);
  lv_dia_fin          varchar2(2);
  lv_mes_ini          varchar2(2);
  lv_anio_ini         varchar2(4);
  lv_mes_fin          varchar2(2);
  lv_anio_fin         varchar2(4);

  BEGIN

    FOR I IN (select a.id_ciclo_admin ,b.dia_ini_ciclo,b.dia_fin_ciclo
              From fa_ciclos_axis_bscs a, fa_ciclos_bscs b
              where a.id_ciclo = b.id_ciclo)

    LOOP

        lv_dia_ini := I.Dia_Ini_Ciclo;
        lv_dia_fin := I.Dia_fin_ciclo;
        --
        If to_NUMBER(to_char(sysdate,'dd')) < to_NUMBER(lv_dia_ini) Then

          If to_NUMBER(to_char(sysdate,'mm')) = 1 Then
            lv_mes_ini := '12';
            lv_anio_ini := to_char(sysdate,'yyyy')-1;
          Else
            lv_mes_ini := lpad(to_char(sysdate,'mm')-1,2,'0');
            lv_anio_ini := to_char(sysdate,'yyyy');
          End If;
          lv_mes_fin := to_char(sysdate,'mm');
          lv_anio_fin := to_char(sysdate,'yyyy');

        Else

          If to_NUMBER(to_char(sysdate,'mm')) = 12 Then
            lv_mes_fin := '01';
            lv_anio_fin := to_char(sysdate,'yyyy')+1;
          Else
            lv_mes_fin := lpad(to_char(sysdate,'mm')+1,2,'0');
            lv_anio_fin := to_char(sysdate,'yyyy');
          End If;
          lv_mes_ini := to_char(sysdate,'mm');
          lv_anio_ini := to_char(sysdate,'yyyy');

        End If;

        GV_CODES_INI(I.id_ciclo_admin) :=to_date(lv_dia_ini||'/'||lv_mes_ini||'/'||lv_anio_ini,'dd/mm/yyyy');
        --pv_per_fin := lv_dia_fin||'/'||lv_mes_fin||'/'||lv_anio_fin;
        --TV_CODES_FIN(ZC.billcycle) := pv_per_fin;

    END LOOP;


   EXCEPTION
      WHEN OTHERS THEN
           PV_ERROR := 'RC_GENERA_RCFECHAS- ' || SUBSTR(SQLERRM, 1, 80);

  END RC_GENERA_RCFECHAS;



  -----------------------------------------------------------------------------
  --- END PROCEDIMIENTO Y FUNCIONES
  -----------------------------------------------------------------------------

END RC_EXTRAE_DATOS_BSCS;
/
