create or replace procedure read.PR_SALDO_BSCS_IR(pv_account in varchar2, pv_ciclo in varchar2, pv_mes_anio in varchar2 , pn_saldo out Number, pv_error out varchar2 ) is


cursor regi(lvIdCiclo in varchar2,lvDescr in varchar2) is 
	select /*+ rule */ mora                              rango, 
	        count(s.customer_id)                clientes ,
	        round(sum(total_deuda),2)         cartera, 
	        0                       saldos, 
	        round(sum(TotPagos_Recuperado),2)    pagos, 
	        round(sum(DebitoRecuperado),2)       notas_debitos,  
	        round(sum(CreditoRecuperado),2)      notas_creditos, 
	        decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
	from ope_cuadre s, customer_all r
	where s.customer_id = r.customer_id
  AND nvl(s.tipo_forma_pago,6) = decode(0,0,nvl(s.tipo_forma_pago,6),0)
  and upper(nvl(s.region,'x')) like '%'||lvDescr||'%'
	and s.customer_id = r.customer_id
	and mora = 0
	and total_deuda >= 0
	and id_ciclo = lvIdCiclo
 -- and s.total_deuda not in(0)
	group by mora;
  
cursor regi_total_cartera_vencida(lvIdCiclo in varchar2,lvDescr in varchar2) is 
	select /*+ rule */ tipo, 
	        count(s.customer_id)                clientes ,
	        round(sum(total_deuda),2)         cartera, 
	        0                       saldos, 
	        round(sum(TotPagos_Recuperado),2)    pagos, 
	        round(sum(DebitoRecuperado),2)       notas_debitos,  
	        round(sum(CreditoRecuperado),2)      notas_creditos, 
	        decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
	from ope_cuadre s, customer_all r
	where  s.customer_id = r.customer_id
  AND nvl(s.tipo_forma_pago,6) = decode(0,0,nvl(s.tipo_forma_pago,6),0)
  and upper(nvl(s.region,'x')) like '%'||lvDescr||'%'
	and s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = lvIdCiclo
 -- and s.total_deuda not in(0)
  group by tipo;  
  
  
  cursor regi_pagos_exceso(lvIdCiclo in varchar2,lvDescr in varchar2) is 
	select /*+ rule */ tipo, 
	        mora                              rango, 
	        count(s.customer_id)                clientes ,
	        round(sum(total_deuda),2)         cartera, 
	        0                       saldos, 
	        round(sum(TotPagos_Recuperado),2)    pagos, 
	        round(sum(DebitoRecuperado),2)       notas_debitos,  
	        round(sum(CreditoRecuperado),2)      notas_creditos, 
	        0 recuperacion
	from ope_cuadre s, customer_all r
	where  s.customer_id = r.customer_id
  AND nvl(s.tipo_forma_pago,6) = decode(0,0,nvl(s.tipo_forma_pago,6),0)
  and upper(nvl(s.region,'x')) like '%'||lvDescr||'%'
	and s.customer_id = r.customer_id
  and tipo = '3_VNE'
  and id_ciclo = lvIdCiclo
  group by tipo, mora;
  
  
  cursor pagos_no_facturados(lvIdCiclo in varchar2,lvfecha_ini in varchar2,lvfecha_fin in varchar2,lvDescr in varchar2) is
    select /*+ rule */ count(*) cantidad, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor
    from cashreceipts_all ca, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where ca.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)
    and ca.catype in (1,3,9)
    and cu.costcenter_id =decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    and ca.cachkdate >= to_date(lvfecha_ini,'dd/mm/yyyy')
    And ca.cachkdate <= to_date(lvfecha_fin,'dd/mm/yyyy')
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.csactivated <= to_date(lvfecha_fin,'dd/mm/yyyy')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo);
    
    
    cursor regi_factura_cero(lvIdCiclo in varchar2,lvDescr in varchar2) is 
     	select /*+ rule */ '1_NF', 
	        mora                              rango, 
	        count(s.customer_id)                clientes ,
	        round(sum(total_deuda),2)         cartera, 
	        0                       saldos, 
	        round(sum(TotPagos_Recuperado),2)    pagos, 
	        round(sum(DebitoRecuperado),2)       notas_debitos,  
	        round(sum(CreditoRecuperado),2)      notas_creditos, 
	        0                                 recuperacion
	from ope_cuadre s, customer_all r
	where s.customer_id = r.customer_id
  AND nvl(s.tipo_forma_pago,6) = decode(0,0,nvl(s.tipo_forma_pago,6),0)
  and upper(nvl(s.region,'x')) like '%'||lvDescr||'%'
	and s.customer_id = r.customer_id
	and tipo='1_NF'
	and id_ciclo = lvIdCiclo
	group by mora;
  
  -- Creditos No Facturados
  
  cursor creditos_no_facturados(lvIdCiclo in varchar2,lvfecha_ini in varchar2,lvfecha_fin in varchar2,lvDescr in varchar2)is
	--CREDITOS REGULARES
   select /*+ rule */ cu.customer_id, sum(oh.ohinvamt_doc) credito
    from orderhdr_all oh, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where oh.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)
   -- and to_char(oh.ohrefdate,'mm/yyyy') =lvfecha_ini
    and cu.costcenter_id = decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    --and (prgcode in (:P_PRODUCTO*2,:P_PRODUCTO*2-1) or prgcode >= decode(:P_PRODUCTO,0,0,7))
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.csactivated <= to_date(lvfecha_fin,'dd/mm/yyyy')		
		and oh.ohstatus = 'CM'
		and oh.ohinvtype <> 5
		-----------------------------------------
		--and to_char(oh.ohrefdate,'dd') <> '24'
		-- SuD Reyna Asencio-------------------
		--------------------------------------
	--	and substr(oh.ohrefdate,1,2)<>substr(:bl_cabecera.fecha_inicia,1,2)
		and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo)
		group by cu.customer_id
		UNION ALL
	--CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
    select /*+ rule */ cu.customer_id, sum(f.amount) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)	
    and f.entdate between 
        to_date(lvfecha_ini,'dd/mm/yyyy') and 
        to_date(lvfecha_fin,'dd/mm/yyyy')
    and cu.costcenter_id = decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.csactivated <= to_date(lvfecha_fin,'dd/mm/yyyy')						
		and cu.paymntresp = 'X'
		and amount < 0
		and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo)
		group by cu.customer_id		
		UNION ALL
    --CREDITOS COMO OCCS NEGATIVOS CTAS hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)
    and f.entdate between 
          to_date(lvfecha_ini,'dd/mm/yyyy') and 
          to_date(lvfecha_fin,'dd/mm/yyyy')
    and cu.costcenter_id = decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    --and (prgcode in (:P_PRODUCTO*2,:P_PRODUCTO*2-1) or prgcode >= decode(:P_PRODUCTO,0,0,7))
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.csactivated <= to_date(lvfecha_fin,'dd/mm/yyyy')				
    and cu.paymntresp is null
    and f.amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = lvIdCiclo)
    group by cu.customer_id;


    -- Cargos no Facturados

    cursor cargos_no_facturados(lvIdCiclo in varchar2,lvfecha_ini in varchar2,lvfecha_fin in varchar2,lvDescr in varchar2) is
  	-- cargos como OCCs negativos cuentas padres
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)
    and f.entdate between
        to_date(lvfecha_ini,'dd/mm/yyyy') and 
        to_date(lvfecha_fin,'dd/mm/yyyy')
    and cu.paymntresp = 'X'
    and f.amount > 0
    and f.sncode <> 103    
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.costcenter_id = decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo =lvIdCiclo)
    group by cu.customer_id
    UNION ALL
	-- cargos como OCCs negativos cuentas hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(lvfecha_ini,1,2)
    and f.entdate between 
         to_date(lvfecha_ini,'dd/mm/yyyy') and 
         to_date(lvfecha_fin,'dd/mm/yyyy')
    and cu.paymntresp is null
    and customer_id_high is not null
    and f.amount > 0
    and f.sncode <> 103
    and cu.csactivated >= to_date(lvfecha_ini,'dd/mm/yyyy')
    and cu.costcenter_id = decode(lvDescr,'GUAYAQUIL','1','QUITO','2','')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = lvIdCiclo)
    group by cu.customer_id;



   CLIENTES_TOTAL                 NUMBER:=0;
   clientes                       NUMBER:=0; 
   --clientes_0                     NUMBER:=0;
   clientes_v                     NUMBER:=0; 
   clientes_pe                    NUMBER:=0;  
   CARTERA_TOTAl                  NUMBER:=0;
   cartera                        NUMBER:=0; 
   --cartera_0                      NUMBER:=0;
   cartera_v                      NUMBER:=0;
   cartera_pe                     NUMBER:=0;
   PAGOS_TOTAL                    NUMBER:=0;    
   pagos                          NUMBER:=0;
   pagos_0                        NUMBER:=0;
   pagos_v                        NUMBER:=0; 
   pagos_pe				              	NUMBER:=0;       
   DEBITOS_TOTAL                	NUMBER:=0;
   debitos 				              	NUMBER:=0;
   debitos_0  			              NUMBER:=0;
   debitos_v 				              NUMBER:=0;
   debitos_pe				              NUMBER:=0;	
   CREDITOS_TOTAL 	              NUMBER:=0;	
   creditos 				              NUMBER:=0;
   creditos_0 		              	NUMBER:=0;
   creditos_v 			              NUMBER:=0;
   creditos_pe			              NUMBER:=0; 
   ln_saldo                       NUMBER:=0 ;  
	 lv_account                     varchar2(10);
   lv_account_descr               varchar2(20);
   --lv_mes_anio                    varchar2(10);
   lv_fecha_ini                   varchar2(10);
   lv_fecha_fin                   varchar2(10);
   lv_mes                         varchar2(2);
   lv_anio                        varchar2(4);
   lv_dia                         varchar2(2);
   lv_linea 				              varchar2(3000);
   lb_cursor                      boolean := False;
   lb_regi_total_cartera_vencida  boolean := False; 
   lb_regi_pagos_exceso           boolean := False;
   lb_regi_factura_cero           boolean := False;
   lr_reg                         regi%ROWTYPE; 
   lregi_total_cartera_vencida    regi_total_cartera_vencida%ROWTYPE;
   lregi_factura_cero             regi_factura_cero%ROWTYPE;  
   lregi_pagos_exceso             regi_pagos_exceso%ROWTYPE;   
   
Begin
   --lv_account:='';
   --lv_mes_anio:='';
   --lv_mes:='';
   --lv_anio:='';
   lv_dia:='';
  lv_account:= pv_account;
  
   lv_mes:=SUBSTR(pv_mes_anio,1,2);
   lv_anio:=SUBSTR(pv_mes_anio,3,4);
   
   If pv_ciclo='01' Then
     lv_dia:='24';
   End If;
   If pv_ciclo='02' Then
     lv_dia:='08';
   End If;
   If pv_ciclo='03' Then
     lv_dia:='15';
   End If;
   If pv_ciclo='04' Then
     lv_dia:='02';
   End If;
   
  
  
  Select to_char(to_date(lv_dia||'/'||lv_mes||'/'||lv_anio,'dd/mm/yyyy'),'dd/mm/yyyy') Into  lv_fecha_ini 
   From dual;
   
   Select to_char(last_day(to_date(lv_dia||'/'||lv_mes||'/'||lv_anio,'dd/mm/yyyy')),'dd/mm/yyyy') Into  lv_fecha_fin
   From dual;

    
  dbms_output.put_line('account -> '||lv_account);
  dbms_output.put_line('ciclo -> '||pv_ciclo);
  
  dbms_output.put_line('fecha inicio -> '|| lv_fecha_ini );
  dbms_output.put_line('fecha final -> '||lv_fecha_fin);
  
 
    if lv_account='130101' then   
         lv_account_descr:='GUAYAQUIL';
    else
       if lv_account='130102' then   
         lv_account_descr:='QUITO';
       end if;
    end if;
    
 
  OPEN regi(pv_ciclo,lv_account_descr);
   FETCH regi INTO lr_reg;
   IF regi%FOUND THEN
       lb_cursor := true; 
   END IF;
   CLOSE regi;
   
   OPEN regi_total_cartera_vencida(pv_ciclo,lv_account_descr);
   FETCH regi_total_cartera_vencida INTO lregi_total_cartera_vencida;
   IF regi_total_cartera_vencida%FOUND THEN
       lb_regi_total_cartera_vencida := true; 
   END IF;
   CLOSE regi_total_cartera_vencida;
   
   
   OPEN regi_pagos_exceso(pv_ciclo,lv_account_descr);
   FETCH regi_pagos_exceso INTO lregi_pagos_exceso;
   IF regi_pagos_exceso%FOUND THEN
       lb_regi_pagos_exceso := true; 
   END IF;
  CLOSE regi_pagos_exceso;
   
   
    OPEN regi_factura_cero(pv_ciclo,lv_account_descr);
   FETCH regi_factura_cero INTO lregi_factura_cero;
   IF regi_factura_cero%FOUND THEN
       lb_regi_factura_cero := true; 
   END IF;
   CLOSE regi_factura_cero;
   
   for k in regi_factura_cero(pv_ciclo,lv_account_descr) loop
	
    pagos_0    := nvl(k.PAGOS, 0);
    debitos_0  := nvl(k.NOTAS_DEBITOS, 0);
    creditos_0 := nvl(k.NOTAS_CREDITOS, 0);	
    	 lv_linea:=pagos_0||','||debitos_0||','||creditos_0;
      	
   
    end loop; 
   	
   for k in pagos_no_facturados(pv_ciclo,lv_fecha_ini,lv_fecha_fin,lv_account_descr) loop
		pagos_0    := pagos_0 + nvl(k.valor, 0);
   end loop;
   
   for k in creditos_no_facturados(pv_ciclo,lv_fecha_ini,lv_fecha_fin,lv_account_descr) loop
		creditos_0    := creditos_0 + nvl(k.credito, 0);
   end loop;
   
   for k in cargos_no_facturados(pv_ciclo,lv_fecha_ini,lv_fecha_fin,lv_account_descr) loop
		debitos_0    := debitos_0 + nvl(k.cargo, 0);
   end loop;
   
    
   If lb_cursor then
    for k in regi(pv_ciclo,lv_account_descr) loop
    	 
        clientes := nvl(k.CLIENTES, 0);
        cartera  := nvl(k.CARTERA, 0);
        pagos    := nvl(k.PAGOS, 0);
        debitos  := nvl(k.NOTAS_DEBITOS, 0);
        creditos := nvl(k.NOTAS_CREDITOS, 0);
       
    end loop;
 end if;  
 
  If lb_regi_total_cartera_vencida then
 	  
    for k in regi_total_cartera_vencida(pv_ciclo,lv_account_descr) loop
    	  
        clientes_v := nvl(k.CLIENTES, 0);
        cartera_v  := nvl(k.CARTERA, 0);
        pagos_v    := nvl(k.PAGOS, 0);
        debitos_v  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_v := nvl(k.NOTAS_CREDITOS, 0);
    
    end loop;
    
 end if;  
  
  If lb_regi_pagos_exceso then
    for k in regi_pagos_exceso(pv_ciclo,lv_account_descr) loop
        clientes_pe := nvl(k.CLIENTES, 0);
        cartera_pe  := nvl(k.CARTERA, 0);
        pagos_pe    := nvl(k.PAGOS, 0);
        debitos_pe  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_pe := nvl(k.NOTAS_CREDITOS, 0);
     
    end loop;
 end if;  

   CLIENTES_TOTAL :=  clientes + clientes_v + clientes_pe;
   CARTERA_TOTAl  :=  cartera  + cartera_v  + cartera_pe;
   PAGOS_TOTAL    :=  pagos    + pagos_v    + pagos_pe + pagos_0;   
   DEBITOS_TOTAL  :=  debitos  + debitos_v  + debitos_pe + debitos_0;
   CREDITOS_TOTAL :=  creditos + creditos_v + creditos_pe + creditos_0;
   
   dbms_output.put_line('CLIENTES TOTAL -> '||CLIENTES_TOTAL);
  ln_saldo:=(CARTERA_TOTAl -(PAGOS_TOTAL - pagos_0)+(DEBITOS_TOTAL - debitos_0) + (CREDITOS_TOTAL - creditos_0));
 
  pn_saldo:=round(ln_saldo,2);
 
  pv_error:=null;
  
   /*bloque para eliminar los warning*/
   
   If lb_regi_factura_cero Then
  
    dbms_output.put_line('usando bandera');
   End If;
   
   /*bloque para eliminar los warning*/
  
 exception 
when  others then
   pn_saldo:=0;
   pv_error:='Error en procedimiento  PR_SALDO_BSCS_IR -> '||sqlerrm;
   rollback;
    
  
end PR_SALDO_BSCS_IR;
/
