CREATE OR REPLACE procedure      llena_ue_base_zona_cliente is

CURSOR llena_tabla is
select a.custcode,a.customer_id
  from customer_all a
  where a.custcode in 
  (select custcode from read.ue_base_zona_cliente) ;

row_count number := 0;
begin    
    
 For t in llena_tabla loop   
    
    update
      read.ue_base_zona_cliente a
    set  a.customer_id=t.customer_id
    where  a.custcode = t.custcode ;
    row_count := row_count +1;
    if row_count = 10000 then
       commit;
       row_count := 0;
    end if;    
 end loop;
 
 commit;
 end;
/

