create or replace procedure read.PR_PSALDO_BSCS_IR(pv_error  out  varchar2) Is
ln_cont                    Number;
ln_saldo                   Number;
ln_saldo_total             Number;
ln_saldo_anexo             Number;
ln_saldo_cuenta            Number;
ln_mes                     Number;
ln_anio                    Number;
lv_cont                    Varchar2(10);
lv_account                 Varchar2(10);
lv_existe                  Varchar2(10);
lv_mes_anio                Varchar2(10);
lv_mes_anio2               Varchar2(10);
lv_mes                     Varchar2(2);
lv_anio                    Varchar2(4);
lv_tipo_account            Varchar2(1);
lv_account_group           Varchar2(10);
lv_error                   Varchar2(300);

Begin

 ln_cont:=4;
 lv_account:='';
 --lv_existe:='';
 --lv_mes_anio2:='';
 --lv_mes_anio:='';
 ln_saldo:=0;
 ln_saldo_anexo:=0;
 ln_saldo_cuenta:=0;
 ln_saldo_total:=0;
 lv_cont:='0';
 ln_mes:=0;
 lv_mes:='';
 lv_anio:='';
 
 /*validacion para extraccion con respecto a mes y a�o */
 
 Select to_number(to_char(Sysdate,'MM')) Into ln_mes
  From  dual;
  
  Select to_number(to_char(Sysdate,'YYYY')) Into ln_anio  
   From  dual;
   
   --ln_mes:= 7;
   --ln_anio:=2010;
   
  If ln_mes  = 1 Then
   ln_anio:=ln_anio-1;
   lv_anio:= to_char(ln_anio);
   ln_mes:= 12;
   lv_mes:=''||to_char(ln_mes);
  Else
   lv_anio:= to_char(ln_anio);
     
     ln_mes:=ln_mes-1;
     
      If ln_mes < 10 Then
         lv_mes:='0'||to_char(ln_mes);
      Else
         lv_mes:=''||to_char(ln_mes);
      End If;
  End If;
  
  lv_mes_anio:=lv_mes||'/'||lv_anio;
  lv_mes_anio2:=lv_mes||lv_anio;
  dbms_output.put_line(' mes anio -> '||lv_mes_anio);				
  dbms_output.put_line(' mes anio -> '||lv_mes_anio2);		
 For j In 1 .. 2 Loop
   
   lv_account:='13010'||j;
   dbms_output.put_line(lv_account);
 /*
  --  select a la tabla ps_pr_asigcta_tbl
    
    Select pr_tipo_account
          ,pr_account_group into lv_tipo_account,lv_account_group
    From sysadm.ps_pr_asigcta_tbl 
    Where Account=lv_account

 */
  For i In 1 .. ln_cont Loop
    lv_cont:=lv_cont||i;
    dbms_output.put_line('ciclo -> '||lv_cont);
    
    pr_saldo_bscs_ir(pv_account => lv_account,
                     pv_ciclo => lv_cont,
                     pv_mes_anio => lv_mes_anio2,
                     pn_saldo => ln_saldo,
                     pv_error => lv_error);
   
    ln_saldo_total:=ln_saldo_total+ ln_saldo;   
                
    ln_saldo:=0;                 
    lv_cont:='0';
  End Loop;
  
  dbms_output.put_line('Saldo Total -> '||ln_saldo_total||' Cuenta -> '||lv_account);
  /*
   -- validacion si ya existe
    
    select 'X',
           pr_salanex,
           pr_salcta into lv_existe,ln_saldo_anexo,ln_saldo_cuenta
    FROM  sysadm.ps_pr_sal_anex_cta 
    where account=lv_account
    and pr_mes_anio=lv_mes_anio 
  
      if lv_existe<>'X' then
  
      Insert Into sysadm.ps_pr_sal_anex_cta(Account,pr_salanex,pr_mes_anio,pr_salcta,pr_tipo_account,pr_account_group) 
      Values(lv_account,ln_saldo_total,lv_mes_anio,0,lv_tipo_account,lv_account_group) 
     
     else
  
     update sysadm.ps_pr_sal_anex_cta set pr_salanex=ln_saldo_total 
     where account=lv_account 
     and pr_mes_anio=lv_mes_anio
    
    end if;
  */
  
   End Loop;
  
  
 pv_error:=lv_error;
 
 exception 
when  others then
  
   pv_error:='Error en procedimiento  PR_PSALDO_BSCS_IR -> -> '||sqlerrm;
   rollback;
   
end PR_PSALDO_BSCS_IR;
/
