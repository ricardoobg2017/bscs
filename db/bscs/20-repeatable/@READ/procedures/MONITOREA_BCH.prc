CREATE OR REPLACE procedure      monitorea_BCH (pv_sesion out number,pv_procesados out number, pv_por_procesar out number,pv_error out number,pv_cantidad out number,pv_tiempo out number) is


sesionesb number;
temp number;
temp2 number;
procesados number;
por_procesar number;
cantidad number;
tiempo date;
tiempo2 date;

begin 

select count(*) contador into sesionesb 
from v$process ,v$session 
where v$process.addr=paddr
and osuser = 'bscsprod'
and upper(v$session.username)='BCH' 
and taddr is null;

pv_sesion := sesionesb;

begin
  SELECT  nvl(count(*),0) proce into procesados FROM BCH_PROCESS_CUST where CUSTOMER_PROCESS_STATUS='S' GROUP BY CUSTOMER_PROCESS_STATUS ;
  pv_procesados := procesados;
  update read.monitor_bch set log1=procesados;
  commit;
  exception
    when no_data_found then
    pv_procesados :=0;  
end;

begin
  SELECT  nvl(count(*),0) por_proce into por_procesar FROM BCH_PROCESS_CUST where CUSTOMER_PROCESS_STATUS='I' GROUP BY CUSTOMER_PROCESS_STATUS ;
  pv_por_procesar := por_procesar;
  update read.monitor_bch set log2=por_procesar;
  commit;
  exception
    when no_data_found then
    pv_por_procesar :=0 ; 
  
end;

if sesionesb = 8 then
  update read.monitor_bch set sesiones=sesionesb;
  commit;
end if;

pv_error := 0;
select sesiones into temp from monitor_bch;

if sesionesb <= 8 then
  pv_error := temp-sesionesb;
  update read.monitor_bch set sesiones=sesionesb;
  commit;
end if;

select cantidad_proc into cantidad from read.monitor_bch;
pv_cantidad := pv_procesados-cantidad;

update read.monitor_bch set cantidad_proc=pv_procesados;
commit;

if  pv_sesion=0 and pv_por_procesar=0 then
  pv_tiempo := 1;
else
  pv_tiempo := 0; 
end if;
--select tiempo_proc into tiempo from read.monitor_bch;

--tiempo2 := sysdate - to_date(tiempo);
--pv_tiempo := to_number(tiempo2);

--update read.monitor_bch set tiempo_proc=to_date(sysdate,'dd/mm/yyyy');


end ;
/

