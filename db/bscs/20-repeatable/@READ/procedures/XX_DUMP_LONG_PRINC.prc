CREATE OR REPLACE procedure      xx_dump_long_princ is

   mytab DBMS_SQL.VARCHAR2S;
BEGIN
   xx_dump_long ('document_all', 'document', NULL, mytab);
   --pck_XCD.Dump_Campo_Long ('document_all', 'document', NULL, mytab);
   FOR longind IN 1 .. mytab.COUNT
   LOOP
      DBMS_OUTPUT.PUT_LINE  (SUBSTR (mytab(longind), 1, 255));
   END LOOP;
END;
/

