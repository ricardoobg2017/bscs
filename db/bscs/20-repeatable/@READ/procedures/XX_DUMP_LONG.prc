CREATE OR REPLACE PROCEDURE      xx_dump_long (
   tab IN VARCHAR2,
   col IN VARCHAR2,
   whr IN VARCHAR2 := NULL,
   pieces IN OUT DBMS_SQL.VARCHAR2S)
/* Requires Oracle 7.3 or above */
IS
   cur PLS_INTEGER := DBMS_SQL.OPEN_CURSOR;
   fdbk PLS_INTEGER;

   TYPE long_rectype IS RECORD (
      piece_len PLS_INTEGER,
      pos_in_long PLS_INTEGER,
      one_piece VARCHAR2(300),
      one_piece_len PLS_INTEGER
      );
   rec long_rectype;

  BEGIN
--       if whr is NULL */
   DBMS_SQL.PARSE (
      cur,
      'SELECT ' || col || 
      '  FROM ' || tab ||
      ' WHERE ' || NVL (whr, '1 = 1'),
      DBMS_SQL.NATIVE);

   /* Define the long column and then execute and fetch... */
   DBMS_SQL.DEFINE_COLUMN_LONG (cur, 1);
   fdbk := DBMS_SQL.EXECUTE (cur);
   fdbk := DBMS_SQL.FETCH_ROWS (cur);

   /* If a row was fetched, loop through the long value until 
   || all pieces are retrieved.
   */
   IF fdbk > 0
   THEN
      rec.piece_len := 255;
      rec.pos_in_long := 0;
      LOOP
         DBMS_SQL.COLUMN_VALUE_LONG (
            cur,
            1,
            rec.piece_len,
            rec.pos_in_long,
            rec.one_piece,
            rec.one_piece_len);
         EXIT WHEN rec.one_piece_len = 0;

         /* Always put the new piece in the next available row */
         pieces (NVL (pieces.LAST, 0) + 1) := rec.one_piece;
         rec.pos_in_long := rec.pos_in_long + rec.one_piece_len;
      END LOOP;
   END IF;
   DBMS_SQL.CLOSE_CURSOR (cur);
END;
/

