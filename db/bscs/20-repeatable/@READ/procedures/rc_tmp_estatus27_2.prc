CREATE OR REPLACE procedure RC_TMP_ESTATUS27_2 is
begin
  delete from rc_tmp_datos_generales_status;
  commit;

  insert into rc_tmp_datos_generales_status
  SELECT /*+ index(cu,pkcustomer_all),
                     index(co,pkcontract_all ),
                     index(cs,pk_contr_services_cap),
                     index(di,pkdirectory_number),
                     index(cd,pkcontr_devices),
                     index(sm,stormed_sepl),
                 +*/
           DISTINCT cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, cu.cstradecode tradecode,
                    co.co_id, co.co_ext_csuin, co.product_history_date, co.tmcode, co.plcode,
                    cur.ch_status,NULL,NULL,NULL, cs.main_dirnum, cs.cs_deactiv_date, di.dn_num, di.dn_status,NULL, 'A', sm.sm_serialnum
             FROM customer_all        cu,
                  contract_all        co,
                  curr_co_status      cur,
                  contr_services_cap  cs,
                  directory_number    di,
                  contr_devices       cd,
                  storage_medium      sm
            WHERE co.customer_id = cu.customer_id 
        AND cur.co_id = co.co_id      
        AND co.tmcode <> 268
              AND cu.cstype not in ('d', 'o') 
              AND co.co_ext_csuin = 3         
              AND cur.ch_status not in ('d', 'o') 
              AND to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') -  trunc(co.product_history_date) >= 1
              /*AND cu.cstradecode IN ( SELECT b.rc_tipo_credito 
                                      FROM rc_tr_tc@axis b
                                      WHERE b.rc_tipo_reloj = 3
                                      ) 
              AND cu.prgcode     IN ( SELECT c.st_id_sub 
                                      FROM rc_sub_tipos_reloj@axis c
                                      WHERE c.st_tipo_reloj = 3
                                      ) */
              AND cu.custcode in ('1.10503784',
'1.10776784',
'1.10796355',
'1.10797476',
'1.10843864',
'1.10855651',
'1.10908160',
'1.10945000',
'1.10960584')
              AND cs.co_id = co.co_id 
              AND cs.seqno       = ( SELECT MAX(ccl.seqno)
                                       FROM contr_services_cap ccl
                                      WHERE ccl.co_id = cs.co_id) 
              AND di.dn_id = cs.dn_id AND (cs.cs_deactiv_date IS NULL) 
              AND di.dn_status = 'a' 
              AND cd.co_id = co.co_id AND (cd.cd_deactiv_date IS NULL) 
              AND cd_seqno      = ( SELECT MAX(cdd.cd_seqno)
                                      FROM contr_devices cdd
                                     WHERE cdd.co_id = co.co_id) 
              AND sm.sm_serialnum = cd.cd_sm_num  
              --AND sm.sm_status = 'a' JHE 20-06-2007
            GROUP BY cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.cstradecode, cu.prgcode,
                     co.co_id, cur.ch_status, co.co_ext_csuin, co.product_history_date,
                     co.tmcode, co.plcode, cs.main_dirnum,cs.cs_deactiv_date,di.dn_num,di.dn_status,
                     sm.sm_serialnum;
  commit;
                     
  delete from rc_tmp_datos_generales_status@axis;
  commit;
  
  insert into rc_tmp_datos_generales_status@axis
  select *
  from rc_tmp_datos_generales_status;
  commit;
  
  update porta.rc_control_procesos@axis
  set process_status='E'
  where rc_process=3;
  commit;
end RC_TMP_ESTATUS27_2;

/
