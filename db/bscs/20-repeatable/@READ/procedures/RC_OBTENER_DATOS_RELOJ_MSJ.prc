CREATE OR REPLACE PROCEDURE RC_OBTENER_DATOS_RELOJ_MSJ ( Pn_accion IN NUMBER, -- 1: datos mensajeria,2: datos reactivacion ,3: datos paso de estatus.
                                                     Pv_Marca  IN VARCHAR2 DEFAULT NULL,
                                                     Pv_Fecha  IN VARCHAR2 DEFAULT NULL,
                                                     Pv_Error  OUT VARCHAR2,
                                                     pn_cant   OUT NUMBER) IS

/* 
   Modificado por    : Jessica Herrera Aguilar
   Fecha modificaci�n: 09-12-2004
   Motivo            : Para obtener solo las cuentas con sus respectivos contratos.
     
*/
                                                       
  -- DECLARACION DE CURSORES
  CURSOR cur_tipo_reloj is
    SELECT *
    FROM rc_reloj_etapa@axis
    WHERE rc_predecesor IN (1, 2, 3, 4, 7)
    ORDER BY rc_tipo_reloj, rc_etapa;
    
  CURSOR Cur_SaldoMinimo IS
    SELECT /*+ rule +*/ par_valor 
    FROM rc_param_global@axis
    WHERE par_nombre = 'SALDO_MINIMO';
    
  CURSOR Cur_SaldoMinPor IS  
    SELECT /*+ rule +*/ par_valor 
    FROM rc_param_global@axis
    WHERE par_nombre = 'SALDO_MIN_POR';  
  
  CURSOR Cur_obtenerCuenta IS
    SELECT /*+ rule +*/ 
           cu.customer_id_high,
           cu.cuenta,
           cu.billcycle, 
           cu.cstype, 
           cu.prgcode, 
           cu.prev_balance, 
           cu.saldo, 
           cu.co_id,
           cu.co_ext_csuin, 
           cu.product_history_date,  
           cu.tmcode, 
           cu.plcode, 
           cu.ch_status, 
           cu.main_dirnum, 
           cu.cs_deactiv_date, 
           cu.dn_num, 
           cu.dn_status, 
           cu.estado, 
           cu.serial,
           cu.tradecode,
           cu.deuda,
           cu.customer_dealer,
           cu.cycles,
           cu.creditos           
      FROM READ.RC_CUSTOMER_RELOJ_MSJ  cu
      GROUP BY 
           cu.customer_id_high,
           cu.cuenta,
           cu.billcycle, 
           cu.CSTYPE,
           cu.PREV_BALANCE,
           cu.saldo,
           cu.prgcode,
           cu.co_id,
           cu.ch_status,
           cu.co_ext_csuin,
           cu.product_history_date,
           cu.tmcode,
           cu.plcode, 
           cu.main_dirnum, 
           cu.cs_deactiv_date,
           cu.dn_num,
           cu.dn_status, 
           cu.estado, 
           cu.serial,
           cu.tradecode,
           cu.deuda,
           cu.customer_dealer,
           cu.cycles,
           cu.creditos;
  
--JRO:1929 --21/08/2006--, Se Modifica este Query y se agrega en la opci�n 1, autorizado por CTU/EGA.
--  CURSOR Cur_obtenerCuenta IS     
--    SELECT /*+ rule +*/ cu.custcode cuenta,cu.billcycle, cu.CSTYPE cstype, cu.prgcode prgcode, cu.PREV_BALANCE prev_balance, nvl(sum(oh.ohopnamt_doc),0) saldo, co.co_id co_id,
--            co.co_ext_csuin co_ext_csuin, co.product_history_date product_history_date,  co.tmcode tmcode, co.plcode plcode, cur.ch_status ch_status, 
--            cs.main_dirnum main_dirnum, cs.cs_deactiv_date cs_deactiv_date, di.dn_num dn_num, 
--            di.dn_status dn_status, 'A' estado, NULL serial,cu.cstradecode tradecode
--    FROM customer_all               cu,
--         rc_comentarios_cuenta@axis rc,
--         orderhdr_all               oh,
--         contract_all               co,
--         curr_co_status             cur,
--         contr_services_cap         cs,
--         directory_number           di
--    WHERE rc.rc_envio_mensaje = Pv_marca 
--    AND rc.rc_fecha_mensaje = to_date(Pv_Fecha, 'DD/MM/YYYY')   
--    AND rc.rc_reg_procesado is null 
--    and rc.rc_cuenta = cu.custcode     
--    AND cu.customer_id = oh.customer_id
--    AND cu.customer_id = co.customer_id 
--    AND co.co_id = cur.co_id
--    AND cu.cstype not in ('d', 'o')
--    AND cur.ch_status not in ('d', 'o')     
--    AND co.co_id = cs.co_id 
--    AND cs.seqno = ( SELECT MAX ( ccl.seqno )FROM contr_services_cap ccl
--                     where ccl.co_id = cs.co_id) 
--    AND cs.dn_id = di.dn_id 
--    GROUP BY cu.custcode,cu.billcycle, cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id,cur.ch_status,co.co_ext_csuin,
--             co.product_history_date,co.tmcode,co.plcode, cs.main_dirnum, cs.cs_deactiv_date,di.dn_num,di.dn_status, cu.cstradecode
--           
-- UNION
--           
--    SELECT /*+ rule +*/ cu.custcode cuenta,cu.billcycle, cu.CSTYPE cstype, cu.prgcode prgcode, cu.PREV_BALANCE prev_balance, nvl(sum(oh.ohopnamt_doc),0) saldo,
--                        co.co_id co_id, co.co_ext_csuin co_ext_csuin, co.product_history_date product_history_date, co.tmcode tmcode, co.plcode plcode, 
--                        cur.ch_status ch_status, cs.main_dirnum main_dirnum, cs.cs_deactiv_date cs_deactiv_date, di.dn_num dn_num, di.dn_status dn_status,
--                        'A' estado, NULL serial,cu.cstradecode tradecode                       
--    FROM customer_all               cu,
--         rc_comentarios_cuenta@axis rc,
--         orderhdr_all               oh,
--         contract_all               co,
--         curr_co_status             cur,
--         contr_services_cap         cs,
--         directory_number           di
--    WHERE rc.rc_envio_mensaje = Pv_marca 
--    AND rc.rc_fecha_mensaje = to_date(Pv_Fecha, 'DD/MM/YYYY') 
--    --and rc.rc_cuenta IN ('1.10172323')--jhe pruebas
--    AND rc.rc_reg_procesado is null 
--    AND cu.custcode = rc.rc_cuenta || '.00.00.100000' 
--    AND cu.customer_ID_HIGH = oh.customer_id 
--    AND cu.customer_id = co.customer_id
--    AND co.co_id = cur.co_id
--    AND cu.cstype not in ('d', 'o')
--    AND cur.ch_status not in ('d', 'o')
--    AND co.co_id = cs.co_id 
--    AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl  where ccl.co_id = cs.co_id) 
--    AND cs.dn_id = di.dn_id 
--    GROUP BY cu.custcode,cu.billcycle,  cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id,cur.ch_status,co.co_ext_csuin,
--             co.product_history_date,co.tmcode,co.plcode, cs.main_dirnum, cs.cs_deactiv_date,di.dn_num, 
--             di.dn_status,cu.cstradecode;
             
  CURSOR cur_Creditos_msj (pv_customer_id number, cv_inicio_periodo VARCHAR2) IS
    SELECT /*+ rule +*/ nvl(sum(amount), 0) creditos
    FROM  fees b
    WHERE b.customer_id =pv_customer_id
    AND b.period > 0 
    AND b.sncode = 46 
    AND trunc(b.entdate) >= to_date(cv_inicio_periodo, 'dd/mm/yyyy');                 


  -- Cr�ditos
  CURSOR cur_Creditos (cv_cuenta VARCHAR2, cv_inicio_periodo VARCHAR2) IS
    SELECT /*+ rule +*/ nvl(sum(amount), 0) creditos
    FROM customer_all a, fees b
    WHERE a.custcode = cv_cuenta 
    AND a.customer_id = b.customer_id 
    AND b.period > 0 
    AND b.sncode = 46 
    AND trunc(b.entdate) >= to_date(cv_inicio_periodo, 'dd/mm/yyyy');                 

  --JRO:1929 --21/08/2006--, Se Modifica este Query y se agrega en la opci�n 1, autorizado por CTU/EGA.    
  -- Deuda para mensajer�a
  -- No se debe considerar la fecha de pago.
--  CURSOR Cur_DeudaSaldo ( cv_cuenta VARCHAR2 ) IS
--    SELECT /*+ rule +*/ 
--          nvl(sum(decode(o.ohstatus,'CO',0,ohinvamt_doc)), 0) deuda,
--          nvl(SUM(o.ohopnamt_doc), 0) saldo
--    FROM customer_all c,
--         orderhdr_all o
--    WHERE c.custcode = cv_cuenta
--    AND c.customer_dealer = 'C' 
--    AND c.customer_id = o.customer_id 
--    AND o.ohstatus IN ( 'IN' , 'CM' , 'CO')
--    --AND o.ohopnamt_doc > 0 
--    --AND trunc(o.ohduedate) < to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
--    GROUP BY c.custcode;


  -- Deuda para Reactivaci�n
  -- Si se debe considerar la fecha de pago.
--JRO:16/05/2006, Se agrega en el query general autorizado por EGA.
--  CURSOR Cur_DeudaSaldoReac ( cv_cuenta VARCHAR2 ) IS
--    SELECT /*+ rule +*/ 
--          nvl(sum(decode(o.ohstatus,'CO',0,ohinvamt_doc)), 0) deuda,
--          nvl(SUM(o.ohopnamt_doc), 0) saldo
--    FROM customer_all c,
--         orderhdr_all o
--    WHERE c.custcode = cv_cuenta
--    AND c.customer_dealer = 'C' 
--    AND c.customer_id = o.customer_id 
    --AND o.ohopnamt_doc > 0 
--    AND trunc(o.ohduedate) < to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy')
--    GROUP BY c.custcode;


--JRO:16/05/2006, Se reemplaza el query de este cursor.    
--  CURSOR cur_cargar_datos IS
--    SELECT /*+ rule +*/ cu.customer_id,cu.billcycle,cu.custcode, cu.CSTYPE, cu.prgcode,cu.PREV_BALANCE, sum(oh.ohopnamt_doc) saldo,
--                        co.co_id, co.co_ext_csuin,co.product_history_date,co.tmcode,co.plcode, cur.ch_status,cs.main_dirnum, 
--                        cs.cs_deactiv_date,di.dn_num, di.dn_status, NULL, NULL,cu.cstradecode tradecode
--      from customer_all        cu,
--
--           orderhdr_all        oh,
--           payment_all         pa,
--           contract_all        co,
--           curr_co_status      cur,
--           contr_services_cap  cs,
--           directory_number    di
--     where 
--     --trunc(oh.ohduedate) < to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') 
--     oh.ohduedate < trunc(Sysdate)
--     AND oh.customer_id = cu.customer_id 
--     AND cu.cstype not in ('d', 'o') 
--     AND pa.customer_id = cu.customer_id 
--     and pa.act_used = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZ�
--     and pa.bank_id not in (33, 34, 35, 36, 37, 38, 39, 41, 42,43, 44, 45, 46, 47, 48,49,50, 51, 52, 53, 54, 55, 
--                            56, 57, 58, 59, 60,61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 74, 75, 76)          
--     and cu.customer_id = co.customer_id 
--     AND co.co_ext_csuin in (1, 2, 3, 4, 7) 
--     and co.co_id = cur.co_id 
--     AND cur.ch_status not in ('d', 'o') 
--     and co.co_id = cs.co_id 
--     AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl where ccl.co_id = cs.co_id) 
--     and cs.dn_id = di.dn_id 
--     AND di.dn_status = 'a' 
--     GROUP BY cu.customer_id, cu.billcycle, cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.prgcode, co.co_id, cur.ch_status,
--              co.co_ext_csuin, co.product_history_date, co.tmcode, co.plcode, cs.main_dirnum, cs.cs_deactiv_date, 
--              di.dn_num, di.dn_status,cu.cstradecode
--     UNION All
    
--     SELECT /*+ rule +*/cu.customer_id,cu.billcycle, cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, sum(oh.ohopnamt_doc) saldo,
--                       co.co_id, co.co_ext_csuin,co.product_history_date, co.tmcode, co.plcode, cur.ch_status, cs.main_dirnum, 
--                       cs.cs_deactiv_date, di.dn_num, di.dn_status, NULL, NULL, cu.cstradecode tradecode
--      from customer_all        cu,
--           orderhdr_all        oh,
--           contract_all        co,
--           curr_co_status      cur,
--           contr_services_cap  cs,
--           directory_number    di,
--           payment_all         pa
    
--     where 
     --trunc(oh.ohduedate) < to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') 
--     oh.ohduedate < trunc(Sysdate)
--     AND oh.customer_id = cu.customer_id_high 
--     AND cu.cstype not in ('d', 'o') 
--     and pa.customer_id = oh.customer_id 
--     AND pa.act_used = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZ�            
--     and pa.bank_id not in (33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
--                            60,61,62,63,64,65,66,67,68,69,70,71,72,74,75,76)
--          
--     and cu.customer_id = co.customer_id 
--     AND co.co_ext_csuin in (1, 2, 3, 4, 7) 
--     and co.co_id = cur.co_id 
--     AND cur.ch_status not in ('d', 'o') 
--     and co.co_id = cs.co_id 
--     AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl where ccl.co_id = cs.co_id) 
--     and cs.dn_id = di.dn_id 
--     AND di.dn_status = 'a' 
--     GROUP BY cu.customer_id,cu.billcycle, cu.custcode,cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id, cur.ch_status,co.co_ext_csuin,
--              co.product_history_date, co.tmcode, co.plcode, cs.main_dirnum,
--              cs.cs_deactiv_date, di.dn_num,di.dn_status,cu.cstradecode;
              
  CURSOR cur_cargar_datos IS
      SELECT /*+ rule +*/ cu.customer_id,cu.customer_id_high,cu.billcycle,cu.custcode, cu.CSTYPE, cu.prgcode,cu.PREV_BALANCE, sum(oh.ohopnamt_doc) saldo,
                        cu.co_id, cu.co_ext_csuin,cu.product_history_date,cu.tmcode,cu.plcode, cu.ch_status,cu.main_dirnum, 
                        cu.cs_deactiv_date,cu.dn_num, cu.dn_status, NULL, NULL,cu.cstradecode tradecode,cycles,deuda
      from 
           RC_CUSTOMER_RELOJ    cu,
           orderhdr_all        oh,
           payment_all         pa
     where   
         oh.ohduedate < trunc(Sysdate)
     and oh.customer_id = cu.customer_id_high
     and pa.customer_id = oh.customer_id
     and pa.act_used = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZ�
     and pa.bank_id not in (33, 34, 35, 36, 37, 38, 39, 41, 42,43, 44, 45, 46, 47, 48,49,50, 51, 52, 53, 54, 55, 
                            56, 57, 58, 59, 60,61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 74, 75, 76)          
     GROUP BY cu.customer_id,cu.customer_id_high, cu.billcycle, cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.prgcode, cu.co_id, cu.ch_status,
              cu.co_ext_csuin, cu.product_history_date, cu.tmcode, cu.plcode, cu.main_dirnum, cu.cs_deactiv_date, 
              cu.dn_num, cu.dn_status,cu.cstradecode,cycles,deuda ;


  CURSOR Cur_countTelefonos IS
    SELECT COUNT(*) 
    FROM rc_tmp_datos_generales_status t
    WHERE procesada='A';

  ------------------------------------------
  -- DECLARACION DE VARIABLES
  ------------------------------------------
  
  v_cursor            NUMBER;
  v_sentencia_string  VARCHAR2(1000);
  LV_EXECUTE          VARCHAR2(1500);
  ln_saldo_impago     NUMBER:=0;      
  ln_cantidad         NUMBER:=0;  
  ln_saldo_real       NUMBER:=0; 
  ln_creditos         NUMBER:=0; 
  Ln_Saldo_Min        NUMBER;
  Ln_Saldo_Min_Por    NUMBER;    
  ln_validacion       NUMBER;
  ln_deuda_periodo    NUMBER;
  ln_por_impago       NUMBER;
  ln_deuda            NUMBER;
  ln_saldo            NUMBER;
  ln_count            NUMBER;  
  lv_error            VARCHAR2(3000);
  lv_cuenta           VARCHAR2(24):='0';
  lv_telefono         VARCHAR2(63):='0';
  lv_mes              VARCHAR2(3); --variable para generar el nombre de la tabla de donde se extrae los datos no procesados
  lv_inicio_periodo   VARCHAR2(10); -- variable para sacar la fecha de inicio del periodo actual
  lv_periodo_iniciado VARCHAR2(10); -- variable para sacar la fecha en que ya ha iniciado el periodo actual
  lv_fin_periodo      VARCHAR2(10); -- variable para sacar la fecha en que finaliza el periodo actual
  lv_numero_dias      VARCHAR2(2); -- variable para almacenar el numero de dias del mes anterior al periodo actual
  Lv_Saldo_Min        VARCHAR2(255);
  Lv_Saldo_Min_Por    VARCHAR2(255);
  lb_found            BOOLEAN;
  --
  --lc_deudaSaldo       Cur_DeudaSaldo%ROWTYPE;
  Lc_Creditos         cur_Creditos%ROWTYPE;
  Lc_Creditos_Msj     cur_Creditos%ROWTYPE;
  --
  ln_duracion         NUMBER;
  ln_predecesor       NUMBER;
  ln_tipo_reloj       NUMBER;
  lv_cicloAdm         Varchar2(2);
  lv_ciclo            Varchar2(2);
  lv_fecha            date;
  lv_dia              varchar2(2);
  -----
  procedure inserta_auditoria_reloj(lv_descripcion in varchar2)
  is
  begin
     insert into RC_AUDITORIA_PROCESO_PR values(sysdate,lv_descripcion);
  end; 

    
BEGIN

  -- Obtener datos para ejecuci�n de la mensajer�a
  --
  IF ( pn_accion = 1 ) THEN
    --------------------------------------------------------   
    --JRO: 1929, Cambios en el Reloj, autorizado por CTU/EGA
    --------------------------------------------------------     
       -- 
       inserta_auditoria_reloj('OP1:INICIO DE PROCESO OPCION 1');
       inserta_auditoria_reloj('OP1:TRUNCATE A LA TABLA TEMPORAL RC_CUSTOMER_RELOJ_MSJ');
       --
       LV_EXECUTE := 'TRUNCATE TABLE READ.RC_CUSTOMER_RELOJ_MSJ';
       EXECUTE IMMEDIATE LV_EXECUTE;        
       --
       inserta_auditoria_reloj('OP1:FINALIZA TRUNCATE A LA TABLA TEMPORAL RC_CUSTOMER_RELOJ_MSJ');
       inserta_auditoria_reloj('OP1:INSERTA REGISTROS ACTIVOS A TABLA TEMPORAL RC_CUSTOMER_RELOJ_MSJ');
       --    
 
       INSERT /*+ APPEND */ INTO READ.RC_CUSTOMER_RELOJ_MSJ 
       SELECT /*+ FIRST_ROWS */ 
               cu.Customer_Id,
               cu.customer_id_high,
               cu.custcode cuenta,
               cu.billcycle, 
               cu.CSTYPE cstype, 
               cu.prgcode prgcode,            
               cu.PREV_BALANCE prev_balance,                       
               0,
               co.co_id co_id,           
               co.co_ext_csuin co_ext_csuin,            
               co.product_history_date product_history_date,           
               co.tmcode tmcode,            
               co.plcode plcode,            
               cur.ch_status ch_status,           
               cs.main_dirnum main_dirnum,            
               cs.cs_deactiv_date cs_deactiv_date,            
               di.dn_num dn_num,            
               di.dn_status dn_status,            
               'A' estado, 
               NULL serial,
               cu.cstradecode tradecode,
               0,
               cu.customer_dealer,
               NULL,
               NULL,
               0,
               'X'        
        FROM customer_all               cu,
             contract_all               co,
             curr_co_status             cur,
             contr_services_cap         cs,
             directory_number           di
       WHERE cu.customer_id IN (
              select customer_id from customer_all
              START WITH customer_id in 
                        (SELECT /*+ rule +*/ 
                                cu.customer_id
                           FROM customer_all    cu
                          WHERE exists         (SELECT 'X'
                                                  FROM rc_comentarios_cuenta@axis rc
                                                 WHERE rc.rc_cuenta=cu.custcode
                                                   AND rc.rc_envio_mensaje = Pv_marca 
                                                   AND rc.rc_fecha_mensaje = to_date(Pv_Fecha, 'DD/MM/YYYY') 
                                                   AND rc.rc_reg_procesado   is null                                 
                                                )    
                            AND cu.cstype not in ('d', 'o')
                        )
                        CONNECT BY PRIOR customer_id = customer_id_high
                               )                                   
        AND cu.customer_id = co.customer_id 
        AND co.co_id = cur.co_id
        AND cur.ch_status not in ('d', 'o')     
        AND co.co_id = cs.co_id 
        AND cs.seqno = ( SELECT MAX ( ccl.seqno )FROM contr_services_cap ccl
                         where ccl.co_id = cs.co_id) 
        AND cs.dn_id = di.dn_id;

        inserta_auditoria_reloj('OP1:FINALIZA LA INSERCION DE REGISTROS A TABLA TEMPORAL RC_CUSTOMER_RELOJ_MSJ');
        inserta_auditoria_reloj('OP1:UPDATE DEL CAMPO CUSTOMER_ID_HIGH DE LOS CAMPOS DEUDA,SALDO,CYCLES');
        
        commit;
        
        UPDATE READ.RC_CUSTOMER_RELOJ_MSJ 
           SET CUSTOMER_ID_HIGH=CUSTOMER_ID
         WHERE CUSTOMER_ID_HIGH IS NULL;
         
        commit; 

         UPDATE READ.RC_CUSTOMER_RELOJ_MSJ c
            SET C.GRUPO='P'
          WHERE C.ROWID IN (SELECT  MAX(ROWID) FROM READ.rc_customer_reloj_msj GROUP BY  CUSTOMER_ID_HIGH)
            AND c.customer_dealer = 'C';
        
        commit;       
          
        
        UPDATE READ.RC_CUSTOMER_RELOJ_MSJ C
               SET C.CYCLES=( 
                       SELECT /*+ RULE +*/ ID_CICLO
                         FROM FA_CICLOS_AXIS_BSCS M
                        WHERE M.ID_CICLO_ADMIN = C.BILLCYCLE
                       );
        commit;
        
        --Actualiza la fecha de ciclo de facturaci�n
        select sysdate into lv_fecha from dual;
        lv_dia := to_char(lv_fecha,'dd');
        
        --Ciclo 01
        if lv_dia >= '24' then
           lv_inicio_periodo   := '24/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
        else
           if to_char(lv_fecha,'mm')='01' then
              lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
           else
              lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
           end if;
        end If;
         
        UPDATE /*+ INDEX(C,IDX_CYCLES_MSJ) */ READ.RC_CUSTOMER_RELOJ_MSJ 
           SET FECHA_CYCLE=lv_inicio_periodo
         WHERE CYCLES='01';
        
        commit;
        
        --ciclo 02   
        if lv_dia >= '08' then
           lv_inicio_periodo   := '08/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
        else
           if to_char(lv_fecha,'mm')='01' then
              lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
           else
              lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
           end if;
        end If;  

        UPDATE /*+ INDEX(C,IDX_CYCLES_MSJ) */ READ.RC_CUSTOMER_RELOJ_MSJ 
           SET FECHA_CYCLE=lv_inicio_periodo
         WHERE CYCLES='02'; 
        
        commit;
        
         UPDATE /*+ INDEX(C,IDX_GRUPO_MSJ) */ READ.rc_customer_reloj_msj c
            SET c.deuda =
                   (SELECT /*+ INDEX(O,oh_cust_id_1)*/ NVL (SUM (DECODE (o.ohstatus, 'CO', 0, ohinvamt_doc)), 0)
                      FROM orderhdr_all o
                     WHERE o.customer_id = c.customer_id_high
                       AND o.ohstatus IN ('IN', 'CM', 'CO')),
                c.saldo =
                   (SELECT /*+ INDEX(O,oh_cust_id_1)*/ NVL (SUM (o.ohopnamt_doc), 0)
                      FROM orderhdr_all o
                     WHERE o.customer_id = c.customer_id_high
                       AND o.ohstatus IN ('IN', 'CM', 'CO')),
                c.creditos=(
                            SELECT /*+ rule +*/ nvl(sum(amount), 0) creditos
                              FROM FEES b
                             WHERE b.customer_id    = c.customer_id_high
                               AND b.period > 0 
                               AND b.sncode = 46 
                               AND trunc(b.entdate) >= c.FECHA_CYCLE
                            )                       
           WHERE C.GRUPO='P';
           
           commit;          

        
           UPDATE /*+ INDEX(cu,IDX_GRUPO_MSJ) */ READ.rc_customer_reloj_msj cu
              SET cu.deuda =
                           (SELECT /*+ rule */ deuda
                              FROM READ.rc_customer_reloj_msj c
                             WHERE c.customer_id_high = cu.customer_id_high 
                               AND c.grupo = 'P'),
                  cu.saldo =
                           (SELECT /*+ rule */ saldo
                              FROM READ.rc_customer_reloj_msj c
                             WHERE c.customer_id_high = cu.customer_id_high 
                               AND c.grupo = 'P'),
                  cu.creditos =
                           (SELECT /*+ rule */ creditos
                              FROM READ.rc_customer_reloj_msj c
                             WHERE c.customer_id_high = cu.customer_id_high 
                               AND c.grupo = 'P')
             WHERE cu.grupo='X';
                
           commit;
    
        --
        -- borrar datos de la tabla rc_tmp_datos_generales
        -- Abrir cursor
        v_cursor := DBMS_SQL.open_cursor;
      
        -- Eliminar la tabla no actualizada que contiene informaci�n desde comisiones
        v_sentencia_string := 'TRUNCATE TABLE READ.RC_TMP_DATOS_GENERALES_MSJ';
        Begin
          DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
        exception
          when others then
            if SQLCODE != -942 then
              raise;
            end if;
        end;
        --  cerrar el cursor
        DBMS_SQL.CLOSE_CURSOR(v_cursor);
        --
    
        INSERT /*+ APPEND */ INTO READ.RC_TMP_DATOS_GENERALES 
               (custcode,cstype,prgcode,prev_balance,saldo,co_id,co_ext_csuin,product_history_date,
                tmcode,plcode,ch_status,sncode,prm_value_id,status,main_dirnum,cs_deactiv_date,
                dn_num,dn_status,spcode,procesada,sm_serialnum,deuda,creditos,cstradecode
               )
        SELECT /*+ rule +*/ 
               cu.cuenta,cu.cstype,cu.prgcode,cu.prev_balance,cu.saldo,cu.co_id,
               cu.co_ext_csuin,cu.product_history_date,cu.tmcode,cu.plcode,cu.ch_status, 
               null,null,null,cu.main_dirnum,cu.cs_deactiv_date,cu.dn_num,cu.dn_status, 
               null,cu.estado,cu.serial,cu.deuda,cu.creditos,cu.tradecode
          FROM READ.RC_CUSTOMER_RELOJ_MSJ  cu
          GROUP BY 
               cu.cuenta,cu.cstype,cu.prgcode,cu.prev_balance,cu.saldo,cu.co_id,
               cu.co_ext_csuin,cu.product_history_date,cu.tmcode,cu.plcode,cu.ch_status, 
               cu.main_dirnum,cu.cs_deactiv_date,cu.dn_num,cu.dn_status,cu.estado, 
               cu.serial,cu.deuda,cu.creditos,cu.tradecode;

        pn_cant  := SQL%ROWCOUNT;
        commit;
     
    --FIN    

    
--j    pn_cant   := 0;
--j    ln_count  :=0;
    
--j    FOR i IN Cur_obtenerCuenta LOOP        
--j      BEGIN
        --
--j        IF i.dn_num <> lv_telefono THEN
          -- JHE 05-01-2005
          --IF substr(i.cuenta, 1, 1) = '1' THEN
          --   lv_cuenta := i.cuenta;
          --ELSE
          --  lv_cuenta := substr(i.cuenta,1,instr(i.cuenta, '.', 1, 2) - 1);
          --END IF;          
          --lv_cuenta   := i.cuenta;
--j          lv_telefono   :=i.dn_num;
          --          
          --------------------------------------------------------   
          --JRO: 1929, Cambios en el Reloj, autorizado por CTU/EGA
          --------------------------------------------------------     
          -- Obtener Deuda del periodo y saldo 
          /*OPEN Cur_deudaSaldo ( lv_cuenta );
          FETCH Cur_deudaSaldo INTO lc_deudaSaldo;
          lb_found := Cur_DeudaSaldo%FOUND;
          CLOSE Cur_deudaSaldo ;
          --
          IF lb_found THEN
             ln_deuda := lc_deudaSaldo.deuda;
             ln_saldo := lc_deudaSaldo.saldo;
          ELSE
             ln_deuda := 0;
             ln_saldo := 0;
          END IF;
          */
          -- obtener el ciclo de la cuenta
          --lv_cicloAdm := i.billcycle;
          --ln_deuda := i.deuda;
          --ln_saldo := i.saldo;
          --lv_ciclo := i.cycles;         

        /*  GENERA_FECHA_PERIODO_PR(lv_cicloAdm,
                                  lv_mes,
                                  lv_inicio_periodo,
                                  lv_fin_periodo,
                                  lv_periodo_iniciado,
                                  lv_numero_dias,
                                  lv_ciclo
                                 );       */           
          --FIN
         
          --------------------------------------------------------   
          --JRO: 1929, Cambios en el Reloj, autorizado por CTU/EGA
          --------------------------------------------------------     
          -- Obtener creditos realizados durante el mes y no aplicados 
          /*OPEN cur_Creditos_msj (i.customer_id_high , lv_inicio_periodo );
          FETCH cur_Creditos_msj INTO Lc_Creditos_Msj;
          lb_found := Cur_creditos_msj%FOUND;
          CLOSE cur_Creditos_msj;
          --
          IF lb_found THEN
             ln_creditos := Lc_Creditos_Msj.creditos;
          ELSE
             ln_creditos := 0;
          END IF;*/
          --
          --  
--j          INSERT /*+ APPEND */ INTO READ.RC_TMP_DATOS_GENERALES_MSJ 
--j          (custcode,   cstype,   prgcode,   prev_balance,    saldo,   co_id,   co_ext_csuin,   product_history_date,   tmcode,   plcode,   ch_status, sncode, prm_value_id, status,   main_dirnum,   cs_deactiv_date,      dn_num,   dn_status, spcode, procesada, sm_serialnum,    deuda,    creditos,cstradecode)
--j          VALUES 
--j          (i.cuenta, i.cstype, i.prgcode, i.prev_balance, i.saldo, i.co_id, i.co_ext_csuin, i.product_history_date, i.tmcode, i.plcode, i.ch_status,   NULL,         NULL,   NULL, i.main_dirnum, i.cs_deactiv_date, lv_telefono, i.dn_status,   NULL,  i.estado,    i.serial , i.deuda, i.creditos,i.tradecode);        
--j          ln_count    := ln_count + 1;
          
          --
--j        END IF;
      --  
--j      EXCEPTION
--j        WHEN OTHERS THEN
--j          lv_error := 'Error: ' || SQLERRM;
--j      END;
      --
--j      IF ln_count = 500 THEN
--j         COMMIT;    
--j         pn_cant  := pn_cant + ln_count;
--j         ln_count := 0;
--j      END IF;
      --        
--j    END LOOP;
    --
--j    pn_cant  := pn_cant + ln_count;
    --
    inserta_auditoria_reloj('OP1:FINALIZA, PROCESO DE INSERCION A LA TABLA rc_tmp_datos_generales');

    COMMIT;
  
    -- Obtener datos para la reactivaci�n
    -- ----------------------------------      
  ELSIF (pn_accion = 2) THEN
        -- 
        inserta_auditoria_reloj('OP2:INICIO DE PROCESO OPCION 2');
        inserta_auditoria_reloj('OP2:TRUNCATE A LA TABLA TEMPORAL RC_CUSTOMER_RELOJ');
        --
        LV_EXECUTE := 'truncate table RC_CUSTOMER_RELOJ';
        EXECUTE IMMEDIATE LV_EXECUTE;        
        --
        inserta_auditoria_reloj('OP2:FINALIZA TRUNCATE A LA TABLA TEMPORAL RC_CUSTOMER_RELOJ');
        inserta_auditoria_reloj('OP2:INSERTA REGISTROS ACTIVOS A TABLA TEMPORAL RC_CUSTOMER_RELOJ');
        --        
       insert /*+ APPEND */ into  RC_CUSTOMER_RELOJ 
       select /*+ index(cu, pkcustomer_all),
              index(co,fkicocsid),
              index(cs,pk_contr_services_cap),
              index(di,pkdirectory_number)
              +*/ 
              cu.customer_id,cu.customer_id_high,cu.billcycle,cu.custcode,cu.CSTYPE,cu.prgcode,cu.PREV_BALANCE,cu.cstradecode,
              co.co_id, co.co_ext_csuin,co.product_history_date,co.tmcode,co.plcode, cur.ch_status,
              cs.main_dirnum,cs.cs_deactiv_date,di.dn_num, di.dn_status,cu.customer_dealer,null,0
        from  customer_all cu, 
              contract_all co, 
              curr_co_status cur,
              contr_services_cap cs,
              directory_number di
        where cu.customer_id in (select /*+ RULE +*/ customer_id from customer_all
                                 START WITH customer_id in (select distinct(customer_id) from orderhdr_all)
                                 CONNECT BY PRIOR customer_id = customer_id_high
                                 )
          and cu.customer_id=co.customer_id
          and cu.cstype not in ('d', 'o') 
          and co.co_ext_csuin in (1, 2, 3, 4, 7)   
          and co.co_id=cur.co_id
          and cur.ch_status not in ('d', 'o')   
          and co.co_id = cs.co_id 
          and cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl where ccl.co_id = cs.co_id) 
          and cs.dn_id = di.dn_id 
          and di.dn_status = 'a';

        --
        inserta_auditoria_reloj('OP2:FINALIZA LA INSERCION DE REGISTROS A TABLA TEMPORAL RC_CUSTOMER_RELOJ');
        inserta_auditoria_reloj('OP2:UPDATE DEL CAMPO CUSTOMER_ID_HIGH DEL CAMPO CUSTOMER_ID');
        --     
        commit;
             
        update RC_CUSTOMER_RELOJ  set customer_id_high=customer_id
         where customer_id_high is null;
         
        commit; 
        --
        inserta_auditoria_reloj('OP2:FINALIZA UPDATE DEL CAMPO CUSTOMER_ID_HIGH DEL CAMPO CUSTOMER_ID');
        inserta_auditoria_reloj('OP2:UPDATE DEL CAMPO DEUDA=A LA SUMA DE TODAS LAS ORDERHDR, SEGUN CUSTOMER');
        --                 
        UPDATE RC_CUSTOMER_RELOJ t
           SET deuda = (
              SELECT /*+ rule +*/  nvl(sum(decode(o.ohstatus,'CO',0,ohinvamt_doc)), 0) deuda
                FROM  orderhdr_all o
               WHERE o.customer_id = t.customer_id_high
                 AND o.ohstatus IN ( 'IN' , 'CM' , 'CO')
                 AND trunc(o.ohduedate) < to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy')
                       )
         WHERE t.customer_dealer = 'C' ;    
         
         commit;
        --
        inserta_auditoria_reloj('OP2:FINALIZA, UPDATE DEL CAMPO DEUDA=A LA SUMA DE TODAS LAS ORDERHDR, SEGUN CUSTOMER');
        inserta_auditoria_reloj('OP2:UPDATE DEL CAMPO CYCLES, CONSULTANDO LA TABLA FA_CICLOS_AXIS_BSCS');
        --                 
        UPDATE RC_CUSTOMER_RELOJ t
       SET CYCLES = (
          SELECT /*+ RULE +*/ ID_CICLO
            FROM fa_ciclos_axis_bscs m
           WHERE m.ID_CICLO_ADMIN = t.BILLCYCLE
                     );
        --
        inserta_auditoria_reloj('OP2:FINALIZA, UPDATE DEL CAMPO CYCLES, CONSULTANDO LA TABLA FA_CICLOS_AXIS_BSCS');
        inserta_auditoria_reloj('OP2:INICIO, PROCESO DE INSERCION A LA TABLA rc_tmp_datos_generales');
        --        
        commit;

    --    
    -- Borrar datos de la tabla rc_tmp_datos_generales
    -- Abrir cursor
    v_cursor := DBMS_SQL.open_cursor;
  
    -- Eliminar la tabla no actualizada que contiene informaci�n desde comisiones
    v_sentencia_string := 'TRUNCATE TABLE RC_TMP_DATOS_GENERALES';
    Begin
      DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
    exception
      when others then
        if SQLCODE != -942 then
          raise;
        end if;
    end;
    --  cerrar el cursor
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    --
    OPEN  Cur_SaldoMinimo;
    FETCH Cur_SaldoMinimo INTO Lv_Saldo_Min;
    CLOSE Cur_SaldoMinimo;    
    --
    Ln_Saldo_Min := to_number(Lv_Saldo_Min);
    --
    OPEN  Cur_SaldoMinPor;
    FETCH Cur_SaldoMinPor INTO Lv_Saldo_Min_Por;
    CLOSE Cur_SaldoMinPor;
    --
    Ln_Saldo_Min_Por := to_number(Lv_Saldo_Min_Por);
    --
    ln_por_impago := ((100 - Ln_Saldo_Min_Por) / 100);
    pn_cant       := 0;
    --
    FOR i IN cur_cargar_datos LOOP
      BEGIN
        --
        IF i.dn_num <> lv_telefono THEN
          --
          -- JHE 10-01-2005
          IF substr(i.custcode, 1, 1) = '1' THEN
             lv_cuenta := i.custcode;
          ELSE
            lv_cuenta := substr(i.custcode,1,instr(i.custcode, '.', 1, 2) - 1);
          END IF;
          --
          lv_telefono   := i.dn_num;
          --
          --JRO:16/05/2006, Se agrega en el query general autorizado por EGA.
          -- Obtener deuda del periodo y saldo          
          /*OPEN  Cur_deudaSaldoReac ( lv_cuenta );
          FETCH Cur_deudaSaldoReac INTO lc_deudaSaldo;
          lb_found := Cur_deudaSaldoReac%FOUND;
          CLOSE Cur_deudaSaldoReac ;
          --
          IF lb_found THEN
             ln_deuda_periodo := lc_deudaSaldo.deuda;
             ln_saldo         := lc_deudaSaldo.saldo;
          ELSE
             ln_deuda_periodo := 0;
             ln_saldo         := 0;
          END IF;
          */
          --FIN
          --
          ln_deuda_periodo := i.deuda;
          ln_saldo         := i.saldo;
          ln_saldo_impago := (ln_deuda_periodo * ln_por_impago);
          --      
          -- obtener el ciclo de la cuenta
          lv_cicloAdm := i.billcycle;
          lv_ciclo := i.cycles;
          -- Obtener fecha inicial del periodo 

                   GENERA_FECHA_PERIODO_PR(lv_cicloAdm,
                                           lv_mes,
                                           lv_inicio_periodo,
                                           lv_fin_periodo,
                                           lv_periodo_iniciado,
                                           lv_numero_dias,
                                           lv_ciclo);
          
          -- Obtener creditos realizados durante el mes y no aplicados 
 /*         OPEN cur_Creditos (lv_cuenta,lv_inicio_periodo);
          FETCH cur_Creditos INTO lc_creditos;
          lb_found := Cur_creditos%FOUND;
          CLOSE cur_Creditos ;
          --
          IF lb_found THEN
             ln_creditos := lc_creditos.creditos;
          ELSE
             ln_creditos := 0;
          END IF;*/
          
          --------------------------------------------------------   
          --JRO: 1929, Cambios en el Reloj, autorizado por CTU/EGA
          --------------------------------------------------------     
          -- Obtener creditos realizados durante el mes y no aplicados 
          OPEN cur_Creditos_msj (i.customer_id_high , lv_inicio_periodo );
          FETCH cur_Creditos_msj INTO Lc_Creditos_Msj;
          lb_found := Cur_creditos_msj%FOUND;
          CLOSE cur_Creditos_msj;
          --
          IF lb_found THEN
             ln_creditos := Lc_Creditos_Msj.creditos;
          ELSE
             ln_creditos := 0;
          END IF;
          
          --
          ln_saldo_real := ln_saldo + ln_creditos;
        END IF;
        --
        -- Si Lv_Saldo es menor o igual a Fv_Saldo_Min, se da por cancelada la deuda       
        -- if (i.saldo <= Ln_Saldo_Min) Then
        IF (ln_saldo_real <= Ln_Saldo_Min) OR (ln_saldo_real <= ln_saldo_impago) THEN
          INSERT INTO RC_TMP_DATOS_GENERALES
          VALUES
            (i.custcode,
             i.cstype,
             i.prgcode,
             i.prev_balance,
             i.saldo,
             i.co_id,
             i.co_ext_csuin,
             i.product_history_date,
             i.tmcode,
             i.plcode,
             i.ch_status,
             NULL,
             NULL,
             NULL,
             i.main_dirnum,
             i.cs_deactiv_date,
             i.dn_num,
             i.dn_status,
             NULL,
             'A',
             null,
             ln_deuda_periodo,
             ln_creditos,
             i.tradecode);
          ln_cantidad := ln_cantidad + 1;
        END IF;      
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error: ' || SQLERRM;
      END;
      --
      IF ln_cantidad = 100 THEN
        COMMIT;
        pn_cant     := pn_cant + ln_cantidad;
        ln_cantidad := 0;
      END IF;    
    END LOOP;
    --
    pn_cant     := pn_cant + ln_cantidad;
    --
    inserta_auditoria_reloj('OP2:FINALIZA, PROCESO DE INSERCION A LA TABLA rc_tmp_datos_generales');
    --       
    COMMIT;      

    -- Obtener datos para paso de estatus.
    -- ----------------------------------
  ELSIF (pn_accion = 3) THEN
    --
    -- Borrar datos de la tabla rc_tmp_datos_generales_status
    -----------------------------------------------------------
    -- Abrir cursor
    v_cursor := DBMS_SQL.open_cursor;
  
    -- Eliminar la tabla no actualizada que contiene informaci�n desde comisiones
    v_sentencia_string := 'TRUNCATE TABLE rc_tmp_datos_generales_status';
    Begin
      DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
    exception
      when others then
        if SQLCODE != -942 then
          raise;
        end if;
    end;
    --  cerrar el cursor
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    --
    --     
    FOR i in cur_tipo_reloj LOOP
      BEGIN  
        --
        ln_validacion := nvl(i.rc_dia_efectivo, 1);
        ln_duracion := i.rc_duracion;
        ln_predecesor := i.rc_predecesor;
        ln_tipo_reloj := i.rc_tipo_reloj;
        IF (ln_validacion = 1) THEN
          INSERT /*+ APPEND */          
          INTO rc_tmp_datos_generales_status NOLOGGING
          (custcode, cstype, prgcode, prev_balance, saldo, co_id, co_ext_csuin, product_history_date, tmcode, plcode, ch_status, sncode, prm_value_id, status, main_dirnum, cs_deactiv_date, dn_num, dn_status, spcode, procesada, sm_serialnum)          
          (SELECT /*+ index(cu,pkcustomer_all),
                     index(co,pkcontract_all ),
                     index(cs,pk_contr_services_cap),
                     index(di,pkdirectory_number),
                     index(cd,pkcontr_devices),
                     index(sm,stormed_sepl),
                 +*/
           DISTINCT cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, cu.cstradecode tradecode,
                    co.co_id, co.co_ext_csuin, co.product_history_date, co.tmcode, co.plcode,
                    cur.ch_status,NULL,NULL,NULL, cs.main_dirnum, cs.cs_deactiv_date, di.dn_num, di.dn_status,NULL, 'A', sm.sm_serialnum
             FROM customer_all        cu,
                  contract_all        co,
                  curr_co_status      cur,
                  contr_services_cap  cs,
                  directory_number    di,
                  contr_devices       cd,
                  storage_medium      sm
            WHERE co.customer_id = cu.customer_id 
			  AND cur.co_id = co.co_id      
              AND cu.cstype not in ('d', 'o') 
              AND co.co_ext_csuin = i.rc_predecesor               
              AND cur.ch_status not in ('d', 'o') 
              AND to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') -  trunc(co.product_history_date) >= i.rc_duracion 
              AND cu.cstradecode IN ( SELECT b.rc_tipo_credito 
                                      FROM rc_tr_tc@axis b
                                      WHERE b.rc_tipo_reloj = i.rc_tipo_reloj
                                      ) 
              AND cu.prgcode     IN ( SELECT c.st_id_sub 
                                      FROM rc_sub_tipos_reloj@axis c
                                      WHERE c.st_tipo_reloj = i.rc_tipo_reloj
                                      ) 
              AND cs.co_id = co.co_id 
              AND cs.seqno       = ( SELECT MAX(ccl.seqno)
                                       FROM contr_services_cap ccl
                                      WHERE ccl.co_id = cs.co_id) 
              AND di.dn_id = cs.dn_id AND (cs.cs_deactiv_date IS NULL) 
              AND di.dn_status = 'a' 
              AND cd.co_id = co.co_id AND (cd.cd_deactiv_date IS NULL) 
              AND cd_seqno      = ( SELECT MAX(cdd.cd_seqno)
                                      FROM contr_devices cdd
                                     WHERE cdd.co_id = co.co_id) 
              AND sm.sm_serialnum = cd.cd_sm_num  
              AND sm.sm_status = 'a'
            GROUP BY cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.cstradecode, cu.prgcode,
                     co.co_id, cur.ch_status, co.co_ext_csuin, co.product_history_date,
                     co.tmcode, co.plcode, cs.main_dirnum,cs.cs_deactiv_date,di.dn_num,di.dn_status,
                     sm.sm_serialnum 
           );
        --
        ELSIF (ln_validacion <> 1) THEN
          --
          INSERT /*+ APPEND */
          INTO rc_tmp_datos_generales_status NOLOGGING
          (custcode, cstype, prgcode, prev_balance, saldo, co_id, co_ext_csuin, product_history_date, tmcode, plcode, ch_status, sncode, prm_value_id, status, main_dirnum, cs_deactiv_date, dn_num, dn_status, spcode, procesada, sm_serialnum)           
         ( SELECT /*+ index(cu,pkcustomer_all),
                     index(co,pkcontract_all ),
                     index(cs,pk_contr_services_cap),
                     index(di,pkdirectory_number),
                     index(cd,pkcontr_devices),
                     index(sm,stormed_sepl),
                 +*/
              DISTINCT cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, cu.cstradecode tradecode,
                       co.co_id, co.co_ext_csuin, co.product_history_date, co.tmcode,  co.plcode,
                       cur.ch_status,NULL,NULL,NULL,cs.main_dirnum, cs.cs_deactiv_date, di.dn_num, di.dn_status,NULL,'A', sm.sm_serialnum
                FROM customer_all        cu,
                     contract_all        co,
                     curr_co_status      cur,
                     contr_services_cap  cs,
                     directory_number    di,
                     contr_devices       cd,                    
                     storage_medium      sm
               WHERE co.customer_id = cu.customer_id 
				 AND cur.co_id = co.co_id            
                 AND cu.cstype NOT IN ('d', 'o') 
                 AND co.co_ext_csuin = i.rc_predecesor 
                 AND cur.ch_status NOT IN ('d', 'o') 
                 AND cu.cstradecode IN ( SELECT b.rc_tipo_credito
                                           FROM rc_tr_tc@axis b
                                          WHERE b.rc_tipo_reloj = i.rc_tipo_reloj
                                          ) 
                 AND cu.prgcode     IN ( SELECT c.st_id_sub
                                           FROM rc_sub_tipos_reloj@axis c
                                          WHERE c.st_tipo_reloj = i.rc_tipo_reloj
                                          ) 
                 AND cs.co_id = co.co_id 
                 AND cs.seqno = ( SELECT MAX(ccl.seqno)
                                    FROM contr_services_cap ccl
                                   WHERE ccl.co_id = cs.co_id) 
                 AND di.dn_id = cs.dn_id AND (cs.cs_deactiv_date IS NULL) 
                 AND di.dn_status = 'a' 
                 AND cd.co_id = co.co_id AND (cd.cd_deactiv_date IS NULL) 
                 AND cd_seqno = ( SELECT MAX(cdd.cd_seqno)
                                    FROM contr_devices cdd
                                   WHERE cdd.co_id = co.co_id)
                 AND sm.sm_serialnum = cd.cd_sm_num 
                 AND sm.sm_status = 'a'
              GROUP BY cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.cstradecode, cu.prgcode, co.co_id,
                       cur.ch_status, co.co_ext_csuin,co.product_history_date, co.tmcode, co.plcode,
                       cs.main_dirnum, cs.cs_deactiv_date, di.dn_num,di.dn_status, sm.sm_serialnum             
             );          
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error: ' || SQLERRM;
      END;
      --
      COMMIT;
      --    
    END LOOP;
    --
    COMMIT;
    --
    --
    OPEN Cur_countTelefonos;
    FETCH Cur_CountTelefonos INTO ln_cantidad;
    CLOSE Cur_CountTelefonos;
    --
    pn_cant:= ln_cantidad;
    
  END IF;
  --
EXCEPTION
  WHEN OTHERS THEN
    Pv_error := 'Ha ocurrido un error general en la carga de datos => ' || SQLERRM;
END RC_OBTENER_DATOS_RELOJ_MSJ ;
/

