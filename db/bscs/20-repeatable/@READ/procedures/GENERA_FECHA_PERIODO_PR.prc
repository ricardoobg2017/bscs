CREATE OR REPLACE PROCEDURE GENERA_FECHA_PERIODO_PR( Pv_cicloAdm         In Varchar2,
                                                    lv_mes              out varchar2,
                                                    lv_inicio_periodo   out varchar2,
                                                    lv_fin_periodo      out varchar2,
                                                    lv_periodo_iniciado out varchar2,
                                                    lv_numero_dias      out varchar2,
                                                    lv_ciclo            in  Varchar2) is

/* 
   Modificado por    : SUD Rene Vega Lascano
   Fecha modificaci�n: 29-01-2008
   Proyecto          : [2702] Tercer Ciclo de Facturacion. Lider: SIS Guillermo Proa�o  
   Comentarios       : Soporte del Reloj de Cobranzas para n ciclos de facturacion.     
*/

lv_fecha                date;        --variable para capturar la fecha del sistema
lv_dia                  varchar2(2); --variable para el rango de un periodo

lv_ciclo_bscs           varchar2(2);
lv_desc_ciclo           varchar2(500);
lv_error                varchar2(100);

begin


select sysdate into lv_fecha from dual;

lv_dia := to_char(lv_fecha,'dd');

--Inicio. [2702] Tercer Ciclo de Facturacion. SUD Rene Vega. Se llama al API para obtener los datos del ciclo

begin
      CLK_API_POLITICA_CICLO.CLP_OBTIENE_CICLO@AXIS
                             (pd_fecha       => SYSDATE,           -- sysdate por default
                              pv_ciclo_axis  => lv_ciclo,          -- ciclo en axis
                              pv_ciclo_bscs  => lv_ciclo_bscs,     -- ciclo en bscs
                              pv_per_ini     => lv_inicio_periodo, -- fecha de inicio de periodo
                              pv_per_fin     => lv_fin_periodo,    -- fecha de fin de periodo
                              pv_desc_ciclo  => lv_desc_ciclo,     -- descripci�n del ciclo
                              pv_codigoerror => lv_error);
   
   exception
      when others then
          dbms_output.put_line('Error en procedure genera_fechas_periodos en llamada al CLK_API_POLITICA_CICLO');           
   end;                           
   
   lv_periodo_iniciado:= to_char(to_date(lv_inicio_periodo,'dd/mm/yyyy')+1,'dd/mm/yyyy');  
   
   lv_numero_dias := to_char(to_date(lv_fin_periodo,'dd/mm/yyyy')+1 - to_date(lv_inicio_periodo,'dd/mm/yyyy'));
  
   select TO_CHAR(round(to_date(lv_inicio_periodo,'dd/mm/yyyy'),'MONTH'),'Mon') into lv_mes from dual; 
   
--[2702] Tercer Ciclo de Facturacion. SUD Rene Vega. Se comenta las validaciones para ciclo 01 y 02
/*If lv_ciclo = '01' Then
  --
  if lv_dia >= '24' then
     lv_numero_dias := to_char(last_day(lv_fecha),'dd');
     lv_mes := to_char(add_months(sysdate,1),'Mon');
     lv_periodo_iniciado := '25/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
     lv_inicio_periodo   := '24/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
     if to_char(lv_fecha,'mm')='12' then
  	   lv_fin_periodo := '23/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(add_months(sysdate,12),'yyyy');-- +1a�o en caso de ser periodo de Enero
     else
  	    lv_fin_periodo := '23/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;
  else
     lv_numero_dias := to_char(last_day(add_months(sysdate,-1)),'dd');
     lv_mes := to_char(lv_fecha,'Mon');
     if to_char(lv_fecha,'mm')='01' then
        lv_periodo_iniciado := '25/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para periodo iniciado
        lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
     else
        lv_periodo_iniciado := '25/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
        lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;
     lv_fin_periodo := '23/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
  end If;
  --
Elsif lv_ciclo = '02' Then
  --
  if lv_dia >= '08' then
     lv_numero_dias := to_char(last_day(lv_fecha),'dd');
     --lv_mes := to_char(add_months(sysdate,1),'Mon');
     lv_mes := to_char(lv_fecha,'Mon');
     lv_periodo_iniciado := '09/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
     lv_inicio_periodo   := '08/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
     if to_char(lv_fecha,'mm')='12' then
  	   lv_fin_periodo := '07/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(add_months(sysdate,12),'yyyy');-- +1a�o en caso de ser periodo de Enero
     else
  	    lv_fin_periodo := '07/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;
  else
     lv_numero_dias := to_char(last_day(add_months(sysdate,-1)),'dd');
     lv_mes := to_char(add_months(sysdate,-1),'Mon');
     --lv_mes := to_char(lv_fecha,'Mon');
     if to_char(lv_fecha,'mm')='01' then
        lv_periodo_iniciado := '09/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para periodo iniciado
        lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
     else
        lv_periodo_iniciado := '09/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
        lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;
     lv_fin_periodo := '07/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
  end If;

  --
End If;*/

commit;
--SUD Rene Vega. Cierre del DB_LINK.
dbms_session.close_database_link('AXIS');
commit;

exception 
   when others then
      rollback;
      dbms_session.close_database_link('AXIS');
      rollback;      
      dbms_output.put_line('Error en procedure genera_fechas_periodos');

end GENERA_FECHA_PERIODO_PR;
/
