CREATE OR REPLACE Procedure read.Isp_prueba (pv_fecha In Date) Is
 ld_fecha   Date;
 ln_dia     Number;
 ln_mes     Number;
 ln_a�o     Varchar2(4);
 lv_error   Varchar2(100);

Begin
  lv_error:='OK';
  Begin 
   ld_fecha := to_char(pv_fecha,'dd/mm/yyyy');
  Exception
    When Others Then
      ln_dia :=to_number(to_char(pv_fecha,'dd'));
      ln_mes :=to_number(to_char(pv_fecha,'mm'));
      ln_a�o :=to_char(pv_fecha,'yyyy');

      If ln_dia <= 31 Then
        If ln_mes <=12 Then
          If Length(ln_a�o) != 4 And to_number(ln_a�o) <= 1000 Then
             lv_error := 'Formato de fecha invalido : El a�o debe ser expresado en 4 digitos';
          End If;
        Else
          lv_error := 'Formato de fecha invalido : El mes nos puede ser mayor a 12';
        End If;
      Else
        lv_error := 'Formato de fecha invalido : El dia nos puede ser mayor a 31';
      End If;
   End;

  Dbms_Output.put_line(lv_error);
End;
/
