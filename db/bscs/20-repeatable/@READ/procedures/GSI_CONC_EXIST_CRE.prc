CREATE OR REPLACE procedure GSI_CONC_EXIST_CRE is
begin
  delete from temporal;
  commit;
  
  insert into temporal
  select telefono
  from axisuser23.temporalcro@axis09;
  commit;
  
  delete temporal where telefono in (
  select /*+ RULE +*/decode(substr(a.dn_num,4,1),'8',substr(a.dn_num,-8),substr(a.dn_num,-7))
       from directory_number a, contr_services_cap b, contract_all c, contr_devices d, temporal e
       where a.dn_id=b.dn_id and b.co_id=c.co_id and c.co_id=d.co_id and d.cd_deactiv_date is null
       and a.dn_status = 'a'
       and b.cs_deactiv_date is null
       and dn_num=decode(length(e.telefono),8,'593','5939')||e.telefono);
  commit;
  
  delete from axisuser23.temporalcro@axis09;
  commit;

  insert into axisuser23.temporalcro@axis09(telefono)
  select *
  from temporal;
  commit;

end GSI_CONC_EXIST_CRE;
/

