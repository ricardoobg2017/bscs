CREATE OR REPLACE procedure SUBIR_ARCHIVO(linea in varchar2) is
 ---TABLA QUE AFECTA SUBIR_DATOS
 --bytes varchar2(14);
 bytes number;
 fecha varchar2(30);
 nombre varchar2(40);
 fec date;

begin
 --bytes := length(linea);
 bytes := substr(linea,1,12);
 fecha := substr(linea,14,12);
 
 fecha := substr(fecha,5,2)|| '/' || substr(fecha,1,3)||'/2006';--||substr(fecha,9,5)||':00';
 fec := to_date(fecha,'dd/mm/yyyy');
 
 nombre := substr(linea,27,length(linea)-1);
 
 --INSERT INTO SUBIR_DATOS VALUES(bytes,to_date(fecha,'dd/mm/yyyy'),nombre);
 INSERT INTO SUBIR_DATOS VALUES(bytes,fec,nombre);
 COMMIT;
 --DBMS_OUTPUT.PUT_LINE(bytes);
 --DBMS_OUTPUT.PUT_LINE(fec);
 --DBMS_OUTPUT.PUT_LINE(nombre);
 --DBMS_OUTPUT.PUT_LINE(linea);
    
end SUBIR_ARCHIVO;
/

