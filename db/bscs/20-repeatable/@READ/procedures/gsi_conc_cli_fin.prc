CREATE OR REPLACE procedure GSI_CONC_CLI_FIN(despacha number) is
  cursor base is
    select a.*,a.rowid
    from temporal a
    where mensaje is null and despachador=despacha;
    
  ln_contador number(4):=0;
begin
  for i in base loop
            ln_contador:=ln_contador+1;
            insert into ops$pagos.clientes@FINGYE_BUROCRED
            select /*+ index(a CUST_CUSTCODE) index(c ORDERCUSTOMER)  */decode(a.costcenter_id,1,2,1) f_comp, a.custcode f_cta,1 f_recau, 
                     decode(b.cssocialsecno,null,'1',substr(decode(b.cssocialsecno,null,'1',' ','2',b.cssocialsecno),1,15))f_ced, 
                     nvl(substr(ltrim(rtrim(b.cclname|| ' ' ||b.ccfname)),1,120),'X') f_cli,
                     substr(b.ccname||' '||b.cccity,1,100) f_dir,
                     b.cctn f_tel1, b.cctn2 f_tel2, /*d.bank_id*/nvl(e.bank_cbill,70) f_formap, 
                     substr(decode(d.bankaccno,null,'otros',d.bankaccno),1,16) f_ctatar,0,0,0,0,0,0,sum(c.ohopnamt_doc) f_sal,
                     sum(c.ohopnamt_doc) f_sal,null,null
              from   customer_all a , ccontact_all b, orderhdr_all c, 
                     payment_all d, ops$inventar.mapeo_fr_pago@FINGYE_BUROCRED e
              where  c.ohstatus in ('IN','CO','CM','FC')
              and    b.ccbill||'' = 'X' 
              and    e.bank_bscs = d.bank_id
              and    a.customer_id > 0
              and    a.paymntresp = 'X'
              and    a.cstype in ('a','s','d')
              and    d.act_used = 'X'
              and    c.ohopnamt_doc >= 0
              and    b.customer_id = a.customer_id
              and    c.customer_id = b.customer_id
              and    d.customer_id = c.customer_id 
              and    a.custcode=i.telefono
              group by a.costcenter_id, a.custcode, 
                       decode(b.cssocialsecno,null,'1',substr(decode(b.cssocialsecno,null,'1',' ','2',b.cssocialsecno),1,15)), 
                       nvl(substr(ltrim(rtrim(b.cclname|| ' ' ||b.ccfname)),1,120),'X'),
                       substr(b.ccname||' '||b.cccity,1,100),
                       b.cctn, b.cctn2,e.bank_cbill, 
                       substr(decode(d.bankaccno,null,'otros',d.bankaccno),1,16);
                       
              update temporal
              set mensaje='OK'
              where rowid=i.rowid;
              if (ln_contador=200) then
                 commit;
                 ln_contador:=0;
              end if;
  end loop;
  commit;
end GSI_CONC_CLI_FIN;

/
