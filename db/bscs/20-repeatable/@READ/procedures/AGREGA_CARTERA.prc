CREATE OR REPLACE procedure agrega_cartera(p_fecha in varchar2,
                                           p_error out varchar2) is

  cursor facturacion(v_fecha varchar2) is
    select b.custcode cuenta, e.prgcode producto, c.cccity canton, c.ccstate provincia, c.cclname apellidos, 
           c.ccfname nombres, c.cssocialsecno ruc, d.bankaccno tarjeta_cuenta, d.accountowner tipo_cuenta, b.csactivated fecha_apertura,
           f.tradecode grupo, d.valid_thru_date fech_expir_tarjeta, c.cctn telefono1, c.cctn2 telefono2, b.prev_balance saldo_anterior,
           c.ccstreet direccion, a.ohinvamt_gl balance, b.customer_id id, b.costcenter_id compania, g.bankname forma_pago, a.ohxact factura
           
    from orderhdr_all a, customer_all b, ccontact_all c, payment_all d, pricegroup_all e, trade_all f, bank_all g
    where a.customer_id=b.customer_id
    and b.customer_id=c.customer_id
    and a.customer_id=c.customer_id
    and b.customer_id=d.customer_id
    and b.prgcode=e.prgcode
    and b.cstradecode=f.tradecode
    and d.bank_id=g.bank_id
    and d.act_used='X'
    and c.ccbill = 'X'
    and a.ohstatus in ('CM','IN')
    --and b.custcode='1.10002404'
    and a.ohentdate=to_date('24/07/2003','dd/mm/yyyy')
    and b.custcode in (select se.cuenta from co_servicios1 se);
    
  
  cursor balances(v_id number) is
    select /*+ rule +*/ t.ohinvamt_doc valor_actual, t.ohopnamt_doc valor_anterior, t.ohentdate fecha
    from orderhdr_all t
    where t.customer_id=v_id
    and t.ohstatus in ('CM','IN')
    order by t.ohentdate desc;
    
  ln_bal1 number;       ln_bal30 number;     ln_bal60 number;     ln_bal90 number;     ln_bal120 number;     
  ln_bal150 number;     lb_found boolean;  lc_facturacion facturacion%ROWTYPE;   lc_balances balances%ROWTYPE;         
  ln_contador number;         ln_cont_may_150 number;
  lf_deuda_vencida float;     lf_deuda_total float;     lv_mayor varchar2(15); 
  ln_contador_facturacion number:=0;            le_exception exception; lv_fecha varchar2(20);                  lv_error varchar2(200);
  lv_mes_actual varchar2(2);                    lv_mes varchar2(2); ln_dif number;
  lv_apellidos varchar2(40):=null; lv_nombres varchar2(40):=null; lv_ruc varchar2(20):=null; lv_direccion varchar2(70):=null;
  lv_telefono1 varchar2(25):=null; lv_telefono2 varchar2(25):=null;
  
  begin
    open facturacion(p_fecha);
    fetch facturacion into lc_facturacion;
    lb_found:=facturacion%found;
    close facturacion;
    
    if lb_found then
      lv_mes_actual:=substr(p_fecha,4,2);
      for a in facturacion(p_fecha) loop
        ln_contador:=0;
        ln_bal1:=0;       ln_bal30:=0;     ln_bal60:=0;     ln_bal90:=0;     ln_bal120:=0;     
        ln_bal150:=0;     ln_cont_may_150:=150;
        lf_deuda_vencida:=0;     lf_deuda_total:=0;     lv_mayor:='V';
        ln_contador_facturacion:=ln_contador_facturacion+1;
        open balances(a.id);
        fetch balances into lc_balances;
        lb_found:=balances%found;
        close balances;
        if lb_found then
          for b in balances(a.id) loop
            ln_contador:=ln_contador+1;
            lv_mes:=substr(to_char(b.fecha,'dd/mm/yyyy'),4,2);
            ln_dif:=to_number(lv_mes_actual) - to_number(lv_mes);
            if ln_dif = 6 and b.valor_anterior > 0 then
              lv_mayor:='180';
            elsif ln_dif = 5 and b.valor_anterior > 0 then
              ln_bal150:=b.valor_anterior;
              lf_deuda_vencida:=lf_deuda_vencida+b.valor_anterior;
              lv_mayor:='150';
            elsif ln_dif = 4 and b.valor_anterior > 0 then
              ln_bal120:=b.valor_anterior;
              lf_deuda_vencida:=lf_deuda_vencida+b.valor_anterior;
              lv_mayor:='120';
            elsif ln_dif = 3 and b.valor_anterior > 0 then
              ln_bal90:=b.valor_anterior;
              lf_deuda_vencida:=lf_deuda_vencida+b.valor_anterior;
              lv_mayor:='90';
            elsif ln_dif = 2 and b.valor_anterior > 0 then
              ln_bal60:=b.valor_anterior;
              lf_deuda_vencida:=lf_deuda_vencida+b.valor_anterior;
              lv_mayor:='60';
            elsif ln_dif = 1 and b.valor_anterior > 0 then
              ln_bal30:=b.valor_anterior;
              lf_deuda_vencida:=lf_deuda_vencida+b.valor_anterior;
              lv_mayor:='30';
            elsif ln_dif = 0 then
              ln_bal1:=b.valor_actual;
              if ln_bal1 < 0 then
                lv_mayor:='Saldo a favor';
              end if;
            end if;
          end loop;
          if ln_contador = 0 then
            ln_bal1:=0;
            lv_mayor:='0';
          end if;
          lf_deuda_total:=lf_deuda_vencida+ln_bal1;
          SELECT REPLACE(a.apellidos,',',' ') INTO lv_apellidos FROM DUAL;
          SELECT REPLACE(a.nombres,',',' ') INTO lv_nombres FROM DUAL;
          SELECT REPLACE(a.ruc,',',' ') INTO lv_ruc FROM DUAL;
          SELECT REPLACE(a.telefono1,',',' ') INTO lv_telefono1 FROM DUAL;
          SELECT REPLACE(a.telefono2,',',' ') INTO lv_telefono2 FROM DUAL;
          SELECT REPLACE(a.direccion,',',' ') INTO lv_direccion FROM DUAL;
          SELECT REPLACE(lv_nombres,'"','') INTO lv_nombres FROM DUAL;
          SELECT REPLACE(lv_apellidos,'"','') INTO lv_apellidos FROM DUAL;
          SELECT REPLACE(lv_direccion,'"','') INTO lv_direccion FROM DUAL;
          insert into co_cartera_2
            (cuenta,cliente,producto,compania,canton,provincia,apellidos,nombres,ruc,forma_pago,tarjeta_cuenta,
             fech_expir_tarjeta,tipo_cuenta,telefono1,telefono2,direccion,grupo,bal1,bal30,bal60,bal90,
             bal120,bal150,saldo_anterior,deuda_vencida,deuda_total,mayor_vencido,factura, FEC_APER_CUENTA)
            values(a.cuenta,a.id,a.producto,a.compania,a.canton,a.provincia,lv_apellidos,lv_nombres,lv_ruc,a.forma_pago,
                   a.tarjeta_cuenta,a.fech_expir_tarjeta,a.tipo_cuenta,lv_telefono1,lv_telefono2,lv_direccion,a.grupo,
                   ln_bal1,ln_bal30,ln_bal60,ln_bal90,ln_bal120,ln_bal150,a.saldo_anterior,lf_deuda_vencida,
                   lf_deuda_total,lv_mayor,a.factura, a.fecha_apertura);
          
          if ln_contador_facturacion = 10000 then
            ln_contador_facturacion:=0;
            commit;
          end if;
        else
          lv_error:='No existen datos para la fecha indicada.';
          raise le_exception;
        end if;       
      end loop;
      commit;
    else
      lv_error:='No existen datos para la fecha indicada.';
      raise le_exception;
    end if;
 exception
   when le_exception then
     p_error:=lv_error; 
     rollback;
   when others then
     p_error:=sqlerrm;
     rollback;
 end;
/

