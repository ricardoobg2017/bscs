CREATE OR REPLACE PROCEDURE      MKP_BP_CARGA_CUSTCODE_bsmay IS
-- Paquete de Puntos obtiene los clientes que participa en la promoción --

CURSOR C_DAT_CUSTCODE  IS
/*  SELECT  \*+ rule +*\  Distinct B.CUSTCODE, B.PRGCODE, B.COSTCENTER_ID
  FROM  ivrpuntos2.MK_SERVICIOS_PUNTUADOS@colector A, CUSTOMER_ALL B
  WHERE A.CODIGO_DOC = B.CUSTCODE
  AND A.ESTADO = 'A';*/
  SELECT  /*+ rule +*/  Distinct B.CUSTCODE, B.PRGCODE, B.COSTCENTER_ID
  FROM  porta.MK_SERVICIOS_PUNTUADOS@axis A, CUSTOMER_ALL B, MK_CONFIGURACION_CICLO C
  --FROM  porta.MK_SERVICIOS_PUNTUADOS@axis A, CUSTOMER_ALL B, MK_CONFIGURACION_CICLO C
  WHERE A.CODIGO_DOC = B.CUSTCODE
  AND A.ID_CICLO = C.CICLO        
  AND A.ESTADO = 'A'
  AND C.ESTADO = 'A';
  

  
  LV_ERROR                   VARCHAR2(100);
  LV_PROGRAMA                VARCHAR2(60):= 'MKP_BP_CARGA_CUSTCODE';
  LE_NO_DATOS                EXCEPTION;
  LV_CNT                     NUMBER; 
  lv_sentencia               Varchar2(500);
Begin

  lv_sentencia:='truncate table  MK_BSCS_CARGA_FACT';
  Execute Immediate lv_sentencia;
  
  Delete ivrpuntos2.MK_AXIS_CARGA_FACT@colector_pto;

  Commit; 
  LV_CNT := 0;
  FOR i IN C_DAT_CUSTCODE LOOP
      INSERT INTO MK_BSCS_CARGA_FACT(custcode,prgcode,costcenter,estado,Fecha)  VALUES(i.CUSTCODE, i.PRGCODE, i.COSTCENTER_ID, 'I', Sysdate);
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
  END LOOP;
 
  COMMIT;  
 
   Insert Into Ivrpuntos2.Mk_Axis_Carga_Fact@colector_pto
      Select Custcode, Prgcode, Costcenter, Estado, Sysdate, Null, Null From Mk_Bscs_Carga_Fact;
      
   Commit;

  EXCEPTION
     WHEN LE_NO_DATOS  THEN
        -- Inserto registro en bitacora con status de error
        NULL;
     WHEN OTHERS THEN
        IF LV_ERROR IS NULL THEN
           LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();
        END IF; 
        -- Inserto registro en bitacora con status de error
END MKP_BP_CARGA_CUSTCODE_bsmay;



/
