CREATE OR REPLACE procedure carga_datos_reporte ( p_anio in number,
                                                                              p_mes in number,
                                                                              p_val_salida out number,
                                                                              p_salida out varchar2) is

cursor c_pagos is
  select cuenpago cuenta, 
            sum(valopago) pago
  from ec_pagos t
  where t.aniopago = p_anio
  and t.mespago = p_mes
  group by cuenpago;
  
cursor c_consumos_inpagos is
 select a.cuencons          cuenta,
           sum(a.ecuacons) consumo
 from   ec_consumos a
 where a.reca_cons = 0
 and     a.fech_reca_cons = null
 and     a.mescons = p_mes
 and     a.aniocons = p_anio
 group by cuencons;
 
---Definición de variables
consumo             number;
saldo_act            number;
saldo_ant            number;
lc_pagos             c_pagos%rowtype;
lb_notfound        boolean;
lv_error              varchar2(100);
le_mierror          exception;
inpago               number;
saldo_inpago      number;
valo_rest            number;
lc_consumos_inpagos   c_consumos_inpagos%rowtype;


begin
p_val_salida := 0;
p_salida := 'Proceso exitoso';

  open c_pagos;
  fetch c_pagos into lc_pagos;
  lb_notfound := c_pagos%notfound;
  close c_pagos;
  
 if lb_notfound then
     lv_error := 'No existen datos de pagos';
     raise le_mierror;
  else   
    for fila in c_pagos loop 
      ---Primero se debe encerar los saldos de la tabla ec_datos_saldos
        begin
          select c.saldrep
          into   saldo_ant
          from  ec_datos_saldos c
          where c.cuenrep = fila.cuenta
          and  saldrep != 0;
          
        exception
           when no_data_found then
              null;
         
        end;

        if saldo_ant is null then
              valo_rest := 0;
        else
             if saldo_ant < fila.pago then
                 valo_rest := fila.pago - saldo_ant;
                 update ec_datos_saldos z
                 set       z.saldrep = 0,
                             z.consrep = 0,
                             z.pagorep = 0
                 where  cuenrep = fila.cuenta;
                 commit;
             end if;
        end if;
        
       ---Ahora se procederá a verificar que los consumos actuales sean cubiertos por el
       ---valor restante del pago
        begin
          select sum(ecuacons)
          into   consumo 
          from ec_consumos a
          where aniocons= p_anio
          and mescons = p_mes
          and cuencons = fila.cuenta
          and a.reca_cons = 0;
          
          if consumo is null then
                consumo := 0; 
          else
            if consumo <= valo_rest then
              update ec_consumos
              set       reca_cons = 1,
                         fech_reca_cons = sysdate
              where cuencons = fila.cuenta
              and     aniocons = p_anio
              and     mescons = p_mes;
              commit;
            else
              saldo_act := nvl(consumo - valo_rest,0);
              update ec_datos_saldos
              set consrep = consumo,
                   PAGOREP = fila.pago ,
                   SALDREP = saldo_act
              where cuenrep = fila.cuenta;
              /*insert into ec_datos_saldos(aniorep,mesrep,cuenrep,consrep,pagorep,saldrep)
              values(p_anio,p_mes,fila.cuenta,consumo,fila.pago,saldo_act);*/
              commit;
            end if;
          end if;
          
          exception
          when no_data_found then
            null;
         end;
         
  end loop;
 end if;
 
 open c_consumos_inpagos;
  fetch c_consumos_inpagos into lc_consumos_inpagos;
  lb_notfound := c_consumos_inpagos%notfound;
  close c_consumos_inpagos;
  
 if lb_notfound then
     lv_error := 'No existen datos de consumos que falten de pagar';
     raise le_mierror;
  else   
    for i in c_consumos_inpagos loop
       inpago := 0;
       saldo_inpago := nvl(i.consumo - inpago,0);
       insert into ec_datos_saldos(cuenrep,consrep,pagorep,saldrep)
       values(i.cuenta,i.consumo,inpago,saldo_inpago);
       commit;
    end loop;
  end if;

 exception
 when le_mierror then
    p_val_salida := 1;
    p_salida := lv_error;
 when no_data_found then
   p_val_salida := 3;
   p_salida := 'No existen datos';
 when others then
    p_val_salida := 2;
    p_salida := 'Existió un error general'||' '||sqlerrm;
  
end carga_datos_reporte;
/

