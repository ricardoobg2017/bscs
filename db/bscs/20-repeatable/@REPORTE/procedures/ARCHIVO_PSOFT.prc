CREATE OR REPLACE procedure archivo_psoft is

  lv_archivo     varchar2(60);
  wline          varchar2(30);
  file_handle    SYS.UTL_FILE.FILE_TYPE;
  lb_eof         boolean := FALSE;
  ln_codi_err    number := 0;
  lv_mens_err    varchar2(1000);
  lv_estado      varchar2(15):= 'OK';
  lv_batch       varchar2(25);
  ERROR_SALIDA   exception;
  ln_saldo       number;
  ln_bankid      number;
  lv_bankaccno   varchar2(16);
  pv_cuenta      varchar2(25);
  
  cursor cuentas is
     select /*+rule */ a.customer_id, a.custcode, a.cssocialsecno, a.costcenter_id, 
            substr(ltrim(rtrim(b.cclname|| ' ' ||b.ccfname)),1,30) nombre
     from  customer_all a, sysadm.ccontact_all b
     where a.customer_id = b.customer_id
     and   a.customer_id_high is null
     and   b.ccbill = 'X' 
     and   a.paymntresp = 'X'
     and   a.cstype in ('a','s','d');
     
  --Telefonos de cuentas planas
  cursor telefonos_1 is 
     select /*+rule*/ substr(f.dn_num,5,7) celular
     from  customer_all d, 
           sysadm.directory_number f, 
           contract_all g,
           contr_services_cap h,
           costcenter j         
	   where d.customer_id = g.customer_id
     and   g.co_id = h.co_id
     and   h.dn_id = f.dn_id
     and   d.costcenter_id = j.cost_id
     and   d.custcode = pv_cuenta;
     
  --Telefonos de cuentas pool
  cursor telefonos_2 is             
     select /*+rule*/ substr(f.dn_num,5,7) celular 
     from  customer_all d, 
           customer_all k,
           sysadm.directory_number f, 
           contract_all g,
           contr_services_cap h,
           costcenter j         
  	 where d.customer_id_high = k.customer_id
     and   d.customer_id = g.customer_id
     and   g.co_id = h.co_id
     and   h.dn_id = f.dn_id
     and   d.costcenter_id = j.cost_id
     and   d.custcode = pv_cuenta;
  telef1  telefonos_1%rowtype;
  telef2  telefonos_2%rowtype;
  
  
begin
    lv_archivo := 'cuentas.dat';      
    file_handle := UTL_FILE.FOPEN ('/bscs/bscsprod/psoft/', lv_archivo, 'W');  
    for i in cuentas loop
        pv_cuenta := i.custcode;
        begin  --Suma del saldo
            select sum(ohopnamt_doc)
            into  ln_saldo
            from  orderhdr_all
            where customer_id = i.customer_id 
            and   ohstatus in ('IN','CO','CM','FC');
        exception
            when others then
              ln_saldo := -1;                  
        end;        
        begin  --Seleccion de la forma de pago y de la cuenta de la que esta pagando
/*          
            select bank_id, substr(decode(bankaccno,null,'otros',bankaccno),1,16)
            into  ln_bankid, lv_bankaccno
            from  sysadm.payment_all
            where customer_id = i.customer_id 
            and   act_used = 'X';
*/
            null;
        exception 
            when others then
               ln_bankid    := 0;
               lv_bankaccno := null;
        end;
        if i.custcode like '1.%' then
           open telefonos_1;
           fetch telefonos_1 into telef1;
           if telefonos_1%notfound then
              close telefonos_1;
              wline := i.custcode||'|'||'SN'||'|'||i.costcenter_id||'|'||i.nombre||'|'||i.cssocialsecno||
                       ln_bankid||'|'||lv_bankaccno||'|'||ln_saldo||'|'||ln_saldo;                       
           else
              close telefonos_1;
              for j in telefonos_1 loop
                  wline := i.custcode||'|'||j.celular||'|'||i.costcenter_id||'|'||i.nombre||'|'||i.cssocialsecno||
                           ln_bankid||'|'||lv_bankaccno||'|'||ln_saldo||'|'||ln_saldo;
              end loop;
           end if;
        else
           open telefonos_2;
           fetch telefonos_2 into telef2;
           if telefonos_2%notfound then
              close telefonos_2;
              wline := i.custcode||'|'||'SN'||'|'||i.costcenter_id||'|'||i.nombre||'|'||i.cssocialsecno||
                       ln_bankid||'|'||lv_bankaccno||'|'||ln_saldo||'|'||ln_saldo;
           else
              close telefonos_2;
              for j in telefonos_2 loop
                  wline := i.custcode||'|'||j.celular||'|'||i.costcenter_id||'|'||i.nombre||'|'||i.cssocialsecno||
                           ln_bankid||'|'||lv_bankaccno||'|'||ln_saldo||'|'||ln_saldo;
              end loop;
           end if;
        end if;
        UTL_FILE.PUT_LINE(file_handle,wline);  
    end loop;
    UTL_FILE.FCLOSE (file_handle);
EXCEPTION

  WHEN ERROR_SALIDA THEN
     DBMS_OUTPUT.PUT_LINE('Error '||lv_mens_err);
     UTL_FILE.FCLOSE (file_handle);
  WHEN OTHERS THEN
     DBMS_OUTPUT.PUT_LINE('Error '||SQLERRM);   
     UTL_FILE.FCLOSE (file_handle);
end archivo_psoft;
/

