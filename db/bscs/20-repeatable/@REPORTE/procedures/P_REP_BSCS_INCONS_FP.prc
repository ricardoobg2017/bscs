CREATE OR REPLACE procedure         p_rep_bscs_incons_fp is

----------- PROCEDURE PARA GENERAR TABLA DE INCONSISTENCIAS DE
----------- FORMAS DE PAGO DE BSCS
----------- DESARROLLADO POR: Romeo E. Cabrera. 17/07/2003
-------------------------------------------------------------------
---- Modificado: CLS - DARWIN ROSERO
---- Fecha Modificaci�n: 27/03/2006
-------------------------------------------------------------------
doc_banco     varchar2(25);
doc_tarje     varchar2(25);
temp_char     char(1);
desc_error    varchar2(50);
  ---======================
  /*select 
             y.customer_id, y.custcode, y.cstype, x.bank_id, z.bankname,
             y.cssocialsecno,v.cclname, v.ccfname ,x.bankaccno,x.valid_thru_date,
             x.payment_type
       
       from payment x,
           customer_all y,
           bank_all z, 
           ccontact_all v
       
       where x.customer_id = y.customer_id 
       and x.bank_id = z.bank_id
       and v.customer_id = x.customer_id
       and y.customer_id = v.customer_id;*/
---======================
cursor formas_pago is
select /*+ rule +*/
       y.customer_id, 
       y.custcode,
       y.cstype,
       y.prgcode,---para los  productos
       p.prgname,--nombre del producto
       y.costcenter_id,--para las regiones
       y.lbc_date,--- fecha del ultimo ciclo.
       x.bank_id,
       z.bankname,
       y.cssocialsecno,
       v.cclname, 
       v.ccfname,
       x.bankaccno,
       x.valid_thru_date,
       x.payment_type
       from payment x,
            ccontact_all v,
            customer_all y,
            bank_all z,
            pricegroup  p
       where x.customer_id = y.customer_id 
       and  x.Seq_Id =(Select Max(a.Seq_Id)
                               From   Payment a
                               Where  a.Customer_Id = x.customer_id
                               and a.ACT_USED='X')
       and v.customer_id = x.customer_id
       and v.ccseq =(Select Max(h.ccseq)
                               From   ccontact_all h
                               Where h.customer_id = v.customer_id)
       and z.bank_id = x.bank_id
       and  p.prgcode=y.prgcode;
begin
for f in formas_pago  LOOP
      ---Condicionante  de forma de Pago (-2) D�bito  Bancario ;(-3) Tarjeta Cr�dito
   if f.payment_type in (-2,-3) then 
       /* null;          --  contrafactura
         elsif f.payment_type = -2 then
           doc_banco := f.bankaccno;
           doc_tarje := null;          -- debito
        elsif f.payment_type = -3 then  
           doc_banco := null;
           doc_tarje := f.bankaccno;    -- Tarjeta de credito
        end if;
        */
        if f.bankaccno is null then
           null;
            --rp_bscs_incons_fp: 
            --y.customer_id, y.custcode, y.cstype, x.bank_id, z.bankname,
            --y.cssocialsecno,v.cclname, v.ccfname ,x.bankaccno,x.valid_thru_date           
           desc_error := 'Cuenta bancaria/TC nula';
           insert into rep_bscs_incons_fp values
                                         (f.customer_id,
                                         f.custcode,
                                         f.cstype,
                                         f.prgcode,---para los  productos
                                         f.prgname,--nombre del producto
                                         f.costcenter_id,--para las regiones
                                         f.lbc_date,--- fecha del u
                                         f.bank_id,
                                         f.bankname,
                                         f.cssocialsecno,
                                         f.cclname,
                                         f.ccfname,
                                         f.bankaccno,
                                         f.valid_thru_date,
                                         desc_error);
          end if; ---if  f.bankaccno

          ---Cuenta Bancaria Diferente a nulo o  cero
          if length(f.bankaccno) > 0 then
            for chars in  1 .. length(f.bankaccno) loop
               temp_char := substr(f.bankaccno,chars,1);
               if ascii(temp_char) not between 48 and 57 then
                 desc_error := 'Caracter no num�rico en cuenta bancaria/TC';
           
                      insert into rep_bscs_incons_fp values
                                         (f.customer_id,
                                         f.custcode,
                                         f.cstype,
                                         f.prgcode,---para los  productos
                                         f.prgname,--nombre del producto
                                         f.costcenter_id,--para las regiones
                                         f.lbc_date,--- fecha del u
                                         f.bank_id,
                                         f.bankname,
                                         f.cssocialsecno,
                                         f.cclname,
                                         f.ccfname,
                                         f.bankaccno,
                                         f.valid_thru_date,
                                         desc_error);
                 end if; --if de ascii(temp_char)
              end loop;--de chars
           elsif to_number(f.bankaccno) = 0 then
              null;
            end if;---if de length
            
        /* if f.payment_type = -2 and
           (f.accountowner not in ('A','C') or f.accountowner is null) then 
            desc_error := 'Cuenta Bancaria no tiene especificado tipo (corriente o ahorros). '||
                'Campo accountowner en tabla payment_type';
            RAISE ERROR_DATOS_CARGA;
         end if;*/
            
        if f.payment_type in (-2,-3) and f.cssocialsecno is null then
            desc_error := 'Cliente no tiene c�dula/ruc';
           
                insert into rep_bscs_incons_fp values
                                         (f.customer_id,
                                         f.custcode,
                                         f.cstype,
                                         f.prgcode,---para los  productos
                                         f.prgname,--nombre del producto
                                         f.costcenter_id,--para las regiones
                                         f.lbc_date,--- fecha del u
                                         f.bank_id,
                                         f.bankname,
                                         f.cssocialsecno,
                                         f.cclname,
                                         f.ccfname,
                                         f.bankaccno,
                                         f.valid_thru_date,
                                         desc_error);
        
         end if;--if  de  f.payment_type 
            
        if (f.valid_thru_date is null and f.payment_type = -3) then

             desc_error := 'Fecha validez TC nula';
           
               insert into rep_bscs_incons_fp values
                                         (f.customer_id,
                                         f.custcode,
                                         f.cstype,
                                         f.prgcode,---para los  productos
                                         f.prgname,--nombre del producto
                                         f.costcenter_id,--para las regiones
                                         f.lbc_date,--- fecha del u
                                         f.bank_id,
                                         f.bankname,
                                         f.cssocialsecno,
                                         f.cclname,
                                         f.ccfname,
                                         f.bankaccno,
                                         f.valid_thru_date,
                                         desc_error);
           
        end if;---if de valid_thru_date

   commit;      
        
  end if; --if de payment_type
end loop; ---loop de formas_pago
end p_rep_bscs_incons_fp;
/

