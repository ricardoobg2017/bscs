CREATE OR REPLACE procedure GENERACION_PAGOS_BSCS_PTH is
begin
  

--=====================================================================================--
-- FECHA_CREACI�N : 14/04/2005
-- ELABORADO POR  : Anl. Wendy Burgos Loy
-- OBJETIVO       : Generar los pagos en l�neas de Caja a trav�s de Apis para
--                  la inserci�n y actualizaci�n de las tablas de BSCS
-- PROYECTO       : BSCS - Facturaci�n
-- VERSION        : 1.0.0
--=====================================================================================--



Begin
  Declare
   ln_valor        number          := 0        ;
   ln_saldo        number          := 0        ;
   ln_saldo_fin    number                      ;
   ln_pago_cred    number          := 0        ;
   ln_pago         number                      ;
   ln_frpago       number                      ;
   ln_frmpag_bscs  number                      ;
   pago_temp       number          := 0        ;
   lo_cms_error    varchar2(500)               ;
   lv_bakcode      varchar2(20)                ;
   lv_bankname     varchar2(58)                ;
   lv_esta         number                      ;
   lv_estado_mora  number                      ;
   lv_cuenta       varchar2(24)                ;
   lv_pay_mode     varchar2(1)                 ;
   lv_user         varchar2(1)                 ;
   lv_usuario      varchar2(20)                ;
   lv_docfactura   varchar2(10)    := 'CE2IN'  ;
   lv_docnotcred   varchar2(10)    := 'CE2CO'  ;
   lv_factura      varchar2(30)                ;
   lv_recibo       varchar2(30)                ;
   lo_cms_error    varchar2(3000)              ;
   rt_pid		       varchar2(10)                ;
   pv_Error        varchar2(500)               ;
   lv_Tick_Number  varchar2(50)                ;
   ld_fecha        varchar2(15)                ;
   salida_error    exception                   ;
   lv_cuenta_ant   varchar2(24)                ;
   lv_estado       varchar2(2)                 ;

   cursor pagos_fin is
     select cuenta, valor, to_char(fecha,'dd/mm/yyyy') fecha, forma_pago,factura,cia_pago
     from   pa_registro_pagos_dat
     where  estado_envio = '1'     
     order by fecha;
 

     cursor saldos(pv_cuenta varchar2) is
     select a.ohentdate fecha, b.custcode cuenta, a.ohopnamt_doc saldo, a.ohrefnum factura
     from orderhdr_all a, customer_all b
     where a.customer_id = b.customer_id 
     and a.ohopnamt_doc > 0
     and b.custcode = pv_cuenta
     order by ohentdate;   
   
   saldos_rec  saldos%rowtype;

Begin

  For j in pagos_fin loop
      lv_cuenta := j.cuenta;
      ln_pago   := j.valor;
      ln_frpago := j.forma_pago;
      lv_recibo := j.factura;
      lv_user   := j.cia_pago;
      ld_fecha  := j.fecha;

      begin
          select bank_bscs
          into   ln_frmpag_bscs
          from   mapeo_fr_pago
          where  bank_cbill = ln_frpago;
      exception
       when others then
            Pv_Error:= 'Error al consultar la forma de pago mapeada..'||ln_frpago||sqlerrm;
     end;

     begin
         select bankcode,bankname
         into   lv_bakcode,lv_bankname
         from   bank_all
         where  bank_id = ln_frmpag_bscs;
     exception
         when others then
              Pv_Error:= 'Error al consultar la descripci�n de la forma de pago..'||ln_frmpag_bscs||sqlerrm;
     end;


     If ln_frmpag_bscs in (33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,
                        54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77) then
        lv_pay_mode:='I';
     end if;

     If ln_frmpag_bscs in (-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16,-17,-18,-19,-20,-21,-22,-23,-24) then
        lv_pay_mode:='R';
     end if;

     If ln_frmpag_bscs in (3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32) then
        lv_pay_mode:='D';
     end if;

     If ln_frmpag_bscs in (1,2) then
        lv_pay_mode:='P';
     end if;

     If lv_user = '1'  then
        lv_usuario:= 'CAJAUIO';
     end if;

     If lv_user = '2'  then
        lv_usuario:= 'CAJAGYE';
     end if;

     If lv_user = '3'  then
        lv_usuario:= 'CAJACUE';
     end if;
  
     If lv_user = '8'  then
        lv_usuario:= 'SERVI';
     end if;

    -- Env�o de pagos sin facturas
    open saldos(lv_cuenta);
    fetch saldos into saldos_rec;
    If saldos%notfound then
       close saldos;

      -- El n�mero de factura aqu� no se env�a cuando se genera un CE2CO porque no hace referencia a ninguna factura---------
      If Graba_Pagos (lv_cuenta, ln_pago, ld_fecha, ld_fecha, lv_pay_mode, null, 'Pago a favor del cliente '||lv_recibo, lv_bakcode,lv_bankname, lv_usuario, lv_docnotcred) <> 0 THEN

        insert into bscs_bitacora_pagos
        values (lv_cuenta, ln_valor, sysdate, ln_frpago, lv_user, pv_error);
        lv_estado:= '9';       
        commit;
        
      Else
        lv_estado:= '3';
               
      End if;
        Begin
           update pa_registro_pagos_dat
           set    estado_envio = lv_estado
           where  cuenta  = lv_cuenta
           and    estado_envio = '1'
           and    factura = lv_recibo;
           commit;
        exception
           when others then
                pv_error:= 'No se actualiz� correctamente el estado de correcto de la cuenta'||sqlerrm;
        end;
        commit;
    Else
     close saldos;
     For i in saldos(lv_cuenta) loop
         ln_valor   := i.saldo;
         lv_factura := i.factura;

         If ln_pago <= ln_valor then
            pago_temp:=ln_pago;
            ln_pago  :=0;
         else
            pago_temp:=ln_valor;
            ln_pago  :=ln_pago-ln_valor;
         End if;
         Begin

         if pago_temp > 0 then
    	      If Graba_Pagos(lv_cuenta, pago_temp, ld_fecha, ld_fecha, lv_pay_mode, lv_factura, 'Pago de factura del cliente '||lv_recibo, lv_bakcode,lv_bankname, lv_usuario,lv_docfactura) <> 0 THEN

               lv_estado := '9';              
               insert into bscs_bitacora_pagos values(lv_cuenta, ln_valor, sysdate, ln_frpago, lv_user, pv_error);                     
               begin
                  update pa_registro_pagos_dat
                  set    estado_envio = lv_estado
                  where  cuenta  = lv_cuenta
                  and    estado_envio = '1'
                  and    factura = lv_recibo;
               exception
                  when others then
                     pv_error:= 'No se actualiz� correctamente el estado de incorrecto de la cuenta'||sqlerrm;
               end;              
               commit;
            End if;
         end if;
        
         If ln_pago <= 0 then   
            lv_estado := '3'; 
            begin
                  update pa_registro_pagos_dat
                  set    estado_envio = lv_estado
                  where  cuenta  = lv_cuenta
                  and    estado_envio = '1'
                  and    factura = lv_recibo;
            exception
                  when others then
                     pv_error:= 'No se actualiz� correctamente el estado de incorrecto de la cuenta'||sqlerrm;
            end;              
            commit;
            exit;
         end if;         
        end; 
     end loop;

     If ln_pago>0 then
        Begin        
        -- El n�mero de factura aqu� no se env�a cuando se genera un CE2CO porque no hace referencia a ninguna factura---------
    	  If Graba_Pagos (lv_cuenta, ln_pago, ld_fecha, ld_fecha, lv_pay_mode, null, 'Pago a favor del cliente '||lv_recibo, lv_bakcode,lv_bankname, lv_usuario, lv_docnotcred) <> 0 THEN
        
           insert into bscs_bitacora_pagos values (lv_cuenta, ln_valor, ld_fecha, ln_frpago, lv_user, pv_error);
           lv_estado:= '9';           
           commit;
        else
           lv_estado:= '3';
        end if;
            begin
                update pa_registro_pagos_dat
                set    estado_envio = lv_estado
                where  cuenta  = lv_cuenta
                and    estado_envio = '1'
                and    factura = lv_recibo;
            exception
               when others then
                pv_error:= 'No se actualiz� correctamente el estado de incorrecto de la cuenta'||sqlerrm;
            end;
            commit;      
        End;
     End if;
  /*Begin
      update PA_REGISTRO_PAGOS_DAT
      set    estado_envio = '3'
      where  cuenta  = lv_cuenta
      and    estado_envio in ('1','2')
      and    factura = lv_recibo;
      commit;
      exception
         when others then
         pv_error:= 'No se actualiz� correctamente el estado de correcto de la cuenta'||sqlerrm;
   End;*/
    --end;
   End if;
  End loop;

COMMIT;
End;
End;
end GENERACION_PAGOS_BSCS_PTH;
/

