CREATE OR REPLACE procedure carga_datos_saldos_ant (p_anio in varchar2,
                                                                                   p_mes in varchar2,
                                                                                   p_salida out varchar2) is

cursor c_pagos_tmp is
  select a.cuenpago cuenta,
            sum(a.valopago) pago
  from  ec_pagos_tmp a
  where a.aniopago = p_anio
  and     a.mespago = p_mes
  group by a.cuenpago;
  
cursor c_consumos_rest is
  select c.cuencons cuenta
  from  ec_consumos c
  where c.reca_cons = 0
  and     c.fech_reca_cons is null
  and     c.aniocons = p_anio
  and     c.mescons = p_mes;
/*  minus
  select d.cuenpago cuenta_pago
  from   ec_pagos_tmp d;
*/  

---Definición de variables
lc_pagos                c_pagos_tmp%rowtype;
lc_consumos_rest   c_consumos_rest%rowtype;
lb_notfound        boolean;
lv_error              varchar2(100);
le_mierror          exception;
saldo_rest          number;
consumo            number;
consumo_ant      number;


begin
p_salida := 'Proceso exitoso';

  open c_pagos_tmp;
  fetch c_pagos_tmp into lc_pagos;
  lb_notfound := c_pagos_tmp%notfound;
  close c_pagos_tmp;
  
 if lb_notfound then
     lv_error := 'No existen datos de pagos en tabla ec_pagos_tmp';
     raise le_mierror;
  else   
    for fila in c_pagos_tmp loop 
      begin
        select sum(a.ecuacons)
        into    consumo
        from   ec_consumos a
        where a.aniocons = p_anio
        and     a.mescons = p_mes
        and     a.cuencons = fila.cuenta
        group by cuencons;
        
        exception
        when no_data_found then
           null;
        end;
        
        if consumo is null then
           null;
        else
        if consumo <= fila.pago then
           update ec_consumos b
           set   b.reca_cons = 1,
                   b.fech_reca_cons = sysdate
           where b.cuencons = fila.cuenta
           and     b.aniocons = p_anio
           and     b.mescons = p_mes;
           commit;
       else
          saldo_rest := consumo - fila.pago;
          insert into ec_datos_saldos(cuenrep,consrep,pagorep,saldrep)
          values(fila.cuenta,consumo,fila.pago,saldo_rest);
          commit;
       end if;
      end if;
    end loop;
  end if;
  
  open c_consumos_rest;
  fetch c_consumos_rest into lc_consumos_rest;
  lb_notfound := c_consumos_rest%notfound;
  close c_consumos_rest;
  
 if lb_notfound then
     lv_error := 'No existen datos de consumos';
     raise le_mierror;
  else   
    for j in c_consumos_rest loop
       select sum(a.ecuacons)
        into    consumo_ant
        from   ec_consumos a
        where a.aniocons = p_anio
        and     a.mescons = p_mes
        and     a.cuencons = j.cuenta
        and     a.reca_cons = 0
        and     a.fech_reca_cons is null
        group by cuencons;
        
        begin
        insert into ec_datos_saldos(cuenrep,consrep,pagorep,saldrep)
        values(j.cuenta,consumo,0,consumo);
        commit;
        
        exception
        when others then
           dbms_output.put_line('Error al insertar datos en ec_datos_saldos de cuenta: '||j.cuenta);
        end;
    end loop;
 end if;      
  
  
  exception
  when le_mierror then
    p_salida := lv_error;
    
    
end carga_datos_saldos_ant;
/

