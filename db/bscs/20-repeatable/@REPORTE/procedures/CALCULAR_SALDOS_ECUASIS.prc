CREATE OR REPLACE PROCEDURE CALCULAR_SALDOS_ECUASIS
                            (P_ANIO number,
                             P_MES number,
                             P_ORIGEN number) IS


--------------------------------------------------                             
----Para el parametro de entrada P_ORIGEN
----El valor 1 corresponde a GYE (BSCS)
----El valor 2 corresponde a UIO (BSCS)
--------------------------------------------------


CURSOR inicio (C_ANIO_ANT number, C_MES_ANT number) is 
       SELECT  CuenCons,
                    AnioCons,
                    MesCons,
                    (TotaCons-EcuaCons) Conecel,
                    EcuaCons,
                    CredCons,
                    CiclCons
       FROM    Ec_Consumos
       WHERE  AnioCons  = P_ANIO
       AND      MesCons   = P_MES
       AND      NumeOrig  = P_ORIGEN
       UNION 
       SELECT CuenSald,
                   P_ANIO,
                   P_MES,
                   0,
                   0,
                   0,
                   CiclSald
       FROM   Ec_Saldos
       WHERE AnioSald =  C_ANIO_ANT
       AND     MesSald  =  C_MES_ANT
       AND     NumeOrig  = P_ORIGEN
       AND     CuenSald in (  SELECT a1.CuenSald
                          	            FROM   Ec_Saldos a1
              	                        WHERE AnioSald  = C_ANIO_ANT
              	                        AND     MesSald   = C_MES_ANT 
                                        AND     NumeOrig  = P_ORIGEN
                                        MINUS
                                        SELECT CuenCons
                                        FROM   Ec_Consumos
              	                        WHERE AnioCons  = P_ANIO
              	                        AND     MesCons   = P_MES 
                                        AND     NumeOrig  = P_ORIGEN)
       ORDER BY 1;


------Definici�n de variables
   pago         number(12,2);
   credito      number(12,2);
   saldo_co     number(12,2);
   saldo_ec     number(12,2);
   saldo_cl     number(12,2);
   saldo_co_ant number(12,2);
   saldo_ec_ant number(12,2);
   saldo_cl_ant number(12,2);
   saldo_co_aux number(12,2);
   saldo_ec_aux number(12,2);
   saldo_cl_aux number(12,2);
   ecua_tot     number(12,2);
   conta        number(2);
   anio_ant     number(4);
   anio_sig     number(4);
   mes_ant      number(2);
   mes_sig      number(2);
   diferencia   number(12,2);
   abono_ec     number(12,2);
   abono_cl     number(12,2);
   
BEGIN
/*  lo nuevo  JGO  febrero 21 2003
 *  para obtener mes siguiente , actual y anterior igual con el a�o.
 *  inicio  
 */
   --
   -- Para obtener mes y anio anterior
   --
   if  p_mes = 1 then
       anio_ant := p_anio - 1;
       mes_ant := 12;
   else
       anio_ant := p_anio;
       mes_ant := p_mes - 1;
   end if;
   --
   -- Para obtener mes y anio siguiente
   --
   if p_mes = 12 then
      anio_sig := p_anio + 1;
      mes_sig  := 1;
   else
      anio_sig := p_anio;
      mes_sig  := p_mes + 1;
   end if;
   --
   saldo_co_aux := 0; 
   --
/* fin */   

   --   OPEN inicio ;
   FOR fila in inicio(anio_ant, mes_ant) LOOP
     --
     /***  aumentado febrero 21 2003  JGO
      *    inicio
      *    porque esas variables auxiliares presentan basura si se las usa directamente
      *    en operaciones como suma o resta en cambio 
      *    cuando son usadas para asignacion no importa porque adopta el nuevo valor
      */
     saldo_co_aux:= 0;
     saldo_ec_aux:= 0;
     saldo_cl_aux:= 0;
     saldo_co_ant:= 0;
     saldo_ec_ant:= 0;
     saldo_cl_ant:= 0;     
     saldo_cl := 0;
	   saldo_co := 0;
	   abono_cl := 0;
     pago     := 0;
     credito  := 0;
     abono_ec := 0;
     abono_cl := 0;
     /*  fin febrero 21 2003  JGO */
   
      conta := 0; 
      SELECT count(*)
      INTO   conta
      FROM   Ec_Saldos
      WHERE  CuenSald = fila.CuenCons
      AND    AnioSald = anio_ant
      AND    MesSald = mes_ant
      AND    NumeOrig  = P_ORIGEN;
      --
      IF conta > 0 THEN   --Chequear si tiene saldo el mes anterior
                                    --Recuperar los saldos del mes anterior
              SELECT ActuConeSald, ActuEcuaSald, ActuColoSald 
              INTO   saldo_co_ant, saldo_ec_ant, saldo_cl_ant
              FROM   Ec_Saldos
              WHERE  CuenSald = fila.CuenCons
      	      AND    AnioSald = anio_ant
      	      AND    MesSald = mes_ant
              AND    NumeOrig  = P_ORIGEN;
      ELSE
              --  IF saldo_co_ant IS NULL THEN
        	       saldo_co_ant := 0;
                 saldo_ec_ant := 0;
        	       saldo_cl_ant := 0;
      END IF;
      --
      conta := 0;
      SELECT count(*)
      INTO   conta
      FROM   Ec_Pagos
      WHERE  CuenPago = fila.CuenCons
      AND    AnioPago = anio_sig
      AND    MesPago  = mes_sig
      AND    NumeOrig  = P_ORIGEN;
      --
      IF conta > 0 THEN  --Chequear si tiene pagos para el mes siguiente
         --Recuperar la suma de los pagos para la cuenta
               SELECT sum(ValoPago),sum(CredPago)
               INTO   pago,credito
               FROM   Ec_Pagos
               WHERE  CuenPago  = fila.CuenCons
               AND    AnioPago  = anio_sig
               AND    MesPago   = mes_sig
               AND    NumeOrig  = P_ORIGEN;
      ELSE
                 --  IF pago IS NULL THEN
        	       pago := 0;
        	       credito := 0;
      END IF;

      -- Primero se procesan los saldos negativos de ecuasistencia
      IF (saldo_ec_ant < 0) THEN
	       saldo_co_aux := (nvl(saldo_co_ant,0) + nvl(saldo_ec_ant,0));
	       saldo_ec_aux := 0;
         abono_ec := saldo_ec_ant;
      ELSE
	       saldo_co_aux := (nvl(saldo_co_ant,0));
         saldo_ec_aux := (nvl(saldo_ec_ant,0));
	       abono_ec := 0;
      END IF;
      --
      IF (saldo_cl_ant < 0) THEN
	       saldo_co_aux := (nvl(saldo_co_aux,0) + nvl(saldo_cl_ant,0));
	       saldo_cl_aux := 0;
         abono_cl := saldo_cl_ant;
      ELSE
	       saldo_cl_aux := (nvl(saldo_cl_ant,0));
	       abono_cl := 0;
      END IF;

      -- Con el pago se cancela inicialmente los saldos anteriores
      saldo_co_aux := saldo_co_aux - nvl(pago,0);
      IF ((saldo_co_aux < 0) AND (saldo_ec_aux > 0)) THEN
	       IF (-saldo_co_aux > saldo_ec_aux) THEN
	          saldo_co_aux := saldo_co_aux + saldo_ec_aux;
	          abono_ec := abono_ec + saldo_ec_aux;
	          saldo_ec_aux := 0;
         ELSE
	          abono_ec := abono_ec - saldo_co_aux;
	          saldo_ec_aux := saldo_co_aux + saldo_ec_aux;
	          saldo_co_aux := 0;
         END IF;
      END IF;
      --
      IF ((saldo_co_aux < 0) AND (saldo_cl_aux > 0)) THEN
	       IF (-saldo_co_aux > saldo_cl_aux) THEN
            saldo_co_aux := saldo_co_aux + saldo_cl_aux;
	          abono_cl := abono_cl + saldo_cl_aux;
	          saldo_cl_aux := 0;
	       ELSE
	          abono_cl := abono_cl - saldo_co_aux;
	          saldo_cl_aux := saldo_co_aux + saldo_cl_aux;
	          saldo_co_aux := 0;
	       END IF;
      END IF;

      -- Se calculan los nuevos saldos acorde a la facturacion del mes actual
      saldo_co := (saldo_co_aux + nvl(fila.conecel,0) - nvl(credito,0));
      IF ((saldo_co < 0) AND (saldo_ec_aux > 0) ) THEN
	       diferencia := saldo_co + nvl(credito,0);
	       if(diferencia <0) then
            IF ((-diferencia) < saldo_ec_aux) THEN
	             saldo_ec := diferencia + saldo_ec_aux;
	             saldo_co := saldo_co - diferencia;
	             abono_ec := abono_ec - diferencia;
            ELSE
	             saldo_ec := 0;
	             saldo_co := saldo_co + saldo_ec_aux;
	             abono_ec := abono_ec + saldo_ec_aux;
            END IF;
         else
	          saldo_ec := saldo_ec_aux;
	       end if;
      ELSE
	       saldo_ec := saldo_ec_aux;
      END IF; 

      ecua_tot := (nvl(fila.EcuaCons,0) + nvl(fila.CredCons,0));
      IF ((saldo_co < 0) AND (ecua_tot + saldo_cl_aux > 0) ) THEN
	       diferencia := saldo_co + nvl(credito,0);
	       if(diferencia <0) then
            IF ((-diferencia) < ecua_tot + saldo_cl_aux) THEN
	             saldo_cl := ecua_tot + diferencia + saldo_cl_aux;
	             saldo_co := saldo_co - diferencia;
	             abono_cl := abono_cl - diferencia;
            ELSE
	             saldo_cl := 0;
	             saldo_co := saldo_co + ecua_tot + saldo_cl_aux;
	             abono_cl := abono_cl + ecua_tot + saldo_cl_aux;
            END IF;
         else
	          saldo_cl := saldo_cl_aux + ecua_tot;
	       end if;
      ELSE
	       saldo_cl := saldo_cl_aux + ecua_tot;
      END IF; 

      begin
         INSERT INTO Ec_Saldos 
            (CuenSald,AnioSald,MesSald,CiclSald,FactConeSald, FactEcuaSald, FactCredSald,
	           PagoSald, CredSald, AnteConeSald, AnteEcuaSald, ActuConeSald, ActuEcuaSald,
	           AnteColoSald, ActuColoSald, AbonEcuaSald, AbonColoSald, NumeOrig)
         VALUES 
            (fila.CuenCons,fila.AnioCons,fila.MesCons, fila.CiclCons,fila.conecel, fila.EcuaCons,
             fila.CredCons, pago, credito, saldo_co_ant, saldo_ec_ant, saldo_co,saldo_ec,
	           saldo_cl_ant, saldo_cl, abono_ec, abono_cl, P_ORIGEN );
      exception
         when dup_val_on_index then
            null;
         when others then
            null;
      end;
      COMMIT;
   END LOOP;
END;
/

