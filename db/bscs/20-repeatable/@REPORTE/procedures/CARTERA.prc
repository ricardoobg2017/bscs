CREATE OR REPLACE procedure CARTERA is

CURSOR CLIENTES_FACTURAS_ACTUAL IS 
SELECT  c.customer_id, f.ohinvamt_doc
FROM payment_all a, 
     paymenttype_all b,
     customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and a.act_used = 'X'
and  f.ohstatus like 'IN'
and  trunc(f.ohentdate) = to_date('24/05/2003', 'dd/mm/yyyy');


CURSOR CLIETES_FACTURAS_DEUDA (c_customer_id number)IS 
SELECT  f.customer_id, f.ohinvamt_doc , f.ohopnamt_doc , f.ohentdate
FROM payment_all a, 
paymenttype_all b,
customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and  trunc(f.ohentdate) < to_date ('24/05/2003','dd/mm/yyyy')
and  f.ohstatus like 'IN'
AND  f.ohopnamt_doc > 0
AND f.customer_id = c_customer_id
ORDER BY  f.ohentdate;


C_CLIENTES_FACTURAS_ACTUAL  CLIENTES_FACTURAS_ACTUAL%rowtype;
C_CLIETES_FACTURAS_DEUDA    CLIETES_FACTURAS_DEUDA%rowtype; 

cnt                  NUMBER;
DEUDA                NUMBER;
num_clientes         NUMBER;
cartera_corriente    NUMBER;
cagos_corrientes     NUMBER;
ndebitos_corrientes  NUMBER;
ncreditos_corrientes NUMBER;


begin
  
  delete from reporte.RPT_CARFACACT;
  delete from REPORTE.RPT_CARCLIDEU;
  commit ;
   open  CLIENTES_FACTURAS_ACTUAL;
   fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   while CLIENTES_FACTURAS_ACTUAL%found loop
       exit  when CLIENTES_FACTURAS_ACTUAL%notfound;
       cnt := cnt +1; 
        INSERT INTO reporte.RPT_CARFACACT VALUES (C_CLIENTES_FACTURAS_ACTUAL.customer_id, 1, C_CLIENTES_FACTURAS_ACTUAL.ohinvamt_doc);
        commit;
        fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   end loop;
   close CLIENTES_FACTURAS_ACTUAL;


   open  CLIENTES_FACTURAS_ACTUAL;
   fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   while CLIENTES_FACTURAS_ACTUAL%found loop
       exit  when CLIENTES_FACTURAS_ACTUAL%notfound;
           open  CLIETES_FACTURAS_DEUDA(C_CLIENTES_FACTURAS_ACTUAL.customer_id);
           fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
           while CLIETES_FACTURAS_DEUDA%found loop
                exit  when CLIETES_FACTURAS_DEUDA%notfound;
                     IF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc > 0 THEN
                        DEUDA :=  C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc;
                     ELSIF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc - C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc = 0 THEN
                        DEUDA := C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc;
                     END IF;
                     INSERT INTO REPORTE.RPT_CARCLIDEU VALUES (C_CLIETES_FACTURAS_DEUDA.customer_id, DEUDA , C_CLIETES_FACTURAS_DEUDA.ohentdate);
                     commit;
           fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
           end loop;
           close CLIETES_FACTURAS_DEUDA;
        fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   end loop;
   close CLIENTES_FACTURAS_ACTUAL;


  /* SELECT COUNT(customer_id) into num_clientes FROM reporte.rpt_carfacact a
   WHERE  a.customer_id not in (select customer_id from reporte.rpt_carclideu)  */

    SELECT Count(f.customer_id)  ,sum( f.ohinvamt_doc) 
    into num_clientes , cartera_corriente
    FROM reporte.rpt_carfacact a, orderhdr_all f
    WHERE  trunc(f.ohentdate) = to_date ('24/05/2003','dd/mm/yyyy')
    and f.customer_id = a.customer_id
    and a.customer_id not in (select customer_id from reporte.rpt_carclideu);
    
    -- PAGOS CORRIENTES
    SELECT   sum(g.cacuramt_pay) 
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , 
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and g.customer_id  not in (select customer_id from reporte.rpt_carclideu);

    -- NOTAS DE DEBITOS CORRIENTES

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and a.customer_id  not in (select customer_id from reporte.rpt_carclideu);

    -- NOTAS DE CREDITOS CORRIENTES
    SELECT  
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and a.customer_id  not in (select customer_id from reporte.rpt_carclideu);

    
    ----------------------------------------------------------------------------------
    
    
    
    
   
  
  
  
  
end CARTERA;
/

