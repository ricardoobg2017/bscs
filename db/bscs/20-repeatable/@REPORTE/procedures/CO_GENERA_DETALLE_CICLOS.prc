CREATE OR REPLACE PROCEDURE co_genera_detalle_ciclos( pd_compania in number,
                                                      pd_lvMensErr out varchar2) is

-- CBR 28 Diciembre 2005
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnTotPago      number;
    lnTotCred      number;
    lnValor        number;
    lvPeriodos     varchar2(2000);

cursor c_migrados is 
select a.id_cliente, a.totaladeuda 
from sysadm.co_repcarcli_24112005_norecla a, co_repcarcli_08122005 b
where a.id_cliente = b.id_cliente
and a.cuenta is not null
and b.cuenta is not null;

cursor c_ciclo1 is
select cuenta, id_cliente, totaladeuda, compania, mayorvencido from sysadm.co_repcarcli_24112005_norecla where id_cliente in(
select id_cliente from sysadm.co_repcarcli_24112005_norecla where cuenta is not null
minus
select id_cliente from co_repcarcli_08122005 where cuenta is not null);

cursor c_ciclo2 is
select cuenta, id_cliente, totaladeuda, compania, mayorvencido from co_repcarcli_08122005 where id_cliente in(
select id_cliente from co_repcarcli_08122005 where cuenta is not null
minus
select id_cliente from sysadm.co_repcarcli_24112005_norecla where cuenta is not null
);
         
cursor c_pagos is         
select /*+ rule */ a.cachkdate, a.customer_id, sum(cachkamt_pay) as TotPago
from  cashreceipts_all a, co_detalle_ciclos b
where a.customer_id = b.customer_id
and a.cachkdate >= to_date('24/11/2005', 'dd/mm/yyyy')
and a.cachkdate < to_date('01/01/2006', 'dd/mm/yyyy')
group by a.cachkdate, a.customer_id
having sum(a.cachkamt_pay) <> 0;
  
cursor c_pagos_corte2 is         
select /*+ rule */ a.customer_id, sum(cachkamt_pay) as TotPago
from  cashreceipts_all a, co_repcarcli_08122005 b
where a.customer_id = b.id_cliente
and b.cuenta is not null
and a.cachkdate >= to_date('08/12/2005', 'dd/mm/yyyy')
and a.cachkdate < to_date('01/01/2006', 'dd/mm/yyyy')
group by a.customer_id
having sum(a.cachkamt_pay) <> 0;
  
  
  BEGIN

    ---------------------------
    --  CLIENTES MIGRADOS
    ---------------------------
    --execute immediate('truncate table sysadm.co_detalle_ciclos');
    --delete from co_detalle_ciclos where compania = pd_compania;
    --commit;
    insert into co_detalle_ciclos(custcode, customer_id, compania, total_deuda, mayorvencido, total_deuda2, mayorvencido2, migro)
    select a.cuenta, a.id_cliente, a.compania, a.totaladeuda, a.mayorvencido, b.totaladeuda, b.mayorvencido, 1
    from sysadm.co_repcarcli_24112005_norecla a, co_repcarcli_08122005 b
    where a.id_cliente = b.id_cliente
    and a.cuenta is not null
    and b.cuenta is not null;
    commit;
    
    ---------------------------
    -- clientes de ciclo 1
    ---------------------------
    lII := 0;
    for i in c_ciclo1 loop
        insert into co_detalle_ciclos(custcode, customer_id, compania, total_deuda, mayorvencido)
        values (i.cuenta, i.id_cliente, i.compania, i.totaladeuda, i.mayorvencido);
        lII := lII + 1;
        if lII = 500 then
           commit;
           lII := 0;
        end if;
    end loop;
    commit;
    
    ---------------------------
    -- clientes de ciclo 2
    ---------------------------
    lII := 0;
    for i in c_ciclo2 loop
        insert into co_detalle_ciclos(custcode, customer_id, compania, total_deuda2, mayorvencido2)
        values (i.cuenta, i.id_cliente, i.compania, i.totaladeuda, i.mayorvencido);
        lII := lII + 1;
        if lII = 500 then
           commit;
           lII := 0;
        end if;
    end loop;
    commit;    

    ---------------------------
    --        PAGOS 1 2 3
    ---------------------------
    lII := 0;
    for i in c_pagos loop
        if i.cachkdate >= to_date('24/11/2005', 'dd/mm/yyyy') and i.cachkdate < to_date('01/12/2005', 'dd/mm/yyyy') then
            update co_detalle_ciclos set pagos1 = pagos1 + i.totpago where customer_id = i.customer_id;
        elsif i.cachkdate >= to_date('01/12/2005', 'dd/mm/yyyy') and i.cachkdate < to_date('08/12/2005', 'dd/mm/yyyy') then
            update co_detalle_ciclos set pagos2 = pagos2 + i.totpago where customer_id = i.customer_id;
        elsif i.cachkdate >= to_date('08/12/2005', 'dd/mm/yyyy') and i.cachkdate < to_date('01/01/2006', 'dd/mm/yyyy') then
            update co_detalle_ciclos set pagos3 = pagos3 + i.totpago where customer_id = i.customer_id;
        end if;
        lII := lII + 1;
        if lII = 500 then
           commit;
           lII := 0;
        end if;        
    end loop
    commit;

    ---------------------------
    --        PAGOS 4
    ---------------------------    
    lII := 0;
    for i in c_pagos_corte2 loop
        update co_detalle_ciclos set pagos4 = pagos4 + i.totpago where customer_id = i.customer_id;
        lII := lII + 1;
        if lII = 500 then
           commit;
           lII := 0;
        end if;        
    end loop
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'co_detalle_ciclos: ' || sqlerrm;
    
  END;
/

