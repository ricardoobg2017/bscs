CREATE OR REPLACE procedure PLANTILLA_BSCS (PD_FECHA IN DATE) is
   CURSOR DATOS IS 
      SELECT DISTINCT PRODUCTO, LOCALIDAD
      FROM PLANTILLA;
      
   CURSOR VALORES (CV_PROD IN VARCHAR2, CV_LOCAL IN VARCHAR2) IS
      select /*+ rule */ 
      h.producto, h.cuenta, b.sncode, H.SIGNO,
      sum(otamt_revenue_gl) as Valor, sum(nvl(A.OTAMT_DISC_DOC,0)) AS Descuento,
      b.des 
      from 
      ordertrailer A, -- items de la factura
      mpusntab b,     -- maestro de servicios
      orderhdr_all c, -- cabecera de facturas
      customer_all d, -- maestro de cliente
      plantilla h,    -- plantilla de cuentas
      COSTCENTER j    -- centros de costos 1: gye 2: uio
      where
      a.otxact=c.ohxact and 
      c.ohentdate=to_date(PD_FECHA,'DD/MM/YYYY') and
      C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
      c.customer_id=d.customer_id and 
      C.CUSTOMER_ID > 0 AND
      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
      instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
      instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=to_char(b.sncode) and
      b.sncode = to_number(h.servicio) and
      h.cuenta = a.otglsale AND  
      j.cost_id=d.costcenter_id and
      d.costcenter_id = 1 AND--CV_LOCAL and
      d.prgcode in (1,2)--(CV_PROD)
      group by h.producto, h.cuenta, b.sncode, h.signo, b.des
      UNION 
      SELECT /*+ rule */ 
      H.PRODUCTO, 
      A.glacode as Cble, 1, H.SIGNO,
      sum(TAXAMT_TAX_CURR) AS VALOR, 0 as Descuento, 
      TO_CHAR(H.SERVICIO)
      FROM 
      ordertax_items A,        -- items de impuestos relacionados a la factura
      sysadm.TAX_CATEGORY B,   -- maestro de tipos de impuestos
      orderhdr_all C,          -- cabecera de facturas
      customer_all d,          -- maestro de cliente
      COSTCENTER J,            -- centros de costos 1: gye 2: uio
      PLANTILLA H              -- plantilla de cuentas
      where 
      c.ohxact=a.otxact and
      c.ohentdate=to_date('24/04/2004','DD/MM/YYYY') and
      C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
      c.customer_id=d.customer_id and 
      C.CUSTOMER_ID > 0 AND
      taxamt_tax_curr>0 and
      A.TAXCAT_ID=B.TAXCAT_ID and
      H.CUENTA = A.GLACODE AND
      j.cost_id=d.costcenter_id and
      d.costcenter_id = 1 and
      d.prgcode in (1,2)  
      group by H.PRODUCTO, A.glacode, 1,  H.SIGNO, TO_CHAR(H.SERVICIO);
      
   LV_PROD   VARCHAR2(20);
   LV_CUENTA VARCHAR2(20);
   LV_LOCAL  NUMBER;
   LN_VALO   NUMBER;
   LN_VALO1  NUMBER;
   LN_DESC   NUMBER := 0;
   LN_TOTA   NUMBER := 0;
   ln_cont   number := 1;
   lV_fecha  varchar2(15) := to_char(pd_fecha,'YYYYDDMM');
   LV_PERI   VARCHAR2(7)  := TO_CHAR(PD_FECHA,'YYYY')||'|'||TO_CHAR(PD_FECHA,'MM');
   lv_linea  VARCHAR2(2000); 
   LV_SIGNO  VARCHAR2(1);
   lv_unid   varchar2(15);
   lv_produ  varchar2(15);
   
   --1000 IVA, 2000 ICE, 3000 TOTAL, 4000 DESCUENTOS
begin
   FOR I IN DATOS LOOP
       IF I.PRODUCTO = 'AUTOC' THEN
          lv_produ := '000002';
          LV_PROD := '1,2';
       ELSE 
          lv_produ := '000002';
          LV_PROD := '3,4';
       END IF;
       IF I.LOCALIDAD = 'GYE' THEN
          LV_LOCAL := 1;
          lv_unid := 'GYE_999';
       ELSE
          LV_LOCAL := 2;
          lv_unid := 'UIO_999';
       END IF;
       FOR J IN VALORES (LV_PROD, LV_LOCAL) LOOP           
           LN_DESC := J.DESCUENTO + LN_DESC;
           LN_VALO := J.VALOR - J.DESCUENTO;
           ln_valo1 := J.VALOR;
           LN_TOTA := LN_VALO + LN_TOTA;
           if J.signo = 'C' then
              LN_VALO1 := LN_VALO1 * -1;
           end if;
           IF LN_TOTA <> 0 THEN
              lv_linea := I.LOCALIDAD||'|'||'CONEC'||'|B'||lV_fecha||'|'||ln_cont||'|REAL|LOCAL|'||LV_FECHA||'|BSCS|CONEC|'||LV_PERI||'|NEXT|'||LV_FECHA||'|'||LN_CONT||'|'||
                          LTRIM(J.cuenta,'0')||'|'||lv_unid||'|'||lv_produ||'|USD|USD|CRRNT|1|1|'||ln_VALO1||'|'||LN_VALO1||'|BSCS|'||LV_FECHA||'| |N';
              dbms_output.put_line(lv_linea);
              ln_cont := ln_cont + 1;
           END IF;                                 
       END LOOP;
       --TOTAL 
         BEGIN
            SELECT H.CUENTA, H.SIGNO
            INTO   LV_CUENTA, LV_SIGNO            
            FROM  PLANTILLA H
            WHERE H.PRODUCTO  = I.PRODUCTO
            AND   H.LOCALIDAD = I.LOCALIDAD
            AND   H.SERVICIO  = 3000;
         EXCEPTION
            WHEN OTHERS THEN
              LV_CUENTA := 'ERROR';
         END;
         IF LV_SIGNO = 'C' then
            ln_TOTA := ln_TOTA * -1;
         END IF;
         lv_linea := I.LOCALIDAD||'|'||'CONEC'||'|B'||lV_fecha||'|'||ln_cont||'|REAL|LOCAL|'||LV_FECHA||'|BSCS|CONEC|'||LV_PERI||'|NEXT|'||LV_FECHA||'|'||LN_CONT||'|'||
                     LTRIM(LV_CUENTA,'0')||'|'||lv_unid||'|'||lv_produ||'|USD|USD|CRRNT|1|1|'||ln_TOTA||'|'||LN_TOTA||'|BSCS|'||LV_FECHA||'| |N';
         dbms_output.put_line(lv_linea);
         ln_cont := ln_cont + 1;
       --DESCUENTO
         BEGIN
            SELECT H.CUENTA, H.SIGNO
            INTO   LV_CUENTA, LV_SIGNO            
            FROM  PLANTILLA H
            WHERE H.PRODUCTO  = I.PRODUCTO
            AND   H.LOCALIDAD = I.LOCALIDAD
            AND   H.SERVICIO  = 4000;
         EXCEPTION
            WHEN OTHERS THEN
              LV_CUENTA := 'ERROR';
         END;
         IF LV_SIGNO = 'C' then
            ln_TOTA := ln_TOTA * -1;
         END IF;
         lv_linea := I.LOCALIDAD||'|'||'CONEC'||'|B'||lV_fecha||'|'||ln_cont||'|REAL|LOCAL|'||LV_FECHA||'|BSCS|CONEC|'||LV_PERI||'|NEXT|'||LV_FECHA||'|'||LN_CONT||'|'||
                     LTRIM(LV_CUENTA,'0')||'|'||lv_unid||'|'||lv_produ||'|USD|USD|CRRNT|1|1|'||LN_DESC||'|'||LN_DESC||'|BSCS|'||LV_FECHA||'| |N';
         dbms_output.put_line(lv_linea);       
   END LOOP;
end PLANTILLA_BSCS;
/

