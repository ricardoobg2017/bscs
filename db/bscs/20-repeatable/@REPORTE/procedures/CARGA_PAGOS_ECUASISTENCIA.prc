CREATE OR REPLACE procedure CARGA_PAGOS_ECUASISTENCIA
                            (P_COD_RET IN OUT NUMBER, 
                             P_MSG_RET IN OUT VARCHAR2) is
begin
declare

cursor pagos is

select /*+ rule +*/
        a.custcode Cuenta, 
        sum(c.cacuramt_pay) ValoPag, 
        a.costcenter_id NumeOrig,
        to_char(c.cachkdate,'mm') Mes,
        to_char(c.cachkdate,'yyyy') Anio
from    cashreceipts_all c, 
        customer_all a, 
        costcenter s
where   c.customer_id   = a.customer_id
and     a.costcenter_id = s.cost_id
and     trunc(c.cachkdate) between to_date('24/'||07||'/'||2003,'dd/mm/yyyy')
                           and  to_date('23/'||11||'/'||2003,'dd/mm/yyyy')
group by a.custcode, 
         a.costcenter_id,
         to_char(c.cachkdate,'mm'),
         to_char(c.cachkdate,'yyyy');


--------
sentencia     varchar2(2000); 
source_cursor number; 
--------
begin
p_cod_ret := 0;
p_msg_ret := null;

/*sentencia := 'truncate table ec_pagos_tmp reuse storage';
source_cursor := dbms_sql.open_cursor;
dbms_sql.parse(source_cursor,sentencia,2);
dbms_sql.close_cursor(source_cursor);*/

for i in pagos loop
     begin
        insert into ec_pagos_tmp (cuenpago,aniopago,mespago,valopago,credpago,numeorig)
        values (i.cuenta, i.anio,  i.mes, i.valopag, 0,  i.numeorig) ;
     exception
        when DUP_VAL_ON_INDEX then
             dbms_output.put_line('Existe un valor duplicado:'||i.cuenta||','||i.anio||','||i.mes||','||i.valopag||','||0||','||i.numeorig);
        when OTHERS then
             p_cod_ret := 1;
             p_msg_ret := 'Error al insertar los pagos: '||sqlerrm;
             exit;
     end;
     
end loop;
commit;
end;

end CARGA_PAGOS_ECUASISTENCIA;
/

