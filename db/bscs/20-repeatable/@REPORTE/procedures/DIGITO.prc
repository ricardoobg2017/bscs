CREATE OR REPLACE procedure DIGITO(p_pais            in  number,
                                   p_empresa         in  number,
                                   p_producto        in  number,
                                   p_digito_verifica out number) is
begin
 declare
 codigo             number;
 pais               number(3) := p_pais;
 empresa            number(5) := p_empresa;
 producto           number(4) := p_producto;
 num_par            number;
 num_impar          number;
 acum_par           number := 0 ;
 acum_impar         number := 0 ;
 j                  number;
 ln_codigo          number := 0;
 resul_par          number := 0;
 sum_par_imp        number := 0;
 dig_verificador    number := 0;
 dec_inmed          number := 0;
 num_div            number := 0;
begin
  
  ------------------------------------------------------------------------------ 
     --aqu� va a tomar la longitud del n�mero total de d�gitos que se ingresa
  ------------------------------------------------------------------------------ 
   J:= LENGTH(codigo);
   dbms_output.put_line (pais);
   dbms_output.put_line (empresa);
   dbms_output.put_line (producto);
   codigo :=  pais||empresa||producto;  
  ------------------------------------------------------------------------------ 
     --este lazo sirve para recorrer cada d�gito y verificar si en par o impar
                    --para sumar o restar, seg�n la l�gica   
  ------------------------------------------------------------------------------ 
   While j > 0 loop
     ln_codigo:= to_number(SUBSTR(codigo,J,1));
      If j in (2,4,6,8,10,12) then
        num_par:= ln_codigo;
        acum_par:= acum_par + num_par;
      else
        num_impar:= ln_codigo; 
        acum_impar:= acum_impar + num_impar;
     end if; 
     j:= j - 1;
   end loop;
    resul_par:= acum_par * 3;
    sum_par_imp:= resul_par + acum_impar;
    num_div := floor(sum_par_imp / 10);
    dec_inmed := (num_div * 10) + 10;
    dig_verificador := dec_inmed - sum_par_imp;
    p_digito_verifica:= dig_verificador;
end;    
  
end DIGITO;
/

