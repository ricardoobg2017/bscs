create or replace package RPK_PAGO_CORIS is

  -- Author  : C�SAR SEVILLA M
  -- Created : 25/08/2005 14:57:35
  -- Purpose : Paquete genera Reporte Diario y Mensual para Pago de Servicio de Asistencia Inmediata a la Empresa CORIS


  PROCEDURE RPP_GENERA_REP_DIARIO(pv_fecha varchar2, 
                                  pv_error out varchar2);
                                  
  PROCEDURE RPP_GENERA_REP_MENSUAL(pv_fecha varchar2,
                                   pv_error out varchar2);
  
end RPK_PAGO_CORIS;
/
create or replace package body RPK_PAGO_CORIS is
/*
 * Procedimiento que genera un archivo de texto con el reporte diario 
 * requiere de la fecha en mes y a�o con formato mm/yyyy
 * hace uso del paquete dbms_sql para un sql din�mico y 
 * del paquete utl_file para manejo de archivos
 */
  PROCEDURE RPP_GENERA_REP_DIARIO(pv_fecha varchar2,
                                  pv_error out varchar2) is
    li_cursor INTEGER;
    lv_telefono varchar2(20);
    lv_estado varchar2(1);
    ln_num_registros number;
    WFOUT SYS.UTL_FILE.FILE_TYPE;
    lv_archivo VARCHAR2(50);
    lv_linea VARCHAR2(100);
    lb_salir boolean:=FALSE;
    lv_dia VARCHAR2(2);
    lv_mes_anio_fa8 varchar2(12);    
    lv_mes_anio_fa24 varchar2(12);    
    lv_mes_anio_ant8 varchar2(12);    
    lv_mes_anio_ant24 varchar2(12);            
    lv_mes_anio8 varchar2(12);
    lv_mes_anio24 varchar2(12);    
    lv_sql VARCHAR2(3000);
    --para desarrollo
   -- Lv_Ruta   varchar2(200):='/ora';
  --  Lv_Error  varchar2(2000);
    -- para produccion
--    Lv_Ruta   varchar2(200):='/ora1/axisweb/importacion';  
    Lv_Ruta   varchar2(200):='/app/oracle/admin/BSCSPROD/udump';  
    Lv_Error  varchar2(2000);
    Lv_Fecha    varchar2(20);
    lv_query varchar2(30000);
  begin  
    lv_error := null;
    lv_dia := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'dd');
    
    IF to_number(lv_dia) >= 1 AND to_number(lv_dia) <= 9 THEN
      lv_mes_anio24    := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
      lv_mes_anio8     := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa8  := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
    ELSIF to_number(lv_dia) >= 10 AND to_number(lv_dia) <= 23 THEN
      lv_mes_anio24    := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio8     := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa8  := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
    ELSIF to_number(lv_dia) >= 24 AND to_number(lv_dia) <= 31 THEN
      lv_mes_anio24    := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), 1),'mm/yyyy');
      lv_mes_anio8     := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
      lv_mes_anio_fa8  := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
    END IF;
--    
      lv_mes_anio_ant24 := to_char(Add_months(to_date(lv_dia||'/'||lv_mes_anio_fa24,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_ant8  := to_char(Add_months(to_date(lv_dia||'/'||lv_mes_anio_fa8,'dd/mm/yyyy'), -1),'mm/yyyy');    
--      
    li_cursor:=dbms_sql.open_cursor;
    
    lv_query := 'Select /*+ RULE +*/ distinct substr(dir.dn_num,5) telefono, '||
      'CASE WHEN (ord.ohopnamt_doc <= 0 OR (ord.ohopnamt_doc > 0 and ( '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''15/'||lv_mes_anio24||''',''dd/mm/rrrr'') '||
                 'and cu.billcycle NOT IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                          'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''24/'||lv_mes_anio_ant24||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = con.customer_id)) '||
                 'OR  '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''23/'||lv_mes_anio8||''',''dd/mm/rrrr'') '||
                 'and cu.billcycle IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                      'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''08/'||lv_mes_anio_ant8||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = con.customer_id))))) '||
      'THEN ''P'' ELSE ''M'' END estado '||
      'from   customer_all cu, contract_all con, profile_service pro, '||
             'contr_services_cap ser, orderhdr_all ord,'||
             'directory_number dir '||
      'where  con.customer_id = cu.customer_id '||
      'and    ord.customer_id = con.customer_id '||
      'and    con.co_id = ser.co_id '||
      'and    con.co_id = pro.co_id '||
      'and    ser.dn_id = dir.dn_id '||
      'and    pro.sncode = 7 '||
      'and    (ord.ohentdate = to_date(''24/'||lv_mes_anio_fa24||''',''dd/mm/yyyy'') or '||
             'ord.ohentdate = to_date(''08/'||lv_mes_anio_fa8||''',''dd/mm/yyyy'')) '||
      'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||
      'and    ser.cs_deactiv_date is null '||
      'and    dir.dn_status = ''a'' '||
      'UNION '||
      'Select /*+ RULE +*/ distinct substr(dir.dn_num,5) telefono, '||
      'CASE WHEN (ord.ohopnamt_doc <= 0 OR (ord.ohopnamt_doc > 0 and ( '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''15/'||lv_mes_anio24||''',''dd/mm/rrrr'') '||
                 'and cuh.billcycle NOT IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                          'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''24/'||lv_mes_anio_ant24||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = cuh.customer_id_high)) '||
                 'OR  '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''23/'||lv_mes_anio8||''',''dd/mm/rrrr'') '||
                 'and cuh.billcycle IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                      'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''08/'||lv_mes_anio_ant8||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = cuh.customer_id_high))))) '||
      'THEN ''P'' ELSE ''M'' END estado '||
      'from   customer_all cuh, contract_all con, profile_service pro, '||
             'contr_services_cap ser, orderhdr_all ord, '||
             'directory_number dir '||
      'where  con.customer_id = cuh.customer_id '||
      'and    ord.customer_id = cuh.customer_id_high '||
      'and    con.co_id = ser.co_id '||
      'and    con.co_id = pro.co_id '||
      'and    ser.dn_id = dir.dn_id '||
      'and    pro.sncode = 7 '||
      'and    (ord.ohentdate = to_date(''24/'||lv_mes_anio_fa24||''',''dd/mm/yyyy'') or '||
             'ord.ohentdate = to_date(''08/'||lv_mes_anio_fa8||''',''dd/mm/yyyy'')) '||
      'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||
      'and    ser.cs_deactiv_date is null '||
      'and    dir.dn_status = ''a'' ';
    
    dbms_sql.parse(li_cursor,lv_query,dbms_sql.V7);
      
      dbms_sql.define_column(li_cursor,1,lv_telefono,20);
      dbms_sql.define_column(li_cursor,2,lv_estado,1);

      ln_num_registros := dbms_sql.execute(li_cursor);
     
      lv_archivo:='rep_pago_coris_diario'||'.dat';
    
      WFOUT:=SYS.UTL_FILE.FOPEN(Lv_Ruta, lv_archivo, 'W'); 
     
      WHILE not lb_salir loop
     
        begin
           if dbms_sql.fetch_rows(li_cursor)>0 then
              dbms_sql.column_value(li_cursor,1,lv_telefono);
              dbms_sql.column_value(li_cursor,2,lv_estado);
              lv_linea := lv_telefono||'|'||lv_estado;
              SYS.UTL_FILE.PUT_LINE(WFOUT,lv_linea);
           else
              lb_salir:=true;
            
           end if;
           EXCEPTION 
             WHEN OTHERS THEN
                  pv_error := 'Error '||sqlerrm;
                  SYS.UTL_FILE.FCLOSE(WFOUT);

        end;
       
      END LOOP;
      
      dbms_sql.close_cursor(li_cursor);
      SYS.UTL_FILE.FCLOSE(WFOUT);
      
      EXCEPTION 
        WHEN UTL_FILE.INVALID_PATH THEN
      --    DBMS_OUTPUT.PUT_LINE('e1 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;
        WHEN UTL_FILE.INVALID_FILEHANDLE THEN
      --    DBMS_OUTPUT.PUT_LINE('e2 : '||sqlerrm);  
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;
        WHEN UTL_FILE.INVALID_OPERATION THEN
      --    DBMS_OUTPUT.PUT_LINE('e3 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN UTL_FILE.WRITE_ERROR THEN
      --    DBMS_OUTPUT.PUT_LINE('e4 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN UTL_FILE.READ_ERROR THEN
      --    DBMS_OUTPUT.PUT_LINE('e5 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN OTHERS THEN
             if dbms_sql.is_open(li_cursor) then
                dbms_sql.close_cursor(li_cursor);
             end if;
             lv_Error:='Error '||sqlerrm;
             pv_error := 'Error '||sqlerrm;

  END RPP_GENERA_REP_DIARIO;
-----------------------------------------------------------------------------
  /*
   * Procedimiento que extrae datos de pagos de asistencia inmediata
   * obtiene el n�mero de usuarios activos y recaudados del mes actual
   * obtiene el n�mero de usuarios pendientes de pago y recaudados del mes anterior y dos meses atras
   * realiza c�lculos para obtener los valores a pagar por cada mes del servicio de asistencia inmediata
   * suma del valor total de los 3 meses
   * Inserta los valores en la tabla historica
   */
  PROCEDURE RPP_GENERA_REP_MENSUAL(pv_fecha varchar2,
                                   pv_error out varchar2) is
    ln_usua_acti     number;
    ln_usua_reca     number;
    ln_dif_acti_reca number;
    ln_usua_pend1    number;
    ln_usua_reca1    number;
    ln_usua_pend2    number;
    ln_usua_reca2    number;
    ln_valor1        number;
    ln_valor2        number;
    ln_valor3        number;
    ln_suma          number;
    ln_precio        number;
    ln_precio_usua   number;
    lv_periodo       varchar2(20);
    ln_mes           number;
    lv_mes           varchar2(2);
    ln_mes2          number;
    lv_mes2          varchar2(2);
    ln_mes3          number;
    lv_mes3          varchar2(2);
    lv_anio          varchar2(4);
    lv_anio2         varchar2(4);
    lv_anio3         varchar2(4);
    ln_num1          number;
    ln_num2          number;
    
  BEGIN
   /* mes y anio */
    lv_mes:=to_char(to_number(to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm')-1));
    lv_anio:=to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyy');
  /* mes anterior */
    ln_mes2:=to_number(lv_mes)-1;
    lv_anio2:=lv_anio;
    if ln_mes2 = 0 then 
       ln_mes2:=12;
       lv_anio2:=to_char(to_number(lv_anio)-1);
    end if;

    if ln_mes2 <=9 then
       lv_mes2:='0'||to_char(ln_mes2);
    else
       lv_mes2:=to_char(ln_mes2);
    end if;
  /* 2 meses atras  */
    ln_mes3:=to_number(lv_mes)-2;
    lv_anio3:=lv_anio;
    if ln_mes3 = 0 then 
       ln_mes3:=12;
       lv_anio3:=to_char(to_number(lv_anio)-1);
    elsif ln_mes3 = -1 then
       ln_mes3:=11;
       lv_anio3:=to_char(to_number(lv_anio)-1);
    end if;

    if ln_mes3 <=9 then
       lv_mes3:='0'||to_char(ln_mes3);
    else
       lv_mes3:=to_char(ln_mes3);
    end if;

  /* usuarios activos mes actual */     
     select sum(telefono) 
     into ln_usua_acti
     from ( 
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cu, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cu.customer_id
        and    ord.customer_id = con.customer_id
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = to_date('24/' || lv_mes || '/' || lv_anio,'dd/mm/yyyy') or
               ord.ohentdate = to_date('08/' || lv_mes || '/' || lv_anio,'dd/mm/yyyy'))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a'
        UNION
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cuh, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cuh.customer_id
        and    ord.customer_id = cuh.customer_id_high
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = to_date('24/' || lv_mes || '/' || lv_anio,'dd/mm/yyyy') or
               ord.ohentdate = to_date('08/' || lv_mes || '/' || lv_anio,'dd/mm/yyyy'))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a');

   /* usuarios recaudados mes actual */
     select sum(telefono)
     into ln_usua_reca
     from ( 
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cu, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cu.customer_id
        and    ord.customer_id = con.customer_id
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = to_date('24/'||lv_mes||'/'||lv_anio,'dd/mm/yyyy') or
               ord.ohentdate = to_date('08/'||lv_mes || '/' || lv_anio,'dd/mm/yyyy'))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a'
        UNION
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cuh, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cuh.customer_id
        and    ord.customer_id = cuh.customer_id_high
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = to_date('24/'||lv_mes || '/' || lv_anio,'dd/mm/yyyy') or
               ord.ohentdate = to_date('08/'||lv_mes || '/' || lv_anio,'dd/mm/yyyy'))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a');
     
    /* diferencia activos y recaudados */
    ln_dif_acti_reca := ln_usua_acti - ln_usua_reca;

    /* calcular precio */
    select nvl(precio,0)
    into ln_precio
    from RP_CORIS_RANGO_PAGOS
    where ln_usua_reca between desde and hasta;
    
    /* precio calculado por usuario */ --considerar en el reporte?
    ln_precio_usua:=ln_precio;
    
    /* valor a pagar mes actual */
    ln_valor1 := ln_usua_reca * ln_precio;
    
    /* usuarios pendientes mes anterior */
    select nvl(USUA_PEND_PERI,0)
    into  ln_usua_pend1
    from  RP_PAGO_CORIS_HIST
    where periodo = lv_mes2
    and   to_char(fecha,'yyyy')=lv_anio2
    and   estado = 'A';
    
    /* usuarios recaudados mes anterior */
    select sum(telefono)
    into ln_num1
    from ( 
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cu, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cu.customer_id
        and    ord.customer_id = con.customer_id
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = Add_months(to_date('24/'||lv_mes2||'/'||lv_anio2,'dd/mm/yyyy'),-1) or 
               ord.ohentdate = Add_months(to_date('08/'||lv_mes2||'/'||lv_anio2,'dd/mm/yyyy'),-1))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a'
        UNION
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cuh, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cuh.customer_id
        and    ord.customer_id = cuh.customer_id_high
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = Add_months(to_date('24/'||lv_mes2||'/'||lv_anio2,'dd/mm/yyyy'),-1) or 
               ord.ohentdate = Add_months(to_date('08/'||lv_mes2||'/'||lv_anio2,'dd/mm/yyyy'),-1))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a');

      select nvl(USUA_RECA,0)
      into ln_num2
      from RP_PAGO_CORIS_HIST
      where periodo = lv_mes2
      and   to_char(fecha,'yyyy')=lv_anio2
      and   estado = 'A';
      
      ln_usua_reca1 := abs(ln_num2-ln_num1);
        
     /* calcular precio */
      select nvl(precio,0)
      into ln_precio
      from RP_CORIS_RANGO_PAGOS
      where ln_usua_reca1 between desde and hasta;
      
    /* valor a pagar mes anterior */
     ln_valor2 := ln_usua_reca1 * ln_precio;
    
    /* usuarios pendientes 2 meses atras */
     select nvl(USUA_PEND_ANTE,0) - nvl(USUA_RECA_ANTE,0)
     into  ln_usua_pend2
     from  RP_PAGO_CORIS_HIST
     where periodo = lv_mes2
     and   to_char(fecha,'yyyy')=lv_anio2
     and   estado = 'A';

    /* usuarios recaudados 2 meses atras */
    select sum(telefono)
    into ln_num1 
    from ( 
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cu, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cu.customer_id
        and    ord.customer_id = con.customer_id
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = Add_months(to_date('24/'||lv_mes3||'/'||lv_anio3,'dd/mm/yyyy'),-2) or 
               ord.ohentdate = Add_months(to_date('08/'||lv_mes3||'/'||lv_anio3,'dd/mm/yyyy'),-2))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a'
        UNION
        Select /*+ RULE +*/ count(distinct dir.dn_num) telefono
        from   customer_all cuh, contract_all con, profile_service pro, 
               contr_services_cap ser, orderhdr_all ord,
               directory_number dir
        where  con.customer_id = cuh.customer_id
        and    ord.customer_id = cuh.customer_id_high
        and    con.co_id = ser.co_id
        and    con.co_id = pro.co_id
        and    ser.dn_id = dir.dn_id
        and    pro.sncode = 7
        and    (ord.ohentdate = Add_months(to_date('24/'||lv_mes3||'/'||lv_anio3,'dd/mm/yyyy'),-2) or 
               ord.ohentdate = Add_months(to_date('08/'||lv_mes3||'/'||lv_anio3,'dd/mm/yyyy'),-2))
        and    ord.ohstatus in ('IN','CM')
        and    instr(ord.ohrefnum,'-') <> 0
        and    ord.ohopnamt_doc <= 0
        and    ser.cs_deactiv_date is null
        and    dir.dn_status = 'a');
    
      select nvl(USUA_RECA_ANTE,0)
      into ln_num2
      from RP_PAGO_CORIS_HIST
      where periodo = lv_mes2
      and   to_char(fecha,'yyyy')=lv_anio2
      and   estado = 'A';
      
      ln_usua_reca2 := abs(ln_num2-ln_num1);
    
    /* calcular precio */
      select nvl(precio,0)
      into ln_precio
      from RP_CORIS_RANGO_PAGOS
      where ln_usua_reca2 between desde and hasta;

    /* valor a pagar 2 meses atras */
     ln_valor3 := ln_usua_reca2 * ln_precio;

    /* suma total a pagar */
     ln_suma := ln_valor1 + ln_valor2 + ln_valor3;
--     
     if to_number(lv_mes) < 10 Then
        lv_mes:='0'||lv_mes;
     end if;
--        
    /* ingreso registro en tabla historica */
     insert into 
            RP_PAGO_CORIS_HIST(FECHA,
                               PERIODO,
                               USUA_ACTI,
                               USUA_RECA,
                               USUA_PEND_PERI,
                               USUA_PEND_ANTE,
                               USUA_RECA_ANTE,
                               USUA_PEND_DOS,
                               USUA_RECA_DOS,
                               TOTAL_PERI,
                               TOTAL_ANTE,
                               TOTAL_DOS,
                               TOTAL,
                               ESTADO)
            values(to_date(pv_fecha,'dd/mm/yyyy'),
                   lv_mes,
                   ln_usua_acti,
                   ln_usua_reca,
                   ln_dif_acti_reca,
                   ln_usua_pend1,
                   ln_usua_reca1,
                   ln_usua_pend2,
                   ln_usua_reca2,
                   ln_valor1,
                   ln_valor2,
                   ln_valor3,
                   ln_suma,
                   'A');
--                   
     /* grabar datos */
     commit;                    
--
    Exception
      when no_data_found then
        pv_error:=sqlerrm;
      when too_many_rows then
        pv_error:=sqlerrm;
      when dup_val_on_index then
        pv_error:=sqlerrm;
        rollback;
      when others then
        pv_error:=sqlerrm;

     /* grabar datos */
     commit;

  END RPP_GENERA_REP_MENSUAL;
--------------------------------------------------------------------------- 
END RPK_PAGO_CORIS;
/
