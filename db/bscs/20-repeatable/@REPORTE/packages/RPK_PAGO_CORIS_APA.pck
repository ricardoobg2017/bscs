CREATE OR REPLACE package RPK_PAGO_CORIS_APA is

  -- Author  : C�SAR SEVILLA M
  -- Created : 25/08/2005 14:57:35
  -- Purpose : Paquete genera Reporte Diario y Mensual para Pago de Servicio de Asistencia Inmediata a la Empresa CORIS


  PROCEDURE RPP_GENERA_REP_DIARIO(pv_fecha varchar2, 
                                  pv_error out varchar2);                                  
  
end RPK_PAGO_CORIS_APA;
/
CREATE OR REPLACE package body RPK_PAGO_CORIS_APA is
/*
 * Procedimiento que genera un archivo de texto con el reporte diario 
 * requiere de la fecha en mes y a�o con formato mm/yyyy
 * hace uso del paquete dbms_sql para un sql din�mico y 
 * del paquete utl_file para manejo de archivos
 */
  PROCEDURE RPP_GENERA_REP_DIARIO(pv_fecha varchar2,
                                  pv_error out varchar2) is
    li_cursor INTEGER;
    lv_telefono varchar2(20);
    lv_estado varchar2(1);
    ln_num_registros number;
    WFOUT SYS.UTL_FILE.FILE_TYPE;
    lv_archivo VARCHAR2(50);
    lv_linea VARCHAR2(100);
    lb_salir boolean:=FALSE;
    lv_dia VARCHAR2(2);
    lv_mes_anio_fa8 varchar2(12);    
    lv_mes_anio_fa24 varchar2(12);    
    lv_mes_anio_ant8 varchar2(12);    
    lv_mes_anio_ant24 varchar2(12);            
    lv_mes_anio8 varchar2(12);
    lv_mes_anio24 varchar2(12);    
    lv_sql VARCHAR2(3000);
    --para desarrollo
   -- Lv_Ruta   varchar2(200):='/ora';
  --  Lv_Error  varchar2(2000);
    -- para produccion
--    Lv_Ruta   varchar2(200):='/ora1/axisweb/importacion';  
    Lv_Ruta   varchar2(200):='/app/oracle/admin/BSCSPROD/udump';  
    Lv_Error  varchar2(2000);
    Lv_Fecha    varchar2(20);
  begin  
    lv_error := null;
    lv_dia := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'dd');
    
    IF to_number(lv_dia) >= 1 AND to_number(lv_dia) <= 9 THEN
      lv_mes_anio24    := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
      lv_mes_anio8     := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa8  := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
    ELSIF to_number(lv_dia) >= 10 AND to_number(lv_dia) <= 23 THEN
      lv_mes_anio24    := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio8     := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_fa8  := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
    ELSIF to_number(lv_dia) >= 24 AND to_number(lv_dia) <= 31 THEN
      lv_mes_anio24    := to_char(Add_months(to_date(pv_fecha,'dd/mm/yyyy'), 1),'mm/yyyy');
      lv_mes_anio8     := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');
      lv_mes_anio_fa24 := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
      lv_mes_anio_fa8  := to_char(to_date(pv_fecha,'dd/mm/yyyy'),'mm/yyyy');    
    END IF;
--    
      lv_mes_anio_ant24 := to_char(Add_months(to_date(lv_dia||'/'||lv_mes_anio_fa24,'dd/mm/yyyy'), -1),'mm/yyyy');
      lv_mes_anio_ant8  := to_char(Add_months(to_date(lv_dia||'/'||lv_mes_anio_fa8,'dd/mm/yyyy'), -1),'mm/yyyy');    
--      
    li_cursor:=dbms_sql.open_cursor;
    dbms_sql.parse(li_cursor,
      'Select /*+ RULE +*/ distinct substr(dir.dn_num,5) telefono, '||
      'CASE WHEN (ord.ohopnamt_doc <= 0 OR (ord.ohopnamt_doc > 0 and ( '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''15/'||lv_mes_anio24||''',''dd/mm/rrrr'') '||
                 'and cu.billcycle NOT IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                          'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''24/'||lv_mes_anio_ant24||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = con.customer_id)) '||
                 'OR  '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''23/'||lv_mes_anio8||''',''dd/mm/rrrr'') '||
                 'and cu.billcycle IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                      'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''08/'||lv_mes_anio_ant8||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = con.customer_id))))) '||
      'THEN ''P'' ELSE ''M'' END estado '||
      'from   customer_all cu, contract_all con, profile_service pro, '||
             'contr_services_cap ser, orderhdr_all ord,'||
             'directory_number dir '||
      'where  con.customer_id = cu.customer_id '||
      'and    ord.customer_id = con.customer_id '||
      'and    con.co_id = ser.co_id '||
      'and    con.co_id = pro.co_id '||
      'and    ser.dn_id = dir.dn_id '||
      'and    pro.sncode = 7 '||
      'and    (ord.ohentdate = to_date(''24/'||lv_mes_anio_fa24||''',''dd/mm/yyyy'') or '||
             'ord.ohentdate = to_date(''08/'||lv_mes_anio_fa8||''',''dd/mm/yyyy'')) '||
      'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||
      'and    ser.cs_deactiv_date is null '||
      'and    dir.dn_status = ''a'' '||
      'UNION '||
      'Select /*+ RULE +*/ distinct substr(dir.dn_num,5) telefono, '||
      'CASE WHEN (ord.ohopnamt_doc <= 0 OR (ord.ohopnamt_doc > 0 and ( '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''15/'||lv_mes_anio24||''',''dd/mm/rrrr'') '||
                 'and cuh.billcycle NOT IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                          'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''24/'||lv_mes_anio_ant24||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = cuh.customer_id_high)) '||
                 'OR  '||
                 '(to_date(sysdate,''dd/mm/rrrr'') <= to_date(''23/'||lv_mes_anio8||''',''dd/mm/rrrr'') '||
                 'and cuh.billcycle IN (SELECT fa.id_ciclo_admin FROM fa_ciclos_axis_bscs fa '||
                                      'WHERE fa.id_ciclo = ''02'') '||
                 'AND EXISTS (SELECT * FROM orderhdr_all ord2 '||
                             'WHERE ord2.ohentdate = to_date(''08/'||lv_mes_anio_ant8||''',''dd/mm/rrrr'') '||
                             'AND   ord2.ohopnamt_doc <= 0 '||
                             'AND   instr(ord2.ohrefnum,''-'') <> 0 '||
                             'AND   ord2.customer_id = cuh.customer_id_high))))) '||
      'THEN ''P'' ELSE ''M'' END estado '||
      'from   customer_all cuh, contract_all con, profile_service pro, '||
             'contr_services_cap ser, orderhdr_all ord, '||
             'directory_number dir '||
      'where  con.customer_id = cuh.customer_id '||
      'and    ord.customer_id = cuh.customer_id_high '||
      'and    con.co_id = ser.co_id '||
      'and    con.co_id = pro.co_id '||
      'and    ser.dn_id = dir.dn_id '||
      'and    pro.sncode = 7 '||
      'and    (ord.ohentdate = to_date(''24/'||lv_mes_anio_fa24||''',''dd/mm/yyyy'') or '||
             'ord.ohentdate = to_date(''08/'||lv_mes_anio_fa8||''',''dd/mm/yyyy'')) '||
      'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||
      'and    ser.cs_deactiv_date is null '||
      'and    dir.dn_status = ''a'' ',
      dbms_sql.V7);
      
      dbms_sql.define_column(li_cursor,1,lv_telefono,20);
      dbms_sql.define_column(li_cursor,2,lv_estado,1);

      ln_num_registros := dbms_sql.execute(li_cursor);
     
      lv_archivo:='rep_pago_coris_dia_APA'||'.dat';
    
      WFOUT:=SYS.UTL_FILE.FOPEN(Lv_Ruta, lv_archivo, 'W'); 
     
      WHILE not lb_salir loop
     
        begin
           if dbms_sql.fetch_rows(li_cursor)>0 then
              dbms_sql.column_value(li_cursor,1,lv_telefono);
              dbms_sql.column_value(li_cursor,2,lv_estado);
              lv_linea := lv_telefono||'|'||lv_estado;
              SYS.UTL_FILE.PUT_LINE(WFOUT,lv_linea);
           else
              lb_salir:=true;
            
           end if;
           EXCEPTION 
             WHEN OTHERS THEN
                  pv_error := 'Error '||sqlerrm;
                  SYS.UTL_FILE.FCLOSE(WFOUT);

        end;
       
      END LOOP;
      
      dbms_sql.close_cursor(li_cursor);
      SYS.UTL_FILE.FCLOSE(WFOUT);
      
      EXCEPTION 
        WHEN UTL_FILE.INVALID_PATH THEN
      --    DBMS_OUTPUT.PUT_LINE('e1 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;
        WHEN UTL_FILE.INVALID_FILEHANDLE THEN
      --    DBMS_OUTPUT.PUT_LINE('e2 : '||sqlerrm);  
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;
        WHEN UTL_FILE.INVALID_OPERATION THEN
      --    DBMS_OUTPUT.PUT_LINE('e3 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN UTL_FILE.WRITE_ERROR THEN
      --    DBMS_OUTPUT.PUT_LINE('e4 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN UTL_FILE.READ_ERROR THEN
      --    DBMS_OUTPUT.PUT_LINE('e5 : '||sqlerrm);
          UTL_FILE.FCLOSE(WFOUT);
          lv_Error:='Error '||sqlerrm;
          pv_error := 'Error '||sqlerrm;          
        WHEN OTHERS THEN
             if dbms_sql.is_open(li_cursor) then
                dbms_sql.close_cursor(li_cursor);
             end if;
             lv_Error:='Error '||sqlerrm;
             pv_error := 'Error '||sqlerrm;

  END RPP_GENERA_REP_DIARIO;

END RPK_PAGO_CORIS_APA;
/

