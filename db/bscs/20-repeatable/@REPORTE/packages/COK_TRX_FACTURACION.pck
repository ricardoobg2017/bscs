CREATE OR REPLACE package COK_TRX_FACTURACION is

  procedure crea_cartera(p_fecha in varchar2,
                         p_error out varchar2);
  
  procedure crea_servicios(p_fecha in varchar2,
                           p_error out varchar2);
  
end;
/

