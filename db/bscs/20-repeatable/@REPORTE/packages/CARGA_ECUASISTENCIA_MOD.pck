CREATE OR REPLACE package CARGA_ECUASISTENCIA_MOD is

PROCEDURE CONSUMO (P_ANIO    IN VARCHAR2,
                   P_MES     IN VARCHAR2,
                   P_COD_RET IN OUT NUMBER, 
                   P_MSG_RET IN OUT VARCHAR2);

PROCEDURE CANTIDAD_TELEFONO(P_COD_RET IN OUT NUMBER, 
                            P_MSG_RET IN OUT VARCHAR2);
                            
PROCEDURE CUENTA_TELEFONO(P_ANIO    IN VARCHAR2,
                          P_MES     IN VARCHAR2,
                          P_COD_RET IN OUT NUMBER, 
                          P_MSG_RET IN OUT VARCHAR2);                            
                            
PROCEDURE CARGA_PAGOS_ECUASISTENCIA
                            (P_ANIO IN NUMBER, 
                             P_MES IN NUMBER,
                             P_COD_RET IN OUT NUMBER, 
                             P_MSG_RET IN OUT VARCHAR2);
                                                         
PROCEDURE CARGA_PAGOS
                            (P_ANIO IN NUMBER, 
                             P_MES IN NUMBER,
                             P_COD_RET IN OUT NUMBER, 
                             P_MSG_RET IN OUT VARCHAR2);
                             
PROCEDURE CREDITOS(P_ANIO    IN VARCHAR2,
                          P_MES     IN VARCHAR2,
                          P_COD_RET IN OUT NUMBER,            
                          P_MSG_RET IN OUT VARCHAR2);
 
End CARGA_ECUASISTENCIA_MOD;
/
CREATE OR REPLACE package body CARGA_ECUASISTENCIA_MOD AS

-- ============================ COPYRIGHT 2003 BY CONECEL-SISTEMAS ============================
-- PROP�SITO      :  Carga de datos de consumos, pagos y saldos para Ecuasistencia
-- ACTUALIZACI�N  :  Agosto 25/2003
-- M�DULO         :  Ecuasistencia
-- AUTOR          :  Wendy Burgos Loy
-- ============================================================================================
PROCEDURE CONSUMO(P_ANIO    IN VARCHAR2,
                  P_MES     IN VARCHAR2,
                  P_COD_RET IN OUT NUMBER, 
                  P_MSG_RET IN OUT VARCHAR2) IS
BEGIN
 DECLARE
 
----------------------------------------------------------------------------- 
-------Cursor para obtener los saldos por Asistencia Inmediata---------------  
-----------------------------------------------------------------------------
 cursor cur_consumo is
   select 
   to_char(c.ohrefdate,'yyyy') Anio,
   d.custcode CuenCons, 
   to_char(c.ohrefdate,'mm') Mes,
   c.ohinvamt_doc  as TotaCons,
   sum(otamt_revenue_gl) as EcuaCons,
   0||d.prgcode CiclCons,   
   d.costcenter_id NumeOrig
   from  ordertrailer a, 
         mpusntab b,  
         orderhdr_all c,
         customer_all d, 
         glaccount_all e 
	 where a.otxact = c.ohxact 
   and   c.customer_id=d.customer_id 
   and   c.ohstatus = 'IN'
   and   to_char(c.ohrefdate,'yyyy') = p_anio
   and   to_char(c.ohrefdate,'mm') = p_mes  
   and   substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = b.sncode 
   and   e.glacode = a.otglsale 
   and   substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = '7'
	 group by c.ohrefdate, d.custcode, d.costcenter_id, 
   substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
   instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
   instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1),
   c.ohinvamt_doc, d.prgcode;     

  ----------------------------------------------------------------------------- 
  ----------Cursor para actualizar el campo de cantidad de tel�fonos-----------
  -------------que tiene un cliente en la tabla de ec_consumos-----------------  
  -----------------------------------------------------------------------------
   cursor cur_actualiza_telefono(pv_cuenta varchar2) is
    select /*+ rule */ max(cantidad) cant_fono 
    from   ec_telefonos_tmp
    where  cuenta = pv_cuenta   
    order by cuenta; 
    
  ----------------------------------------------------------------------------- 
  ---------------Cursor para actualizar el campo cr�dito-----------------------
  -------------que tiene un cliente en la tabla de ec_consumos-----------------  
  -----------------------------------------------------------------------------
   cursor cur_actualiza_credito(pv_cuenta varchar2) is
    select /*+ rule +*/ o.ohopnamt_doc saldo
    from   orderhdr_all o, customer_all c
    where  o.customer_id = c.customer_id
    and    c.custcode = pv_cuenta
    and    o.ohstatus = 'CM'
    and    to_char(o.ohrefdate,'yyyy') = p_anio
    and    to_char(o.ohrefdate,'mm') = p_mes
    order by c.custcode;
    
    sentencia            varchar2(2000); 
    source_cursor        number;
    sentencia_c          varchar2(2000); 
    source_cursor_c      number;
    sentencia_drop       varchar2(2000); 
    source_cursor_drop   number;
    sentencia_drop_i     varchar2(2000); 
    source_cursor_drop_i number;           
  BEGIN            
      p_cod_ret := 0;
      p_msg_ret := null;
        FOR i in cur_consumo LOOP
                 BEGIN
                   INSERT INTO EC_CONSUMOS(aniocons, cuencons, mescons,totacons,  ecuacons,  canttelecons, ciclcons,   numeorig, Reca_Cons,Fech_Reca_Cons)
                   VALUES                 (i.anio, i.cuencons, i.mes, i.totacons, i.ecuacons,   0,        i.ciclcons, i.numeorig,0,null);

                 EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                       NULL;
                    WHEN OTHERS THEN
                       P_COD_RET := 1;
                       P_MSG_RET := 'Error al insertar los consumos '||sqlerrm;
                      
                 END;
                 commit;
                                
                For j in cur_actualiza_telefono(i.cuencons) loop
                  Begin
                  
                    update ec_consumos 
                    set    canttelecons = j.cant_fono 
                    where  cuencons = i.cuencons;
                  Exception
                    When others then
                       P_COD_RET := 1;
                       P_MSG_RET := 'Error al actualizar la tabla de consumos '||i.cuencons||' '||sqlerrm; 
                  End;   
                  commit;               
                End loop;
                
                For k in cur_actualiza_credito(i.cuencons) loop
                  Begin
                   
                    update ec_consumos 
                    set    credcons = k.saldo
                    where  cuencons = i.cuencons;
                  Exception
                    When no_data_found then
                      update ec_consumos 
                      set    credcons = 0
                      where  cuencons = i.cuencons;
                      
                    When others then
                       P_COD_RET := 1;
                       P_MSG_RET := 'Error al actualizar la tabla de consumos el campo cr�dito '||sqlerrm; 
                  End;   
                  commit;               
                End loop;
                
                
         END LOOP;
          
         sentencia_drop:= 'alter table EC_CONSUMOS drop constraint CONS_PK cascade';
         source_cursor_drop := dbms_sql.open_cursor;         
         dbms_sql.parse(source_cursor_drop,sentencia_drop,2);
         dbms_sql.close_cursor(source_cursor_drop);         
         
         sentencia_drop_i:= 'drop index REFEC_ORIGEN1';
         source_cursor_drop_i := dbms_sql.open_cursor;
         dbms_sql.parse(source_cursor_drop_i,sentencia_drop_i,2);
         dbms_sql.close_cursor(source_cursor_drop_i);         
                
         sentencia:= 'alter table EC_CONSUMOS add constraint CONS_PK primary key ' ||
                     ' (NUMEORIG,ANIOCONS,MESCONS,CUENCONS)' ||
                     ' using index tablespace DATA';                      
         source_cursor := dbms_sql.open_cursor;
         dbms_sql.parse(source_cursor,sentencia,2);
         dbms_sql.close_cursor(source_cursor);

         sentencia_c :='create index REFEC_ORIGEN1 on EC_CONSUMOS (NUMEORIG) ' ||
                       'tablespace DATA';
         source_cursor_c := dbms_sql.open_cursor;
         dbms_sql.parse(source_cursor_c,sentencia_c,2);
         dbms_sql.close_cursor(source_cursor_c);                       
         
  END;
END;

PROCEDURE CANTIDAD_TELEFONO(P_COD_RET IN OUT NUMBER, 
                            P_MSG_RET IN OUT VARCHAR2) IS
BEGIN
 DECLARE
  cursor cur_total_telefono is
    select count(d.dn_num) cantidad, 
           a.custcode cuenta
    from   customer_all a, contract_all b, contr_services_cap c, directory_number d
    where  a.customer_id = b.customer_id
    and    b.co_id = c.co_id
    and    c.dn_id = d.dn_id
    group by a.custcode
    order by a.custcode;
  
-----             
    lv_cuenta  varchar2(24);
-----

  Begin
     p_cod_ret := 0;
      p_msg_ret := null;
        FOR i in cur_total_telefono  LOOP
          If  substr(i.cuenta,1,1) <> '1' then 
            lv_cuenta := substr(i.cuenta,1,instr(i.cuenta,'.',1,2)-1) ;
          else  
             lv_cuenta := i.cuenta;
          end if;
            Begin
                 INSERT INTO EC_TELEFONOS_TMP(cuenta, cantidad)
                 VALUES(lv_cuenta, i.cantidad);             
            Exception
               WHEN DUP_VAL_ON_INDEX THEN
                 NULL;
               WHEN OTHERS THEN
                 P_COD_RET := 1;
                 P_MSG_RET := 'Error al insertar la cantidad de tel�fonos x cuenta '||sqlerrm;                      
            End;
            commit;       
        End Loop;  
  End;    
END;

PROCEDURE CUENTA_TELEFONO(P_ANIO    IN VARCHAR2,
                          P_MES     IN VARCHAR2,
                          P_COD_RET IN OUT NUMBER,            
                          P_MSG_RET IN OUT VARCHAR2) IS
                          
----------------------------------------------------------------------------- 
-------Ingreso de Consumos de Asistencia Inmediata por cuenta----------------  
-----------------------con sus tel�fonos-------------------------------------
-----------------------------------------------------------------------------
                          
BEGIN
Declare
cursor cur_ecuacons is
   select count(*) cantidad, cuencons cuenta   
   from   ec_detalle_consumos 
   where  anio = p_anio
   and    mes = p_mes
   group by ecuacons, cuencons
   having count(cuencons)>1
   order by cuencons;

   ln_cantidad    number;
   ln_cant        number;
   lv_cuenta     varchar2(24);   
   
Begin

insert into ec_detalle_consumos
 select
   d.custcode CuenCons, 
   substr(f.dn_num,5,7) celular,
   i.ccfname nombre,
   i.cclname apellido,
   i.ccname direccion,
   d.cssocialsecno cedula,
   sum(otamt_revenue_gl) as EcuaCons,
   to_char(c.ohrefdate,'yyyy') Anio,
   to_char(c.ohrefdate,'mm') Mes,
   0||d.prgcode CiclCons,
   decode(to_char(c.ohrefdate,'mm'),'02','ENE 24','03','FEB 24','04','MAR 24','05','ABR 24','06','MAY 24',
   '07','JUN 24','08','JUL 24','09','AGO 24','10','SEP 24','11','OCT 24','12','NOV 24','DIC 24') fecha_inicio,
   decode(to_char(c.ohrefdate,'mm'),'01','ENE 23','02','FEB 23','03','MAR 23','04','ABR 23','05','MAY 23',
   '06','JUN 23','07','JUL 23','08','AGO 23','09','SEP 23','10','OCT 23','11','NOV 23','DIC 23') fecha_fin,
   d.costcenter_id region 
   from  ordertrailer a, 
         mpusntab b,  
         orderhdr_all c, 
         customer_all d, 
         glaccount_all e,
         ccontact_all i,
         directory_number f, 
         contract_all g,
         contr_services_cap h,
         costcenter j,
         profile_service p  
	 where a.otxact = c.ohxact 
   and   c.customer_id=d.customer_id 
   and   d.customer_id = i.customer_id
   and   i.customer_id = g.customer_id
   and   g.co_id = h.co_id
   and   g.co_id = p.co_id
   and   h.dn_id = f.dn_id                  
   and   d.costcenter_id = j.cost_id
   and   i.ccbill = 'X'
   and   c.ohstatus = 'IN'
   and   to_char(c.ohrefdate,'yyyy') = p_anio
   and   to_char(c.ohrefdate,'mm') =   p_mes  
   and      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = b.sncode 
   and   b.sncode = p.sncode  
   and      e.glacode = a.otglsale 
   and      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = '7'
	 group by c.ohrefdate,f.dn_num,d.costcenter_id ,d.custcode,i.ccfname,i.cclname,i.ccname,d.cssocialsecno,
   substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
   instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
   instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1),
   c.ohinvamt_doc, d.prgcode   
 UNION
 select 
   decode(substr(d.custcode,1,1),1,d.custcode,substr(d.custcode,1,instr(d.custcode,'.',1,2)-1)) CuenCons, 
   substr(f.dn_num,5,7) celular,   
   i.ccfname nombre,
   i.cclname apellido,
   i.ccname direccion,
   d.cssocialsecno cedula,
   sum(otamt_revenue_gl) as EcuaCons,
   to_char(c.ohrefdate,'yyyy') Anio,
   to_char(c.ohrefdate,'mm') Mes,
   0||d.prgcode CiclCons,
   decode(to_char(c.ohrefdate,'mm'),'02','ENE 24','03','FEB 24','04','MAR 24','05','ABR 24','06','MAY 24',
   '07','JUN 24','08','JUL 24','09','AGO 24','10','SEP 24','11','OCT 24','12','NOV 24','DIC 24') fecha_inicio,
   decode(to_char(c.ohrefdate,'mm'),'01','ENE 23','02','FEB 23','03','MAR 23','04','ABR 23','05','MAY 23',
   '06','JUN 23','07','JUL 23','08','AGO 23','09','SEP 23','10','OCT 23','11','NOV 23','DIC 23') fecha_fin,
   d.costcenter_id region 
   from  ordertrailer a,
         mpusntab b,    
         orderhdr_all c, 
         customer_all d, 
         customer_all k,
         glaccount_all e, 
         directory_number f, 
         contract_all g,
         contr_services_cap h,
         ccontact_all i,
         costcenter j,
         profile_service p
	 where a.otxact = c.ohxact 
   and   d.customer_id = i.customer_id
   and   i.customer_id = g.customer_id
   and   d.customer_id_high = k.customer_id
   and   k.customer_id = c.customer_id    
   and   g.co_id = h.co_id
   and   g.co_id = p.co_id
   and   h.dn_id = f.dn_id
   and   i.ccbill = 'X'
   and   d.costcenter_id = j.cost_id
   and   c.ohstatus = 'IN'  
   and   to_char(c.ohrefdate,'yyyy') = p_anio
   and   to_char(c.ohrefdate,'mm') =   p_mes  
   and   h.cs_deactiv_date is null
   and      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = b.sncode 
   and   b.sncode = p.sncode     
   and      e.glacode = a.otglsale 
   and      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
                 instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1) = '7'
	 group by  c.ohrefdate,d.custcode,d.costcenter_id,f.dn_num,i.ccfname,i.cclname,i.ccname,d.cssocialsecno,
   substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
   instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
   instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1),
   c.ohinvamt_doc, d.prgcode;
   --order by 1;
   
COMMIT; 
   
  For i in cur_ecuacons loop
      ln_cantidad := i.cantidad;      
      lv_cuenta   := i.cuenta;
      ln_cant:= ln_cantidad - 1;
      --ln_cant_aux:= ln_cantidad;
    While ln_cant < ln_cantidad loop
     If ln_cant <> 0 then
       Begin
        update ec_detalle_consumos
        set    ecuacons = 0--round(ecuacons/ln_cantidad,2)
        where  anio     = p_anio
        and    mes      = p_mes
        and    cuencons  = lv_cuenta
        and    rowid not in (select max(rowid) from ec_detalle_consumos 
                                               where anio = p_anio
                                               and    mes      = p_mes
                                               and    cuencons  = lv_cuenta);
        Exception
        When others then
          P_COD_RET := 1;
          P_MSG_RET := 'Error al actualizar la tabla de ec_detalle_consumos'||sqlerrm;                          
       End;
      commit;    
      ln_cantidad:= ln_cantidad - 1;
      ln_cant:= ln_cant -1;
     else
       exit;
     end if;
     end loop;
   End loop;

End;
   
Exception
         WHEN DUP_VAL_ON_INDEX THEN
          NULL;
         WHEN OTHERS THEN
           P_COD_RET := 1;
           P_MSG_RET := 'Error al insertar la cantidad de tel�fonos x cuenta '||sqlerrm;                          

END;


PROCEDURE CARGA_PAGOS_ECUASISTENCIA
                            (P_ANIO IN NUMBER, 
                             P_MES IN NUMBER,
                             P_COD_RET IN OUT NUMBER, 
                             P_MSG_RET IN OUT VARCHAR2) is
begin
declare

cursor pagos (anio_ant varchar2, mes_ant varchar2,
                      p_anio varchar2, p_mes  varchar2) is
select 
        a.custcode Cuenta, 
        sum(c.cacuramt_pay) ValoPag, 
        a.costcenter_id NumeOrig
from    cashreceipts_all c, 
        customer_all a, 
        costcenter s
where   c.customer_id   = a.customer_id
and     a.costcenter_id = s.cost_id
and     trunc(c.cachkdate) between to_date('24/'||mes_ant||'/'||anio_ant,'dd/mm/yyyy')
                                        and  to_date('23/'||p_mes||'/'||p_anio,'dd/mm/yyyy')
group by a.custcode, 
               a.costcenter_id;

--------
sentencia     varchar2(2000); 
source_cursor number; 
anio_ant      varchar2(4);
mes_ant      varchar2(2);
anio_sig      varchar2(4);
mes_sig       varchar2(2);
--------
begin
p_cod_ret := 0;
p_msg_ret := null;


/*sentencia := 'truncate table ec_pagos reuse storage';
source_cursor := dbms_sql.open_cursor;
dbms_sql.parse(source_cursor,sentencia,2);
dbms_sql.close_cursor(source_cursor);*/

------
   -- Para obtener mes y anio anterior
   --
   if  p_mes = 1 then
       anio_ant := p_anio - 1;
       mes_ant := 12;
   else
       anio_ant := p_anio;
       mes_ant := p_mes - 1;
   end if;
   --
   -- Para obtener mes y anio siguiente
   --
   if p_mes = 12 then
      anio_sig := p_anio + 1;
      mes_sig  := 1;
   else
      anio_sig := p_anio;
      mes_sig  := p_mes + 1;
   end if;
------

for i in pagos (anio_ant, mes_ant, p_anio, p_mes) loop
     begin
        insert into ec_pagos (cuenpago,aniopago,mespago,valopago,credpago,numeorig)
        values (i.cuenta, p_anio,  p_mes, i.valopag, 0,  i.numeorig) ;
     exception
        when DUP_VAL_ON_INDEX then
             dbms_output.put_line('Existe un valor duplicado:'||i.cuenta||','||p_anio||','||p_mes||','||i.valopag||','||0||','||i.numeorig);
        when OTHERS then
             p_cod_ret := 1;
             p_msg_ret := 'Error al insertar los pagos: '||sqlerrm;
             exit;
     end;
     
end loop;
commit;
end;

end CARGA_PAGOS_ECUASISTENCIA;

PROCEDURE CARGA_PAGOS
                            (P_ANIO IN NUMBER, 
                             P_MES IN NUMBER,
                             P_COD_RET IN OUT NUMBER, 
                             P_MSG_RET IN OUT VARCHAR2) is
begin
declare
   
cursor cur_payment is
  select 
         distinct a.custcode Cuenta,        
         a.costcenter_id NumeOrig, ecuacons valopag
  from   cashreceipts_all c, 
         customer_all a, 
         costcenter s,
         ec_detalle_consumos e
  where  e.ecuacons > 0
  and    c.customer_id   = a.customer_id
  and    a.custcode      = e.cuencons
  and    a.costcenter_id = s.cost_id
  and    s.cost_id = e.region
  and    e.anio = p_anio
  and    e.mes = p_mes  
  and    trunc(c.cachkdate) between to_date('24/'||to_char(p_mes-1)||'/'||p_anio,'dd/mm/yyyy')
                            and  to_date('23/'||to_char(p_mes)||'/'||p_anio,'dd/mm/yyyy')
  order by a.custcode ;

--------
sentencia     varchar2(2000); 
source_cursor number; 
--------
begin
p_cod_ret := 0;
p_msg_ret := null;


/*sentencia := 'truncate table ec_pagos reuse storage';
source_cursor := dbms_sql.open_cursor;
dbms_sql.parse(source_cursor,sentencia,2);
dbms_sql.close_cursor(source_cursor);*/

for i in cur_payment loop
     begin
        insert into ec_pagos_tmp (cuenpago,aniopago,mespago,valopago,credpago,numeorig)
        values (i.cuenta, p_anio,  p_mes, i.valopag, 0,  i.numeorig) ;
     exception
        when DUP_VAL_ON_INDEX then
             dbms_output.put_line('Existe un valor duplicado:'||i.cuenta||','||p_anio||','||p_mes||','||i.valopag||','||0||','||i.numeorig);
        when OTHERS then
             p_cod_ret := 1;
             p_msg_ret := 'Error al insertar los pagos: '||sqlerrm;
             exit;
     end;
     
end loop;
commit;
end;

end CARGA_PAGOS;


PROCEDURE CREDITOS(P_ANIO    IN VARCHAR2,
                                      P_MES     IN VARCHAR2,
                                      P_COD_RET IN OUT NUMBER,            
                                      P_MSG_RET IN OUT VARCHAR2) IS
                          
----------------------------------------------------------------------------- 
-------Ingreso de Creditos------------------------------------------
-----------------------------------------------------------------------------
                          
BEGIN
declare

cursor tipos_creditos is
select codigo
from  ec_tipos_creditos;

cursor datos_creditos (tipo number, fecha_inic date, fecha_fina date) is
select b.custcode cuenta, 
   ' ' celular, 
   a.entdate fecha, 
   i.ccfname nombre,
   i.cclname apellido,
   i.ccname direccion,
   b.cssocialsecno cedula,
   a.amount credito,
   to_char(a.entdate,'yyyy') anio,
   to_char(a.entdate,'mm') mes,
   0||b.prgcode ciclo,
   /*decode(to_char(a.entdate,'mm'),'02','ENE 24','03','FEB 24','04','MAR 24','05','ABR 24','06','MAY 24',
   '07','JUN 24','08','JUL 24','09','AGO 24','10','SEP 24','11','OCT 24','12','NOV 24','DIC 24') fecha_inicio,
   decode(to_char(a.entdate,'mm'),'01','ENE 23','02','FEB 23','03','MAR 23','04','ABR 23','05','MAY 23',
   '06','JUN 23','07','JUL 23','08','AGO 23','09','SEP 23','10','OCT 23','11','NOV 23','DIC 23') fecha_fin,*/
   b.costcenter_id region,
   a.sncode 
from  fees a, 
         customer_all b, 
         ccontact_all i
where a.sncode = tipo
and    trunc(a.entdate) between fecha_inic
                                  and  fecha_fina
and a.customer_id = b.customer_id
and b.customer_id = i.customer_id
and   i.ccbill = 'X';

-----
fecha_inicial  varchar2(10);
fecha_final    varchar2(10);
fecha_ini       date;
fecha_fin       date;

begin
select add_months(to_date('24/'||p_mes||'/'||p_anio,'dd/mm/yyyy'),-1), 
          to_date('23/'||p_mes||'/'||p_anio,'dd/mm/yyyy')
into fecha_ini, fecha_fin
from dual;


for i in tipos_creditos loop
    for j in datos_creditos (i.codigo,fecha_ini,fecha_fin) loop
      begin
      select decode(p_mes,'02','ENE 24','03','FEB 24','04','MAR 24','05','ABR 24','06','MAY 24',
       '07','JUN 24','08','JUL 24','09','AGO 24','10','SEP 24','11','OCT 24','12','NOV 24','DIC 24'),
       decode(p_mes,'01','ENE 23','02','FEB 23','03','MAR 23','04','ABR 23','05','MAY 23',
       '06','JUN 23','07','JUL 23','08','AGO 23','09','SEP 23','10','OCT 23','11','NOV 23','DIC 23')
       into fecha_inicial, fecha_final
       from dual;
       
       insert into ec_creditos_tmp(cuencons,celular,nombre,apellido,direccion,cedula,ecuacons,anio,
                                                 mes,ciclcons,fecha_inicio,fecha_fin,region,tipo_credito)
       values(j.cuenta,'',j.nombre,j.apellido,j.direccion,j.cedula,j.credito,j.anio,j.mes,j.ciclo,
                  fecha_inicial,fecha_final,j.region,j.sncode);
       commit;
                                                        
       exception
          when others then
             dbms_output.put_line('No se pudo insertar en la tabla ec_creditos_tmp:  '||j.cuenta||' '||sqlerrm);
       end;
    end loop;
end loop;
end;

end CREDITOS;


end CARGA_ECUASISTENCIA_MOD;
/

