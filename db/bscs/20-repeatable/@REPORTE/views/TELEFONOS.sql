CREATE OR REPLACE TELEFONOS AS
select
    /*+rule */
    substr(dn_num, 5, 7) f_tel,
    a.custcode custcode
from
    customer_all a,
    contract_all b,
    contr_services_cap c,
    directory_number d,
    customer_all ab
where
    (
        a.customer_id = ab.customer_id_high
        or a.customer_id = b.customer_id
    )
    and ab.customer_id = b.customer_id
    and b.co_id = c.co_id
    and c.dn_id = d.dn_id
    and a.custcode like '1.%'