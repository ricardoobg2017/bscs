CREATE OR REPLACE FUNCTION CREA_TABLA_TEST (pdFechaPeriodo in  date, 
                         pv_error       out varchar2 ) RETURN NUMBER IS
    --
    lvSentencia     VARCHAR2(20000);
    lvMensErr       VARCHAR2(1000);
    --
    BEGIN
           lvSentencia := 'CREATE TABLE CO_REPCARCLI_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||
                          '( CUENTA               VARCHAR2(24),'||
                          '  ID_CLIENTE            NUMBER,'||
                          '  PRODUCTO              VARCHAR2(30),'||
                          '  CANTON                VARCHAR2(40),'||
                          '  PROVINCIA             VARCHAR2(25),'||
                          '  APELLIDOS             VARCHAR2(40),'||
                          '  NOMBRES               VARCHAR2(40),'||
                          '  RUC                   VARCHAR2(20),'||
                          '  FORMA_PAGO            VARCHAR2(58),'||
                          '  TARJETA_CUENTA        VARCHAR2(25),'||
                          '  FECH_EXPIR_TARJETA    VARCHAR2(20),'||
                          '  TIPO_CUENTA           VARCHAR2(40),'||
                          '  FECH_APER_CUENTA      DATE,'||
                          '  TELEFONO1             VARCHAR2(25),'||
                          '  TELEFONO2             VARCHAR2(25),'||
                          '  DIRECCION             VARCHAR2(70),'||
                          '  GRUPO                 VARCHAR2(10),'||
                          '  SALDOANTER            NUMBER default 0,'||
                          '  TOTAL2                NUMBER default 0,'||
                          '  TOTAL3                NUMBER default 0,'||
                          '  TOTAL_FACT            NUMBER default 0,'||
                          '  NUM_FACTURA           NUMBER default 0,'||
                          '  COMPANIA              NUMBER default 0'||
                          ')'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';                     
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                          
/*           lvSentencia := 'create index IDX_CARTERA_CLIENTES on CO_REPCARCLI_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' (CUENTA)'||
                          ' tablespace DATA'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                
           lvSentencia := 'create public synonym CO_REPCARCLI_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' for sysadm.CO_REPCARCLI_'||to_char(pdFechaPeriodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_REPCARCLI_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
*/           
           return 1;
           
    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA_TEST;
/

