CREATE
OR REPLACE VIEW CIBER_CDR_REJECT_DESC_FIXED AS
select
    udr_id,
    reject_batch_id,
    status,
    invalid_field_id,
    ciber_reject_reason_id,
    invalid_field_value,
    additional_description,
    daily_surchARGE_IND
from
    ciber_cdr_reject_description
where
    status = '3'
    AND DAILY_SURCHARGE_IND = 'N';