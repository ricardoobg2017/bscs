CREATE OR REPLACE VIEW NETWORKTOPRODUCT AS 
(
    select
        distinct tm.tmcode,
        pl.plcode
    from
        mputmtab tm,
        mpdpltab pl
    where
        tm.tmcode not in (
            select
                distinct nxg.tmcode
            from
                mpulknxg nxg
            where
                nxg.typeind = 'N'
                and nxg.sncode is null
        )
    UNION ALL
    select
        distinct nxg.tmcode,
        nxg.plcode
    from
        mpulknxg nxg
    where
        nxg.typeind = 'N'
        and nxg.tmcode is not null
) WITH READ ONLY;