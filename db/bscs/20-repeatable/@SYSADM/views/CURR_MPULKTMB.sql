CREATE OR REPLACE VIEW CURR_MPULKTMB AS 
select
   "TMCODE",
   "VSCODE",
   "VSDATE",
   "STATUS",
   "SPCODE",
   "SNCODE",
   "SUBSCRIPT",
   "ACCESSFEE",
   "EVENT",
   "ECHIND",
   "AMTIND",
   "FRQIND",
   "SRVIND",
   "PROIND",
   "ADVIND",
   "SUSIND",
   "LTCODE",
   "PLCODE",
   "BILLFREQ",
   "FREEDAYS",
   "ACCGLCODE",
   "SUBGLCODE",
   "USGGLCODE",
   "ACCJCID",
   "USGJCID",
   "SUBJCID",
   "CSIND",
   "CLCODE",
   "BILL_FMT",
   "ACCSERV_CATCODE",
   "ACCSERV_CODE",
   "ACCSERV_TYPE",
   "USGSERV_CATCODE",
   "USGSERV_CODE",
   "USGSERV_TYPE",
   "SUBSERV_CATCODE",
   "SUBSERV_CODE",
   "SUBSERV_TYPE",
   "DEPOSIT",
   "INTERVAL_TYPE",
   "INTERVAL",
   "SUBGLCODE_DISC",
   "ACCGLCODE_DISC",
   "USGGLCODE_DISC",
   "SUBGLCODE_MINCOM",
   "ACCGLCODE_MINCOM",
   "USGGLCODE_MINCOM",
   "SUBJCID_DISC",
   "ACCJCID_DISC",
   "USGJCID_DISC",
   "SUBJCID_MINCOM",
   "ACCJCID_MINCOM",
   "USGJCID_MINCOM",
   "PV_COMBI_ID",
   "PRM_PRINT_IND",
   "PRINTSUBSCRIND",
   "PRINTACCESSIND",
   "REC_VERSION",
   "PREPAID_SERVICE_IND",
   "INSERTIONDATE",
   "LASTMODDATE"
from
    MPULKTMB x
where
    x.vscode = (
        select
            max(vscode)
        from
            MPULKTMB z
        where
            x.tmcode = z.tmcode
            and x.sncode = 2
    )
    and x.sncode = 2;