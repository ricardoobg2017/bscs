CREATE OR REPLACE VIEW TT  AS 
select
    a.co_id,
    b.sncode,
    a.status,
    a.valid_from_date,
    b.sncode SN,
    b.status ST,
    b.valid_from_date VF
from
    pr_serv_status_hist a,
    pr_serv_status_hist b
where
    -- a.co_id = 138109     and 
    a.co_id = b.co_id
    and a.sncode = b.sncode
    and a.status = 'A'
    and b.status = 'D'
    and a.valid_from_date > b.valid_from_date;