CREATE OR REPLACE VIEW ERMES_ALERT AS 
select
    cd.port_id port_id,
    ps.CHANNEL_NUM alert,
    ps.CHANNEL_EXCL excl,
    count(*) total,
    sum(decode(ps.CHANNEL_EXCL, NULL, 0, 1)) total_excl
from
    profile_service ps,
    curr_co_status st,
    contr_devices cd
where
    ps.co_id = st.co_id
    and st.ch_status != 'd'
    and cd.co_id = ps.co_id
    and cd.cd_deactiv_date is null
    and (
        cd.cd_validfrom = (
            select
                max(cd_validfrom)
            from
                contr_devices
            where
                co_id = cd.co_id
                and cd_validfrom <= sysdate
        )
        or cd.cd_pending_state = 'a'
    )
    and ps.CHANNEL_NUM is not null
group by
    cd.port_id,
    ps.CHANNEL_NUM,
    ps.CHANNEL_EXCL;