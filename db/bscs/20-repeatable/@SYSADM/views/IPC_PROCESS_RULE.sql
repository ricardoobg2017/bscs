CREATE
OR REPLACE VIEW IPC_PROCESS_RULE(
    to_be_started,
    running,
    action,
    priority,
    must_param_running,
    mustnot_param_running,
    must_param_to_be_started,
    mustnot_param_to_be_started
) AS
SELECT
    AP1.APP_PROGRAM_NAME,
    AP2.APP_PROGRAM_NAME,
    AM.MLD_ACTION,
    AM.PRIORITY,
    AM.MANDAT_PARAM_RUNNING,
    AM.FORBID_PARAM_RUNNING,
    AM.MANDAT_PARAM_TO_BE_STARTED,
    AM.FORBID_PARAM_TO_BE_STARTED
FROM
    APP_PROGRAM AP1,
    APP_PROGRAM AP2,
    APP_MLD_RULE AM
WHERE
    AP1.APP_PROGRAM_ID = AM.PROGRAM_ID_TO_BE_STARTED
    AND AP2.APP_PROGRAM_ID = AM.PROGRAM_ID_RUNNING;