CREATE OR REPLACE VIEW MPV_PROFILE AS 
SELECT
    SC.SCCODE,
    decode(U.DURAAIH, NULL, D.DURAAIH, U.DURAAIH) DURAAIH,
    decode(
        U.DURALEFTMAXAGE,
        NULL,
        D.DURALEFTMAXAGE,
        U.DURALEFTMAXAGE
    ) DURALEFTMAXAGE,
    decode(
        U.ASSEMBLY_GAP,
        NULL,
        D.ASSEMBLY_GAP,
        U.ASSEMBLY_GAP
    ) ASSEMBLY_GAP,
    UDS_MEMBER_CODE,
    UDS_ELEMENT_CODE,
    THRESHOLD_VALUE
FROM
    MPUPFTAB U,
    MPUPFTAB D,
    MPDSCTAB SC,
    UDC_MARKET_THRESHOLD UMT
WHERE
    D.ENTRY_ID = (
        SELECT
            MAX(ENTRY_ID)
        FROM
            MPUPFTAB
        WHERE
            SCCODE IS NULL
    )
    AND (
        U.ENTRY_ID = (
            SELECT
                MAX(ENTRY_ID)
            FROM
                MPUPFTAB X
            WHERE
                U.SCCODE = X.SCCODE
        )
        OR U.SCCODE is NULL
    )
    AND D.SCCODE IS NULL
    AND decode(
        U.THRESHOLD_SET_ID,
        NULL,
        D.THRESHOLD_SET_ID,
        U.THRESHOLD_SET_ID
    ) = UMT.THRESHOLD_SET_ID
    AND U.SCCODE (+) = SC.SCCODE
    AND SC.SCNETIND = 'Y';