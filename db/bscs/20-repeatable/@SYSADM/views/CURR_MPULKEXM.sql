CREATE OR REPLACE VIEW CURR_MPULKEXM  AS 
SELECT
   "ECCODE",
   "VSDATE",
   "IMCODE",
   "IPCODE",
   "OMCODE",
   "OPCODE",
   "ECGLCODE",
   "BM_ID",
   "CUSTOMER_ID",
   "TIME_SLICE",
   "REC_VERSION"
FROM
    MPULKEXM m
WHERE
    m.VSDATE = (
        SELECT
            MAX(mi.VSDATE)
        FROM
            MPULKEXM mi
        WHERE
            m.ECCODE = mi.ECCODE
    ) WITH READ ONLY;