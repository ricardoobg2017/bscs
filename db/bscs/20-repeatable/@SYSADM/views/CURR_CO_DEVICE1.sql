CREATE OR REPLACE VIEW CURR_CO_DEVICE1  AS 
SELECT
    CD_ID,
    CD_SEQNO,
    CO_ID,
    PORT_ID,
    DN_ID,
    EQ_ID,
    CD_STATUS,
    CD_ACTIV_DATE,
    CD_DEACTIV_DATE,
    CD_VALIDFROM,
    CD_ENTDATE,
    CD_MODDATE,
    CD_USERLASTMOD,
    CD_SM_NUM,
    CD_CHANNELS,
    CD_CHANNELS_EXCL,
    CD_EQ_NUM,
    CD_PENDING_STATE,
    CD_RS_ID,
    CD_PLCODE,
    HLCODE
FROM
    CONTR_DEVICES CDX
WHERE
    CDX.CD_SEQNO = (
        SELECT
            MAX(CDY.CD_SEQNO)
        FROM
            CONTR_DEVICES CDY
        WHERE
            CDY.CD_VALIDFROM <= SYSDATE
            AND CDY.CO_ID = CDX.CO_ID
            AND CDY.CD_ID = CDX.CD_ID
    ) WITH READ ONLY;