CREATE OR REPLACE VIEW PRICINGSCHEMEHIERARCHY  (parent_id, child_id) AS 
(
    select
        distinct tmcode,
        SPCODE
    from
        CURR_MPULKTMB
) WITH READ ONLY;