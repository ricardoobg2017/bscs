CREATE OR REPLACE VIEW CTA_ACT_BSCS AS 
select
    /*+ RULE +*/
    distinct a.custcode,
    a.customer_id,
    a.csentdate "ACTIVA_CTA",
    a.cstype 
    --select     count(*)
from
    customer_all a
where
    a.paymntresp = 'X'
    and nvl(a.customer_id_high, 0) = 0
order by
    a.custcode,
    3 desc;