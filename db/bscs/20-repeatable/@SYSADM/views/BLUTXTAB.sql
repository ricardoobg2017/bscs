CREATE OR REPLACE VIEW BLUTXTAB AS 
SELECT
  "TEXT_ID",
  "SHDES",
  "ENTDATE",
  "MODDATE",
  "MODIFIED",
  "REWARD",
  "MLG_ID",
  "REC_VERSION"
FROM
    BLUTXTAB_ALL
WHERE
    MODIFIED IS NULL
    OR MODIFIED != 'D';