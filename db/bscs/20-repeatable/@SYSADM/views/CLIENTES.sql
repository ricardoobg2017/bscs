CREATE OR REPLACE VIEW CLIENTES AS 
select
    document_id,
    type_id,
    nvl(contract_id, 0) contract_id
from
    TIMM_PROCESO
where
    ID_PROCESO = 6
    AND PROCESADO = 0 with READ ONLY;