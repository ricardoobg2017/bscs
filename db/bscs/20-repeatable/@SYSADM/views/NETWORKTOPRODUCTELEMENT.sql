CREATE OR REPLACE VIEW NETWORKTOPRODUCTELEMENT AS 
(
    select
        distinct sn.sncode,
        pl.plcode
    from
        mpusntab sn,
        mpdpltab pl
    where
        sn.sncode not in (
            select
                distinct nxg.sncode
            from
                mpulknxg nxg
            where
                nxg.typeind = 'N'
                and nxg.tmcode is null
        )
    UNION ALL
    select
        distinct nxg.sncode,
        nxg.plcode
    from
        mpulknxg nxg
    where
        nxg.typeind = 'N'
        and nxg.sncode is not null
) WITH READ ONLY;