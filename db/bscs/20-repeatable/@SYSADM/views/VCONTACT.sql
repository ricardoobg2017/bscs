CREATE OR REPLACE VIEW VCONTACT AS 
SELECT
     "VENDCODE",
    "VCSEQ",
    "VCNAME",
    "VCADDR1",
    "VCADDR2",
    "VCADDR3",
    "VCCITY",
    "VCST",
    "VCZIP",
    "VCCOUNTRY",
    "VCCRC",
    "VCTN",
    "VCTN2",
    "VCFAX",
    "VCTITLE",
    "VCJOBDESC",
    "VCFNAME",
    "VCLNAME",
    "VCRMT",
    "VCSHIP",
    "VCORD",
    "VCCRED",
    "VCSFTY",
    "VCLAB",
    "VCENTDATE",
    "VCMODDATE",
    "VCMOD",
    "REC_VERSION"
FROM
    VCONTACT_ALL
WHERE
    VCMOD IS NULL
    OR VCMOD != 'D';