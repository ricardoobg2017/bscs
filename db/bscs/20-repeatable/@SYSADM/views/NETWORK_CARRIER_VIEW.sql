CREATE OR REPLACE VIEW NETWORK_CARRIER_VIEW AS 
SELECT
    ECCODE,
    DES
FROM
    MPDECTAB
UNION
SELECT
    -1,
    SOURCE_TYPE_DES
FROM
    UDC_SOURCE_TYPE_TABLE
WHERE
    SOURCE_TYPE_CODE = 'H' WITH READ ONLY;