CREATE OR REPLACE VIEW CURR_CO_PLAN  AS 
select
    co_id,
    tmcode,
    a.tmcode_date
from
    rateplan_hist a
where
    tmcode_date < to_date ('25/06/2007', 'DD/MM/YYYY')
    and a.seqno =(
        select
            max (b.seqno)
        from
            rateplan_hist b
        where
            tmcode_date < to_date ('25/06/2007', 'DD/MM/YYYY')
            and b.co_id = a.co_id
    );