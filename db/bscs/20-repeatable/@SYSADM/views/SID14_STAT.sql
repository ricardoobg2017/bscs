create or replace view sid14_stat as
select a.name,b.value
from v$statname a,v$sesstat b
where  b.sid = 220
and a.statistic# = b.statistic#
and b.statistic# = 4;