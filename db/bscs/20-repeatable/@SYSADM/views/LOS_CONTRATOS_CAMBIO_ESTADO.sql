CREATE OR REPLACE VIEW LOS_CONTRATOS_CAMBIO_ESTADO AS 
select
    co_id,
    count(*) total
from
    contract_history
where
    trunc(ch_validfrom) > to_date ('24/06/2004', 'DD/MM/YYYY')
    and ch_status = 'a'
    and co_id in (
        select
            co_id
        from
            contract_history
        where
            ch_status <> 'o'
        group by
            co_id
        having
            count(*) > 2
    )
group by
    co_id
    /*select distinct co_id from contract_history where trunc(ch_validfrom) > to_date ('24/04/2004','DD/MM/YYYY') and ch_status <> 'o'
    and co_id in
    (select co_id from contract_history where trunc(ch_validfrom) > to_date ('24/01/2004','DD/MM/YYYY') and ch_status <> 'o' 
    group by co_id having count(*) >1)*/
    /*select distinct co_id from contract_history where trunc(ch_validfrom) > to_date ('24/04/2004','DD/MM/YYYY') and ch_status <> 'o'
    and co_id in
    (select co_id from contract_history where trunc(ch_validfrom) > to_date ('24/04/2004','DD/MM/YYYY') and ch_status <> 'o' 
    group by co_id having count(*) >1)*/