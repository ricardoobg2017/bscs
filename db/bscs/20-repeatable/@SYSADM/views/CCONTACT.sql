CREATE
OR REPLACE VIEW SYSADM.CCONTACT AS
SELECT
    "CUSTOMER_ID",
    "CCSEQ",
    "CCTITLE",
    "CCNAME",
    "CCFNAME",
    "CCLNAME",
    "CCSTREET",
    "CCSTREETNO",
    "CCLNAMEMC",
    "CCADDR1",
    "CCADDR2",
    "CCADDR3",
    "CCCITY",
    "CCZIP",
    "CCCOUNTRY",
    "CCTN",
    "CCTN2",
    "CCFAX",
    "CCLINE1",
    "CCLINE2",
    "CCLINE3",
    "CCLINE4",
    "CCLINE5",
    "CCLINE6",
    "CCTN_AREA",
    "CCTN2_AREA",
    "CCFAX_AREA",
    "CCJOBDESC",
    "CCDEFTRK",
    "CCUSER",
    "CCBILL",
    "CCBILLDETAILS",
    "CCCONTRACT",
    "CCSHIP",
    "CCMAGAZINE",
    "CCDIRECTORY",
    "CCFORWARD",
    "CCURGENT",
    "COUNTRY",
    "CCLANGUAGE",
    "CCADDITIONAL",
    "SORT_CRITERIA",
    "CCENTDATE",
    "CCMODDATE",
    "CCMOD",
    "CCCOUNTY",
    "CCSTATE",
    "CCVALIDDATE",
    "CCBILL_PREVIOUS",
    "WELCOME_CRIT",
    "CCMNAME",
    "CCEMAIL",
    "CCADDRYEARS",
    "CCSMSNO",
    "CCBILLTEMP",
    "USERLASTMOD",
    "CCVALIDATION",
    "CCUSER_INST",
    "CCLOCATION_1",
    "CCLOCATION_2",
    "CCREMARK",
    "REC_VERSION",
    "MARITAL_STATUS",
    "CSNATIONALITY",
    "CSSOCIALSECNO",
    "CSDRIVELICENCE",
    "CSEMPLOYER",
    "EMPLOYEE",
    "COMPANY_TYPE",
    "ID_TYPE",
    "PASSPORTNO",
    "BIRTHDATE",
    "CSCOMPREGNO",
    "CSCOMPTAXNO",
    "CSCUSTTYPE",
    "CCSEX"
FROM
    CCONTACT_ALL
WHERE
    CCMOD IS NULL
    OR CCMOD != 'D';