CREATE OR REPLACE VIEW PLANES_SERVICIOS_IVR  AS 
select
    tmcode,
    sncode,
    accessfee
from
    mpulktmb g
where
    g.sncode in (1, 2, 5, 6)
    and g.tmcode in (
        select
            tmcode
        from
            rateplan minus
        select
            tmcode
        from
            rateplan
        where
            upper(des) like ('%CONTROL%')
    )
    and vscode = (
        select
            max(y.vscode)
        from
            mpulktmb y
        where
            y.tmcode = g.tmcode
    );