CREATE OR REPLACE VIEW GSI_V_UDR_PLAN_DATOS AS 
Select
    s_p_number_address,
    cust_info_customer_id,
    cust_info_contract_id,
    uds_stream_id,
    uds_record_id,
    uds_base_part_id,
    uds_charge_part_id,
    entry_date_timestamp,
    follow_up_call_type,
    initial_start_time_timestamp,
    initial_start_time_time_offset,
    o_p_normed_num_address,
    o_p_number_address,
    rated_flat_amount,
    rated_volume,
    service_logic_code,
    tariff_info_rpcode,
    tariff_info_rpversion,
    tariff_info_sncode,
    tariff_info_spcode,
    tariff_info_tmcode,
    tariff_info_zncode,
    tariff_info_zpcode,
    xfile_ind
From
    sysadm.udr_lt_201009_TAT_1 @bscs_to_rtx_link