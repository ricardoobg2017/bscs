CREATE OR REPLACE VIEW DIRECTORY_NUMBER_VIEW AS 
select
    D.DN_ID,
    D.PLCODE,
    D.NDC,
    D.HLCODE,
    D.DN_NUM,
    D.DN_STATUS,
    D.DN_STATUS_MOD_DATE,
    D.DEALER_ID,
    D.DN_ASSIGN_DATE,
    D.DN_PHONE_FLAG,
    D.DN_TYPE,
    D.DN_STATUS_REQU,
    D.DN_STA_REQU_DATE,
    D.DN_ASSIGN,
    D.DN_ENTDATE,
    D.DN_MODDATE,
    D.DN_MODFLAG,
    D.DN_AUTH,
    D.EVCODE,
    D.HMCODE,
    D.LOCAL_PREFIX_LEN,
    D.SIMPOH_ID,
    D.SIMPOT_ID,
    D.SIMPOT_BATCH,
    D.EXTERNAL_IND,
    D.BLOCK_IND,
    D.EXTENSION_LENGTH_MAX,
    D.REC_VERSION,
    D.DN_AUTH_PENDING,
    D.DIRNUM_NPCODE,
    D.VPN_ID,
    D.STATIC_PDP_REQUIRED,
    D.ROAMING_ALLOWED,
    M.NPCODE_CLASS,
    TRANSACTION_ID
from
    DIRECTORY_NUMBER D,
    MPDNPTAB M
where
    D.DIRNUM_NPCODE = M.NPCODE;