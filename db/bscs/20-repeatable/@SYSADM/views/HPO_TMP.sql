CREATE OR REPLACE VIEW HPO_TMP AS 
select
    zncode,
    zocode,
    zpcode,
    cgi,
    count(*) ocurrencia
from
    mpulkgvm
where
    zpcode = 2117
    and zncode in (38, 39)
group by
    zncode,
    zocode,
    zpcode,
    cgi
having
    count(*) > 1;