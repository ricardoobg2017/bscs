CREATE OR REPLACE VIEW PARAMETERTOPRODUCTELEMENT AS 
(
    select
        p.sncode,
        s.sccode,
        s.parameter_id,
        s.prm_no
    from
        mpusntab p,
        service_parameter s,
        mpulknxv l,
        mpssvtab d
    where
        p.sncode = l.sncode
        and (
            l.s1code = d.svcode
            or l.s2code = d.svcode
            or l.sscode = d.svcode
        )
        and d.svcode = s.svcode
) WITH READ ONLY;