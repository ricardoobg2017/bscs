CREATE OR REPLACE VIEW EXCH_FMT_RECFLD_TO_BSCS AS 
select
    a.from_exchange_format_id,
    a.from_record_id,
    a.from_field_id,
    a.from_field_value_id,
    b.field_value FROM_FIELD_VALUE,
    b.field_value_desc FROM_FIELD_VALUE_DESC,
    a.mapping_id,
    a.to_exchange_format_id,
    a.to_record_id,
    a.to_field_id,
    a.to_field_value_id,
    c.field_value TO_FIELD_VALUE,
    e.svcode TO_SVCODE,
    e.srvind TO_srvind,
    e.srvcode TO_srvcode,
    c.field_value_desc TO_FIELD_VALUE_DESC
from
    exchange_format_recfld_mapping a,
    exchange_format_field_value b,
    exchange_format_field_value c,
    mpssvtab e
where
    a.to_exchange_format_id = 0
    and a.from_exchange_format_id = b.exchange_format_id
    and a.from_field_id = b.field_id
    and a.from_field_value_id = b.field_value_id
    and c.exchange_format_id = e.exchange_format_id(+)
    and c.field_id = e.field_id(+)
    and c.field_value_id = e.field_value_id(+)
    and a.to_exchange_format_id = c.exchange_format_id
    and a.to_field_id = c.field_id
    and a.to_field_value_id = c.field_value_id;