CREATE
OR REPLACE VIEW UDM_CALL_DEST_VIEW (o_p_number_type_of_number, call_dest) AS
SELECT
    1,
    'IP'
FROM
    DUAL
UNION
SELECT
    0,
    'NP'
FROM
    DUAL;