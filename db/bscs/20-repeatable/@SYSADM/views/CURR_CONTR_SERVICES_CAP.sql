CREATE OR REPLACE VIEW CURR_CONTR_SERVICES_CAP AS 
(
    SELECT
       "CO_ID",
       "SNCODE",
       "SEQNO",
       "SEQNO_PRE",
       "BCCODE",
       "PENDING_BCCODE",
       "DN_ID",
       "DN_BLOCK_ID",
       "MAIN_DIRNUM",
       "CS_STATUS",
       "CS_ACTIV_DATE",
       "CS_DEACTIV_DATE",
       "CS_REQUEST",
       "REC_VERSION",
       "PUBLIC_NUMBER_DN_ID",
       "VPN_ID",
       "PROFILE_ID",
       "DN_SHOWN_IN_BILL"
    FROM
        CONTR_SERVICES_CAP CSC
    WHERE
        CSC.SEQNO NOT IN (
            SELECT
                SEQNO_PRE
            FROM
                CONTR_SERVICES_CAP
            WHERE
                CO_ID = CSC.CO_ID
                AND SNCODE = CSC.SNCODE
                AND PROFILE_ID = CSC.PROFILE_ID
                AND SEQNO_PRE IS NOT NULL
        )
) WITH READ ONLY;