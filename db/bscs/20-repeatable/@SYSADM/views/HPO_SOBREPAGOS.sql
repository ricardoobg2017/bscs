CREATE OR REPLACE VIEW HPO_SOBREPAGOS AS 
select
    cadxact,
    cadoxact,
    sum(cadamt_doc) Monto
from
    cashdetail
where
    cadxact in (
        select
            caxact
        from
            cashreceipts
        where
            customer_id = 3678
            and catype = 3
    )
group by
    cadxact,
    cadoxact;