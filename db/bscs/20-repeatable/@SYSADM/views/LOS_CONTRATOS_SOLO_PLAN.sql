CREATE OR REPLACE VIEW LOS_CONTRATOS_SOLO_PLAN AS 
select
    co_id,
    total
from
    los_contratos_cambio_plan minus
select
    co_id,
    total
from
    Los_Contratos_Cambio_estado;