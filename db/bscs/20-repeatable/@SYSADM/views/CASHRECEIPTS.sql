CREATE
OR REPLACE VIEW SYSADM.CASHRECEIPTS AS
SELECT
    "CAXACT",
    "CUSTOMER_ID",
    "CAENTDATE",
    "CARECDATE",
    "CACHKNUM",
    "CACHKDATE",
    "CAGLCASH",
    "CAGLDIS",
    "CATYPE",
    "CABATCH",
    "CAREM",
    "CAPOSTGL",
    "CAPP",
    "CABANKNAME",
    "CABANKACC",
    "CABANKSUBACC",
    "CAUSERNAME",
    "CAAPPLICATION",
    "CATRANSFER",
    "CAJOBCOST",
    "CADEBIT_INFO1",
    "CADEBIT_DATE",
    "CADEBIT_INFO2",
    "CAMOD",
    "CAMICROFICHE",
    "CAPAYM_PLACE",
    "CAGLEXACT",
    "CACOSTCENT",
    "CAREASONCODE",
    "CADOCREFNUM",
    "CAPRINTED",
    "CAPRINTEDBY",
    "CAGLEXACT_TAX",
    "CAXACT_RELATED_TRANSFER",
    "PAYMENT_CURRENCY",
    "GL_CURRENCY",
    "CONVRATETYPE_GL",
    "CONVRATETYPE_DOC",
    "CACHKAMT_GL",
    "CADISAMT_GL",
    "CACURAMT_GL",
    "CACHKAMT_PAY",
    "CADISAMT_PAY",
    "CACURAMT_PAY",
    "BALANCE_EXCH_DIFF_GL",
    "BALANCE_EXCH_DIFF_GLACODE",
    "BALANCE_EXCH_DIFF_JCID",
    "CABALANCE_HOME",
    "CURRENCY",
    "REC_VERSION"
FROM
    CASHRECEIPTS_ALL
WHERE
    CAMOD != 'D';