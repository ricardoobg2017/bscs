CREATE OR REPLACE VIEW COMPOSITEPRODUCTHANDLE  AS 
(
    select
        distinct TMCODE
    from
        MPULKTMB
    WHERE
        STATUS = 'P'
) WITH READ ONLY;