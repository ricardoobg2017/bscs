CREATE
OR REPLACE VIEW UDS_ELEMENT_VIEW (
    uds_element_code,
    uds_element_name,
    uds_element_des,
    data_class_id,
    data_class_des,
    data_class_type,
    data_type_id,
    data_type_code,
    data_type_des,
    string_length,
    reference_value_type
) AS
select
    UDE.UDS_ELEMENT_CODE,
    UDE.UDS_ELEMENT_NAME,
    NVL(UDE.UDS_ELEMENT_DES, UDE.UDS_ELEMENT_NAME),
    DAC.DATA_CLASS_ID,
    DAC.DESCRIPTION,
    DAC.DATA_CLASS_TYPE,
    DAT.DATA_TYPE_ID,
    DAT.DATA_TYPE_CODE,
    DAT.DESCRIPTION,
    UDE.STRING_LENGTH,
    DAT.REFERENCE_VALUE_TYPE
from
    UDS_ELEMENT UDE,
    DATA_CLASS DAC,
    DATA_TYPE DAT
where
    UDE.DATA_CLASS_ID = DAC.DATA_CLASS_ID
    and DAC.DATA_TYPE_ID = DAT.DATA_TYPE_ID WITH READ ONLY;