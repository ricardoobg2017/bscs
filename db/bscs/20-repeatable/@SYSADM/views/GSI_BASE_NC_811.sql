CREATE OR REPLACE VIEW GSI_BASE_NC_811  AS 
select
    "CUENTA",
    "NOMBRE",
    "DIRECCIONFACTLINEA1",
    "COMPA?IA",
    "ARCHIVO_FISICO",
    "SECUENCIAL_EN_ARCHIVO",
    "FACTURA",
    "CUSTCODE",
    "BILLCYCLE",
    "VALOR_TOTAL",
    "IVA",
    "VALOR_SIN",
    "RUC",
    "CUSTOMER_ID"
from
    gsi_base_nc
where
    archivo_fisico = 'INV_39_20071108_GYE_1_3_0_0911072129_00.ps'
order by
    secuencial_en_archivo asc;