CREATE OR REPLACE VIEW MKT_PARAMETER_VIEW AS 
select
    t0.sccode,
    t0.parameter_id,
    t0.prm_des,
    t0.port_ind,
    t0.net_ind,
    t0.charge_ind,
    t0.cug_ind,
    t0.entry_date,
    t0.modify_date,
    t0.modify_user,
    t0.parameter_change_ind,
    t0.value_change_ind,
    t0.vas_ind,
    t0.rec_version,
    t0.multiple_values_ind,
    t0.max_number_parameter,
    t0.min_number_parameter,
    t0.resource_like,
    t0.response_prm_ind,
    t0.print_ind,
    t0.override_ind,
    t0.listboxformat_id,
    t1.parameter_area_id,
    t1.data_type_id,
    t2.parameter_type_id,
    t2.parameter_type_code,
    t2.description,
    t3.range_from,
    t3.range_to,
    t3.prm_default,
    t3.prm_format,
    t3.prm_picture_format,
    t0.RATING_IND
from
    mkt_parameter t0,
    parameter_area t1,
    parameter_type t2,
    mkt_parameter_range t3
where
    t0.parameter_id = t3.parameter_id (+)
    and t0.sccode = t3.sccode (+)
    and t0.parameter_area_id = t1.parameter_area_id
    and t1.parameter_type_id = t2.parameter_type_id;