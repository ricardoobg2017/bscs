CREATE OR REPLACE  VIEW MPULKGVM_HPO1 AS 
select
  "GVCODE",
    "VSCODE",
    "VSDATE",
    "ZNCODE",
    "ZOCODE",
    "ZPCODE",
    "GZCODE",
    "GZNCODE",
    "ZODES",
    "CGI",
    "ZPDES",
    "DIGITS",
    "OVERRIDE_GZ",
    "DEST_NPCODE",
    "REC_VERSION",
    "ORIGIN_NPCODE"
from
    mpulkgvm
where
    gvcode <> 4;