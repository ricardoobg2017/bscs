CREATE
OR REPLACE VIEW INTEREST_RATE AS
select
    "INT_RATE_ID",
    "INT_RATE",
    "INT_EFFECT_DATE",
    "INT_USERLASTMOD",
    "INT_ENTDATE",
    "INT_LASTMODDATE",
    "INT_RATE_MOD",
    "REC_VERSION"
from
    interest_rate_all
where
    int_rate_mod != 'D';