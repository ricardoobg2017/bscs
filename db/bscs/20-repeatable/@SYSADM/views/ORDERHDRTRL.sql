CREATE OR REPLACE VIEW ORDERHDRTRL AS 
SELECT
    OHXACT,
    OHSTATUS,
    OHSTATUSFLG,
    OHENTDATE,
    OHREFNUM,
    OHTSHDATE,
    OHSLS,
    CUSTOMER_ID,
    OHCSEQ,
    OHSNAME,
    OHSADDR1,
    OHSADDR2,
    OHSADDR3,
    OHSCITY,
    OHSST,
    OHSZIP,
    OHSCOUNTRY,
    OHSCRC,
    OHATTN,
    OHTERMS,
    OHTRUCKER,
    OHPONUM,
    OHRELEASE,
    OHPSNO,
    OHFRTTERMS,
    OHORDTYP,
    OHCANDATE,
    OHASSOCXACT,
    OHPRT1,
    OHPRT2,
    OHPRT3,
    OHUSERID,
    OHREFDATE,
    OHSHPDATE,
    OHDUEDATE,
    OHIPP,
    OHILPP,
    OHPOSTGL,
    OHPOSTAR,
    OHARCUSTOMER_ID,
    OHINVAMT_GL,
    OHINVAMT_DOC,
    OHGLAR,
    OHOPNAMT_GL,
    OHOPNAMT_DOC,
    OHDISAMT_GL,
    OHDISAMT_DOC,
    OHDISTAKAMT_GL,
    OHDISTAKAMT_DOC,
    OHDISDATE,
    DDEBIT_FLAG,
    OHARFLG,
    OHMOD,
    OHINVTYPE,
    OHFULFILDATE,
    OHPAYMENT_ID,
    OHPOSTFIBU,
    OHREASON,
    OHRS_ID,
    OHDDHFLAG,
    OHDDHDATE,
    OHFCT,
    OHLCT,
    PAYMENTFAILED,
    OTXACT,
    OTSEQ,
    OTMEAS,
    OTUM,
    OTUP,
    OTQORD,
    OTQBKORD,
    OTQTOSHP,
    OTDFTPRC,
    OTPRGROUP,
    OTGPRICE_DOC,
    OTFRTWGT,
    OTJOBCOST,
    OTNAME,
    OTGLSALE,
    OTGLCOGS,
    OTGLINV,
    OTQSHIP,
    OTPRMEAS,
    OTPRUM,
    OTBILLUNITS,
    OTINVUNITS,
    OTCOSTFLG,
    OTINVALLOC,
    OTMERCH_GL,
    OTSHIPDATE,
    SNCODE,
    OTEQ_ID,
    OTEQRATE,
    OTLEASING,
    OTLEAS_FINISH,
    otflsqn,
    otvplmn,
    OTFLNAME,
    OHGLEXACT,
    OHDUNSTEP,
    OHFLFCT,
    OHFLLCT,
    OTFCT,
    OTLCT,
    OTSIM_FROM,
    OTSIM_TO,
    CO_ID,
    OTCURAMT_GL,
    OTCURAMT_DOC,
    COMPLAINT,
    COMPLAINT_DATE,
    RTX_STATUS,
    OHBILLSEQNO,
    OHCANCELFLAG,
    oh.GL_CURRENCY,
    oh.DOCUMENT_CURRENCY,
    CONVDATE_EXCHANGE,
    OTMERCH_DOC,
    OTMERCH_GROSS_GL,
    OTMERCH_GROSS_DOC,
    OTCURAMT_GROSS_GL,
    OTCURAMT_GROSS_DOC,
    OHGLEXACT_TAX,
    OHTAXAMT_GL,
    OHTAXAMT_DOC,
    OTTAX_EXEMPTED_DOC,
    GLACODE_DIFF,
    JOBCOST_ID_DIFF,
    TAXAMT_DIFF_GL,
    OTTRANSACTION_DATE,
    SERVCAT_CODE,
    SERV_CODE,
    SERV_TYPE,
    TAX_INCLUSIVE_IND,
    TAX_ROUNDING_FLAG,
    OTGPRICE_DIS_DOC,
    OTGL_DISC,
    OTJCID_DISC,
    OTGL_MINCOM,
    OTJCID_MINCOM,
    OTAMT_REVENUE_GL,
    OTAMT_REVENUE_GROSS_GL,
    OTAMT_REVENUE_DOC,
    OTAMT_REVENUE_GROSS_DOC,
    OTCURAMT_REVENUE_GL,
    OTCURAMT_REVENUE_DOC,
    OTCURAMT_REVENUE_GROSS_GL,
    OTCURAMT_REVENUE_GROSS_DOC,
    OTAMT_DISC_GL,
    OTAMT_DISC_GROSS_GL,
    OTAMT_DISC_DOC,
    OTAMT_DISC_GROSS_DOC,
    OTCURAMT_DISC_GL,
    OTCURAMT_DISC_DOC,
    OTCURAMT_DISC_GROSS_GL,
    OTCURAMT_DISC_GROSS_DOC,
    OTAMT_MINCOM_GL,
    OTAMT_MINCOM_GROSS_GL,
    OTAMT_MINCOM_DOC,
    OTAMT_MINCOM_GROSS_DOC,
    OTCURAMT_MINCOM_GL,
    OTCURAMT_MINCOM_DOC,
    OTCURAMT_MINCOM_GROSS_GL,
    OTCURAMT_MINCOM_GROSS_DOC
FROM
    ORDERHDR_ALL oh,
    ORDERTRAILER ot
WHERE
    OHMOD != 'D'
    AND ohmod is not null
    AND ohxact = otxact;