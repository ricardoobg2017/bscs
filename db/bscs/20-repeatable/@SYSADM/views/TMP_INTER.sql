CREATE OR REPLACE VIEW TMP_INTER AS 
select
    custcode
from
    co_fact_24102003
where
    nombre like 'Inter%'
group by
    custcode
having
    sum(valor) <= 2.50;