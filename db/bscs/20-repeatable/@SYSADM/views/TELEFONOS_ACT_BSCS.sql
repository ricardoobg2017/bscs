create
or replace view telefonos_act_bscs as
select
    /*+ RULE +*/
    e.custcode,
    substr(c.dn_num, 5) "TELEFONO",
    e.csentdate "ACTIVA_CTA",
    a.co_entdate "ACTIVA_TEL",
    d.ch_status,
    e.customer_id,
    a.co_id,
    d.ch_reason
from
    contract_all a,
    contr_services_cap b,
    directory_number c,
    curr_co_status d,
    customer_all e
where
    a.customer_id = e.customer_id
    and a.co_id = b.co_id
    and a.co_id = d.co_id
    and b.dn_id = c.dn_id
    and e.cstype = 'a'
    and trunc(d.ch_validfrom) = (
        select
            max(trunc(dd.ch_validfrom))
        from
            contr_services_cap bb,
            curr_co_status dd
        where
            bb.co_id = dd.co_id
            and bb.dn_id = b.dn_id
    )
order by
    1,
    3 desc,
    4 desc;