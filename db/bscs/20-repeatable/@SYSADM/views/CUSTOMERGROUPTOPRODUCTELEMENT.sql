CREATE OR REPLACE VIEW CUSTOMERGROUPTOPRODUCTELEMENT AS 
(
    select
        distinct sn.sncode,
        prg.prgseqnum
    from
        mpusntab sn,
        pricegroup prg
    where
        sn.sncode not in (
            select
                distinct nxg.sncode
            from
                mpulknxg nxg
            where
                nxg.typeind = 'G'
                and nxg.tmcode is null
        )
    UNION ALL
    select
        distinct nxg.sncode,
        nxg.prgcode
    from
        mpulknxg nxg
    where
        nxg.typeind = 'G'
        and nxg.sncode is not null
) WITH READ ONLY;