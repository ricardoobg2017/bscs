CREATE
OR REPLACE VIEW TAX_REFERENCE_VIEW (
    tax_group_id,
    valid_from,
    invalid_from,
    tax_group_version,
    tax_group_threshold,
    lower_bound,
    lower_bound_currency,
    taxcat_id,
    taxcode,
    tax_seqno,
    taxrate,
    tax_amount,
    tax_amount_currency,
    glacode,
    zero_tax_default,
    legalcode,
    taxtype_id,
    taxcode_version_des,
    taxcat_name,
    jurisdiction_type_id
) AS
SELECT
    TGV.TAX_GROUP_ID,
    GREATEST(TCV.VALID_FROM, TGV.VALID_FROM),
    LEAST(TCV.INVALID_FROM, TGV.INVALID_FROM),
    TGV.TAX_GROUP_VERSION_ID,
    TGT.TAX_GROUP_THRESHOLD_ID,
    TGT.LOWER_BOUND,
    TGT.LOWER_BOUND_CURRENCY,
    TCV.TAXCAT_ID,
    TCV.TAXCODE,
    TCV.TAX_SEQNO,
    TCV.TAXRATE,
    TCV.TAX_AMOUNT,
    TCV.TAX_AMOUNT_CURRENCY,
    TCV.GLACODE,
    TCV.ZERO_TAX_DEFAULT,
    TCV.LEGALCODE,
    TCV.TAXTYPE_ID,
    TCV.TAXCODE_VERSION_DES,
    TCV.TAXCAT_NAME,
    TCV.JURISDICTION_TYPE_ID
FROM
    TAX_GROUP_VIEW TGV,
    TAX_GROUP_THRESHOLD TGT,
    TAX_CODE_REFERENCE TGR,
    TAX_CODE_VIEW TCV
WHERE
    TGV.TAX_GROUP_ID = TGT.TAX_GROUP_ID
    AND TGV.TAX_GROUP_VERSION_ID = TGT.TAX_GROUP_VERSION_ID
    AND TGT.TAX_GROUP_THRESHOLD_ID = TGR.TAX_GROUP_THRESHOLD_ID
    AND TGR.TAXCODE = TCV.TAXCODE
    AND TCV.INVALID_FROM > TGV.VALID_FROM
    AND TCV.VALID_FROM < TGV.INVALID_FROM WITH READ ONLY;