CREATE
OR REPLACE VIEW PRICINGELEMENT_VIEW (
    tmcode,
    spcode,
    sncode,
    fee,
    chargetypeid,
    amtind,
    vsdate,
    vscode,
    pv_combi_id
) AS (
    SELECT
        TMCODE,
        SPCODE,
        SNCODE,
        ACCESSFEE,
        3,
        AMTIND,
        VSDATE,
        VSCODE,
        PV_COMBI_ID
    from
        MPULKTMB
    where
        accessfee is not null
    UNION ALL
    SELECT
        TMCODE,
        SPCODE,
        SNCODE,
        SUBSCRIPT,
        1,
        AMTIND,
        VSDATE,
        VSCODE,
        PV_COMBI_ID
    from
        MPULKTMB
    where
        subscript is not null
    UNION ALL
    SELECT
        TMCODE,
        SPCODE,
        SNCODE,
        EVENT,
        5,
        AMTIND,
        VSDATE,
        VSCODE,
        PV_COMBI_ID
    from
        MPULKTMB
    where
        event is not null
) WITH READ ONLY;