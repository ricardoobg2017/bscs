CREATE OR REPLACE VIEW TMP_TOTALES_REPORTE AS 
select
    f.customer_id,
    sum(f.ohopnamt_doc + f.cm_ohinvamt_doc) suma
from
    reporte.rpt_carclietot_tmp f
group by
    f.customer_id;