CREATE OR REPLACE VIEW LOS_AFECTADOS AS 
select
    customer_id,
    seqno
from
    fees a
where
    sncode = 103
    and period <> 0
    and customer_id in (
        select
            customer_id
        from
            fees
        where
            period <> 0
            and sncode = 103
        group by
            customer_id
        having
            count (*) > 1
    )
order by
    customer_id,
    seqno;