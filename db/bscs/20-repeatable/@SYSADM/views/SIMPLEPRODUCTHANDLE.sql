CREATE OR REPLACE VIEW SIMPLEPRODUCTHANDLE AS 
(
    select
        distinct TMCODE,
        SPCODE
    from
        MPULKTMB
    WHERE
        STATUS = 'P'
) WITH READ ONLY;