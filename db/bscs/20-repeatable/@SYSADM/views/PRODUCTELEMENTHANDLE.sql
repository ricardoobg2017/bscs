CREATE OR REPLACE VIEW PRODUCTELEMENTHANDLE AS 
(
    select
        distinct TMCODE,
        SPCODE,
        SNCODE
    from
        MPULKTMB
    WHERE
        STATUS = 'P'
) WITH READ ONLY;