CREATE OR REPLACE VIEW PLANES_SERVICIOS_IVR2 AS 
select
    a.dn_num,
    b.dn_id,
    c.customer_id,
    b.co_id,
    c.tmcode,
    d.sncode,
    d.accessfee,
    e.units,
    e.pool,
    0 AIRE,
    0 MIN_ACUM,
    0 MIN_DIA,
    0 TOLL
from
    directory_number a,
    contr_services_cap b,
    contract_all c,
    planes_servicios_ivr d,
    free_units @bscs_to_rtx_link e
where
    dn_num in (
        select
            numero
        from
            numeros_temp1 @bscs_to_rtx_link
    )
    and e.tmcode = c.tmcode
    and a.dn_id = b.dn_id
    and b.cs_deactiv_date is null
    and c.co_id = b.co_id
    and c.tmcode = d.tmcode