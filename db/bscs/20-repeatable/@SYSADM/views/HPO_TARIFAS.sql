CREATE OR REPLACE VIEW HPO_TARIFAS AS 
select  p.tmcode Cod_PLN, p.vscode Ver_PLN,  r.des Des_PLN,
p.sncode, 
c.des Des_RIP, a.ricode Cod_RIP ,a.vscode Ver_RIP,a.vsdate Fec_RIP,
 h.zncode Cod_ZON, o.des Des_ZON ,
 h.interconnect_rating_mode ,
 j.ttcode Cod_HOR, m.des Des_HOR,
 j.rate_type_id Cod_TIP, n.rate_type_des Des_TIP,
 l.parameter_value_float Val_SEG,
 l.parameter_value_float*60 Val_MIN
  from 
 mpurivsd a, 
 mpuritab c,
 rate_pack_zone h, mpulkrim j,
 rate_pack_element k, rate_pack_parameter_value l,
 mputttab m, udc_rate_type_table n, mpuzntab o,
  mpulktmm p,
  rateplan r
  where 
 a.ricode=c.ricode and
 --a.vscode in (select max(vscode) from mpurivsd b where a.ricode=b.ricode) and
 c.ricode=h.ricode and
 a.vscode=h.vscode and 
 a.ricode=j.ricode and
 j.vscode=h.vscode and
 h.zncode=j.zncode and
 k.rate_pack_entry_id=j.rate_pack_entry_id and
 k.rate_pack_element_id=l.rate_pack_element_id and 
 m.ttcode=j.ttcode and
 n.rate_type_id=j.rate_type_id and
 p.ricode=j.ricode and
 --p.vscode in (select max(vscode) from mpulktmm q where p.tmcode=q.tmcode) and
  p.sncode in (55,91,61,62,63,64) --and tmcode=26
 and not p.ricode in(2,42) and
 r.tmcode=p.tmcode and
 o.zncode=h.zncode 
 --a.ricode=138 
 and l.parameter_seqnum=4 and l.parameter_rownum=1; 
