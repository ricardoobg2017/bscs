CREATE OR REPLACE VIEW DET_LLAMADAS08 AS 
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_GYE_B_NVIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_UIO_B_NVIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_GYE_VIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_UIO_VIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_GYE_NOVIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_UIO_NOVIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_GYE_B_VIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_UIO_B_VIP_D24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_GYE_BULK_REZ24 @axis
UNION ALL
select
    FACTURA,
    CUENTA,
    NOMBRES,
    APELLIDO1,
    APELLIDO2,
    GRUPO
from
    porta.BASE_COURIER08_UIO_BULK_REZ24 @axis