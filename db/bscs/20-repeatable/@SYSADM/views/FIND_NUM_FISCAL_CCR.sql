
CREATE OR REPLACE VIEW FIND_NUM_FISCAL_CCR AS 
select
    distinct a.customer_id,
    a.custcode,
    a.ohrefnum
from
    co_fact_24122006 a
where
    customer_id in (
        select
            a.customer_id
        from
            temp_cuenta_ccr a
    )
    and a.ohrefnum like '001-%'
union all
select
    distinct a.customer_id,
    a.custcode,
    'NO' as ohrefnum
from
    co_fact_24122006 a
where
    customer_id in (
        select
            a.customer_id
        from
            temp_cuenta_ccr a
    )
    and a.ohrefnum not like '001-%'