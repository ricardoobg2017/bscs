CREATE OR REPLACE VIEW SIMPLEPRODUCT_VIEW AS 
(
    select
        distinct TMCODE,
        SPCODE,
        VSDATE,
        VSCODE
    from
        MPULKTMB
    WHERE
        STATUS = 'P'
) WITH READ ONLY;