
CREATE OR REPLACE VIEW CUSTOMERGROUPTOPRODUCT AS 
(
    select
        distinct tm.tmcode,
        prg.prgseqnum
    from
        mputmtab tm,
        pricegroup prg
    where
        tm.tmcode not in (
            select
                distinct nxg.tmcode
            from
                mpulknxg nxg
            where
                nxg.typeind = 'G'
                and nxg.sncode is null
        )
    UNION ALL
    select
        distinct nxg.tmcode,
        nxg.prgcode
    from
        mpulknxg nxg
    where
        nxg.typeind = 'G'
        and nxg.tmcode is not null
) WITH READ ONLY;