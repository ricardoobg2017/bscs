CREATE OR REPLACE VIEW ASIGNADOS AS 
select
    b.custcode
from
    contract_all a,
    customer_all b
where
    a.tmcode in (73, 81, 91, 92, 281, 284, 205, 212)
    and a.customer_id = b.customer_id
group by
    b.custcode;