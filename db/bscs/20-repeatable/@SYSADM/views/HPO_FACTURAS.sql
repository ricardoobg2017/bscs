CREATE OR REPLACE VIEW HPO_FACTURAS  AS 
select
    ohxact,
    ohstatus,
    ohentdate,
    ohrefnum,
    ohinvamt_gl,
    f.ohopnamt_gl
from
    orderhdr_all f
where
    customer_id in(
        select
            customer_id
        from
            customer_all
        where
            custcode = '5.26792'
    )
    and ohstatus in ('CM', 'IN');