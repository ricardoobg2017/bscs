CREATE OR REPLACE VIEW TIMM_CLIENTES AS 
select
    document_id,
    type_id,
    nvl(contract_id, 0) contract_id,
    ID_PROCESO,
    procesado
from
    TIMM_PROCESO
WHERE
    PROCESADO = 0 with READ ONLY;