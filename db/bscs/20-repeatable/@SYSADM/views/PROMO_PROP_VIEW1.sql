CREATE OR REPLACE VIEW PROMO_PROP_VIEW1 (uds_member_code, uds_element_code, description) AS 
SELECT
    UDS_MEMBER_CODE,
    UDS_ELEMENT_CODE,
    COLUMN_NAME
FROM
    UDS_ITEM MINUS
SELECT
    A.UDS_MEMBER_CODE,
    A.UDS_ELEMENT_CODE,
    A.COLUMN_NAME
FROM
    UDS_ITEM A,
    PROMO_PROP_VIEW B,
    PROMO_PROP_TO_TYPE C
WHERE
    A.UDS_MEMBER_CODE = B.UDS_MEMBER_CODE
    AND A.UDS_ELEMENT_CODE = B.UDS_ELEMENT_CODE
    AND B.PROP_ID = C.PROP_ID MINUS
SELECT
    A.UDS_MEMBER_CODE,
    A.UDS_ELEMENT_CODE,
    A.COLUMN_NAME
FROM
    UDS_ITEM A,
    PROMO_PROP_VIEW B,
    PROMO_PROP_TO_TYPE C
WHERE
    B.UDS_MEMBER_CODE is NULL
    AND A.UDS_ELEMENT_CODE = B.UDS_ELEMENT_CODE
    AND B.PROP_ID = C.PROP_ID;