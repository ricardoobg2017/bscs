CREATE OR REPLACE  VIEW TAX_SERVICE AS 
select
    tr.taxcat_id TAXCAT_ID,
    tm.tmcode TMCODE,
    tm.shdes TMSHDES,
    tm.vscode VSCODE,
    sp.shdes SPSHDES,
    sn.sncode SNCODE,
    sn.shdes SNSHDES,
    sn.des SNDES,
    tr.taxcat_name TAXCATEGORY,
    tr.taxrate TAXRATE
from
    tax_reference_view tr,
    tax_group_item tg,
    service_taxation st,
    servicecatcode sc,
    mpulktmb lk,
    mpusntab sn,
    mputmtab tm,
    mpusptab sp
where
    lk.tmcode = tm.tmcode
    and lk.vscode = tm.vscode
    and lk.sncode = sn.sncode
    and lk.spcode = sp.spcode
    and sc.servcat_code = lk.accserv_catcode
    and sc.serv_code = lk.accserv_code
    and sc.serv_type = lk.accserv_type
    and st.servcat_id = sc.servcat_id
    and tg.servtax_id = st.servtax_id
    and tr.tax_group_id = tg.tax_group_id with read only;