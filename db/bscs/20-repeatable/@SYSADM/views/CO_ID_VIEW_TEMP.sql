CREATE OR REPLACE VIEW CO_ID_VIEW_TEMP AS 
select
    count(*) cant,
    a.tmcode
from
    contract_all a,
    curr_co_status b
where
    a.co_id = b.co_id
    and b.ch_status = 'a'
group by
    a.customer_id,
    a.tmcode;