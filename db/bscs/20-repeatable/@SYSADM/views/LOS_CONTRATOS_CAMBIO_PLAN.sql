CREATE OR REPLACE VIEW LOS_CONTRATOS_CAMBIO_PLAN AS 
select
    co_id,
    count(*) total
from
    rateplan_hist
where
    trunc(tmcode_date) > to_date ('24/06/2004', 'DD/MM/YYYY')
    and co_id in (
        select
            co_id
        from
            rateplan_hist --        where            trunc(tmcode_date) > to_date ('24/03/2004', 'DD/MM/YYYY')
        group by
            co_id
        having
            count(*) > 1
    )
group by
    co_id;