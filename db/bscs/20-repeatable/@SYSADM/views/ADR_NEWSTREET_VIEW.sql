CREATE
OR REPLACE VIEW ADR_NEWSTREET_VIEW (
    city_name,
    street_name,
    district_name,
    lower_number_num,
    upper_number_num,
    zip_code
) AS (
    SELECT
        C.CITY_NAME,
        S.STREET_NAME,
        D.DISTRICT_NAME,
        TO_NUMBER(NULL),
        TO_NUMBER(NULL),
        Z.ZIP_CODE
    FROM
        ADR_STREET S,
        ADR_STREET_ZIP Z,
        ADR_CITY C,
        ADR_DISTRICT D
    WHERE
        S.CITY_ID = Z.CITY_ID
        AND Z.CITY_ID = C.CITY_ID
        AND S.STREET_ID = Z.STREET_ID
        AND S.STREET_ZIP_REL = 1
        AND S.DISTRICT_ID = D.DISTRICT_ID(+)
        AND S.CITY_ID = D.CITY_ID(+)
    UNION
    SELECT
        C.CITY_NAME,
        S.STREET_NAME,
        D.DISTRICT_NAME,
        SEC.LOWER_NUMBER_NUM,
        SEC.UPPER_NUMBER_NUM,
        SEC.ZIP_CODE
    FROM
        ADR_STREET S,
        ADR_STREET_SEC SEC,
        ADR_CITY C,
        ADR_DISTRICT D
    WHERE
        S.CITY_ID = SEC.CITY_ID
        AND S.STREET_ID = SEC.STREET_ID
        AND SEC.CITY_ID = C.CITY_ID
        AND SEC.DISTRICT_ID = D.DISTRICT_ID(+)
        AND SEC.CITY_ID = D.CITY_ID(+)
);