CREATE
OR REPLACE VIEW UDS_MEMBER_VIEW (
    uds_member_code,
    uds_member_name,
    uds_member_des,
    uds_node_code,
    uds_type_code
) AS
select
    UDM.UDS_MEMBER_CODE,
    UDM.UDS_MEMBER_NAME,
    NVL(UDM.UDS_MEMBER_DES, UDM.UDS_MEMBER_NAME),
    UDM.UDS_NODE_CODE,
    UDM.UDS_TYPE_CODE
from
    UDS_MEMBER UDM WITH READ ONLY;