CREATE OR REPLACE VIEW CURR_CO_STATUS_CIERRE AS 
SELECT CO_ID , CH_SEQNO , CH_STATUS , CH_REASON , CH_VALIDFROM , ENTDATE ,
         USERLASTMOD , REC_VERSION , REQUEST
  FROM   CONTRACT_HISTORY x
  WHERE  x.CH_SEQNO = ( SELECT max( z.CH_SEQNO )
                        FROM   CONTRACT_HISTORY z
                        WHERE  (z.CH_PENDING IS NULL OR z.CH_PENDING != 'X')
                      AND    z.CO_ID = x.CO_ID  and ENTDATE  <to_date ('25/06/2007','DD/MM/YYYY') );

