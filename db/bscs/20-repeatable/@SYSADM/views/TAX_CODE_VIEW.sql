CREATE OR REPLACE VIEW TAX_CODE_VIEW (taxcode, tax_seqno, taxcode_name, taxcat_id, valid_from, invalid_from, taxrate, tax_amount, tax_amount_currency, glacode, taxcat_name, zero_tax_default, legalcode, taxtype_id, taxcode_version_des, jurisdiction_type_id) AS 
SELECT
    -- Get highest version
    A.TAXCODE,
    A.TAX_SEQNO,
    C.TAXCODE_NAME,
    A.TAXCAT_ID,
    A.VALID_FROM,
    to_date('2037.01.01', 'YYYY.MM.DD'),
    B.TAXRATE,
    B.TAX_AMOUNT,
    B.TAX_AMOUNT_CURRENCY,
    B.GLACODE,
    B.TAXCAT_NAME,
    B.ZERO_TAX_DEFAULT,
    A.LEGALCODE,
    C.TAXTYPE_ID,
    A.TAXCODE_VERSION_DES,
    C.JURISDICTION_TYPE_ID
FROM
    TAX_CODE_VERSION A,
    TAX_CATEGORY B,
    TAX_CODE C
WHERE
    A.TAXCAT_ID = B.TAXCAT_ID
    AND A.TAXCODE = C.TAXCODE
    AND A.VALID_FROM = (
        SELECT
            MAX(X.VALID_FROM)
        FROM
            TAX_CODE_VERSION X
        WHERE
            X.TAXCODE = A.TAXCODE
    )
UNION    -- Get other versions
SELECT
    A.TAXCODE,
    A.TAX_SEQNO,
    C.TAXCODE_NAME,
    A.TAXCAT_ID,
    A.VALID_FROM,
    A_NEXT.VALID_FROM,
    B.TAXRATE,
    B.TAX_AMOUNT,
    B.TAX_AMOUNT_CURRENCY,
    B.GLACODE,
    B.TAXCAT_NAME,
    B.ZERO_TAX_DEFAULT,
    A.LEGALCODE,
    C.TAXTYPE_ID,
    A.TAXCODE_VERSION_DES,
    C.JURISDICTION_TYPE_ID
FROM
    TAX_CODE_VERSION A,
    TAX_CODE_VERSION A_NEXT,
    TAX_CATEGORY B,
    TAX_CODE C
WHERE
    A.TAXCAT_ID = B.TAXCAT_ID
    AND A.TAXCODE = C.TAXCODE
    AND A.TAXCODE = A_NEXT.TAXCODE
    AND A_NEXT.VALID_FROM = (
        SELECT
            MIN(X.VALID_FROM)
        FROM
            TAX_CODE_VERSION X
        WHERE
            X.TAXCODE = A.TAXCODE
            AND X.VALID_FROM > A.VALID_FROM
    ) WITH READ ONLY;