CREATE OR REPLACE VIEW BITACORA_PLANES2  AS 
select /*b.vscode,*/upper('tdm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR,substr(e.long_name,5,20) MINUTOS_GRATIS,upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c,fup_tariff d,fu_pack e,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
and b.sncode=c.sncode
and a.tmcode=d.tmcode
and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'TP%'
and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3/*2=duplica,3=pcs,4,8=autocontrol*/)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
union
(select upper('tdm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR, upper('0'),upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c/*,fup_tariff d--,fu_pack e,*/,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and b.sncode=c.sncode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
--and a.tmcode=d.tmcode
--and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'TP%'
and upper(a.des) not like ('%POOL%')
--and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
minus
select upper('tdm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR, upper('0'),upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c,fup_tariff d,fu_pack e,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
and b.sncode=c.sncode
and a.tmcode=d.tmcode
and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'TP%'
and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
)
union
-----------------=================BITACORA GSM ==================----------------------------
select upper('gsm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR,substr(e.long_name,5,20) MINUTOS_GRATIS,upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c,fup_tariff d,fu_pack e,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
and b.sncode=c.sncode
and a.tmcode=d.tmcode
and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'GS%'
and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3/*2=duplica,3=pcs,4,8=autocontrol*/)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
union
(select upper('gsm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR, upper('0'),upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c/*,fup_tariff d--,fu_pack e,*/,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
and b.sncode=c.sncode
--and a.tmcode=d.tmcode
--and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'GS%'
and upper(a.des) not like ('%POOL%')
--and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
minus
select upper('gsm') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR, upper('0'),upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c,fup_tariff d,fu_pack e,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y
where a.tmcode=b.tmcode
and z.cod_bscs=a.tmcode
and z.id_detalle_plan=y.id_detalle_plan
and b.sncode=c.sncode
and a.tmcode=d.tmcode
and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
and a.shdes like 'GS%'
and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
)
-----------------=================BITACORA POOL ==================----------------------------
union
--select * from bs_minutos_planes@axis
select upper('pool') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR,substr(x.long_name,5,20) MINUTOS_GRATIS,upper(substr(g.des,4,25)) CMP_CMNP_TOOL
from rateplan a,mpulktmb b,mpusntab c/*,fup_tariff d--,fu_pack e,*/,mpulktmm f,mpuritab g,bs_planes@axis z,ge_detalles_planes@axis y,bs_minutos_planes@axis x
where 
z.id_detalle_plan in (select id_detalle_plan from bs_planes@axis where cod_bscs in (select a.tmcode from rateplan a,mpulktm1 b,mpusntab c,mpulktm2 f,mpuritab g where a.tmcode=b.tmcode and b.sncode=c.sncode and f.tmcode=a.tmcode and f.ricode=g.ricode and b.sncode =2 and upper(a.des) like ('%POOL%') and f.sncode in (91,55) and f.upcode in (1,7,4,8,2,3))) 
and a.tmcode=z.cod_bscs
and z.id_detalle_plan=y.id_detalle_plan
and a.tmcode=b.tmcode
and b.sncode=c.sncode
--and a.tmcode=d.tmcode
--and e.fu_pack_id=d.fu_pack_id
and f.tmcode=a.tmcode
and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
--and a.shdes like 'GS%'
and upper(a.des) like ('%POOL%')
--and d.valid_to is null
and f.sncode in (91,55)
and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)
and f.vscode=(select max(n.vscode) from mpulktmm n where n.tmcode=f.tmcode)
and a.tmcode=x.tmcode
union
select upper('pool') RED,y.id_plan,z.id_detalle_plan,a.tmcode CODIGO_BSCS,a.des Nombre,c.des SERVICIO,b.accessfee VALOR,upper('0'),upper('0')
from rateplan a,mpulktmb b,mpusntab c/*,fup_tariff d,fu_pack e,mpulktmm f,mpuritab g*/,bs_planes@axis z,ge_detalles_planes@axis y
where 
z.id_detalle_plan in (select id_detalle_plan from bs_planes@axis where cod_bscs in (select a.tmcode from rateplan a,mpulktm1 b,mpusntab c,mpulktm2 f,mpuritab g where a.tmcode=b.tmcode and b.sncode=c.sncode and f.tmcode=a.tmcode and f.ricode=g.ricode and b.sncode =2 and upper(a.des) like ('%POOL%') and f.sncode in (91,55) and f.upcode in (1,7,4,8,2,3))) 
and a.tmcode=z.cod_bscs
and z.id_detalle_plan=y.id_detalle_plan
and a.tmcode=b.tmcode
and b.sncode=c.sncode
--and a.tmcode=d.tmcode
--and e.fu_pack_id=d.fu_pack_id
--and f.tmcode=a.tmcode
--and f.ricode=g.ricode
and (b.sncode in (select sn_code from bs_servicios_paquete@axis where sn_code>0) or b.sncode in (1,2))
--and a.shdes like 'GS%'
and upper(a.des) like ('%MAESTRO%')
--and d.valid_to is null
--and f.sncode in (91,55)
--and f.upcode in (1,7,4,8,2,3)
and b.vscode=(select max(m.vscode) from mpulktmb m where m.tmcode=b.tmcode)

