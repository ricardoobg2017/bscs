CREATE OR REPLACE VIEW GSI_DOC1_FORMATEO_GYE_CCR_V AS 
select
    "ORDEN",
    "CAMPO1",
    "CAMPO2",
    "CAMPO3",
    "CAMPO4",
    "CAMPO5",
    "CAMPO6",
    "CAMPO7",
    "CAMPO8",
    "CAMPO9",
    "CAMPO1O",
    "CAMPO11",
    "CAMPO13",
    "CAMPO14",
    "CAMPO15",
    "CAMPO16"
from
    gsi_doc1_formateo_ccr
where
    campo15 = 'INV_99_20071008_GYE_9_0_0_2810071052_00.ps'
order by
    orden asc ;