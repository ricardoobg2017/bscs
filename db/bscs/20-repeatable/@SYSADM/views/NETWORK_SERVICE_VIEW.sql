CREATE
OR REPLACE VIEW NETWORK_SERVICE_VIEW (
    sncode,
    main_service,
    srv_type,
    regular_contract_ind,
    main_contract_ind,
    al_contract_ind,
    profile_flag,
    vpn_owner_contract_ind,
    vpn_user_contract_ind
) AS
SELECT
    DISTINCT NXV.SNCODE SNCODE,
    S3V.SVCODE,
    S3V.SRV_TYPE,
    S3V.REGULAR_CONTRACT_IND,
    S3V.MAIN_CONTRACT_IND,
    S3V.AL_CONTRACT_IND,
    S3V.PROFILE_FLAG,
    S3V.VPN_OWNER_CONTRACT_IND,
    S3V.VPN_USER_CONTRACT_IND
FROM
    MPULKNXV NXV,
    MPSSVTAB SSV,
    MPSSVTAB S2V,
    MPSSVTAB S3V
WHERE
    NXV.SSCODE = SSV.SVCODE
    AND NXV.S2CODE = S2V.SVCODE
    AND S3V.SVCODE = DECODE(
        SSV.SRVIND,
        4,
        DECODE(S2V.SRVIND, 4, NXV.S1CODE, NXV.S2CODE),
        NXV.SSCODE
    );