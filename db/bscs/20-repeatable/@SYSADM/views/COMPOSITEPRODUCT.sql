CREATE OR REPLACE VIEW COMPOSITEPRODUCT  AS 
(
    select
        distinct TMCODE,
        VSDATE,
        VSCODE
    from
        MPULKTMB
    WHERE
        STATUS = 'P'
) WITH READ ONLY;