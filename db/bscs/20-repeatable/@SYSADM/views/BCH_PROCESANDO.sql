CREATE OR REPLACE VIEW BCH_PROCESANDO AS 
select
    a.custcode,
    c.bch_process_id,
    b.customer_process_status
from
    customer_all a,
    bch_process_cust b,
    bch_process_package c
where
    b.customer_process_status <> 'S'
    and not c.bch_process_id is null
    and a.customer_id = b.customer_id
    and b.bch_package_num = c.bch_package_num;