CREATE OR REPLACE function GSI_BUSCA_DESTINO(DESTINO in varchar2 ) return number is
  Result number;
  
  Ancho_destino number; 
  Indice number;
  Cadena_Buscar varchar2(100);
  Cadena_sql varchar2(1000);
  V_zpcode number;
begin
 Ancho_destino:=length(Destino);
 Cadena_Buscar:=substr(Destino,1,Ancho_destino);
 Indice:=0;
 Result:=0;
while Indice <Ancho_destino
loop
  Cadena_Buscar:= '+' || substr(Destino,1,Ancho_destino -Indice);
  
  Begin
  
  select zpcode into V_zpcode from mpuzptab where digits= Cadena_Buscar;
  
  exception
  when no_data_found then
  V_zpcode:= 0;
   
  end;
  
  If V_zpcode >0 then
  Result:=V_zpcode;
  exit;
  end if;
  
  Indice:=Indice+1; 

End loop;
   
  return(Result);
end ;
/

