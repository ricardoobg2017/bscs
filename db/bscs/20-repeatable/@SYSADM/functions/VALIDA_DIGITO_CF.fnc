create or replace function VALIDA_DIGITO_CF(pv_billcycles IN VARCHAR2) return number is  
  ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Katerin Rivadeneira
  -- Created:  11/07/2016 11:10:05
  -- Proyecto: [10987]-Proyecto documento electronicos por reimpresion de servicios no inventariables
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Jimmy Larrosa 
  -- Version:   1.0
  -- Motivo:    Valida si una factura es una sociedad
  ---------------------------------------------------------------------------------------------------
  

 LV_VALOR VARCHAR2(1000); 
 LV_ESTADO VARCHAR2(5);
 lv_trama VARCHAR2(1000);
 LN_VALOR NUMBER;
 
   cursor c_billcycles(cn_campo_24 number )  is 
      SELECT (
            select f.billcycle || '|' || 'S' || '|' || f.fup_account_period_id
              from sysadm.billcycles f
             where upper(f.description) like upper('%SOCIEDAD%')
                and f.billcycle = k.billcycle
            union
            select f.billcycle || '|' || 'D' || '|' || f.fup_account_period_id
              from sysadm.billcycles f
             where upper(f.description) like upper('%DTH%')
             and f.billcycle = k.billcycle
            union
            select f.billcycle || '|' || 'P' || '|' || f.fup_account_period_id
              from sysadm.billcycles f
             where upper(f.description) not like ('%SOCIEDAD%')
               and upper(f.description) not like ('%DTH%')
               and f.billcycle = k.billcycle
                )
     FROM sysadm.billcycles K
     WHERE K.BILLCYCLE=cn_campo_24;
 
   begin
     
            OPEN c_billcycles(pv_billcycles);
            FETCH c_billcycles
              INTO lv_trama;
            CLOSE c_billcycles;
    
  
         LV_VALOR:= substr(lv_trama, instr(lv_trama,'|' ,1)+1 );
         LV_ESTADO:= substr(LV_VALOR, 0,instr(LV_VALOR,'|')-1);
         
          IF LV_ESTADO IN ('S','D')then
             --VALIDA SI ES SOCIEDAD
                LN_VALOR:=1;
                return LN_VALOR;
           ELSE
              --NO ES UNA SOCIEDAD
                LN_VALOR:=0;
                return LN_VALOR;
           END IF; 
          

  exception
    when others then
      LN_VALOR:=0;
      return LN_VALOR;


end VALIDA_DIGITO_CF;
/
