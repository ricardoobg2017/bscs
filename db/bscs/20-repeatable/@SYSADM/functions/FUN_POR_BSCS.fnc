create or replace function FUN_POR_BSCS(pd_fecha in date,pn_tipo in number) return number is
 
  cursor c_valor(cd_fecha in date)  is
    select x.porc_hist,x.fech_prorateo,x.fech_varia 
    from bscs_in_impu_hist x
    where cd_fecha between fech_ini_hist and fech_fin_hist;

  ln_valor        number:=0;
  lv_fech_pro     varchar2(10);
  lv_fecha_varia  varchar2(10);
begin
   ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Cynthia Quinde.
  -- Created:  28/07/2016 14:55:37
  -- Proyecto: [11163]-Cambio de Fecha de Facturacion de BSCS
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Jimmy Larrosa
  -- Motivo:    Funci�n que retorna el porcentaje de iva aplicar actual
  --Retorno 1 significa que esta factura cogera los valores de prorateo
  --Retorno 2 significa que esta factura cogera los valores del xml es decir eis de bscs para notas de credito en este ejemplo mayo
  --Retorno 0 significa que esta factura realizara los calculos internos del paquete de cambio de fecha
  ---------------------------------------------------------------------------------------------------
  --El tipo me indica 1 saca el valor de porcentaje para calculos de prorateo
  if pn_tipo=1 then 
    ln_valor:=0;   
    open c_valor(pd_fecha);
    fetch c_valor into ln_valor,lv_fech_pro,lv_fecha_varia;
    close c_valor;
    return   ln_valor/100 ;
 else
   --Tipo =2 Retorna 1 si es prorateo y 2 si es de mayo    
   --Retorna 0 si no es de prorateo ni de mayo(fecha varia) el caculo lo realizara procesos de caja
   ln_valor:=0;  
   open c_valor(pd_fecha);
    fetch c_valor into ln_valor,lv_fech_pro,lv_fecha_varia;
    close c_valor;
    if lv_fech_pro = to_char(pd_fecha,'MMYYYY') then
       return   1 ;
    elsif lv_fecha_varia = to_char(pd_fecha,'MMYYYY') then
       return   2 ;
    else
      return 0;
    end if;
 end if;
 
exception
  when others then
    return 0;
end FUN_POR_BSCS;
/
