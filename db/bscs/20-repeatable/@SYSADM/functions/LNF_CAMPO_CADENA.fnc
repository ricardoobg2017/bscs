CREATE OR REPLACE FUNCTION LNF_CAMPO_CADENA(PV_CADENA_PARAMETROS IN VARCHAR2,PN_POS_CAMPO IN NUMBER) RETURN varchar2 IS
----------------------------------------------------------------
-- PROYECTO: 9394 - Mejoras en el modulo de promociones.
-- CIMA - DPA
-- Fecha: 09/12/2012
----------------------------------------------------------------
lv_campo_cadena varchar2(4000);
ln_pos_pipe_desde number;
ln_pos_pipe_hasta number;

BEGIN

   ln_pos_pipe_desde:=instr(PV_CADENA_PARAMETROS,'|',1,PN_POS_CAMPO);
   ln_pos_pipe_hasta:=instr(PV_CADENA_PARAMETROS,'|',1,PN_POS_CAMPO+1);
   lv_campo_cadena:=substr(PV_CADENA_PARAMETROS,ln_pos_pipe_desde+1,ln_pos_pipe_hasta-ln_pos_pipe_desde-1);

   return lv_campo_cadena;

EXCEPTION
   WHEN OTHERS THEN
      RETURN null;
END LNF_CAMPO_CADENA;
/
