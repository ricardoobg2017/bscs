CREATE OR REPLACE function sa_obtiene_parametro(pd_parametro varchar2) return varchar2 is
  result varchar2(20);
  --===========================================================
  -- Proyecto: Clientes vip
  -- Creado por :  CLS Reinaldo Burgos
  -- Fecha:    28/09/2005
  -- Motivo: NUEVO CICLO DE FACTURACIÓN
  --===========================================================

  
  -- obtener datos de una tabla de parametros
begin
   Select Par_Val
     Into Result
     From Sa_Parametros
    Where Par_Id = Pd_Parametro;

   return(result);    
   
exception
   when others then 
        return null;

end sa_obtiene_parametro;
/

