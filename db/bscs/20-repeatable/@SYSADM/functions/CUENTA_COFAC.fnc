CREATE OR REPLACE FUNCTION CUENTA_COFAC(PV_NOMBRE_TABLA IN VARCHAR2,
                                        PV_ID_CUENTA    IN VARCHAR2)
  RETURN VARCHAR2 IS

   
  query_dinamico     varchar2(100):=null;
  
    
    TYPE LC_CURSOR_ciclos IS REF CURSOR;
    lc_ciclos LC_CURSOR_ciclos;
    
     ln_ciclos number:=0;
     ln_cont   number;
     lv_error  Varchar2(100);
     
BEGIN

            query_dinamico := ' select count(*) numero from ' ||
                                      ' co_fact_' || PV_NOMBRE_TABLA ||
                                      ' where custcode='||''''||PV_ID_CUENTA||'''';

             open lc_ciclos for query_dinamico;
             fetch lc_ciclos 
             into ln_ciclos;
             ln_cont := lc_ciclos%ROWCOUNT;
            
            if ln_cont = 0 or ln_cont is null  then
              ln_ciclos:=0;
            end if;
            
            return(ln_ciclos);
    
 EXCEPTION
    WHEN OTHERS THEN
      Lv_Error := (Sqlerrm);
      return Lv_Error;
 END CUENTA_COFAC;
/
