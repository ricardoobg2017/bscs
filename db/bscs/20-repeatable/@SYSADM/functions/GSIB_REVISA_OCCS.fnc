CREATE OR REPLACE FUNCTION GSIB_REVISA_OCCS RETURN VARCHAR2 is
 cursor c_dat is
    select * from gsib_valida_sncode;

 /* CURSOR C_EXISTE (CV_USUARIO VARCHAR2,CN_ID_EJECUCION NUMBER)IS
    SELECT 'X' EXISTE
      FROM BL_CARGA_EJECUCION E --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA
     WHERE E.USUARIO = CV_USUARIO
       AND E.ID_EJECUCION = CN_ID_EJECUCION
       AND E.ESTADO NOT IN ('F','E');
    */
  pv_sql varchar2(1000);
--  pv_usuario varchar2(30):='PROMAN1';
--  pd_entdate date:= to_date(sysdate,'dd/mm/yyyy');
--  pn_id_ejecucion number;
  lv_valor number:=0;
  Ln_Iva        number;       --10920 SUD MNE 30/05/2016
  Lv_Iva        varchar2(10); --10920 SUD MNE 30/05/2016
begin
  --10920 INI SUD MNE
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(1+ln_iva/100);
  --10920 FIN SUD MNE
    begin
      pv_sql:= 'drop table gsib_resultante';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    begin
      pv_sql:= 'create table gsib_resultante (billcycle number, lbc_date date, sncode_cargo number, sncode_credito number)';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    begin
      pv_sql:= 'drop table gsib_subtotal';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    begin
      pv_sql:= 'create table gsib_subtotal (Occs varchar2(30), lbc_date varchar2(15), cantidad number)';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
/*******CREACION DE TABLAS DE ESCENARIOS Y LLENADO DE TABLA DE OBSERVACIONES*******/
  for cdat in c_dat loop
    begin
      pv_sql:= 'drop table tmp_hmr_val_occs';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    pv_sql:= 'create table tmp_hmr_val_occs as select a.customer_id,a.co_id, sum(a.amount) VAL_CARGO, b.billcycle, b.prgcode from fees a, customer_all b where a.sncode = ';
    pv_sql:= pv_sql || cdat.sncode_cargo || 'and a.period <>0 and a.username <> ''PROMAN1'' and a.customer_id = b.customer_id group by a.customer_id,a.co_id, b.billcycle, b.prgcode';
    execute immediate pv_sql;
    commit;
    pv_sql:= 'Create Index idxE1_occ1 On tmp_hmr_val_occs(customer_id)';
    execute immediate pv_sql;
    pv_sql:= 'Create Index idxE1_occ2 On tmp_hmr_val_occs(co_id)';
    execute immediate pv_sql;
    begin
      pv_sql:= 'drop table tmp_hmr_val_occ_cre';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    pv_sql:= 'create table tmp_hmr_val_occ_cre as select a.customer_id,a.co_id, sum(a.amount) VAL_CARGO, b.billcycle, b.prgcode from fees a, customer_all b where a.sncode = ';
    pv_sql:= pv_sql || cdat.sncode_credito || 'and a.period <>0 and a.username <> ''PROMAN1'' and a.customer_id = b.customer_id group by a.customer_id,a.co_id, b.billcycle, b.prgcode';
    execute immediate pv_sql;
    commit;
    pv_sql:= 'Create Index idxE1_occ3 On tmp_hmr_val_occ_cre(customer_id)';
    execute immediate pv_sql;
    pv_sql:= 'Create Index idxE1_occ4 On tmp_hmr_val_occ_cre(co_id)';
    execute immediate pv_sql;
    begin
      pv_sql:= 'drop table gsib_esc1_cargo_credito';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
--**
--** ESCENARIO 1::: VERIFICAR SI EL CARGO SUMADO MAS LOS IMPUESTOS ES IGUAL AL CREDITO.
    pv_sql:= 'create table gsib_esc1_cargo_credito as select a.customer_id,a.co_id, a.val_cargo CARGO ,round((a.val_cargo *'||Ln_iva||'),2) val_cargo, b.val_cargo CREDITO, ' || cdat.sncode_cargo || ' sncode_cargo,' || cdat.sncode_credito || ' sncode_credito ';--10920 SUD MNE
    pv_sql:= pv_sql || ' from tmp_hmr_val_occs a, tmp_hmr_val_occ_cre b where a.co_id = b.co_id and round((a.val_cargo *'||Ln_iva||'),2) <> (b.val_cargo*-1)';--10920 SUD MNE
    execute immediate pv_sql;
    commit;
    select count(*) into lv_valor from gsib_esc1_cargo_credito;
    if lv_valor > 0 then
      insert into gsib_observacion values (sysdate, 'existen diferencias en el primer escenario: VERIFICAR SI EL CARGO SUMADO MAS LOS IMPUESTOS ES IGUAL AL CREDITO; consultar la tabla gsib_E1_cargo_credito',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
      begin
        pv_sql:= 'drop table gsib_E1_cargo_credito';
        execute immediate pv_sql;
      exception
        when others then
          null;
      end;
      pv_sql:= 'create table gsib_E1_cargo_credito as select CUSTOMER_ID,CO_ID,CARGO,VAL_CARGO,CREDITO, ' || cdat.sncode_cargo || ' sncode_cargo,' || cdat.sncode_credito || ' sncode_credito from gsib_esc1_cargo_credito';
      execute immediate pv_sql;
      commit;
      pv_sql:= 'insert into gsib_resultante select c.billcycle, c.lbc_date, a.sncode_cargo, a.sncode_credito from gsib_E1_cargo_credito a, customer_all c where  a.customer_id = c.customer_id';
      execute immediate pv_sql;
      commit;
    else
      insert into gsib_observacion values (sysdate, 'no existen diferencias en el primer escenario: VERIFICAR SI EL CARGO SUMADO MAS LOS IMPUESTOS ES IGUAL AL CREDITO',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
    end if;
--** ESCENARIO 2::: VERIFICAR SI HAY CARGOS SIN CREDITOS
--** CASO DE HABER DIFERENCIAS REVISAR Y CARGOS LOS CREDITOS CON EL SNCODE 2624 Y SUMAR LOS IMPUESTOS.
    begin
      pv_sql:= 'drop table gsib_esc2_cargo_sin_cred';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    pv_sql:= 'create table gsib_esc2_cargo_sin_cred as select co_id from tmp_hmr_val_occs MINUS select co_id from tmp_hmr_val_occ_cre';
    execute immediate pv_sql;

    select count(*) into lv_valor from gsib_esc2_cargo_sin_cred;
    if lv_valor > 0 then
      insert into gsib_observacion values (sysdate, 'existen diferencias en el primer escenario: VERIFICAR SI HAY CARGOS SIN CREDITOS; consultar la tabla gsib_E2_cargo_sin_cred',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
      begin
        pv_sql:= 'drop table gsib_E2_cargo_sin_cred';
        execute immediate pv_sql;
      exception
        when others then
          null;
      end;
      pv_sql:= 'create table gsib_E2_cargo_sin_cred as select co_id, ' || cdat.sncode_cargo || ' sncode_cargo,' || cdat.sncode_credito || ' sncode_credito from gsib_esc2_cargo_sin_cred';
      execute immediate pv_sql;
      commit;
      pv_sql:= 'insert into gsib_resultante select c.billcycle, c.lbc_date, a.sncode_cargo, a.sncode_credito from gsib_E2_cargo_sin_cred a, contract_all b, customer_all c where a.co_id = b.co_id and b.customer_id = c.customer_id';
      execute immediate pv_sql;
      commit;
    else
      insert into gsib_observacion values (sysdate, 'no existen diferencias en el primer escenario: VERIFICAR SI HAY CARGOS SIN CREDITOS',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
    end if;

--** ESCENARIO 3::: VERIFICAR SI HAY CREDITOS SIN CARGOS
--** CASO DE HABER DIFERENCIAS REVISAR Y CARGAR LOS CARGOS CON EL SNCODE 2959 Y QUITAR LOS IMPUESTOS.
    begin
      pv_sql:= 'drop table gsib_esc3_cred_sin_cargo';
      execute immediate pv_sql;
    exception
      when others then
        null;
    end;
    pv_sql:= 'create table gsib_esc3_cred_sin_cargo as select co_id from tmp_hmr_val_occ_cre MINUS select co_id from tmp_hmr_val_occs';
    execute immediate pv_sql;
    select count(*) into lv_valor from gsib_esc3_cred_sin_cargo;
    if lv_valor > 0 then
      insert into gsib_observacion values (sysdate, 'existen diferencias en el primer escenario: VERIFICAR SI HAY CREDITOS SIN CARGOS; consultar la tabla gsib_E2_cred_sin_cargo',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
      begin
        pv_sql:= 'drop table gsib_E2_cred_sin_cargo';
        execute immediate pv_sql;
      exception
        when others then
          null;
      end;
      pv_sql:= 'create table gsib_E2_cred_sin_cargo as select CO_ID, ' || cdat.sncode_cargo || ' sncode_cargo,' || cdat.sncode_credito || ' sncode_credito from gsib_esc3_cred_sin_cargo';
      execute immediate pv_sql;
      commit;
      pv_sql:= 'insert into gsib_resultante select c.billcycle, c.lbc_date, a.sncode_cargo, a.sncode_credito from gsib_E2_cred_sin_cargo a, contract_all b, customer_all c where a.co_id = b.co_id and b.customer_id = c.customer_id';
      execute immediate pv_sql;
      commit;
    else
      insert into gsib_observacion values (sysdate, 'no existen diferencias en el primer escenario: VERIFICAR SI HAY CREDITOS SIN CARGOS',cdat.sncode_cargo,cdat.sncode_credito);
      commit;
    end if;
end loop;
/*******FIN DE CREACION DE TABLAS DE ESCENARIOS Y LLENADO DE TABLA DE OBSERVACIONES*******/

/*******PRESENTACIÓN DE RESULTADOS / FALTANTES / REVISIÓN DE TABLAS RESULTANTES*******/

      insert into gsib_subtotal select to_char(sncode_cargo) || '-' || to_char(sncode_credito), 
      lbc_date, count(*) 
      from gsib_resultante 
      group by sncode_cargo || '-' || sncode_credito, lbc_date;
      commit;
      select count(*)
      into lv_valor from gsib_subtotal;

      If lv_valor > 0 then
         return 'Existen diferencias en Occs. Favor revisar tablas: gsib_subtotal / gsib_resultante y gsib_observacion para la revisión de los casos';
      else
         return 'Las Occs se encuentras cuadradas. Favor revise la tabla gsi_observacion para mas detalles';
      end if;
/*******FIN PRESENTACIÓN DE RESULTADOS / FALTANTES / REVISIÓN DE TABLAS RESULTANTES*******/
end GSIB_REVISA_OCCS;
/
