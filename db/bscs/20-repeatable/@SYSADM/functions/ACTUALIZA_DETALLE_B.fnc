CREATE OR REPLACE function ACTUALIZA_DETALLE_B (V_SQL VARCHAR2,V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2, V_ESTADO VARCHAr2,V_PORCENTAJE VARCHAR2) return BOOLEAN
  is
    w_sql_4      varchar2(200);
    -- Rundeck
  begin
    
  UPDATE READ.gsi_detalle_bitacora_proc SET LOG= V_SQL  , estado= V_ESTADO ,porcentaje=V_PORCENTAJE,
  FECHA_FIN =SYSDATE
  WHERE ID_BITACORA=V_ID_BITACORA AND ID_DETALLE_BITACORA=  format_cod(V_ID_DET_BITACORA+1,4)
  AND  ID_PROCESO=  V_ID_PROCESO  ;
 
   commit;  
    return  TRUE;

   
  Exception when others then
      w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
     dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
   
   return  FALSE;
end ACTUALIZA_DETALLE_B ;
/

