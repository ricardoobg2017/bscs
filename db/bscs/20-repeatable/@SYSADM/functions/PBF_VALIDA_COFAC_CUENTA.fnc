CREATE OR REPLACE FUNCTION PBF_VALIDA_COFAC_CUENTA(PV_ID_CUENTA IN VARCHAR2,
                                                   PV_FECHA_INI IN VARCHAR2,
                                                   PV_FECHA_FIN IN VARCHAR2)
  RETURN VARCHAR2 IS


  -- PROYECTO   : [10111] PAYBACK
  -- CREADO POR : CLS LUIS OLVERA
  -- FECHA      : 15/04/2015
  -- OBJETIVO   : CONSULTAR LOS RUBROS DE LA HOJA DE C�LCULO DE PROCESO NOTA DE CR�DITO 
  
  CURSOR C_CANT_MESES(CV_FECHA_INI VARCHAR2,CV_FECHA_FIN VARCHAR2)IS
    SELECT MONTHS_BETWEEN(TO_DATE(CV_FECHA_FIN,'DD/MM/RRRR'),TO_DATE(CV_FECHA_INI,'DD/MM/RRRR')) FROM DUAL;


  CURSOR C_VERIFICA_CUENTA(CV_CUENTA VARCHAR2,CD_FECHA DATE) IS
  SELECT A.OHOPNAMT_DOC
    FROM ORDERHDR_ALL A, CUSTOMER_ALL E
   WHERE E.CUSTCODE = CV_CUENTA
     AND NVL(E.CUSTOMER_ID_HIGH, E.CUSTOMER_ID) = A.CUSTOMER_ID
     AND A.OHSTATUS = 'IN'
     AND A.OHENTDATE = CD_FECHA;



  LN_CANTIDAD_MESES NUMBER;

  
  LD_FECHA_PROCESO  DATE;
  LB_ENCONTRO       BOOLEAN := FALSE;
  LV_CANT_FACT      VARCHAR2(10);
  LN_CONTEO_FACT    NUMBER:=0;
  LN_VALOR_FACT     NUMBER:=0;

  
BEGIN

  BEGIN
    
    OPEN C_CANT_MESES(PV_FECHA_INI,PV_FECHA_FIN);
    FETCH C_CANT_MESES
    INTO LN_CANTIDAD_MESES;
    CLOSE C_CANT_MESES;     
    
    FOR CONTAR IN 1..LN_CANTIDAD_MESES LOOP
    
    LD_FECHA_PROCESO:=ADD_MONTHS(TO_DATE(PV_FECHA_INI,'DD/MM/RRRR'),CONTAR-1);
    
    OPEN C_VERIFICA_CUENTA(PV_ID_CUENTA,LD_FECHA_PROCESO);
    FETCH C_VERIFICA_CUENTA
    INTO LN_VALOR_FACT;
    LB_ENCONTRO :=C_VERIFICA_CUENTA%FOUND;
    CLOSE C_VERIFICA_CUENTA;
    

    IF (LN_VALOR_FACT<0 AND LB_ENCONTRO=TRUE)THEN
       
       IF CONTAR=1 THEN
       
       LN_CONTEO_FACT:=LN_CONTEO_FACT+1;
       
       ELSIF CONTAR=2 THEN
       
             IF LN_CONTEO_FACT=1 THEN

                LN_CONTEO_FACT:=LN_CONTEO_FACT+1;                
             
             ELSE
         
                LN_CONTEO_FACT:=LN_CONTEO_FACT-1;      
         
             END IF;
       
       END IF;
    
    ELSIF(LN_VALOR_FACT>=0 OR LB_ENCONTRO=FALSE)THEN

       IF CONTAR=2 THEN
     
             IF LN_CONTEO_FACT=0 THEN
               
                LN_CONTEO_FACT:=-2; 
                
         
             END IF;
       
       END IF;
    
    
    END IF;
    
    
    END LOOP;
    
    
    LV_CANT_FACT:=LN_CONTEO_FACT;
    
    
  
  EXCEPTION
  
    WHEN OTHERS THEN
      LV_CANT_FACT := 'ERROR';
  END;

  RETURN(LV_CANT_FACT);
END PBF_VALIDA_COFAC_CUENTA;
/
