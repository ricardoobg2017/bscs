CREATE OR REPLACE FUNCTION CREA_TABLA_CBILL RETURN NUMBER IS
    --
    lvSentencia     VARCHAR2(20000);
    lvMensErr       VARCHAR2(1000);
    
    BEGIN
         --------------------------------------------------------------
         -- TABLA DE CARTERA
         -- Se almacenaran los saldos de la tabla de migracion de cbill
         --------------------------------------------------------------
           lvSentencia := 'CREATE TABLE CO_CARTERA_CBILL'||
                          '( CUENTA               VARCHAR2(24),'||
                          '  CUSTOMER_ID          NUMBER,'||
                          '  BALANCE              NUMBER DEFAULT 0,'||
                          '  FECHA                DATE'||
                          ')'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';                     
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                          
           lvSentencia := 'create index IDX_CARTERA_CBILL on CO_CARTERA_CBILL (CUSTOMER_ID)'||
                          ' tablespace DATA'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                
           lvSentencia := 'create public synonym CO_CARTERA_CBILL for CO_CARTERA_CBILL';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_CARTERA_CBILL to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           


         ------------------------------------------------------------------
         -- TABLA DE CARTERA
         -- Se almacenaran los pagos + creditos y la edad de mora correcta
         ------------------------------------------------------------------
           lvSentencia := 'CREATE TABLE CO_DISPONIBLE_CBILL'||
                          '( CUSTOMER_ID          NUMBER,'||
                          '  DISPONIBLE           NUMBER DEFAULT 0,'||
                          '  MORA                 NUMBER DEFAULT 0'||
                          ')'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';                     
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                          
           lvSentencia := 'create index IDX_DISPONIBLE_CBILL on CO_DISPONIBLE_CBILL (CUSTOMER_ID)'||
                          ' tablespace DATA'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                
           lvSentencia := 'create public synonym CO_DISPONIBLE_CBILL for CO_DISPONIBLE_CBILL';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_DISPONIBLE_CBILL to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;
           
    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA_CBILL;
/

