CREATE OR REPLACE FUNCTION VALIDA_RUC(p_ruc IN VARCHAR) RETURN BOOLEAN IS

  /*[10798] Validacion de numero de cedula y ruc*/

  coef1    VARCHAR2(9) := '432765432';
  coef2    VARCHAR2(8) := '32765432';
  coef3    VARCHAR2(9) := '212121212';
  prov     VARCHAR2(2);
  tipo     VARCHAR2(1);
  estab    VARCHAR2(4);
  verif    VARCHAR2(1);
  conse    VARCHAR2(6);
  coefx    VARCHAR2(9);
  RESULT   BOOLEAN := FALSE;
  dig_veri NUMBER(2);
  cad_val  VARCHAR(9);
  longitud VARCHAR(13);
  bandera1 NUMBER(1) := 0;
  bandera2 NUMBER(1) := 0;
  bandera3 NUMBER(1) := 0;

BEGIN
  
  longitud := length(p_ruc);
  prov     := substr(p_ruc, 1, 2);
  tipo     := substr(p_ruc, 3, 1);

  -- Los dos primeros digitos corresponden a la Provincia
  IF to_number(prov) > 24 OR to_number(prov) < 1 THEN
    RESULT   := FALSE;
    bandera1 := '1';
  END IF;

  IF bandera1 = 0 THEN
    IF longitud = 10 THEN
      conse    := substr(p_ruc, 4, 6);
      verif    := substr(p_ruc, 10, 1);
      cad_val  := prov || tipo || conse;
      dig_veri := calculo_verificador(cad_val, coef3);
    
    ELSIF longitud = 13 THEN
      IF tipo = '9' THEN
        conse := substr(p_ruc, 4, 6);
        verif := substr(p_ruc, 10, 1);
        estab := substr(p_ruc, 11, 3);
      ELSIF tipo = '6' THEN
        conse := substr(p_ruc, 4, 5);
        verif := substr(p_ruc, 9, 1);
        estab := substr(p_ruc, 10, 4);
      ELSIF tipo IN ('0', '1', '2', '3', '4', '5') THEN
        conse := substr(p_ruc, 4, 6);
        verif := substr(p_ruc, 10, 1);
        estab := substr(p_ruc, 11, 3);
      END IF;
    
      IF tipo IN ('0', '1', '2', '3', '4', '5', '9') AND estab = '000' THEN
        bandera2 := '1';
        RESULT   := FALSE;
      ELSIF tipo = '6' AND estab = '0000' THEN
        bandera2 := '1';
        RESULT   := FALSE;
      END IF;
    
      IF bandera2 = 0 THEN
        cad_val := prov || tipo || conse;
        IF tipo = '9' THEN
          coefx := coef1;
        ELSIF tipo = '6' THEN
          coefx := coef2;
        ELSIF tipo IN ('0', '1', '2', '3', '4', '5') THEN
          coefx := coef3;
        END IF;
        dig_veri := calculo_verificador(cad_val, coefx);
        IF dig_veri = 1 AND tipo IN ('6', '9') THEN
          bandera3 := 1;
        END IF;
      END IF;
    END IF;
  
    IF tipo IN ('0', '1', '2', '3', '4', '5') AND dig_veri <> 0 THEN
      dig_veri := 10 - dig_veri;
    END IF;
  
    IF longitud = 10 AND tipo IN ('6') THEN
      dig_veri := to_number(verif);
    ELSE
      IF tipo IN ('6', '9') AND bandera3 = 0 AND dig_veri NOT IN (0, 1) THEN
        dig_veri := 11 - dig_veri;
      END IF;
    END IF;
  
    IF (to_number(verif) = dig_veri) AND bandera3 = 0 THEN
      RESULT := TRUE;
    ELSE
      RESULT := FALSE;
    END IF;
  END IF;

  RETURN RESULT;

EXCEPTION
  WHEN OTHERS THEN
    RETURN(RESULT);
END;
/
