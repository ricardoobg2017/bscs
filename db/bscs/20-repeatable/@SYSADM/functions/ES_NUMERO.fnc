CREATE OR REPLACE function ES_NUMERO(Pt_Valor varchar2) return NUMBER is

NUMERO FLOAT;
begin  
   
   NUMERO:= TO_NUMBER(Pt_Valor);
   RETURN 1;

EXCEPTION
  WHEN OTHERS THEN
   RETURN 0;    
end ES_NUMERO;
/

