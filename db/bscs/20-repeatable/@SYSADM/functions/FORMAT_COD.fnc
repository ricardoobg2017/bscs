CREATE OR REPLACE function format_cod(v_char  varchar2, V_tam  number) return varchar2
  is
    new_codigo      varchar2(10);
    i            number;
  begin
     new_codigo:=v_char;
     i:=length(v_char);
    while i <v_tam
      loop
        i := i + 1;
        new_codigo:= '0'|| new_codigo;
         
    end loop;

    return  new_codigo;

end format_cod;
/

