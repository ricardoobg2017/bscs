create or replace function fnc_obtener_valor_facturas(Pv_query varchar2)
  return number is
  lv_query        varchar2(4000);
  ln_valor_cuadre number;
begin

  lv_query := Pv_query;

  execute immediate lv_query
    into ln_valor_cuadre;

  return ln_valor_cuadre;

end;
/
