create or replace function FNC_VALIDA_CAMBIO_BILLCYCLE(new_billcycle in varchar2,
                                                         old_billcycle in varchar2)
  return boolean is
  /*
  PROPOSITO DE LA FUNCION: BLOQUEAR EL CAMBIO DE CICLO EN CUSTOMER_ALL PARA DISMINUIR ERRORES EN PROCESOS DE FACTURACION
  LA FUNCION RETORNA FALSE, para desbloquear el cambio de ciclo, es decir PERMITE EL CAMBIO DE CICLO
  TRUE cuando se debe habilitar el bloqueo de cambio de ciclo, es decir NO PERMITE EL CAMBIO DE CICLO
  ES invocada desde el trigger TGR_UPD_BILLCYCLE
  */
  cursor get_ban_trg is
    select valor
      from gv_parametros a
     where a.id_tipo_parametro = 10861
       and a.id_parametro = 'BAN_TRG_CAMBIO_CICLO';

  cursor usuario_permite(cv_usuario varchar2) is
    select 1
      from dual
     where cv_usuario in (select a.valor
                            from gv_parametros a
                           where a.id_tipo_parametro = 10861
                             and a.nombre = 'USRS_TRG_CICLO');

  cursor get_ciclo(c_billcycle varchar2) is
    select id_ciclo, id_ciclo_admin, ciclo_default
      from fa_ciclos_axis_bscs
     where id_ciclo_admin = c_billcycle;

  Result boolean;

  lv_id_ciclo_o      varchar2(2) := '';
  lv_ciclo_admin_o   varchar2(3) := '';
  lv_ciclo_default_o varchar2(1) := '';

  lv_id_ciclo_n      varchar2(2) := '';
  lv_ciclo_admin_n   varchar2(3) := '';
  lv_ciclo_default_n varchar2(1) := '';
  lv_usuario         varchar2(100);
  ban_trg            varchar2(1) := '';
  lv_permite         varchar2(1) := '';
  lb_permite_usr     boolean := false;
  lb_found_o         boolean := false;
  lb_found_n         boolean := false;
  --Begin

begin
  Result := false; --cualquiera que no sea Administrator PERMITE CAMBIO DE CICLO

  open get_ban_trg;
  fetch get_ban_trg
    into ban_trg;
  close get_ban_trg;

  if (ban_trg = 'N') then
    return false;
  end if;

  SELECT SYS_CONTEXT('USERENV', 'OS_USER') INTO lv_usuario FROM dual;

  open get_ciclo(old_billcycle);
  fetch get_ciclo
    into lv_id_ciclo_o, lv_ciclo_admin_o, lv_ciclo_default_o;
  lb_found_o := get_ciclo%NOTFOUND;
  close get_ciclo;

  open get_ciclo(new_billcycle);
  fetch get_ciclo
    into lv_id_ciclo_n, lv_ciclo_admin_n, lv_ciclo_default_n;
  lb_found_n := get_ciclo%NOTFOUND;
  close get_ciclo;

  --obtengo valores para ciclo old

  if lb_found_o or lb_found_n then
    return false;
  end if;

  open usuario_permite(lv_usuario);
  fetch usuario_permite
    into lv_permite;
  lb_permite_usr := usuario_permite%FOUND;
  close usuario_permite;

  if lb_permite_usr then
    --ponerlo en gv_parametros el usuario por posibles cambios(soporte varios usuarios a bloquear)
    if lv_id_ciclo_o <> lv_id_ciclo_n then
      --cambio de ciclo de 01 a 02, se permite
      Result := false;
      /*
      elsif lv_ciclo_default_o='S' then--cambio al ciclo default NO SE PERMITE
            Result := true;
            */
    elsif lv_id_ciclo_o = lv_id_ciclo_n then
      --cambio dentro del mismo ciclo NO SE PERMITE
      Result := true;
    end if;
  end if;
  return(Result);

exception
  when others then
    return(Result);
  
end FNC_VALIDA_CAMBIO_BILLCYCLE;
/
