CREATE OR REPLACE FUNCTION CALCULO_VERIFICADOR(p_numero IN VARCHAR,
                                               p_coef   IN VARCHAR) RETURN NUMBER IS
  
  /*[10798] Validacion de numero de cedula y ruc*/

  j      NUMBER(2);
  f_tipo VARCHAR2(1);
  k      NUMBER(2);
  total  NUMBER(3);
  a      NUMBER(2);
  b      NUMBER(2);
  p_dig  NUMBER(2) := 0;

  /* Esta funcion calcula el digito verificador del RUC o Cedula */

BEGIN
  
  total  := 0;
  j      := length(p_numero);
  f_tipo := substr(p_numero, 3, 1);
  WHILE j > 0 LOOP
    a := to_number(substr(p_numero, j, 1));
    b := to_number(substr(p_coef, j, 1));
    k := a * b;
    IF f_tipo IN ('0', '1', '2', '3', '4', '5') THEN
      WHILE k > 9 LOOP
        k := k - 10 + 1;
      END LOOP;
    END IF;
    total := total + k;
    j     := j - 1;
    k     := 0;
    a     := 0;
    b     := 0;
  END LOOP;
  IF f_tipo IN ('0', '1', '2', '3', '4', '5') THEN
    p_dig := MOD(total, 10); -- Cedulas
  ELSE
    p_dig := MOD(total, 11); -- RUCs
  END IF;
  
  RETURN p_dig;
  
END;
/
