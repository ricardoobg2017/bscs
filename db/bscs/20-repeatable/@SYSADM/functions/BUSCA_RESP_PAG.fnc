CREATE OR REPLACE function busca_resp_pag (v_telin varchar2) return number is


cursor busca_cuenta_tel is
select c.customer_id,c.custcode,c.cstype,c.paymntresp,
       t.co_id,c.customer_id_high,
       d.dn_id,d.dn_num,d.dn_status
  from customer_all c,
       contract_all t,
       contr_services_cap p,
       directory_number d
  where c.customer_id = t.customer_id
        and t.co_id = p.co_id
        and p.dn_id = d.dn_id
        and d.dn_num = v_telin
        ;

cursor busca_resp(p_cta number) is
select c.customer_id,c.customer_id_high,c.paymntresp,c.custcode,
       c.cstype
  from customer_all c
  where c.customer_id = p_cta  ;

v_br busca_resp%ROWTYPE;
cuenta number;
resultado number;

begin

  for t in busca_cuenta_tel loop

      cuenta := t.customer_id;

      LOOP

          OPEN busca_resp(cuenta);
          FETCH busca_resp into v_br;

          EXIT when v_br.paymntresp = 'X';

          CLOSE busca_resp;
          cuenta := v_br.customer_id_high;

      END LOOP;



      CLOSE busca_resp;

  resultado := v_br.customer_id;


  END LOOP;

return(resultado);

end busca_resp_pag;
/

