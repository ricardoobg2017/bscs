CREATE OR REPLACE function GSI_SEPARA( cadena varchar2, posicion NUMBER) return varchar2 is

primero BOOLEAN;
salir BOOLEAN;
numero NUMBER;
separa VARCHAR2(3000);
contador NUMBER;

BEGIN
primero := FALSE;
salir := TRUE;
numero := 0;
separa := '';
contador := 0;
    While salir
     LOOP
       numero := numero + 1;
        If substr(cadena, numero, 1) = '|' And primero = False Then
            contador := contador + 1;
        ELSE
           If substr(cadena, numero, 1) = '|' And contador = posicion Then
                EXIT;
            End IF;
        End IF;
        If contador = posicion Then
            primero := TRUE;
        Else
            primero := FALSE;
        End IF;
        If primero Then
            SEPARA := SEPARA || substr(cadena, numero, 1);
        End IF;
    END LOOP;
    SEPARA := Replace(SEPARA, '|', '');

    return  rtrim(ltrim(SEPARA));

EXCEPTION
  WHEN OTHERS THEN
   RETURN 0;
END;
/

