create or replace function FUN_VALIDA_CORTE_SOCIEDAD(pv_factura_bscs IN VARCHAR2,
                                                     pd_fecha in date) return number is
  Result number;
  cursor c_es_sociedad (cv_fact_bscs in varchar2) is
   select sysadm.VALIDA_DIGITO_CF(r.billcycle)
    from sysadm.orderhdr_all e, sysadm.document_reference r
   where  e.customer_id=r.customer_id
   and e.ohentdate =r.date_created
   and e.ohrefnum = cv_fact_bscs;
  ln_valor_sociedad number:=0;
  ln_sociedad number:=0;
  lv_parametro varchar2(50); 
  lv_valor_parametro varchar2(50); 
begin
   ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Cynthia Quinde.
  -- Created:  28/07/2016 14:55:37
  -- Proyecto: [10897]-Proyecto documento electronicos por reimpresion de servicios no inventariables
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Jimmy Larrosa
  -- Motivo:    Paquete que realiza consultas de las facturas de BSCS y retorna un XML
  ---------------------------------------------------------------------------------------------------
  EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY'' NLS_NUMERIC_CHARACTERS= ''.,''';
  open c_es_sociedad(pv_factura_bscs);
  fetch c_es_sociedad into ln_sociedad;
  close c_es_sociedad;
  lv_parametro:=to_char(pd_fecha,'DD')||'_'||ln_sociedad;  
  lv_valor_parametro:=INK_API_FACTURABSCS_CF.GETPARAMETROS(lv_parametro);  
  ln_valor_sociedad:=to_number(lv_valor_parametro);
   return   ln_valor_sociedad ;
exception
  when others then
    return 0;
end FUN_VALIDA_CORTE_SOCIEDAD;
/
