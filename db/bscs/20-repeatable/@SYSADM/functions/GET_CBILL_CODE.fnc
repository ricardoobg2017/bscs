create or replace function get_cbill_code(pv_bank_id number) return number is
  Result number;

  cursor xx(c_pv_bank_id number) is
    select bank_cbill

      from mm_mapeo_prod_formas
     where bank_bscs = c_pv_bank_id;

begin

  open xx(pv_bank_id);

  fetch xx
    into Result;

  if xx%notfound then
    return(-1);
  end if;

  close xx;

  return(Result);
end get_cbill_code;
/
