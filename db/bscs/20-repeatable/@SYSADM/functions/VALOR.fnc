CREATE OR REPLACE function valor(num varchar2) return boolean as
var number;
begin
     var:= to_number(num);
     
     return true;
     
exception
when others then
     return false;
     
end;
/

