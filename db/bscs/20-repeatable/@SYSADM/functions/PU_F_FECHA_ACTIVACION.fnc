CREATE OR REPLACE function pu_f_fecha_activacion(celular in varchar2) return varchar2 is
  Result varchar2(10);
begin
/* Creado el 07/09/2006 por CLS - MJO 
   Funci�n para obtener la fecha de activaci�n del feature de seguro de equipo
   de una l�nea
*/

  select distinct to_char(x.fecha_desde,'dd/mm/yyyy')
  into Result
  from cl_detalles_servicios@axis x,
       bs_servicios_paquete@axis  y
  where y.sn_code=29 and
        x.id_tipo_detalle_serv like '%'||y.cod_axis||'%'  and
        x.id_servicio=celular and 
        x.estado='A';
        
  return(Result);
Exception 
 when no_data_found then
   return null;  
end pu_f_fecha_activacion;
/

