CREATE OR REPLACE FUNCTION PBF_DEVUELVE_VALOR(PV_TRAMA     IN VARCHAR2,
                              PV_DATO      IN VARCHAR2,
                              PV_SEPARADOR IN VARCHAR2) RETURN VARCHAR2 IS
  --===========================================================
  -- Proyecto: 10111 Notas de Credito Payback
  -- Creado por :  CLS Luis Olvera
  -- Fecha:    28/07/2005
  -- Motivo: EXTRAER RESULTADOS DE TRAMA
  --===========================================================
  
        
    
    
    LV_TRAMA     VARCHAR2(5000);
    LV_DATO      VARCHAR2(100);
    LV_SEPARADOR VARCHAR2(10);
  
    LN_POS_PARAMETRO   NUMBER;
    LN_POS_IGUAL       NUMBER;
    LN_POS_SEP         NUMBER;
    LV_VALOR_PARAMETRO VARCHAR2(4000);
  
  BEGIN
    LV_TRAMA     := PV_TRAMA;
    LV_DATO      := PV_DATO;
    LV_SEPARADOR := PV_SEPARADOR;
  
    LN_POS_PARAMETRO := INSTR(LV_TRAMA, LV_DATO);
  
    IF LN_POS_PARAMETRO = 0 THEN
    
      RETURN NULL;
    ELSE
      LN_POS_IGUAL       := INSTR(LV_TRAMA, '=', LN_POS_PARAMETRO);
      LN_POS_SEP         := INSTR(LV_TRAMA, LV_SEPARADOR, LN_POS_PARAMETRO);
      LV_VALOR_PARAMETRO := SUBSTR(LV_TRAMA,
                                   LN_POS_IGUAL + 1,
                                   LN_POS_SEP - LN_POS_IGUAL - 1);
      RETURN LV_VALOR_PARAMETRO;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END PBF_DEVUELVE_VALOR;
/
