CREATE OR REPLACE FUNCTION PBF_NOTA_CREDITO(PV_ID_CUENTA         IN VARCHAR2,
                                            PV_ID_IDENTIFICACION IN VARCHAR2,
                                            PV_NEGOCIACION       IN VARCHAR2,
                                            PV_FECHA_FAC         IN VARCHAR2,
                                            PN_MONTO_PLAN        IN NUMBER)
  -- PROYECTO   : [10111] PAYBACK
  -- CREADO POR : CLS LUIS OLVERA
  -- FECHA      : 15/04/2015
  -- OBJETIVO   : CONSULTAR LOS RUBROS DE LA HOJA DE C�LCULO DE PROCESO NOTA DE CR�DITO 
  
  RETURN VARCHAR2 IS

  CURSOR C_RUBROS IS
    SELECT DISTINCT A.NOMBRE2,
                    (SELECT DISTINCT ID_PRODUCTO
                       FROM COB_SERVICIOS
                      WHERE UPPER(TRIM(NOMBRE2)) = UPPER(A.NOMBRE2)
                        AND ID_PRODUCTO IS NOT NULL
                        AND ROWNUM < 2) ID_PRODUCTO,
                    (SELECT NVL((SELECT VALOR
                                  FROM GV_PARAMETROS
                                 WHERE ID_TIPO_PARAMETRO = 8878
                                   AND NOMBRE = 'GRUPO_PRODUCTO'
                                   AND DESCRIPCION = BALANCE),
                                ID_PRODUCTO)
                       FROM COB_SERVICIOS
                      WHERE UPPER(TRIM(NOMBRE2)) = UPPER(A.NOMBRE2)
                        AND ID_PRODUCTO IS NOT NULL
                        AND ROWNUM < 2) GRUPO_PRODUCTO,
                    A.NOMBRE,
                    A.TIPO,
                    TO_NUMBER(A.COBRADO, '99999999999.9999') -
                    TO_NUMBER(A.DESCUENTO, '99999999999.9999') VALOR_NOTA
      FROM PB_COMPLAIN_TEMPORAL A,
           (SELECT DISTINCT UPPER(TRIM(NOMBRE2)) NOMBRE2,
                            UPPER(TRIM(NOMBRE)) NOMBRE,
                            ID_PRODUCTO
              FROM COB_SERVICIOS) B
     WHERE A.TIPO IS NOT NULL
       AND UPPER(A.NOMBRE2) = UPPER(B.NOMBRE2)
       AND TO_NUMBER(A.COBRADO, '99999999999.9999') -
           TO_NUMBER(A.DESCUENTO, '99999999999.9999') > 0
    
    UNION ALL
    
    SELECT A.NOMBRE2,
           B.ID_PRODUCTO,
           '' GRUPO_PRODUCTO,
           A.NOMBRE,
           A.TIPO,
           TO_NUMBER(A.COBRADO, '99999999999.9999') -
           TO_NUMBER(A.DESCUENTO, '99999999999.9999') VALOR_NOTA
      FROM PB_COMPLAIN_TEMPORAL A, DOC1.SERVICE_LABEL_DOC1 B
     WHERE A.TIPO IS NOT NULL
       AND REPLACE(UPPER(A.NOMBRE2), 'PORTA', 'CLARO') = UPPER(B.DES)
       AND REPLACE(A.NOMBRE, 'PORTA', 'CLARO') = B.DES
       AND TO_NUMBER(A.COBRADO, '99999999999.9999') -
           TO_NUMBER(A.DESCUENTO, '99999999999.9999') > 0;

  -----------------------------------------------------------   
  CURSOR C_COMPLAIN_DESCTO(CV_SHORT_NAME VARCHAR2) IS
    SELECT D.CTA_DEVOL, D.TIPO_IVA
      FROM PB_COMPLAIN_TEMPORAL D
     WHERE D.NOMBRE2 = CV_SHORT_NAME;

  -----------------------------------------------------------   
  CURSOR C_TIPOS_GRABAN_IVA_ICE(CV_TIPO VARCHAR2) IS
    SELECT COUNT('X')
      FROM GV_PARAMETROS
     WHERE NOMBRE = 'TIPO DE SERVICIO ADICIONALES'
       AND ID_TIPO_PARAMETRO = 10111
       AND VALOR = CV_TIPO;
  ------------------------------------------------------------
  CURSOR C_CUSTOMER_ALL(CV_CUENTA VARCHAR2) IS
    SELECT DECODE(COSTCENTER_ID, 1, 'GYE', 2, 'UIO') ID_LOCALIDAD
      FROM CUSTOMER_ALL
     WHERE CUSTCODE = CV_CUENTA;
  ------------------------------------------------------------
/*  CURSOR C_BODEGA(CV_IDPARAMETRO VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS
     WHERE ID_TIPO_PARAMETRO = 10111
       AND ID_PARAMETRO = CV_IDPARAMETRO;
  ------------------------------------------------------------*/
  CURSOR C_CICLO(PV_PERIODOS VARCHAR2) IS
    SELECT CB.ID_CICLO
      FROM FA_CICLOS_BSCS CB
     WHERE CB.DIA_INI_CICLO = SUBSTR(PV_PERIODOS, 1, 2);
     

  ------------------------------------------------------------
  CURSOR C_OBTENER_VALOR(CN_ID_TIPO_PARAMETRO NUMBER,
                         CV_ID_PARAMETRO      VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS GP
     WHERE GP.ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
       AND GP.ID_PARAMETRO = CV_ID_PARAMETRO;

  ------------------------------------------------------------
  CURSOR C_EXTRAE_FACTURA(CV_CUENTA VARCHAR2) IS
    SELECT DISTINCT CT.OHREFNUM
      FROM PB_COMPLAIN_TEMPORAL CT
     WHERE CT.CUSTCODE = CV_CUENTA;
  ------------------------------------------------------------


  LN_TOTAL_X_F       NUMBER := 0;
  LN_TOTAL_IMPON_X_F NUMBER := 0;
  LN_TOTAL_EXCTO_X_F NUMBER := 0;
  LN_TOTAL_IVA_X_F   NUMBER := 0;
  LN_TOTAL_ICE_X_F   NUMBER := 0;

  LE_ERROR EXCEPTION;
  LV_CICLO                VARCHAR2(100);
  LV_PARAMETRO            VARCHAR2(100);
  LV_CUENTA               VARCHAR2(100);
  LV_ERROR                VARCHAR2(1000);
  LV_REGION               VARCHAR2(100);
  LV_COMPANIA             VARCHAR2(100);
  LV_BODEGA               NUMBER;
  LV_FORMATO              VARCHAR2(100) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LV_CTA_DEVOL_NEW        VARCHAR2(30);
  LN_TIPO_GRABA_IVA_ICE   NUMBER;
  LV_TIPO_IVA             VARCHAR2(30);
  LV_CUENTA_ICE           VARCHAR2(50) := 0;
  LV_ID_IDENTIFICACION    VARCHAR2(100);
  LV_USUARIO              VARCHAR2(100);
  LV_ID_NEGOCIACION       VARCHAR2(50);
  LV_FECHA_FACT           VARCHAR2(8);
  LV_MES                  VARCHAR2(2);
  LV_APLICA_CTA_DCTO      VARCHAR2(20);
  LN_ID_TIP_PAR           NUMBER := 10111;
  LV_ID_PAR_CTA_DCTO      VARCHAR(20) := 'CTA_DEVOLUCION_NC';
  LV_USUARIO_PAYBACK      VARCHAR2(50):='USUARIO_NC';
  LV_TRAMA_RES            VARCHAR2(3000);
  LV_FACTURA              VARCHAR2(50);
  LV_ID_REQ               VARCHAR2(50);
  LV_ID_REQ_EIS           VARCHAR2(50);
  LB_FOUND                BOOLEAN := TRUE;
  LV_FAULT_CODE           VARCHAR2(500);
  LV_FAULT_STRING         VARCHAR2(500);
  LV_TRAMA_PARAMETROS     VARCHAR2(30000);
  LX_RESPUESTA            SYS.XMLTYPE;
  LN_ERROR_GYE            NUMBER;
  LV_ERROR_GYE            VARCHAR2(500);
  LN_ERROR                NUMBER;
  --LV_RESPONSE_XML         VARCHAR2(30000);
  LV_NOTA_CREDITO_ORDEN   VARCHAR2(50);
  LV_NOTA_CREDITO_FACTURA VARCHAR2(50);
  LV_AUTORIZACION         VARCHAR2(50);
  LN_TOT_IMPON            NUMBER;
  LN_IVA_IMPO             NUMBER;
  LN_TOTAL_IVA            NUMBER;
  LN_MONTO_PLAN           NUMBER;
  --LN_TOTAL_PAG            NUMBER;
  LN_RESIDUO              NUMBER;
  LV_DS                   VARCHAR2(20);
  LN_SERV_INFO            NUMBER;
  LV_PARAMETROS           VARCHAR2(500);
  LV_TRAMA                VARCHAR2(500);
  LN_TOTAL                NUMBER;

BEGIN

  LV_CUENTA            := PV_ID_CUENTA;
  LV_ID_IDENTIFICACION := PV_ID_IDENTIFICACION;
  LV_ID_NEGOCIACION    := PV_NEGOCIACION;
  LV_FECHA_FACT        := PV_FECHA_FAC;
  LN_MONTO_PLAN        := PN_MONTO_PLAN;
  

  IF LV_CUENTA IS NULL THEN
    LV_ERROR := 'INGRESE EL NUMERO DE CUENTA DEL CLIENTE';
    RAISE LE_ERROR;
  END IF;

  IF LV_ID_IDENTIFICACION IS NULL THEN
    LV_ERROR := 'INGRESE LA IDENTIFICACION DEL CLIENTE';
    RAISE LE_ERROR;
  END IF;

  IF LV_ID_NEGOCIACION IS NULL THEN
    LV_ERROR := 'INGRESE EL NUMERO DE NEGOCIACION';
    RAISE LE_ERROR;
  END IF;

  IF LV_FECHA_FACT IS NULL THEN
    LV_ERROR := 'INGRESE EL PERIODO A APLICAR EL CREDITO.';
    RAISE LE_ERROR;
  END IF;
  
  IF LN_MONTO_PLAN IS NULL THEN
    LV_ERROR := 'INGRESE EL VALOR PLANIFICADO.';
    RAISE LE_ERROR;
  END IF;
  
  
  
  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR,  LV_USUARIO_PAYBACK);
  FETCH C_OBTENER_VALOR
    INTO LV_USUARIO;
  CLOSE C_OBTENER_VALOR;
  

  OPEN C_EXTRAE_FACTURA(LV_CUENTA);
  FETCH C_EXTRAE_FACTURA
    INTO LV_FACTURA;
  CLOSE C_EXTRAE_FACTURA;

  OPEN C_CUSTOMER_ALL(LV_CUENTA);
  FETCH C_CUSTOMER_ALL
    INTO LV_REGION;
  CLOSE C_CUSTOMER_ALL;

  IF LV_REGION = 'UIO' THEN
    LV_COMPANIA  := '1';
    LV_PARAMETRO := 'GV_BODEGA_UIO';
  
  ELSE
    LV_COMPANIA  := '2';
    LV_PARAMETRO := 'GV_BODEGA_GYE';
    --
  END IF;

  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR,LV_PARAMETRO);
  FETCH C_OBTENER_VALOR
    INTO LV_BODEGA;
  CLOSE C_OBTENER_VALOR;

  OPEN C_CICLO(LV_FECHA_FACT);
  FETCH C_CICLO
    INTO LV_CICLO;
  CLOSE C_CICLO;

  LV_MES := SUBSTR(LV_FECHA_FACT, 3, 2);

  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR,LV_ID_PAR_CTA_DCTO);
  FETCH C_OBTENER_VALOR
    INTO LV_APLICA_CTA_DCTO;
  CLOSE C_OBTENER_VALOR;

  
  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR, 'IVA_NC');
  FETCH C_OBTENER_VALOR
    INTO LN_IVA_IMPO;
  LB_FOUND := C_OBTENER_VALOR%FOUND;
  CLOSE C_OBTENER_VALOR;

  
  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR, 'ID_REQ_EIS_NC');
  FETCH C_OBTENER_VALOR
    INTO LV_ID_REQ_EIS;
  CLOSE C_OBTENER_VALOR;
  

  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR, 'DATA_SOURCE_GYE');
  FETCH C_OBTENER_VALOR
    INTO LV_DS;
  CLOSE C_OBTENER_VALOR;
  

  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR, 'ID_SERVICIO_INFORMACION_1');
  FETCH C_OBTENER_VALOR
    INTO LN_SERV_INFO;
  CLOSE C_OBTENER_VALOR; 

      
    -- CONSUMO DEL EIS                                                                 
  

      LV_PARAMETROS := 'dsId=' || LV_DS || ';' ||
                       'pnIdServicioInformacion=' || LN_SERV_INFO || ';' ||
                       'pvParametroBind1=' || LV_COMPANIA || ';' ||
                       'pvParametroBind2=' || LV_ID_IDENTIFICACION || ';' ||
                       'pvParametroBind3=' || LV_FACTURA || ';' ||
                       'pvParametroBind4=;pvParametroBind5=;';


      LX_RESPUESTA := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LV_ID_REQ_EIS,
                                                                PV_PARAMETROS_REQUERIMIENTO => LV_PARAMETROS,
                                                                PV_FAULT_CODE               => LV_FAULT_CODE,
                                                                PV_FAULT_STRING             => LV_FAULT_STRING);
    
      IF LV_FAULT_CODE IS NULL AND LV_FAULT_STRING IS NULL THEN
        IF LX_RESPUESTA IS NOT NULL THEN
        
        
          LV_TRAMA := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LV_ID_REQ_EIS,
                                                             PR_RESPUESTA        => LX_RESPUESTA,
                                                             PV_NOMBRE_PARAMETRO => 'pvresultadoOut');
        
          LN_TOTAL := PBF_DEVUELVE_VALOR(PV_TRAMA     => LV_TRAMA,
                                         PV_DATO      => 'TOTAL',
                                         PV_SEPARADOR => '|');
        

        END IF;
      ELSE
        LV_ERROR := LV_FAULT_CODE || ' - ' || LV_FAULT_STRING;
        RAISE LE_ERROR;
      END IF;




  FOR J IN C_RUBROS LOOP
  
    LV_CTA_DEVOL_NEW := NULL;
    LV_TIPO_IVA      := NULL;
  
    OPEN C_COMPLAIN_DESCTO(J.NOMBRE2);
    FETCH C_COMPLAIN_DESCTO
      INTO LV_CTA_DEVOL_NEW, LV_TIPO_IVA;
    CLOSE C_COMPLAIN_DESCTO;
    
 
  
    IF J.NOMBRE2 = 'IVA' AND J.TIPO = '003 - IMPUESTOS' THEN
    
      LN_TOTAL_IVA_X_F := NVL(LN_TOTAL_IVA_X_F, 0) + J.VALOR_NOTA;
    
    ELSIF J.NOMBRE2 = 'ICE' AND J.TIPO = '003 - IMPUESTOS' THEN
    
      LN_TOTAL_ICE_X_F := NVL(LN_TOTAL_ICE_X_F, 0) + J.VALOR_NOTA;
    
      IF LV_CTA_DEVOL_NEW IS NOT NULL THEN
        LV_CUENTA_ICE := nvl(LV_CTA_DEVOL_NEW, 0);
      END IF;
    
    ELSIF J.TIPO <> '003 - IMPUESTOS' THEN
    
      OPEN C_TIPOS_GRABAN_IVA_ICE(J.TIPO);
      FETCH C_TIPOS_GRABAN_IVA_ICE
        INTO LN_TIPO_GRABA_IVA_ICE;
      CLOSE C_TIPOS_GRABAN_IVA_ICE;
    
      IF LN_TIPO_GRABA_IVA_ICE > 0 THEN
      
        LN_TOTAL_IMPON_X_F := NVL(LN_TOTAL_IMPON_X_F, 0) +
                              NVL(J.VALOR_NOTA, 0);
      
      ELSE
      
        --LN_TOTAL_EXCTO_X_F := NVL(LN_TOTAL_EXCTO_X_F, 0) +
        --                      NVL(J.VALOR_NOTA, 0);
        
        LN_TOTAL_EXCTO_X_F:=0;
      
      END IF;
    
    END IF;
  END LOOP;

  LN_TOTAL_X_F := NVL(LN_TOTAL_IMPON_X_F, 0) + NVL(LN_TOTAL_EXCTO_X_F, 0) +
                  NVL(LN_TOTAL_IVA_X_F, 0) + NVL(LN_TOTAL_ICE_X_F, 0);
  
  
  IF (LN_TOTAL<>0)THEN
  
  LN_TOTAL_X_F:=LN_TOTAL_X_F-LN_TOTAL;
    
  
  END IF;
  

  
  LN_RESIDUO:=LN_MONTO_PLAN-LN_TOTAL_X_F;
  
  IF(LN_RESIDUO<0)THEN
  
  LN_TOTAL_X_F:=LN_MONTO_PLAN;
  
  END IF;
  
  LN_TOT_IMPON:=ROUND((LN_TOTAL_X_F/LN_IVA_IMPO),2);
  
  LN_TOTAL_IVA:=LN_TOTAL_X_F-LN_TOT_IMPON;

  

  
  OPEN C_OBTENER_VALOR(LN_ID_TIP_PAR, 'ID_REQ_SRE_NC');
  FETCH C_OBTENER_VALOR
    INTO LV_ID_REQ;
  CLOSE C_OBTENER_VALOR;


  
    --- ID_REQUERIMIENTO_SOAP_SRE
  
    LV_TRAMA_PARAMETROS := 'PV_COMPANIA=' || LV_COMPANIA || ';PV_USUARIO=' ||
                           LV_USUARIO || ';PN_BODEGA=' || LV_BODEGA ||
                           ';PV_FACTURA=' || LV_FACTURA || ';PV_CLIENTE=' ||
                           LV_ID_IDENTIFICACION || ';PV_CUENTA=' ||
                           LV_CUENTA || ';PN_TOTAL=' || LN_TOTAL_X_F ||
                           ';PN_TOTAL_IMPONIBLE=' || LN_TOT_IMPON ||
                           ';PN_TOTAL_EXCENTO=' || LN_TOTAL_EXCTO_X_F ||
                           ';PN_TOTAL_IVA=' || LN_TOTAL_IVA ||
                           ';PN_TOTAL_ICE=' || LN_TOTAL_ICE_X_F ||
                           ';PV_CICLO=' || LV_CICLO || ';PV_MES=' || LV_MES ||
                           ';PV_CUENTA_ICE=' || LV_CUENTA_ICE ||
                           ';PV_APLICA_CTA_DCTO=' || LV_APLICA_CTA_DCTO ||
                           ';PV_PAYBACK=' || LV_ID_NEGOCIACION || ';';
  
    LX_RESPUESTA := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => TO_NUMBER(LV_ID_REQ),
                                                              PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETROS,
                                                              PV_FAULT_CODE               => LV_FAULT_CODE,
                                                              PV_FAULT_STRING             => LV_FAULT_STRING);
  
    IF LV_FAULT_CODE IS NULL AND LV_FAULT_STRING IS NULL THEN
    
      IF LX_RESPUESTA IS NOT NULL THEN
      
       -- LV_RESPONSE_XML := SUBSTR(LX_RESPUESTA.GETSTRINGVAL(), 1, 4000);
      
        LN_ERROR := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                           PR_RESPUESTA        => LX_RESPUESTA,
                                                           PV_NOMBRE_PARAMETRO => 'COD_RESPUESTA');
        
        LV_ERROR := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                           PR_RESPUESTA        => LX_RESPUESTA,
                                                           PV_NOMBRE_PARAMETRO => 'MENSAJE');
        
        
        IF LN_ERROR <> 0 AND LV_ERROR IS NULL THEN
          LN_ERROR := -1;
          RAISE LE_ERROR;
        END IF;

        
         
        LV_NOTA_CREDITO_ORDEN:=SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                                      PR_RESPUESTA        => LX_RESPUESTA,
                                                                      PV_NOMBRE_PARAMETRO => 'PV_NOTA_CREDITO_ORDEN');  
        
        LV_NOTA_CREDITO_FACTURA:=SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                                        PR_RESPUESTA        => LX_RESPUESTA,
                                                                        PV_NOMBRE_PARAMETRO => 'PV_NOTA_CREDITO_FACTURA');
        
        LV_AUTORIZACION:=SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                                PR_RESPUESTA        => LX_RESPUESTA,
                                                                PV_NOMBRE_PARAMETRO => 'PV_AUTORIZACION');

        
        
        LN_ERROR_GYE := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                               PR_RESPUESTA        => LX_RESPUESTA,
                                                               PV_NOMBRE_PARAMETRO => 'PN_ERROR');
      
        LV_ERROR_GYE := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => TO_NUMBER(LV_ID_REQ),
                                                               PR_RESPUESTA        => LX_RESPUESTA,
                                                               PV_NOMBRE_PARAMETRO => 'PV_ERROR');
      

        IF LN_ERROR_GYE IS NULL AND LV_ERROR_GYE IS NULL THEN
          LN_ERROR_GYE := 0;
          LV_ERROR_GYE :='PROCESO EXITOSO';
        END IF;


        
        
        
        LV_TRAMA_RES:='NC_ORDEN='||LV_NOTA_CREDITO_ORDEN||'|'||
                      'NC_FACTURA='||LV_NOTA_CREDITO_FACTURA||'|'||
                      'AUTORIZACION='||LV_AUTORIZACION||'|'||
                      'TOTAL_FACT='||LN_TOTAL_X_F||'|'||
                      'RESIDUO='||LN_RESIDUO||'|'||
                      'PN_ERROR='||LN_ERROR_GYE||'|'||
                      'PV_ERROR='||LV_ERROR_GYE||'|';
        
        
      
      END IF;
    ELSE
      LN_ERROR := -1;
      LV_ERROR := LV_FAULT_CODE || '-' || LV_FAULT_STRING;
      RAISE LE_ERROR;
    END IF;
  
  
  
  RETURN LV_TRAMA_RES;

EXCEPTION
  WHEN LE_ERROR THEN
  
    ROLLBACK;
    
    LV_TRAMA_RES:='NC_ORDEN='||LV_NOTA_CREDITO_ORDEN||'|'||
                  'NC_FACTURA='||LV_NOTA_CREDITO_FACTURA||'|'||
                  'AUTORIZACION='||LV_AUTORIZACION||'|'||
                  'TOTAL_FACT='||LN_TOTAL_X_F||'|'||
                  'RESIDUO='||LN_TOTAL_X_F||'|'||
                  'PN_ERROR='||LN_ERROR||'|'||
                  'PV_ERROR='||LV_ERROR||'|';

    
    
    RETURN LV_TRAMA_RES;
  
  WHEN OTHERS THEN
    ROLLBACK;
    LN_ERROR := -1;
    LV_ERROR := SUBSTR(SQLERRM, 1, 300);

    LV_TRAMA_RES:='NC_ORDEN='||LV_NOTA_CREDITO_ORDEN||'|'||
                  'NC_FACTURA='||LV_NOTA_CREDITO_FACTURA||'|'||
                  'AUTORIZACION='||LV_AUTORIZACION||'|'||
                  'TOTAL_FACT='||LN_TOTAL_X_F||'|'||
                  'RESIDUO='||LN_TOTAL_X_F||'|'||
                  'PN_ERROR='||LN_ERROR||'|'||
                  'PV_ERROR='||LV_ERROR||'|';
    
    RETURN LV_TRAMA_RES;
  
END PBF_NOTA_CREDITO;
/
