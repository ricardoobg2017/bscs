CREATE OR REPLACE Function OBTIENE_TELEFONO_DNC_INT (pv_telefono         In Varchar2,
                                                    pn_red              In Number Default Null,
                                                    pv_version_telefono In Varchar2 Default 'N')
  Return Varchar2 Is

  -- Lider Proy : SIS William Reyes
  --Created     : 25/06/2013 12:09:54
  -- Proyecto   : [9054] - Nuevo Plan de numeración dnc 
  -- AUTOR      :   RGT GIANCARLOS CERCADO
  
  lv_telefono         Varchar2(20);
   v_lista1           Varchar2(4000);
  lv_version_telefono varchar2(5);
  --band_lineas_prueba varchar2(2);
 lv_pv_telefono      Varchar2(20);

BEGIN
  
  lv_pv_telefono :=TRIM(pv_telefono);

  IF pnk_numeros_ptfn.fnc_es_numerico(lv_pv_telefono) = 'S' THEN --if principal que valida si es un numero y no contenga caracteres
    
  lv_version_telefono := pv_version_telefono;
  
  -- si pnk_numeros_ptfn.get_band es igual S valido telefono en v_lista1
  -- si pnk_numeros_ptfn.get_band es igual N respeto lo q venga parametro pv_version_telefono
  if pnk_numeros_ptfn.get_band = 'S' then
    select instr(pnk_numeros_ptfn.get_numeros,substr(lv_pv_telefono,-8)) into v_lista1 from dual;
   
    -- si lv_pv_telefono existe en la lista de telefono configurados
    -- respeto lo q venga parametro pv_version_telefono
    -- caso contrario aplico logica nueva
    if v_lista1 not in ('0') then
      lv_version_telefono := pv_version_telefono;
    else
       lv_version_telefono := 'V'; 
    end if;
  else
    lv_version_telefono := pv_version_telefono;
  end if;


/*BLOQUE GCERCADO*/

/*************CASOS ESPECIALES***************/
-- EXISTEN PROCESOS QUE AGREGAN EL 9 AL NUMERO YA INGRESADO POR PARAMETRO.

  IF LENGTH(lv_pv_telefono)=13 AND INSTR(lv_pv_telefono,'9',1) = 1 THEN
    lv_pv_telefono := SUBSTR(lv_pv_telefono,2);

     ELSIF LENGTH(lv_pv_telefono)=12 AND INSTR(lv_pv_telefono,'9',1) = 1 THEN
    lv_pv_telefono := SUBSTR(lv_pv_telefono,2);

     ELSIF LENGTH(lv_pv_telefono)=11 AND INSTR(lv_pv_telefono,'9',1) = 1 THEN
    lv_pv_telefono := SUBSTR(lv_pv_telefono,2);

     ELSIF LENGTH(lv_pv_telefono)=10 AND INSTR(lv_pv_telefono,'9',1) = 1 THEN
    lv_pv_telefono := SUBSTR(lv_pv_telefono,2);
     END IF;



/************FIN CASOS ESPECIALES*********/



/***************TRATAMIENTO*********************/

    IF LENGTH(lv_pv_telefono)=12 THEN
      IF INSTR(lv_pv_telefono,'5939',1) = 1 THEN
      lv_telefono := SUBSTR(lv_pv_telefono,5,8);
      ELSE
       RETURN pv_telefono;
      END IF;


     ELSIF LENGTH(lv_pv_telefono)=11 THEN
      IF INSTR(lv_pv_telefono,'593',1) = 1 THEN
      lv_telefono := SUBSTR(lv_pv_telefono,4,8);
     ELSE
       RETURN pv_telefono;
      END IF;

      ELSIF LENGTH(lv_pv_telefono)=10 THEN
      IF INSTR(lv_pv_telefono,'09',1) = 1 OR /**/INSTR(lv_pv_telefono,'99',1) = 1/*CASO DE PORTAL PORTA*/ THEN
      lv_telefono := SUBSTR(lv_pv_telefono,3,8);
      ELSE
       RETURN pv_telefono;
      END IF;

      ELSIF LENGTH(lv_pv_telefono)=9 THEN
      IF INSTR(lv_pv_telefono,'0',1) = 1 OR INSTR(lv_pv_telefono,'9',1) = 1 THEN
      lv_telefono := SUBSTR(lv_pv_telefono,2,8);
      ELSE
       RETURN pv_telefono;
      END IF;

      ELSIF LENGTH(lv_pv_telefono)=8 THEN
        lv_telefono:=lv_pv_telefono;

     ELSE
       RETURN pv_telefono;
       END IF;



/* FIN TRATAMIENTO DE LOS POSIBLES NUMEROS*/
    IF NVL(lv_version_telefono,'N') = 'N' THEN
       
        RETURN '5939'||lv_telefono;
              
    ELSE
           
        RETURN '593'||lv_telefono;
           
    END IF;



ELSE 
   
RETURN lv_pv_telefono;
END IF;
  
  
end OBTIENE_TELEFONO_DNC_INT;
/
