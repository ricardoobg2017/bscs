CREATE OR REPLACE function numtohex(v_hex number) return varchar2
  is

    hex          varchar2(4);
    num1         number;
    num2         number;

  begin

    num1 := trunc(v_hex/16);
    num2 := v_hex-(num1*16);

    if ( num1 >= 0 and num1 <= 9 ) then
      hex  := hex||to_char(num1);
    end if;

    if num1 = 10 then hex := hex||'A'; end if;
    if num1 = 11 then hex := hex||'B'; end if;
    if num1 = 12 then hex := hex||'C'; end if;
    if num1 = 13 then hex := hex||'D'; end if;
    if num1 = 14 then hex := hex||'E'; end if;
    if num1 = 15 then hex := hex||'F'; end if;

    if ( num2 >= 0 and num2 <= 9 ) then
      hex  := hex||to_char(num2);
    end if;

    if num2 = 10 then hex := hex||'A'; end if;
    if num2 = 11 then hex := hex||'B'; end if;
    if num2 = 12 then hex := hex||'C'; end if;
    if num2 = 13 then hex := hex||'D'; end if;
    if num2 = 14 then hex := hex||'E'; end if;
    if num2 = 15 then hex := hex||'F'; end if;

    return hex;
end numtohex;
/

