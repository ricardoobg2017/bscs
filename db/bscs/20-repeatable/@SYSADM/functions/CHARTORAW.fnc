CREATE OR REPLACE function chartoraw(v_char long) return long raw
  is
    rawdata      long raw;
    rawlen       number;
--    hex          varchar2(32760);
    hex          long;
    i            number;
  begin
    rawlen := length(v_char);
    i := 1;

    while i <= rawlen
      loop
        hex  := numtohex(ascii(substrb(v_char,i,1)));
        rawdata := rawdata || HEXTORAW(hex);
      i := i + 1;
    end loop;

    return rawdata;

end chartoraw;
/

