create or replace function FUN_PRORATEO_BSCS(pd_fecha in date) return VARCHAR2 is
 cursor c_valor(cd_fecha in date, cv_estado in varchar2)  is
    select x.fech_prorateo 
    from bscs_in_impu_hist x
    where cd_fecha between fech_ini_hist and fech_fin_hist
    and x.estado=nvl(cv_estado,x.estado) ;

  lv_fech_pro varchar2(20):=null;
begin
   ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Cynthia Quinde.
  -- Created:  28/07/2016 14:55:37
  -- Proyecto: [11163]-Cambio de Fecha de Facturacion de BSCS
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Jimmy Larrosa
  -- Motivo:    Funci�n que retorna el mes de prorateo
  ---------------------------------------------------------------------------------------------------

    open c_valor(pd_fecha,null);
    fetch c_valor into lv_fech_pro;
    close c_valor;

    return   lv_fech_pro ;
exception
  when others then
    return null;
end FUN_PRORATEO_BSCS;
/
