CREATE OR REPLACE function GET_PRORATEO(pn_fecha_cambio in date, pn_saldo_plan in number) return number is

ln_inception_date number;

begin
 ln_inception_date:=to_number(to_char(pn_fecha_cambio,'dd'));
  dbms_output.put_line (ln_inception_date);
  
 if ln_inception_date > 23 then
  return ((53 - ln_inception_date + 1) / 30) * pn_saldo_plan;
 else
  return ((23 - ln_inception_date + 1) / 30) * pn_saldo_plan;                       
 end if;         
end ;
/

