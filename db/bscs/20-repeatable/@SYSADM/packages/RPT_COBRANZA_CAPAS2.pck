CREATE OR REPLACE package RPT_COBRANZA_CAPAS2 is

    FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                           pv_region      in varchar2,
                           pn_monto       out number,
                           pv_error       out varchar2)
                           RETURN NUMBER;

    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER;

    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pv_region       in varchar2,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER;

    PROCEDURE COBRANZAS_EFECTIVO (pd_fecha_ini in date,
                                  pd_fecha_fin in date);

    PROCEDURE COBRANZAS_CREDITO (pd_fecha_ini in date,
                                 pd_fecha_fin in date);

    PROCEDURE ACTUALIZA_TOTALES;

end RPT_COBRANZA_CAPAS2;
/
CREATE OR REPLACE package body RPT_COBRANZA_CAPAS2 is

  -- Variables locales
  gv_funcion     varchar2(30);
  gv_mensaje     varchar2(500);
  ge_error       exception;

  -- Funci�n que calcula el total facturado por per�odo y regi�n
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pv_error       out varchar2)
                         RETURN NUMBER IS

    lb_notfound  boolean;
    ln_retorno   number;

    BEGIN

      select sum(ohinvamt_doc) -- total facturado
      into   pn_monto
      from   orderhdr_all t, costcenter r, customer_all c
      where  t.customer_id   = c.customer_id
      and    c.costcenter_id = r.cost_id
      and    t.ohstatus = 'IN'
      and    r.cost_code     = pv_region
      and    t.ohentdate     = pd_facturacion;

      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_FACTURA '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_FACTURA '||sqlerrm;
    END TOTAL_FACTURA;

    -- Funci�n que calcula el porcentaje de los pagos y notas de cr�dito
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER IS
    ln_retorno   number;

    BEGIN
      pn_porc    := ROUND((pn_amortizado*100)/pn_total,2);
      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when others then
        return(0);
        pv_error := 'FUNCION: PORCENTAJE '||sqlerrm;
    END PORCENTAJE;

    -- Funci�n que calcula la cantidad de facturas amortizadas diariamente
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pv_region       in varchar2,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS

    cursor c_valores(pd_fecha date, periodo date, region varchar2) is
      select count(*)
      from   orderhdr_all t, cashdetail d, cashreceipts_all a, costcenter r
      where  t.customer_id  = a.customer_id
      and    a.cacostcent   = r.cost_id
      and    d.cadxact      = a.caxact
      and    r.cost_code    = pv_region
      and    t.ohopnamt_doc = 0
      and    t.ohstatus     = 'IN'
      and    t.ohentdate    = periodo
      and    a.cachkdate   <= pd_fecha;

      ln_retorno   number;
      lb_notfound  boolean;
      ld_fecha     date;
      ln_cancelado number;
      ln_facturas  number;

    BEGIN
      select count(*)
      into  ln_facturas
      from  orderhdr_all t, customer_all c, costcenter r
      where t.customer_id   = c.customer_id
      and   c.costcenter_id = r.cost_id
      and   t.ohstatus = 'IN'
      and   t.ohentdate     = pd_facturacion
      and   r.cost_code     = pv_region;

      open c_valores(pd_fecha, pd_facturacion, pv_region);
      fetch c_valores into ln_cancelado;
      lb_notfound := c_valores%notfound;
      close c_valores;

      if lb_notfound then
        pv_error := 'No se encontraron datos';
      else
       pn_amortizado := ln_facturas - ln_cancelado;
      end if;

      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
    END CANT_FACTURAS;


/***************************************************************************
 *     PROCEDURE COBRANZAS_EFECTIVO (pd_fecha_ini in date,                 *
 *                                  pd_fecha_fin in date)                  *
 *     Procesa los pagos diarios de tipo IN y CO                           *
 ***************************************************************************/
    PROCEDURE COBRANZAS_EFECTIVO (pd_fecha_ini in date,
                                  pd_fecha_fin in date) IS

    cursor c_region is
      select cost_code
      from costcenter;

    cursor c_pagos(ld_fecha date, lv_region varchar2, periodo date) is
      select /*+ rule +*/r.cost_code, o.ohentdate, sum(c.cacuramt_pay) total
      from   orderhdr_all o, cashreceipts_all c, cashdetail d, costcenter r
      where  o.customer_id = c.customer_id
      and    c.cacostcent  = r.cost_id
      and    c.caxact      = d.cadxact
      and    r.cost_code   = lv_region
      and    c.cachkdate   = ld_fecha
      and    o.ohentdate   = periodo
      and    o.ohstatus    = 'IN'
      group by r.cost_code, o.ohentdate;

    cursor c_acumulado (periodo date, reg varchar2)is
      select /*+ rule +*/max(fecha_factura) fecha, efectivo, porc_efectivo
      from   cobranzas_capas_tmp
      where  periodo_facturacion = periodo
      and    region              = reg
      and    fecha_factura       = (select max(fecha_factura)
                                    from   cobranzas_capas_tmp
                                    where  periodo_facturacion = periodo
                                    and    region              = reg)
      group by efectivo, porc_efectivo;

    cursor c_pagos_favor(ld_fecha date, lv_region varchar2) is
      select r.cost_code, sum(c.cacuramt_pay) total
      from   orderhdr_all o, cashreceipts_all c, cashdetail d, costcenter r
      where  c.cacostcent = r.cost_id
      and    c.caxact     = d.cadxact
      and    o.ohxact     = d.cadoxact
      and    r.cost_code  = lv_region
      and    c.cachkdate  = ld_fecha
      and    o.ohstatus   = 'CO'
      group by r.cost_code;

    cursor c_periodos is
      select distinct(t.ohentdate) facturacion
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN';

      lv_region          varchar2(3);
      lv_region1         varchar2(3);
      lv_error           varchar2(30);
      ld_periodo         date;
      ld_fecha           date;
      pd_fecha           date;
      ln_efectivo        number := 0;
      lb_notfound        boolean;
      ln_ind             number;
      ln_total           number := 0;
      ln_porc_efectivo   number := 0;
      ln_valor_ant       number := 0;
      ln_porc_ant        number := 0;
      mes                varchar2(2);
      dia                varchar2(2);
      anio               varchar2(4);
      lv_dia             varchar2(2);
      fecha_ini          date;
      fecha_fin          date;
      lc_pagos           c_pagos%rowtype;
      lbTienePago        boolean:=false;
      lbFirstTime        boolean:=true;

    BEGIN

      gv_funcion  := 'COBRANZAS_EFECTIVO';
      ln_efectivo := 0;

      pd_fecha    := pd_fecha_ini;

      while pd_fecha <= pd_fecha_fin loop

        for k in c_region loop

          ln_valor_ant := 0;

          for p in c_periodos loop

              lbTienePago := false;
              lbFirstTime := true;

              -- Si se encontraron pagos. Criterios => per�odo, fecha, regi�n
              for i in c_pagos(pd_fecha, k.cost_code, p.facturacion) loop
              begin
                -- se setea varible que indica que si existen pagos
                lbTienePago := true;
                -- solo se busca el total de la factura la primera vez que se ingresa al loop
                if lbFirstTime = true then
                   lbFirstTime := false;
                   ln_ind     := total_factura(p.facturacion, k.cost_code, ln_total, lv_error); -- Total facturado por el per�odo
                end if;

                lv_region  := i.cost_code;  -- GYE, UIO
                ld_periodo := i.ohentdate;
                lv_error   := null;

                -- Se consulta el valor acumulado por per�odo y regi�n
                open c_acumulado(ld_periodo, k.cost_code);
                fetch c_acumulado into ld_fecha, ln_valor_ant, ln_porc_ant;
                ld_fecha     := null;
                lb_notfound  := c_acumulado%notfound;
                close c_acumulado;

                -- Si no hay valores acumulados se repite el acumulado anterior
                if lb_notfound then
                  ln_efectivo := i.total;
                  lv_error    := null;
                  ln_ind      := porcentaje(ln_total, ln_efectivo, ln_porc_efectivo, lv_error);

                   if ln_ind = 1 then
                     begin
                       -- Se inserta con el acumulado anterior
                       insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo)
                                                values (lv_region, p.facturacion, pd_fecha, ln_efectivo, ln_porc_efectivo);
                       commit;
                     exception
                       when others then
                         gv_mensaje := sqlerrm;
                         insert into BITACORA_COBRANZAS_TMP values(p.facturacion, pd_fecha, gv_funcion, gv_mensaje);
                         commit;
                     end;
                   else
                     -- Error en la funci�n
                     gv_mensaje := lv_error;
                     raise ge_error;
                   end if;
                else
                  -- Se insertan los valores acumulados
                  ln_efectivo := i.total + ln_valor_ant;
                  lv_error    := null;
                  ln_ind      := porcentaje(ln_total, ln_efectivo, ln_porc_efectivo, lv_error);

                  if ln_ind = 1 then
                    begin
                      insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo)
                                               values (lv_region, p.facturacion, pd_fecha, ln_efectivo, ln_porc_efectivo);
                      commit;
                    exception
                      when others then
                        gv_mensaje := sqlerrm;
                        insert into BITACORA_COBRANZAS_TMP values(p.facturacion, pd_fecha, gv_funcion, gv_mensaje);
                        commit;
                    end;
                  else
                    -- Error en la funci�n
                    gv_mensaje := lv_error;
                    raise ge_error;
                  end if;
                end if;
              exception
                when ge_error then
                  insert into BITACORA_COBRANZAS_TMP values (p.facturacion, pd_fecha, gv_funcion, gv_mensaje);
                  commit;
                when others then
                  gv_mensaje := sqlerrm;
                  insert into BITACORA_COBRANZAS_TMP values (p.facturacion, pd_fecha, gv_funcion, gv_mensaje);
                  commit;
              end;
              end loop;  --fin del loop de pagos

              --------------------------------------
              -- se pregunta si no existieron pagos
              --------------------------------------
              if lbTienePago = false then
               lv_dia := substr(to_char(pd_fecha),1,2);
               -- Si es inicio de per�odo se inserta el primer registro con $0
               if to_number(lv_dia) = 24 then
                 insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo)
                                          values (k.cost_code, p.facturacion, pd_fecha,0,0);
                 commit;
               else
                 -- Si no es inicio de per�odo se consulta el valor acumualdo del per�odo y regi�n
                 open c_acumulado(p.facturacion, k.cost_code);
                 fetch c_acumulado into ld_fecha, ln_valor_ant, ln_porc_ant;
                 lb_notfound := c_acumulado%notfound;
                 close c_acumulado;
                 -- Si no hay valores acumulados se inserta el registro con $0
                 if lb_notfound then
                   insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo)
                                            values (k.cost_code, p.facturacion, pd_fecha,0,0);
                   commit;
                 else
                   -- Se inserta el registro con el valor acumualdo
                   insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo)
                                            values (k.cost_code, p.facturacion, pd_fecha, ln_valor_ant, ln_porc_ant);
                   commit;
                 end if;

               end if;
              end if;
            --end if;
          end loop;

          --cursor de pagos a favor
          for a in c_pagos_favor(pd_fecha,k.cost_code) loop

            dia  := substr(to_char(pd_fecha,'dd/MM/yyyy'),1,2);
            mes  := substr(to_char(pd_fecha,'dd/MM/yyyy'),4,2);
            anio := substr(to_char(pd_fecha,'dd/MM/yyyy'),7,4);

            -- Se calcula el per�odo efectivo de los pagos a favor
            if to_number(dia) >= 24 then
              if to_number(mes) = 12 then
                fecha_ini := to_date('24/'||mes||'/'||anio,'dd/mm/yyyy');
                fecha_fin := to_date('23/01'||'/'||to_char(to_number(anio)+1),'dd/mm/yyyy');
              else
                fecha_ini := to_date('24/'||mes||'/'||anio,'dd/mm/yyyy');
                fecha_fin := to_date('23/'||to_char(to_number(mes)+1)||'/'||anio,'dd/mm/yyyy');
              end if;
            else
              fecha_ini := to_date('24/'||to_char(to_number(mes)-1)||'/'||anio,'dd/mm/yyyy');
              fecha_fin := to_date('23/'||mes||'/'||anio,'dd/mm/yyyy');
            end if;

            ld_periodo := fecha_ini; -- Per�odo en el que se realiza el pago a favor

            -- Se verifica que existan los datos
            --begin
            update COBRANZAS_CAPAS_TMP
            set    pagos_co            = a.total
            where  fecha_factura       = pd_fecha
            and    region              = k.cost_code
            and    periodo_facturacion = ld_periodo;
            commit;
            --CBR: Jamas entrar�a por aqu�
            --exception
            --  when no_data_found then
            --    insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, pagos_co)
            --                             values (k.cost_code, ld_periodo, pd_fecha, 0);
            --    commit;
            --end;
          end loop;
        end loop;
        pd_fecha := pd_fecha +1;
      end loop;

      -- Ejecuta el procedimiento de Notas de Cr�ditos
      RPT_COBRANZA_CAPAS2.COBRANZAS_CREDITO(pd_fecha_ini, pd_fecha_fin);

    EXCEPTION
      when ge_error then
        insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
        commit;
      when others then
        gv_mensaje := sqlerrm;
        insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
        commit;


    END COBRANZAS_EFECTIVO;

/***************************************************************************
 *     PROCEDURE COBRANZAS_CREDITO (pd_fecha_ini in date,                  *
 *                                  pd_fecha_fin in date)                  *
 *     Procesa los pagos diarios por Notas de cr�dito => Tipo CM           *
 ***************************************************************************/
    PROCEDURE COBRANZAS_CREDITO (pd_fecha_ini in date,
                                 pd_fecha_fin in date) IS

    cursor c_region is
      select cost_code
      from costcenter;

    cursor c_pagos(ld_fecha date, lv_region varchar2, periodo date) is
      select /*+ rule +*/ r.cost_code, o.ohentdate, sum(o.ohopnamt_doc) total
      from   orderhdr_all o, cashreceipts_all c, cashdetail d, costcenter r
      where  o.customer_id   = c.customer_id
      and    c.cacostcent    = r.cost_id
      and    c.caxact        = d.cadxact
      and    r.cost_code     = lv_region
      and    c.cachkdate     = ld_fecha
      and    o.ohentdate     = periodo
      and    o.ohstatus      = 'CM'
      group by r.cost_code, o.ohentdate;

    cursor c_acumulado (periodo date, reg varchar2)is
      select /*+ rule +*/max(fecha_factura) fecha, creditos, porc_creditos
      from   cobranzas_capas_tmp
      where  periodo_facturacion = periodo
      and    region              = reg
      and    creditos            is not null
      and    fecha_factura       = (select max(fecha_factura)
                                    from   cobranzas_capas_tmp
                                    where  periodo_facturacion = periodo
                                    and    region              = reg
                                    and    creditos            is not null)
      group by creditos, porc_creditos;

    cursor c_periodos is
      select distinct(ohentdate) facturacion
      from orderhdr_all
      where ohentdate is not null
      and ohstatus = 'IN';

      lv_region          varchar2(3);
      lv_region1         varchar2(3);
      lv_error           varchar2(30);
      lv_dia             varchar2(2);
      ld_periodo         date;
      ld_fecha           date;
      pd_fecha           date;
      ln_creditos        number := 0;
      lb_notfound        boolean;
      ln_ind             number;
      ln_total           number := 0;
      ln_porc_credito    number := 0;
      ln_valor_ant       number := 0;
      ln_porc_ant        number := 0;
      lc_pagos           c_pagos%rowtype;
      lbTienePago        boolean:=false;
      --lbFirstTime        boolean:=true;

    BEGIN

    gv_funcion  := 'COBRANZAS_CREDITO';
    ln_creditos := 0;

    pd_fecha    := pd_fecha_ini;

    while pd_fecha <= pd_fecha_fin loop

      for k in c_region loop

        ln_valor_ant := 0;

        for p in c_periodos loop

            lbTienePago := false;

            for i in c_pagos(pd_fecha, k.cost_code, p.facturacion) loop
            begin

              -- se setea varible que indica que si existen pagos
              lbTienePago := true;

              lv_region  := i.cost_code;  -- GYE, UIO
              ld_periodo := i.ohentdate;
              lv_error   := null;
              ln_ind     := total_factura(ld_periodo, lv_region, ln_total, lv_error);

              open c_acumulado(ld_periodo, k.cost_code);
              fetch c_acumulado into ld_fecha, ln_valor_ant, ln_porc_ant;
              ld_fecha     := null;
              lb_notfound  := c_acumulado%notfound;
              close c_acumulado;

              if lb_notfound then
                ln_creditos := i.total;
                lv_error    := null;
                ln_ind      := porcentaje(ln_total, ln_creditos, ln_porc_credito, lv_error);

                if ln_ind = 1 then
                begin
                  update COBRANZAS_CAPAS_TMP
                  set    creditos            = ln_creditos,
                         porc_creditos       = ln_porc_credito
                  where  fecha_factura       = pd_fecha
                  and    periodo_facturacion = ld_periodo
                  and    region              = k.cost_code;
                  commit;

                exception
                  /* CBR: jamas entrar�a por aqu�
                  when no_data_found then
                    insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, creditos, porc_creditos)
                                             values (lv_region, ld_periodo, pd_fecha, ln_creditos, ln_porc_credito);
                    commit;*/
                  when others then
                    gv_mensaje := sqlerrm;
                    insert into BITACORA_COBRANZAS_TMP values(ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
                    commit;
                end;
                else
                  gv_mensaje := lv_error; -- Error en la funci�n
                  raise ge_error;
                end if;
              else
                ln_creditos := i.total + ln_valor_ant;
                lv_error    := null;
                ln_ind      := porcentaje(ln_total, ln_creditos, ln_porc_credito, lv_error);

                if ln_ind = 1 then
                  begin
                    update COBRANZAS_CAPAS_TMP
                    set    creditos            = ln_creditos,
                           porc_creditos       = ln_porc_credito
                    where  fecha_factura       = pd_fecha
                    and    periodo_facturacion = ld_periodo
                    and    region              = k.cost_code;
                    commit;

                  exception
                    /* CBR: jamas entrar�a por aqui
                    when no_data_found then
                      insert into COBRANZAS_CAPAS_TMP (region, periodo_facturacion, fecha_factura, creditos, porc_creditos)
                                              values (lv_region, ld_periodo, pd_fecha, ln_creditos, ln_porc_credito);
                      commit;*/
                    when others then
                      gv_mensaje := sqlerrm;
                      insert into BITACORA_COBRANZAS_TMP values(ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
                      commit;
                  end;
                else
                  gv_mensaje := lv_error; -- Error en la funci�n
                  raise ge_error;
                end if;
              end if;
            exception
              when ge_error then
                insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
                commit;
              when others then
                gv_mensaje := sqlerrm;
                insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
                commit;
            end;
            end loop;
          --end if;

            --------------------------------------
            -- se pregunta si no existieron CM
            --------------------------------------
            if lbTienePago = false then
              lv_dia := substr(to_char(pd_fecha),1,2);

              if to_number(lv_dia) = 24 then
                update COBRANZAS_CAPAS_TMP
                set   creditos            = 0,
                      porc_creditos       = 0
                where region              = k.cost_code
                and   fecha_factura       = pd_fecha
                and   periodo_facturacion = p.facturacion;
                commit;
              else
                open c_acumulado(p.facturacion, k.cost_code);
                fetch c_acumulado into ld_fecha, ln_valor_ant, ln_porc_ant;
                lb_notfound := c_acumulado%notfound;
                close c_acumulado;

                if lb_notfound then
                  update COBRANZAS_CAPAS_TMP
                  set   creditos            = 0,
                       porc_creditos        = 0
                  where region              = k.cost_code
                  and   fecha_factura       = pd_fecha
                  and   periodo_facturacion = p.facturacion;
                  commit;
                else
                  update COBRANZAS_CAPAS_TMP
                  set   creditos            = ln_valor_ant,
                       porc_creditos        = ln_porc_ant
                  where region              = k.cost_code
                  and   fecha_factura       = pd_fecha
                  and   periodo_facturacion = p.facturacion;
                  commit;
                end if;
              end if;  --fin de validacion del primer d�a
            end if;  --fin de validacion si tiene pago
        end loop;
      end loop;
      pd_fecha := pd_fecha +1;
    end loop;

    RPT_COBRANZA_CAPAS2.ACTUALIZA_TOTALES; -- Ejecuta el procedimiento para actualizar totales

    exception
      when ge_error then
        insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
        commit;
      when others then
        gv_mensaje := sqlerrm;
        insert into BITACORA_COBRANZAS_TMP values (ld_periodo, pd_fecha, gv_funcion, gv_mensaje);
        commit;

    END COBRANZAS_CREDITO;


/***************************************************************************
 *     PROCEDURE ACTUALIZA_TOTALES                                         *
 *                                                                         *
 *     Se actualizan los totales del reporte                               *
 ***************************************************************************/
    PROCEDURE ACTUALIZA_TOTALES IS

    cursor c_region is
      select cost_code
      from costcenter;

    cursor c_totales (reg varchar2, periodo date)is
      select /*+ rule +*/region, periodo_facturacion, fecha_factura, efectivo, porc_efectivo, creditos, porc_creditos
      from   cobranzas_capas_tmp
      where  region              = reg
      and    periodo_facturacion = periodo
      and    acumulado is null
      order by fecha_factura asc;

    cursor c_periodos is
      select distinct(ohentdate) facturacion
      from orderhdr_all
      where ohentdate is not null
      and ohstatus = 'IN';

      ln_porc_total   number;
      ln_diferencia   number;
      ln_acumulado    number;
      ln_ind          number;
      ln_monto        number;
      ln_facturas     number;
      ln_dias         number;
      lv_error        varchar2(30);

    BEGIN

      gv_funcion := 'ACTUALIZA_TOTALES';

      for j in c_region loop

        for p in c_periodos loop
          -- Calcula los totales por regi�n y per�odo
          ln_ind := TOTAL_FACTURA(p.facturacion, j.cost_code, ln_monto, lv_error);
          for i in c_totales (j.cost_code, p.facturacion) loop
          begin
            ln_acumulado  := i.efectivo + i.creditos;
            ln_porc_total := i.porc_efectivo + i.porc_creditos;
            lv_error      := null;
            ln_dias       := i.fecha_factura - p.facturacion + 1;

            if ln_ind = 1 then
              ln_diferencia := ln_monto - ln_acumulado;
              lv_error      := null;
              ln_ind        := CANT_FACTURAS(p.facturacion, j.cost_code, i.fecha_factura, ln_facturas, lv_error);

              if ln_ind = 1 then
              begin
                update COBRANZAS_CAPAS_TMP
                set    monto               = ln_diferencia,
                       porc_recuperado     = ln_porc_total,
                       acumulado           = ln_acumulado,
                       total_factura       = nvl(ln_facturas,0)
                where  region              = j.cost_code
                and    periodo_facturacion = p.facturacion
                and    fecha_factura       = i.fecha_factura;
                commit;
              exception
                when others then
                  gv_mensaje := sqlerrm;
                  insert into BITACORA_COBRANZAS_TMP values (p.facturacion, i.fecha_factura, gv_funcion, gv_mensaje);
                  commit;
              end;
              else
               gv_mensaje := lv_error; -- Error en la funci�n
               raise ge_error;
              end if;
            else
              gv_mensaje := lv_error; -- Error en la funci�n
              raise ge_error;
            end if;
            begin
              update COBRANZAS_CAPAS_TMP
              set    dias                = ln_dias
              where  periodo_facturacion = p.facturacion
              and    fecha_factura       = i.fecha_factura;
              commit;
            exception
              when others then
                gv_mensaje := sqlerrm;
                insert into BITACORA_COBRANZAS_TMP values (p.facturacion, i.fecha_factura, gv_funcion, gv_mensaje);
                commit;
            end;
          exception
            when ge_error then
              insert into BITACORA_COBRANZAS_TMP values (p.facturacion, i.fecha_factura, gv_funcion, gv_mensaje);
              commit;
            when others then
              gv_mensaje := sqlerrm;
              insert into BITACORA_COBRANZAS_TMP values (p.facturacion, i.fecha_factura, gv_funcion, gv_mensaje);
              commit;
          end;
          end loop;
        end loop;
      end loop;
    END ACTUALIZA_TOTALES;

end RPT_COBRANZA_CAPAS2;
/

