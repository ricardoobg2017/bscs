CREATE OR REPLACE package COK_PROCESS_REPORT_HIGH is
  
  FUNCTION COF_EXTRAE_CARGOS(pv_customer_id in number, pvFechIni in varchar2, pvFechFin in varchar2) RETURN NUMBER;
  PROCEDURE CO_EDAD_REAL(pv_error out varchar2);
  PROCEDURE CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                   pdFechaCorteMaximo        date,
                                   PV_ERROR                  IN OUT VARCHAR2);                                   
  PROCEDURE CUADRA;                                   
  PROCEDURE SETEA_PLAN_IDEAL(pd_lvMensErr out varchar2);
  PROCEDURE INSERTA_OPE_CUADRE(pdNombre_tabla varchar2);
  PROCEDURE ACTUALIZA_OPE_CUADRE(pdFechCierrePeriodo date);
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  Procedure Llena_DatosCliente(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2);
  
  PROCEDURE UPD_FPAGO_PROD_CCOSTO(pdFechCierrePeriodo date,
                                  pdNombre_tabla      varchar2,
                                  pd_lvMensErr        out varchar2);

  PROCEDURE CONV_CBILL_TIPOFPAGO(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  PROCEDURE Upd_Disponible_Totpago(pdFechCierrePeriodo  date,
                                   pdNombre_tabla       varchar2,
                                   pdTabla_Cashreceipts varchar2,
                                   pdTabla_OrderHdr     varchar2,
                                   pd_lvMensErr         out varchar2);

  PROCEDURE Llena_NroFactura(pdFechCierrePeriodo date,
                             pdNombre_tabla      varchar2);
  PROCEDURE Credito_a_Disponible(pdFechCierrePeriodo  date,
                                 pdNombre_tabla       varchar2,
                                 --pdTabla_Cashreceipts varchar2,
                                 --pdTabla_OrderHdr     varchar2,
                                 --pdTabla_Customer     varchar2,
                                 pd_lvMensErr         out varchar2);

  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);

  PROCEDURE CALCULA_MORA; --(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2);
  PROCEDURE ACTUALIZA_CMPER(pdFechCierrePeriodo in date,
                            pdNombre_tabla      in varchar2,
                            pNombre_orderhdr    in varchar2,
                            pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CONSMPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CREDTPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_PAGOSPER(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pdTabla_Cashreceipts in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_DESCUENTO(pdFechCierrePeriodo in date,
                                pdNombre_tabla      in varchar2,
                                pdMensErr           out varchar2);
  PROCEDURE LLENA_BALANCES(pdFechCierrePeriodo date,
                           pdNombre_tabla      varchar2,
                           pd_Tabla_OrderHdr   varchar2,
                           pd_lvMensErr        out varchar2);
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);

  PROCEDURE Copia_Fact_Actual(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO2(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2);
  PROCEDURE LLENA_CARGORECUPERADO2(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);                                    
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);
  PROCEDURE Llena_total_deuda_cierre(pdFechCierrePeriodo date,
                                     pdNombre_tabla      varchar2,
                                     pd_lvMensErr        out varchar2);

  PROCEDURE LLENATIPO(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  Procedure Llena_telefono(pdNombreTabla varchar2,
                           pd_lvMensErr  out varchar2);

  Procedure LLENA_BUROCREDITO(pdNombreTabla  varchar2,
                              pd_lvMensErr   out varchar2);  
                              
  PROCEDURE LLENA_FECH_MAX_PAGO(pdFechCierrePeriodo in date,
                                pdNombre_Tabla  in varchar2,
                                pd_lvMensErr   out varchar2); 
                                                                                    

  PROCEDURE PROCESA_CUADRE(pdFechaPeriodo       in date,
                           pdNombre_Tabla       in varchar2,
                           pdTabla_OrderHdr     in varchar2,
                           pdTabla_cashreceipts in varchar2,
                           pdTabla_Customer     in varchar2,
                           pd_lvMensErr         out varchar2);

  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2;

  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER;
                                                         
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2);

end COK_PROCESS_REPORT_HIGH;
/
CREATE OR REPLACE package body COK_PROCESS_REPORT_HIGH is

  -- Variables locales
  gv_funcion varchar2(30);
  gv_mensaje varchar2(500);
  ge_error exception;
  
  FUNCTION COF_EXTRAE_CARGOS(pv_customer_id in number, pvFechIni in varchar2, pvFechFin in varchar2) RETURN NUMBER IS
    lvSentencia varchar2(2000);
    lnCargo     number;
    
  BEGIN
  
    lnCargo := 0;

    SELECT SUM(f.amount) into lnCargo
    FROM fees f
    where  f.customer_id IN (select cu.customer_id 
                             from customer_all cu
                             START WITH cu.customer_id= pv_customer_id
                             CONNECT BY PRIOR cu.customer_id = cu.customer_id_high)
    and f.entdate BETWEEN to_date(pvFechIni|| ' 00:00', 'dd/mm/yyyy hh24:mi') 
                      AND to_date(pvFechFin|| ' 23:59', 'dd/mm/yyyy hh24:mi')
    and f.amount > 0
    and f.sncode <> 103;
    
    return lnCargo;

  EXCEPTION
      when others then
          return 0;
  END COF_EXTRAE_CARGOS;
  
  PROCEDURE CO_EDAD_REAL(pv_error out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lvFechaCorteAnualAnterior varchar2(15);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;
    
    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;
  
  BEGIN
  
    PV_ERROR := NULL;
    -- actualizo la mora en la mora_real
    update co_cuadre set mora_real = mora;
    commit;
    
    -- se actualiza la mora de los clientes con 330
    select valor into lvFechaCorteAnualAnterior from co_parametros_cliente where campo = 'FECHA_RECLASIFICACION';
    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.total_deuda+b.balance_12, b.mora, b.customer_id ' ||
                     ' from co_repcarcli_'||lvFechaCorteAnualAnterior||
                     ' a, co_cuadre b ' ||
                     ' where a.id_cliente = b.customer_id' ||
                     ' and b.total_deuda > 0' ||
                     ' and b.mora = 330';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);
      
        execute immediate 'update co_cuadre'||
                          ' set mora_real = :1' ||
                          ' where customer_id = :2'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente;
      
        lII := lII + 1;
        if lII = 1000 then
          lII := 0;
          commit;
        end if;
      
      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;
  
    dbms_sql.close_cursor(source_cursor);
  
    commit;
  
    pv_error := 'Proceso terminado con Exito';
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR CO_EDAD_REAL: ' || SQLERRM;
  END;  
  
  
  PROCEDURE CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                   pdFechaCorteMaximo        date,
                                   pv_error                  in out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;
    lnDiff         number;
    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;
  
  BEGIN
  
    PV_ERROR := NULL;
  
    BEGIN
      execute immediate 'create table co_repcarcli_' ||
                        to_char(pdFechaCorteMaximo, 'ddMMyyyy') ||
                        '_norecla as select * from co_repcarcli_' ||
                        to_char(pdFechaCorteMaximo, 'ddMMyyyy');
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al respaldar tabla ' || 'co_repcarcli_' ||
                    to_char(pdFechaCorteMaximo, 'ddMMyyyy');
        RETURN;
    END;
  
    select months_between(pdFechaCorteMaximo, pdFechaCorteAnualAnterior)
      into lnDiff
      from dual;
  
    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.totaladeuda, b.mayorvencido, b.id_cliente ' ||
                     ' from co_repcarcli_' ||
                     to_char(pdFechaCorteAnualAnterior, 'ddMMyyyy') ||
                     ' a, co_repcarcli_' ||
                     to_char(pdFechaCorteMaximo, 'ddMMyyyy') || ' b ' ||
                     ' where a.id_cliente = b.id_cliente' ||
                     ' and b.Totalvencida > 0' ||
                     ' and b.mayorvencido = ''330''';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);
      
        execute immediate 'update co_repcarcli_' ||
                          to_char(pdFechaCorteMaximo, 'ddMMyyyy') || '' ||
                          ' set mayorvencido = :1' ||
                          ' where id_cliente = :2'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente;
      
        lII := lII + 1;
        if lII = 1000 then
          lII := 0;
          commit;
        end if;
      
      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;
  
    dbms_sql.close_cursor(source_cursor);
  
    commit;
  
    pv_error := 'Proceso terminado con Exito.... Error al actualizar = ' ||
                Ln_ContadorErr;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR GENERAL: ' || SQLERRM;
  END;
  PROCEDURE CUADRA is
  BEGIN
       update co_cuadre 
       set saldoant = (balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12) +pagosper-credtper-cmper-consmper;
       commit;
  END;
  PROCEDURE SETEA_PLAN_IDEAL(pd_lvMensErr out varchar2) is
    lII number;
    cursor c_planes is
      select nvl(cu.customer_id_high, cu.customer_id) codigo
        from rateplan       rt,
             contract_all   co,
             curr_co_status cr,
             customer_all   cu
       where rt.tmcode = co.tmcode
         and co.co_id = cr.co_id
         and co.customer_id = cu.customer_id
         and cr.ch_status = 'a'
         and rt.tmcode >= 452
         and rt.tmcode <= 467
       group by cu.customer_id, cu.customer_id_high;
  BEGIN
    lII := 0;
    for p in c_planes loop
      execute immediate 'update co_cuadre' || ' set plan = ''IDEAL''' ||
                        ' where customer_id = :1'
        using p.codigo;
    
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'SETEA_PLAN_IDEAL: ' || sqlerrm;
  END;

  PROCEDURE INSERTA_OPE_CUADRE(pdNombre_tabla varchar2) is
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
  
  BEGIN
  
    -- se llena trunca la tabla de reportes 
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'truncate table ope_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade from ' ||
                   pdNombre_tabla;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
  END;
 
  PROCEDURE ACTUALIZA_OPE_CUADRE(pdFechCierrePeriodo date) is
  
    lnMesFinal     NUMBER;
    lvSentencia    VARCHAR2(1000);
    lvMensErr      VARCHAR2(3000);
    lII            NUMBER;
    source_cursor  INTEGER;
    rows_processed INTEGER;
    lnCustomerId   NUMBER;
  
  BEGIN
  
    -- se trunca la tabla destino
    lvSentencia := 'truncate table ope_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    -- se adjuntan datos de la tabla de cartera de detalle de clientes     
    lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre (custcode, customer_id, producto, canton, provincia, total_deuda,  mora, region, tipo, total_deuda_cierre)' ||
                   ' select cuenta, id_cliente, producto, canton, provincia, totaladeuda, decode(mayorvencido, ''V'', 0, ''''''-V'', 0, to_number(mayorvencido)), decode(compania, 1, ''Guayaquil'', ''Quito''), 0, balance_' ||
                   lnMesFinal || '  from co_repcarcli_' ||
                   to_char(pdFechCierrePeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
    -- se actualiza el tipo
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id from ope_cuadre' || ' minus' ||
                     ' select customer_id from orderhdr_all where ohstatus = ''IN'' and  ohentdate = to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
    
      -- actualizo los campos de la tabla final
      update ope_cuadre set tipo = '1_NF' where customer_id = lnCustomerId;
    
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
    -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
    lnMesFinal := to_number(to_char(pdFechCierrePeriodo, 'MM'));
    update ope_cuadre set tipo = '3_VNE' where total_deuda_cierre < 0;
    commit;
  
    -- Se actualiza campo tipo para saber que es Mora
    update ope_cuadre set tipo = '5_R' where mora > 0;
    commit;
  
  END;

  ---------------------------------------------------------------------
  --  INSERTADATOS_CUSTOMER
  --  Extraer los codigos de las cuentas de la tabla customer_all
  --  para tener la referencia inicial de datos de clientes a procesar
  ---------------------------------------------------------------------
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(500);
    lvMensErr   varchar2(1000);
    ciclo       varchar2(2);
  
  BEGIN
  
    lvSentencia := 'insert into ' || pdNombre_tabla ||
                   ' NOLOGGING (custcode,customer_id) ' ||
                   ' select a.custcode, a.customer_id' ||
                   ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma' ||
                   ' where a.billcycle = ma.id_ciclo_admin' ||
                   ' and ci.id_ciclo = ma.id_ciclo' ||
                   ' and a.paymntresp  = ''X''' ||
                   ' and ci.dia_ini_ciclo = ''' ||
                   to_char(pdFechCierrePeriodo, 'dd') || '''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
    pd_lvMensErr := lvMensErr;
  
  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'InsertaDatosCustomer: ' || sqlerrm;
  END;

  ---------------------------------------------------------------------
  -- Llena_DatosCliente
  -- agrega informaci�n general del cliente de la vista DATOSCLIENTES
  ---------------------------------------------------------------------
  Procedure Llena_DatosCliente(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2) is
  
    lnCustomerId       number;
    CURSOR DATOSPERS IS
      select /*+ rule */
       d.customer_id,
       f.ccfname,
       f.cclname,
       f.cssocialsecno,
       nvl(f.cccity, 'x') cccity,
       nvl(f.ccstate, 'x') ccstate,
       f.ccname,
       f.cctn,
       f.cctn2,
       f.ccline3 || f.ccline4 dir2,
       h.tradename
        from customer_all d, -- maestro de cliente
             ccontact_all f, -- informaci�n demografica de la cuente
             payment_all  g, --Forna de pago
             COSTCENTER   j,
             trade_all    h
       where d.customer_id = f.customer_id
         and f.ccbill = 'X'
         and d.customer_id = g.customer_id
         and d.costcenter_id = j.cost_id
         and d.cstradecode = h.tradecode(+);
  
    lv_sentencia_upd varchar2(2000);
    lvMensErr        varchar2(2000);
    lII              number;
  
  BEGIN
  
    lII := 0;
    FOR b IN DATOSPERS LOOP
      lnCustomerId:=b.customer_id;
      execute immediate 'update ' || pdnombretabla || ' set' ||
                        ' nombres = :1,' || ' apellidos = :2,' ||
                        ' ruc = :3,' || ' canton = :4,' ||
                        ' provincia = :5,' || ' direccion = :6,' ||
                        ' direccion2 = :7,' || ' cont1 = :8,' ||
                        ' cont2 = :9,' || ' trade = :10' ||
                        ' where customer_id = :11'
        using b.ccfname, b.cclname, b.cssocialsecno, b.cccity, b.ccstate, b.ccname, b.dir2, b.cctn, b.cctn2, b.tradename, b.customer_id;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    END LOOP;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'llena_datoscliente: ' ||lnCustomerId||' ERROR:'|| sqlerrm;
    
  END;

  -----------------------------------------------------
  --     UPD_FPAGO_PROD_CCOSTO
  --     Se llenan algunos datos generales del cliente
  --     como la forma de pago y tarjeta de credito
  -----------------------------------------------------
  PROCEDURE UPD_FPAGO_PROD_CCOSTO(pdFechCierrePeriodo date,
                                  pdNombre_tabla      varchar2,
                                  pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
  
    CURSOR Registros IS
      select /*+ rule */
       d.customer_id,
       k.bank_id,
       k.bankname,
       nvl(i.producto, 'x') producto,
       nvl(j.cost_desc, 'x') cost_desc,
       g.bankaccno,
       g.valid_thru_date,
       g.accountowner,
       d.csactivated,
       d.cstradecode
        from customer_all    d,
             payment_all     g,
             read.COB_GRUPOS I,
             COSTCENTER      j,
             bank_all        k
       where d.customer_id = g.customer_id
         and act_used = 'X'
         and D.PRGCODE = I.PRGCODE
         and j.cost_id = d.costcenter_id
         and g.bank_id = k.bank_id;
  
  BEGIN
  
    lII := 0;
    -- se actualizan la forma de pago, el producto y centro de costo
    FOR b IN Registros LOOP
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set forma_pago = :1,' || ' des_forma_pago = :2,' ||
                        ' producto = :3,' || ' tarjeta_cuenta = :4,' ||
                        ' fech_expir_tarjeta = :5,' || ' tipo_cuenta = :6,' ||
                        ' fech_aper_cuenta = :7,' || ' grupo = :8,' ||
                        ' region = :9' || ' where customer_id = :10'
        using b.bank_id, b.bankname, b.producto, b.bankaccno, b.valid_thru_date, b.accountowner, b.csactivated, b.cstradecode, b.cost_desc, b.customer_id;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    END LOOP;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'upd_fpago_prod_ccosto: ' || sqlerrm;
    
  End;

  ---------------------------------------------------------
  --     CONV_CBILL_TIPOFPAGO
  --     Coloca la forma de pago que se utilizaba en CBILL
  ---------------------------------------------------------
  PROCEDURE CONV_CBILL_TIPOFPAGO(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
  
    CURSOR RPT_PAYMENT IS
      select rpt_cod_bscs, rpt_payment_id, rpt_nombrefp
        from ope_rpt_payment_type;
  
  BEGIN
  
    -- se actualizan los valores para tipo de forma de pago
    FOR b in RPT_PAYMENT LOOP
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set tipo_forma_pago = :1,' ||
                        ' des_tipo_forma_pago = :2' ||
                        ' where forma_pago = :3'
        using b.rpt_payment_id, b.rpt_nombrefp, b.rpt_cod_bscs;
    END LOOP;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'conv_cbill_tipofpago: ' || sqlerrm;
  END;

  ------------------------------------------------------------------------
  --     UPD_DISPONIBLE_TOTPAGO
  --     Procediento que llena los siguientes campos
  --     totpagos: contiene el total de pagos realizados hasta el corte
  --     creditos: total de creditos realizados hasta el corte
  --     disponible: pagos + creditos utiles para el balanceo
  ------------------------------------------------------------------------
  PROCEDURE Upd_Disponible_Totpago(pdFechCierrePeriodo  date,
                                   pdNombre_tabla       varchar2,
                                   pdTabla_Cashreceipts varchar2,
                                   pdTabla_OrderHdr     varchar2,
                                   pd_lvMensErr         out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnTotPago      number;
    lnTotCred      number;
    lvPeriodos     varchar2(2000);
  
    cursor c_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01'; --invoices since July
  
  BEGIN
  
    ---------------------------
    --        PAGOS
    ---------------------------
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select /*+ rule */ customer_id, cachkamt_pay as TotPago' ||
                     ' from  ' || pdTabla_Cashreceipts ||
                     ' where  caentdate  < to_date(''' ||to_char(pdFechCierrePeriodo, 'yyyy/MM/dd')||''',''yyyy/MM/dd'')' ||
                     ' and cachkamt_pay <> 0';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnTotPago);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnTotPago);
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set totpagos = totpagos + :1,' ||
                        ' disponible = disponible + :2' ||
                        ' where customer_id = :3'
        using lnTotPago, lnTotPago, lnCustomerId;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
    ---------------------------
    --        CREDITOS
    ---------------------------
    lvPeriodos := '';
    for i in c_periodos loop
      lvPeriodos := lvPeriodos || ' to_date(''' ||
                    to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                    ''',''dd/MM/yyyy''),';
    end loop;
    lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select /*+ rule */ customer_id,sum(ohinvamt_doc) as Totcred' ||
                     ' from  ' || pdTabla_OrderHdr ||
                     ' where ohstatus  =''CM''' ||
                     ' and not ohentdate in (' || lvPeriodos || ')' ||
                     ' group by customer_id' ||
                     ' having sum(ohinvamt_doc)<>0';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnTotCred);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnTotCred);
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set creditos = :1 * -1,' ||
                        ' disponible = disponible + :2 * -1' ||
                        ' where customer_id = :3'
        using lnTotCred, lnTotCred, lnCustomerId;
    
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'upd_disponible_totpago: ' || sqlerrm;
    
  END;

  ------------------------------------------------------------
  --     Llena_NroFactura
  --     Setea el numero de la factura de la tabla de facturas
  ------------------------------------------------------------
  PROCEDURE Llena_NroFactura(pdFechCierrePeriodo date,
                             pdNombre_tabla      varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
  
    CURSOR FACTURA IS
      select customer_id, OHREFNUM
        from orderhdr_all
       where ohstatus = 'IN'
         and ohentdate = pdFechCierrePeriodo;
  
  Begin
  
    lII := 0;
    FOR b IN FACTURA LOOP
      execute immediate 'update ' || pdNombre_Tabla || ' set FACTURA = :1' ||
                        ' where customer_id = :2'
        using b.ohrefnum, b.customer_id;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    END LOOP;
    commit;
  
  End;

  ---------------------------------------------------------------
  --    Adiciona_credito_a_IN 
  --    Procedimiento que coloca los creditos como parte de
  --    la factura para efectos de cuadratura ya que BSCS
  --    no lo estaba haciendo
  ---------------------------------------------------------------
  PROCEDURE Credito_a_Disponible(pdFechCierrePeriodo  date,
                                 pdNombre_tabla       varchar2,
                                 --pdTabla_Cashreceipts varchar2,
                                 --pdTabla_OrderHdr     varchar2,
                                 --pdTabla_Customer     varchar2,
                                 pd_lvMensErr         out varchar2) is
  
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    lnMes          number;
    lII            number;
    lnCustomerId   number;
    lnValor        number;
  
    cursor cur_creditos_cliente is
      select a.customer_id, sum(a.credito) credito
      from co_creditos_cliente a, co_cuadre b
      where a.customer_id = b.customer_id
      group by a.customer_id;
  
  BEGIN

       ------------------------------------------
       -- se actualiza la informacion de creditos
       ------------------------------------------
       delete from co_creditos_cliente where fecha = pdFechCierrePeriodo;
       commit;
        source_cursor := DBMS_SQL.open_cursor;
        lvSentencia   := 'SELECT a.customer_id, sum(a.valor)' ||
                         ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                         ' WHERE a.tipo = ''006 - CREDITOS''' ||
                         ' GROUP BY a.customer_id';
        dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        dbms_sql.define_column(source_cursor, 1, lnCustomerId);
        dbms_sql.define_column(source_cursor, 2, lnValor);
        rows_processed := Dbms_sql.execute(source_cursor);
      
        lII := 0;
        loop
          if dbms_sql.fetch_rows(source_cursor) = 0 then
            exit;
          end if;
          dbms_sql.column_value(source_cursor, 1, lnCustomerId);
          dbms_sql.column_value(source_cursor, 2, lnValor);
          execute immediate 'insert into co_creditos_cliente'||
                              ' values (:1, :2, :3)'
          using lnCustomerId, pdFechCierrePeriodo, lnValor;
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
        end loop;
        commit;
        dbms_sql.close_cursor(source_cursor);

       ------------------------------------------
       -- se actualiza la informacion para balanc
       ------------------------------------------
       for i in cur_creditos_cliente loop
           -- actualizar creditos y disponible
           execute immediate 'update co_cuadre set '||
                          'creditos = creditos + :1  * -1,'||
                          'disponible = disponible + :2  * -1'||
                          'where customer_id=:3'
           using i.credito, i.credito, i.customer_id;
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
       end loop;
       commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'credito_a_disponible: ' || sqlerrm;
    
  END;

  -------------------------------------------------------
  --     EJECUTA_SENTENCIA
  --     Funci�n general que ejecuta sentencias dinamicas
  -------------------------------------------------------
  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2) is
    x_cursor integer;
    z_cursor integer;
    name_already_used exception;
    pragma exception_init(name_already_used, -955);
  begin
    x_cursor := dbms_sql.open_cursor;
    DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
    z_cursor := DBMS_SQL.EXECUTE(x_cursor);
    DBMS_SQL.CLOSE_CURSOR(x_cursor);
  EXCEPTION
    WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
  END EJECUTA_SENTENCIA;

  -------------------------------
  --  CALCULA MORA
  --  Calcula las edades de mora
  -------------------------------

  PROCEDURE CALCULA_MORA is
  
    --variables
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
    lnDiff         number;
    lnRango        number;
    lnI            number;
    lnJ            number;
    source_cursor  integer;
    rows_processed integer;
    lvDia          varchar2(5);
  
    lnCustomerId number;
    lnBalance1   number;
    ldFecha1     date;
    lnBalance2   number;
    ldFecha2     date;
    lnBalance3   number;
    ldFecha3     date;
    lnBalance4   number;
    ldFecha4     date;
    lnBalance5   number;
    ldFecha5     date;
    lnBalance6   number;
    ldFecha6     date;
    lnBalance7   number;
    ldFecha7     date;
    lnBalance8   number;
    ldFecha8     date;
    lnBalance9   number;
    ldFecha9     date;
    lnBalance10  number;
    ldFecha10    date;
    lnBalance11  number;
    ldFecha11    date;
    lnBalance12  number;
    ldFecha12    date;
    
    cursor cu_edades_migradas is 
    select customer_id from co_cuadre
    where balance_1 = 0 and balance_2 = 0 and balance_3 = 0 and balance_4 = 0 and balance_5 = 0 and balance_6 = 0 and balance_7 = 0 and balance_8 = 0 and balance_9 = 0 and balance_10 = 0 and balance_11 > 0 and balance_12 > 0;
  
  Begin
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, balance_1, fecha_1, balance_2, fecha_2, balance_3, fecha_3, balance_4, fecha_4, balance_5, fecha_5, balance_6, fecha_6, balance_7, fecha_7, balance_8, fecha_8, balance_9, fecha_9, balance_10, fecha_10, balance_11, fecha_11, balance_12, fecha_12' ||
                     ' from co_cuadre';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnBalance1);
    dbms_sql.define_column(source_cursor, 3, ldFecha1);
    dbms_sql.define_column(source_cursor, 4, lnBalance2);
    dbms_sql.define_column(source_cursor, 5, ldFecha2);
    dbms_sql.define_column(source_cursor, 6, lnBalance3);
    dbms_sql.define_column(source_cursor, 7, ldFecha3);
    dbms_sql.define_column(source_cursor, 8, lnBalance4);
    dbms_sql.define_column(source_cursor, 9, ldFecha4);
    dbms_sql.define_column(source_cursor, 10, lnBalance5);
    dbms_sql.define_column(source_cursor, 11, ldFecha5);
    dbms_sql.define_column(source_cursor, 12, lnBalance6);
    dbms_sql.define_column(source_cursor, 13, ldFecha6);
    dbms_sql.define_column(source_cursor, 14, lnBalance7);
    dbms_sql.define_column(source_cursor, 15, ldFecha7);
    dbms_sql.define_column(source_cursor, 16, lnBalance8);
    dbms_sql.define_column(source_cursor, 17, ldFecha8);
    dbms_sql.define_column(source_cursor, 18, lnBalance9);
    dbms_sql.define_column(source_cursor, 19, ldFecha9);
    dbms_sql.define_column(source_cursor, 20, lnBalance10);
    dbms_sql.define_column(source_cursor, 21, ldFecha10);
    dbms_sql.define_column(source_cursor, 22, lnBalance11);
    dbms_sql.define_column(source_cursor, 23, ldFecha11);
    dbms_sql.define_column(source_cursor, 24, lnBalance12);
    dbms_sql.define_column(source_cursor, 25, ldFecha12);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lnI := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnBalance1);
      dbms_sql.column_value(source_cursor, 3, ldFecha1);
      dbms_sql.column_value(source_cursor, 4, lnBalance2);
      dbms_sql.column_value(source_cursor, 5, ldFecha2);
      dbms_sql.column_value(source_cursor, 6, lnBalance3);
      dbms_sql.column_value(source_cursor, 7, ldFecha3);
      dbms_sql.column_value(source_cursor, 8, lnBalance4);
      dbms_sql.column_value(source_cursor, 9, ldFecha4);
      dbms_sql.column_value(source_cursor, 10, lnBalance5);
      dbms_sql.column_value(source_cursor, 11, ldFecha5);
      dbms_sql.column_value(source_cursor, 12, lnBalance6);
      dbms_sql.column_value(source_cursor, 13, ldFecha6);
      dbms_sql.column_value(source_cursor, 14, lnBalance7);
      dbms_sql.column_value(source_cursor, 15, ldFecha7);
      dbms_sql.column_value(source_cursor, 16, lnBalance8);
      dbms_sql.column_value(source_cursor, 17, ldFecha8);
      dbms_sql.column_value(source_cursor, 18, lnBalance9);
      dbms_sql.column_value(source_cursor, 19, ldFecha9);
      dbms_sql.column_value(source_cursor, 20, lnBalance10);
      dbms_sql.column_value(source_cursor, 21, ldFecha10);
      dbms_sql.column_value(source_cursor, 22, lnBalance11);
      dbms_sql.column_value(source_cursor, 23, ldFecha11);
      dbms_sql.column_value(source_cursor, 24, lnBalance12);
      dbms_sql.column_value(source_cursor, 25, ldFecha12);
    
      for lnJ in 1 .. 12 loop
        if lnJ = 1 and lnBalance1 > 0 then
          select months_between(ldFecha12, ldFecha1) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 2 and lnBalance2 > 0 then
          select months_between(ldFecha12, ldFecha2) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 3 and lnBalance3 > 0 then
          select months_between(ldFecha12, ldFecha3) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 4 and lnBalance4 > 0 then
          select months_between(ldFecha12, ldFecha4) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 5 and lnBalance5 > 0 then
          select months_between(ldFecha12, ldFecha5) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 6 and lnBalance6 > 0 then
          select months_between(ldFecha12, ldFecha6) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 7 and lnBalance7 > 0 then
          select months_between(ldFecha12, ldFecha7) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 8 and lnBalance8 > 0 then
          select months_between(ldFecha12, ldFecha8) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 9 and lnBalance9 > 0 then
          select months_between(ldFecha12, ldFecha9) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 10 and lnBalance10 > 0 then
          select months_between(ldFecha12, ldFecha10) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 11 and lnBalance11 > 0 then
          select months_between(ldFecha12, ldFecha11) * 30
            into lnDiff
            from dual;
          exit;
        else
          lnDiff := 0;
        end if;
      end loop;
    
      if lnDiff < 0 then
        lnRango := 0;
      elsif lnDiff = 0 then
        lnRango := 0;
      elsif lnDiff >= 1 and lnDiff <= 30 then
        lnRango := 30;
      elsif lnDiff >= 31 and lnDiff <= 60 then
        lnRango := 60;
      elsif lnDiff >= 61 and lnDiff <= 90 then
        lnRango := 90;
      elsif lnDiff >= 91 and lnDiff <= 120 then
        lnRango := 120;
      elsif lnDiff >= 121 and lnDiff <= 150 then
        lnRango := 150;
      elsif lnDiff >= 151 and lnDiff <= 180 then
        lnRango := 180;
      elsif lnDiff >= 181 and lnDiff <= 210 then
        lnRango := 210;
      elsif lnDiff >= 211 and lnDiff <= 240 then
        lnRango := 240;
      elsif lnDiff >= 241 and lnDiff <= 270 then
        lnRango := 270;
      elsif lnDiff >= 271 and lnDiff <= 300 then
        lnRango := 300;
      elsif lnDiff >= 301 and lnDiff <= 330 then
        lnRango := 330;
      else
        lnRango := 330;
      end if;
    
      execute immediate 'update co_cuadre' || ' set mora = :1' ||
                        ' where customer_id = :2'
        using lnRango, lnCustomerId;
    
      lnI := lnI + 1;
      if lnI = 500 then
        lnI := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
    
    -- se actualiza el campo mora_real_mig antes de cambiarlo
    update co_cuadre set mora_real_mig = mora;
    commit;
    
    -- se actualizan aquellas que hayan migrado
    select to_char(fecha_12, 'dd') into lvDia from co_cuadre where rownum = 1;
    if lvDia = '08' then
        lnI := 0;
        for i in cu_edades_migradas loop
             update co_cuadre set mora = 0 where customer_id = i.customer_id;
             lnI := lnI + 1;
             if lnI = 500 then
                 lnI := 0;
                 commit;
             end if;        
        end loop
        commit;
    end if;
    
    
  
  END CALCULA_MORA;

  -----------------------
  --  BALANCEO
  ----------------------

  PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2) is
    -- variables 
    val_fac_         varchar2(20);
    lv_sentencia     varchar2(2000);
    lv_sentencia_upd varchar2(2000);
    v_sentencia      varchar2(2000);
    lv_campos        varchar2(2000);
    mes              varchar2(2);
    nombre_campo     varchar2(20);
    lvMensErr        varchar2(2000) := null;
  
    wc_rowid       varchar2(100);
    wc_customer_id number;
    wc_disponible  number;
    val_fac_1      number;
    val_fac_2      number;
    val_fac_3      number;
    val_fac_4      number;
    val_fac_5      number;
    val_fac_6      number;
    val_fac_7      number;
    val_fac_8      number;
    val_fac_9      number;
    val_fac_10     number;
    val_fac_11     number;
    val_fac_12     number;
    --
    nro_mes             number;
    contador_mes        number;
    contador_campo      number;
    v_CursorId          number;
    v_cursor_asigna     number;
    v_Dummy             number;
    v_Row_Update        number;
    aux_val_fact        number;
    total_deuda_cliente number;
    leError exception;
  BEGIN
    -- CBR: 27 Enero 2004
    -- Desde el 2004 se trabajaran siempre con las 12 columnas
    mes := 12;
    nro_mes := 12;

    if mes = '01' then
      lv_campos := 'balance_1';
    elsif mes = '02' then
      lv_campos := 'balance_1, balance_2';
    elsif mes = '03' then
      lv_campos := 'balance_1, balance_2, balance_3';
    elsif mes = '04' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4';
    elsif mes = '05' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
    elsif mes = '06' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
    elsif mes = '07' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
    elsif mes = '08' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
    elsif mes = '09' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
    elsif mes = '10' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
    elsif mes = '11' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
    elsif mes = '12' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
    end if;
  
    v_cursorId := 0;
    v_CursorId := DBMS_SQL.open_cursor;
  
    --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
    lv_sentencia := ' select c.rowid, customer_id, disponible, ' ||
                    lv_campos || ' from ' || pdNombre_tabla || ' c';
    --' where customer_id in (6310,14180,22404,42182,49171,77622,109222,115226,115227,120152,121491,121492,126562,126563,129546,129548,139370,141398,141399,142499,142500,143186,143187,149766,149767,164455,164456,164684,164685,164719,164720,173515,173516,173517,173518,182978,182979,185538,185539,187436,187437)';
    Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
    contador_campo := 0;
    contador_mes   := 1;
    dbms_sql.define_column(v_cursorId, 1, wc_rowid, 30);
    dbms_sql.define_column(v_cursorId, 2, wc_customer_id);
    dbms_sql.define_column(v_cursorId, 3, wc_disponible);
  
    -- define variables de salida dinamica
    if nro_mes >= 1 then
      dbms_sql.define_column(v_cursorId, 4, val_fac_1);
    end if;
    if nro_mes >= 2 then
      dbms_sql.define_column(v_cursorId, 5, val_fac_2);
    end if;
    if nro_mes >= 3 then
      dbms_sql.define_column(v_cursorId, 6, val_fac_3);
    end if;
    if nro_mes >= 4 then
      dbms_sql.define_column(v_cursorId, 7, val_fac_4);
    end if;
    if nro_mes >= 5 then
      dbms_sql.define_column(v_cursorId, 8, val_fac_5);
    end if;
    if nro_mes >= 6 then
      dbms_sql.define_column(v_cursorId, 9, val_fac_6);
    end if;
    if nro_mes >= 7 then
      dbms_sql.define_column(v_cursorId, 10, val_fac_7);
    end if;
    if nro_mes >= 8 then
      dbms_sql.define_column(v_cursorId, 11, val_fac_8);
    end if;
    if nro_mes >= 9 then
      dbms_sql.define_column(v_cursorId, 12, val_fac_9);
    end if;
    if nro_mes >= 10 then
      dbms_sql.define_column(v_cursorId, 13, val_fac_10);
    end if;
    if nro_mes >= 11 then
      dbms_sql.define_column(v_cursorId, 14, val_fac_11);
    end if;
    if nro_mes = 12 then
      dbms_sql.define_column(v_cursorId, 15, val_fac_12);
    end if;
    v_Dummy := Dbms_sql.execute(v_cursorId);
  
    Loop
      total_deuda_cliente := 0;
      lv_sentencia_upd    := '';
      lv_sentencia_upd    := 'update ' || pdNombre_tabla || ' set ';
    
      if dbms_sql.fetch_rows(v_cursorId) = 0 then
        exit;
      end if;
      --   
      -- recupero valores en los campos  y disminuyo valores de factura segun monto disponible
      --
      dbms_sql.column_value(v_cursorId, 1, wc_rowid);
      dbms_sql.column_value(v_cursorId, 2, wc_customer_id);
      dbms_sql.column_value(v_cursorId, 3, wc_disponible);
    
      if nro_mes >= 1 then
        dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_1, 0);
        if wc_disponible >= aux_val_fact then
          --  900        >   100
          val_fac_1     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
          --
        else
          --  900 dispo       <  1000 ene
          val_fac_1     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 1 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_1 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = ' || val_fac_1 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 2 then
        dbms_sql.column_value(v_cursorId, 5, val_fac_2); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_2, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_2     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_2     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 2 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_2 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = ' || val_fac_2 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 3 then
        dbms_sql.column_value(v_cursorId, 6, val_fac_3); -- recupero el valor en la variable 
        aux_val_fact := nvl(val_fac_3, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_3     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_3     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 3 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_3 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = ' || val_fac_3 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 4 then
        dbms_sql.column_value(v_cursorId, 7, val_fac_4); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_4, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_4     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_4     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 4 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_4 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = ' || val_fac_4 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 5 then
        dbms_sql.column_value(v_cursorId, 8, val_fac_5); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_5, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_5     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_5     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 5 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_5 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = ' || val_fac_5 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      if nro_mes >= 6 then
        dbms_sql.column_value(v_cursorId, 9, val_fac_6); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_6, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_6     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_6     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 6 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_6 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = ' || val_fac_6 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 7 then
        dbms_sql.column_value(v_cursorId, 10, val_fac_7); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_7, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_7     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_7     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 7 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_7 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = ' || val_fac_7 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 8 then
        dbms_sql.column_value(v_cursorId, 11, val_fac_8); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_8, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_8     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_8     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 8 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_8 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = ' || val_fac_8 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 9 then
        dbms_sql.column_value(v_cursorId, 12, val_fac_9); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_9, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_9     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_9     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 9 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_9 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = ' || val_fac_9 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 10 then
        dbms_sql.column_value(v_cursorId, 13, val_fac_10); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_10, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_10    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_10    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 10 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_10 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = ' ||
                            val_fac_10 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 11 then
        dbms_sql.column_value(v_cursorId, 14, val_fac_11); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_11, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_11    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_11    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 11 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_11 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = ' ||
                            val_fac_11 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes = 12 then
        dbms_sql.column_value(v_cursorId, 15, val_fac_12); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_12, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_12    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_12    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 12 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_12 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = ' ||
                            val_fac_12 || ' , '; -- Armando sentencia para UPDATE
      end if;
      -- Quito la coma y finalizo la sentencia
      lv_sentencia_upd := substr(lv_sentencia_upd,
                                 1,
                                 length(lv_sentencia_upd) - 2);
      lv_sentencia_upd := lv_sentencia_upd || ' where customer_id = ' ||
                          wc_customer_id;
      --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';
    
      EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;
    
    End Loop; -- 1. Para extraer los datos del cursor
    commit;
  
    dbms_sql.close_cursor(v_CursorId);
  
  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := 'balanceo: ' || lvMensErr;
    
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(v_CursorId) THEN
        DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      END IF;
      pd_lvMensErr := 'balanceo: ' || sqlerrm;
    
  END BALANCEO;

  -----------------------------------------------------
  --    ACTUALIZA_CMPER
  --    Setea los valores por cm del periodo actual
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CMPER(pdFechCierrePeriodo in date,
                            pdNombre_tabla      in varchar2,
                            pNombre_orderhdr    in varchar2,
                            pdMensErr           out varchar2) is
  
    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
  
  BEGIN
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, nvl(sum(ohinvamt_doc),0) valor' ||
                     ' from  ' || pNombre_orderhdr ||
                     ' where ohstatus  =''CM''' ||
                     ' and ohentdate  > to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')' ||
                     ' and ohentdate  < to_date(''' ||
                     to_char(pdFechCierrePeriodo, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' || ' group  by customer_id';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set cmper = cmper + :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_CMPER;

  -----------------------------------------------------
  --    ACTUALIZA_CONSMPER
  --    Setea los valores facturados del perido
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CONSMPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2) is
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
  
  BEGIN
    source_cursor := DBMS_SQL.open_cursor;
  
    lvSentencia := 'SELECT a.customer_id, nvl(sum(a.valor),0), nvl(sum(a.descuento),0)' ||
                   ' FROM co_fact_' ||
                   to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                   pdNombre_tabla || ' b' ||
                   ' WHERE a.customer_id = b.customer_id and' ||
                   ' a.tipo != ''006 - CREDITOS''' ||
                   ' GROUP BY a.customer_id';
  
    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    dbms_sql.define_column(source_cursor, 3, lnDescuento);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);
    
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set consmper = consmper + :1 - :2' ||
                        ' where customer_id = :3'
        using lnValor, lnDescuento, lnCustomerId;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
  
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_CONSMPER;

  -----------------------------------------------------
  --    ACTUALIZA_CREDTPER
  --    Setea los valores de creditos del perido
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CREDTPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2) is
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnValor        number;
    lnCustomerId   number;
  
  BEGIN
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'SELECT a.customer_id, nvl(sum(a.valor),0)' ||
                     ' FROM co_fact_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                     pdNombre_tabla || ' b' ||
                     ' WHERE a.customer_id = b.customer_id and' ||
                     ' a.tipo = ''006 - CREDITOS''' ||
                     ' GROUP BY a.customer_id, a.cost_desc';
  
    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set credtper = credtper + :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
    
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
  
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_CREDTPER;

  -----------------------------------------------------
  --    ACTUALIZA_PAGOSPER
  --    Setea los valores de pagos del periodo
  -----------------------------------------------------    
  PROCEDURE ACTUALIZA_PAGOSPER(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pdTabla_Cashreceipts in varchar2,
                               pdMensErr            out varchar2) is
  
    lvMaxPagoAgo    number;
    lvMaxPagoSep    number;
    lII             number;
    lvSentencia     varchar2(2000);
    lvMensErr       varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    lnCustomerId    number;
    lnValor         number;
    --pdFechaFinal    date;
    pdFechaAnterior date;
    ldMesCierrAnt   date;
  
    /*cursor cierres is
      select distinct lrstart
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;*/
  
  BEGIN
  
    select add_months(pdFechCierrePeriodo, -1) into ldMesCierrAnt from dual;
    pdFechaAnterior := to_date(to_char(pdFechCierrePeriodo, 'dd')||'/'||to_char(ldMesCierrAnt, 'MM')||'/'||to_char(ldMesCierrAnt, 'yyyy'),'dd/MM/yyyy');
  
    /*-- se extraen las dos ultimas fechas de cierre para el query posterior
    lII := 0;
    for i in cierres loop
      lII := lII + 1;
      if lII = 1 then
        pdFechaFinal := i.lrstart;
      elsif lII = 2 then
        pdFechaAnterior := i.lrstart;
        exit;
      end if;
    end loop;*/
    
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select /*+ rule */ customer_id, decode(sum(cachkamt_pay),null,0,sum(cachkamt_pay)) TotPago' ||
                     ' from  ' || pdTabla_Cashreceipts ||
                     ' where  caentdate  >= to_date(''' ||
                     to_char(pdFechaAnterior, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' ||
                     ' and  caentdate  < to_date(''' ||
                     to_char(pdFechCierrePeriodo, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' || ' group  by customer_id' ||
                     ' having sum(cachkamt_pay) <>0';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
    
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set pagosper = pagosper + :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_PAGOSPER;

  -----------------------------------------------
  --           ACTUALIZA_SALDOANT                                         
  -----------------------------------------------
  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2) is
  
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    lII            number;
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
  
  BEGIN
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, cscurbalance ' || ' from  ' ||
                     pTablaCustomerAllAnt;
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
    
      execute immediate 'update ' || pdNombre_Tabla || ' set saldoant = :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
    
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_SALDOANT;

  -----------------------------------------------------
  --    ACTUALIZA_DESCUENTO
  --    Llena los valores de la columna descuento
  --    CBR: 28 Noviembre
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_DESCUENTO(pdFechCierrePeriodo in date,
                                pdNombre_tabla      in varchar2,
                                pdMensErr           out varchar2) is
    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
  
    cursor cur_descuento is
      select /*+ rule */
       d.customer_id, sum(A.OTAMT_DISC_DOC) as descuento
        from ordertrailer  A, --items de la factura
             mpusntab      b, --maestro de servicios
             orderhdr_all  c, --cabecera de facturas
             customer_all  d, -- maestro de cliente
             ccontact_all  f, -- informaci�n demografica de la cuente
             payment_all   g, --Forna de pago
             COB_SERVICIOS h, --formato de reporte
             COB_GRUPOS    I, --NOMBRE GRUPO
             COSTCENTER    j
       where a.otxact = c.ohxact
         and c.ohentdate = pdFechCierrePeriodo
         and C.OHSTATUS IN ('IN', 'CM')
         AND C.OHUSERID IS NULL
         AND c.customer_id = d.customer_id
         and substr(otname,
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                    instr(otname,
                          '.',
                          instr(otname,
                                '.',
                                instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
             b.sncode
         and d.customer_id = f.customer_id
         and f.ccbill = 'X'
         and g.customer_id = d.customer_id
         and act_used = 'X'
         and h.servicio = b.sncode
         and h.ctactble = a.otglsale
         AND D.PRGCODE = I.PRGCODE
         and j.cost_id = d.costcenter_id
       group by d.customer_id
      having sum(A.OTAMT_DISC_DOC) > 0;
  
  BEGIN
    lII := 0;
    for i in cur_descuento loop
      --lvSentencia := ' Update ' || pdNombre_tabla ||
      --               ' set descuento = descuento + ' || i.descuento ||
      --               ' where customer_id = ' || i.customer_id;
      --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      execute immediate ' Update ' || pdNombre_tabla ||
                        ' set descuento = descuento + :1' ||
                        ' where customer_id = :2'
        using i.descuento, i.customer_id;
    
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_DESCUENTO;

  

  ------------------------------
  --     LLENA_BALANCES
  ------------------------------
  PROCEDURE LLENA_BALANCES(pdFechCierrePeriodo date,
                           pdNombre_tabla      varchar2,
                           pd_Tabla_OrderHdr   varchar2,
                           pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;
  
    nombre_campo       varchar2(20);
    nombre_fecha       varchar2(20);
  
    nro_mes        number;
    cuenta_mes     number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;
  
    cursor cur_consumos_cliente is
      select a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre b
      where a.customer_id = b.customer_id
      order by a.customer_id, a.fecha desc;
    cursor cur_consumos_cliente2(pfecha date) is
      select a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre b
      where a.customer_id = b.customer_id
      and a.fecha = pfecha;
    cursor cur_consumos_cliente3(pfecha date) is
      select a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre b
      where a.customer_id = b.customer_id
      and a.fecha <= pfecha;      
    cursor cierres is
      select distinct lrstart
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;
  
  BEGIN
       ------------------------------------------
       -- se actualiza la informacion de consumos
       ------------------------------------------
       delete from co_consumos_cliente where fecha = pdFechCierrePeriodo;
       commit;
        source_cursor := DBMS_SQL.open_cursor;
        lvSentencia   := 'SELECT a.customer_id, sum(a.valor), nvl(sum(a.descuento),0)' ||
                         ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                         ' WHERE a.tipo != ''006 - CREDITOS''' ||
                         ' GROUP BY a.customer_id';
        dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        dbms_sql.define_column(source_cursor, 1, lnCustomerId);
        dbms_sql.define_column(source_cursor, 2, lnValor);
        dbms_sql.define_column(source_cursor, 3, lnDescuento);
        rows_processed := Dbms_sql.execute(source_cursor);
      
        lII := 0;
        loop
          if dbms_sql.fetch_rows(source_cursor) = 0 then
            exit;
          end if;
          dbms_sql.column_value(source_cursor, 1, lnCustomerId);
          dbms_sql.column_value(source_cursor, 2, lnValor);
          dbms_sql.column_value(source_cursor, 3, lnDescuento);
          execute immediate 'insert into co_consumos_cliente'||
                              ' values (:1, :2, :3)'
          using lnCustomerId, pdFechCierrePeriodo, lnValor-lnDescuento;
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
        end loop;
        commit;
        dbms_sql.close_cursor(source_cursor);

       ------------------------------------------
       -- se actualiza la informacion de balances
       ------------------------------------------
       cuenta_mes := 12;
       lII:=0;
       for i in cierres loop
           if i.lrstart <= pdFechCierrePeriodo then
           if cuenta_mes < 1 then
               nombre_campo := 'balance_1';
               lII := 0;
               for j in cur_consumos_cliente3(i.lrstart) loop
                 execute immediate 'update co_cuadre set '||
                                nombre_campo || ' = ' || nombre_campo ||' + :1'||
                                'where customer_id=:2'
                 using j.consumo, j.customer_id;                             
                 lII := lII + 1;
                 if lII = 2000 then
                   lII := 0;
                   commit;
                 end if;                 
               end loop;
               commit;
               exit;
           else
               nombre_campo := 'balance_'||cuenta_mes;
               if to_char(pdFechCierrePeriodo,'dd') = '08' or (to_char(pdFechCierrePeriodo,'dd') = '24' and to_char(i.lrstart,'dd') = '24') then
                  lII := 0;
                  for j in cur_consumos_cliente2(i.lrstart) loop
                     execute immediate 'update co_cuadre set '||
                                    nombre_campo || ' = ' || nombre_campo ||' + :1'||
                                    'where customer_id=:2'
                     using j.consumo, j.customer_id;              
                     lII := lII + 1;
                     if lII = 2000 then
                       lII := 0;
                       commit;
                     end if;
                  end loop;
                  commit;
                  if cuenta_mes >= 1 then
                    execute immediate 'update co_cuadre set fecha_' ||
                                        cuenta_mes || ' = to_date(''' ||
                                        to_char(i.lrstart, 'dd/MM/yyyy') ||
                                        ''',''dd/MM/yyyy'')';
                    commit;
                  end if;
                  cuenta_mes := cuenta_mes - 1;
               end if;
           end if;
           end if;
       end loop;
       commit;
                
    pd_lvMensErr := '';
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := sqlerrm;
    
  END;
  
  
  --------------------------
  --   CalculaTotalDeuda
  --------------------------
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;
  
  Begin
  
    lvSentencia := 'Update ' || pdNombre_tabla || ' set total_deuda = ';
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || ' BALANCE_' || lII || ' +';
    end loop;
    lvSentencia := substr(lvSentencia, 1, length(lvSentencia) - 1);
  
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
    if lvMensErr is not null then
      raise leError;
    end if;
  
  exception
    when leError then
      pd_lvMensErr := 'calculatotaldeuda: ' || lvMensErr;
  End;

  --------------------------
  --   Copia_Fact_Actual
  --------------------------
  PROCEDURE Copia_Fact_Actual(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;
    --lnMesFinal number;
  
  Begin
  
    lvSentencia := ' Update ' || pdNombre_tabla ||
                   ' t  set TOTAL_FACT_ACTUAL  = balance_12';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
    if lvMensErr is not null then
      raise leError;
    end if;
  
  exception
    when leError then
      pd_lvMensErr := 'copia_fact_actual: ' || lvMensErr;
    
  End;

  ----------------------------------------------
  --   INIT_RECUPERADO
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------    
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2) is
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
  BEGIN
    lvSentencia := 'update ' || pdNombre_tabla ||
                   ' nologging set totPagos_recuperado = 0,' ||
                   ' CreditoRecuperado = 0, DebitoRecuperado = 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  END;
  
  PROCEDURE LLENA_PAGORECUPERADO2(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2) is
    -- variables
    lvMensErr      varchar2(2000);
    v_cursor       number;
    lvSentencia    varchar2(2000);
    lII            number;
    ldPeriodo      date;
    source_cursor  integer;
    rows_processed integer;
    rows_fetched   integer;
    lnCustomerId   number;
    lnValor        number;
    lnExisteTabla  number;
  
    -- cursor de pagos extraidos de la tabla online    
    cursor pagos is
      select customer_id, decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
        from cashreceipts_all
        where caentdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
         and catype in (1, 3, 9)
       group by customer_id;
  
  BEGIN
  
    ldperiodo := to_date('24/10/2005', 'dd/mm/yyyy');
  
    -- se valida la fecha de solicitud de cartera a recuperar
    if to_date(pvFechIni, 'dd/mm/yyyy') >= ldPeriodo then
      if to_date(pvFechFin, 'dd/mm/yyyy') >= ldPeriodo then
        lII := 0;
        for i in pagos loop
          execute immediate 'update ' || pdNombre_tabla ||
                            ' set totPagos_recuperado = :1 where customer_id = :2'
            using i.valor, i.customer_id;
        
          lII := lII + 1;
          if lII = 1000 then
            lII := 0;
            commit;
          end if;
        end loop;
      end if;
      commit;
    else
      begin
        lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CASHRECEIPTS_ALL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') || '''';
        source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
        rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS        
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
      
        if lnExisteTabla = 1 then
          lvSentencia := 'select customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay))' ||
                         ' from cashreceipts_all_'||to_char(ldPeriodo, 'ddMMyyyy') ||
                         ' where caentdate >= to_date(''' || pvFechIni ||''', ''dd/mm/yyyy'')' ||
                         ' and catype <> ''2''' ||
                         ' and cachkdate >= to_date(''' || pvFechIni ||''', ''dd/mm/yyyy'')' ||
                         ' and cachkdate <= to_date(''' || pvFechFin ||''', ''dd/mm/yyyy'')' || 
                         ' group by customer_id';
        
          -- se toman los pagos de la ultima tabla de pagos respaldada
          source_cursor := DBMS_SQL.open_cursor;
          Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        
          dbms_sql.define_column(source_cursor, 1, lnCustomerId);
          dbms_sql.define_column(source_cursor, 2, lnValor);
          rows_processed := Dbms_sql.execute(source_cursor);
        
          lII := 0;
          loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnValor);
          
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using lnValor, lnCustomerId;
            lII := lII + 1;
            if lII = 1000 then
              lII := 0;
              commit;
            end if;
          end loop;
          dbms_sql.close_cursor(source_cursor);
          commit;
        else
          -- existe la posibilidad que no se encuentra la tabla 
          -- respaldada de pagos entonces se toma de la online
          -- los datos, esto se da por tardanza en la facturacion
          -- ya que se encuentra un nuevo periodo en la orderhdr_all
          -- pero no se encuentran los datos que se requieren.          
          lII := 0;
          for i in pagos loop
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using i.valor, i.customer_id;
            lII := lII + 1;
            if lII = 1000 then
              lII := 0;
              commit;
            end if;
          end loop;
          commit;
        end if;
      exception
        when others then
          lvMensErr := sqlerrm;
      end;
    end if;
  END;
  

  ----------------------------------------------
  --   LlenaPagoRecuperado
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2) is
    -- variables
    lvMensErr      varchar2(2000);
    v_cursor       number;
    lvSentencia    varchar2(2000);
    lII            number;
    ldPeriodo      date;
    source_cursor  integer;
    rows_processed integer;
    rows_fetched   integer;
    lnCustomerId   number;
    lnValor        number;
    lnExisteTabla  number;
  
    -- cursor de pagos extraidos de la tabla online    
    cursor pagos is
      select customer_id,
             decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
        from cashreceipts_all
       where cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
         and catype in (1, 3, 9)
       group by customer_id;
  
  BEGIN
  
    -- se busca el ultimo periodo vigente
    --SIS RCA 18/oct/2005: Eliminado para que tome el ciclo 1.
    /*select max(t.ohentdate)
     into ldPeriodo
     from orderhdr_all t
    where t.ohentdate is not null
      and t.ohstatus = 'IN';*/
  
    --quemado SIS RCABRERA!!! PARA QUE TOME EL CICLO 1!!!!
    --18/oct/2005
    --LUEGO DE LA CORRECCI�N, QUITAR!!
    ldperiodo := to_date('24/10/2005', 'dd/mm/yyyy');
  
    -- se valida la fecha de solicitud de cartera a recuperar
    if to_date(pvFechIni, 'dd/mm/yyyy') >= ldPeriodo then
      if to_date(pvFechFin, 'dd/mm/yyyy') >= ldPeriodo then
        lII := 0;
        for i in pagos loop
          execute immediate 'update ' || pdNombre_tabla ||
                            ' set totPagos_recuperado = :1 where customer_id = :2'
            using i.valor, i.customer_id;
        
          lII := lII + 1;
          if lII = 1000 then
            lII := 0;
            commit;
          end if;
        end loop;
      end if;
      commit;
    else
      begin
        lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CASHRECEIPTS_ALL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') || '''';
        source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
        rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS        
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
      
        if lnExisteTabla = 1 then
          lvSentencia := 'select customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay))' ||
                         ' from cashreceipts_all_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') ||
                         ' where catype <> ''2''' ||
                         ' and cachkdate >= to_date(''' || pvFechIni ||
                         ''', ''dd/mm/yyyy'')' ||
                         ' and cachkdate <= to_date(''' || pvFechFin ||
                         ''', ''dd/mm/yyyy'')' || ' group by customer_id';
        
          -- se toman los pagos de la ultima tabla de pagos respaldada
          source_cursor := DBMS_SQL.open_cursor;
          Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        
          dbms_sql.define_column(source_cursor, 1, lnCustomerId);
          dbms_sql.define_column(source_cursor, 2, lnValor);
          rows_processed := Dbms_sql.execute(source_cursor);
        
          lII := 0;
          loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnValor);
          
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using lnValor, lnCustomerId;
            lII := lII + 1;
            if lII = 1000 then
              lII := 0;
              commit;
            end if;
          end loop;
          dbms_sql.close_cursor(source_cursor);
          commit;
        else
          -- existe la posibilidad que no se encuentra la tabla 
          -- respaldada de pagos entonces se toma de la online
          -- los datos, esto se da por tardanza en la facturacion
          -- ya que se encuentra un nuevo periodo en la orderhdr_all
          -- pero no se encuentran los datos que se requieren.          
          lII := 0;
          for i in pagos loop
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using i.valor, i.customer_id;
            lII := lII + 1;
            if lII = 1000 then
              lII := 0;
              commit;
            end if;
          end loop;
          commit;
        end if;
      exception
        when others then
          lvMensErr := sqlerrm;
      end;
    end if;
  END;

  ------------------------------------------
  --    LLENA_CREDITORECUPERADO                                    
  --    Funcion utilizada por forms 
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;
  
    -------CREDITOS REGULARES
    cursor creditos is
      select customer_id id_cliente, ohinvamt_doc credito
        from orderhdr_all
       --where ohrefdate between
       where ohentdate BETWEEN  --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and to_char(ohrefdate, 'dd') <> '24'
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount credito
        from customer_all cu, fees f
       where cu.customer_id = f.customer_id(+)
        /* and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate between         --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and amount < 0
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id id_cliente, f.amount credito
        from customer_all cu, fees f
       where cu.customer_id = f.customer_id(+)
         /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN     --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')             
         and cu.paymntresp is null
         and f.amount < 0;
  
  BEGIN
  
    lII := 0;
    for i in creditos loop
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set CreditoRecuperado = CreditoRecuperado + :1 where customer_id = :2'
        using i.credito, i.id_cliente;
    
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    commit;
  
  END;

  
  PROCEDURE LLENA_CARGORECUPERADO2(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;
    lnCargo     number;
      
    cursor cuentas is
    select customer_id from ope_cuadre;
  
  BEGIN
  
    lII := 0;
    for i in cuentas loop
      update ope_cuadre set debitorecuperado = COK_PROCESS_REPORT_HIGH.cof_extrae_cargos(i.customer_id, pvFechIni, pvFechFin)
      where customer_id = i.customer_id;

      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
  
  END;
  
  ------------------------------------------
  --    LLENA_CARGORECUPERADO                                    
  --    Funcion utilizada por forms 
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;
  
    cursor cargos is
    ---------CARGOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento 
        from customer_all cu, fees f
       where cu.customer_id = f.customer_id(+)
         and f.entdate BETWEEN   --CBR 2006-05-16: para mejorar el rendimiento 
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and f.amount > 0
         --and f.sncode <> 103 --cargos por facturaci�n --CBR 2006-05-19: para mejorar el rendimiento 
      UNION ALL
      ---------CARGOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id_high id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento 
        from customer_all cu, fees f
       where cu.customer_id_high = f.customer_id(+)
         and f.entdate BETWEEN  --CBR 2006-05-16: para mejorar el rendimiento 
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and customer_id_high is not null
         and f.amount > 0;
         --and f.sncode <> 103; --cargos por facturaci�n --CBR 2006-05-19: para mejorar el rendimiento 
  
  BEGIN
  
    lII := 0;
    for i in cargos loop
      if i.sncode <> 103 then --CBR 2006-05-19: para mejorar el rendimiento 
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set DebitoRecuperado = DebitoRecuperado + :1 where customer_id = :2'
        using i.cargo, i.id_cliente;
      end if;
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
  
  END;

  --------------------------
  --   Llena_total_deuda_cierre
  --------------------------
  PROCEDURE Llena_total_deuda_cierre(pdFechCierrePeriodo date,
                                     pdNombre_tabla      varchar2,
                                     pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
  
  Begin
  
    lvSentencia := ' Update ' || pdNombre_tabla ||
                   ' t  set TOTAL_deuda_cierre  = total_deuda + total_fact_actual';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
  End;
  --------------------------
  --   LLENATIPO
  --------------------------

  PROCEDURE LLENATIPO(pdFechCierrePeriodo date, pdNombre_tabla varchar2) is
  
    lv_sentencia_upd varchar2(2000);
    lvMensErr        varchar2(2000);
    v_CursorUpdate   number;
    v_Row_Update     number;
    lnMesFinal       number;
  
    lII            number;
    lvSentencia    varchar2(2000);
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
  
  Begin
    -- Son 5 tipos:
    --   = '1_NF'  Cartera vigente. 
    --             Clientes que no tienen factura generada. Actualizado en LLENA BALANCE
    --   = '2_SF'  Se llenara cuando el tipo sea nulo... Pero debe ser realizado al final.
    --   = '3_VNE'. Cartera Vencida. Son valores Negativos o sea saldos a favor del cliente
    --   = '5_R'.   Esto es para los clientes que tienen Mora
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id from ' || pdNombre_tabla ||
                     ' minus' ||
                     ' select customer_id from orderhdr_all where ohstatus = ''IN'' and  ohentdate = to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
    
      -- actualizo los campos de la tabla final
      --lvSentencia := 'update ' || pdNombre_Tabla || ' set  tipo = ''1_NF''' ||
      --               ' where customer_id=' || lnCustomerId;
      --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set  tipo = ''1_NF''' ||
                        ' where customer_id = :1'
        using lnCustomerId;
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
    -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
    --lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));
    lnMesFinal       := 12;
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''3_VNE''' || ' where  BALANCE_' ||
                        lnMesFinal || ' < 0 ';
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;
  
    -- Se actualiza campo tipo para saber que es Mora
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''5_R''' || ' where  mora > 0 ';
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;
  
  end;

  ---------------------------------------------------------------------
  -- Llena_telefono_celular
  ---------------------------------------------------------------------
  Procedure Llena_telefono(pdNombreTabla varchar2,
                           pd_lvMensErr  out varchar2) is
  
    CURSOR TELEFONO IS
      select /*+ RULE +*/
       cu.customer_id, dn.dn_num
        from directory_number   dn,
             contr_services_cap cc,
             contract_all       co,
             customer_all       cu,
             contract_history   ch
       where co.customer_id = cu.customer_id
         and cc.co_id = co.co_id
         and cc.cs_deactiv_date is null
         and cc.seqno = (select max(cc1.seqno)
                           from contr_services_cap cc1
                          where cc1.co_id = cc.co_id)
         and dn.dn_id = cc.dn_id
         and ch.co_id = co.co_id
         and ch.ch_seqno = (select max(chl.ch_seqno)
                              from contract_history chl
                             where chl.co_id = ch.co_id)
       order by cu.customer_id, ch.ch_status;
  
    lv_telefono      varchar2(30);
    lv_customer      number;
    lv_sentencia_upd varchar2(2000);
    lvMensErr        varchar2(2000);
    lII              number;
  
  Begin
  
    lv_telefono := 0;
    lII         := 0;
    FOR i IN TELEFONO LOOP
      if nvl(lv_customer, 0) <> i.customer_id then
        execute immediate ' update ' || pdnombretabla ||
                          ' set telefono = :1' || ' where customer_id = :2'
          using i.dn_num, i.customer_id;
      
        lII         := lII + 1;
        lv_customer := i.customer_id;
        if lII = 1000 then
          lII := 0;
          commit;
        end if;
      end if;
    END LOOP;
  
    commit;
  
  exception
    when others then
      pd_lvMensErr := 'Llena_telefono: ' || sqlerrm;
    
  End;

  Procedure LLENA_BUROCREDITO(pdNombreTabla varchar2,
                              pd_lvMensErr  out varchar2) is
  
    CURSOR CHK_BUROCREDITO IS
      SELECT I.CUSTOMER_ID, decode(I.CHECK01, 'X', 'S', I.CHECK01) check01
        FROM INFO_CUST_CHECK I
       ORDER BY I.customer_id;
  
    lv_customer number;
    ln_i        number := 0;
  
  Begin
  
    FOR i IN CHK_BUROCREDITO LOOP
      if nvl(lv_customer, 0) <> i.customer_id then
        execute immediate ' update ' || pdnombretabla ||
                          ' set burocredito = :1' ||
                          ' where customer_id = :2'
          using i.check01, i.customer_id;
      
        ln_i        := ln_i + 1;
        lv_customer := i.customer_id;
        if ln_i = 1000 then
          ln_i := 0;
          commit;
        end if;
      end if;
    END LOOP;
    commit;
  
  exception
    when others then
      pd_lvMensErr := 'Llena_BuroCredito: ' || sqlerrm;
  End;

  --------------------------------------------
  ---- Llena fecha maxima de pago  del cliente  
  --------------------------------------------
  PROCEDURE LLENA_FECH_MAX_PAGO(pdFechCierrePeriodo in date,
                                pdNombre_Tabla      in varchar2,
                                pd_lvMensErr        out varchar2) is
  
    source_cursor  integer;
    lv_fecha_max   date;
    lv_fecha_max1  date;
    lvsentencia    varchar2(2000);
    lnCustomerId   number;
    rows_processed integer;
    lII            number;
  
    cursor cur_termcode is
      select termcode, termnet from terms_all;
  
  begin
    for i in cur_termcode loop
    
      source_cursor := DBMS_SQL.open_cursor;
      --lvSentencia   := 'select customer_id from customer_all_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||' a'||
      lvSentencia := 'select customer_id from customer_all a' ||
                     ' where a.termcode = ' || i.termcode;
    
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    
      dbms_sql.define_column(source_cursor, 1, lnCustomerId);
      rows_processed := Dbms_sql.execute(source_cursor);
      lII            := 0;
      lv_fecha_max   := pdFechCierrePeriodo + i.termnet;
      loop
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnCustomerId);
        execute immediate 'update ' || pdNombre_Tabla ||
                          ' set fech_max_pago = to_date(''' ||
                          to_char(lv_fecha_max, 'dd/MM/yyyy') ||
                          ''',''dd/MM/yyyy'') where customer_id = :1'
          using lnCustomerId;
      
        lII := lII + 1;
        if lII = 2000 then
          lII := 0;
          commit;
        end if;
      end loop;
    end loop;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'LLENA_FECH_MAX_PAGO: ' || sqlerrm;
    
  END;

  ---------------------------
  -- PROCESA_CUADRE
  ---------------------------
  PROCEDURE PROCESA_CUADRE(pdFechaPeriodo       in date,
                           pdNombre_Tabla       in varchar2,
                           pdTabla_OrderHdr     in varchar2,
                           pdTabla_cashreceipts in varchar2,
                           pdTabla_Customer     in varchar2,
                           pd_lvMensErr         out varchar2) is
  
    --variables
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    Saldo       number;
    vBalance    number;
    vDISPONIBLE number;
    leError exception;
  
  BEGIN
  
    -- se insertan todos los cliente a la base de datos de la base de datos de producci�n
    COK_PROCESS_REPORT_HIGH.InsertaDatosCustomer(pdFechaPeriodo,
                                            pdNombre_tabla,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- procedimiento que actualiza los campos balance de saldos
    COK_PROCESS_REPORT_HIGH.llena_balances(pdFechaPeriodo,
                                      pdNombre_Tabla,
                                      pdTabla_OrderHdr,
                                      lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- se actualizan los valores de pago y el disponible
    COK_PROCESS_REPORT_HIGH.Upd_Disponible_TotPago(pdFechaPeriodo,
                                              pdNombre_tabla,
                                              pdTabla_Cashreceipts,
                                              pdTabla_OrderHdr,
                                              lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- Creditos del 24 se colocan en disponible
    COK_PROCESS_REPORT_HIGH.Credito_a_Disponible(pdFechaPeriodo,
                                            pdNombre_Tabla,
                                            --pdTabla_Cashreceipts,
                                            --pdTabla_OrderHdr,
                                            --pdTabla_Customer,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- Proceso que realiza el balanceo 
    COK_PROCESS_REPORT_HIGH.balanceo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- Llena campo Total_deuda
    COK_PROCESS_REPORT_HIGH.CalculaTotalDeuda(pdFechaPeriodo,
                                         pdNombre_Tabla,
                                         lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- copia el valor de la factura del ultimos mes al campo "Valor_Fact_Actual"
    COK_PROCESS_REPORT_HIGH.Copia_Fact_Actual(pdFechaPeriodo,
                                         pdNombre_Tabla,
                                         lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- Proceso que calcula la mora
    COK_PROCESS_REPORT_HIGH.calcula_mora;
  
    -- Proceso que realiza LlenaTipo
    COK_PROCESS_REPORT_HIGH.LlenaTipo(pdFechaPeriodo, pdNombre_tabla);
  
    -- ACTUALIZA LOS DATOS INFORMATIVOS
    COK_PROCESS_REPORT_HIGH.llena_DatosCliente(pdNombre_Tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- se actualiza la forma de pago
    COK_PROCESS_REPORT_HIGH.Llena_NroFactura(pdFechaPeriodo, pdNombre_tabla);
  
    -- se actualizan la forma de pago, el producto y centro de costo
    COK_PROCESS_REPORT_HIGH.UPd_FPago_Prod_CCosto(pdFechaPeriodo,
                                             pdNombre_tabla,
                                             lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- se actualizan los valores para tipo de forma de pago. Seg�n CBILL para convertir
    COK_PROCESS_REPORT_HIGH.Conv_Cbill_TipoFPago(pdFechaPeriodo,
                                            pdNombre_tabla,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    -- se actualizan los valores para telefono. segun tabla directory number
    COK_PROCESS_REPORT_HIGH.llena_telefono(pdNombre_Tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.SETEA_PLAN_IDEAL(lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    --agrega indicador si el CustomerID tiene Buro de Credito asignado     
    COK_PROCESS_REPORT_HIGH.LLENA_BUROCREDITO(pdNombreTabla => pdNombre_Tabla,
                                         pd_lvMensErr  => lvMensErr);
  
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.LLENA_FECH_MAX_PAGO(pdfechaperiodo,
                                           pdNombre_Tabla,
                                           pd_lvMensErr);
  
    if lvMensErr is not null then
      raise leError;
    end if;
  
  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
    
    WHEN OTHERS THEN
      pd_lvMensErr := 'procesa_cuadre: ' || sqlerrm;
    
  END PROCESA_CUADRE;

  ---------------------------
  -- SET_CAMPOS_MORA
  ---------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2 IS
    lvSentencia varchar2(2000);
    lnMesFinal  number;
  BEGIN
    lnMesFinal  := to_number(to_char(pdFecha, 'MM'));
    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  END SET_CAMPOS_MORA;

  -----------------
  --  CREA_TABLA
  -----------------
  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER IS
  
    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);
  
  BEGIN
  
    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura                                                    
                       COK_PROCESS_REPORT_HIGH.set_campos_mora(pdFechaPeriodo) ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0)'||
                       '  tablespace DATA' ||
                       '  pctfree 10' || '  pctused 40' || '  initrans 1' ||
                       '  maxtrans 255' || '  storage' || '  (initial 256K' ||
                       '    next 256K' || '    minextents 1' ||
                       '    maxextents 505' || '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    -- Crea clave primaria
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PKCUSTOMER_ID' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                       '  primary key (CUSTOMER_ID)' || '  using index' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    lvSentenciaCrea := 'create index CO_CUST_IDX on CO_CUADRE (CUSTCODE)' ||
                       'tablespace IND' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents 505' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    --  Crea indice
    lvSentenciaCrea := 'create index ID_CUSTOMER_ID_' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (CUSTOMER_ID, tipo)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
  
    return 1;
  
  EXCEPTION
    when others then
      return 0;
  END CREA_TABLA_CUADRE;

  /***************************************************************************
  *
  *                             MAIN PROGRAM                                                                          
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2) IS
  
    -- variables
    lvSentencia            VARCHAR2(2000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    lvMensErr              VARCHAR2(2000) := null;
    lnExito                NUMBER;
    lv_nombre_tabla        varchar2(50);
    lv_Tabla_OrderHdr      varchar2(50);
    lv_Tabla_cashreceipts  varchar2(50);
    lv_Tabla_customer      varchar2(50);
    lv_TablaCustomerAllAnt varchar2(50);
    lnMes                  number;
    leError exception;
    
  BEGIN
    
    select valor into lv_nombre_tabla from co_parametros_cliente where campo = 'TABLA_PROCESO';
    select valor into lv_Tabla_OrderHdr from co_parametros_cliente where campo = 'TABLA_ORDERHDR';
    select valor into lv_Tabla_cashreceipts from co_parametros_cliente where campo = 'TABLA_PAGOS';
    select valor into lv_Tabla_customer from co_parametros_cliente where campo = 'TABLA_CLIENTE';
    select valor into lv_TablaCustomerAllAnt from co_parametros_cliente where campo = 'TABLA_CLIENTE_ANT';
          
    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''' ||
                     lv_nombre_tabla || '''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS        
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
  
    if lnExisteTabla = 1 then
      -- se trunca la tabla
      lvSentencia := 'truncate table ' || lv_nombre_tabla;
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;
    else
      --se crea la tabla 
      lnExito := COK_PROCESS_REPORT_HIGH.crea_tabla_cuadre(pdFechCierrePeriodo,
                                                      lvMensErr,
                                                      lv_nombre_tabla);
      if lvMensErr is not null then
        raise leError;
      end if;
    end if;
  
    -- se llama al procedure que procesa el cuadre de la tabla
    COK_PROCESS_REPORT_HIGH.procesa_cuadre(pdFechCierrePeriodo,
                                      lv_nombre_tabla,
                                      lv_Tabla_OrderHdr,
                                      lv_Tabla_cashreceipts,
                                      lv_Tabla_customer,
                                      lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
  
    COK_PROCESS_REPORT_HIGH.actualiza_pagosper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lv_Tabla_cashreceipts,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.actualiza_credtper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.actualiza_cmper(pdFechCierrePeriodo,
                                       lv_nombre_tabla,
                                       lv_Tabla_OrderHdr,
                                       lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.actualiza_consmper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
  
    COK_PROCESS_REPORT_HIGH.actualiza_descuento(pdFechCierrePeriodo,
                                           lv_nombre_tabla,
                                           lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
    
    COK_PROCESS_REPORT_HIGH.CUADRA;
  
    -- se llama a procedimiento de calculo de edad real de mora
    COK_PROCESS_REPORT_HIGH.CO_EDAD_REAL(lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;
    
  
  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
    
    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
    
  END MAIN;

end COK_PROCESS_REPORT_HIGH;
/

