create or replace package gsi_reporte is

PROCEDURE PRINCIPAL;

end gsi_reporte;
/
create or replace package body gsi_reporte is

PROCEDURE PRINCIPAL IS

  CURSOR C1 is
      select rowid, id_contrato from chio_contratos;
  

  CURSOR C2(Pt_Contrato Number, Pt_Proceso number) IS
     SELECT valor_factura FROM PORTA.FA_FACTURAS@AXIS
     WHERE ID_CONTRATO = Pt_Contrato
     AND   ID_PROCESO = Pt_Proceso;
     
     
Var_Proceso    number;
Max_IdProceso  number;
Contador       number:=0;
     
BEGIN

  for i in C1 loop
     
    loop 
      exit when Contador = 6;                      
      
         Contador:= Contador +1;
         
         if Contador = 1 theN
           Var_Proceso := 99999;
         end if;
         
          select max(id_proceso) INTO Max_IdProceso from PORTA.FA_FACTURAS@AXIS
          WHERE  ID_CONTRATO = i.id_contrato 
          and    id_proceso < Var_Proceso;      
          
          for j in C2(i.id_contrato, Max_IdProceso) loop
           
              Var_Proceso:=Max_IdProceso;   
            
              update chio_contratos
              set    promedio_6meses = promedio_6meses + j.valor_factura
              where  rowid = i.rowid;
              commit;
          
          end loop;      
    
    end loop; 
    Contador:=0;
     
  end loop;
  COMMIT;
  
END PRINCIPAL;
  
end gsi_reporte;
/
