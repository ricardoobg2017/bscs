create or replace package doc1_update_bscs_new is

  -- Author  : PCARVAJAL
  -- Created : 22/05/2006 11:02:27
  -- Purpose : Actualizaci�n de BSCS del proyecto DOC1

  procedure doc1_selecciona_cuentas(pv_error out varchar2);

  procedure doc1_obtengo_numero_fiscal(pv_custcode         in  varchar2,
                                       pn_customer_id      in  number,
                                       pn_seq_id           in  number,
                                       pn_ejecucion        in  number,
                                       pd_periodo          in  date,
                                       pv_tipo             in  varchar2,
                                       pv_prgcode          in  varchar2,
                                       pn_costcenter       in  number,
                                       pv_numero_fiscal    out varchar2,
                                       pv_estado           out varchar2,
                                       pv_error            out varchar2);

  procedure doc1_actualiza_orderhdr(pv_numero_fiscal varchar2,
                                    pd_periodo date,
                                    pn_customer_id number);

   function doc1_obtiene_fecha_final return date;

   function doc1_obtiene_tipo return varchar2;
   
  procedure doc1_actualiza_nueva_secuencia(pv_fk_prgcode        varchar2,
                                           pn_fk_costcenter     number,
                                           pn_seq_id            number,
                                           pv_error         out varchar2);
                                           
  function doc1_valida_genera_sec_fiscal(pd_date_create     date,
                                         pn_customer_id     number,
                                         pv_error       out varchar2) return boolean;
end doc1_update_bscs_new;
/
create or replace package body doc1_update_bscs_new is
/*
   ----------------------------------------------------------
   Version   : 1.0.0
   Autor     : Paola Carvajal
   Proyecto  : [1537] - Conecel Proyecto DOC1
   Motivo    : Actualizar Informaci�n orderhdr_all y aut_seq
   Fecha     : 22/05/2006
   Autorizado: SIS Patricio Chonillo
   -----------------------------------------------------------
   Fecha: 16-Junio-2009
   Autor: Paola Carvajal
   Proyecto:[4490] Secuencial de Facturas BSCS - Nuevo Puntos de Emisi�n
   Solicitado: Patricio Chonillo
   -----------------------------------------------------------   
*/

  lv_error     varchar2(1000);


  procedure doc1_selecciona_cuentas(pv_error out varchar2) is

  ln_despachador    number;
  ln_ejecucion      number;
  le_error          exception;
  -- Tomo la informaci�n de Ciclos a procesar --
  cursor c_principal is
    select a.billcycle, b.tipo, count(*)
      from doc1_cuentas a, doc1_parametros_facturacion b
     where a.estado = 'A'
       and b.procesar = 'S'
       and b.estado = 'A'
       and a.customer_id_high is null
       and a.billcycle <> 99
       and a.billcycle = b.billcycle
     group by a.billcycle, b.tipo;

  -- Selecciono las cuentas a considerar --
  cursor c_cuentas(cn_billcycle  number) is
    select a.rowid rowid1,
           b.rowid rowid2,
           a.custcode,
           a.prgcode,
           a.costcenter_id,
           a.periodo,
           a.customer_id
      from doc1_cuentas a, doc1_cuentas b
     where a.billcycle = cn_billcycle and a.procesador = 0 and
           b.customer_id_high(+) = a.customer_id and
           a.customer_id_high is null
           and a.estado = 'A' --SIS RCA 13/02/2007
           and  b.tipo(+) = a.tipo --SIS RCA 25/02/2007
     order by a.last_billing_duration desc;

 cursor c_aut_prod(cv_fk_prgcode      varchar2,
                    cn_fk_costcenter   number) is
   select prod.fk_seq_id
     from aut_prod prod
    where prod.fk_prgcode = cv_fk_prgcode and
          prod.fk_costcenter = cn_fk_costcenter and
          prod.fk_vsd_id =1;

  cursor c_numero_fiscal(cv_custcode   varchar2,
                         pv_periodo    varchar2) is
   select f.ohrefnum
     from doc1_numero_fiscal f
    where f.custcode = cv_custcode
      and f.periodo = to_date(pv_periodo, 'dd/mm/yyyy');

  -- Declaraci�n de Variables --
  lv_numero_fiscal           doc1_numero_fiscal.ohrefnum%type;
  lc_aut_prod                c_aut_prod%rowtype;
  lv_estado                  varchar2(1);
  ln_contador                number:=0;
  lv_tipo                    varchar2(2);
  lv_tipo_ejecuta            varchar2(2);
  le_salida                  exception;
  lb_bandera_genera          boolean;
  begin

   -- Secuencia para tener control de ejecuciones --
   select s_doc1_ejecucion.Nextval into ln_ejecucion from dual;

   ln_despachador:=1;

   for billcycle in c_principal loop

       for i in c_cuentas(billcycle.billcycle) loop
          begin
                -- Obtengo seq_id del producto para obtener la secuencia fiscal --
                lc_aut_prod.fk_seq_id:=null;
                open c_aut_prod(i.prgcode, i.costcenter_id);
                fetch c_aut_prod into lc_aut_prod;
                close c_aut_prod;

                lv_estado:='A';
                lv_numero_fiscal:=null;

                /*Solicitado por Mario Aycart para Ciclo 34
                  Generar el n�mero secuencia Fiscal solo para Facturaci�n
                */
                lv_tipo:=billcycle.tipo;
                lv_tipo_ejecuta:='S';
                -- Comentariado por Solicitud de MAY proyecto [3311] MEJORAS AL PROCESOS DE DOC1
              /*  if doc1_obtiene_tipo = 'F' and lv_tipo = 'CG' then
                  if billcycle.billcycle = '34' then
                     lv_tipo_ejecuta:='N';
                     lv_tipo:='C';
                  end if;
                end if;*/

                -- Cambio Solitado por Patricio Chonillo
                -- Para no generar secuencias fiscales a Cuentas que 
                -- no van a generar facturas 21-Junio-2010
                -- L�gica de detecci�n generada por Billing
                lb_bandera_genera:=doc1_valida_genera_sec_fiscal(i.periodo,
                                                                 i.customer_id,
                                                                 pv_error);
                if pv_error is not null then
                   raise le_error;
                end if;                                                 
                
                if lv_tipo ='C' and lb_bandera_genera then
                   if ln_contador = 0 then -- Bitacorizo aut_Seq_Hist --
                      insert into doc1_aut_seq_hist
                      select seq_id,
                             sri_com,
                             sri_prd,
                             seq_ini,
                             seq_fin,
                             seq_last,
                             fk_vsd_id,
                             doc1_obtiene_fecha_final,
                             ln_ejecucion,
                             sysdate
                        from aut_seq;
                     end if;
                    ln_contador:=ln_contador+1;
                    -- Verifico si existe n�meros fiscal ya registrados --
                    open c_numero_fiscal(i.custcode, to_char(i.periodo, 'dd/mm/yyyy'));
                    fetch c_numero_fiscal into lv_numero_fiscal;
                    close c_numero_fiscal;

                    lv_numero_fiscal:=nvl(lv_numero_fiscal,'000-000-0000000');

                    if lv_numero_fiscal = '000-000-0000000' then
                      -- Si no existe obtengo el correspondiente --
                      doc1_obtengo_numero_fiscal(pv_custcode      => i.custcode,
                                                 pn_customer_id   => i.customer_id,
                                                 pn_seq_id        => lc_aut_prod.fk_seq_id,
                                                 pn_ejecucion     => ln_ejecucion,
                                                 pd_periodo       => i.periodo,
                                                 pv_tipo          => lv_tipo_ejecuta,
                                                 pv_prgcode       => i.prgcode, 
                                                 pn_costcenter    => i.costcenter_id,
                                                 pv_numero_fiscal => lv_numero_fiscal,                                         
                                                 pv_estado        => lv_estado,
                                                 pv_error         => pv_error);
                        if pv_error is not null then
                           raise le_error;
                        end if;                                                 

                    else

                              doc1_actualiza_orderhdr(lv_numero_fiscal,
                                    i.periodo,
                                    i.customer_id);

                    end if;

                else
                    lv_numero_fiscal:='000-000-0000000';
                end if;

                update doc1_cuentas t
                   set t.procesador = ln_despachador,
                       t.seq_id     = lc_aut_prod.fk_seq_id,
                       t.ohrefnum   = lv_numero_fiscal,
                       t.estado     = lv_estado
                 where rowid = i.rowid1;

                 if i.rowid2 is not null then
                    update doc1_cuentas t
                       set t.procesador = ln_despachador,
                           t.seq_id     = lc_aut_prod.fk_seq_id,
                           t.estado     = lv_estado
                     where rowid = i.rowid2;
                 end if;

                 commit;
                 ln_despachador:=ln_despachador +1;

                 if ln_despachador = 9 then
                   ln_despachador:=1;
                 end if;
          exception
            when le_error then
              raise le_salida;
            when others then
               pv_error:=sqlerrm;
               raise le_salida;
           end;
       end loop;

    commit;
   end loop;
  exception
   when le_salida then
     pv_error:=pv_error;
   when others then
     pv_error:=sqlerrm;
  end doc1_selecciona_cuentas;

  procedure doc1_obtengo_numero_fiscal(pv_custcode         in  varchar2,
                                       pn_customer_id      in  number,
                                       pn_seq_id           in  number,
                                       pn_ejecucion        in  number,
                                       pd_periodo          in  date,
                                       pv_tipo             in  varchar2,
                                       pv_prgcode          in  varchar2,
                                       pn_costcenter       in  number,                                       
                                       pv_numero_fiscal    out varchar2,
                                       pv_estado           out varchar2,
                                       pv_error            out varchar2) is
  --  Generaci�n de N�mero de Secuencias --
  cursor c_aut_seq(cn_seq_id   number) is
   select s.seq_ini, s.seq_fin, s.sri_com, s.sri_prd, s.seq_last
     from aut_seq s
    where s.seq_id = cn_seq_id
      for update of s.seq_last;

  lb_notfound                boolean;
  lc_c_aut_seq               c_aut_seq%rowtype;
  ln_seq_last                aut_seq.seq_last%type;
  le_error                   exception;
  begin
      pv_estado:='A';
      open c_aut_seq(pn_seq_id);
      fetch c_aut_seq into lc_c_aut_seq;
      lb_notfound:=c_aut_seq%notfound;

      if not lb_notfound then

          -- Si llega al m�ximo de Secuencia se reinicia autom�ticamente --
          if lc_c_aut_seq.seq_last = lc_c_aut_seq.seq_fin then
--           ln_seq_last:=lc_c_aut_seq.seq_ini +1;
              lv_error:='Secuencia llego al Final Tabla aut_seq campo seq_id '||pn_seq_id;
              pv_estado:='E';
              raise le_error;              
          else
             ln_seq_last:=lc_c_aut_seq.seq_last+1;
          end if;

--          ln_seq_last:=lc_c_aut_seq.seq_last+1;
          update aut_seq s
             set s.seq_last = ln_seq_last
           where current of c_aut_seq;

          pv_numero_fiscal:=lc_c_aut_seq.sri_com||'-'||lc_c_aut_seq.sri_prd||'-'||lpad(ln_seq_last,7,0);

          insert into doc1_numero_fiscal
            (ohrefnum, orden, custcode, seq_id, estado, periodo)
          values
            (pv_numero_fiscal,
             pn_ejecucion,
             pv_custcode,
             pn_seq_id,
             'A',
             pd_periodo);
          close c_aut_seq;

          if pv_tipo ='S' then

          doc1_actualiza_orderhdr(pv_numero_fiscal,
                                    pd_periodo,
                                    pn_customer_id);
         /*   update orderhdr_all ord
               set ord.ohrefnum = pv_numero_fiscal
             where ord.ohentdate = pd_periodo
               and ord.customer_id = pn_customer_id
               and ord.ohstatus = 'IN';*/
          end if;
          
          -- Verifico si el n�mero secuencial ha llegado al final seq_fin --
          -- [4490] Secuencial de Facturas BSCS - Nuevo Puntos de Emisi�n --
           if ln_seq_last = lc_c_aut_seq.seq_fin then
             doc1_actualiza_nueva_secuencia(pv_fk_prgcode    => pv_prgcode,
                                            pn_fk_costcenter => pn_costcenter,
                                            pn_seq_id        => pn_seq_id,
                                            pv_error         => pv_error) ;        
             if pv_error is not null then
                pv_estado:='E';
             end if;
           end if;
      else
           pv_estado:='E';
      end if;

  exception
   when le_error then
     pv_error:=lv_error;
   when others then
     lv_error:=substr(sqlerrm,1,1000);
     pv_estado:='E';
  end doc1_obtengo_numero_fiscal;

  procedure doc1_actualiza_orderhdr(pv_numero_fiscal varchar2,
                                    pd_periodo date,
                                    pn_customer_id number) is

  begin

            update orderhdr_all ord
               set ord.ohrefnum = pv_numero_fiscal
             where ord.ohentdate = pd_periodo
               and ord.customer_id = pn_customer_id
               and ord.ohstatus = 'IN'
               and exists (select 1 from sysadm.document_reference re where
                           re.customer_id = pn_customer_id and re.date_created=pd_periodo
                           and re.ohxact is not null) ;


  exception
   when others then null;

  end doc1_actualiza_orderhdr;




  function doc1_obtiene_fecha_final return date is
  ld_fecha              date;
  begin
       select periodo_final
         into ld_fecha
         from doc1_parametro_inicial
        where estado = 'A';
  return ld_fecha;
  end doc1_obtiene_fecha_final;

  function doc1_obtiene_tipo return varchar2 is
  lv_tipo              doc1_parametro_inicial.tipo%type;
  begin
       select tipo
         into lv_tipo
         from doc1_parametro_inicial
        where estado = 'A';
  return lv_tipo;
  end doc1_obtiene_tipo;

  procedure doc1_actualiza_nueva_secuencia(pv_fk_prgcode        varchar2,
                                           pn_fk_costcenter     number,
                                           pn_seq_id            number,
                                           pv_error         out varchar2) is
                                           
     cursor c_aut_prod(cv_fk_prgcode    varchar2, 
                       cn_fk_costcenter number, 
                       cn_fk_vsd_id     number) is 
        select prod.fk_seq_id
          from aut_prod prod
         where fk_prgcode = cv_fk_prgcode and
               fk_costcenter = cn_fk_costcenter and
               fk_vsd_id = cn_fk_vsd_id;

  ln_fk_vsd_id            number:=0;
  ln_fk_vsd_id_actual     number:=1;      
  ln_fk_vsd_id_anterior   number:=2;  
  lc_c_aut_prod           c_aut_prod%rowtype;
  lb_found                boolean;
  le_error                exception;
  begin
      -- Consulto el nuevo seq_id a utilizar Con c�digo fk_vsd_id 0 --
      open c_aut_prod(pv_fk_prgcode , pn_fk_costcenter, ln_fk_vsd_id);
      fetch c_aut_prod into lc_c_aut_prod;
      lb_found:=c_aut_prod%found;
      close c_aut_prod;
      if not lb_found then
         pv_error:='No se encuentra asignado la siguiente secuencia para el prgcode '||pv_fk_prgcode ||' Costcenter '||pn_fk_costcenter ||' Table aut_prod';
         raise le_error;
      end if;
   
      -- Primero deshabilita el anterior -- 
      INSERT into aut_seq
        SELECT seq_id,
               sri_com,
               sri_prd,
               seq_ini,
               seq_fin,
               seq_last,
               ln_fk_vsd_id_anterior  -- 2
          from aut_seq
         where seq_id = pn_seq_id;

      update aut_prod prod
         set prod.fk_vsd_id = ln_fk_vsd_id_anterior  -- 2
       where prod.fk_seq_id  = pn_seq_id;
                                   
       DELETE aut_seq seq
        where seq.seq_id = pn_seq_id AND
              SEQ.FK_VSD_ID = ln_fk_vsd_id_actual; -- 1
      
       -- Habilito la nueva Secuencia --  
        INSERT into aut_seq
          SELECT seq_id,
                 sri_com,
                 sri_prd,
                 seq_ini,
                 seq_fin,
                 seq_last,
                 ln_fk_vsd_id_actual -- 1
            from aut_seq 
           where seq_id = lc_c_aut_prod.fk_seq_id;

      update aut_prod prod
         set prod.fk_vsd_id = ln_fk_vsd_id_actual -- 1
       where prod.fk_seq_id  = lc_c_aut_prod.fk_seq_id;
                                   
       DELETE aut_seq seq
        where seq.seq_id = lc_c_aut_prod.fk_seq_id AND
              SEQ.FK_VSD_ID = ln_fk_vsd_id; -- 0      
            
      
  exception 
    when le_error then
      pv_error:=pv_error;    
    when others then
      pv_error:=sqlerrm;
  end doc1_actualiza_nueva_secuencia;
  
  function doc1_valida_genera_sec_fiscal(pd_date_create     date,
                                         pn_customer_id     number,
                                         pv_error       out varchar2) return boolean is
  ln_valor              number;
  lb_bandera            boolean;
  begin
  
  begin
    select count(*)
      into ln_valor
      from ordertax_items
     where otxact =
           (select ohxact
              from sysadm.document_reference
             where date_created = to_date(pd_date_create, 'dd/mm/yyyy')
               and customer_id = pn_customer_id)
       and taxcat_id = 3;
  exception
   when others then
     ln_valor:=0;
  end;
  
  if ln_valor = 0 then
        begin
        select /*+ rule +*/ 1
         into ln_valor
          from customer_all
         where customer_id in
               (select /*+ rule +*/
                 customer_id
                  from document_reference
                 where ohxact in
                       (select /*+ rule +*/
                        distinct otxact
                          from ordertrailer
                         where otxact in
                               (select /*+ rule +*/
                                 ohxact
                                  from document_reference
                                 where billcycle = '99'
                                   and date_created =
                                       to_date(pd_date_create, 'dd/mm/yyyy'))
                           and (substr(substr(substr(substr(otname,
                                                            instr(otname, '.', 1) + 1),
                                                     instr(substr(otname,instr(otname, '.', 1) + 1),'.',1) + 1),
                                              instr(substr(substr(otname,instr(otname, '.', 1) + 1),
                                                           instr(substr(otname,instr(otname,'.',1) + 1),'.',1) + 1),
                                                    '.',1) + 1),1,
                                       instr(substr(substr(substr(otname,
                                                                  instr(otname, '.', 1) + 1),
                                                           instr(substr(otname,instr(otname,'.',1) + 1),'.',1) + 1),
                                                    instr(substr(substr(otname,instr(otname,
                                                                              '.',1) + 1),
                                                                 instr(substr(otname,instr(otname,
                                                                                    '.',1) + 1),'.',
                                                                       1) + 1),'.',1) + 1),'.',1) - 1)
                               in
                               (select to_Char(servicio)
                                   from cob_servicios
                                  where tipo not in ('005 - CARGOS', '006 - CREDITOS')) or
                               otname like '261.4.13.55.U.BASE%')));
         exception
           when others then
             ln_valor:=0;
          end;
    end if;
    
    lb_bandera:=false;
    if ln_valor = 1 then
       lb_bandera:=true;
    end if;
    return lb_bandera;
  exception
    when others then
         pv_error:=sqlerrm;
         return false;
  end doc1_valida_genera_sec_fiscal;
  
end doc1_update_bscs_new;
/
