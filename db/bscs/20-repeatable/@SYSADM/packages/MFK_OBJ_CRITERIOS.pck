CREATE OR REPLACE PACKAGE MFK_OBJ_CRITERIOS
 /* 
 ||   Name       : MFK_OBJ_CRITERIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */ 
 IS 
 /*HELP 
   Overview of MFK_OBJ_CRITERIOS
   HELP*/ 
 
 /*EXAMPLES 
   Examples of test 
   EXAMPLES*/ 
 
 /* Constants */ 
 
 /* Exceptions */ 
 
 /* Variables */ 
 
 /* Toggles */ 
 
 /* Windows */ 
 
 /* Programs */ 
 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL); 

PROCEDURE MFP_INSERTAR(PV_DESCRIPCION IN  MF_CRITERIOS.DESCRIPCION%TYPE,
                       PV_CONTENIDO   IN  MF_CRITERIOS.CONTENIDO%TYPE,
                       PV_FUNCION     IN  MF_CRITERIOS.FUNCION%TYPE,
                       PN_secuencia   OUT NUMBER,
                       PV_MSG_ERROR   IN  OUT VARCHAR2
--
); 
--
--
PROCEDURE MFP_ACTUALIZAR( 
 PN_IDCRITERIO IN  MF_CRITERIOS.IDCRITERIO%TYPE,
 PV_DESCRIPCION IN  MF_CRITERIOS.DESCRIPCION%TYPE,
 PV_CONTENIDO IN  MF_CRITERIOS.CONTENIDO%TYPE,
 PV_FUNCION IN  MF_CRITERIOS.FUNCION%TYPE,
 PV_MSG_ERROR IN  OUT VARCHAR2 
--
); 
 
--
PROCEDURE MFP_ELIMINAR(PN_IDCRITERIO  IN  MF_CRITERIOS.IDCRITERIO%TYPE, 
                       PV_MSG_ERROR IN  OUT VARCHAR2); 
--
--

END MFK_OBJ_CRITERIOS ; 
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_CRITERIOS
 /* 
 ||   Name       : MFK_OBJ_CRITERIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 IS 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL) is 
BEGIN
 NULL;
END MFP_HELP; 

PROCEDURE MFP_INSERTAR(PV_DESCRIPCION IN  MF_CRITERIOS.DESCRIPCION%TYPE,
                       PV_CONTENIDO   IN  MF_CRITERIOS.CONTENIDO%TYPE,
                       PV_FUNCION     IN  MF_CRITERIOS.FUNCION%TYPE,
                       PN_secuencia   OUT NUMBER,
                       PV_MSG_ERROR   IN  OUT VARCHAR2 
                       --
                       ) 
                      IS  
 /* 
 ||   Name       : MFP_INSERTAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIOS.MFP_INSERTAR';
 LN_IDCRITERIO  MF_CRITERIOS.IDCRITERIO%TYPE;
 
BEGIN

 PV_MSG_ERROR := NULL; 
 
 SELECT mfs_criterios_sec.NEXTVAL INTO LN_IDCRITERIO FROM  dual;

 INSERT INTO MF_CRITERIOS(IDCRITERIO, DESCRIPCION,CONTENIDO,FUNCION)
  VALUES  (LN_IDCRITERIO,
           PV_DESCRIPCION, 
           PV_CONTENIDO,
           PV_FUNCION);

 PN_secuencia := LN_IDCRITERIO;           
           
   
EXCEPTION -- Exception 
WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
END MFP_INSERTAR;
 
PROCEDURE MFP_ACTUALIZAR(PN_IDCRITERIO  IN  MF_CRITERIOS.IDCRITERIO%TYPE,
                         PV_DESCRIPCION IN  MF_CRITERIOS.DESCRIPCION%TYPE,
                         PV_CONTENIDO   IN  MF_CRITERIOS.CONTENIDO%TYPE,
                         PV_FUNCION     IN  MF_CRITERIOS.FUNCION%TYPE,
                         PV_MSG_ERROR   IN  OUT VARCHAR2 
                        ) 
AS  
 /* 
 ||   Name       : MFP_ACTUALIZAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 CURSOR C_table_cur(Cn_criterio number) IS 
    SELECT * FROM MF_CRITERIOS c
    WHERE c.idCriterio = Cn_criterio;

 Lc_CurrentRow     MF_CRITERIOS%ROWTYPE ;
 Le_NoDataUpdated  EXCEPTION;

 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIOS.MFP_ACTUALIZAR';
BEGIN

 PV_MSG_ERROR := NULL; 
 OPEN C_table_cur(PN_IDCRITERIO) ;
 FETCH C_table_cur INTO Lc_CurrentRow ;
 IF C_table_cur%NOTFOUND THEN 
    RAISE Le_NoDataUpdated ;
 END IF;

 UPDATE MF_CRITERIOS SET -- Revisar los campos que desea actualizar
    DESCRIPCION = NVL(PV_DESCRIPCION, DESCRIPCION)
   ,CONTENIDO = NVL(PV_CONTENIDO, CONTENIDO)
   ,FUNCION = NVL(PV_FUNCION, FUNCION)
 WHERE idCriterio = PN_IDCRITERIO;

 CLOSE C_table_cur ;

EXCEPTION -- Exception 
WHEN Le_NoDataUpdated THEN
    PV_MSG_ERROR := 'No se encontro el registro que desea actualizar. '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END MFP_ACTUALIZAR;
 
PROCEDURE MFP_ELIMINAR(PN_IDCRITERIO  IN  MF_CRITERIOS.IDCRITERIO%TYPE, 
                       PV_MSG_ERROR IN  OUT VARCHAR2) 
AS  
 /* 
 ||   Name       : MFP_ELIMINAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 CURSOR C_table_cur(cn_idCriterio number) IS 
     SELECT * FROM MF_CRITERIOS c
     where c.idcriterio = cn_idCriterio;
 Lc_CurrentRow     MF_CRITERIOS%ROWTYPE ;
 Le_NoDataDeleted  EXCEPTION;
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIOS.MFP_ELIMINAR';
BEGIN

 PV_MSG_ERROR := NULL;
 OPEN C_table_cur(PN_IDCRITERIO) ;
 FETCH C_table_cur INTO Lc_CurrentRow ;
 IF C_table_cur%NOTFOUND THEN 
    RAISE Le_NoDataDeleted ;
 END IF;

 DELETE FROM MF_CRITERIOS
 WHERE idCriterio = PN_IDCRITERIO; 

 CLOSE C_table_cur ;

EXCEPTION -- Exception 
 WHEN Le_NoDataDeleted  THEN
    PV_MSG_ERROR := 'El registro que desea borrar no existe. '||LV_APLICACION; 
    CLOSE C_table_cur ;
 WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END MFP_ELIMINAR;
--

END MFK_OBJ_CRITERIOS ;
/

