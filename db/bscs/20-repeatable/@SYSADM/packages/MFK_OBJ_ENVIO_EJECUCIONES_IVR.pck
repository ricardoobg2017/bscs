CREATE OR REPLACE PACKAGE MFK_OBJ_ENVIO_EJECUCIONES_IVR IS

  PROCEDURE mfp_insertar(pn_idenvio                  IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                         pd_fecha_ejecucion          IN mf_envio_ejecuciones_ivr.fecha_ejecucion%TYPE,
                         pd_fecha_inicio             IN mf_envio_ejecuciones_ivr.fecha_inicio%TYPE,
                         pd_fecha_fin                IN mf_envio_ejecuciones_ivr.fecha_fin%TYPE,
                         pv_mensaje_proceso          IN mf_envio_ejecuciones_ivr.mensaje_proceso%TYPE,
                         pn_tot_mensajes_enviados    IN mf_envio_ejecuciones_ivr.tot_mensajes_enviados%TYPE,
                         pn_tot_mensajes_no_enviados IN mf_envio_ejecuciones_ivr.tot_mensajes_no_enviados%TYPE DEFAULT NULL,
                         pn_secuencia                IN OUT mf_envio_ejecuciones_ivr.secuencia%TYPE,
                         pv_msgerror                 IN OUT VARCHAR2,
                         pv_estado_envio             IN VARCHAR2 DEFAULT NULL);

  PROCEDURE mfp_actualizar(pn_idenvio                  IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                           pn_secuencia                IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                           pv_estado_envio             IN mf_envio_ejecuciones_ivr.estado_envio%TYPE,
                           pd_fecha_ejecucion          IN mf_envio_ejecuciones_ivr.fecha_ejecucion%TYPE,
                           pd_fecha_inicio             IN mf_envio_ejecuciones_ivr.fecha_inicio%TYPE,
                           pd_fecha_fin                IN mf_envio_ejecuciones_ivr.fecha_fin%TYPE,
                           pv_mensaje_proceso          IN mf_envio_ejecuciones_ivr.mensaje_proceso%TYPE,
                           pn_tot_mensajes_enviados    IN mf_envio_ejecuciones_ivr.tot_mensajes_enviados%TYPE,
                           pn_tot_mensajes_no_enviados IN mf_envio_ejecuciones_ivr.tot_mensajes_no_enviados%TYPE DEFAULT NULL,
                           pv_msgerror                 IN OUT VARCHAR2);

  PROCEDURE mfp_eliminar(pn_idenvio   IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                         pn_secuencia IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                         pv_msg_error IN OUT VARCHAR2);

  PROCEDURE mfp_eliminacion_fisica(pn_idenvio   IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                                   pn_secuencia IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                   pv_msg_error IN OUT VARCHAR2);

END MFK_OBJ_ENVIO_EJECUCIONES_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_ENVIO_EJECUCIONES_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar el envio de ejecuciones en la tabla
  **               MF_ENVIO_EJECUCIONES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar(pn_idenvio                  IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                         pd_fecha_ejecucion          IN mf_envio_ejecuciones_ivr.fecha_ejecucion%TYPE,
                         pd_fecha_inicio             IN mf_envio_ejecuciones_ivr.fecha_inicio%TYPE,
                         pd_fecha_fin                IN mf_envio_ejecuciones_ivr.fecha_fin%TYPE,
                         pv_mensaje_proceso          IN mf_envio_ejecuciones_ivr.mensaje_proceso%TYPE,
                         pn_tot_mensajes_enviados    IN mf_envio_ejecuciones_ivr.tot_mensajes_enviados%TYPE,
                         pn_tot_mensajes_no_enviados IN mf_envio_ejecuciones_ivr.tot_mensajes_no_enviados%TYPE DEFAULT NULL,
                         pn_secuencia                IN OUT mf_envio_ejecuciones_ivr.secuencia%TYPE,
                         pv_msgerror                 IN OUT VARCHAR2,
                         pv_estado_envio             IN VARCHAR2 DEFAULT NULL) IS
  
    lv_aplicacion   VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES_IVR.MFP_INSERTAR';
    ln_secuencia    mf_envio_ejecuciones_ivr.secuencia%TYPE;
    lv_estado_envio mf_envio_ejecuciones_ivr.estado_envio%TYPE;
    lv_estado       mf_envio_ejecuciones_ivr.estado%TYPE;
    
  BEGIN
  
    SELECT mfs_envioejecucion_sec_ivr.nextval INTO ln_secuencia FROM dual;
    lv_estado_envio := 'P'; --Significa Pendiente
    IF nvl(pv_estado_envio, 'P') = 'R' THEN
      lv_estado := NULL;
    ELSE
      lv_estado := 'A';
    END IF;
  
    INSERT INTO mf_envio_ejecuciones_ivr
      (idenvio,
       secuencia,
       estado_envio,
       fecha_ejecucion,
       fecha_inicio,
       fecha_fin,
       mensaje_proceso,
       tot_mensajes_enviados,
       tot_mensajes_no_enviados,
       estado)
    VALUES
      (pn_idenvio,
       ln_secuencia,
       nvl(pv_estado_envio, lv_estado_envio),
       pd_fecha_ejecucion,
       pd_fecha_inicio,
       pd_fecha_fin,
       pv_mensaje_proceso,
       pn_tot_mensajes_enviados,
       pn_tot_mensajes_no_enviados,
       lv_estado);
    pn_secuencia := ln_secuencia;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite actualizar el envio de ejecuciones en la tabla
  **               MF_ENVIO_EJECUCIONES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_actualizar(pn_idenvio                  IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                           pn_secuencia                IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                           pv_estado_envio             IN mf_envio_ejecuciones_ivr.estado_envio%TYPE,
                           pd_fecha_ejecucion          IN mf_envio_ejecuciones_ivr.fecha_ejecucion%TYPE,
                           pd_fecha_inicio             IN mf_envio_ejecuciones_ivr.fecha_inicio%TYPE,
                           pd_fecha_fin                IN mf_envio_ejecuciones_ivr.fecha_fin%TYPE,
                           pv_mensaje_proceso          IN mf_envio_ejecuciones_ivr.mensaje_proceso%TYPE,
                           pn_tot_mensajes_enviados    IN mf_envio_ejecuciones_ivr.tot_mensajes_enviados%TYPE,
                           pn_tot_mensajes_no_enviados IN mf_envio_ejecuciones_ivr.tot_mensajes_no_enviados%TYPE DEFAULT NULL,
                           pv_msgerror                 IN OUT VARCHAR2) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER, cn_secuencia NUMBER) IS
      SELECT *
        FROM mf_envio_ejecuciones_ivr ex
       WHERE ex.idenvio = cn_idenvio
         AND ex.secuencia = cn_secuencia;
  
    lc_currentrow    mf_envio_ejecuciones_ivr%ROWTYPE;
    le_nodataupdated EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES_IVR.MFP_ACTUALIZAR';
    
  BEGIN
  
    OPEN c_table_cur(pn_idenvio, pn_secuencia);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodataupdated;
    END IF;
  
    UPDATE mf_envio_ejecuciones_ivr
       SET estado_envio             = nvl(pv_estado_envio, estado_envio),
           fecha_inicio             = nvl(pd_fecha_inicio, fecha_inicio),
           fecha_fin                = nvl(pd_fecha_fin, fecha_fin),
           mensaje_proceso          = nvl(pv_mensaje_proceso, mensaje_proceso),
           tot_mensajes_enviados    = nvl(pn_tot_mensajes_enviados, tot_mensajes_enviados),
           tot_mensajes_no_enviados = nvl(pn_tot_mensajes_no_enviados, tot_mensajes_no_enviados),
           fecha_ejecucion          = nvl(pd_fecha_ejecucion, fecha_ejecucion)
     WHERE idenvio = pn_idenvio
       AND secuencia = pn_secuencia;
    CLOSE c_table_cur;
  
  EXCEPTION 
    WHEN le_nodataupdated THEN
      pv_msgerror := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite eliminar el envio de ejecuciones en la tabla
  **               MF_ENVIO_EJECUCIONES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_eliminar(pn_idenvio   IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                         pn_secuencia IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                         pv_msg_error IN OUT VARCHAR2) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER, cn_secuencia NUMBER) IS
      SELECT *
        FROM mf_envio_ejecuciones_ivr
       WHERE idenvio = cn_idenvio
         AND secuencia = cn_secuencia;
  
    lc_currentrow    mf_envio_ejecuciones_ivr%ROWTYPE;
    le_nodatadeleted EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES_IVR.MFP_ELIMINAR';
    
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_table_cur(pn_idenvio, pn_secuencia);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodatadeleted;
    END IF;
  
    DELETE FROM mf_envio_ejecuciones_ivr
     WHERE idenvio = pn_idenvio
       AND secuencia = pn_secuencia;
    CLOSE c_table_cur;
  
  EXCEPTION 
    WHEN le_nodatadeleted THEN
      pv_msg_error := 'El registro que desea borrar no existe. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite eliminar el envio de ejecuciones en la tabla
  **               MF_ENVIO_EJECUCIONES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_eliminacion_fisica(pn_idenvio   IN mf_envio_ejecuciones_ivr.idenvio%TYPE,
                                   pn_secuencia IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                   pv_msg_error IN OUT VARCHAR2) AS
  
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES_IVR.MFP_ELIMINACION_FISICA';
    
  BEGIN
  
    DELETE FROM mf_envio_ejecuciones_ivr
     WHERE idenvio = pn_idenvio
       AND secuencia = pn_secuencia;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || substr(SQLERRM, 1, 250) || '. ' || lv_aplicacion;
  END;

END MFK_OBJ_ENVIO_EJECUCIONES_IVR;
/
