CREATE OR REPLACE PACKAGE RC_TRX_UTILITIES IS


FUNCTION FCT_RETORNA_PARAMETRO (Pn_IdTipoParametro      NUMBER,
                                Pv_IdParametro          VARCHAR2,
                                Pv_Error            OUT VARCHAR2) RETURN VARCHAR2;

PROCEDURE PRC_REP_CUSTOMER_SUSP(Pv_Error OUT VARCHAR2);

PROCEDURE PRC_GENERA_REPORTE_SUSP(Pv_Error OUT VARCHAR2);

FUNCTION FCT_ENVIA_CORREO(PV_NOMBRE_ARCH IN VARCHAR2,
                          Pv_Error           OUT VARCHAR2)RETURN VARCHAR2;

PROCEDURE SCF_VALOR_PARAMETRO_REPORT(Pv_Cadena_Parametros   IN VARCHAR2,
                                     Pv_Parametro           IN VARCHAR2,
                                     Pv_param_a             IN VARCHAR2,      
                                     Pv_param_b             IN VARCHAR2,
                                     PV_DES_ERROR          OUT VARCHAR2,
                                     PV_VALOR_ERROR        OUT VARCHAR2,
                                     PV_ERROR              OUT VARCHAR2);

PROCEDURE PRC_DATOS_CTA(Pv_Cuenta            VARCHAR2,
                        Pv_Servicio          VARCHAR2,
                        Pn_CustomerId    OUT NUMBER,
                        Pv_CstraDecode   OUT VARCHAR2,
                        Pd_FechaPeriodo  OUT DATE,
                        Pv_StatusSup     OUT VARCHAR2,
                        Pv_Reason        OUT VARCHAR2,
                        Pn_SaldoReal     OUT NUMBER,
                        Pn_Deuda         OUT NUMBER,
                        Pn_SaldoImpago   OUT NUMBER,
                        Pv_Error         OUT VARCHAR2);

PROCEDURE RC_NOTIFICACIONES(PN_ID_PROCESO       IN NUMBER DEFAULT 0,
                            PV_JOB              IN VARCHAR2 DEFAULT NULL,
                            PV_SHELL            IN VARCHAR2 DEFAULT NULL,
                            PV_OBJETO           IN VARCHAR2 DEFAULT NULL,
                            PV_MENSAJE_NOTIF    IN VARCHAR2,
                            PV_ASUNTO           IN VARCHAR2,
                            Pv_RutaRemota       IN VARCHAR2 DEFAULT NULL,
                            PV_IdDestinatario   IN VARCHAR2,
                            PV_TipoNotificacion IN VARCHAR2,
                            PN_ERROR           OUT NUMBER,
                            PV_ERROR           OUT VARCHAR2);

PROCEDURE PRC_DESCARTA_MALA_VENTA(PV_CUENTA           VARCHAR2 DEFAULT NULL,
                                  PN_REGION           NUMBER DEFAULT NULL,
                                  PV_FECH_MAX_PAGO    VARCHAR2 DEFAULT NULL,
                                  PV_FECH_MAX_JUSTICA VARCHAR2 DEFAULT NULL,
                                  PN_COD_VENDEDOR     NUMBER DEFAULT NULL,
                                  PV_VENDEDOR         VARCHAR2 DEFAULT NULL,
                                  PV_USUARIO          VARCHAR2 DEFAULT NULL,
                                  PV_ERROR            OUT VARCHAR2);

FUNCTION FCT_MENSAJE_NOTIFIC_RELOJ(Pv_Mensaje        VARCHAR2,
                                   Pv_Proceso        VARCHAR2,
                                   Pv_Error      OUT VARCHAR2) RETURN VARCHAR2 ;

PROCEDURE PRC_REPORTE_INACTIVACIONES (Pv_Mensaje  OUT VARCHAR2,
                                      Pv_Error    OUT VARCHAR2);

END RC_TRX_UTILITIES;
/
CREATE OR REPLACE PACKAGE BODY RC_TRX_UTILITIES IS

--===============================================================================--
-- Creado por: Andres Balladares
-- Fecha:      28/06/2016
-- PROYECTO:   [10695] Mejoras al proceso de Reloj de Cobranzas
-- LIDER IRO:  IRO Juan Romero
-- LIDER :     SIS Antonio Mayorga
-- MOTIVO:     Envio de reporte de los clientes proximos a suspenderse
--=====================================================================================--
--===============================================================================--
-- Modificado por: Andres Balladares
-- Fecha:      30/06/2016
-- PROYECTO:   [10695] Mejoras al proceso de Reloj de Cobranzas
-- LIDER IRO:  IRO Juan Romero
-- LIDER :     SIS Antonio Mayorga
-- MOTIVO:     Validacion de la fecha fin de ciclo para las cuentas proximas a suspenderse
--=====================================================================================--
--===============================================================================--
-- Creado por: Kevin Murillo
-- Fecha:      15/08/2016
-- PROYECTO:   [10995] Equipo Cambios Continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:  IRO Juan Romero
-- LIDER :     SIS Antonio Mayorga
-- MOTIVO:     Creacion del Procedimiento Prc_genera_reporte_susp y la funcion FCT_ENVIA_CORREO, los cuales son los encargados 
--             de generar el reporte de las suspenciones realizadas diariamente.
--=====================================================================================--
--==================================================================================================--
-- Modificado por: Jordan Rodriguez
-- Fecha:      29/09/2016
-- PROYECTO:   [10995] Mejoras al proceso de Reloj de Cobranzas
-- LIDER IRO:  IRO Juan Romero
-- LIDER :     SIS Antonio Mayorga
-- MOTIVO:     Creacion del procedimiento RC_NOTIFICACIONES para el envio de notificaciones via mail
--==================================================================================================--
--==================================================================================================--
-- Modificado por: Anabell Amayquema
-- Fecha:      05/10/2016
-- PROYECTO:   [10995] Mejoras al proceso de Reloj de Cobranzas
-- LIDER IRO:  IRO Juan Romero
-- LIDER :     SIS Antonio Mayorga
-- MOTIVO:     Ajustes en las consultas del reporte de cuentas suspendidas.
--==================================================================================================--
/*================================================================================================
  -- Projecto       : [11477] Equipo Cambio Continuo Reloj de Cobranzas y Reactivacion
  -- Creado por     : IRO Jordan Rodriguez A.
  -- Fecha          : 11/08/2017
  -- Lider IRO      : IRO Juan Romero
  -- Lider SIS      : Antonio Mayorga
  -- Motivo         : Se creo el procedimiento PRC_DESCARTA_MALA_VENTA el cual se encargará de 
  --                  actualizar las tablas PORTA.GC_CUENTAS_C1_R1, PORTA.GC_CUENTAS_C1_R2,
  --                  con la finalidad de descartar las cuentas las cuales no se les debe aplicar
  --                  la mala venta o asignar la fecha de ejecucion del proceso de mala venta
  --===============================================================================================*/
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      09/07/2018
-- PROYECTO:       [11922] Equipo Ágil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Notificaciones del proceso de suspensión en fecha de corte.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      30/07/2018
-- PROYECTO:       [12018] Equipo Ágil Procesos Cobranzas.
-- LIDER IRO:      IRO Nery Amayquema
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Notificaciones del proceso de inactivacion de reloj y cambio en la fecha de inactivacion.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      14/01/2019
-- PROYECTO:       [12151] One Reloj de cobranzas DBC 1.
-- LIDER HTS:      HTS Gloria Rojas
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Actualizacion notificaciones reloj.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: David Hernandez Ch.
-- FECHA MOD:      04/02/2019
-- PROYECTO:       [12151] One Reloj de cobranzas DBC 1. Notificacion de Inactivaciones
-- LIDER HTS:      HTS Gloria Rojas
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Actualizacion notificaciones reloj por Categoria de SMA y DTH.
--=====================================================================================--

FUNCTION FCT_RETORNA_PARAMETRO (Pn_IdTipoParametro      NUMBER,
                                Pv_IdParametro          VARCHAR2,
                                Pv_Error            OUT VARCHAR2) RETURN VARCHAR2 IS

  CURSOR C_PARAMETROS IS
  SELECT P.VALOR
    FROM GV_PARAMETROS P
   WHERE P.ID_TIPO_PARAMETRO = Pn_IdTipoParametro
     AND P.ID_PARAMETRO = Pv_IdParametro;

  Lv_Valor  VARCHAR2(4000);
  Le_Error  EXCEPTION;

BEGIN

  OPEN C_PARAMETROS;
  FETCH C_PARAMETROS INTO Lv_Valor;
  CLOSE C_PARAMETROS;

  IF Lv_Valor IS NULL THEN
     Pv_Error := 'No se encontro configurado el ID_PARAMETRO: '||Pv_IdParametro||' en la tabla GV_PARAMETROS.';
     RAISE Le_Error;
  END IF;

  RETURN Lv_Valor;

EXCEPTION
  WHEN Le_Error THEN
    Pv_Error := 'FCT_RETORNA_PARAMETRO => '|| Pv_Error;
    RETURN NULL;
  WHEN OTHERS THEN
    Pv_Error := 'FCT_RETORNA_PARAMETRO => '|| SUBSTR(SQLERRM, 1, 300);
    RETURN NULL;
END FCT_RETORNA_PARAMETRO;

PROCEDURE PRC_REP_CUSTOMER_SUSP(Pv_Error   OUT VARCHAR2)IS

  CURSOR C_CUENTAS (Cd_Fecha DATE, Cn_IntFecha NUMBER, Cv_Dia VARCHAR2)IS
  SELECT DISTINCT C.CUSTOMER_ID, D.CSACTIVATED ANTIGUEDAD, D.CUSTCODE CUENTA, D.CSTRADECODE, D.LBC_DATE FECHA_CORTE
  FROM RELOJ.TTC_CONTRACTPLANNIG C, CUSTOMER_ALL D
 WHERE C.INSERTIONDATE BETWEEN Cd_Fecha - Cn_IntFecha AND Cd_Fecha
   AND C.CUSTOMER_ID = D.CUSTOMER_ID
   AND TO_CHAR(D.LBC_DATE,'DD') = Cv_Dia
   AND C.CO_STATUS_A IN ('34', '80');
  
  CURSOR C_DATOS_BSCS (Cn_CustomerId  NUMBER)IS
  SELECT D.DES PLAN
  FROM CONTRACT_ALL P, RATEPLAN D
 WHERE P.CUSTOMER_ID = Cn_CustomerId
   AND P.TMCODE = D.TMCODE;
  
  CURSOR C_TIPO_CLIENTE (Cv_CustCode VARCHAR2)IS
  SELECT D.NOMBRES, (D.APELLIDO1||' '||D.APELLIDO2) APELLIDOS, C.TIPO, C.PORCENTAJE, F.DESCRIPCION FORMA_PAGOS
    FROM PORTA.CL_PERSONAS_EMPRESA@AXIS A, PORTA.CL_CONTRATOS@AXIS B, PORTA.RC_PORCENTAJES_DEUDA@AXIS C, PORTA.CL_PERSONAS@AXIS D, PORTA.GE_FORMA_PAGOS@AXIS F
   WHERE B.CODIGO_DOC = Cv_CustCode
     AND B.ID_PERSONA = A.ID_PERSONA
     AND A.ID_PERSONA = D.ID_PERSONA
     AND B.ID_PERSONA = D.ID_PERSONA
     AND B.ID_FORMA_PAGO = F.ID_FORMA_PAGO
     AND C.ID_CLASE_CLIENTE =
         NVL((SELECT X.ID_CLASE_CLIENTE FROM PORTA.RC_PORCENTAJES_DEUDA@AXIS X WHERE X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE),'GEN');
  
  Lr_TipoCliente     C_TIPO_CLIENTE%ROWTYPE;
  LF_OUTPUT          UTL_FILE.FILE_TYPE;
  Ld_Fecha           DATE;
  Ln_Saldo           NUMBER;
  Ln_UltFact         NUMBER;
  Ln_SaldoImp        NUMBER;
  Ln_IdSecuencia     NUMBER;
  Ln_Error           NUMBER;
  Ln_Count           NUMBER :=0;
  Ln_IntFecha        NUMBER;
  Lv_Dia             VARCHAR2(2);
  Lv_DiaParamet      VARCHAR2(50);
  Lv_Plan            VARCHAR2(32000);
  Lv_TipoCliente     VARCHAR2(30);
  Lv_Directorio      VARCHAR2(50);
  Lv_NombreArch      VARCHAR2(100);
  Lv_UsuFinan27      VARCHAR2(50);
  Lv_IpFinan27       VARCHAR2(50);
  Lv_RutaRemota      VARCHAR2(1000);
  Lv_Asunto          VARCHAR2(600);
  Lv_Body            VARCHAR2(6000);
  Lv_Remitente       VARCHAR2(1000);
  Lv_Destinatarios   VARCHAR2(4000);
  Lv_Puerto          VARCHAR2(50);
  Lv_Servidor        VARCHAR2(50);
  Lv_ErrorFtp        VARCHAR2(1000);
  Lv_Fecha           varchar2(12);
  Le_Error           EXCEPTION;
  LE_EJECUCION       EXCEPTION;
  
BEGIN
  
  Ld_Fecha := SYSDATE+2;
  --Ln_Dia := SUBSTR(to_char(Ld_Fecha,'dd/mm/rrrr'),1,2);
  Lv_Dia := to_char(Ld_Fecha,'dd');
  Lv_DiaParamet := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_PARAT_DIAS',Pv_Error);
  
  IF INSTR(Lv_DiaParamet,'|'||Lv_Dia||'|') > 0 THEN

  Lv_Fecha := TO_CHAR((Ld_Fecha - INTERVAL '1' MONTH),'DD/MM/YYYY');
  Ld_Fecha := TO_DATE(Lv_Fecha,'DD/MM/YYYY');
  
  Ln_IntFecha := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_INTERVAL_FECH',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_Directorio := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_NOM_DIRE',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_NombreArch := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_NOM_ARCH',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_UsuFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_USU_FIN',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_IpFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_IP_FIN',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_RutaRemota := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_RUT_REPO',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_Asunto := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_ASU_MAIL',Pv_Error);
  
  Lv_Asunto := Lv_Asunto||to_char(SYSDATE+2,'DDMMYYYY');

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_BODY_MAIL',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_Remitente := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_REM_MAIL',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_Destinatarios := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_DEST_MAIL',Pv_Error);

  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;

  Lv_NombreArch := Lv_NombreArch||to_char(SYSDATE+2,'DDMMYYYY')||'.xls';

  FOR I IN C_CUENTAS(Ld_Fecha, Ln_IntFecha, Lv_Dia) LOOP
      
      Ln_Count := Ln_Count +1;
      Lv_Plan := NULL;
      
      IF Ln_Count = 1 THEN
         LF_OUTPUT  := UTL_FILE.FOPEN(Lv_Directorio, Lv_NombreArch, 'W');
         UTL_FILE.PUT_LINE(LF_OUTPUT,'ANTIGUEDAD'||CHR(9)||'CUENTA'||CHR(9)||'NOMBRES'||CHR(9)||'APELLIDOS'||CHR(9)||'TIPO CLIENTES'||CHR(9)||
                                     'FORMA PAGO'||CHR(9)||'SALDO'||CHR(9)||'PLAN'||CHR(9)||'FECHA CORTE'||CHR(9)||'VALOR FACTURA VENCIDA'||CHR(9)||
                                     'FECHA SUSPENSION');
      END IF;
      
      FOR J IN C_DATOS_BSCS(I.CUSTOMER_ID) LOOP
          
          IF Lv_Plan IS NULL THEN
             Lv_Plan :='|'||J.PLAN||'|';
          ELSIF INSTR(Lv_Plan,'|'||J.PLAN||'|') = 0 THEN
             Lv_Plan :=Lv_Plan||J.PLAN||'|';
          END iF;
          
      END LOOP;
      
      RELOJ.RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_SALDO_REAL_CTA_MENS(I.CUENTA,100,Ln_Saldo,Ln_UltFact,Ln_SaldoImp);
      
      OPEN C_TIPO_CLIENTE(I.CUENTA);
      FETCH C_TIPO_CLIENTE INTO Lr_TipoCliente;
      CLOSE C_TIPO_CLIENTE;
      
      IF I.CSTRADECODE = 18 AND Lr_TipoCliente.TIPO <> 'V' THEN
         Lv_TipoCliente := 'INMEDIATO';
      ELSE
         IF Lr_TipoCliente.TIPO = 'V' THEN
            IF Lr_TipoCliente.PORCENTAJE = 100 THEN
               Lv_TipoCliente := 'INSIGNIA/INSI_REL';
            ELSE
               Lv_TipoCliente := 'VIP';
            END IF;
         ELSIF Lr_TipoCliente.TIPO = 'C' THEN
            Lv_TipoCliente := 'COORPORATIVOS/PYMENS';
         ELSE
            Lv_TipoCliente := 'NORMAL';
         END IF;
      END IF;
      
      UTL_FILE.PUT_LINE(LF_OUTPUT,I.ANTIGUEDAD||CHR(9)||I.CUENTA||CHR(9)||Lr_TipoCliente.NOMBRES||CHR(9)||
                                  Lr_TipoCliente.APELLIDOS||CHR(9)||Lv_TipoCliente||CHR(9)||Lr_TipoCliente.FORMA_PAGOS||CHR(9)||Ln_Saldo||CHR(9)||
                                  Lv_Plan||CHR(9)||I.FECHA_CORTE||CHR(9)||Ln_UltFact||CHR(9)||Ld_Fecha);
      
  END LOOP;

  IF Ln_Count > 0 THEN
     
     UTL_FILE.FCLOSE(LF_OUTPUT);
     --Traspaso de archivo a ruta de finan27
     scp_dat.sck_ftp_gtw.scp_transfiere_archivo(pv_ip           => Lv_IpFinan27,
                                                pv_usuario      => Lv_UsuFinan27,
                                                pv_ruta_remota  => Lv_RutaRemota,
                                                pv_nombre_arch  => Lv_NombreArch,
                                                pv_ruta_local   => Lv_Directorio,
                                                pv_borrar_arch_local => 'N',
                                                pn_error        => Ln_Error,
                                                pv_error        => Lv_ErrorFtp);
     
     IF Lv_ErrorFtp IS NULL THEN
        
        Lv_RutaRemota := '</ARCHIVO1='||Lv_NombreArch||'|DIRECTORIO1='||Lv_RutaRemota||'|/>';
        
        --Envio del mail en caso de exito
        Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar (pv_nombresatelite  => 'PRC_REP_CUSTOMER_SUSP',
                                                               pd_fechaenvio      => SYSDATE,
                                                               pd_fechaexpiracion => SYSDATE+1,
                                                               pv_asunto          => Lv_Asunto,
                                                               pv_mensaje         => Lv_Body||' '||Lv_RutaRemota,
                                                               pv_destinatario    => Lv_Destinatarios,
                                                               pv_remitente       => Lv_Remitente,
                                                               pv_tiporegistro    => 'A',
                                                               pv_clase           => 'RCS',
                                                               pv_puerto          => Lv_Puerto,
                                                               pv_servidor        => Lv_Servidor,
                                                               pv_maxintentos     => 5,
                                                               pv_idusuario       => 'RELOJ',
                                                               pv_mensajeretorno  => PV_ERROR,
                                                               pn_idsecuencia     => Ln_IdSecuencia);
        
     ELSE
        
        Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_ERROR_BODY',Pv_Error);
                
     END IF;
     
  ELSE
     
     Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,'REP_VACIO_BODY',Pv_Error);
     
  END IF;
  
  IF Ln_Count = 0 OR Lv_ErrorFtp IS NOT NULL THEN
     --Envio del mail cuando no se obtuvo registros de suspension
     Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar (pv_nombresatelite  => 'PRC_REP_CUSTOMER_SUSP',
                                                            pd_fechaenvio      => SYSDATE,
                                                            pd_fechaexpiracion => SYSDATE+1,
                                                            pv_asunto          => Lv_Asunto,
                                                            pv_mensaje         => Lv_Body,
                                                            pv_destinatario    => Lv_Destinatarios,
                                                            pv_remitente       => Lv_Remitente,
                                                            pv_tiporegistro    => 'M',
                                                            pv_clase           => 'RCS',
                                                            pv_puerto          => Lv_Puerto,
                                                            pv_servidor        => Lv_Servidor,
                                                            pv_maxintentos     => 5,
                                                            pv_idusuario       => 'RELOJ',
                                                            pv_mensajeretorno  => PV_ERROR,
                                                            pn_idsecuencia     => Ln_IdSecuencia);
     
  END IF;
  
  IF PV_ERROR IS NOT NULL THEN
     PV_ERROR := 'Error en el envio del mail: '||Ln_Error||'-'||PV_ERROR;
     RAISE Le_Error;
  END IF;
  
  AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  COMMIT;
  
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    Pv_Error := 'ERROR: PRC_REP_CUSTOMER_SUSP => '|| Pv_Error;
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    ROLLBACK;
  WHEN OTHERS THEN
    Pv_Error := 'ERROR: PRC_REP_CUSTOMER_SUSP => '|| SUBSTR(SQLERRM, 1, 300);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    ROLLBACK;
END PRC_REP_CUSTOMER_SUSP;

PROCEDURE PRC_GENERA_REPORTE_SUSP(Pv_Error OUT VARCHAR2) IS

  LF_OUTPUT     UTL_FILE.FILE_TYPE;
  Lv_Directorio VARCHAR2(100);
  Lv_NombreArch VARCHAR2(100);
  LE_FINALIZA EXCEPTION;
  LN_DEUDA_TOTAL   NUMBER := 0;
  LV_TIPO_SERVICIO VARCHAR2(20);
  LV_OBSERVACION   VARCHAR2(32760);
  LN_SALDO_IMPAGO  NUMBER;
  LV_GEN_ESTADO    VARCHAR2(32677);
  lv_email         VARCHAR2(10000);
  LV_ERROR_CONFIG  VARCHAR2(32760);
  LV_DES_ERROR     VARCHAR2(1000);
  LV_VALOR_ERROR   VARCHAR2(1000);
  CONT_ERROR       NUMBER := 1;
  LV_PARAM_A       VARCHAR2(10) := '|';
  LV_PARAM_B       VARCHAR2(10) := ';';
  LV_ERROR         VARCHAR2(10);
  BAND_EJEC        VARCHAR2(10);
  LV_MAX_COMMIT    VARCHAR2(1000);
  LV_EJECUCION     VARCHAR2(1000);
  CONT_EJECUCION   NUMBER:=0;
  LR_Error         EXCEPTION;
  LE_NO_DATOS      EXCEPTION;
  LN_EJECUCION     NUMBER:=0; --IRO KMU INI/FIN 22/09/2016
  LN_CONT_SUSP     NUMBER:=0;
  LB_CONTINUA      BOOLEAN:=TRUE;  --IRO KMU FIN 22/09/2016

  CURSOR C_DATOS_PERSONA(CV_CUSTOMER_ID VARCHAR2) IS
    SELECT T.*
      FROM CCONTACT_ALL T
     WHERE CUSTOMER_ID = CV_CUSTOMER_ID
       AND T.CCSEQ = (select f.CCSEQ
                        from (SELECT L.CCSEQ
                                FROM CCONTACT_ALL L
                               WHERE CUSTOMER_ID = CV_CUSTOMER_ID
                               order by l.CCSEQ desc) f
                       where rownum = 1);
  LC_DATOS_PERSONA C_DATOS_PERSONA%ROWTYPE;

  CURSOR C_VERIFICA_SUSPENSION IS
 SELECT T.CUSTCODE          CUENTA,
        T.CO_STATUS_A,
        T.DN_NUM            SERVICIO,
        T.CUSTOMER_ID,
        T.CO_ID,
        T.ADMINCYCLE,
        T.CH_TIMEACCIONDATE,
        R.DN_NUM,
        R.CO_REMARKTRX,
        R.CUSTCODE,
        R.CO_COMMANDTRX_3,
        R.CO_STATUSTRX,
        R.INSERTIONDATE
   FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS  T,
        RELOJ.TTC_PROCESS_CONTRACTHISTORY R
  WHERE T.CH_TIMEACCIONDATE BETWEEN TO_DATE(SYSDATE - 1, 'DD/MM/RRRR') AND
        TO_DATE(SYSDATE, 'DD/MM/RRRR')
    AND T.CO_STATUSTRX <> 'D'
    AND T.CO_STATUSTRX <> 'A'
    AND T.CO_STATUSTRX <> 'L'
    AND T.CO_STATUS_A <> '90'
    AND T.LASTMODDATE BETWEEN
        TO_DATE(SYSDATE || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
        SYSDATE
    AND R.LASTMODDATE(+) BETWEEN
        TO_DATE(SYSDATE || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
        SYSDATE
    AND R.CUSTOMER_ID(+) = T.CUSTOMER_ID
    AND R.CO_ID(+) = T.CO_ID
    AND R.DN_NUM(+) = T.DN_NUM
    AND R.CUSTCODE(+) = T.CUSTCODE;

  CURSOR C_BAND_EJEC(CV_PARAMETRO VARCHAR2, CN_ID_PARAMETRO NUMBER) IS
    SELECT VALOR
      FROM GV_PARAMETROS
     WHERE ID_TIPO_PARAMETRO = CN_ID_PARAMETRO
       AND ID_PARAMETRO = CV_PARAMETRO;

begin

  Lv_Directorio := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                          'REP_DIRE_SUSP',
                                                          Pv_Error);

  IF Pv_Error IS NOT NULL THEN
    Lv_Directorio := 'CMS_MASIVO';
  END IF;
  Lv_NombreArch := 'REPORTE_RESULTADO_SUSPENSION' ||
                   to_char(SYSDATE, 'DDMMYYYY') || '.CSV';


  LV_ERROR_CONFIG := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                            'ERROR_CONFIG',
                                                            Pv_Error);
  LV_MAX_COMMIT   := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                            'MAX_COMMIT',
                                                            Pv_Error);
  LV_EJECUCION := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                             'NUM_EJECU',
                                                             Pv_Error);

   
    LF_OUTPUT := UTL_FILE.FOPEN(Lv_Directorio,
                                Lv_NombreArch,
                                'W');
  
      UTL_FILE.PUT_LINE(LF_OUTPUT,
                        'CUENTA,' || 'SERVICIO,' || 
                        'CO_ID,' ||'TIPO_SERVICIO,' ||
                        'CICLO,' ||  'LOCALIDAD,' || 
                        'APELLIDOS,' ||'NOMBRES,' ||
                        'RUC/CEDULA,' ||  'TOTAL_DEUDA,' ||
                        'STATUS_RELOJ,' || 'FECHA_PLANIFICACION,' ||
                         'FECHA_PROCESADA,' || 
                        'RESULTADO');
  
  
    FOR J IN C_VERIFICA_SUSPENSION LOOP
      
          
      IF J.DN_NUM IS NULL THEN
        J.CO_STATUSTRX := 'P';
        LV_OBSERVACION := 'PENDIENTE';
        LV_GEN_ESTADO  := LV_OBSERVACION;
        
        J.DN_NUM:=J.SERVICIO;
        
      ELSE

        IF J.CO_STATUSTRX = 'F' THEN
          LV_OBSERVACION := J.CO_REMARKTRX;

        ELSE
          J.CO_STATUSTRX := 'E';
          
          
          
          LV_OBSERVACION := case
                              when J.CO_REMARKTRX like '%estado%26%' then
                               J.CO_REMARKTRX
                              else
                               REGEXP_REPLACE(J.CO_REMARKTRX,
                                              '[^,a-z, A-Z,.,:,<,>,_,-]',
                                              'x')
                            end;
          LV_ERROR       := NULL;
          WHILE LV_ERROR IS NULL LOOP
          
            BEGIN
              RC_TRX_UTILITIES.SCF_VALOR_PARAMETRO_REPORT(LV_ERROR_CONFIG,
                                                            CONT_ERROR,
                                                            LV_PARAM_A,
                                                            LV_PARAM_B,
                                                            LV_DES_ERROR,
                                                            LV_VALOR_ERROR,
                                                            lV_ERROR);
            
              IF J.CO_REMARKTRX LIKE LV_DES_ERROR THEN
                LV_GEN_ESTADO := LV_VALOR_ERROR;
                EXIT;
              END IF;
              CONT_ERROR := CONT_ERROR + 1;
              IF lV_ERROR IS NOT NULL THEN
                LV_GEN_ESTADO := 'Error desconocido';
                EXIT;
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                LV_GEN_ESTADO := 'Error desconocido';
            END;
          END LOOP;
        END IF;
      END IF;
    
      CONT_ERROR := 1;
      
  
      
      IF J.DN_NUM LIKE '%DTH%' THEN
        LV_TIPO_SERVICIO := 'DTH';
      ELSE
        LV_TIPO_SERVICIO := 'MOVIL';
      END IF;
    
    
          
      RELOJ.RC_API_TIMETOCASH_RULE_BSCS.RC_RETORNA_SALDO_REAL(PN_CUSTOMER_ID   => J.CUSTOMER_ID,
                                                              PN_SALDO_MIN_POR => 100,
                                                              PN_SALDOREAL     => LN_DEUDA_TOTAL,
                                                              PN_SALDO_IMPAGO  => LN_SALDO_IMPAGO);
    
      OPEN C_DATOS_PERSONA(J.CUSTOMER_ID);
      FETCH C_DATOS_PERSONA
        INTO LC_DATOS_PERSONA;
      CLOSE C_DATOS_PERSONA;
     
    
               
      UTL_FILE.PUT_LINE(LF_OUTPUT,
                        J.CUENTA ||',' || J.DN_NUM || ',' || J.CO_ID ||
                       ',' || LV_TIPO_SERVICIO || ',' ||
                        J.ADMINCYCLE || ',' || LC_DATOS_PERSONA.CCCITY ||
                        ',' || LC_DATOS_PERSONA.CCLNAME || ',' ||
                        LC_DATOS_PERSONA.CCNAME || ',' ||
                        LC_DATOS_PERSONA.CSSOCIALSECNO ||',' ||
                        LN_DEUDA_TOTAL || ',' || J.CO_STATUS_A ||',' ||
                        J.CH_TIMEACCIONDATE || ',' || J.INSERTIONDATE ||
                        ','|| LV_OBSERVACION);
 
     
          BEGIN --23/09/2016 
          INSERT INTO RELOJ.TTC_SUSPENSIONES_PENDIENTES(CUENTA,
                                                 CO_ID,
                                                 SERVICIO,
                                                 CH_TIMEACCIONDATE,
                                                 CUSTOMER_ID,
                                                 STATUS_A,
                                                 REASON,
                                                 ADMINCYCLE,
                                                 STATUSTRX,
                                                 VERIFICADO,
                                                 TIPO_SERVICIO,
                                                 OBSERVACION)
                                          VALUES(J.CUENTA,
                                                 NULL,
                                                 J.DN_NUM,
                                                 NULL,
                                                 NULL,
                                                 J.CO_STATUS_A,
                                                 NULL,
                                                 NULL,
                                                 J.CO_STATUSTRX,
                                                 NULL,
                                                 LV_TIPO_SERVICIO,
                                                 LV_GEN_ESTADO);
           LN_CONT_SUSP:=LN_CONT_SUSP+1;
           
           IF LN_CONT_SUSP >=LV_MAX_COMMIT THEN
              LN_CONT_SUSP:=0;
              COMMIT;
           END IF;
         EXCEPTION
           WHEN OTHERS THEN NULL;
         END;
       
      UTL_FILE.fflush(LF_OUTPUT);
      
          LN_EJECUCION:=LN_EJECUCION+1;
      
      IF LN_EJECUCION>= LV_EJECUCION THEN
          OPEN C_BAND_EJEC('BAND_EJECU_REP', 10995);
        FETCH C_BAND_EJEC
          INTO BAND_EJEC;
        CLOSE C_BAND_EJEC;
        IF NVL(BAND_EJEC, 'N') = 'N' THEN
          LB_CONTINUA := FALSE;
          EXIT;
        END IF;
      END IF;
      
       IF LB_CONTINUA = FALSE THEN
        EXIT;
      END IF;
    CONT_EJECUCION:=CONT_EJECUCION+1;
    END LOOP;
    COMMIT;
    UTL_FILE.FCLOSE(LF_OUTPUT);

    
    IF CONT_EJECUCION=0 THEN
      PV_ERROR:='ERROR NO EXISTE DATA A PROCESARSE PARA EL REPORTE DE SUSPENSION';
     ELSIF LB_CONTINUA=FALSE THEN
     
     PV_ERROR:='ERROR PROCESO ABORTADO MANUALMENTE';
     ELSE
     lv_email := RC_TRX_UTILITIES.FCT_ENVIA_CORREO(PV_NOMBRE_ARCH => Lv_NombreArch,
                                                  Pv_Error       => Pv_Error);
     END IF;
EXCEPTION
  WHEN LE_NO_DATOS THEN
    PV_ERROR:=NULL;  
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 100);
  
end Prc_genera_reporte_susp;

FUNCTION FCT_ENVIA_CORREO(PV_NOMBRE_ARCH IN VARCHAR2,
                          Pv_Error       OUT VARCHAR2) RETURN VARCHAR2 IS

  Lv_Asunto        VARCHAR2(6000);
  Lv_Body          VARCHAR2(6000);
  Lv_Remitente     VARCHAR2(1000);
  Lv_Destinatarios VARCHAR2(4000);
  Lv_Puerto        VARCHAR2(50) := NULL;
  Lv_Servidor      VARCHAR2(50) := NULL;
  Le_Error EXCEPTION;
  Ln_Error       NUMBER;
  Ln_IdSecuencia NUMBER;

  Lv_mens_correo      VARCHAR(32760);
  Lv_mens_correo_susp VARCHAR(32760);
  LV_NO_SUSP_DTH      VARCHAR2(32760);
  LV_NO_DTH_BODY      VARCHAR2(32760);
  LV_SUSP_MOVIL       VARCHAR2(32760);
  LV_NO_SUSP_MOVIL    VARCHAR2(32760);
  LV_SUS_MOVIL_BODY   VARCHAR2(32760);
  LV_NO_SUSP_BODY     VARCHAR2(32760);
  LV_SUSP_DTH         VARCHAR2(32760);
  LV_SUS_DTH_BODY     VARCHAR2(32760);
  SQL_DELETE          VARCHAR2(1000);

  CURSOR C_GLOBAL_SUS IS
    SELECT STATUS_A, STATUSTRX, TIPO_SERVICIO, COUNT(*) CANTIDAD
      FROM RELOJ.TTC_SUSPENSIONES_PENDIENTES T
     WHERE STATUSTRX = 'F'
     GROUP BY STATUS_A, STATUSTRX, TIPO_SERVICIO;

  CURSOR C_GLOBAL_NO_SUS IS
    SELECT STATUS_A,
           STATUSTRX,
           TIPO_SERVICIO,
           OBSERVACION,
           COUNT(*) CANTIDAD
      FROM RELOJ.TTC_SUSPENSIONES_PENDIENTES T
     WHERE STATUSTRX IN ('E', 'P')
     GROUP BY STATUS_A, STATUSTRX, TIPO_SERVICIO, OBSERVACION;

BEGIN
  Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                    'REP_MENS_SUSP',
                                                    Pv_Error);

  IF Pv_Error IS NOT NULL THEN
    lv_Body := 'Estimados.
Se adjunta resultado global del procesamiento de las suspensiones para el dia de hoy. Para consultar el detalle de estas suspensiones, por favor consultar el archivo "' ||
               PV_NOMBRE_ARCH || '" que se encuentra en la ruta en BSCS /home/gsioper/RELOJ_COBRANZA/TTC/DAT.

';
  END IF;
  lv_Body := Replace(Lv_Body, '<<nombre_archivo>>', PV_NOMBRE_ARCH);

  Lv_Asunto := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                      'REP_ASUN_SUSP',
                                                      Pv_Error);
  IF Pv_Error IS NOT NULL THEN
    Lv_Asunto := 'REPORTE DE CLIENTES SUSPENDIDOS EN EL DIA ';
  END IF;
  Lv_Asunto := Lv_Asunto || to_char(SYSDATE, 'DDMMYYYY');

  Lv_Destinatarios := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                             'REP_DEST_SUSP',
                                                             Pv_Error);
  IF Pv_Error IS NOT NULL THEN
    Lv_Destinatarios := 'Amayorga@claro.com.ec//nery.amayquema@iroute.com.ec;juan.romero@iroute.com.ec;';
  END IF;
  Lv_Remitente := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                         'REP_REMI_SUSP',
                                                         Pv_Error);
  IF Pv_Error IS NOT NULL THEN
    Lv_Remitente := 'reloj_cobranza@claro.com.ec';
  END IF;

  FOR I IN C_GLOBAL_NO_SUS LOOP
  
    IF I.TIPO_SERVICIO = 'DTH' THEN
      LV_NO_SUSP_DTH := I.TIPO_SERVICIO || CHR(9) || CHR(9) || CHR(9) ||
                        I.STATUS_A || CHR(9) || CHR(9) || I.CANTIDAD ||
                        CHR(9) || CHR(9) || I.OBSERVACION;
      LV_NO_DTH_BODY := LV_NO_DTH_BODY || LV_NO_SUSP_DTH || CHR(13);
    ELSE
      LV_NO_SUSP_MOVIL := I.TIPO_SERVICIO || CHR(9) || CHR(9) || CHR(9) ||
                          I.STATUS_A || CHR(9) || CHR(9) || I.CANTIDAD ||
                          CHR(9) || CHR(9) || I.OBSERVACION;
      LV_NO_SUSP_BODY  := LV_NO_SUSP_BODY || LV_NO_SUSP_MOVIL || CHR(13);
    END IF;
  
    Lv_mens_correo := CHR(9) || CHR(9) || 'TOTAL DE CUENTAS NO SUSPENDIDAS' ||
                      CHR(13) || 'TIPO_SERVICIO' || CHR(9) || 'ESTATUS' ||
                      CHR(9) || 'CANTIDAD' || CHR(9) || 'ERROR' || CHR(13) ||
                      LV_NO_SUSP_BODY || LV_NO_DTH_BODY || CHR(13) ||
                      CHR(13) || 'Saludos Cordiales.';
  
  END LOOP;

  FOR I IN C_GLOBAL_SUS LOOP
    IF I.TIPO_SERVICIO = 'DTH' THEN
      LV_SUSP_DTH     := I.TIPO_SERVICIO || CHR(9) || CHR(9) || CHR(9) ||
                         I.STATUS_A || CHR(9) || CHR(9) || I.CANTIDAD;
      LV_SUS_DTH_BODY := LV_SUS_DTH_BODY || CHR(9) || LV_SUSP_DTH ||
                         CHR(13);
    ELSE
      LV_SUSP_MOVIL     := I.TIPO_SERVICIO || CHR(9) || CHR(9) || CHR(9) ||
                           I.STATUS_A || CHR(9) || CHR(9) || I.CANTIDAD;
      LV_SUS_MOVIL_BODY := LV_SUS_MOVIL_BODY || LV_SUSP_MOVIL || CHR(13);
    END IF;
  
    Lv_mens_correo_susp := CHR(9) || CHR(9) ||
                           'TOTAL DE CUENTAS SUSPENDIDAS' || CHR(13) ||
                           'TIPO_SERVICIO' || CHR(9) || 'ESTATUS' || CHR(9) ||
                           'CANTIDAD' || CHR(13) || LV_SUS_MOVIL_BODY ||
                           LV_SUS_DTH_BODY||CHR(13);
  
  END LOOP;
  
  SQL_DELETE := 'TRUNCATE TABLE RELOJ.TTC_SUSPENSIONES_PENDIENTES';
  EXECUTE IMMEDIATE SQL_DELETE;
  COMMIT;
  Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'PRC_GENERA_REPORTE_SUSP',
                                                        pd_fechaenvio      => SYSDATE,
                                                        pd_fechaexpiracion => SYSDATE + 1,
                                                        pv_asunto          => Lv_Asunto,
                                                        pv_mensaje         => Lv_Body ||
                                                                              Lv_mens_correo_susp ||
                                                                              Lv_mens_correo,
                                                        pv_destinatario    => Lv_Destinatarios,
                                                        pv_remitente       => Lv_Remitente,
                                                        pv_tiporegistro    => 'M',
                                                        pv_clase           => 'RSD',
                                                        pv_puerto          => Lv_Puerto,
                                                        pv_servidor        => Lv_Servidor,
                                                        pv_maxintentos     => 5,
                                                        pv_idusuario       => 'RELOJ',
                                                        pv_mensajeretorno  => PV_ERROR,
                                                        pn_idsecuencia     => Ln_IdSecuencia);

  IF PV_ERROR IS NOT NULL THEN
    PV_ERROR := 'Error en el envio del mail: ' || Ln_Error || '-' ||
                PV_ERROR;
  ELSE
    PV_ERROR := NULL;
  END IF;
  RETURN PV_ERROR;

END FCT_ENVIA_CORREO;

PROCEDURE SCF_VALOR_PARAMETRO_REPORT(Pv_Cadena_Parametros IN VARCHAR2,
                                     Pv_Parametro         IN VARCHAR2,
                                     Pv_param_a           IN VARCHAR2,
                                     Pv_param_b           IN VARCHAR2,
                                     PV_DES_ERROR         OUT VARCHAR2,
                                     PV_VALOR_ERROR       OUT VARCHAR2,
                                     PV_ERROR             OUT VARCHAR2) IS

  Ln_Pos_Parametro     NUMBER;
  Ln_Pos_Igual         NUMBER;
  Ln_Pos_Pipe          NUMBER;
  Lv_Valor_Parametro   VARCHAR2(4000);
  Lv_error_report      VARCHAR2(4000);
  Lv_error_descripcion VARCHAR2(4000);

BEGIN

  Ln_Pos_Parametro := instr(Pv_Cadena_Parametros, pv_parametro);

  IF Ln_Pos_Parametro = 0 THEN
    PV_ERROR := 'NO ENCONTRADO';
  
  ELSE
  
    Ln_Pos_Pipe  := INSTR(Pv_Cadena_Parametros,
                          pv_param_a,
                          Ln_Pos_Parametro);
    Ln_Pos_Igual := INSTR(Pv_Cadena_Parametros,
                          pv_param_b,
                          Ln_Pos_Parametro);
  
    Lv_Valor_Parametro   := SUBSTR(Pv_Cadena_Parametros,
                                   Ln_Pos_Igual + 1,
                                   Ln_Pos_Pipe - Ln_Pos_Igual - 1);
    Ln_Pos_Parametro     := instr(Lv_Valor_Parametro, ';');
    Lv_error_report      := substr(Lv_Valor_Parametro,
                                   0,
                                   Ln_Pos_Parametro - 1);
    Lv_error_descripcion := substr(Lv_Valor_Parametro, Ln_Pos_Parametro + 1);
    PV_DES_ERROR         := Lv_error_report;
    PV_VALOR_ERROR       := Lv_error_descripcion;
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    null;
  
END SCF_VALOR_PARAMETRO_REPORT;


PROCEDURE PRC_DATOS_CTA(Pv_Cuenta            VARCHAR2,
                        Pv_Servicio          VARCHAR2,
                        Pn_CustomerId    OUT NUMBER,
                        Pv_CstraDecode   OUT VARCHAR2,
                        Pd_FechaPeriodo  OUT DATE,
                        Pv_StatusSup     OUT VARCHAR2,
                        Pv_Reason        OUT VARCHAR2,
                        Pn_SaldoReal     OUT NUMBER,
                        Pn_Deuda         OUT NUMBER,
                        Pn_SaldoImpago   OUT NUMBER,
                        Pv_Error         OUT VARCHAR2)IS
  
  CURSOR C_DATOS_DNID (Cv_DnNum VARCHAR2)IS
  SELECT D.DN_ID 
    FROM DIRECTORY_NUMBER D
   WHERE D.DN_NUM = Cv_DnNum
     AND D.DN_STATUS = 'a';
  
  CURSOR C_DATOS_BSCS_SRV (Cn_DnId NUMBER)IS
  SELECT C.CSTRADECODE, C.PRGCODE, NVL(C.CUSTOMER_ID_HIGH, C.CUSTOMER_ID) CUSTOMER_ID
    FROM CONTR_SERVICES_CAP X,
         CONTRACT_ALL S, CUSTOMER_ALL C
   WHERE X.DN_ID = Cn_DnId
     AND X.CS_DEACTIV_DATE IS NULL
     AND S.CO_ID = X.CO_ID
     AND C.CUSTOMER_ID = S.CUSTOMER_ID;
  
  CURSOR C_DATOS_BSCS_CTA IS
  SELECT C.CSTRADECODE, C.PRGCODE, NVL(C.CUSTOMER_ID_HIGH, C.CUSTOMER_ID) CUSTOMER_ID
    FROM CUSTOMER_ALL C
   WHERE C.CUSTCODE = Pv_Cuenta;

  CURSOR C_DATOS_PKID (Cv_CstraDecode VARCHAR2, Cv_PrgCode VARCHAR2)IS
  SELECT T.PK_ID 
    FROM RELOJ.TTC_WORKFLOW_CUSTOMER T
   WHERE T.CSTRADECODE = Cv_CstraDecode
     AND T.PRGCODE = Cv_PrgCode;

  CURSOR C_DATOS_SUSP (Cn_PkId NUMBER) IS
  SELECT Z.ST_STATUS_A, Z.ST_REASON
    FROM RELOJ.TTC_WORKFLOW_RELATION Y, RELOJ.TTC_WORKFLOW_STATUS_ALL Z
   WHERE Y.PK_ID = Cn_PkId
     AND Y.RE_ORDEN = 1
     AND Z.ST_ID = Y.ST_ID
     AND Z.ST_SEQNO = Y.ST_SEQNO;
  
  CURSOR C_FECHA_PERIODO (Cn_CustomerId NUMBER, Cd_Fecha DATE) IS
  SELECT F.OHREFDATE FECHA_PERIODO
    FROM (SELECT P.OHREFDATE 
            FROM ORDERHDR_ALL P 
           WHERE P.CUSTOMER_ID = Cn_CustomerId
             AND P.OHSTATUS = 'IN'
             AND P.OHDUEDATE < Cd_Fecha
           ORDER BY P.OHREFDATE DESC)F
   WHERE ROWNUM <= 1;
  
  Lr_DatosBscsCta       C_DATOS_BSCS_CTA%ROWTYPE;
  Lr_DatosSusp          C_DATOS_SUSP%ROWTYPE;
  Ln_PkId               NUMBER;
  Ln_DnId               NUMBER;
  Lv_DnNum              VARCHAR2(63);
  Lv_Fecha              VARCHAR2(20);
  Ld_Fecha              DATE;
  Le_Error              EXCEPTION;
  
BEGIN
  
  IF Pv_Cuenta IS NOT NULL THEN
     
     OPEN C_DATOS_BSCS_CTA;
     FETCH C_DATOS_BSCS_CTA INTO Lr_DatosBscsCta;
     CLOSE C_DATOS_BSCS_CTA;
     
  ELSIF Pv_Servicio IS NOT NULL THEN
     
     IF INSTR(Pv_Servicio,'DTH',1) > 0 THEN
        Pv_Error := 'El servicio es DTH debe de enviar la cuenta.';
        RAISE Le_Error;
     END IF; 
     
     Lv_DnNum := '5939'||Pv_Servicio;
     
     OPEN C_DATOS_DNID(Lv_DnNum);
     FETCH C_DATOS_DNID INTO Ln_DnId;
     CLOSE C_DATOS_DNID;
     
     IF Ln_DnId IS NULL THEN
        Pv_Error := 'No se pudo encontrar el DN_ID en la tabla DIRECTORY_NUMBER del servicio: '||Pv_Servicio;
        RAISE Le_Error;
     END IF;
     
     OPEN C_DATOS_BSCS_SRV(Ln_DnId);
     FETCH C_DATOS_BSCS_SRV INTO Lr_DatosBscsCta;
     CLOSE C_DATOS_BSCS_SRV;
     
  ELSE
     
     Pv_Error := 'Debe de enviar la cuenta o el servicio, los dos no pueden ser nulo.';
     RAISE Le_Error;
     
  END IF;
  
  OPEN C_DATOS_PKID(Lr_DatosBscsCta.CSTRADECODE, Lr_DatosBscsCta.PRGCODE);
  FETCH C_DATOS_PKID INTO Ln_PkId;
  CLOSE C_DATOS_PKID;
  
  IF Ln_PkId IS NULL THEN
     Pv_Error := 'No se encontro el PK_ID en la tabla TTC_WORKFLOW_CUSTOMER para el CustomerId: '||Lr_DatosBscsCta.CUSTOMER_ID;
     RAISE Le_Error;
  END IF;
  
  OPEN C_DATOS_SUSP(Ln_PkId);
  FETCH C_DATOS_SUSP INTO Lr_DatosSusp;
  CLOSE C_DATOS_SUSP;
  
  IF Lr_DatosSusp.ST_STATUS_A IS NULL THEN
     Pv_Error := 'No se encontro el estado de suspencion para el CustomerId: '||Lr_DatosBscsCta.CUSTOMER_ID;
     RAISE Le_Error;
  END IF;
  
  Pn_CustomerId := Lr_DatosBscsCta.CUSTOMER_ID;
  Pv_CstraDecode := Lr_DatosBscsCta.CSTRADECODE;
  Pv_StatusSup := Lr_DatosSusp.ST_STATUS_A;
  Pv_Reason := Lr_DatosSusp.ST_REASON;
  
  RELOJ.RC_Api_TimeToCash_Rule_Bscs.RC_RETORNA_SALDO_REAL_REACT(Lr_DatosBscsCta.CUSTOMER_ID,
                                                                100,
                                                                Pn_SaldoReal,
                                                                Pn_Deuda,
                                                                Pn_SaldoImpago);
  
  Lv_Fecha := TO_CHAR(SYSDATE,'DD/MM/YYYY');
  Ld_Fecha := TO_DATE(Lv_Fecha,'DD/MM/YYYY');
  
  OPEN C_FECHA_PERIODO(Lr_DatosBscsCta.CUSTOMER_ID, Ld_Fecha);
  FETCH C_FECHA_PERIODO INTO Pd_FechaPeriodo;
  CLOSE C_FECHA_PERIODO;
  
EXCEPTION
  WHEN Le_Error THEN
    Pv_Error := 'PRC_DATOS_CTA => '|| Pv_Error;
  WHEN OTHERS THEN
    Pv_Error := 'PRC_DATOS_CTA => '|| SUBSTR(SQLERRM, 1, 300);
END PRC_DATOS_CTA;

PROCEDURE RC_NOTIFICACIONES(PN_ID_PROCESO       IN NUMBER DEFAULT 0,
                            PV_JOB              IN VARCHAR2 DEFAULT NULL,
                            PV_SHELL            IN VARCHAR2 DEFAULT NULL,
                            PV_OBJETO           IN VARCHAR2 DEFAULT NULL,
                            PV_MENSAJE_NOTIF    IN VARCHAR2,
                            PV_ASUNTO           IN VARCHAR2,
                            Pv_RutaRemota       IN VARCHAR2 DEFAULT NULL,
                            PV_IdDestinatario   IN VARCHAR2,
                            PV_TipoNotificacion IN VARCHAR2,
                            PN_ERROR           OUT NUMBER,
                            PV_ERROR           OUT VARCHAR2) IS

  CURSOR C_DEVUELVE_MAILS(CV_DEST VARCHAR2, CV_MAIL VARCHAR2) IS
  SELECT M.MAIL
    FROM RC_MAILS M
   WHERE M.DESTINATARIO = CV_DEST
     AND M.MAIL LIKE '%' || CV_MAIL || '%'
     AND M.ESTADO = 'A';
  
  CURSOR C_PARAMETROS (Cn_IdTipoParametros NUMBER,
                       Cv_IdParametro      VARCHAR2)IS
  SELECT VALOR
    FROM SYSADM.GV_PARAMETROS
   WHERE ID_TIPO_PARAMETRO = Cn_IdTipoParametros
     AND ID_PARAMETRO = Cv_IdParametro;
  
  LV_PROGRAMA_SCP    VARCHAR2(200) := 'RC_TRX_UTILITIES.RC_NOTIFICACIONES';
  LV_UNIDAD_REGISTRO VARCHAR2(200) := 'BITACORIZAR';
  LN_ID_BITACORA_SCP NUMBER;
  LN_ERROR_SCP       NUMBER := 0;
  LV_ERROR_SCP       VARCHAR2(500);
  LV_MENSAJE         VARCHAR2(500);
  LV_MENSAJE_APL_SCP VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP VARCHAR2(10000);
  LV_MENSAJE_NOTIF   VARCHAR2(3000);
  
  LV_REMITENTE_MAIL   VARCHAR2(50) := 'reloj_cobranza@claro.com.ec';
  LV_MENSAJE_RETORNO  VARCHAR2(200);
  LN_ID_SECUENCIA     NUMBER;
  LV_PUERTO           VARCHAR2(6) := NULL;
  LN_RESULTADO_NOTIF  NUMBER;
  LV_DIR_PRINC_CORREO VARCHAR2(2000) := '';
  LV_DIR_SEC_CORREO   VARCHAR2(2000) := '';
  LV_DESTINATARIOS    varchar2(2000);
  
  Lv_Servidor        VARCHAR2(50);
  LN_ID_DETALLE      NUMBER := 0;
  Ln_MailProvedor       VARCHAR2(1000);
  
BEGIN
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('RC_NOTIFICACIONES_RELOJ',
                                            LV_PROGRAMA_SCP,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            0,
                                            LV_UNIDAD_REGISTRO,
                                            LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  IF LN_ERROR_SCP <> 0 THEN
     LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' || LV_ERROR_SCP;
  END IF;
  IF PV_TipoNotificacion = 'ERROR' THEN
    IF PV_OBJETO = 'FILE_SYSTEM' THEN
      LV_MENSAJE_NOTIF := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995, 'MENSAJE_FILE_SYSTEM', PV_ERROR);
      --INI [11922] 09/07/2018 IRO ABA PROCESO DE NOTIFICACIONES DEL PROCESO DE SUSPENSIÓN EN FECHA DE CORTE
      LV_MENSAJE_NOTIF := REPLACE(LV_MENSAJE_NOTIF,'<valor>',PV_MENSAJE_NOTIF);
      
      LV_MENSAJE_NOTIF := RC_TRX_UTILITIES.FCT_MENSAJE_NOTIFIC_RELOJ(Pv_Mensaje  => LV_MENSAJE_NOTIF,
                                                                     Pv_Proceso  => PV_JOB,
                                                                     Pv_Error    => PV_ERROR);
      --FIN [11922] 09/07/2018 IRO ABA PROCESO DE NOTIFICACIONES DEL PROCESO DE SUSPENSIÓN EN FECHA DE CORTE
      /*LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<valor>', PV_MENSAJE_NOTIF);
      LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<process>', PV_JOB);
      LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<fecha>', trunc(sysdate));*/
    ELSIF PV_OBJETO = 'DISPONIBILIDAD' then
      LV_MENSAJE_NOTIF := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995, 'MENSAJE_DISPONIBILIDAD', PV_ERROR);
      --INI [11922] 09/07/2018 IRO ABA PROCESO DE NOTIFICACIONES DEL PROCESO DE SUSPENSIÓN EN FECHA DE CORTE
      LV_MENSAJE_NOTIF := REPLACE(LV_MENSAJE_NOTIF,'<valor>',PV_MENSAJE_NOTIF);
      
      LV_MENSAJE_NOTIF := RC_TRX_UTILITIES.FCT_MENSAJE_NOTIFIC_RELOJ(Pv_Mensaje  => LV_MENSAJE_NOTIF,
                                                                     Pv_Proceso  => PV_JOB,
                                                                     Pv_Error    => PV_ERROR);
      --FIN [11922] 09/07/2018 IRO ABA PROCESO DE NOTIFICACIONES DEL PROCESO DE SUSPENSIÓN EN FECHA DE CORTE
      /*LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<valor>', PV_MENSAJE_NOTIF);
      LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<process>', PV_JOB);
      LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<fecha>', trunc(sysdate));*/
    ELSE
     LV_MENSAJE_NOTIF := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995, 'MENSAJE_GENERICO', PV_ERROR);
     LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<process>', PV_JOB);
     LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<shell>', PV_SHELL);
     LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<procedi>', PV_OBJETO);
     LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<fecha>', trunc(sysdate));
     LV_MENSAJE_NOTIF := replace(LV_MENSAJE_NOTIF, '<error>', PV_MENSAJE_NOTIF);
    END IF;
  ELSIF PV_TipoNotificacion = 'OK' THEN
    LV_MENSAJE_NOTIF := PV_MENSAJE_NOTIF;
  END IF;
  LV_MENSAJE_APL_SCP := ' NOTIFICACION DEL PROCESO ' || PV_SHELL;
  LV_MENSAJE_TEC_SCP := 'ID_PROCESO=' || PN_ID_PROCESO || '|PROCESO=' || PV_JOB || '|PROCESS_PRIN=' || PV_SHELL ||
                        '|OBJETO=' || PV_OBJETO || '|OBSERVACION=' || LV_MENSAJE_NOTIF;
  
  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            PN_ID_PROCESO,
                                            PV_JOB,
                                            PV_SHELL,
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);
  
  BEGIN
    
    FOR I IN C_DEVUELVE_MAILS(PV_IdDestinatario, 'claro') LOOP
        Lv_dir_princ_correo := Lv_dir_princ_correo || I.MAIL;
    END LOOP;
    
    OPEN C_PARAMETROS(12151,'MAIL_GLOBALHITSS');
    FETCH C_PARAMETROS INTO Ln_MailProvedor;
    CLOSE C_PARAMETROS;
    
    FOR I IN C_DEVUELVE_MAILS(PV_IdDestinatario, /*'iroute'*/Ln_MailProvedor) LOOP
        Lv_dir_sec_correo := Lv_dir_sec_correo || I.MAIL;
    END LOOP;
    
    Lv_dir_princ_correo := substr(Lv_dir_princ_correo, 1, length(Lv_dir_princ_correo) - 1);
    Lv_dir_sec_correo   := substr(Lv_dir_sec_correo, 1, length(Lv_dir_sec_correo) - 1);
    
    lv_destinatarios := Lv_dir_princ_correo || '//' || lv_dir_sec_correo;
    IF Pv_RutaRemota IS NOT NULL THEN
       ln_resultado_notif := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => PV_OBJETO,
                                                                       pd_fechaenvio      => SYSDATE,
                                                                       pd_fechaexpiracion => SYSDATE + 1,
                                                                       pv_asunto          => PV_ASUNTO,
                                                                       pv_mensaje         => lv_mensaje_notif || ' ' || Pv_RutaRemota,
                                                                       pv_destinatario    => Lv_Destinatarios,
                                                                       pv_remitente       => lv_remitente_mail,
                                                                       pv_tiporegistro    => 'A',
                                                                       pv_clase           => PV_IdDestinatario,
                                                                       pv_puerto          => Lv_Puerto,
                                                                       pv_servidor        => Lv_Servidor,
                                                                       pv_maxintentos     => 5,
                                                                       pv_idusuario       => 'GSIOPER',
                                                                       pv_mensajeretorno  => lv_mensaje_retorno,
                                                                       pn_idsecuencia     => ln_id_secuencia);
    ELSE
       ln_resultado_notif := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => PV_OBJETO,
                                                                       pd_fechaenvio      => SYSDATE,
                                                                       pd_fechaexpiracion => SYSDATE + 1,
                                                                       pv_asunto          => PV_ASUNTO,
                                                                       pv_mensaje         => lv_mensaje_notif,
                                                                       pv_destinatario    => lv_destinatarios,
                                                                       pv_remitente       => lv_remitente_mail,
                                                                       pv_tiporegistro    => 'M',
                                                                       pv_clase           => PV_IdDestinatario,
                                                                       pv_puerto          => LV_PUERTO,
                                                                       pv_servidor        => Lv_Servidor,
                                                                       pv_maxintentos     => 5,
                                                                       pv_idusuario       => 'GSIOPER',
                                                                       pv_mensajeretorno  => lv_mensaje_retorno,
                                                                       pn_idsecuencia     => ln_id_secuencia);
    END IF;
    
    IF lv_mensaje_retorno IS NOT NULL THEN
       PV_ERROR := 'Error al intentar enviar mail: ' || lv_mensaje_retorno;
       PN_ERROR := NVL(ln_id_secuencia, 1);
       
       LV_MENSAJE_APL_SCP := ' Error en el proceso RC_NOTIFICACIONES';
       LV_MENSAJE_TEC_SCP := PV_ERROR;
       
       SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                 LV_MENSAJE_APL_SCP,
                                                 LV_MENSAJE_TEC_SCP,
                                                 NULL,
                                                 0,
                                                 PN_ID_PROCESO,
                                                 PV_JOB,
                                                 PV_SHELL,
                                                 0,
                                                 0,
                                                 'N',
                                                 LN_ID_DETALLE,
                                                 LN_ERROR_SCP,
                                                 LV_MENSAJE);
       
    ELSE
       DBMS_OUTPUT.PUT_LINE('Mail(s) enviado(s).');
       PN_ERROR := 0;
       PV_ERROR := NULL;
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error al intentar enviar mail: ' || lv_mensaje_retorno ||' - ' || SUBSTR(SQLERRM, 1, 500);
      PN_ERROR := ln_id_secuencia;
      
      LV_MENSAJE_APL_SCP := ' Error en el proceso RC_NOTIFICACIONES';
      LV_MENSAJE_TEC_SCP := PV_ERROR;
      
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                PN_ID_PROCESO,
                                                PV_JOB,
                                                PV_SHELL,
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
  END;
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
EXCEPTION
  WHEN OTHERS THEN
    PN_ERROR := 1;
    PV_ERROR := SUBSTR(SQLERRM, 1, 200);
END RC_NOTIFICACIONES;
PROCEDURE PRC_DESCARTA_MALA_VENTA(PV_CUENTA           VARCHAR2 DEFAULT NULL,
                                  PN_REGION           NUMBER DEFAULT NULL,
                                  PV_FECH_MAX_PAGO    VARCHAR2 DEFAULT NULL,
                                  PV_FECH_MAX_JUSTICA VARCHAR2 DEFAULT NULL,
                                  PN_COD_VENDEDOR     NUMBER DEFAULT NULL,
                                  PV_VENDEDOR         VARCHAR2 DEFAULT NULL,
                                  PV_USUARIO          VARCHAR2 DEFAULT NULL,
                                  PV_ERROR            OUT VARCHAR2) IS
                                  
  LV_SQL VARCHAR2(4000);
BEGIN
  IF PV_CUENTA IS NOT NULL AND PN_REGION IS NOT NULL and
     PV_FECH_MAX_JUSTICA IS NULL THEN
    LV_SQL := 'UPDATE PORTA.GC_CUENTAS_C1_R<REGION>@AXIS A SET  A.ESTADO_APROBACION = 2,
               A.USUARIO_ACTUALIZACION=''<USUARIO>'', FECHA_ACTUALIZACION=SYSDATE
               WHERE A.CUENTA = ''<CUENTA>''';
    LV_SQL := REPLACE(LV_SQL, '<REGION>', PN_REGION);
    LV_SQL := REPLACE(LV_SQL, '<CUENTA>', PV_CUENTA);
    LV_SQL := REPLACE(LV_SQL, '<USUARIO>', PV_USUARIO);
    EXECUTE IMMEDIATE LV_SQL;
  ELSIF PV_CUENTA IS NOT NULL AND PN_REGION IS NOT NULL AND
        PV_FECH_MAX_PAGO IS NOT NULL AND PV_FECH_MAX_JUSTICA IS NOT NULL THEN
    LV_SQL := 'UPDATE PORTA.GC_CUENTAS_C1_R<REGION>@AXIS A
               SET A.FECHA_MAX_PROCESO = TO_DATE(''<FECH_MAX_JUSTI>'', ''DD/MM/RRRR''),
               A.USUARIO_ACTUALIZACION=''<USUARIO>'', A.FECHA_ACTUALIZACION=SYSDATE
               WHERE A.CUENTA =''<CUENTA>''
               AND A.FECH_MAX_PAGO =  TO_DATE(''<FECH_MAX_PAGO>'', ''DD/MM/RRRR'')';
    LV_SQL := REPLACE(LV_SQL, '<REGION>', PN_REGION);
    LV_SQL := REPLACE(LV_SQL, '<FECH_MAX_JUSTI>', PV_FECH_MAX_JUSTICA);
    LV_SQL := REPLACE(LV_SQL, '<FECH_MAX_PAGO>', PV_FECH_MAX_PAGO);
    LV_SQL := REPLACE(LV_SQL, '<CUENTA>', PV_CUENTA);
    LV_SQL := REPLACE(LV_SQL, '<USUARIO>', PV_USUARIO);
    EXECUTE IMMEDIATE LV_SQL;
  ELSIF PV_CUENTA IS NULL AND PN_REGION IS NOT NULL AND
        PV_FECH_MAX_JUSTICA IS NOT NULL AND
        (PN_COD_VENDEDOR IS NOT NULL OR PV_VENDEDOR IS NOT NULL) THEN
    IF PN_REGION = 3 THEN
      LV_SQL := 'UPDATE PORTA.GC_CUENTAS_C1_R<REGION>@AXIS A
               SET A.FECHA_MAX_PROCESO = TO_DATE(''<FECH_MAX_JUSTI>'', ''DD/MM/RRRR''),
               A.USUARIO_ACTUALIZACION=''<USUARIO>'', A.FECHA_ACTUALIZACION=SYSDATE
               WHERE A.COD_VENDEDOR = (DECODE(''<COD_VEN>'', NULL, A.COD_VENDEDOR, ''<COD_VEN>''))
               AND REPLACE(A.VENDEDOR,'' '','''') = (DECODE(''<VENDEDOR>'', NULL, REPLACE(A.VENDEDOR,'' '',''''),''TODOS'', REPLACE(A.VENDEDOR,'' '',''''), ''<VENDEDOR>''))';
      LV_SQL := REPLACE(LV_SQL, '<REGION>', 1);
      LV_SQL := REPLACE(LV_SQL, '<FECH_MAX_JUSTI>', PV_FECH_MAX_JUSTICA);
      LV_SQL := REPLACE(LV_SQL, '<COD_VEN>', PN_COD_VENDEDOR);
      LV_SQL := REPLACE(LV_SQL, '<VENDEDOR>', REPLACE(PV_VENDEDOR, ' ', ''));
      LV_SQL := REPLACE(LV_SQL, '<USUARIO>', PV_USUARIO);
      EXECUTE IMMEDIATE LV_SQL;
      LV_SQL := 'UPDATE PORTA.GC_CUENTAS_C1_R<REGION>@AXIS A
               SET A.FECHA_MAX_PROCESO = TO_DATE(''<FECH_MAX_JUSTI>'', ''DD/MM/RRRR''),
               A.USUARIO_ACTUALIZACION=''<USUARIO>'', A.FECHA_ACTUALIZACION=SYSDATE
               WHERE A.COD_VENDEDOR = (DECODE(''<COD_VEN>'', NULL, A.COD_VENDEDOR, ''<COD_VEN>''))
               AND REPLACE(A.VENDEDOR,'' '','''') = (DECODE(''<VENDEDOR>'', NULL, REPLACE(A.VENDEDOR,'' '',''''),''TODOS'', REPLACE(A.VENDEDOR,'' '',''''), ''<VENDEDOR>''))';
      LV_SQL := REPLACE(LV_SQL, '<REGION>', 2);
      LV_SQL := REPLACE(LV_SQL, '<FECH_MAX_JUSTI>', PV_FECH_MAX_JUSTICA);
      LV_SQL := REPLACE(LV_SQL, '<COD_VEN>', PN_COD_VENDEDOR);
      LV_SQL := REPLACE(LV_SQL, '<VENDEDOR>', REPLACE(PV_VENDEDOR, ' ', ''));
      LV_SQL := REPLACE(LV_SQL, '<USUARIO>', PV_USUARIO);
      EXECUTE IMMEDIATE LV_SQL;
    ELSE
      LV_SQL := 'UPDATE PORTA.GC_CUENTAS_C1_R<REGION>@AXIS A
               SET A.FECHA_MAX_PROCESO = TO_DATE(''<FECH_MAX_JUSTI>'', ''DD/MM/RRRR''),
               A.USUARIO_ACTUALIZACION=''<USUARIO>'', A.FECHA_ACTUALIZACION=SYSDATE
               WHERE A.COD_VENDEDOR = (DECODE(''<COD_VEN>'', NULL, A.COD_VENDEDOR, ''<COD_VEN>''))
               AND REPLACE(A.VENDEDOR,'' '','''') = (DECODE(''<VENDEDOR>'', NULL, REPLACE(A.VENDEDOR,'' '',''''),''TODOS'', REPLACE(A.VENDEDOR,'' '',''''), ''<VENDEDOR>''))';
      LV_SQL := REPLACE(LV_SQL, '<REGION>', PN_REGION);
      LV_SQL := REPLACE(LV_SQL, '<FECH_MAX_JUSTI>', PV_FECH_MAX_JUSTICA);
      LV_SQL := REPLACE(LV_SQL, '<COD_VEN>', PN_COD_VENDEDOR);
      LV_SQL := REPLACE(LV_SQL, '<VENDEDOR>', REPLACE(PV_VENDEDOR, ' ', ''));
      LV_SQL := REPLACE(LV_SQL, '<USUARIO>', PV_USUARIO);
      EXECUTE IMMEDIATE LV_SQL;
    END IF;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);
END;

FUNCTION FCT_MENSAJE_NOTIFIC_RELOJ(Pv_Mensaje        VARCHAR2,
                                   Pv_Proceso        VARCHAR2,
                                   Pv_Error      OUT VARCHAR2) RETURN VARCHAR2 IS
  
  CURSOR C_TOTAL_SERVICIOS_X_SUSP(Cv_FechaFact VARCHAR2)IS
  SELECT /*+ RULE */ COUNT(CPL.CUSTOMER_ID) CANT
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS_S CPL
   WHERE CPL.CH_TIMEACCIONDATE BETWEEN (TO_DATE(Cv_FechaFact,'DD/MM/YYYY') - 
                                               (SELECT NVL(TO_NUMBER(R.VALUE_RETURN_SHORTSTRING), 210)
                                                  FROM RELOJ.TTC_RULE_VALUES R
                                                 WHERE PROCESS = 3
                                                   AND R.VALUE_RULE_STRING = 'DIAS_SUSPENSION_PEND'))
                                   AND TO_DATE(Cv_FechaFact,'DD/MM/YYYY')
     AND PRGCODE <> (SELECT VALOR FROM SCP.SCP_PARAMETROS_PROCESOS WHERE ID_PARAMETRO = 'PRGCODE_DTH' AND ID_PROCESO = 'RELOJ_DTH')
     AND CPL.CO_STATUSTRX IN ('P','T')
     AND CPL.CO_STATUS_A IN ('34', '80');
  
  CURSOR C_TOTAL_SERVICIOS_SUSP(Cv_FechaFact VARCHAR2)IS
  SELECT /*+ RULE */ COUNT(CPL.CUSTOMER_ID) CANT
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS_S CPL
   WHERE CPL.LASTMODDATE BETWEEN TO_DATE(Cv_FechaFact||' 00:00:00','DD/MM/YYYY HH24:MI:SS')
                             AND TO_DATE(Cv_FechaFact||' 23:59:59','DD/MM/YYYY HH24:MI:SS') 
     AND CPL.CH_TIMEACCIONDATE BETWEEN (TO_DATE(Cv_FechaFact,'DD/MM/YYYY') - 
                                               (SELECT NVL(TO_NUMBER(R.VALUE_RETURN_SHORTSTRING), 210)
                                                  FROM RELOJ.TTC_RULE_VALUES R
                                                 WHERE PROCESS = 3
                                                   AND R.VALUE_RULE_STRING = 'DIAS_SUSPENSION_PEND'))
                                   AND TO_DATE(Cv_FechaFact,'DD/MM/YYYY')
     AND CPL.PRGCODE <> (SELECT VALOR FROM SCP.SCP_PARAMETROS_PROCESOS WHERE ID_PARAMETRO = 'PRGCODE_DTH' AND ID_PROCESO = 'RELOJ_DTH')
     AND CPL.CO_STATUSTRX IN ('F')
     AND CPL.CO_STATUS_A IN ('34', '80');
  
  CURSOR C_SERVICIOS_PEND(Cv_FechaFact VARCHAR2)IS
  SELECT /*+ RULE */ COUNT(CPL.CUSTOMER_ID) CANT
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS_S CPL
   WHERE CPL.CH_TIMEACCIONDATE = TO_DATE(Cv_FechaFact,'DD/MM/YYYY')
     AND CPL.PRGCODE <> (SELECT S.VALOR FROM SCP.SCP_PARAMETROS_PROCESOS S WHERE S.ID_PARAMETRO = 'PRGCODE_DTH' AND ID_PROCESO = 'RELOJ_DTH')
     AND CPL.CO_STATUSTRX IN ('P')
     AND CPL.CO_STATUS_A IN ('34', '80');
  
  CURSOR C_OTRA_FECHA_SERV_PEND(Cv_FechaFact VARCHAR2)IS
  SELECT A.CH_TIMEACCIONDATE FROM 
  (SELECT /*+ RULE */ COUNT(CPL.CUSTOMER_ID) CANT, CPL.CH_TIMEACCIONDATE
     FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS_S CPL
    WHERE CPL.CH_TIMEACCIONDATE BETWEEN (TO_DATE(Cv_FechaFact,'DD/MM/YYYY')-1) - 
          (SELECT NVL(TO_NUMBER(R.VALUE_RETURN_SHORTSTRING), 210) FROM RELOJ.TTC_RULE_VALUES R WHERE R.PROCESS = 3 AND R.VALUE_RULE_STRING = 'DIAS_SUSPENSION_PEND')-1 
          AND (TO_DATE(Cv_FechaFact,'DD/MM/YYYY')-1)
      AND CPL.PRGCODE <> (SELECT S.VALOR FROM SCP.SCP_PARAMETROS_PROCESOS S WHERE S.ID_PARAMETRO = 'PRGCODE_DTH' AND ID_PROCESO = 'RELOJ_DTH')
      AND CPL.CO_STATUSTRX IN ('P')
      AND CPL.CO_STATUS_A IN ('34', '80')
    GROUP BY CPL.CH_TIMEACCIONDATE
    ORDER BY CANT DESC) A
    WHERE ROWNUM < = 1;
  
  CURSOR C_TOTAL_SERVICIOS_PEND(Cv_FechaFact VARCHAR2)IS
  SELECT /*+ RULE */ COUNT(CPL.CUSTOMER_ID) CANT
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS_S CPL
   WHERE CPL.CH_TIMEACCIONDATE BETWEEN (TO_DATE(Cv_FechaFact,'DD/MM/YYYY')-1) - 
         (SELECT NVL(TO_NUMBER(R.VALUE_RETURN_SHORTSTRING), 210) FROM RELOJ.TTC_RULE_VALUES R WHERE R.PROCESS = 3
             AND R.VALUE_RULE_STRING = 'DIAS_SUSPENSION_PEND')-1 
         AND (TO_DATE(Cv_FechaFact,'DD/MM/YYYY')-1)
     AND CPL.PRGCODE <> (SELECT S.VALOR FROM SCP.SCP_PARAMETROS_PROCESOS S WHERE S.ID_PARAMETRO = 'PRGCODE_DTH' AND ID_PROCESO = 'RELOJ_DTH')
     AND CPL.CO_STATUSTRX IN ('P')
     AND CPL.CO_STATUS_A IN ('34', '80');
  
  Ln_CantServSusp      NUMBER;
  Ln_CantServxSusp     NUMBER;
  Ln_CantTotalServ     NUMBER;
  Ln_CantServPendient  NUMBER;
  Ln_CantOtrosServPen  NUMBER;
  Lv_FechaEjecucion    VARCHAR2(50);
  Lv_FechaOtrasSusp    VARCHAR2(100);
  Lv_MensajeNotif      VARCHAR2(3000);
  Lv_MensajeMail       VARCHAR2(4000);
  
BEGIN
  
  Lv_FechaEjecucion := TO_CHAR(SYSDATE,'DD/MM/YYYY');
  
  OPEN C_TOTAL_SERVICIOS_SUSP(Lv_FechaEjecucion);
  FETCH C_TOTAL_SERVICIOS_SUSP INTO Ln_CantServSusp;
  CLOSE C_TOTAL_SERVICIOS_SUSP;
  
  OPEN C_TOTAL_SERVICIOS_X_SUSP(Lv_FechaEjecucion);
  FETCH C_TOTAL_SERVICIOS_X_SUSP INTO Ln_CantServxSusp;
  CLOSE C_TOTAL_SERVICIOS_X_SUSP;
  
  Ln_CantTotalServ := NVL(Ln_CantServSusp,0) + NVL(Ln_CantServxSusp,0);
  
  OPEN C_SERVICIOS_PEND(Lv_FechaEjecucion);
  FETCH C_SERVICIOS_PEND INTO Ln_CantServPendient;
  CLOSE C_SERVICIOS_PEND;
  
  OPEN C_TOTAL_SERVICIOS_PEND(Lv_FechaEjecucion);
  FETCH C_TOTAL_SERVICIOS_PEND INTO Ln_CantOtrosServPen;
  CLOSE C_TOTAL_SERVICIOS_PEND;
  
  OPEN C_OTRA_FECHA_SERV_PEND(Lv_FechaEjecucion);
  FETCH C_OTRA_FECHA_SERV_PEND INTO Lv_FechaOtrasSusp;
  CLOSE C_OTRA_FECHA_SERV_PEND;
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,1)+1, (INSTR(Pv_Mensaje,'|',1,2)-INSTR(Pv_Mensaje,'|',1,1))-1);
  Lv_MensajeNotif:= '<html><table><tr><td width="1300" height="70" align="justify" style="padding:5px"><p>'||Lv_MensajeMail||'</p>';
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,2)+1, (INSTR(Pv_Mensaje,'|',1,3)-INSTR(Pv_Mensaje,'|',1,2))-1);
  Lv_MensajeNotif:= Lv_MensajeNotif||'<p>'||Lv_MensajeMail||'</p></td></tr></table>';
  Lv_MensajeNotif:= Lv_MensajeNotif||'<table border="1" bordercolor="FFC133" cellspacing="1" cellpadding="1"><tr><td width="200" bgcolor="FF6133" style="padding:2px"><b>DESCRIPCION</b></td><td width="75" bgcolor="FF6133" style="padding:2px"><b>CANTIDAD</b></td></tr>';
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,3)+1, (INSTR(Pv_Mensaje,'|',1,4)-INSTR(Pv_Mensaje,'|',1,3))-1);
  Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Pv_Proceso ||'</td></tr>';
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,4)+1, (INSTR(Pv_Mensaje,'|',1,5)-INSTR(Pv_Mensaje,'|',1,4))-1);
  Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| TRUNC(SYSDATE) ||'</td></tr>';
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,5)+1, (INSTR(Pv_Mensaje,'|',1,6)-INSTR(Pv_Mensaje,'|',1,5))-1);
  Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_CantTotalServ ||'</td></tr>';
  
  Lv_MensajeMail := SUBSTR(Pv_Mensaje, INSTR(Pv_Mensaje,'|',1,6)+1, (INSTR(Pv_Mensaje,'|',1,7)-INSTR(Pv_Mensaje,'|',1,6))-1);
  
  IF Ln_CantOtrosServPen > 0 AND Lv_FechaOtrasSusp IS NOT NULL THEN
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||TRUNC(SYSDATE)||'</td><td width="50" align="right" style="padding:2px">'|| Ln_CantServPendient ||'</td></tr>';
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||Lv_FechaOtrasSusp||'</td><td width="50" align="right" style="padding:2px">'|| Ln_CantOtrosServPen ||'</td></tr>';
  ELSE
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="250" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_CantServPendient ||'</td></tr>';
  END IF;
  
  Lv_MensajeNotif:= Lv_MensajeNotif||'</table><table><tr><td align="justify" style="padding:5px"><br><p>Saludos cordiales.</p></td></tr></table></html>';
  
  RETURN Lv_MensajeNotif;
  
EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := 'FCT_MENSAJE_NOTIFIC_RELOJ => '||SUBSTR(SQLERRM, 1, 200);
END FCT_MENSAJE_NOTIFIC_RELOJ;

PROCEDURE PRC_REPORTE_INACTIVACIONES (Pv_Mensaje  OUT VARCHAR2,
                                      Pv_Error    OUT VARCHAR2)IS
  
  CURSOR C_PARAMETROS (Cn_TipoParamet NUMBER, Cv_Parametro VARCHAR2)IS
  SELECT X.VALOR
    FROM SYSADM.GV_PARAMETROS X
   WHERE X.ID_TIPO_PARAMETRO = Cn_TipoParamet
     AND X.ID_PARAMETRO = Cv_Parametro;
     
 CURSOR C_INACTIVACIONES_PLANIFICADAS (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2 )IS
  SELECT COUNT(X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('P', 'T', 'F', 'E', 'A', 'D')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM <> 'DTH';    
     
 CURSOR C_INACTIVACIONES_PLANIF_DTH (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT COUNT(X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('P', 'T', 'F', 'E', 'A', 'D')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM = 'DTH';      
  
  CURSOR C_INACTIVACIONES_EXITOSAS (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT COUNT (X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X 
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('T', 'F')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM <> 'DTH';
     
  CURSOR C_INACTIVACIONES_EXIT_DTH (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT COUNT (X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X 
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('T', 'F')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM = 'DTH';     
     
  CURSOR C_INACTIVACIONES_ERROR (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT COUNT(X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('E')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM <> 'DTH';     
  
  CURSOR C_INACTIVACIONES_ERROR_DTH (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT COUNT(X.CO_ID)
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('E')
     AND X.CO_STATUS_A = '35'
     AND X.DN_NUM = 'DTH';
  
  CURSOR C_DETALLE_INACTIVACION_SERV (Cv_Fecha_Inicio VARCHAR2 , Cv_Fecha_Fin VARCHAR2)IS
  SELECT X.CUSTCODE, X.DN_NUM, X.CO_STATUS_A, X.CH_TIMEACCIONDATE, X.CO_STATUSTRX,
         DECODE(X.CO_STATUSTRX,'E',X.CO_REMARKTRX,'P','Pendiente de inactivacion', 'A','Cuenta sin deuda', 'D', X.CO_REMARKTRX,
                'OK') CO_REMARKTRX
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X
   WHERE X.CH_TIMEACCIONDATE BETWEEN TO_DATE(Cv_Fecha_Inicio, 'DD/MM/YYYY') 
     AND TO_DATE(Cv_Fecha_Fin, 'DD/MM/YYYY')
     AND X.CO_STATUSTRX IN ('T','F','E','P','A','D')
     AND X.CO_STATUS_A = '35'
   ORDER BY X.CO_STATUSTRX ASC;
  
  Ln_ServPlanificados  NUMBER:=0;
  Ln_ServInactivados   NUMBER:=0;
  Ln_ServError         NUMBER:=0;
  Ln_Count             NUMBER:=0;
  Ln_Error             NUMBER;
  Ln_ResultadoNotif    NUMBER;
  Ln_IdSecuencia       NUMBER;
--  Lv_Fecha             VARCHAR2(50);
  Lv_CuerpoMail        VARCHAR2(3000);
  Lv_MensajeMail       VARCHAR2(5000);
  Lv_MensajeNotif      VARCHAR2(5000);
  Lv_Directorio        VARCHAR2(100);
  Lv_NombreArchivo     VARCHAR2(100);
  Lv_AsuntoMail        VARCHAR2(1000);
  Lv_IpFinan27         VARCHAR2(100);
  Lv_UsuFinan27        VARCHAR2(100);
  Lv_RutaFinan27       VARCHAR2(1000);
  Lv_ErrorFTP          VARCHAR2(1000);
  Lv_Destinatarios     VARCHAR2(2000);
  Lv_Clase             VARCHAR2(4);
  Lv_Puerto            VARCHAR2(1000);
  Lv_Servidor          VARCHAR2(1000);
  Le_Error             EXCEPTION;
  LF_OUTPUT            UTL_FILE.FILE_TYPE;
  Ln_ServPlanificados_DTH  NUMBER :=0;
  Ln_ServInactivados_DTH   NUMBER :=0;
  Ln_ServErrorDTH          NUMBER :=0;
  Lv_Fecha_Inicio          VARCHAR2(50);
  Lv_Fecha_Fin             VARCHAR2(50);
  Ld_Fecha_Inicio          DATE;
  
BEGIN
  
  -------------------------------------------------------------------------------------------------------
  --- Se Mejora la Presentacion de Inactivaciones Planificadas en caso de que tenga cuentas pendientes
  -------------------------------------------------------------------------------------------------------
  
  Lv_Fecha_Fin    := TO_CHAR(SYSDATE,'DD/MM/YYYY'); 
  Ld_Fecha_Inicio := SYSDATE - 2;
  Lv_Fecha_Inicio := TO_CHAR(Ld_Fecha_Inicio,'DD/MM/YYYY'); 
  
  OPEN C_INACTIVACIONES_PLANIFICADAS(Lv_Fecha_Inicio , Lv_Fecha_Fin);
  FETCH C_INACTIVACIONES_PLANIFICADAS INTO Ln_ServPlanificados;
  CLOSE C_INACTIVACIONES_PLANIFICADAS;
  
  OPEN C_INACTIVACIONES_PLANIF_DTH(Lv_Fecha_Inicio , Lv_Fecha_Fin);
  FETCH C_INACTIVACIONES_PLANIF_DTH INTO Ln_ServPlanificados_DTH;
  CLOSE C_INACTIVACIONES_PLANIF_DTH;
  
    IF NVL(Ln_ServPlanificados,0 ) > 0 OR NVL(Ln_ServPlanificados_DTH,0 ) > 0 THEN
    
     OPEN C_INACTIVACIONES_EXITOSAS(Lv_Fecha_Inicio , Lv_Fecha_Fin);
     FETCH C_INACTIVACIONES_EXITOSAS INTO Ln_ServInactivados;
     CLOSE C_INACTIVACIONES_EXITOSAS;

     OPEN C_INACTIVACIONES_EXIT_DTH(Lv_Fecha_Inicio , Lv_Fecha_Fin);
     FETCH C_INACTIVACIONES_EXIT_DTH INTO Ln_ServInactivados_DTH;
     CLOSE C_INACTIVACIONES_EXIT_DTH;     
      
     OPEN C_INACTIVACIONES_ERROR(Lv_Fecha_Inicio , Lv_Fecha_Fin);
     FETCH C_INACTIVACIONES_ERROR INTO Ln_ServError;
     CLOSE C_INACTIVACIONES_ERROR;
     
     OPEN C_INACTIVACIONES_ERROR_DTH(Lv_Fecha_Inicio , Lv_Fecha_Fin);
     FETCH C_INACTIVACIONES_ERROR_DTH INTO Ln_ServErrorDTH;
     CLOSE C_INACTIVACIONES_ERROR_DTH;
          
     OPEN C_PARAMETROS(12018, 'BODY_MAIL_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_CuerpoMail;
     CLOSE C_PARAMETROS;
     
  --------------------------------------------------------------------------------------------------------------
  ---  Se Modifica Cuerpo del Mensaje del Correo a enviar, mostrando las inactivaciones por Categoria SMA y DTH
  --------------------------------------------------------------------------------------------------------------     
     
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,1)+1, (INSTR(Lv_CuerpoMail,'|',1,2)-INSTR(Lv_CuerpoMail,'|',1,1))-1);
     Lv_MensajeNotif:= '<html><table><tr><td width="1300" height="70" align="justify" style="padding:5px"><p>'||Lv_MensajeMail||'</p>';
     
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,2)+1, (INSTR(Lv_CuerpoMail,'|',1,3)-INSTR(Lv_CuerpoMail,'|',1,2))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<p>'||Lv_MensajeMail||'</p></td></tr></table>';
     Lv_MensajeNotif:= Lv_MensajeNotif||'<table border="1" bordercolor="FFC133" cellspacing="1" cellpadding="1"><tr><td width="250" bgcolor="FF6133" style="padding:2px"><b>DESCRIPCION</b></td><td width="75" bgcolor="FF6133" style="padding:2px"><b>CANTIDAD</b></td></tr>';
     
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,3)+1, (INSTR(Lv_CuerpoMail,'|',1,4)-INSTR(Lv_CuerpoMail,'|',1,3))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| TRUNC(SYSDATE) ||'</td></tr>';
  
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,4)+1, (INSTR(Lv_CuerpoMail,'|',1,5)-INSTR(Lv_CuerpoMail,'|',1,4))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServPlanificados ||'</td></tr>';
  
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,5)+1, (INSTR(Lv_CuerpoMail,'|',1,6)-INSTR(Lv_CuerpoMail,'|',1,5))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServPlanificados_DTH ||'</td></tr>';

     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,6)+1, (INSTR(Lv_CuerpoMail,'|',1,7)-INSTR(Lv_CuerpoMail,'|',1,6))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServInactivados ||'</td></tr>';

     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,7)+1, (INSTR(Lv_CuerpoMail,'|',1,8)-INSTR(Lv_CuerpoMail,'|',1,7))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServInactivados_DTH ||'</td></tr>';
     
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,8)+1, (INSTR(Lv_CuerpoMail,'|',1,9)-INSTR(Lv_CuerpoMail,'|',1,8))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServError ||'</td></tr>';
     
     Lv_MensajeMail := SUBSTR(Lv_CuerpoMail, INSTR(Lv_CuerpoMail,'|',1,9)+1, (INSTR(Lv_CuerpoMail,'|',1,10)-INSTR(Lv_CuerpoMail,'|',1,9))-1);
     Lv_MensajeNotif:= Lv_MensajeNotif||'<tr><td width="275" style="padding:2px">'||Lv_MensajeMail||'</td><td width="50" align="right" style="padding:2px">'|| Ln_ServErrorDTH ||'</td></tr>';    
     
     Lv_MensajeNotif:= Lv_MensajeNotif||'</table><table><tr><td align="justify" style="padding:5px"><br><p>Saludos Cordiales.</p></td></tr></table></html>';
     
     OPEN C_PARAMETROS(12018, 'DIRECTORIO_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_Directorio;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'NOM_ARCHIV_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_NombreArchivo;
     CLOSE C_PARAMETROS;
     
     Lv_NombreArchivo := Lv_NombreArchivo||TO_CHAR(SYSDATE,'DDMMYYYY')||'.csv';
     
     OPEN C_PARAMETROS(12018, 'ASUNT_MAIL_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_AsuntoMail;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'IP_FINAN27_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_IpFinan27;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'USU_FINAN27_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_UsuFinan27;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'RUT_FINAN27_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_RutaFinan27;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'DEST_MAIL_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_Destinatarios;
     CLOSE C_PARAMETROS;
     
     OPEN C_PARAMETROS(12018, 'CLASE_INACT_RELOJ');
     FETCH C_PARAMETROS INTO Lv_Clase;
     CLOSE C_PARAMETROS;
     
     FOR I IN C_DETALLE_INACTIVACION_SERV(Lv_Fecha_Inicio , Lv_Fecha_Fin ) LOOP
         
         Ln_Count := Ln_Count +1;
         IF Ln_Count = 1 THEN
            LF_OUTPUT := UTL_FILE.FOPEN(Lv_Directorio, Lv_NombreArchivo, 'W');
            UTL_FILE.PUT_LINE(LF_OUTPUT, 'CUENTA|SERVICIO|ESTADO|FECHA_INACTIVACION|ESTADO_TRX|MENSAJE_ERROR');
         END IF;
         
         UTL_FILE.PUT_LINE(LF_OUTPUT, I.CUSTCODE||'|'||I.DN_NUM||'|'||I.CO_STATUS_A||'|'||I.CH_TIMEACCIONDATE||'|'||I.CO_STATUSTRX||'|'||I.CO_REMARKTRX);
         
     END LOOP;
     
     IF Ln_Count > 0 THEN
        UTL_FILE.FCLOSE(LF_OUTPUT);
        
        SCP_DAT.SCK_FTP_GTW.SCP_TRANSFIERE_ARCHIVO(PV_IP                => Lv_IpFinan27,
                                                   PV_USUARIO           => Lv_UsuFinan27,
                                                   PV_RUTA_REMOTA       => Lv_RutaFinan27,
                                                   PV_NOMBRE_ARCH       => Lv_NombreArchivo,
                                                   PV_RUTA_LOCAL        => Lv_Directorio,
                                                   PV_BORRAR_ARCH_LOCAL => 'N',
                                                   PN_ERROR             => Ln_Error,
                                                   PV_ERROR             => Lv_ErrorFTP);
        
        IF Lv_ErrorFtp IS NULL THEN
           Lv_RutaFinan27 := '</ARCHIVO1='||Lv_NombreArchivo||'|DIRECTORIO1='||Lv_RutaFinan27||'|/>';
        ELSE
           Lv_RutaFinan27 := NULL;
        END IF;
        
        IF Lv_RutaFinan27 IS NOT NULL THEN
           Ln_ResultadoNotif := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'RC_TRX_TIMETOCASH_PLANNING.PRC_REPORTE_INACTIVACIONES',
                                                                          pd_fechaenvio      => SYSDATE,
                                                                          pd_fechaexpiracion => SYSDATE + 1,
                                                                          pv_asunto          => Lv_AsuntoMail,
                                                                          pv_mensaje         => Lv_MensajeNotif||' '||Lv_RutaFinan27,
                                                                          pv_destinatario    => Lv_Destinatarios,
                                                                          pv_remitente       => 'reloj_cobranza@claro.com.ec',
                                                                          pv_tiporegistro    => 'A',
                                                                          pv_clase           => Lv_Clase,
                                                                          pv_puerto          => Lv_Puerto,
                                                                          pv_servidor        => Lv_Servidor,
                                                                          pv_maxintentos     => 5,
                                                                          pv_idusuario       => 'GSIOPER',
                                                                          pv_mensajeretorno  => Pv_Error,
                                                                          pn_idsecuencia     => Ln_IdSecuencia);
        ELSE
           Ln_ResultadoNotif := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'RC_TRX_TIMETOCASH_PLANNING.PRC_REPORTE_INACTIVACIONES',
                                                                          pd_fechaenvio      => SYSDATE,
                                                                          pd_fechaexpiracion => SYSDATE + 1,
                                                                          pv_asunto          => Lv_AsuntoMail,
                                                                          pv_mensaje         => Lv_MensajeNotif,
                                                                          pv_destinatario    => Lv_Destinatarios,
                                                                          pv_remitente       => 'reloj_cobranza@claro.com.ec',
                                                                          pv_tiporegistro    => 'M',
                                                                          pv_clase           => Lv_Clase,
                                                                          pv_puerto          => Lv_Puerto,
                                                                          pv_servidor        => Lv_Servidor,
                                                                          pv_maxintentos     => 5,
                                                                          pv_idusuario       => 'GSIOPER',
                                                                          pv_mensajeretorno  => Pv_Error,
                                                                          pn_idsecuencia     => Ln_IdSecuencia);
        END IF;
        
     END IF;
     
  END IF;
  
  IF Lv_ErrorFTP IS NOT NULL THEN
     Pv_Error := 'Error en la transferencia del archivo '||Lv_NombreArchivo||' '||Lv_ErrorFTP;
     RAISE Le_Error;
  END IF;
  
  IF Pv_Error IS NULL AND Ln_ServPlanificados = 0 THEN
     Pv_Mensaje := 'No hay inactivaciones de reloj planificadas para notificar.';
  ELSIF Pv_Error IS NULL AND Ln_ServPlanificados > 0 THEN
     Pv_Mensaje := 'Ejecucion exitosa del proceso de notificacion de inactivaciones de reloj';
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    Pv_Error := 'PRC_REPORTE_INACTIVACIONES => '|| Pv_Error;
  WHEN OTHERS THEN
    Pv_Error := 'PRC_REPORTE_INACTIVACIONES => '|| SUBSTR(SQLERRM, 1, 300);
END PRC_REPORTE_INACTIVACIONES;

END RC_TRX_UTILITIES;
/
