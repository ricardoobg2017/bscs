CREATE OR REPLACE PACKAGE INSERT_TDMA AS

PROCEDURE MAIN ;


END INSERT_TDMA;
/
CREATE OR REPLACE PACKAGE BODY INSERT_TDMA AS


PROCEDURE MAIN IS

/*
v_hlcode := 2
v_hmcode := 1
v_inicio := 7000000
v_final  := 7059999 */

v_plcode    NUMBER := 2;
v_ExistDN	NUMBER ;
v_Error		VARCHAR2(100);
v_count		NUMBER := 0;
v_dn_id		NUMBER ;
vPORT_ID_Nxt NUMBER;


  v_hlcode NUMBER := 3 ;
  v_hmcode NUMBER := 39 ;
  v_inicio NUMBER := 7999000 ;
  v_final  NUMBER := 7999999 ;



BEGIN







  select nvl(max(dn_id),0)+1 into v_dn_id from directory_number;
  select nvl(max(port_id),0)+1 into vPORT_ID_Nxt  from port ;

  LOOP
	--
	BEGIN
    insert into DIRECTORY_NUMBER (
          DN_ID,
          PLCODE,
          NDC,
          HLCODE,
          HMCODE,
          DN_NUM,
          DN_STATUS,
          DN_STATUS_MOD_DATE,
          DEALER_ID,
          DN_ASSIGN_DATE,
          DN_TYPE,
          DN_ASSIGN,
          DN_ENTDATE,
          DN_MODDATE,
          DN_MODFLAG,
          EVCODE,
          LOCAL_PREFIX_LEN,
          EXTERNAL_IND,
          BLOCK_IND,
          REC_VERSION,
          DIRNUM_NPCODE )
        values (
          v_dn_id,
          v_plcode,
          '9',
          v_hlcode,
          v_hmcode,
          '5939'||rtrim(ltrim(to_char(v_inicio))),
          'r',
          sysdate,
          -1,
          sysdate,
          decode( mod(v_inicio,100), 00, 1, 11, 1, 88, 2, 3 ),
          1,
          sysdate,
          sysdate,
          'X',
          decode( mod(v_inicio,100), 00, 136, 11, 136, 88, 135, NULL ),
          4,
          'N',
          'N',
          0,
          1 ) ;
   	    BEGIN
          insert into PORT (
               PORT_ID,
               PLCODE,
               HLCODE,
               PORT_NUM,
               PORT_STATUS,
               PORT_STATUSMODDAT,
               SM_ID,
	           DN_ID,
               DEALER_ID,
               PORT_ACTIV_DATE,
               PORT_ENTDATE,
               PORT_MODDATE,
               PORT_USERLASTMOD,
               EXTERNAL_IND,
               REC_VERSION,
               PORT_NPCODE )
            values (
               vPORT_ID_Nxt,
               v_plcode,
               v_hlcode,
               '9' || v_inicio,
               'r',
               sysdate,
               NULL,
	           v_dn_id,
               -1,
               trunc(sysdate),
               sysdate,
               trunc(sysdate),
               'SLB',
               'N',
               0,
               28 ) ;
          vPORT_ID_Nxt := vPORT_ID_Nxt + 1 ;

		EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN
    	  --dbms_output.put_line(v_inicio || ' duplicado...');
          NULL;
		END;
      v_dn_id := v_dn_id + 1;

	  EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        dbms_output.put_line(v_inicio || ' duplicado...');
	    NULL;
    END;

	--
	IF v_inicio >= v_final THEN
	  exit;
	END IF;
	v_inicio := v_inicio + 1;

	IF v_count = 1000 THEN
	  COMMIT;
	  v_count := 0;
	END IF;
	v_count := v_count + 1;
  END LOOP;

  update APP_SEQUENCE_VALUE set
   next_free_value = (SELECT NVL(MAX(DN_ID),0) + 1 FROM DIRECTORY_NUMBER)
  where app_sequence_id = 8 ;
  update APP_SEQUENCE_VALUE set
   next_free_value = (SELECT NVL(MAX(PORT_ID),0) + 1 FROM PORT)
  where app_sequence_id = 9 ;

  COMMIT;


END MAIN;



END INSERT_TDMA;
/

