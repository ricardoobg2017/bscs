CREATE OR REPLACE PACKAGE GCK_TRX_CONCILIA_LINEA_MASIVA IS

--======================================================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Validacion de deuda de los servicios
--======================================================================================--

PROCEDURE GCP_MQU_RELOJ (DESPACHA NUMBER);

END GCK_TRX_CONCILIA_LINEA_MASIVA;
/
CREATE OR REPLACE PACKAGE BODY GCK_TRX_CONCILIA_LINEA_MASIVA IS
--======================================================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Validacion de deuda de los servicios
--======================================================================================--
-------------------------------------------------------------------
PROCEDURE GCP_MQU_RELOJ (DESPACHA NUMBER)IS

    CURSOR CV_CUR_DEUDASALDO (CV_CUENTA VARCHAR2) IS
        SELECT /*+ RULE +*/
         NVL(SUM(DECODE(B.OHSTATUS, 'CO', 0, B.OHINVAMT_DOC)), 0) DEUDA,
         NVL(SUM(B.OHOPNAMT_DOC), 0) SALDO
          FROM CUSTOMER_ALL A, ORDERHDR_ALL B
         WHERE A.CUSTCODE = CV_CUENTA
           AND A.CUSTOMER_DEALER = 'C'
           AND A.CUSTOMER_ID = B.CUSTOMER_ID
           AND B.OHSTATUS IN ('IN', 'CM', 'CO')
           AND B.OHDUEDATE < TRUNC(SYSDATE)
         GROUP BY A.CUSTCODE;

    CURSOR CV_BASE IS
        SELECT A.*, A.ROWID
          FROM BL_GSI_RELOJ A
         WHERE MENSAJE IS NULL
           AND DESPACHADOR = DESPACHA;


    LB_FOUND         BOOLEAN;
    LC_DEUDASALDO    CV_CUR_DEUDASALDO%ROWTYPE;
    LN_SALDO         NUMBER;
    LN_DEUDA         NUMBER;

BEGIN

   FOR I IN CV_BASE LOOP
       BEGIN
          OPEN CV_CUR_DEUDASALDO ( I.CUENTA );
          FETCH CV_CUR_DEUDASALDO INTO LC_DEUDASALDO;
          LB_FOUND := CV_CUR_DEUDASALDO%FOUND;
          CLOSE CV_CUR_DEUDASALDO;

          IF LB_FOUND THEN
             LN_DEUDA := LC_DEUDASALDO.DEUDA;
             LN_SALDO := LC_DEUDASALDO.SALDO;
          ELSE
             LN_DEUDA := 0;
             LN_SALDO := 0;
          END IF;
         
          IF LN_SALDO <=0 THEN
             UPDATE BL_GSI_RELOJ
                SET DEUDA = LN_DEUDA, SALDO = LN_SALDO, MENSAJE = 'OK'
              WHERE ROWID = I.ROWID;
          ELSE
             UPDATE BL_GSI_RELOJ
                SET DEUDA   = LN_DEUDA,
                    SALDO   = LN_SALDO,
                    MENSAJE = 'CON DEUDA'
              WHERE ROWID = I.ROWID;
          END IF;
          --
          COMMIT;
          --
       EXCEPTION
          WHEN OTHERS THEN
               UPDATE BL_GSI_RELOJ
                  SET DEUDA = LN_DEUDA, SALDO = LN_SALDO, MENSAJE = 'KO' -- ERROR
                WHERE ROWID = I.ROWID;
       END;
       
   END LOOP;
   COMMIT;

END GCP_MQU_RELOJ;

END GCK_TRX_CONCILIA_LINEA_MASIVA;
/
