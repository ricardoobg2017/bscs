CREATE OR REPLACE PACKAGE Migration
IS
/*
**
** References:
**    HLD plsql/40917 Enhance Database Migration Concept
**
** Author: Ingrid Schueler, LH Specifications GmbH
**
** Overview:
**
** This package manages task on the migration.
**
**
** Major Modifications (when, who, what)
**
** 11/98 - ISC - initial version
** 01/99 - PSO - defect 53165: Write error message also with dbms_output.
** 01/99 - ISC - defect 53434: Procedure SelectStartKey changed.
** 04/00 - KSA - defect 71471-1: Support Parallel Processing.
** 07/00 - PSO - defect 81975: MIGUTIL: Enhance utility Migration.Getvalue
** 11/00 - PSO - defect 89865: Separate package body from package header
**                             Support new procedures StartScript, FinishScript
**                             Improve version control
** 02/01 - PSO - pn 00201591:  Enhancements for Dublin.
** 02/08 - KEJ - pn 00201591:  ADD RegisterScript
*/

   /*------------------------- CMVC version control------------------------*/
   cosHeadScriptVersion CONSTANT DabUtil.tosScriptVersion  := '/main/8';
   cosHeadScriptWhat    CONSTANT DabUtil.tosScriptWhat     := 'but_bscs/bscs/database/migutil/orapack/migration.sph, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

   /*------------------------- Package Types ------------------------------*/
   SUBTYPE tosScriptName IS MIG_PROCESS.SCRIPT_NAME%TYPE;
   SUBTYPE tosTrack IS MIG_PROCESS.TRACK%TYPE;
   SUBTYPE tosScriptVersion IS MIG_PROCESS.SCRIPT_VERSION%TYPE;
   SUBTYPE tosScriptWhat IS MIG_PROCESS.SCRIPT_WHAT%TYPE;
   SUBTYPE tosDescription IS MIG_PROCESS.DESCRIPTION%TYPE;

   /*
   ** This record is used as a container for control and statistic information
   ** for a migration step. Its elements need not be accessed in migration scripts.
   */
   TYPE torMigControl IS RECORD
   (
       bReentrantInd      BOOLEAN
      ,nUpperRowNumLimit  INTEGER
      ,nMigParallelNum    INTEGER
      ,sTableName         DabUtil.tosObjectName
      ,sKeyColumnName     DabUtil.tosAttributeName
      ,sKeyMaxValue       DabUtil.tosKeyValue
      ,nKeyCounter        INTEGER
      ,nRowCounter        INTEGER
   );

   /*------------------------- Package Parameters -------------------------*/

   gobCommitInd BOOLEAN := TRUE;
   gobSecureInd BOOLEAN := TRUE;
   gonUpperRowNumLimit INTEGER := 1000;
   gosAppPrefix VARCHAR2(8) := 'SEMA-MIG';
   gonMipErrorCode INTEGER := -20007;

   /*------------------------- Package Modules ----------------------------*/

   /*
   ** =====================================================================
   **
   ** Procedure: RegisterFixScript
   **
   ** Purpose: Make protocol in the migration process table.
   **
   ** =====================================================================
   */

   PROCEDURE RegisterFixScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack DEFAULT ('00000')

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion DEFAULT ('0.0')

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat DEFAULT ('Fix not necessary')

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription DEFAULT ('Fix not necessary')

      -- Reentrant Indicator DEFAULT (TRUE)

      ,piobReentrantInd BOOLEAN DEFAULT ( TRUE )
   );


   /*
   ** =====================================================================
   **
   ** Procedure: StartScript
   **
   ** Purpose: Make protocol in the migration process table.
   **          Raise an error if the migration is already finished.
   **
   ** =====================================================================
   */

   PROCEDURE StartScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription
      -- Reentrant Indicator

      ,piobReentrantInd BOOLEAN DEFAULT ( FALSE )
   );

   /*
   ** =====================================================================
   **
   ** Procedure: FinishScript
   **
   ** Purpose: Set the status of the script to completed.
   **
   ** =====================================================================
   */
   PROCEDURE FinishScript
   (
       -- Script Name
       piosScriptName IN tosScriptName
   );

   /*
   ** =====================================================================
   **
   ** Procedure: StartMigScript
   **
   ** Purpose: Start script for a migration step. This function is only called
   **          if ProcessNumber > 0.
   **          - Validate against MIG_PROCESS
   **          - Get Unix process Identifier
   **          - Update MIG_PROGRESS or (process number = 1)
   **            insert into MIG_PROGRESS if not in parallel mode.
   **
   ** =====================================================================
   */
   PROCEDURE StartMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

      -- Table name
      ,piosTableName IN DabUtil.tosObjectName

      -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

      -- Reentrant Indicator
      ,piobReentrantInd IN BOOLEAN

      -- Continue processing flag
      ,poobContinue  OUT BOOLEAN

       -- Start key for the migration
      ,poosStartKeyValue OUT DabUtil.tosKeyValue

      -- Stop key for the migration
      ,poosStopKeyValue OUT DabUtil.tosKeyValue

      -- Secret migration control structur
      ,poorMigControl OUT torMigControl
   );

   /*
   ** =====================================================================
   **
   ** Procedure: ContinueMigScript
   **
   ** Purpose: Write migration process procedure.
   **          - Get the range of key to be migrated in the next commit cycle
   **          - Maintain entry in MIG_PROGRESS
   **          - Raise an error if the migration is already finished.
   **
   ** =====================================================================
   */
   PROCEDURE ContinueMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

      -- Continue processing
      ,poobContinue OUT BOOLEAN

      -- Start key for the migration
      ,pmosStartKeyValue IN OUT DabUtil.tosKeyValue

      -- Stop key for the migration
      ,pmosStopKeyValue IN OUT DabUtil.tosKeyValue

      -- Migration control structure record
      ,pmorMigControl IN OUT torMigControl
   );

   /*
   ** =====================================================================
   **
   ** Procedure: TransferTable
   **
   ** Purpose: Transfer all data from a source table or view into a target table
   **          using the method "insert into select from"
   **
   ** =====================================================================
   */
   PROCEDURE TransferTable
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

       -- Target table name
      ,piosTargetTableName IN DabUtil.tosObjectName

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   );

   /*
   ** Overload: with already prepared transfer statement
   **           in format INSERT INTO... SELECT FROM...
   */
   PROCEDURE TransferTable
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

       -- Target table name
      ,piosTargetTableName IN DabUtil.tosObjectName

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- Select statement which will be used as source
      ,piosSourceStatement IN DabUtil.tasText

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   );

   /*
   ** =====================================================================
   **
   ** Procedure: SetMigScript
   **
   ** Purpose: Set migration control parameters.
   **          - Insert into tables MIG_PROCESS, MIG_SCRIPT_CONTROL, MIG_PROGRESS.
   **
   ** =====================================================================
   */
   PROCEDURE SetMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Table name
      ,piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Number of parallel processes
      ,pionMigParallelNum IN INTEGER

      -- Commit cycle size
      ,pionUpperRowLimit IN INTEGER
   );

   /*
   ** =====================================================================
   **
   ** Procedure: ProveScript
   **
   ** Purpose: Prove whether a script has been executed with success.
   **
   ** =====================================================================
   */
   PROCEDURE ProveScript
   (
       -- Script Name
       piosScriptName IN tosScriptName

       -- Return Code
      ,poonReturnCode OUT INTEGER
   );

   /*
   ** =====================================================================
   **
   ** Procedure: MigrationErrorLog
   **
   ** Purpose: Write Error message into MIG_ERROR_LOG, raise application error
   **
   ** =====================================================================
   */
   PROCEDURE MigrationErrorLog
   (
       -- Track
       piosTrack IN tosTrack

       -- Script Name
      ,piosScriptName IN tosScriptName

       -- Application Error Text
      ,piosErrorText IN VARCHAR2

       -- Oracle Error Code
      ,piosErrorCode IN VARCHAR2

       -- Oracle Error Text
      ,piosErrorDescription IN VARCHAR2

       -- Process Number
       -- Used to identify Process in case of Parallel Processing.
      ,pionProcessNumber IN INTEGER  DEFAULT 0
   );

   /*
   ** =====================================================================
   **
   ** Procedure: LogMigScript
   **
   ** Purpose: Write Error message into MIG_ERROR_LOG, raise application error
   **
   ** =====================================================================
   */
   PROCEDURE LogMigScript
   (
       -- Script Name
       piosScriptName IN tosScriptName

       -- Process Number
       -- Used to identify Process in case of Parallel Processing.
      ,pionProcessNumber IN INTEGER  DEFAULT 0

       -- Oracle Error Text
      ,piosErrorDescription IN VARCHAR2 DEFAULT NULL

   );

   /*
   ** =====================================================================
   **
   ** Function: GetString
   **
   ** Purpose: Load a configurable migration value of type varchar2
   **
   ** =====================================================================
   */
   FUNCTION GetString
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Default Value String
      ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   )
   RETURN DabUtil.tosParaValue;

   /*
   ** =====================================================================
   **
   ** Function: GetNumber
   **
   ** Purpose: Load a configurable migration value of number type
   **
   ** =====================================================================
   */
   FUNCTION GetNumber
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Default Value String
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   )
   RETURN DabUtil.tonParaValue;

   /*
   ** =====================================================================
   **
   ** Procedure: GetValue
   **
   ** Purpose: Load a configurable migration value
   **
   ** Overload: Character type Values
   ** =====================================================================
   */
   PROCEDURE GetValue
   (
      -- Parameter Key
      piosParaKey IN DabUtil.tosParaKey

      -- Value String
     ,poosParaValue OUT DabUtil.tosParaValue

      -- Default Value String
     ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   );

   /*
   ** Overload: Number type Values
   */
   PROCEDURE GetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value Number
      ,poonParaValue OUT DabUtil.tonParaValue

       -- Default Value Number
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   );


   /*
   ** =====================================================================
   **
   ** Procedure: SetValue
   **
   ** Purpose: Set a configurable migration value
   **
   ** Overload: Character type values
   ** =====================================================================
   */
   PROCEDURE SetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,piosParaValue IN DabUtil.tosParaValue
   );

   /*
   ** Overload: Number type Values
   */
   PROCEDURE SetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,pionParaValue IN DabUtil.tonParaValue
   );

   /*
   ** =====================================================================
   **
   ** Function: TableStorage
   **
   ** Purpose: Provide storage parameters for a table
   **
   ** =====================================================================
   */
   FUNCTION TableStorage
   (
      -- Table Name
       piosTableName IN  DabUtil.tosObjectName

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause;

   /*
   ** =====================================================================
   **
   ** Function: PkeyStorage
   **
   ** Purpose: Provide storage parameters for a primary key constraint
   **
   ** =====================================================================
   */
   FUNCTION PkeyStorage
   (
      -- Table Name
       piosTableName IN  DabUtil.tosObjectName

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause;

   /*
   ** =====================================================================
   **
   ** Function: UkeyStorage
   **
   ** Purpose: Provide storage parameters for a unique key constraint
   **
   ** =====================================================================
   */
   FUNCTION UkeyStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Unique Key Number (enumerate all unique keys a table)
      ,pionUkeyNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause;

   /*
   ** =====================================================================
   **
   ** Function: IndexStorage
   **
   ** Purpose: Provide storage parameters for an index
   **
   ** =====================================================================
   */
   FUNCTION IndexStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Index Number (enumerate all indexes of a table)
      ,pionIndexNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause;

   /*
   ** =====================================================================
   **
   ** Function: PartitionStorage
   **
   ** Purpose: Provide storage parameters for partition of a table
   **
   ** =====================================================================
   */
   FUNCTION PartitionStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Partition Number (enumerate all partitions of a table)
      ,pionPartitionNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause;

   /*
   ** =====================================================================
   **
   ** Procedure: FinishMigScript
   **
   ** Purpose: Set the status of the migration script to completed.
   **
   ** =====================================================================
   */
   PROCEDURE FinishMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   );

END Migration;
/
CREATE OR REPLACE PACKAGE BODY Migration
IS
/*
** Major Modifications (when, who, what)
**
** 11/00 - PSO - defect 85218: Wrong handling in FinishMigration
**                      88489: Raise error in MigrationErrorLog
**                      82202: Change mig.spp (SplitDomainofTable)
**                      89865: Introduce version control, StartScript, FinishScript.
** 02/01 - PSO - pn     00201591: Enhancements for Dublin.
**                                Use migration service package
*/


   /*------------------------- CMVC version control------------------------*/
   cosBodyScriptVersion CONSTANT DabUtil.tosScriptVersion := '/main/15';
   cosBodyScriptWhat    CONSTANT DabUtil.tosScriptWhat    := 'but_bscs/bscs/database/migutil/orapack/migration.spb, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';

   /*
   ** Constants
   */
   cosNewline VARCHAR2(1) := CHR( 10 );

   /* ---------------------------------------------------------------------
   **  Static variables and exceptions
   */

   /*------------------------- Package Constants --------------------------*/
   cosMigStatus VARCHAR2(1) := 'X';
   SUBTYPE  tosMigStatus IS cosMigStatus%TYPE;
   cosStatusCompleted CONSTANT tosMigStatus := 'C';
   cosStatusInitial   CONSTANT tosMigStatus := 'I';
   cosStatusStarted   CONSTANT tosMigStatus := 'S';
   cosStatusException CONSTANT tosMigStatus := 'E';

   /*------------------------- Package Globals ----------------------------*/
   geoApplicationError EXCEPTION;
   gosErrorText VARCHAR2(2000) := NULL;

   /*------------------------- Privat Procedures --------------------------*/

   /*
   **=====================================================
   **
   ** Procedure: CallError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE CallError( piosErrorText IN VARCHAR2 )
   IS
   BEGIN
      RAISE_APPLICATION_ERROR( -20010 , 'MIG-ERROR: ' || piosErrorText, TRUE );
   END CallError;

   /*
   **=====================================================
   **
   ** Procedure: RaiseError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE RaiseError
   (
       piosProcName IN VARCHAR2
      ,piosErrorText IN VARCHAR2 DEFAULT NULL
   )
   IS
      losErrorText VARCHAR2(2000) ;
   BEGIN
      losErrorText := 'MIG-ERROR ' || piosProcName || ' : ' || piosErrorText ;
      RAISE_APPLICATION_ERROR( -20011 , losErrorText , TRUE );
   END RaiseError;

   /*
   **=====================================================
   **
   ** Procedure: CallInfo
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */
   PROCEDURE CallInfo( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( 'REM INFO: ' || piosInfoText );
   END CallInfo;

   /*
   **=====================================================
   **
   ** Procedure: CallWarning
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE CallWarning( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( Substr( 'REM WARNING: ' || piosInfoText || SQLERRM , 1, 255 ) );
   END CallWarning;

   /*
   **=====================================================
   **
   ** Procedure: InitializeProcess
   **
   ** Purpose:  Initialize Migration Process
   **           Set application information in V$SESSION
   **           Request a exclusive lock
   **
   **=====================================================
   */
   FUNCTION InitializeProcess
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER DEFAULT (0)
   )
   RETURN BOOLEAN
   IS
     losProcessIdentifier VARCHAR2( 64 );
     losLockHandle        VARCHAR2( 128 );
     lonLockResult        INTEGER;
     lobDeadlock          BOOLEAN DEFAULT FALSE;
   BEGIN
      losProcessIdentifier := substr( gosAppPrefix || '#' || piosScriptName || '#' || pionProcessNumber , 1 , 64 );

      dbms_application_info.set_client_info( losProcessIdentifier );
      dbms_lock.allocate_unique( losProcessIdentifier , losLockHandle );

      lonLockResult := dbms_lock.request( losLockHandle , dbms_lock.x_mode , 0 );
      IF 0 != lonLockResult
      THEN
-- PN214574
         CallInfo( 'Request for exlusive lock ' || losProcessIdentifier || ' returned ' || lonLockResult );
         CallInfo( 'Probably script is already running.' );
         lobDeadlock := TRUE;
      END IF;
      RETURN lobDeadlock;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'InitializeProcess' );
   END InitializeProcess;

   /*
   **=====================================================
   **
   ** Procedure: CleanProcess
   **
   ** Purpose:  Clean up  Migration Process
   **           Reset application information in V$SESSION (to NULL)
   **           Release  the exclusive lock
   **
   **=====================================================
   */
   PROCEDURE CleanProcess
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER DEFAULT (0)
   )
   IS
     losProcessIdentifier VARCHAR2( 64 );
     losLockHandle        VARCHAR2( 128 );
     lonLockResult        INTEGER;
   BEGIN
      losProcessIdentifier := substr( gosAppPrefix || '#' || piosScriptName || '#' || pionProcessNumber , 1 , 64 );

      dbms_application_info.set_client_info( NULL );

      dbms_lock.allocate_unique( losProcessIdentifier , losLockHandle );

      lonLockResult := dbms_lock.release( losLockHandle );
      IF 0 != lonLockResult
      THEN
         CallWarning( 'Release of lock ' || losProcessIdentifier || ' returned ' || lonLockResult );
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CleanProcess' );
   END CleanProcess;

   /*
   ** =====================================================================
   **
   ** Procedure: GetMigControl
   **
   ** Purpose: Get migration status and control parameters.
   **          - Read table MIG_PROCESS
   **          - Read table MIG_SCRIPT_CONTROL.
   **          - Evaluate and check settings.
   **          - Maintain tables MIG_PROCESS and MIG_SCRIPT_CONTROL
   **            in the non-parallel case
   **
   ** =====================================================================
   */
   PROCEDURE GetMigControl
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

      -- Table name
      ,piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

      -- Reentrant Indicator
      ,piobReentrantInd BOOLEAN DEFAULT ( FALSE )

      -- Migration control structur
      ,poorMigControl OUT torMigControl
   )
   IS
      lobMigProcessFound     BOOLEAN := TRUE;
      lobMigControlFound      BOOLEAN := FALSE;
      lobUpdateMigProcess    BOOLEAN := FALSE;
      lobUpdateMigControl    BOOLEAN := FALSE;
      losMigStatus           tosMigStatus;
      losScriptVersion       DabUtil.tosScriptVersion;
      lorMigControl          torMigControl;
   BEGIN

      BEGIN
         SELECT  MIG_STATUS
                ,SCRIPT_VERSION
         INTO    losMigStatus
                ,losScriptVersion
         FROM    MIG_PROCESS
         WHERE  SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobMigProcessFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      IF lobMigProcessFound
      THEN
         lobMigControlFound := TRUE;
         ------------------------------------------------------------------------
         -- Check status.
         IF losMigStatus = cosStatusCompleted
         THEN
            IF piobReentrantInd
            THEN
               CallInfo( 'Script had already been executed.' );
            ELSIF gobSecureInd
            THEN
               CallError( 'Script is already executed.' );
            ELSE
               CallWarning( 'Script had already been executed.' );
            END IF;

         ELSIF losMigStatus NOT IN ( cosStatusStarted, cosStatusInitial , cosStatusException )
         THEN
           CallError( 'Unknown migration status: ' || losMigStatus );
         END IF;

         ------------------------------------------------------------------------
         -- Check script version
         IF losScriptVersion != piosScriptVersion
         THEN
            CallWarning( 'Mismatch in script version. Version in table MIG_PROCESS was: '
                          || losScriptVersion );
         END IF;

         ------------------------------------------------------------------------
         -- Read migration parameter

         BEGIN
            SELECT  MIG_PARALLEL_NUM
                   ,UPPER_ROWNUM_LIMIT
                   ,MIG_TABLE_NAME
                   ,MIG_KEY_NAME
            INTO    lorMigControl.nMigParallelNum
                   ,lorMigControl.nUpperRowNumLimit
                   ,lorMigControl.sTableName
                   ,lorMigControl.sKeyColumnName
            FROM    MIG_SCRIPT_CONTROL
            WHERE  SCRIPT_NAME = piosScriptName;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lobMigControlFound := FALSE;
            WHEN OTHERS
            THEN
               CallError( 'Select from MIG_SCRIPT_CONTROL' );
         END;

         IF lobMigControlFound
         THEN

            ------------------------------------------------------------------------
            -- Check process number
            IF  lorMigControl.nMigParallelNum < pionProcessNumber
            THEN
               CallError( 'Process number has to be less than MIG_PARALLEL_NUM = '
                           || lorMigControl.nMigParallelNum );
            END IF;

         END IF;

      END IF;

      IF NOT lobMigControlFound
      AND pionProcessNumber = 1
      THEN
         -- This is the single process insert mode
         IF NOT lobMigProcessFound
         THEN
            -- make protocol into the migration process table
            INSERT INTO MIG_PROCESS
            (
                TRACK
               ,SCRIPT_NAME
               ,SCRIPT_VERSION
               ,SCRIPT_WHAT
               ,MIG_STATUS
               ,DESCRIPTION
               ,START_DATE
            )
            VALUES
            (
                 piosTrack
                ,piosScriptName
                ,piosScriptVersion
                ,piosScriptWhat
                ,cosStatusStarted
                ,piosDescription
                ,SYSDATE
            );
         ELSE
            lobUpdateMigProcess := TRUE;
         END IF;

         BEGIN
            INSERT INTO MIG_SCRIPT_CONTROL
            (
                SCRIPT_NAME
               ,MIG_TABLE_NAME
               ,MIG_KEY_NAME
            )
            VALUES
            (
                piosScriptName
               ,piosTableName
               ,piosKeyColumn
            );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Insert into MIG_SCRIPT_CONTROL' );
         END;
         lorMigControl.nMigParallelNum := 1;

      ELSIF lobMigControlFound
      AND   lorMigControl.nMigParallelNum = 1
      THEN
         ------------------------------------------------------------------------
         -- This is the single process update mode
         lobUpdateMigProcess := TRUE;
         lobUpdateMigControl := TRUE;


         ------------------------------------------------------------------------
         -- Check table and key column name
         IF  lorMigControl.sTableName != piosTableName
         OR  lorMigControl.sKeyColumnName != piosKeyColumn
         THEN
            CallWarning( 'Inconsistent setup of table name or key column.' );
         END IF;

      ELSIF lobMigControlFound
      AND   lorMigControl.nMigParallelNum > 1
      THEN
         -- This is the parallel process mode
         -- In this case none of the parallel processes may operate
         -- on MIG_PROCESS and MIG_CONFIG

         ------------------------------------------------------------------------
         -- Check script version
         IF losScriptVersion != piosScriptVersion
         THEN
            CallError( 'Mismatch in script version. Version in table MIG_PROCESS was: '
                          || losScriptVersion );
         END IF;

         ------------------------------------------------------------------------
         -- Check table and key column name
         IF  lorMigControl.sTableName != piosTableName
         OR  lorMigControl.sKeyColumnName != piosKeyColumn
         THEN
            CallError( 'Inconsistent setup of table name or key column.' );
         END IF;

      ELSIF NOT lobMigControlFound
      AND   pionProcessNumber > 1
      THEN
         -- This is the last case where some one tried to run a parallel
         -- process without setting up MIG_CONTROL and MIG_PROCESS
         CallError( 'Setup of MIG_CONTROL missing for parallel mode.' );
      ELSE
         -- This is an unexpected case, perhapts the parameters are inconsistent.
            CallError( 'Unexpected migration step.' );
      END IF;

      IF lobUpdateMigProcess
      THEN
         ------------------------------------------------------------------------
         -- Check script version
         IF losScriptVersion != piosScriptVersion
         THEN
            CallWarning( 'Mismatch in script version. Version in table MIG_PROCESS was: '
                          || losScriptVersion );
         END IF;

         ------------------------------------------------------------------------
         -- set start date to the current date
         UPDATE  MIG_PROCESS
            SET  MIG_STATUS = cosStatusStarted
                ,SCRIPT_VERSION = piosScriptVersion
                ,SCRIPT_WHAT = piosScriptWhat
                ,TRACK = piosTrack
                ,START_DATE = SYSDATE
                ,DESCRIPTION = piosDescription
          WHERE SCRIPT_NAME = piosScriptName;
      END IF;

      IF lobUpdateMigControl
      THEN
         ------------------------------------------------------------------------
         -- Check table and key column name
         IF  lorMigControl.sTableName != piosTableName
         OR  lorMigControl.sKeyColumnName != piosKeyColumn
         THEN
            CallWarning( 'Inconsistent setup of table name or key column.' );
            UPDATE MIG_SCRIPT_CONTROL
            SET    MIG_TABLE_NAME = piosTableName
                  ,MIG_KEY_NAME = piosKeyColumn
            WHERE  SCRIPT_NAME = piosScriptName;
         END IF;
      END IF;

      ------------------------------------------------------------------------
      -- Set Control structure
      lorMigControl.sTableName := piosTableName;
      lorMigControl.sKeyColumnName := piosKeyColumn;

      ------------------------------------------------------------------------
      -- Get upper limit for ROWNUM
      IF lorMigControl.nUpperRowNumLimit IS NULL
      THEN
         lorMigControl.nUpperRowNumLimit := gonUpperRowNumLimit;
      END IF;

      ------------------------------------------------------------------------
      -- populate return structur
      poorMigControl := lorMigControl;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetMigControl' , 'Script Name: ' || piosScriptName || ' Process Number: ' || pionProcessNumber );
   END GetMigControl;

   /*------------------------- Public Modules -----------------------------*/
   /*
   **  CMVC version control
   */
   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN cosBodyScriptVersion || '|' || cosBodyScriptWhat;
   END PrintBodyVersion;


   /*
   ** =====================================================================
   **
   ** Procedure: RegisterFixScript
   **
   ** Purpose: Make protocol in the migration process table.
   **
   ** =====================================================================
   */
   PROCEDURE RegisterFixScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack DEFAULT ('00000')

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion DEFAULT ('0.0')

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat DEFAULT ('Fix not necessary')

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription DEFAULT ('Fix not necessary')

      -- Reentrant Indicator DEFAULT (TRUE)

      ,piobReentrantInd BOOLEAN DEFAULT ( TRUE )
   )
   IS
      lobDataFound        BOOLEAN := TRUE;
      losMigStatus        tosMigStatus;
      losScriptVersion    DabUtil.tosScriptVersion;
   BEGIN
      IF piosScriptName IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      END IF;

      BEGIN
         SELECT  MIG_STATUS
                ,SCRIPT_VERSION
         INTO    losMigStatus
                ,losScriptVersion
         FROM    MIG_PROCESS
         WHERE  SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      IF lobDataFound
      THEN
         IF losMigStatus = cosStatusCompleted
         THEN
            IF piobReentrantInd
            THEN
               CallInfo( 'Script had already been executed.' );
            ELSIF gobSecureInd
            THEN
               CallError( 'Script is already executed.' );
            ELSE
               CallWarning( 'Script had already been executed.' );
            END IF;

         END IF;

      ELSE

        -- make protocol into the migration process table
        INSERT INTO MIG_PROCESS (
                TRACK,
                SCRIPT_NAME,
                SCRIPT_VERSION,
                SCRIPT_WHAT,
                MIG_STATUS,
                START_DATE,
                DESCRIPTION)
         VALUES (
                piosTrack,
                piosScriptName,
                piosScriptVersion,
                piosScriptWhat,
                cosStatusCompleted,
                SYSDATE,
                piosDescription
                );

      END IF;

      CallInfo( 'Register script ' || piosScriptName
                  || '( ' || piosScriptVersion || ' ). '
                  || piosDescription || '.' );
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'RegisterFixScript' , 'Script Name: ' || piosScriptName );
   END RegisterFixScript;




   /*
   ** =====================================================================
   **
   ** Procedure: StartScript
   **
   ** Purpose: Make protocol in the migration process table.
   **          Raise an error if the migration is already finished.
   **
   ** =====================================================================
   */
   PROCEDURE StartScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

      -- Reentrant Indicator
      ,piobReentrantInd BOOLEAN DEFAULT ( FALSE )
   )
   IS
      lobDataFound        BOOLEAN := TRUE;
      losMigStatus        tosMigStatus;
      losScriptVersion    DabUtil.tosScriptVersion;
   BEGIN
      IF piosScriptName IS NULL
      OR piosTrack IS NULL
      OR piosScriptVersion IS NULL
      OR piosScriptWhat IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      END IF;

      BEGIN
         SELECT  MIG_STATUS
                ,SCRIPT_VERSION
         INTO    losMigStatus
                ,losScriptVersion
         FROM    MIG_PROCESS
         WHERE  SCRIPT_NAME = piosScriptName
         FOR UPDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      IF lobDataFound
      THEN
         IF losMigStatus = cosStatusCompleted
         THEN
            IF piobReentrantInd
            THEN
               CallInfo( 'Script had already been executed.' );
            ELSIF gobSecureInd
            THEN
               CallError( 'Script is already executed.' );
            ELSE
               CallWarning( 'Script had already been executed.' );
            END IF;

         ELSIF losMigStatus != cosStatusStarted
         THEN
           CallError( 'Unknown migration status: ' || losMigStatus );
         END IF;

         ------------------------------------------------------------------------
         -- Check script version
         IF losScriptVersion != piosScriptVersion
         THEN
            CallWarning( 'Mismatch in script version. Version in table MIG_PROCESS was: '
                          || losScriptVersion );
         END IF;

         -- set start date to the current date
         -- column DESCRIPTION added by PN 00224696
         UPDATE  MIG_PROCESS
            SET  MIG_STATUS = cosStatusStarted
                ,SCRIPT_VERSION = piosScriptVersion
                ,SCRIPT_WHAT = piosScriptWhat
                ,START_DATE = SYSDATE
                ,DESCRIPTION = piosDescription
          WHERE SCRIPT_NAME = piosScriptName;

      ELSE

        -- make protocol into the migration process table
        INSERT INTO MIG_PROCESS (
                TRACK,
                SCRIPT_NAME,
                SCRIPT_VERSION,
                SCRIPT_WHAT,
                MIG_STATUS,
                START_DATE,
                DESCRIPTION)
         VALUES (
                piosTrack,
                piosScriptName,
                piosScriptVersion,
                piosScriptWhat,
                cosStatusStarted,
                SYSDATE,
                piosDescription
                );

      END IF;

      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      CallInfo( 'Start script ' || piosScriptName
                  || '( ' || piosScriptVersion || ' ). '
                  || piosDescription || '.' );
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'StartScript' , 'Script Name: ' || piosScriptName );
   END StartScript;

   /*
   ** =====================================================================
   **
   ** Procedure: FinishScript
   **
   ** Purpose: Set the status of the script to completed.
   **
   ** =====================================================================
   */
   PROCEDURE FinishScript
   (
       -- Script Name
       piosScriptName IN tosScriptName
   )
   IS
      lobDataFound        BOOLEAN := TRUE;
      losMigStatus        tosMigStatus;
   BEGIN

      IF piosScriptName IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      END IF;

      BEGIN
         SELECT  MIG_STATUS
         INTO    losMigStatus
         FROM    MIG_PROCESS
         WHERE  SCRIPT_NAME = piosScriptName
         FOR UPDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      IF lobDataFound
      THEN
         -- Determine status
         IF losMigStatus = cosStatusStarted
         THEN
            UPDATE MIG_PROCESS
            SET    MIG_STATUS = cosStatusCompleted
                  ,FINISH_DATE = SYSDATE
            WHERE  SCRIPT_NAME = piosScriptName;
         ELSIF losMigStatus = cosStatusCompleted
         THEN
            CallError( 'Migration status is already completed.' );
         ELSE
            CallError( 'Migration status: '
                            || losMigStatus
                            || ' invalid in this context.' );
         END IF;
      ELSE
         CallError( 'No entry found in MIG_PROCESS' );
      END IF;

      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      CallInfo( 'Finish script ' || piosScriptName || '.' );

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'FinishScript', 'Script Name: ' || piosScriptName );
   END FinishScript;

   /*
   ** =====================================================================
   **
   ** Procedure: ProveScript
   **
   ** Purpose: Prove whether a script has been executed with success.
   **
   ** =====================================================================
   */
   PROCEDURE ProveScript
   (
       -- Script Name
       piosScriptName IN tosScriptName

       -- Return Code
      ,poonReturnCode OUT INTEGER
   )
   IS
      lobDataFound        BOOLEAN := TRUE;
      losMigStatus        tosMigStatus;
      losScriptVersion    tosScriptVersion;
   BEGIN
      poonReturnCode := 1;
      IF piosScriptName IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      END IF;

      BEGIN
         SELECT  MIG_STATUS
                ,SCRIPT_VERSION
         INTO    losMigStatus
                ,losScriptVersion
         FROM    MIG_PROCESS
         WHERE  SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      IF lobDataFound
      THEN
         -- Determine status
         IF losMigStatus = cosStatusCompleted
         THEN
            poonReturnCode := 0;
            CallInfo( 'Status of script ' || piosScriptName
                       || 'is COMPLETED' );
         ELSE
            CallInfo( 'Status of script ' || piosScriptName
                       || 'is NOT COMPLETED' );
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'ProveScript' , 'Script Name: ' || piosScriptName );
   END ProveScript;

   /*
   ** =====================================================================
   **
   ** Procedure: MigrationErrorLog
   **
   ** Purpose: Write Error message into MIG_ERROR_LOG, raise application error
   **
   ** =====================================================================
   */
   PROCEDURE MigrationErrorLog
   (
       -- Track
       piosTrack IN tosTrack

       -- Script Name
      ,piosScriptName IN tosScriptName

       -- Application Error Text
      ,piosErrorText IN VARCHAR2

       -- Oracle Error Code
      ,piosErrorCode IN VARCHAR2

       -- Oracle Error Text
      ,piosErrorDescription IN VARCHAR2

       -- Process Number
       -- Used to identify Process in case of Parallel Processing.
      ,pionProcessNumber IN INTEGER  DEFAULT 0
   )
   IS
     lonProcessNumber INTEGER;
     losErrorText     VARCHAR2(4000);
   BEGIN
      lonProcessNumber := nvl(pionProcessNumber,0);

      IF gobCommitInd
      THEN
         ROLLBACK;
      END IF;


      INSERT INTO MIG_ERROR_LOG
      (
          TRACK
         ,SCRIPT_NAME
         ,PROCESS_NUMBER
         ,ENTRY_DATE
         ,ERROR_TEXT
         ,ERROR_CODE
         ,ERROR_DES
       )
       VALUES
       (
           piosTrack
          ,piosScriptName
          ,lonProcessNumber
          ,SYSDATE
          ,piosErrorText
          ,piosErrorCode
          ,piosErrorDescription
       );

--  Commit transaction

      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      RAISE geoApplicationError;

   EXCEPTION
      WHEN OTHERS
      THEN
         losErrorText := cosNewline ||
              'MIG-ERROR in script ' || piosScriptName || cosNewline ||
              'Track: ' || piosTrack || cosNewline ||
              'Error Code: ' || piosErrorCode || cosNewline ||
              'Error Text: ' || piosErrorText || cosNewline ||
              'Error Description: ' || piosErrorDescription ;
         RAISE_APPLICATION_ERROR( -20013 , losErrorText , TRUE );

   END MigrationErrorLog;

   /*
   ** =====================================================================
   **
   ** Procedure: LogMigScript
   **
   ** Purpose: Write Error message into MIG_ERROR_LOG, raise application error
   **
   ** =====================================================================
   */
   PROCEDURE LogMigScript
   (
       -- Script Name
       piosScriptName IN tosScriptName

       -- Process Number
       -- Used to identify Process in case of Parallel Processing.
      ,pionProcessNumber IN INTEGER  DEFAULT 0

       -- Oracle Error Text
      ,piosErrorDescription IN VARCHAR2 DEFAULT NULL

   )
   IS
     losErrorText     VARCHAR2(4000);
     losErrorCode     INTEGER;
     leoScriptError   EXCEPTION;
   BEGIN
      losErrorText := SQLERRM;
      losErrorCode := SQLCODE;

      IF gobCommitInd
      THEN
         ROLLBACK;
      END IF;


      INSERT INTO MIG_ERROR_LOG
      (
          SCRIPT_NAME
         ,PROCESS_NUMBER
         ,ENTRY_DATE
         ,ERROR_TEXT
         ,ERROR_CODE
         ,ERROR_DES
       )
       VALUES
       (
           piosScriptName
          ,NVL( pionProcessNumber, 0 )
          ,SYSDATE
          ,losErrorText
          ,losErrorCode
          ,piosErrorDescription
       );


      IF pionProcessNumber > 0
      THEN

         ------------------------------------------------------------------------
         -- Set migration status to exception.
         UPDATE MIG_PROGRESS
         SET    MIG_STATUS = cosStatusException
         WHERE  SCRIPT_NAME = piosScriptName
         AND    PROCESS_NUMBER = pionProcessNumber
         AND    MIG_STATUS != cosStatusCompleted;

      ELSE
         UPDATE MIG_PROCESS
         SET    MIG_STATUS = cosStatusException
         WHERE  SCRIPT_NAME = piosScriptName
         AND    MIG_STATUS != cosStatusCompleted;
      END IF;

      --  Commit transaction
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      ------------------------------------------------------------------------
      -- Clean up process, release lock.
      IF pionProcessNumber > 0
      THEN
         CleanProcess
         (
             piosScriptName
            ,pionProcessNumber
         );
      END IF;

      ------------------------------------------------------------------------
      -- Propagate the exception
      RAISE leoScriptError;

   EXCEPTION
      WHEN leoScriptError
      THEN
         losErrorText := 'MIG-ERROR in script ' || piosScriptName
                          || ' for process ' || pionProcessNumber
                          || ' : ' || piosErrorDescription || cosNewline
                          || losErrorText;
         RAISE_APPLICATION_ERROR( -20013 , losErrorText , FALSE );
      WHEN OTHERS
      THEN
         losErrorText := 'MIG-ERROR in migration utility : ' || piosScriptName || ' for process ' || pionProcessNumber;
         RAISE_APPLICATION_ERROR( -20014 , losErrorText , TRUE );

   END LogMigScript;

   /*
   ** =====================================================================
   **
   ** Procedure: StartMigScript
   **
   ** Purpose: Start script for a migration step. This function is only called
   **          if ProcessNumber > 0.
   **          - Validate against MIG_PROCESS
   **          - Get Unix process Identifier
   **          - Update MIG_PROGRESS or (process number = 1)
   **            insert into MIG_PROGRESS if not in parallel mode.
   **
   ** =====================================================================
   */
   PROCEDURE StartMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

      -- Table name
      ,piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

      -- Reentrant Indicator
      ,piobReentrantInd IN BOOLEAN

      -- Continue processing flag
      ,poobContinue  OUT BOOLEAN

       -- Start key for the migration
      ,poosStartKeyValue OUT DabUtil.tosKeyValue

      -- Stop key for the migration
      ,poosStopKeyValue OUT DabUtil.tosKeyValue

      -- Secret migration control structur
      ,poorMigControl OUT torMigControl
   )
   IS
      lorMigControl       torMigControl;
      losStartKeyValue    DabUtil.tosKeyValue;
      losStopKeyValue     DabUtil.tosKeyValue;
      lobDataFound        BOOLEAN := TRUE;
      losKeyMaxValue      DabUtil.tosKeyValue;
      losKeyInfValue      DabUtil.tosKeyValue;
      losKeyLastValue     DabUtil.tosKeyValue;
      losMigStatus        tosMigStatus;
      losNewMigStatus     tosMigStatus;
      losScriptVersion    DabUtil.tosScriptVersion;
      lonNumberOfRows     INTEGER;
      lonNumberOfKeys     INTEGER;
      lobReloadProgress   BOOLEAN := FALSE ;
   BEGIN
      ------------------------------------------------------------------------
      -- Check input parameter
      IF piosScriptName IS NULL
      OR piosTrack IS NULL
      OR piosScriptVersion IS NULL
      OR piosScriptWhat IS NULL
      OR pionProcessNumber IS NULL
      OR piosTableName IS NULL
      OR piosKeyColumn IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      ELSIF pionProcessNumber < 0
      THEN
         CallError( 'Invalid process number.' );
      END IF;

      ------------------------------------------------------------------------
      -- Initialize process, request lock.
-- PN214574
      IF InitializeProcess
            (
              piosScriptName
             ,pionProcessNumber
            )
      THEN
        poobContinue := FALSE;
        RETURN;
      END IF;
      ------------------------------------------------------------------------
      -- Get process status
      -- Read information from MIG_SCRIPT_CONTROL
      GetMigControl
      (
          piosScriptName
         ,pionProcessNumber
         ,piosTableName
         ,piosKeyColumn
         ,piosTrack
         ,piosScriptVersion
         ,piosScriptWhat
         ,piosDescription
         ,piobReentrantInd
         ,lorMigControl
      );

      ------------------------------------------------------------------------
      -- Read information from MIG_PROGRESS
      BEGIN
         SELECT  KEY_MAX_VALUE
                ,KEY_INFIMUM_VALUE
                ,KEY_LAST_COMMITED_VALUE
                ,MIG_STATUS
         INTO   losKeyMaxValue
               ,losKeyInfValue
               ,losKeyLastValue
               ,losMigStatus
         FROM   MIG_PROGRESS
         WHERE  SCRIPT_NAME = piosScriptName
         AND    PROCESS_NUMBER = pionProcessNumber;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROGRESS' );
      END;

      IF lobDataFound = TRUE
      THEN

         IF losMigStatus IN ( cosStatusStarted , cosStatusException )
         THEN
            losKeyInfValue := NVL( losKeyLastValue, losKeyInfValue );
         ELSIF losMigStatus = cosStatusInitial
         THEN
            lobReloadProgress := TRUE;
         ELSIF losMigStatus = cosStatusCompleted
         THEN
            IF piobReentrantInd
            OR NOT gobSecureInd
            THEN
               CallInfo( 'Script had already been executed.' );
               lobReloadProgress := TRUE;
            ELSE
-- PN214574
               CallInfo( 'Script has been already executed.' );
               poobContinue := FALSE;
               RETURN;
            END IF;
         ELSE
            CallError( 'Unknown migration status: ' || losMigStatus );
         END IF;

      ELSIF lorMigControl.nMigParallelNum = 1
      THEN
         losKeyInfValue := NULL;
         losKeyMaxValue := NULL;
      ELSE
         CallError( 'No entry in MIG_PROGRESS found.' );
      END IF;

      IF lorMigControl.nMigParallelNum > 1
      AND losKeyInfValue IS NULL
      AND losKeyMaxValue IS NULL
      THEN
         CallError( 'Inconsistent setup of MIG_PROGRESS: KEY_INFIMUM_VALUE and KEY_MAX_VALUE are NULL.' );
      END IF;

      ------------------------------------------------------------------------
      -- Get start and stop key
      lonNumberOfRows := lorMigControl.nUpperRowNumLimit;
      MigService.GetKeyRange
      (
          lorMigControl.sTableName
         ,lorMigControl.sKeyColumnName
         ,losKeyInfValue
         ,losKeyMaxValue
         ,lonNumberOfRows
         ,losStartKeyValue
         ,losStopKeyValue
         ,lonNumberOfKeys
      );

      ------------------------------------------------------------------------
      -- Determine if there are any rows to be migrated.
      IF lonNumberOfKeys = 0
      THEN
         losNewMigStatus := cosStatusCompleted;
         poobContinue := FALSE;
      ELSE
         losNewMigStatus := cosStatusStarted;
         poobContinue := TRUE;
      END IF;

      IF lobDataFound = TRUE
      THEN
         IF lobReloadProgress
         THEN
            UPDATE  MIG_PROGRESS
            SET     MIG_STATUS = losNewMigStatus
                   ,MODIFY_DATE = SYSDATE
                   ,START_DATE = SYSDATE
                   ,KEY_LAST_COMMITED_VALUE = NULL
                   ,KEY_COUNTER = NULL
                   ,ROW_COUNTER = NULL
            WHERE   SCRIPT_NAME = piosScriptName
            AND     PROCESS_NUMBER = pionProcessNumber;
         ELSE
            UPDATE  MIG_PROGRESS
            SET     MIG_STATUS = losNewMigStatus
                   ,MODIFY_DATE = SYSDATE
                   ,START_DATE = SYSDATE
            WHERE   SCRIPT_NAME = piosScriptName
            AND     PROCESS_NUMBER = pionProcessNumber;
         END IF;
      ELSE
         ------------------------------------------------------------------------
         -- Make initial entry into MIG_PROGRESS
         BEGIN
            INSERT INTO MIG_PROGRESS
            (
                SCRIPT_NAME
               ,PROCESS_NUMBER
               ,MIG_STATUS
               ,MODIFY_DATE
               ,START_DATE
            )
            VALUES
            (
                 piosScriptName
                ,pionProcessNumber
                ,losNewMigStatus
                ,SYSDATE
                ,SYSDATE
            );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Insert into MIG_PROGRESS' );
         END;
      END IF;


      ----------------------------------------------------------------
      -- Set return parameters
      lorMigControl.nKeyCounter := lonNumberOfKeys;
      lorMigControl.nRowCounter := lonNumberOfRows;
      lorMigControl.sKeyMaxValue := losKeyMaxValue;
      poorMigControl := lorMigControl;
      poosStartKeyValue := losStartKeyValue;
      poosStopKeyValue  := losStopKeyValue;

      ------------------------------------------------------------------------
      -- Maintain status in MIG_PROCESS in case of the single process
      -- and no rows to be migrated
      IF 1 = lorMigControl.nMigParallelNum
      AND lonNumberOfKeys = 0
      THEN
         BEGIN
            UPDATE MIG_PROCESS
            SET    MIG_STATUS = cosStatusCompleted
                  ,FINISH_DATE = SYSDATE
            WHERE  SCRIPT_NAME = piosScriptName
            AND    MIG_STATUS = cosStatusStarted;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Update MIG_PROCESS' );
         END;
      END IF;

      ----------------------------------------------------------------
      -- Print information log
      CallInfo( 'StartMigScript: ' || piosScriptName
                || ', Process Number: ' || pionProcessNumber
                || ', Script Version: ' || piosScriptVersion );

      CallInfo( 'Table  ' || poorMigControl.sTableName
                || ', Infimum Key: ' || NVL( losKeyInfValue, 'NONE' )
                || ', Max Key: ' || NVL( lorMigControl.sKeyMaxValue, 'NONE' )
                || ', Start Key: ' || NVL( poosStartKeyValue, 'NONE' ) );

      ----------------------------------------------------------------
      -- Commit transaction
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      IF NOT poobContinue
      THEN
         CallInfo( 'Table  ' || poorMigControl.sTableName || ': Migration for process '
                             || pionProcessNumber || ' already finished.' );

         ------------------------------------------------------------------------
         -- Clean up process, release lock.
         CleanProcess
         (
             piosScriptName
            ,pionProcessNumber
         );
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'StartMigScript' , 'Script Name: ' || piosScriptName || ' Process Number: ' || pionProcessNumber );
   END StartMigScript;

   /*
   ** =====================================================================
   **
   ** Procedure: ContinueMigScript
   **
   ** Purpose: Write migration process procedure.
   **          - Get the range of key to be migrated in the next commit cycle
   **          - Maintain entry in MIG_PROGRESS
   **          - Raise an error if the migration is already finished.
   **
   ** =====================================================================
   */
   PROCEDURE ContinueMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

      -- Continue processing
      ,poobContinue OUT BOOLEAN

      -- Start key for the migration
      ,pmosStartKeyValue IN OUT DabUtil.tosKeyValue

      -- Stop key for the migration
      ,pmosStopKeyValue IN OUT DabUtil.tosKeyValue

      -- Migration control structure record
      ,pmorMigControl IN OUT torMigControl
   )
   IS
      losMigStatus     tosMigStatus;
      lonNumberOfRows  INTEGER;
      lonNumberOfKeys  INTEGER;
      losStartKeyValue DabUtil.tosKeyValue;
      losStopKeyValue  DabUtil.tosKeyValue;
      lobContinue      BOOLEAN;
   BEGIN

      ------------------------------------------------------------------------
      -- Get start and stop key
      lonNumberOfRows := pmorMigControl.nUpperRowNumLimit;
      MigService.GetKeyRange
      (
          pmorMigControl.sTableName
         ,pmorMigControl.sKeyColumnName
         ,pmosStopKeyValue
         ,pmorMigControl.sKeyMaxValue
         ,lonNumberOfRows
         ,losStartKeyValue
         ,losStopKeyValue
         ,lonNumberOfKeys
      );

      ------------------------------------------------------------------------
      -- Determine Status and continue flag
      IF lonNumberOfKeys = 0
      THEN
           losMigStatus := cosStatusCompleted;
           poobContinue := FALSE;
      ELSE
           losMigStatus := cosStatusStarted;
           poobContinue := TRUE;
      END IF;

      ------------------------------------------------------------------------
      -- Make progress entry
      BEGIN
         UPDATE MIG_PROGRESS
         SET  MIG_STATUS = losMigStatus
             ,KEY_LAST_COMMITED_VALUE = pmosStopKeyValue
             ,KEY_COUNTER = NVL( KEY_COUNTER, 0 ) + pmorMigControl.nKeyCounter
             ,ROW_COUNTER = NVL( ROW_COUNTER, 0 ) + pmorMigControl.nRowCounter
             ,MODIFY_DATE = SYSDATE
         WHERE  SCRIPT_NAME = piosScriptName
         AND    PROCESS_NUMBER = pionProcessNumber
         AND    MIG_STATUS = cosStatusStarted;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Update MIG_PROGRESS' );
      END;

      IF SQL%NOTFOUND
      THEN
          CallError( 'No entry in MIG_PROGRESS' );
      END IF;


      ------------------------------------------------------------------------
      -- Maintain status in MIG_PROCESS in the single process case
      IF 1 = pmorMigControl.nMigParallelNum
      AND cosStatusCompleted = losMigStatus
      THEN
         BEGIN
            UPDATE MIG_PROCESS
            SET    MIG_STATUS = cosStatusCompleted
                  ,FINISH_DATE = SYSDATE
            WHERE  SCRIPT_NAME = piosScriptName
            AND    MIG_STATUS = cosStatusStarted;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Update MIG_PROCESS' );
         END;
      END IF;

      ------------------------------------------------------------------------
      -- Report success
      CallInfo( 'Table  ' || pmorMigControl.sTableName
                 || ': Number of Rows = ' || NVL( pmorMigControl.nRowCounter, 0 )
                 || ', Number of Keys = ' || NVL( pmorMigControl.nKeyCounter, 0 )
                 || ', Committed Value = ' || NVL( pmosStopKeyValue , 'NONE' ) );

      ------------------------------------------------------------------------
      -- Set return parameters
      pmorMigControl.nKeyCounter := lonNumberOfKeys;
      pmorMigControl.nRowCounter := lonNumberOfRows;
      pmosStartKeyValue  := losStartKeyValue;
      pmosStopKeyValue   := losStopKeyValue;

      ------------------------------------------------------------------------
      -- Commit this migration cycle
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

      IF NOT poobContinue
      THEN
         CallInfo( 'Table  ' || pmorMigControl.sTableName
                   || ': Migration for process ' || pionProcessNumber || ' finished.' );

         ------------------------------------------------------------------------
         -- Clean up process, release lock.
         CleanProcess
         (
             piosScriptName
            ,pionProcessNumber
         );
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'ContinueMigScript' , 'Script Name: ' || piosScriptName || ' Process Number: ' || pionProcessNumber);
   END ContinueMigScript;

   /*
   ** =====================================================================
   **
   ** Procedure: SetMigScript
   **
   ** Purpose: Set migration control parameters.
   **          - Insert into tables MIG_PROCESS, MIG_SCRIPT_CONTROL, MIG_PROGRESS.
   **
   ** =====================================================================
   */
   PROCEDURE SetMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Table name
      ,piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Number of parallel processes
      ,pionMigParallelNum IN INTEGER

      -- Commit cycle size
      ,pionUpperRowLimit IN INTEGER
   )
   IS
      losMigStatus       tosMigStatus;
      lobDataFound       BOOLEAN := TRUE;
      lonProcessNumber   INTEGER;
      losKeyMaxValue     DabUtil.tosKeyValue;
      lonTotalRowNum     INTEGER;
      losKeyInfValue     DabUtil.tosKeyValue;
      larKeyValues       DabUtil.tarKeyValues;
      lonMigParallelNum  INTEGER;
   BEGIN

      CallInfo( 'Setup parallel migration for script ' || piosScriptName );
      CallInfo( 'Migration of table ' || piosTableName || ' using key column ' || piosKeyColumn );

      lonMigParallelNum := pionMigParallelNum;

      ------------------------------------------------------------------------
      -- Get process status
      BEGIN
         SELECT  MIG_STATUS
         INTO    losMigStatus
         FROM    MIG_PROCESS
         WHERE   SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lobDataFound := FALSE;
         WHEN OTHERS
         THEN
            CallError( 'Select from MIG_PROCESS' );
      END;

      ------------------------------------------------------------------------
      -- Check status.
      IF losMigStatus != cosStatusInitial
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Invalid migration status in MIG_PROCESS: ' || losMigStatus );
         ELSE
            CallWarning( 'Invalid migration status in MIG_PROCESS: ' || losMigStatus  );
         END IF;
      END IF;

      ------------------------------------------------------------------------
      -- Clean up
      BEGIN
        DELETE FROM MIG_SCRIPT_CONTROL
        WHERE  SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Delete from MIG_SCRIPT_CONTROL' );
      END;

      BEGIN
        DELETE FROM MIG_PROGRESS
        WHERE  SCRIPT_NAME = piosScriptName;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Delete from MIG_PROGRESS' );
      END;

      ------------------------------------------------------------------------
      -- Insert into MIG_PROCESS
      IF NOT lobDataFound
      THEN
         BEGIN
           INSERT INTO MIG_PROCESS
           (
               SCRIPT_NAME
              ,MIG_STATUS
           )
           VALUES
           (
               piosScriptName
              ,cosStatusInitial
           );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Insert into MIG_PROCESS' );
         END;
      END IF;

      ------------------------------------------------------------------------
      -- Deal with the parallel processing case
      IF pionMigParallelNum > 1
      THEN

         MigService.GetKeyValues
         (
             piosTableName
            ,piosKeyColumn
            ,pionMigParallelNum
            ,lonTotalRowNum
            ,larKeyValues
         );

         IF pionMigParallelNum > lonTotalRowNum
         THEN
           lonMigParallelNum := 1;
           CallInfo( 'Number of rows in table ' || piosTableName || ' is less than number' || chr(10)
                      || '          of parallel processes. Therefore the number of parallel processes is set to 1.');

         ELSE
           lonMigParallelNum := pionMigParallelNum;
           ------------------------------------------------------------------------
           -- Validate array of max values (indexing).
           IF larKeyValues.FIRST != 1
           OR larKeyValues.LAST != pionMigParallelNum
           OR larKeyValues.COUNT != pionMigParallelNum
           THEN
              CallError( 'Invalid array of maximal key values: Incorrect indexing.' );
           END IF;

           CallInfo( 'Total number of rows = ' || lonTotalRowNum );

           lonProcessNumber := 1;
           losKeyInfValue := NULL;

           WHILE lonProcessNumber < pionMigParallelNum
           LOOP
              losKeyMaxValue := larKeyValues( lonProcessNumber ).sKeyMaxValue;

              ------------------------------------------------------------------------
              -- We always asume that losKeyInfValue < losKeyMaxValue or with other words
              -- that larKeyValues( lonProcessNumber ).sKeyMaxValue is increasing.
              -- This can not be checked here without knowing the datatype of the key column.

              INSERT INTO MIG_PROGRESS
              (
                  SCRIPT_NAME
                 ,PROCESS_NUMBER
                 ,MIG_STATUS
                 ,KEY_INFIMUM_VALUE
                 ,KEY_MAX_VALUE
              )
              VALUES
              (
                  piosScriptName
                 ,lonProcessNumber
                 ,cosStatusInitial
                 ,losKeyInfValue
                 ,losKeyMaxValue
              );
              CallInfo( 'Setup range for process number ' || lonProcessNumber );
              CallInfo( 'KeyInfValue : ' || nvl( losKeyInfValue, 'NONE' )
                         || ' KeyMaxValue: ' || nvl( losKeyMaxValue , 'NONE' )
                         || ' NumberOfRows: ' || larKeyValues( lonProcessNumber ).nNumberOfRows
                         || ' NumberOfKeys: ' || larKeyValues( lonProcessNumber ).nNumberOfKeys );
              losKeyInfValue := losKeyMaxValue;
              lonProcessNumber := lonProcessNumber + 1;
           END LOOP;

-- PN 224594  Modified code:
           losKeyMaxValue := larKeyValues( lonProcessNumber ).sKeyMaxValue;

           INSERT INTO MIG_PROGRESS
           (
               SCRIPT_NAME
              ,PROCESS_NUMBER
              ,MIG_STATUS
              ,KEY_INFIMUM_VALUE
              ,KEY_MAX_VALUE
           )
           VALUES
           (
               piosScriptName
              ,lonProcessNumber
              ,cosStatusInitial
              ,losKeyInfValue
              ,losKeyMaxValue
           );

-- PN 224594  Before Image:
--         INSERT INTO MIG_PROGRESS
--         (
--             SCRIPT_NAME
--            ,PROCESS_NUMBER
--            ,MIG_STATUS
--            ,KEY_INFIMUM_VALUE
--         )
--         VALUES
--         (
--             piosScriptName
--            ,lonProcessNumber
--            ,cosStatusInitial
--            ,losKeyInfValue
--         );
--
--         losKeyMaxValue := larKeyValues( lonProcessNumber ).sKeyMaxValue;

           CallInfo( 'Setup range for process number ' || lonProcessNumber );
           CallInfo( 'KeyInfValue : ' || nvl( losKeyInfValue, 'NONE' )
                      || ' KeyMaxValue: ' || nvl( losKeyMaxValue, 'NONE' )
                      || ' NumberOfRows: ' || larKeyValues( lonProcessNumber ).nNumberOfRows
                      || ' NumberOfKeys: ' || larKeyValues( lonProcessNumber ).nNumberOfKeys );
        END IF;
      END IF;

      ------------------------------------------------------------------------
      -- Insert into MIG_SCRIPT_CONTROL
      BEGIN
        INSERT INTO MIG_SCRIPT_CONTROL
        (
            SCRIPT_NAME
           ,MIG_PARALLEL_NUM
           ,UPPER_ROWNUM_LIMIT
           ,MIG_TABLE_NAME
           ,MIG_KEY_NAME
        )
        VALUES
        (
            piosScriptName
           ,lonMigParallelNum
           ,pionUpperRowLimit
           ,piosTableName
           ,piosKeyColumn
        );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Insert into MIG_SCRIPT_CONTROL' );
      END;

      ------------------------------------------------------------------------
      -- Commit transaction
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'SetMigScript' , 'Script Name: ' || piosScriptName );
   END SetMigScript;


   /*
   ** =====================================================================
   **
   ** Procedure: TransferTable
   **
   ** Purpose: Transfer all data from a source table or view into a target table
   **          using the method "insert into select from"
   **
   ** =====================================================================
   */
   PROCEDURE TransferTable
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

       -- Target table name
      ,piosTargetTableName IN DabUtil.tosObjectName

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   )
   IS
      lobContinue          BOOLEAN := FALSE;
      lorMigControl        torMigControl;
      losStartKeyValue     DabUtil.tosKeyValue;
      losStopKeyValue      DabUtil.tosKeyValue;
      lasWhereClause       DabUtil.tasText;
      lasTransferStatement DabUtil.tasText;
      lasSQLStatement      DBMS_SQL.VARCHAR2S;
      lasMessageText       DabUtil.tasText;

      lonDynamicCursor    INTEGER;
      lonNumberOfRows     INTEGER;
   BEGIN
      ------------------------------------------------------------------------
      -- Check input parameter
      IF piosScriptName IS NULL
      OR piosTrack IS NULL
      OR piosScriptVersion IS NULL
      OR piosScriptWhat IS NULL
      OR pionProcessNumber IS NULL
      OR piosTargetTableName IS NULL
      OR piosSourceTableName IS NULL
      OR piosKeyColumn IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      ELSIF pionProcessNumber < 0
      THEN
         CallError( 'Invalid process number.' );
      END IF;

      ------------------------------------------------------------------------
      -- Initialize nested tables
      lasWhereClause       := DabUtil.tasText();
      lasTransferStatement := DabUtil.tasText();
      lasMessageText       := DabUtil.tasText();

      ------------------------------------------------------------------------
      -- Build where clause
      DabUtil.AddText
      (
          'WHERE ' || piosKeyColumn || ' BETWEEN :pcosStartKey AND :pcosStopKey'
         ,lasWhereClause
      );

      ------------------------------------------------------------------------
      -- Get "insert into select from" statement
      MigService.GetTransferStatement
      (
          piosTargetTableName
         ,USER
         ,piosSourceTableName
         ,USER
         ,NULL
         ,lasWhereClause
         ,lasTransferStatement
         ,lasMessageText
      );
      DabUtil.PrintText( lasMessageText );

      ------------------------------------------------------------------------
      -- Copy SQL statement into data type expected by DBMS_SQL
      DabUtil.CopySql
      (
          lasTransferStatement
         ,lasSQLStatement
      );

      ------------------------------------------------------------------------
      -- Parse the SQL to Oracle Server.
      lonDynamicCursor := DBMS_SQL.OPEN_CURSOR;

      BEGIN
         DBMS_SQL.PARSE
         (
             lonDynamicCursor
            ,lasSQLStatement
            ,lasSQLStatement.FIRST
            ,lasSQLStatement.LAST
            ,TRUE
            ,DBMS_SQL.NATIVE
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            DabUtil.PrintText( lasTransferStatement );
            CallError( 'Parse cursor for dynamic SQL' );
      END;

      ------------------------------------------------------------------------
      -- Call start script
      StartMigScript
      (
          piosScriptName
         ,pionProcessNumber
         ,piosSourceTableName
         ,piosKeyColumn
         ,piosTrack
         ,piosScriptVersion
         ,piosScriptWhat
         ,piosDescription
         ,FALSE
         ,lobContinue
         ,losStartKeyValue
         ,losStopKeyValue
         ,lorMigControl
      );

      ------------------------------------------------------------------------
      -- Run SQL
      WHILE lobContinue
      LOOP
         DBMS_SQL.BIND_VARIABLE
         (
             lonDynamicCursor
            ,':pcosStartKey'
            , losStartKeyValue
         );

         DBMS_SQL.BIND_VARIABLE
         (
             lonDynamicCursor
            ,':pcosStopKey'
            , losStopKeyValue
         );

         lonNumberOfRows := DBMS_SQL.EXECUTE( lonDynamicCursor );

         ContinueMigScript
         (
             piosScriptName
            ,pionProcessNumber
            ,lobContinue
            ,losStartKeyValue
            ,losStopKeyValue
            ,lorMigControl
         );

      END LOOP;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'TransferTable' , 'Script Name: ' || piosScriptName || ' Process Number: ' || pionProcessNumber );
   END TransferTable;

   /*
   ** Overload: with already prepared transfer statement
   **           in format INSERT INTO... SELECT FROM...
   */
   PROCEDURE TransferTable
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Process Number
      ,pionProcessNumber IN INTEGER

       -- Target table name
      ,piosTargetTableName IN DabUtil.tosObjectName

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- SQL statement in form INSERT INTO ... SELECT FROM ...
      ,piosSourceStatement IN DabUtil.tasText

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   )
   IS
      lobContinue          BOOLEAN := FALSE;
      lorMigControl        torMigControl;
      losStartKeyValue     DabUtil.tosKeyValue;
      losStopKeyValue      DabUtil.tosKeyValue;
      lasWhereClause       DabUtil.tasText;
      lasTransferStatement DabUtil.tasText := piosSourceStatement;
      lasSQLStatement      DBMS_SQL.VARCHAR2S;
      lasMessageText       DabUtil.tasText;

      lonDynamicCursor    INTEGER;
      lonNumberOfRows     INTEGER;

      lonIndex            PLS_INTEGER := 0;
      lobFound            BOOLEAN := FALSE;

   BEGIN
      ------------------------------------------------------------------------
      -- Check input parameter
      IF piosScriptName IS NULL
      OR piosTrack IS NULL
      OR piosScriptVersion IS NULL
      OR piosScriptWhat IS NULL
      OR pionProcessNumber IS NULL
      OR piosTargetTableName IS NULL
      OR piosSourceTableName IS NULL
      OR piosSourceStatement IS NULL
      OR piosKeyColumn IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      ELSIF pionProcessNumber < 0
      THEN
         CallError( 'Invalid process number.' );
      END IF;

      ------------------------------------------------------------------------
      -- Initialize nested tables
      lasWhereClause       := DabUtil.tasText();
      lasMessageText       := DabUtil.tasText();

      ------------------------------------------------------------------------
      -- Build where clause
      lonIndex := lasTransferStatement.LAST;
      WHILE lonIndex >= lasTransferStatement.FIRST LOOP
        IF SUBSTR(lasTransferStatement(lonIndex), 1, 6) = 'WHERE ' THEN
          lobFound := TRUE;
          EXIT;
        ELSE
          lonIndex := lonIndex - 1;
        END IF;
      END LOOP;

      IF lobFound THEN
        DabUtil.AddText
        (
          'AND ' || piosKeyColumn || ' BETWEEN :pcosStartKey AND :pcosStopKey'
         ,lasWhereClause
        );
      ELSE
        DabUtil.AddText
        (
          'WHERE ' || piosKeyColumn || ' BETWEEN :pcosStartKey AND :pcosStopKey'
         ,lasWhereClause
        );
      END IF;

      DabUtil.AddText
      (
        lasWhereClause
       ,lasTransferStatement
      );

      ------------------------------------------------------------------------
      -- Copy SQL statement into data type expected by DBMS_SQL
      DabUtil.CopySql
      (
          lasTransferStatement
         ,lasSQLStatement
      );

      ------------------------------------------------------------------------
      -- Parse the SQL to Oracle Server.
      lonDynamicCursor := DBMS_SQL.OPEN_CURSOR;

      BEGIN
         DBMS_SQL.PARSE
         (
             lonDynamicCursor
            ,lasSQLStatement
            ,lasSQLStatement.FIRST
            ,lasSQLStatement.LAST
            ,TRUE
            ,DBMS_SQL.NATIVE
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            DabUtil.PrintText( lasTransferStatement );
            CallError( 'Parse cursor for dynamic SQL' );
      END;

      ------------------------------------------------------------------------
      -- Call start script
      StartMigScript
      (
          piosScriptName
         ,pionProcessNumber
         ,piosSourceTableName
         ,piosKeyColumn
         ,piosTrack
         ,piosScriptVersion
         ,piosScriptWhat
         ,piosDescription
         ,FALSE
         ,lobContinue
         ,losStartKeyValue
         ,losStopKeyValue
         ,lorMigControl
      );

      ------------------------------------------------------------------------
      -- Run SQL
      WHILE lobContinue
      LOOP
         DBMS_SQL.BIND_VARIABLE
         (
             lonDynamicCursor
            ,':pcosStartKey'
            , losStartKeyValue
         );

         DBMS_SQL.BIND_VARIABLE
         (
             lonDynamicCursor
            ,':pcosStopKey'
            , losStopKeyValue
         );

         lonNumberOfRows := DBMS_SQL.EXECUTE( lonDynamicCursor );

         ContinueMigScript
         (
             piosScriptName
            ,pionProcessNumber
            ,lobContinue
            ,losStartKeyValue
            ,losStopKeyValue
            ,lorMigControl
         );

      END LOOP;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'TransferTable' , 'Script Name: ' || piosScriptName || ' Process Number: ' || pionProcessNumber );
   END TransferTable;

   /*
   ** =====================================================================
   **
   ** Function: GetString
   **
   ** Purpose: Load a configurable migration value of type varchar2
   **
   ** =====================================================================
   */
   FUNCTION GetString
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Default Value String
      ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   )
   RETURN DabUtil.tosParaValue
   IS
      losParaValue DabUtil.tosParaValue;
   BEGIN
      MigService.GetParaValue
      (
          piosParaKey
         ,losParaValue
         ,piosDefaultValue
      );
      RETURN losParaValue;
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetString' );
   END GetString;

   /*
   ** =====================================================================
   **
   ** Function: GetNumber
   **
   ** Purpose: Load a configurable migration value of number type
   **
   ** =====================================================================
   */
   FUNCTION GetNumber
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Default Value String
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   )
   RETURN DabUtil.tonParaValue
   IS
      lonParaValue DabUtil.tonParaValue;
   BEGIN
      MigService.GetParaValue
      (
          piosParaKey
         ,lonParaValue
         ,pionDefaultValue
      );
      RETURN lonParaValue;
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetNumber' );
   END GetNumber;

   /*
   ** =====================================================================
   **
   ** Procedure: GetValue
   **
   ** Purpose: Load a configurable migration value
   **
   ** Overload: Character type Values
   ** =====================================================================
   */
   PROCEDURE GetValue
   (
      -- Parameter Key
      piosParaKey IN DabUtil.tosParaKey

      -- Value String
     ,poosParaValue OUT DabUtil.tosParaValue

      -- Default Value String
     ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   )
   IS
   BEGIN
      MigService.GetParaValue
      (
          piosParaKey
         ,poosParaValue
         ,piosDefaultValue
      );
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetValue' );
   END GetValue;

   /*
   ** Overload: Number type Values
   */
   PROCEDURE GetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value Number
      ,poonParaValue OUT DabUtil.tonParaValue

       -- Default Value Number
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   )
   IS
   BEGIN
      MigService.GetParaValue
      (
          piosParaKey
         ,poonParaValue
         ,pionDefaultValue
      );
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetValue' );
   END GetValue;

   /*
   ** =====================================================================
   **
   ** Procedure: SetValue
   **
   ** Purpose: Set a configurable migration value
   **
   ** Overload: Character type values
   ** =====================================================================
   */
   PROCEDURE SetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,piosParaValue IN DabUtil.tosParaValue
   )
   IS
   BEGIN
      MigService.SetParaValue
      (
          piosParaKey
         ,piosParaValue
      );
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'SetValue' );
   END SetValue;

   /*
   ** Overload: Number type Values
   */
   PROCEDURE SetValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,pionParaValue IN DabUtil.tonParaValue
   )
   IS
   BEGIN
      MigService.SetParaValue
      (
          piosParaKey
         ,pionParaValue
      );
      IF gobCommitInd
      THEN
         COMMIT;
      END IF;

   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'SetValue' );
   END SetValue;

   /*
   ** =====================================================================
   **
   ** Function: TableStorage
   **
   ** Purpose: Provide storage parameters for a table
   **
   ** =====================================================================
   */
   FUNCTION TableStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause
   IS
      losStorageClause DabUtil.tosStorageClause;
   BEGIN

      MigService.ObjectStorage
      (
          piosTableName
         ,DabUtil.cosTable
         ,losStorageClause
         ,1
         ,piosDefaultStorage
      );
      RETURN losStorageClause;
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'TableStorage' , 'Table Name: ' || piosTableName );
   END TableStorage;

   /*
   ** =====================================================================
   **
   ** Function: PkeyStorage
   **
   ** Purpose: Provide storage parameters for a primary key constraint
   **
   ** =====================================================================
   */
   FUNCTION PkeyStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause
   IS
      losStorageClause DabUtil.tosStorageClause;
   BEGIN

      MigService.ObjectStorage
      (
          piosTableName
         ,DabUtil.cosPkey
         ,losStorageClause
         ,1
         ,piosDefaultStorage
      );
      RETURN losStorageClause;
   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'PkeyStorage', 'Table Name: ' || piosTableName );
   END PkeyStorage;

   /*
   ** =====================================================================
   **
   ** Function: UkeyStorage
   **
   ** Purpose: Provide storage parameters for a unique key constraint
   **
   ** =====================================================================
   */
   FUNCTION UkeyStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Unique Key Number (enumerate all unique keys a table)
      ,pionUkeyNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause
   IS
      losStorageClause DabUtil.tosStorageClause;
   BEGIN

      MigService.ObjectStorage
      (
          piosTableName
         ,Dabutil.cosUkey
         ,losStorageClause
         ,pionUkeyNumber
         ,piosDefaultStorage
      );
      RETURN losStorageClause;
   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'UkeyStorage', 'Table Name: ' || piosTableName );
   END UkeyStorage;

   /*
   ** =====================================================================
   **
   ** Function: IndexStorage
   **
   ** Purpose: Provide storage parameters for an index
   **
   ** =====================================================================
   */
   FUNCTION IndexStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Index Number (enumerate all indexes of a table)
      ,pionIndexNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause
   IS
      losStorageClause DabUtil.tosStorageClause;
   BEGIN

      MigService.ObjectStorage
      (
          piosTableName
         ,DabUtil.cosIndex
         ,losStorageClause
         ,pionIndexNumber
         ,piosDefaultStorage
      );
      RETURN losStorageClause;
   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'IndexStorage', 'Table Name: ' || piosTableName );
   END IndexStorage;

   /*
   ** =====================================================================
   **
   ** Function: PartitionStorage
   **
   ** Purpose: Provide storage parameters for partition of a table
   **
   ** =====================================================================
   */
   FUNCTION PartitionStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Partition Number (enumerate all partitions of a table)
      ,pionPartitionNumber IN INTEGER DEFAULT ( 1 )

      -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   RETURN DabUtil.tosStorageClause
   IS
      losStorageClause DabUtil.tosStorageClause;
   BEGIN

      MigService.ObjectStorage
      (
          piosTableName
         ,DabUtil.cosPartition
         ,losStorageClause
         ,pionPartitionNumber
         ,piosDefaultStorage
      );
      RETURN losStorageClause;
   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'PartitionStorage', 'Table Name: ' || piosTableName );
   END PartitionStorage;

   /*
   ** =====================================================================
   **
   ** Procedure: FinishMigScript
   **
   ** Purpose: Set the status of the migration script to completed.
   **
   ** =====================================================================
   */
   PROCEDURE FinishMigScript
   (
      -- Script Name
       piosScriptName IN tosScriptName

      -- Track
      ,piosTrack IN tosTrack

      -- Script Version (CMVC version)
      ,piosScriptVersion IN tosScriptVersion

      -- Script What (CMVC what)
      ,piosScriptWhat IN tosScriptWhat

      -- Description of the purpose of the script
      ,piosDescription IN tosDescription

   )
   IS
      lobDataFound        BOOLEAN := TRUE;
      losMigStatus        tosMigStatus;
      lonProcessNumber    INTEGER;
      lonMigParallelNum   INTEGER;

   BEGIN
      ------------------------------------------------------------------------
      -- Check input parameter
      IF piosScriptName IS NULL
      OR piosTrack IS NULL
      OR piosScriptVersion IS NULL
      OR piosScriptWhat IS NULL
      OR piosDescription IS NULL
      THEN
         CallError( 'Input parameter is NULL.' );
      END IF;

      ------------------------------------------------------------------------
      -- Read number of processes from MIG_SCRIPT_CONTROL
      BEGIN
        SELECT MIG_PARALLEL_NUM INTO lonMigParallelNum
        FROM MIG_SCRIPT_CONTROL
        WHERE SCRIPT_NAME = piosScriptName;
      EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
          CallError('No enry for script ' || piosScriptName || ' in the table MIG_SCRIPT_CONTROL');
        WHEN OTHERS
        THEN
          CallError('Select from MIG_SCRIPT_CONTROL');
      END;

      ------------------------------------------------------------------------
      -- Number of entries in MIG_PROGRESS
      BEGIN
        SELECT COUNT(*) INTO lonProcessNumber
        FROM MIG_PROGRESS
        WHERE SCRIPT_NAME = piosScriptName;
      EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
          CallError('No enry for script ' || piosScriptName || ' in the table MIG_PROGRESS');
        WHEN OTHERS
        THEN
          CallError('Select from MIG_PROGRESS');
      END;

      IF lonProcessNumber = lonMigParallelNum
      THEN
      -- Check whether all processes are completed.
        FOR i IN 1..lonProcessNumber LOOP
          SELECT MIG_STATUS INTO losMigStatus
          FROM MIG_PROGRESS
          WHERE SCRIPT_NAME = piosScriptName
            AND PROCESS_NUMBER = i;

          EXIT WHEN losMigStatus != cosStatusCompleted;
        END LOOP;
      ELSE
        CallError('Number of processes does not corespond to setup of MIG_PROGRESS');
      END IF;

      IF losMigStatus != cosStatusCompleted
      THEN
        CallError('Not all processes for script ' || piosScriptName || ' are completed');
      ELSE
        BEGIN
          SELECT MIG_STATUS
          INTO losMigStatus
          FROM MIG_PROCESS
          WHERE SCRIPT_NAME = piosScriptName
          FOR UPDATE;
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            lobDataFound := FALSE;
          WHEN OTHERS
          THEN
            CallError('Select from MIG_PROCESS');
        END;

        IF lobDataFound
        THEN
         -- Determine status
          IF losMigStatus = cosStatusInitial
          THEN
            UPDATE MIG_PROCESS
            SET    MIG_STATUS = cosStatusCompleted
                  ,FINISH_DATE = SYSDATE
                  ,TRACK = piosTrack
                  ,SCRIPT_VERSION = piosScriptVersion
                  ,SCRIPT_WHAT = piosScriptWhat
                  ,DESCRIPTION = piosDescription
                  ,START_DATE = (SELECT MIN(START_DATE) FROM MIG_PROGRESS
                                  WHERE SCRIPT_NAME = piosScriptName)
            WHERE SCRIPT_NAME = piosScriptName;
          ELSIF losMigStatus = cosStatusCompleted
          THEN
             CallError('Migration status is already completed.');
          ELSE
            CallError('Migration status: '
                         || losMigStatus
                         || ' invalid in this context.');
          END IF;
        ELSE
          CallError('No entry found in MIG_PROCESS');
        END IF;
      END IF;

      IF gobCommitInd
      THEN
        COMMIT;
      END IF;

      CallInfo('Finish migration script ' || piosScriptName || ' with process number: ' || lonProcessNumber);

   EXCEPTION
     WHEN OTHERS
     THEN
       RaiseError('FinishMigScript', 'Script Name: ' || piosScriptName);

   END FinishMigScript;

END Migration;
/

