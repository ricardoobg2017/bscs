CREATE OR REPLACE PACKAGE GRABA_SAFI_FACT IS
--
PROCEDURE GRABA_SAFI_PRINCIPAL(PDFECHCIERREPERIODO  IN  DATE,
                               pv_producto   in varchar2,   -- NCA 'DTH' solo DTH, 'VOZ' todos los productos exceptio DTH, nulo para procesar voz y dth.	
	                             pv_ejecucion  in varchar2,   -- tipo de ejecucion 'C' commit 'CG' control group	
                               RESULTADO            OUT VARCHAR2);

--
PROCEDURE CREA_ASIENTOS (TIPO_FACTURACION    IN NUMBER,
                         FECHACIERRER2       IN VARCHAR2,
                         FECHACIERREP        IN DATE,
                         LNANOFINAL          IN NUMBER,
                         LNMESFINAL          IN NUMBER,
                         FECHACIERRER        IN DATE,
                         GLOSA               IN VARCHAR2,
                         DOC_TYPE            IN VARCHAR2,
                         LVFECHA             IN VARCHAR2,
                         LNDIAFINAL          IN VARCHAR2,
                         LVFECHACTUAL        IN VARCHAR2,
                         CODIGO_ERROR        OUT NUMBER,
                         RESULTADO           IN OUT VARCHAR2
                         );
--
FUNCTION VERIFICA_TIPO_FACTURA (PD_PERIODO_FIN IN DATE,
                                PV_CUSTOMER_ID IN INTEGER) RETURN NUMBER ;
--
FUNCTION GVF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO IN NUMBER,
                                     PV_ID_PARAMETRO       IN VARCHAR2)
                                                           RETURN VARCHAR2;

--
FUNCTION GVF_OBTENER_SECUENCIAS (PV_CODIGO   IN VARCHAR2)
                                             RETURN NUMBER;
--
FUNCTION FN_GLOSA_DTH (PV_NOMBRE_CTA IN VARCHAR2,
                       PV_CTA_CTBLE  IN VARCHAR2,                                         
                       PV_FECHACTUAL IN VARCHAR2,
                       PV_GLOSA      IN VARCHAR2,
                       PV_TIPO       IN VARCHAR2 DEFAULT NULL)RETURN VARCHAR2;
--
FUNCTION FN_IMPUESTO_DTH_IVA_ICE(PV_NOMBRE IN VARCHAR2, 
                                 PV_CTA_CTBLE IN VARCHAR2,
                                 PV_TIPO_IMP IN VARCHAR2)
                                 RETURN NUMBER;
--
PROCEDURE GEN_RUBROS_NEG_DTH (FECHACIERREP  IN DATE,                         
                              LV_ERROR IN OUT VARCHAR2
                              );
--
END GRABA_SAFI_FACT;
/
CREATE OR REPLACE PACKAGE BODY GRABA_SAFI_FACT IS

PROCEDURE GRABA_SAFI_PRINCIPAL(PDFECHCIERREPERIODO IN DATE,
                               PV_PRODUCTO   IN VARCHAR2,   -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
	                             PV_EJECUCION  IN VARCHAR2,   -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP	
                               RESULTADO     OUT VARCHAR2) is
--
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Creaci�n de Asiento Contables para Facturaci�n F�sica y Electr�nica
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 14/05/2012 9:42:15
  -- Proyecto: [5328] Facturaci�n Electr�nica
  --=====================================================================================--
  --=====================================================================================--
  -- Modificado por: CLS Luis Rivera (LRI)
  -- L�der proyecto: Ing. Diego Ocampo
  -- PDS: CLS Ing. Allan Endara C.
  -- Fecha de modificaci�n: 15/01/2012 12:10:20
  -- Proyecto: [8708] � Modificaci�n Proceso BSCS JGE_ACC_EN
  --=====================================================================================--
    --=====================================================================================--
  -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SUD SRE)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Cristhian Acosta Ch.
  -- Fecha de creaci�n: 28/05/2013 11:57:50
  -- Proyecto: [8693] VENTA DTH POSTPAGO
  --=====================================================================================--
  --=====================================================================================--
  -- Modificado por: SUD Norman Castro (SUD NCA)
  -- L�der proyecto: SIS Paola Carvajal
  -- Lider PDS:      SUD Cristhian Acosta Chambers
  -- Fecha de modificaci�n: 04/07/2013
  -- Proyecto: [8693] VENTA DTH POSTPAGO
	-- Proposito: Se agrega parametro de simulacion 'CG' o commit 'C', se a�ade el producto a procesar
	--             solo DTH, solo VOZ o todos los productos VOZ y DTH.
  --=====================================================================================--	
  --=====================================================================================--
  -- Modificado por: SUD Cristhian Acosta Chambers (SUD CAC)
  -- L�der proyecto: SIS Xavier Trivi�o
  -- Lider PDS:      SUD Cristhian Acosta Chambers
  -- Fecha de modificaci�n: 31/07/2014
  -- Proyecto: [9587] - Facturaci�n Electr�nica DTH
	-- Proposito: Generaci�n de asiento contable DTH de Factura Electr�nica
  --=====================================================================================--	
--
LVSENTENCIA    VARCHAR2(18000);
LNMESFINAL     NUMBER;
LNANOFINAL     NUMBER;
LNDIAFINAL     VARCHAR2(2);
LVFECHA        VARCHAR2(10);
LVFECHANTERIOR VARCHAR(12);
LVFECHACTUAL   VARCHAR(12);
GLOSA          VARCHAR2(200);
FECHACIERREP   DATE;
FECHACIERRER   DATE;
FECHACIERRER2  VARCHAR2(20);
CODIGO_ERROR   NUMBER:=0;
PV_APLICACION  VARCHAR2(50):='GRABA_SAFI_FACT.GRABA_SAFI_PRINCIPAL';
MY_ERROR       EXCEPTION;
RESULTADO2    VARCHAR2(18000):=NULL;
DOC_TYPE      VARCHAR2(100):=NULL;

-- NCA [8693] 04/07/2013
LV_ERROR      VARCHAR2(2000):=NULL;
LE_ERROR      EXCEPTION;
--
CURSOR DESCX IS
SELECT R.CTAPSOFT,R.CTA_DSCTO, R.CONTRAPARTIDA_GYE, R.CONTRAPARTIDA_UIO  
 FROM READ.COB_SERVICIOS R ;
-- 
--TIPOS DE FACTURACI�N CON SUS RESPECTIVAS GLOSAS Y DOC_TYPE - SUD SRE
CURSOR C_TIPOS_FACTURAS IS
   SELECT S.* 
   FROM TIPOS_FACTURAS_SAFI S
   WHERE S.ESTADO = 'A'
   ORDER BY 1;
--   
--RECOGE CUENTAS POST PAGO DEL PRODUCTO DTH - SUD SRE
CURSOR C_CTAPOST IS 
    SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
      FROM READ.PRUEBAFINSYS D
     WHERE D.CTA_CTRAP IS NULL
       AND D.PRODUCTO = 'DTH';
--
--
BEGIN
-- [8693] - INI SUD NCA
   IF PV_EJECUCION NOT IN ('C', 'CG') THEN
		 LV_ERROR :='El parametro PV_EJECUCION es erroneo. Ingrese C (COMMIT) � CG (CONTROL GROUP) => ';
		RAISE LE_ERROR;
	 END IF;
	 IF PV_PRODUCTO IS NOT NULL THEN 
		 IF PV_PRODUCTO NOT IN ('DTH','VOZ') THEN 
		    LV_ERROR :='El parametro PV_PRODUCTO no es correcto. Debe ingresar DTH,VOZ => ';
		    RAISE LE_ERROR;		 
		 END IF;
	 END IF;
-- [8693] - FIN SUD NCA
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
--
FECHACIERREP := TO_DATE(PDFECHCIERREPERIODO, 'DD/MM/YYYY');
FECHACIERRER := TO_DATE(SYSDATE, 'DD/MM/YYYY');
FECHACIERRER2 := TRIM(TO_CHAR(SYSDATE, 'YYYYMMDD'));
LNMESFINAL  := TO_NUMBER(TO_CHAR(PDFECHCIERREPERIODO, 'MM'));
LNDIAFINAL  := TO_CHAR(PDFECHCIERREPERIODO, 'DD');
LNANOFINAL  := TO_NUMBER(TO_CHAR(PDFECHCIERREPERIODO, 'YYYY'));
LVFECHANTERIOR:=TO_CHAR(ADD_MONTHS(PDFECHCIERREPERIODO,-1),'DD/MM/YYYY');
LVFECHACTUAL:=TO_CHAR(PDFECHCIERREPERIODO,'DD/MM/YYYY');
LVSENTENCIA := 'TRUNCATE  TABLE READ.PRUEBAFINSYS';
LVFECHA :=TO_CHAR(PDFECHCIERREPERIODO, 'DDMMYYYY');
EXEC_SQL(LVSENTENCIA);
--
SELECT DECODE(LNDIAFINAL,'02','04','08','02','15','03','24','01') INTO LNDIAFINAL  FROM DUAL;
COMMIT;
LVSENTENCIA := NULL;
--CARGA DATOS DE LAS CUENTAS POR TIPO DE FACTURACION
-- INI - 8693 - VENTA DTH POST PAGO
--CAMBIO EN LA ESTRUCTURA DEL SELECT SEGUN EL PRODUCTO QUE SE NECESITA PROCESAR - SUD NCA
LVSENTENCIA := 'INSERT INTO READ.PRUEBAFINSYS  ' ||
' SELECT /* + RULE */  H.TIPO,H.NOMBRE,H.PRODUCTO,SUM(H.VALOR) VALOR ,SUM(H.DESCUENTO) DESCUENTO,H.COST_DESC COMPA�IA , '||
' DECODE(H.PRODUCTO,''DTH'',H.CTACLBLEP,MAX(H.CTACLBLEP)), '||--SUD SRE DECODE PARA DTH
' NULL  CUENTA_DESC, GRABA_SAFI_FACT.VERIFICA_TIPO_FACTURA(TO_DATE('''||FECHACIERREP||''',''DD/MM/YYYY''),H.CUSTOMER_ID) TIPO_FACTURACION, NULL FROM CO_FACT_'||LVFECHA||' H'||
' WHERE H.CUSTOMER_ID NOT  IN (SELECT CUSTOMER_ID FROM CUSTOMER_TAX_EXEMPTION CTE WHERE CTE.EXEMPT_STATUS = ''A'') ';
--
IF PV_PRODUCTO = 'DTH' THEN --SOLO DTH - RUBRO POSITIVOS
	LVSENTENCIA := LVSENTENCIA||' AND H.PRODUCTO = ''DTH'' AND H.VALOR > 0 ';
ELSIF PV_PRODUCTO = 'VOZ' THEN --SOLO VOZ
	LVSENTENCIA := LVSENTENCIA||' AND H.PRODUCTO <> ''DTH'' ';	
ELSIF PV_PRODUCTO IS NULL THEN --SI EL PRODUCTO ES NULO TODOS LOS PRODUCTOS VOZ - DTH RUBROS POSITIVOS
	LVSENTENCIA := LVSENTENCIA||' AND (H.PRODUCTO <> ''DTH'' OR (H.PRODUCTO = ''DTH'' AND H.VALOR > 0)) ';
END IF;
  LVSENTENCIA := LVSENTENCIA||' GROUP BY H.TIPO,H.NOMBRE,H.PRODUCTO,H.COST_DESC, GRABA_SAFI_FACT.VERIFICA_TIPO_FACTURA(TO_DATE('''||FECHACIERREP||''',''DD/MM/YYYY''),H.CUSTOMER_ID),H.CTACLBLEP';
--  
EXEC_SQL(LVSENTENCIA);
COMMIT;
--
--ASIGNA CUENTAS CONTRAPARTIDAS SEGUN LA COMPA�IA - SUD SRE
FOR RD IN DESCX LOOP
  UPDATE READ.PRUEBAFINSYS F
     SET F.CTA_DESC  = RD.CTA_DSCTO,
         F.CTA_CTRAP = DECODE(F.COMPA�IA,'Guayaquil',RD.CONTRAPARTIDA_GYE,
                              'Quito',RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
   WHERE F.CTACTBLE = RD.CTAPSOFT;
END LOOP;
COMMIT;
--ACTUALIZA CUENTAS POST PAGADAS CON LA SU CONTRAPARTIDA - SUD SRE
FOR AZ IN C_CTAPOST LOOP
  UPDATE READ.PRUEBAFINSYS A
     SET A.CTA_CTRAP =
     (SELECT DECODE(A.COMPA�IA,'Guayaquil',D.CONTRAPARTIDA_GYE,
                    'Quito',D.CONTRAPARTIDA_UIO)
        FROM COB_SERVICIOS D WHERE D.CTAPOST = AZ.CTACTBLE
       AND D.NOMBRE = AZ.NOMBRE)
   WHERE A.ROWID = AZ.ROWID;
END LOOP
COMMIT;
--
--CONTROL RUBROS NEGATIVOS DTH
 IF PV_PRODUCTO IN ('DTH') OR PV_PRODUCTO IS NULL THEN     
    GEN_RUBROS_NEG_DTH(FECHACIERREP =>FECHACIERREP ,
                       LV_ERROR => LV_ERROR);
    
    IF LV_ERROR IS NOT NULL THEN    
       RAISE LE_ERROR;	
    END IF;
 END IF;
-- FIN - 8693 - VENTA DTH POST PAGO
--INSERTO LOS SERVICIOS PREPAGADOS A TABLA DE DEVENGACION -SUD NCA
IF PV_EJECUCION = 'C' THEN  -- SI ES TIPO DE EJECUCION COMMIT GRABA EN SERVICIOS_PREPAGADOS_DTH
 IF PV_PRODUCTO IN ('DTH') OR PV_PRODUCTO IS NULL THEN  
    LVSENTENCIA:= NULL;
    LVSENTENCIA:= 'insert into servicios_prepagados_dth (ciclo, dia, mes, anio, tipo, nombre, valor, ctactble, cta_ctrap, compa�ia, shdes ) '||
                  ' select (select f.id_ciclo from fa_ciclos_bscs f'|| 
                           ' where f.dia_ini_ciclo = extract(day from to_date('''||FECHACIERREP||''',''dd/mm/yyyy''))) ciclo, '||
                  ' extract(day from to_date('''||FECHACIERREP||''',''dd/mm/yyyy'')) dia,'||
                  ' extract(month from to_date('''||FECHACIERREP||''',''dd/mm/yyyy'')) mes, '||
                  ' extract(year from to_date('''||FECHACIERREP||''',''dd/mm/yyyy'')) anio,'||
                  ' a.tipo, a.nombre, round(a.valor,3), b.ctapost, a.cta_ctrap, a.compa�ia, c.shdes'||
                  ' from read.pruebafinsys a, cob_servicios b, mpusntab c'||
                  ' where a.ctactble <> b.ctapost'||
                  ' and a.nombre = b.nombre'||
                  ' and a.producto = ''DTH'' and b.nombre = c.des';
    EXEC_SQL(LVSENTENCIA);              
    COMMIT;
 END IF;
END IF;
--
LVSENTENCIA := NULL;

LVSENTENCIA := 'TRUNCATE TABLE READ.GSI_FIN_SAFI';
EXEC_SQL(LVSENTENCIA);
COMMIT;
LVSENTENCIA := ' TRUNCATE TABLE PS_PR_JGEN_ACCT_EN_TMP ';
EXEC_SQL(LVSENTENCIA);
COMMIT;

LVSENTENCIA := 'TRUNCATE TABLE READ.GSI_FIN_SAFI_EXCENTOS';
EXEC_SQL(LVSENTENCIA);
COMMIT;

--LLAMAR AL PROCESO QUE CREA LOS ASIENTOS
 FOR X IN C_TIPOS_FACTURAS LOOP
 --
       --
    GLOSA:=X.GLOSA;
    DOC_TYPE:=X.DOC_TYPE;
       --
        IF GLOSA IS NULL THEN
     RESULTADO:='GLOSA NO CONFIGURADA PARA '||X.DESCRIPCION;
         RAISE MY_ERROR;
        END IF;
    IF DOC_TYPE IS NULL THEN
     RESULTADO:='DOC_TYPE NO CONFIGURADO PARA '||X.DESCRIPCION;
         RAISE MY_ERROR;
        END IF;
        --
    GLOSA:=REPLACE(REPLACE(GLOSA,'[%%FECHANTERIOR%%]',LVFECHANTERIOR),'[%%FECHACTUAL%%]',LVFECHACTUAL);
    --
    --CREA LOS DIFERENTES ASIENTOS CONTABLES PARA UIO Y GYE
    GRABA_SAFI_FACT.CREA_ASIENTOS (TIPO_FACTURACION => X.ID_TIPO, --SE MANEJAN LOS TIPOS DE FACTURACION EXISTENTES
                                   FECHACIERRER2    => FECHACIERRER2,
                                   FECHACIERREP     => FECHACIERREP,
                                   LNANOFINAL       => LNANOFINAL,
                                   LNMESFINAL       => LNMESFINAL,
                                   FECHACIERRER     => FECHACIERRER,
                                   GLOSA            => GLOSA,
                                   DOC_TYPE         => DOC_TYPE,
                                   LVFECHA          => LVFECHA,
                                   LNDIAFINAL       => LNDIAFINAL,
                                   LVFECHACTUAL     => LVFECHACTUAL,
                                   CODIGO_ERROR     => CODIGO_ERROR,
                                   RESULTADO        => RESULTADO
                                    );
     IF CODIGO_ERROR = 1 THEN
      RAISE MY_ERROR;
     END IF;

     RESULTADO2:=RESULTADO2||' '||RESULTADO;
 --
 END LOOP;

   IF CODIGO_ERROR=2 THEN
/*cls lri ini*/
     begin
 			 if pv_ejecucion = 'C' then
          insert into ps_jgen_acct_entry@finsys
          (BUSINESS_UNIT,TRANSACTION_ID,TRANSACTION_LINE,LEDGER_GROUP,LEDGER,ACCOUNTING_DT,APPL_JRNL_ID,
           BUSINESS_UNIT_GL,FISCAL_YEAR,ACCOUNTING_PERIOD,JOURNAL_ID,JOURNAL_DATE,JOURNAL_LINE,ACCOUNT,ALTACCT,
           DEPTID,OPERATING_UNIT,PRODUCT,FUND_CODE,CLASS_FLD,PROGRAM_CODE,BUDGET_REF,AFFILIATE,AFFILIATE_INTRA1,
           AFFILIATE_INTRA2,CHARTFIELD1,CHARTFIELD2,CHARTFIELD3,PROJECT_ID,CURRENCY_CD,STATISTICS_CODE,
           FOREIGN_CURRENCY,RT_TYPE,RATE_MULT,RATE_DIV,MONETARY_AMOUNT,FOREIGN_AMOUNT,STATISTIC_AMOUNT,
           MOVEMENT_FLAG,DOC_TYPE,DOC_SEQ_NBR,DOC_SEQ_DATE,JRNL_LN_REF,LINE_DESCR,IU_SYS_TRAN_CD,IU_TRAN_CD,
           IU_ANCHOR_FLG,GL_DISTRIB_STATUS,PROCESS_INSTANCE
          )
          SELECT
            BUSINESS_UNIT, TRANSACTION_ID, TRANSACTION_LINE, LEDGER_GROUP, LEDGER, ACCOUNTING_DT, APPL_JRNL_ID,
            BUSINESS_UNIT_GL, FISCAL_YEAR, ACCOUNTING_PERIOD, JOURNAL_ID, JOURNAL_DATE, JOURNAL_LINE, ACCOUNT,
            ALTACCT, DEPTID, OPERATING_UNIT, PRODUCT, FUND_CODE, CLASS_FLD, PROGRAM_CODE, BUDGET_REF, AFFILIATE, 
            AFFILIATE_INTRA1, AFFILIATE_INTRA2, CHARTFIELD1, CHARTFIELD2, CHARTFIELD3, PROJECT_ID, CURRENCY_CD, 
            STATISTICS_CODE, FOREIGN_CURRENCY, RT_TYPE, RATE_MULT, RATE_DIV, MONETARY_AMOUNT, FOREIGN_AMOUNT,
            STATISTIC_AMOUNT, MOVEMENT_FLAG, DOC_TYPE, DOC_SEQ_NBR, DOC_SEQ_DATE, JRNL_LN_REF, substr(LINE_DESCR,1,100), 
            IU_SYS_TRAN_CD, IU_TRAN_CD, IU_ANCHOR_FLG, GL_DISTRIB_STATUS, PROCESS_INSTANCE
          FROM READ.GSI_FIN_SAFI;

          insert into SYSADM.PS_PR_JGEN_ACCT_EN@FINSYS
          (BUSINESS_UNIT,TRANSACTION_ID,TRANSACTION_LINE,LEDGER_GROUP,LEDGER,PR_JRNL_LN_REF2,PR_JRNL_TIPO_IVA,
           PR_JRNL_CICLO,PR_ID_CIA_REL,PR_JRNL_LN_REF3,PR_LINE_DESC2,PR_JRNL_LN_REF_INT,PR_FEC_PROV_INTERC,
           PR_RUC_PROV_INTERC,PR_SERVICIO_INTERC,PR_ESTADO_INTERC,PR_ID_PROV_INTERC,PR_TIPO_SERV,ID_AGRUPACION,
           PR_FEC_INI_DEVEN,PR_FEC_FIN_DEVEN)
          SELECT 
           BUSINESS_UNIT,TRANSACTION_ID, TRANSACTION_LINE, LEDGER_GROUP, LEDGER, PR_JRNL_LN_REF2, 
           PR_JRNL_TIPO_IVA, PR_JRNL_CICLO,' ',' ',' ',' ',trunc(sysdate),' ',' ',' ',' ',' ',' ',trunc(sysdate),trunc(sysdate)
          FROM PS_PR_JGEN_ACCT_EN_TMP;
          commit; 
          null;
				end if;					
Exception
  when others then
    RESULTADO := PV_APLICACION||' '||sqlerrm;
    raise MY_ERROR; 
end;
/*cls lri fin*/
     if pv_ejecucion = 'C' then
      REGUN.SEND_MAIL.MAIL@COLECTOR('gsibilling@claro.com.ec','mgavilanes@conecel.com,lmedina@conecel.com','tescalante@conecel.com','GSIFacturacion@conecel.com','Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  '||lvfechactual,'Saludos,'||CHr(13)||CHr(13)|| 'Se informa, se actualiz� los registros de financiero del cierre de facturaci�n a la fecha '||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).'||chr(13)||'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'SIS GSI-Billing.'||CHr(13)||CHr(13)||'Conecel.S.A - America Movil.');
      COMMIT;
     end if;
     RESULTADO:=RESULTADO2;
   END IF;
--
EXCEPTION
	WHEN LE_ERROR THEN
		RESULTADO := LV_ERROR||' '||PV_APLICACION;	

  WHEN MY_ERROR THEN
    RESULTADO:=RESULTADO||' '||PV_APLICACION;

  WHEN OTHERS THEN
   RESULTADO:='ERROR '||SQLERRM||' '||PV_APLICACION;

END GRABA_SAFI_PRINCIPAL;

--
PROCEDURE CREA_ASIENTOS (TIPO_FACTURACION IN NUMBER,
                          FECHACIERRER2 IN VARCHAR2,
                          FECHACIERREP  IN DATE,
                          LNANOFINAL IN NUMBER,
                          LNMESFINAL IN NUMBER,
                          FECHACIERRER IN DATE,
                          GLOSA IN VARCHAR2,
                         DOC_TYPE IN VARCHAR2, 
                          LVFECHA IN VARCHAR2,
                          LNDIAFINAL IN VARCHAR2,
                          LVFECHACTUAL IN VARCHAR2,
                          CODIGO_ERROR OUT NUMBER,
                          RESULTADO IN OUT VARCHAR2
                          ) IS

--
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Creaci�n de Asiento Contables por tipos de Documento
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 14/05/2012 9:42:15
  -- Proyecto: [5328] Facturaci�n Electr�nica
  --=====================================================================================--
  --=====================================================================================--
  -- Versi�n:  1.0.1
  -- Descripci�n: Creaci�n de Asiento Contables por tipos de Documento
  --=====================================================================================--
  -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Cristhian Acosta Ch.
  -- Fecha de creaci�n: 28/05/2013 11:57:50
  -- Proyecto: [8693] VENTA DTH POSTPAGO
  --=====================================================================================--
--
LV_CUADRE       NUMBER;
LV_CUADRE2      NUMBER;
TOTU            NUMBER:=0;
TOTG            NUMBER:=0;
SECU            NUMBER;
SECU2           NUMBER;
LVSENTENCIA     VARCHAR2(18000);
LV_CODIGO_GYE   VARCHAR2(3);
LV_CODIGO_UIO   VARCHAR2(3);
LV_DOC_TYPE     VARCHAR2(8);
MY_ERROR        EXCEPTION;
PV_APLICACION   VARCHAR2(50):='GRABA_SAFI_FACT.CREA_ASIENTOS';
--
-- --INI [8693] - CURSOR SUMA POR CONTRAPARTIDAS -- 
CURSOR C_SUM_CTRAP (CV_COMPANIA VARCHAR2,CN_TIPOFACT NUMBER)IS          
     SELECT X.CTA_CTRAP, nvl(SUM(X.SUMA_IVA_DTH),0) VALOR
      FROM (
           --TODOS LOS SERVICIOS 
           SELECT NVL(decode(D.PRODUCTO,'DTH',
                     GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,
                     DECODE(D.COMPA�IA,'Guayaquil','GYE_'||D.CTA_CTRAP||'_IMP','Quito','UIO_'||D.CTA_CTRAP||'_IMP')),
                     D.CTA_CTRAP),D.CTA_CTRAP)  CTA_CTRAP,
           nvl(SUM(D.VALOR),0) SUMA_IVA_DTH 
           FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
           GROUP BY  D.CTA_CTRAP,D.PRODUCTO,D.COMPA�IA
           UNION
           --TODOS LOS DESCUENTOS 
           SELECT NVL(decode(D.PRODUCTO,'DTH',
                      GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,
                      DECODE(D.COMPA�IA,'Guayaquil','GYE_'||D.CTA_CTRAP||'_IMP','Quito','UIO_'||D.CTA_CTRAP||'_IMP')),
                      D.CTA_CTRAP),D.CTA_CTRAP)  CTA_CTRAP,
           (nvl(SUM(D.DESCUENTO),0)*-1) SUMA_IVA_DTH 
           FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS') AND D.DESCUENTO >0
           GROUP BY  D.CTA_CTRAP,D.PRODUCTO,D.COMPA�IA
           UNION
           --VALOR DEL IVA A SUMAR A LAS CUENTAS DE INTALACI�N - SOLO APLICA A DTH
           SELECT D.CTA_CTRAP,NVL(nvl(SUM(D.VALOR),0) * 
           GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'IVA'),0) SUMA_IVA_DTH
           FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND D.TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
           AND D.CTA_CTRAP =GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                   'Guayaquil','GYE_IVA_','Quito','UIO_IVA_',NULL) || D.CTA_CTRAP)
           AND D.PRODUCTO = 'DTH' GROUP BY  D.CTA_CTRAP, GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'IVA')
            UNION
            --VALOR DEL IVA A RESTAR DEL TOTAL - SOLO APLICA A DTH
           SELECT GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                    'Guayaquil','GYE_'||D.CTA_CTRAP||'_RIVA','Quito','UIO_'||D.CTA_CTRAP||'_RIVA',NULL)),
                    NVL((nvl(SUM(D.VALOR),0) * 
                    GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'IVA')) * -1,0) SUMA_IVA_DTH
           FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND D.TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
           AND D.CTA_CTRAP = GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                   'Guayaquil','GYE_IVA_','Quito','UIO_IVA_',NULL) || D.CTA_CTRAP)
           AND D.PRODUCTO = 'DTH'GROUP BY D.CTA_CTRAP, D.COMPA�IA,GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'IVA')
           union
           --VALOR DEL ICE A SUMAR A LAS CUENTAS  - SOLO APLICA A DTH
           SELECT D.CTA_CTRAP,NVL(nvl(SUM(D.VALOR),0) *GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'ICE'),0)
           SUMA_IVA_DTH FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND D.TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
           AND D.CTA_CTRAP =GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                   'Guayaquil','GYE_ICE_','Quito','UIO_ICE_',NULL) || D.CTA_CTRAP)
           AND D.PRODUCTO = 'DTH' GROUP BY  D.CTA_CTRAP,GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'ICE')  
           UNION
            --VALOR DEL ICE A RESTAR DEL TOTAL - SOLO APLICA A DTH
           SELECT GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                    'Guayaquil','GYE_'||D.CTA_CTRAP||'_RICE','Quito','UIO_'||D.CTA_CTRAP||'_RICE',NULL)),
                    NVL((nvl(SUM(D.VALOR),0)*GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'ICE')) * -1,0) SUMA_IVA_DTH
           FROM READ.PRUEBAFINSYS D
           WHERE D.COMPA�IA = CV_COMPANIA AND D.TIPO_FACTURA = CN_TIPOFACT
           AND D.TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
           AND D.CTA_CTRAP = GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8693,DECODE(D.COMPA�IA,
                   'Guayaquil','GYE_ICE_','Quito','UIO_ICE_',NULL) || D.CTA_CTRAP)
           AND D.PRODUCTO = 'DTH' GROUP BY D.CTA_CTRAP, D.COMPA�IA,GRABA_SAFI_FACT.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,D.CTACTBLE,'ICE')          
          ) X
        GROUP BY X.CTA_CTRAP;                
-- FIN [8693] - CURSOR SUMA POR CONTRAPARTIDAS -- 
BEGIN
--
    --
     LV_CODIGO_GYE:=GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(5328,'CODIGO_GYE_F');
     --
     IF LV_CODIGO_GYE IS NULL THEN
       RESULTADO:='PARAMETRO CODIGO_GYE_F NO CONFIGURADO EN LA GV_PARAMTROS ID_TIPO_PARAMETROS 5328';
       RAISE MY_ERROR;
     END IF;
    --
    LV_CODIGO_UIO:=GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(5328,'CODIGO_UIO_F');
    --
      IF LV_CODIGO_UIO IS NULL THEN
         RESULTADO:='PARAMETRO CODIGO_UIO_F NO CONFIGURADO EN LA GV_PARAMTROS ID_TIPO_PARAMETROS 5328';
         RAISE MY_ERROR;
      END IF;
    --
    LV_DOC_TYPE:=DOC_TYPE;  
    --
-----------GYE--NORMAL------------
SECU:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
---
insert into READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_GYE||FECHACIERRER2,ROWNUM+SECU,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU,TRIM(G.CTACTBLE),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
trim(decode(g.producto,'DTH',FN_GLOSA_DTH(g.nombre,g.ctactble,lvfechactual,glosa,TIPO_FACTURACION),glosa))--SRE
,' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Guayaquil' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.CTACTBLE <> '410102';

COMMIT;

SECU:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_GYE||FECHACIERRER2,ROWNUM+SECU,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU,TRIM(G.CTACTBLE),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',TRIM(GLOSA),
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS G WHERE COMPA�IA='Guayaquil' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.PRODUCTO IN ('TARIFARIO','BULK','PYMES')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.CTACTBLE = '410102';
COMMIT;

SECU:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_GYE||FECHACIERRER2,ROWNUM+SECU,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU,TRIM(G.CTACTBLE),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',TRIM(GLOSA),
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS G WHERE COMPA�IA='Guayaquil' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.PRODUCTO='AUTOCONTROL'
AND G.CTACTBLE = '410102';
COMMIT;

--DESCUENTOS
SECU:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_GYE||FECHACIERRER2,ROWNUM+SECU,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU,TRIM(G.CTA_DESC),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(G.DESCUENTO ) VALOR ,(G.DESCUENTO ) VALOR,0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
trim(decode(g.producto,'DTH','DESC '||FN_GLOSA_DTH(g.nombre,g.ctactble,lvfechactual,glosa,TIPO_FACTURACION),'DESC '||glosa)), --SRE
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Guayaquil' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND  G.DESCUENTO>0;
COMMIT;

--DESCUENTOS
/*CLS LRI aumento nvl antes del into*/
-----------------
--INI [8693] - SUMA DE CONTRAPARTIDAS GYE
/*SELECT nvl((SUM(FOREIGN_AMOUNT)),0) INTO TOTG FROM  READ.GSI_FIN_SAFI WHERE OPERATING_UNIT = 'GYE_999'
  AND DOC_TYPE = LV_DOC_TYPE;--5328 PARA DIFERENCIAR LAS CUENTAS POR TIPO DE FACTURACI�N;*/
FOR AZ IN C_SUM_CTRAP('Guayaquil',TIPO_FACTURACION) LOOP
---    
SECU:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_GYE||FECHACIERRER2,ROWNUM+SECU,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
 TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU,AZ.CTA_CTRAP,' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
 'USD',' ','USD','CRRNT',1,1,(AZ.VALOR),(AZ.VALOR),0,
 ' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
 trim(decode(TIPO_FACTURACION,7,REPLACE(glosa,'[%%SERVICIO%%]',NULL),
                              8,REPLACE(glosa,'[%%SERVICIO%%]',NULL),glosa)), --SRE
' ',' ',' ','N',0
FROM DUAL;
END LOOP;
--FIN [8693] - SUMA DE CONTRAPARTIDAS GYE
COMMIT;

--------------------------------
---------UIO--NORMAL---------
SECU2:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_UIO||FECHACIERRER2,ROWNUM+SECU2,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU2,G.CTACTBLE,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
trim(decode(g.producto,'DTH',FN_GLOSA_DTH(g.nombre,g.ctactble,lvfechactual,glosa,TIPO_FACTURACION),glosa)) --SRE
,' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Quito' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.CTACTBLE <> '410102';
COMMIT;

SECU2:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
---
INSERT INTO  READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_UIO||FECHACIERRER2,ROWNUM+SECU2,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU2,G.CTACTBLE,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',TRIM(GLOSA),
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Quito' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.PRODUCTO IN ('TARIFARIO','BULK','PYMES')
AND G.CTACTBLE = '410102';
COMMIT;

SECU2:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_UIO||FECHACIERRER2,ROWNUM+SECU2,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU2,G.CTACTBLE,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(VALOR * -1),(VALOR * -1),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',TRIM(GLOSA),
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Quito' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND G.PRODUCTO='AUTOCONTROL'
AND G.CTACTBLE = '410102';
COMMIT;


--DESCUENTOS
SECU2:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_UIO||FECHACIERRER2,ROWNUM+SECU2,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU2,G.CTA_DESC,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(G.DESCUENTO) VALOR,(G.DESCUENTO) VALOR,0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
trim(decode(g.producto,'DTH','DESC '||FN_GLOSA_DTH(g.nombre,g.ctactble,lvfechactual,glosa,TIPO_FACTURACION),'DESC '||glosa)), --SRE
' ',' ',' ','N',0
FROM READ.PRUEBAFINSYS  G WHERE COMPA�IA='Quito' AND TIPO NOT IN ('006 - CREDITOS','005 - CARGOS')
AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
AND  G.DESCUENTO>0;

COMMIT;

--DESCUENTOS
/*CLS LRI aumento nvl antes del into*/
--INI [8693] - SUMA DE CONTRAPARTIDAS UIO

/*SELECT nvl((SUM(Y.FOREIGN_AMOUNT)),0) INTO TOTU FROM  READ.GSI_FIN_SAFI Y WHERE OPERATING_UNIT = 'UIO_999'
AND DOC_TYPE = LV_DOC_TYPE;--5328 PARA DIFERENCIAR LAS CUENTAS POR TIPO DE FACTURACI�N
*/
FOR AZ IN C_SUM_CTRAP('Quito',TIPO_FACTURACION) LOOP
---
SECU2:=GRABA_SAFI_FACT.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
---
INSERT INTO READ.GSI_FIN_SAFI
SELECT 'CONEC',LV_CODIGO_UIO||FECHACIERRER2,ROWNUM+SECU2,'REAL','LOCAL',TRIM(FECHACIERREP),'BSCS','CONEC',TRIM(LNANOFINAL),
TRIM(LNMESFINAL),'NEXT',TRIM(FECHACIERRER),ROWNUM+SECU2,AZ.CTA_CTRAP,' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(AZ.VALOR),(AZ.VALOR ),0,
' ',LV_DOC_TYPE,' ',TRIM(FECHACIERREP),' ',
trim(decode(TIPO_FACTURACION,7,REPLACE(glosa,'[%%SERVICIO%%]',NULL),
                             8,REPLACE(glosa,'[%%SERVICIO%%]',NULL),glosa)), --SRE
' ',' ',' ','N',0
FROM DUAL;
END LOOP;
--FIN [8693] - SUMA DE CONTRAPARTIDAS UIO
COMMIT;

--OBTENGO EL VALOR PARA EL CUADRE
/*CLS LRI aumento nvl antes del into*/

SELECT nvl((SUM(MONETARY_AMOUNT)),0) INTO LV_CUADRE FROM READ.GSI_FIN_SAFI T
 WHERE T.DOC_TYPE = LV_DOC_TYPE;
--COPIO A FINNACIERO
 RESULTADO:= LV_CUADRE || ' <--> ' ||LV_CUADRE2;
--VALIDO QUE NO EXISTAN DIFERENCIAS
IF LV_CUADRE =0  THEN
  --NORMALES
  --SOLO IVA 12%
  LVSENTENCIA := NULL;
  LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA';
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_'|| LVFECHA ;
  LVSENTENCIA :=  LVSENTENCIA || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS''))';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE ';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;

  --SOLO CICLO
  LVSENTENCIA := NULL;
  LVSENTENCIA := ' insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| LNDIAFINAL ||'''  PR_JRNL_CICLO';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA';-- (cuenta_contable)
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE';
  EXEC_SQL(LVSEntencia);
  COMMIT;

 --IVA 12% + CICLO
  LVSENTENCIA := NULL;
  LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| LNDIAFINAL ||'''  PR_JRNL_CICLO ';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA ';-- (cuenta_contable)
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo =''Y'') ';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_'|| LVFECHA ;
  LVSENTENCIA :=  LVSENTENCIA || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS'')) ';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE ';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;

  --CARGOS Y CREDITOS   + CICLO
  LVSENTENCIA := NULL;
  LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, ''I0''  PR_JRNL_TIPO_IVA, '''|| LNDIAFINAL ||'''  PR_JRNL_CICLO ';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA ';-- (cuenta_contable)
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_'|| LVFECHA ;
  LVSENTENCIA :=  LVSENTENCIA || ' k where k.tipo   in ( ''005 - CARGOS'',''006 - CREDITOS'')) ';
  LVSENTENCIA :=  LVSENTENCIA || ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp x)';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE ';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;


 --- SOLO IVA 12%
  LVSENTENCIA := NULL;
  LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA';
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  LVSENTENCIA :=  LVSENTENCIA || ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp x)';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE ';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;

  --- SOLO CICLO
  LVSENTENCIA := NULL;
  LVSENTENCIA := ' insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| LNDIAFINAL ||'''  PR_JRNL_CICLO';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA';-- (cuenta_contable)
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  LVSENTENCIA :=  LVSENTENCIA || ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp x)';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;

 --IVA 12% + CICLO
  LVSENTENCIA := NULL;
  LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp ';
  LVSENTENCIA :=  LVSENTENCIA || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  LVSENTENCIA :=  LVSENTENCIA || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ';
  LVSENTENCIA :=  LVSENTENCIA || ' from read.gsi_fin_safi t where t.account in ( select /* + rule */ A.CUENTA ';-- (cuenta_contable)
  LVSENTENCIA :=  LVSENTENCIA || ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo =''Y'') ';
  LVSENTENCIA :=  LVSENTENCIA || ' and (t.transaction_id,t.transaction_line) not in (SELECT X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp x)';
  LVSENTENCIA :=  LVSENTENCIA || ' and t.doc_type = '''||LV_DOC_TYPE||'''';--5328 para diferenciar las cuentas por tipo de facturaci�n
  LVSENTENCIA :=  LVSENTENCIA || ' order by TRANSACTION_LINE ';
  EXEC_SQL(LVSENTENCIA);
  COMMIT;

-- GRABA DATOS A FINANCIERO

/*  INSERT INTO PS_JGEN_ACCT_ENTRY@FINSYS
  SELECT * FROM READ.GSI_FIN_SAFI T;

  INSERT INTO SYSADM.PS_PR_JGEN_ACCT_EN@FINSYS
  SELECT PR.*, ' ', ' ' FROM PS_PR_JGEN_ACCT_EN_TMP PR;*/

-- GRABA DATOS A FINANCIERO

--****************************
  commit;
  RESULTADO:= RESULTADO || 'SE COPIO CORRECTAMENTE LAS CUENTAS NORMALES ;) TIPO DE CUENTAS: '||LV_DOC_TYPE;
  RESULTADO:= RESULTADO || ' SE COPIO LA INFORMACION CORRECTAMENTE';
  CODIGO_ERROR:=2;

else

  RESULTADO:= 'NO SE PUDO COPIAR A LA TABLA PS_JGEN_ACCT_ENTRY POR QUE NO CUADRO  RESULTADO = ' || LV_CUADRE ;
  RESULTADO:= RESULTADO|| 'ERROR REVISE POR FAVOR NO CUADRO';
  CODIGO_ERROR:=1;

end if;

EXCEPTION
  WHEN MY_ERROR THEN
  CODIGO_ERROR:=1;
  RESULTADO:= RESULTADO||' '||PV_APLICACION;
  WHEN OTHERS THEN
  RESULTADO:='ERROR '||SQLERRM||' '||PV_APLICACION;
  CODIGO_ERROR:=1;

END CREA_ASIENTOS;
---
FUNCTION VERIFICA_TIPO_FACTURA (PD_PERIODO_FIN IN DATE,
                                PV_CUSTOMER_ID IN INTEGER) RETURN NUMBER IS
--
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Verifica el Tipo de Facturaci�n al que pertenece la Factura(Fisica o Electr�nica)
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 14/05/2012 9:42:15
  -- Proyecto: [5328] Facturaci�n Electr�nica
  --=====================================================================================--
  -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Cristhian Acosta Chambers
  -- Fecha de Modificaci�n: 13/06/2013
  -- Proyecto: [8693] - Venta DTH Post Pago
  --=====================================================================================--
  --=====================================================================================--
  -- Modificado por  :  SUD Cristhian Acosta. (CAC)
  -- Lider proyecto  : SIS Xavier Trivi�o.
  -- Lider PDS       : SUD Cristhian Acosta Chambers
  -- Fecha de Modificacion: 31/07/2014
  -- Proyecto: [9587] - Factura Electronica DTH
  --=====================================================================================--
  --=====================================================================================--
  -- Modificado por  :  SUD Cristhian Acosta. (CAC)
  -- Lider proyecto  : SIS Ma. Elizabeth Estevez
  -- Lider PDS       : SUD Cristhian Acosta Chambers
  -- Fecha de Modificacion: 30/10/2010
  -- Proyecto: [8968] - Activaci�n Masiva de Factura Electronica
  --=====================================================================================--
--
   CURSOR C_TIPOS_FACTURAS (CN_CUSTOMER_ID INTEGER, CD_PERIODO_FIN DATE) IS
     SELECT X.ESTADO, X.FECHA_FIN
       FROM DOC1.CON_MARCA_FACT_ELECT X
      WHERE X.CUSTOMER_ID = CN_CUSTOMER_ID
        AND X.FECHA_INICIO =
            (SELECT MAX(X.FECHA_INICIO)
               FROM DOC1.CON_MARCA_FACT_ELECT X
              WHERE X.CUSTOMER_ID = CN_CUSTOMER_ID
                AND X.FECHA_INICIO < TO_DATE(TO_CHAR(CD_PERIODO_FIN,'DD/MM/YYYY'),'DD/MM/YYYY'))
      ORDER BY X.FECHA_FIN DESC;
  --
    --SUD SRE CURSOR QUE OBTIENE EL ESTADO Y FECHA DE LAS CUENTAS DTH 
   CURSOR C_TIPOS_FACTURAS_DTH (CN_CUSTOMER_ID INTEGER) IS
      SELECT C.PRGCODE
        FROM CUSTOMER_ALL C
       WHERE C.CUSTOMER_ID = CN_CUSTOMER_ID
         AND C.PRGCODE = 7;
  --
  LC_C_TIPOS_FACTURAS C_TIPOS_FACTURAS%ROWTYPE;
  LN_TIPO_FACTURA NUMBER ;
  TIPO_FACTURA NUMBER:=1;--POR DEFECTO FACT. FISICA
  LV_TIPOS_FACTURAS_DTH VARCHAR2(10);
 BEGIN
    --8968 - INI - SUD CAC
    --ESQUEMA PARA QUE APLICA A TODO FACTURA ELECTR�NICA
    IF NVL(GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8968,'GV_APLICA_TIPO_FACTURA'),'N') = 'S' THEN      
      -- Verifica el Cuentas DTH - prgcode 7
      OPEN C_TIPOS_FACTURAS_DTH(PV_CUSTOMER_ID);
      FETCH C_TIPOS_FACTURAS_DTH
      INTO LV_TIPOS_FACTURAS_DTH;
      CLOSE C_TIPOS_FACTURAS_DTH;  
      --
      IF LV_TIPOS_FACTURAS_DTH = '7' THEN
         --FACTURA DTH ELECTR�NICA - 8
         TIPO_FACTURA := NVL(GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8968,'EXTRAE_FACT_ELECT_DTH'),8);
         RETURN TIPO_FACTURA;
      END IF;
      --PARA CUENTAS DE VOZ
      --VALIDACI�N TODOS COMO ELECTRONICA VOZ - 4
      TIPO_FACTURA := NVL(GRABA_SAFI_FACT.GVF_OBTENER_VALOR_PARAMETRO(8968,'EXTRAE_FACT_ELECT'),4);
      RETURN TIPO_FACTURA;      
      --
    END IF;
    --8968 - FIN - SUD CAC
    --ESQUEMA FISICA O ELECTRONICA DEPENDIENTE DE LA ACTIVACION DE F.E.
    -- Busca clientes con marca Factura Electronica
    OPEN C_TIPOS_FACTURAS(PV_CUSTOMER_ID, PD_PERIODO_FIN);
    FETCH C_TIPOS_FACTURAS
      INTO LC_C_TIPOS_FACTURAS;
    CLOSE C_TIPOS_FACTURAS;
    -- Verifica el Cuentas DTH - prgcode 7
    OPEN C_TIPOS_FACTURAS_DTH(PV_CUSTOMER_ID);
    FETCH C_TIPOS_FACTURAS_DTH
      INTO LV_TIPOS_FACTURAS_DTH;
    CLOSE C_TIPOS_FACTURAS_DTH;  
    --INI - [9587] - FACTURA ELECTRONICA DTH 
    IF LV_TIPOS_FACTURAS_DTH = '7' THEN
      TIPO_FACTURA := 7; --FACTURA DTH FISICA
      IF LC_C_TIPOS_FACTURAS.ESTADO = 'A' THEN
        TIPO_FACTURA := 8; --FACTURA DTH ELECTR�NICA
      END IF;
      RETURN TIPO_FACTURA;
    END IF;
    --FIN - [9587] - FACTURA ELECTRONICA DTH 
    --Por defecto TIPO_FACTURA es 1 Factura F�sica VOZ
    IF LC_C_TIPOS_FACTURAS.ESTADO = 'A' THEN      
      TIPO_FACTURA := 4; --FACTURA ELECTRONICA VOZ
    END IF;
    --
    RETURN TIPO_FACTURA;
  
END VERIFICA_TIPO_FACTURA;
--
FUNCTION GVF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO  IN NUMBER,
                                      PV_ID_PARAMETRO       IN VARCHAR2)
                                                            RETURN VARCHAR2 IS
--
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Recoge los Datos configurados desde la gv_parametros
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 14/05/2012 9:42:15
  -- Proyecto: [5328] Facturaci�n Electr�nica
  --=====================================================================================--
--

 CURSOR C_PARAMETRO (CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
 SELECT VALOR FROM GV_PARAMETROS WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
 AND ID_PARAMETRO = CV_ID_PARAMETRO;

 LV_VALOR GV_PARAMETROS.VALOR%TYPE;

BEGIN

 OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
 FETCH C_PARAMETRO INTO LV_VALOR;
 CLOSE C_PARAMETRO;

 RETURN LV_VALOR;

END GVF_OBTENER_VALOR_PARAMETRO;
--
FUNCTION GVF_OBTENER_SECUENCIAS (PV_CODIGO   IN VARCHAR2)
                                 RETURN NUMBER IS
--
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Obtiene las secuencias
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: Ing. Paola Carvajal.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 14/05/2012 9:42:15
  -- Proyecto: [5328] Facturaci�n Electr�nica
  --=====================================================================================--
--

 CURSOR C_SECUENCIA (CV_CODIGO VARCHAR2) IS
    SELECT MAX(ROWNUM)
      FROM read.GSI_FIN_SAFI G
     WHERE SUBSTR(G.TRANSACTION_ID, 1, 2) = CV_CODIGO; --5328 DIFERENCIAR TIPOS

  LN_VALOR_SECUENCIA INTEGER:=0;

BEGIN

  OPEN C_SECUENCIA(PV_CODIGO);
  FETCH C_SECUENCIA INTO LN_VALOR_SECUENCIA;
  CLOSE C_SECUENCIA;

  RETURN NVL(LN_VALOR_SECUENCIA,0);

END GVF_OBTENER_SECUENCIAS;
--
FUNCTION FN_GLOSA_DTH (PV_NOMBRE_CTA IN VARCHAR2,
                       PV_CTA_CTBLE  IN VARCHAR2,                                         
                       PV_FECHACTUAL IN VARCHAR2,
                       PV_GLOSA      IN VARCHAR2,
                       PV_TIPO       IN VARCHAR2 DEFAULT NULL)
                       RETURN VARCHAR2 IS
                       
--=====================================================================================--
-- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
-- Lider proyecto: Ing. Paola Carvajal.
-- PDS: SUD Cristhian Acosta Chambers
-- Fecha de Creaci�n: 13/06/2013
-- Proyecto: [8693] - Venta DTH Post Pago
-- Motivo: Arma la glosa correspondiente para el producto DTH
--=====================================================================================--    
--=====================================================================================--
-- Modificado por  :  SUD Cristhian Acosta. (CAC)
-- Lider proyecto  : SIS Xavier Trivi�o.
-- Lider PDS       : SUD Cristhian Acosta Chambers
-- Fecha de Modificacion: 31/07/2014
-- Proyecto: [9587] - Factura Electronica DTH
-- Motivo: Arma la glosa correspondiente para el producto DTH por tipo de Facturaci�n
--=====================================================================================--                   

LN_EXISTE           NUMBER;
LE_ERROR            EXCEPTION;
LB_FOUND            BOOLEAN := FALSE;
GLOSA               VARCHAR2(6000);
LV_FECHAPOSTERIOR   VARCHAR2(20);

CURSOR C_ES_POST (CV_CTA_CTBLE VARCHAR2, CV_NOMBRE_CTA VARCHAR2) IS     
  SELECT DISTINCT 1 FROM COB_SERVICIOS C
  WHERE C.CTAPSOFT=CV_CTA_CTBLE
  AND C.NOMBRE=CV_NOMBRE_CTA
  AND C.CTAPOST IS NULL;  
--  
CURSOR C_ES_POST_1(CV_CTA_CTBLE VARCHAR2, CV_NOMBRE_CTA VARCHAR2) IS
   SELECT DISTINCT 1 FROM COB_SERVICIOS B
   WHERE B.NOMBRE =CV_NOMBRE_CTA
   AND B.CTAPOST =CV_CTA_CTBLE;   
    
CURSOR C_TIPOS_FACTURAS IS
   SELECT S.GLOSA
   FROM TIPOS_FACTURAS_SAFI S
   WHERE S.ESTADO = 'A'    
   AND S.ID_TIPO IN (PV_TIPO) --[9587] - SUD CAC
   ORDER BY 1;  

BEGIN

  IF PV_CTA_CTBLE IS NULL THEN
    RAISE LE_ERROR;
  END IF;
 
  IF PV_NOMBRE_CTA IS NULL THEN
    RAISE LE_ERROR;
  END IF;
  
  IF PV_FECHACTUAL IS NULL THEN
    RAISE LE_ERROR;
  END IF;
  
  IF PV_GLOSA IS NULL THEN
    RAISE LE_ERROR;
  END IF;
  --
  OPEN C_ES_POST(PV_CTA_CTBLE, PV_NOMBRE_CTA);
  FETCH C_ES_POST INTO LN_EXISTE;
    LB_FOUND := C_ES_POST%FOUND;
  CLOSE C_ES_POST;
  --
  IF NOT LB_FOUND THEN 
     OPEN C_ES_POST_1(PV_CTA_CTBLE, PV_NOMBRE_CTA);
     FETCH C_ES_POST_1 INTO LN_EXISTE;
      LB_FOUND := C_ES_POST_1%FOUND;
     CLOSE C_ES_POST_1;
  END IF; 
  --
  IF LB_FOUND THEN
    RETURN REPLACE(PV_GLOSA,'[%%SERVICIO%%]',PV_NOMBRE_CTA);
  ELSE
    OPEN C_TIPOS_FACTURAS;
      FETCH C_TIPOS_FACTURAS INTO GLOSA;
      LB_FOUND := C_TIPOS_FACTURAS%FOUND;
    CLOSE C_TIPOS_FACTURAS;
    --
    IF LB_FOUND THEN      
      LV_FECHAPOSTERIOR:=TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHACTUAL,'DD/MM/YYYY'),1),'DD/MM/YYYY');           
      RETURN REPLACE(REPLACE(REPLACE(GLOSA,'[%%SERVICIO%%]',PV_NOMBRE_CTA),'[%%FECHANTERIOR%%]',PV_FECHACTUAL),'[%%FECHACTUAL%%]',LV_FECHAPOSTERIOR);      
    ELSE
      RAISE LE_ERROR; 
    END IF;    
   END IF;

EXCEPTION
  WHEN LE_ERROR THEN
    RETURN 'Error al Obtener la glosa DTH';
  WHEN OTHERS THEN
    RETURN 'Error al Obtener la glosa DTH';

END FN_GLOSA_DTH;
--
FUNCTION FN_IMPUESTO_DTH_IVA_ICE(PV_NOMBRE IN VARCHAR2, 
                                 PV_CTA_CTBLE IN VARCHAR2,
                                 PV_TIPO_IMP IN VARCHAR2)
                                 RETURN NUMBER IS

--=====================================================================================--
-- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
-- Lider proyecto: Ing. Paola Carvajal.
-- PDS: SUD Cristhian Acosta Chambers
-- Fecha de Creaci�n: 13/06/2013
-- Proyecto: [8693] - Venta DTH Post Pago
-- Motivo: Calculo de Impuestos IVA - ICE
--=====================================================================================--                                  

CURSOR C_OBT_IVA(CV_NOMBRE_SERV VARCHAR2,CV_CTA_CTBLE VARCHAR2) IS
   SELECT DISTINCT B.TIPO_IVA 
   FROM  COB_SERVICIOS B
   WHERE B.NOMBRE =CV_NOMBRE_SERV
   AND B.CTAPSOFT =CV_CTA_CTBLE;
/**/
CURSOR C_OBT_IVA_POST(CV_NOMBRE_SERV VARCHAR2,CV_CTA_CTBLE VARCHAR2) IS
   SELECT DISTINCT B.TIPO_IVA 
   FROM  COB_SERVICIOS B
   WHERE B.NOMBRE =CV_NOMBRE_SERV
   AND B.CTAPOST =CV_CTA_CTBLE;   
   

CURSOR C_OBT_VALOR_IVA(CN_VALOR NUMBER) IS
   SELECT T.TAXRATE/100 
   FROM TAX_CATEGORY T 
   WHERE T.TAXCAT_ID=CN_VALOR;   

LV_OBT_IVA        VARCHAR2(30);
LN_OBT_VALOR_IVA      NUMBER(4,2);
LE_ERROR              EXCEPTION;
LB_FOUND              BOOLEAN := FALSE;

BEGIN
--  
   IF PV_NOMBRE IS NULL THEN   
     RAISE LE_ERROR;
   END IF;  
   IF PV_CTA_CTBLE IS NULL THEN 
      RAISE LE_ERROR;
   END IF;

  OPEN C_OBT_IVA(PV_NOMBRE,PV_CTA_CTBLE);
  FETCH C_OBT_IVA INTO LV_OBT_IVA;
    LB_FOUND := C_OBT_IVA%NOTFOUND;
  CLOSE C_OBT_IVA;  
  --
    IF LB_FOUND THEN
         OPEN C_OBT_IVA_POST(PV_NOMBRE,PV_CTA_CTBLE);
        FETCH C_OBT_IVA_POST INTO LV_OBT_IVA;
          LB_FOUND := C_OBT_IVA_POST%NOTFOUND;
        CLOSE C_OBT_IVA_POST;
    END IF;
    --
    IF LV_OBT_IVA IS NULL THEN
        RETURN 0;
    END IF;
  --
  IF PV_TIPO_IMP = 'IVA' THEN
      IF LV_OBT_IVA ='I12' THEN
          OPEN C_OBT_VALOR_IVA(3);
          FETCH C_OBT_VALOR_IVA INTO LN_OBT_VALOR_IVA;
          CLOSE C_OBT_VALOR_IVA;
          IF LN_OBT_VALOR_IVA IS NULL THEN
              RETURN 0;
          END IF;
      ELSIF LV_OBT_IVA ='I27' THEN          
          OPEN C_OBT_VALOR_IVA(8);
          FETCH C_OBT_VALOR_IVA INTO LN_OBT_VALOR_IVA;
          CLOSE C_OBT_VALOR_IVA;
          IF LN_OBT_VALOR_IVA IS NULL THEN
              RETURN 0;
          END IF;
      END IF;
   ELSIF PV_TIPO_IMP = 'ICE' THEN 
      IF LV_OBT_IVA ='I27' THEN
          OPEN C_OBT_VALOR_IVA(4);
          FETCH C_OBT_VALOR_IVA INTO LN_OBT_VALOR_IVA;
          CLOSE C_OBT_VALOR_IVA;
          IF LN_OBT_VALOR_IVA IS NULL THEN
              RETURN 0;
          END IF;
       END IF;
   END IF;
   RETURN LN_OBT_VALOR_IVA;
EXCEPTION 
  WHEN LE_ERROR THEN 
    RETURN 0;
  WHEN OTHERS THEN
    RETURN 0;    
END FN_IMPUESTO_DTH_IVA_ICE;
--
PROCEDURE GEN_RUBROS_NEG_DTH (FECHACIERREP  IN DATE,                         
                              LV_ERROR IN OUT VARCHAR2
                              ) IS
                              
--=====================================================================================--
  -- Creador por:    SUD Cristhian Acosta Chambers
  -- L�der proyecto: SIS Paola Carvajal
  -- Lider PDS:      SUD Cristhian Acosta Chambers
  -- Proyecto:       [8693] - VENTA DTH POSTPAGO
	-- Proposito:      Control de Rubros Negativos DTH.
--=====================================================================================--	          
--CONTROL RUBROS NEGATIVOS 
CURSOR C_RUBRO_NEG_DTH IS 
    SELECT Y.*, ROWID LROWID FROM PRUEBAFINSYS_DTH Y;  
--ACTUALIZA RUBROS NEGATIVOS
CURSOR C_CTAPOST_NEG(CV_PERIODO VARCHAR2) IS 
    SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
      FROM PRUEBAFINSYS_DTH D
     WHERE D.CTA_CTRAP IS NULL
       AND D.PRODUCTO = 'DTH'
       AND D.FECHA_CORTE = TO_DATE(CV_PERIODO,'DD/MM/YYYY');
--
CURSOR DESCX(CV_PERIODO VARCHAR2) IS
SELECT DISTINCT R.CTAPSOFT,R.CTA_DSCTO, R.CONTRAPARTIDA_GYE, R.CONTRAPARTIDA_UIO  
 FROM READ.COB_SERVICIOS R 
 WHERE R.CTAPSOFT IN ( SELECT DISTINCT D.CTACTBLE FROM PRUEBAFINSYS_DTH D
                        WHERE D.CTA_CTRAP IS NULL AND D.PRODUCTO = 'DTH'
                        AND D.FECHA_CORTE = TO_DATE(CV_PERIODO,'DD/MM/YYYY')
                      ); 
--
CURSOR CONSULTA_RUBRO (CV_TIPO PRUEBAFINSYS_DTH.TIPO%TYPE,
                       CV_NOMBRE PRUEBAFINSYS_DTH.NOMBRE%TYPE,
                       CV_PRODUCTO PRUEBAFINSYS_DTH.PRODUCTO%TYPE,
                       CV_COMPA�IA PRUEBAFINSYS_DTH.COMPA�IA%TYPE,
                       CV_CTACTBLE PRUEBAFINSYS_DTH.CTACTBLE%TYPE,
                       CV_TIPO_FACTURA PRUEBAFINSYS_DTH.TIPO_FACTURA%TYPE) IS
      select 'X' from  READ.PRUEBAFINSYS S
       WHERE S.TIPO = CV_TIPO--LC_RUBRO_NEG_DTH.TIPO
       AND S.NOMBRE = CV_NOMBRE--LC_RUBRO_NEG_DTH.NOMBRE
       AND S.PRODUCTO = CV_PRODUCTO--LC_RUBRO_NEG_DTH.PRODUCTO
       AND S.COMPA�IA = CV_COMPA�IA--LC_RUBRO_NEG_DTH.COMPA�IA
       AND S.CTACTBLE = CV_CTACTBLE--LC_RUBRO_NEG_DTH.CTACTBLE
       AND S.TIPO_FACTURA = CV_TIPO_FACTURA--LC_RUBRO_NEG_DTH.TIPO_FACTURA
       ;
LV_EXISTE_RUBRO VARCHAR2(2):=NULL;  
LB_BOOLEAN_EXI BOOLEAN:=FALSE;  
--
 --RECOGE CUENTAS POST PAGO DEL PRODUCTO DTH
CURSOR C_CTAPOST IS 
    SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
      FROM READ.PRUEBAFINSYS D
     WHERE D.CTA_CTRAP IS NULL
       AND D.PRODUCTO = 'DTH';
--
CURSOR DESCX_2 IS
SELECT DISTINCT R.CTAPSOFT,R.CTA_DSCTO, R.CONTRAPARTIDA_GYE, R.CONTRAPARTIDA_UIO  
 FROM READ.COB_SERVICIOS R 
 WHERE R.CTAPSOFT IN ( SELECT DISTINCT D.CTACTBLE FROM read.PRUEBAFINSYS D
                        WHERE D.CTA_CTRAP IS NULL AND D.PRODUCTO = 'DTH'                        
                      );       
--
TYPE RC_CURSOR IS REF CURSOR;
LC_CURSOR RC_CURSOR;
LC_RUBRO_NEG_DTH C_RUBRO_NEG_DTH%ROWTYPE;                              
--                              
LVSENTENCIA    VARCHAR2(18000):=NULL;
LVFECHA        VARCHAR2(10):=TO_CHAR(FECHACIERREP, 'DDMMYYYY');
PV_FECHACIERRE VARCHAR2(70):= TO_CHAR(FECHACIERREP,'DD/MM/YYYY'); 
LE_ERROR       EXCEPTION;
LE_ERROR2      EXCEPTION;
--
BEGIN
   LVSENTENCIA:=NULL;
   LVSENTENCIA:=' DELETE  /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH X WHERE X.FECHA_CORTE = TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY'')';   
   BEGIN 
   EXECUTE IMMEDIATE LVSENTENCIA;
   COMMIT;
   EXCEPTION 
     WHEN OTHERS THEN
       LV_ERROR :='ERROR => '||SQLERRM;
        RAISE LE_ERROR;	
     END;
   --RECOGE LOS RUBROS NEGATIVOS DEL PRODUCTO DTH
   LVSENTENCIA:= NULL;
   LVSENTENCIA:= ' INSERT INTO PRUEBAFINSYS_DTH ';
   LVSENTENCIA:= LVSENTENCIA||' SELECT /* + RULE */  H.TIPO,H.NOMBRE,H.PRODUCTO,SUM(H.VALOR) VALOR ,SUM(H.DESCUENTO) DESCUENTO,H.COST_DESC COMPA�IA , '
                            ||' DECODE(H.PRODUCTO,''DTH'',H.CTACLBLEP,MAX(H.CTACLBLEP)), '
                            ||' NULL  CUENTA_DESC, GRABA_SAFI_FACT.VERIFICA_TIPO_FACTURA(TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY''),H.CUSTOMER_ID) TIPO_FACTURACION, NULL ';
   LVSENTENCIA:= LVSENTENCIA||' ,H.CUSTCODE, '''||PV_FECHACIERRE||''', NULL , H.CUSTOMER_ID ';
   LVSENTENCIA:= LVSENTENCIA||' FROM CO_FACT_'||LVFECHA||' H '||
                              ' WHERE H.CUSTOMER_ID NOT  IN (SELECT CUSTOMER_ID FROM CUSTOMER_TAX_EXEMPTION CTE WHERE CTE.EXEMPT_STATUS = ''A'') ';
   --LVSENTENCIA:= LVSENTENCIA||' AND H.PRODUCTO = ''DTH'' AND H.VALOR < 0 AND SERVICIO NOT IN (46,38) '; --9578 - Se cambia la validaci�n para excluir por tipo
   LVSENTENCIA:= LVSENTENCIA||' AND H.PRODUCTO = ''DTH'' AND H.VALOR < 0 AND TIPO NOT IN (''006 - CREDITOS'',''005 - CARGOS'') ';    
   LVSENTENCIA:= LVSENTENCIA||' GROUP BY H.TIPO,H.NOMBRE,H.PRODUCTO,H.COST_DESC, GRABA_SAFI_FACT.VERIFICA_TIPO_FACTURA(TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY''),H.CUSTOMER_ID),H.CTACLBLEP ,H.CUSTCODE, H.CUSTOMER_ID ';
   --
   BEGIN
   EXECUTE IMMEDIATE LVSENTENCIA;
   COMMIT;
    EXCEPTION 
     WHEN OTHERS THEN
       LV_ERROR :='ERROR => '||SQLERRM;
        RAISE LE_ERROR;	
     END;
   --
   LVSENTENCIA:=NULL;
   LVSENTENCIA:=' SELECT  /*+INDEX (X IDX_FINSYS_3)+*/ X.*, ROWID FROM PRUEBAFINSYS_DTH X WHERE X.FECHA_CORTE = TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY'')';
   OPEN LC_CURSOR FOR LVSENTENCIA;
   --SE REVISA Y ACTUALIZA SI EL RUBRO APLICA A SER CONTABILIZADO
    LOOP
      LC_RUBRO_NEG_DTH := NULL;
      FETCH LC_CURSOR INTO LC_RUBRO_NEG_DTH;
      EXIT WHEN LC_CURSOR%NOTFOUND;
      --
      LVSENTENCIA:=NULL;
      LVSENTENCIA:=' UPDATE PRUEBAFINSYS_DTH X '||
                   ' SET X.ESTADO = NVL((SELECT ''S'' FROM ORDERHDR_ALL D WHERE D.CUSTOMER_ID IN (X.CUSTOMER_ID) '||
                                     ' AND D.OHENTDATE = TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY'') '||
                                     ' AND D.OHSTATUS = ''IN'' AND D.OHINVAMT_DOC > 0),''N'') '||
                   ' WHERE X.ROWID = '''||LC_RUBRO_NEG_DTH.LROWID||'''';
      -- 
      BEGIN            
      EXECUTE IMMEDIATE LVSENTENCIA;
      COMMIT;                                      
       EXCEPTION 
      WHEN OTHERS THEN
       LV_ERROR :='ERROR => '||SQLERRM;
        RAISE LE_ERROR;	
      END;
    END LOOP;
   CLOSE LC_CURSOR;
    --
    LVSENTENCIA:=NULL;    
    LVSENTENCIA:=' SELECT /*+INDEX (X IDX_FINSYS_3)+*/ S.TIPO TIPO,S.NOMBRE NOMBRE,S.PRODUCTO PRODUCTO,SUM(S.VALOR) VALOR,SUM(S.DESCUENTO) DESCUENTO, '||
                 ' S.COMPA�IA COMPA�IA,S.CTACTBLE CTACTBLE,NULL CTA_DESC,S.TIPO_FACTURA TIPO_FACTURA, '||
                 ' NULL CTA_CTRAP, NULL CUSTCODE ,NULL FECHA_CORTE,NULL ESTADO,NULL CUSTOMER_ID, NULL '||
                 ' FROM PRUEBAFINSYS_DTH S WHERE S.ESTADO <>''N'' AND S.FECHA_CORTE = TO_DATE('''||PV_FECHACIERRE||''',''DD/MM/YYYY'') '||
                 ' GROUP BY S.TIPO,S.NOMBRE,S.PRODUCTO,S.COMPA�IA,S.TIPO_FACTURA,S.CTACTBLE ';
    --                                 
    OPEN LC_CURSOR FOR LVSENTENCIA;
    LOOP
      LC_RUBRO_NEG_DTH := NULL;
      FETCH LC_CURSOR INTO LC_RUBRO_NEG_DTH;
      EXIT WHEN LC_CURSOR%NOTFOUND;
      --                     
      BEGIN
      --
      LV_EXISTE_RUBRO:=NULL;
      LB_BOOLEAN_EXI:=FALSE;
      OPEN CONSULTA_RUBRO (LC_RUBRO_NEG_DTH.TIPO,
                           LC_RUBRO_NEG_DTH.NOMBRE,
                           LC_RUBRO_NEG_DTH.PRODUCTO,
                           LC_RUBRO_NEG_DTH.COMPA�IA,
                           LC_RUBRO_NEG_DTH.CTACTBLE,
                           LC_RUBRO_NEG_DTH.TIPO_FACTURA);
       --                           
       FETCH CONSULTA_RUBRO INTO LV_EXISTE_RUBRO;
       LB_BOOLEAN_EXI:=CONSULTA_RUBRO%FOUND;
       CLOSE CONSULTA_RUBRO;
       --
       IF LB_BOOLEAN_EXI THEN
         --SE ACTUALIZAN LOS VALORES 
         UPDATE READ.PRUEBAFINSYS S
          SET S.VALOR = S.VALOR + LC_RUBRO_NEG_DTH.VALOR,
              S.DESCUENTO = S.DESCUENTO + LC_RUBRO_NEG_DTH.DESCUENTO
         WHERE S.TIPO = LC_RUBRO_NEG_DTH.TIPO
         AND S.NOMBRE = LC_RUBRO_NEG_DTH.NOMBRE
         AND S.PRODUCTO = LC_RUBRO_NEG_DTH.PRODUCTO
         AND S.COMPA�IA = LC_RUBRO_NEG_DTH.COMPA�IA
         AND S.CTACTBLE = LC_RUBRO_NEG_DTH.CTACTBLE
         AND S.TIPO_FACTURA = LC_RUBRO_NEG_DTH.TIPO_FACTURA;
        --
         IF SQL%FOUND THEN         
          COMMIT;
         ELSIF SQL%NOTFOUND THEN
            LV_ERROR :='ERROR => '||'No se actualizo rubro negativo Tipo:'||LC_RUBRO_NEG_DTH.TIPO||
                                    ' - Nombre: '||LC_RUBRO_NEG_DTH.NOMBRE||' - '||LC_RUBRO_NEG_DTH.PRODUCTO;
            RAISE LE_ERROR2;	
         END IF; 
       ELSE 
         --SE INSERTAN LOS RUBROS
         insert into READ.PRUEBAFINSYS(VALOR, 
                                       DESCUENTO, 
                                       TIPO,
                                       NOMBRE,
                                       PRODUCTO,
                                       COMPA�IA,
                                       CTACTBLE,
                                       TIPO_FACTURA)
                                 VALUES(LC_RUBRO_NEG_DTH.VALOR,
                                        LC_RUBRO_NEG_DTH.DESCUENTO,
                                        LC_RUBRO_NEG_DTH.TIPO,
                                        LC_RUBRO_NEG_DTH.NOMBRE,
                                        LC_RUBRO_NEG_DTH.PRODUCTO,
                                        LC_RUBRO_NEG_DTH.COMPA�IA,
                                        LC_RUBRO_NEG_DTH.CTACTBLE,
                                        LC_RUBRO_NEG_DTH.TIPO_FACTURA
                                        );
       END IF;
       --      
      EXCEPTION 
      WHEN LE_ERROR2 THEN
        RAISE LE_ERROR;	        
      WHEN OTHERS THEN
       LV_ERROR :='ERROR => '||SQLERRM;
        RAISE LE_ERROR;	
      END;                                          
     END LOOP;
     CLOSE LC_CURSOR;
     --   
     --ACTUALIZA EN BITACORA LAS CUENTAS CONTRAPARTIDAS
     FOR RD IN DESCX(PV_FECHACIERRE)LOOP
     UPDATE /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH F
     SET F.CTA_DESC  = RD.CTA_DSCTO,
         F.CTA_CTRAP = DECODE(F.COMPA�IA,'Guayaquil',RD.CONTRAPARTIDA_GYE,
                              'Quito',RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
     WHERE F.CTACTBLE = RD.CTAPSOFT
     AND F.FECHA_CORTE = TO_DATE(PV_FECHACIERRE,'DD/MM/YYYY');   
     END LOOP;
     COMMIT;     
     --ACTUALIZA CUENTAS EN BITACORA POST PAGADAS CON LA SU CONTRAPARTIDA - SUD NCA
     FOR AX IN C_CTAPOST_NEG(PV_FECHACIERRE) LOOP
         UPDATE /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH A
         SET A.CTA_CTRAP =(SELECT DECODE(A.COMPA�IA,'Guayaquil',D.CONTRAPARTIDA_GYE,
                                  'Quito',D.CONTRAPARTIDA_UIO)
                           FROM COB_SERVICIOS D WHERE D.CTAPOST = AX.CTACTBLE
                           AND D.NOMBRE = AX.NOMBRE)
         WHERE A.ROWID = AX.ROWID;    
     END LOOP
     COMMIT;     
     --
     --ACTUALIZA CUENTAS POST PAGADAS CON LA SU CONTRAPARTIDA - SUD SRE
     FOR AZ IN C_CTAPOST LOOP
      UPDATE READ.PRUEBAFINSYS A
         SET A.CTA_CTRAP =
         (SELECT DECODE(A.COMPA�IA,'Guayaquil',D.CONTRAPARTIDA_GYE,
                        'Quito',D.CONTRAPARTIDA_UIO)
            FROM COB_SERVICIOS D WHERE D.CTAPOST = AZ.CTACTBLE
           AND D.NOMBRE = AZ.NOMBRE)
       WHERE A.ROWID = AZ.ROWID;
     END LOOP
     COMMIT;
     --
     --ASIGNA CUENTAS CONTRAPARTIDAS SEGUN LA COMPA�IA - SUD SRE
     FOR RD IN DESCX_2 LOOP
      UPDATE READ.PRUEBAFINSYS F
      SET F.CTA_DESC  = RD.CTA_DSCTO,
          F.CTA_CTRAP = DECODE(F.COMPA�IA,'Guayaquil',RD.CONTRAPARTIDA_GYE,
                              'Quito',RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
      WHERE F.CTACTBLE = RD.CTAPSOFT;
      END LOOP;
      COMMIT;
     --
 EXCEPTION
   WHEN LE_ERROR THEN
     LV_ERROR:='GEN_RUBROS_NEG_DTH - '||LV_ERROR;
      IF LC_CURSOR%ISOPEN THEN
        CLOSE LC_CURSOR;        
        END IF;
   WHEN OTHERS THEN
     LV_ERROR:='GEN_RUBROS_NEG_DTH - ERROR => '||SQLERRM;
        IF LC_CURSOR%ISOPEN THEN
        CLOSE LC_CURSOR;        
        END IF;
END GEN_RUBROS_NEG_DTH;
--
end GRABA_SAFI_FACT;
/
