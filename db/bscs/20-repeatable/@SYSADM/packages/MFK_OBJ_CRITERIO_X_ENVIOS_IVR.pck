CREATE OR REPLACE PACKAGE MFK_OBJ_CRITERIO_X_ENVIOS_IVR IS

  PROCEDURE mfp_insertar(pn_idenvio    IN mf_criterios_x_envios_ivr.idenvio%TYPE,
                         pn_idcriterio IN mf_criterios_x_envios_ivr.idcriterio%TYPE,
                         pv_valor      IN mf_criterios_x_envios_ivr.valor%TYPE,
                         pv_efecto     IN mf_criterios_x_envios_ivr.efecto%TYPE,
                         pv_sub_categ  IN VARCHAR2 DEFAULT NULL,
                         pv_msg_error  IN OUT VARCHAR2);

  PROCEDURE mfp_detalle_inserta(pn_id_envio    IN mf_detalles_envios_ivr.idenvio%TYPE,
                                pn_id_criterio IN mf_criterios_x_envios_ivr.idcriterio%TYPE,
                                pv_trama       IN VARCHAR2,
                                pv_error       OUT VARCHAR2);

  PROCEDURE mfp_clonar(pn_idenvio     IN mf_criterios_x_envios_ivr.idenvio%TYPE,
                       pn_out_idenvio IN OUT mf_criterios_x_envios_ivr.idenvio%TYPE,
                       pv_msg_error   IN OUT VARCHAR2);

END MFK_OBJ_CRITERIO_X_ENVIOS_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_CRITERIO_X_ENVIOS_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar los criterios en la tabla
  **               MF_CRITERIOS_X_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar(pn_idenvio    IN mf_criterios_x_envios_ivr.idenvio%TYPE,
                         pn_idcriterio IN mf_criterios_x_envios_ivr.idcriterio%TYPE,
                         pv_valor      IN mf_criterios_x_envios_ivr.valor%TYPE,
                         pv_efecto     IN mf_criterios_x_envios_ivr.efecto%TYPE,
                         pv_sub_categ  IN VARCHAR2 DEFAULT NULL,
                         pv_msg_error  IN OUT VARCHAR2) IS
                         
    CURSOR c_exist(cn_id_envio NUMBER, cn_idcriterio NUMBER) IS
      SELECT COUNT(*) AS cantidad
        FROM mf_criterios_x_envios_ivr
       WHERE idenvio = cn_id_envio
         AND idcriterio = cn_idcriterio;
  
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS_IVR.MFP_INSERTAR';
    lv_estado     mf_criterios_x_envios_ivr.estado%TYPE := 'A';
    ln_secuencia  mf_criterios_x_envios_ivr.secuencia%TYPE;
    le_error      EXCEPTION;
    lv_error      VARCHAR2(1000);
    lc_exist      c_exist%ROWTYPE;
  
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_exist(pn_idenvio, pn_idcriterio);
    FETCH c_exist
      INTO lc_exist;
    CLOSE c_exist;
  
    IF lc_exist.cantidad = 0 THEN
      SELECT mfs_criteriosxenvio_sec_ivr.nextval
        INTO ln_secuencia
        FROM dual;
    
      INSERT INTO mf_criterios_x_envios_ivr
        (idenvio, idcriterio, secuencia, valor, efecto, estado)
      VALUES
        (pn_idenvio,
         pn_idcriterio,
         ln_secuencia,
         pv_valor,
         pv_efecto,
         lv_estado);
    
      IF pv_sub_categ IS NOT NULL THEN
        mfp_detalle_inserta(pn_idenvio,
                            pn_idcriterio,
                            pv_sub_categ,
                            lv_error);
        IF lv_error IS NOT NULL THEN
          RAISE le_error;
        END IF;
      END IF;
    END IF;
    
  EXCEPTION
    WHEN le_error THEN
      pv_msg_error := lv_error;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar el detalle de criterios por envios en
  **               la tabla MF_DET_CRITERIOS_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_detalle_inserta(pn_id_envio    IN mf_detalles_envios_ivr.idenvio%TYPE,
                                pn_id_criterio IN mf_criterios_x_envios_ivr.idcriterio%TYPE,
                                pv_trama       IN VARCHAR2,
                                pv_error       OUT VARCHAR2) AS
                                
    CURSOR c_exist(cn_id_envio     NUMBER,
                   cn_idcriterio   NUMBER,
                   cv_subcategoria VARCHAR2) IS
      SELECT COUNT(*) AS cantidad
        FROM mf_det_criterios_envios_ivr
       WHERE id_envio = cn_id_envio
         AND idcriterio = cn_idcriterio
         AND subcategoria = cv_subcategoria;
  
    lv_aplicacion   VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS_IVR.MFP_DETALLE_INSERTA';
    lv_error        VARCHAR2(1000);
    le_error        EXCEPTION;
    --10798 INI SUD KBA
    /*lv_data         VARCHAR2(2000) := pv_trama;
    lv_data_tmp     VARCHAR2(100) := lv_data;
    lv_data_temp    VARCHAR2(100) := lv_data;*/
    lv_data          VARCHAR2(32000):= pv_trama;
    lv_data_tmp     VARCHAR2(32000) := lv_data;
    lv_data_temp    VARCHAR2(32000) := lv_data;
    --10798 FIN SUD KBA
    ln_posicion_act NUMBER(10) := 1;
    lv_id           VARCHAR2(1000);
    ln_avanza       NUMBER(10) := length(lv_data_tmp);
    lc_exist        c_exist%ROWTYPE;
    
  BEGIN
    
    WHILE (ln_avanza > 1) LOOP
      ln_posicion_act := instr(lv_data_tmp, '|', 1);
      lv_data_temp    := substr(lv_data_tmp,
                                ln_posicion_act + 1,
                                length(lv_data_tmp));
      lv_id           := substr(lv_data_tmp, 1, ln_posicion_act - 1);
      lv_data_tmp     := lv_data_temp;
    
      IF lv_id IS NOT NULL THEN
        OPEN c_exist(pn_id_envio, pn_id_criterio, lv_id);
        FETCH c_exist
          INTO lc_exist;
        CLOSE c_exist;
        IF lc_exist.cantidad = 0 THEN
          BEGIN
            INSERT INTO mf_det_criterios_envios_ivr
              (id_envio, idcriterio, subcategoria, estado)
            VALUES
              (pn_id_envio, pn_id_criterio, lv_id, 'A');
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := 'ERROR EN LA APLICACION ' || lv_aplicacion ||
                          '|EN LA INSERCION DEL DETALLE INTERNO ' ||
                          substr(SQLERRM, 1, 250);
              RAISE le_error;
          END;
        END IF;
      END IF;
      ln_avanza := length(lv_data_tmp);
    END LOOP;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error;
    WHEN OTHERS THEN
      pv_error := 'ERROR EN LA APLICACION ' || lv_aplicacion || '|' || substr(SQLERRM, 1, 250);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite clonar los envios para IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_clonar(pn_idenvio     IN mf_criterios_x_envios_ivr.idenvio%TYPE,
                       pn_out_idenvio IN OUT mf_criterios_x_envios_ivr.idenvio%TYPE,
                       pv_msg_error   IN OUT VARCHAR2) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER) IS
      SELECT * FROM mf_envios_ivr e WHERE e.idenvio = cn_idenvio;
  
    lc_currentrow   c_table_cur%ROWTYPE;
    le_nodataclonar EXCEPTION;
    lv_aplicacion   VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS_IVR.MFP_ELIMINAR';
    lb_found        BOOLEAN;
    ln_secuencia    NUMBER;
    lv_error        VARCHAR2(2000);
  
  BEGIN
    
    OPEN c_table_cur(pn_idenvio);
    FETCH c_table_cur
      INTO lc_currentrow;
    lb_found := c_table_cur%FOUND;
    CLOSE c_table_cur;
    IF NOT lb_found THEN
      RAISE le_nodataclonar;
    END IF;
    
    IF lb_found THEN
      SELECT mfs_envios_sec_ivr.nextval INTO ln_secuencia FROM dual;
      BEGIN
        INSERT INTO mf_envios_ivr
          (idenvio,
           descripcion,
           estado,
           cantidad_mensajes,
           horas_separacion_msn,
           forma_envio,
           mensaje,
           monto_minimo,
           monto_maximo,
           region,
           tipo_cliente,
           requiere_edad_mora,
           usuario_ingreso,
           fecha_ingreso,
           usuario_modificacion,
           fecha_modificacion,
           id_ciclo,
           score,
           tipo_envio,FINANCIAMIENTO)
          (SELECT ln_secuencia,
                  descripcion,
                  'I',
                  cantidad_mensajes,
                  horas_separacion_msn,
                  forma_envio,
                  mensaje,
                  monto_minimo,
                  monto_maximo,
                  region,
                  tipo_cliente,
                  requiere_edad_mora,
                  usuario_ingreso,
                  fecha_ingreso,
                  usuario_modificacion,
                  fecha_modificacion,
                  id_ciclo,
                  score,
                  tipo_envio,FINANCIAMIENTO
             FROM mf_envios_ivr
            WHERE estado = 'A'
              AND idenvio = pn_idenvio);
      EXCEPTION
        WHEN no_data_found THEN
          lv_error := 'ERROR EN CLONAR MF_ENVIOS_IVR ' || 'NO_DATA_FOUND';
          RAISE le_nodataclonar;
        WHEN OTHERS THEN
          lv_error := 'ERROR EN CLONAR MF_ENVIOS_IVR ' || substr(SQLERRM, 1, 255);
          RAISE le_nodataclonar;
      END;
    
      BEGIN
        INSERT INTO mf_detalles_envios_ivr
          (idenvio, idcliente, tipoenvio)
          (SELECT ln_secuencia, idcliente, tipoenvio
             FROM mf_detalles_envios_ivr
            WHERE idenvio = pn_idenvio);
      EXCEPTION
        WHEN no_data_found THEN
          lv_error := 'ERROR EN CLONAR MF_DETALLES_ENVIOS_IVR ' || 'NO_DATA_FOUND';
          RAISE le_nodataclonar;
        WHEN OTHERS THEN
          lv_error := 'ERROR EN CLONAR MF_DETALLES_ENVIOS_IVR ' || substr(SQLERRM, 1, 255);
          RAISE le_nodataclonar;
      END;
    END IF;
  
    BEGIN
      INSERT INTO mf_criterios_x_envios_ivr
        (idenvio, idcriterio, secuencia, valor, efecto, estado)
        (SELECT ln_secuencia,
                idcriterio,
                mfs_criteriosxenvio_sec_ivr.nextval,
                valor,
                efecto,
                'A'
           FROM mf_criterios_x_envios_ivr
          WHERE idenvio = pn_idenvio
            AND estado = 'A');
    EXCEPTION
      WHEN no_data_found THEN
        lv_error := 'ERROR EN CLONAR MF_CRITERIOS_X_ENVIOS_IVR ' || 'NO_DATA_FOUND';
        RAISE le_nodataclonar;
      WHEN OTHERS THEN
        lv_error := 'ERROR EN CLONAR MF_CRITERIOS_X_ENVIOS_IVR ' || substr(SQLERRM, 1, 255);
        RAISE le_nodataclonar;
    END;
  
    BEGIN
      INSERT INTO mf_det_criterios_envios_ivr
        (id_envio, idcriterio, subcategoria, estado)
        (SELECT ln_secuencia, idcriterio, subcategoria, 'A'
           FROM mf_det_criterios_envios_ivr
          WHERE id_envio = pn_idenvio
            AND estado = 'A');
    EXCEPTION
      WHEN no_data_found THEN
        lv_error := 'ERROR EN CLONAR MF_DET_CRITERIOS_ENVIOS_IVR ' || 'NO_DATA_FOUND';
        RAISE le_nodataclonar;
      WHEN OTHERS THEN
        lv_error := 'ERROR EN CLONAR MF_DET_CRITERIOS_ENVIOS_IVR ' || substr(SQLERRM, 1, 255);
        RAISE le_nodataclonar;
    END;
    
    BEGIN
      INSERT INTO mf_time_ejecucion_ivr
        (idenvio,
         diaejecucion,
         horamaxejecucion,
         periodoejecucion,
         minutosmaxejecucion,
         estado,
         numerosmsmaxenvio)
        (SELECT ln_secuencia,
                diaejecucion,
                horamaxejecucion,
                periodoejecucion,
                minutosmaxejecucion,
                estado,
                numerosmsmaxenvio
           FROM mf_time_ejecucion_ivr
          WHERE idenvio = pn_idenvio
            AND estado = 'A');
    EXCEPTION
      WHEN no_data_found THEN
        lv_error := 'ERROR EN CLONAR MF_TIME_EJECUCION_IVR ' || 'NO_DATA_FOUND';
        RAISE le_nodataclonar;
      WHEN OTHERS THEN
        lv_error := 'ERROR EN CLONAR MF_TIME_EJECUCION_IVR ' || substr(SQLERRM, 1, 255);
        RAISE le_nodataclonar;
    END;
  
    BEGIN
      INSERT INTO mf_dias_semana_ivr
        (idenvio, dia)
        (SELECT ln_secuencia, dia
           FROM mf_dias_semana_ivr
          WHERE idenvio = pn_idenvio);
    EXCEPTION
      WHEN no_data_found THEN
        lv_error := 'ERROR EN CLONAR MF_DIAS_SEMANA_IVR ' || 'NO_DATA_FOUND';
        RAISE le_nodataclonar;
      WHEN OTHERS THEN
        lv_error := 'ERROR EN CLONAR MF_DIAS_SEMANA_IVR ' || substr(SQLERRM, 1, 255);
        RAISE le_nodataclonar;
    END;
  
    COMMIT;
    pn_out_idenvio := ln_secuencia;
    
  EXCEPTION
    WHEN le_nodataclonar THEN
      pv_msg_error := ' ERROR:' || lv_error || ' En ' || lv_aplicacion;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || substr(SQLERRM, 1, 250) || '. ' || lv_aplicacion;
  END;

END MFK_OBJ_CRITERIO_X_ENVIOS_IVR;
/
