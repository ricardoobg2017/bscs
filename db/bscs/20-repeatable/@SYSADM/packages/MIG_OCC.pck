CREATE OR REPLACE PACKAGE MIG_OCC AS

PROCEDURE ADJUSTMENTS;

PROCEDURE INSERT_FEES (
  /*1*/pcustomer_id             IN fees.customer_id%TYPE,
  /*2*/pseqno                   IN fees.seqno%TYPE,
  /*3*/pfee_type                IN fees.fee_type%TYPE,
  /*4*/pamount                  IN fees.amount%TYPE,
  /*5*/premark                  IN fees.remark%TYPE,
  /*6*/pglcode                  IN fees.glcode%TYPE,
  /*7*/pentdate                 IN fees.entdate%TYPE,
  /*8*/pperiod                  IN fees.period%TYPE,
  /*9*/pusername                IN fees.username%TYPE,
  /*10*/pvalid_from             IN fees.valid_from%TYPE,
  /*11*/pjobcost                IN fees.jobcost%TYPE,
  /*12*/pservcat_code   		IN fees.servcat_code%TYPE,
  /*13*/pserv_code              IN fees.serv_code%TYPE,
  /*14*/pserv_type              IN fees.serv_type%TYPE,
  /*15*/pcurrency               IN fees.currency%TYPE,
  /*16*/pglcode_disc    		IN fees.glcode_disc%TYPE,
  /*17*/pjobcost_id_disc        IN fees.jobcost_id_disc%TYPE,
  /*18*/pglcode_mincom          IN fees.glcode_mincom%TYPE,
  /*19*/pjobcost_id_mincom      IN fees.jobcost_id_mincom%TYPE,
  /*20*/prec_version            IN fees.rec_version%TYPE,
  /*21*/ptmcode                 IN fees.tmcode%TYPE,
  /*22*/pvscode                 IN fees.vscode%TYPE,
  /*23*/pspcode                 IN fees.spcode%TYPE,
  /*24*/psncode                 IN fees.sncode%TYPE,
  /*25*/pevcode                 IN fees.evcode%TYPE,
  /*26*/pfee_class              IN fees.fee_class%TYPE );

END MIG_OCC;
/
CREATE OR REPLACE PACKAGE BODY MIG_OCC AS

PROCEDURE ADJUSTMENTS IS

 CURSOR cAdjustments IS

 select u.customer_id,
		   ac.postamt, 	 	 -- amount
		       mo.des,		 -- remark
	     tm.accglcode, 		 -- glcode
	     ac.effective, 		 -- valid_from
   tm.accserv_catcode,	     -- servcat_code
 	  tm.accserv_code,		 --	serv_code
	  tm.accserv_type,		 -- serv_type
	tm.accglcode_disc,  	 -- glcode_disc
  tm.accglcode_mincom,	     -- glcode_mincom
			tm.tmcode,		 -- tmcode
			tm.vscode,		 -- vscode
			tm.spcode,		 -- spcode
			tm.sncode,		 -- sncode
			ex.evcode		 -- evcode
 from mpulktmb tm, mpusntab sn, mpulkexn ex, mig_lkocc mo,
      acpost@cbill ac, customer_all u
 where tm.sncode = sn.sncode
 and ex.sncode = sn.sncode
 and mo.sncode = ex.sncode
 and mo.shdes = sn.shdes
 and ac.chgnum = mo.chgnum
 and ac.posttype = mo.posttype
 and u.target_reached = ac.account
 and tm.tmcode = 35
 and tm.vscode = 2
 and u.paymntresp = 'X'
 order by u.customer_id;

  vcom       cAdjustments%ROWTYPE;
  nSec       INTEGER:=0;
  AcctN      INTEGER:=0;
  nAcctnu    INTEGER:=0;
  nCommit	 INTEGER:=100;

BEGIN

   -- Loops thru all adjustments
   OPEN cAdjustments;
   LOOP
     FETCH cAdjustments INTO vcom;
     EXIT WHEN cAdjustments%NOTFOUND;



	   select nvl(max(seqno),0) into nSec
	     from fees
		where customer_id = vcom.customer_id;

--     BEGIN
/*	   IF vcom.customer_id = AcctN THEN
         nSec := nSec + 1;
       ELSIF nSec = 1 THEN
	     nSec := nSec + 1;
	   ELSIF nSec IS NULL THEN
	     nSec := 0;
	   ELSE
         nSec := 1;
       END IF;
       AcctN := vcom.customer_id;   */

      INSERT_FEES ( /*1*/vcom.customer_id,
	                /*2*/nSec+1,
	 	            /*3*/'N',
                    /*4*/vcom.postamt,
					/*5*/vcom.des,
					/*6*/vcom.accglcode,
                    /*7*/sysdate-1,
					/*8*/1,
					/*9*/'SLB',
                    /*10*/sysdate-1,
					/*11*/NULL,
					/*12*/vcom.accserv_catcode,
                    /*13*/vcom.accserv_code,
					/*14*/vcom.accserv_type,
					/*15*/19,
                    /*16*/vcom.accglcode_disc,
					/*17*/NULL,
					/*18*/vcom.accglcode_mincom,
                    /*19*/NULL,
					/*20*/0,
					/*21*/vcom.tmcode,
                    /*22 vscode*/vcom.vscode,
					/*23 spcode*/vcom.spcode,
					/*24 sncode*/vcom.sncode,
                    /*25 evcode*/vcom.evcode,
					/*26 fee_class*/3 );

       IF MOD(cAdjustments%ROWCOUNT,nCommit) = 0 THEN -- COMMIT every nCommit invoices
         COMMIT;
       END IF;


     END LOOP;
     CLOSE cAdjustments; -- Adjustments
	 COMMIT;

END ADJUSTMENTS;


PROCEDURE INSERT_FEES
 (/*1*/pcustomer_id             IN fees.customer_id%TYPE,
  /*2*/pseqno                   IN fees.seqno%TYPE,
  /*3*/pfee_type                IN fees.fee_type%TYPE,
  /*4*/pamount                  IN fees.amount%TYPE,
  /*5*/premark                  IN fees.remark%TYPE,
  /*6*/pglcode                  IN fees.glcode%TYPE,
  /*7*/pentdate                 IN fees.entdate%TYPE,
  /*8*/pperiod                  IN fees.period%TYPE,
  /*9*/pusername                IN fees.username%TYPE,
  /*10*/pvalid_from             IN fees.valid_from%TYPE,
  /*11*/pjobcost                IN fees.jobcost%TYPE,
  /*12*/pservcat_code   		IN fees.servcat_code%TYPE,
  /*13*/pserv_code              IN fees.serv_code%TYPE,
  /*14*/pserv_type              IN fees.serv_type%TYPE,
  /*15*/pcurrency               IN fees.currency%TYPE,
  /*16*/pglcode_disc    		IN fees.glcode_disc%TYPE,
  /*17*/pjobcost_id_disc        IN fees.jobcost_id_disc%TYPE,
  /*18*/pglcode_mincom          IN fees.glcode_mincom%TYPE,
  /*19*/pjobcost_id_mincom      IN fees.jobcost_id_mincom%TYPE,
  /*20*/prec_version            IN fees.rec_version%TYPE,
  /*21*/ptmcode                 IN fees.tmcode%TYPE,
  /*22*/pvscode                 IN fees.vscode%TYPE,
  /*23*/pspcode                 IN fees.spcode%TYPE,
  /*24*/psncode                 IN fees.sncode%TYPE,
  /*25*/pevcode                 IN fees.evcode%TYPE,
  /*26*/pfee_class              IN fees.fee_class%TYPE )    IS

BEGIN

INSERT INTO FEES ( /*1*/customer_id,
                   /*2*/seqno,
				   /*3*/fee_type,
                   /*4*/amount,
				   /*5*/remark,
				   /*6*/glcode,
                   /*7*/entdate,
				   /*8*/period,
				   /*9*/username,
                   /*10*/valid_from,
				   /*11*/jobcost,
				   /*12*/servcat_code,
                   /*13*/serv_code,
				   /*14*/serv_type,
				   /*15*/currency,
                   /*16*/glcode_disc,
				   /*17*/jobcost_id_disc,
				   /*18*/glcode_mincom,
                   /*19*/jobcost_id_mincom,
				   /*20*/rec_version,
				   /*21*/tmcode,
                   /*22*/vscode,
				   /*23*/spcode,
				   /*24*/sncode,
                   /*25*/evcode,
				   /*26*/fee_class )
          VALUES ( /*1*/pcustomer_id,
		           /*2*/pseqno,
				   /*3*/pfee_type,
                   /*4*/pamount,
				   /*5*/premark,
				   /*6*/pglcode,
                   /*7*/pentdate,
				   /*8*/pperiod,
				   /*9*/pusername,
                   /*10*/pvalid_from,
				   /*11*/pjobcost,
				   /*12*/pservcat_code,
                   /*13*/pserv_code,
				   /*14*/pserv_type,
				   /*15*/pcurrency,
                   /*16*/pglcode_disc,
				   /*17*/pjobcost_id_disc,
				   /*18*/pglcode_mincom,
                   /*19*/pjobcost_id_mincom,
				   /*20*/prec_version,
				   /*21*/ptmcode,
                   /*22*/pvscode,
				   /*23*/pspcode,
				   /*24*/psncode,
                   /*25*/pevcode,
				   /*26*/pfee_class );


END INSERT_FEES;

END MIG_OCC;
/

