CREATE OR REPLACE PACKAGE GSI_GENERA_REMARK_MANUAL IS

  PROCEDURE GSI_OBTIENE_NUM_TELEFONO(PV_HILO     NUMBER,
                                     PN_CANTIDAD OUT NUMBER,
                                     PV_ERROR    OUT VARCHAR2);

  PROCEDURE GSI_OBTIENE_CO_ID(PV_HILO     NUMBER,
                              PN_CANTIDAD OUT NUMBER,
                              PV_ERROR    OUT VARCHAR2);

  PROCEDURE GSI_OBTIENE_CUSTOMER_ID(PV_HILO     NUMBER,
                                    PN_CANTIDAD OUT NUMBER,
                                    PV_ERROR    OUT VARCHAR2);

  PROCEDURE GSI_OBTIENE_CUSTCUDE(PV_HILO     NUMBER,
                                 PN_CANTIDAD OUT NUMBER,
                                 PV_ERROR    OUT VARCHAR2);

END GSI_GENERA_REMARK_MANUAL;
/
CREATE OR REPLACE PACKAGE BODY GSI_GENERA_REMARK_MANUAL IS

  --=========================================================================================================--
  -- Creado por:           CLS Yanina Alvarez [CLS YAL]
  -- Fecha de creaci�n:    25/11/2015
  -- Lider SIS:            Jackeline G�mez
  -- LIDER CLS:            Sheyla Ramirez
  -- Proyecto:             [10400] CARGA TXT A LA FEES CON PARAMETROS
  -- Comentario:           VALIDAR Y CORREGIR QUE EL PROCESO QUE REALIZA LA CARGA DE TXT A LA FEES, SE PUEDA
  --                       REALIZAR MEDIANTE LOS SIGUIENTES PARAMETROS:
  --                       POR NUMERO DE TELEFONO
  --                       POR NUMERO DE CUENTA
  --                       POR CO_ID
  --                       POR CUSTOMER_ID
  --=========================================================================================================--

  PROCEDURE GSI_OBTIENE_NUM_TELEFONO(PV_HILO     NUMBER,
                                     PN_CANTIDAD OUT NUMBER,
                                     PV_ERROR    OUT VARCHAR2) IS
  
    CURSOR C_CUENTAS_TMP(LN_HILO NUMBER) IS
      SELECT A.TELEFONO_DNC, A.CODIGO, REPLACE(A.VALOR, ',', '.') VALOR
        FROM GSI_TMP_TIEMPO_AIRE A
       WHERE SUBSTR(A.TELEFONO_DNC, LENGTH(A.TELEFONO_DNC), 1) = LN_HILO;
  
    --DECLARACION DE VARIABLES
    LV_COID       NUMBER := 0;
    LN_CODIGO_DNC VARCHAR2(20);
    LV_TELEFONO   VARCHAR2(20);
    PV_MENSAJE    VARCHAR2(400);
    LN_CONTROL    NUMBER := 0;
    LN_SALIDA     NUMBER := 0;
  
  BEGIN
    FOR C_CUENTAS IN C_CUENTAS_TMP(PV_HILO) LOOP
      LN_SALIDA := LN_SALIDA + 1;
      BEGIN
        SELECT DN_NUM, CO_ID
          INTO LV_TELEFONO, LV_COID
          FROM (SELECT A.DN_NUM, SC.CO_ID, MAX(SC.CS_ACTIV_DATE) INI
                  FROM DIRECTORY_NUMBER A, CONTR_SERVICES_CAP SC
                 WHERE A.DN_ID = SC.DN_ID
                   AND A.DN_NUM = OBTIENE_TELEFONO_DNC_INT(C_CUENTAS.TELEFONO_DNC,
                                                           NULL,
                                                           'N')
                 GROUP BY A.DN_NUM, SC.CO_ID
                 ORDER BY INI DESC)
         WHERE ROWNUM = 1;
      
        INSERT INTO BL_CARGA_OCC_TMP
          (CO_ID,
           AMOUNT,
           SNCODE,
           STATUS,
           ERROR,
           HILO,
           CUSTOMER_ID,
           CUSTCODE)
        VALUES
          (LV_COID,
           C_CUENTAS.VALOR,
           C_CUENTAS.CODIGO,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL);
      
        LN_CODIGO_DNC := TO_NUMBER(C_CUENTAS.CODIGO);
        LV_TELEFONO   := OBTIENE_TELEFONO_DNC_INT(C_CUENTAS.TELEFONO_DNC,
                                                  NULL,
                                                  'N');
      
        LN_CONTROL := LN_CONTROL + 1;
        --PRIMER COMMIT  
        IF LN_CONTROL = 100 THEN
          COMMIT;
          LN_CONTROL := 0;
        END IF;
      
      EXCEPTION
      
        WHEN OTHERS THEN
          --CONTROL DE ERRORES
          PV_ERROR   := 'NO SE CARGARON TODOS LOS REGISTROS DEL HILO' ||
                        ' - ' || PV_HILO || ' - ' || SQLERRM;
          PV_MENSAJE := 'ERROR AL BUSCAR CO_ID EN EL HILO' || ' - ' ||
                        PV_HILO || ' - ' || SQLERRM;
        
          INSERT INTO SVA_FEES_RECHAZA
            (TRANSACTION_ID, ID_SVA, MENSAJE, FECHA_EVENTO, VALOR)
          VALUES
            (NULL,
             C_CUENTAS.CODIGO,
             PV_MENSAJE,
             SYSDATE,
             C_CUENTAS.TELEFONO_DNC);
        
      END;
    
    END LOOP;
    PN_CANTIDAD := LN_SALIDA;
    --SEGUNDO COMMIT
    COMMIT;
  
  EXCEPTION
  
    WHEN OTHERS THEN
    
      PV_ERROR := 'ERROR AL EJECUTAR EL PROCESO' || ' - ' || SQLERRM;
    
  END GSI_OBTIENE_NUM_TELEFONO;

  PROCEDURE GSI_OBTIENE_CO_ID(PV_HILO     NUMBER,
                              PN_CANTIDAD OUT NUMBER,
                              PV_ERROR    OUT VARCHAR2) IS
  
    CURSOR C_CUENTAS_TMP(LN_HILO NUMBER) IS
      SELECT A.TELEFONO_DNC, A.CODIGO, REPLACE(A.VALOR, ',', '.') VALOR
        FROM GSI_TMP_TIEMPO_AIRE A
       WHERE SUBSTR(A.TELEFONO_DNC, LENGTH(A.TELEFONO_DNC), 1) = LN_HILO;
  
    --DECLARACION DE VARIABLES
    LV_COID    NUMBER := 0;
    PV_MENSAJE VARCHAR2(400);
    LN_CONTROL NUMBER := 0;
    LN_SALIDA  NUMBER := 0;
  
  BEGIN
    FOR C_CUENTAS IN C_CUENTAS_TMP(PV_HILO) LOOP
      LN_SALIDA := LN_SALIDA + 1;
      BEGIN
      
        SELECT CO_ID
          INTO LV_COID
          FROM (SELECT SC.CO_ID, MAX(SC.CS_ACTIV_DATE) INI
                  FROM CONTR_SERVICES_CAP SC, CONTRACT_ALL CO
                 WHERE SC.CO_ID = CO.CO_ID
                   AND SC.CO_ID = C_CUENTAS.TELEFONO_DNC
                 GROUP BY SC.CO_ID)
         WHERE ROWNUM = 1
         ORDER BY INI DESC;
      
        INSERT INTO BL_CARGA_OCC_TMP
          (CO_ID,
           AMOUNT,
           SNCODE,
           STATUS,
           ERROR,
           HILO,
           CUSTOMER_ID,
           CUSTCODE)
        VALUES
          (LV_COID,
           C_CUENTAS.VALOR,
           C_CUENTAS.CODIGO,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL);
      
        LN_CONTROL := LN_CONTROL + 1;
        --PRIMER COMMIT  
        IF LN_CONTROL = 100 THEN
          COMMIT;
          LN_CONTROL := 0;
        END IF;
      
      EXCEPTION
      
        WHEN OTHERS THEN
          --CONTROL DE ERRORES
          PV_ERROR   := 'NO SE CARGARON TODOS LOS REGISTROS DEL HILO' ||
                        ' - ' || PV_HILO || ' - ' || SQLERRM;
          PV_MENSAJE := 'ERROR AL BUSCAR CO_ID EN EL HILO' || ' - ' ||
                        PV_HILO || ' - ' || SQLERRM;
        
          INSERT INTO SVA_FEES_RECHAZA
            (TRANSACTION_ID, ID_SVA, MENSAJE, FECHA_EVENTO, VALOR)
          VALUES
            (NULL,
             C_CUENTAS.CODIGO,
             PV_MENSAJE,
             SYSDATE,
             C_CUENTAS.TELEFONO_DNC);
        
      END;
    
    END LOOP;
    PN_CANTIDAD := LN_SALIDA;
    --SEGUNDO COMMIT
    COMMIT;
  
  EXCEPTION
  
    WHEN OTHERS THEN
    
      PV_ERROR := 'ERROR AL EJECUTAR EL PROCESO' || ' - ' || SQLERRM;
    
  END GSI_OBTIENE_CO_ID;

  PROCEDURE GSI_OBTIENE_CUSTOMER_ID(PV_HILO     NUMBER,
                                    PN_CANTIDAD OUT NUMBER,
                                    PV_ERROR    OUT VARCHAR2) IS
  
    CURSOR C_CUENTAS_TMP(LN_HILO NUMBER) IS
      SELECT A.TELEFONO_DNC, A.CODIGO, REPLACE(A.VALOR, ',', '.') VALOR
        FROM GSI_TMP_TIEMPO_AIRE A
       WHERE SUBSTR(A.TELEFONO_DNC, LENGTH(A.TELEFONO_DNC), 1) = LN_HILO;
  
    --DECLARACION DE VARIABLES
    PV_MENSAJE     VARCHAR2(400);
    LN_CONTROL     NUMBER := 0;
    LN_SALIDA      NUMBER := 0;
    LV_CUSTOMER_ID VARCHAR2(50);
  
  BEGIN
    FOR C_CUENTAS IN C_CUENTAS_TMP(PV_HILO) LOOP
      LN_SALIDA := LN_SALIDA + 1;
      BEGIN
        SELECT CUSTOMER_ID
          INTO LV_CUSTOMER_ID
          FROM (SELECT CA.CUSTOMER_ID, MAX(CA.CSACTIVATED) INI
                  FROM CUSTOMER_ALL CA, CONTRACT_ALL CO
                 WHERE CO.CUSTOMER_ID = CA.CUSTOMER_ID
                   AND CA.CUSTOMER_ID = C_CUENTAS.TELEFONO_DNC
                 GROUP BY CA.CUSTOMER_ID)
         WHERE ROWNUM = 1
         ORDER BY INI DESC;
      
        INSERT INTO BL_CARGA_OCC_TMP
          (CO_ID,
           AMOUNT,
           SNCODE,
           STATUS,
           ERROR,
           HILO,
           CUSTOMER_ID,
           CUSTCODE)
        VALUES
          (NULL,
           C_CUENTAS.VALOR,
           C_CUENTAS.CODIGO,
           NULL,
           NULL,
           NULL,
           LV_CUSTOMER_ID,
           NULL);
      
        LN_CONTROL := LN_CONTROL + 1;
        --PRIMER COMMIT  
        IF LN_CONTROL = 100 THEN
          COMMIT;
          LN_CONTROL := 0;
        END IF;
      
      EXCEPTION
      
        WHEN OTHERS THEN
          --CONTROL DE ERRORES
          PV_ERROR   := 'NO SE CARGARON TODOS LOS REGISTROS DEL HILO' ||
                        ' - ' || PV_HILO || ' - ' || SQLERRM;
          PV_MENSAJE := 'ERROR AL BUSCAR CO_ID EN EL HILO' || ' - ' ||
                        PV_HILO || ' - ' || SQLERRM;
        
          INSERT INTO SVA_FEES_RECHAZA
            (TRANSACTION_ID, ID_SVA, MENSAJE, FECHA_EVENTO, VALOR)
          VALUES
            (NULL,
             C_CUENTAS.CODIGO,
             PV_MENSAJE,
             SYSDATE,
             C_CUENTAS.TELEFONO_DNC);
        
      END;
    
    END LOOP;
    PN_CANTIDAD := LN_SALIDA;
    --SEGUNDO COMMIT
    COMMIT;
  
  EXCEPTION
  
    WHEN OTHERS THEN
    
      PV_ERROR := 'ERROR AL EJECUTAR EL PROCESO' || ' - ' || SQLERRM;
    
  END GSI_OBTIENE_CUSTOMER_ID;

  PROCEDURE GSI_OBTIENE_CUSTCUDE(PV_HILO     NUMBER,
                                 PN_CANTIDAD OUT NUMBER,
                                 PV_ERROR    OUT VARCHAR2) IS
  
    CURSOR C_CUENTAS_TMP(LN_HILO NUMBER) IS
      SELECT A.TELEFONO_DNC, A.CODIGO, REPLACE(A.VALOR, ',', '.') VALOR
        FROM GSI_TMP_TIEMPO_AIRE A
       WHERE SUBSTR(A.TELEFONO_DNC, LENGTH(A.TELEFONO_DNC), 1) = LN_HILO;
  
    --DECLARACION DE VARIABLES
    PV_MENSAJE  VARCHAR2(400);
    LN_CONTROL  NUMBER := 0;
    LN_SALIDA   NUMBER := 0;
    LV_CUSTCODE VARCHAR2(100);
  
  BEGIN
    FOR C_CUENTAS IN C_CUENTAS_TMP(PV_HILO) LOOP
      LN_SALIDA := LN_SALIDA + 1;
      BEGIN
        SELECT CUSTCODE
          INTO LV_CUSTCODE
          FROM (SELECT CU.CUSTCODE, MAX(CA.CSACTIVATED) INI
                  FROM CUSTOMER_ALL CA, CUSTOMER CU
                 WHERE CA.CUSTCODE = CU.CUSTCODE
                   AND CU.CUSTCODE = C_CUENTAS.TELEFONO_DNC
                 GROUP BY CU.CUSTCODE)
         WHERE ROWNUM = 1
         ORDER BY INI DESC;
      
        INSERT INTO BL_CARGA_OCC_TMP
          (CO_ID,
           AMOUNT,
           SNCODE,
           STATUS,
           ERROR,
           HILO,
           CUSTOMER_ID,
           CUSTCODE)
        VALUES
          (NULL,
           C_CUENTAS.VALOR,
           C_CUENTAS.CODIGO,
           NULL,
           NULL,
           NULL,
           NULL,
           LV_CUSTCODE);
      
        LN_CONTROL := LN_CONTROL + 1;
        --PRIMER COMMIT  
        IF LN_CONTROL = 100 THEN
          COMMIT;
          LN_CONTROL := 0;
        END IF;
      
      EXCEPTION
      
        WHEN OTHERS THEN
          --CONTROL DE ERRORES
          PV_ERROR   := 'NO SE CARGARON TODOS LOS REGISTROS DEL HILO' ||
                        ' - ' || PV_HILO || ' - ' || SQLERRM;
          PV_MENSAJE := 'ERROR AL BUSCAR CO_ID EN EL HILO' || ' - ' ||
                        PV_HILO || ' - ' || SQLERRM;
        
          INSERT INTO SVA_FEES_RECHAZA
            (TRANSACTION_ID, ID_SVA, MENSAJE, FECHA_EVENTO, VALOR)
          VALUES
            (NULL,
             C_CUENTAS.CODIGO,
             PV_MENSAJE,
             SYSDATE,
             C_CUENTAS.TELEFONO_DNC);
        
      END;
    
    END LOOP;
    PN_CANTIDAD := LN_SALIDA;
    --SEGUNDO COMMIT
    COMMIT;
  
  EXCEPTION
  
    WHEN OTHERS THEN
    
      PV_ERROR := 'ERROR AL EJECUTAR EL PROCESO' || ' - ' || SQLERRM;
    
  END GSI_OBTIENE_CUSTCUDE;

END GSI_GENERA_REMARK_MANUAL;
/
