CREATE OR REPLACE PACKAGE cok_reporte_datacredito IS

   ---==================================COK_REPORTE_DATACREDITO_V2=========================================--   
   -- Versi�n:             1.0.0   
   -- Descripci�n:     Paquete BSCS   
   --            ..................................................   
   --=====================================================================================--   
   -- Desarrollado por:     Jhonny Morej�n   
   -- Fecha de creaci�n:    22/Marzo/2006   
   -- Proyecto:                  [1556]Alcance Buro de cr�dito   
   -- Autorizado por:       Juan Carlos Morales Carrillo   
   --=====================================================================================--   
   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.1   
   -- Fecha:                5/04/2006   
   -- Descripci�n: correcci�n de errores y mejoramiento de queries   
   --=====================================================================================--   

   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.2   
   -- Fecha:                7/04/2006   
   -- Descripci�n: Se crea una nueva tabla para llenarla con la informaci�n del reporte   
   --                tal y como datacr�dito la va a recibir.  Con esto se evita realizar   
   --                el union de los queries principales y hacer la consulta m�s r�pida   
   --                Se optimizan los queries utilizando hints   
   --=====================================================================================--   
   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.3   
   -- Fecha:                11/04/2006   
   -- Descripci�n: Se crea una nueva tabla para bitacorizar errores a nivel de registros   
   --                que no se pudieron insertar en co_union_clientes   
   --=====================================================================================--   
   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.4   
   -- Fecha:                5/05/2006   
   -- Descripci�n: Se cambia el nombre del archivo generado, con el objetivo que todos los   
   --                meses se sobreescriban, evitando as� que queden acumulados en dicha ruta   
   --              archivos de meses pasados.  Esto debido a lo cr�tico que resulta el tema   
   --              de almacenamiento en el servidor de bscs   
   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.5   
   -- Fecha:               19/07/2006,   
   -- Descripci�n: No insertamos los registros en la tabla si no tenemos identificaci�n,   
   --              ya que luego se generan inconsistencias en los sistemas de datacr�dito.   
   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.6   
   -- Fecha:               04/08/2006,   
   -- Descripci�n: Se aumenta un trim para evitar espacios en la identificaci�n   
   --=====================================================================================--   
   -- Actualizado por:    (SUD)    Jose Palma   
   -- Versi�n:                1.0.7   
   -- Fecha:         21/07/2008,   
   -- Proyecto:    [3509] Reporte Datacredito   
   -- Descripci�n: Se redefine los procedimientos, para mejorar el tiempo de ejecucion:   
   -- COP_INGRESA_VALORES_DATACRE   
   -- COP_CALIFICA_CLIENTES_GYE   
   -- COP_CALIFICA_CLIENTES_UIO   
   -- Se a�ade los procedimientos:   
   -- COF_CALIFICA_CUENTA   
   -- COF_RECLAMO   
   -- COP_EXISTE_TABLA   
   -- COP_HABILITA_INDICE   

   /*******************************************************************************/
   /*
   /*******************************************************************************/
   TYPE cur_typ IS REF CURSOR;

   PROCEDURE cop_califica_clientes_bscs(pdfechcierreperiodo DATE,
                                        pv_error            OUT VARCHAR2);

   PROCEDURE cop_ingresa_valores_datacre(pv_nombre_tabla   IN VARCHAR2,
                                         pv_cuenta         IN VARCHAR2,
                                         pv_novedad        IN VARCHAR2,
                                         pv_adjetivo       IN VARCHAR2,
                                         pv_calificacion   IN VARCHAR2,
                                         pv_cuentas_mora   IN VARCHAR2,
                                         pv_edad_mora      IN VARCHAR2,
                                         pv_reclamo        IN VARCHAR2,
                                         pv_tipo_id        IN VARCHAR2,
                                         pv_tipo_persona   IN VARCHAR2,
                                         pv_identificacion VARCHAR2,
                                         pn_codigo_error   OUT NUMBER,
                                         pv_error          OUT VARCHAR2
                                         
                                         );

   PROCEDURE cop_crea_tabla(pv_nombre_tabla IN VARCHAR2,
                            pv_esquema      IN VARCHAR2,
                            pn_codigo_error OUT NUMBER,
                            pv_error        OUT VARCHAR2);

   PROCEDURE cop_bitacora_ejecucion(pv_nombretabla    IN VARCHAR2 DEFAULT NULL,
                                    pn_reg_exito      IN NUMBER DEFAULT NULL,
                                    pn_reg_error      IN NUMBER DEFAULT NULL,
                                    pv_descripcion    IN VARCHAR2 DEFAULT NULL,
                                    pv_estado_reporte IN VARCHAR2 DEFAULT NULL,
                                    pv_tipo_proceso   IN VARCHAR2 DEFAULT NULL,
                                    pv_consulta       IN VARCHAR2 DEFAULT NULL,
                                    pv_error          OUT VARCHAR2,
                                    pv_tablaorigen    IN VARCHAR2 DEFAULT NULL);

   FUNCTION cof_valida_identificacon(pv_identificacion VARCHAR2,
                                     pv_error          OUT VARCHAR2)
      RETURN VARCHAR2;

   PROCEDURE cop_califica_clientes_gye;

   PROCEDURE cop_califica_clientes_uio;

   PROCEDURE cop_genera_archivo;

   PROCEDURE cop_actualiza_estados_bitacora(pn_codigo IN NUMBER,
                                            pv_estado IN VARCHAR2,
                                            pv_error  OUT VARCHAR2);

   PROCEDURE cop_union_clientes(pv_error OUT VARCHAR2);

   PROCEDURE cop_crear_archivo(pv_error OUT VARCHAR2);

   PROCEDURE cop_detalle_bitacora(pv_cuenta      VARCHAR2,
                                  pv_descripcion VARCHAR2);

   --INICIO    PROYECTO[3509] REPORTE DATACREDITO 18/07/2008    SE AUMETARON ESTAS 2 FUNCIONES Y TRES PROCEDIMIENTOS   

   FUNCTION cof_califica_cuenta(pv_campo VARCHAR2) RETURN VARCHAR2;

   FUNCTION cof_reclamo(pv_cuenta VARCHAR2) RETURN VARCHAR2;

   PROCEDURE cop_existe_tabla(pv_tabla IN VARCHAR2,
                              pv_error OUT VARCHAR2);

   PROCEDURE cop_habilita_indice(pv_nombre_tabla IN VARCHAR2,
                                 pv_error        OUT VARCHAR2);

   PROCEDURE cop_deshabilita_indice(pv_nombre_tabla IN VARCHAR2,
                                    pv_error        OUT VARCHAR2);

   PROCEDURE cop_genera_datos_buro(pv_fecha_corte VARCHAR2 DEFAULT NULL);

--FIN      PROYECTO[3509] REPORTE DATACREDITO  18/07/2008   
END cok_reporte_datacredito;
/
CREATE OR REPLACE PACKAGE BODY cok_reporte_datacredito IS
   ---MODIFICAR   

   ---==================================COK_REPORTE_DATACREDITO=========================================--   
   -- Versi�n:             1.0.0   
   -- Descripci�n:     Paquete BSCS   
   --            ..................................................   
   --=====================================================================================--   
   -- Desarrollado por:     Jhonny Morej�n   
   -- Fecha de creaci�n:    22/Marzo/2006   
   -- Proyecto:                  [1556]Alcance Buro de cr�dito   
   -- Autorizado por:       Juan Carlos Morales Carrillo   
   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.1   
   -- Fecha:                5/04/2006   
   -- Descripci�n: correcci�n de errores y mejoramiento de queries   
   --=====================================================================================--   

   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.2   
   -- Fecha:                7/04/2006   
   -- Descripci�n: Se crea una nueva tabla para llenarla con la informaci�n del reporte   
   --                tal y como datacr�dito la va a recibir.  Con esto se evita realizar   
   --                el union de los queries principales y hacer la consulta m�s r�pida   
   --                Se optimizan los queries utilizando hints   
   --=====================================================================================--   

   --=====================================================================================--   
   -- Actualizado por:        Jhonny Morej�n   
   -- Versi�n:                1.0.3   
   -- Fecha:                11/04/2006   
   -- Descripci�n: Se crea una nueva tabla para bitacorizar errores a nivel de registros   
   --                que no se pudieron INSERTar en co_union_clientes   
   --=====================================================================================--   

   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.4   
   -- Fecha:                5/05/2006   
   -- Descripci�n: Se cambia el nombre del archivo generado, con el objetivo que todos los   
   --                meses se sobreescriban, evitando as� que queden acumulados en dicha ruta   
   --              archivos de meses pasados.  Esto debido a lo cr�tico que resulta el tema   
   --              de almacenamiento en el servidor de bscs   
   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.5   
   -- Fecha:               19/07/2006,   
   -- Descripci�n: No INSERTamos los registros en la tabla si no tenemos identificaci�n,   
   --              ya que luego se generan inconsistencias en los sistemas de datacr�dito.   
   --=====================================================================================--   
   -- Actualizado por:        Juan Carlos Morales   
   -- Versi�n:                1.0.6   
   -- Fecha:               04/08/2006,   
   -- Descripci�n: Se aumenta un trim para evitar espacios en la identificaci�n   
   --=====================================================================================--   
   -- Actualizado por:    (SUD)    Jose Palma   
   -- Versi�n:                1.0.7   
   -- Fecha:         21/07/2008,   
   -- Proyecto:    [3509] Reporte Datacredito   
   -- Descripci�n: Se redefine los procedimientos, para mejorar el tiempo de ejecucion:   
   -- COP_INGRESA_VALORES_DATACRE   
   -- COP_CALIFICA_CLIENTES_GYE   
   -- COP_CALIFICA_CLIENTES_UIO   
   -- Se a�ade los procedimientos:   
   -- COF_CALIFICA_CUENTA   
   -- COF_RECLAMO   
   -- COP_EXISTE_TABLA   
   -- COP_HABILITA_INDICE   

   gn_commit NUMBER;
   gv_parametro CONSTANT co_parametro_repdatacre.parametro%TYPE := 'TRAN_COMMIT';
   gn_idfeature CONSTANT NUMBER := 1001;
   gn_valor     CONSTANT NUMBER := 41;
   gv_estado    CONSTANT VARCHAR2(1) := 'A';
   gv_ohstatus  CONSTANT VARCHAR2(2) := 'IN';
   gv_estado_n  CONSTANT VARCHAR2(1) := 'N';
   gv_estado_s  CONSTANT VARCHAR2(1) := 'S';

   --Comprueba si existe la tabla   
   CURSOR cg_existe_tabla(cv_nombre_tabla VARCHAR2) IS
      SELECT 'X' existe
      FROM   all_tables t
      WHERE  t.table_name = cv_nombre_tabla;

   gr_existe_tabla cg_existe_tabla%ROWTYPE;

   CURSOR gc_parametros(cv_parametro co_parametro_repdatacre.parametro%TYPE) IS
      SELECT nvl(valor, 500)
      FROM   co_parametro_repdatacre
      WHERE  parametro = cv_parametro;

   CURSOR gc_numero_moras(cv_cuenta   VARCHAR2,
                          cv_ohstatus VARCHAR2) IS
      SELECT COUNT(u.customer_id) nu_moras, cssocialsecno AS identificacion
      FROM   customer_all u, orderhdr_all o
      WHERE  u.customer_id = o.customer_id
             AND u.custcode = cv_cuenta
             AND ohstatus = cv_ohstatus --'IN'   
             AND ohopnamt_doc > 0
      GROUP  BY cssocialsecno;

   --Obtengo la identificacion de cuentas reclasificadas   
   CURSOR gc_numero_moras_reclasificada(cv_cuenta VARCHAR2) IS
      SELECT a.cuen_edad_mora nu_moras, b.clie_cedu identificacion
      FROM   cart_cuentas_dat@fingye_burocred a, cart_clientes_dat@fingye_burocred b
      WHERE  a.cuen_nume_cuen = cv_cuenta
             AND a.cuen_nume_cuen = b.clie_nume_cuen;

   --[5037] INI GBO 03-06-2010

   CURSOR c_valida_cta_alta(cv_codigo_doc VARCHAR2) IS
      SELECT bd.alta
      FROM   bs_cuentas_diferidas bd
      WHERE  bd.codigo_doc = cv_codigo_doc
             AND bd.estado = 'A';

   lv_band_5037       gv_parametros.valor%TYPE;
   lc_valida_cta_alta c_valida_cta_alta%ROWTYPE;

   --[5037] FIN GBO 03-06-2010  
   
   --10393 DIAS MORA NEGOCIACION  RTH 12/02/2016         
      CURSOR cl_parametros (c_id_tipo_parametro NUMBER, c_id_parametro VARCHAR2) IS
        select w.valor 
          from gv_parametros w 
         where w.id_tipo_parametro = c_id_tipo_parametro
           and w.id_parametro = c_id_parametro;
   
   --10393 DIAS MORA NEGOCIACION  RTH 12/02/2016

   PROCEDURE cop_califica_clientes_bscs(pdfechcierreperiodo DATE,
                                        pv_error            OUT VARCHAR2) IS
   
      lv_fecha        VARCHAR2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
      ln_codigo_error NUMBER;
      lv_nombre_tabla VARCHAR2(60) := 'CO_REPDATCRE_';
      lv_tabla_origen VARCHAR2(60) := 'CO_REPCARCLI_' || lv_fecha;
      lv_consulta     VARCHAR2(1000);
      lv_adjetivo     VARCHAR2(2);
      lv_reclamo      VARCHAR2(1);
      ln_reg_error    NUMBER := 0;
      ln_reg_exito    NUMBER := 0;
      ln_ruc          NUMBER := 0;
   
      -- JPA  SUD PROYECTO[3509]   18/07/2008   
      lv_tipo_iden     VARCHAR2(100);
      lv_tipo_persona  VARCHAR2(100);
      lv_cuenta        VARCHAR2(25);
      lv_mayor_vencido VARCHAR2(15);
      ln_nu_moras      NUMBER;
      lv_ruc           VARCHAR2(20);
      ln_saldodeuda    NUMBER;
      lv_novedad       VARCHAR2(5);
      lv_calificacion  co_calificacion_repdatacre.calificacion%TYPE;
   
      lt_cuenta        cot_string := cot_string();
      lt_mayor_vencido cot_string := cot_string();
      lt_nu_moras      cot_number := cot_number();
      lt_ruc           cot_string := cot_string();
      lt_saldodeuda    cot_number := cot_number();
      lt_novedad       cot_string := cot_string();
      lt_calificacion  cot_string := cot_string();
      lt_tipo_iden     cot_string := cot_string();
      lt_tipo_persona  cot_string := cot_string();
      -- JPA  SUD PROYECTO[3509]   18/07/2008   
      --Variables del CURSOR   
   
      le_error EXCEPTION;
      lv_programa VARCHAR2(100) := 'COP_CALIFICA_CLIENTES_BSCS ';
   
   
   
      -- INI CLS RQUI - 9335 MIGRACION DE BSCS 
      TYPE r_detalle_ws IS RECORD(
         cuenta              VARCHAR2(24),
         nu_moras            NUMBER,
         ln_saldodeuda       NUMBER,
         mayorvencido        VARCHAR2(60),
         novedad             VARCHAR2(60),
         evaluacion          VARCHAR2(60),
         ruc                 VARCHAR2(60),
         tipo_identificacion VARCHAR2(60),
         tipo_persona        VARCHAR2(60));
   
   
      TYPE t_rowid IS TABLE OF r_detalle_ws INDEX BY BINARY_INTEGER;
      lt_row_id t_rowid;
   
      TYPE c_cursor IS REF CURSOR;
      c_det_registros_rowid c_cursor;
   
      ln_max        NUMBER := 1000;
      ln_con_row    NUMBER := 0;
      ln_reg_commit NUMBER := 1000;
   
      -- FIN CLS RQUI - 9335 MIGRACION DE BSCS 
   
   
   BEGIN
   
      -- Creo la tabla   
      lv_nombre_tabla := lv_nombre_tabla || lv_fecha;
      pv_error := NULL;
   
      -- El procedimiento pregunta si existe la tabla destino, Si la encuentra borrra los datos o sino la crea.   
      -- Tambien deshabilita el indicie para bajar el tiempo del proceso   
      cop_existe_tabla(lv_nombre_tabla, pv_error);
   
      IF pv_error IS NOT NULL THEN
         RAISE le_error;
      END IF;
   
   
   
      /* 
      --- COMENTADO POR LA MIGRACION DE BSCS 28052014 MEJORAS 
       --Leo los datos del cliente usando BULK COLLECT              JPA  SUD PROYECTO[3509]   18/07/2008   
      
        lv_consulta := 'begin select cuenta,' ||
                       '(decode (balance_1,0,0,1) +  decode (balance_2,0,0,1) + decode (balance_3,0,0,1) + decode (balance_4,0,0,1) + ' ||
                       'decode (balance_5,0,0,1) +  decode (balance_6,0,0,1) + decode (balance_7,0,0,1) + decode (balance_8,0,0,1) + ' ||
                       'decode (balance_9,0,0,1) +  decode (balance_10,0,0,1) + decode (balance_11,0,0,1)) nu_moras, ' ||
                       'TOTALVENCIDA ln_saldoDeuda,' ||
                       'DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ) mayorvencido, ' ||
                       'DECODE (DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ), ''01'', ''01'', ''30'', ''06'',''60'',''07'',''90'', ''08'',''09'' ) NOVEDAD,' ||
                       'COK_REPORTE_DATACREDITO.COF_CALIFICA_CUENTA( DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' ,''01'', mayorvencido )) evaluacion,' ||
                       'REPLACE(TRIM(ruc), ''-'', NULL) RUC,' ||
                       'DECODE(length(ruc),10,''CED'',13,''RUC'',''PAS'')TIPO_IDENTIFICACION,' ||
                       'DECODE(length(ruc),10,''N'',13,''J'',''N'') TIPO_PERSONA ' ||
                       'bulk collect into :1, :2, :3, :4, :5, :6, :7, :8, :9  ' ||
                       'from ' || lv_tabla_origen || ' A;' || 'end;';
      
        EXECUTE IMMEDIATE lv_consulta
          using out lt_cuenta, out lt_nu_moras, out lt_saldoDeuda, out lt_mayor_vencido, out lt_novedad, out lt_calificacion, out lt_ruc, out lt_tipo_iden, out lt_tipo_persona;
      
        FOR a IN lt_cuenta.first .. lt_cuenta.last LOOP
        
          lv_cuenta        := lt_cuenta(a);
          ln_nu_moras      := lt_nu_moras(a);
          ln_saldoDeuda    := lt_saldoDeuda(a);
          lv_mayor_vencido := lt_mayor_vencido(a);
          lv_novedad       := lt_novedad(a);
          lv_calificacion  := lt_calificacion(a);
          lv_ruc           := lt_ruc(a);
          lv_tipo_iden     := lt_tipo_iden(a);
          lv_tipo_persona  := lt_tipo_persona(a);
        
          ---Valido tipo persona   
          pv_error := null;
          begin
            ln_ruc := TO_NUMBER(lv_ruc);
          exception
            when INVALID_NUMBER then
              lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                                 pv_error          => pv_error);
            when others then
              lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                                 pv_error          => pv_error);
          end;
        
          --Encuentro la autorizacion   
          lv_reclamo := COF_RECLAMO(lv_cuenta);
        
          --Si el saldo de la deuda es menor que 0.5 entonces se pone al d�a   
          IF ln_saldoDeuda < 0.5 THEN
            lv_mayor_vencido := '01';
            lv_novedad       := '01'; --AL DIA   
            lv_adjetivo      := '00';
            ln_nu_moras      := 0;
            lv_calIFicacion  := 'A';
          END IF;
        
          --Valido el adjetivo  en base a la novedad   
          IF lv_novedad = '08' or lv_novedad = '09' THEN
            lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90   
          ELSE
            IF lv_novedad = '06' or lv_novedad = '07' THEN
              lv_adjetivo := '11'; --Linea desconectada   
            ELSE
              lv_adjetivo := '00';
            END IF;
          END IF;
        
          --Ingreso la calIFicaci�n de cada cliente   
          pv_error        := NULL;
          ln_codigo_error := NULL;
        
          cop_ingresa_valores_datacre(pv_nombre_tabla   => lv_nombre_tabla,
                                      pv_cuenta         => lv_cuenta,
                                      pv_novedad        => lv_novedad,
                                      pv_adjetivo       => lv_adjetivo,
                                      pv_calIFicacion   => lv_calIFicacion,
                                      pv_cuentas_mora   => to_char(ln_nu_moras),
                                      pv_edad_mora      => lv_mayor_vencido,
                                      pv_reclamo        => lv_reclamo,
                                      pv_tipo_id        => lv_tipo_iden,
                                      pv_tipo_persona   => lv_tipo_persona,
                                      pv_identIFicacion => lv_ruc,
                                      pn_codigo_error   => ln_codigo_error,
                                      pv_error          => pv_error);
        
          IF ln_codigo_error = 0 THEN
            ln_reg_error := ln_reg_error + 1;
          ELSE
            ln_reg_exito := ln_reg_exito + 1;
          END IF;
        
          IF mod(ln_reg_exito, gn_commit) = 0 THEN
            commit;
          END IF;
        
        END LOOP;
      
        commit;*/
   
   
      -- INI CLS RQUI - 9335 - MIGRACION DE BSCS 11g
      lv_consulta := ' select cuenta,' ||
                     '(decode (balance_1,0,0,1) +  decode (balance_2,0,0,1) + decode (balance_3,0,0,1) + decode (balance_4,0,0,1) + ' ||
                     'decode (balance_5,0,0,1) +  decode (balance_6,0,0,1) + decode (balance_7,0,0,1) + decode (balance_8,0,0,1) + ' ||
                     'decode (balance_9,0,0,1) +  decode (balance_10,0,0,1) + decode (balance_11,0,0,1)) nu_moras, ' ||
                     'TOTALVENCIDA ln_saldoDeuda,' ||
                     'DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ) mayorvencido, ' ||
                     'DECODE (DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ), ''01'', ''01'', ''30'', ''06'',''60'',''07'',''90'', ''08'',''09'' ) NOVEDAD,' ||
                     'COK_REPORTE_DATACREDITO.COF_CALIFICA_CUENTA( DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' ,''01'', mayorvencido )) evaluacion,' ||
                     'REPLACE(TRIM(ruc), ''-'', NULL) RUC,' ||
                     'DECODE(length(ruc),10,''CED'',13,''RUC'',''PAS'')TIPO_IDENTIFICACION,' ||
                     'DECODE(length(ruc),10,''N'',13,''J'',''N'') TIPO_PERSONA ' ||
                    --'bulk collect into :1, :2, :3, :4, :5, :6, :7, :8, :9  ' ||
                     ' from ' || lv_tabla_origen || ' A' ||
                    --' WHERE CUENTA = ''1.10150678''';
                     ' where cuenta is not null ';
   
      OPEN c_det_registros_rowid FOR lv_consulta;
      LOOP
         lt_row_id.delete;
         FETCH c_det_registros_rowid BULK COLLECT
            INTO lt_row_id LIMIT ln_max;
      
         IF lt_row_id.count > 0 THEN
            FOR i IN lt_row_id.first .. lt_row_id.last
            LOOP
            
               lv_cuenta := lt_row_id(i).cuenta;
               ln_nu_moras := lt_row_id(i).nu_moras;
               ln_saldodeuda := lt_row_id(i).ln_saldodeuda;
               lv_mayor_vencido := lt_row_id(i).mayorvencido;
               lv_novedad := lt_row_id(i).novedad;
               lv_calificacion := lt_row_id(i).evaluacion;
               lv_ruc := lt_row_id(i).ruc;
               lv_tipo_iden := lt_row_id(i).tipo_identificacion;
               lv_tipo_persona := lt_row_id(i).tipo_persona;
            
               ---Valido tipo persona   
               pv_error := NULL;
               BEGIN
                  ln_ruc := to_number(lv_ruc);
               EXCEPTION
                  WHEN invalid_number THEN
                     lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => pv_error);
                  WHEN OTHERS THEN
                     lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => pv_error);
               END;
            
            
               --Encuentro la autorizacion   
               lv_reclamo := cof_reclamo(lv_cuenta);
            
               --Si el saldo de la deuda es menor que 0.5 entonces se pone al d�a   
               IF ln_saldodeuda < 0.5 THEN
                  lv_mayor_vencido := '01';
                  lv_novedad := '01'; --AL DIA   
                  lv_adjetivo := '00';
                  ln_nu_moras := 0;
                  lv_calificacion := 'A';
               END IF;
            
               --Valido el adjetivo  en base a la novedad   
               IF lv_novedad = '08' OR lv_novedad = '09' THEN
                  lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90   
               ELSE
                  IF lv_novedad = '06' OR lv_novedad = '07' THEN
                     lv_adjetivo := '11'; --Linea desconectada   
                  ELSE
                     lv_adjetivo := '00';
                  END IF;
               END IF;
            
               --Ingreso la calIFicaci�n de cada cliente   
               pv_error := NULL;
               ln_codigo_error := NULL;
            
            
               cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => to_char(ln_nu_moras), pv_edad_mora => lv_mayor_vencido, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => pv_error);
            
               IF ln_codigo_error = 0 THEN
                  ln_reg_error := ln_reg_error + 1;
               ELSE
                  ln_reg_exito := ln_reg_exito + 1;
               END IF;
            
               IF MOD(ln_reg_exito, gn_commit) = 0 THEN
                  COMMIT;
               END IF;
            
               ln_con_row := ln_con_row + 1;
               IF ln_con_row >= ln_reg_commit THEN
                  COMMIT;
                  ln_con_row := 0;
               END IF;
            
            END LOOP;
         END IF;
         EXIT WHEN c_det_registros_rowid%NOTFOUND;
      END LOOP;
      CLOSE c_det_registros_rowid;
      COMMIT;
      lt_row_id.delete;
      -- FIN  CLS RQUI - 9335 MIGRACION DE BSCS           
   
      pv_error := NULL;
   
      -- Habilito constraints   
      cop_habilita_indice(lv_nombre_tabla, pv_error);
   
      IF pv_error IS NOT NULL THEN
         RAISE le_error;
      
      END IF;
      ---   
   
      cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => pv_error, pv_estado_reporte => 'N', pv_tipo_proceso => 'P', pv_consulta => NULL, pv_error => pv_error, pv_tablaorigen => lv_tabla_origen);
   
      -------------------------------------------------------------------   
   EXCEPTION
   
      WHEN le_error THEN
         pv_error := lv_programa || ' - ' || pv_error;
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => pv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => pv_error, pv_tablaorigen => lv_tabla_origen);
      WHEN OTHERS THEN
         pv_error := lv_programa || ' - ' || SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => pv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => pv_error, pv_tablaorigen => lv_tabla_origen);
      
   END cop_califica_clientes_bscs;

   PROCEDURE cop_crea_tabla(pv_nombre_tabla IN VARCHAR2,
                            pv_esquema      IN VARCHAR2,
                            pn_codigo_error OUT NUMBER,
                            pv_error        OUT VARCHAR2) IS
   
      lv_sentecia VARCHAR2(1000);
      le_error EXCEPTION;
      lv_programa VARCHAR2(60) := 'COP_CREA_TABLA';
   
   BEGIN
      pv_error := NULL;
      lv_sentecia := 'CREATE TABLE ' || pv_nombre_tabla || '(' ||
                     ' Cuenta                       varchar2(24),' ||
                     ' Novedad                      varchar(2),' ||
                     ' Adjetivo                     varchar2(2),' ||
                     ' CalIFicacion                 varchar2(2),' ||
                     ' Cuentas_mora                 varchar2(3),' ||
                     ' Edad_mora                    varchar2(3),' ||
                     ' Reclamo                      varchar(1),' ||
                     ' Tipo_ID                      varchar(3),' ||
                     ' Tipo_persona                  varchar(1),' ||
                     ' IdentIFicacion               varchar2(20)) ' ||
                     ' tablespace  REP_CRED_DAT pctfree 10 ' ||
                     '  pctused 40  initrans 1  maxtrans 255 ' ||
                     '   storage (initial 25M ' || '   next 10M ' ||
                     '   minextents 1 ' || '   maxextents 505 ' ||
                     '   pctincrease 0)' || ' NOLOGGING';
   
      BEGIN
         EXECUTE IMMEDIATE lv_sentecia;
      
      EXCEPTION
         WHEN OTHERS THEN
            pv_error := 'Error al crear la tabla';
            RAISE le_error;
      END;
   
      --Crear el indice   
   
      lv_sentecia := 'create index idx_' || pv_nombre_tabla || ' on ' ||
                     pv_nombre_tabla || ' (CUENTA) ' ||
                     ' tablespace  REP_CRED_IDX ' || ' pctfree 10 ' ||
                     ' initrans 2 ' || ' maxtrans 255 ' || ' storage ' ||
                     ' ( initial 10M ' || '  next 5M ' || '  minextents 1 ' ||
                     '  maxextents unlimited ' || '  pctincrease 0 )';
   
      BEGIN
         EXECUTE IMMEDIATE lv_sentecia;
      EXCEPTION
         WHEN OTHERS THEN
            pv_error := 'Error al crear el indice';
            RAISE le_error;
      END;
   
      --Permisos   
   
      lv_sentecia := 'grant all on ' || pv_nombre_tabla || ' to public';
      BEGIN
         EXECUTE IMMEDIATE lv_sentecia;
      EXCEPTION
         WHEN OTHERS THEN
            pv_error := 'Error dar privelegios sobre la tabla';
            RAISE le_error;
      END;
   
      lv_sentecia := 'create public synonym ' || pv_nombre_tabla || ' for ' ||
                     pv_esquema || '.' || pv_nombre_tabla;
      BEGIN
         EXECUTE IMMEDIATE lv_sentecia;
      EXCEPTION
         WHEN OTHERS THEN
            pv_error := 'Error al crear sinonimo de la tabla';
            RAISE le_error;
      END;
   
      pn_codigo_error := 1;
   
   EXCEPTION
      WHEN le_error THEN
         pn_codigo_error := 0;
         pv_error := 'Error : ' || pv_error || ' en el siguiente programa ' ||
                     lv_programa;
      
      WHEN OTHERS THEN
         pn_codigo_error := 0;
         pv_error := 'Error general en : ' || lv_programa;
      
   END cop_crea_tabla;

   PROCEDURE cop_ingresa_valores_datacre(pv_nombre_tabla   IN VARCHAR2,
                                         pv_cuenta         IN VARCHAR2,
                                         pv_novedad        IN VARCHAR2,
                                         pv_adjetivo       IN VARCHAR2,
                                         pv_calificacion   IN VARCHAR2,
                                         pv_cuentas_mora   IN VARCHAR2,
                                         pv_edad_mora      IN VARCHAR2,
                                         pv_reclamo        IN VARCHAR2,
                                         pv_tipo_id        IN VARCHAR2,
                                         pv_tipo_persona   IN VARCHAR2,
                                         pv_identificacion VARCHAR2,
                                         pn_codigo_error   OUT NUMBER,
                                         pv_error          OUT VARCHAR2
                                         
                                         ) IS
   
      lv_programa VARCHAR2(60) := 'COP_INGRESA_VALORES_DATACREDITO';
      lv_sentecia VARCHAR2(1000) := NULL;
   
      leerror EXCEPTION;
      lvctasmora VARCHAR(1) := 'd';
   
   BEGIN
      -- jmo 19/07/2006, no insertamos los registros si no tenemos identificaci�n, ya que luego   
      -- se generan inconsistencias en los sistemas de datacr�dito.   
      -- jmo 7/08/2006, aumentamos trim para evitar espacios en blanco.   
      pv_error := NULL;
   
      IF TRIM(pv_identificacion) IS NOT NULL THEN
      
         lv_sentecia := '  INSERT /*+ APPEND */ INTO ' || pv_nombre_tabla ||
                        ' nologging (' || ' Cuenta,' || ' Novedad,' ||
                        ' Adjetivo,' || ' CalIFicacion,' ||
                        ' Cuentas_mora,' || ' Edad_mora,' || ' Reclamo,' ||
                        ' Tipo_ID,' || ' Tipo_persona,' ||
                        ' IdentIFicacion)' ||
                        ' values(:pv_cuenta, :pv_novedad, :pv_adjetivo, :pv_calIFicacion, :lvCtasMora, :pv_edad_mora, :pv_reclamo, :pv_tipo_id, :pv_tipo_persona, :pv_identIFicacion )';
      
         EXECUTE IMMEDIATE lv_sentecia
            USING pv_cuenta, pv_novedad, pv_adjetivo, pv_calificacion, lvctasmora, pv_edad_mora, pv_reclamo, pv_tipo_id, pv_tipo_persona, pv_identificacion;
      
      END IF;
   
      pn_codigo_error := 1;
   
   EXCEPTION
      WHEN OTHERS THEN
         pn_codigo_error := 0;
         pv_error := 'Error al insertar en la tabla ' || pv_nombre_tabla ||
                     ' en el siguiente programa : ' || lv_programa;
   END cop_ingresa_valores_datacre;

   PROCEDURE cop_bitacora_ejecucion(pv_nombretabla    IN VARCHAR2 DEFAULT NULL,
                                    pn_reg_exito      IN NUMBER DEFAULT NULL,
                                    pn_reg_error      IN NUMBER DEFAULT NULL,
                                    pv_descripcion    IN VARCHAR2 DEFAULT NULL,
                                    pv_estado_reporte IN VARCHAR2 DEFAULT NULL,
                                    pv_tipo_proceso   IN VARCHAR2 DEFAULT NULL,
                                    pv_consulta       IN VARCHAR2 DEFAULT NULL,
                                    pv_error          OUT VARCHAR2,
                                    pv_tablaorigen    IN VARCHAR2 DEFAULT NULL) IS
   
      -- pragma autonomous_transaction;   
   
      lv_nombre_tabla VARCHAR2(30) := pv_nombretabla;
      ln_codigo       NUMBER := 0;
      gr_existe_tabla cg_existe_tabla%ROWTYPE;
      lb_notfound     BOOLEAN;
      lv_descripcion  VARCHAR(500) := substr(pv_descripcion, 1, 500);
   
   BEGIN
   
      IF pv_tipo_proceso = 'P' THEN
         OPEN cg_existe_tabla(lv_nombre_tabla);
         FETCH cg_existe_tabla
            INTO gr_existe_tabla;
         lb_notfound := cg_existe_tabla%NOTFOUND;
         CLOSE cg_existe_tabla;
      
         IF lb_notfound THEN
            lv_nombre_tabla := 'Tabla no creada';
         END IF;
      
      END IF;
   
      --Encuentro el siguiente c�digo de la bitacora   
      SELECT sc_bitacora_repdatcre.nextval
      INTO   ln_codigo
      FROM   dual;
   
      INSERT INTO co_bitacora_repdatacre
         (cod_bitacora_repdatacre, fecha_ejecucion, tabla_origen, tabla_destino, registros_exito, registros_error, descripcion, estado_reporte, tipo_proceso, consulta)
      VALUES
         (ln_codigo, SYSDATE, pv_tablaorigen, lv_nombre_tabla, pn_reg_exito, pn_reg_error, substr(lv_descripcion, 1, 100), pv_estado_reporte, pv_tipo_proceso, pv_consulta);
   
      COMMIT;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := 'Error al ingresar en la Bit�cora ' || SQLERRM;
      
   END cop_bitacora_ejecucion;

   ----   
   FUNCTION cof_valida_identificacon(pv_identificacion VARCHAR2,
                                     pv_error          OUT VARCHAR2)
      RETURN VARCHAR2 IS
   
      lv_programa       VARCHAR2(60) := 'COF_VALIDA_IDENTIFICACON ';
      lv_identificacion VARCHAR2(20) := pv_identificacion;
      ln_longitud       NUMBER := length(pv_identificacion);
      i                 NUMBER := 1;
      lv_valor          VARCHAR2(1);
      lv_respuesta      VARCHAR2(20) := NULL;
   BEGIN
      LOOP
         lv_valor := substr(lv_identificacion, i, 1);
      
         IF ascii(lv_valor) >= 48 AND ascii(lv_valor) <= 57 THEN
            lv_respuesta := lv_respuesta || lv_valor;
         END IF;
      
         i := i + 1;
      
         EXIT WHEN i > ln_longitud;
      END LOOP;
   
      RETURN lv_respuesta;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := 'Error al validar c�dula ' || lv_programa || SQLERRM;
   END cof_valida_identificacon;

   ---   
   PROCEDURE cop_califica_clientes_gye IS
   
      pdfechcierreperiodo            DATE := SYSDATE;
      lv_fecha                       VARCHAR2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
      lv_nombre_tabla                VARCHAR2(60) := 'CO_REPDATCRE_GYE';
      lv_tabla_origen                VARCHAR2(60) := 'CART_CUENTAS_DAT' ||
                                                     '@FINGYE_BUROCRED';
      lv_novedad                     VARCHAR2(2);
      lv_adjetivo                    VARCHAR2(2);
      lv_calificacion                VARCHAR2(1);
      lv_tipo_iden                   VARCHAR2(3);
      lv_tipo_persona                VARCHAR2(1);
      lv_reclamo                     VARCHAR2(1);
      ln_reg_error                   NUMBER := 0;
      ln_reg_exito                   NUMBER := 0;
      ln_nu_moras                    NUMBER;
      lv_ruc                         VARCHAR2(20);
      lgc_numero_moras               gc_numero_moras%ROWTYPE;
      lgc_numero_moras_reclasificada gc_numero_moras_reclasificada%ROWTYPE;
      lb_found                       BOOLEAN;
      ln_codigo_error                NUMBER;
      ln_ruc                         NUMBER;
      lv_error                       VARCHAR2(1000);
      le_error EXCEPTION;
      lv_programa  VARCHAR2(60) := 'COP_CALIFICA_CLIENTES_INVEN ';
      lv_sentencia VARCHAR(1000) := NULL;
      -- JPA  SUD PROYECTO[3509]   18/07/2008   
      lv_cuenta   VARCHAR2(20) := NULL;
      lv_saldo    VARCHAR2(10) := NULL;
      lv_edadmora VARCHAR2(10) := NULL;
      lv_tipocart VARCHAR2(10) := NULL;
      lt_tipocart cot_string := cot_string();
      lt_cuenta   cot_string := cot_string();
      lt_saldo    cot_string := cot_string();
      lt_edadmora cot_number := cot_number();
   
      -- JPA  SUD PROYECTO[3509]   18/07/2008 
      lv_valor                       varchar2(1);   -- 10393      
      lv_calificacion_neg            varchar2(1);  -- 10393      
   BEGIN
   
      -- Creo la tabla   
      lv_nombre_tabla := lv_nombre_tabla || '_' || lv_fecha;
      -- El procedimiento pregunta si existe la tabla destino, Si la encuentra borrra los datos o sino la crea.   
      -- Tambien deshabilita el indicie para bajar el tiempo del proceso   
      lv_error := NULL;
      cop_existe_tabla(lv_nombre_tabla, lv_error);
   
      IF lv_error IS NOT NULL THEN
         RAISE le_error;
      END IF;
   
      --Leo los datos del cliente usando BULK COLLECT              JPA  SUD PROYECTO[3509]   18/07/2008   
      lv_sentencia := 'begin select  cuen_nume_cuen Cuenta,' ||
                      ' cuen_sald      Saldo, ' ||
                      ' cuen_edad_mora Edad_mora,' ||
                      ' cuen_tipo_cart Tipo_cart ' ||
                      'bulk collect into :1, :2, :3, :4 ' || ' from ' ||
                      lv_tabla_origen || ', CUSTOMER_ALL b ' ||
                      ' WHERE cuen_nume_cuen= b.custcode' || ';' || 'end;';
   
      EXECUTE IMMEDIATE lv_sentencia
         USING OUT lt_cuenta, OUT lt_saldo, OUT lt_edadmora, OUT lt_tipocart;
   
      FOR a IN lt_cuenta.first .. lt_cuenta.last
      LOOP
      
         lv_cuenta := lt_cuenta(a);
         lv_saldo := lt_saldo(a);
         lv_edadmora := lt_edadmora(a);
         lv_tipocart := lt_tipocart(a);
         --Inicializar variables. DCH 20110406 Ecarfo 1706
         lv_novedad := NULL;
         ln_nu_moras := NULL;
         lv_ruc := NULL;
         lv_reclamo := NULL;
         lv_calificacion := NULL;
         lv_adjetivo := NULL;
         lv_tipo_iden := NULL;
         lv_tipo_persona := NULL;
         ln_ruc := NULL;
         --Valida novedad   
         IF to_number(lv_saldo) > 0 AND lv_tipocart = '0' THEN
            lv_novedad := '12'; --Cartera reclasIFicada   
         ELSE
            IF to_number(lv_saldo) > 0 AND lv_tipocart = '1' THEN
               lv_novedad := '13'; --Cartera Castigada   
            END IF;
         END IF;
      
         --La edad de mora se calIFica en dias si es mayor a 12 meses se pone como 360   
         IF to_number(lv_edadmora) >= 12 THEN
            lv_edadmora := '360';
         ELSE
            lv_edadmora := to_char(to_number(lv_edadmora) * 30);
         END IF;
      
         --CalIFico la cuenta -- PASAR COMO COLUMNA DEL SELECT   
         lv_calificacion := cof_califica_cuenta(lv_edadmora);
      
         -- N�mero de cuentas en mora de BSCS   
         OPEN gc_numero_moras(lv_cuenta, gv_ohstatus);
         FETCH gc_numero_moras
            INTO lgc_numero_moras;
         lb_found := gc_numero_moras%FOUND;
         CLOSE gc_numero_moras;
         IF lb_found THEN
            ln_nu_moras := lgc_numero_moras.nu_moras; --Autorizado por el cliente   
            lv_ruc := lgc_numero_moras.identificacion; -- IdentIFicaci�n del cliente en BSCS   
         ELSE
            --Para cuentas reclasificadas ya no se busca los datos en BSCS   
            --sino se debe buscar en FINANCIERO   
            OPEN gc_numero_moras_reclasificada(lv_cuenta);
            FETCH gc_numero_moras_reclasificada
               INTO lgc_numero_moras_reclasificada;
            lb_found := gc_numero_moras_reclasificada%FOUND;
            CLOSE gc_numero_moras_reclasificada;
            IF lb_found THEN
               ln_nu_moras := lgc_numero_moras_reclasificada.nu_moras; --Autorizado por el cliente   
               lv_ruc := lgc_numero_moras_reclasificada.identificacion; -- IdentIFicaci�n del cliente en BSCS   
            END IF;
         END IF;
      
         --Encuentro el tipo de persona   
         lv_reclamo := cof_reclamo(lv_cuenta);
      
         ---Valido tipo persona   
         IF lv_ruc IS NOT NULL THEN
            --DCH 20110406
            BEGIN
               ln_ruc := to_number(lv_ruc);
            
            EXCEPTION
               WHEN invalid_number THEN
                  lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => lv_error);
               WHEN OTHERS THEN
                  lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => lv_error);
            END;
         
            --Valido la IdentIFicaci�n de acuerdo al n�mero de caracteres   
            IF (length(lv_ruc)) = 10 THEN
               lv_tipo_iden := 'CED';
               lv_tipo_persona := 'N';
            ELSE
               IF (length(lv_ruc)) = 13 THEN
                  lv_tipo_iden := 'RUC';
                  lv_tipo_persona := 'J';
               ELSE
                  lv_tipo_iden := 'PAS';
                  lv_tipo_persona := 'N';
               END IF;
            END IF;
         END IF;
         --Para los clientes que cayeron en mora pero la cartera fue recuperada   
         IF to_number(nvl(lv_saldo, 0)) < 0.50 THEN
            lv_novedad := '14'; --Cartera recuperada   
            lv_calificacion := 'A'; --Bajo riesgo   
            ln_nu_moras := 0;
            lv_edadmora := '0';
            lv_saldo := '0'; --Saldo menor que 0.50 se hace 0   
         END IF;
      
         --Valido el adjetivo  en base a la novedad   
         IF lv_novedad = '08' OR lv_novedad = '09' OR lv_novedad = '12' OR
            lv_novedad = '13' THEN
            lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90   
         ELSE
            IF lv_novedad = '06' OR lv_novedad = '07' THEN
               lv_adjetivo := '11'; --Linea desconectada   
            ELSE
               lv_adjetivo := '00';
            END IF;
         END IF;
      
         --10393  RTH    12/02/2016
         OPEN cl_parametros(10393, 'NEG_DEUDAS_BURO');
        FETCH cl_parametros
         INTO lv_valor;
        CLOSE cl_parametros;
        
        IF nvl(lv_valor, 'N') = 'S' THEN
           lv_calificacion_neg := porta.cok_trx_negociaciones.cof_calificacion_buro@axis(lv_cuenta); 
           IF lv_calificacion_neg IS NOT NULL and (length(lv_calificacion_neg)) = 1 THEN
                lv_calificacion :=  lv_calificacion_neg;
           END IF;                       
        END IF;
         --10393  RTH    12/02/2016
         
         --Ingreso valores   
         /**********************************/
         --[5037] -INI GBO 04-06-2010
         lv_band_5037 := bsk_api_clasificacion.bsf_obtener_parametro(5037, 'GV_BANDERA_5037_BURO');
      
         lv_band_5037 := nvl(lv_band_5037, 'N');
      
         IF lv_band_5037 = 'S' THEN
         
            lb_found := bsk_api_clasificacion.isdiferida(lv_cuenta);
         
            IF lb_found THEN
               --SI ES DIFERIDA Y DADA DE ALTA INSERTE
            
               OPEN c_valida_cta_alta(lv_cuenta);
               FETCH c_valida_cta_alta
                  INTO lc_valida_cta_alta;
               CLOSE c_valida_cta_alta;
            
               IF lc_valida_cta_alta.alta = 'S' THEN
               
                  cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
               
                  IF ln_codigo_error = 0 THEN
                     ln_reg_error := ln_reg_error + 1;
                  
                  ELSE
                     ln_reg_exito := ln_reg_exito + 1;
                  END IF;
               END IF;
            ELSE
               --NO EXISTE DIFERIDA
               cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
            
               IF ln_codigo_error = 0 THEN
                  ln_reg_error := ln_reg_error + 1;
               
               ELSE
                  ln_reg_exito := ln_reg_exito + 1;
               END IF;
            END IF;
         
         ELSE
            --SI BANDERA ABAJO
         
            /************************************/
            cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
         
            IF ln_codigo_error = 0 THEN
               ln_reg_error := ln_reg_error + 1;
            
            ELSE
               ln_reg_exito := ln_reg_exito + 1;
            END IF;
         
         END IF; --[5037] -FIN BANDERA 04-06-2010
         --[5037] -FIN GBO 04-06-2010     
         IF MOD(ln_reg_exito, gn_commit) = 0 THEN
            COMMIT;
         END IF;
      
      END LOOP;
   
      COMMIT;
   
      lv_error := NULL;
      -- Habilito INDICE   
      cop_habilita_indice(lv_nombre_tabla, lv_error);
   
      IF lv_error IS NOT NULL THEN
         RAISE le_error;
      END IF;
      ---   
   
      cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'N', pv_tipo_proceso => 'P', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
   
   EXCEPTION
      WHEN le_error THEN
         lv_error := lv_error || ' ' || lv_programa || SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
      WHEN OTHERS THEN
         lv_error := 'Error general ' || lv_programa || ': ' || SQLERRM;
      
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
      
   END cop_califica_clientes_gye;

   PROCEDURE cop_califica_clientes_uio IS
   
      pdfechcierreperiodo            DATE := SYSDATE;
      lv_fecha                       VARCHAR2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
      lv_nombre_tabla                VARCHAR2(60) := 'CO_REPDATCRE_UIO';
      lv_tabla_origen                VARCHAR2(60) := 'CART_CUENTAS_DAT' ||
                                                     '@B_BUROCRED';
      lv_novedad                     VARCHAR2(2);
      lv_adjetivo                    VARCHAR2(2);
      lv_calificacion                VARCHAR2(1);
      lv_tipo_iden                   VARCHAR2(3);
      lv_tipo_persona                VARCHAR2(1);
      lv_reclamo                     VARCHAR2(1);
      ln_reg_error                   NUMBER := 0;
      ln_reg_exito                   NUMBER := 0;
      ln_nu_moras                    NUMBER;
      lv_ruc                         VARCHAR2(20);
      lgc_numero_moras               gc_numero_moras%ROWTYPE;
      lgc_numero_moras_reclasificada gc_numero_moras_reclasificada%ROWTYPE;
      lb_found                       BOOLEAN;
      ln_codigo_error                NUMBER;
      ln_ruc                         NUMBER;
      lv_error                       VARCHAR2(1000);
      le_error EXCEPTION;
      lv_programa  VARCHAR2(60) := 'COP_CALIFICA_CLIENTES_INVEN ';
      lv_sentencia VARCHAR(1000) := NULL;
   
      -- JPA  SUD PROYECTO[3509]   18/07/2008   
      lv_cuenta   VARCHAR2(20) := NULL;
      lv_saldo    VARCHAR2(10) := NULL;
      lv_edadmora VARCHAR2(10) := NULL;
      lv_tipocart VARCHAR2(10) := NULL;
      lt_tipocart cot_string := cot_string();
      lt_cuenta   cot_string := cot_string();
      lt_saldo    cot_string := cot_string();
      lt_edadmora cot_number := cot_number();
      -- JPA  SUD PROYECTO[3509]   18/07/2008  
      lv_valor                       varchar2(1);   -- 10393      
      lv_calificacion_neg            varchar2(1);  -- 10393  
   BEGIN
   
      -- Creo la tabla   
      lv_nombre_tabla := lv_nombre_tabla || '_' || lv_fecha;
      -- El procedimiento pregunta si existe la tabla destino, Si la encuentra borrra los datos o sino la crea.   
      -- Tambien deshabilita el indicie para bajar el tiempo del proceso   
      cop_existe_tabla(lv_nombre_tabla, lv_error);
   
      IF lv_error IS NOT NULL THEN
         RAISE le_error;
      END IF;
   
      --Leo los datos del cliente usando BULK COLLECT              JPA  SUD PROYECTO[3509]   18/07/2008   
      lv_sentencia := 'BEGIN SELECT  cuen_nume_cuen Cuenta,' ||
                      ' cuen_sald      Saldo, ' ||
                      ' cuen_edad_mora Edad_mora,' ||
                      ' cuen_tipo_cart Tipo_cart ' ||
                      'bulk collect INTO :1, :2, :3, :4 ' || ' FROM ' ||
                      lv_tabla_origen || ', CUSTOMER_ALL b ' ||
                      ' WHERE cuen_nume_cuen= b.custcode' || ';' || 'END;';
   
      EXECUTE IMMEDIATE lv_sentencia
         USING OUT lt_cuenta, OUT lt_saldo, OUT lt_edadmora, OUT lt_tipocart;
   
      FOR a IN lt_cuenta.first .. lt_cuenta.last
      LOOP
      
         lv_cuenta := lt_cuenta(a);
         lv_saldo := lt_saldo(a);
         lv_edadmora := lt_edadmora(a);
         lv_tipocart := lt_tipocart(a);
      
         --Inicializar variables. DCH 20110406 Ecarfo 1706
         lv_novedad := NULL;
         ln_nu_moras := NULL;
         lv_ruc := NULL;
         lv_reclamo := NULL;
         lv_calificacion := NULL;
         lv_adjetivo := NULL;
         lv_tipo_iden := NULL;
         lv_tipo_persona := NULL;
         ln_ruc := NULL;
      
         --Valida novedad   
         IF to_number(lv_saldo) > 0 AND lv_tipocart = '0' THEN
            lv_novedad := '12'; --Cartera reclasIFicada   
         ELSE
            IF to_number(lv_saldo) > 0 AND lv_tipocart = '1' THEN
               lv_novedad := '13'; --Cartera Castigada   
            END IF;
         END IF;
      
         --La edad de mora se calIFica en dias si es mayor a 12 meses se pone como 360   
         IF to_number(lv_edadmora) >= 12 THEN
            lv_edadmora := '360';
         ELSE
            lv_edadmora := to_char(to_number(lv_edadmora) * 30);
         END IF;
      
         --CalIFico la cuenta   
         lv_calificacion := cof_califica_cuenta(lv_edadmora);
      
         -- N�mero de cuentas en mora de BSCS   
         OPEN gc_numero_moras(lv_cuenta, gv_ohstatus);
         FETCH gc_numero_moras
            INTO lgc_numero_moras;
         lb_found := gc_numero_moras%FOUND;
         CLOSE gc_numero_moras;
         IF lb_found THEN
            ln_nu_moras := lgc_numero_moras.nu_moras; --Autorizado por el cliente   
            lv_ruc := lgc_numero_moras.identificacion; -- IdentIFicaci�n del cliente en BSCS   
         ELSE
            --Para cuentas reclasificadas ya no se busca los datos en BSCS   
            --sino se debe buscar en FINANCIERO   
            OPEN gc_numero_moras_reclasificada(lv_cuenta);
            FETCH gc_numero_moras_reclasificada
               INTO lgc_numero_moras_reclasificada;
            lb_found := gc_numero_moras_reclasificada%FOUND;
            CLOSE gc_numero_moras_reclasificada;
            IF lb_found THEN
               ln_nu_moras := lgc_numero_moras_reclasificada.nu_moras; --Autorizado por el cliente   
               lv_ruc := lgc_numero_moras_reclasificada.identificacion; -- IdentIFicaci�n del cliente en BSCS   
            END IF;
         END IF;
      
         --Encuentro el tipo de persona   
         lv_reclamo := cof_reclamo(lv_cuenta);
      
         ---Valido tipo persona   
         IF lv_ruc IS NOT NULL THEN
            --DCH 20110406 
            BEGIN
               ln_ruc := to_number(lv_ruc);
            EXCEPTION
               WHEN invalid_number THEN
                  lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => lv_error);
               WHEN OTHERS THEN
                  lv_ruc := cof_valida_identificacon(pv_identificacion => lv_ruc, pv_error => lv_error);
            END;
         
            --Valido la IdentIFicaci�n de acuerdo al n�mero de caracteres   
            IF (length(lv_ruc)) = 10 THEN
               lv_tipo_iden := 'CED';
               lv_tipo_persona := 'N';
            ELSE
               IF (length(lv_ruc)) = 13 THEN
                  lv_tipo_iden := 'RUC';
                  lv_tipo_persona := 'J';
               ELSE
                  lv_tipo_iden := 'PAS';
                  lv_tipo_persona := 'N';
               END IF;
            END IF;
         END IF;
      
         --Para los clientes que cayeron en mora pero la cartera fue recuperada   
         IF to_number(nvl(lv_saldo, 0)) < 0.50 THEN
            lv_novedad := '14'; --Cartera recuperada   
            lv_calificacion := 'A'; --Bajo riesgo   
            ln_nu_moras := 0;
            lv_edadmora := '0';
            lv_saldo := '0'; --Saldo menor que 0.50 se hace 0   
         END IF;
      
         --Valido el adjetivo  en base a la novedad   
         IF lv_novedad = '08' OR lv_novedad = '09' OR lv_novedad = '12' OR
            lv_novedad = '13' THEN
            lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90   
         ELSE
            IF lv_novedad = '06' OR lv_novedad = '07' THEN
               lv_adjetivo := '11'; --Linea desconectada   
            ELSE
               lv_adjetivo := '00';
            END IF;
         END IF;
        
        
        --10393  RTH    12/02/2016
         OPEN cl_parametros(10393, 'NEG_DEUDAS_BURO');
        FETCH cl_parametros
         INTO lv_valor;
        CLOSE cl_parametros;
        
        IF nvl(lv_valor, 'N') = 'S' THEN
         lv_calificacion_neg := porta.cok_trx_negociaciones.cof_calificacion_buro@axis(lv_cuenta); 
           IF lv_calificacion_neg IS NOT NULL and (length(lv_calificacion_neg)) = 1 THEN 
                lv_calificacion :=  lv_calificacion_neg;
           END IF;   
         END IF; 
         --10393  RTH    12/02/2016
      
      
         --[5037] -INI GBO 04-06-2010
         lv_band_5037 := bsk_api_clasificacion.bsf_obtener_parametro(5037, 'GV_BANDERA_5037_BURO');
      
         lv_band_5037 := nvl(lv_band_5037, 'N');
      
         IF lv_band_5037 = 'S' THEN
         
            lb_found := bsk_api_clasificacion.isdiferida(lv_cuenta);
         
            IF lb_found THEN
               --SI ES DIFERIDA Y DADA DE ALTA INSERTA
            
               OPEN c_valida_cta_alta(lv_cuenta);
               FETCH c_valida_cta_alta
                  INTO lc_valida_cta_alta;
               CLOSE c_valida_cta_alta;
            
               IF lc_valida_cta_alta.alta = 'S' THEN
               
                  cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
               
                  IF ln_codigo_error = 0 THEN
                     ln_reg_error := ln_reg_error + 1;
                  
                  ELSE
                     ln_reg_exito := ln_reg_exito + 1;
                  END IF;
               END IF;
            
            ELSE
               --NO EXISTE COMO DIFERIDA
               cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
            
               IF ln_codigo_error = 0 THEN
                  ln_reg_error := ln_reg_error + 1;
               
               ELSE
                  ln_reg_exito := ln_reg_exito + 1;
               END IF;
            END IF;
         
         ELSE
            -- BANDERA ABAJO
         
            /************************************/
            cop_ingresa_valores_datacre(pv_nombre_tabla => lv_nombre_tabla, pv_cuenta => lv_cuenta, pv_novedad => lv_novedad, pv_adjetivo => lv_adjetivo, pv_calificacion => lv_calificacion, pv_cuentas_mora => ln_nu_moras, pv_edad_mora => lv_edadmora, pv_reclamo => lv_reclamo, pv_tipo_id => lv_tipo_iden, pv_tipo_persona => lv_tipo_persona, pv_identificacion => lv_ruc, pn_codigo_error => ln_codigo_error, pv_error => lv_error);
         
            IF ln_codigo_error = 0 THEN
               ln_reg_error := ln_reg_error + 1;
            
            ELSE
               ln_reg_exito := ln_reg_exito + 1;
            END IF;
         END IF; --[5037] -  FIN BANDERA 04-06-2010 
         --[5037] -FIN GBO 04-06-2010
      
         IF MOD(ln_reg_exito, gn_commit) = 0 THEN
            COMMIT;
         END IF;
      
      END LOOP;
   
      COMMIT;
      -- HABILITAR INDICE   
      lv_error := NULL;
      cop_habilita_indice(lv_nombre_tabla, lv_error);
   
      IF lv_error IS NOT NULL THEN
         RAISE le_error;
      END IF;
   
      cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'N', pv_tipo_proceso => 'P', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
   
   EXCEPTION
      WHEN le_error THEN
         lv_error := lv_error || ' ' || lv_programa || SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
      WHEN OTHERS THEN
         lv_error := 'Error general ' || lv_programa || ': ' || SQLERRM;
      
         cop_bitacora_ejecucion(pv_nombretabla => lv_nombre_tabla, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'E', pv_tipo_proceso => 'E', pv_consulta => NULL, pv_error => lv_error, pv_tablaorigen => lv_tabla_origen);
      
   END cop_califica_clientes_uio;

   -- JPA  SUD PROYECTO[3509]   15/08/2008   
   PROCEDURE cop_genera_archivo IS
   
      lv_error         VARCHAR2(1000);
      lv_generaarchivo VARCHAR2(100);
      le_error EXCEPTION;
   
   BEGIN
   
      --Se encarga de realizar un truncate a la tabla co_union_clientes y procesar todos los ciclos existentes   
      cop_union_clientes(lv_error);
   
      IF lv_error IS NOT NULL THEN
         lv_generaarchivo := 'Error en COP_UNION_CLIENTES';
         RAISE le_error;
      END IF;
   
      -- Este procedimiento saca el total de registros de la tabla co_union_clientes y la sumatoria por novedad genera el   
      -- archivo rep_datacre.dat lo abre solo con escritura y luego ingresa a la bitacora si el reporte se genero con �xito   
   
      cop_crear_archivo(lv_error);
   
      IF lv_error IS NOT NULL THEN
         lv_generaarchivo := 'Error en COP_CREAR_ARCHIVO';
         RAISE le_error;
      END IF;
   
   EXCEPTION
      WHEN le_error THEN
      
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => 0, pn_reg_error => 0, pv_descripcion => 'Error al generar reporte :' ||
                                                   lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'R', pv_consulta => lv_generaarchivo, pv_error => lv_error);
      
      WHEN OTHERS THEN
         lv_error := SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => 0, pn_reg_error => 0, pv_descripcion => 'Error al generar reporte :' ||
                                                   lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'R', pv_consulta => lv_generaarchivo, pv_error => lv_error);
      
   END cop_genera_archivo;

   PROCEDURE cop_actualiza_estados_bitacora(pn_codigo IN NUMBER,
                                            pv_estado IN VARCHAR2,
                                            pv_error  OUT VARCHAR2) IS
   
   BEGIN
   
      UPDATE co_bitacora_repdatacre
      SET    estado_reporte = pv_estado
      WHERE  cod_bitacora_repdatacre = pn_codigo;
   
      COMMIT;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := SQLERRM;
         ROLLBACK;
   END cop_actualiza_estados_bitacora;

   PROCEDURE cop_crear_archivo(pv_error OUT VARCHAR2) IS
   
      file_handle1 sys.utl_file.file_type;
      fs_arch       VARCHAR2(100);
      --path          VARCHAR2(50) := '/home/gsioper/procesos/datacredito';
      --path          VARCHAR2(50) := '/gsioper/procesos/datacredito';
      path          VARCHAR2(50) := 'DATACREDITO';
      lv_fecha      VARCHAR2(10) := to_char(SYSDATE, 'mmyyyy');
      pv_nomb_arch1 VARCHAR2(100) := 'rep_datacre.dat'; --SE MODIFICA NOMBRE PARA QUE NO INCLUYA FECHA   
      lv_menserr    VARCHAR2(1000);
      le_error EXCEPTION;
      lv_sentencia     VARCHAR2(20000) := NULL;
      lv_cuentareg     VARCHAR2(20000) := NULL;
      lv_generaarchivo VARCHAR2(20000) := NULL;
      lv_totalreg      VARCHAR2(10);
      ln_reg_exito     NUMBER;
      lv_totalnov      VARCHAR2(10);
      lv_error         VARCHAR2(1000);
      lv_menserr       VARCHAR2(1000);
      ln_reg_error     NUMBER;
      -- JPA  SUD PROYECTO[3509]   15/08/2008   
      lt_totalreg cot_string := cot_string();
      lt_totalnov cot_string := cot_string();
      lt_datos    cot_string := cot_string();
      lv_datos    VARCHAR2(1000);
      lt_wline    cot_string := cot_string();
      wline       VARCHAR2(2000);
      -- JPA  SUD PROYECTO[3509]   15/08/2008   
      /*[5488] SUD Glenn Avil�s Vicu�a */
      CURSOR c_parametros(cv_parametro VARCHAR2) IS
         SELECT p.valor
         FROM   gv_parametros p
         WHERE  p.id_parametro = cv_parametro;
   
      lc_parametros           c_parametros%ROWTYPE;
      lv_excepciones_clientes VARCHAR2(20) := 'GV_EXCEPCION_BURO';
      /*****************************************************************/
   
   BEGIN
   
      OPEN c_parametros(lv_excepciones_clientes);
      FETCH c_parametros
         INTO lc_parametros;
      CLOSE c_parametros;
   
      /*[5488] SUD Glenn Avil�s V*/
      IF (lc_parametros.valor = 'S') THEN
         lv_sentencia := 'SELECT t.datos_registro, t.novedad
                            FROM co_union_clientes t
                            WHERE t.identificacion NOT IN (SELECT e.identificacion 
                                                             FROM co_excepciones_clientes e
                                                             WHERE e.estado = ''A'')';
      ELSE
         lv_sentencia := 'SELECT datos_registro , novedad  FROM CO_UNION_CLIENTES';
      END IF;
      /*************************************************************************************/
   
      lv_cuentareg := 'begin SELECT  count (*) Total_Reg, sum(to_number(novedad)) Total_novedad bulk collect into :1, :2 FROM(' ||
                      lv_sentencia || ') ; end;';
   
      --CUENTO LOS ARCHIVOS Y LAS NOVEDADES   
      BEGIN
      
         EXECUTE IMMEDIATE lv_cuentareg
            USING OUT lt_totalreg, OUT lt_totalnov;
      
         FOR a IN lt_totalreg.first .. lt_totalreg.last
         LOOP
         
            lv_totalreg := lt_totalreg(a);
            lv_totalnov := lt_totalnov(a);
         
         END LOOP;
      
      EXCEPTION
         WHEN OTHERS THEN
            lv_error := 'Error al contar registros en el archivo ' ||
                        SQLERRM;
            RAISE le_error;
         
      END;
   
      lv_totalreg := lpad(to_char(to_number(lv_totalreg) + 2), 8, 0);
      lv_totalnov := lpad(lv_totalnov, 7, 0);
   
      SELECT valor
      INTO   fs_arch
      FROM   gv_parametros
      WHERE  id_parametro = 'FS_ARCH_BURO';
      --Abro el archivo   
      --file_handle1 := sys.utl_file.fopen(fs_arch || path, pv_nomb_arch1, 'W');
      file_handle1 := sys.utl_file.fopen(path, pv_nomb_arch1, 'W');
   
      IF NOT sys.utl_file.is_open(file_handle1) THEN
         lv_error := 'Hubo error al abrir el archivo ' || SQLERRM;
         RAISE le_error;
      END IF;
   
      --Consulta que genera el archivo   
   
      lv_generaarchivo := ' begin SELECT *  bulk collect into :1, :2  FROM (SELECT rpad((''00000000000000000023000423''||(to_char(sysdate,''yyyymm'')-1 ||''MM'')),600,''0'') as REGISTRO, ''00'' novedad FROM dual union all  ' ||
                          lv_sentencia || ' ' ||
                          'union all SELECT rpad(''ZZZZZZZZZZZZZZZZZZ''||to_char(sysdate,''yyyymmdd'')||''' ||
                          lv_totalreg || lv_totalnov ||
                          ''' ,600,''0'') as REGISTRO,''00'' novedad FROM dual); END;';
   
      EXECUTE IMMEDIATE lv_generaarchivo
         USING OUT lt_wline, OUT lt_datos;
   
      FOR a IN lt_wline.first .. lt_wline.last
      LOOP
         wline := lt_wline(a);
         lv_datos := lt_datos(a);
      
         BEGIN
            wline := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(upper(wline), '�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U'), '�', 'N');
         EXCEPTION
            WHEN OTHERS THEN
               lv_error := SQLERRM;
               RETURN;
         END;
      
         --Escribo en el archivo y cuento lo que se generen con �xito o error   
         BEGIN
            sys.utl_file.put_line(file_handle1, wline);
            ln_reg_exito := ln_reg_exito + 1;
         EXCEPTION
            WHEN OTHERS THEN
               ln_reg_error := ln_reg_error + 1;
               lv_error := SQLERRM;
         END;
      END LOOP;
   
      sys.utl_file.fclose(file_handle1);
   
      --Ingreso a la bitacora que el reporte se genero con �xito   
      cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => lv_totalreg, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'S', --estado generado con �xito   
                             pv_tipo_proceso => 'R', --Indicador de reporte   
                             pv_consulta => lv_generaarchivo, pv_error => lv_error);
   
      --Actualizo estados   
   
   EXCEPTION
      WHEN le_error THEN
         pv_error := SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al generar reporte :' ||
                                                   lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'R', pv_consulta => lv_generaarchivo, pv_error => lv_error);
      
      WHEN OTHERS THEN
         pv_error := SQLERRM;
         IF sys.utl_file.is_open(file_handle1) THEN
            sys.utl_file.fclose(file_handle1);
         END IF;
         lv_error := SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al generar reporte :' ||
                                                   lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'R', pv_consulta => lv_generaarchivo, pv_error => lv_error);
      
   END cop_crear_archivo;

   /*========================================================   
      Proyecto       : [2702] Tercer Ciclo de Facturacion   
      Fecha ModIF.   : 19/11/2007   
      Solicitado por : SIS Guillermo Proa�o   
      ModIFicado por : Anl. Eloy Carchi Rojas   
      Motivo         : No debe haber c�digo quemado.   
   =========================================================*/

   PROCEDURE cop_union_clientes(pv_error OUT VARCHAR2) IS
   
      CURSOR c_nombre_tabla(pc_tabla VARCHAR2) IS
         SELECT cod_bitacora_repdatacre, tabla_origen, tabla_destino
         FROM   co_bitacora_repdatacre
         WHERE  cod_bitacora_repdatacre =
                (SELECT MAX(cod_bitacora_repdatacre)
                 FROM   co_bitacora_repdatacre br
                 WHERE  br.estado_reporte IN (gv_estado_n, gv_estado_s)
                        AND br.fecha_ejecucion BETWEEN SYSDATE - 30 AND
                        SYSDATE
                        AND br.tabla_destino LIKE '%' || pc_tabla || '%');
   
      lv_fecha   VARCHAR2(10) := to_char(SYSDATE, 'mmyyyy');
      lv_menserr VARCHAR2(1000);
      le_error EXCEPTION;
      lv_sentencia VARCHAR2(20000) := NULL;
      --lv_sentencia2          VARCHAR2(1000):=NULL;   
      --lv_sentencia3           VARCHAR2 (20000):=NULL;   
      lv_generaarchivo  VARCHAR2(20000) := NULL;
      lv_datos          VARCHAR2(1000);
      lv_error          VARCHAR2(1000);
      lv_novedad        VARCHAR2(10);
      lv_cuenta         VARCHAR2(20);
      lv_identificacion VARCHAR2(20); --[5488] SUD Glenn Avil�s v
      ln_reg_exito      NUMBER := 0;
      ln_reg_error      NUMBER := 0;
   
      --[2702] ECA   
      /*lv_parametro_08  varchar2(50):='co_repdatcre_08';   
      lv_parametro_24  varchar2(50):='co_repdatcre_24';*/
      lv_parametro_ciclo VARCHAR2(50);
      lv_parametro_gye   VARCHAR2(50) := 'CO_REPDATCRE_GYE';
      lv_parametro_uio   VARCHAR2(50) := 'CO_REPDATCRE_UIO';
      lv_repcarcli_ciclo VARCHAR2(50);
      lv_repdacre_ciclo  VARCHAR2(50);
      lv_inven_gye       VARCHAR2(50);
      lv_inven_uio       VARCHAR2(50);
      lb_found           BOOLEAN;
      ln_cod_ciclo       NUMBER;
      ln_codgye          NUMBER;
      ln_coduio          NUMBER;
      lc_nombre_tabla    c_nombre_tabla%ROWTYPE;
      -- JPA  SUD PROYECTO[3509]   15/08/2008   
      lt_datos          cot_string := cot_string();
      lt_novedad        cot_string := cot_string();
      lt_cuenta         cot_string := cot_string();
      lt_identificacion cot_string := cot_string();
   
      -- JPA  SUD PROYECTO[3509]   15/08/2008   
      CURSOR c_procesaciclos IS
         SELECT *
         FROM   fa_ciclos_bscs
         ORDER  BY id_ciclo ASC;
   
      --INI SBR 4502
   
   
      lv_tabla_buro VARCHAR2(50) := 'Co_Financiamiento_Buro_Datos';
   
      --BSE CLS 4502
      --SCP
      lv_proceso_par_scp     VARCHAR2(100) := 'COK_DAT_BURO';
      lv_valor_par_scp       VARCHAR2(10);
      lv_descripcion_par_scp VARCHAR2(100);
      ln_error_scp           NUMBER;
      lv_error_scp           VARCHAR2(1000);
      lv_referencia_scp      VARCHAR2(1000) := 'Ejecucion De Proceso COP_UNION_CLIENTES En BSCS  - ' ||
                                               SYSDATE || ' - ' || USER;
      lv_unidad_registro_scp VARCHAR2(100) := 'LLAMADO_PROCESO_AXIS';
      ln_id_bitacora_scp     NUMBER;
      ln_cantidad_commit     NUMBER;
      ln_cantidad_registros  NUMBER;
      --SCP
      le_error_format EXCEPTION;
      le_error_insert EXCEPTION;
      ln_cant_commit    NUMBER;
      ln_reg_exito_buro NUMBER;
      ln_total_reg_buro NUMBER;
      ln_no_commit      NUMBER;
      ln_errores_reg    NUMBER;
      lb_flag_eje       BOOLEAN;
      --BSE CLS 4502
      /*[5488] SUD Glenn Avil�s V*/
      le_error_rep EXCEPTION;
      /***************************/
   BEGIN
      BEGIN
         lv_sentencia := 'truncate table co_union_clientes';
      
         EXECUTE IMMEDIATE lv_sentencia;
         --EJECUTA_SENTENCIA(lv_sentencia,lv_MensErr);   
      EXCEPTION
         WHEN OTHERS THEN
            RAISE le_error;
      END;
   
      --[2702] INI ECA   
      --Ciclo para procesar todos los ciclos existentes   
      FOR c IN c_procesaciclos
      LOOP
         --Seteo variables   
         lv_parametro_ciclo := 'CO_REPDATCRE_' || c.dia_ini_ciclo;
         ln_reg_exito := 0;
         ln_reg_error := 0;
      
         --Encuentro los nombre de tablas   
         OPEN c_nombre_tabla(lv_parametro_ciclo);
         FETCH c_nombre_tabla
            INTO lc_nombre_tabla;
         lb_found := c_nombre_tabla%FOUND;
         CLOSE c_nombre_tabla;
      
         IF lb_found THEN
            lv_repcarcli_ciclo := lc_nombre_tabla.tabla_origen;
            lv_repdacre_ciclo := lc_nombre_tabla.tabla_destino;
            ln_cod_ciclo := lc_nombre_tabla.cod_bitacora_repdatacre;
         END IF;
      
         lb_found := FALSE;
      
         BEGIN
            lv_sentencia := 'begin   SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '') ||   
                                 lpad(b.identIFicacion,13,''0'')||   
                                 rpad(decode(instr(apellidos,'' ''),0,apellidos,NVL(substr(apellidos,1,instr(apellidos,'' '')),'' '')),15,'' '' )||   
                                 rpad(decode(instr(apellidos,'' ''),0,'' '',NVL(substr(apellidos,instr(apellidos,'' '')+1,length(apellidos)),'' '')),15,'' '' )||   
                                 rpad(decode(instr(nombres,'' ''),0,nombres,NVL(substr(nombres,1,instr(nombres,'' '')),'' '')),15,'' '' )||   
                                 rpad(decode(instr(nombres,'' ''),0,'' '',NVL(substr(nombres,instr(nombres,'' '')+1,length(nombres)),'' '')),15,'' '' )||   
                                 ''000000''||   
                                 to_char(csactivated,''yyyymm'')||   
                                 rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||   
                                 lpad(round(decode(sign(balance_12),-1,0,balance_12)),10,0) ||   
                                 b.novedad ||   
                                 b.adjetivo||   
                                 decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||   
                                 ''0000000000'' ||   
                                 lpad(round(decode(sign(a.totaladeuda),-1,0,a.totaladeuda)),10,0) ||   
                                 lpad(round(a.totalvencida),10,0) ||   
                                 ''0000000000''  ||   
                                 ''142''         ||   
                                 b.calIFicacion||   
                                 rpad(nvl(a.canton,'' ''),20,'' '') ||   
                                 rpad(nvl(a.direccion2,'' ''),120,'' '') ||   
                                 ''0000000000000000000000000000000000000000000000000000000000000000000000'' ||   
                                  rpad(nvl(a.direccion,'' ''),120,'' '')||   
                                  ''000000000'' ||  --4.26   --4.27   
                                  ''000''|| --4.28   
                                  lpad(b.cuentas_mora,3,''0'')|| --4.29   
                                  ''00000000000000000000000000000000000000000000'' ||   
                                  ''1''|| --4.34   
                                 lpad(b.edad_mora,3,0)|| --4.35   
                                 ''00000000''|| 
                                 decode( b.reclamo,''S'',0,9)|| 
                                 ''000000000000000000000000000000'' as REGISTRO, 
                                  b.novedad as novedad, 
                                  a.cuenta as cuenta, 
                                  b.identIFicacion AS identificacion  bulk collect into :1, :2, :3, :4   
   
                          FROM   
                          customer_all          c,   
                          ' ||
                            lv_repcarcli_ciclo ||
                            ' a,   
                          ' || lv_repdacre_ciclo ||
                            ' b   
                          WHERE a.cuenta = b.cuenta   
                          AND   b.cuenta= c.custcode;   end;';
            -- dbms_output.put_line(lv_sentencia);   
            EXECUTE IMMEDIATE lv_sentencia
               USING OUT lt_datos, OUT lt_novedad, OUT lt_cuenta, OUT lt_identificacion;
         
            IF (lt_datos.count = 0) THEN
               RAISE le_error_rep;
            END IF;
         
            FOR a IN lt_datos.first .. lt_datos.last
            LOOP
            
               lv_datos := lt_datos(a);
               lv_novedad := lt_novedad(a);
               lv_cuenta := lt_cuenta(a);
               lv_identificacion := lt_identificacion(a);
            
               BEGIN
                  /*  SELECT   
                  replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')   
                  INTO lv_datos   
                  FROM  dual;*/
                  lv_datos := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(upper(lv_datos), '�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U'), '�', 'N');
               EXCEPTION
                  WHEN OTHERS THEN
                     lv_error := SQLERRM;
                     RETURN;
               END;
            
               --Escribo en el archivo y cuento lo que se generen con �xito o error   
               BEGIN
                  INSERT INTO co_union_clientes
                     (datos_registro, novedad, cuenta, identificacion)
                  VALUES
                     (lv_datos, lv_novedad, lv_cuenta, lv_identificacion);
                  ln_reg_exito := ln_reg_exito + 1;
               EXCEPTION
                  WHEN OTHERS THEN
                     ln_reg_error := ln_reg_error + 1;
                     lv_error := substr(SQLERRM, 1, 500);
                  
                     cop_detalle_bitacora(pv_cuenta => lv_cuenta, pv_descripcion => lv_error);
                  
               END;
            
               IF MOD(ln_reg_exito, gn_commit) = 0 THEN
                  COMMIT;
               END IF;
            
            END LOOP;
         
            cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'S', pv_tipo_proceso => 'U' ||
                                                       to_number(c.id_ciclo), pv_consulta => lv_sentencia, pv_error => lv_error);
         
         EXCEPTION
            WHEN le_error_rep THEN
               lv_error := 'No existen datos para el corte del ' ||
                           c.dia_ini_ciclo || ' en la estructura ' ||
                           lv_repcarcli_ciclo || ' y ' || lv_repdacre_ciclo;
               cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al generar datos para corte del ' ||
                                                         c.dia_ini_ciclo || ' :' ||
                                                         lv_error, pv_estado_reporte => 'E', --Generado con error   
                                      pv_tipo_proceso => 'U' ||
                                                          to_number(c.id_ciclo), pv_consulta => lv_sentencia, pv_error => lv_error);
            
            WHEN OTHERS THEN
               lv_error := SQLERRM;
               cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al generar datos para corte del ' ||
                                                         c.dia_ini_ciclo || ' :' ||
                                                         lv_error, pv_estado_reporte => 'E', --Generado con error   
                                      pv_tipo_proceso => 'U' ||
                                                          to_number(c.id_ciclo), pv_consulta => lv_sentencia, pv_error => lv_error);
            
         END;
      
         IF ln_cod_ciclo IS NOT NULL THEN
            cop_actualiza_estados_bitacora(pn_codigo => ln_cod_ciclo, pv_estado => 'S', pv_error => lv_error);
         END IF;
      END LOOP;
   
      --Inventario GYE   
      OPEN c_nombre_tabla(lv_parametro_gye);
      FETCH c_nombre_tabla
         INTO lc_nombre_tabla;
      lb_found := c_nombre_tabla%FOUND;
      CLOSE c_nombre_tabla;
   
      IF lb_found THEN
         lv_inven_gye := lc_nombre_tabla.tabla_destino;
         ln_codgye := lc_nombre_tabla.cod_bitacora_repdatacre;
      END IF;
   
      lb_found := FALSE;
   
      /*--Inventario UIO   
      OPEN c_nombre_tabla(lv_parametro_uio);
      FETCH c_nombre_tabla
        INTO Lc_nombre_tabla;
      lb_found := c_nombre_tabla%FOUND;
      CLOSE c_nombre_tabla;
      
      IF lb_found THEN
        lv_inven_uio := lc_nombre_tabla.tabla_destino;
        ln_coduio    := lc_nombre_tabla.cod_bitacora_repdatacre;
      END IF;*/
   
      ---------------------------   
      ln_reg_exito := 0;
      ln_reg_error := 0;
   
      BEGIN
         lv_sentencia := 'begin SELECT rpad(b.cuenta,18,'' '')|| ' || --Cuenta   
                         '  lpad(b.identIFicacion,13,''0'')|| ' || --Iden   
                         '  rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )|| ' ||
                         '  rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )|| ' ||
                         '  rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )|| ' ||
                         '  rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )|| ' ||
                         '  ''000000''|| ' ||
                         '  to_char(csactivated,''yyyymm'')|| ' ||
                         '  rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')|| ' ||
                         '  ''0000000000''|| ' || '  b.novedad|| ' ||
                         '  b.adjetivo|| ' ||
                         '  decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )|| ' ||
                         '  ''0000000000'' || ' || '  ''0000000000''|| ' || --Saldo Deuda   
                         '  lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| ' || --Saldo mora   
                         '  ''0000000000''|| ' || '  ''142''         || ' ||
                         '  b.calIFicacion|| ' ||
                         '  rpad(nvl(d.cccity,'' ''),20,'' '')|| ' || --Canton   
                         '  rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')|| ' ||
                         '  ''0000000000000000000000000000000000000000000000000000000000000000000000''|| ' ||
                         '   rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| ' ||
                         '   ''000000000'' || ' || --4.26 -- 4.27   
                         '   ''000''|| ' || --4.28   
                         '   lpad(b.cuentas_mora,3,''0'')|| ' ||
                         '   ''00000000000000000000000000000000000000000000''|| ' ||
                         '   ''1''|| ' || --4.34   
                         '  lpad(b.edad_mora,3,0)|| ' ||
                         '  ''00000000''|| ' ||
                         '  decode(b.reclamo,''S'',0,9)|| ' ||
                         '  ''000000000000000000000000000000'' as REGISTRO, ' ||
                         '   b.novedad as novedad, ' ||
                         '   b.cuenta as cuenta, ' ||
                         '   b.identIFicacion as identificacion  bulk collect into :1, :2, :3, :4' ||
                         ' FROM ' ||
                         ' cart_cuentas_dat@FINGYE_BUROCRED a, ' ||
                         ' sysadm.' || lv_inven_gye || ' b, ' ||
                         ' customer_all c, ' || ' ccontact_all d ' ||
                         ' WHERE b.cuenta=a.cuen_nume_cuen ' ||
                         ' AND   b.cuenta=c.custcode ' ||
                         ' AND   b.edad_mora is not null ' ||
                         ' AND   b.calIFicacion is not null' ||
                         ' AND  c.customer_id=d.customer_id ' ||
                         ' AND  d.ccbill=''X'';  end; ';
         --    dbms_output.put_line(' 2  '||lv_sentencia);   
         EXECUTE IMMEDIATE lv_sentencia
            USING OUT lt_datos, OUT lt_novedad, OUT lt_cuenta, OUT lt_identificacion;
      
         FOR a IN lt_datos.first .. lt_datos.last
         LOOP
         
            lv_datos := lt_datos(a);
            lv_novedad := lt_novedad(a);
            lv_cuenta := lt_cuenta(a);
            lv_identificacion := lt_identificacion(a);
            BEGIN
               /*  SELECT   
               replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')   
               INTO lv_datos   
               FROM  dual;*/
               lv_datos := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(upper(lv_datos), '�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U'), '�', 'N');
            EXCEPTION
               WHEN OTHERS THEN
                  lv_error := SQLERRM;
            END;
         
            --Escribo en el archivo y cuento lo que se generen con �xito o error   
            BEGIN
               INSERT INTO co_union_clientes
                  (datos_registro, novedad, cuenta, identificacion)
               VALUES
                  (lv_datos, lv_novedad, lv_cuenta, lv_identificacion);
               ln_reg_exito := ln_reg_exito + 1;
            
            EXCEPTION
               WHEN OTHERS THEN
                  ln_reg_error := ln_reg_error + 1;
                  lv_error := substr(SQLERRM, 1, 500);
                  cop_detalle_bitacora(pv_cuenta => lv_cuenta, pv_descripcion => lv_error);
               
            END;
         
            IF MOD(ln_reg_exito, gn_commit) = 0 THEN
               COMMIT;
            END IF;
         
         END LOOP;
      
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'S', pv_tipo_proceso => 'UG', --U3   
                                pv_consulta => lv_sentencia, pv_error => lv_error);
      
      EXCEPTION
         WHEN OTHERS THEN
            lv_error := SQLERRM;
            cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al procesar datos de los clientes GYE :' ||
                                                      lv_error, pv_estado_reporte => 'E', --Generado con error   
                                   pv_tipo_proceso => 'UG', --U3   
                                   pv_consulta => lv_sentencia, pv_error => lv_error);
         
      END;
   
      ---------------------------   
      ln_reg_exito := 0;
      ln_reg_error := 0;
   
      /*Begin
        lv_sentencia := 'begin SELECT rpad(b.cuenta,18,'' '')|| ' || --Cuenta   
                        '  lpad(b.identIFicacion,13,''0'')|| ' || --Iden   
                        '  rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )|| ' ||
                        '  rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )|| ' ||
                        '  rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )|| ' ||
                        '  rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )|| ' ||
                        '  ''000000''|| ' ||
                        '  to_char(csactivated,''yyyymm'')|| ' ||
                        '  rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')|| ' ||
                        '  ''0000000000''|| ' || '  b.novedad|| ' ||
                        '  b.adjetivo|| ' ||
                        '  decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )|| ' ||
                        '  ''0000000000'' || ' || '  ''0000000000''|| ' || --Saldo Deuda   
                        '  lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| ' || --Saldo mora   
                        '  ''0000000000''|| ' || '  ''142''         || ' ||
                        '  b.calIFicacion|| ' ||
                        '  rpad(nvl(d.cccity,'' ''),20,'' '')|| ' || --Canton   
                        '  rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')|| ' ||
                        '  ''0000000000000000000000000000000000000000000000000000000000000000000000''|| ' ||
                        '   rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| ' ||
                        '   ''000000000'' || ' || --4.26 -- 4.27   
                        '   ''000''|| ' || --4.28   
                        '   lpad(b.cuentas_mora,3,''0'')|| ' ||
                        '   ''00000000000000000000000000000000000000000000''|| ' ||
                        '   ''1''|| ' || --4.34   
                        '  lpad(b.edad_mora,3,0)|| ' || '  ''00000000''|| ' ||
                        '  decode(b.reclamo,''S'',0,9)|| ' ||
                        '  ''000000000000000000000000000000'' as REGISTRO, ' ||
                        '   b.novedad as novedad, ' ||
                        '   b.cuenta as cuenta, ' ||
                        '   b.identIFicacion as identificacion    bulk collect into :1, :2, :3, :4 ' ||
                        ' FROM ' || ' cart_cuentas_dat@B_BUROCRED a, ' ||
                        ' sysadm.' || lv_inven_uio || ' b, ' ||
                        ' customer_all c, ' || ' ccontact_all d ' ||
                        ' WHERE b.cuenta=a.cuen_nume_cuen ' ||
                        ' AND   b.cuenta=c.custcode ' ||
                        ' AND  c.customer_id=d.customer_id ' ||
                        ' AND  d.ccbill=''X'';  end; ';
      
        -- dbms_output.put_line(' 3  '||lv_sentencia);   
        EXECUTE IMMEDIATE lv_sentencia
          using out lt_datos, out lt_novedad, out lt_cuenta, OUT lt_identificacion;
      
        FOR a IN lt_datos.first .. lt_datos.last LOOP
          lv_datos              := lt_datos(a);
          lv_novedad            := lt_novedad(a);
          lv_cuenta             := lt_cuenta(a);
          lv_identificacion     := lt_identificacion(a);
        
          BEGIN
            \*   SELECT   
            replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')   
            INTO lv_datos   
            FROM  dual;   
            *\
            lv_datos := replace(replace(replace(replace(replace(replace(upper(lv_datos),
                                                                        '�',
                                                                        'A'),
                                                                '�',
                                                                'E'),
                                                        '�',
                                                        'I'),
                                                '�',
                                                'O'),
                                        '�',
                                        'U'),
                                '�',
                                'N');
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := sqlerrm;
          END;
        
          --Escribo en el archivo y cuento lo que se generen con �xito o error   
          BEGIN
            INSERT INTO CO_UNION_CLIENTES
              (datos_registro, novedad, CUENTA, identificacion )
            VALUES
              (LV_DATOS, LV_NOVEDAD, lv_cuenta, lv_identificacion);
            ln_reg_exito := ln_reg_exito + 1;
          
          EXCEPTION
            WHEN OTHERS THEN
              ln_reg_error := ln_reg_error + 1;
              lv_error     := substr(sqlerrm, 1, 500);
              cop_detalle_bitacora(pv_cuenta      => lv_cuenta,
                                   pv_descripcion => lv_error);
            
          END;
        
          IF mod(ln_reg_exito, gn_commit) = 0 THEN
            commit;
          END IF;
        
        END loop;
      
        cop_bitacora_ejecucion(Pv_nombreTabla    => null,
                               pn_reg_exito      => ln_reg_exito,
                               pn_reg_error      => ln_reg_error,
                               pv_descripcion    => lv_error,
                               pv_estado_reporte => 'S',
                               pv_tipo_proceso   => 'UQ', --U4   
                               pv_consulta       => lv_sentencia,
                               pv_error          => lv_error);
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := sqlerrm;
          cop_bitacora_ejecucion(pv_nombreTabla    => null,
                                 pn_reg_exito      => ln_reg_exito,
                                 pn_reg_error      => ln_reg_error,
                                 pv_descripcion    => 'Error al procesar datos de los clientes UIO :' ||
                                                      lv_error,
                                 pv_estado_reporte => 'E', --Generado con error   
                                 pv_tipo_proceso   => 'UQ', --U4   
                                 pv_consulta       => lv_sentencia,
                                 pv_error          => lv_error);
        
      END;*/
   
      IF ln_codgye IS NOT NULL THEN
         cop_actualiza_estados_bitacora(pn_codigo => ln_codgye, pv_estado => 'S', pv_error => lv_error);
      END IF;
   
      IF ln_coduio IS NOT NULL THEN
         cop_actualiza_estados_bitacora(pn_codigo => ln_coduio, pv_estado => 'S', pv_error => lv_error);
      END IF;
      COMMIT;
   
      --4502 ini sbr
      --Esto se va a ejecutar siempre y cuando 
      --exista la bandera se ejecutaran los cambios
      --Bandera de scp
   
      --SCP
      scp.sck_api.scp_parametros_procesos_lee('PAR_EJEC_REP_BURO', --nombre de parametro
                                              lv_proceso_par_scp, lv_valor_par_scp, lv_descripcion_par_scp, ln_error_scp, lv_error_scp);
      --SCP
      IF (nvl(lv_valor_par_scp, 'N') = 'S') THEN
      
         --SCP
         scp.sck_api.scp_parametros_procesos_lee('PAR_CANT_COMMIT', --nombre de parametro
                                                 lv_proceso_par_scp, lv_valor_par_scp, lv_descripcion_par_scp, ln_error_scp, lv_error_scp);
      
         IF (lv_valor_par_scp IS NULL) THEN
            ln_cant_commit := 1000;
         ELSE
            ln_cant_commit := to_number(lv_valor_par_scp);
         END IF;
         --SCP
      
         BEGIN
         
            --SCP
            scp.sck_api.scp_bitacora_procesos_ins(lv_proceso_par_scp, --NOMBRE DE PROCESO
                                                  lv_referencia_scp, --CUALQUIER COSA
                                                  NULL, NULL, NULL, NULL, NULL, --numero de items a procesar
                                                  lv_unidad_registro_scp, --que cosa vas a procesar
                                                  ln_id_bitacora_scp, --devuelve un id
                                                  ln_error_scp, lv_error_scp);
         
            --SCP
         
            --Llamado a la ejecucion del buro de credito
            BEGIN
               lb_flag_eje := TRUE;
               /*[5488] SUD Glenn Avil�s V*/
               lv_sentencia := ' begin select co.numero_obligacion|| ' ||
                               ' co.numero_identificacion|| ' ||
                               ' co.nombre_completo|| ' ||
                               ' co.fecha_nacimiento|| ' ||
                               ' co.fecha_apertura|| ' ||
                               ' co.fecha_vencimiento|| ' ||
                               ' co.valor_cuota_mensual|| ' ||
                               ' co.novedad|| ' || ' co.adjetivo|| ' ||
                               ' co.tipo_identificacion|| ' ||
                               ' co.valor_inicial|| ' ||
                               ' co.valor_saldo_deuda|| ' ||
                               ' co.valor_saldo_mora|| ' ||
                               ' co.valor_disponible|| ' ||
                               ' co.tipo_moneda|| ' ||
                               ' co.tipo_obligacion|| ' ||
                               ' co.tipo_garantia|| ' ||
                               ' co.calificacion|| ' ||
                               ' co.ciudad_residencia|| ' ||
                               ' co.direccion_residencia|| ' ||
                               ' co.telefono_residencia|| ' ||
                               ' co.ciudad_laboral|| ' ||
                               ' co.telefono_laboral|| ' ||
                               ' co.ciudad_correspondencia|| ' ||
                               ' co.direccion_correspondencia|| ' ||
                               ' co.ciiu|| ' || ' co.total_cuotas|| ' ||
                               ' co.cuotas_canceladas|| ' ||
                               ' co.cuotas_mora|| ' ||
                               ' co.fecha_ultimo_pago|| ' ||
                               ' co.oficina_radicacion|| ' ||
                               ' co.ciudad_radicacion|| ' ||
                               ' co.forma_pago|| ' ||
                               ' co.periodicidad_pago|| ' ||
                               ' co.edad_mora|| ' ||
                               ' co.fecha_actualizacion|| ' ||
                               ' co.reclamo|| ' || ' co.garante|| ' ||
                               ' co.filler as registro,' ||
                               ' co.novedad as novedad, ' ||
                               ' co.cuenta as cuenta, ' ||
                               ' co.numero_identificacion  bulk collect into :1, :2, :3, :4' ||
                               ' from ' ||
                               ' co_financiamiento_buro_datos co ; end; ';
            
               --    dbms_output.put_line(' 2  '||lv_sentencia);
               EXECUTE IMMEDIATE lv_sentencia
                  USING OUT lt_datos, OUT lt_novedad, OUT lt_cuenta, OUT lt_identificacion;
            EXCEPTION
               WHEN OTHERS THEN
                  lb_flag_eje := FALSE;
                  scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'ERROR OBTENER LA INFORMACION DE LA TABLA co_financiamiento_buro_datos  ', pv_mensaje_tecnico => SQLERRM, pv_mensaje_accion => 'CONSIDERAR UN POSIBLE NUEVO LANZAMIENTO DEL PROCESO', pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                        pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
               
                  scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora_scp, pn_error => ln_error_scp, pv_error => lv_error_scp);
            END;
            IF (lb_flag_eje) THEN
            
               ln_reg_exito_buro := 0;
               ln_no_commit := 0;
               ln_errores_reg := 0;
               ln_total_reg_buro := 0;
               ln_total_reg_buro := lt_datos.count();
               FOR a IN lt_datos.first .. lt_datos.last
               LOOP
               
                  lv_datos := lt_datos(a);
                  lv_novedad := lt_novedad(a);
                  lv_cuenta := lt_cuenta(a);
                  lv_identificacion := lt_identificacion(a);
                  BEGIN
                     --SCP
                     BEGIN
                        /*  SELECT
                        replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')
                        INTO lv_datos
                        FROM  dual;*/
                        lv_datos := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(upper(lv_datos), '�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U'), '�', 'N');
                     EXCEPTION
                        WHEN OTHERS THEN
                           lv_error := 'Error al formatear los datos a enviar de la cuenta ' ||
                                       lv_cuenta || ', ' || SQLERRM;
                           RAISE le_error_format;
                     END;
                     --Escribo en el archivo y cuento lo que se generen con �xito o error
                     BEGIN
                        INSERT INTO co_union_clientes
                           (datos_registro, novedad, cuenta, identificacion)
                        VALUES
                           (lv_datos, lv_novedad, lv_cuenta, lv_identificacion);
                        ln_reg_exito_buro := ln_reg_exito_buro + 1;
                     
                     EXCEPTION
                        WHEN OTHERS THEN
                           lv_error := 'Error al insertar los datos de la cuenta ' ||
                                       lv_cuenta ||
                                       ', en la tabla CO_UNION_CLIENTES ' ||
                                       SQLERRM;
                           RAISE le_error_insert;
                     END;
                  
                  EXCEPTION
                     WHEN le_error_format THEN
                        --contar los q tienen error
                        ln_errores_reg := ln_errores_reg + 1;
                        scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'ERROR AL PROCESAR EL RESUMEN DE LOS DATOS EN LA TABLA CO_UNION_CLIENTES  ', pv_mensaje_tecnico => lv_error, pv_mensaje_accion => 'CONSIDERAR UN POSIBLE REPROCESO DE LOS NO CONSIDERADOS', pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                              pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
                     WHEN le_error_insert THEN
                        --contar los q tienen error
                        ln_errores_reg := ln_errores_reg + 1;
                        scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'ERROR AL INSERTAR EL RESUMEN DE LOS DATOS EN LA TABLA CO_UNION_CLIENTES  ', pv_mensaje_tecnico => lv_error, pv_mensaje_accion => 'CONSIDERAR UN POSIBLE REPROCESO DE LOS NO CONSIDERADOS', pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                              pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
                  END; --SCP
                  ln_no_commit := ln_no_commit + 1;
               
                  IF (ln_no_commit > ln_cant_commit) THEN
                     COMMIT;
                     ln_no_commit := 0;
                  END IF;
               END LOOP;
               --puede ser que se quede algo sin commit
               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'RESUMEN DE RESULTADOS DEL PROCESO', pv_mensaje_tecnico => 'MENSAJE INFORMATIVO', pv_mensaje_accion => 'REVISAR LA CANTIDAD DE PROCESADOS Y LOS QUE NO LO FUERON', pn_nivel_error => 0, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                     pv_cod_aux_1 => '' ||
                                                                      ln_total_reg_buro, pv_cod_aux_2 => '' ||
                                                                      ln_reg_exito, pv_cod_aux_3 => '' ||
                                                                      ln_errores_reg, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
            
               scp.sck_api.scp_bitacora_procesos_act(pn_id_bitacora => ln_id_bitacora_scp, pn_registros_procesados => ln_reg_exito, pn_registros_error => ln_errores_reg, pn_error => ln_error_scp, pv_error => lv_error_scp);
            
               scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora_scp, pn_error => ln_error_scp, pv_error => lv_error_scp);
            
               COMMIT;
            END IF;
         
         EXCEPTION
            WHEN OTHERS THEN
               NULL; --ESTO ES FUERA DEL AMBITO DE SCP
         END;
         --4502 fin sbr 
      END IF;
      --BSE CLS 4502
      COMMIT; ---Esto en el caso de que hayan quedado registros sin commit
      --BSE CLS 4502
   
   EXCEPTION
      WHEN le_error THEN
         lv_error := 'Error al truncar co_union_clientes favor truncarla manualmente :' ||
                     SQLERRM;
         pv_error := lv_error;
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'U1', pv_consulta => lv_sentencia, pv_error => lv_error);
      
      WHEN OTHERS THEN
         lv_error := SQLERRM;
         cop_bitacora_ejecucion(pv_nombretabla => NULL, pn_reg_exito => ln_reg_exito, pn_reg_error => ln_reg_error, pv_descripcion => 'Error al generar union de proceso :' ||
                                                   lv_error, pv_estado_reporte => 'E', --Generado con error   
                                pv_tipo_proceso => 'U', pv_consulta => lv_generaarchivo, pv_error => lv_error);
      
   END cop_union_clientes;
   ---------   
   PROCEDURE cop_detalle_bitacora(pv_cuenta      VARCHAR2,
                                  pv_descripcion VARCHAR2) IS
   
      PRAGMA AUTONOMOUS_TRANSACTION;
   
   BEGIN
      INSERT INTO co_detalle_bitacora_repdatacre
         (cuenta, descripcion, fecha)
      VALUES
         (pv_cuenta, pv_descripcion, SYSDATE);
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
      
   END cop_detalle_bitacora;

   --INICIO    PROYECTO[3509] REPORTE DATACREDITO 18/07/2008    SE AUMETARON ESTAS 2 FUNCIONES Y TRES PROCEDIMIENTOS   
   --Calificaci�n seg�n el riesgo del cliente entre un rango de la edad de la mora   

   FUNCTION cof_califica_cuenta(pv_campo VARCHAR2) RETURN VARCHAR2 IS
   
      CURSOR c_califica_cuenta(cv_edad_mora VARCHAR2) IS
         SELECT calificacion
         FROM   co_calificacion_repdatacre
         WHERE  to_number(cv_edad_mora) <= rango_max
                AND to_number(cv_edad_mora) >= rango_min;
   
      lv_calificacion co_calificacion_repdatacre.calificacion%TYPE;
   
   BEGIN
   
      OPEN c_califica_cuenta(pv_campo);
      FETCH c_califica_cuenta
         INTO lv_calificacion;
      CLOSE c_califica_cuenta;
      RETURN lv_calificacion;
   
   END cof_califica_cuenta;

   FUNCTION cof_reclamo(pv_cuenta VARCHAR2) RETURN VARCHAR2 IS
      --Consulto si el cliente permite compartir su informaci�n con datacredito   
   
      CURSOR c_reclamo(lv_cuenta    VARCHAR2,
                       cv_estado    VARCHAR2,
                       cn_idfeature NUMBER,
                       cn_valor     NUMBER) IS
      
         SELECT 'S'
         FROM   co_permiso_datacredito a
         WHERE  a.id_cuenta = lv_cuenta;
   
      lv_reclamo VARCHAR2(1);
      lb_found   BOOLEAN;
   
   BEGIN
      --Encuentro el tipo de persona   
      OPEN c_reclamo(pv_cuenta, gv_estado, gn_idfeature, gn_valor);
      FETCH c_reclamo
         INTO lv_reclamo;
      lb_found := c_reclamo%FOUND;
      CLOSE c_reclamo;
   
      IF lb_found THEN
         lv_reclamo := 'S'; --Autorizado por el cliente   
      ELSE
         lv_reclamo := 'N'; --No autoriza el cliente   
      END IF;
   
      RETURN lv_reclamo;
   
   EXCEPTION
      WHEN OTHERS THEN
         lv_reclamo := 'N';
   END cof_reclamo;

   PROCEDURE cop_existe_tabla(pv_tabla IN VARCHAR2,
                              pv_error OUT VARCHAR2) IS
   
      --Comprueba si existe la tabla   
      CURSOR cg_existe_tabla(lv_nombre_tabla VARCHAR2) IS
         SELECT 'X' existe
         FROM   all_tables t
         WHERE  t.table_name = lv_nombre_tabla;
   
      gr_existe_tabla cg_existe_tabla%ROWTYPE;
      lb_found        BOOLEAN;
      lv_truncate     VARCHAR2(100);
      le_error EXCEPTION;
      ln_codigo_error NUMBER;
      lv_programa     VARCHAR2(60) := 'COP_EXISTE_TABLA';
   BEGIN
      pv_error := NULL;
   
      OPEN cg_existe_tabla(pv_tabla);
      FETCH cg_existe_tabla
         INTO gr_existe_tabla;
      lb_found := cg_existe_tabla%FOUND;
      CLOSE cg_existe_tabla;
   
      --Si encuentra la tabla borrra los datos o sino la crea   
      IF lb_found THEN
         BEGIN
            lv_truncate := 'truncate table ' || pv_tabla;
            EXECUTE IMMEDIATE lv_truncate;
         EXCEPTION
            WHEN OTHERS THEN
               pv_error := 'Error al truncar la tabla ' || SQLERRM;
               --raise le_error;   
               RETURN;
         END;
         --Se actualiza los que tienen el mismo nombre para no tomarlos en cuenta   
         BEGIN
            UPDATE co_bitacora_repdatacre
            SET    estado_reporte = 'E'
            WHERE  tabla_destino LIKE '%' || pv_tabla || '%';
         EXCEPTION
            WHEN OTHERS THEN
               pv_error := 'Error actualizar co_bitacora_repdatacre ' ||
                           SQLERRM;
               RETURN;
            
         END;
      
      ELSE
         -- CREA LA TABLA   
         cop_crea_tabla(pv_tabla, 'sysadm', ln_codigo_error, pv_error);
      END IF;
   
      IF pv_error IS NOT NULL THEN
         RETURN;
      END IF;
   
      -- DESHABILITA EL INDICE   
      cop_deshabilita_indice(pv_tabla, pv_error);
   
      IF pv_error IS NOT NULL THEN
         RETURN;
      END IF;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := 'Error general en : ' || lv_programa || ' - ' ||
                     SQLERRM;
   END cop_existe_tabla;

   -- HABILITA EL INDICE LUEGO DE HABER TERMINADO CON TODO EL PROCESO PARA QUE SIGUA SU FLUJO NORMAL   
   PROCEDURE cop_habilita_indice(pv_nombre_tabla IN VARCHAR2,
                                 pv_error        OUT VARCHAR2) IS
   
      lv_sentecia VARCHAR2(1000);
   
      le_error EXCEPTION;
      lv_programa VARCHAR2(60) := 'COP_HABILITA_INDICE';
   
   BEGIN
      pv_error := NULL;
      lv_sentecia := 'create index idx_' || pv_nombre_tabla || ' on ' ||
                     pv_nombre_tabla || '(CUENTA)' ||
                     ' tablespace  REP_CRED_IDX ' || ' pctfree 10 ' ||
                     ' initrans 2 ' || ' maxtrans 255 ' || ' storage ' ||
                     ' ( initial 10M ' || '  next 5M ' || '  minextents 1 ' ||
                     '  maxextents unlimited ' || '  pctincrease 0 )';
   
      EXECUTE IMMEDIATE lv_sentecia;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := 'Error general  al crear el indice: idx_' ||
                     pv_nombre_tabla || ' en el programa ' || lv_programa;
   END cop_habilita_indice;

   -- DESHABILITA EL INDICE ANTES DE REALIZAR EL PROCESO PARA HACER MAS RAPIDO LA EJECUCION   
   PROCEDURE cop_deshabilita_indice(pv_nombre_tabla IN VARCHAR2,
                                    pv_error        OUT VARCHAR2) IS
   
      --ANTES DESHABILITAR EL INDICE CONSULTO SI YA ESTA DESHABILITADO   
      CURSOR cl_validaindice(cv_indice VARCHAR2) IS
         SELECT 'S'
         FROM   all_indexes i
         WHERE  i.index_name = cv_indice;
   
      lv_encontro VARCHAR2(1) := 'N';
      lv_programa VARCHAR2(60) := 'COP_DESHABILITA_INDICE';
      lv_sentecia VARCHAR2(1000);
   
   BEGIN
      OPEN cl_validaindice('IDX_' || pv_nombre_tabla);
      FETCH cl_validaindice
         INTO lv_encontro;
      IF cl_validaindice%NOTFOUND THEN
         lv_encontro := 'N';
      END IF;
      CLOSE cl_validaindice;
   
      IF lv_encontro = 'S' THEN
         lv_sentecia := 'DROP INDEX IDX_' || pv_nombre_tabla;
         EXECUTE IMMEDIATE lv_sentecia;
      END IF;
   
   EXCEPTION
      WHEN OTHERS THEN
         pv_error := 'Error general  al eliminar  el indice: idx_' ||
                     pv_nombre_tabla || ' en el programa ' || lv_programa;
   END cop_deshabilita_indice;
   --FIN      PROYECTO[3509] REPORTE DATACREDITO  18/07/2008   
   PROCEDURE cop_genera_datos_buro(pv_fecha_corte VARCHAR2 DEFAULT NULL) IS
   
      lv_error       VARCHAR2(2000);
      ld_fecha_corte DATE;
      lv_sentencia   VARCHAR2(20000) := NULL;
      le_error EXCEPTION;
      lv_fecha_aux VARCHAR2(10);
      --SCP
      lv_proceso_par_scp     VARCHAR2(100) := 'COK_DAT_BURO';
      lv_valor_par_scp       VARCHAR2(10);
      lv_descripcion_par_scp VARCHAR2(100);
      ln_error_scp           NUMBER;
      lv_error_scp           VARCHAR2(1000);
      lv_referencia_scp      VARCHAR2(1000) := 'Ejecucion De Proceso Genera Datos Buro En BSCS  - ' ||
                                               SYSDATE || ' - ' || USER;
      lv_unidad_registro_scp VARCHAR2(100) := 'LLAMADO_PROCESO_AXIS';
      ln_id_bitacora_scp     NUMBER;
      ln_cantidad_commit     NUMBER;
      ln_cantidad_registros  NUMBER;
      --SCP
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --SCP
      scp.sck_api.scp_bitacora_procesos_ins(lv_proceso_par_scp, --NOMBRE DE PROCESO
                                            lv_referencia_scp, --CUALQUIER COSA
                                            NULL, NULL, NULL, NULL, NULL, --numero de items a procesar
                                            lv_unidad_registro_scp, --que cosa vas a procesar
                                            ln_id_bitacora_scp, --devuelve un id
                                            ln_error_scp, lv_error_scp);
   
      --Interpretacion de feha eniadaq
      IF (pv_fecha_corte IS NULL) THEN
         ld_fecha_corte := trunc(SYSDATE);
         ld_fecha_corte := add_months(ld_fecha_corte, -1);
         lv_fecha_aux := to_char(ld_fecha_corte, 'mm/yyyy');
         lv_fecha_aux := '23/' || lv_fecha_aux;
         ld_fecha_corte := to_date(lv_fecha_aux, 'dd/mm/yyyy');
      ELSE
      
         BEGIN
            ld_fecha_corte := to_date(pv_fecha_corte, 'dd/mm/yyyy');
         EXCEPTION
            WHEN OTHERS THEN
               scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'Error general no ejecucion del proceso', pv_mensaje_tecnico => 'Error ' ||
                                                                            SQLERRM, pv_mensaje_accion => 'Verificar la fecha de corte esta en formato erroneo', pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                     pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
            
               scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora_scp, pn_error => ln_error_scp, pv_error => lv_error_scp);
            
               COMMIT;
               RETURN;
         END;
      
      END IF;
   
      BEGIN
      
         --SCP
         scp.sck_api.scp_parametros_procesos_lee('PAR_DET_PROC_BURO', --nombre de parametro
                                                 lv_proceso_par_scp, lv_valor_par_scp, lv_descripcion_par_scp, ln_error_scp, lv_error_scp);
      
         IF (nvl(lv_valor_par_scp, 'N') = 'S') THEN
            lv_error := 'Proceso detenido desde el modulo de SCP, verificar el valor de la bandera o si existe creada';
            RAISE le_error;
         END IF;
         --SCP
         BEGIN
            lv_sentencia := 'truncate table CO_FINANCIAMIENTO_BURO_DATOS';
            EXECUTE IMMEDIATE lv_sentencia;
            --EJECUTA_SENTENCIA(lv_sentencia,lv_MensErr);
         EXCEPTION
            WHEN OTHERS THEN
               lv_error := 'Error al truncar la tabla CO_FINANCIAMIENTO_BURO_DATOS ' ||
                           SQLERRM;
               RAISE le_error;
         END;
         BEGIN
            porta.cok_pro_financiamiento_buro.cop_genera_datos@axis(ld_fecha_corte, lv_error);
         EXCEPTION
            WHEN OTHERS THEN
               lv_error := ' general ' || SQLERRM;
               RAISE le_error;
         END;
         ---Hay que ver que hacemos con el error
         IF (lv_error IS NOT NULL) THEN
            lv_error := 'Error al llamar al proceso COK_PRO_FINANCIAMIENTO_BURO.COP_GENERA_DATOS@AXIS ' ||
                        lv_error;
            RAISE le_error;
         END IF;
      
         BEGIN
            INSERT INTO co_financiamiento_buro_datos nologging
               SELECT *
               FROM   porta.co_financiamiento_buro_datos@axis;
            COMMIT;
         EXCEPTION
            WHEN OTHERS THEN
               lv_error := 'Error en el traslado de datos de la tabla CO_FINANCIAMIENTO_BURO_DATOS@AXIS a bscs ' ||
                           SQLERRM;
               RAISE le_error;
         END;
      
         BEGIN
            SELECT COUNT(*)
            INTO   ln_cantidad_registros
            FROM   co_financiamiento_buro_datos;
         EXCEPTION
            WHEN OTHERS THEN
               ln_cantidad_registros := 0;
         END;
         scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'INFORMACION GENERAL EL PROCESO ' ||
                                                                         ld_fecha_corte, pv_mensaje_tecnico => 'INFORMACION GENERAL EL PROCESO ' ||
                                                                      ld_fecha_corte, pv_mensaje_accion => 'RESULTADOS TOTALES', pn_nivel_error => 0, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                               pv_cod_aux_1 => '' ||
                                                                ln_cantidad_registros, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
      
      EXCEPTION
         WHEN le_error THEN
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'ERROR AL PROCESAR EN LA TABLA CO_FINANCIAMIENTO_BURO_DATOS  ' ||
                                                                            ld_fecha_corte,
                                                  
                                                  pv_mensaje_tecnico => lv_error, pv_mensaje_accion => 'CONSIDERAR UN POSIBLE REPROCESO DE LOS NO ENVIADOS ' ||
                                                                        ld_fecha_corte, pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                  pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
         
         WHEN OTHERS THEN
            scp.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora_scp, pv_mensaje_aplicacion => 'ERROR AL PROCESAR EL FINANCIAMIENTO CO_FINANCIAMIENTO_BURO_DATOS ' ||
                                                                            ld_fecha_corte,
                                                  
                                                  pv_mensaje_tecnico => 'ERROR NO CONTROLADO ' ||
                                                                         SQLERRM, pv_mensaje_accion => 'CONSIDERAR UN POSIBLE REPROCESO DE LOS NO ENVIADOS  ' ||
                                                                        ld_fecha_corte, pn_nivel_error => 3, --ESTO DEBEMOS REVISAR NO RECUERDO CUAL ERA EL NIVEL
                                                  pv_cod_aux_1 => NULL, pv_cod_aux_2 => NULL, pv_cod_aux_3 => NULL, pn_cod_aux_4 => NULL, pn_cod_aux_5 => NULL, pv_notifica => NULL, pn_error => ln_error_scp, pv_error => lv_error_scp);
            --detalles errores
         --Bitacorizar
      END;
   
      scp.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora_scp, pn_error => ln_error_scp, pv_error => lv_error_scp);
   
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
   END; --



BEGIN
   OPEN gc_parametros(gv_parametro);
   FETCH gc_parametros
      INTO gn_commit;
   CLOSE gc_parametros;

END cok_reporte_datacredito;
/
