create or replace package ALARMAS_BSCS is
 PROCEDURE EJECUTA_ALARMAS;
 PROCEDURE ALARMA_COLAS_BSCS;
 PROCEDURE SEND_EMAIL (pv_msg in varchar2,pv_subject in varchar2,grupo in number);
end ALARMAS_BSCS;
/
create or replace package body ALARMAS_BSCS is
--========================================================================--
-- Sistema de Alaramas SMS y MAIL
--================================================================================================--
  PROCEDURE EJECUTA_ALARMAS is
  begin
   ALARMA_COLAS_BSCS();
  END;
  
  PROCEDURE ALARMA_COLAS_BSCS IS
   cursor colas is
   select * from alarm_colas
   where estado = 'A';
   
   cursor telefonos is
   select TELEFONO from ALARMAS_TELEFONOS;

   li_cursor         integer;
   li_cursor1        integer;
   lv_sentencia      varchar2(400);
   lv_msg            varchar2(2500);
   ln_cola          number:=0;
   lb_cola          boolean:=false;
   hora             varchar2(20);
  begin
   for i in colas loop
   --
     lb_cola:=false;
     lv_sentencia :='select /*+ rule +*/count(1) from '||i.nombre_tabla;
     li_cursor := DBMS_SQL.OPEN_CURSOR;
     DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
     DBMS_SQL.DEFINE_COLUMN(li_cursor,1,ln_cola);
     li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
    LOOP
      EXIT WHEN DBMS_SQL.FETCH_ROWS(li_cursor) = 0;
      DBMS_SQL.COLUMN_VALUE(li_cursor, 1, ln_cola);
    END LOOP;
     
     hora:=to_char(sysdate,'hh24:mi:ss');
     update alarm_colas
     set last_count=ln_cola,LAST_DATE=sysdate
     where nombre_tabla=i.nombre_tabla;
     commit;
     
     if ln_cola > i.max_tx_cola /* and  hora > to_char('07:30:00')*/ and i.frecuencia_ejecucioin='T' then
         lv_msg:='ALARMA: BSCS';
         lv_msg:=lv_msg||' Tabla '||upper(i.nombre_tabla)||' tiene '||ln_cola||' Transacciones en cola'||chr(13)||chr(13)||i.mensaje||chr(13);  
         ALARMAS_BSCS.SEND_EMAIL (lv_msg,'Problemas de encolamiento BSCS -> '||i.nombre_tabla,i.grupo);
         for fono in telefonos 
         loop 
         porta.swk_sms.send@axis('sms',fono.telefono,lv_msg,lv_msg);
         COMMIT;
         end loop; 
         
     end if; 
     
      if ln_cola > i.max_tx_cola /* and  hora > to_char('07:30:00')*/ and i.frecuencia_ejecucioin='P' then
         if i.hora_ejecucion=to_char(sysdate,'hh24:mi') then
            lv_msg:='ALARMA: BSCS';
            lv_msg:=lv_msg||' Tabla '||upper(i.nombre_tabla)||' tiene '||ln_cola||' Transacciones en cola'||chr(13)||chr(13)||i.mensaje||chr(13);  
            ALARMAS_BSCS.SEND_EMAIL (lv_msg,'Problemas de encolamiento BSCS -> '||i.nombre_tabla,i.grupo);
            for fono in telefonos 
            loop 
            porta.swk_sms.send@axis('sms',fono.telefono,lv_msg,lv_msg);
            end loop; 
            COMMIT;
         end if;

     end if;    
    --
    DBMS_SQL.CLOSE_CURSOR(li_cursor);
   -- 
   end loop;
   
     
     
  EXCEPTION
  when others then
     dbms_output.put_line(sqlerrm);  
  END ALARMA_COLAS_BSCS;

  --================================================================================================--
  
 PROCEDURE send_email (pv_msg in varchar2,pv_subject in varchar2, grupo in number) IS
  
    cursor email_alarma (ln_grupo number) is
    select to1,cc,cco from alarm_email
    where grupo = ln_grupo;
    
    lv_msg      varchar2(1000);
    lv_subject       varchar2(1000);
    lv_firma         varchar2(150):=chr(13)||'BSCS'||chr(13)||'130.2.18.14'||chr(13)||'@bscs'||chr(13)||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');
  begin

   lv_subject:=pv_subject;
   lv_msg:=pv_msg||chr(13)||chr(13)||chr(13)||lv_firma;
   --
   
   for j in email_alarma(grupo) loop 
     --
    porta.send_mail.mail@axis(from_name => 'BSCS@conecel.com',
                     to_name => j.to1,
                     cc => j.cc,
                     cco => j.cco,
                     subject => lv_subject,
                     message => lv_msg);
    end loop; 
 
 END send_email;
 
end ALARMAS_BSCS;
/
