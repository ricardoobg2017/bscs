CREATE OR REPLACE package COK_CARTERA_CLIENTES_CS is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================

  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date);
  FUNCTION SET_MORA(pdFecha in date) RETURN number;
  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2) RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2) RETURN VARCHAR2;
  FUNCTION CREA_TABLA (pdFechaPeriodo in  date,
                         pv_error       out varchar2 ) RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date);
end;
/
CREATE OR REPLACE package body COK_CARTERA_CLIENTES_CS is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  -- Objetivo: Crea la tabla de reporte de 
  --           detalle de clientes del Mes de septiembre
  --           menos los servicios que fueron ajustados
  --           por problemas en la facturaci�n
  --===================================================

    PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(10000);
    lvServ2            varchar2(5000);
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;

    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
    select distinct tipo, nombre2
    from cob_servicios
    where cargo2 = 0;

    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
    select distinct tipo, nombre2
    from cob_servicios
    where cargo2 = 1;

    BEGIN
         lvServ1:='';
         for s in c_servicios1 loop
             lvServ1 := lvServ1||s.nombre2||'+';
         end loop;
         lvServ1 := substr(lvServ1,1,length(lvServ1)-1);

         lvServ2:='';
         for t in c_servicios2 loop
             lvServ2 := lvServ2||t.nombre2||'+';
         end loop;
         lvServ2 := substr(lvServ2,1,length(lvServ2)-1);

         lvSentencia := 'update CO_REPCARCLI_CS_'||to_char(pdFecha,'ddMMyyyy')||
                       ' set TOTAL1 = '||lvServ1||','||
                       ' TOTAL2 = '||lvServ2||','||
                       ' TOTAL3 = credito_promocion,'||
                       ' TOTAL_FACT = '||lvServ1||'+'||lvServ2||'+saldoanter';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         commit;

    EXCEPTION
    when others then
         pvError := sqlerrm;
    END;

    -------------------------------------------
    --             SET_SERVICIOS
    -------------------------------------------
    PROCEDURE SET_SERVICIOS(pdFecha in date) IS

    lII              NUMBER;
    lvSentencia      VARCHAR2(1000);
    lvMensErr        VARCHAR2(1000);

    cursor cur_servicios is
    --Agosto
    --select * from ope_obt_cre_deb_per;-- where nombre2 = 'transferencia_llamada_gsm';
    --Septiembre
    select * from co_cre_deb_24092003;

    BEGIN
        lII:=0;
        for i in cur_servicios loop
            lvSentencia := 'update CO_REPCARCLI_CS_'||to_char(pdFecha,'ddMMyyyy')||
                           ' set '||i.nombre2||' = '||i.nombre2||'+'||to_char(i.valor)||
                           ' where cuenta = '''||i.custcode||'''';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;

    END;

    -------------------------------------------------------
    --      SET_MORA
    --      Funci�n que coloca la letra -V en caso
    --      de que el saldo del ultimo periodo sea negativo
    -------------------------------------------------------
    FUNCTION SET_MORA(pdFecha in date) RETURN number is
    lnMes            number;
    lvSentencia      varchar2(1000);
    lvMensErr        varchar2(1000);
    BEGIN
         lnMes:=to_number(to_char(pdFecha,'MM'));
         lvSentencia := 'update CO_REPCARCLI_CS_'||to_char(pdFecha,'ddMMyyyy')||
                        ' set mayorvencido = ''''''-V'''||
                        ' where balance_'||lnMes||' < 0';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         commit;
         return 1;
    END;


    FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
    BEGIN
         lnMesFinal:= to_number(to_char(pdFecha,'MM'));
         lvSentencia:='';
         for lII in 1..lnMesFinal-1 loop
            lvSentencia := lvSentencia||'balance_'||lII||',';
         end loop;
         lvSentencia := lvSentencia||'balance_'||lnMesFinal;
         return(lvSentencia);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END SET_BALANCES;

    --------------------------------------------
    -- SET_CAMPOS_SERVICIOS
    --------------------------------------------
    FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia       varchar2(10000);
    lvNombreServicio  varchar2(100);

    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
    select distinct tipo, nombre2
    from cob_servicios
    where cargo2 = 0;

    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
    select distinct tipo, nombre2
    from cob_servicios
    where cargo2 = 1;

    BEGIN
         lvSentencia:='';
         for s in c_servicios1 loop
             lvSentencia := lvSentencia||s.nombre2||' NUMBER default 0, ';
         end loop;
         -- este campo divide los servicios de tipo no cargo
         -- de los tipo occ o cargos segun el reporte de detalle de clientes.
         lvSentencia := lvSentencia||' TOTAL1 NUMBER default 0, ';
         for t in c_servicios2 loop
             lvSentencia := lvSentencia||t.nombre2||' NUMBER default 0, ';
         end loop;

         return (lvSentencia);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END SET_CAMPOS_SERVICIOS;

    --------------------------------------------
    -- SET_CAMPOS_MORA
    --------------------------------------------
    FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
    BEGIN
         lnMesFinal:= to_number(to_char(pdFecha,'MM'));
         lvSentencia:='';
         for lII in 1..lnMesFinal loop
            lvSentencia := lvSentencia||'BALANCE_'||lII||' NUMBER default 0, ';
         end loop;
         return(lvSentencia);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END SET_CAMPOS_MORA;

    --------------------------------------------
    -- CREA_TABLA
    --------------------------------------------
    FUNCTION CREA_TABLA (pdFechaPeriodo in  date,
                         pv_error       out varchar2 ) RETURN NUMBER IS
    --
    lvSentencia     VARCHAR2(20000);
    lvMensErr       VARCHAR2(1000);
    --
    BEGIN
           lvSentencia := 'CREATE TABLE CO_REPCARCLI_CS_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||
                          '( CUENTA               VARCHAR2(24),'||
                          '  ID_CLIENTE            NUMBER,'||
                          '  PRODUCTO              VARCHAR2(30),'||
                          '  CANTON                VARCHAR2(40),'||
                          '  PROVINCIA             VARCHAR2(25),'||
                          '  APELLIDOS             VARCHAR2(40),'||
                          '  NOMBRES               VARCHAR2(40),'||
                          '  RUC                   VARCHAR2(20),'||
                          '  FORMA_PAGO            VARCHAR2(58),'||
                          '  TARJETA_CUENTA        VARCHAR2(25),'||
                          '  FECH_EXPIR_TARJETA    VARCHAR2(20),'||
                          '  TIPO_CUENTA           VARCHAR2(40),'||
                          '  FECH_APER_CUENTA      DATE,'||
                          '  TELEFONO1             VARCHAR2(25),'||
                          '  TELEFONO2             VARCHAR2(25),'||
                          '  DIRECCION             VARCHAR2(70),'||
                          '  DIRECCION2            VARCHAR2(200),'||
                          '  GRUPO                 VARCHAR2(10),'||
                          cok_cartera_clientes_cs.set_campos_mora(pdFechaPeriodo, lvMensErr)||
                          '  TOTALVENCIDA          NUMBER default 0,'||
                          '  TOTALADEUDA           NUMBER default 0,'||
                          '  MAYORVENCIDO          VARCHAR2(10),'||
                          cok_cartera_clientes_cs.set_campos_servicios(lvMensErr)||
                          '  SALDOANTER            NUMBER default 0,'||
                          '  TOTAL2                NUMBER default 0,'||
                          '  TOTAL3                NUMBER default 0,'||
                          '  TOTAL_FACT            NUMBER default 0,'||
                          '  SALDOAGO              NUMBER default 0,'||
                          '  PAGOSSEP              NUMBER default 0,'||
                          '  CREDTSEP              NUMBER default 0,'||
                          '  CMSEP                 NUMBER default 0,'||
                          '  CONSMSEP              NUMBER default 0,'||
                          '  NUM_FACTURA           VARCHAR2(20),'||
                          '  COMPANIA              NUMBER default 0'||
                          ')'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index IDX_CARCLI_CS'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' on CO_REPCARCLI_CS_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' (CUENTA)'||
                          ' tablespace DATA'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create public synonym CO_REPCARCLI_CS_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' for sysadm.CO_REPCARCLI_CS_'||to_char(pdFechaPeriodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_REPCARCLI_CS_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;



/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFechCierrePeriodo in date) IS

    -- variables
    lvSentencia     VARCHAR2(2000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lII             NUMBER;        --contador para los commits
    lnMes           NUMBER;

BEGIN


        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_CUADRE_CONCILIA''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        -- si la tabla del cual consultamos existe entonces
        -- se procede a calcular los valores
        if lnExisteTabla = 1 then

            --busco la tabla de detalle de clientes
            lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPCARCLI_CS_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'''';
            source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
            dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
            dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
            rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
            rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
            dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
            dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

            if lnExisteTabla = 1 then
               -- si la tabla existe la dropeamos...
               lvSentencia := 'drop table CO_REPCARCLI_CS_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
               EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            end if;

            -- creamos la tabla de acuerdo a los servicios del periodo
            -- y las diferentes edades de mora
            lnExito:=cok_cartera_clientes_cs.crea_tabla(pdFechCierrePeriodo, lvMensErr);

            --se insertan los datos generales y los saldos de la tabla
            --de proceso inicial de reportes
            lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
            lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_CS_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||' NOLOGGING '||
                           '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, '||cok_cartera_clientes_cs.set_balances(pdFechCierrePeriodo, lvMensErr)||', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoago, pagossep, credtsep, cmsep, consmsep, num_factura, compania) '||
                           'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, '||cok_cartera_clientes_cs.set_balances(pdFechCierrePeriodo, lvMensErr)||', total_deuda, total_deuda+balance_'||lnMes||', decode(mora,0,''V'',mora), saldoant, saldoago, pagossep, credtsep, cmsep, consmsep, factura, decode(region,''Guayaquil'',1,2) from CO_CUADRE_CONCILIA'; 
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            commit;

            -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
            lnExito := cok_cartera_clientes_cs.set_mora(pdFechCierrePeriodo);

            -- luego se insertan los servicios de la vista
            cok_cartera_clientes_cs.set_servicios(Pdfechcierreperiodo);

            -- se calculan las columnas de totales
            cok_cartera_clientes_cs.set_totales(Pdfechcierreperiodo, lvMensErr);

        end if;

END MAIN;


END;
/

