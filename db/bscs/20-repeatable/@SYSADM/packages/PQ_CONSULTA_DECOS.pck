create or replace package PQ_CONSULTA_DECOS is

 /*
 ============================================================================
  -- CREADO POR         : SUD ANDRES VASQUEZ
  -- LIDER SIS         : SIS. RICARDO ARGUELLO
  -- LIDER PDS         : SUD RICARDO VERGARA
  -- FECHA DE CREACI�N : 04/10/2017
  -- PROYECTO          : [11497] Cambio de Producto DTH
  -- DESCRIPCI�N       : PROCESO QUE CONSULTA LOS CO_ID, CUSTOMER_ID Y ESTATUS PARA ACTIVARLOS EN AXIS.
  --==========================================================================
  */

  PROCEDURE OBTIENE_DECOS(PV_CUENTA      IN VARCHAR2,
                          PV_STATUS      IN VARCHAR2,
                          PV_BUSCAR      IN VARCHAR2,
                          PV_TRAMA       OUT VARCHAR2,
                          PV_ERROR       OUT VARCHAR2,
                          PN_ERROR       OUT NUMBER);

end PQ_CONSULTA_DECOS;
/
create or replace package body PQ_CONSULTA_DECOS is
 /*
 ============================================================================
  -- CREADO POR        : SUD ANDRES VASQUEZ
  -- LIDER SIS         : SIS. RICARDO ARGUELLO
  -- LIDER PDS         : SUD RICARDO VERGARA
  -- FECHA DE CREACI�N : 04/10/2017
  -- PROYECTO          : [11497] Cambio de Producto DTH
  -- DESCRIPCI�N       : PROCESO QUE CONSULTA LOS CO_ID, CUSTOMER_ID Y ESTATUS PARA ACTIVARLOS EN AXIS.
  --==========================================================================
  */
  PROCEDURE OBTIENE_DECOS(PV_CUENTA      IN VARCHAR2,
                          PV_STATUS      IN VARCHAR2,
                          PV_BUSCAR      IN VARCHAR2,
                          PV_TRAMA       OUT VARCHAR2,
                          PV_ERROR       OUT VARCHAR2,
                          PN_ERROR       OUT NUMBER) IS
                    
  
    CURSOR OBTENER_DECOS(LC_CUENTA VARCHAR2, LC_STATUS VARCHAR2) IS
      SELECT *
        FROM (SELECT DISTINCT dn.dn_num,
                              cu.custcode,
                              cc.cclname,
                              cc.ccfname,
                              cc.ccstreet,
                              cc.ccstreetno,
                              cc.cczip,
                              cc.cccity,
                              co.co_id,
                              decode(ch.ch_status,
                                     'o',
                                     1,
                                     'a',
                                     2,
                                     's',
                                     3,
                                     'd',
                                     4) ch_status,
                              co.subm_id,
                              co.plcode,
                              co.customer_id,
                              co.co_rel_type,
                              cap.vpn_id,
                              decode(cmv.service_mix_type,
                                     1,
                                     'False',
                                     2,
                                     'True')
                FROM curr_co_status     ch,
                     ccontact           cc,
                     customer           cu,
                     contract_all       co,
                     directory_number   dn,
                     profile_service    ccs,
                     contr_services_cap cap,
                     mputmtab           cmv
               WHERE (cc.ccbill = 'X')
                 AND (dn.block_ind = 'N' OR dn.dn_id IS NULL)
                 AND (co.tmcode = cmv.tmcode)
                 AND (co.customer_id = cc.customer_id)
                 AND (co.contract_template IS NULL)
                 AND (co.customer_id = cu.customer_id)
                 AND (co.co_id = ch.co_id)
                 AND (cu.custcode = LC_CUENTA)
                 AND (ch.ch_status in nvl(LOWER(LC_STATUS), ch.ch_status)) ---- si es que env?a
                 AND (co.co_id = ccs.co_id)
                 AND (co.co_id = cap.co_id(+))
                 AND (co.svp_contract IS NULL)
                 AND (cap.dn_id = dn.dn_id(+))
                 AND (cap.seqno NOT IN
                     (SELECT seqno_pre
                         FROM contr_services_cap
                        WHERE co_id = cap.co_id
                          AND sncode = cap.sncode
                          AND seqno_pre IS NOT NULL)));
  
    
    LV_NOMBRE_PROCESO VARCHAR2(100) := 'OBTENER_DECOS';
    LC_CUSTOMER_ID    CLOB;
    LC_CO_ID          CLOB;
    LC_STATUS         CLOB;
  begin
  
    IF PV_CUENTA IS NULL THEN
      PN_ERROR := -1;
      PV_ERROR := 'LA CUENTA NO PUEDE SER NULA';
      RETURN;
    END IF;
    IF PV_BUSCAR IS NULL THEN
      PN_ERROR := -1;
      PV_ERROR := 'ERROR AL BUSCAR LA TRAMA';
      RETURN; 
    END IF;
    FOR I IN OBTENER_DECOS(PV_CUENTA, PV_STATUS) LOOP
      LC_CUSTOMER_ID := LC_CUSTOMER_ID || TO_CHAR(I.CUSTOMER_ID) || CHR(10);--Se cambia espacio para trama
      LC_CO_ID       := LC_CO_ID || I.CO_ID || CHR(10);--Se cambia espacio para trama
      LC_STATUS      := LC_STATUS || I.CH_STATUS || CHR(10);--Se cambia espacio para trama
    END LOOP;
      IF(PV_BUSCAR ='CUSTOMER')THEN
        PV_TRAMA:=SUBSTR(LC_CUSTOMER_ID,1,LENGTH(LC_CUSTOMER_ID)-1) ;
      ELSIF(PV_BUSCAR='CO_ID')THEN
        PV_TRAMA:=SUBSTR(LC_CO_ID,1,LENGTH(LC_CO_ID)-1);
      ELSIF (PV_BUSCAR='STATUS')THEN
        PV_TRAMA:=SUBSTR(LC_STATUS,1,LENGTH(LC_STATUS)-1);
      END IF;
      PV_ERROR:=NULL;
      PN_ERROR :=0;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR EN EL PROCESO ' || LV_NOMBRE_PROCESO || ' => ' ||
                  SUBSTR(SQLERRM, 1, 200);
      PN_ERROR := -1;
    
  end OBTIENE_DECOS;

end PQ_CONSULTA_DECOS;
/
