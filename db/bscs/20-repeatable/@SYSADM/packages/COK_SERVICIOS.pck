create or replace package COK_SERVICIOS is

  --===================================================
  -- Proyecto: Reporte de Detalle de Clientes por factura
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  --=================================================================
  -- Proyecto:       [10400]ENVIO DE MENSAJES DE FIN FACTURACION AUTOMATICO
  -- Proposito:      Verificaci�n e envio de notificaci�n automatico
  -- Modificado por: CLS Matheo S�nchez
  -- Lider CLARO:    SIS Jackeline Gomez
  -- Lider CLS:      CLS Sheyla Ramirez
  -- Fecha:          19/10/2015
  --=================================================================
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_TOTALES2(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date);
  PROCEDURE SET_SERVICIOS2(pdFecha in date);
  PROCEDURE FILL_SERVICIOS(pdFecha in date);
  PROCEDURE FILL_SERVICIOS2(pdFecha in date);
  FUNCTION SET_CAMPOS_CANTONES(pdFechaCierre in date, pvError out varchar2) RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_PROVINCIA(pdFechaCierre in date, pvError out varchar2) RETURN VARCHAR2;
  FUNCTION CREA_TABLA (pdFechaPeriodo in  date, pv_error out varchar2 ) RETURN NUMBER;
  FUNCTION CREA_TABLA2 (pdFechaPeriodo in  date, pv_error out varchar2) RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date);
  PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date);
end;
/
create or replace package body COK_SERVICIOS is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  --           Resumido por Provincia y por Canton
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  --=================================================================
  -- Proyecto:       [10400]ENVIO DE MENSAJES DE FIN FACTURACION AUTOMATICO
  -- Proposito:      Verificaci�n e envio de notificaci�n automatico
  -- Modificado por: CLS Matheo S�nchez
  -- Lider CLARO:    SIS Jackeline Gomez
  -- Lider CLS:    CLS Sheyla Ramirez
  -- Fecha:          19/10/2015
  --=================================================================

    PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvProv            varchar2(10000);
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTot              number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
    lvTmp1             varchar2(50);
    lvTmp2             varchar2(50);
    lvDescripcion      varchar2(80);
    lvTipo             varchar2(100);
    lnCompania         number;
    lvProducto         varchar2(30);

    --cursor para las provincias
    cursor cur_provincia is
    select distinct nvl(upper(co.ccstate),'X') state
    from orderhdr_all oa, ccontact_all co
    where oa.customer_id = co.customer_id
    and oa.ohentdate = pdFecha;

    BEGIN
         lvProv:='';
         for s in cur_provincia loop
            select replace(s.state,' ','_') into lvTmp1 from dual;
            select replace(lvTmp1,'?','') into lvTmp2 from dual;
             lvProv := lvProv||lvTmp2||'+';
         end loop;
         lvProv := substr(lvProv,1,length(lvProv)-1);

         lvSentencia := 'select '||lvProv||', descripcion, compania, producto, tipo from CO_REPSERVPROV_'||to_char(pdFecha,'ddMMyyyy');
         source_cursor := dbms_sql.open_cursor;                     		 --ABRIR CURSOR DE SQL DINAMICO
         dbms_sql.parse(source_cursor,lvSentencia,2);               		 --EVALUAR CURSOR (obligatorio) (2 es constante)
         dbms_sql.define_column(source_cursor, 1, lnTot);
         dbms_sql.define_column(source_cursor, 2, lvDescripcion, 80);
         dbms_sql.define_column(source_cursor, 3, lnCompania);
         dbms_sql.define_column(source_cursor, 4, lvProducto, 30);
         dbms_sql.define_column(source_cursor, 5, lvTipo, 100);
         rows_processed := dbms_sql.execute(source_cursor);         		 --EJECUTAR COMANDO DINAMICO

         loop
        		rows_fetched := dbms_sql.fetch_rows(source_cursor);
        		if rows_fetched > 0 then
        				dbms_sql.column_value(source_cursor, 1, lnTot);
                dbms_sql.column_value(source_cursor, 2, lvDescripcion);
                dbms_sql.column_value(source_cursor, 3, lnCompania);
                dbms_sql.column_value(source_cursor, 4, lvProducto);
                dbms_sql.column_value(source_cursor, 5, lvTipo);

        				-- se procede a actualizar la tabla
                lvSentencia := 'update CO_REPSERVPROV_'||to_char(pdFecha,'ddMMyyyy')||
                               ' set TOTAL = '||to_char(lnTot,'999999999.90')||
                               ' where descripcion = '''||lvDescripcion||''''||
                               ' and compania = '||lnCompania||
                               ' and tipo = '''||lvTipo||''''||
                               ' and producto = '''||lvProducto||'''';
                EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        		else
        			  exit;
            end if;
         end loop;
         commit;
         dbms_sql.close_cursor(source_cursor);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END;

    -----------------------------------------------------------
    --SET TOTALES 2
    --Proceso para Cantones
    -----------------------------------------------------------
    PROCEDURE SET_TOTALES2(pdFecha in date, pvError out varchar2) IS
    lvProv            varchar2(20000);
    lvSentencia        varchar2(20000);
    lvMensErr          varchar2(2000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTot              number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
    lvDescripcion      varchar2(80);
    lnCompania         number;
    lvProducto         varchar2(30);
    lvTipo             varchar2(100);

    --cursor para las provincias
    cursor cur_canton is
    select distinct nvl(upper(replace(replace(replace(replace(replace(replace(replace(co.cccity,'-',''),')',''),'(',''),' ','_'),'.',''),'?',''),';','_')),'x') city
    from orderhdr_all oa, ccontact_all co
    where oa.customer_id = co.customer_id
    and oa.ohentdate = pdFecha;

    BEGIN
         lvProv:='';
         for s in cur_canton loop
             lvProv := lvProv||s.city||'+';
         end loop;
         lvProv := substr(lvProv,1,length(lvProv)-1);

         lvSentencia := 'select '||lvProv||', descripcion, compania, producto, tipo from CO_REPSERVCANT_'||to_char(pdFecha,'ddMMyyyy');
         source_cursor := dbms_sql.open_cursor;                     		 --ABRIR CURSOR DE SQL DINAMICO
         dbms_sql.parse(source_cursor,lvSentencia,2);               		 --EVALUAR CURSOR (obligatorio) (2 es constante)
         dbms_sql.define_column(source_cursor, 1, lnTot);
         dbms_sql.define_column(source_cursor, 2, lvDescripcion, 80);
         dbms_sql.define_column(source_cursor, 3, lnCompania);
         dbms_sql.define_column(source_cursor, 4, lvProducto, 30);
         dbms_sql.define_column(source_cursor, 5, lvTipo, 100);
         rows_processed := dbms_sql.execute(source_cursor);         		 --EJECUTAR COMANDO DINAMICO

         loop
        		rows_fetched := dbms_sql.fetch_rows(source_cursor);
        		if rows_fetched > 0 then
        				dbms_sql.column_value(source_cursor, 1, lnTot);
                dbms_sql.column_value(source_cursor, 2, lvDescripcion);
                dbms_sql.column_value(source_cursor, 3, lnCompania);
                dbms_sql.column_value(source_cursor, 4, lvProducto);
                dbms_sql.column_value(source_cursor, 5, lvTipo);

        				-- se procede a actualizar la tabla
                lvSentencia := 'update CO_REPSERVCANT_'||to_char(pdFecha,'ddMMyyyy')||
                               ' set TOTAL = '||to_char(lnTot,'999999999.90')||
                               ' where descripcion = '''||lvDescripcion||''''||
                               ' and compania = '||lnCompania||
                               ' and tipo = '''||lvTipo||''''||
                               ' and producto = '''||lvProducto||'''';
                EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        		else
        			  exit;
            end if;
         end loop;
         commit;
         dbms_sql.close_cursor(source_cursor);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END;


    -------------------------------------------
    --             SET_SERVICIOS
    --             Para provincias
    -------------------------------------------
    PROCEDURE SET_SERVICIOS(pdFecha in date) IS
    lvSentencia      VARCHAR2(1000);
    lvMensErr        VARCHAR2(1000);
    lvTmp1           varchar2(50);
    lvTmp2           varchar2(50);
    lvquery          varchar2(2000);
    lvstate         varchar2(25);
    lvtipo           varchar2(40);
    lvnombre         varchar2(40);
    lncentro_costo   number;
    lvproducto       varchar2(40);
    lnvalor          number;
    lndscto          number;

    TYPE Rc_ReSerPro1 IS REF CURSOR;
    C_ReSerPro1       Rc_ReSerPro1;

    BEGIN
       --Inicio: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 03/12/2007.
       lvquery := 'select nvl(ccstate,';
       lvquery := lvquery || '''x''';
       lvquery := lvquery || ') state, tipo, nombre, decode(cost_desc,';
       lvquery := lvquery || '''Guayaquil''';
       lvquery := lvquery || ',1,2) centro_costo, producto, sum(valor) sum_valor, sum(descuento) dscto';
       lvquery := lvquery || ' from  CO_FACT_'||to_char(pdFecha,'ddMMyyyy');
       --lvquery := lvquery || ' from  CUST_3_'||to_char(pdFecha,'ddMMyyyy');
       lvquery := lvquery || ' group by cost_desc, ccstate, producto, tipo, nombre';

       OPEN C_ReSerPro1 FOR lvquery;
       loop
           FETCH C_ReSerPro1 INTO lvstate, lvtipo, lvnombre, lncentro_costo, lvproducto, lnvalor, lndscto;
           exit when C_ReSerPro1%notfound;

           select replace(lvstate,' ','_') into lvTmp1 from dual;
           select replace(lvTmp1,'?','') into lvTmp2 from dual;

           lvSentencia := 'update CO_REPSERVPROV_'||to_char(pdFecha,'ddMMyyyy')||
                           ' set '||lvTmp2||'='||lvTmp2||'+'||to_char(lnvalor,'99999999.90')||'-'||nvl(to_char(lndscto,'99999999.90'),0)||
                           --' set '||lvTmp2||'='||lvTmp2||'+'||i.sum_valor||
                           ' where descripcion = '''||lvnombre||''''||
                           ' and compania = '||lncentro_costo||
                           ' and tipo = '''||lvtipo||''''||
                           ' and producto = '''||lvproducto||'''';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       --Fin: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 03/12/2007.
       end loop;
       commit;
       close C_ReSerPro1;
    END;


    -------------------------------------------
    --             SET_SERVICIOS2
    --             Para cantones
    -------------------------------------------
    PROCEDURE SET_SERVICIOS2(pdFecha in date) IS
    lvSentencia      VARCHAR2(2000);
    lvMensErr        VARCHAR2(2000);
    lvTmp1      varchar2(50);
    lvTmp2      varchar2(50);
    lvQuery     varchar2(2000);
    lv_city          varchar2(40);
    lv_tipo          varchar2(40);
    lv_nombre        varchar2(40);
    lv_centro_costo  varchar2(30);
    lv_producto      varchar2(30);
    ln_sumvalor      number;
    ln_dscto         number;

    TYPE Rc_ReSerCan1 IS REF CURSOR;
    C_ReSerCan1          Rc_ReSerCan1;

    BEGIN
       --Inicio: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 29/11/2007.
       lvQuery := 'select nvl(upper(replace(replace(replace(replace(replace(replace(cccity,';
       lvQuery := lvQuery || '''-''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''''';--
       lvQuery := lvQuery || ')';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || ''')''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''''';--
       lvQuery := lvQuery || ')';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''(''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''''';--
       lvQuery := lvQuery || ')';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || ''' ''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''_''';
       lvQuery := lvQuery || ')';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''.''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''''';--
       lvQuery := lvQuery || ')';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''?''';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''''';--
       lvQuery := lvQuery || '))';
       lvQuery := lvQuery || ',';
       lvQuery := lvQuery || '''x''';
       lvQuery := lvQuery || ') city, tipo, nombre, decode(cost_desc,';
       lvQuery := lvQuery || '''Guayaquil''';
       lvQuery := lvQuery || ',1,2) centro_costo, producto, sum(valor) sum_valor, sum(descuento) dscto';
       lvQuery := lvQuery || ' from CO_FACT_'||to_char (pdFecha,'ddMMyyyy');
       --lvQuery := lvQuery || ' from CUST_3_'||to_char (pdFecha,'ddMMyyyy');
       lvQuery := lvQuery || ' group by cost_desc, cccity, producto, tipo, nombre' ;

       OPEN C_ReSerCan1 for lvQuery;

       loop
           FETCH C_ReSerCan1 into lv_city, lv_tipo, lv_nombre, lv_centro_costo, lv_producto, ln_sumvalor, ln_dscto;
           exit when C_ReSerCan1%notfound;
           lvSentencia := 'update CO_REPSERVCANT_'||to_char(pdFecha,'ddMMyyyy')||
                          ' set '||lv_city||'='||lv_city||'+'||to_char(ln_sumvalor,'99999999.90')||'-'||nvl(to_char(ln_dscto,'99999999.90'),0)||
                          --' set '||i.city||'='||i.city||'+'||i.sum_valor||
                          ' where descripcion = '''||lv_nombre||''''||
                          ' and compania = '||lv_centro_costo||
                          ' and tipo = '''||lv_tipo||''''||
                          ' and producto = '''||lv_producto||'''';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end loop;
        commit;
        close C_ReSerCan1;
        --Fin: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 29/11/2007.
    END;


    -------------------------------------------
    --             FILL_SERVICIOS
    --             Proceso para provincias
    -------------------------------------------
    PROCEDURE FILL_SERVICIOS(pdFecha in date) IS
    lvSentencia      VARCHAR2(2000);
    lvMensErr        VARCHAR2(2000);
    lvQueryP         varchar2(2000);
    lv_tipo          varchar2(40);
    lv_nombre        varchar2(40);
    lv_centro_costo  varchar2(40);
    lv_producto      varchar2(40);

    TYPE Rc_ReSerPro IS REF CURSOR;
    C_ReSerPro          Rc_ReSerPro;

    BEGIN
       --Inicio: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 03/12/2007.
       lvQueryP := 'select tipo, nombre, decode(cost_desc,';
       lvQueryP := lvQueryP || '''Guayaquil''';
       lvQueryP := lvQueryP || ',1,2) centro_costo, producto';
       lvQueryP := lvQueryP || ' from CO_FACT_'||to_char (pdFecha,'ddMMyyyy');
       --lvQueryP := lvQueryP || ' from CUST_3_'||to_char (pdFecha,'ddMMyyyy');
       lvQueryP := lvQueryP || ' group by cost_desc, producto, tipo, nombre';

       OPEN C_ReSerPro for lvQueryP;
       loop
          FETCH C_ReSerPro into lv_tipo, lv_nombre, lv_centro_costo, lv_producto;
          exit when C_ReSerPro%notfound;
          lvSentencia := 'insert into CO_REPSERVPROV_'||to_char(pdFecha,'ddMMyyyy')||
                         '(descripcion, tipo, compania, producto)'||
                         ' values('''||lv_nombre||''','''||lv_tipo||''','||lv_centro_costo||','''||lv_producto||''')';
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end loop;
        commit;
        close C_ReSerPro;
        --Fin: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 03/12/2007.
    END;

    -------------------------------------------
    --             FILL_SERVICIOS2
    --             Proceso para cantones
    -------------------------------------------
    PROCEDURE FILL_SERVICIOS2(pdFecha in date) IS
    lvSentencia      VARCHAR2(1000);
    lvMensErr        VARCHAR2(1000);
    lvQuery        varchar2(2000);
    lvnombre       varchar2(100);
    lvtipo         varchar2(100);
    lvcentro_costo varchar2(100);
    lvproducto     varchar2(100);

    TYPE Rc_ReSerCan IS REF CURSOR;
    C_ReSerCan          Rc_ReSerCan;

    BEGIN
       --Inicio: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 29/11/2007.
       lvQuery := 'select tipo, nombre, decode(cost_desc,';
       lvQuery := lvQuery || '''Guayaquil''';
       lvQuery := lvQuery || ',1,2) centro_costo, producto';
       lvQuery := lvQuery ||' from CO_FACT_'||to_char (pdFecha,'ddMMyyyy');
       --lvQuery := lvQuery ||' from CUST_3_'||to_char (pdFecha,'ddMMyyyy');
       lvQuery := lvQuery ||' group by cost_desc, producto, tipo, nombre';

       OPEN C_ReSerCan for lvQuery;
       loop
          FETCH C_ReSerCan into lvtipo, lvnombre ,lvcentro_costo, lvproducto;
          exit when C_ReSerCan%notfound;
          lvSentencia := 'insert into CO_REPSERVCANT_'||to_char(pdFecha,'ddMMyyyy')||
                         '(descripcion, tipo, compania, producto)'||
                         ' values('''||lvnombre||''','''||lvtipo||''','||lvcentro_costo||','''||lvproducto||''')';
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end loop;
        commit;
        close C_ReSerCan;
        --Fin: 2702 - SUD Rene Vega. Query dinamico para servicios desde la co_fact. 29/11/2007.
    END;

    --------------------------------------------------------
    --SET_CAMPOS_CANTONES
    -------------------------------------------------------
    FUNCTION SET_CAMPOS_CANTONES(pdFechaCierre in date, pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia varchar2(20000);

    cursor cur_cantones is
    select distinct nvl(upper(replace(replace(replace(replace(replace(replace(replace(co.cccity,'-',''),')',''),'(',''),' ','_'),'.',''),'?',''),';','_')),'x') city
    from orderhdr_all oa, ccontact_all co
    where oa.customer_id = co.customer_id
    and oa.ohentdate = pdFechaCierre;

    BEGIN
         lvSentencia:='';
         for i in cur_cantones loop
            lvSentencia := lvSentencia||i.city||' NUMBER default 0, ';
         end loop;
         return(lvSentencia);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END SET_CAMPOS_CANTONES;

    --------------------------------------------
    -- SET_CAMPOS_PROVINCIA
    --------------------------------------------
    FUNCTION SET_CAMPOS_PROVINCIA(pdFechaCierre in date, pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia varchar2(2000);
    lvTmp1      varchar2(50);
    lvTmp2      varchar2(50);

    cursor cur_provincia is
    select distinct nvl(upper(co.ccstate),'X') state
    from orderhdr_all oa, ccontact_all co
    where oa.customer_id = co.customer_id
    and oa.ohentdate = pdFechaCierre;

    BEGIN
         lvSentencia:='';
         for i in cur_provincia loop
            select replace(i.state,' ','_') into lvTmp1 from dual;
            select replace(lvTmp1,'?','') into lvTmp2 from dual;
            lvSentencia := lvSentencia||lvTmp2||' NUMBER default 0, ';
         end loop;
         return(lvSentencia);
    EXCEPTION
    when others then
         pvError := sqlerrm;
    END SET_CAMPOS_PROVINCIA;

    --------------------------------------------
    -- CREA_TABLA
    -- Creaci�n de tabla para provincias
    --------------------------------------------
    FUNCTION CREA_TABLA (pdFechaPeriodo in  date,
                         pv_error       out varchar2 ) RETURN NUMBER IS
    --
    lvSentencia     VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);
    --
    BEGIN
           -- provincias
           lvSentencia := 'CREATE TABLE CO_REPSERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||
                          '( DESCRIPCION           VARCHAR2(80),'||
                          '  TIPO                  VARCHAR2(40),'||
                          '  COMPANIA              NUMBER,'||
                          '  PRODUCTO              VARCHAR2(30),'||
                          cok_servicios.set_campos_provincia(pdFechaPeriodo, lvMensErr)||
                          '  TOTAL                 NUMBER default 0'||
                          ')'||
                          'tablespace CO_REP_DAT'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index IDX_SERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' on CO_REPSERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' (DESCRIPCION, COMPANIA, PRODUCTO)'||
                          ' tablespace CO_REP_IDX'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create public synonym CO_REPSERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||' for sysadm.CO_REPSERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_REPSERVPROV_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;


    -----------------------------------------------
    -- CREA_TABLA2
    -- Creaci�n de tabla para reporte x cantones
    ----------------------------------------------
    FUNCTION CREA_TABLA2 (pdFechaPeriodo in  date,
                         pv_error       out varchar2 ) RETURN NUMBER IS
    --
    lvSentencia     VARCHAR2(30000);
    lvSentenciaCrea VARCHAR2(30000);
    lvMensErr       VARCHAR2(2000);
    --
    BEGIN
           -- cantones
           lvSentenciaCrea := 'CREATE TABLE CO_REPSERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy') ||
                          '( DESCRIPCION           VARCHAR2(80),'||
                          '  TIPO                  VARCHAR2(40),'||
                          '  COMPANIA              NUMBER,'||
                          '  PRODUCTO              VARCHAR2(30),'||
                          cok_servicios.set_campos_cantones(pdFechaPeriodo, lvMensErr)||
                          '  TOTAL                 NUMBER default 0'||
                          ')'||
                          'tablespace CO_REP_DAT'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1040K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

           lvSentencia := 'create index IDX_SERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' on CO_REPSERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy')  ||' (DESCRIPCION, COMPANIA, PRODUCTO)'||
                          ' tablespace CO_REP_IDX'||
                          ' pctfree 10'||
                          ' initrans 2'||
                          ' maxtrans 255'||
                          ' storage'||
                          ' ( initial 1M'||
                          '   next 1040K'||
                          '   minextents 1'||
                          '   maxextents 505'||
                          '   pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create public synonym CO_REPSERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy')  ||' for sysadm.CO_REPSERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           Lvsentencia := 'grant all on CO_REPSERVCANT_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to public';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA2;


/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFechCierrePeriodo in date) IS

    -- variables
    lvSentencia     VARCHAR2(2000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(2000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lII             NUMBER;        --contador para los commits


BEGIN
            -----------------------------------------------
            --            proceso para provincias
            -----------------------------------------------

            lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPSERVPROV_'||to_char(pdFechCierrePeriodo,'ddMMyyyy') ||'''';
            source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
            dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
            dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
            rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
            rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
            dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
            dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

            if lnExisteTabla = 1 then
               -- si la tabla existe la dropeamos...
               lvSentencia := 'drop table CO_REPSERVPROV_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
               EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            end if;
            lnExito:=cok_servicios.crea_tabla(pdFechCierrePeriodo, lvMensErr);
            cok_servicios.fill_servicios(pdFechCierrePeriodo);
            cok_servicios.set_servicios(pdFechCierrePeriodo);
            cok_servicios.set_totales(pdFechCierrePeriodo, lvMensErr);
            -----------------------------------------------
            --           proceso para cantones
            -----------------------------------------------
            lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPSERVCANT_'||to_char(pdFechCierrePeriodo,'ddMMyyyy') ||'''';
            source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
            dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
            dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
            rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
            rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
            dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
            dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

            if lnExisteTabla = 1 then
               -- si la tabla existe la dropeamos...
               lvSentencia := 'drop table CO_REPSERVCANT_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
               EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            end if;
            lnExito:=cok_servicios.crea_tabla2(pdFechCierrePeriodo, lvMensErr);
            cok_servicios.fill_servicios2(pdFechCierrePeriodo);
            cok_servicios.set_servicios2(pdFechCierrePeriodo);
            cok_servicios.set_totales2(pdFechCierrePeriodo, lvMensErr);
            ----------------------------------------------------
            --           proceso para verificaci�n de Datos
            ----------------------------------------------------
            cok_servicios.pr_envio_notificacion(pdfechcierreperiodo => pdFechCierrePeriodo); --[10400] MSA 
            
            
END MAIN;

-- [10400] MSA INI --  
PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date) IS
  
  lvSentencia     VARCHAR2(2000);
  lnCant_can      NUMBER;
  lnCant_pro      NUMBER;
  lfCant_tot1     FLOAT;
  lfCant_tot2     FLOAT;
  lv_asunto       VARCHAR2(1000);
  lv_mensaje      VARCHAR2(1000);
  lv_cantidad     VARCHAR2(1000);
  lv_sumatoria    VARCHAR2(1000);
  lv_host         VARCHAR2(1000);
  lv_from         VARCHAR2(1000);
  lv_to           VARCHAR2(1000);
  lv_cc           VARCHAR2(1000);
  lvMensErr       VARCHAR2(2000);
  ln_id_bitacora_scp number:=0;
  ln_error_scp       number:=0;
  lv_error_scp       varchar2(500);
  ln_registros_error_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_SERVICIOS';
  lv_referencia_scp varchar2(100):='COK_SERVICIOS.PR_ENVIO_NOTIFICACION';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_total_registros_scp number:=0;
  le_error        EXCEPTION;
    
   Cursor c_parametros(cv_id_parametro varchar2,cv_tipo_parametro varchar2) is
        select valor
          from gv_parametros
         where id_parametro = cv_id_parametro
           and id_tipo_parametro = cv_tipo_parametro; 
            
begin
  
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
  gv_fecha_fin := NULL; 
  ----------------------------------------------------------------------
  -- SCP: Codigo generado automaticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  ln_registros_error_scp:=ln_registros_error_scp+1;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'INICIO DE COK_SERVICIOS.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;
    
  lvSentencia := 'select count(*) contador from co_repservprov_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
  EXECUTE IMMEDIATE lvSentencia INTO lnCant_pro;
  lvSentencia := 'select Count(*) contador from co_repservcant_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
  EXECUTE IMMEDIATE lvSentencia INTO lnCant_can;
  lvSentencia := 'select sum(total) contador from co_repservprov_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
  EXECUTE IMMEDIATE lvSentencia INTO lfCant_tot1;
  lvSentencia := 'select sum(total) contador from co_repservcant_'||to_char(pdFechCierrePeriodo,'ddMMyyyy');
  EXECUTE IMMEDIATE lvSentencia INTO lfCant_tot2;
            
  Open c_parametros('MAIL_HOST','7399');
  fetch c_parametros into lv_host;
  close c_parametros;
      
  Open c_parametros('FROM','5328');
  fetch c_parametros into lv_from;
  close c_parametros;
          

  if lnCant_pro = lnCant_can and lfCant_tot1 = lfCant_tot2 then
             
    Open c_parametros('ASUNTO_REPORTE_CANT_PROV','7399');
    fetch c_parametros into lv_asunto;
    close c_parametros;
    
    Open c_parametros('MENSAJE_REPORTE_CANT_PROV','7399');
    fetch c_parametros into lv_mensaje;
    close c_parametros;
    
    Open c_parametros('TO_REP_EXIT_CANT_PROV','7399');
    fetch c_parametros into lv_to;
    close c_parametros;
    
    Open c_parametros('CC_REP_EXIT_CANT_PROV','7399');
    fetch c_parametros into lv_cc;
    close c_parametros;
            
    lv_asunto:= replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
    lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
           
    wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                         pv_de      => lv_from,
                                         pv_para    => lv_to,
                                         pv_cc      => lv_cc,
                                         pv_asunto  => lv_asunto,
                                         pv_mensaje => lv_mensaje,
                                         pv_error   => lvMensErr);
    if lvMensErr is null then                                    
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_SERVICIOS.PR_ENVIO_NOTIFICACION: Se verifica que el Reporte cuadra correctamente, se procede a enviar notificaci�n de confirmaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------                                    
      COMMIT;
    else
      raise le_error;
    end if;
             
  else
            
    Open c_parametros('ASUNTO_REPORTE_CANT_PROV1','7399');
    fetch c_parametros into lv_asunto;
    close c_parametros;
    
    Open c_parametros('MENSAJE_REPORTE_CANT_PROV1','7399');
    fetch c_parametros into lv_mensaje;
    close c_parametros;
    
    Open c_parametros('TO_REP_FALL_CANT_PROV','7399');
    fetch c_parametros into lv_to;
    close c_parametros;
    
    Open c_parametros('CC_REP_FALL_CANT_PROV','7399');
    fetch c_parametros into lv_cc;
    close c_parametros;
    
    lv_asunto := replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/MM/yyyy'));        
    lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'ddMMyyyy'));
    
    if lnCant_pro <> lnCant_can then
      lv_cantidad:='<br>No tienen la misma cantidad de registros.';
    end if;
    
    if lfCant_tot1 <> lfCant_tot2 then
      lv_sumatoria:='<br>No tienen la misma sumatoria.';
    end if;    
    
    lv_mensaje:= replace(lv_mensaje,'[%ERROR1%]',nvl(lv_cantidad,' '));
    lv_mensaje:= replace(lv_mensaje,'[%ERROR2%]',nvl(lv_sumatoria,' '));
    lv_mensaje:= replace(lv_mensaje,'[%CANT_CAN%]',lnCant_can);
    lv_mensaje:= replace(lv_mensaje,'[%CANT_PROV%]',lnCant_pro);
    lv_mensaje:= replace(lv_mensaje,'[%TOTAL1%]',lfCant_tot1);
    lv_mensaje:= replace(lv_mensaje,'[%TOTAL2%]',lfCant_tot2);
               
    wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                         pv_de      => lv_from,
                                         pv_para    => lv_to,
                                         pv_cc      => lv_cc,
                                         pv_asunto  => lv_asunto,
                                         pv_mensaje => lv_mensaje,
                                         pv_error   => lvMensErr);
                                         
    if lvMensErr is null then                                     
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_SERVICIOS.PR_ENVIO_NOTIFICACION: Se verifica que el Reporte no cuadra correctamente, se procede a enviar notificaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
                                          
    else
      raise le_error;
    end if;                                     
                                         
  end if;
  
  ----------------------------------------------------------------------
  -- SCP: Codigo generado automaticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  ln_registros_error_scp:=ln_registros_error_scp+1;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'FIN DE COK_SERVICIOS.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;
      
  Exception
  when le_error then
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'ERROR: COK_SERVICIOS.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
    COMMIT;
    
  when others then
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'ERROR: COK_SERVICIOS.PR_ENVIO_NOTIFICACION',sqlerrm,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
    COMMIT; 
       
end PR_ENVIO_NOTIFICACION;            
-- [10400] MSA FIN --  
end;
/
