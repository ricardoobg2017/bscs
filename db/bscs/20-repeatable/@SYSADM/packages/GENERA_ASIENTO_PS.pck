create or replace package GENERA_ASIENTO_PS is
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    : Programa principal en la ejecuci�n de los
--             procesos : GRABA_SAFI_LAN y GRABA_SAFI_CIA_REL. 
-------------------------------------------------------
PROCEDURE PRINCIPAL (pdFechCierrePeriodo in date, resultado out varchar2);
---
---
PROCEDURE GRABA_SAFI_LAN(pdFechCierrePeriodo in date, 
                         resultado           out varchar2,
                         pn_bandera          in out number);
---
---
PROCEDURE GRABA_SAFI_CIA_REL(pdFechCierrePeriodo in date,
                             resultado           out varchar2,
                             pn_bandera          in out number);
---
---
PROCEDURE REGISTRA_CLI_CIA_REL_TMP ;
---
---
PROCEDURE VERIFICA_REGISTRO_RELACIONAL (pn_customer_id  in  number,
                                        pv_custcode     in  varchar2,
                                        pv_ruc_ced      in  varchar2,
                                        pv_cod_cia_rel  in  varchar2,
                                        pn_encontro     in  out varchar2,
                                        pv_error        out varchar2);
---
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Registra temporalmente informaci�n de
--            de los clientes de cia relacionadas.
-------------------------------------------------------
PROCEDURE INSERTA_CLIENTES_CIA_REL (pn_customer_id  in  number,
                                    pv_custcode      in  varchar2,
                                    pv_ruc_ced      in  varchar2,
                                    pv_cod_cia_rel  in  varchar2,
                                    pv_error        out varchar2);
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Registra los errores ocurridos en la 
--            Bitacora.
-------------------------------------------------------
PROCEDURE INSERTA_BITACORA_CIA_REL (pd_fecha        in  date,
                                    pv_programa     in  varchar2,
                                    pv_error        in  out varchar2) ;                                    

  

end GENERA_ASIENTO_PS;
/
create or replace package body GENERA_ASIENTO_PS is

-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    : Programa principal en la ejecuci�n de los
--             procesos : GRABA_SAFI_LAN y GRABA_SAFI_CIA_REL. 
-------------------------------------------------------
PROCEDURE PRINCIPAL (pdFechCierrePeriodo in date, resultado out varchar2) IS

---variables---
---------------
ln_bandera number:=0;
lv_programa varchar2(1000):='GENERA_ASIENTO_PS.PRINCIPAL --> ';  
le_error EXCEPTION;
BEGIN
  
   IF pdFechCierrePeriodo IS NULL  THEN
     resultado:='Ingrese la fecha de cierre de periodo.';
     RAISE le_error;
   END IF;
   
   --Actualizacion de la tabla [REGISTRA_CLI_CIA_TMP] de los clientes de cia relacionadas 
 --  REGISTRA_CLI_CIA_REL_TMP;
   
   --- Se Graba cuentas que facturan solo IVA 12%, solo CICLO y CICLO + IVA 12%
   GRABA_SAFI_LAN(pdFechCierrePeriodo, resultado ,ln_bandera);
  
   --Si el valor de la variable [ln_bandera = 0  (OK) ;ln_bandera = 1 (Error) ]
   
   IF ln_bandera=0  THEN 
       resultado:=NULL;
       -- Se Graba cuentas que facturan solo IVA 12%, solo CICLO y CICLO + IVA 12%
       GRABA_SAFI_CIA_REL(pdFechCierrePeriodo,resultado,ln_bandera); 
       --Cuando surgio alg�n error en el proceso de loa clientes para aquellos que son
       -- de compania relacionada. 
       
       IF ln_bandera = -1  THEN 
          RAISE le_error;
       END IF;
 
   ELSE
    RAISE le_error;
   END IF;
   commit; 

EXCEPTION
  WHEN  le_error THEN 
     resultado:= 'Error :'||lv_programa||resultado;
     rollback;
END;

---------------------------------------------------------------------
---------------------------------------------------------------------
---------------------------------------------------------------------
PROCEDURE GRABA_SAFI_LAN(pdFechCierrePeriodo in date, resultado out varchar2,pn_bandera in out number) is

---- SIS LEONARDO ANCHUNDIA MENENDEZ
-----Creaci�n : 29/01/2008 -- Ultima actualizacion: 21/09/2010
-----pdFechCierrePeriodo 24/mm/yyyy o 08/mm/yyyy
-----pdFechCierreReport  31/mm/yyyy o 15/mm/yyyy
-----glosa = Facturaci�n consumo celular del 24/11/05 al 23/12/05
-----descuentos 21/04/2010
-----Graba cuentas que facturan solo IVA 12%, solo CICLO y CICLO + IVA 12%

lvSentencia    varchar2(18000);
lnMesFinal number;
lnanoFinal number;
lndiaFinal varchar2(2);
lvFecha varchar2(10);
lvfechanterior varchar(12);
lvfechactual varchar(12);
glosa varchar2(200);
glosa_exentos varchar2(200);
lv_cuadre number;
lv_cuadre2 number;
duio number;
dgye number;
fechacierreP date;
fechacierreR date;
fechacierreR2 varchar2(20);
fechacierreR3 varchar2(20);
totu number;
totg number;
secu number;
secu2 number;

--5779 cls dro ini
secu_cia_rel_gye  number;
secu_cia_rel_uio  number;
totg_cia_rel_g    number;  
totg_cia_rel_u    number;
lv_cuadre_cia_rel number;
ln_maxsec_g       number;
ln_maxsec_u       number;
ln_maxsec_g1      number;
ln_maxsec_u1      number;
ln_registros      number;
lv_transaccion_id_gye varchar2(30);
ld_fecha_acct     date;

CURSOR verifica_pruebasys IS
 select count(*) from read.pruebafinsys_dr;

---5779 Fin

CURSOR descX is
select r.ctapsoft,r.cta_dscto  from read.cob_servicios r ;

BEGIN

fechacierreP := to_date(pdFechCierrePeriodo, 'dd/mm/yyyy');
fechacierreR := to_date(sysdate, 'dd/mm/yyyy');
fechacierreR2 := trim(to_char(sysdate, 'yyyymmdd'));
fechacierreR3 := trim(to_char(sysdate+1, 'yyyymmdd'));
lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
lndiaFinal  := to_char(pdFechCierrePeriodo, 'DD');
lnanoFinal  := to_number(to_char(pdFechCierrePeriodo, 'YYYY'));
lvfechanterior:=to_char(add_months(pdFechCierrePeriodo,-1),'dd/mm/yyyy');
lvfechactual:=to_char(pdFechCierrePeriodo,'dd/mm/yyyy');
glosa:='Facturaci�n consumo celular del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
glosa_exentos:='Fact_Excenta consumo celular  del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
  lvSentencia := 'truncate  table read.pruebafinsys_dr';
--lvSentencia := 'delete from  read.pruebafinsys_dr';
lvFecha :=to_char(pdFechCierrePeriodo, 'DDMMYYYY');
exec_sql(lvSentencia);



select decode(lndiaFinal,'02','04','08','02','15','03','24','01') into lndiaFinal  from dual;
commit;
lvSentencia := null;
--CLS DRO.
lvSentencia := 'insert into read.pruebafinsys_dr  ' ||
--'select /* + rule */  h.tipo,h.nombre,h.producto,sum(h.valor) valor ,sum(h.descuento) descuento,h.cost_desc compa�ia , max(h.ctaclblep) ' ||
--' , null  CUENTA_DESC from co_fact_'|| lvFecha ||
-- ' h    where  h.customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
--                  '    WHERE cte.exempt_status = ''A'') ' ||
--'   group by h.tipo,h.nombre,h.producto,h.cost_desc';

'select /* + rule */  h.tipo,h.nombre,h.producto,sum(h.valor) valor ,sum(h.descuento) descuento,h.cost_desc compa�ia , max(h.ctaclblep) ' ||
' , null  CUENTA_DESC, 0,'' '' from co_fact_'|| lvFecha ||
  ' h    where  h.customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
  ' and  h.customer_id not in (SELECT   g.customer_id  FROM  REGISTRA_CLI_CIA_TMP g where g.customer_id=h.customer_id)   ' ||                  
  '   group by h.tipo,h.nombre,h.producto,h.cost_desc';
exec_sql(lvSentencia);

commit;

---CLS  DRO - verifica si se cargo informaci�n la tabla read.pruebafinsys
 open  verifica_pruebasys;
 fetch verifica_pruebasys  into  ln_registros;
 close verifica_pruebasys;
---
IF ln_registros > 0  THEN 

for rd in descX loop
update read.pruebafinsys_dr f set f.cta_desc=rd.cta_dscto where f.ctactble=rd.ctapsoft;

end loop;
commit;

lvSentencia := null;

 --Se obtiene la m�xima secuencia de la estrutura nueva
   -- �ra  poder asignar a los nuevos registros a obtenerse
   -- Por Compania Relacionada.
   --GYE
    select  nvl(max(a.transaction_line),0)  
            into ln_maxsec_g 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and  a.ledger_group='REAL'
    and  a.ledger='LOCAL'
    and  a.appl_jrnl_id='BSCS'
    and  a.Transaction_Id='BG'||fechacierreR2
    and  a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');

    --UIO
    select  nvl(max(a.transaction_line),0)  
            into ln_maxsec_u 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and  a.ledger_group='REAL'
    and  a.ledger='LOCAL'
    and  a.appl_jrnl_id='BSCS'
    and  a.Transaction_Id='BU'||fechacierreR2
    and  a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');

   
lvSentencia := 'truncate table READ.GSI_FIN_SAFI_DR';
--lvSentencia := 'delete from  READ.GSI_FIN_SAFI_DR';
exec_sql(lvSentencia);
commit;
lvSentencia := ' truncate table ps_pr_jgen_acct_en_tmp_dr';
--lvSentencia := ' delete from ps_pr_jgen_acct_en_tmp_dr ';
exec_sql(lvSentencia);
commit;

lvSentencia := 'truncate table READ.GSI_FIN_SAFI_EXCENTOS';
--lvSentencia := ' delete from READ.GSI_FIN_SAFI_EXCENTOS';
exec_sql(lvSentencia);
commit;
-----------GYE normal


insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BG'||fechacierreR2,rownum+ln_maxsec_g,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+ln_maxsec_g,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';

commit;

---select max(rownum) into secu from READ.GSI_FIN_SAFI_dr;

select nvl(max(a.transaction_line),0) into secu 
from   READ.GSI_FIN_SAFI_DR a 
where  a.business_unit='CONEC'
and    a.ledger_group='REAL'
and    a.ledger='LOCAL'
and    a.appl_jrnl_id='BSCS'
and    a.operating_unit='GYE_999'
and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu = 0 THEN 
    secu:=ln_maxsec_g;
END IF ;        
--
insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

--select max(rownum) into secu from READ.GSI_FIN_SAFI_dr;
select nvl(max(a.transaction_line),0)  into secu 
from   READ.GSI_FIN_SAFI_DR a 
where  a.business_unit='CONEC'
and    a.ledger_group='REAL'
and    a.ledger='LOCAL'
and    a.appl_jrnl_id='BSCS'
and    a.operating_unit='GYE_999'
and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu = 0 THEN 
    secu:=ln_maxsec_g;
END IF ;        
--

insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

/*21/04/2010*/
--DESCUENTOS--QUITAR DESPUES EL NVL DEL  CAMPO (g.cta_desc) PORQUE  NO VA  
--select max(rownum) into secu from READ.GSI_FIN_SAFI_dr;
select nvl(max(a.transaction_line),0)  into secu 
from   READ.GSI_FIN_SAFI_DR a 
where  a.business_unit='CONEC'
and    a.ledger_group='REAL'
and    a.ledger='LOCAL'
and    a.appl_jrnl_id='BSCS'
and    a.operating_unit='GYE_999'
and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu = 0 THEN 
    secu:=ln_maxsec_g;
END IF ;        
--
insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,NVL(trim(g.cta_desc),' '),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(g.descuento ) valor ,(g.descuento ) valor,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim('DES '||glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and  g.descuento>0;
commit;
--DESCUENTOS
/*21/04/2010*/


--SE AGREGO NVL  CLS DRO QUITAR DESPUES
select NVL (sum(foreign_amount),0) into totg from  READ.GSI_FIN_SAFI_DR where OPERATING_UNIT = 'GYE_999';

--select max(rownum) into secu from READ.GSI_FIN_SAFI_dr;

IF totg <> 0 THEN
  select nvl(max(a.transaction_line),0)  into secu 
  from   READ.GSI_FIN_SAFI_DR a 
  where  a.business_unit='CONEC'
  and    a.ledger_group='REAL'
  and    a.ledger='LOCAL'
  and    a.appl_jrnl_id='BSCS'
  and    a.operating_unit='GYE_999'
  and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
  --
  IF  secu = 0 THEN 
      secu:=ln_maxsec_g;
  END IF ;        
  --

  insert into READ.GSI_FIN_SAFI_DR
  select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
  trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
  'USD',' ','USD','CRRNT',1,1,(totg * -1),(totg * -1),0,
  ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
  ' ',' ',' ','N',0,' '
  from dual;


  commit;
END IF ;

-----------------------------------------UIO--NORMAL---------

--select max(rownum) into secu from READ.GSI_FIN_SAFI_dr;
select nvl(max(a.transaction_line),0) into secu 
from   READ.GSI_FIN_SAFI_DR a 
where  a.business_unit='CONEC'
and    a.ledger_group='REAL'
and    a.ledger='LOCAL'
and    a.appl_jrnl_id='BSCS'
and    a.operating_unit='GYE_999'
and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu = 0 THEN 
    secu:=ln_maxsec_g;
END IF ;        
--

insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BU'||fechacierreR2,rownum+ln_maxsec_u,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+ln_maxsec_u,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';
commit;
--
--select max(rownum) into secu2 from READ.GSI_FIN_SAFI_dr;
select  nvl(max(a.transaction_line),0)  into secu2 
from READ.GSI_FIN_SAFI_DR a 
where a.business_unit='CONEC'
and   a.ledger_group='REAL'
and   a.ledger='LOCAL'
and   a.appl_jrnl_id='BSCS'
and   a.operating_unit='UIO_999'
and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu2 = 0 THEN 
    secu2:=ln_maxsec_u;
END IF ;        
--

insert into  READ.GSI_FIN_SAFI_DR
select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),--aqui fue el error
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

--select max(rownum) into secu2 from READ.GSI_FIN_SAFI_dr;
select  nvl(max(a.transaction_line),0) into secu2 
from READ.GSI_FIN_SAFI_DR a 
where a.business_unit='CONEC'
and   a.ledger_group='REAL'
and   a.ledger='LOCAL'
and   a.appl_jrnl_id='BSCS'
and   a.operating_unit='UIO_999'
and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu2 = 0 THEN 
    secu2:=ln_maxsec_u;
END IF ;        
--

insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;


--DESCUENTOS  /*21/04/2010*/--SE LE  AGREGO  EL  NVL ( g.cta_desc ) QUITAR DESPUES 
--select max(rownum) into secu2 from READ.GSI_FIN_SAFI_dr;
select  nvl(max(a.transaction_line),0)  into secu2 
from READ.GSI_FIN_SAFI_DR a 
where a.business_unit='CONEC'
and   a.ledger_group='REAL'
and   a.ledger='LOCAL'
and   a.appl_jrnl_id='BSCS'
and   a.operating_unit='UIO_999'
and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
--
IF  secu2 = 0 THEN 
    secu2:=ln_maxsec_u;
END IF ;        
--

insert into READ.GSI_FIN_SAFI_DR
select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,NVL(trim(g.cta_desc),' '),' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(g.descuento) valor,(g.descuento) valor,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim('DESC'||glosa),
' ',' ',' ','N',0,' '
from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and  g.descuento>0;
commit;
--DESCUENTOS /*21/04/2010*/

---SE AGREGO  NVL  QUITA DESPUES CLS DRO
      
select NVL(sum(y.foreign_amount),0) into totu from  READ.GSI_FIN_SAFI_DR y where OPERATING_UNIT = 'UIO_999';
---select max(rownum) into secu2 from READ.GSI_FIN_SAFI_dr;

IF  totu  <>  0  THEN 

  select  nvl(max(a.transaction_line),0)  into secu2 
  from READ.GSI_FIN_SAFI_DR a 
  where a.business_unit='CONEC'
  and   a.ledger_group='REAL'
  and   a.ledger='LOCAL'
  and   a.appl_jrnl_id='BSCS'
  and   a.operating_unit='UIO_999'
  and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
  --
  IF  secu2 = 0 THEN 
      secu2:=ln_maxsec_u;
  END IF ;        
  --
  insert into READ.GSI_FIN_SAFI_DR
  select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
  trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
  'USD',' ','USD','CRRNT',1,1,(totu * -1),(totu * -1),0,
  ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
  ' ',' ',' ','N',0,' '
  from dual;
  commit;
END IF;
--obtengo el valor para el cuadre
select sum(monetary_amount) into lv_cuadre from read.GSI_FIN_SAFI_DR t;
-- copio a finnaciero
 resultado:= lv_cuadre || ' <--> ' ||lv_cuadre2;
--- valido que no existan diferencias
if lv_cuadre =0  then
    --normales

  --- SOLO IVA 12%
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS''))';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;

  --- SOLO CICLO
  lvSentencia := null;
  lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
  exec_sql(lvSentencia);
  commit;

 --IVA 12% + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS'')) ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;




   --EXP  + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''EXP''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''004 - EXENTO'')) ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;

    --CARGOS Y CREDITOS   + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I0''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */  A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */  k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''005 - CARGOS'',''006 - CREDITOS'')) ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;
--**DESCUENTOS    12/01/2011



 --- SOLO IVA 12%
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;

  --- SOLO CICLO
  lvSentencia := null;
  lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
  exec_sql(lvSentencia);
  commit;

 --IVA 12% + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
  lvSentencia :=  lvSentencia || ' from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;---12/01/2011

-- GRABA DATOS A FINANCIERO

-- 12012011

 --DESCOMETARIAR DESPUES CLS  DRO 
  insert into PS_JGEN_ACCT_ENTRY@finsys
  --select * from read.gsi_fin_safi_dr t;
   select  business_unit,transaction_id, transaction_line,ledger_group,ledger,accounting_dt,
    appl_jrnl_id, business_unit_gl, fiscal_year, accounting_period, journal_id,journal_date,journal_line,
    account,altacct, deptid, operating_unit, product, fund_code, class_fld, program_code, budget_ref,
    affiliate, affiliate_intra1, affiliate_intra2, chartfield1, chartfield2, chartfield3, project_id,
    currency_cd, statistics_code, foreign_currency, rt_type, rate_mult, rate_div, monetary_amount,
    foreign_amount, statistic_amount, movement_flag, doc_type, doc_seq_nbr, doc_seq_date, jrnl_ln_ref,
    line_descr, iu_sys_tran_cd,iu_tran_cd,iu_anchor_flg,gl_distrib_status,process_instance
  from read.GSI_FIN_SAFI_DR t;
  

   insert into sysadm.ps_pr_jgen_acct_en@finsys
   select * from  ps_pr_jgen_acct_en_tmp_dr;
 --DESCOMETARIAR DESPUES CLS  DRO 

-- 12012011

-- GRABA DATOS A FINANCIERO
--****************************



  commit;
  resultado:= resultado || 'se copio correctamente las cuentas normales ;)';

 
 regun.send_mail.mail@colector('gsibilling@claro.com.ec','mgavilanes@conecel.com,lmedina@conecel.com','tescalante@conecel.com','GSIFacturacion@conecel.com','Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  '||lvfechactual,'Saludos,'||CHr(13)||CHr(13)|| 'Se informa, se actualiz� los registros de financiero del cierre de facturaci�n a la fecha '||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).'||chr(13)||'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'SIS GSI-Billing.'||CHr(13)||CHr(13)||'Conecel.S.A - America Movil.');
---
 commit;
 resultado:= resultado || 'SE COPIO LA INFORMACION CORRECTAMENTE';
 pn_bandera:=0;
else
  resultado:= 'No se pudo copiar a la tabla PS_JGEN_ACCT_ENTRY por que no cuadro  RESULTADO = ' || lv_cuadre ;
 --- regun.send_mail.mail@colector('lanchundia@conecel.com','lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','Saludos,'||CHr(13)||CHr(13)|| 'ERROR en la actualizaci�n de cuentas SAFI','No cuadro el campo monetary amount de la Tabla Read.gsi_fin_safi :'||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. ' ||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
  commit;
  resultado:= resultado|| 'ERROR REVISE POR FAVOR NO CUADRO';
  pn_bandera:=-1;
  
end if;

ELSE
  resultado:= 'No se pudo copiar informacion a la tabla read.pruebafinsys,por favor revise la ejecucion del procedimiento GRABA_SAFI_LAN';     
  pn_bandera:= 1;

END IF;

end GRABA_SAFI_LAN;
------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
PROCEDURE GRABA_SAFI_CIA_REL(pdFechCierrePeriodo in date, resultado out varchar2,pn_bandera in out number) is
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    : Procedimiento que  permite grabar 
--             transacciones hacia
--             Peoplesoft los clientes de compania
--             Relacionada. 
-------------------------------------------------------
-----pdFechCierrePeriodo 24/mm/yyyy o 08/mm/yyyy
-----pdFechCierreReport  31/mm/yyyy o 15/mm/yyyy
-----glosa = Facturaci�n consumo celular del 24/11/05 al 23/12/05
-----descuentos 21/04/2010
-----Graba cuentas que facturan solo IVA 12%, solo CICLO y CICLO + IVA 12%
lvSentencia    varchar2(18000);
lnMesFinal number;
lnanoFinal number;
lndiaFinal varchar2(2);
lvFecha varchar2(10);
lvfechanterior varchar(12);
lvfechactual varchar(12);
glosa varchar2(200);
glosa_exentos varchar2(200);
lv_cuadre number;
lv_cuadre2 number;
duio number;
dgye number;
fechacierreP date;
fechacierreR date;
fechacierreR2 varchar2(20);
fechacierreR3 varchar2(20);
totu number;
totg number;
secu number;
secu2 number;
--5779 cls dro ini
secu_cia_rel_gye  number;
secu_cia_rel_uio  number;
totg_cia_rel_g    number;  
totg_cia_rel_u    number;
lv_cuadre_cia_rel number;
ln_maxsec_g       number;
ln_maxsec_u       number;
ln_maxsec_g1      number;
ln_maxsec_u1      number;
ln_registros      number;
lv_transaccion_id_gye varchar2(30);
ld_fecha_acct     date;

CURSOR verifica_pruebasys IS
 select count(*) from read.pruebafinsys_dr;
--
CURSOR TOTAL_GYE_RELACIONADA IS
  select NVL (sum(foreign_amount),0) valor_g , pr_id_cia_rel  
  from  READ.GSI_FIN_SAFI_DR where OPERATING_UNIT = 'GYE_999'
  group  by  pr_id_cia_rel;
--
CURSOR TOTAL_UIO_RELACIONADA IS
  select NVL (sum(foreign_amount),0) valor_u, pr_id_cia_rel  
  from  READ.GSI_FIN_SAFI_DR where OPERATING_UNIT = 'UIO_999'
  group  by  pr_id_cia_rel;
--
ln_tot_cia_g_rel      number;
ln_tot_cia_u_rel      number;
ln_linea   number:=0;
---5779 Fin

CURSOR descX is
 select r.ctapsoft,r.cta_dscto  from read.cob_servicios r ;

BEGIN

fechacierreP := to_date(pdFechCierrePeriodo, 'dd/mm/yyyy');
fechacierreR := to_date(sysdate, 'dd/mm/yyyy');
fechacierreR2 := trim(to_char(sysdate, 'yyyymmdd'));
fechacierreR3 := trim(to_char(sysdate+1, 'yyyymmdd'));
lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
lndiaFinal  := to_char(pdFechCierrePeriodo, 'DD');
lnanoFinal  := to_number(to_char(pdFechCierrePeriodo, 'YYYY'));
lvfechanterior:=to_char(add_months(pdFechCierrePeriodo,-1),'dd/mm/yyyy');
lvfechactual:=to_char(pdFechCierrePeriodo,'dd/mm/yyyy'); 
glosa:='Facturaci�n consumo celular del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
glosa_exentos:='Fact_Excenta consumo celular  del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
lvSentencia := 'truncate  table read.pruebafinsys_dr';
--lvSentencia := 'delete from  read.pruebafinsys_dr';
lvFecha :=to_char(pdFechCierrePeriodo, 'DDMMYYYY');
exec_sql(lvSentencia);



select decode(lndiaFinal,'02','04','08','02','15','03','24','01') into lndiaFinal  from dual;
commit;
lvSentencia := null;
--CLS DRO.
lvSentencia := 'insert into read.pruebafinsys_dr  ' ||
--'select /* + rule */ h.tipo,h.nombre,h.producto,sum(h.valor) valor ,sum(h.descuento) descuento,h.cost_desc compa�ia , max(h.ctaclblep) ' ||
--' , null  CUENTA_DESC,h.customer_id from co_fact_'|| lvFecha ||
--  ' h    where  h.customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
--                  '    WHERE cte.exempt_status = ''A'') ' ||
--  ' and  h.customer_id in (SELECT  g.customer_id  FROM  REGISTRA_CLI_CIA_TMP g where g.customer_id=h.customer_id)   ' ||                  
--  '   group by h.tipo,h.nombre,h.producto,h.cost_desc,h.customer_id';
--exec_sql(lvSentencia);
--commit;
'select /* + rule */ h.tipo,h.nombre,h.producto,sum(h.valor) valor ,sum(h.descuento) descuento,h.cost_desc compa�ia , max(h.ctaclblep) ' ||
' , null  CUENTA_DESC,h.customer_id,g.cod_cia_rel from co_fact_'|| lvFecha ||
  ' h , REGISTRA_CLI_CIA_TMP g  where  h.customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
  ' and  h.customer_id =g.CUSTOMER_ID   ' ||                  
  '   group by h.tipo,h.nombre,h.producto,h.cost_desc,h.customer_id,g.cod_cia_rel';
exec_sql(lvSentencia);
commit;

---CLS  DRO - verifica si se cargo informaci�n la tabla read.pruebafinsys
 open  verifica_pruebasys;
 fetch verifica_pruebasys  into  ln_registros;
 close verifica_pruebasys;
---
IF ln_registros > 0  THEN 

for rd in descX loop
update read.pruebafinsys_dr f set f.cta_desc=rd.cta_dscto where f.ctactble=rd.ctapsoft;

end loop;
commit;

  lvSentencia := null;
 
   --Se obtiene la m�xima secuencia de la estrutura nueva
   -- �ra  poder asignar a los nuevos registros a obtenerse
   -- Por Compania Relacionada.
   --GYE
    select  nvl(max(a.transaction_line),0)  
            into ln_maxsec_g 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and  a.ledger_group='REAL'
    and  a.ledger='LOCAL'
    and  a.appl_jrnl_id='BSCS'
    and  a.Transaction_Id='BG'||fechacierreR2
    and  a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');

    --UIO
    select  nvl(max(a.transaction_line),0)  
            into ln_maxsec_u 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and  a.ledger_group='REAL'
    and  a.ledger='LOCAL'
    and  a.appl_jrnl_id='BSCS'
    and  a.Transaction_Id='BU'||fechacierreR2
    and  a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    
lvSentencia := 'truncate table READ.GSI_FIN_SAFI_DR';
--lvSentencia := ' delete from READ.GSI_FIN_SAFI_DR';
exec_sql(lvSentencia);
commit;

lvSentencia := ' truncate table ps_pr_jgen_acct_en_tmp_dr ';
--lvSentencia := ' delete from ps_pr_jgen_acct_en_tmp_dr ';
exec_sql(lvSentencia);
commit;

lvSentencia := 'truncate table READ.GSI_FIN_SAFI_EXCENTOS';
--lvSentencia := ' delete from READ.GSI_FIN_SAFI_EXCENTOS';
exec_sql(lvSentencia);
commit;


    -----------GYE normal
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BG'||fechacierreR2,rownum+ln_maxsec_g,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+ln_maxsec_g,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.ctactble <> '410102';
    commit;
    --
    --select max(rownum)+ ln_maxsec_g into secu from READ.GSI_FIN_SAFI_dr;
    select nvl(max(a.transaction_line),0)  into secu 
    from   READ.GSI_FIN_SAFI_DR a 
    where  a.business_unit='CONEC'
    and    a.ledger_group='REAL'
    and    a.ledger='LOCAL'
    and    a.appl_jrnl_id='BSCS'
    and    a.operating_unit='GYE_999'
    and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --
    IF  secu = 0 THEN 
        secu:=ln_maxsec_g;
    END IF ;        
    -- 
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.producto in ('TARIFARIO','BULK','PYMES')
    and g.ctactble = '410102';
    commit;
    --
    --select max(rownum)+ ln_maxsec_g into secu from READ.GSI_FIN_SAFI_dr;
    select nvl(max(a.transaction_line),0)  into secu 
    from   READ.GSI_FIN_SAFI_DR a 
    where  a.business_unit='CONEC'
    and    a.ledger_group='REAL'
    and    a.ledger='LOCAL'
    and    a.appl_jrnl_id='BSCS'
    and    a.operating_unit='GYE_999'
    and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --
    IF  secu = 0 THEN 
        secu:=ln_maxsec_g;
    END IF ;        
    --    
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.producto='AUTOCONTROL'
    and g.ctactble = '410102';

    commit;
    /*21/04/2010*/
    --DESCUENTOS--QUITAR DESPUES EL NVL DEL  CAMPO (g.cta_desc) PORQUE  NO VA  
  
    -- select max(rownum)+ ln_maxsec_g into secu from READ.GSI_FIN_SAFI_dr;
    select nvl(max(a.transaction_line),0)  into secu 
    from   READ.GSI_FIN_SAFI_DR a 
    where  a.business_unit='CONEC'
    and    a.ledger_group='REAL'
    and    a.ledger='LOCAL'
    and    a.appl_jrnl_id='BSCS'
    and    a.operating_unit='GYE_999'
    and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --
    IF  secu = 0 THEN 
        secu:=ln_maxsec_g;
    END IF ;        
    --   
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,NVL(trim(g.cta_desc),' '),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(g.descuento ) valor ,(g.descuento ) valor,0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim('DES '||glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and  g.descuento>0;
    

    commit;
    --DESCUENTOS
    /*21/04/2010*/
    --SE AGREGO NVL  CLS DRO QUITAR DESPUES
     select nvl(max(a.transaction_line),0)   into secu 
     from   READ.GSI_FIN_SAFI_DR a 
     where  a.business_unit='CONEC' 
     and    a.ledger_group='REAL'
     and    a.ledger='LOCAL'
     and    a.appl_jrnl_id='BSCS'
     and    a.operating_unit='GYE_999'
     and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
      --
     IF  secu = 0 THEN 
          secu:=ln_maxsec_g;
     END IF ;        
     
    FOR  G IN  TOTAL_GYE_RELACIONADA  LOOP 
      ln_linea:=ln_linea + 1;
      insert into READ.GSI_FIN_SAFI_DR VALUES ('CONEC','BG'||fechacierreR2,(ln_linea + secu) ,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),trim(lnMesFinal),'NEXT',trim(fechacierreR),(ln_linea+secu),'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','USD',' ','USD','CRRNT',1,1,(G.valor_g * -1),(G.valor_g * -1),0,' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),' ',' ',' ','N',0,G.pr_id_cia_rel);
      commit;
    END LOOP;
      
    --
    --    
    ---------------------------------------------------------------------------------------------------------------------

    -----------------------------------------UIO--NORMAL-----------------------------------------------------------------

    --select max(rownum) + ln_maxsec_g into secu from READ.GSI_FIN_SAFI_dr;
    select nvl(max(a.transaction_line),0)  into secu 
    from   READ.GSI_FIN_SAFI_DR a 
    where  a.business_unit='CONEC'
    and    a.ledger_group='REAL'
    and    a.ledger='LOCAL'
    and    a.appl_jrnl_id='BSCS'
    and    a.operating_unit='GYE_999'
    and    a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --

    --Se agrego estructura imagen de la GSI_FIN_SAFI_dr
    --select max(rownum) into secu_cia_rel_gye from READ.GSI_FIN_SAFI_CIA_REL;
    --

    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BU'||fechacierreR2,rownum+ln_maxsec_u,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+ln_maxsec_u,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.ctactble <> '410102';
   
    commit;
    ----------------------------------------------------------------------------------------------------------------------
    select  nvl(max(a.transaction_line),0)  into secu2 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and   a.ledger_group='REAL'
    and   a.ledger='LOCAL'
    and   a.appl_jrnl_id='BSCS'
    and   a.operating_unit='UIO_999'
    and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --
    IF  secu2 = 0 THEN 
        secu2:=ln_maxsec_u;
    END IF ;        
    --
   insert into  READ.GSI_FIN_SAFI_DR
    select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),--aqui fue el error
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.producto in ('TARIFARIO','BULK','PYMES')
    and g.ctactble = '410102';
    commit;
    -----------------------------------------------------------------------------------------------------------------------
    select  nvl(max(a.transaction_line),0)  into secu2 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and   a.ledger_group='REAL'
    and   a.ledger='LOCAL'
    and   a.appl_jrnl_id='BSCS'
    and   a.operating_unit='UIO_999'
    and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --   
    IF  secu2 = 0 THEN 
        secu2:=ln_maxsec_u;
    END IF ;        
    --
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and g.producto='AUTOCONTROL'
    and g.ctactble = '410102';

    commit;
    ----------------------------------------------------------------------------------------------------------------------

    --DESCUENTOS  /*21/04/2010*/--SE LE  AGREGO  EL  NVL ( g.cta_desc ) QUITAR DESPUES 
    --select max(rownum) + ln_maxsec_u  into secu2 from READ.GSI_FIN_SAFI_dr;
    
    select  nvl(max(a.transaction_line),0)  into secu2 
    from READ.GSI_FIN_SAFI_DR a 
    where a.business_unit='CONEC'
    and   a.ledger_group='REAL'
    and   a.ledger='LOCAL'
    and   a.appl_jrnl_id='BSCS'
    and   a.operating_unit='UIO_999'
    and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
    --
    IF  secu2 = 0 THEN 
        secu2:=ln_maxsec_u;
    END IF ;        
    --
    
    insert into READ.GSI_FIN_SAFI_DR
    select 'CONEC','BU'||fechacierreR2,rownum+secu2,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
    trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2,NVL(trim(g.cta_desc),' '),' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'USD',' ','USD','CRRNT',1,1,(g.descuento) valor,(g.descuento) valor,0,
    ' ','CBSCS',' ',trim(fechacierreP),' ',trim('DESC'||glosa),
    ' ',' ',' ','N',0,g.PR_ID_CIA_REL
    from read.pruebafinsys_dr  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
    and  g.descuento>0 ;

    commit;
    ----------------------------------------------------------------------------------------------------------------------   
    ---SE AGREGO  NVL  QUITA DESPUES CLS DRO
   ln_linea:=0;
   select  nvl(max(a.transaction_line),0)  into secu2 
   from READ.GSI_FIN_SAFI_DR a 
   where a.business_unit='CONEC'
   and   a.ledger_group='REAL'
   and   a.ledger='LOCAL'
   and   a.appl_jrnl_id='BSCS'
   and   a.operating_unit='UIO_999'
   and   a.accounting_dt = to_date(lvfechactual,'dd/mm/yyyy');
   -- 
   IF  secu2 = 0 THEN 
       secu2:=ln_maxsec_u;
   END IF ;        
   
   FOR  U IN  TOTAL_UIO_RELACIONADA  LOOP 
     ln_linea:=ln_linea + 1;
     insert into READ.GSI_FIN_SAFI_DR VALUES ('CONEC','BU'||fechacierreR2,(ln_linea + secu2) ,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),trim(lnMesFinal),'NEXT',trim(fechacierreR),(ln_linea + secu2),'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','USD',' ','USD','CRRNT',1,1,(U.valor_u * -1),(U.valor_u * -1),0,' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),' ',' ',' ','N',0,U.pr_id_cia_rel);
     commit;
   END LOOP;    
    -----------------------------------------------------------------------------------------------------------------------
    --obtengo el valor para el cuadre

        
    select sum(monetary_amount) into lv_cuadre from read.GSI_FIN_SAFI_DR t;
    
   /* -- Se agrego estructura imagen de la GSI_FIN_SAFI_dr
    select sum(monetary_amount) into lv_cuadre_cia_rel from read.GSI_FIN_SAFI_CIA_REL x;
   */     
    -- copio a finnaciero
     resultado:= lv_cuadre || ' <--> ' ||lv_cuadre2;
    --- valido que no existan diferencias
    if lv_cuadre =0  then
        --normales
      --- SOLO IVA 12%
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL  ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
      lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
      lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS''))';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;

      --- SOLO CICLO
      lvSentencia := null;
      lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL  ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
      exec_sql(lvSentencia);
      commit;

     --IVA 12% + CICLO
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
      lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
      lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS'')) ';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;

       --EXP  + CICLO
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''EXP''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
      lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
      lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''004 - EXENTO'')) ';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;

        --CARGOS Y CREDITOS   + CICLO
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I0''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */  A.CUENTA ';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
      lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */  k.ctaclblep from co_fact_'|| lvFecha ;
      lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''005 - CARGOS'',''006 - CREDITOS'')) ';
      lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;
      --**DESCUENTOS    12/01/2011

      --- SOLO IVA 12%
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL  ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
      lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;

      --- SOLO CICLO
      lvSentencia := null;
      lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || '  from read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
      lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
      exec_sql(lvSentencia);
      commit;

     --IVA 12% + CICLO
      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || ' from  read.GSI_FIN_SAFI_DR t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
      lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;---12/01/2011
     ---
     --- Para aquellos clientes que  no tienen ni iva ni ciclo  ingresan  por este bloque codigo 

      lvSentencia := null;
      lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp_dr ';
      lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
      lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO ,t.PR_ID_CIA_REL ';
      lvSentencia :=  lvSentencia || ' from  read.GSI_FIN_SAFI_DR t where t.account not in  ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
      lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' )';
      lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp_dr x)';
      lvSentencia :=  lvSentencia || ' and t.pr_id_cia_rel <> '' ''';    
      lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
      exec_sql(lvSentencia);
      commit;---12/01/2011

-- GRABA DATOS A FINANCIERO

-- 12012011

 --DESCOMETARIAR DESPUES CLS  DRO 
  insert into PS_JGEN_ACCT_ENTRY@finsys
  select  business_unit,transaction_id, transaction_line,ledger_group,ledger,accounting_dt,
    appl_jrnl_id, business_unit_gl, fiscal_year, accounting_period, journal_id,journal_date,journal_line,
    account,altacct, deptid, operating_unit, product, fund_code, class_fld, program_code, budget_ref,
    affiliate, affiliate_intra1, affiliate_intra2, chartfield1, chartfield2, chartfield3, project_id,
    currency_cd, statistics_code, foreign_currency, rt_type, rate_mult, rate_div, monetary_amount,
    foreign_amount, statistic_amount, movement_flag, doc_type, doc_seq_nbr, doc_seq_date, jrnl_ln_ref,
    line_descr, iu_sys_tran_cd,iu_tran_cd,iu_anchor_flg,gl_distrib_status,process_instance
  from read.GSI_FIN_SAFI_DR t;

 insert into sysadm.ps_pr_jgen_acct_en@finsys
  select * from ps_pr_jgen_acct_en_tmp_dr;
 --DESCOMETARIAR DESPUES CLS  DRO 

-- 12012011
  commit;
-- GRABA DATOS A FINANCIERO
--****************************


 
  resultado:= resultado || 'se copio correctamente las cuentas normales ;)';


 
 regun.send_mail.mail@colector('gsibilling@claro.com.ec','mgavilanes@conecel.com,lmedina@conecel.com','tescalante@conecel.com','GSIFacturacion@conecel.com','Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  '||lvfechactual,'Saludos,'||CHr(13)||CHr(13)|| 'Se informa, se actualiz� los registros de financiero del cierre de facturaci�n a la fecha '||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).'||chr(13)||'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'SIS GSI-Billing.'||CHr(13)||CHr(13)||'Conecel.S.A - America Movil.');
---
 commit;
 resultado:= resultado || 'SE COPIO LA INFORMACION CORRECTAMENTE';
 pn_bandera:=0;
else
  resultado:= 'No se pudo copiar a la tabla PS_JGEN_ACCT_ENTRY por que no cuadro  RESULTADO = ' || lv_cuadre ;
 --- regun.send_mail.mail@colector('lanchundia@conecel.com','lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','Saludos,'||CHr(13)||CHr(13)|| 'ERROR en la actualizaci�n de cuentas SAFI','No cuadro el campo monetary amount de la Tabla Read.gsi_fin_safi :'||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. ' ||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
  commit;
  resultado:= resultado|| 'ERROR REVISE POR FAVOR NO CUADRO';
  pn_bandera:=-1;


end if;

ELSE
  resultado:= 'Se copio la informacion correctamente , pero no se encontraron coincidencias de Clientes que pertenezcan a una Compa�ia Relacionados en la tabla '||'co_fact_'|| lvFecha||', que se ejecuto desde el procedimiento GRABA_SAFI_CIA_REL';     
  pn_bandera:=1;

END IF;
end GRABA_SAFI_CIA_REL;
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------

-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Registra  informaci�n de
--            de los clientes de cia relacionadas a trav�s
--            de la ejecuci�n del JOB.
-------------------------------------------------------
PROCEDURE REGISTRA_CLI_CIA_REL_TMP IS
--------CURSORES----------
--Datos de los Clientes de companias relacionadas.
CURSOR c_datos_cia_rel IS
 SELECT c.* 
 FROM   cl_param_cia_rel_bscs c
 WHERE  c.estado='A';
-- Datos del Clientes
CURSOR c_datos_clientes (cv_ruc varchar2) IS
 SELECT g.customer_id,g.custcode, g.cssocialsecno 
 FROM   customer_all g 
 WHERE /*g.customer_id > 0 
 and   */g.cssocialsecno= cv_ruc;
-- and    g.cstype='a';
  
-------VARIABLES -------
 lv_programa varchar2(1000):='GENERA_ASIENTO_PS.REGISTRA_CLI_CIA_REL_TMP --> ';  
 lv_mensaje  varchar2(18000);
 lv_error     varchar2(2000);
 ln_encontro  number:=0;
 le_error     exception;
------------------------ 
BEGIN
--Registra en bitacora .
  lv_mensaje:='Inicio de ejecuci�n del proceso de carga de datos a la tabla REGISTRA_CLI_CIA_TMP';
  INSERTA_BITACORA_CIA_REL(SYSDATE,lv_programa||lv_mensaje,lv_error);
  commit;
 FOR  i in c_datos_cia_rel  LOOP
   FOR k in c_datos_clientes(i.RUC)  LOOP
       IF trim(i.RUC) = trim(K.CSSOCIALSECNO) THEN 
         -- Se  verifica si ya fue ingresado en la estrutura relacional.
         VERIFICA_REGISTRO_RELACIONAL(k.CUSTOMER_ID,K.CUSTCODE,i.RUC,i.COD_RELACIONADA,ln_encontro,lv_error);
         IF lv_error IS NOT NULL THEN
             RAISE le_error;
         ELSE
             --Si devuelve [0] el  registro no existe y se ingresa, si es [1] el registro existe y no se ingresa
             IF ln_encontro=0 THEN 
                INSERTA_CLIENTES_CIA_REL(k.CUSTOMER_ID,K.CUSTCODE,i.RUC,i.COD_RELACIONADA,lv_error);
                IF lv_error IS NOT NULL THEN
                  RAISE le_error;
                END IF;
             END IF; 
        END IF; 
      END IF;
   END LOOP;
 END LOOP;
 COMMIT;
 lv_mensaje:='Fin de ejecuci�n del proceso de carga de datos a la tabla REGISTRA_CLI_CIA_TMP';
  INSERTA_BITACORA_CIA_REL(SYSDATE,lv_programa||lv_mensaje,lv_error);
  COMMIT;
 EXCEPTION
   WHEN le_error THEN 
     --Registra en bitacora el error ocurrido.
     INSERTA_BITACORA_CIA_REL(SYSDATE,lv_programa,lv_error);
     commit;
END;
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Registra temporalmente informaci�n de
--            de los clientes de cia relacionadas.
-------------------------------------------------------
PROCEDURE INSERTA_CLIENTES_CIA_REL (pn_customer_id  in  number,
                                    pv_custcode      in  varchar2,
                                    pv_ruc_ced      in  varchar2,
                                    pv_cod_cia_rel  in  varchar2,
                                    pv_error        out varchar2) IS


lv_programa varchar2(1000):='GENERA_ASIENTO_PS.INSERTA_CLIENTES_CIA_REL --> ';  
BEGIN

INSERT INTO REGISTRA_CLI_CIA_TMP VALUES (pn_customer_id,
                                         pv_custcode,    
                                         pv_ruc_ced,    
                                         pv_cod_cia_rel);

EXCEPTION 
 WHEN  OTHERS  THEN 
 pv_error:= 'Error:'||lv_programa||Sqlerrm; 

END;
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Permite realizar una verificarci�n de registros  
--            a la tabla REGISTRA_CLI_CIA_TMP y evitar que se
--            guarden  registro duplicados. 
-------------------------------------------------------
PROCEDURE VERIFICA_REGISTRO_RELACIONAL (pn_customer_id  in  number,
                                        pv_custcode     in  varchar2,
                                        pv_ruc_ced      in  varchar2,
                                        pv_cod_cia_rel  in  varchar2,
                                        pn_encontro     in out varchar2,
                                        pv_error        out varchar2) IS
cursor c_datos_relacional(cn_customer_id  number,
                          cv_custcode     varchar2,
                          cv_ruc_ced      varchar2,
                          cv_cod_cia_rel  varchar2) IS
  SELECT count(*)
  FROM REGISTRA_CLI_CIA_TMP k
  WHERE  k.customer_id=cn_customer_id
  AND    k.custcode=cv_custcode
  AND    k.ruc_ced=cv_ruc_ced
  AND    k.cod_cia_rel=cv_cod_cia_rel;
lv_programa varchar2(1000):='GENERA_ASIENTO_PS.VERIFICA_REGISTRO_RELACIONAL --> ';  
BEGIN

 open  c_datos_relacional (pn_customer_id,pv_custcode,pv_ruc_ced,pv_cod_cia_rel);
 fetch c_datos_relacional into  pn_encontro;
 close c_datos_relacional;

EXCEPTION 
 WHEN  OTHERS  THEN 
 pv_error:= 'Error:'||lv_programa||Sqlerrm; 

END;
-------------------------------------------------------
-- Proyecto: [5779] - Automatizaci�n de Conciliaci�n
--                    de Intercompanias.
-- Lider CLS : Allan Endara. 
-- Autor     : CLS Darwin Rosero.
-- Motivo    :Registra los errores ocurridos en la 
--            Bitacora.
-------------------------------------------------------
PROCEDURE INSERTA_BITACORA_CIA_REL (pd_fecha        in     date,
                                    pv_programa     in     varchar2,
                                    pv_error        in out varchar2) IS

lv_programa varchar2(1000):='GENERA_ASIENTO_PS.INSERTA_BITACORA_CIA_REL --> ';  
BEGIN

INSERT INTO BITACORA_CIA_REL VALUES (pd_fecha,
                                     pv_programa,    
                                     pv_error);
EXCEPTION 
 WHEN OTHERS  THEN 
   null;
END;


end GENERA_ASIENTO_PS;
/
