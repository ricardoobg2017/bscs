CREATE OR REPLACE package COK_PROCESS_REPORT_OLD is

    PROCEDURE TRUNCATABLACUADRE ( pdFechCierrePeriodo     date,
                                  pdNombre_tabla	        varchar2,
                                  pd_lvMensErr           out varchar2  ) ;
    --
     PROCEDURE InsertaDatosCustomer ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	        varchar2,
                                      pdTabla_Customer        varchar2,
                                      pdLink                  varchar2,
                                      pd_lvMensErr       out  varchar2);
    --                                      
    Procedure  Llena_DatosCliente (pdNombreTabla   varchar2,
                                   pd_lvMensErr  out  varchar2) ;
    --
    PROCEDURE UPD_FPAGO_PROD_CCOSTO ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	        varchar2,
                                      pdTabla_Customer        varchar2,
                                      pd_lvMensErr       out  varchar2  );
    --
    PROCEDURE CONV_CBILL_TIPOFPAGO ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	       varchar2,
                                      pd_lvMensErr      out  varchar2);
    --
    PROCEDURE Upd_Disponible_Totpago ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                      pd_lvMensErr      out  varchar2);
    --
    /*PROCEDURE UPD_DISPONIBLE_TOTPAGOXXXX ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pd_lvMensErr      out  varchar2);    */
    --
    PROCEDURE Llena_NroFactura       ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	         varchar2,
                                       pdTabla_OrderHdr        varchar2 );
    PROCEDURE SeteaCeroNull  ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pdLink                in varchar2,
                                       pd_lvMensErr      out  varchar2);    
    PROCEDURE Credito_a_Disponible  ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pdLink                in varchar2,
                                       pd_lvMensErr      out  varchar2);    
    --
    procedure EJECUTA_SENTENCIA (pv_sentencia in  varchar2,
                                 pv_error     out varchar2);
    --
    procedure CALCULA_FECHA_PERIODO (pdFechCierrePeriodo   date,
                                     pdNombre_Tabla        varchar2);
    --
    PROCEDURE CALCULA_MORA ( pdFechCierrePeriodo     date,
                             pdNombre_tabla			     varchar2  );
    --
    PROCEDURE BALANCEO ( pdFechCierrePeriodo  date,
                         pdNombre_tabla			 varchar2,
                         pd_lvMensErr       out  varchar2  ) ;
    PROCEDURE ACTUALIZA_CMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);                         
    PROCEDURE ACTUALIZA_CONSMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);                         
    PROCEDURE ACTUALIZA_CREDTSEP(pdFechCierrePeriodo in date, 
                                     pdNombre_tabla in varchar2,
                                     pdMensErr out varchar2);
    PROCEDURE ACTUALIZA_PAGOSREZAGO(pdFechCierrePeriodo in date, 
                                     pdNombre_tabla in varchar2,
                                     pdMensErr out varchar2);
    PROCEDURE ACTUALIZA_PAGOSSEP(pdFechCierrePeriodo in date, 
                                     pdNombre_tabla in varchar2,
                                     pdMensErr out varchar2);                                                          
    PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);
    --
    PROCEDURE LLENA_BALANCES ( pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pdLink                varchar2,
                               pd_lvMensErr       out  varchar2  );
    PROCEDURE LLENA_BALANCES_XXX ( pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pdLink                varchar2,
                               pd_lvMensErr       out  varchar2  );    
    PROCEDURE CalculaTotalDeuda            ( pdFechCierrePeriodo     date,
                                             pdNombre_tabla	     varchar2,
                                             pd_lvMensErr        out varchar2  );
    
    PROCEDURE Copia_Fact_Actual ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pd_lvMensErr      out  varchar2);
    PROCEDURE INIT_RECUPERADO ( pdNombre_tabla			    varchar2);
    PROCEDURE LLENA_PAGORECUPERADO(  pvFechIni                varchar2,
                                     pvFechFin                varchar2,
                                     pdNombre_tabla			     varchar2);
    PROCEDURE LLENA_CREDITORECUPERADO ( pvFechIni     varchar2,
                                        pvFechFin     varchar2,
                                        pdNombre_tabla			    varchar2);                                  
    
    PROCEDURE Llena_Debito_Periodo       ( pdFechCierrePeriodo     date,
                                             pdNombre_tabla	         varchar2,
                                             pd_lvMensErr     out    varchar2);
    
    PROCEDURE Llena_total_deuda_cierre ( pdFechCierrePeriodo     date,
                                         pdNombre_tabla	       varchar2,
                                         pd_lvMensErr      out  varchar2);
    
    PROCEDURE LLENATIPO ( pdFechCierrePeriodo     date,
                          pdNombre_tabla			    varchar2  );
    
    PROCEDURE PROCESA_CUADRE(pdFechaPeriodo        in date,
                             pdNombre_Tabla        in varchar2,
                             pdTabla_OrderHdr      in varchar2,
                             pdTabla_cashreceipts  in varchar2,
                             pdTabla_Customer      in varchar2,
                             pdLink                in varchar2,
                             pd_lvMensErr          out varchar2);
    
    FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2;  
    
    FUNCTION CREA_TABLA_CUADRE (pdFechaPeriodo in  date, 
                         pv_error       out varchar2,
                         pvNombreTabla  in  varchar2) RETURN NUMBER;
    
    PROCEDURE MAIN (pdFechCierrePeriodo in date);
    
end COK_PROCESS_REPORT_OLD;
/
CREATE OR REPLACE package body COK_PROCESS_REPORT_OLD is

  -- Variables locales
  gv_funcion     varchar2(30);
  gv_mensaje     varchar2(500);
  ge_error       exception;

    ------------------------------
    --     TRUNCATABLACUADRE
    ------------------------------
    PROCEDURE TruncaTablaCuadre ( pdFechCierrePeriodo     date,
                                  pdNombre_tabla	        varchar2,
                                  pd_lvMensErr           out varchar2  ) is
    --
    lvSentencia            varchar2(1000);
    lvMensErr                   varchar2(1000);
    --
    Begin
       --
       lvSentencia :='truncate table '|| pdNombre_Tabla;
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       --
       pd_lvMensErr := lvMensErr;
    end;
    
    ------------------------------
    --     INSERTADATOS_CUSTOMER
    ------------------------------
     PROCEDURE InsertaDatosCustomer ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	        varchar2,
                                      pdTabla_Customer        varchar2,
                                      pdLink                  varchar2,
                                      pd_lvMensErr       out  varchar2) is
    --
    lvSentencia            varchar2(500);
    lvMensErr                   varchar2(1000);
    --
    Begin
       --
       
       lvSentencia:='insert /*+ APPEND */ into '||pdNombre_tabla||pdLink ||' NOLOGGING (custcode,customer_id) '||
                    'select custcode,customer_id from '|| pdTabla_Customer ||' a where a.paymntresp=''X''';       
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);             
       commit;       
       --
       pd_lvMensErr:= lvMensErr;
    end;

    ---------------------------
    -- Llena_DatosCliente
    ---------------------------
    Procedure  Llena_DatosCliente (pdNombreTabla   varchar2,
                                   pd_lvMensErr  out  varchar2) is

        CURSOR DATOSPERS IS
           SELECT /*+ rule */ CUSTOMER_ID, CCFNAME,CCLNAME,CSSOCIALSECNO,nvl(CCCITY,'x') cccity ,nvl(CCSTATE,'x') ccstate,CCNAME,CCTN,CCTN2,DIR2,TRADENAME
           FROM READ.DATOSCLIENTES;
     
        lv_sentencia_upd            varchar2(500);
        lvMensErr                   varchar2(1000);     
        lII                         number;

    Begin
        
        lII := 0;
        FOR b IN DATOSPERS LOOP
           lv_sentencia_upd:=  ' update '|| pdnombretabla || ' a ' ||
                          ' set  ' ||
                          ' a.nombres   =' || ''''||b.ccfname||''' ,' ||
                          ' a.apellidos =' || ''''||b.cclname||''' ,' ||
                          ' a.ruc       =' || ''''||b.cssocialsecno||''' ,' ||
                          ' a.canton    =' || ''''||b.cccity ||''' ,' ||
                          ' a.provincia =' || ''''||b.ccstate||''' ,' ||
                          ' a.direccion =' || ''''||b.ccname||''' ,' ||
                          ' a.direccion2=' || ''''||b.dir2||''' ,' ||
                          ' a.cont1     =' || ''''||b.cctn||''' ,' ||
                          ' a.cont2     =' || ''''||b.cctn2||''' ,' ||
                          ' a.trade     =' || ''''||b.tradename||'''' ||
                          ' where a.customer_id = ' || b.customer_id  ;
           EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
           lII:=lII+1;
           if lII = 5000 then
              lII := 0;
              commit;
           end if;                             
        END LOOP;
        pd_lvMensErr:= lvMensErr;
        commit;

    End;
    
    -----------------------------------------------------
    --     UPD_FPAGO_PROD_CCOSTO
    --     Se llenan algunos datos generales del cliente
    -----------------------------------------------------
    PROCEDURE UPD_FPAGO_PROD_CCOSTO ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	        varchar2,
                                      pdTabla_Customer        varchar2,
                                      pd_lvMensErr       out  varchar2  ) is
    --
    lvSentencia            varchar2(500);
    lvMensErr                   varchar2(1000);
    --
           CURSOR Registros IS
           select /*+ rule */ d.customer_id,k.bank_id,k.bankname,nvl(i.producto,'x') producto,nvl(j.cost_desc,'x') cost_desc, g.bankaccno, g.valid_thru_date, g.accountowner, d.csactivated, d.cstradecode
           from 
           customer_all    d,  
           payment_all     g,  
           read.COB_GRUPOS I,  
           COSTCENTER      j, 
           bank_all        k 
           where
           d.customer_id=g.customer_id and
           act_used ='X' and
           D.PRGCODE=I.PRGCODE and 
           j.cost_id=d.costcenter_id and
           g.bank_id=k.bank_id;
    --
    Begin
       -- se actualizan la forma de pago, el producto y centro de costo
       FOR b IN Registros LOOP    
          lvSentencia:='update '|| pdNombre_tabla ||
                       ' set forma_pago='||b.bank_id||','||
                       ' des_forma_pago='''||b.bankname||''','||
                       ' producto='''||b.producto||''','||
                       ' tarjeta_cuenta='''||b.bankaccno||''','||
                       ' fech_expir_tarjeta='''||b.valid_thru_date||''','||
                       ' tipo_cuenta='''||b.accountowner||''','||
                       ' fech_aper_cuenta= '||'to_date('''||to_char(b.csactivated,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                       ' grupo='''||b.cstradecode||''','||
                       ' region='''||b.cost_desc||''''||
                       ' where customer_id='||b.customer_id;
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       END LOOP;
       pd_lvMensErr := lvMensErr;
       commit;

    End;
    ------------------------------
    --     CONV_CBILL_TIPOFPAGO
    ------------------------------
    PROCEDURE CONV_CBILL_TIPOFPAGO ( pdFechCierrePeriodo     date,
                                      pdNombre_tabla	       varchar2,
                                      pd_lvMensErr      out  varchar2) is
    --
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    --
    CURSOR RPT_PAYMENT IS
           select rpt_cod_bscs, rpt_payment_id, rpt_nombrefp from ope_rpt_payment_type;
    --
    Begin
       --
       -- se actualizan los valores para tipo de forma de pago
       FOR b in RPT_PAYMENT LOOP
          lvSentencia:='update ' ||pdNombre_Tabla ||
                       ' set tipo_forma_pago='||b.rpt_payment_id||','||
                       ' des_tipo_forma_pago='''||b.rpt_nombrefp||''''||
                       ' where forma_pago='||b.rpt_cod_bscs;
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);       END LOOP;
       commit; 
       pd_lvMensErr := lvMensErr;
       --
    End;
    ------------------------------------------------------------------------
    --     UPD_DISPONIBLE_TOTPAGO
    --     Procediento que llena los siguientes campos
    --     totpagos: contiene el total de pagos realizados hasta el corte
    --     creditos: total de creditos realizados hasta el corte
    --     disponible: pagos + creditos utiles para el balanceo
    ------------------------------------------------------------------------
    PROCEDURE Upd_Disponible_Totpago ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pd_lvMensErr      out  varchar2) is

    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;

     CURSOR Pagos is
     select /*+ rule */ customer_id,sum(cachkamt_pay) as TotPago 
     --from   read.cashreceipts_allsep
     from   read.cashreceipts_alloct
     where  caentdate  < pdFechCierrePeriodo
     group  by customer_id
     having sum(cachkamt_pay) <>0;
    
     --No tomo en cuenta los creditos de julio y Agosto
     CURSOR creditos is
     select /*+ rule */ customer_id,sum(ohinvamt_doc) as Totcred 
     --from read.orderhdr_allsep 
     from  read.orderhdr_alloct
     where ohstatus  ='CM'
     AND NOT TO_CHAR(TRUNC(OHENTDATE)) IN ('24-JUL-03','24-AUG-03','24-SEP-03', '24-OCT-03')
     group by customer_id
     having sum(ohinvamt_doc)<>0; 

    Begin

       FOR B IN PAGOS LOOP
           lvSentencia:='update ' ||pdNombre_Tabla ||
                        ' set totpagos='||B.TotPago||','||
                        ' DISPONIBLE='||B.TotPago||
                        ' WHERE CUSTOMER_ID='||B.CUSTOMER_ID;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       end loop;
       pd_lvMensErr := lvMensErr;
       commit;
       
       --ACTUALIZA VALORES DE CREDITO, EL VALOR DISPONIBLE DEL CLIENTE ES =PAGOS+CREDITOS
       FOR B IN creditos LOOP
           lvSentencia:='update ' ||pdNombre_Tabla ||
                        ' set creditos=('||B.Totcred*-1||'),'||
                        ' disponible = disponible+'||B.Totcred*-1||
                        ' WHERE CUSTOMER_ID='||B.CUSTOMER_ID;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       end loop;
       pd_lvMensErr := lvMensErr;       
       commit;
       
    End;

    ------------------------------
    --     Llena_NroFactura
    ------------------------------
    PROCEDURE Llena_NroFactura       ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_OrderHdr        varchar2 ) is

    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);


           CURSOR FACTURA IS
           select customer_id,OHREFNUM
           from orderhdr_all
           where ohstatus  ='IN'
           and ohentdate = pdFechCierrePeriodo;

    Begin

       FOR b IN FACTURA LOOP    
           lvSentencia := 'update ' ||pdNombre_Tabla ||
                          ' set FACTURA='||B.OHREFNUM||
                          ' where customer_id='||b.customer_id;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       END LOOP;
       commit;

    End;
    
    
    PROCEDURE SeteaCeroNull  ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pdLink                in varchar2,
                                       pd_lvMensErr      out  varchar2) is
    
    mes		                      varchar2(2);
    anio                        varchar2(4);
    mes_recorrido_char          varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);                                       
    mes_recorrido               number;
    nro_mes                     number;
    lvMensErr                   varchar2(1000);
    
    BEGIN
    
       mes     := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),3,2);
       nro_mes := to_number(mes);       
       mes_recorrido := 1;
       while mes_recorrido <= nro_mes loop
             --  Los valores quedaron nulos es porque no generaron factura
             nombre_campo := 'balance_'||mes_recorrido;
             lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                                ' set   '||nombre_campo ||' = 0'||
                                ' where '||nombre_campo|| ' is null';
             EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
             commit;
             mes_recorrido:=mes_recorrido+1;
       end loop;
       
                                              
    END;                                                                              
    ---------------------------------------------------------------
    --    Adiciona_credito_a_IN 
    --    Procedimiento que coloca los creditos como parte de
    --    la factura para efectos de cuadratura ya que BSCS
    --    no lo estaba haciendo
    ---------------------------------------------------------------
    PROCEDURE Credito_a_Disponible  ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pdTabla_Cashreceipts    varchar2,
                                       pdTabla_OrderHdr        varchar2,
                                       pdTabla_Customer        varchar2,
                                       pdLink                in varchar2,
                                       pd_lvMensErr      out  varchar2) is

    --
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;
    
    cursor cur_creditos(pdFecha date) is 
    select /*+ rule */ 
      d.customer_id,d.custcode,c.ohrefnum,f.cccity,
      f.ccstate,g.bank_id,I.PRODUCTO,
      j.cost_desc,a.otglsale as Cble,
      sum(otamt_revenue_gl) as Valor,
      h.tipo,h.nombre
      from 
      ordertrailer A, --items de la factura
      mpusntab b,  --maestro de servicios
      orderhdr_all c, --cabecera de facturas
      customer_all d, -- maestro de cliente
      ccontact_all f, -- informaci�n demografica de la cuente
      payment_all g,  --Forna de pago
      COB_SERVICIOS h, --formato de reporte
      COB_GRUPOS I, --NOMBRE GRUPO
      COSTCENTER j
      where
      a.otxact=c.ohxact and 
      --c.ohentdate=to_date('24/10/2003','DD/MM/YYYY') and
      c.ohentdate = pdFecha and
      C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
      c.customer_id=d.customer_id and 
      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
      instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
      instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
      d.customer_id= f.customer_id and
      f.ccbill='X' and
      g.customer_id=d.customer_id and
      act_used ='X' and
      h.servicio=b.sncode and
      h.ctactble=a.otglsale AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo='006 - CREDITOS'
      group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, a.otglsale, a.otxact, h.tipo,h.nombre
      
     UNION
     
     SELECT /*+ rule */ 
     d.customer_id,d.custcode,c.ohrefnum,f.cccity,
     f.ccstate,g.bank_id,I.PRODUCTO,
     j.cost_desc,A.glacode as Cble,
     sum(TAXAMT_TAX_CURR) AS VALOR, 
     h.tipo,h.nombre
     FROM ORDERTAX_ITEMS A, TAX_CATEGORY B, orderhdr_all C, customer_all d, ccontact_all f, payment_all g, COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER J
     WHERE 
       c.ohxact=a.otxact and
       --c.ohentdate=to_date('24/10/2003','DD/MM/YYYY') and
       c.ohentdate = pdFecha and
       C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
       c.customer_id=d.customer_id and 
       taxamt_tax_curr>0 and
       A.TAXCAT_ID=B.TAXCAT_ID and
       d.customer_id=f.customer_id and
       f.ccbill='X' and 
       g.customer_id=d.customer_id and
       act_used ='X' and
       h.servicio=A.TAXCAT_ID and
      h.ctactble=A.glacode AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo='006 - CREDITOS'
     group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, A.glacode, h.tipo,h.nombre;

    cursor cur_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July
               
    Begin
    
         for i in cur_periodos loop
             lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
             lII := 0;
             for b in cur_creditos(i.cierre_periodo) loop
                 lvSentencia:='update ' ||pdNombre_Tabla ||
                              --' set cre_julio='||b.val||','||
                              ' set creditos = creditos + '||b.valor*-1||','||
                              ' disponible = disponible + '||b.valor*-1||--','||                             
                              --' balance_'||lnMes||' = balance_'||lnMes||' + '||b.valor*-1|| 
                              ' WHERE CUSTOMER_ID='||B.CUSTOMER_ID;
                 EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                 lII:=lII+1;
                 if lII = 2000 then
                    lII := 0;
                    commit;
                 end if;                 
             end loop;
             commit;
         end loop;
       
    End;
    
    ------------------------------
    --     EJECUTA_SENTENCIA
    ------------------------------
    procedure EJECUTA_SENTENCIA (pv_sentencia in varchar2,
                                 pv_error    out varchar2
                                 ) is
       x_cursor          integer;
       z_cursor          integer;
       name_already_used exception;
       pragma exception_init(name_already_used, -955);
    begin
       x_cursor := dbms_sql.open_cursor;
       DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
       z_cursor := DBMS_SQL.EXECUTE(x_cursor);
       DBMS_SQL.CLOSE_CURSOR(x_cursor);
    EXCEPTION
       WHEN NAME_ALREADY_USED THEN
          IF DBMS_SQL.IS_OPEN(x_cursor) THEN
             DBMS_SQL.CLOSE_CURSOR(x_cursor);
          END IF;
          pv_error := null;
       WHEN OTHERS THEN
          IF DBMS_SQL.IS_OPEN(x_cursor) THEN
             DBMS_SQL.CLOSE_CURSOR(x_cursor);
          END IF;
          pv_error := sqlerrm;
    END EJECUTA_SENTENCIA;  

procedure CALCULA_FECHA_PERIODO (pdFechCierrePeriodo   date,
                                 pdNombre_Tabla        varchar2)  is

--pdFechCierrePeriodo :=  24/08/2003

-- variables
mes_inicial	                 varchar2(2);
anio_inicial	               varchar2(4);
mes_final_char               varchar2(2);
anio_final_char              varchar2(4);
lv_nombre_tabla              varchar2(30);
--
v_cursor           	     number;
v_sentencia_string           varchar2(1000);
--
mes_final                    number;
anio_final                   number;
nro_mes_inicial              number;
nro_anio_inicial             number;
--
fecha_inicial	               date;
fecha_final	                 date;
--
Begin
   --
   --  encero la tabla de las fechas
   --
   --  abrir el cursor
   v_cursor:= DBMS_SQL.open_cursor;
   v_sentencia_string := 'TRUNCATE TABLE ope_rpt_cab_proc';
   DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);  -- analiza la sentencia
   DBMS_SQL.CLOSE_CURSOR(v_cursor);      --  cerrar el cursor
   --
   --lv_nombre_tabla := 'cuadre_'||to_char(pdFechCierrePeriodo, 'ddmmyyyy');
   lv_nombre_tabla := pdNombre_Tabla;
   --
   mes_inicial :=  substr(to_char(pdFechCierrePeriodo, 'ddmmyyyy'),3,2);
   anio_inicial:=  substr(to_char(pdFechCierrePeriodo, 'ddmmyyyy'),5,4);
   --
   nro_mes_inicial := to_number(mes_inicial);
   nro_anio_inicial:= to_number(anio_inicial);
   --
   -- Calculo para obtener mes y anio final segun fecha inicial
   --
   mes_final  := nro_mes_inicial + 1;
   anio_final := nro_anio_inicial;
   --
   if    mes_inicial  = '12' then -- Debo rebajar el mes y el a�o en 1
         mes_final := 1 ;
         anio_final  := nro_anio_inicial + 1 ;
   end if;
   -- 
   -- convierto a char mes y anio inicial
   --
   mes_final_char := to_char(mes_final);
   if   length(mes_final) < 2 then
        mes_final_char := '0'||mes_final;
   end if;
   anio_final_char := to_char(anio_final);
   --
   -- Armo fechas del periodo a guardar
   --
   fecha_inicial := to_date('24/'||mes_inicial||'/'||anio_inicial,'dd/mm/yyyy');
   fecha_final   := to_date('23/'||mes_final_char||'/'||anio_final_char,'dd/mm/yyyy');
   -- 
   -- inserta el registro
   --
   insert into ope_rpt_cab_proc (fecha_inicio,  fecha_fin,   fecha_cierre,   fecha_proceso, usuario, nombre_tabla)
                        values  (fecha_inicial, fecha_final, pdFechCierrePeriodo, sysdate, user, lv_nombre_tabla);
   commit;
   --
end CALCULA_FECHA_PERIODO;    
    
    
    -----------------------------
    --  CALCULA MORA

    -----------------------------

    PROCEDURE CALCULA_MORA ( pdFechCierrePeriodo     date,
                             pdNombre_tabla			    varchar2  ) is

    --variables
    mes		                      varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);
    
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;
    --
    Begin
       --
       -- obtengo el n�mero del mes 
       mes     := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),3,2);
       nro_mes := to_number(mes);
       --
       -- Obtener la maxima mora segun el mes
       --
       max_mora := 0;
       if    nro_mes = 1 then
             max_mora := 0;
       elsif nro_mes = 2 then
             max_mora := 30;
       elsif nro_mes = 3 then
             max_mora := 60;
       elsif nro_mes = 4 then
             max_mora := 90;
       elsif nro_mes = 5 then
             max_mora := 120;
       elsif nro_mes = 6 then
             max_mora := 150;
       elsif nro_mes = 7 then
             max_mora := 180;
       elsif nro_mes = 8 then
             max_mora := 210;
       elsif nro_mes = 9 then
             max_mora := 240;
       elsif nro_mes = 10 then
             max_mora := 270;
       elsif nro_mes = 11 then
             max_mora := 300;
       elsif nro_mes = 12 then
             max_mora := 330;
          end if;
       --
       -- Por cada mes que recorro, inicia con enero y va en aumento hasta llegar al nro_mes
       -- mientras que  la maxima mora, disminuye de 30 en 30
       -- La sentecia de Actualizaci�n se arma con el nombre del campo de la condici�n 
       -- ('Balance_') concatenado con mes que recorro
       -- Se actualiza en la tabla.
       --
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes
       --
       while mes_recorrido <= nro_mes
       loop
           --
           nombre_campo  :=  'balance_'|| mes_recorrido;
           lv_sentencia_upd:=  ' Update '|| pdNombre_tabla || 
                             ' set  mora = '|| max_mora||
                             ' where  '|| nombre_campo || ' > 0 and mora =0';
           mes_recorrido := mes_recorrido + 1 ;
           max_mora      := max_mora - 30;
           --
           -- Actualiza mediante cursor y sentencia armada.
           --
           v_cursorUpdate := dbms_sql.open_cursor;
           dbms_sql.parse(v_cursorUpdate, lv_sentencia_upd, dbms_sql.v7);
           v_Row_Update   := dbms_Sql.execute(v_cursorUpdate);
           dbms_sql.close_cursor(v_cursorUpdate);
           --
       End loop;
       --
       commit;
       --
       
       
    END CALCULA_MORA;

    
    -----------------------
    --  BALANCEO
    ----------------------
    
    PROCEDURE BALANCEO ( pdFechCierrePeriodo  date,
                         pdNombre_tabla			 varchar2,
                         pd_lvMensErr       out  varchar2  ) is
    --
    -- variables 
    --
    val_fac_          varchar2(20);
    lv_sentencia	    varchar2(1000);
    lv_sentencia_upd  varchar2(1000);
    v_sentencia       varchar2(1000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    nombre_campo      varchar2(20);
    lvMensErr         varchar2(1000);
    --
    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    v_cursor_asigna   number;	
    v_Dummy           number;
    v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;
    --
    Begin
       --
       mes := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'), 3, 2) ;
       nro_mes := to_number(mes);
       --
       -- Defino los campos para la tabla
       --
       if    mes = '01' then
             lv_campos := 'balance_1';
       elsif mes = '02' then
             lv_campos := 'balance_1, balance_2';
       elsif mes = '03' then
             lv_campos := 'balance_1, balance_2, balance_3';
       elsif mes = '04' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4';
       elsif mes = '05' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
       elsif mes = '06' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
       elsif mes = '07' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
       elsif mes = '08' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
       elsif mes = '09' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
       elsif mes = '10' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
       elsif mes = '11' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
       elsif mes = '12' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
       end if;
    

       v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;
    
       --
       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       --
       lv_sentencia:= ' select c.rowid, customer_id, disponible, '|| lv_campos||
                      ' from ' || pdNombre_tabla ||' c';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30 );
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id );
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible  );
    
       --
       -- define variables de salida dinamica
       --
           if    nro_mes >= 1 then
                 dbms_sql.define_column(v_cursorId, 4, val_fac_1); 
           end if;
           if    nro_mes >= 2 then
                 dbms_sql.define_column(v_cursorId, 5, val_fac_2);
           end if;
           if    nro_mes >= 3 then
                 dbms_sql.define_column(v_cursorId, 6, val_fac_3);
           end if;
           if    nro_mes >= 4 then
                 dbms_sql.define_column(v_cursorId, 7, val_fac_4);
           end if;
           if    nro_mes >= 5 then
                 dbms_sql.define_column(v_cursorId, 8, val_fac_5);
           end if;
           if    nro_mes >= 6 then
                 dbms_sql.define_column(v_cursorId, 9, val_fac_6);
           end if;
           if    nro_mes >= 7 then
                 dbms_sql.define_column(v_cursorId, 10, val_fac_7);
           end if;
           if    nro_mes >= 8 then
                 dbms_sql.define_column(v_cursorId, 11, val_fac_8);
           end if;
           if    nro_mes >= 9 then
                 dbms_sql.define_column(v_cursorId, 12, val_fac_9);
           end if;
           if    nro_mes >= 10 then
                 dbms_sql.define_column(v_cursorId, 13, val_fac_10);
           end if;
           if    nro_mes >= 11 then 
                 dbms_sql.define_column(v_cursorId, 14, val_fac_11);
           end if;
           if    nro_mes = 12 then
                 dbms_sql.define_column(v_cursorId, 15, val_fac_10);                   
           end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);
    
       -- 1. Para extraer los datos del cursor
       Loop 
    
          --  inicio de armar la sentencias para aCtualizar en la tabla de cuadre segun el rowid
          --
          total_deuda_cliente := 0;
          lv_sentencia_upd := '';
          lv_sentencia_upd := 'update '|| pdNombre_tabla ||' set ';
          --
          -- si no tiene datos sale
          --
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;
          --   
          -- recupero valores en los campos  y disminuyo valores de factura segun monto disponible
          --
          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );
    
          if    nro_mes >= 1 then
                 dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_1,0);
                 if  wc_disponible  >= aux_val_fact then
                     --  900        >   100
                     val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                     --
                 else
                     --  900 dispo       <  1000 ene
                     val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 1 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_1 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , '; -- Armando sentencia para UPDATE
          end if;
          --
          if    nro_mes >= 2 then
                 dbms_sql.column_value(v_cursorId, 5, val_fac_2);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_2,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 2 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_2 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 3 then
                 dbms_sql.column_value(v_cursorId, 6, val_fac_3);  -- recupero el valor en la variable 
                 aux_val_fact   := nvl(val_fac_3,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 3 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_3 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 4 then
                 dbms_sql.column_value(v_cursorId, 7, val_fac_4);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_4,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 4 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_4 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 5 then
                 dbms_sql.column_value(v_cursorId, 8, val_fac_5);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_5,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 5 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_5 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
           end if;
           if    nro_mes >= 6 then
                 dbms_sql.column_value(v_cursorId, 9, val_fac_6);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_6,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 6 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_6 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 7 then
                 dbms_sql.column_value(v_cursorId, 10, val_fac_7);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_7,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 7 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_7 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 8 then
                 dbms_sql.column_value(v_cursorId, 11, val_fac_8);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_8,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 8 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_8 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 9 then
                 dbms_sql.column_value(v_cursorId, 12, val_fac_9);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_9,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 9  and wc_disponible > 0 then  --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_9 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 10 then
                 dbms_sql.column_value(v_cursorId, 13, val_fac_10);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_10,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 10  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_10 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 11 then
                 dbms_sql.column_value(v_cursorId, 14, val_fac_11);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_11,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 11  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_11 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes = 12 then
                 dbms_sql.column_value(v_cursorId, 15, val_fac_12);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_12,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 if  nro_mes = 12  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_12 := wc_disponible  * -1 ;
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
           end if;    
           -- Quito la coma y finalizo la sentencia
           lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
           lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';
    
          -- actualizo mediante el cursor
          EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    
       End Loop; -- 1. Para extraer los datos del cursor
       commit;

       dbms_sql.close_cursor(v_CursorId);

    END BALANCEO;
    
    PROCEDURE ACTUALIZA_CMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_cm is
           select customer_id,sum(ohinvamt_doc) valor
           --from read.orderhdr_allsep
           from read.orderhdr_alloct
           where ohstatus  ='CM' 
           --and ohentdate >= add_months(pdFechCierrePeriodo, -1)--to_date ('24/09/2003','DD/MM/YYYY')
           and ohentdate >= add_months(pdFechCierrePeriodo, -1)
           and ohentdate < pdFechCierrePeriodo--to_date('24/10/2003','DD/MM/YYYY')
           group by customer_id;
    BEGIN
         lII:=0;
        for i in cu_cm loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set cmoct = cmoct + '||i.valor||                          
                          ' where customer_id = '||i.customer_id;
                          --' where id_cliente = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;        
    END ACTUALIZA_CMSEP;
    
    PROCEDURE ACTUALIZA_CONSMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_consmsep is
           select customer_id, sum(valor) val
           --from   read.septiembre2003
           from  read.octubre2003
           where tipo != '006 - CREDITOS'
           group by customer_id;
               
    BEGIN
         lII:=0;
        for i in cu_consmsep loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set consmoct = '||i.val||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_CONSMSEP;    
    
    PROCEDURE ACTUALIZA_CREDTSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_credtsep is
           select customer_id, sum(valor) val
           --from   read.septiembre2003
           from  read.octubre2003
           where tipo= '006 - CREDITOS'
           group by customer_id;
               
    BEGIN
         lII:=0;
        for i in cu_credtsep loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set credtoct = '||i.val||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_CREDTSEP;


    PROCEDURE ACTUALIZA_PAGOSREZAGO(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    
    lvMaxPagoAgo                 number;
    lvMaxPagoSep                 number;
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_pagossep(ppagoini number, ppagofin number) is
    select  customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor_periodo
    from read.cashreceipts_allsep o
    -- no utilizo la tabla actual porque es probable
    -- que al levantar el PTH 
    --from cashreceipts_all o
    where caxact > ppagoini
    and   caxact < ppagofin
    and   cachkdate >= to_date('2003/08/24 00:00', 'yyyy/MM/dd hh24:mi')
    group by customer_id;
                                                                      
    BEGIN                                 
        -- se calcula el maximo pago de agosto
        select max(caxact) into lvMaxPagoAgo from read.cashreceipts_allago;
        
        -- se calcula el maximo pago de Septiembre    
        select max(caxact) into lvMaxPagoSep from read.cashreceipts_allsep;
        
        lII := 0;
        for i in cu_pagossep(232376,lvMaxPagoAgo) loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set pagorezago = '||i.valor_periodo||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_PAGOSREZAGO;

    
    PROCEDURE ACTUALIZA_PAGOSSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    
    lvMaxPagoAgo                 number;
    lvMaxPagoSep                 number;
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_pagossep(ppagoini number, ppagofin number) is
    select  customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor_periodo
    --from read.cashreceipts_allsep o
    from read.cashreceipts_alloct o
    -- no utilizo la tabla actual porque es probable
    -- que al levantar el PTH 
    --from cashreceipts_all o
    where caxact > ppagoini
    and   caxact < ppagofin
    group by customer_id;
    --having customer_id = 48163;
                                                                      
    BEGIN                                 
        -- se calcula el maximo pago de agosto
        select max(caxact) into lvMaxPagoAgo from read.cashreceipts_allsep;
        --select max(caxact) into lvMaxPagoAgo from read.cashreceipts_allago;
        
        -- se calcula el maximo pago de Septiembre    
        select max(caxact) into lvMaxPagoSep from read.cashreceipts_alloct;
        --select max(caxact) into lvMaxPagoSep from read.cashreceipts_allsep;
        
        lII := 0;
        for i in cu_pagossep(lvMaxPagoAgo, Lvmaxpagosep) loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set pagosoct = '||i.valor_periodo||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_PAGOSSEP;
    
    -----------------------------------------------
    --           ACTUALIZA_SALDOANT                                         
    -----------------------------------------------
    PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is

    lvSentencia         varchar2(1000);
    lvMensErr           varchar2(1000);
    lII                 number;
    
    /* CBR 
       cambiado por definici�n de OPE Jorge Garces
       al corte de septiembre debe tomarse como saldo
       anterior el prevbalance de Septiembre ya que 
       ese saldo se ve afectado con pagos o creditos*/    
    cursor cur_current is
    select customer_id, cscurbalance
    --from read.customer_allago;
    from read.customer_allsep;
    
    BEGIN
         lII := 0;
         for i in cur_current loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set saldoant = '||nvl(i.cscurbalance,0)||','||
                          ' saldosep = '||nvl(i.cscurbalance,0)||
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
         end loop;
         commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm; 
    END ACTUALIZA_SALDOANT;

    ---------------------------------------------------------------
    ------------------------------
    --     LLENA_BALANCES
    ------------------------------
    PROCEDURE LLENA_BALANCES  ( pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pdLink                varchar2,
                               pd_lvMensErr       out  varchar2  ) is

    
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;

    mes		                      varchar2(2);
    anio                        varchar2(4);
    mes_recorrido_char          varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);
    
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;    
    
    cursor cur_creditos(pdFecha date) is 
    select /*+ rule */ 
      d.customer_id,d.custcode,c.ohrefnum,f.cccity,
      f.ccstate,g.bank_id,I.PRODUCTO,
      j.cost_desc,a.otglsale as Cble,
      sum(otamt_revenue_gl) as Valor,
      h.tipo,h.nombre
      from 
      ordertrailer A, --items de la factura
      mpusntab b,  --maestro de servicios
      orderhdr_all c, --cabecera de facturas
      customer_all d, -- maestro de cliente
      ccontact_all f, -- informaci�n demografica de la cuente
      payment_all g,  --Forna de pago
      COB_SERVICIOS h, --formato de reporte
      COB_GRUPOS I, --NOMBRE GRUPO
      COSTCENTER j
      where
      a.otxact=c.ohxact and 
      --c.ohentdate=to_date('24/10/2003','DD/MM/YYYY') and
      c.ohentdate = pdFecha and
      C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
      c.customer_id=d.customer_id and 
      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
      instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
      instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
      d.customer_id= f.customer_id and
      f.ccbill='X' and
      g.customer_id=d.customer_id and
      act_used ='X' and
      h.servicio=b.sncode and
      h.ctactble=a.otglsale AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo!='006 - CREDITOS'
      group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, a.otglsale, a.otxact, h.tipo,h.nombre
      
     UNION
     
     SELECT /*+ rule */ 
     d.customer_id,d.custcode,c.ohrefnum,f.cccity,
     f.ccstate,g.bank_id,I.PRODUCTO,
     j.cost_desc,A.glacode as Cble,
     sum(TAXAMT_TAX_CURR) AS VALOR, 
     h.tipo,h.nombre
     FROM ORDERTAX_ITEMS A, TAX_CATEGORY B, orderhdr_all C, customer_all d, ccontact_all f, payment_all g, COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER J
     WHERE 
       c.ohxact=a.otxact and
       --c.ohentdate=to_date('24/10/2003','DD/MM/YYYY') and
       c.ohentdate = pdFecha and
       C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
       c.customer_id=d.customer_id and 
       taxamt_tax_curr>0 and
       A.TAXCAT_ID=B.TAXCAT_ID and
       d.customer_id=f.customer_id and
       f.ccbill='X' and 
       g.customer_id=d.customer_id and
       act_used ='X' and
       h.servicio=A.TAXCAT_ID and
      h.ctactble=A.glacode AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo!='006 - CREDITOS'
     group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, A.glacode, h.tipo,h.nombre;

    cursor cur_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July
               
    BEGIN
    
       mes     := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),3,2);
       nro_mes := to_number(mes);
       anio    := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),5,4);
       --
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       --
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes   
       
       while mes_recorrido <= 6
       loop
           -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           -- Armo sentencia segun mes que recorro
           nombre_campo    := 'balance_'|| mes_recorrido;
           lv_sentencia_upd:= ' Update /*+ rule */ '|| pdNombre_tabla || ' t '||
                              ' set '||nombre_campo ||' =( select /*+ rule */ ohinvamt_doc'||
                                                         ' from '|| pd_Tabla_OrderHdr||pdLink || ' o '||
                                                         ' where o.customer_id= t.customer_id'||
                                                         ' and   o.ohentdate  = to_date('||'''24/'||mes_recorrido_char||'/'||anio||''',''dd/mm/yyyy'''||')'||
                                                         ' and   o.ohstatus   = ''IN'')';
           EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);           
           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
       End loop;
       commit;

       -- se procesan a partir de Julio solo los cargos facturados    
       for i in cur_periodos loop
             lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
             lII := 0;
             for b in cur_creditos(i.cierre_periodo) loop
                 lvSentencia:='update ' ||pdNombre_Tabla ||                           
                              ' set balance_'||lnMes||' = balance_'||lnMes||' + '||b.valor|| 
                              ' WHERE CUSTOMER_ID='||B.CUSTOMER_ID;
                 EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                 lII:=lII+1;
                 if lII = 2000 then
                    lII := 0;
                    commit;
                 end if;                 
             end loop;
             commit;
       end loop;

/*       mes_recorrido := 1;
       while mes_recorrido <= nro_mes loop
             --  Los valores quedaron nulos es porque no generaron factura
             nombre_campo := 'balance_'||mes_recorrido;
             
             if  mes_recorrido = nro_mes then
                 lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                                    ' set   tipo '||' = ''1_NF'''||
                                    ' where '||nombre_campo|| ' is null';
                 EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
             end if;
                          
             lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                                ' set   '||nombre_campo ||' = 0'||
                                ' where '||nombre_campo|| ' is null';
             EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
       end loop;
       commit;*/
               
    END;
             
    PROCEDURE LLENA_BALANCES_XXX ( pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pdLink                varchar2,
                               pd_lvMensErr       out  varchar2  ) is

    --variables
    mes		                      varchar2(2);
    anio                        varchar2(4);
    mes_recorrido_char          varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);
    lvMensErr                   varchar2(1000);
    
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;
    --
    Begin
       --
       -- obtengo el n�mero del mes 
       mes     := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),3,2);
       nro_mes := to_number(mes);
       anio    := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),5,4);
       --
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       --
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes   
       --
       while mes_recorrido <= nro_mes
       loop
           -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           -- Armo sentencia segun mes que recorro
           nombre_campo    := 'balance_'|| mes_recorrido;
           lv_sentencia_upd:= ' Update /*+ rule */ '|| pdNombre_tabla || ' t '||
                              ' set '||nombre_campo ||' =( select /*+ rule */ ohinvamt_doc'||
                                                         ' from '|| pd_Tabla_OrderHdr||pdLink || ' o '||
                                                         ' where o.customer_id= t.customer_id'||
                                                         ' and   o.ohentdate  = to_date('||'''24/'||mes_recorrido_char||'/'||anio||''',''dd/mm/yyyy'''||')'||
                                                         ' and   o.ohstatus   = ''IN'')';
           EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);

           if  mes_recorrido = nro_mes then
               --  Los valores quedaron nulos es porque no generaron factura
               nombre_campo := 'balance_'||mes_recorrido;
               lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                                  ' set   tipo '||' = ''1_NF'''||
                                  ' where '||nombre_campo|| ' is null';
               EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
               pd_lvMensErr := lvMensErr;
           end if;           

           -- Si qued� nulo que actualice con cero.
           nombre_campo    := 'balance_'|| mes_recorrido;
           lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                              ' set   '||nombre_campo ||' = 0'||
                              ' where '||nombre_campo|| ' is null';
           EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
           pd_lvMensErr := lvMensErr;

           mes_recorrido := mes_recorrido + 1;
       End loop;
       commit;
       --
       -- Actualizo columna 
       --
       lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t  set TOTAL_FACT_ACTUAL  = '||nombre_campo;
       EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
       commit;

       pd_lvMensErr := lvMensErr;
    END LLENA_BALANCES_XXX;
    --------------------------
    --   CalculaTotalDeuda
    --------------------------
    PROCEDURE CalculaTotalDeuda            ( pdFechCierrePeriodo     date,
                                             pdNombre_tabla	     varchar2,
                                             pd_lvMensErr        out varchar2  ) is

    lvSentencia            varchar2(500);
    lvSentencia_Campos     varchar2(500);
    lvMensErr              varchar2(1000);
    lnMesFinal  	   number;
    --
    Begin
         lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));

         lvSentencia := 'Update ' ||pdNombre_tabla || ' set total_deuda = ';
         for  lII in 1..lnMesFinal-1 loop
              lvSentencia := lvSentencia ||' BALANCE_'||lII||' +';
         end loop;
         lvSentencia:= substr(lvSentencia,1, length(lvSentencia)-1);
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         
         pd_lvMensErr := lvMensErr;
         commit;
         pd_lvMensErr := lvMensErr;
    End;  
    --------------------------
    --   Copia_Fact_Actual
    --------------------------
    PROCEDURE Copia_Fact_Actual ( pdFechCierrePeriodo     date,
                                       pdNombre_tabla	       varchar2,
                                       pd_lvMensErr      out  varchar2) is

    --
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    --
    lnMesFinal             number;
    --
    Begin
       --
       lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));
       --
       -- Actualizo columna 
       lvSentencia:= ' Update '|| pdNombre_tabla || ' t  set TOTAL_FACT_ACTUAL  = balance_'||lnMesFinal;
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       --
       commit;
       --       
    End;

PROCEDURE INIT_RECUPERADO ( pdNombre_tabla			    varchar2) is
lvSentencia    varchar2(500);
lvMensErr      varchar2(1000);
BEGIN
     lvSentencia := 'update '||pdNombre_tabla|| 
     ' set totPagos_recuperado = 0,'||
     ' CreditoRecuperado = 0,'||
     ' DebitoRecuperado = 0';
     EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     commit;

END;    
      
    --------------------------
    --   LlenaPagoRecuperado
    --------------------------
PROCEDURE LLENA_PAGORECUPERADO ( pvFechIni     varchar2,
                                 pvFechFin     varchar2,
                                 pdNombre_tabla			    varchar2) is
-- variables
lvMensErr                    varchar2(500);
v_cursor           	         number;
v_sentencia_string           varchar2(1000);
lII                          number;

cursor regi is
   select  /*+ rule */ customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor
   from cashreceipts_all o
   where o.cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
   and o.cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
   --where o.cachkdate  between to_date('24/08/2003','dd/mm/yyyy')             
   --and to_date('23/09/2003','dd/mm/yyyy')
   group by customer_id;

BEGIN   
    -- si se trata de la fecha del cierre entonces no 
    -- existen pagos recuperados
    if to_number(substr(pvFechFin,1,2)) = 24 then
          v_sentencia_string:='update '||pdNombre_tabla|| 
                       ' set   totPagos_recuperado = 0';
          EJECUTA_SENTENCIA(v_sentencia_string, lvMensErr);
    else
        lII := 0;
        for i in regi 
        loop
          v_sentencia_string:='update '||pdNombre_tabla|| 
                       ' set   totPagos_recuperado ='||i.valor||
                       ' where customer_id='||i.customer_id;
          EJECUTA_SENTENCIA(v_sentencia_string, lvMensErr);
        end loop;
        lII:=lII+1;
        if lII = 5000 then
           lII := 0;
           commit;
        end if;
    end if;
    commit;
END;


PROCEDURE LLENA_CREDITORECUPERADO ( pvFechIni     varchar2,
                                    pvFechFin     varchar2,
                                    pdNombre_tabla			    varchar2) is
-- variables
lvMensErr                    varchar2(500);
v_cursor           	         number;
v_sentencia_string           varchar2(1000);
lII                          number;

cursor regi is
   select customer_id,sum(ohinvamt_doc) valor
   from orderhdr_all
   where ohstatus  ='CM' 
   and ohentdate >= to_date(pvFechIni, 'dd/mm/yyyy')
   and ohentdate <= to_date(pvFechFin, 'dd/mm/yyyy')
   group by customer_id;

BEGIN
    -- si se trata de la fecha del cierre entonces no 
    -- existen creditos recuperados
    if to_number(substr(pvFechFin,1,2)) = 24 then
          v_sentencia_string := 'update '||pdNombre_tabla|| 
                                ' set   CreditoRecuperado = 0';
          EJECUTA_SENTENCIA(v_sentencia_string, lvMensErr);
    else
        lII := 0;
        for i in regi 
        loop
          v_sentencia_string := 'update '||pdNombre_tabla|| 
                                ' set   CreditoRecuperado ='||i.valor||
                                ' where customer_id='||i.customer_id;
          EJECUTA_SENTENCIA(v_sentencia_string, lvMensErr);
        end loop;
        lII:=lII+1;
        if lII = 5000 then
           lII := 0;
           commit;
        end if;
    end if;
    commit;

END;   

--
   /*PROCEDURE LlenaPagoRecuperadoxx ( pdFechCierrePeriodo     date,
                                     pdNombre_tabla			     varchar2,
                                     pdTabla_cashreceipts    varchar2,
                                     pdLink                  varchar2  ) is
    --
    --variables
    nombre_campo                varchar2(30);
    lv_sentencia_upd            varchar2(1000);
    lvMensErr                   varchar2(1000);
    --
    v_CursorUpdate	            number;
    v_Row_Update	              number;
    --
    lv_fecha_inicio             date;
    lv_fecha_fin                date;
    --
    Begin
        --
        -- para obtener fecha y nombre de la tabla desde "ope_rpt_cab_proc"
        --
        Begin
      	   select fecha_inicio,    fecha_fin
      	   into   lv_fecha_inicio, lv_fecha_fin
      	   from   ope_rpt_cab_proc;
        exception
           when no_data_found then
                COK_PROCESS_REPORT_OLD.CALCULA_FECHA_PERIODO(pdFechCierrePeriodo, pdNombre_tabla);
      	        select fecha_inicio,    fecha_fin
      	        into   lv_fecha_inicio, lv_fecha_fin
      	        from   ope_rpt_cab_proc;                
        End;
        --
        nombre_campo := 'totpagos_recuperado';
           lv_sentencia_upd:= ' Update '|| pdNombre_tabla || ' t '||
                              ' set '||nombre_campo ||' =( select decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) '||
                                                         ' from '|| pdTabla_cashreceipts ||pdLink || ' o '||
                                                         ' where o.customer_id= t.customer_id'||
                                                         ' and   o.cachkdate  between to_date('''||lv_fecha_inicio||''',''dd/mm/yyyy'''||')'||
                                                                                ' and to_date('''||lv_fecha_fin||''',''dd/mm/yyyy'''||'))';
           --
           -- Actualiza mediante cursor y sentencia armada y confirma trabajo
           --
           EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
           commit;
           --
    END;
    */
    
    PROCEDURE Llena_Debito_Periodo       ( pdFechCierrePeriodo     date,
                                             pdNombre_tabla	         varchar2,
                                             pd_lvMensErr     out    varchar2) is

    --
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    --
    cursor DebitoPer is
    select /*+ rule */ customer_id, sum(valor) val 
    --from  read.septiembre2003
    from read.octubre2003
    where tipo= '005 - CARGOS'
    group by customer_id;
    --
    Begin
       -- actualiza en la tabla los Debitos del Periodo
       for b in DebitoPer loop
           lvSentencia:=' update ' ||pdNombre_Tabla ||
                        ' set DebitoRecuperado = '||B.val||
                        ' WHERE CUSTOMER_ID  = '||B.CUSTOMER_ID;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       end loop; 
       pd_lvMensErr := lvMensErr;       
       commit;
    End;  
    --------------------------
    --   Llena_total_deuda_cierre
    --------------------------
    PROCEDURE Llena_total_deuda_cierre ( pdFechCierrePeriodo     date,
                                         pdNombre_tabla	       varchar2,
                                         pd_lvMensErr      out  varchar2) is

    --
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    --
    Begin
       --
       -- Actualizo columna 
       lvSentencia:= ' Update '|| pdNombre_tabla || ' t  set TOTAL_deuda_cierre  = total_deuda + total_fact_actual';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       --
       commit;
       --       
    End;    
    --------------------------
    --   LLENATIPO
    --------------------------

    PROCEDURE LLENATIPO ( pdFechCierrePeriodo     date,
                          pdNombre_tabla			    varchar2  ) is

    --variables
    mes		                      varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);
    lvMensErr                   varchar2(1000);
    
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;
    lnMesFinal                  number;
    
    -- cursor para clientes que no generaron factura en el periodo 
    cursor cur_nofact is
    select customer_id from co_cuadre
    minus
    select customer_id from orderhdr_all where ohstatus = 'IN' and  ohentdate = pdFechCierrePeriodo;     
    
    Begin
       -- Son 5 tipos:
       --   = '1_NF'  Cartera vigente. 
       --             Clientes que no tienen factura generada. Actualizado en LLENA BALANCE
       --   = '2_SF'  Se llenara cuando el tipo sea nulo... Pero debe ser realizado al final.
       --   = '3_VNE'. Cartera Vencida. Son valores Negativos o sea saldos a favor del cliente
       --   = '5_R'.   Esto es para los clientes que tienen Mora

       /*lv_sentencia_upd:=  ' Update '|| pdNombre_tabla ||' a '|| 
                           ' set  a.tipo = ''1_NF'''||
                           ' where  a.customer_id not in (select b.customer_id from orderhdr_all b where b.ohstatus = ''IN'' and b.OHENTDATE = to_date('''||to_char(pdFechCierrePeriodo,'dd/mm/yyyy')||''',''dd/mm/yyyy''))';
       EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);*/
       for i in cur_nofact loop
         lv_sentencia_upd:=  ' Update '|| pdNombre_tabla || 
                             ' set  tipo = ''1_NF'''||
                             ' where  customer_id = '||i.customer_id;
         EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);           
       end loop;
       commit;
       
       -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
       lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));
       lv_sentencia_upd:=  ' Update '|| pdNombre_tabla || 
                           ' set  tipo = ''3_VNE'''||
                           ' where  BALANCE_'||lnMesFinal||' < 0 ';
       EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
       commit;
       
       -- Se actualiza campo tipo para saber que es Mora
       lv_sentencia_upd:=  ' Update '|| pdNombre_tabla || 
                           ' set  tipo = ''5_R'''||
                           ' where  mora > 0 ';
       EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
       commit;
       
       -- Se actualiza. Indica Valores que deben de la factura del mes actual.}
       -- CBR: 12 Nov 2003
       -- La definici�n queda que son los que que tienen solo cartera Vigente 
       -- es decir la edad de mora = 0
       /*lv_sentencia_upd:=  ' Update '|| pdNombre_tabla || 
                             ' set  tipo = ''2_SF'' where mora = 0';
                             --' where  tipo is null';
       EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
       commit;*/

end;

    ---------------------------
    -- PROCESA_CUADRE
    ---------------------------
    PROCEDURE PROCESA_CUADRE(pdFechaPeriodo        in date,
                             pdNombre_Tabla        in varchar2,
                             pdTabla_OrderHdr      in varchar2,
                             pdTabla_cashreceipts  in varchar2,
                             pdTabla_Customer      in varchar2,
                             pdLink                in varchar2,
                             pd_lvMensErr          out varchar2) is
    --
    --variables
    lvSentencia   varchar2(2000);
    lvMensErr     varchar2(1000);
    Saldo                    number;
    vBalance                 number;
    vDISPONIBLE              number;
       

    --        
    BEGIN

       -- truncamos la tabla en caso de haber informaci�n
       COK_PROCESS_REPORT_OLD.TruncaTablaCuadre(pdFechaPeriodo, pdNombre_tabla, lvMensErr);
       
       -- se insertan todos los cliente a la base de datos de la base de datos de producci�n
       COK_PROCESS_REPORT_OLD.InsertaDatosCustomer(pdFechaPeriodo, pdNombre_tabla, pdTabla_Customer, pdLink, lvMensErr);
       
       -- se actualizan los valores de pago y el disponible
       COK_PROCESS_REPORT_OLD.Upd_Disponible_TotPago(pdFechaPeriodo, pdNombre_tabla, pdTabla_OrderHdr, pdTabla_Cashreceipts, pdTabla_Customer, lvMensErr);
         
       -- procedimiento que actualiza los campos balance de saldos
       COK_PROCESS_REPORT_OLD.llena_balances(pdFechaPeriodo,pdNombre_Tabla ,pdTabla_OrderHdr,pdLink, lvMensErr);
       
       -- Creditos del 24 se colocan en disponible
       COK_PROCESS_REPORT_OLD.Credito_a_Disponible(pdFechaPeriodo,pdNombre_Tabla , pdTabla_Cashreceipts, pdTabla_OrderHdr, pdTabla_Customer, pdLink, lvMensErr);
       
       COK_PROCESS_REPORT_OLD.SeteaCeroNull(pdFechaPeriodo,pdNombre_Tabla , pdTabla_Cashreceipts, pdTabla_OrderHdr, pdTabla_Customer, pdLink, lvMensErr);
       
       -- Proceso que realiza el balanceo 
       COK_PROCESS_REPORT_OLD.balanceo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);

       -- Llena campo Total_deuda
       COK_PROCESS_REPORT_OLD.CalculaTotalDeuda(pdFechaPeriodo, pdNombre_Tabla ,  lvMensErr);
       
       -- copia el valor de la factura del ultimos mes al campo "Valor_Fact_Actual"
       COK_PROCESS_REPORT_OLD.Copia_Fact_Actual(pdFechaPeriodo, pdNombre_Tabla ,  lvMensErr);       
          
       -- Proceso que calcula la mora
       COK_PROCESS_REPORT_OLD.calcula_mora(pdFechaPeriodo, pdNombre_tabla);

       -- Proceso que realiza LlenaTipo
       COK_PROCESS_REPORT_OLD.LlenaTipo(pdFechaPeriodo, pdNombre_tabla);

       -- ACTUALIZA LOS DATOS INFORMATIVOS
       COK_PROCESS_REPORT_OLD.llena_DatosCliente (pdNombre_Tabla, lvMensErr);
       
       -- se actualizan la forma de pago, el producto y centro de costo
       COK_PROCESS_REPORT_OLD.UPd_FPago_Prod_CCosto(pdFechaPeriodo, pdNombre_tabla, pdTabla_Customer, lvMensErr);

       -- se actualizan los valores para tipo de forma de pago. Seg�n CBILL para convertir
       COK_PROCESS_REPORT_OLD.Conv_Cbill_TipoFPago(pdFechaPeriodo, pdNombre_tabla, lvMensErr);

       -- Proceso que realiza LlenaDebito_periodo
       COK_PROCESS_REPORT_OLD.Llena_Debito_periodo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);
      
    end PROCESA_CUADRE;

    ---------------------------
    -- SET_CAMPOS_MORA
    ---------------------------
    FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
    BEGIN
         lnMesFinal:= to_number(to_char(pdFecha,'MM'));
         lvSentencia:='';
         for lII in 1..lnMesFinal loop
            lvSentencia := lvSentencia||'BALANCE_'||lII||' NUMBER default 0, ';
         end loop;
         return(lvSentencia);
    END SET_CAMPOS_MORA;
    
    -----------------
    --  CREA_TABLA
    -----------------
    FUNCTION CREA_TABLA_CUADRE (pdFechaPeriodo in  date, 
                         pv_error       out varchar2,
                         pvNombreTabla  in  varchar2 ) RETURN NUMBER IS
    --
    lvSentenciaCrea     VARCHAR2(20000);
    lvMensErr           VARCHAR2(1000);
    --
    BEGIN
--           lvSentenciaCrea := 'create table CUADRE_'||to_char(pdFechaPeriodo,'ddMMyyyy')||
           lvSentenciaCrea := 'CREATE TABLE '|| pvNombreTabla ||
                          '( CUSTCODE             VARCHAR2(24),'||
                          '  CUSTOMER_ID         NUMBER,'||
                          '  REGION              VARCHAR2(30),'||                          
                          '  PROVINCIA           VARCHAR2(40),'||                          
                          '  CANTON              VARCHAR2(40),'||
                          '  PRODUCTO            VARCHAR2(30),'||
                          '  NOMBRES             VARCHAR2(40),'||
                          '  APELLIDOS           VARCHAR2(40),'||
                          '  RUC                 VARCHAR2(50),'||
                          '  DIRECCION           VARCHAR2(70),'||
                          '  DIRECCION2          VARCHAR2(200),'||
                          '  CONT1               VARCHAR2(15),'||
                          '  CONT2               VARCHAR2(15),'||
                          '  GRUPO               VARCHAR2(40),'||
                          '  FORMA_PAGO          NUMBER,'||
                          '  DES_FORMA_PAGO      VARCHAR2(58),'||
                          '  TIPO_FORMA_PAGO     NUMBER,'||  
                          '  DES_TIPO_FORMA_PAGO VARCHAR2(30),'||
                          '  tipo_cuenta         varchar2(40),'||
                          '  tarjeta_cuenta      varchar2(25),'||
                          '  fech_aper_cuenta    date,'||
                          '  fech_expir_tarjeta  varchar2(20),'||
                          '  FACTURA             VARCHAR2(20),'||
                          COK_PROCESS_REPORT_OLD.set_campos_mora(pdFechaPeriodo)||
                          '  TOTAL_FACT_ACTUAL   NUMBER default 0,'||
                          '  TOTAL_DEUDA         NUMBER default 0,'||
                          '  TOTAL_DEUDA_CIERRE  NUMBER default 0,'||
                          '  TOTPAGOS            NUMBER default 0,'||
 --
                          '  TOTPAGOS_recuperado NUMBER default 0,'||
                          '  SALDOANT            NUMBER default 0,'||
                          '  SALDOFAVOR          NUMBER default 0,'||
                          '  NEWSALDO            NUMBER default 0,'||
                          '  NEWSALDOCAL         NUMBER default 0,'||
                          '  DISPONIBLE          NUMBER default 0,'||
                          '  TOTCONSUMO          NUMBER default 0,'||
                          --'  PAGOSJULIO          NUMBER default 0,'||
                          --'  SALDOJUNIO          NUMBER default 0,'||
                          --'  FAC_JULIO           NUMBER default 0,'||
                          --'  CRE_JULIO           NUMBER default 0,'||
                          --'  FAC_AGOSTO          NUMBER default 0,'||
                          --'  CRE_AGOSTO          NUMBER default 0,'||
                          '  CREDITOS            NUMBER default 0,'||
                          --'  FAC_JUL_REAL        NUMBER default 0,'||
                          --'  FAC_AGO_REAL        NUMBER default 0,'||
                          --'  CRE_JUNIO           NUMBER default 0,'||
                          '  MORA                NUMBER default 0,'||
                          '  CREDITOrecuperado   NUMBER default 0,'||
                          '  DEBITOrecuperado    NUMBER default 0,'||
                          '  SALDOSEP              NUMBER default 0,'||
                          '  PAGOSOCT              NUMBER default 0,'||
                          '  CREDTOCT              NUMBER default 0,'||
                          '  CMOCT                 NUMBER default 0,'||
                          '  CONSMOCT              NUMBER default 0,'||                          
                          '  TIPO                  varchar2(6),'||
                          '  TRADE                 varchar2(40),'||
                          '  SALDOSEP              NUMBER default 0,'||
                          '  PAGOSOCT              NUMBER default 0,'||
                          '  CREDTOCT              NUMBER default 0,'||
                          '  CMOCT                 NUMBER default 0,'||
                          '  CONSMOCT              NUMBER default 0'||                          
                          ')'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 9360K'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);      
           -- Crea clave primaria
           lvSentenciaCrea := 'alter table '|| pvNombreTabla ||
           '   add constraint PKCUSTOMER_ID  primary key (CUSTOMER_ID)'||
                          '  using index'||
                          '  tablespace DATA'||
                          '  pctfree 10'||
                          '  initrans 2'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1M'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents unlimited'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);                          
           --  Crea indice
           lvSentenciaCrea := 'create index ID_CUSTOMER_ID_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' on '|| pvNombreTabla ||' (CUSTOMER_ID, tipo)'||
           --lvSentenciaCrea := 'create index ID_CUSTOMER_ID_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' on '|| pvNombreTabla ||' (CUSTOMER_ID, ohentdate, ohstatus)'||
                          '  tablespace DATA'||
                          '  pctfree 10'||
                          '  initrans 2'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1M'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents unlimited'||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
           --
--           lvSentenciaCrea := 'create public synonym CUADRE_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' for sysadm.CUADRE_'||to_char(pdFechaPeriodo,'ddMMyyyy');
           lvSentenciaCrea := 'create public synonym ' || pvNombreTabla||' for sysadm.'||pvNombreTabla;
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
--           Lvsentenciacrea := 'grant all on CUADRE_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to public';
           Lvsentenciacrea := 'grant all on ' || pvNombreTabla ||' to public';
           EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);
                          
           return 1;
           
    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA_CUADRE;

/***************************************************************************
 *
 *                             MAIN PROGRAM                                                                          
 *
 **************************************************************************/    
PROCEDURE MAIN(pdFechCierrePeriodo in date) IS
               
    -- variables
    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lv_nombre_tabla       varchar2(50);
    lv_Tabla_OrderHdr     varchar2(50);
    lv_Tabla_cashreceipts varchar2(50);
    lv_Tabla_customer     varchar2(50);
    lv_pdLink             varchar2(50);
    lnMes                 number;
    
    -- cursores
    cursor c_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July

BEGIN

     --lv_nombre_tabla        := 'co_cuadre_concilia';
     lv_nombre_tabla        := 'CO_CUADRE';
     lv_Tabla_OrderHdr      :='OrderHdr_all';
     lv_Tabla_cashreceipts  :='cashreceipts_all';         
     lv_Tabla_customer := 'customer_all';
     lv_pdlink              := '';

      --search the table, if it exists...
      lvSentencia := 'select count(*) from user_all_tables where table_name = '''||lv_nombre_tabla||'''';
      source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
      rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS        
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR
      
      if lnExisteTabla is null or lnExisteTabla = 0 then
         --se crea la tabla 
         lnExito:=COK_PROCESS_REPORT_OLD.crea_tabla_cuadre(pdFechCierrePeriodo, lvMensErr, lv_nombre_tabla);

      end if;
      
      -- se llama al procedure que procesa el cuadre de la tabla
      COK_PROCESS_REPORT_OLD.procesa_cuadre( pdFechCierrePeriodo, 
                                         lv_nombre_tabla, 
                                         lv_Tabla_OrderHdr,
                                         lv_Tabla_cashreceipts,
                                         lv_Tabla_customer,
                                         lv_pdLink, lvMensErr );
    
      -- se llena trunca la tabla de reportes 
      -- cartera vs pago, mas 150 dias, vigente vencida
      lvSentencia := 'truncate table ope_cuadre';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      
      -- se insertan los datos para los reportes o forms o reports
      -- cartera vs pago, mas 150 dias, vigente vencida
      lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, total_deuda,  mora, saldoant, factura, region, tipo, trade)'||
                     'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, total_deuda+balance_'||lnMes||',  mora, saldoant, factura, region, tipo, trade from CO_CUADRE';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      commit;                                                                                       
                    
END MAIN;

end COK_PROCESS_REPORT_OLD;




/*
 *  INICIO
 *  No Borrar... Aqui se tiene respaldo para crear la tabla de Administraci�n para Ingreso 
 *  de los usuarios para ejecutar la reporteria. 
 *
 
-- Create table
create table READ.OPE_PERMISOREPORT
(
  PSW     VARCHAR2(10) not null,
  USUARIO VARCHAR2(10) not null,
  REP_1   VARCHAR2(2) default ('N') not null,
  REP_2   VARCHAR2(2) default ('N') not null,
  REP_3   VARCHAR2(2) default ('N') not null,
  REP_4   VARCHAR2(2) default ('N') not null,
  REP_5   VARCHAR2(2) default ('N') not null,
  REP_6   VARCHAR2(2) default ('N') not null,
  REP_7   VARCHAR2(2) default ('N') not null,
  REP_8   VARCHAR2(2) default ('N') not null,
  REP_9   VARCHAR2(2) default ('N') not null,
  REP_10  VARCHAR2(2) default ('N') not null,
  REP_11  VARCHAR2(2) default ('N') not null,
  REP_12  VARCHAR2(2) default ('N') not null,
  REP_13  VARCHAR2(2) default ('N') not null,
  REP_14  VARCHAR2(2) default ('N') not null,
  REP_15  VARCHAR2(2) default ('N') not null,
  REP_16  VARCHAR2(2) default ('N') not null,
  REP_17  VARCHAR2(2) default ('N') not null,
  REP_18  VARCHAR2(2) default ('N') not null,
  REP_19  VARCHAR2(2) default ('N') not null,
  REP_20  VARCHAR2(2) default ('N') not null,
  STATUS  VARCHAR2(2) default ('A') not null
)
tablespace DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 120K
    next 104K
    minextents 1
    maxextents 255
    pctincrease 0
  );
-- Add comments to the columns 
comment on column READ.OPE_PERMISOREPORT.REP_1
  is 'N --> no tiene permiso ;  S --> si tiene permiso';
comment on column READ.OPE_PERMISOREPORT.STATUS
  is 'A -->  activo;  I -->  Inactivo';
-- Create/Recreate primary, unique and foreign key constraints 
alter table READ.OPE_PERMISOREPORT
  add constraint PKOPE_PERMISOREPORT primary key (USUARIO)
  using index 
  tablespace DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 120K
    next 104K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete on READ.OPE_PERMISOREPORT to BSCS_ROLE;


 *  No Borrar... Aqui se tiene respaldo para crear la tabla de Administraci�n para Ingreso 
 *  de los usuarios para ejecutar la reporteria. 
 *  FIN de No Borrar
*/
/

