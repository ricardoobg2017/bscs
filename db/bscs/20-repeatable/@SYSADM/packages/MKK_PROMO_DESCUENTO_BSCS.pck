CREATE OR REPLACE PACKAGE MKK_PROMO_DESCUENTO_BSCS IS

  -- Author  : PCARVAJAL
  -- Created : 29/09/2008 14:27:49
  -- Purpose : INSERTAR REGISTRO DE LA PROMOCI�N

  -- Modificado: SUD Diana Schnabel
  -- Created   : 19/11/2008 12:27:49
  -- Purpose : INSERTAR REGISTRO DE LA PROMOCI�N EN TABLA TEMPORAL 3761.- DS:

  -- Declaraci�n de constantes
  Cv_NombrePaquete CONSTANT VARCHAR2(200) := 'MKK_PROMO_DESCUENTO_BSCS';

  PROCEDURE MKP_PROCESA_DESCUENTO(PN_PACK_ID         NUMBER,
                                  PN_CUSTOMER_ID     NUMBER,
                                  PN_CO_ID           NUMBER,
                                  PD_ASSIGN_DATE     DATE,
                                  PV_ERROR       OUT VARCHAR2);

  PROCEDURE MKP_PROMO_ASSIGN_STATE(PN_CUSTOMER_ID      NUMBER,
                                   PN_PACK_ID          NUMBER,
                                   PD_ASSIGN_DATE      DATE,
                                   PV_ERROR        OUT VARCHAR2);

  PROCEDURE MKP_PROMO_ASSIGN(PN_CUSTOMER_ID     NUMBER,
                             PN_PACK_ID         NUMBER,
                             PD_ASSIGN_DATE     DATE,
                             PV_ERROR       OUT VARCHAR2);

  PROCEDURE MKP_PROMO_QUAL_RESTRICT(PN_SECUENCIA        NUMBER,
                                    PN_CUSTOMER_ID      NUMBER,
                                    PN_CO_ID            NUMBER,
                                    PN_SEQ              NUMBER,
                                    PV_ESTADO           VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2);

  PROCEDURE MKP_PROMO_MECH_SET(PN_SECUENCIA      NUMBER,
                               PV_ERROR      OUT VARCHAR2);

  PROCEDURE MKP_PROMO_MECH_SET_DATA(PN_SECUENCIA  NUMBER,
                                    PN_PACK_ID    NUMBER,
                                    PV_ERROR      OUT VARCHAR2);

  FUNCTION MKF_OBTIENE_SECUENCIA(PV_ERROR      OUT VARCHAR2) RETURN NUMBER;

  -- Autor    : Prog. Diana Schnabel
  -- Creaci�n : 19/11/2008 12:27:49
  -- Prop�sito: 3761.- DS: Permite insertar datos en la tabla temporarl MK_PROMO_AJUSTE_TECNOMEN_TMP
  PROCEDURE MKK_PR_INSERTA_PROMO_BSCS(Pv_Id_Servicio    IN VARCHAR2,
                                      Pv_Codigo_Doc     IN VARCHAR2, -- Cuenta del servicio BSCS
                                      Pn_Monto_Dolares  IN NUMBER,
                                      Pd_Expiracion     IN DATE,
                                      Pv_Estado         IN VARCHAR2,
                                      Pn_Id_Promocion   IN NUMBER,
                                      Pv_Id_Ciclo       IN VARCHAR2,
                                      Pd_Fecha_Acredita IN DATE,
                                      Pd_Fecha_Ingreso  IN DATE,
                                      Pn_Despachador    IN NUMBER,
                                      Pv_Observacion    IN VARCHAR2,
                                      Pv_Error          OUT VARCHAR2);

  -- Autor    : Prog. Diana Schnabel
  -- Creaci�n : 19/11/2008 12:27:49
  -- Prop�sito: 
  PROCEDURE MKK_PR_ACREDITA_BSCS(Pv_Id_Servicio    IN VARCHAR2,
                                 Pv_Codigo_Doc     IN VARCHAR2, -- Cuenta del servicio BSCS
                                 Pn_Monto_Dolares  IN NUMBER,
                                 Pd_Expiracion     IN DATE,
                                 Pv_Estado         IN VARCHAR2,
                                 Pn_Id_Promocion   IN NUMBER,
                                 Pv_Id_Ciclo       IN VARCHAR2,
                                 Pd_Fecha_Acredita IN DATE,
                                 Pd_Fecha_Ingreso  IN DATE,
                                 Pn_Despachador    IN NUMBER,
                                 Pv_Observacion    IN VARCHAR2,
                                 Ppn_Pack_id       IN NUMBER,   -- The promotion package this promotion evaluation was done for.FK to PROMO_PACK
                                 --Pn_Customer_ID    IN NUMBER,   -- Identifier for the customer the promotion package was assigned to. FK to CUSTOMER_ALL.
                                 Pn_Co_Id          IN NUMBER,   -- Identifier for the contract. FK to CONTRACT_ALL.
                                 Pv_Error          OUT VARCHAR2);

PROCEDURE MKK_PR_ACTUALIZA_ESTATUS(PV_TRAMA          IN VARCHAR2,
                                   PN_Error          OUT NUMBER,
                                   Pv_Error          OUT VARCHAR2) ;
                                   
PROCEDURE PR_VALIDA_PROCESA_DESCUENTO(PN_PACK_ID     NUMBER,
                                      PN_CUSTOMER_ID NUMBER,
                                      PN_CO_ID       NUMBER,
                                      PD_ASSIGN_DATE DATE,
                                      PV_ERROR       OUT VARCHAR2);
                                   
  --10425 SUD VGU
  PROCEDURE PR_VALIDA_PROCESA_DESCUENTO2(PN_PACK_ID     NUMBER,
                                         PN_CUSTOMER_ID NUMBER,
                                         PN_CO_ID       NUMBER,
                                         PD_ASSIGN_DATE DATE,
                                         PN_SECUENCIA   OUT NUMBER,
                                         PV_ERROR       OUT VARCHAR2);
  
  PROCEDURE MKP_PROMO_ASSIGN_STATE_N(PN_CUSTOMER_ID      NUMBER,
                                     PN_PACK_ID          NUMBER,
                                     PN_ASSIGN_SEQ       NUMBER,
                                     PD_ASSIGN_DATE      DATE,
                                     PV_ERROR            OUT VARCHAR2);
  
  PROCEDURE MKP_PROCESA_DESCUENTO_2(PN_PACK_ID     NUMBER,
                                    PN_CUSTOMER_ID NUMBER,
                                    PN_CO_ID       NUMBER,
                                    PD_ASSIGN_DATE DATE,
                                    PN_SECUENCIA   OUT NUMBER,
                                    PV_ERROR       OUT VARCHAR2);
  
  PROCEDURE MKP_PROMO_ASSIGN_2(PN_CUSTOMER_ID     NUMBER,
                               PN_PACK_ID         NUMBER,
                               PD_ASSIGN_DATE     DATE,
                               PN_ASSING_SEQ      OUT NUMBER,
                               PV_ERROR           OUT VARCHAR2);
                               
  PROCEDURE MKP_UPDATE_MECH_SET_DATA(PN_SECUENCIA      NUMBER,
                                     PN_PACK_ID        NUMBER,
                                     PN_ASSING_SEQ     NUMBER,
                                     PV_ERROR          OUT VARCHAR2);
  
  PROCEDURE MKP_PROMO_ASSIGN_STATE_2(PN_CUSTOMER_ID      NUMBER,
                                     PN_PACK_ID          NUMBER,
                                     PD_ASSIGN_DATE      DATE,
                                     PV_ERROR            OUT VARCHAR2);
  
  PROCEDURE MKP_INACTIVA_DESCUENTO(PN_CUSTOMER_ID NUMBER,
                                   PN_CO_ID       NUMBER,
                                   PN_PACK_ID     NUMBER,
                                   PN_SECUENCIA   NUMBER,
                                   PV_ERROR       OUT VARCHAR2);
  --10425 SUD VGU
  
  --07-09-2015
  PROCEDURE MKP_REGISTRA_DESCUENTO_TMP(PV_SERVICIO             VARCHAR2 DEFAULT NULL,
                                       PN_CO_ID                NUMBER DEFAULT NULL,
                                       PN_MONTO_DOLARES        NUMBER DEFAULT NULL,
                                       PD_EXPIRACION           DATE DEFAULT NULL,
                                       PV_ESTADO               VARCHAR2 DEFAULT NULL,
                                       PN_PROMOCION            NUMBER DEFAULT NULL,
                                       PV_CICLO                VARCHAR2 DEFAULT NULL,
                                       PD_FECHA_ACREDITA       DATE DEFAULT NULL,
                                       PD_FECHA_INGRESO        DATE DEFAULT NULL,
                                       PN_DESPACHADOR          NUMBER DEFAULT NULL,
                                       PV_OBSERVACION          VARCHAR2 DEFAULT NULL,
                                       PV_CODIGO_DOC           VARCHAR2 DEFAULT NULL,
                                       PN_CONTRATO             NUMBER DEFAULT NULL,
                                       PV_ID_SUBPRODUCTO       VARCHAR2 DEFAULT NULL,
                                       PN_CUSTOMER_ID          NUMBER DEFAULT NULL,
                                       PN_PACK_ID              NUMBER DEFAULT NULL,
                                       PN_SECUENCIA_BSCS       NUMBER DEFAULT NULL,
                                       PV_TIPO_PROMO           VARCHAR2 DEFAULT NULL,
                                       PD_FECHA_REGISTRO       DATE DEFAULT NULL,
                                       PV_ERROR                OUT VARCHAR2);
  
  PROCEDURE MKP_DESACTIVA_DESCUENTO(PD_FECHA_INI         DATE,
                                    PV_ERROR             OUT VARCHAR2);
  --07-09-2015

END MKK_PROMO_DESCUENTO_BSCS;
/
CREATE OR REPLACE PACKAGE BODY MKK_PROMO_DESCUENTO_BSCS IS


  PROCEDURE MKP_PROCESA_DESCUENTO(PN_PACK_ID     NUMBER,
                                  PN_CUSTOMER_ID NUMBER,
                                  PN_CO_ID       NUMBER,
                                  PD_ASSIGN_DATE DATE,
                                  PV_ERROR       OUT VARCHAR2) IS
  LN_SECUENCIA    NUMBER;
  LE_ERROR        EXCEPTION;
  BEGIN
   -- Obtengo la Secuencia --
   LN_SECUENCIA:=MKF_OBTIENE_SECUENCIA(PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

   -- promo_mech_set --
   MKP_PROMO_MECH_SET(LN_SECUENCIA,PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

   -- promo_mech_set_data --
   MKP_PROMO_MECH_SET_DATA(LN_SECUENCIA,PN_PACK_ID,PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

  --promo_qual_restrict  --
   MKP_PROMO_QUAL_RESTRICT(LN_SECUENCIA,PN_CUSTOMER_ID, NULL,1,'E',PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;


   MKP_PROMO_QUAL_RESTRICT(LN_SECUENCIA,PN_CUSTOMER_ID,PN_CO_ID, 2,'I',PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

   -- promo_assign --
   MKP_PROMO_ASSIGN(PN_CUSTOMER_ID, PN_PACK_ID, PD_ASSIGN_DATE,PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

   -- promo_assign_state--
   MKP_PROMO_ASSIGN_STATE(PN_CUSTOMER_ID, PN_PACK_ID, PD_ASSIGN_DATE,PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;


  EXCEPTION
    WHEN LE_ERROR THEN
       PV_ERROR:=PV_ERROR;
    WHEN OTHERS THEN
       PV_ERROR:=SQLERRM;
  END MKP_PROCESA_DESCUENTO;

  PROCEDURE MKP_PROMO_ASSIGN_STATE(PN_CUSTOMER_ID      NUMBER,
                                   PN_PACK_ID          NUMBER,
                                   PD_ASSIGN_DATE      DATE,
                                   PV_ERROR        OUT VARCHAR2) IS
  BEGIN
    INSERT INTO promo_assign_state
      (customer_id,
       pack_id,
       assign_seq,
       seq,
       work_state,
       state_date,
       rec_version)
    VALUES
      (PN_CUSTOMER_ID, PN_PACK_ID, 1,1, 'A', PD_ASSIGN_DATE, 1);
  EXCEPTION
    WHEN OTHERS THEN
       PV_ERROR:='PROMO_ASSIGN_STATE ' ||SQLERRM;
  END MKP_PROMO_ASSIGN_STATE;

  PROCEDURE MKP_PROMO_ASSIGN(PN_CUSTOMER_ID     NUMBER,
                             PN_PACK_ID         NUMBER,
                             PD_ASSIGN_DATE     DATE,
                             PV_ERROR       OUT VARCHAR2) IS
  BEGIN
      INSERT INTO promo_assign
        (customer_id, pack_id, assign_seq, assign_date, delete_date, rec_version)
      VALUES
        (PN_CUSTOMER_ID, PN_PACK_ID, 1, PD_ASSIGN_DATE, NULL, 1);
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:='PROMO_ASSIGN ' ||SQLERRM;
  END MKP_PROMO_ASSIGN;

  PROCEDURE MKP_PROMO_QUAL_RESTRICT(PN_SECUENCIA      NUMBER,
                                    PN_CUSTOMER_ID    NUMBER,
                                    PN_CO_ID          NUMBER,
                                    PN_SEQ            NUMBER,
                                    PV_ESTADO         VARCHAR2,
                                    PV_ERROR      OUT VARCHAR2) IS
   BEGIN
      INSERT INTO PROMO_QUAL_RESTRICT
        (cust_id_assign,
         mech_set_id,
         seq,
         customer_id,
         contract_id,
         incl_or_excl,
         rec_version)
      VALUES
        (PN_CUSTOMER_ID, PN_SECUENCIA, PN_SEQ, PN_CUSTOMER_ID, PN_CO_ID, PV_ESTADO, 1);
  EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='PROMO_QUAL_RESTRICT ' ||SQLERRM;
  END MKP_PROMO_QUAL_RESTRICT;

  PROCEDURE MKP_PROMO_MECH_SET(PN_SECUENCIA      NUMBER,
                               PV_ERROR      OUT VARCHAR2) IS
  BEGIN
    INSERT INTO promo_mech_set
      (mech_set_id, short_description, description, rec_version)
    VALUES
      (PN_SECUENCIA, 'RC', 'Limitado a contrato(s)', 1);
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:='PROMO_MECH_SET ' ||SQLERRM;
  END MKP_PROMO_MECH_SET;

  PROCEDURE MKP_PROMO_MECH_SET_DATA(PN_SECUENCIA      NUMBER,
                                    PN_PACK_ID        NUMBER,
                                    PV_ERROR      OUT VARCHAR2) IS
  BEGIN
    INSERT INTO PROMO_MECH_SET_DATA
      (MECH_SET_ID,
       SEQ,
       PACK_ID,
       ASSIGN_SEQ,
       MECH_ID,
       REC_VERSION,
       RULE_MODEL_ID,
       RULE_ELEMENT_ID)
    VALUES
      (PN_SECUENCIA, 1, PN_PACK_ID, 1, NULL, 1, NULL, NULL);
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:='PROMO_MECH_SET_DATA '||SQLERRM;
  END MKP_PROMO_MECH_SET_DATA;

  FUNCTION MKF_OBTIENE_SECUENCIA(PV_ERROR      OUT VARCHAR2)  RETURN NUMBER IS
  ln_secuencia    number;
  BEGIN
    --SELECT NVL(MAX(P.MECH_SET_ID),0)+1 INTO LN_SECUENCIA FROM PROMO_MECH_SET P;
    SELECT MAX_MECH_SET_ID_SEQ.NEXTVAL INTO LN_SECUENCIA FROM DUAL;--[5307]AGREGADO DEBIDO QUE LA ANTERIOR LINEA TENIA INCONVENIENTES 
    RETURN LN_SECUENCIA;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:='Obtener Secuencia ' ||SQLERRM;
  END;

  PROCEDURE MKK_PR_INSERTA_PROMO_BSCS(Pv_Id_Servicio    IN VARCHAR2,
                                      Pv_Codigo_Doc     IN VARCHAR2, -- Cuenta del servicio BSCS
                                      Pn_Monto_Dolares  IN NUMBER,
                                      Pd_Expiracion     IN DATE,
                                      Pv_Estado         IN VARCHAR2,
                                      Pn_Id_Promocion   IN NUMBER,
                                      Pv_Id_Ciclo       IN VARCHAR2,
                                      Pd_Fecha_Acredita IN DATE,
                                      Pd_Fecha_Ingreso  IN DATE,
                                      Pn_Despachador    IN NUMBER,
                                      Pv_Observacion    IN VARCHAR2,
                                      Pv_Error          OUT VARCHAR2) IS

  -- Autor    : Prog. Diana Schnabel
  -- Creaci�n : 19/11/2008 12:27:49
  -- Prop�sito: Permite insertar datos en la tabla temporarl MK_PROMO_AJUSTE_TECNOMEN_TMP
  Lv_NombreObjeto  CONSTANT VARCHAR(40) := '.MKK_PR_INSERTA_PROMO_BSCS';

  BEGIN

     --Valida de datos de ingreso
     IF Pv_Id_Servicio IS NULL THEN
       PV_ERROR := MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete || Lv_NombreObjeto || ' No se tiene Parametro de Servicio';
       RETURN;
     END IF;

     IF Pv_Codigo_Doc IS NULL THEN
       PV_ERROR := MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete || Lv_NombreObjeto || ' No se tiene Parametro de C�digo del documento';
       RETURN;
     END IF;

     IF Pv_Estado IS NULL THEN
       PV_ERROR := MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete || Lv_NombreObjeto || ' No se tiene Parametro de estado';
       RETURN;
     END IF;

     IF Pv_Id_Ciclo IS NULL THEN
       PV_ERROR := MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete || Lv_NombreObjeto || ' No se tiene Parametro de estado';
       RETURN;
     END IF;

    INSERT INTO MK_PROMO_AJUSTE_BSCS_TMP(
                 ID_SERVICIO
                ,CODIGO_DOC
                ,MONTO_DOLARES
                ,EXPIRACION
                ,ESTADO
                ,ID_PROMOCION
                ,ID_CICLO
                ,FECHA_ACREDITA
                ,FECHA_INGRESO
                ,DESPACHADOR
                ,OBSERVACION)
    VALUES      (
                Pv_Id_Servicio,
                Pv_Codigo_Doc,
                Pn_Monto_Dolares,
                Pd_Expiracion,
                Pv_Estado,
                Pn_Id_Promocion,
                Pv_Id_Ciclo,
                Pd_Fecha_Acredita,
                Pd_Fecha_Ingreso,
                Pn_Despachador,
                Pv_Observacion);

   EXCEPTION
  WHEN OTHERS THEN
      Pv_Error := 'Error t�cnico en: ' || MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete ||Lv_NombreObjeto || SQLERRM;
  END MKK_PR_INSERTA_PROMO_BSCS;

  PROCEDURE MKK_PR_ACREDITA_BSCS(Pv_Id_Servicio    IN VARCHAR2,
                                 Pv_Codigo_Doc     IN VARCHAR2, -- Cuenta del servicio BSCS
                                 Pn_Monto_Dolares  IN NUMBER,
                                 Pd_Expiracion     IN DATE,
                                 Pv_Estado         IN VARCHAR2,
                                 Pn_Id_Promocion   IN NUMBER,
                                 Pv_Id_Ciclo       IN VARCHAR2,
                                 Pd_Fecha_Acredita IN DATE,
                                 Pd_Fecha_Ingreso  IN DATE,
                                 Pn_Despachador    IN NUMBER,
                                 Pv_Observacion    IN VARCHAR2,
                                 Ppn_Pack_id       IN NUMBER,   -- The promotion package this promotion evaluation was done for.FK to PROMO_PACK
                                 Pn_Co_Id          IN NUMBER,   -- Identifier for the contract. FK to CONTRACT_ALL.
                                 Pv_Error          OUT VARCHAR2) IS

    -- Autor    : Prog. Diana Schnabel
    -- Creaci�n : 18/Noviembre/2008
    -- Prop�sito: Permite realizar la inserci�n y la acreditaci�n para la promoci�n
    Lv_NombreObjeto  CONSTANT VARCHAR(40) := '.MKK_PR_ACREDITA_BSCS';
    ln_customer_id   CUSTOMER_ALL.Customer_Id%TYPE;
    lb_Found         BOOLEAN;

    CURSOR C_Customer(Pv_Custcode VARCHAR2) IS
    SELECT CA.customer_id
    FROM   CUSTOMER_ALL CA
    WHERE  CA.custcode = Pv_Custcode;

  BEGIN
     -- Verifico que me enviaron correcto el codigo de la cuenta
     OPEN C_Customer(Pv_Codigo_Doc);
     FETCH C_Customer INTO ln_customer_id;
     lb_Found := C_Customer%FOUND;
     CLOSE C_Customer;
     IF NOT lb_Found THEN
       Pv_Error := 'No se encuentra el c�digo en CUSTOMER_ALL, verifique';
       RETURN;
     END IF;

     -- Permite hacer la inserci�n en la tabla temporal
     MKK_PR_INSERTA_PROMO_BSCS(Pv_Id_Servicio    => Pv_Id_Servicio,
                               Pv_Codigo_Doc     => Pv_Codigo_Doc, -- Cuenta del servicio BSCS
                               Pn_Monto_Dolares  => Pn_Monto_Dolares,
                               Pd_Expiracion     => Pd_Expiracion,
                               Pv_Estado         => Pv_Estado,
                               Pn_Id_Promocion   => Pn_Id_Promocion,
                               Pv_Id_Ciclo       => Pv_Id_Ciclo,
                               Pd_Fecha_Acredita => Pd_Fecha_Acredita,
                               Pd_Fecha_Ingreso  => Pd_Fecha_Ingreso,
                               Pn_Despachador    => Pn_Despachador,
                               Pv_Observacion    => Pv_Observacion,
                               Pv_Error          => Pv_Error);
      IF Pv_Error IS NOT NULL THEN
       RETURN;
      END IF;

     -- Paquete que me permite hacer la acreditaci�n de los clientes
     MKP_PROCESA_DESCUENTO(PN_PACK_ID     => Ppn_Pack_id,
                           PN_CUSTOMER_ID => ln_customer_id, --Pn_Customer_ID,Identifier for the customer the promotion package was assigned to. FK to CUSTOMER_ALL.
                           PN_CO_ID       => Pn_Co_Id,
                           PD_ASSIGN_DATE => Pd_Fecha_Ingreso, --Pd_Assign_Date,
                           PV_ERROR       => Pv_Error); -- Error de la aplicaci�n
      IF Pv_Error IS NOT NULL THEN
       RETURN;
      END IF;

   EXCEPTION
  WHEN OTHERS THEN
      Pv_Error := 'Error t�cnico en: ' || MKK_PROMO_DESCUENTO_BSCS.Cv_NombrePaquete ||Lv_NombreObjeto || SQLERRM;
  END MKK_PR_ACREDITA_BSCS;

PROCEDURE MKK_PR_ACTUALIZA_ESTATUS(Pv_Trama IN VARCHAR2,
                                   Pn_Error OUT NUMBER, -- Identifier for the contract. FK to CONTRACT_ALL.
                                   Pv_Error OUT VARCHAR2) IS
  Ln_Customer_Id NUMBER;
  Ln_Pack_Id     NUMBER;

BEGIN
  Ln_Pack_Id     := LNF_CAMPO_CADENA_ALT('-'||Pv_Trama, 1);
  Ln_Customer_Id := LNF_CAMPO_CADENA_ALT('-'||Pv_Trama, 2);

  UPDATE Promo_Assign_State a
     SET a.Work_State = 'I'
   WHERE Customer_Id = Ln_Customer_Id
     AND a.Pack_Id = Ln_Pack_Id;
   
  IF SQL%NOTFOUND THEN
    Pv_Error := 'No actualiza-->'||Ln_Customer_Id ||'-'||Ln_Pack_Id;
    Pn_Error := 0;
  ELSE
    Pv_Error := 'EXITOSO';
    Pn_Error := 0;  
  END IF;
     
  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := 'Error t�cnico en: MKK_PROMO_DESCUENTO_BSCS.MKK_PR_ACTUALIZA_ESTATUS'||substr(sqlerrm,1,500);
    Pn_Error := -1;
END Mkk_Pr_Actualiza_Estatus;

PROCEDURE PR_VALIDA_PROCESA_DESCUENTO(PN_PACK_ID     NUMBER,
                                      PN_CUSTOMER_ID NUMBER,
                                      PN_CO_ID       NUMBER,
                                      PD_ASSIGN_DATE DATE,
                                      PV_ERROR       OUT VARCHAR2) IS
  Ln_Customer_Id NUMBER;
  Ln_Pack_Id     NUMBER:=283;
  Lb_Found       BOOLEAN;
  lv_error       varchar2(2000); 
  LE_ERROR       exception;


  CURSOR c_Customer(cn_Customer_id number) IS
    SELECT ca.Customer_Id
      FROM Promo_Assign_State ca
     WHERE ca.Customer_Id = cn_Customer_id
       AND ca.Pack_Id = 283;

BEGIN
  
     OPEN C_Customer(PN_CUSTOMER_ID);
     FETCH C_Customer INTO ln_customer_id;
     lb_Found := C_Customer%FOUND;
     CLOSE C_Customer;
     
    
     IF lb_Found THEN
      UPDATE Promo_Assign_State a
         SET a.Work_State = 'A'
       WHERE Customer_Id = Ln_Customer_Id
         AND a.Pack_Id = Ln_Pack_Id;
       RETURN;
     END IF;

  
  
  MKK_PROMO_DESCUENTO_BSCS.MKP_PROCESA_DESCUENTO(pn_pack_id     => PN_PACK_ID, --determinar como lo vamos a obtener
                                                 pn_customer_id => PN_CUSTOMER_ID,
                                                 pn_co_id       => PN_CO_ID,
                                                 pd_assign_date => PD_ASSIGN_DATE,
                                                 pv_error       => lv_error);
                                                 
  IF lv_error IS NOT NULL THEN
     RAISE LE_ERROR; 
  END IF; 

EXCEPTION
   WHEN LE_ERROR THEN
    Pv_Error := 'Error t�cnico en: MKK_PROMO_DESCUENTO_BSCS.PR_VALIDA_PROCESA_DESCUENTO'||substr(lv_error,1,500);
  WHEN OTHERS THEN
    Pv_Error := 'Error t�cnico en: MKK_PROMO_DESCUENTO_BSCS.PR_VALIDA_PROCESA_DESCUENTO'||substr(sqlerrm,1,500);
END PR_VALIDA_PROCESA_DESCUENTO;

  --10425 SUD VGU
  PROCEDURE PR_VALIDA_PROCESA_DESCUENTO2(PN_PACK_ID     NUMBER,
                                         PN_CUSTOMER_ID NUMBER,
                                         PN_CO_ID       NUMBER,
                                         PD_ASSIGN_DATE DATE,
                                         PN_SECUENCIA   OUT NUMBER,
                                         PV_ERROR       OUT VARCHAR2) IS

    CURSOR C_CO_ID(CN_CUSTOMER_ID NUMBER, CN_CO_ID NUMBER) IS
      SELECT K.*, H.ASSIGN_SEQ
        FROM PROMO_QUAL_RESTRICT K, PROMO_MECH_SET_DATA H
       WHERE K.CUSTOMER_ID = CN_CUSTOMER_ID
         AND K.CONTRACT_ID = CN_CO_ID
         AND H.MECH_SET_ID = K.MECH_SET_ID
         AND H.PACK_ID = PN_PACK_ID;
    
    CURSOR C_WORK_STATE(CN_CUSTOMER_ID NUMBER, LN_ASSIGN_SEQ NUMBER) IS
      SELECT PS.*
        FROM PROMO_ASSIGN_STATE PS
       WHERE PS.CUSTOMER_ID = CN_CUSTOMER_ID
         AND PS.PACK_ID = PN_PACK_ID
         AND PS.ASSIGN_SEQ = LN_ASSIGN_SEQ
         AND PS.WORK_STATE = 'A';
    
    LC_WORK_STATE  C_WORK_STATE%ROWTYPE;
    LC_CO_ID       C_CO_ID%ROWTYPE;
    LB_FOUNDQ      BOOLEAN;
    LB_FOUNDST     BOOLEAN;
    LV_ERROR       VARCHAR2(2000); 
    LN_SECUENCIA   NUMBER := NULL;
    LE_ERROR       EXCEPTION;

  BEGIN

    OPEN C_CO_ID(PN_CUSTOMER_ID, PN_CO_ID);
    FETCH C_CO_ID INTO LC_CO_ID;
    LB_FOUNDQ := C_CO_ID%FOUND;
    CLOSE C_CO_ID;
         
    IF LB_FOUNDQ = TRUE THEN
       OPEN C_WORK_STATE(PN_CUSTOMER_ID, LC_CO_ID.ASSIGN_SEQ);
       FETCH C_WORK_STATE INTO LC_WORK_STATE;
       LB_FOUNDST := C_WORK_STATE%FOUND;
       CLOSE C_WORK_STATE;
       
       LN_SECUENCIA := LC_CO_ID.MECH_SET_ID;

       IF LB_FOUNDST = FALSE THEN
          MKK_PROMO_DESCUENTO_BSCS.MKP_PROMO_ASSIGN_STATE_N(PN_CUSTOMER_ID => PN_CUSTOMER_ID,
                                                            PN_PACK_ID     => PN_PACK_ID,
                                                            PN_ASSIGN_SEQ  => LC_CO_ID.ASSIGN_SEQ,
                                                            PD_ASSIGN_DATE => PD_ASSIGN_DATE,
                                                            PV_ERROR       => LV_ERROR);

       END IF;
    ELSE
       MKK_PROMO_DESCUENTO_BSCS.MKP_PROCESA_DESCUENTO_2(PN_PACK_ID     => PN_PACK_ID,
                                                        PN_CUSTOMER_ID => PN_CUSTOMER_ID,
                                                        PN_CO_ID       => PN_CO_ID,
                                                        PD_ASSIGN_DATE => PD_ASSIGN_DATE,
                                                        PN_SECUENCIA   => LN_SECUENCIA,
                                                        PV_ERROR       => LV_ERROR);
                                                     
       IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR; 
       END IF;
    END IF;
     
    PN_SECUENCIA := LN_SECUENCIA; 

  EXCEPTION
     WHEN LE_ERROR THEN
      PV_ERROR := 'Error t�cnico en: MKK_PROMO_DESCUENTO_BSCS.PR_VALIDA_PROCESA_DESCUENTO2'||SUBSTR(LV_ERROR,1,500);
    WHEN OTHERS THEN
      PV_ERROR := 'Error t�cnico en: MKK_PROMO_DESCUENTO_BSCS.PR_VALIDA_PROCESA_DESCUENTO2'||SUBSTR(SQLERRM,1,500);
  END PR_VALIDA_PROCESA_DESCUENTO2;
  
  PROCEDURE MKP_PROMO_ASSIGN_STATE_N(PN_CUSTOMER_ID      NUMBER,
                                     PN_PACK_ID          NUMBER,
                                     PN_ASSIGN_SEQ       NUMBER,
                                     PD_ASSIGN_DATE      DATE,
                                     PV_ERROR            OUT VARCHAR2) IS
    CURSOR C_SEQ IS
      SELECT NVL(MAX(T.SEQ), 0) + 1 SEQ
        FROM PROMO_ASSIGN_STATE T
       WHERE CUSTOMER_ID = PN_CUSTOMER_ID
         AND T.ASSIGN_SEQ = PN_ASSIGN_SEQ;
    
    LN_SEQ            NUMBER;
  BEGIN
    OPEN C_SEQ;
    FETCH C_SEQ INTO LN_SEQ;
    CLOSE C_SEQ;

    INSERT INTO PROMO_ASSIGN_STATE
      (CUSTOMER_ID,
       PACK_ID,
       ASSIGN_SEQ,
       SEQ,
       WORK_STATE,
       STATE_DATE,
       REC_VERSION)
    VALUES
      (PN_CUSTOMER_ID, PN_PACK_ID, PN_ASSIGN_SEQ, LN_SEQ, 'A', PD_ASSIGN_DATE, 1);
  EXCEPTION
    WHEN OTHERS THEN
       PV_ERROR := 'MKP_PROMO_ASSIGN_STATE_N ' ||SQLERRM;
  END MKP_PROMO_ASSIGN_STATE_N;
  
  PROCEDURE MKP_PROCESA_DESCUENTO_2(PN_PACK_ID     NUMBER,
                                    PN_CUSTOMER_ID NUMBER,
                                    PN_CO_ID       NUMBER,
                                    PD_ASSIGN_DATE DATE,
                                    PN_SECUENCIA   OUT NUMBER,
                                    PV_ERROR       OUT VARCHAR2) IS
      LN_SECUENCIA    NUMBER;
      LE_ERROR        EXCEPTION;
      LN_ASSING_SEQ   NUMBER;

    BEGIN
      
       -- Obtengo la Secuencia --
       LN_SECUENCIA:=MKF_OBTIENE_SECUENCIA(PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;

       -- promo_mech_set --
       MKP_PROMO_MECH_SET(LN_SECUENCIA,PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;

       -- promo_mech_set_data --
       MKP_PROMO_MECH_SET_DATA(LN_SECUENCIA,PN_PACK_ID,PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;

      --promo_qual_restrict  --
       MKP_PROMO_QUAL_RESTRICT(LN_SECUENCIA,PN_CUSTOMER_ID, NULL,1,'E',PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;

       MKP_PROMO_QUAL_RESTRICT(LN_SECUENCIA,PN_CUSTOMER_ID,PN_CO_ID, 2,'I',PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;

       -- promo_assign --
       MKP_PROMO_ASSIGN_2(PN_CUSTOMER_ID, PN_PACK_ID, PD_ASSIGN_DATE,LN_ASSING_SEQ,PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
       
       MKP_UPDATE_MECH_SET_DATA(LN_SECUENCIA,PN_PACK_ID,LN_ASSING_SEQ,PV_ERROR);

       -- promo_assign_state--
       MKP_PROMO_ASSIGN_STATE_2(PN_CUSTOMER_ID, PN_PACK_ID, PD_ASSIGN_DATE,PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
       
       PN_SECUENCIA := LN_SECUENCIA;
       
    EXCEPTION
      WHEN LE_ERROR THEN
         PV_ERROR := PV_ERROR;
      WHEN OTHERS THEN
         PV_ERROR := SQLERRM;
  END MKP_PROCESA_DESCUENTO_2;
  
  PROCEDURE MKP_PROMO_ASSIGN_2(PN_CUSTOMER_ID     NUMBER,
                               PN_PACK_ID         NUMBER,
                               PD_ASSIGN_DATE     DATE,
                               PN_ASSING_SEQ      OUT NUMBER,
                               PV_ERROR           OUT VARCHAR2) IS
    CURSOR C_SEQ IS
      SELECT NVL(MAX(T.ASSIGN_SEQ), 0) + 1 ASSIGN_SEQ
        FROM PROMO_ASSIGN T
       WHERE CUSTOMER_ID = PN_CUSTOMER_ID;
       
    LN_ASSING_SEQ            NUMBER;  
  BEGIN
      OPEN C_SEQ;
      FETCH C_SEQ INTO LN_ASSING_SEQ;
      CLOSE C_SEQ;

      INSERT INTO PROMO_ASSIGN
        (CUSTOMER_ID, PACK_ID, ASSIGN_SEQ, ASSIGN_DATE, DELETE_DATE, REC_VERSION)
      VALUES
        (PN_CUSTOMER_ID, PN_PACK_ID, LN_ASSING_SEQ, PD_ASSIGN_DATE, NULL, 1);

      PN_ASSING_SEQ := LN_ASSING_SEQ;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'PROMO_ASSIGN_2 ' ||SQLERRM;
  END MKP_PROMO_ASSIGN_2;
  
  PROCEDURE MKP_UPDATE_MECH_SET_DATA(PN_SECUENCIA      NUMBER,
                                      PN_PACK_ID        NUMBER,
                                      PN_ASSING_SEQ     NUMBER,
                                      PV_ERROR          OUT VARCHAR2) IS
  BEGIN
    UPDATE PROMO_MECH_SET_DATA S
       SET S.ASSIGN_SEQ = PN_ASSING_SEQ
     WHERE S.MECH_SET_ID = PN_SECUENCIA
       AND S.PACK_ID = PN_PACK_ID; 
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:='UPDATE_MECH_SET_DATA '||SQLERRM;
  END MKP_UPDATE_MECH_SET_DATA;
  
  PROCEDURE MKP_PROMO_ASSIGN_STATE_2(PN_CUSTOMER_ID      NUMBER,
                                     PN_PACK_ID          NUMBER,
                                     PD_ASSIGN_DATE      DATE,
                                     PV_ERROR            OUT VARCHAR2) IS    
    CURSOR C_ASSIGN_SEQ IS
      SELECT NVL(MAX(T.ASSIGN_SEQ), 0) + 1 ASSIGN_SEQ
        FROM PROMO_ASSIGN_STATE T
       WHERE CUSTOMER_ID = PN_CUSTOMER_ID;
       
    LN_ASSING_SEQ     NUMBER;
  BEGIN
    OPEN C_ASSIGN_SEQ;
    FETCH C_ASSIGN_SEQ INTO LN_ASSING_SEQ;
    CLOSE C_ASSIGN_SEQ;
    
    INSERT INTO PROMO_ASSIGN_STATE
      (CUSTOMER_ID,
       PACK_ID,
       ASSIGN_SEQ,
       SEQ,
       WORK_STATE,
       STATE_DATE,
       REC_VERSION)
    VALUES
      (PN_CUSTOMER_ID, PN_PACK_ID, LN_ASSING_SEQ, 1, 'A', PD_ASSIGN_DATE, 1);
  EXCEPTION
    WHEN OTHERS THEN
       PV_ERROR := 'PROMO_ASSIGN_STATE_2 ' ||SQLERRM;
  END MKP_PROMO_ASSIGN_STATE_2;
  
  PROCEDURE MKP_INACTIVA_DESCUENTO(PN_CUSTOMER_ID NUMBER,
                                   PN_CO_ID       NUMBER,
                                   PN_PACK_ID     NUMBER,
                                   PN_SECUENCIA   NUMBER,
                                   PV_ERROR       OUT VARCHAR2) IS
      
      CURSOR C_CO_ID IS
        SELECT K.*, H.ASSIGN_SEQ
          FROM PROMO_QUAL_RESTRICT K, PROMO_MECH_SET_DATA H
         WHERE K.CUSTOMER_ID = PN_CUSTOMER_ID
           AND K.CONTRACT_ID = PN_CO_ID
           AND K.MECH_SET_ID = PN_SECUENCIA
           AND H.MECH_SET_ID = K.MECH_SET_ID
           AND H.PACK_ID = PN_PACK_ID;
        
      LC_CO_ID       C_CO_ID%ROWTYPE;
      LB_FOUNDQ      BOOLEAN := FALSE;
      
    BEGIN
      
      OPEN C_CO_ID;
      FETCH C_CO_ID INTO LC_CO_ID;
      LB_FOUNDQ := C_CO_ID%FOUND;
      CLOSE C_CO_ID;
         
      IF LB_FOUNDQ = TRUE THEN
        UPDATE PROMO_ASSIGN_STATE A
           SET A.WORK_STATE = 'I'
         WHERE A.CUSTOMER_ID = PN_CUSTOMER_ID
           AND A.PACK_ID = PN_PACK_ID
           AND A.ASSIGN_SEQ = LC_CO_ID.ASSIGN_SEQ
           AND A.WORK_STATE = 'A';
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
       PV_ERROR := 'MKP_INACTIVA_DESCUENTO ' || SQLERRM;
  END MKP_INACTIVA_DESCUENTO;
  --10425 SUD VGU
  
  --07-09-2015
  PROCEDURE MKP_REGISTRA_DESCUENTO_TMP(PV_SERVICIO             VARCHAR2 DEFAULT NULL,
                                       PN_CO_ID                NUMBER DEFAULT NULL,
                                       PN_MONTO_DOLARES        NUMBER DEFAULT NULL,
                                       PD_EXPIRACION           DATE DEFAULT NULL,
                                       PV_ESTADO               VARCHAR2 DEFAULT NULL,
                                       PN_PROMOCION            NUMBER DEFAULT NULL,
                                       PV_CICLO                VARCHAR2 DEFAULT NULL,
                                       PD_FECHA_ACREDITA       DATE DEFAULT NULL,
                                       PD_FECHA_INGRESO        DATE DEFAULT NULL,
                                       PN_DESPACHADOR          NUMBER DEFAULT NULL,
                                       PV_OBSERVACION          VARCHAR2 DEFAULT NULL,
                                       PV_CODIGO_DOC           VARCHAR2 DEFAULT NULL,
                                       PN_CONTRATO             NUMBER DEFAULT NULL,
                                       PV_ID_SUBPRODUCTO       VARCHAR2 DEFAULT NULL,
                                       PN_CUSTOMER_ID          NUMBER DEFAULT NULL,
                                       PN_PACK_ID              NUMBER DEFAULT NULL,
                                       PN_SECUENCIA_BSCS       NUMBER DEFAULT NULL,
                                       PV_TIPO_PROMO           VARCHAR2 DEFAULT NULL,
                                       PD_FECHA_REGISTRO       DATE DEFAULT NULL,
                                       PV_ERROR                OUT VARCHAR2) IS
    CURSOR C_EXISTE IS
      SELECT 'X'
        FROM MK_DESCUENTO_BSCS_TMP I
       WHERE I.ID_SERVICIO = PV_SERVICIO
         AND I.ID_PROMOCION = PN_PROMOCION
         AND I.FECHA_ACREDITA = PD_FECHA_ACREDITA
         AND I.CO_ID = PN_CO_ID
         AND I.CUSTOMER_ID = PN_CUSTOMER_ID
         AND I.ESTADO = 'B';
    LV_EXISTE              VARCHAR2(1);
  BEGIN
    OPEN C_EXISTE;
    FETCH C_EXISTE INTO LV_EXISTE;
    CLOSE C_EXISTE;
    
    IF LV_EXISTE IS NULL THEN
        INSERT INTO MK_DESCUENTO_BSCS_TMP(
        ID_SERVICIO,
        CO_ID,
        MONTO_DOLARES,
        EXPIRACION,
        ESTADO,
        ID_PROMOCION,
        ID_CICLO,
        FECHA_ACREDITA,
        FECHA_INGRESO,
        DESPACHADOR,
        OBSERVACION,
        CODIGO_DOC,
        ID_CONTRATO,
        ID_SUBPRODUCTO,
        CUSTOMER_ID,
        PACK_ID,
        SECUENCIA_BSCS,
        TIPO_PROMO,
        FECHA_REGISTRO
        )
        VALUES(
        PV_SERVICIO,
        PN_CO_ID,
        PN_MONTO_DOLARES,
        PD_EXPIRACION,
        PV_ESTADO,
        PN_PROMOCION,
        PV_CICLO,
        PD_FECHA_ACREDITA,
        PD_FECHA_INGRESO,
        PN_DESPACHADOR,
        PV_OBSERVACION,
        PV_CODIGO_DOC,
        PN_CONTRATO,
        PV_ID_SUBPRODUCTO,
        PN_CUSTOMER_ID,
        PN_PACK_ID,
        PN_SECUENCIA_BSCS,
        PV_TIPO_PROMO,
        PD_FECHA_REGISTRO
        );
    ELSE
      UPDATE MK_DESCUENTO_BSCS_TMP PAT
         SET PAT.CODIGO_DOC = NVL(PV_CODIGO_DOC,PAT.CODIGO_DOC),
             PAT.ID_CONTRATO = NVL(PN_CONTRATO,PAT.ID_CONTRATO),
             PAT.SECUENCIA_BSCS = NVL(PN_SECUENCIA_BSCS,PAT.SECUENCIA_BSCS),
             PAT.MONTO_DOLARES = NVL(PN_MONTO_DOLARES,PAT.MONTO_DOLARES),
             PAT.DESPACHADOR = NVL(PN_DESPACHADOR,PAT.DESPACHADOR),
             PAT.OBSERVACION = NVL(PV_OBSERVACION,PAT.OBSERVACION),
             PAT.TIPO_PROMO = NVL(PV_TIPO_PROMO,PAT.TIPO_PROMO),
             PAT.ID_SUBPRODUCTO = NVL(PV_ID_SUBPRODUCTO,PAT.ID_SUBPRODUCTO)
       WHERE PAT.ID_SERVICIO = PV_SERVICIO
         AND PAT.ID_PROMOCION = PN_PROMOCION
         AND PAT.FECHA_ACREDITA = PD_FECHA_ACREDITA
         AND PAT.CO_ID = PN_CO_ID
         AND PAT.CUSTOMER_ID = PN_CUSTOMER_ID
         AND PAT.ESTADO = 'B';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR: '|| SQLERRM;
  END MKP_REGISTRA_DESCUENTO_TMP;
  
  PROCEDURE MKP_DESACTIVA_DESCUENTO(PD_FECHA_INI         DATE,
                                    PV_ERROR             OUT VARCHAR2) IS
    
    CURSOR C_DESCUENTOS(CD_FECHA_INI DATE) IS
      SELECT A.ROWID DROWID,
             A.ID_PROMOCION,
             A.ID_SERVICIO,
             A.CO_ID,
             A.CODIGO_DOC,
             A.ID_CONTRATO,
             A.ID_SUBPRODUCTO,
             A.MONTO_DOLARES,
             A.ESTADO,
             A.ID_CICLO,
             A.DESPACHADOR,
             A.FECHA_ACREDITA,
             A.FECHA_INGRESO,
             A.EXPIRACION,
             A.OBSERVACION,
             A.CUSTOMER_ID,
             A.PACK_ID,
             A.SECUENCIA_BSCS
        FROM MK_DESCUENTO_BSCS_TMP A
       WHERE A.TIPO_PROMO = 'TARIFA_BASICA'
         AND A.FECHA_ACREDITA = TO_DATE(CD_FECHA_INI,'DD/MM/YYYY')
         AND A.ESTADO = 'B'
         ;
    
    CURSOR C_CICLO(CV_DIA_INICIO IN VARCHAR2) IS
      SELECT FA.ID_CICLO
        FROM FA_CICLOS_BSCS FA
       WHERE FA.DIA_INI_CICLO = CV_DIA_INICIO;

    LV_ERROR             VARCHAR2(1000);
    LV_DIA               VARCHAR2(2);
    LV_CICLO             VARCHAR2(2);
    LN_CONTADOR          NUMBER := 0;
    LB_FOUND             BOOLEAN := FALSE;
    LE_ERROR             EXCEPTION;
    
    PRAGMA AUTONOMOUS_TRANSACTION;
    
  BEGIN
    
    EXECUTE IMMEDIATE 'alter session set nls_date_format = ''dd/mm/rrrr''';
    
    LV_DIA := TO_CHAR(PD_FECHA_INI, 'DD');
  
    OPEN C_CICLO(LV_DIA);
    FETCH C_CICLO INTO LV_CICLO;
    LB_FOUND := C_CICLO%FOUND;
    CLOSE C_CICLO;
  
    IF NOT LB_FOUND THEN
      PV_ERROR := 'No se pudo encontrar el ciclo para este d�a';
      RETURN;
    END IF;
    
    FOR X IN C_DESCUENTOS(PD_FECHA_INI) LOOP
      BEGIN
        IF LB_FOUND = TRUE THEN
            MKK_PROMO_DESCUENTO_BSCS.MKP_INACTIVA_DESCUENTO(PN_CUSTOMER_ID => X.CUSTOMER_ID,
                                                            PN_CO_ID       => X.CO_ID,
                                                            PN_PACK_ID     => X.PACK_ID,
                                                            PN_SECUENCIA   => X.SECUENCIA_BSCS,
                                                            PV_ERROR       => LV_ERROR);

            IF LV_ERROR IS NOT NULL THEN
               LV_ERROR := 'Error en proceso descuento BSCS: ' || LV_ERROR;
               RAISE LE_ERROR;
            ELSE
              UPDATE MK_DESCUENTO_BSCS_TMP PAT
                 SET PAT.ESTADO = 'P'
               WHERE PAT.ID_PROMOCION = X.ID_PROMOCION
                 AND PAT.ID_SERVICIO = X.ID_SERVICIO
                 AND PAT.CUSTOMER_ID = X.CUSTOMER_ID
                 AND PAT.PACK_ID = X.PACK_ID
                 AND PAT.SECUENCIA_BSCS = X.SECUENCIA_BSCS
                 AND PAT.FECHA_ACREDITA = X.FECHA_ACREDITA
                 AND PAT.ESTADO = 'B'
                 AND PAT.ROWID = X.DROWID
              ;
            END IF;
            
            LN_CONTADOR := LN_CONTADOR + 1;
            IF LN_CONTADOR >= 200 THEN
               COMMIT;
               LN_CONTADOR := 0;
            END IF;
        END IF;
      EXCEPTION
       WHEN LE_ERROR THEN
         NULL;
       WHEN OTHERS THEN
         NULL;
      END;
     
    END LOOP;
    
    COMMIT;
    
    PV_ERROR := 'EXITO';
    
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      ROLLBACK;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR: '||SQLERRM;
      ROLLBACK;
  END MKP_DESACTIVA_DESCUENTO;
  --07-09-2015
  
END MKK_PROMO_DESCUENTO_BSCS;
/
