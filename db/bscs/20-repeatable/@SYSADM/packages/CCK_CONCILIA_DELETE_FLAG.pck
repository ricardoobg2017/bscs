CREATE OR REPLACE PACKAGE CCK_CONCILIA_DELETE_FLAG IS

PROCEDURE Principal (Pd_Fecha        OUT DATE,
                     Pv_Error        OUT VARCHAR2);

END CCK_CONCILIA_DELETE_FLAG;
/
CREATE OR REPLACE PACKAGE BODY CCK_CONCILIA_DELETE_FLAG IS

  --=====================================================================================--
  -- CREADO POR:     Andres Balladares.
  -- FECHA MOD:      15/09/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Conciliar el delete flag
  --=====================================================================================--

  PROCEDURE Principal (Pd_Fecha        OUT DATE,
                       Pv_Error        OUT VARCHAR2) IS
    --Obtiene los SnCode activos y suspendidos marcados con delete_flag
    CURSOR C_FEAT_ACT_DELET_FLAG IS
    SELECT P.CO_ID, P.SNCODE, P.LASTMODDATE,
           PH.STATUS, PH.VALID_FROM_DATE,NULL, NULL
      FROM PR_SERV_STATUS_HIST PH, PROFILE_SERVICE P
     WHERE PH.STATUS IN ( 'A','S')
       AND PH.VALID_FROM_DATE BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE + 1)
       AND PH.CO_ID = P.CO_ID
       AND PH.SNCODE = P.SNCODE
       AND PH.HISTNO=P.STATUS_HISTNO
       AND P.DELETE_FLAG IS NOT NULL;
    
    TYPE TAB_CC_DF IS TABLE OF CC_DELETE_FLAG%ROWTYPE;   
    Lr_TAB_CC_DF TAB_CC_DF;
    
  BEGIN
    
    Pd_Fecha:=SYSDATE;
    
    OPEN C_FEAT_ACT_DELET_FLAG;
    WHILE TRUE LOOP 
      FETCH C_FEAT_ACT_DELET_FLAG BULK COLLECT INTO Lr_TAB_CC_DF LIMIT 500;
      
      IF Lr_TAB_CC_DF.count <=0 THEN
         EXIT;
      END IF;
      
      FOR J IN Lr_TAB_CC_DF.FIRST .. Lr_TAB_CC_DF.LAST LOOP
      
          Pv_Error:=NULL; 
          
          BEGIN 
            UPDATE PROFILE_SERVICE P
               SET P.DELETE_FLAG = NULL
             WHERE P.CO_ID = Lr_TAB_CC_DF(J).CO_ID
               AND P.SNCODE = Lr_TAB_CC_DF(J).SNCODE;
          EXCEPTION
            WHEN OTHERS THEN
              Pv_Error := SUBSTR(SQLERRM,1,200);
          END;
          
          Lr_TAB_CC_DF(J).FECHA_REGISTRO := SYSDATE;
          
          IF Pv_Error IS NULL THEN
             Lr_TAB_CC_DF(J).OBSERVACION := 'Conciliado';
          ELSE
             Lr_TAB_CC_DF(J).OBSERVACION := 'Error: '||Pv_Error;
          END IF;
          
      END LOOP;
      
      FORALL I IN Lr_TAB_CC_DF.FIRST .. Lr_TAB_CC_DF.LAST
      INSERT INTO CC_DELETE_FLAG VALUES (Lr_TAB_CC_DF(I).co_id, Lr_TAB_CC_DF(I).sncode,
      Lr_TAB_CC_DF(I).lastmoddate,
      Lr_TAB_CC_DF(I).status, Lr_TAB_CC_DF(I).valid_from_date,Lr_TAB_CC_DF(I).observacion,
      Lr_TAB_CC_DF(I).fecha_registro );
      
      COMMIT;
      
    END LOOP;
    CLOSE C_FEAT_ACT_DELET_FLAG;    
    
    Lr_TAB_CC_DF.DELETE;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'CCK_CONCILIA_DELETE_FLAG.Principal: '||SUBSTR(SQLERRM,1,200);
      Lr_TAB_CC_DF.DELETE;
      IF C_FEAT_ACT_DELET_FLAG%ISOPEN THEN
         CLOSE C_FEAT_ACT_DELET_FLAG;
      END IF;
      ROLLBACK;
  END Principal;

END CCK_CONCILIA_DELETE_FLAG;
/
