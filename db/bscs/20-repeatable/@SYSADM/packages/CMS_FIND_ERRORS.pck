CREATE OR REPLACE package CMS_Find_Errors is

  -- Author  : Ing. Eduardo Mora
  -- Created : 21/05/2004 16:26:01
  -- Purpose : Detecta errores en la Base Datos que provocan falloss en las CMSs
  
  lv_nombre_proceso      varchar2(100):=null;
  lv_nombre_error        varchar2(100):=null;
  -- Public function and procedure declarations
  procedure detecta_errores_BSCS  ;
  procedure corrige_inconsistencias_BSCS ; 
  procedure corrige_origenErroresCMS ; 

end;
/
CREATE OR REPLACE package body CMS_Find_Errors is

  --=====================================================================================--
  -- Versión: 		1.0.0
  -- Author  : Ing. Eduardo Mora
  -- Created : 21/05/2004 16:26:01
  -- Purpose : Detecta errores en la Base Datos que provocan falloss en las CMSs
  -- DESARROLLADO EN CONJUNTO CON CONSULTOR SLB DANIEL SOUZA
  --=====================================================================================--

  -- detecta errores de 4 tipos
  -- por servicios que no pertenecen al plan
  -- por pending request
  -- por hlcodes diferentes
  -- por plcode diferentes
  -- Tiempo estinmado 2h20m
  procedure detecta_errores_bscs is 
  
         v_dn_hlcode number:=0;
         v_cd_hlcode number:=0;
         v_pt_hlcode number:=0;
         v_ca_plcode number:=0;
         v_dn_plcode number:=0;
         v_cd_plcode number:=0;
         v_pt_plcode number:=0;
         v_sm_plcode number:=0;
         v_pending   varchar2(20):=null;
         v_instr	   number:=0;
         v_count	   number:=0;
      begin
         lv_nombre_proceso:='Detecta_error_BSCS';
         for rec in (select co_id, ch_status  from curr_co_status where ch_status in ('a','s')) loop
      		 begin
              -- Hace la busqueda de servicios que no pertenece al plan del contracto.
              lv_nombre_error:='Servicios inconsistentes';
              for rec_inc in (
                      SELECT rh.tmcode,sp.spcode,sp.sncode
                			FROM profile_service ps,
                				 pr_serv_spcode_hist sp,
                				 rateplan_hist rh
                			where ps.co_id = rec.co_id
                			and ps.spcode_histno = sp.histno
                			and sp.co_id = ps.co_id
                			and ps.CO_ID = rh.CO_ID
                			and rh.SEQNO = (select max(seqno) from rateplan_hist x where x.co_id = ps.co_id)
                			and not exists (select 1 from  mpulktmb x where x.tmcode = rh.tmcode
                						   	and x.spcode = sp.spcode and x.sncode = sp.sncode
                							and x.status = 'P'
                							and x.vscode = (select max(vscode) from mpulktmb y where y.tmcode = x.tmcode))) 
              loop
          						
          		    -- Salida, puede direcionala para una tabla
            	    insert into read.cms_find_errores 
                  (co_id,tipo,proceso,sncode,spcode,tmcode,ch_status,observacion )
                  values(rec.co_id, lv_nombre_error, lv_nombre_proceso, rec_inc.sncode, rec_inc.spcodE, rec_inc.tmcode, rec.ch_status, null); 
                  COMMIT;
              end loop;    
      		
          		select dn.hlcode, cd.hlcode, pt.hlcode, ca.plcode, dn.plcode, cd.cd_plcode, pt.plcode,sm.plcode,
          			   decode(dn.DN_STATUS_REQU,null,'N','X')||decode(cd.CD_PENDING_STATE,null,'N','X')||decode(sm.SM_STATUS_REQU,null,'N','X')||
          			   decode(sm.SM_STA_REQU_DATE,null,'N','X')||decode(pt.PORT_SM_REQU,null,'N','X')||decode(pt.PORT_STA_REQU_DATE,null,'N','X')||
          			   decode(pt.PORT_STATUS_REQU,null,'N','X')|| decode(csc.CS_ACTIV_DATE,null,'X','N')||decode(cd.CD_ACTIV_DATE,null,'X','N')||
          			   decode(pt.PORT_ACTIV_DATE,null,'X','N')|| decode(ch.ch_pending,null,'N','X') PENDING
          		into v_dn_hlcode,v_cd_hlcode,v_pt_hlcode,v_ca_plcode,v_dn_plcode,v_cd_plcode,v_pt_plcode,v_sm_plcode,v_pending
          		from storage_medium sm,
          		   		port pt,
          				contr_devices cd,
          				contr_services_cap csc,
          		   		directory_number dn,
          				contract_all ca,
          				contract_history ch
          		where ca.co_id = rec.co_id
          		and cd.co_id = ca.co_id
          		and csc.co_id = ca.co_id
          		and csc.DN_ID = dn.DN_ID
          		and cd.port_id = pt.port_id
          		and cd.cd_seqno = (SELECT max(z.cd_seqno) FROM contr_devices z where z.co_id = cd.co_id)
          		and csc.seqno = (SELECT max(x.seqno) FROM contr_services_cap x where x.co_id = csc.co_id)
          		and cd.CD_SM_NUM = sm.SM_SERIALNUM
          		and ca.co_id = ch.co_id
          		and ch.CH_SEQNO = (SELECT max(y.ch_seqno) FROM contract_history y where y.co_id = ch.co_id)
          		and rownum =1;
          		
          		SELECT count(1) into v_count FROM pr_serv_status_hist where co_id = rec.co_id and valid_from_date is null;
          		
          		-- validacion de los HLR.
          		lv_nombre_error:='Diferentes HLR';
              if (v_dn_hlcode <> v_cd_hlcode) or (v_dn_hlcode <> v_pt_hlcode) then
          		   -- Salida, puede direcionala para una tabla
            	    insert into read.cms_find_errores 
                  (co_id,tipo,proceso,sncode,spcode,tmcode,ch_status,observacion )
                  values(rec.co_id, lv_nombre_error, lv_nombre_proceso, NULL, NULL, NULL, NULL, null); 
          		end if;
          		
          		-- validacion de los PLCODE.
          		lv_nombre_error:='Diferentes PLCODE';
          		if (v_ca_plcode <> v_dn_plcode) or (v_dn_plcode <> v_cd_plcode) or (v_cd_plcode <> v_pt_plcode) or (v_pt_plcode <> v_sm_plcode)then
            	    insert into read.cms_find_errores 
                  (co_id,tipo,proceso,sncode,spcode,tmcode,ch_status,observacion )
                  values(rec.co_id, lv_nombre_error, lv_nombre_proceso, NULL, NULL, NULL, NULL, null); 
          		   -- Salida, puede direcionala para una tabla
          		end if;
          		
          		-- validacion de los Pending Request.
          		lv_nombre_error:='Pending Request';
              SELECT instr(v_pending,'X') into v_instr FROM dual;
          		if v_instr <> 0 or v_count > 0 then
            	    insert into read.cms_find_errores 
                  (co_id,tipo,proceso,sncode,spcode,tmcode,ch_status,observacion )
                  values(rec.co_id, lv_nombre_error, lv_nombre_proceso, NULL, NULL, NULL, NULL, null); 
          		   -- Salida, puede direcionala para una tabla
          		end if;
              COMMIT;
           exception 
              when no_data_found then
                 null;
              when others then null;   
           end; 
      	 end loop;
      end;  

  -- corrige inconsistencias anteriores segun sea el tipo
  -- detecta errores de 4 tipos
  -- por servicios que no pertenecen al plan
  -- por pending request
  -- por hlcodes diferentes
  -- por plcode diferentes
  procedure corrige_inconsistencias_BSCS is

    cursor SERVICIOS is 
         select fe.co_id, fe.rowid id, fe.sncode
         from read.cms_find_errores fe 
         where 
               fe.tipo='Servicios inconsistentes' and 
               fe.observacion is null ;
          
    cursor PENDING is 
         select fe.co_id, fe.rowid id 
         from read.cms_find_errores fe 
         where fe.tipo='Pending Request' and 
               fe.observacion is null ;
         
    cursor HLR_PLCODE is 
         select fe.co_id, fe.rowid id 
         from read.cms_find_errores fe 
         where fe.tipo = 'Diferentes PLCODE' or fe.tipo = 'Diferentes HLR' and 
               fe.observacion is null ;
              
    v_parm_id parameter_value.prm_value_id%type:=null;
    v_co_id contract_all.co_id%type;
    v_sncode mpusntab.sncode%type;
    lv_error           varchar2(256);
    
    begin 
        -- corrige los servicios inconsistentes
        --se pone en comentario SERVICIOS para análisis de casos DCH-13/04/2006
/*        for i in SERVICIOS  loop
          begin
            v_co_id  :=i.co_id;
            v_sncode :=i.sncode;
            begin 
          		select prm_value_id into v_parm_id 
          		from profile_service 
          		where co_id = v_co_id
          		and sncode = v_sncode 
          		and prm_value_id is not null;
          	exception when others then
          	   v_parm_id:= null;
          	end;
          	
          	delete from pr_serv_spcode_hist where co_id = v_co_id and sncode = v_sncode;
          	delete from pr_serv_status_hist where co_id = v_co_id and sncode = v_sncode;
          	delete from contr_vas where co_id = v_co_id and sncode = v_sncode;
          	delete from profile_service where co_id = v_co_id and sncode = v_sncode;
          	delete from contract_service where co_id = v_co_id and sncode = v_sncode;
          	
          	if v_parm_id is not null then
          		delete from parameter_value where prm_value_id = v_parm_id;
          		delete from parameter_value_base where prm_value_id = v_parm_id;
          	end if;
            
   		      update read.cms_find_errores fe 
            set fe.observacion='EXITO Servicio Borrado'
            where fe.co_id = i.co_id and fe.rowid = i.id ;
                        
          	--
          	commit;
            --
          exception
            when others then 
              rollback;
              lv_error:=substr(sqlerrm,1,256);
     		      update read.cms_find_errores fe 
              set fe.observacion=lv_error
              where fe.co_id = i.co_id and fe.rowid = i.id ;
              commit;
          end;  
        end loop;    
        commit;*/
        ---------------------------------------------------------------------------------------

        -- corrige los pending request
        declare 
             cursor csc_dn(p_co_id contract_all.co_id%type) is
               select csc.seqno, csc.dn_id, csc.cs_activ_date, csc.cs_deactiv_date, csc.cs_request, dn.dn_status, dn.dn_status_requ
          	 from contr_services_cap csc,
          	 	  directory_number dn
          	 where csc.co_id = p_co_id
          	 and csc.dn_id = dn.dn_id
          	 order by seqno;
          	 
             cursor cd_pt(p_co_id contract_all.co_id%type) is
               select cd.cd_seqno, cd.port_id, cd.cd_activ_date, cd.cd_deactiv_date, cd.cd_validfrom, cd.cd_moddate, cd.cd_sm_num, cd.cd_pending_state,
          	  		pt.port_status, pt.port_status_requ, pt.port_activ_date
          	 from contr_devices cd,
          	 	  port pt
          	 where cd.co_id = p_co_id
          	 and cd.port_id = pt.port_id
          	 order by cd_seqno;
          
             v_count_rec number:=0;
             v_count_cur number:=0;
             v_count_prt number:=0;
             v_count_smd number:=0;
             v_count_drn number:=0;
             v_count_pnd number:=0;
             v_count_ssh number:=0;
             v_pending   varchar2(20);
             v_status	   varchar2(1);
             v_status_2   varchar2(1);
             v_date    contract_history.ch_validfrom%type;
             v_co_id contract_all.co_id%type;

        begin      
        for i in PENDING loop 
          begin 
            v_co_id :=i.co_id;
            
          	select ch_status into v_status from curr_co_status where co_id = v_co_id;
              -- chequear se el contracto sigue activo o suspendido.
              if v_status <> 'd' then
                 
          	   select a.ch_status, a.ch_validfrom into v_status_2,v_date
                 from contract_history a 
                 where a.co_id = v_co_id 
                 and a.ch_seqno = (select max(b.ch_seqno) from contract_history b where b.co_id = a.co_id);
          	   
          	   -- arregla contr_services_cap y directory_number.   
                 select count(1) into v_count_rec from contr_services_cap where co_id = v_co_id;
                 
                 for rec in csc_dn(v_co_id) loop
                    v_count_cur := csc_dn%rowcount;
          		  
          		  -- chequear se el numero de telefono no eres activo en otro contracto.
          	      select count(1) into v_count_drn 
          	      from contr_services_cap
          	      where dn_id = rec.dn_id 
          	      and co_id <> v_co_id 
          	      and cs_deactiv_date is null;
                    
              	  -- chequear se eres la ultima secuencia.
              	  if v_count_cur = v_count_rec then
          		      -- chequear se no hay un deactivacion piendente.
          			  if v_status_2 <> 'd' then
          	    	     if rec.cs_activ_date is null then
          	    			update contr_services_cap
          	    			set cs_activ_date = (select ts from mdsrrtab where request = rec.cs_request),
          	    				cs_deactiv_date = null
          	    			where co_id = v_co_id
          	    			and seqno = rec.seqno;
          	    		 end if;
          	    		 
          	    		 if v_count_drn = 0 then
          	    			update directory_number
          	    			set dn_status_requ = null,
          	    			   	  dn_status = 'a'
          	    			where dn_id = rec.dn_id;
          	    		 else
          	    		    update read.cms_find_errores fe 
                        set fe.observacion='dn_id activo en otro contracto'
                        where fe.co_id = i.co_id and fe.rowid = i.id ;
          	    		 end if;
              		 else
          	    		update contr_services_cap
          	    		set cs_deactiv_date = v_date
          	    		where co_id = v_co_id
          	    		and seqno = rec.seqno;
          	    		 
          	    		 if v_count_drn = 0 then
          	    			update directory_number
          	    			set dn_status_requ = null,
          	    			   	  dn_status = 'r'
          	    			where dn_id = rec.dn_id;
          	    		 end if;
          				 
          			 end if;
                    else
              	     if rec.cs_deactiv_date is null then
              			update contr_services_cap
              			set cs_deactiv_date = (select ts from mdsrrtab where request = rec.cs_request)
              			where co_id = v_co_id
              			and seqno = rec.seqno;
              		 end if;
              		 
              		 if v_count_drn = 0 then
              			update directory_number
              			set dn_status_requ = null,
              			   	  dn_status = 'r'
              			where dn_id = rec.dn_id;
              		 end if;
              		 
              	  end if;
                 end loop;
                 
                 -- Arregla CONTR_DEVICES, PORT y STORAGE_MEDIUM
                 select count(1) into v_count_rec from contr_devices where co_id = v_co_id;
                 
                 for rec_2 in cd_pt(v_co_id) loop
                    v_count_cur := cd_pt%rowcount;
          		  
              	 -- chequear se el port no eres activo en otro contracto.
              	 select count(1) into v_count_prt 
              	 from contr_devices 
              	 where port_id = rec_2.port_id 
              	 and co_id <> v_co_id 
              	 and cd_deactiv_date is null;
          		 
              	 -- chequear se el esn no eres activo en otro contracto.
              	 select count(1) into v_count_smd
              	 from contr_devices 
              	 where cd_sm_num = rec_2.cd_sm_num 
              	 and co_id <> v_co_id 
              	 and cd_deactiv_date is null;
                    
              	  -- chequear se eres la ultima secuencia.
              	  if v_count_cur = v_count_rec then
          		  	 if v_status_2 <> 'd' then
              				update contr_devices
              	    		set cd_activ_date = decode(rec_2.cd_activ_date,null,rec_2.cd_moddate,rec_2.cd_activ_date),
              	    			cd_deactiv_date = null,
                          cd_validfrom = decode(cd_validfrom,null,rec_2.cd_moddate,cd_validfrom),
              	    			cd_pending_state = null
              	    		where co_id = v_co_id
              	    		and cd_seqno = rec_2.cd_seqno;
          	    
          	    		 if v_count_prt = 0 then
          	       		    update port
          	       		    set port_status = 'a',
          	       		   	    port_sm_requ = null,
          	       			    port_status_requ = null,
                            port_activ_date = decode(port_activ_date,null,rec_2.cd_moddate,port_activ_date),
          	    				port_deactiv_date = null
          	       		    where port_id = rec_2.port_id;
          	    		 else
          	    		    update read.cms_find_errores fe 
                        set fe.observacion='port activo en otro contracto'
                        where fe.co_id = i.co_id and fe.rowid = i.id ;
          	    		 end if;
          	    		 
          	    		 if v_count_smd = 0 then
          	    			 update storage_medium
          	    			 set sm_status = 'a',
          	    				 sm_status_requ = null,
          	    				 sm_sta_requ_date = null
          	    			 where sm_serialnum = rec_2.cd_sm_num;
          	    		 else
          	    		    update read.cms_find_errores fe 
                        set fe.observacion='esn activo en otro contracto'
                        where fe.co_id = i.co_id and fe.rowid = i.id ;
          	    		 end if;	
              		 else
          	    		update contr_devices
          	    		set cd_deactiv_date = v_date,
          	    			cd_validfrom = v_date,
          					cd_pending_state = null
          	    		where co_id = v_co_id
          	    		and cd_seqno = rec_2.cd_seqno;
          	    
          	    		 if v_count_prt = 0 then
          	       		    update port
          	       		    set port_status = 'r',
          	       		   	    port_sm_requ = null,
          	       			    port_status_requ = null,
          	       			    port_activ_date = null,
          	    				port_deactiv_date = null
          	       		    where port_id = rec_2.port_id;
          	    		 end if;
          	    		 
          	    		 if v_count_smd = 0 then
          	    			 update storage_medium
          	    			 set sm_status = 'r',
          	    				 sm_status_requ = null,
          	    				 sm_sta_requ_date = null
          	    			 where sm_serialnum = rec_2.cd_sm_num;
          	    		 end if;	
          			 
          			 end if;
                    else
                			update contr_devices
                    		set cd_deactiv_date = decode(rec_2.cd_deactiv_date,null,rec_2.cd_moddate,rec_2.cd_deactiv_date),
                          cd_validfrom = decode(cd_validfrom,null,rec_2.cd_moddate,cd_validfrom),
                    			cd_pending_state = null
                    		where co_id = v_co_id
                    		and cd_seqno = rec_2.cd_seqno;
              		 
              		if v_count_prt = 0 then
              		   update port
              		   set port_status = 'r',
              		   	   port_sm_requ = null,
              			   port_status_requ = null,
              			   port_activ_date = null
              		   where port_id = rec_2.port_id;
              		end if;		
              		
              		if v_count_smd = 0 then
              		   update storage_medium
              		   set sm_status = 'r',
              			   sm_status_requ = null,
              			   sm_sta_requ_date = null
              		   where sm_serialnum = rec_2.cd_sm_num;
              		end if;	
              	  
              	  end if;
              	  
                 end loop;
                 
                 -- pending en la tabla pr_serv_status_hist.
                 update pr_serv_status_hist
                 set valid_from_date = entry_date
                 where co_id = v_co_id
                 and valid_from_date is null;
                 
              	-- Chequear se despues del updates el contracto sigue con Pending Request
              	SELECT DECODE(DN.DN_STATUS_REQU,NULL,'N','X')||DECODE(CD.CD_PENDING_STATE,NULL,'N','X')||DECODE(SM.SM_STATUS_REQU,NULL,'N','X')||
              		   DECODE(SM.SM_STA_REQU_DATE,NULL,'N','X')||DECODE(PT.PORT_SM_REQU,NULL,'N','X')||DECODE(PT.PORT_STA_REQU_DATE,NULL,'N','X')||
              		   DECODE(PT.PORT_STATUS_REQU,NULL,'N','X')|| DECODE(CSC.CS_ACTIV_DATE,NULL,'X','N')||DECODE(CD.CD_ACTIV_DATE,NULL,'X','N')||
              		   DECODE(PT.PORT_ACTIV_DATE,NULL,'X','N') PENDING
              	into v_pending
              	from storage_medium sm,
              	   		port pt,
              			contr_devices cd,
              			contr_services_cap csc,
              	   		directory_number dn,
              			contract_all ca
              	where ca.co_id = v_co_id
              	and cd.co_id = ca.co_id
              	and csc.co_id = ca.co_id
              	and csc.dn_id = dn.dn_id
              	and cd.port_id = pt.port_id
              	and cd.cd_seqno = (select max(z.cd_seqno) from contr_devices z where z.co_id = cd.co_id)
              	and csc.seqno = (select max(x.seqno) from contr_services_cap x where x.co_id = csc.co_id)
              	and cd.cd_sm_num = sm.sm_serialnum
              	and rownum =1;
              	
              	select count(1) into v_count_ssh from pr_serv_status_hist where co_id = v_co_id and valid_from_date is null;
              	
              	SELECT INSTR(V_PENDING,'X') INTO V_COUNT_PND FROM DUAL;
              	
              	if v_count_pnd <> 0 or v_count_ssh > 0 then
      	    		   update read.cms_find_errores fe 
                   set fe.observacion='sigue con pending request'
                   where fe.co_id = i.co_id and fe.rowid = i.id ;
              	else 
          	   	   update contract_history
          	   	   set ch_pending = null
          	  	   where co_id = v_co_id;
                   
      	    		   update read.cms_find_errores fe 
                   set fe.observacion='EXITO Corregido Pending'
                   where fe.co_id = i.co_id and fe.rowid = i.id ;
                end if;
              	  
                 commit;
              else
  	    		    update read.cms_find_errores fe 
                set fe.observacion='el contracto esta dectivado'
                where fe.co_id = i.co_id and fe.rowid = i.id ;
          	end if;
          exception 
            when others then 
  	    		    rollback;
                lv_error:=substr(sqlerrm,1,256);
                update read.cms_find_errores fe 
                set fe.observacion=lv_error
                where fe.co_id = i.co_id and fe.rowid = i.id ;
          	    commit;
          end;  
        end loop;
        end;
        ---------------------------------------------------------------------------------------
    
        -- se reprocesa los errores de HLR y Plcode diferentes
        declare
        	v_co_id   contract_all.co_id%type;
        	v_dn_hlr	directory_number.hlcode%type;
        	v_cd_hlr	contr_devices.hlcode%type;
        	v_pt_hlr	port.hlcode%type;
        	v_ca_plc	contract_all.plcode%type;
        	v_dn_plc	directory_number.plcode%type;
        	v_cd_plc	contr_devices.cd_plcode%type;
        	v_sm_plc	storage_medium.plcode%type;
        	v_dn_dn_id  directory_number.dn_id%type;
        	v_pt_dn_id  port.dn_id%type;
        	v_port_num  port.port_num%type;
        	v_dn_num	directory_number.dn_num%type;
        	v_cd_seq	contr_devices.cd_seqno%type;
        	v_cs_seq	contr_services_cap.seqno%type;
        	v_port_id	port.port_id%type;
        	v_pt_id_act	port.port_id%type;
        	v_sncode	mpusntab.sncode%type;
        	v_count		number;
        	v_count_prt number;
        	v_upd_pt	number:=1;
        	v_date		contr_devices.cd_activ_date%type;
        	v_manual	number:=1;
          
        begin 
        for i in HLR_PLCODE  loop
           begin 

            	v_co_id  :=i.co_id;
              
            	-- Busqueda de datos.
            	select dn.hlcode, cd.hlcode, pt.hlcode,ca.plcode, dn.plcode, cd.cd_plcode, sm.plcode, cd.cd_activ_date, 
            		   dn.dn_id, pt.dn_id, pt.port_num, dn.dn_num, cd.cd_seqno, csc.seqno, pt.port_id, csc.sncode
            	into v_dn_hlr, v_cd_hlr, v_pt_hlr, v_ca_plc, v_dn_plc, v_cd_plc, v_sm_plc, v_date,
            		 v_dn_dn_id, v_pt_dn_id, v_port_num, v_dn_num, v_cd_seq, v_cs_seq, v_port_id, v_sncode
            	from storage_medium sm,
            	   		port pt,
            			contr_devices cd,
            			contr_services_cap csc,
            	   		directory_number dn,
            			contract_all ca
            	where ca.co_id = v_co_id
            	and cd.co_id = ca.co_id
            	and csc.co_id = ca.co_id
            	and csc.dn_id = dn.dn_id
            	and cd.port_id = pt.port_id
            	and cd.cd_seqno = (select max(z.cd_seqno) from contr_devices z where z.co_id = cd.co_id)
            	and csc.seqno = (select max(x.seqno) from contr_services_cap x where x.co_id = csc.co_id)
            	and cd.cd_sm_num = sm.sm_serialnum;
            	
            	select count(1) into v_count 
            	from contr_devices 
            	where port_id = v_port_id
            	and co_id <> v_co_id
            	and cd_deactiv_date is null;
            	
            	begin
            	   select port_id into v_pt_id_act
            	   from port
            	   where port_num = substr(v_dn_num,-8);
            	exception when no_data_found then
            	   v_upd_pt :=0;
            	   goto jump;
            	end;
            	
            	select count(1) into v_count_prt 
                from contr_devices 
                where port_id = v_pt_id_act 
                and co_id <> v_co_id 
                and cd_deactiv_date is null;
            	
            	if v_count_prt > 0 then
            	   v_manual := 0;   
            	   goto final;
            	end if;
            
            	<<jump>>
            	
            	if (v_dn_hlr = v_pt_hlr) then
               	   if v_pt_hlr <> v_cd_hlr then
            	   	  update contr_devices
            	   	  set hlcode = v_dn_hlr,
            		  	  cd_plcode = v_dn_plc
            	   	  where co_id = v_co_id
            	   	  and cd_seqno = v_cd_seq;
            	   end if;
            	   
            	   -- Actualizase solo para TDMA.
            	   if v_dn_dn_id = v_pt_dn_id and v_sncode = 91 then
            		  if  v_port_num <> substr(v_dn_num,-8) then
            			  if v_upd_pt = 0 then
            			     update port
            		  	  	 set port_num =  substr(v_dn_num,-8)
            		  	  	 where port_id = v_port_id;
            			  else
            			  	 update contr_devices
            				 set port_id = v_pt_id_act
            				 where co_id = v_co_id
            	   	  		 and cd_seqno = v_cd_seq;
            				 
            				 update port
            				 set port_status = 'a',
            				 	 plcode = v_dn_plc,
            					 hlcode = v_dn_hlr,
            					 dn_id = v_dn_dn_id,
            					 port_activ_date = v_date
            				 where port_id = v_pt_id_act;
            			  end if;
            		  end if;
            	   else
            	   	  if v_sncode = 91 then
            			 if v_count = 0 then
            			 	if v_upd_pt = 0 then
            				    update port
            				 	set port_num = substr(v_dn_num,-8),
            				 	    dn_id = v_dn_dn_id
            				    where port_id = v_port_id;
            			     else
            			  	    update contr_devices
            				 	set port_id = v_pt_id_act
            				 	where co_id = v_co_id
            	   	  		 	and cd_seqno = v_cd_seq;
            				 
            				 	update port
            				 	set port_status = 'a',
            				 	   plcode = v_dn_plc,
            					   hlcode = v_dn_hlr,
            					   dn_id = v_dn_dn_id,
            					   port_activ_date = v_date
            				 	where port_id = v_pt_id_act;
            			    end if;
            			 else
        	    		    update read.cms_find_errores fe 
                      set fe.observacion='Arreglar PORT manualmente'
                      where fe.co_id = i.co_id and fe.rowid = i.id ;
            			 end if;
            		  end if;
            		  
            	   end if; 
            	else  
            	   if v_dn_dn_id = v_pt_dn_id then
            	   	  if  v_sncode = 91 then
            		  	 if v_upd_pt = 0 then
            			    update port
            		  	 	set hlcode = v_dn_hlr,
            			 	   port_num =  substr(v_dn_num,-8)
            		  	 	where port_id = v_port_id;
            			 else
            			  	 update contr_devices
            				 set port_id = v_pt_id_act
            				 where co_id = v_co_id
            	   	  		 and cd_seqno = v_cd_seq;
            				 
            				 update port
            				 set port_status = 'a',
            				 	 plcode = v_dn_plc,
            					 hlcode = v_dn_hlr,
            					 dn_id = v_dn_dn_id,
            					 port_activ_date = v_date
            				 where port_id = v_pt_id_act;
            			 end if;
            		  end if;
            	   else
            	   	  if v_sncode = 91 then
            			 if v_count = 0 then
            				 if v_upd_pt = 0 then
            				    update port
            				 	set port_num = substr(v_dn_num,-8),
            				 	   dn_id = v_dn_dn_id,
            					   hlcode = v_dn_hlr
            				 	where port_id = v_port_id;
            				 else
            				  	 update contr_devices
            					 set port_id = v_pt_id_act
            					 where co_id = v_co_id
            		   	  		 and cd_seqno = v_cd_seq;
            					 
            					 update port
            					 set port_status = 'a',
            					 	 plcode = v_dn_plc,
            						 hlcode = v_dn_hlr,
            						 dn_id = v_dn_dn_id,
            						 port_activ_date = v_date
            					 where port_id = v_pt_id_act;
            				 end if;
            			 else
        	    		    update read.cms_find_errores fe 
                      set fe.observacion='Arreglar PORT manualmente'
                      where fe.co_id = i.co_id and fe.rowid = i.id ;
            			 end if;
            		  end if;
            	   end if;
            	   
               	   if v_dn_hlr <> v_cd_hlr then
            	   	  update contr_devices
            	   	  set hlcode = v_dn_hlr,
            		  	  cd_plcode = v_dn_plc
            	   	  where co_id = v_co_id
            	   	  and cd_seqno = v_cd_seq;
            	   end if;
            	end if;
 
	    		    update read.cms_find_errores fe 
              set fe.observacion='EXITO correcion PL-HLR'
              where fe.co_id = i.co_id and fe.rowid = i.id ;
 
            	--
              commit;
            	--
            	<<final>>
            	if v_manual = 0 then
     	    		    update read.cms_find_errores fe 
                  set fe.observacion='Hacer la actualización de PORT y HLR manualmente'
                  where fe.co_id = i.co_id and fe.rowid = i.id ;
            	end if;
           exception 
     	    		when others then    
                  rollback;
                  lv_error:=substr(sqlerrm,1,250);
                  update read.cms_find_errores fe 
                  set fe.observacion=lv_error
                  where fe.co_id = i.co_id and fe.rowid = i.id ;
                  commit;
           end;
        end loop;
        end;
        ---------------------------------------------------------------------------------------
    end;

    
    -- Corrige Inconsistenciaqs en BScs que desembocan en errores Cms 
    -- Desarrollados y avalizado por Daniel Souza
    -- Junio 23 2004, 
    -- Antes se lo ejecutaba desde Axis(julio 2003)
    procedure corrige_origenErroresCMS is 

    	v_qtde number   :=0;
    	v_update number :=0;
      lv_observación  varchar2(256);
      
    begin	
    	lv_nombre_proceso:='corrige_origenErroresCMS';
      -- Fix the services without VALID_FROM_DATE.
    	for rec in (SELECT co_id,sncode,histno 
        		   FROM pr_serv_status_hist 
    	           where VALID_FROM_DATE is null) loop
    	
    		update pr_serv_status_hist
    		set valid_from_date = entry_date
    		where co_id = rec.co_id 
    		and sncode = rec.sncode
    		and histno = rec.histno
    		and valid_from_date is null;
    		
    		v_update:=v_update+1;
        -- Salida, puede direcionala para una tabla
        lv_nombre_error:='VALID_FROM_DATE';
        lv_observación:='EXITO corregido';
  	    insert into read.cms_find_errores 
        (co_id,tipo,proceso,sncode,observacion )
        values(rec.co_id, lv_nombre_error, lv_nombre_proceso, rec.sncode, lv_observación ); 
    	end loop;
    	commit;
     	--dbms_output.put_line('Services without VALID_FROM_DATE fixed: '||v_update);
    	v_update :=0;
    	
       -- Fix ESN with status 'a' without CO_ID active.'
       for rec in (SELECT sm_id,sm_serialnum FROM storage_medium where plcode = 2 and sm_status = 'a') loop
          SELECT count(1) into v_qtde FROM contr_devices where CD_SM_NUM = rec.sm_serialnum and CD_DEACTIV_DATE is null;
    	  if v_qtde = 0 then
    	     update storage_medium set sm_status = 'r' where sm_id = rec.sm_id;
           -- Salida, puede direcionala para una tabla
           lv_nombre_error:='CONTRATOS INACTIVOS - ESN ACTIVOS';
           lv_observación:='EXITO corregido';
      	   insert into read.cms_find_errores 
           (sm_serialnum,tipo,proceso,observacion)
           values(rec.sm_serialnum, lv_nombre_error, lv_nombre_proceso, lv_observación ); 
         	 v_update:=v_update+1;
    	  end if;
      end loop;
       commit;
       --dbms_output.put_line('ESN with status "a" without CO_ID active fixed: '||v_update);
    	 v_update :=0;
    	 
       -- Fix DN_ID with status 'r' with CO_ID active,
       for rec in (select /*+ RULE +*/ dn_id 
           	   	  from contr_services_cap
          		  where cs_deactiv_date is null and CS_ACTIV_DATE is not null
          		  and dn_id in (select dn_id from directory_number where dn_status <> 'a')) loop
    	  update directory_number
    	  set dn_status = 'a'
    	  where dn_id = rec.dn_id;
    	  v_update:=v_update+1;
        -- Salida, puede direcionala para una tabla
        lv_nombre_error:='CONTRATOS ACTIVOS - DN INACTIVOS';
        lv_observación:='EXITO corregido';
  	    insert into read.cms_find_errores 
        (dn_id,tipo,proceso,observacion)
        values(rec.dn_id, lv_nombre_error, lv_nombre_proceso, lv_observación ); 
       end loop;
       commit;
     	--dbms_output.put_line('DN_ID with status "r" with CO_ID active: '||v_update);
      
      -- emo 26 feb 2004
      -- Fix the storage mediums with status a and cont_devices inactives
       for rec in (select /*+ RULE +*/ sm_id,sm_serialnum from storage_medium sm
                   where sm.sm_status = 'a' and sm.plcode=1
                   and not exists (select 1 from contr_devices cd where cd.cd_sm_num = sm.sm_serialnum 
                   and cd.cd_deactiv_date is null) )loop 
        update storage_medium sm 
        set sm_status = 'r'
        where sm.sm_id = rec.sm_id;
        v_update:=v_update+1;
        -- Salida, puede direcionala para una tabla
        lv_nombre_error:='CONTRATOS INACTIVOS - SIM ACTIVOS';
        lv_observación:='EXITO corregido';
  	    insert into read.cms_find_errores 
        (sm_serialnum,tipo,proceso,observacion)
        values(rec.sm_serialnum, lv_nombre_error, lv_nombre_proceso, lv_observación ); 
       end loop;
       commit;
     	 --dbms_output.put_line('SM_ID with status "a" with CO_ID INactive: '||v_update);
      
        -- emo 08 jul 2004
        -- Libera telefonos y StorageMedium 
        update /*+ rule +*/ storage_medium b set sm_status='r'
        where plcode = 1 and b.sm_status = 'd' ; --and b.sm_status_mod_date > to_date('01/05/2003','dd/mm/yyyy')
        
        update /*+ rule +*/ storage_medium b set sm_status='r'
        where plcode = 2 and b.sm_status = 'd' ; 
        
        update /*+ rule +*/ directory_number dn set dn.dn_status='r'
        where dn.plcode = 1 and dn.dn_status = 'd' ;
        
        update /*+ rule +*/ directory_number dn set dn.dn_status='r'
        where dn.plcode = 2 and dn.dn_status = 'd' ;
        
        commit;
      
    end;

    
end;  
/

