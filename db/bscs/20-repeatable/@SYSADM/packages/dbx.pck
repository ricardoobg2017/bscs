CREATE OR REPLACE PACKAGE dbx AUTHID CURRENT_USER
/*
**
** Author: Joachim Keck, LH Specifications GmbH
**
** Filenames: dbx_spp.sql - Package header
**            dbx_spp.sql - Package body
**
** Version: /main/231
**
** Level:   DBEXTRACT_2.00_130710
**
** Overview:
**
** This package contains utility procedures for database administration.
** It wrappes some Oracle DDL statements for better error handling.
**
**
** Major Modifications (when, who, what)
**
** 02/05 - JKE - add
** 08/03 - JKE - add SetPartition
** 09/01 - JKE - add DATAPUMP support with Oracle 11
** 09/01 - JKE - mod FillTableDBXCash , insert instead of deletes , ORDER and CASH tables can be excluded
**
** 30.May.2008 - JKE - 307155 DBX> Different handling of dealers in BSCS IX Rel II
** 04.Jul.2008 - JKE - 309623 DBX> Implement support for Oracle 11 datapump since exp is desupported
** 22.Jul.2008 - JKE - 309861 DAB_SCRIPTS: DB Extract Problem
** 12.Aug.2008 - JKE - 306807 DATABASE SCRIPTS: db-extract tool takes alot of time without finishing (hangs)
** 15.Aug.2008 - JKE - 311352 DBX> Colum contract_all.ohxact must not be used in BSCS IX Rel. II
** 20.Mar.2009 - JKE - 323795 DBX> Adjust restrictions  for parameter special_cust_id
** 20.Mar.2009 - JKE - 324195 DBX> Adjust the way STORAGE_MEDIUM are selected for some releases.
** 23.Apr.2009 - JKE - 322813 DAB_SCRIPTS: DBX (IMP) flat customer not "implicitely" included
** 28.Apr.2009 - JKE - 325917 DBX: DBEXTRACT performance
** 11.May.2009 - JKE - 326620 DBX> Reduce LC_TRANSACTION tables
** 12.Jun.2009 - JKE - 326897 DBX> Adjustments of DBExtract for BSCS IX Release III
** 19.Jun.2009 - JKE - 329368 DBX> Adjustments of DBExtract for NII
** 09.Jul.2009 - JKE - 329178 DBX: FillTableDBXCustomer_Roam: Error
** 14.Jul.2009 - JKE - 330975 DBX> Problem in Oracle 9  with ORA-00972 and ORA-2063 with database links
** 28.May.2010 - JKE - 348259 DBX> problem in the extraction criteria
** 02.Jul.2010 - JKE - 350421 DBX> Change query for Exclude_Tab_Without replace ALL_TAB_PRIVS_MADE by DBA_TAB
** 19.Jul.2010 - JKE - 350882 DBX> - index index FKICONSCAP_CS_REQUEST
** 06.Sep.2010 - JKE - 353519 Statistics on working tables of DBExtract
** 29.Sep.2010 - JKE - 355425 DBX> workaround for Oracle bugfix 3431498 for DBMS_STATS package
** 07.Oct.2010 - JKE - 355933 DBX: DBEXTRACT_2.00_35310.tar: dbextract.sql is hanging
** 10.Nov.2010 - JKE - 358062 DBX> bundle feature requires more data of parameter_value table
** 16.Nov.2010 - JKE - 358313 DBX:Disable export of REQUEST tables
** 18.Nov.2010 - JKE - 358307 DBX: Disable export of FEES with period=0
** 19.Nov.2010 - JKE - 358314 DBX: Restriction of PROMO_MECH_SET tables
** 06.Jan.2011 - JKE - 360910 DBextract Tool on IDC03 shipment not working
** 28.Feb.2011 - JKE - 363386 DBX> Move log info for procedure SetPartition to table DBX_LOG
** 20.Apr.2011 - JKE - 365984 DBX> Allocate Extents of partitions for Oracle 11.2
** 03.May.2011 - JKE - 358438 Deletion of FEES by DBX_IMP_DEL_FK slow
** 25.May.2011 - JKE - 367198 DBX> Reduce table FUP_ACCOUNTS_HIST in BSCS 6.00 Releases
** 12.Jul.2011 - JKE - 368583_dbx DBX: Support for ECMS DB extracts
** 26.Sep.2011 - JKE - 372451 DBX does not filter the table CUSTOMER_ALL_HIST
** 06.Dec.2011 - JKE - 376556 DATABASE SCRIPTS: DBExtract tool for BSCS 5.01
** 14.Dec.2011 - JKE - 377271 DBX> Increase table name in Exclude_Tab procedure
** 20.Feb.2012 - JKE - 381122 DBX> Remove dropped tables in IX Release IV from DBExtract
** 03.May.2012 - JKE - 384712_dbx> DAB_SCRIPTS: Incorrect Billing_Account_ID in PROMISED_PAYMENT table
** 10.Jul.2012 - JKE - 387900 DAB_SCRIPTS: DBExtract: no max number of contracts for dummy customer
** 10.Jul.2012 - JKE - 387901 DBX: DBExtract: dr_documents does not exist
** 12.Sep.2012 - JKE - 390304 DAB_SCRIPTS: Problem to extract UDR data
** 25.Oct.2012 - JKE - 391707 DBX: DBEXTRACT_2.00_28808: dbx ignores [2-9]_large_account if 1_l
** 25.Oct.2012 - JKE - 391767 DBX: DBEXTRACT_2.00_28808: dbx.WherePKAll wrong clause selecting
** 16.Jan.2013 - JKE - 393922 DBX> Export table PARAMETER_VALUE by using the CO_ID when possible
** 29.Jan.2013 - JKE - 392832 DBX> DBExtract> not aligned to ECMS
*/
IS


 /*
   **=====================================================
   **
   ** Procedure: SQLTraceOn
   **
   ** Purpose:   Procedure to switch SQL output on
   **
   **=====================================================
   */

PROCEDURE SQLTraceOn;


 /*
   **=====================================================
   **
   ** Procedure: SQLTraceOff
   **
   ** Purpose:   Procedure to switch SQL output on
   **
   **=====================================================
   */

PROCEDURE SQLTraceOff;


 /*
   **=====================================================
   **
   ** Procedure: CheckRangeID
   **
   ** Purpose:   Verify if between clauses should be used
   **
   **=====================================================
*/


PROCEDURE CheckRangeID (
        piosRangeName IN VARCHAR2
);



 /*
   **=====================================================
   **
   ** Procedure: CallInfo
   **
   ** Purpose:   Procedure for information output
   **
   **=====================================================
   */

PROCEDURE CallInfo( piosInfoText IN VARCHAR2 );

 /*
   **=====================================================
   **
   ** Procedure: CallError
   **
   ** Purpose:   Procedure for error output
   **
   **=====================================================
   */

PROCEDURE CallError( piosInfoText IN VARCHAR2 );




 /*
   **=====================================================
   **
   ** Procedure: SetParameter
   **
   ** Purpose:  Writes a parameter to table dbx_paramatere
   **
   **=====================================================
*/
   PROCEDURE SetParameter
   (
        -- Parameter Name
        piosParameterName IN VARCHAR2
        -- Parameter Value
        ,piosParameterValue IN VARCHAR2  DEFAULT NULL
   );

 /*
   **=====================================================
   **
   ** Function:  GetParameter
   **
   ** Purpose:  Reads a parameter from table dbx_parameters
   **
   **=====================================================
*/


   FUNCTION  GetParameter
   (
        -- Parameter Name
        piosParameterName IN VARCHAR2
   ) RETURN VARCHAR2 ;


 /*
   **=====================================================
   **
   ** Function:  GetTime
   **
   ** Purpose:   Get actual Time string
   **
   **=====================================================
*/

FUNCTION GetTime
RETURN VARCHAR2 ;


 /*
   **=====================================================
   **
   ** Procedure: CreateWhereClause
   **
   ** Purpose:  Creates where clause to get cusotmer_ids
   **
   **=====================================================
*/

PROCEDURE CreateWhereClause;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_1
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_1;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomerIXPre
   **
   ** Purpose:   Fill Table DBX_Customer
   **
   **=====================================================
*/

PROCEDURE FillTableDBXCustomerIXPre;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_2
   **
   ** Purpose:   Fill Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_2;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Roam
   **
   ** Purpose:   Fill Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Roam;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Roam6
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Roam6;




 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Large
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Large;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_SP
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_SP;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_LargeL
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_LargeL;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Admin
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Admin;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_SELECT
   **
   ** Purpose:   Fill Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_SELECT;




 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract_OHXACT
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXContract_OHXACT;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract_OHXACT2
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/

PROCEDURE FillTableDBXContract_OHXACT2;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXorderhdr_cust
   **
   ** Purpose:   Fille Table DBX_orderhdr_customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXorderhdr_cust;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_oh
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_oh;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPrmMechSet
   **
   ** Purpose:   Fille Table DBX_PRM_MECH_SET
   **
   **=====================================================
*/

PROCEDURE FillTableDBXPrmMechSet;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Customer
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Customer
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
     -- Where Dealer
    ,piosDealer      IN VARCHAR2  DEFAULT NULL
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
);

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_PRM_MechSetID
   **
   ** Purpose:   Define Where condition for PROMO_MECH_SET and PROMO_MECH_SET_DATA
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_PRM_MechSetID
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_dealer
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_dealer;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract
   **
   ** Purpose:   Fille Table DBX_Contract
   **
   **=====================================================
*/


PROCEDURE FillTableDBXContract;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXTickler
   **
   ** Purpose:   Fille Table DBX_Tickler
   **
   **=====================================================
*/


PROCEDURE FillTableDBXTickler;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Tickler
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Tickler
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Contract
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Contract
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCRHRequest
   **
   ** Purpose:   Fille Table DBX_CRH_Request
   **
   **=====================================================
*/

PROCEDURE FillTableDBXCRHRequest;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXRequest
   **
   ** Purpose:   Fille Table DBX_Request
   **
   **=====================================================
*/


PROCEDURE FillTableDBXRequest;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXRequest6
   **
   ** Purpose:   Fille Table DBX_Request
   **
   **=====================================================
*/


PROCEDURE FillTableDBXRequest6;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_CRHRequest
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_CRHRequest
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Request
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Request
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPort
   **
   ** Purpose:   Fille Table DBX_PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPort;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPort6
   **
   ** Purpose:   Fille Table DBX_PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPort6;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPORTFree;
   **
   ** Purpose:   Fille Table DBX_PORT with free port numbers
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPORTFree;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Port
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Port
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDN
   **
   ** Purpose:   Fille Table DBX_DN
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDN;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDNFree
   **
   ** Purpose:   Fille Table DBX_DN with free directory numbers
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDNFree;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_DN
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_DN
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXSM
   **
   ** Purpose:   Fille Table DBX_SM
   **
   **=====================================================
*/


PROCEDURE FillTableDBXSM;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXSMFree
   **
   ** Purpose:   Fille Table DBX_SM with free storage mediums without PORTS
   **
   **=====================================================
*/


PROCEDURE FillTableDBXSMFree;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_SM
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_SM
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXOrderhdr
   **
   ** Purpose:   Fille Table DBX_ORDERHDR
   **
   **=====================================================
*/


PROCEDURE FillTableDBXOrderhdr;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_GLENTRY
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_GLENTRY
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Orderhdr
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Orderhdr
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCash
   **
   ** Purpose:   Fille Table DBX_ORDERHDR
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCash;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXParameterValue
   **
   ** Purpose:   Fille Table DBX_PARAMETER_VALUE
   **
   **=====================================================
*/


PROCEDURE FillTableDBXParameterValue;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXParameterValue6
   **
   ** Purpose:   Fille Table DBX_PARAMETER_VALUE
   **
   **=====================================================
*/


PROCEDURE FillTableDBXParameterValue6;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_ParaValue
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_ParaValue
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashrcpt
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashrcpt
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashdetail
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashdetail
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashtrl
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashtrl
(
     -- Table Name
     piosTableName      IN VARCHAR2
     -- Column Name
    ,piosColumnName     IN VARCHAR2  DEFAULT NULL
     -- Column NameCash
    ,piosColumnNameCash IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint           IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Equip
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Equip
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_UserAccess
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_UserAccess
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXMpurhtab
   **
   ** Purpose:   Fille Table DBX_MPURHTAB
   **
   **=====================================================
*/


PROCEDURE FillTableDBXMpurhtab;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXMpurhtab6
   **
   ** Purpose:   Fille Table DBX_MPURHTAB
   **
   **=====================================================
*/


PROCEDURE FillTableDBXMpurhtab6;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Mpurhtab
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Mpurhtab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Mpurhtab6
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Mpurhtab6
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Date
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Date
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Column Value
    ,piosColumnValue IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDocument
   **
   ** Purpose:   Fille Table DBX_ORDERHDR
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDocument;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Document
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Document
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab
   **
   ** Purpose:   Exclude Tables from being copied
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab
(
     -- Table Name
     piosTableName IN VARCHAR2
    ,piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
);

 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Without_Seg
   **
   ** Purpose:   Exclude Tables from being copied, which are empty
   **            and do not have a segment. This is new with Oracle 11.2 .
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Without_Seg
(
     piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
);


 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Without
   **
   ** Purpose:   Exclude Tables from being copied, which do not have column comment or index or constraint
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Without
(
     piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
);




 /*
   **=====================================================
   **
   ** Procedure: Include_Any_Tab
   **
   ** Purpose:   Include Tables to be copied completely
   **            piosOverwrite = Y , Includes also tables, which are already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Include_Any_Tab
(
     -- Table Name
     piosTableName IN VARCHAR2
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
);





 /*
   **=====================================================
   **
   ** Procedure: WhereAll
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE WhereAll
(
     -- Table Name
     piosColumnName IN VARCHAR2
     -- Column Exclude List
    ,piosColumnExcludeList IN VARCHAR2
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
     -- Timestamp required
    ,piosTIMESTAMP   IN VARCHAR2  DEFAULT 'Y'
     -- Storage ID
    ,piosStorageIDList   IN VARCHAR2  DEFAULT 'NULL'
    -- Context Key
    ,piosContextKeyList  IN VARCHAR2  DEFAULT 'NULL'
);



 /*
   **=====================================================
   **
   ** Procedure: Include_Tab
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE Include_Tab;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXFfcode
   **
   ** Purpose:   Fille Table DBX_Ffcode
   **
   **=====================================================
*/


PROCEDURE FillTableDBXFfcode;

 /*
   **=====================================================
   **
   ** Procedure: Mark_long
   **
   ** Purpose:   Exclude tables with long or long raw columns
   **
   **=====================================================
*/


PROCEDURE Mark_long;

 /*
   **=====================================================
   **
   ** Procedure: Mark_iot_table
   **
   ** Purpose:   Exclude tables which are Index organized or Partitioned or Clustered
   **
   **=====================================================
*/


PROCEDURE Mark_iot_table
(
    -- DB Link
     piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
);



 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSTables
   **
   ** Purpose:   Copy BSCS tables from BSCS DB and UDR DB to DBX DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSTables;

 /*
   **=====================================================
   **
   ** Procedure: CreateDatabaseLink
   **
   ** Purpose:   Create a database link
   **
   **=====================================================
*/


PROCEDURE CreateDatabaseLink
(
     -- DB Link
     piosDBLink     IN VARCHAR2
     -- DB User
    ,piosDBUser     IN VARCHAR2
     -- DB Password
    ,piosDBPW       IN VARCHAR2
     -- DB Connect String
    ,piosDBConnect  IN VARCHAR2
     -- DB Connect String Current
    ,piosDBConnectCurr IN VARCHAR2
);


 /*
   **=====================================================
   **
   ** Procedure: DropTable
   **
   ** Purpose:   Drop a DBX Table
   **
   **=====================================================
*/


PROCEDURE DropTable
(
     -- Table Name
     piosTableName  IN VARCHAR2
);

 /*
   **=====================================================
   **
   ** Procedure: CreateTable
   **
   ** Purpose:   Create a Table
   **
   **=====================================================
*/

PROCEDURE CreateTable
(
     -- Table Name
     piosTableName  IN VARCHAR2
    ,piosStatement  IN VARCHAR2
);

 /*
   **=====================================================
   **
   ** Procedure: CreateDBXTables
   **
   ** Purpose:   Create a Table
   **
   **=====================================================
*/

PROCEDURE CreateDBXTables
(
     -- OrganisationType
     piosOrgType   IN VARCHAR2
);



 /*
   **=====================================================
   **
   ** Procedure: GatherDBXTableStat
   **
   ** Purpose:   Gather Table statistics for DBX tables
   **
   **=====================================================
*/

PROCEDURE GatherDBXTableStat
(
     -- OrganisationType
     piosSchema    IN VARCHAR2
    ,piosTablename IN VARCHAR2 DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: GatherTableStat
   **
   ** Purpose:   Gather Table statistics for DBX tables
   **
   **=====================================================
*/


PROCEDURE GatherTableStat
(
     -- OrganisationType
     piosSchema    IN VARCHAR2
    ,piosTablename IN VARCHAR2
);

 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSColumnDefaults
   **
   ** Purpose:   Copy BSCS default column values from BSCS DB and UDR DB to DBX DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSColumnDefaults
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
);

 /*
   **=====================================================
   **
   ** Procedure: SetStorage
   **
   ** Purpose:   Set Storage in DBX_TABLES and CreateClauses
   **
   **=====================================================
*/


PROCEDURE SetStorage
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
);


 /*
   **=====================================================
   **
   ** Procedure: SetPartition
   **
   ** Purpose:   Set Partition Info in DBX_TABLES
   **
   **=====================================================
*/


PROCEDURE SetPartition
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
);




 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Col
   **
   ** Purpose:   Exclude Tables from being copied
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Col
(
     -- Table Name
     piosColumnName IN VARCHAR2
    ,piosExclude    IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite  IN VARCHAR2 DEFAULT 'N'
);

 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSUser
   **
   ** Purpose:   Copy Database User from BSCS DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSUser;


 /*
   **=====================================================
   **
   ** Procedure: DropSynonym
   **
   ** Purpose:   Drop a DBX Synonym
   **
   **=====================================================
*/

PROCEDURE DropSynonym
(
     -- Synonym Name
     piosSynonymName  IN VARCHAR2
    ,piosSynonymOwner IN VARCHAR2 DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: IsRelease
   **
   ** Purpose:   check BSCS Version
   **
   **=====================================================
*/


PROCEDURE IsRelease (
    -- ReleaseNo
    piosReleaseNo IN OUT INTEGER
);


 /*
   **=====================================================
   **
   ** Procedure: IsMPUFFTAB
   **
   ** Purpose:   check MPUFFTAB structure
   **
   **=====================================================
*/


PROCEDURE IsMPUFFTAB;


 /*
   **=====================================================
   **
   ** Procedure: IsMPURHTAB6
   **
   ** Purpose:   check MPURHTAB structure
   **
   **=====================================================
*/


PROCEDURE IsMPURHTAB6 (
    -- Release No for structure
    piosReleaseNo IN OUT INTEGER
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXBillseqno
   **
   ** Purpose:   Fille Table DBX_BILLSEQNO
   **
   **=====================================================
*/

PROCEDURE FillTableDBXBillseqno;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Billseqno
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Billseqno
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXThufitab
   **
   ** Purpose:   Fill Table DBX_THUFITAB
   **
   **=====================================================
*/

PROCEDURE FillTableDBXThufitab;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Thufitab
   **
   ** Purpose:   Define Where condition for THUFITAB
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Thufitab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXThufotab
   **
   ** Purpose:   Fill Table DBX_THUFOTAB
   **
   **=====================================================
*/

PROCEDURE FillTableDBXThufotab;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Thufotab
   **
   ** Purpose:   Define Where condition for THUFOTAB
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Thufotab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);



 /*
   **=====================================================
   **
   ** Procedure: WherePKALL
   **
   ** Purpose:   Define a WHERE condition on all tables with FK to piosTableName
   **            where FK column is also PK column of that table
   **
   **=====================================================
*/


PROCEDURE WherePKAll
(
     -- Table Name
     piosTableName      IN VARCHAR2
     -- Table Name
    ,piosConstraintName IN VARCHAR2
     -- Column Name
    ,piosColumnName IN VARCHAR2
     -- DBX Table Name
    ,piosDBXTableName   IN VARCHAR2
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
);


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_BILL
   **
   ** Purpose:   Update Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_BILL;

 /*
   **=====================================================
   **
   ** Procedure: DBXLogStart
   **
   ** Purpose:  Writes an info to table dbx_log
   **
   **=====================================================
*/

PROCEDURE DBXLogStart
(
     -- Table     Name
     piosTableName IN VARCHAR2
     -- Action    Name
    ,piosActionName IN VARCHAR2
     -- Message   Value
    ,piosMessage    IN VARCHAR2  DEFAULT ' '
     -- Para1     Value
    ,piosPara1      IN VARCHAR2  DEFAULT ' '
     -- Para2     Value
    ,piosPara2      IN VARCHAR2  DEFAULT ' '
     -- Para3     Value
    ,piosPara3      IN VARCHAR2  DEFAULT ' '
);

 /*
   **=====================================================
   **
   ** Procedure: DBXLogStop
   **
   ** Purpose:  Writes an info to table dbx_log
   **
   **=====================================================
*/

PROCEDURE DBXLogStop
(
     -- Table     Name
     piosTableName IN VARCHAR2
     -- Action    Name
    ,piosActionName IN VARCHAR2
     -- Message   Value
    ,piosMessage    IN VARCHAR2  DEFAULT NULL
     -- Para1     Value
    ,piosPara1      IN VARCHAR2  DEFAULT NULL
     -- Para2     Value
    ,piosPara2      IN VARCHAR2  DEFAULT NULL
     -- Para3     Value
    ,piosPara3      IN VARCHAR2  DEFAULT NULL
);

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXLCTransaction
   **
   ** Purpose:   Fill Table DBX_LCTransaction
   **
   **=====================================================
*/


PROCEDURE FillTableDBXLCTransaction;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXFupAccounts
   **
   ** Purpose:   Fill Table DBX_FUP_ACCOUNTS
   **
   **=====================================================
*/


PROCEDURE FillTableDBXFupAccounts;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPayment
   **
   ** Purpose:   Fille Table DBX_Payment
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPayment;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPortReq
   **
   ** Purpose:   Fille Table DBX_PortReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPortReq;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCrpReq
   **
   ** Purpose:   Fille Table DBX_CrpReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCrpReq;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCdsReq
   **
   ** Purpose:   Fille Table DBX_CdsReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCdsReq;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPaymentProp
   **
   ** Purpose:   Fille Table DBX_Payment_Prop
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPaymentProp;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_FUP_ACCOUNTS_HIST
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_FupAccountsHist
(
    piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Function : SetDBX_Where_Entdate
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Entdate
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
);


 /*
   **=====================================================
   **
   ** Function : PrintBodyVersion
   **
   ** Purpose:   return the version control information
   **
   **=====================================================
*/

  FUNCTION PrintBodyVersion RETURN VARCHAR2;



/*
 * Variables
 */

ShowStatementFlag VARCHAR2(1) := 'Y';

END DBX;
/
CREATE OR REPLACE PACKAGE BODY DBX
/*
**
**
** References:
**    HLD plsql/73998.rtf: Database Migration Concepts II
**
** Author: Joachim Keck, LH Specifications GmbH
**
** Filenames: dabutil.sph - Package header
**            dabutil.spb - Package body
**
** Overview:
**
** This package contains utility procedures Rfor database administration.
** It wrappes some Oracle DDL statements for better error handling.
**
**
** Major Modifications (when, who, what)
**
** 11/00 - PSO - Initial version of DabUtil package
**
** 06/02 - NKU - Drop All Constaints are added
**
** 07/02 - OLA - Recompile invalid Packages and Triggers
**
** 07/02 - OLA - Drop Sequence
**
** 02/05 - JKE - add AlterTableAddColumn and AlterTableModifyColumn
**
** 02/05 - JKE - add AlterTableAddConstraint
*/
IS



   /*
   **=====================================================
   **
   ** Procedure: ShowStatement
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */

      PROCEDURE ShowStatement( piosInfoText IN VARCHAR2 )
      IS
      BEGIN
       if ShowStatementFlag = 'Y' then
         CallInfo( substr(piosInfoText ,1,70) );
         if length(piosInfoText) > 70 then
            CallInfo( substr(piosInfoText ,71,70) );
         end if;
         if length(piosInfoText) > 140 then
            CallInfo( substr(piosInfoText ,141,70) );
         end if;
         if length(piosInfoText) > 210 then
            CallInfo( substr(piosInfoText ,211,70) );
         end if;
         if length(piosInfoText) > 280 then
            CallInfo( substr(piosInfoText ,281,70) );
         end if;
         if length(piosInfoText) > 350 then
            CallInfo( substr( piosInfoText ,351,70) );
         end if;
         if length(piosInfoText) > 420 then
            CallInfo( substr(piosInfoText ,421,70) );
         end if;
         if length(piosInfoText) > 490 then
            CallInfo( substr( piosInfoText ,491,70) );
         end if;
         if length(piosInfoText) > 560 then
            CallInfo( substr( piosInfoText ,561,70) );
         end if;
         if length(piosInfoText) > 630 then
            CallInfo( substr(piosInfoText ,631,70) );
         end if;
         if length(piosInfoText) > 700 then
            CallInfo( substr( piosInfoText ,701,70) );
         end if;
         if length(piosInfoText) > 770 then
            CallInfo( substr( piosInfoText ,771,70) );
         end if;
         if length(piosInfoText) > 840 then
            CallInfo( substr( piosInfoText ,841,70) );
         end if;
         if length(piosInfoText) > 910 then
            CallInfo( substr( piosInfoText ,911,70) );
         end if;
         if length(piosInfoText) > 980 then
            CallInfo( substr( piosInfoText ,981,70) );
         end if;
         if length(piosInfoText) > 1050 then
            CallInfo( substr( piosInfoText ,1051,70) );
         end if;
         if length(piosInfoText) > 1120 then
            CallInfo( substr( piosInfoText ,1121,70) );
         end if;
         if length(piosInfoText) > 1190 then
            CallInfo( substr( piosInfoText ,1191,70) );
         end if;
         if length(piosInfoText) > 1260 then
            CallInfo( substr( piosInfoText ,1261,70) );
         end if;
         if length(piosInfoText) > 1330 then
            CallInfo( substr( piosInfoText ,1331,70) );
         end if;
         if length(piosInfoText) > 1400 then
            CallInfo( substr( piosInfoText ,1401,70) );
         end if;
         if length(piosInfoText) > 1470 then
            CallInfo( substr( piosInfoText ,1471,70) );
         end if;
         if length(piosInfoText) > 1540 then
            CallInfo( substr( piosInfoText ,1541,70) );
         end if;
         if length(piosInfoText) > 1610 then
            CallInfo( substr( piosInfoText ,1611,70) );
         end if;
         if length(piosInfoText) > 1680 then
            CallInfo( substr( piosInfoText ,1681,70) );
         end if;
         if length(piosInfoText) > 1750 then
            CallInfo( substr( piosInfoText ,1751,70) );
         end if;
         if length(piosInfoText) > 1820 then
            CallInfo( substr( piosInfoText ,1821,70) );
         end if;
         if length(piosInfoText) > 1890 then
            CallInfo( substr( piosInfoText ,1891,70) );
         end if;
         if length(piosInfoText) > 1960 then
            CallInfo( substr( piosInfoText ,1961,70) );
         end if;
         if length(piosInfoText) > 2030 then
            CallInfo( substr( piosInfoText ,2031,70) );
         end if;
         if length(piosInfoText) > 2100 then
            CallInfo( substr( piosInfoText ,2101,70) );
         end if;
         if length(piosInfoText) > 2170 then
            CallInfo( substr( piosInfoText ,2171,70) );
         end if;
         if length(piosInfoText) > 2240 then
            CallInfo( substr( piosInfoText ,2241,70) );
         end if;
         if length(piosInfoText) > 2310 then
            CallInfo( substr( piosInfoText ,2311,70) );
         end if;
         if length(piosInfoText) > 2380 then
            CallInfo( substr( piosInfoText ,2381,70) );
         end if;
         if length(piosInfoText) > 2450 then
            CallInfo( substr( piosInfoText ,2451,70) );
         end if;
         if length(piosInfoText) > 2520 then
            CallInfo( substr( piosInfoText ,2521,70) );
         end if;
         if length(piosInfoText) > 2590 then
            CallInfo( substr( piosInfoText ,2591,70) );
         end if;
       end if;


      END ShowStatement;


 /*
   **=====================================================
   **
   ** Procedure: SQLTraceOn
   **
   ** Purpose:   Procedure to switch SQL output on
   **
   **=====================================================
   */

      PROCEDURE SQLTraceOn
      IS
      BEGIN
         dbx.ShowStatementFlag := 'Y';
         DBMS_OUTPUT.PUT_LINE( 'REM INFO: SQLTraceOn ' );
      END SQLTraceOn;


 /*
   **=====================================================
   **
   ** Procedure: SQLTraceOff
   **
   ** Purpose:   Procedure to switch SQL output on
   **
   **=====================================================
   */

      PROCEDURE SQLTraceOff
      IS
      BEGIN
         dbx.ShowStatementFlag := 'N';
         DBMS_OUTPUT.PUT_LINE( 'REM INFO: SQLTraceOff' );
      END SQLTraceOff;


   /*
   **=====================================================
   **
   ** Procedure: CallInfo
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */

      PROCEDURE CallInfo( piosInfoText IN VARCHAR2 )
      IS
      BEGIN
         DBMS_OUTPUT.PUT_LINE( substr('REM INFO: ' || piosInfoText ,1,250) );
      END CallInfo;
   /*
   **=====================================================
   **
   ** Procedure: CallError
   **
   ** Purpose:  Privat procedure for error print out (debug).
   **
   **=====================================================
   */

      PROCEDURE CallError( piosInfoText IN VARCHAR2 )
      IS
      BEGIN
--       DBMS_OUTPUT.PUT_LINE( substr('REM ERROR: ' || piosInfoText ,1,250) );
         RAISE_APPLICATION_ERROR(-20001,substr(piosInfoText ,1,512) );
      END CallError;


 /*
   **=====================================================
   **
   ** Procedure: SetParameter
   **
   ** Purpose:  Writes a parameter to table dbx_paramatere
   **
   **=====================================================
*/


PROCEDURE SetParameter
(
     -- Parameter Name
     piosParameterName IN VARCHAR2
     -- Parameter Value
    ,piosParameterValue IN VARCHAR2  DEFAULT NULL
)
AS
 lonCounter    INTEGER;
 str           VARCHAR2(30000);
BEGIN
 str := 'select count(*) ';
 str := str || ' from   dbx_parameter ';
 str := str || ' where  dbx_parameter_name = upper(' || '''' || piosParameterName || '''' || ')';

 execute immediate str into lonCounter;
 if lonCounter = 0 then
     str := 'insert /*+ APPEND */ into dbx_parameter (dbx_parameter_name,dbx_parameter_value) ';
     str := str || 'values (upper(' || '''' || piosParameterName || '''' || '),' ||
                   '''' || replace(piosParameterValue,'''','''''') || '''' || ')';
     execute immediate str;
 else
    str := 'update dbx_parameter set dbx_parameter_value = ' || '''' || replace(piosParameterValue,'''','''''') || '''';
    str := str || ' where  dbx_parameter_name = upper(' || '''' || piosParameterName || '''' || ')';
    execute immediate str;
 end if;
 commit;

EXCEPTION WHEN OTHERS THEN
  ShowStatement(str);
  CallInfo('SetParameter: Error:');
  CallError(SQLERRM);
END SetParameter;


 /*
   **=====================================================
   **
   ** Procedure: DBXLogStart
   **
   ** Purpose:  Writes an info to table dbx_log
   **
   **=====================================================
*/

PROCEDURE DBXLogStart
(
     -- Table     Name
     piosTableName IN VARCHAR2
     -- Action    Name
    ,piosActionName IN VARCHAR2
     -- Message   Value
    ,piosMessage    IN VARCHAR2  DEFAULT ' '
     -- Para1     Value
    ,piosPara1      IN VARCHAR2  DEFAULT ' '
     -- Para2     Value
    ,piosPara2      IN VARCHAR2  DEFAULT ' '
     -- Para3     Value
    ,piosPara3      IN VARCHAR2  DEFAULT ' '
)
AS
 str           VARCHAR2(30000);
BEGIN
 str := 'INSERT /*+ APPEND */ INTO dbx_log (dbx_table_name,action,message,para1 ,para2,para3,starttime) VALUES (';
 str := str || '''' || piosTableName || '''';
 str := str || ',' || '''' || piosActionName || '''';
 str := str || ',' || '''' || piosMessage || '''';
 str := str || ',' || '''' || replace(replace(piosPara1,'''',''''''),chr(10),' ') || '''';
 str := str || ',' || '''' || replace(replace(piosPara2,'''',''''''),chr(10),' ') || '''';
 str := str || ',' || '''' || replace(replace(piosPara3,'''',''''''),chr(10),' ') || '''';
 str := str || ', sysdate )';
 execute immediate str;
 commit;
EXCEPTION WHEN OTHERS THEN
  ShowStatement(str);
  CallInfo('SetParameter: Error:');
--  CallError(SQLERRM);
END DBXLogStart;

 /*
   **=====================================================
   **
   ** Procedure: DBXLogStop
   **
   ** Purpose:  Writes an info to table dbx_log
   **
   **=====================================================
*/

PROCEDURE DBXLogStop
(
     -- Table     Name
     piosTableName IN VARCHAR2
     -- Action    Name
    ,piosActionName IN VARCHAR2
     -- Message   Value
    ,piosMessage    IN VARCHAR2  DEFAULT NULL
     -- Para1     Value
    ,piosPara1      IN VARCHAR2  DEFAULT NULL
     -- Para2     Value
    ,piosPara2      IN VARCHAR2  DEFAULT NULL
     -- Para3     Value
    ,piosPara3      IN VARCHAR2  DEFAULT NULL
)
AS
 str           VARCHAR2(30000);
BEGIN
 str := 'UPDATE dbx_log SET ';
 str := str || ' finishtime =  sysdate ';
 IF piosMessage IS NOT NULL THEN
    str := str || ',message = ' || '''' || replace(replace(piosMessage,'''',''''''),chr(10),' ') || '''';
 END IF;
 IF piosPara1 IS NOT NULL THEN
    str := str || ',para1   = ' || '''' || replace(replace(piosPara1,'''',''''''),chr(10),' ') || '''';
 END IF;
 IF piosPara2 IS NOT NULL THEN
    str := str || ',para2   = ' || '''' || replace(replace(piosPara2,'''',''''''),chr(10),' ') || '''';
 END IF;
 IF piosPara3 IS NOT NULL THEN
    str := str || ',para3   = ' || '''' || replace(replace(piosPara3,'''',''''''),chr(10),' ') || '''';
 END IF;
 str := str || ' WHERE dbx_table_name = ' ||  '''' || piosTableName || '''' ;
 str := str || ' AND   action  = ' ||  '''' || piosActionName || '''' ;
 execute immediate str;
 commit;
EXCEPTION WHEN OTHERS THEN
  ShowStatement(str);
  CallInfo('SetParameter: Error:');
--  CallError(SQLERRM);
END DBXLogStop;



 /*
   **=====================================================
   **
   ** Function:  GetParameter
   **
   ** Purpose:  Reads a parameter from table dbx_parameters
   **
   **=====================================================
*/

FUNCTION GetParameter
(
     -- Parameter Name
     piosParameterName IN VARCHAR2
) RETURN VARCHAR2
AS
 losParameterValue VARCHAR2(4000);
 str               VARCHAR2(30000);
BEGIN
 str := 'select dbx_parameter_value ';
 str := str || ' from   dbx_parameter ';
 str := str || ' where  dbx_parameter_name = upper(' || '''' || piosParameterName || '''' || ')';

 execute immediate str into losParameterValue ;

 RETURN losParameterValue;

EXCEPTION
WHEN no_data_found  THEN
  ShowStatement(str);
  CallInfo('GetParameter: No Value for ' || piosParameterName || ' found.');
  losParameterValue := '';
  RETURN losParameterValue;
WHEN OTHERS THEN
  ShowStatement(str);
  CallError(SQLERRM);
  RETURN('');
END GetParameter;



 /*
   **=====================================================
   **
   ** Function:  GetTime
   **
   ** Purpose:   Get actual Time string
   **
   **=====================================================
*/

FUNCTION GetTime
RETURN VARCHAR2
AS
BEGIN
  RETURN TO_CHAR(sysdate,'YYYY-MM-DD HH24.MI.SS') ;
EXCEPTION
WHEN OTHERS THEN
  CallError(SQLERRM);
  RETURN('');
END GetTime;


 /*
   **=====================================================
   **
   ** Procedure: SetDBXWhereClause
   **
   ** Purpose:  Writes a row to table dbx_tables
   **
   **=====================================================
*/


PROCEDURE SetDBXWhereClause
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosCreateClause IN VARCHAR2  DEFAULT NULL
     -- Where Link Clause
    ,piosWhereClause IN VARCHAR2  DEFAULT NULL
     -- Where Link Clause
    ,piosWhereLinkClause IN VARCHAR2  DEFAULT NULL
     -- Hint
    ,piosHint        IN VARCHAR2  DEFAULT NULL
     -- Hint
    ,piosStorage     IN VARCHAR2  DEFAULT NULL
     -- DBConnect
    ,piosDBConnect   IN VARCHAR2  DEFAULT NULL
     -- Exclude flag
    ,piosExclude     IN VARCHAR2  DEFAULT 'N'
     -- Overwrite flag
    ,piosOverwrite   IN VARCHAR2  DEFAULT 'Y'
    -- Schema Name
    ,piosSchemaName  IN VARCHAR2  DEFAULT 'SYSADM'
)
AS
 lonCounter    INTEGER;
 str           VARCHAR2(30000);
BEGIN
 str := 'select count(*) ';
 str := str || ' from  dbx_tables ';
 str := str || 'where  dbx_table_name = upper(' || '''' || piosTableName || '''' || ')';
 str := str || '  and  dbx_schema_name = upper(' || '''' || piosSchemaName || '''' || ')';

-- ShowStatement(str);
 execute immediate str into lonCounter;

 if lonCounter = 0 then

    str := 'insert /*+  APPEND */ into dbx_tables ' ;
    str := str || ' (dbx_table_name,dbx_column_name,dbx_create_clause,';
    str := str || '  dbx_where_condition,dbx_where_link_condition,dbx_hint,';
    str := str || '  dbx_db_connect , dbx_exclude ,dbx_schema_name ) ';
    str := str || ' values (upper(' || '''' || piosTableName  || '''' || ') ';
    str := str || '        ,upper(' || '''' || piosColumnName || '''' || ')';
    str := str || '        ,' || '''' || replace(piosCreateClause,'''','''''')     || '''' || ' ' ;
    str := str || '        ,' || '''' || replace(piosWhereClause,'''','''''')      || '''' || ' ' ;
    str := str || '        ,' || '''' || replace(piosWhereLinkClause,'''','''''')  || '''' || ' ' ;
    str := str || '        ,' || '''' || piosHint             || '''' || ' ' ;
    str := str || '        ,' || '''' || piosDBConnect        || '''' || ' ' ;
    str := str || '        ,' || '''' || piosExclude          || '''' || ' ' ;
    str := str || '        ,' || '''' || piosSchemaName       || '''' || ')' ;
    execute immediate str;
    commit;
    if piosWhereClause = '1 = 2' then
       if piosSchemaName = 'SYSADM' then
          CallInfo('Exclude: ' || piosTableName);
       else
          CallInfo('Exclude: ' || piosSchemaName || '.' || piosTableName);
       end if;
    end if;
 else
    if piosOverwrite = 'Y' then
       str := 'update dbx_tables set ';
       str := str || ' dbx_column_name = upper(' || '''' || piosColumnName || '''' || ')';
       str := str || ',dbx_create_clause = ' || '''' || replace(piosCreateClause,'''','''''') || '''';
       str := str || ',dbx_where_condition = ' || '''' || replace(piosWhereClause,'''','''''') || '''';
       str := str || ',dbx_where_link_condition = ' || '''' || replace(piosWhereLinkClause,'''','''''') || '''';
       str := str || ',dbx_hint = ' || '''' || piosHint || '''';
       str := str || ',dbx_db_connect  = ' || '''' || piosDBConnect || '''';
       str := str || ',dbx_exclude     = ' || '''' || piosExclude || '''';
       str := str || ',dbx_schema_name = ' || '''' || piosSchemaName || '''';
       str := str || ' where  dbx_table_name = upper(' || '''' || piosTableName || '''' || ')';
       str := str || '   and  dbx_schema_name = upper(' || '''' || piosSchemaName || '''' || ')';
       execute immediate str;
       commit;
       if piosWhereClause = '1 = 2' then
          if piosSchemaName = 'SYSADM' then
             CallInfo('Exclude: ' || piosTableName);
          else
             CallInfo('Exclude: ' || piosSchemaName || '.' || piosTableName);
          end if;
       end if;
    end if;
 end if;

EXCEPTION WHEN OTHERS THEN
  ShowStatement(str);
  CallInfo('SetDBXWhereClause: Error:');
  CallError(SQLERRM);
END SetDBXWhereClause;




 /*
   **=====================================================
   **
   ** Procedure: CreateWhereClause
   **
   ** Purpose:  Creates where clause to get customer_ids
   **
   **=====================================================
*/

PROCEDURE CreateWhereClause
AS

dummy          integer;
dummy_number   NUMBER;
vl0            VARCHAR2(200);
vl1            VARCHAR2(200);
vl2            VARCHAR2(200);
vl3            VARCHAR2(200);
vl4            VARCHAR2(200);
vl5            VARCHAR2(200);
vl6            VARCHAR2(200);
vl7            VARCHAR2(200);
vl8            VARCHAR2(200);
vl9            VARCHAR2(200);
vl             VARCHAR2(2000);
vl_counter     INTEGER := 0;
vs             VARCHAR2(2000);
vr             VARCHAR2(2000);
vr_a           VARCHAR2(2000);
vr_b           VARCHAR2(2000);
vr_c           VARCHAR2(2000);
vc_high        VARCHAR2(2000);
vc_low         VARCHAR2(2000);
vc_total       VARCHAR2(2000);
vc_special     VARCHAR2(4000);
vcs            VARCHAR2(4000);
vc             VARCHAR2(2000);
vc_cstestbill  VARCHAR2(2000);
vc_billcycle   VARCHAR2(2000);
vm_customer_id INTEGER;
GLOBAL_HINT    VARCHAR2(2000);
CUSTOMER_ALL_HINT VARCHAR2(2000);
vdbBSCS              VARCHAR2(2000);

vstr           VARCHAR2(32000);
vialink        VARCHAR2(1);
BEGIN


-- CallInfo('CreateWhereClause: Step BEGIN  ');

 vc_high             := GetParameter('cust_id_high');
 vc_low              := GetParameter('cust_id_low');
 vc_total            := GetParameter('cust_total');
 vc_special          := GetParameter('special_cust_id');
 vc_cstestbill       := GetParameter('cg');
 vc_billcycle        := GetParameter('billcyc');
 GLOBAL_HINT         := GetParameter('GLOBAL_HINT');
 CUSTOMER_ALL_HINT   := GetParameter('CUSTOMER_ALL_HINT');
 vdbBSCS             := GetParameter('vdbBSCS');
 vialink             := GetParameter('vialink');

 IF vc_high IS NULL THEN
    vc_high := '0';
 END IF;

 BEGIN
   dummy_number := TO_NUMBER(LTRIM(RTRIM(vc_high)));
 EXCEPTION WHEN OTHERS THEN
   CallError('CreateWhereClause: Invalid values for cust_id_high.');
 END;

 IF vc_low IS NULL THEN
    vc_low := '1';
 END IF;

 BEGIN
   dummy_number := TO_NUMBER(LTRIM(RTRIM(vc_low )));
 EXCEPTION WHEN OTHERS THEN
   CallError('CreateWhereClause: Invalid values for cust_id_low.');
 END;

 IF vc_total IS NULL THEN
    vc_total := '0';
 END IF;

 BEGIN
   dummy_number := TO_NUMBER(LTRIM(RTRIM(vc_total)));
 EXCEPTION WHEN OTHERS THEN
   CallError('CreateWhereClause: Invalid values for cust_total.');
 END;


 IF TO_NUMBER(LTRIM(RTRIM(vc_total))) > 0 THEN
  -- CallInfo('CreateWhereClause: Step 1 ');
  -- If High Bound is not set take the maximum CUSTOMER_ID
  IF (vc_high  IS NULL
      OR TO_NUMBER(LTRIM(RTRIM(vc_high ))) = 0) THEN
     -- Get MAX Customer ID
       BEGIN
         vstr := 'SELECT /*+ ' || GLOBAL_HINT || ' ' ||  CUSTOMER_ALL_HINT || ' */ NVL(MAX(CUSTOMER_ID),0) ';
         vstr :=  vstr || ' FROM   CUSTOMER_ALL';
         if vialink = 'Y' then
            vstr :=  vstr || '@' || vdbBSCS;
         end if;
         ShowStatement(vstr); -- PN 329178
         execute immediate vstr into vm_customer_id ;
         CallInfo('CreateWhereClause: vm_customer_id ' || TO_CHAR(vm_customer_id) );
         vc_high := TO_CHAR(vm_customer_id);
         CallInfo('CreateWhereClause: vc_high        ' || vc_high );
       EXCEPTION WHEN OTHERS THEN
         CallInfo(vstr);
         CallInfo(SQLERRM);
         CallError('CreateWhereClause: Error in selecting max CUSTOMER_ID.');
       END;
  END IF;

 -- Check cust_id_high and cust_id_low
  dummy := vc_high - vc_low;

  -- CallInfo('CreateWhereClause: Step 2 ');
  if dummy < -1 then
   CallError('CreateWhereClause: Invalid values for cust_id_high and cust_id_low.');
  else
   CallInfo('CreateWhereClause: No errors found.');
  end if;

 end if;
 -- CallInfo('CreateWhereClause: Step 3 ');

-- Build WHERE condition for large_accounts
vl0 := GetParameter('0_large_account');
vl1 := GetParameter('1_large_account');
vl2 := GetParameter('2_large_account');
vl3 := GetParameter('3_large_account');
vl4 := GetParameter('4_large_account');
vl5 := GetParameter('5_large_account');
vl6 := GetParameter('6_large_account');
vl7 := GetParameter('7_large_account');
vl8 := GetParameter('8_large_account');
vl9 := GetParameter('9_large_account');

-- CallInfo('CreateWhereClause: Step 4 ');
IF vl0 IS NOT NULL AND length(RTRIM(vl0)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl0 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl0 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl1 IS NOT NULL AND length(RTRIM(vl1)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl1 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl1 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl2 IS NOT NULL AND length(RTRIM(vl2)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl2 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl2 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl3 IS NOT NULL AND length(RTRIM(vl3)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl3 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl3 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl4 IS NOT NULL AND length(RTRIM(vl4)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl4 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl4 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl5 IS NOT NULL AND length(RTRIM(vl5)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl5 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl5 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl6 IS NOT NULL AND length(RTRIM(vl6)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl6 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl6 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl7 IS NOT NULL AND length(RTRIM(vl7)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl7 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl7 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl8 IS NOT NULL AND length(RTRIM(vl8)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl8 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl8 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl9 IS NOT NULL AND length(RTRIM(vl9)) > 0 THEN
  IF vl_counter = 0 THEN
     vl := 'AND (CUSTCODE LIKE ' || '''' || vl9 || '''';
  ELSE
     vl := vl || ' OR CUSTCODE LIKE ' || '''' || vl9 || '''';
  END IF;
  vl_counter := vl_counter + 1;
END IF;
IF vl IS NOT NULL THEN
 vl := vl || ')';
-- Defect 91442 Uwe Meier: If no large account is specified in dbextract.cfg or default.cfg,
-- no large account should be selected. Therefore add ELSE branch.
ELSE
 vl := 'AND 1=2';
END IF;

-- CallInfo('CreateWhereClause: Step 5 ');

-- Build WHERE condition for Service Providers
vs :=  GetParameter('vservice_providers');

 BEGIN
   dummy_number := TO_NUMBER(LTRIM(RTRIM(vs)));
 EXCEPTION WHEN OTHERS THEN
   CallError('CreateWhereClause: Invalid values for vservice_providers.');
 END;

IF vs IS NOT NULL AND length(vs) > 0 THEN
   vs := ' AND ' || vs ||
         ' > (SELECT count(*) FROM contract_all';
   if vialink = 'Y' then
      vs := vs ||  '@' || vdbBSCS;
   end if;
   vs := vs ||  ' CA WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID )';
END IF;

-- Build WHERE condition for Roaming Partners
vr :=  GetParameter('vroaming_partner');

 BEGIN
   dummy_number := TO_NUMBER(LTRIM(RTRIM(vr)));
 EXCEPTION WHEN OTHERS THEN
   CallError('CreateWhereClause: Invalid values for vroaming_partner.');
 END;

IF vr IS NOT NULL AND length(vr) > 0 THEN
   vr := ' AND ' || vr ||
         ' > (SELECT count(*) FROM contract_all';
   if vialink = 'Y' then
      vr := vr ||  '@' || vdbBSCS;
   end if;
   vr := vr ||  ' CA WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID )';
END IF;

vr_a :=  GetParameter('vroaming_partner');
vr_c :=  GetParameter('vroaming_partner');

IF vr_a IS NOT NULL AND length(vr_a) > 0 THEN
   vr_a := ' C.CUSTOMER_ID IN ' ||
         ' (SELECT CA.CUSTOMER_ID FROM contract_all';
   if vialink = 'Y' then
      vr_a := vr_a ||  '@' || vdbBSCS;
   end if;
   vr_a := vr_a || ' CA WHERE CA.CUSTOMER_ID IN (';
   -- SUB Select
   vr_b := ') ';
   vr_b := vr_b || ' GROUP BY CA.CUSTOMER_ID ';
   vr_b := vr_b || ' HAVING COUNT(*) < ' || vr_c || ')';
ELSE
   vr_a := ' C.CUSTOMER_ID IN ' ||
         ' (SELECT CA.CUSTOMER_ID FROM contract_all';
   if vialink = 'Y' then
      vr_a := vr_a ||  '@' || vdbBSCS;
   end if;
   vr_a := vr_a || ' CA WHERE CA.CUSTOMER_ID IN (';
   -- SUB Select
   vr_b :=  ') ';
   vr_b := vr_b || ' GROUP BY CA.CUSTOMER_ID ) ';
END IF;



IF TO_NUMBER(LTRIM(RTRIM(vc_total))) > 0 THEN

 -- CallInfo('CreateWhereClause: Step 10 ');
 -- If High Bound is not set take the maximum CUSTOMER_ID
 IF (vc_high  IS NULL
     OR TO_NUMBER(LTRIM(RTRIM(vc_high ))) = 0)
    AND vm_customer_id IS NOT NULL
    AND (vc_total IS NOT NULL AND length(vc_total) > 0 )
    AND (vc_total > 0  )
 THEN
       vc_high  := to_char(vm_customer_id,'99999999999999999999999999999');
 END IF;

-- CallInfo('CreateWhereClause: Step 11 ');
 vc := 'WHERE customer_id_high is null AND CSLEVEL = 40 ';

 IF  (vc_low IS NOT NULL AND length(vc_low) > 0 )
 AND (vc_high IS NOT NULL AND length(vc_high) > 0 )
 AND (vc_total IS NOT NULL AND length(vc_total) > 0 )
 AND (vc_total > 0 ) THEN
 -- vc := vc || 'and floor(mod(customer_id,(' || vc_high || '-' || vc_low ||
 --     ')/' || vc_total || ')) = 0 ';
 vc := vc || 'and customer_id in ( ';
 vc := vc || 'select floor( (rownum -1) * (' || vc_high || '-' || vc_low || ')/' ||  vc_total || ') +' || vc_low || ' ';
-- vc := vc || ' FROM   CUSTOMER_ALL'; -- PN 330975
 vc := vc || ' AS result FROM CUSTOMER_ALL';
 if vialink = 'Y' then
    vc := vc || '@' || vdbBSCS;
 end if;
 vc := vc || ' where rownum <= ' || vc_total || ') ';
 END IF;

 IF  (vc_low IS NOT NULL AND length(vc_low) > 0 )
 AND (vc_high IS NOT NULL AND length(vc_high) > 0 )
 AND (vc_total IS NOT NULL AND length(vc_total) > 0 )
 AND (vc_total > 0 )
 THEN
 vc := vc || ' and customer_id between ' || vc_low || ' and ' || vc_high;
 END IF;

 IF  (vc_low IS NOT NULL AND length(vc_low) > 0 )
 AND (vc_high IS NULL )
 AND (vc_total IS NOT NULL AND length(vc_total) > 0 )
 AND (vc_total > 0 )
 THEN
 vc := vc || ' and customer_id > ' || vc_low;
 END IF;

 IF  (vc_high IS NOT NULL AND length(vc_high) > 0 )
 AND (vc_low  IS NULL )
 AND (vc_total IS NOT NULL AND length(vc_total) > 0 )
 AND (vc_total > 0 )
 THEN
 vc := vc || ' and customer_id between 0 and ' || vc_high;
 END IF;
ELSE
 vc := ' WHERE 1 = 2 ';
END IF;
-- vcs := 'WHERE customer_id_high is null AND (CSLEVEL = 40 OR CSLEVEL IS NULL) AND customer_id in (';
-- vcs := 'WHERE customer_id_high is null AND (CSLEVEL IS NULL OR CSLEVEL NOT IN (10,20,30)) AND customer_id in (';
 vcs := 'WHERE customer_id in (';
 IF  (vc_special IS NOT NULL AND length(vc_special) > 0 ) THEN
      vcs := vcs || vc_special;
 END IF;
 vcs := vcs || ')';

-- CallInfo('CreateWhereClause: Step 12 ');

--  IF  (vc_cstestbill IS NOT NULL AND length(vc_cstestbill) > 0 ) THEN
--       vc := vc || ' or cstestbillrun like ' || '''' || vc_cstestbill ||'''';
--  END IF;
--
--  IF  (vc_billcycle  IS NOT NULL AND length(vc_billcycle ) > 0 ) THEN
--       vc := vc || ' or billcycle     like ' || '''' || vc_billcycle  ||'''';
--  END IF;

-- Return Value for Large Accounts
 ShowStatement('vlarge_accounts:'|| vl ); -- PN 329178
 SetParameter ('vlarge_accounts',vl);
-- Return Value for Service Providers
 ShowStatement('vservice_providers:'|| vs ); -- PN 329178
 SetParameter ('vservice_providers',vs);
-- Return Value for Roaming Partner
 ShowStatement('vroaming_partner:'|| vr ); -- PN 329178
 SetParameter ('vroaming_partner',vr);
 ShowStatement('vroaming_partner_a:'|| vr_a ); -- PN 329178
 SetParameter ('vroaming_partner_a',vr_a);
 ShowStatement('vroaming_partner_b:'|| vr_b ); -- PN 329178
 SetParameter ('vroaming_partner_b',vr_b);

-- Return Value for Flat Subscribers
 ShowStatement('vwhere_customerid:'|| vc ); -- PN 329178
 SetParameter ('vwhere_customerid',vc);
-- Return Value for Flat Subscribers special
 ShowStatement('vwhere_customerid_spec:'|| vcs ); -- PN 329178
 SetParameter ('vwhere_customerid_spec',vcs);

 CallInfo('CreateWhereClause: END ');

EXCEPTION WHEN OTHERS THEN
  CallInfo('CreateWhereClause: Error:');
  CallError(SQLERRM);
END CreateWhereClause;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_1
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_1
AS
 GLOBAL_HINT            VARCHAR2(4000);
 CUSTOMER_ALL_HINT      VARCHAR2(4000);
 vdbBSCS                VARCHAR2(4000);
 vwhere_customerid      VARCHAR2(4000);
 vwhere_customerid_spec VARCHAR2(4000);
 vIsRelease             VARCHAR2(4000);
 vialink                VARCHAR2(1);
 str                    VARCHAR2(32000);
 start_time             VARCHAR2(30);
 stop_time              VARCHAR2(30);
 counter                INTEGER := 0;
 los_username           VARCHAR2(30);
BEGIN

 start_time               := GetTime;
 GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
 CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
 vdbBSCS                  := GetParameter('vdbBSCS');
 vialink                  := GetParameter('vialink');
 vwhere_customerid        := GetParameter('vwhere_customerid');
 vwhere_customerid_spec   := GetParameter('vwhere_customerid_spec');
 vIsRelease               := GetParameter('IsRelease');


 str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
 str := str || '(CUSTOMER_ID,';
 str := str || ' CUSTOMER_ID_HIGH,';
 str := str || ' CSLEVEL,';
 str := str || ' CSTYPE,';
 str := str || ' CSRESELLER ) ';
 str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
 str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE';

 if TO_NUMBER(vIsRelease) < 10 then
    str := str || ',CSRESELLER ';
 else
    str := str || ',NULL       ';
 end if;
 str := str || ' FROM CUSTOMER_ALL';

 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS;
 end if;

 str := str || ' ' || vwhere_customerid;
-- NEW
 str := str || ' MINUS ';
 str := str || ' SELECT ';
 str := str || ' CUSTOMER_ID,';
 str := str || ' CUSTOMER_ID_HIGH,';
 str := str || ' CSLEVEL,';
 str := str || ' CSTYPE,';
 str := str || ' CSRESELLER FROM DBX_CUSTOMER ';

 ShowStatement(str);
 -- vwhere_customerid = 47  with followin string: "WHERE customer_id_high is null AND CSLEVEL = 40"
 if length(trim(vwhere_customerid)) = 47 then
    callinfo('FillTableDBXCustomer_1: No Rows inserted ');
 else
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_1a'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_1: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_1a'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
 end if;

 select username
   into los_username
   from user_users;

 GatherDBXTableStat ( piosSchema    => los_username
                  ,piosTablename => 'DBX_CUSTOMER');

 str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
 str := str || '(CUSTOMER_ID,';
 str := str || ' CUSTOMER_ID_HIGH,';
 str := str || ' CSLEVEL,';
 str := str || ' CSTYPE,';
 str := str || ' CSRESELLER ) ';
 str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
 str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE';

 if TO_NUMBER(vIsRelease) < 10 then
    str := str || ',CSRESELLER ';
 else
    str := str || ',NULL       ';
 end if;
 str := str || ' FROM CUSTOMER_ALL';

 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS;
 end if;

 str := str || ' ' || vwhere_customerid_spec;
-- NEW
 str := str || ' MINUS ';
 str := str || ' SELECT ';
 str := str || ' CUSTOMER_ID,';
 str := str || ' CUSTOMER_ID_HIGH,';
 str := str || ' CSLEVEL,';
 str := str || ' CSTYPE,';
 str := str || ' CSRESELLER FROM DBX_CUSTOMER ';

-- if length(trim(vwhere_customerid_spec)) = 90 then
-- if length(trim(vwhere_customerid_spec)) = 103 then
 if length(trim(vwhere_customerid_spec)) = 23 then
    callinfo('FillTableDBXCustomer_1b: No Rows to insert');
 else
    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_1b'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_1: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_1b'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
 end if;
 if length(trim(vwhere_customerid_spec)) > 23 then
    str := 'SELECT count(*) FROM dbx_customer WHERE customer_id_high is not null OR CSLEVEL IN (10,20,30)';
    execute immediate str into counter;
    if counter > 0 then
       callinfo('FillTableDBXCustomer_1b: Performance Warning: ' || TO_CHAR(counter) || ' Large Accounts with unknown number of contracts selected!');
    end if;
 end if;

 GatherDBXTableStat ( piosSchema    => los_username
                  ,piosTablename => 'DBX_CUSTOMER');

 stop_time := GetTime;
 callinfo('FillTableDBXCustomer_1: Start:' || start_time );
 callinfo('FillTableDBXCustomer_1: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_1: Error:');
  if SQLCODE = -942  then
            CallInfo('The user used to connect to the BSCS database may not have BSCS_ROLE granted');
            CallInfo('Connect to BSCS database as user SYSADM and execute:');
            CallInfo('SQL> grant BSCS_ROLE to <username>;');
  end if;
  CallError(SQLERRM);
END FillTableDBXCustomer_1;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomerIXPre
   **
   ** Purpose:   Fill Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomerIXPre
AS
 GLOBAL_HINT            VARCHAR2(4000);
 CUSTOMER_ALL_HINT      VARCHAR2(4000);
 vdbBSCS                VARCHAR2(4000);
 vIsRelease             VARCHAR2(4000);
 MAX_CO_PER_DUMMY_CUST  VARCHAR2(4000);
 vialink                VARCHAR2(1);
 str                    VARCHAR2(32000);
 start_time             VARCHAR2(30);
 stop_time              VARCHAR2(30);
BEGIN

 start_time               := GetTime;
 GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
 CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
 vdbBSCS                  := GetParameter('vdbBSCS');
 vialink                  := GetParameter('vialink');
 vIsRelease               := GetParameter('IsRelease');
 MAX_CO_PER_DUMMY_CUST    := GetParameter('MAX_CO_PER_DUMMY_CUST');

 if TO_NUMBER(vIsRelease) >= 10 then
    str := 'update dbx_customer C';
    str := str || ' set C.CSRESELLER = ' || '''' || 'X' || '''' || ' ';
    str := str || ' where C.CUSTOMER_ID IN ( SELECT CUSTOMER_ID ';
    str := str ||                          ' FROM   CUSTOMER_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str ||                   ' R ';
    str := str ||                   ' ,PARTY_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str ||                   ' P ';
    str := str ||                   ' WHERE P.PARTY_ROLE_ID = R.PARTY_ROLE_ID';
    str := str ||                   ' AND   P.SHNAME = ' || '''' || 'ServiceProvider' || '''' || ')';

    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomerIXPre_1'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomerIXPre_1: Rows updated  ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomerIXPre_1'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;

    if MAX_CO_PER_DUMMY_CUST is not null and MAX_CO_PER_DUMMY_CUST != '0' then

       str := 'update dbx_customer C';
       str := str || ' set C.PARTY_ROLE_ID  = (SELECT DISTINCT RU.PARTY_ROLE_ID ';
       str := str ||                        ' FROM   CUSTOMER_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' RU ';
       str := str ||                   ' ,PARTY_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' PU ';
       str := str ||                   ' WHERE PU.PARTY_ROLE_ID = RU.PARTY_ROLE_ID';
       str := str ||                   ' AND   PU.SHNAME = ' || '''' || 'Pre-activeSubscriber' || '''' || ')';
       str := str || ' where C.PARTY_ROLE_ID IS NULL ';
       str := str || ' and   C.CUSTOMER_ID IN ( SELECT R.CUSTOMER_ID ';
       str := str ||                          ' FROM   CUSTOMER_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' R ';
       str := str ||                   ' ,PARTY_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' P ';
       str := str ||                   ' WHERE P.PARTY_ROLE_ID = R.PARTY_ROLE_ID';
       str := str ||                   ' AND   P.SHNAME = ' || '''' || 'Pre-activeSubscriber' || '''' || ')';

       ShowStatement(str);
       DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                    ,piosActionName => 'FillTableDBXCustomerIXPre_2'
                    ,piosMessage    => 'FILL'
                    ,piosPara1      => str);
       execute immediate str;
       callinfo('FillTableDBXCustomerIXPre_2: Rows updated  ' || to_CHAR(SQL%ROWCOUNT) );
       DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                    ,piosActionName => 'FillTableDBXCustomerIXPre_2'
                    ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
       commit;

       str := 'update dbx_customer C';
       str := str || ' set C.PARTY_ROLE_ID  = (SELECT DISTINCT RU.PARTY_ROLE_ID ';
       str := str ||                        ' FROM   CUSTOMER_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' RU ';
       str := str ||                   ' ,PARTY_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' PU ';
       str := str ||                   ' WHERE PU.PARTY_ROLE_ID = RU.PARTY_ROLE_ID';
       str := str ||                   ' AND   PU.SHNAME = ' || '''' || 'SubscriberTemplate' || '''' || ')';
       str := str || ' where C.PARTY_ROLE_ID IS NULL ';
       str := str || ' and   C.CUSTOMER_ID IN ( SELECT R.CUSTOMER_ID ';
       str := str ||                          ' FROM   CUSTOMER_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' R ';
       str := str ||                   ' ,PARTY_ROLE';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str ||                   ' P ';
       str := str ||                   ' WHERE P.PARTY_ROLE_ID = R.PARTY_ROLE_ID';
       str := str ||                   ' AND   P.SHNAME = ' || '''' || 'SubscriberTemplate' || '''' || ')';

       ShowStatement(str);
       DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                    ,piosActionName => 'FillTableDBXCustomerIXPre_3'
                    ,piosMessage    => 'FILL'
                    ,piosPara1      => str);
       execute immediate str;
       callinfo('FillTableDBXCustomerIXPre_3: Rows updated  ' || to_CHAR(SQL%ROWCOUNT) );
       DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                    ,piosActionName => 'FillTableDBXCustomerIXPre_3'
                    ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
       commit;


    end if;
 end if;

 stop_time := GetTime;
 callinfo('FillTableDBXCustomerIXPre: Start:' || start_time );
 callinfo('FillTableDBXCustomerIXPre: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomerIXPre: Error:');
  if SQLCODE = -942  then
            CallInfo('The user used to connect to the BSCS database may not have BSCS_ROLE granted');
            CallInfo('Connect to BSCS database as user SYSADM and execute:');
            CallInfo('SQL> grant BSCS_ROLE to <username>;');
  end if;
  CallError(SQLERRM);
END FillTableDBXCustomerIXPre;





 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_2
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_2
AS
 GLOBAL_HINT       VARCHAR2(4000);
 CUSTOMER_ALL_HINT VARCHAR2(4000);
 vdbBSCS           VARCHAR2(4000);
 vIsRelease        VARCHAR2(4000);
 vwhere_customerid VARCHAR2(4000);
 vialink           VARCHAR2(1);
 str               VARCHAR2(32000);
 vc_cstestbill     VARCHAR2(4000);
 vc_billcycle      VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 los_username      VARCHAR2(30);
BEGIN

  start_time          := GetTime;
  GLOBAL_HINT         := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT   := GetParameter('CUSTOMER_ALL_HINT');
  vdbBSCS             := GetParameter('vdbBSCS');
  vialink             := GetParameter('vialink');
  vwhere_customerid   := GetParameter('vwhere_customerid');
  vc_cstestbill       := GetParameter('cg');
  vc_billcycle        := GetParameter('billcyc');
  vIsRelease          := GetParameter('IsRelease');


  select username
    into los_username
    from user_users;

  IF vc_billcycle IS NOT NULL
  AND vc_cstestbill IS NULL THEN

    str := 'INSERT /*+ APPEND */ INTO dbx_customer ';
    str := str || '(CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER ) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
    str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ',CSRESELLER ';
    else
       str := str || ',NULL       ';
    end if;

    str := str || 'FROM CUSTOMER_ALL';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ' WHERE billcycle = ' || '''' || vc_billcycle || '''';
    else
       str := str || ' WHERE customer_id IN ( SELECT customer_id ';
       str := str || ' FROM BILLCYCLE_ACTUAL_VIEW';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str || ' WHERE BILLCYCLE = ' || '''' || vc_billcycle || '''';
       str := str || ' ) ';
    end if;

    -- NEW
    str := str || ' MINUS ';
    str := str || ' SELECT ';
    str := str || ' CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER FROM DBX_CUSTOMER ';
    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2a'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_2: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2a'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;

    GatherDBXTableStat ( piosSchema    => los_username
                     ,piosTablename => 'DBX_CUSTOMER');
  END IF;

  IF vc_billcycle IS NOT NULL
  AND vc_cstestbill IS NOT NULL THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
    str := str || '(CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER ) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
    str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ',CSRESELLER ';
    else
       str := str || ',NULL       ';
    end if;
    str := str || 'FROM CUSTOMER_ALL';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ' WHERE billcycle = ' || '''' || vc_billcycle || '''';
       str := str || ' AND CSTESTBILLRUN = ' || '''' || vc_cstestbill || '''';
    else
       str := str || ' WHERE CSTESTBILLRUN = ' || '''' || vc_cstestbill || '''';
       str := str || ' AND   customer_id IN ( SELECT customer_id ';
       str := str || ' FROM BILLCYCLE_ACTUAL_VIEW';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS;
       end if;
       str := str || ' WHERE BILLCYCLE = ' || '''' || vc_billcycle || '''';
       str := str || ' ) ';
    end if;

    -- NEW
    str := str || ' MINUS ';
    str := str || ' SELECT ';
    str := str || ' CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER FROM DBX_CUSTOMER ';

    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2b'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_2: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2b'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
    GatherDBXTableStat ( piosSchema    => los_username
                     ,piosTablename => 'DBX_CUSTOMER');
  END IF;

  IF vc_billcycle IS NULL
  AND vc_cstestbill IS NOT NULL THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
    str := str || '(CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER ) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
    str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ',CSRESELLER ';
    else
       str := str || ',NULL       ';
    end if;
    str := str || 'FROM CUSTOMER_ALL';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    str := str || ' WHERE CSTESTBILLRUN = ' || '''' || vc_cstestbill || '''';

    -- NEW
    str := str || ' MINUS ';
    str := str || ' SELECT ';
    str := str || ' CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER FROM DBX_CUSTOMER ';

    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2c'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_2: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_2c'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
    GatherDBXTableStat ( piosSchema    => los_username
                     ,piosTablename => 'DBX_CUSTOMER');
  END IF;

 stop_time := GetTime;
 callinfo('FillTableDBXCustomer_2: Start:' || start_time );
 callinfo('FillTableDBXCustomer_2: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_2: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_2;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Roam
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Roam
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 ROAMING_AGREEMENT_IXHINT  VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 vwhere_customerid         VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vr                        VARCHAR2(4000);
 vr_a                      VARCHAR2(4000);
 vr_b                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 los_username      VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  ROAMING_AGREEMENT_IXHINT := GetParameter('ROAMING_AGREEMENT_IXHINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vwhere_customerid        := GetParameter('vwhere_customerid');
  vr                       := GetParameter('vroaming_partner');
  vr_a                     := GetParameter('vroaming_partner_a');
  vr_b                     := GetParameter('vroaming_partner_b');
  vIsRelease               := GetParameter('IsRelease');

  select username
    into los_username
    from user_users;

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ C.CUSTOMER_ID,C.CUSTOMER_ID_HIGH,C.CSLEVEL,C.CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' C ';

  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ' WHERE ' || vr_a ;
     str := str || ' SELECT /*+ ' || ROAMING_AGREEMENT_IXHINT || ' */ CUSTOMER_ID ';
     str := str || ' FROM   ROAMING_AGREEMENT';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;

--   str := str || ' WHERE C.CUSTOMER_ID IN (' ;
--   str := str || ' SELECT /*+ ' || ROAMING_AGREEMENT_IXHINT || ' */ CUSTOMER_ID ';
--   str := str || ' FROM   ROAMING_AGREEMENT';
--
--   if vialink = 'Y' then
--      str := str ||  '@' || vdbBSCS;
--   end if;
  else
--   str := str || ' where C.CUSTOMER_ID IN ( SELECT CUSTOMER_ID ';
     str := str || ' WHERE ' || vr_a ;
     str := str || ' SELECT CUSTOMER_ID ';
     str := str ||                          ' FROM   CUSTOMER_ROLE';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str ||                   ' R ';
     str := str ||                   ' ,PARTY_ROLE';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str ||                   ' P ';
     str := str ||                   ' WHERE P.PARTY_ROLE_ID = R.PARTY_ROLE_ID';
     str := str ||                   ' AND   P.SHNAME IN (' || '''' || 'RoamingOutcollectTAP' || '''';
     str := str ||                   ' ,' || '''' || 'RoamingIncollectTAP' || '''' ;
     str := str ||                   ' ,' || '''' || 'ExternalCarrierInbound' || '''' ;
     str := str ||                   ' ,' || '''' || 'ExternalCarrierOutbound' || '''' ;
     str := str ||                   ' ,' || '''' || 'RoamingIncollectCIBER' || '''' ;
     str := str ||                   ' ,' || '''' || 'RoamingOutcollectCIBER' || '''' ;
     str := str ||                   ' ,' || '''' || 'SubscriberTemplate' || '''' || ') ';
  end if;
  str := str || vr_b ;
--  str := str || ' ) ' || vr;
  str := str || ' AND ';
  str := str || ' C.CUSTOMER_ID NOT IN ';
  str := str || '( SELECT CUSTOMER_ID FROM dbx_customer';
  str := str ||  ')';

 if TO_NUMBER(vIsRelease) > 5 then
  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam1a'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Roam: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam1a'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');
 end if;

/* ADD Customer from Roaming Partner        */
  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ C.CUSTOMER_ID,C.CUSTOMER_ID_HIGH,C.CSLEVEL,C.CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C ';
  str := str || ' WHERE CUSTOMER_ID IN (';
  str := str || ' SELECT CUSTOMER_ID ';
  str := str || ' FROM   MPULKEXM';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' ) ' || vr;
  str := str || ' AND ';
  str := str || ' C.CUSTOMER_ID NOT IN ';
  str := str || '( SELECT CUSTOMER_ID FROM dbx_customer)';

  if TO_NUMBER(vIsRelease) < 10 then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_Roam1b'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCustomer_Roam: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_Roam1b'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
     GatherDBXTableStat ( piosSchema    => los_username
                      ,piosTablename => 'DBX_CUSTOMER');
  end if;

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_Roam: Start:' || start_time );
  callinfo('FillTableDBXCustomer_Roam: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_Roam: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_Roam;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Roam6
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Roam6
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 ROAMING_AGREEMENT_IXHINT  VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 vwhere_customerid         VARCHAR2(4000);
 vr                        VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  ROAMING_AGREEMENT_IXHINT := GetParameter('ROAMING_AGREEMENT_IXHINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vwhere_customerid        := GetParameter('vwhere_customerid');
  vr                       := GetParameter('vroaming_partner');

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ C.CUSTOMER_ID,C.CUSTOMER_ID_HIGH,C.CSLEVEL,C.CSTYPE,C.CSRESELLER ';
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C ' ;
  str := str || ' WHERE C.CUSTOMER_ID IN (' ;
-- Different for 6.00
--str := str || ' SELECT /*+ ' || ROAMING_AGREEMENT_IXHINT || ' */ CUSTOMER_ID ';
--str := str || ' FROM   ROAMING_AGREEMENT';
  str := str || ' SELECT CUSTOMER_ID ';
  str := str || ' FROM   MPURATAB';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' ) ' || vr;
  str := str || ' AND ';
  str := str || ' C.CUSTOMER_ID NOT IN ';
  str := str || '( SELECT CUSTOMER_ID FROM dbx_customer';
  str := str ||  ')';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam6-1a'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Roam: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam6-1a'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

/* ADD Customer from Roaming Partner        */
  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ C.CUSTOMER_ID,C.CUSTOMER_ID_HIGH,C.CSLEVEL,C.CSTYPE,C.CSRESELLER ';
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C ';
  str := str || ' WHERE CUSTOMER_ID IN (';
  str := str || ' SELECT CUSTOMER_ID ';
  str := str || ' FROM   MPULKEXM';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' ) ' || vr;
  str := str || ' AND ';
  str := str || 'C.CUSTOMER_ID NOT IN ';
  str := str || '( SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam6-1b'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Roam6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Roam6-1b'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_Roam6: Start:' || start_time );
  callinfo('FillTableDBXCustomer_Roam6: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_Roam6: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_Roam6;





 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Large
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Large
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 ROAMING_AGREEMENT_IXHINT  VARCHAR2(4000);
 vdbBSCS                      VARCHAR2(4000);
 vlarge_accounts           VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 los_username      VARCHAR2(30);
BEGIN

  start_time          := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  ROAMING_AGREEMENT_IXHINT := GetParameter('ROAMING_AGREEMENT_IXHINT');
  vdbBSCS                     := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vlarge_accounts          := GetParameter('vlarge_accounts');
  vIsRelease          := GetParameter('IsRelease');

  select username
    into los_username
    from user_users;

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CSLEVEL = ' || '''' || '10' || '''' ;

  str := str || ' AND ';
  str := str || 'EXISTS (SELECT 1 FROM customer_all';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CA ';
  str := str || ' WHERE  C.CUSTOMER_ID = CA.CUSTOMER_ID_HIGH) ';
  str := str || vlarge_accounts ;

  -- NEW
  str := str || ' MINUS ';
  str := str || ' SELECT ';
  str := str || ' CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER FROM DBX_CUSTOMER ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Large'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Large: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Large'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_Large: Start:' || start_time );
  callinfo('FillTableDBXCustomer_Large: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_Large: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_Large;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_SP
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_SP
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 vservice_providers        VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN
  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vservice_providers       := GetParameter('vservice_providers');
  vIsRelease               := GetParameter('IsRelease');


  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',' || '''' || 'X' || '''';
  end if;

  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ' C WHERE C.CSRESELLER = ' || '''' || 'X' || '''' ;
     str := str || ' AND ( C.CSLEVEL IS NULL OR C.CSLEVEL = ' || '''' || '40' || '''' || ')' ;
     str := str || ' AND ( C.CUSTOMER_ID_HIGH IS NULL )' ;
     str := str || ' ' || vservice_providers ;
     str := str || ' AND C.CUSTOMER_ID NOT IN ( SELECT CUSTOMER_ID FROM dbx_customer)';
  else
    str := str || ' C where C.CUSTOMER_ID IN ( SELECT R.CUSTOMER_ID ';
    str := str ||                        ' FROM   CUSTOMER_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str ||                   ' R ';
    str := str ||                   ' ,PARTY_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str ||                   ' P ';
    str := str ||                   ' WHERE P.PARTY_ROLE_ID = R.PARTY_ROLE_ID';
    str := str ||                   ' AND   P.SHNAME IN (' || '''' || 'ServiceProvider' || '''';
    str := str ||                   ')) ';
    str := str || ' AND ( C.CSLEVEL IS NULL OR C.CSLEVEL = ' || '''' || '40' || '''' || ')' ;
    str := str || ' AND ( C.CUSTOMER_ID_HIGH IS NULL )' ;
    str := str || ' ' || vservice_providers ;
    str := str || ' AND C.CUSTOMER_ID NOT IN ( SELECT CUSTOMER_ID FROM dbx_customer)';

  end if;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_SP'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_SP: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_SP'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_SP: Start:' || start_time );
  callinfo('FillTableDBXCustomer_SP: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_SP:' );
  CallError(SQLERRM);
END FillTableDBXCustomer_SP;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_LargeL
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_LargeL
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL2_HINT        VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 vlarge_accounts           VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 los_username      VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL2_HINT       := GetParameter('CUSTOMER_ALL2_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vlarge_accounts          := GetParameter('vlarge_accounts');
  vIsRelease               := GetParameter('IsRelease');

  select username
    into los_username
    from user_users;

  /* First CUSTOMER_ID_HIGH */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID_HIGH IN ( ';

  str := str || 'SELECT CA.CUSTOMER_ID FROM dbx_customer CA ';
  str := str || 'WHERE  CA.CSLEVEL = ' || '''' || '10' || '''';
  str := str || ') AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_LargeL: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

/* Second CUSTOMER_ID_HIGH */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID_HIGH IN ( ';

  str := str || 'SELECT CA.CUSTOMER_ID FROM dbx_customer CA ';
  str := str || 'WHERE  CA.CSLEVEL = ' || '''' || '20' || '''';
  str := str || ') AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_LargeL: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

/* Third CUSTOMER_ID_HIGH */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID_HIGH IN ( ';

  str := str || 'SELECT CA.CUSTOMER_ID FROM dbx_customer CA ';
  str := str || 'WHERE  CA.CSLEVEL = ' || '''' || '30' || '''';
  str := str || ') AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_LargeL: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_LargeL3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_LargeL: Start:' || start_time );
  callinfo('FillTableDBXCustomer_LargeL: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_LargeL:' );
  CallError(SQLERRM);
END FillTableDBXCustomer_LargeL;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_Admin
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_Admin
AS
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL2_HINT        VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 vlarge_accounts           VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 los_username              VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL2_HINT       := GetParameter('CUSTOMER_ALL2_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vlarge_accounts          := GetParameter('vlarge_accounts');
  vIsRelease               := GetParameter('IsRelease');

  select username
    into los_username
    from user_users;

  /* CUG_ADMINISTRATION ADMIN_CUSTOMER_ID */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID IN ( ';

  str := str || 'SELECT distinct ADMIN_CUSTOMER_ID FROM cug_administration';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE ADMIN_CUSTOMER_ID IS NOT NULL ) AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Admin CUG_ADMIN: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');


  /* CUG_ADMINISTRATION PAYMENT_CUSTOMER_ID */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;
  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID IN ( ';

  str := str || 'SELECT distinct PAYMENT_CUSTOMER_ID FROM cug_administration';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE PAYMENT_CUSTOMER_ID IS NOT NULL ) AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Admin CUG_PAYMENT: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

  /* PREPAID_PROFILE CONTRACT_TEMPLATE_ID */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ';
  str := str || ' ,PARTY_ROLE_ID ';     -- PN 387900
  str := str || ' ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER,2 ';   -- PN 387900
  else
     str := str || ',NULL,2     ';     -- PN 387900
  end if;
  str := str || 'FROM CUSTOMER_ALL';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' C WHERE C.CUSTOMER_ID IN ( ';

  str := str || 'SELECT co.CUSTOMER_ID FROM contract_all';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' co, prepaid_profile';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' p ';
  str := str || ' WHERE co.co_id = p.contract_template_id AND p.contract_template_id IS NOT NULL ';
  str := str || ' GROUP BY co.CUSTOMER_ID ';
  str := str || ' ) AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  begin
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Admin PREPAID_PROFILE: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');
  exception when others then
     callinfo('FillTableDBXCustomer_Admin PREPAID_PROFILE do not exist in this Release!' );
  -- callinfo(SQLERRM);
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_Admin3'
                  ,piosMessage    => 'SUCCESS'||'(NULL)' );
     commit;
  end;

  /* PREPAID_PROVIDER  CONTRACT_ANCHOR */
-- PN 309746
--str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
--str := str || '(CUSTOMER_ID,';
--str := str || ' CUSTOMER_ID_HIGH,';
--str := str || ' CSLEVEL,';
--str := str || ' CSTYPE,';
--str := str || ' CSRESELLER ) ';
--str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
--str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
--if TO_NUMBER(vIsRelease) < 10 then
--   str := str || ',CSRESELLER ';
--else
--   str := str || ',NULL       ';
--end if;
--
--str := str || 'FROM CUSTOMER_ALL';
--
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS;
--end if;
--
--str := str || ' C WHERE C.CUSTOMER_ID IN ( ';
--
--str := str || 'SELECT co.CUSTOMER_ID FROM contract_all';
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS;
--end if;
--str := str || ' co, prepaid_provider';
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS;
--end if;
--str := str || ' p ';
--str := str || ' WHERE co.co_id = p.CONTRACT_ANCHOR AND P.CONTRACT_ANCHOR IS NOT NULL';
--str := str || ' ) AND C.CUSTOMER_ID NOT IN (';
--str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';
--str := str || ' minus ';
--str := str || 'SELECT CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE,CSRESELLER FROM dbx_customer';
--
--ShowStatement(str);
--begin
--DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
--             ,piosActionName => 'FillTableDBXCustomer_Admin4'
--             ,piosMessage    => 'FILL'
--             ,piosPara1      => str);
--execute immediate str;
--callinfo('FillTableDBXCustomer_Admin PREPAID_PROV_CO: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
--DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
--             ,piosActionName => 'FillTableDBXCustomer_Admin4'
--             ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
--commit;
--exception when others then
--   callinfo('FillTableDBXCustomer_Admin PREPAID_PROV_CO do not exist in this Release!' );
---- callinfo(SQLERRM);
--   DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
--                ,piosActionName => 'FillTableDBXCustomer_Admin4'
--                ,piosMessage    => 'SUCCESS'||'(NULL)' );
--   commit;
--end;
--
  /* PREPAID_PROVIDER CUSTOMER_ID */
-- PN 309746
--str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
--str := str || '(CUSTOMER_ID,';
--str := str || ' CUSTOMER_ID_HIGH,';
--str := str || ' CSLEVEL,';
--str := str || ' CSTYPE,';
--str := str || ' CSRESELLER ) ';
--str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
--str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
--if TO_NUMBER(vIsRelease) < 10 then
--   str := str || ',CSRESELLER ';
--else
--   str := str || ',NULL       ';
--end if;
--
--str := str || 'FROM CUSTOMER_ALL';
--
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS;
--end if;
--
--str := str || ' C WHERE C.CUSTOMER_ID IN ( ';
--
--str := str || 'SELECT distinct CUSTOMER_ID FROM prepaid_provider';
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS;
--end if;
--str := str || ' WHERE CUSTOMER_ID IS NOT NULL ) AND C.CUSTOMER_ID NOT IN (';
--str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';
--
--ShowStatement(str);
--begin
--DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
--             ,piosActionName => 'FillTableDBXCustomer_Admin5'
--             ,piosMessage    => 'FILL'
--             ,piosPara1      => str);
--execute immediate str;
--DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
--             ,piosActionName => 'FillTableDBXCustomer_Admin5'
--             ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
--callinfo('FillTableDBXCustomer_Admin PREPAID_PROV_CU: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
--commit;
--exception when others then
--   callinfo('FillTableDBXCustomer_Admin PREPAID_PROV_CU do not exist in this Release!' );
--   DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
--                ,piosActionName => 'FillTableDBXCustomer_Admin5'
--                ,piosMessage    => 'SUCCESS'||'(NULL)' );
--   commit;
--end;

  /* CRH_BA_DETAILS CUSTOMER_ID_TARGET */

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID,';
  str := str || ' CUSTOMER_ID_HIGH,';
  str := str || ' CSLEVEL,';
  str := str || ' CSTYPE,';
  str := str || ' CSRESELLER ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL2_HINT;
  str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
  if TO_NUMBER(vIsRelease) < 10 then
     str := str || ',CSRESELLER ';
  else
     str := str || ',NULL       ';
  end if;

  str := str || 'FROM CUSTOMER_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' C WHERE C.CUSTOMER_ID IN ( ';

  str := str || 'SELECT d.CUSTOMER_ID_TARGET FROM crh_request';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' r,  crh_ba_details';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' d ';
  str := str || ' where  r.REQUEST_ID = d.REQUEST_ID and d.CUSTOMER_ID_TARGET IS NOT NULL ';
  str := str || ' and  r.co_id in (SELECT co.co_id ';
  str := str || ' FROM   contract_all';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' co,dbx_customer';
  str := str || ' dcu';
  str := str || ' WHERE  co.customer_id = dcu.customer_id)';
  str := str || ' GROUP BY d.CUSTOMER_ID_TARGET ';
  str := str || ' ) AND C.CUSTOMER_ID NOT IN (';
  str := str || 'SELECT CUSTOMER_ID FROM dbx_customer)';

  ShowStatement(str);
  begin
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin6'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_Admin CRH_BA_DETAILS_CUID: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_Admin6'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  exception when others then
     callinfo('FillTableDBXCustomer_Admin CRH_BA_DETAILS_CUID do not exist in this Release!' );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_Admin6'
                  ,piosMessage    => 'SUCCESS'||'(NULL)' );
     commit;
  end;
  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CUSTOMER');

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_Admin: Start:' || start_time );
  callinfo('FillTableDBXCustomer_Admin: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_Admin:' );
  CallError(SQLERRM);
END FillTableDBXCustomer_Admin;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_BILL
   **
   ** Purpose:   Update Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_BILL
AS
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 vdbBSCS                      VARCHAR2(4000);
 vservice_providers        VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vIsCustBillCycle          VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 bcgroup_min       INTEGER := 0;
 bcgroup_max       INTEGER := 0;
BEGIN
  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  vservice_providers       := GetParameter('vservice_providers');
  vIsRelease               := GetParameter('IsRelease');
  vIsCustBillCycle         := GetParameter('IsCustBillCycle');

  if TO_NUMBER(vIsRelease) >= 10 then
     str := 'UPDATE dbx_customer DC ';
     str := str || ' SET BILLCYCLE = (';
     str := str || ' SELECT BILLCYCLE FROM BILLCYCLE_ACTUAL_VIEW';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' B ';
     str := str || ' WHERE B.customer_id = DC.customer_id )';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_BILL1'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCustomer_BILL: Rows updated ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'FillTableDBXCustomer_BILL1'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  else
   if TO_NUMBER(vIsRelease) > 5 then
     IF vIsCustBillCycle = 'Y' THEN
        str := 'UPDATE dbx_customer DC ';
        str := str || ' SET BILLCYCLE = (';
        str := str || ' SELECT BILLCYCLE FROM CUSTOMER_ALL';
        if vialink = 'Y' then
           str := str ||  '@' || vdbBSCS;
        end if;
        str := str || ' B ';
        str := str || ' WHERE B.customer_id = DC.customer_id )';

        ShowStatement(str);
        DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                     ,piosActionName => 'FillTableDBXCustomer_BILL2'
                     ,piosMessage    => 'FILL'
                     ,piosPara1      => str);
        execute immediate str;
        callinfo('FillTableDBXCustomer_BILL: Rows updated ' || to_CHAR(SQL%ROWCOUNT) );
        DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                     ,piosActionName => 'FillTableDBXCustomer_BILL2'
                     ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
        commit;
     END IF;
   END IF;
  end if;
 if TO_NUMBER(vIsRelease) > 5 then
   /*
    * Get min/max BILLCYCLE_GROUP_ID
    */
    str := 'SELECT nvl(min(BILLCYCLE_GROUP_ID),0),nvl(max(BILLCYCLE_GROUP_ID),0) ';
    str := str || ' FROM BILLCYCLE_GROUP_MEMBER';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE BILLCYCLE IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLCYCLE FROM dbx_customer ';
    str := str || ' WHERE BILLCYCLE IS NOT NULL ';
    str := str || ' GROUP BY BILLCYCLE) ';
    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL3'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str into bcgroup_min, bcgroup_max ;
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL3'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
    callinfo('FillTableDBXCustomer_BILL: bcgroup_min  ' || to_CHAR(bcgroup_min) );
    callinfo('FillTableDBXCustomer_BILL: bcgroup_max  ' || to_CHAR(bcgroup_max) );

    SetParameter('BILLCYCLE_GROUP_ID_MIN',TO_CHAR(bcgroup_min) );
    SetParameter('BILLCYCLE_GROUP_ID_MAX',TO_CHAR(bcgroup_max) );

    str := 'INSERT INTO DBX_BILLCYCLE_GROUP (BILLCYCLE_GROUP_ID) ' ;
    str := str || ' SELECT DISTINCT BILLCYCLE_GROUP_ID FROM BILLCYCLE_GROUP_MEMBER';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE BILLCYCLE IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLCYCLE FROM dbx_customer ';
    str := str || ' WHERE BILLCYCLE IS NOT NULL ';
    str := str || ' GROUP BY BILLCYCLE) ';
    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL4'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL4'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
 end if;

 if TO_NUMBER(vIsRelease) > 10 then
    str := 'INSERT INTO DBX_FAMILY_GROUP (FAMILY_GROUP_ID) ' ;
    str := str || ' SELECT DISTINCT fa.family_group_id ';
    str := str || '   FROM customer_family';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' cf, dbx_customer dc, family_all';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' fa ';
    str := str || ' WHERE cf.customer_id = dc.customer_id AND cf.family_id = fa.family_id';

    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL5'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_BILL5'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
 end if;

 stop_time := GetTime;
 callinfo('FillTableDBXCustomer_BILL: Start:' || start_time );
 callinfo('FillTableDBXCustomer_BILL: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_BILL:' );
  CallError(SQLERRM);
END FillTableDBXCustomer_BILL;





 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_SELECT
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_SELECT
AS
 GLOBAL_HINT       VARCHAR2(4000);
 GLOBAL_SUB_HINT   VARCHAR2(4000);
 CUSTOMER_ALL_HINT VARCHAR2(4000);
 vdbBSCS           VARCHAR2(4000);
 vIsRelease        VARCHAR2(4000);
 vialink           VARCHAR2(1);
 str               VARCHAR2(32000);
 str_select        VARCHAR2(32000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 select_is_active  INTEGER := 0;
 counter           INTEGER := 0;
 los_username      VARCHAR2(30);
BEGIN

 start_time          := GetTime;
 GLOBAL_HINT         := GetParameter('GLOBAL_HINT');
 GLOBAL_SUB_HINT     := GetParameter('GLOBAL_SUB_HINT');
 CUSTOMER_ALL_HINT   := GetParameter('CUSTOMER_ALL_HINT');
 vdbBSCS             := GetParameter('vdbBSCS');
 vialink             := GetParameter('vialink');
 vIsRelease          := GetParameter('IsRelease');

 select username
   into los_username
   from user_users;

 str_select := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str_select := str_select ||  '@' || vdbBSCS;
 end if;
 str_select := str_select || ' WHERE table_name = ' || '''' || 'DBX_SELECTION' || '''';
 str_select := str_select || ' AND   owner = ' || '''' || 'SYSADM' || '''';

 ShowStatement(str_select);
 execute immediate str_select into select_is_active;

 if select_is_active = 1 then
    callinfo('FillTableDBXCustomer_SELECT: Table DBX_SELECTION is available!');
    str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
    str := str || '(CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER ) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
    str := str || ' */ CUSTOMER_ID,CUSTOMER_ID_HIGH,CSLEVEL,CSTYPE ';
    if TO_NUMBER(vIsRelease) < 10 then
       str := str || ',CSRESELLER ';
    else
       str := str || ',NULL       ';
    end if;

    str := str || 'FROM CUSTOMER_ALL';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_SELECTION';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str ||  ' ) ';

   -- NEW
    str := str || ' MINUS ';
    str := str || ' SELECT ';
    str := str || ' CUSTOMER_ID,';
    str := str || ' CUSTOMER_ID_HIGH,';
    str := str || ' CSLEVEL,';
    str := str || ' CSTYPE,';
    str := str || ' CSRESELLER FROM DBX_CUSTOMER ';
    ShowStatement(str);
    DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_SELECT'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => str);
    execute immediate str;
    callinfo('FillTableDBXCustomer_SELECT: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
    DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                 ,piosActionName => 'FillTableDBXCustomer_SELECT'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
    commit;
    GatherDBXTableStat ( piosSchema    => los_username
                     ,piosTablename => 'DBX_CUSTOMER');
    str := 'SELECT count(*) FROM dbx_customer WHERE customer_id_high is not null OR CSLEVEL IN (10,20,30)';
    execute immediate str into counter;
    if counter > 0 then
       callinfo('FillTableDBXCustomer_SELECT: Performance Warning: ' || TO_CHAR(counter) || ' Large Accounts with unknown number of contracts selected!');
    end if;
    stop_time := GetTime;
    callinfo('FillTableDBXCustomer_SELECT: Start:' || start_time );
    callinfo('FillTableDBXCustomer_SELECT: Stop: ' || stop_time );
 else
    callinfo('FillTableDBXCustomer_SELECT: Table DBX_SELECTION is not available!');
 end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_SELECT: Error:');
  if SQLCODE = -942  then
            CallInfo('The user used to connect to the BSCS database may not have BSCS_ROLE granted');
            CallInfo('Connect to BSCS database as user SYSADM and execute:');
            CallInfo('SQL> grant BSCS_ROLE to <username>;');
  end if;
  CallError(SQLERRM);
END FillTableDBXCustomer_SELECT;






 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract_OHXACT
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXContract_OHXACT
AS
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ALL1_HINT        VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 TCU_ID_MIN                VARCHAR2(4000);
 TCU_ID_MAX                VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ALL1_HINT       := GetParameter('CONTRACT_ALL1_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  TCU_ID_MIN               := GetParameter('TCU_ID_MIN');
  TCU_ID_MAX               := GetParameter('TCU_ID_MAX');
  vIsRelease               := GetParameter('IsRelease');

  str := 'INSERT /*+  APPEND */ INTO dbx_contract_ohxact ';
  str := str || '(OHXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CONTRACT_ALL1_HINT ;
  str := str || ' */ distinct OHXACT ';
  str := str || 'FROM CONTRACT_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ C.CUSTOMER_ID ';
  str := str || '                       FROM  DBX_CUSTOMER';

  str := str || ' C ';
  str := str || '                       WHERE C.CUSTOMER_ID > 0 ) ' ;
--  str := str || '                       WHERE C.CSLEVEL = ' || '''' || '40' || '''' ;
--  str := str || '                       AND   CUSTOMER_ID_HIGH IS NULL ) ';
  str := str || ' AND    CUSTOMER_ID BETWEEN ' || TCU_ID_MIN || ' AND ' || TCU_ID_MAX ;
  str := str || ' AND    OHXACT IS NOT NULL  ';

  if TO_NUMBER(vIsRelease) < 10 then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CONTRACT_OHXACT'
                  ,piosActionName => 'FillTableDBXContract_OHXACT'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXContract_OHXACT: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CONTRACT_OHXACT'
                  ,piosActionName => 'FillTableDBXContract_OHXACT'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;

  stop_time := GetTime;
  callinfo('FillTableDBXContract_OHXACT: Start:' || start_time );
  callinfo('FillTableDBXContract_OHXACT: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXContract_OHXACT:' );
  CallError(SQLERRM);
END FillTableDBXContract_OHXACT;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract_OHXACT2
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXContract_OHXACT2
AS
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ALL1_HINT        VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 TCU_ID_MIN                VARCHAR2(4000);
 TCU_ID_MAX                VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ALL1_HINT       := GetParameter('CONTRACT_ALL1_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  TCU_ID_MIN               := GetParameter('TCU_ID_MIN');
  TCU_ID_MAX               := GetParameter('TCU_ID_MAX');
  vIsRelease               := GetParameter('IsRelease');

  str := 'INSERT /*+  APPEND */ INTO dbx_contract_ohxact ';
  str := str || '(OHXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CONTRACT_ALL1_HINT ;
  str := str || ' */ distinct OHXACT ';
  str := str || 'FROM CONTRACT_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ C.CUSTOMER_ID ';
  str := str || '                       FROM  DBX_orderhdr_customer';

  str := str || ' C WHERE C.CUSTOMER_ID > 0 )';
  str := str || ' AND    OHXACT IS NOT NULL  ';
  str := str || ' MINUS  ';
  str := str || ' SELECT OHXACT FROM dbx_contract_ohxact ';

  if TO_NUMBER(vIsRelease) < 10 then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CONTRACT_OHXACT'
                  ,piosActionName => 'FillTableDBXContract_OHXACT2'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXContract_OHXACT2: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CONTRACT_OHXACT'
                  ,piosActionName => 'FillTableDBXContract_OHXACT2'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;

  stop_time := GetTime;
  callinfo('FillTableDBXContract_OHXACT2: Start:' || start_time );
  callinfo('FillTableDBXContract_OHXACT2: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXContract_OHXACT2:' );
  CallError(SQLERRM);
END FillTableDBXContract_OHXACT2;






 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXorderhdr_cust
   **
   ** Purpose:   Fille Table DBX_orderhdr_customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXorderhdr_cust
AS
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 ORDERHDR_ALL_HINT         VARCHAR2(4000);
 vdbBSCS                   VARCHAR2(4000);
 TOHXACT_MIN               VARCHAR2(4000);
 TOHXACT_MAX               VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  ORDERHDR_ALL_HINT        := GetParameter('ORDERHDR_ALL_HINT');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  TOHXACT_MIN              := GetParameter('TOHXACT_MIN');
  TOHXACT_MAX              := GetParameter('TOHXACT_MAX');

  str := 'INSERT /*+  APPEND */ INTO DBX_orderhdr_customer ';
  str := str || '(CUSTOMER_ID) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || ORDERHDR_ALL_HINT ;
  str := str || ' */ distinct customer_id ';
  str := str || 'FROM orderhdr_all';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE OHXACT IN ( SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_CONTRACT_OHXACT';

  str := str || ' ) AND OHXACT BETWEEN ' || TOHXACT_MIN || ' AND ' || TOHXACT_MAX ;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_ORDERHDR_CUSTOMER'
               ,piosActionName => 'FillTableDBXorderhdr_cust'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXorderhdr_cust: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_ORDERHDR_CUSTOMER'
               ,piosActionName => 'FillTableDBXorderhdr_cust'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  stop_time := GetTime;
  callinfo('FillTableDBXorderhdr_cust: Start:' || start_time );
  callinfo('FillTableDBXorderhdr_cust: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXorderhdr_cust: Error:');
  CallError(SQLERRM);
END FillTableDBXorderhdr_cust;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_oh
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_oh
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN


  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');

  str := 'INSERT /*+  APPEND */ INTO dbx_customer ';
  str := str || '(CUSTOMER_ID) ';
  str := str || 'SELECT  customer_id FROM DBX_orderhdr_customer ';
  str := str || ' MINUS ';
  str := str || 'SELECT  customer_id FROM DBX_customer ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_oh'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_oh: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
               ,piosActionName => 'FillTableDBXCustomer_oh'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_oh: Start:' || start_time );
  callinfo('FillTableDBXCustomer_oh: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_oh: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_oh;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCustomer_dealer
   **
   ** Purpose:   Fille Table DBX_Customer
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCustomer_dealer
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 CUSTOMER_ALL_HINT         VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 IsRelease                 VARCHAR2(30);
 vtable_name               VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CUSTOMER_ALL_HINT        := GetParameter('CUSTOMER_ALL_HINT');
  IsRelease                := GetParameter('IsRelease');


  /* First CUSTOMER_ID_HIGH */

  IF TO_NUMBER(IsRelease) >= 10 THEN
    vtable_name := 'DBX_DEALER';
    str := 'INSERT /*+ APPEND */ INTO dbx_dealer ';
    str := str || '(CUSTOMER_ID) ';
    str := str || 'SELECT /*+ RULE ' ;
    str := str || ' */ CA.CUSTOMER_ID ';
    str := str || 'FROM CUSTOMER_ALL';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' CA ,CUSTOMER_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' CR ,PARTY_ROLE';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' PR ';
    str := str || ' WHERE PR.SHNAME IN (';
    str := str || ' ' || '''' || 'ServiceProvider' || '''';
    str := str || ',' || '''' || 'InterconnectInbound' || '''';
    str := str || ',' || '''' || 'InterconnectOutbound' || '''';
    str := str || ',' || '''' || 'NetworkProvider' || '''';
    str := str || ',' || '''' || 'Dealer' || '''';
    str := str || ',' || '''' || 'ContentProvider' || '''' || ') ';
    str := str || ' AND   PR.PARTY_ROLE_ID = CR.PARTY_ROLE_ID ';
    str := str || ' AND   CR.CUSTOMER_ID   = CA.CUSTOMER_ID   ';
    str := str || ' MINUS (';
    str := str || ' SELECT CUSTOMER_ID FROM dbx_dealer ';
    str := str || ' UNION                              ';
    str := str || ' SELECT CUSTOMER_ID FROM dbx_customer) ';
  ELSE
    vtable_name := 'DBX_CUSTOMER';
    str := 'INSERT /*+ APPEND */ INTO dbx_customer ';
    str := str || '(CUSTOMER_ID)';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CUSTOMER_ALL_HINT;
    str := str || ' */ CUSTOMER_ID ';
    str := str || 'FROM CUSTOMER_ALL';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE CUSTOMER_ID < 0';
    str := str || ' MINUS ';
    str := str || ' SELECT CUSTOMER_ID FROM dbx_customer ';
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => vtable_name
               ,piosActionName => 'FillTableDBXCustomer_dealer'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXCustomer_dealer: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => vtable_name
               ,piosActionName => 'FillTableDBXCustomer_dealer'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXCustomer_dealer: Start:' || start_time );
  callinfo('FillTableDBXCustomer_dealer: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCustomer_dealer: Error:');
  CallError(SQLERRM);
END FillTableDBXCustomer_dealer;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXContract
   **
   ** Purpose:   Fille Table DBX_Contract
   **
   **=====================================================
*/


PROCEDURE FillTableDBXContract
AS
 TYPE DynCurTyp IS REF CURSOR;
 cur                       DynCurTyp;
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ALL1_HINT        VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 MAX_CO_PER_DUMMY_CUST     VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 v_customer_id             INTEGER;
 i                         INTEGER :=0;
 vIsRelease                VARCHAR2(4000);
 v_select_statement        VARCHAR2(4000);
 row_counter               INTEGER :=0;
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ALL1_HINT       := GetParameter('CONTRACT_ALL1_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  MAX_CO_PER_DUMMY_CUST    := GetParameter('MAX_CO_PER_DUMMY_CUST');
  vIsRelease          := GetParameter('IsRelease');

  /* First CUSTOMER_ID_HIGH */

  str := 'INSERT /*+ APPEND */ INTO dbx_contract ';
  str := str || '(CO_ID,CUSTOMER_ID) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CONTRACT_ALL1_HINT;
  str := str || ' */ CO_ID,CUSTOMER_ID ';
  str := str || 'FROM CONTRACT_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
  -- PN 387900
--if TO_NUMBER(vIsRelease) >= 10 and MAX_CO_PER_DUMMY_CUST is not null and MAX_CO_PER_DUMMY_CUST != '0' then
  if TO_NUMBER(vIsRelease) >=  7 and MAX_CO_PER_DUMMY_CUST is not null and MAX_CO_PER_DUMMY_CUST != '0' then
     str := str || ' WHERE PARTY_ROLE_ID IS NULL OR PARTY_ROLE_ID NOT IN (2,3) '; -- 2=Subscriber Template 3=Pre-activeSubscriber
  end if;
  str := str || ' ) ';
  str := str || ' AND   CUSTOMER_ID BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_CONTRACT'
               ,piosActionName => 'FillTableDBXContract'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXContract: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_CONTRACT'
               ,piosActionName => 'FillTableDBXContract'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  -- PN 387900
--if TO_NUMBER(vIsRelease) >= 10 and MAX_CO_PER_DUMMY_CUST is not null and MAX_CO_PER_DUMMY_CUST != '0' then
  if TO_NUMBER(vIsRelease) >=  7 and MAX_CO_PER_DUMMY_CUST is not null and MAX_CO_PER_DUMMY_CUST != '0' then

    v_select_statement := 'SELECT CUSTOMER_ID FROM DBX_CUSTOMER WHERE PARTY_ROLE_ID IN (2,3) ORDER BY CUSTOMER_ID ' ;
    DBXLogStart ( piosTableName  => 'DBX_CONTRACT'
                 ,piosActionName => 'FillTableDBXContractPre'
                 ,piosMessage    => 'QUERY'
                 ,piosPara1      => v_select_statement);

    ShowStatement(v_select_statement);
    OPEN cur FOR v_select_statement ;
    LOOP
      FETCH cur INTO v_customer_id ;
      EXIT WHEN cur%NOTFOUND;

      str := 'INSERT /*+ APPEND */ INTO dbx_contract ';
      str := str || '(CO_ID,CUSTOMER_ID) ';
      str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CONTRACT_ALL1_HINT;
      str := str || ' */ CO_ID,CUSTOMER_ID ';
      str := str || 'FROM CONTRACT_ALL';

      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;

      str := str || ' WHERE CUSTOMER_ID = ' || TO_CHAR(v_customer_id);
      str := str || ' AND   ROWNUM <= ' || MAX_CO_PER_DUMMY_CUST ;

      ShowStatement(str);
      DBXLogStart ( piosTableName  => 'DBX_CONTRACT'
               ,piosActionName => 'FillTableDBXContractPre1_' || TO_CHAR(v_customer_id)
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
      execute immediate str;
      row_counter := SQL%ROWCOUNT;
      DBXLogStop  ( piosTableName  => 'DBX_CONTRACT'
                   ,piosActionName => 'FillTableDBXContractPre1_' || TO_CHAR(v_customer_id)
                   ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(row_counter) ||')' );
      callinfo('FillTableDBXContractPre1_' || TO_CHAR(v_customer_id) || ': Rows inserted ' || to_CHAR(row_counter) );
      i := i + row_counter;
      commit;

     if TO_NUMBER(vIsRelease) < 12 then
      str := 'INSERT /*+ APPEND */ INTO dbx_contract ';
      str := str || '(CO_ID,CUSTOMER_ID) ';
      str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CONTRACT_ALL1_HINT;
      str := str || ' */ A.CO_ID,A.CUSTOMER_ID ';
      str := str || 'FROM CONTRACT_ALL';

      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;

      str := str || ' A ';
      str := str || ' WHERE A.CUSTOMER_ID = ' || TO_CHAR(v_customer_id);
      str := str || ' AND   A.CO_ID IN (SELECT contract_template_id FROM PREPAID_PROFILE';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ') AND   NOT EXISTS (SELECT 1 FROM dbx_contract WHERE CO_ID = A.CO_ID) ';

      ShowStatement(str);
      DBXLogStart ( piosTableName  => 'DBX_CONTRACT'
               ,piosActionName => 'FillTableDBXContractPre2_' || TO_CHAR(v_customer_id)
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
      execute immediate str;
      row_counter := SQL%ROWCOUNT;
      DBXLogStop  ( piosTableName  => 'DBX_CONTRACT'
                   ,piosActionName => 'FillTableDBXContractPre2_' || TO_CHAR(v_customer_id)
                   ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(row_counter) ||')' );
      callinfo('FillTableDBXContractPre2_' || TO_CHAR(v_customer_id) || ': Rows inserted ' || to_CHAR(row_counter) );

      i := i + row_counter;
      commit;
     end if;

    END LOOP;
    CLOSE cur;
    DBXLogStop  ( piosTableName  => 'DBX_CONTRACT'
                 ,piosActionName => 'FillTableDBXContractPre'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(i) ||')' );
    callinfo('FillTableDBXContractPre: Rows inserted in total: ' || to_CHAR(i) );

  end if;


  stop_time := GetTime;
  callinfo('FillTableDBXContract: Start:' || start_time );
  callinfo('FillTableDBXContract: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXContract: Error:');
  CallError(SQLERRM);
END FillTableDBXContract;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXTickler
   **
   ** Purpose:   Fille Table DBX_Tickler
   **
   **=====================================================
*/


PROCEDURE FillTableDBXTickler
AS
 vdbBSCS                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 TICKLER_RECORDS_HINT      VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 tckrccredate              VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  TICKLER_RECORDS_HINT     := GetParameter('TICKLER_RECORDS_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  tckrccredate             := GetParameter('TCKRCCREDATE');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CUSTOMER_ID_RANGE');

  str := 'INSERT /*+  APPEND */ INTO dbx_tickler ';
  str := str || '(CUSTOMER_ID,TICKLER_NUMBER,CREATED_DATE ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || TICKLER_RECORDS_HINT;
  str := str || ' */ CUSTOMER_ID,TICKLER_NUMBER,CREATED_DATE ';
  str := str || 'FROM TICKLER_RECORDS';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE (CUSTOMER_ID,CO_ID) IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID,CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
     str := str || ' AND   CUSTOMER_ID BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX;
  END IF;
  str := str || ' AND   CREATED_DATE IS NOT NULL ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_TICKLER'
               ,piosActionName => 'FillTableDBXTickler1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXTickler: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_TICKLER'
               ,piosActionName => 'FillTableDBXTickler1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  IF tckrccredate IS NOT NULL AND tckrccredate  > 0 THEN

     str := 'DELETE FROM dbx_tickler ';
     str := str || 'WHERE  CREATED_DATE < (SELECT max(CREATED_DATE) - ' || tckrccredate ;
     str := str || ' FROM DBX_TICKLER)';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_TICKLER'
                  ,piosActionName => 'FillTableDBXTickler2'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXTickler: Rows deleted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_TICKLER'
                  ,piosActionName => 'FillTableDBXTickler2'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;

  END IF;

 stop_time := GetTime;
 callinfo('FillTableDBXTickler: Start:' || start_time );
 callinfo('FillTableDBXTickler: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXTickler: Error:');
  CallError(SQLERRM);
END FillTableDBXTickler;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPayment
   **
   ** Purpose:   Fille Table DBX_Payment
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPayment
AS
 vdbBSCS                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');
  -- Release IX 3
  IF TO_NUMBER(IsRelease) >= 11 THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_payment ';
    str := str || '(PAYMENT_ID) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(payment IX_PAYMENTALL_CUSTOMERID) ';
    str := str || ' */ PAYMENT_ID ';
    str := str || 'FROM PAYMENT';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
    str := str || ' ) ';


     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PAYMENT'
                  ,piosActionName => 'FillTableDBXPayment'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXPayment: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PAYMENT'
                  ,piosActionName => 'FillTableDBXPayment'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;


   stop_time := GetTime;
   callinfo('FillTableDBXPayment: Start:' || start_time );
   callinfo('FillTableDBXPayment: Stop: ' || stop_time );
  end if;

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPayment: Error:');
  CallError(SQLERRM);
END FillTableDBXPayment;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPaymentProp
   **
   ** Purpose:   Fille Table DBX_Payment_Prop
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPaymentProp
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 OHXACT_MIN                VARCHAR2(4000);
 OHXACT_MAX                VARCHAR2(4000);
 USE_OHXACT_RANGE          VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  USE_OHXACT_RANGE         := GetParameter('USE_OHXACT_RANGE');

  -- Release IX 3
  IF TO_NUMBER(IsRelease) >= 11 THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_payment_prop ';
    str := str || '(PAY_PROPOSAL_ID) ';
    str := str || 'SELECT DISTINCT /*+ ' || GLOBAL_HINT || ' index(payment IX_PAYMENTALL_CUSTOMERID) ';
    str := str || ' */ PAYMENT_PROPOSAL_ID ';
    str := str || 'FROM ORDERHDR_ALL';

    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;

    str := str || ' WHERE OHXACT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR';
    IF USE_OHXACT_RANGE = 'Y' THEN
       str := str || ' WHERE OHXACT ';
       str := str || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
    END IF;
    str := str || ' ) AND PAYMENT_PROPOSAL_ID IS NOT NULL';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PAYMENT_PROP'
                  ,piosActionName => 'FillTableDBXPaymentProp'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXPaymentProp: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PAYMENT_PROP'
                  ,piosActionName => 'FillTableDBXPaymentProp'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;


   stop_time := GetTime;
   callinfo('FillTableDBXPaymentProp: Start:' || start_time );
   callinfo('FillTableDBXPaymentProp: Stop: ' || stop_time );
  end if;

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPaymentProp: Error:');
  CallError(SQLERRM);
END FillTableDBXPaymentProp;





 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPortReq
   **
   ** Purpose:   Fille Table DBX_PortReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPortReq
AS
 vdbBSCS           VARCHAR2(4000);
 str               VARCHAR2(32000);
 GLOBAL_HINT       VARCHAR2(4000);
 GLOBAL_SUB_HINT   VARCHAR2(4000);
 IsRelease         VARCHAR2(4000);
 vialink           VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
 counter           INTEGER := 0;
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');

  str := 'SELECT count(*) ';
  str := str || '  FROM dba_indexes';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE OWNER = ''SYSADM'' ';
  str := str || '   AND INDEX_NAME IN (''I_PORT_REQ_DN'',''I_PORT_REQ_HIST_DN'',''PK_DN_PORTED_IN_OUT'') ';
  ShowStatement(str);
  execute immediate str into counter;
  IF counter = 3 THEN
     SetParameter('PORTING_INDEX_AVAILABLE','Y');
  ELSE
     SetParameter('PORTING_INDEX_AVAILABLE','N');
  END IF;

  -- Release IX 1
  IF TO_NUMBER(IsRelease) >= 9 OR ( TO_NUMBER(IsRelease) >= 7 AND counter = 3 ) THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_PortReq ';
    str := str || '(REQUEST_ID) ';
    str := str || 'SELECT DISTINCT REQUEST_ID FROM ( ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(PORTING_REQUEST I_PORT_REQ_DN) ';
    str := str || ' */ REQUEST_ID ';
    str := str || 'FROM PORTING_REQUEST';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE DN_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM DBX_DN';
    str := str || ' ) ';
    str := str || ' UNION ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(PORTING_REQUEST_HISTORY I_PORT_REQ_HIST_DN) ';
    str := str || ' */ REQUEST_ID ';
    str := str || 'FROM PORTING_REQUEST_HISTORY';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE DN_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM DBX_DN';
    str := str || ' ) ';
    str := str || ' UNION ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(DN_PORTED_IN_OUT PK_DN_PORTED_IN_OUT) ';
    str := str || ' */ REQUEST_ID ';
    str := str || 'FROM DN_PORTED_IN_OUT';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE DN_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM DBX_DN)';
    str := str || ' AND REQUEST_ID IS NOT NULL ';
    str := str || ') '; -- end of from clause

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PORTREQ'
                  ,piosActionName => 'FillTableDBXPortReq'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXPortReq: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PORTREQ'
                  ,piosActionName => 'FillTableDBXPortReq'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;


   stop_time := GetTime;
   callinfo('FillTableDBXPortReq: Start:' || start_time );
   callinfo('FillTableDBXPortReq: Stop: ' || stop_time );
  end if;

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPortReq: Error:');
  CallError(SQLERRM);
END FillTableDBXPortReq;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCrpReq
   **
   ** Purpose:   Fille Table DBX_CrpReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCrpReq
AS
 vdbBSCS                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');
  -- Release IX 3
  IF TO_NUMBER(IsRelease) >= 10 THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_CrpReq ';
    str := str || '(REQUEST_ID) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(CRP_CONTRACT I_DBX_CRP_CONTRACT_COID) ';
    str := str || ' */ DISTINCT REQUEST_ID ';
    str := str || 'FROM CRP_CONTRACT';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE CO_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
    str := str || ' ) ';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CRPREQ'
                  ,piosActionName => 'FillTableDBXCrpReq'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCrpReq: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CRPREQ'
                  ,piosActionName => 'FillTableDBXCrpReq'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;


   stop_time := GetTime;
   callinfo('FillTableDBXCrpReq: Start:' || start_time );
   callinfo('FillTableDBXCrpReq: Stop: ' || stop_time );
  end if;

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXCrpReq: Error:');
  CallError(SQLERRM);
END FillTableDBXCrpReq;




 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCdsReq
   **
   ** Purpose:   Fille Table DBX_CdsReq
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCdsReq
AS
 vdbBSCS                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 start_time        VARCHAR2(30);
 stop_time         VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');
  -- Release IX 3
  IF TO_NUMBER(IsRelease) >= 10 THEN
    str := 'INSERT /*+  APPEND */ INTO dbx_CdsReq ';
    str := str || '(REQUEST_ID) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(CDS_CONTRACT I_DBX_CDS_CONTRACT_COID) ';
    str := str || ' */ DISTINCT REQUEST_ID ';
    str := str || 'FROM CDS_CONTRACT';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE CO_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
    str := str || ' ) ';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CRPREQ'
                  ,piosActionName => 'FillTableDBXCdsReq'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCdsReq: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CRPREQ'
                  ,piosActionName => 'FillTableDBXCdsReq'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;


   stop_time := GetTime;
   callinfo('FillTableDBXCdsReq: Start:' || start_time );
   callinfo('FillTableDBXCdsReq: Stop: ' || stop_time );
  end if;

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXCdsReq: Error:');
  CallError(SQLERRM);
END FillTableDBXCdsReq;





PROCEDURE FillTableDBXPrmMechSet
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 los_username              VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  IsRelease                := GetParameter('IsRelease');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');

    str := 'INSERT /*+  APPEND */ INTO DBX_PRM_MECH_SET ';
    str := str || '(MECH_SET_ID) ';
    str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' index(CDS_CONTRACT I_DBX_CDS_CONTRACT_COID) ';
    str := str || ' */ DISTINCT MECH_SET_ID ';
    str := str || 'FROM PROMO_QUAL_RESTRICT';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS;
    end if;
    str := str || ' WHERE CUST_ID_ASSIGN IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
    str := str || ' WHERE CUST_ID_ASSIGN BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX;
    str := str || ' ) ';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PRM_MECH_SET'
                  ,piosActionName => 'FillTableDBXPrmMechSet'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXPrmMechSet: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PRM_MECH_SET'
                  ,piosActionName => 'FillTableDBXPrmMechSet'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;

   stop_time := GetTime;
   callinfo('FillTableDBXPrmMechSet: Start:' || start_time );
   callinfo('FillTableDBXPrmMechSet: Stop: ' || stop_time );

   select username
     into los_username
     from user_users;

   GatherDBXTableStat ( piosSchema    => los_username
                       ,piosTablename => 'DBX_PRM_MECH_SET');

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPrmMechSet: Error:');
  CallError(SQLERRM);
END FillTableDBXPrmMechSet;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_PRM_MechSetID
   **
   ** Purpose:   Define Where condition for PROMO_MECH_SET and PROMO_MECH_SET_DATA
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_PRM_MechSetID
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  if piosHint IS NOT NULL and length(trim(piosHint)) > 0 then
     str_hint              := GetParameter(piosHint);
  else
     str_hint              := '';
  end if;
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ MECH_SET_ID FROM ' || vuserDBXHOME || '.DBX_PRM_MECH_SET';
  if viadbxlink = 'Y' then
     str_create := str_create || '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName ||' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ MECH_SET_ID FROM ' || vuserDBXHOME || '.DBX_PRM_MECH_SET)';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ MECH_SET_ID FROM DBX_PRM_MECH_SET@' || vdbDBXHOME  ;
  str_where_link := str_where_link || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_PRM_MechSetID: Error:');
  CallError(SQLERRM);
END SetDBX_Where_PRM_MechSetID;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Customer
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Customer
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
     -- Dealer Clause
    ,piosDealer      IN VARCHAR2  DEFAULT NULL
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CUSTOMER_ID_MIN           INTEGER;
 CUSTOMER_ID_MAX           INTEGER;
 v_low_dealer_id           INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
 IsRelease                 VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter(piosDBLink);
  vdbconBSCS               := GetParameter(piosDBConnect);
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  IsRelease                := GetParameter('IsRelease');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT || ' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  if piosDealer is null then
     str_create := str_create || ' WHERE CUSTOMER_ID ' ;
     str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--   str_create := str_create || ' WHERE CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  -- Release IX 2
  IF TO_NUMBER(IsRelease) >= 10 and piosDealer is not null THEN
     str_create := str_create || ' UNION ';
     str_create := str_create || ' SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_DEALER';
     if viadbxlink = 'Y' then
        str_create := str_create ||  '@' || vdbDBXHOME;
     end if;
  END IF;
  str_create := str_create || ' ) ';

  if piosDealer is null and USE_CUSTOMER_ID_RANGE = 'Y' then
     str_create := str_create || ' AND ' || piosColumnName;
     str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--   str_create := str_create || ' AND CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
  if piosDealer is null then
     str_where := str_where || ' WHERE CUSTOMER_ID ';
     str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--   str_where := str_where || ' WHERE CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  -- Release IX 2
  IF TO_NUMBER(IsRelease) >= 10 and piosDealer is not null THEN
     str_where  := str_where  || ' UNION ';
     str_where  := str_where  || ' SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_DEALER';
  END IF;
  str_where := str_where || ' ) ';

  if piosDealer is null and USE_CUSTOMER_ID_RANGE = 'Y' then
     str_where  := str_where  || ' AND ' || piosColumnName;
     str_where  := str_where  || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--   str_where  := str_where  || ' AND CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER@' || vdbDBXHOME ;
  if piosDealer is null then
    str_where_link := str_where_link || ' WHERE CUSTOMER_ID  ';
    str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--  str_where_link := str_where_link || ' WHERE CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  -- Release IX 2
  IF TO_NUMBER(IsRelease) >= 10 and piosDealer is not null THEN
     str_where_link  := str_where_link  || ' UNION ';
     str_where_link  := str_where_link  || ' SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_DEALER' ||'@' || vdbDBXHOME;
  END IF;
  str_where_link := str_where_link || ' ) ';

  if piosDealer is null and USE_CUSTOMER_ID_RANGE = 'Y' then
    str_where_link := str_where_link || ' AND ' || piosColumnName ;
    str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
--else
--  str_where_link := str_where_link || ' AND CUSTOMER_ID <= ' || CUSTOMER_ID_MAX ;
  end if;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );


  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );


EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Customer: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Customer;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Tickler
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Tickler
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE (CUSTOMER_ID,TICKLER_NUMBER) IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID,TICKLER_NUMBER FROM ' ||
                               vuserDBXHOME ||  '.DBX_TICKLER';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';
  str_create := str_create || ' AND ( CUSTOMER_ID ';
  str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
  str_create := str_create || ' OR    CUSTOMER_ID < 0 )';

  str_where := '(CUSTOMER_ID,TICKLER_NUMBER) IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID,TICKLER_NUMBER FROM ' || vuserDBXHOME || '.DBX_TICKLER)';
  str_where := str_where || ' AND ( CUSTOMER_ID ';
  str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
  str_where := str_where || ' OR    CUSTOMER_ID < 0 )';

  str_where_link := '(CUSTOMER_ID,TICKLER_NUMBER) IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID,TICKLER_NUMBER FROM DBX_TICKLER@' || vdbDBXHOME || ' )';
  str_where_link := str_where_link || ' AND ( CUSTOMER_ID ';
  str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
  str_where_link := str_where_link || ' OR    CUSTOMER_ID < 0 )';


  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Tickler: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Tickler;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Contract
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Contract
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 TURNOVERDATE              VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CONTRACT_ID_MIN           INTEGER;
 CONTRACT_ID_MAX           INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  TURNOVERDATE             := GetParameter('TURNOVERDATE');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' ||
                 vuserDBXHOME || '.DBX_CONTRACT';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str_create := str_create || ' AND ' || piosColumnName;
     str_create := str_create || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  END IF;
  IF piosTableName = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
     str_create := str_create || ' AND (CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')';
  END IF;

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT )';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str_where := str_where || ' AND ' || piosColumnName;
     str_where := str_where || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  END IF;
  IF piosTableName = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
     str_where := str_where || ' AND (CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')';
  END IF;

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT@' || vdbDBXHOME || ' )';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str_where_link := str_where_link || ' AND ' || piosColumnName ;
     str_where_link := str_where_link || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  END IF;
  IF piosTableName = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
     str_where_link := str_where_link || ' AND (CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')';
  END IF;


  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Contract: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Contract;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCRHRequest
   **
   ** Purpose:   Fille Table DBX_CRH_Request
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCRHRequest
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 tab_exist                 INTEGER := 0;
 tab_exist2                INTEGER := 0;
 minval                    NUMBER;
 maxval                    NUMBER;
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');

  str := 'SELECT COUNT(*) FROM DBA_TABLES';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
  str := str || ' AND   table_name = ' || '''' || 'CRH_REQUEST' || '''';
  execute immediate str INTO tab_exist ;

  str := 'SELECT COUNT(*) FROM DBA_TABLES';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
  str := str || ' AND   table_name = ' || '''' || 'CRH_REQUEST_HISTORY' || '''';
  execute immediate str INTO tab_exist2 ;

  str := 'INSERT /*+  APPEND */ INTO dbx_crh_request ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ INDEX (CRH_REQUEST IX_CRHREQST_COID_STATUS ) */ REQUEST_ID FROM CRH_REQUEST';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE REQUEST_ID IS NOT NULL ';

  str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  str := str || ' AND  REQUEST_ID NOT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM dbx_crh_request) ';
-- CRH_REQUEST_HISTORY
  if tab_exist2 > 0 then
     str := str || ' UNION ';
     str := str || 'SELECT /*+ INDEX (CRH_REQUEST_HISTORY IX_CRHREQSTH_COID_STATUS ) */ REQUEST_ID FROM CRH_REQUEST_HISTORY';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' WHERE REQUEST_ID IS NOT NULL ';
     str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
     str := str || ' ) ';
     str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
     str := str || ' AND  REQUEST_ID NOT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM dbx_crh_request) ';
  end if;

  if tab_exist > 0 then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CRH_REQUEST'
                  ,piosActionName => 'FillTableDBXCRHRequest'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCRHRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CRH_REQUEST'
                  ,piosActionName => 'FillTableDBXCRHRequest'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;

  str := 'SELECT nvl(min(REQUEST_ID),0), nvl(max(REQUEST_ID),0) FROM dbx_crh_request ';
  ShowStatement(str);
  execute immediate str into minval,maxval;

  SetParameter('CRHREQUEST_MIN',TO_CHAR(minval) );
  SetParameter('CRHREQUEST_MAX',TO_CHAR(maxval) );

  stop_time := GetTime;
  callinfo('FillTableDBXCRHRequest: Start:' || start_time );
  callinfo('FillTableDBXCRHRequest: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCRHRequest: Error:');
  CallError(SQLERRM);
END FillTableDBXCRHRequest;




 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXRequest
   **
   ** Purpose:   Fille Table DBX_Request
   **
   **=====================================================
*/


PROCEDURE FillTableDBXRequest
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 minval                    NUMBER;
 maxval                    NUMBER;
 PROFILE_CHANGE_HIST_FHINT VARCHAR2(4000);
 PR_SERV_SPCODE_HIST_FHINT VARCHAR2(4000);
 PR_SERV_STATUS_HIST_FHINT VARCHAR2(4000);
 CONTRACT_HISTORY_FHINT    VARCHAR2(4000);
 CONTR_SERVICES_CAP_FHINT  VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 EXCLUDE_GMD_REQUESTS      VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  PROFILE_CHANGE_HIST_FHINT:= GetParameter('PROFILE_CHANGE_HIST_FHINT');
  PR_SERV_SPCODE_HIST_FHINT:= GetParameter('PR_SERV_SPCODE_HIST_FHINT');
  PR_SERV_STATUS_HIST_FHINT:= GetParameter('PR_SERV_STATUS_HIST_FHINT');
  CONTRACT_HISTORY_FHINT   := GetParameter('CONTRACT_HISTORY_FHINT');
  CONTR_SERVICES_CAP_FHINT := GetParameter('CONTR_SERVICES_CAP_FHINT');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  EXCLUDE_GMD_REQUESTS     := GetParameter('EXCLUDE_GMD_REQUESTS');

 if trim(EXCLUDE_GMD_REQUESTS) IS NULL or trim(EXCLUDE_GMD_REQUESTS) != '1' then
  str := 'INSERT /*+  APPEND */ INTO dbx_request_1 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ ' || PROFILE_CHANGE_HIST_FHINT ;
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ DISTINCT tab.REQUEST_ID FROM PROFILE_CHANGE_HIST';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';

  str := str || ' WHERE tab.REQUEST_ID IS NOT NULL ';

  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request_2 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ ' || PR_SERV_SPCODE_HIST_FHINT ;
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ distinct tab.REQUEST_ID FROM PR_SERV_SPCODE_HIST';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';
  str := str || ' WHERE tab.REQUEST_ID IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request_3 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ ' || PR_SERV_STATUS_HIST_FHINT ;
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ distinct REQUEST_ID FROM PR_SERV_STATUS_HIST';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';
  str := str || ' WHERE tab.REQUEST_ID IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request_4 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ ' || CONTRACT_HISTORY_FHINT ;
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ distinct REQUEST FROM CONTRACT_HISTORY';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';
  str := str || ' WHERE tab.REQUEST IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest4'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest4'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request_5 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT /*+ ' || CONTR_SERVICES_CAP_FHINT ;
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ distinct CS_REQUEST FROM CONTR_SERVICES_CAP';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';
  str := str || ' WHERE tab.CS_REQUEST IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest5'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest5'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_1 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_2 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_3 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_4 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_5 ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'SELECT nvl(min(REQUEST_ID),0), nvl(max(REQUEST_ID),0) FROM dbx_request ';
  ShowStatement(str);
  execute immediate str into minval,maxval;

  SetParameter('REQUEST_MIN',TO_CHAR(minval) );
  SetParameter('REQUEST_MAX',TO_CHAR(maxval) );

  stop_time := GetTime;
  callinfo('FillTableDBXRequest: Start:' || start_time );
  callinfo('FillTableDBXRequest: Stop: ' || stop_time );
 else
  callinfo('FillTableDBXRequest: Excluded via parameter EXCLUDE_GMD_REQUESTS in dbextract.cfg or default.cfg');
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest'
               ,piosMessage    => 'EXCLUDED'
               ,piosPara1      => 'Excluded via parameter EXCLUDE_GMD_REQUESTS in dbextract.cfg or default.cfg');
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest'
               ,piosMessage    => 'EXCLUDED');
  commit;
 end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXRequest: Error:');
  CallError(SQLERRM);
END FillTableDBXRequest;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXRequest6
   **
   ** Purpose:   Fille Table DBX_Request
   **
   **=====================================================
*/


PROCEDURE FillTableDBXRequest6
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 BSCS523                   VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 minval                    NUMBER;
 maxval                    NUMBER;
 EXCLUDE_GMD_REQUESTS      VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  EXCLUDE_GMD_REQUESTS     := GetParameter('EXCLUDE_GMD_REQUESTS');
  BSCS523                  := GetParameter('BSCS523');

-- Different
 if trim(EXCLUDE_GMD_REQUESTS) IS NULL or trim(EXCLUDE_GMD_REQUESTS) != '1' then
  str := 'INSERT /*+  APPEND */ INTO dbx_request_1 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT DISTINCT CS_REQUEST FROM CONTR_SERVICES';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CS_REQUEST IS NOT NULL ';

  str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+  APPEND */ INTO dbx_request_2 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT distinct REQUEST FROM CONTRACT_HISTORY';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE REQUEST IS NOT NULL ';

  str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  str := str || ' AND  REQUEST NOT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM dbx_request) ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

 if BSCS523 IS NULL or BSCS523 = 'N' then
  str := 'INSERT /*+  APPEND */ INTO dbx_request_3 ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT distinct CS_REQUEST FROM CONTR_SERVICES_CAP';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CS_REQUEST IS NOT NULL ';

  str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  str := str || ' AND  CS_REQUEST NOT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM dbx_request) ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
 end if;
  str := 'INSERT /*+  APPEND */ INTO dbx_request ';
  str := str || '(REQUEST_ID) ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_1 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_2 ';
  str := str || 'UNION ';
  str := str || 'SELECT REQUEST_ID FROM dbx_request_3 ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_4'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXRequest6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest6_4'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'SELECT nvl(min(REQUEST_ID),0), nvl(max(REQUEST_ID),0) FROM dbx_request ';
  ShowStatement(str);
  execute immediate str into minval,maxval;

  SetParameter('REQUEST_MIN',TO_CHAR(minval) );
  SetParameter('REQUEST_MAX',TO_CHAR(maxval) );

  stop_time := GetTime;
  callinfo('FillTableDBXRequest6: Start:' || start_time );
  callinfo('FillTableDBXRequest6: Stop: ' || stop_time );
 else
  callinfo('FillTableDBXRequest: Excluded via parameter EXCLUDE_GMD_REQUESTS in dbextract.cfg or default.cfg');
  DBXLogStart ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest'
               ,piosMessage    => 'EXCLUDED'
               ,piosPara1      => 'Excluded via parameter EXCLUDE_GMD_REQUESTS in dbextract.cfg or default.cfg');
  callinfo('FillTableDBXRequest: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_REQUEST'
               ,piosActionName => 'FillTableDBXRequest'
               ,piosMessage    => 'EXCLUDED');
  commit;
 end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXRequest6: Error:');
  CallError(SQLERRM);
END FillTableDBXRequest6;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_CRHRequest
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_CRHRequest
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 tab_exist                 INTEGER := 0;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  -- Check first if table exists , specially for 801
  str := 'SELECT COUNT(*) FROM DBA_TABLES';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
  str := str || ' AND   table_name = ' || '''' || piosTableName || '''';
  execute immediate str INTO tab_exist ;


  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' ||
                   vuserDBXHOME   || '.DBX_CRH_REQUEST';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CRH_REQUEST  )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_CRH_REQUEST@' || vdbDBXHOME || ' )';

 if tab_exist > 0 then
  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

   CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
  end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_CRHRequest: Error:');
  CallError(SQLERRM);
END SetDBX_Where_CRHRequest;





 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Request
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Request
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
 EXCLUDE_GMD_REQUESTS      VARCHAR2(4000);
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  EXCLUDE_GMD_REQUESTS     := GetParameter('EXCLUDE_GMD_REQUESTS');

 if trim(EXCLUDE_GMD_REQUESTS) IS NULL or trim(EXCLUDE_GMD_REQUESTS) != '1' then

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' ||
                vuserDBXHOME || '.DBX_REQUEST';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_REQUEST  )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_REQUEST@' || vdbDBXHOME || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
 else
  Exclude_Tab ( piosTableName => piosTableName ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
 end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Request: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Request;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPort
   **
   ** Purpose:   Fille Table DBX_PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPort
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 CONTR_DEVICES_FHINT       VARCHAR2(4000);
 CONTR_SERVICES_PORT_FHINT VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  CONTR_DEVICES_FHINT      := GetParameter('CONTR_DEVICES_FHINT');
  CONTR_SERVICES_PORT_FHINT:= GetParameter('CONTR_SERVICES_PORT_FHINT');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

  str := 'INSERT INTO dbx_port ';
  str := str || '(PORT_ID) ';
  str := str || 'SELECT /*+ ' || CONTR_DEVICES_FHINT || ' */ distinct tab.PORT_ID  FROM CONTR_DEVICES';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, DBX_CONTRACT dbx ';
  str := str || ' WHERE tab.PORT_ID IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.co_id ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  str := str || ' UNION ';
  str := str || 'SELECT /*+ ' || CONTR_SERVICES_PORT_FHINT || ' */ distinct tab.PORT_ID  FROM CONTR_SERVICES_PORT';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, DBX_CONTRACT dbx ';
  str := str || ' WHERE tab.PORT_ID IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.co_id ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PORT'
               ,piosActionName => 'FillTableDBXPort'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXPort: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PORT'
               ,piosActionName => 'FillTableDBXPort'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXPort: Start:' || start_time );
  callinfo('FillTableDBXPort: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPort: Error:');
  CallError(SQLERRM);
END FillTableDBXPort;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPort6
   **
   ** Purpose:   Fille Table DBX_PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPort6
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');


  str := 'INSERT INTO dbx_port ';
  str := str || '(PORT_ID) ';
  str := str || 'SELECT distinct PORT_ID  FROM CONTR_DEVICES';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE PORT_ID IS NOT NULL ';

  str := str || ' AND  CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
  str := str || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
--  str := str || ' AND  PORT_ID NOT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM DBX_PORT) ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PORT'
               ,piosActionName => 'FillTableDBXPort6'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXPort: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PORT'
               ,piosActionName => 'FillTableDBXPort6'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXPort6: Start:' || start_time );
  callinfo('FillTableDBXPort6: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXPort6: Error:');
  CallError(SQLERRM);
END FillTableDBXPort6;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXPORTFree
   **
   ** Purpose:   Fille Table DBX_PORT with free port numbers
   **
   **=====================================================
*/


PROCEDURE FillTableDBXPORTFree
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 v_plcode                  INTEGER;
 v_hlcode                  INTEGER;
 v_free_port               INTEGER;
 i                         INTEGER :=0;
BEGIN
  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  v_free_port              := GetParameter('FREE_PORT');

  IF v_free_port IS NOT NULL and v_free_port > 0 THEN
    v_select_statement := 'SELECT p.plcode,h.hlcode ' ;
    v_select_statement := v_select_statement || ' FROM mpdpltab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' p, mpdhltab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' h ';
    v_select_statement := v_select_statement || ' WHERE p.plmntype = ' ||''''|| 'H' || '''' ;
    v_select_statement := v_select_statement || ' AND   p.plcode = h.plcode' ;
    v_select_statement := v_select_statement || ' ORDER BY p.plcode,h.plcode ';

    DBXLogStart ( piosTableName  => 'DBX_PORT'
                 ,piosActionName => 'FillTableDBXPORTFree'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => v_select_statement);
    OPEN cur FOR v_select_statement ;
    LOOP
      FETCH cur INTO v_plcode, v_hlcode  ;
      EXIT WHEN cur%NOTFOUND;

      str := 'INSERT /*+ APPEND */ INTO dbx_port ';
      str := str || '(PORT_ID) ';
      str := str || 'SELECT PORT_ID  FROM PORT';

      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' WHERE PORT_STATUS = ' || '''' || 'f' || '''';

      str := str || ' AND  plcode = ' || v_plcode;
      str := str || ' AND  hlcode = ' || v_hlcode;
      str := str || ' AND  rownum <= ' || v_free_port;
      str := str || ' MINUS ';
      str := str || ' SELECT PORT_ID FROM dbx_port';

      ShowStatement(str);
      execute immediate str;
      callinfo('FillTableDBXPORTFree: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
      commit;
      i := i + SQL%ROWCOUNT;
    END LOOP;
    CLOSE cur;
    DBXLogStop  ( piosTableName  => 'DBX_PORT'
                 ,piosActionName => 'FillTableDBXPORTFree'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(i) ||')' );
  END IF;

  stop_time := GetTime;
  callinfo('FillTableDBXPORTFree: Start:' || start_time );
  callinfo('FillTableDBXPORTFree: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXPORTFree: Error:');
  CallError(SQLERRM);
END FillTableDBXPORTFree;





 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Port
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Port
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM ' || vuserDBXHOME || '.DBX_PORT';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM ' || vuserDBXHOME || '.DBX_PORT  )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM DBX_PORT@' || vdbDBXHOME || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Port: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Port;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDN
   **
   ** Purpose:   Fille Table DBX_DN
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDN
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 CONTR_DEVICES_FHINT       VARCHAR2(4000);
 PORT_FHINT                VARCHAR2(4000);
 CONTR_SERVICES_CAP2_FHINT VARCHAR2(4000);
 DIRECTORY_NUMBER_FHINT    VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 BSCS523                   VARCHAR2(4000);
BEGIN
  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  CONTR_DEVICES_FHINT      := GetParameter('CONTR_DEVICES_FHINT');
  PORT_FHINT               := GetParameter('PORT_FHINT');
  CONTR_SERVICES_CAP2_FHINT:= GetParameter('CONTR_SERVICES_CAP2_FHINT');
  DIRECTORY_NUMBER_FHINT   := GetParameter('DIRECTORY_NUMBER_FHINT');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  vIsRelease               := GetParameter('IsRelease');
  BSCS523                  := GetParameter('BSCS523');


  str := 'INSERT INTO dbx_dn ';
  str := str || '(DN_ID) ';
  str := str || 'SELECT /*+ ' || CONTR_DEVICES_FHINT || ' */ DISTINCT tab.DN_ID  FROM CONTR_DEVICES';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, DBX_CONTRACT dbx ';
  str := str || ' WHERE tab.DN_ID IS NOT NULL ';
  str := str || ' AND   tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  str := str || ' UNION ';
  str := str || 'SELECT /*+' || PORT_FHINT || ' */ DISTINCT tab.DN_ID  FROM PORT ';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, DBX_PORT dbx ';
  str := str || ' WHERE tab.DN_ID IS NOT NULL ';
  str := str || ' AND  tab.PORT_ID = dbx.PORT_ID ';
 if BSCS523 IS NULL OR BSCS523 = 'N' then
  str := str || ' UNION ';
  str := str || 'SELECT /*+ ' || CONTR_SERVICES_CAP2_FHINT || ' */ distinct tab.DN_ID  FROM CONTR_SERVICES_CAP';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, DBX_CONTRACT dbx ';
  str := str || ' WHERE tab.DN_ID IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  if TO_NUMBER(vIsRelease) >= 11 then
     str := str || ' UNION ';
     str := str || 'SELECT /*+ ' || CONTR_SERVICES_CAP2_FHINT || ' */ distinct tab.PUBLIC_NUMBER_DN_ID  FROM CONTR_SERVICES_CAP';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' tab, DBX_CONTRACT dbx ';
     str := str || ' WHERE tab.PUBLIC_NUMBER_DN_ID IS NOT NULL ';
     str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
     IF USE_CONTRACT_ID_RANGE = 'Y' THEN
        str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
     END IF;
  end iF;
 end iF;
 if TO_NUMBER(vIsRelease) > 5 then
  str := str || ' UNION ';
  str := str || 'SELECT /*+ ' || DIRECTORY_NUMBER_FHINT || ' */ distinct tab.DN_ID  FROM DIRECTORY_NUMBER';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab, MPDNPTAB';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' dbx ';
  str := str || ' WHERE tab.DIRNUM_NPCODE = dbx.NPCODE ';
  str := str || ' AND   dbx.NPCODE_CLASS = ' || '''' || 'A' || '''' ;
 end if;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_DN'
               ,piosActionName => 'FillTableDBXDN'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXDN: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_DN'
               ,piosActionName => 'FillTableDBXDN'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  stop_time := GetTime;
  callinfo('FillTableDBXDN: Start:' || start_time );
  callinfo('FillTableDBXDN: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXDN: Error:');
  CallError(SQLERRM);
END FillTableDBXDN;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDNFree
   **
   ** Purpose:   Fill Table DBX_DN with free directory numbers
   **            ,which are not configured by table PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDNFree
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 v_plcode                  INTEGER;
 v_hlcode                  INTEGER;
 v_free_dn                 INTEGER;
 i                         INTEGER := 0;
BEGIN
  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  v_free_dn                := GetParameter('FREE_DN');

  IF v_free_dn IS NOT NULL and v_free_dn > 0 THEN
    v_select_statement := 'SELECT p.plcode,h.hlcode ' ;
    v_select_statement := v_select_statement || ' FROM mpdpltab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' p, mpdhltab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' h, mpdsctab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' s ';
    v_select_statement := v_select_statement || ' WHERE p.plmntype = ' ||''''|| 'H' || '''' ;
    v_select_statement := v_select_statement || ' AND   p.plcode = h.plcode' ;
    v_select_statement := v_select_statement || ' AND   p.sccode = s.sccode' ;
    v_select_statement := v_select_statement || ' AND   s.scslprefix NOT IN (' ||''''|| 'GSM' || '''' || ') ';
    v_select_statement := v_select_statement || ' ORDER BY p.plcode,h.plcode ';
   /*
    * For GSM DNs are linked with Ports per default
    */

    DBXLogStart ( piosTableName  => 'DBX_DN'
                 ,piosActionName => 'FillTableDBXDNFree'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => v_select_statement);
    OPEN cur FOR v_select_statement ;
    LOOP
      FETCH cur INTO v_plcode, v_hlcode  ;
      EXIT WHEN cur%NOTFOUND;

      str := 'INSERT INTO dbx_dn ';
      str := str || '(DN_ID) ';
      str := str || 'SELECT D.DN_ID  FROM DIRECTORY_NUMBER';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' D , PORT';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' P ';
      str := str || ' WHERE D.DN_STATUS = ' || '''' || 'f' || '''';

      str := str || ' AND  D.plcode = ' || v_plcode;
      str := str || ' AND  D.hlcode = ' || v_hlcode;
      str := str || ' AND  D.dn_id  = P.DN_ID (+) ';
      str := str || ' AND  P.DN_ID IS NULL ';
      str := str || ' AND  rownum <= ' || v_free_dn;
      str := str || ' MINUS ';
      str := str || ' SELECT DN_ID FROM dbx_dn';

      ShowStatement(str);
      execute immediate str;
      callinfo('FillTableDBXDNFree: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
      commit;
      I := i + SQL%ROWCOUNT;
    END LOOP;
    CLOSE cur;
    DBXLogStop  ( piosTableName  => 'DBX_DN'
                 ,piosActionName => 'FillTableDBXDNFree'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(i) ||')' );

  END IF;

  stop_time := GetTime;
  callinfo('FillTableDBXDNFree: Start:' || start_time );
  callinfo('FillTableDBXDNFree: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXDNFree: Error:');
  CallError(SQLERRM);
END FillTableDBXDNFree;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_DN
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_DN
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM  ' || vuserDBXHOME || '.DBX_DN';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM ' || vuserDBXHOME || '.DBX_DN  )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM DBX_DN@' || vdbDBXHOME || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_DN: Error:');
  CallError(SQLERRM);
END SetDBX_Where_DN;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXSM
   **
   ** Purpose:   Fille Table DBX_SM
   **
   **=====================================================
*/


PROCEDURE FillTableDBXSM
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 IS_NII_SPECIFIC           VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);

BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  IS_NII_SPECIFIC          := GetParameter('IS_NII_SPECIFIC');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CUSTOMER_ID_RANGE');

  str := 'INSERT INTO dbx_sm ';
  str := str || '(SM_ID) ';
  str := str || 'SELECT DISTINCT SM_ID FROM PORT';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE PORT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM DBX_PORT';
  str := str || ' ) ';
  str := str || ' AND   SM_ID IS NOT NULL ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_SM'
               ,piosActionName => 'FillTableDBXSM-1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXSM-1: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_SM'
               ,piosActionName => 'FillTableDBXSM-1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  if  IS_NII_SPECIFIC = 'Y' then

     str := 'INSERT INTO dbx_sm ';
     str := str || '(SM_ID) ';
     str := str || 'SELECT DISTINCT RL.SM_ID FROM CONTR_DEVICES ';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' CD, RESOURCE_LINK ';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' RL ';
     str := str || ' WHERE CD.EQ_ID = RL.EQUIPMENT_ID ';
     str := str || ' AND   RL.SM_ID IS NOT NULL ';
     str := str || ' AND   CD.CO_ID IS NOT NULL ';
     str := str || ' AND   CD.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
     str := str || ' WHERE  CO_ID  BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
     str := str || ' ) ';
     str := str || 'MINUS ';
     str := str || 'SELECT SM_ID FROM dbx_sm';

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_SM'
                  ,piosActionName => 'FillTableDBXSM-2'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXSM-2: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_SM'
                  ,piosActionName => 'FillTableDBXSM-2'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;

  stop_time := GetTime;
  callinfo('FillTableDBXSM: Start:' || start_time );
  callinfo('FillTableDBXSM: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  CallInfo('FillTableDBXSM: Error:');
  CallError(SQLERRM);
END FillTableDBXSM;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXSMFree
   **
   ** Purpose:   Fille Table DBX_SM with free storage mediums without PORTS
   **            ,which are not configured by table PORT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXSMFree
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 v_plcode                  INTEGER;
 v_free_sm                 INTEGER;
 i                         INTEGER := 0;
BEGIN
  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  v_free_sm                := GetParameter('FREE_SM');

  IF v_free_sm IS NOT NULL and v_free_sm > 0 THEN
    v_select_statement := 'SELECT p.plcode ' ;
    v_select_statement := v_select_statement || ' FROM mpdpltab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' p , mpdsctab';
    if vialink = 'Y' then
       v_select_statement := v_select_statement ||  '@' || vdbBSCS;
    end if;
    v_select_statement := v_select_statement || ' s ';
    v_select_statement := v_select_statement || ' WHERE p.plmntype = ' ||''''|| 'H' || '''' ;
    v_select_statement := v_select_statement || ' AND   p.sccode =  s.sccode ';
    v_select_statement := v_select_statement || ' AND   s.scslprefix NOT IN (' ||''''|| 'GSM' || '''' || ') ';
    v_select_statement := v_select_statement || ' ORDER BY p.plcode';
    /*
     * In GSM a storage_medium is always linked to a port
     */

    DBXLogStart ( piosTableName  => 'DBX_SM'
                 ,piosActionName => 'FillTableDBXSMFree'
                 ,piosMessage    => 'FILL'
                 ,piosPara1      => v_select_statement);
    OPEN cur FOR v_select_statement ;
    LOOP
      FETCH cur INTO v_plcode  ;
      EXIT WHEN cur%NOTFOUND;

      str := 'INSERT INTO dbx_sm ';
      str := str || '(SM_ID) ';
      str := str || 'SELECT S.SM_ID  FROM STORAGE_MEDIUM';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' S , PORT';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' P ';
      str := str || ' WHERE SM_STATUS = ' || '''' || 'f' || '''';

      str := str || ' AND  S.plcode = ' || v_plcode;
      str := str || ' AND  S.SM_ID = P.SM_ID (+) ';
      str := str || ' AND  P.SM_ID IS NULL ';
      str := str || ' AND  rownum <= ' || v_free_sm;
      str := str || ' MINUS ';
      str := str || ' SELECT SM_ID FROM dbx_sm';

      ShowStatement(str);
      execute immediate str;
      callinfo('FillTableDBXSMFree: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
      commit;
      i := i + SQL%ROWCOUNT ;
    END LOOP;
    CLOSE cur;
    DBXLogStop  ( piosTableName  => 'DBX_SM'
                 ,piosActionName => 'FillTableDBXSMFree'
                 ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(i) ||')' );
  END IF;

  stop_time := GetTime;
  callinfo('FillTableDBXSMFree: Start:' || start_time );
  callinfo('FillTableDBXSMFree: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXSMFree: Error:');
  CallError(SQLERRM);
END FillTableDBXSMFree;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_SM
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_SM
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ SM_ID FROM ' || vuserDBXHOME || '.DBX_SM';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ SM_ID FROM ' || vuserDBXHOME || '.DBX_SM  )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ SM_ID FROM DBX_SM@' || vdbDBXHOME || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_SM: Error:');
  CallError(SQLERRM);
END SetDBX_Where_SM;

 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXOrderhdr
   **
   ** Purpose:   Fille Table DBX_ORDERHDR
   **
   **=====================================================
*/


PROCEDURE FillTableDBXOrderhdr
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 ORDERHDR_ALL1_HINT        VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 ORDERHDR_ALL_HINT         VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 BSCS523                   VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 vohentdate_string         VARCHAR2(30);
 v_count_cont_ohxact       INTEGER;
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  ORDERHDR_ALL_HINT        := GetParameter('ORDERHDR_ALL_HINT');
  ORDERHDR_ALL1_HINT       := GetParameter('ORDERHDR_ALL1_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  ordhdrentdate            := GetParameter('ordhdrentdate');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CUSTOMER_ID_RANGE');
  BSCS523                  := GetParameter('BSCS523');

  if ordhdrentdate is not null and ordhdrentdate != '0' and ordhdrentdate != '-1' then
     str := 'select TO_CHAR(nvl(max(ohentdate),sysdate) - '|| ordhdrentdate || ',' || '''' || 'YYYY-MM-DD' || '''' ||') from orderhdr_all';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     execute immediate str into vohentdate_string;
     SetParameter ('vohentdate_string',vohentdate_string);
  end if;

  str := 'INSERT /*+  APPEND */ INTO dbx_orderhdr_1 ';
  str := str || '(OHXACT,OHENTDATE,OHBILLSEQNO) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || ORDERHDR_ALL1_HINT ;
  if BSCS523 is null or BSCS523 = 'N' then
     str := str || ' */ OHXACT,OHENTDATE,OHBILLSEQNO FROM ORDERHDR_ALL';
  else
     str := str || ' */ OHXACT,OHENTDATE,NULL        FROM ORDERHDR_ALL';
  end if;

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
  str := str || ' WHERE CUSTOMER_ID BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX;
  str := str || ' ) ';
  IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
     str := str || ' AND CUSTOMER_ID ';
     str := str || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
  END IF;
  if ordhdrentdate is not null and ordhdrentdate != '0' and ordhdrentdate != '-1' then
     str := str || ' AND ohentdate >= TO_DATE(' || '''' || vohentdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')';
  end if;

  if ordhdrentdate is null or (ordhdrentdate is not null and ordhdrentdate != '-1') then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_ORDERHDR'
                  ,piosActionName => 'FillTableDBXOrderhdr1'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXOrderhdr: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_ORDERHDR'
                  ,piosActionName => 'FillTableDBXOrderhdr1'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;

  str := 'select count(*) from dbx_contract_ohxact';
  execute immediate str into v_count_cont_ohxact;

  IF v_count_cont_ohxact > 0 THEN
     str := 'INSERT /*+  APPEND */ INTO dbx_orderhdr_2 ';
     str := str || '(OHXACT,OHENTDATE,OHBILLSEQNO) ';
     str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || ORDERHDR_ALL_HINT ;
     if BSCS523 is null or BSCS523 = 'N' then
        str := str || ' */ OHXACT,OHENTDATE,OHBILLSEQNO FROM ORDERHDR_ALL';
     else
        str := str || ' */ OHXACT,OHENTDATE,NULL        FROM ORDERHDR_ALL';
     end if;
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' WHERE OHXACT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM dbx_contract_ohxact';
     str := str || ' ) ';

     if ordhdrentdate is null or (ordhdrentdate is not null and ordhdrentdate != '-1') then
        ShowStatement(str);
        DBXLogStart ( piosTableName  => 'DBX_ORDERHDR'
                     ,piosActionName => 'FillTableDBXOrderhdr2'
                     ,piosMessage    => 'FILL'
                     ,piosPara1      => str);
        execute immediate str;
        callinfo('FillTableDBXOrderhdr: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
        DBXLogStop  ( piosTableName  => 'DBX_ORDERHDR'
                     ,piosActionName => 'FillTableDBXOrderhdr2'
                     ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     end if;
  end if;

  str := 'INSERT /*+ APPEND */ INTO DBX_ORDERHDR  ';
  str := str || ' SELECT * from DBX_ORDERHDR_1 ';
  IF v_count_cont_ohxact > 0 THEN
     str := str || ' union  ';
     str := str || ' select * from DBX_ORDERHDR_2 ';
  END IF;
  if ordhdrentdate is null or (ordhdrentdate is not null and ordhdrentdate != '-1') then
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_ORDERHDR'
                  ,piosActionName => 'FillTableDBXOrderhdr3'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCash (DBX_ORDERHDR): Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_ORDERHDR'
                  ,piosActionName => 'FillTableDBXOrderhdr3'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  end if;
  stop_time := GetTime;
  callinfo('FillTableDBXOrderhdr: Start:' || start_time );
  callinfo('FillTableDBXOrderhdr: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXOrderhdr: Error:');
  CallError(SQLERRM);
END FillTableDBXOrderhdr;




 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXBillseqno
   **
   ** Purpose:   Fille Table DBX_BILLSEQNO
   **
   **=====================================================
*/


PROCEDURE FillTableDBXBillseqno
AS
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vialink                  := GetParameter('vialink');

  str := 'INSERT /*+  APPEND */ INTO dbx_billseqno ';
  str := str || '(BILLSEQNO) ';
  str := str || 'SELECT DISTINCT OHBILLSEQNO FROM DBX_ORDERHDR ';
  str := str || 'WHERE  OHBILLSEQNO IS NOT NULL ' ;
  str := str || 'MINUS  ' ;
  str := str || 'SELECT BILLSEQNO FROM dbx_billseqno ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_BILLSEQNO'
               ,piosActionName => 'FillTableDBXBillseqno'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXBillseqno: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_BILLSEQNO'
               ,piosActionName => 'FillTableDBXBillseqno'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  stop_time := GetTime;
  callinfo('FillTableDBXBillseqno: Start:' || start_time );
  callinfo('FillTableDBXBillseqno: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXOrderhdr: Error:');
  CallError(SQLERRM);
END FillTableDBXBillseqno;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXThufitab
   **
   ** Purpose:   Fill Table DBX_THUFITAB
   **
   **=====================================================
*/

PROCEDURE FillTableDBXThufitab
AS
 vialink                   VARCHAR2(1);
 vdbBSCS                   VARCHAR2(4000);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 GLOBAL_HINT               VARCHAR2(4000);
 THUFITAB_HINT             VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  THUFITAB_HINT            := GetParameter('THUFITAB_HINT');

  -- Load all different file types
  str := 'INSERT INTO dbx_thufitab ';
  str := str || '(FT_ID) ';
  str := str || 'SELECT FT_ID FROM THSFTTAB' ;
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS || ' ';
  end if;
  str := str || ' MINUS  ' ;
  str := str || 'SELECT FT_ID FROM dbx_thufitab';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufitab: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  -- Get one FILE_ID for every file type when possible
  str := 'UPDATE dbx_thufitab XI ';
  str := str || 'SET XI.FILE_ID = ';
  str := str || '(SELECT FI.FILE_ID FROM THUFITAB' ;
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS || ' ';
  end if;
  str := str || ' FI ' ;
  str := str || 'WHERE XI.FT_ID = FI.FILE_TYPE AND ROWNUM = 1)' ;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufitab: Rows updated  ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  -- delete all unused file ids
  str := 'DELETE FROM dbx_thufitab WHERE FILE_ID IS NULL';
  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufitab: Rows deleted  ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFITAB'
               ,piosActionName => 'FillTableDBXThufitab3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

--str := 'INSERT INTO dbx_thufitab ';
--str := str || '(FILE_ID) ';
--str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || THUFITAB_HINT ||' */ MAX(FILE_ID) FROM THUFITAB' ;
--if vialink = 'Y' then
--   str := str ||  '@' || vdbBSCS || ' ';
--end if;
--str := str || 'GROUP BY (FILE_TYPE) ' ;
--str := str || 'MINUS  ' ;
--str := str || 'SELECT FILE_ID FROM dbx_thufitab';
--
--ShowStatement(str);
--execute immediate str;
--callinfo('FillTableDBXThufitab: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );

  stop_time := GetTime;
  callinfo('FillTableDBXThufitab: Start:' || start_time );
  callinfo('FillTableDBXThufitab: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXThufitab: Error:');
  CallError(SQLERRM);
END FillTableDBXThufitab;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXThufotab
   **
   ** Purpose:   Fill Table DBX_THUFOTAB
   **
   **=====================================================
*/

PROCEDURE FillTableDBXThufotab
AS
 vialink                   VARCHAR2(1);
 vdbBSCS                   VARCHAR2(4000);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 GLOBAL_HINT               VARCHAR2(4000);
 THUFOTAB_HINT             VARCHAR2(4000);
 BSCS523                   VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  THUFOTAB_HINT            := GetParameter('THUFOTAB_HINT');
  BSCS523                  := GetParameter('BSCS523');

  -- Load all different file types
  str := 'INSERT INTO dbx_thufotab ';
  str := str || '(FT_ID) ';
  str := str || 'SELECT FT_ID FROM THSFTTAB' ;
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS || ' ';
  end if;
  str := str || ' MINUS  ' ;
  str := str || 'SELECT FT_ID FROM dbx_thufotab';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufotab: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  if BSCS523 IS NULL or BSCS523 = 'N' then
     -- Get one FILE_ID for every file type when possible
     str := 'UPDATE dbx_thufotab XI ';
     str := str || 'SET XI.FILE_ID = ';
     str := str || '(SELECT FI.FILE_ID FROM THUFOTAB' ;
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS || ' ';
     end if;
     str := str || ' FI ' ;
     str := str || 'WHERE XI.FT_ID = FI.FT_ID AND ROWNUM = 1)' ;
  else
     -- Get one FILE_ID for every file type when possible
     str := 'UPDATE dbx_thufotab XI ';
     str := str || 'SET XI.FILE_ID = ';
     str := str || '(SELECT FI.FILE_ID FROM THUFITAB' ;
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS || ' ';
     end if;
     str := str || ' FI ' ;
     str := str || 'WHERE XI.FT_ID = FI.FILE_TYPE AND ROWNUM = 1)' ;
  end if;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufotab: Rows updated  ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  -- delete all unused file ids
  str := 'DELETE FROM dbx_thufotab WHERE FILE_ID IS NULL';
  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXThufotab: Rows deleted  ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_THUFOTAB'
               ,piosActionName => 'FillTableDBXThufotab3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXThufotab: Start:' || start_time );
  callinfo('FillTableDBXThufotab: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXThufotab: Error:');
  CallError(SQLERRM);
END FillTableDBXThufotab;





 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Billseqno
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Billseqno
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 viadbxlink                VARCHAR2(1);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');
  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');

  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  begin

     str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
     str_create := str_create || ' AS ';
     str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
     str_create := str_create || '  */ * FROM ' || piosTableName;
     str_create := str_create ||  '@' || vdbBSCS;
     str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO   FROM ' || vuserDBXHOME || '.DBX_BILLSEQNO';
     if viadbxlink = 'Y' then
        str_create := str_create ||  '@' || vdbDBXHOME ;
     end if;
     str_create := str_create ||  ')';

     str_where := piosColumnName ||  ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO   FROM ' || vuserDBXHOME || '.DBX_BILLSEQNO )';

     str_where_link := piosColumnName ||  ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO FROM DBX_BILLSEQNO';
     str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;

     SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );

   EXCEPTION WHEN OTHERS THEN
      CallError(SQLERRM);
   end;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Billseqno: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Billseqno;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_GLENTRY
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_GLENTRY
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 GLOBAL_HINT               VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 los_pperiod               VARCHAR2(100);
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');

  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str := 'SELECT max(PPPERIOD) FROM   POSTPERIOD_ALL';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE PPGLCURMTH =' || '''' || 'C' || '''';

  begin
     ShowStatement(str);
     execute immediate str into los_pperiod ;
     callinfo('SetDBX_Where_GLENTRY: Posting period: ' || los_pperiod );

     str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
     str_create := str_create || ' AS ';
     str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
     str_create := str_create || '  */ * FROM ' || piosTableName;
     str_create := str_create || '@' || vdbBSCS ;
     str_create := str_create || ' WHERE ' || piosColumnName || ' = ' || '''' || los_pperiod || '''' ;

     str_where := piosColumnName ||  ' = ' || '''' || los_pperiod || '''' ;

     str_where_link := piosColumnName ||  ' = ' || '''' || los_pperiod || '''' ;

     SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
   EXCEPTION WHEN no_data_found THEN
      CallInfo('Table GLENTRY can not be reduced!');
             WHEN OTHERS THEN
      CallError(SQLERRM);
   end;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_GLENTRY: Error:');
  CallError(SQLERRM);
END SetDBX_Where_GLENTRY;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Orderhdr
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Orderhdr
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 USE_OHXACT_RANGE          VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 OHXACT_MIN                INTEGER;
 OHXACT_MAX                INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  USE_OHXACT_RANGE         := GetParameter('USE_OHXACT_RANGE');
  ordhdrentdate            := GetParameter('ordhdrentdate');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str_create := str_create || ' AND ' || piosColumnName;
     str_create := str_create || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR )';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str_where := str_where || ' AND ' || piosColumnName;
     str_where := str_where || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR@' || vdbDBXHOME || ' )';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str_where_link := str_where_link || ' AND ' || piosColumnName ;
     str_where_link := str_where_link || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  IF ordhdrentdate IS NOT NULL AND ordhdrentdate = '-1' THEN
     Exclude_Tab ( piosTableName => piosTableName ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
  ELSE
     SetDBXWhereClause( piosTableName => piosTableName
                       ,piosColumnName => piosColumnName
                       ,piosCreateClause => str_create
                       ,piosWhereClause => str_where
                       ,piosWhereLinkClause => str_where_link
                       ,piosHint => str_hint
                       ,piosStorage => str_storage
                       ,piosDBConnect => vdbconBSCS );
     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
  END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Orderhdr: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Orderhdr;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXCash
   **
   ** Purpose:   Fille Table DBX_ORDERHDR
   **
   **=====================================================
*/


PROCEDURE FillTableDBXCash
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 CASHRECEIPTS_ALL_HINT     VARCHAR2(4000);
 CASHDETAIL2_HINT          VARCHAR2(4000);
 OHXACT_MIN                VARCHAR2(4000);
 OHXACT_MAX                VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 vIsRelease                VARCHAR2(4000);
 USE_OHXACT_RANGE          VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 minval                    NUMBER;
 maxval                    NUMBER;
 los_username              VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CASHRECEIPTS_ALL_HINT    := GetParameter('CASHRECEIPTS_ALL_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  CASHDETAIL2_HINT         := GetParameter('CASHDETAIL2_HINT');
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  ordhdrentdate            := GetParameter('ordhdrentdate');
  vIsRelease               := GetParameter('IsRelease');
-- Is no yet set at excution time , so it is hard coded
-- USE_OHXACT_RANGE         := GetParameter('USE_OHXACT_RANGE');
  USE_OHXACT_RANGE         := 'Y';


  str := 'INSERT /*+ APPEND */ INTO dbx_cashdetail ';
  str := str || '(CADXACT,CADOXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CASHDETAIL2_HINT ;
  str := str || ' */ CADXACT,CADOXACT FROM CASHDETAIL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CADOXACT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR )';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str := str || ' AND   CADOXACT ';
     str := str || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  IF ordhdrentdate IS NULL OR (ordhdrentdate IS NOT NULL AND ordhdrentdate != '-1') THEN
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CASHRECEIPTS'
                  ,piosActionName => 'FillTableDBXCash1'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCash (DBX_CASHDETAIL_1): Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CASHRECEIPTS'
                  ,piosActionName => 'FillTableDBXCash1'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  END IF;

  select username
    into los_username
  from user_users;

  GatherDBXTableStat ( piosSchema    => los_username
                   ,piosTablename => 'DBX_CASHDETAIL');

  str := 'INSERT /*+ APPEND */ INTO dbx_cashreceipts ';
  str := str || '(CUSTOMER_ID,CAXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || CASHRECEIPTS_ALL_HINT ;
  str := str || ' */ CUSTOMER_ID,CAXACT FROM CASHRECEIPTS_ALL';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CAXACT IN (SELECT CADXACT FROM dbx_cashdetail';
  str := str || ' ) ';

  IF ordhdrentdate IS NULL OR (ordhdrentdate IS NOT NULL AND ordhdrentdate != '-1') THEN
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CASHRECEIPTS'
                  ,piosActionName => 'FillTableDBXCash2'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;
     callinfo('FillTableDBXCash (DBX_CASHRECEIPTS): Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_CASHRECEIPTS'
                  ,piosActionName => 'FillTableDBXCash2'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
  END IF;

  str := 'SELECT nvl(min(CAXACT),0), nvl(max(caxact),0) FROM dbx_cashreceipts ';
  ShowStatement(str);
  execute immediate str into minval,maxval;

  SetParameter('CAXACT_MIN',TO_CHAR(minval) );
  SetParameter('CAXACT_MAX',TO_CHAR(maxval) );

  str := 'SELECT nvl(min(CADXACT),0), nvl(max(cadxact),0) FROM dbx_cashdetail ';
  ShowStatement(str);
  execute immediate str into minval,maxval;

  SetParameter('CADXACT_MIN',TO_CHAR(minval) );
  SetParameter('CADXACT_MAX',TO_CHAR(maxval) );


 stop_time := GetTime;
 callinfo('FillTableDBXCash: Start:' || start_time );
 callinfo('FillTableDBXCash: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXCash: Error:');
  CallError(SQLERRM);
END FillTableDBXCash;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXParameterValue
   **
   ** Purpose:   Fille Table DBX_PARAMETER_VALUE
   **
   **=====================================================
*/


PROCEDURE FillTableDBXParameterValue
AS
 vdbBSCS                        VARCHAR2(4000);
 vialink                        VARCHAR2(1);
 str                            VARCHAR2(32000);
 GLOBAL_HINT                    VARCHAR2(4000);
 GLOBAL_SUB_HINT                VARCHAR2(4000);
 CONTRACT_ID_MIN                VARCHAR2(4000);
 CONTRACT_ID_MAX                VARCHAR2(4000);
 PRM_VALUE_ID_MIN               VARCHAR2(4000);
 PRM_VALUE_ID_MAX               VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE          VARCHAR2(4000);
 PRMVALUE_PRMVAL5A_HINT         VARCHAR2(4000);
 PRMVALUE_PRMVAL5B_HINT         VARCHAR2(4000);
 EXIST_PARAMETER_VALUE_COID_IDX VARCHAR2(4000);
 vIsRelease                     VARCHAR2(4000);
 start_time                     VARCHAR2(30);
 stop_time                      VARCHAR2(30);
 los_username                   VARCHAR2(30);
BEGIN

  start_time                     := GetTime;
  vdbBSCS                        := GetParameter('vdbBSCS');
  vialink                        := GetParameter('vialink');
  GLOBAL_HINT                    := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT                := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN                := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX                := GetParameter('CONTRACT_ID_MAX');
  USE_CONTRACT_ID_RANGE          := GetParameter('USE_CONTRACT_ID_RANGE');
  PRMVALUE_PRMVAL5A_HINT         := GetParameter('PRMVALUE_PRMVAL5A_HINT');
  PRMVALUE_PRMVAL5B_HINT         := GetParameter('PRMVALUE_PRMVAL5B_HINT');
  EXIST_PARAMETER_VALUE_COID_IDX := GetParameter('EXIST_PARAMETER_VALUE_COID_IDX');
  vIsRelease                     := GetParameter('IsRelease');

  select username
    into los_username
    from user_users;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_1 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(PS) */ ';
  end if;
  str := str || ' DISTINCT PS.PRM_VALUE_ID FROM PROFILE_SERVICE';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' PS ';
  str := str || ' WHERE PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   PS.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   PS.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_2 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(CSBSG) */ ';
  end if;
  str := str || ' DISTINCT CSBSG.BSG_PRM_VALUE_ID FROM CONTR_SERVICES_BSG';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CSBSG ';
  str := str || ' WHERE CSBSG.BSG_PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   CSBSG.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CSBSG.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_3 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(CSCUG) */ ';
  end if;
  str := str || ' DISTINCT CSCUG.INTRA_CUG_PRM_VALUE_ID FROM CONTR_SERVICES_CUG';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CSCUG ';
  str := str || ' WHERE CSCUG.INTRA_CUG_PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   CSCUG.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CSCUG.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;


  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_4 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(P) */ ';
  end if;
  str := str || ' DISTINCT P.PRM_VALUE_ID FROM PORT';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' P ';
  str := str || ' WHERE P.PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   P.PORT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM DBX_PORT )';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue4'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue4'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  if TO_NUMBER(vIsRelease) >= 10 then
     str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_5 ';
     str := str || '(PRM_VALUE_ID ) ';
     str := str || 'SELECT ';
     if vialink = 'Y' then
        str := str ||  ' /*+ DRIVING_SITE(PV) ' || PRMVALUE_PRMVAL5A_HINT || ' */ ';
     else
        str := str ||  ' /*+ ' || PRMVALUE_PRMVAL5A_HINT || ' */ ';
     end if;
     str := str || ' PRM_VALUE_ID FROM PARAMETER_VALUE';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' PV ';
     str := str || ' MINUS ';
     str := str || ' SELECT ';
     if vialink = 'Y' then
        str := str ||  ' /*+ DRIVING_SITE(PS) ' || PRMVALUE_PRMVAL5B_HINT || ' */ ';
     else
        str := str ||  ' /*+ ' || PRMVALUE_PRMVAL5B_HINT || ' */ ';
     end if;
     str := str || ' PRM_VALUE_ID FROM PROFILE_SERVICE';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' PS ';
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
                  ,piosActionName => 'FillTableDBXParameterValue5'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;

     callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
                  ,piosActionName => 'FillTableDBXParameterValue5'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
     GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_5');
  end if;
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_1');
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_2');
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_3');
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_4');


  if EXIST_PARAMETER_VALUE_COID_IDX = 'Y' then
     str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_6 ';
     str := str || '(PRM_VALUE_ID ) ';
     str := str || 'SELECT ';
     if vialink = 'Y' then
        str := str ||  ' /*+ DRIVING_SITE(P)  */ ';
     else
        str := str ||  ' /*+  */ ';
     end if;
     str := str || ' DISTINCT PRM_VALUE_ID FROM PARAMETER_VALUE';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' P ';
     str := str || ' WHERE P.CO_ID IS NOT NULL ';
     str := str || ' AND   P.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT )';
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
                  ,piosActionName => 'FillTableDBXParameterValue6'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str;

     callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
     DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
                  ,piosActionName => 'FillTableDBXParameterValue6'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;
     GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_6');
  end if;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_1 ';
  str := str || 'UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_2 ';
  str := str || 'UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_3 ';
  str := str || 'UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_4 ';
  str := str || 'UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_5 ';
  str := str || 'UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_6 ';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue7'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue7'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE');

  stop_time := GetTime;
  callinfo('FillTableDBXParameterValue: Start:' || start_time );
  callinfo('FillTableDBXParameterValue: Stop: ' || stop_time );

  str := 'select to_char(nvl(min(PRM_VALUE_ID),0)),TO_CHAR(nvl(max(PRM_VALUE_ID),0)) from dbx_Parameter_Value';
  execute immediate str into PRM_VALUE_ID_MIN,PRM_VALUE_ID_Max;

  SetParameter('PRM_VALUE_ID_MIN',PRM_VALUE_ID_MIN);
  SetParameter('PRM_VALUE_ID_MAX',PRM_VALUE_ID_MAX);
EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXParameterValue: Error:');
  CallError(SQLERRM);
END FillTableDBXParameterValue;



 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXParameterValue6
   **
   ** Purpose:   Fille Table DBX_PARAMETER_VALUE
   **
   **=====================================================
*/


PROCEDURE FillTableDBXParameterValue6
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 PRM_VALUE_ID_MIN          VARCHAR2(4000);
 PRM_VALUE_ID_MAX          VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 los_username              VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

  select username
    into los_username
    from user_users;

  -- Different
  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_1 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(CS) */ ';
  end if;
  str := str || ' DISTINCT CS.PRM_VALUE_ID FROM CONTR_SERVICES';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CS ';
  str := str || ' WHERE CS.PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   CS.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CS.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  str := str || ' UNION ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(CSBSG) */ ';
  end if;
  str := str || ' DISTINCT CSBSG.BSG_PRM_VALUE_ID FROM CONTR_SERVICES_BSG';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CSBSG ';
  str := str || ' WHERE CSBSG.BSG_PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   CSBSG.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CSBSG.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  str := str || ' UNION ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(CSCUG) */ ';
  end if;
  str := str || ' DISTINCT CSCUG.INTRA_CUG_PRM_VALUE_ID FROM CONTR_SERVICES_CUG';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' CSCUG ';
  str := str || ' WHERE CSCUG.INTRA_CUG_PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   CSCUG.CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT ';
  str := str ||                 ' WHERE CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX || ')';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CSCUG.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;
  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_1');
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value_2 ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT ';
  if vialink = 'Y' then
     str := str ||  ' /*+ DRIVING_SITE(P) */ ';
  end if;
  str := str || ' DISTINCT PRM_VALUE_ID FROM PORT';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' P ';
  str := str || ' WHERE P.PRM_VALUE_ID IS NOT NULL ';
  str := str || ' AND   P.PORT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PORT_ID FROM DBX_PORT )';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;
  GatherDBXTableStat ( piosSchema    => los_username ,piosTablename => 'DBX_PARAMETER_VALUE_2');
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_Parameter_Value ';
  str := str || '(PRM_VALUE_ID ) ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_1 ';
  str := str || ' UNION ';
  str := str || 'SELECT PRM_VALUE_ID FROM dbx_Parameter_Value_2';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_3'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXParameterValue: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_PARAMETER_VALUE'
               ,piosActionName => 'FillTableDBXParameterValue6_3'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXParameterValue6: Start:' || start_time );
  callinfo('FillTableDBXParameterValue6: Stop: ' || stop_time );

  str := 'select to_char(nvl(min(PRM_VALUE_ID),0)),TO_CHAR(nvl(max(PRM_VALUE_ID),0)) from dbx_Parameter_Value';
  execute immediate str into PRM_VALUE_ID_MIN,PRM_VALUE_ID_Max;

  SetParameter('PRM_VALUE_ID_MIN',PRM_VALUE_ID_MIN);
  SetParameter('PRM_VALUE_ID_MAX',PRM_VALUE_ID_MAX);
EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXParameterValue6: Error:');
  CallError(SQLERRM);
END FillTableDBXParameterValue6;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_ParaValue
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_ParaValue
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                        VARCHAR2(4000);
 vdbconBSCS                     VARCHAR2(4000);
 vdbDBXHOME                     VARCHAR2(4000);
 viadbxlink                     VARCHAR2(1);
 GLOBAL_HINT                    VARCHAR2(4000);
 GLOBAL_SUB_HINT                VARCHAR2(4000);
 str_create                     VARCHAR2(30000);
 str_where                      VARCHAR2(30000);
 str_where_link                 VARCHAR2(30000);
 str_storage                    VARCHAR2(30000) := ' ';
 str_hint                       VARCHAR2(4000) := ' ';
 vuserDBXHOME                   VARCHAR2(60) := ' ';
 EXIST_PARAMETER_VALUE_COID_IDX VARCHAR2(4000);
BEGIN

  vdbBSCS                        := GetParameter('vdbBSCS');
  vdbconBSCS                     := GetParameter('vdbconBSCS');
  vdbDBXHOME                     := GetParameter('vdbDBXHOME');
  viadbxlink                     := GetParameter('viadbxlink');

  GLOBAL_HINT                    := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT                := GetParameter('GLOBAL_SUB_HINT');
  str_hint                       := GetParameter(piosHint);
  vuserDBXHOME                   := GetParameter('vuserDBXHOME');
  EXIST_PARAMETER_VALUE_COID_IDX := GetParameter('EXIST_PARAMETER_VALUE_COID_IDX');

IF piosTableName = 'PARAMETER_VALUE' AND EXIST_PARAMETER_VALUE_COID_IDX = 'Y' THEN
  CallInfo('No action for ' || piosTableName || ' because an index on column CO_ID exists.');
ELSE
  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PRM_VALUE_ID FROM ' || vuserDBXHOME || '.DBX_PARAMETER_VALUE';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PRM_VALUE_ID FROM ' || vuserDBXHOME || '.DBX_PARAMETER_VALUE )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PRM_VALUE_ID FROM DBX_PARAMETER_VALUE@' || vdbDBXHOME || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_ParaValue: Error:');
  CallError(SQLERRM);
END SetDBX_Where_ParaValue;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashrcpt
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashrcpt
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  ordhdrentdate            := GetParameter('ordhdrentdate');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM ' || vuserDBXHOME || '.DBX_CASHRECEIPTS';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM ' || vuserDBXHOME || '.DBX_CASHRECEIPTS )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM DBX_CASHRECEIPTS@' || vdbDBXHOME || ' )';

  IF ordhdrentdate IS NOT NULL AND ordhdrentdate = '-1' THEN
     Exclude_Tab ( piosTableName => piosTableName ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
  ELSE
     SetDBXWhereClause( piosTableName => piosTableName
                       ,piosColumnName => piosColumnName
                       ,piosCreateClause => str_create
                       ,piosWhereClause => str_where
                       ,piosWhereLinkClause => str_where_link
                       ,piosHint => str_hint
                       ,piosStorage => str_storage
                       ,piosDBConnect => vdbconBSCS );

     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
  END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Cashrcpt: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Cashrcpt;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashdetail
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashdetail
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  ordhdrentdate            := GetParameter('ordhdrentdate');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CADXACT,CADOXACT FROM ' || vuserDBXHOME || '.DBX_CASHDETAIL';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CADXACT,CADOXACT FROM ' || vuserDBXHOME || '.DBX_CASHDETAIL )';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CADXACT,CADOXACT FROM DBX_CASHDETAIL@' || vdbDBXHOME || ' )';

  IF ordhdrentdate IS NOT NULL AND ordhdrentdate = '-1' THEN
     Exclude_Tab ( piosTableName => piosTableName ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
  ELSE
     SetDBXWhereClause( piosTableName => piosTableName
                       ,piosColumnName => piosColumnName
                       ,piosCreateClause => str_create
                       ,piosWhereClause => str_where
                       ,piosWhereLinkClause => str_where_link
                       ,piosHint => str_hint
                       ,piosStorage => str_storage
                       ,piosDBConnect => vdbconBSCS );

     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
  END IF;
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Cashdetail: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Cashdetail;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Cashtrl
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Cashtrl
(
     -- Table Name
     piosTableName      IN VARCHAR2
     -- Column Name
    ,piosColumnName     IN VARCHAR2  DEFAULT NULL
     -- Column NameCash
    ,piosColumnNameCash IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint           IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 USE_OHXACT_RANGE          VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 OHXACT_MIN                INTEGER;
 OHXACT_MAX                INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  USE_OHXACT_RANGE         := GetParameter('USE_OHXACT_RANGE');
  ordhdrentdate            := GetParameter('ordhdrentdate');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnNameCash || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM ' || vuserDBXHOME || '.DBX_CASHRECEIPTS';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';
  str_create := str_create || ' AND   ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str_create := str_create || ' AND ' || piosColumnName;
     str_create := str_create || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  str_where := piosColumnNameCash || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM ' || vuserDBXHOME || '.DBX_CASHRECEIPTS )';
  str_where := str_where || ' AND ' || piosColumnName;
  str_where := str_where || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR) ';
  IF USE_OHXACT_RANGE = 'Y' THEN
     str_where := str_where || ' AND ' || piosColumnName;
     str_where := str_where || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  str_where_link := piosColumnNameCash || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CAXACT FROM DBX_CASHRECEIPTS@' || vdbDBXHOME  || ' )';
  str_where_link := str_where_link || ' AND ' || piosColumnName ;
  str_where_link := str_where_link || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR@' || vdbDBXHOME  || ' )';
  IF USE_OHXACT_RANGE = 'Y' THEN
  str_where_link := str_where_link || ' AND ' || piosColumnName ;
  str_where_link := str_where_link || ' BETWEEN ' || OHXACT_MIN || ' AND ' || OHXACT_MAX ;
  END IF;

  IF ordhdrentdate IS NOT NULL AND ordhdrentdate = '-1' THEN
     Exclude_Tab ( piosTableName => piosTableName ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
  ELSE
     SetDBXWhereClause( piosTableName => piosTableName
                       ,piosColumnName => piosColumnName
                       ,piosCreateClause => str_create
                       ,piosWhereClause => str_where
                       ,piosWhereLinkClause => str_where_link
                       ,piosHint => str_hint
                       ,piosStorage => str_storage
                       ,piosDBConnect => vdbconBSCS );
     CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
  END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Cashtrl: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Cashtrl;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Equip
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Equip
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CONTRACT_ID_MIN           INTEGER;
 CONTRACT_ID_MAX           INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT EQ_ID FROM CONTR_DEVICES';
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT';
  if viadbxlink = 'Y' then
     str_create := str_create || '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' WHERE CO_ID ' ;
  str_create := str_create || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  str_create := str_create || ' )) ';

  str_where := piosColumnName || ' IN (SELECT EQ_ID FROM CONTR_DEVICES';
  str_where := str_where || ' WHERE CO_ID IN (SELECT /*+ '|| GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT ';
  str_where := str_where || ' WHERE CO_ID ';
  str_where := str_where || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  str_where := str_where || ' )) ';

  str_where_link := piosColumnName || ' IN (SELECT EQ_ID FROM CONTR_DEVICES';
  str_where_link := str_where_link || ' WHERE CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT@' || vdbDBXHOME  ;
  str_where_link := str_where_link || ' WHERE CO_ID ';
  str_where_link := str_where_link || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
  str_where_link := str_where_link || ' ))';


  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Equip: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Equip;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_UserAccess
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_UserAccess
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 SY_ADMIN_USER             VARCHAR2(4000) := ' ';
 USR_SRCH_PATTERN          VARCHAR2(4000) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  str_hint                 := GetParameter(piosHint);
  SY_ADMIN_USER            := GetParameter('SY_ADMIN_USER');
  USR_SRCH_PATTERN         := GetParameter('USR_SRCH_PATTERN');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' LIKE ' || '''' || SY_ADMIN_USER || '''' || ' ';
  if  USR_SRCH_PATTERN is not null and  length(USR_SRCH_PATTERN) > 0 then
     str_create := str_create || ' OR    ' || piosColumnName || ' LIKE ' || '''' || USR_SRCH_PATTERN || '''' || ' ';
  end if;
  str_create := str_create || ' OR    ' || piosColumnName || ' LIKE ' || '''' || 'SOICS' || '''' || ' ';

  str_where := piosColumnName ||  ' LIKE ' || '''' || SY_ADMIN_USER || '''' || ' ';
  if  USR_SRCH_PATTERN is not null and  length(USR_SRCH_PATTERN) > 0 then
     str_where := str_where || ' OR ' || piosColumnName || ' LIKE ' || '''' || USR_SRCH_PATTERN || '''' || ' ';
  end if;
  str_where := str_where || ' OR ' || piosColumnName || ' LIKE ' || '''' || 'SOICS' || '''' || ' ';

  str_where_link := piosColumnName ||   ' LIKE ' || '''' || SY_ADMIN_USER || '''' || ' ';
  if  USR_SRCH_PATTERN is not null and  length(USR_SRCH_PATTERN) > 0 then
     str_where_link := str_where_link || ' OR ' || piosColumnName ||' LIKE '|| '''' || USR_SRCH_PATTERN || '''' || ' ';
  end if;
  str_where_link := str_where_link || ' OR ' || piosColumnName ||' LIKE '|| '''' || 'SOICS' || '''' || ' ';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_UserAccess: Error:');
  CallError(SQLERRM);
END SetDBX_Where_UserAccess;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXMpurhtab
   **
   ** Purpose:   Fille Table DBX_MPURHTAB
   **
   **=====================================================
*/


PROCEDURE FillTableDBXMpurhtab
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 MPURHTAB_HINT             VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  MPURHTAB_HINT            := GetParameter('MPURHTAB_HINT');

  str := 'INSERT /*+  APPEND */ INTO dbx_mpurhtab ';
  str := str || '(HPLCODE,VPLCODE,FLSQN ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || MPURHTAB_HINT || ' */ ';
  str := str || ' HPLCODE,VPLCODE,MAX(FLSQN) FROM MPURHTAB';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' GROUP BY HPLCODE,VPLCODE';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_MPURHTAB'
               ,piosActionName => 'FillTableDBXMpurhtab'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXMpurhtab: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_MPURHTAB'
               ,piosActionName => 'FillTableDBXMpurhtab'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXMpurhtab: Start:' || start_time );
  callinfo('FillTableDBXMpurhtab: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXMpurhtab: Error:');
  CallError(SQLERRM);
END FillTableDBXMpurhtab;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXMpurhtab6
   **
   ** Purpose:   Fille Table DBX_MPURHTAB
   **
   **=====================================================
*/


PROCEDURE FillTableDBXMpurhtab6
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 MPURHTAB_HINT             VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  MPURHTAB_HINT            := GetParameter('MPURHTAB_HINT');

  str := 'INSERT /*+ APPEND */ INTO dbx_mpurhtab ';
  str := str || '(HPLCODE,VPLCODE,FLSQN ) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || MPURHTAB_HINT || ' */ ';
  str := str || ' 1,PLCODE,MAX(FLSQN) FROM MPURHTAB';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' GROUP BY PLCODE';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_MPURHTAB'
               ,piosActionName => 'FillTableDBXMpurhtab6'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXMpurhtab6: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_MPURHTAB'
               ,piosActionName => 'FillTableDBXMpurhtab6'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXMpurhtab6: Start:' || start_time );
  callinfo('FillTableDBXMpurhtab6: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXMpurhtab6: Error:');
  CallError(SQLERRM);
END FillTableDBXMpurhtab6;





 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Mpurhtab
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Mpurhtab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 SY_ADMIN_USER             VARCHAR2(4000) := ' ';
 USR_SRCH_PATTERN          VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  str_hint                 := GetParameter(piosHint);
  SY_ADMIN_USER            := GetParameter('SY_ADMIN_USER');
  USR_SRCH_PATTERN         := GetParameter('USR_SRCH_PATTERN');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN ( SELECT HPLCODE,VPLCODE,FLSQN FROM ' || vuserDBXHOME || '.DBX_MPURHTAB';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create ||  ')';

  str_where := piosColumnName ||   ' IN ( SELECT HPLCODE,VPLCODE,FLSQN FROM ' || vuserDBXHOME || '.DBX_MPURHTAB)';

  str_where_link := piosColumnName || ' IN ( SELECT HPLCODE,VPLCODE,FLSQN FROM DBX_MPURHTAB@'  || vdbDBXHOME || ')' ;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Mpurhtab: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Mpurhtab;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Mpurhtab6
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Mpurhtab6
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 SY_ADMIN_USER             VARCHAR2(4000) := ' ';
 USR_SRCH_PATTERN          VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  str_hint                 := GetParameter(piosHint);
  SY_ADMIN_USER            := GetParameter('SY_ADMIN_USER');
  USR_SRCH_PATTERN         := GetParameter('USR_SRCH_PATTERN');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN ( SELECT VPLCODE,FLSQN FROM ' || vuserDBXHOME || '.DBX_MPURHTAB';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create ||  ')';

  str_where := piosColumnName ||   ' IN ( SELECT VPLCODE,FLSQN FROM ' || vuserDBXHOME || '.DBX_MPURHTAB)';

  str_where_link := piosColumnName || ' IN ( SELECT VPLCODE,FLSQN FROM DBX_MPURHTAB@'  || vdbDBXHOME || ')' ;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Mpurhtab6: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Mpurhtab6;



 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Date
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Date
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Column Value
    ,piosColumnValue IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 str_column_value          VARCHAR2(4000) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  str_hint                 := GetParameter(piosHint);
  str_column_value         := GetParameter(piosColumnValue);

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' > sysdate - ' || str_column_value ;

  str_where := piosColumnName ||   ' > sysdate - ' || str_column_value ;

  str_where_link := piosColumnName ||  ' > sysdate - ' || str_column_value ;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Date: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Date;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXDocument
   **
   ** Purpose:   Fille Table DBX_DOCUMENT
   **
   **=====================================================
*/


PROCEDURE FillTableDBXDocument
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 DOCUMENT_REFERENCE_HINT   VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 EXIST_DOCUMENT_REFERENCE  VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  DOCUMENT_REFERENCE_HINT  := GetParameter('DOCUMENT_REFERENCE_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CUSTOMER_ID_RANGE');
  EXIST_DOCUMENT_REFERENCE := GetParameter('EXIST_DOCUMENT_REFERENCE');

 IF EXIST_DOCUMENT_REFERENCE = 'Y' THEN

  str := 'INSERT /*+ APPEND */ INTO dbx_document_1 ';
  str := str || '(CUSTOMER_ID,DOCUMENT_ID,OHXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || DOCUMENT_REFERENCE_HINT || ' */ ';
  str := str || ' CUSTOMER_ID,DOCUMENT_ID,OHXACT FROM DOCUMENT_REFERENCE';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' WHERE CUSTOMER_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER )';
  IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
     str := str || ' AND   CUSTOMER_ID ';
     str := str || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_DOCUMENT'
               ,piosActionName => 'FillTableDBXDocument1'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXDocument: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_DOCUMENT'
               ,piosActionName => 'FillTableDBXDocument1'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  str := 'INSERT /*+ APPEND */ INTO dbx_document ';
  str := str || '(CUSTOMER_ID,DOCUMENT_ID,OHXACT) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' */ CUSTOMER_ID,DOCUMENT_ID,OHXACT FROM dbx_document_1 ';
  str := str || ' WHERE OHXACT IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR )';

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_DOCUMENT'
               ,piosActionName => 'FillTableDBXDocument2'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXDocument: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_DOCUMENT'
               ,piosActionName => 'FillTableDBXDocument2'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXDocument: Start:' || start_time );
  callinfo('FillTableDBXDocument: Stop: ' || stop_time );
 end if;

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXDocument: Error:');
  CallError(SQLERRM);
END FillTableDBXDocument;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Document
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_Document
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName ;
  str_create := str_create ||  '@' || vdbBSCS;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN ( SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM ' || vuserDBXHOME || '.DBX_DOCUMENT';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ')';

  str_where := piosColumnName ||   '  IN ( SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM ' || vuserDBXHOME || '.DBX_DOCUMENT)';

  str_where_link := piosColumnName || ' IN ( SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM DBX_DOCUMENT@'  || vdbDBXHOME || ')' ;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Document: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Document;


 /*
   **=====================================================
   **
   ** Procedure: Include_Any_Tab
   **
   ** Purpose:   Include Tables to be copied completely
   **            piosOverwrite = Y , Includes also tables, which are already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Include_Any_Tab
(
     -- Table Name
     piosTableName IN VARCHAR2
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_object_owner            VARCHAR2(30);
 v_object_name             VARCHAR2(30);
 v_object_type             VARCHAR2(30);
 v_status                  VARCHAR2(30);

BEGIN

 IF piosTableName IS NOT NULL AND LENGTH(RTRIM(piosTableName)) > 1 THEN
  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');

  v_select_statement := 'SELECT o.owner,o.object_name,o.object_type,o.status' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_objects';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' o ';
  v_select_statement := v_select_statement || ' WHERE (o.object_type =    ' ||''''|| 'TABLE' || '''' ;
  v_select_statement := v_select_statement || ') AND (o.owner = ' || '''' || 'SYSADM' || '''' || ')' ;
-- Problem with 8.1.7 DBLINK + SUB-Select + ESCAPE
-- v_select_statement := v_select_statement || ' AND (object_name  NOT IN (';
-- v_select_statement := v_select_statement || ' SELECT dbx_table_name FROM dbx_tables))';
  v_select_statement := v_select_statement || ' AND (object_name  like ';
  v_select_statement := v_select_statement || '''' || upper(rtrim(piosTableName)) || '''';
  v_select_statement := v_select_statement || ' ESCAPE ' || '''' || '\' || '''' || ') ' ;
  v_select_statement := v_select_statement || ' ORDER BY o.owner,o.object_name DESC';

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_object_owner, v_object_name , v_object_type , v_status ;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_object_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_object_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_object_name || '@' || vdbBSCS ;

      CallInfo('Include ' || v_object_name );
      SetDBXWhereClause( piosTableName => v_object_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => NULL
                        ,piosWhereLinkClause => NULL
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosOverwrite => piosOverwrite); -- N = Do not update, when entry already exists

  END LOOP;
  CLOSE cur;

 END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('Include_Any_Tab: Error:');
  CallError(SQLERRM);
END Include_Any_Tab;



 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Without
   **
   ** Purpose:   Exclude Tables from being copied, which do not have column comment or index or constraint
   **            Additionally exclude tables without grants to BSCS_ROLE
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Without
(
     piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_table_name              VARCHAR2(30);
 str                       VARCHAR2(30000);
 counter                   INTEGER;
 USE_ORACLE_DATAPUMP       VARCHAR2(4000);
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  USE_ORACLE_DATAPUMP      := GetParameter('USE_ORACLE_DATAPUMP');

  CallInfo('Verify Comments, Indexes, Constraints.');
  v_select_statement := 'SELECT table_name ' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' WHERE (owner = ' || '''' || 'SYSADM' || '''' || ')' ;
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS%' || '''' || ') ';
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || '%$%' || '''' || ') ';
  v_select_statement := v_select_statement || '  MINUS (';
  v_select_statement := v_select_statement || '  SELECT table_name  FROM sys.dba_tab_comments';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '  WHERE owner = ' || '''' || 'SYSADM' || '''' || ' AND comments IS NOT NULL group by table_name ';
  v_select_statement := v_select_statement || '  UNION ';
  v_select_statement := v_select_statement || '  SELECT table_name  FROM sys.dba_indexes';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '  WHERE table_owner = ' || '''' || 'SYSADM' || '''' || ' group by table_name ';
  v_select_statement := v_select_statement || '  UNION ';
  v_select_statement := v_select_statement || '  SELECT table_name  FROM sys.dba_constraints';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '  WHERE owner = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || '  AND constraint_name NOT LIKE ' || '''' || 'SYS_%' || '''';
  v_select_statement := v_select_statement || '  AND constraint_type NOT IN (' || '''' || 'C' || '''' || ')';
  v_select_statement := v_select_statement || '  group by table_name ';
  v_select_statement := v_select_statement || '  ) ';
  v_select_statement := v_select_statement || ' ORDER BY table_name ';

  ShowStatement(v_select_statement);

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_table_name || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE 1 = 2 ';

--    CallInfo('Exclude ' || v_table_name);
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => '1 = 2'
                        ,piosWhereLinkClause => ' 1 = 2 '
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosExclude   => 'Y'
                        ,piosOverwrite => piosOverwrite); -- N = Do not update, when entry already exists
  END LOOP;
  CLOSE cur;

 /*
  * BSCS_ROLE
  */
  CallInfo('Verify BSCS_ROLE.');

  v_select_statement := 'SELECT table_name ' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' WHERE (owner = ' || '''' || 'SYSADM' || '''' || ')' ;
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS%' || '''' || ') ';
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || '%$%' || '''' || ') ';
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'DBX_%' || '''' || ') ';
  v_select_statement := v_select_statement || '  AND  (table_name  NOT IN (' || '''' || 'BILLING_INVOICE_ITEM_CLASS' || '''' ||
                                                                         ',' || '''' || 'BILLING_CHARGE_TYPE' || '''' ||
                                                                         ',' || '''' || 'UDC_RTX_CHARGE_TYPE' || '''' || '))';
  v_select_statement := v_select_statement || '  MINUS ';
  v_select_statement := v_select_statement || '  SELECT table_name  FROM sys.DBA_TAB_PRIVS';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '  WHERE OWNER = ' || '''' || 'SYSADM' || '''' ||
                                              '  AND   GRANTOR = ' || '''' || 'SYSADM' || '''' ||
                                              '  AND   GRANTEE IN (' || '''' || 'BSCS_ROLE'  || '''' ||
                                                                 ',' || '''' || 'JAVASERVER' || '''' || ') ' ||
                                              '  AND   PRIVILEGE = ' || '''' || 'SELECT' || '''' ;
  v_select_statement := v_select_statement || ' ORDER BY table_name ';

  ShowStatement(v_select_statement);

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_table_name || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE 1 = 2 ';

--    CallInfo('Exclude ' || v_table_name);
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => '1 = 2'
                        ,piosWhereLinkClause => ' 1 = 2 '
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosExclude   => 'Y'
                        ,piosOverwrite => piosOverwrite); -- N = Do not update, when entry already exists
  END LOOP;
  CLOSE cur;

 /*
  * Tables in DBX_TABLES do exist in BSCS DB
  */
  CallInfo('Verify tables in DBX_TABLES for BSCSDB, which do not exist in BSCS DB.');

  v_select_statement := 'SELECT dbx_table_name table_name FROM dbx_tables';
  v_select_statement := v_select_statement || '  WHERE  dbx_db_connect = ' || '''' || vdbconBSCS || '''';
  v_select_statement := v_select_statement || '  AND    dbx_table_name  NOT LIKE ' || '''' || 'DBX_%' || '''' || ' ';
  v_select_statement := v_select_statement || '  AND    dbx_exclude     =  ' || '''' || 'N' || '''' || ' ';
  v_select_statement := v_select_statement || '  AND    dbx_schema_name   =  ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || '  MINUS ';
  v_select_statement := v_select_statement || 'SELECT table_name ' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' WHERE (owner = ' || '''' || 'SYSADM' || '''' || ')' ;
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS%' || '''' || ') ';
  v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'DBX_%' || '''' || ') ';
  v_select_statement := v_select_statement || ' ORDER BY table_name ';

  ShowStatement(v_select_statement);

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_table_name || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE 1 = 2 ';

--    CallInfo('Exclude ' || v_table_name);
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => '1 = 2'
                        ,piosWhereLinkClause => ' 1 = 2 '
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosExclude   => 'Y'
                        ,piosOverwrite => 'Y'); -- N = Do not update, when entry already exists
  END LOOP;
  CLOSE cur;


EXCEPTION WHEN OTHERS THEN
  callinfo('Exclude_Tab_Without: Error:');
  CallError(SQLERRM);
END Exclude_Tab_Without;


 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Without_Seg
   **
   ** Purpose:   Exclude Tables from being copied, which are empty
   **            and do not have a segment. This is new with Oracle 11.2 .
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **            Excludes only tables, which are not already configured in dbx_tables
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Without_Seg
(
     piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 counterdbxtables          INTEGER := 0;
 work                      INTEGER := 0;
 vdb_i                     INTEGER;
 vdb                       VARCHAR2(30);
 vdbBSCS                   VARCHAR2(4000);
 vdbUDR                    VARCHAR2(4000);
 vdbBILL                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbconUDR                 VARCHAR2(4000);
 vdbconBILL                VARCHAR2(4000);
 vdbcon                    VARCHAR2(4000);

 vialink                   VARCHAR2(1);
 viaUDRlink                VARCHAR2(1);
 viaBILLlink               VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_table_name              VARCHAR2(30);
 v_part_name               VARCHAR2(30);
 str                       VARCHAR2(30000);
 counter                   INTEGER;
 USE_ORACLE_DATAPUMP       VARCHAR2(4000);
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbUDR                   := GetParameter('vdbUDR');
  vdbBILL                  := GetParameter('vdbBILL');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbconUDR                := GetParameter('vdbconUDR');
  vdbconBILL               := GetParameter('vdbconBILL');
  vialink                  := GetParameter('vialink');
  viaUDRlink               := GetParameter('viaUDRlink');
  viaBILLlink              := GetParameter('viaBILLlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  USE_ORACLE_DATAPUMP      := GetParameter('USE_ORACLE_DATAPUMP');

 FOR vdb_i IN 1..3
 LOOP
  work := 0;
  if vdb_i = 1 then
     vdb    := 'BSCS';
     vdbcon := vdbconBSCS;
  end if;
  if vdb_i = 2 then
     vdb    := 'UDR';
     vdbcon := vdbconUDR;
  end if;
  if vdb_i = 3 then
     vdb    := 'BILL';
     vdbcon := vdbconBILL;
  end if;
  -- Exit in case UDR or BILL is no seperate database
  if vdb_i > 1 and trim(upper(vdbcon)) = trim(upper(vdbconBSCS)) then
     work := 1;
  end if;
  -- Exit in case BILL is no seperate database
  if vdb_i > 2 and trim(upper(vdbcon)) = trim(upper(vdbconUDR)) then
     work := 1;
  end if;

  if work = 0 then
     callinfo('Exclude_Tab_Without_Seg: work on ' || vdb );
    /*
     * Oracle 11 which has tables without segments
     */
     str := 'select count(*) ';
     str := str || '  from dba_tab_columns';
     if vialink = 'Y' and vdb = 'BSCS' then
        str := str ||  '@' || vdbBSCS;
     end if;
     if viaUDRlink = 'Y' and vdb = 'UDR' then
        str := str ||  '@' || vdbUDR;
     end if;
     if viaBILLlink = 'Y' and vdb = 'BILL' then
        str := str ||  '@' || vdbBILL;
     end if;
     str := str || ' where OWNER = ''SYS'' ';
     str := str || '   and TABLE_NAME=''DBA_TABLES'' ';
     str := str || '   and column_name = ''SEGMENT_CREATED'' ';
     ShowStatement(str);
     execute immediate str into counter;

     if counter = 1 then
        callinfo('Exclude_Tab_Without_Seg: ' || vdb || ' DB is an Oracle 11.2 DB');
       /*
        * Verify Normal Tables without segments Oracle 11.2
        */
        CallInfo('Verify Normal Tables without segments Oracle 11.2');

        v_select_statement := 'SELECT table_name ' ;
        v_select_statement := v_select_statement || ' FROM sys.dba_tables';
        if vialink = 'Y' and vdb = 'BSCS' then
           v_select_statement := v_select_statement ||  '@' || vdbBSCS;
        end if;
        if viaUDRlink = 'Y' and vdb = 'UDR' then
           v_select_statement := v_select_statement ||  '@' || vdbUDR;
        end if;
        if viaBILLlink = 'Y' and vdb = 'BILL' then
           v_select_statement := v_select_statement ||  '@' || vdbBILL;
        end if;
        v_select_statement := v_select_statement || ' WHERE (owner = ' || '''' || 'SYSADM' || '''' || ')' ;
        v_select_statement := v_select_statement || '  AND  (SEGMENT_CREATED = ' || '''' || 'NO' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_EXP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IMP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IOT%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || '%$%' || '''' || ') ';
        v_select_statement := v_select_statement || ' ORDER BY table_name ';

        ShowStatement(v_select_statement);

        OPEN cur FOR v_select_statement ;
        LOOP
         FETCH cur INTO v_table_name;
         EXIT WHEN cur%NOTFOUND;

         counterdbxtables := 1;
         if  vdb = 'UDR' or vdb = 'BILL' then
             str := 'SELECT count(*) FROM dbx_tables WHERE dbx_db_connect = ' || '''' || vdbcon || '''' ||
                    ' AND dbx_table_name = ' || '''' || v_table_name || '''' ||
                    ' AND dbx_schema_name = ' || '''' || 'SYSADM' || '''';
             execute immediate str into counterdbxtables;
         end if;

         if counterdbxtables = 1 then
            if trim(USE_ORACLE_DATAPUMP) = '0' then
               -- Otherwise exp will ignore the table also in structure export
               str := 'begin ' || chr(10);
               str := str || '  dbx_allocate_extent';
               if vialink = 'Y' and vdb = 'BSCS' then
                   str := str  ||  '@' || vdbBSCS;
               end if;
               if viaUDRlink = 'Y' and vdb = 'UDR' then
                  str := str ||  '@' || vdbUDR;
               end if;
               if viaBILLlink = 'Y' and vdb = 'BILL' then
                  str := str ||  '@' || vdbBILL;
               end if;
               str := str || ' (v_table_name => ' || '''' || v_table_name || '''' || '); ' || chr(10);
               str := str || 'end;';
               -- ShowStatement(str);
               begin
                  DBXLogStart ( piosTableName  => v_table_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'ALLOCATE'
                               ,piosPara1      => str);
                  execute immediate str;
                  DBXLogStop  ( piosTableName  => v_table_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'SUCCESS');
               exception when others then
                  CallInfo(SQLERRM);  -- Only an Info
               end;
            end if;

            str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
            str_create := str_create || ' AS ';
            str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
            str_create := str_create || '  */ * FROM ' || v_table_name ;
            if vialink = 'Y' and vdb = 'BSCS' then
               str_create := str_create  ||  '@' || vdbBSCS;
            end if;
            if viaUDRlink = 'Y' and vdb = 'UDR' then
               str_create := str_create ||  '@' || vdbUDR;
            end if;
            if viaBILLlink = 'Y' and vdb = 'BILL' then
               str_create := str_create ||  '@' || vdbBILL;
            end if;
            str_create := str_create || ' WHERE 1 = 2 ';

--          CallInfo('Exclude ' || v_table_name);
            SetDBXWhereClause( piosTableName => v_table_name
                              ,piosColumnName => NULL
                              ,piosCreateClause => str_create
                              ,piosWhereClause => '1 = 2'
                              ,piosWhereLinkClause => ' 1 = 2 '
                              ,piosHint => ' '
                              ,piosStorage => ' '
                              ,piosDBConnect => vdbcon
                              ,piosExclude   => 'Y'
                              ,piosOverwrite => piosOverwrite); -- N = Do not update, when entry already exists
         else
            callinfo('No work on table ' || v_table_name );
         end if;
        END LOOP;
        CLOSE cur;


       /*
        * Verify Simple Partitioned Tables without segments Oracle 11.2
        */
        CallInfo('Verify Simple Partitioned Tables without segments Oracle 11.2');

        v_select_statement := 'SELECT table_name,partition_name ' ;
        v_select_statement := v_select_statement || ' FROM sys.dba_tab_partitions';
        if vialink = 'Y' and vdb = 'BSCS' then
           v_select_statement := v_select_statement ||  '@' || vdbBSCS;
        end if;
        if viaUDRlink = 'Y' and vdb = 'UDR' then
           v_select_statement := v_select_statement ||  '@' || vdbUDR;
        end if;
        if viaBILLlink = 'Y' and vdb = 'BILL' then
           v_select_statement := v_select_statement ||  '@' || vdbBILL;
        end if;
        v_select_statement := v_select_statement || ' WHERE (table_owner = ' || '''' || 'SYSADM' || '''' || ')' ;
        v_select_statement := v_select_statement || '  AND  (SEGMENT_CREATED NOT IN (' || '''' || 'YES' || '''' || ')) ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_EXP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IMP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IOT%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || '%$%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (subpartition_count = 0) ';
        v_select_statement := v_select_statement || ' ORDER BY table_name,partition_name ';

        ShowStatement(v_select_statement);

        OPEN cur FOR v_select_statement ;
        LOOP
         FETCH cur INTO v_table_name, v_part_name;
         EXIT WHEN cur%NOTFOUND;

         counterdbxtables := 1;
         if  vdb = 'UDR' or vdb = 'BILL' then
             str := 'SELECT count(*) FROM dbx_tables WHERE dbx_db_connect = ' || '''' || vdbcon || '''' ||
                    ' AND dbx_table_name = ' || '''' || v_table_name || ':' || v_part_name || '''' ||
                    ' AND dbx_schema_name = ' || '''' || 'SYSADM' || '''';
             execute immediate str into counterdbxtables;
         end if;

         if counterdbxtables = 1 then
            if trim(USE_ORACLE_DATAPUMP) = '0' then
               -- Otherwise exp will ignore the table also in structure export
               str := 'begin ' || chr(10);
               str := str || '  dbx_part_allocate_extent';
               if vialink = 'Y' and vdb = 'BSCS' then
                   str := str  ||  '@' || vdbBSCS;
               end if;
               if viaUDRlink = 'Y' and vdb = 'UDR' then
                  str := str ||  '@' || vdbUDR;
               end if;
               if viaBILLlink = 'Y' and vdb = 'BILL' then
                  str := str ||  '@' || vdbBILL;
               end if;
               str := str || ' (v_table_name => ' || '''' || v_table_name || '''' ||
                             '  ,v_part_name => ' || '''' || v_part_name || '''' || '); ' || chr(10);
               str := str || 'end;';
               -- ShowStatement(str);
               begin
                  DBXLogStart ( piosTableName  => v_table_name || '_' || v_part_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'ALLOCATE'
                               ,piosPara1      => str);
                  execute immediate str;
                  DBXLogStop  ( piosTableName  => v_table_name || '_' || v_part_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'SUCCESS');
               exception when others then
                  CallInfo(SQLERRM);  -- Only an Info
               end;
            end if;
            str_create := 'DELETE FROM dbx_part ';
            str_create := str_create || ' WHERE dbx_table_name = ' || '''' || v_table_name || '''';
            str_create := str_create || ' AND   dbx_partition_name = ' || '''' || v_part_name || '''' ||
                                        ' AND   dbx_schema_name = ' || '''' || 'SYSADM' || '''';
     --     ShowStatement(str_create);
            execute immediate str_create;
            CallInfo('Exclude ' || v_table_name || ':' || v_part_name );
            commit;
         else
            callinfo('No work on table ' || v_table_name );
         end if;
        END LOOP;
        CLOSE cur;

       /*
        * Verify SUB Partitioned Tables without segments Oracle 11.2
        */
        CallInfo('Verify Sub Partitioned Tables without segments Oracle 11.2');

        v_select_statement := 'SELECT table_name,subpartition_name ' ;
        v_select_statement := v_select_statement || ' FROM sys.dba_tab_subpartitions';
        if vialink = 'Y' and vdb = 'BSCS' then
           v_select_statement := v_select_statement ||  '@' || vdbBSCS;
        end if;
        if viaUDRlink = 'Y' and vdb = 'UDR' then
           v_select_statement := v_select_statement ||  '@' || vdbUDR;
        end if;
        if viaBILLlink = 'Y' and vdb = 'BILL' then
           v_select_statement := v_select_statement ||  '@' || vdbBILL;
        end if;
        v_select_statement := v_select_statement || ' WHERE (table_owner = ' || '''' || 'SYSADM' || '''' || ')' ;
        v_select_statement := v_select_statement || '  AND  (SEGMENT_CREATED = ' || '''' || 'NO' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_EXP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IMP%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || 'SYS_IOT%' || '''' || ') ';
        v_select_statement := v_select_statement || '  AND  (table_name  NOT LIKE ' || '''' || '%$%' || '''' || ') ';
        v_select_statement := v_select_statement || ' ORDER BY table_name,partition_name ';

        ShowStatement(v_select_statement);

        OPEN cur FOR v_select_statement ;
        LOOP
         FETCH cur INTO v_table_name, v_part_name;
         EXIT WHEN cur%NOTFOUND;

         counterdbxtables := 1;
         if  vdb = 'UDR' or vdb = 'BILL' then
             str := 'SELECT count(*) FROM dbx_tables WHERE dbx_db_connect = ' || '''' || vdbcon || '''' ||
                    ' AND dbx_table_name = ' || '''' || v_table_name || ':' || v_part_name || '''' ||
                    ' AND dbx_schema_name = ' || '''' || 'SYSADM' || '''';
             execute immediate str into counterdbxtables;
         end if;

         if counterdbxtables = 1 then
            if trim(USE_ORACLE_DATAPUMP) = '0' then
               -- Otherwise exp will ignore the table also in structure export
               str := 'begin ' || chr(10);
               str := str || '  dbx_subpart_allocate_extent';
               if vialink = 'Y' and vdb = 'BSCS' then
                   str := str  ||  '@' || vdbBSCS;
               end if;
               if viaUDRlink = 'Y' and vdb = 'UDR' then
                  str := str ||  '@' || vdbUDR;
               end if;
               if viaBILLlink = 'Y' and vdb = 'BILL' then
                  str := str ||  '@' || vdbBILL;
               end if;
               str := str || ' (v_table_name => ' || '''' || v_table_name || '''' ||
                             '  ,v_part_name => ' || '''' || v_part_name || '''' || '); ' || chr(10);
               str := str || 'end;';
               -- ShowStatement(str);
               begin
                  DBXLogStart ( piosTableName  => v_table_name || '_' || v_part_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'ALLOCATE'
                               ,piosPara1      => str);
                  execute immediate str;
                  DBXLogStop  ( piosTableName  => v_table_name || '_' || v_part_name
                               ,piosActionName => 'Exclude_Tab_Without_Seg'
                               ,piosMessage    => 'SUCCESS');
               exception when others then
                  CallInfo(SQLERRM);  -- Only an Info
               end;
            end if;

            str_create := 'DELETE FROM dbx_part ';
            str_create := str_create || ' WHERE dbx_table_name = ' || '''' || v_table_name || '''';
            str_create := str_create || ' AND   dbx_partition_name = ' || '''' || v_part_name || '''' ||
                                        ' AND   dbx_schema_name = ' || '''' || 'SYSADM' || '''';
--          ShowStatement(str_create);
            execute immediate str_create;
            CallInfo('Exclude ' || v_table_name || ':' || v_part_name );
            commit;
         else
            callinfo('No work on table ' || v_table_name );
         end if;
        END LOOP;
        CLOSE cur;
     else
        callinfo('Exclude_Tab_Without_Seg: BSCS DB is not an Oracle 11.2 DB');
     end if;
  else
     callinfo('Exclude_Tab_Without_Seg: no work on ' || vdb );
  end if;
 END LOOP;
 commit;

EXCEPTION WHEN OTHERS THEN
  callinfo('Exclude_Tab_Without_Seg: Error:');
  CallError(SQLERRM);
END Exclude_Tab_Without_Seg;


 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab
   **
   ** Purpose:   Exclude Tables from being copied
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab
(
     -- Table Name
     piosTableName IN VARCHAR2
    ,piosExclude   IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite IN VARCHAR2 DEFAULT 'N'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_object_owner            VARCHAR2(30);
 v_object_name             VARCHAR2(30);
 v_object_type             VARCHAR2(30);
 v_status                  VARCHAR2(30);
 v_schema_name             VARCHAR2(30) := 'SYSADM';
 v_table_name              VARCHAR2(60);

BEGIN

 IF piosTableName IS NOT NULL AND LENGTH(RTRIM(piosTableName)) > 1 THEN
  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');

  if instr(piosTableName,'.') > 0 then
    v_schema_name := substr(piosTableName,1,instr(piosTableName,'.')-1);
    v_table_name  := substr(piosTableName,instr(piosTableName,'.')+1);
  else
    v_schema_name := 'SYSADM';
    v_table_name := piosTableName;
  end if;

  v_select_statement := 'SELECT o.owner,o.object_name,o.object_type,o.status' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_objects';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' o ';
  v_select_statement := v_select_statement || ' WHERE (o.object_type =    ' ||''''|| 'TABLE' || '''' ;
  v_select_statement := v_select_statement || ') AND (o.owner like ' || '''' || upper(rtrim(v_schema_name)) || '''' || ')' ;
-- Problem with 8.1.7 DBLINK + SUB-Select + ESCAPE
-- v_select_statement := v_select_statement || ' AND (object_name  NOT IN (';
-- v_select_statement := v_select_statement || ' SELECT dbx_table_name FROM dbx_tables))';
  v_select_statement := v_select_statement || ' AND (object_name  like ';
  v_select_statement := v_select_statement || '''' || upper(rtrim(v_table_name)) || '''';
  v_select_statement := v_select_statement || ' ESCAPE ' || '''' || '\' || '''' || ') ' ;
  v_select_statement := v_select_statement || ' ORDER BY o.owner,o.object_name DESC';


  ShowStatement(v_select_statement);
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_object_owner, v_object_name , v_object_type , v_status ;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE ' || v_schema_name || '.' || v_object_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_object_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_object_name || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE 1 = 2 ';

      SetDBXWhereClause( piosTableName => v_object_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => '1 = 2'
                        ,piosWhereLinkClause => ' 1 = 2 '
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosExclude   => piosExclude
                        ,piosOverwrite => piosOverwrite    -- N = Do not update, when entry already exists
                        ,piosSchemaName => v_object_owner);

  END LOOP;
  CLOSE cur;

 END IF;


EXCEPTION WHEN OTHERS THEN
  callinfo('Exclude_Tab: Error:');
  CallError(SQLERRM);
END Exclude_Tab;



 /*
   **=====================================================
   **
   ** Procedure: Exclude_Tab_Col
   **
   ** Purpose:   Exclude Tables from being copied
   **            piosExclude = Y , do not copy at all
   **            piosExclude = N , copy structure only
   **
   **=====================================================
*/


PROCEDURE Exclude_Tab_Col
(
     -- Table Name
     piosColumnName IN VARCHAR2
    ,piosExclude    IN VARCHAR2 DEFAULT 'Y'
    ,piosOverwrite  IN VARCHAR2 DEFAULT 'N'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_table_name              VARCHAR2(60);
 v_schema_name             VARCHAR2(30) := 'SYSADM';
 v_column_name             VARCHAR2(60);
 v_object_owner            VARCHAR2(30);

BEGIN

 IF piosColumnName IS NOT NULL AND LENGTH(RTRIM(piosColumnName)) > 1 THEN
  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');

  if instr(piosColumnName,'.') > 0 then
    v_schema_name := substr(piosColumnName,1,instr(piosColumnName,'.')-1);
    v_column_name := substr(piosColumnName,instr(piosColumnName,'.')+1);
  else
    v_schema_name := 'SYSADM';
    v_column_name := piosColumnName;
  end if;


  v_select_statement := 'SELECT o.table_name, t.owner ' ;
  v_select_statement := v_select_statement || ' FROM dba_tab_columns';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' o ';
  v_select_statement := v_select_statement || ' ,    dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' t ';
  v_select_statement := v_select_statement || ' WHERE (o.table_name = t.table_name ) ';
  v_select_statement := v_select_statement || ' AND   (t.owner like ' || '''' || v_schema_name || '''' || ')' ;
  v_select_statement := v_select_statement || ' AND   (o.owner like ' || '''' || v_schema_name || '''' || ')' ;
-- Problem with 8.1.7 DBLINK + SUB-Select + ESCAPE
--  v_select_statement := v_select_statement || ' AND (o.table_name NOT IN (';
--  v_select_statement := v_select_statement || ' SELECT dbx_table_name FROM dbx_tables))';
  v_select_statement := v_select_statement || ' AND (o.column_name  like ';
  v_select_statement := v_select_statement || '''' || upper(rtrim(v_column_name)) || '''';
  v_select_statement := v_select_statement || ' ESCAPE ' || '''' || '\' || '''' || ') ' ;
  v_select_statement := v_select_statement || ' ORDER BY o.table_name DESC';


  ShowStatement(v_select_statement);
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name, v_object_owner ;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE ' || v_object_owner || '.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_table_name || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE 1 = 2 ';

--    CallInfo('Exclude ' || v_table_name);
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => '1 = 2'
                        ,piosWhereLinkClause => ' 1 = 2 '
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS
                        ,piosExclude   => piosExclude
                        ,piosOverwrite => piosOverwrite   -- N = Do not update, when entry already exists
                        ,piosSchemaName => v_object_owner);
  END LOOP;
  CLOSE cur;

 END IF;


EXCEPTION WHEN OTHERS THEN
  callinfo('Exclude_Tab_Col: Error:');
  CallError(SQLERRM);
END Exclude_Tab_Col;







 /*
   **=====================================================
   **
   ** Procedure: Include_Tab
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE Include_Tab
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);

 v_table_name              VARCHAR2(30);

BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');

  v_select_statement := 'SELECT o.table_name' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' o ';
  v_select_statement := v_select_statement || ' WHERE o.IOT_NAME IS NULL ';
  v_select_statement := v_select_statement || ' AND   o.CLUSTER_NAME IS NULL ';
  v_select_statement := v_select_statement || 'AND o.owner IN (' || '''' || 'SYSADM' || '''' || ') ' ;
  v_select_statement := v_select_statement || 'AND o.table_name = upper(o.table_name) ' ;
  v_select_statement := v_select_statement || 'AND o.table_name NOT IN (' ;
  v_select_statement := v_select_statement || 'SELECT dbx_table_name FROM dbx_tables' ;
  v_select_statement := v_select_statement || ' WHERE dbx_schema_name = ' || '''' || 'SYSADM' || '''' ;
  v_select_statement := v_select_statement || ') ' ;
  v_select_statement := v_select_statement || ' ORDER BY o.table_name DESC';


  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name ;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || v_table_name || ') ' || ' ' ;
      str_create := str_create || '  */ * FROM ' || v_table_name || '@' || vdbBSCS ;

      CallInfo('Include ' || v_table_name);
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => NULL
                        ,piosCreateClause => str_create
                        ,piosWhereClause => NULL
                        ,piosWhereLinkClause => NULL
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS );
  END LOOP;
  CLOSE cur;


EXCEPTION WHEN OTHERS THEN
  callinfo('Include_Tab: Error:');
  CallError(SQLERRM);
END Include_Tab;





 /*
   **=====================================================
   **
   ** Procedure: WhereAll
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE WhereAll
(
     -- Table Name
     piosColumnName IN VARCHAR2
     -- Column Exclude List
    ,piosColumnExcludeList IN VARCHAR2
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
     -- Timestamp required
    ,piosTIMESTAMP   IN VARCHAR2  DEFAULT 'Y'
     -- Storage ID
    ,piosStorageIDList   IN VARCHAR2  DEFAULT 'NULL'
    -- Context Key
    ,piosContextKeyList  IN VARCHAR2  DEFAULT 'NULL'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 OHXACT_MIN                VARCHAR2(4000);
 OHXACT_MAX                VARCHAR2(4000);
 udrentdate                VARCHAR2(4000);
 ordhdrentdate             VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 USE_CUSTOMER_ID_RANGE     VARCHAR2(4000);
 USE_OHXACT_RANGE          VARCHAR2(4000);
 IsRelease                 VARCHAR2(4000);
 EXCLUDE_GMD_REQUESTS      VARCHAR2(4000);
 EXP_FEES_PERIOD_ZERO      VARCHAR2(4000);
 TURNOVERDATE              VARCHAR2(4000);
 EXIST_FEES_PERIOD         VARCHAR2(4000);
 PORTING_INDEX_AVAILABLE   VARCHAR2(4000);
 IS_BSCS6_FUP_ACCOUNT_ID   VARCHAR2(4000);
 EXIST_FUPACCHIST_ACCSTARTDATE VARCHAR2(4000);
 FUPACCHISTSTARTDATE       VARCHAR2(4000);
 vfupaccstartdate_string   VARCHAR2(60);
 vohentdate_string         VARCHAR2(60);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);

 v_object_owner            VARCHAR2(30);
 v_object_name             VARCHAR2(30);
 l_object_name             VARCHAR2(30) := '-';
 v_object_type             VARCHAR2(30);
 v_status                  VARCHAR2(30);
 v_index_name              VARCHAR2(30);
 v_index_column_name       VARCHAR2(30);
 count_timestamp           INTEGER := 0;
 count_billcycle           INTEGER := 0;
 v_date_column_name        VARCHAR2(30);
 vuserDBXHOME              VARCHAR2(60) := ' ';
 vdbBSCS_BSCS              VARCHAR2(4000);
 vialink_BSCS              VARCHAR2(1);
 count_synonym             INTEGER := 0;
 count_context             INTEGER := 0;
 v_ContextKeyList          VARCHAR2(4000);
 count_is_part_cuid        INTEGER := 0;
 count_is_part_coid        INTEGER := 0;

BEGIN

  vdbBSCS                  := GetParameter(piosDBLink);
  vdbconBSCS               := GetParameter(piosDBConnect);
  vialink                  := GetParameter(piosVIALink);

  vdbBSCS_BSCS             := GetParameter('vdbBSCS');
  vialink_BSCS             := GetParameter('vialink');

  CallInfo('REM INFO: vdbBSCS ' || vdbBSCS || ' ' || vdbconBSCS || ' ' || vialink );
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  udrentdate               := GetParameter('udrentdate');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  ordhdrentdate            := GetParameter('ordhdrentdate');
  vohentdate_string        := GetParameter('vohentdate_string');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  USE_CUSTOMER_ID_RANGE    := GetParameter('USE_CUSTOMER_ID_RANGE');
  USE_OHXACT_RANGE         := GetParameter('USE_OHXACT_RANGE');
  IsRelease                := GetParameter('IsRelease');
  EXCLUDE_GMD_REQUESTS     := GetParameter('EXCLUDE_GMD_REQUESTS');
  EXP_FEES_PERIOD_ZERO     := GetParameter('EXP_FEES_PERIOD_ZERO'); -- 1
  EXIST_FEES_PERIOD        := GetParameter('EXIST_FEES_PERIOD');   -- Y
  TURNOVERDATE             := GetParameter('TURNOVERDATE'); -- 100
  PORTING_INDEX_AVAILABLE  := GetParameter('PORTING_INDEX_AVAILABLE'); -- Y

  IS_BSCS6_FUP_ACCOUNT_ID  := GetParameter('IS_BSCS6_FUP_ACCOUNT_ID');
  EXIST_FUPACCHIST_ACCSTARTDATE := GetParameter('EXIST_FUPACCHIST_ACCSTARTDATE');
  FUPACCHISTSTARTDATE      := GetParameter('FUPACCHISTSTARTDATE');
  vfupaccstartdate_string  := GetParameter('vfupaccstartdate_string');


  v_select_statement := 'SELECT o.owner,o.object_name,o.object_type,o.status' ;
  v_select_statement := v_select_statement || ',i.index_name, i.column_name ';
  v_select_statement := v_select_statement || ' FROM sys.dba_objects';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' o ';
  v_select_statement := v_select_statement || '    ,sys.dba_ind_columns';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' i ';
  v_select_statement := v_select_statement || ' WHERE (o.object_type =    ' ||''''|| 'TABLE' || '''' ;
  v_select_statement := v_select_statement || ') AND (o.owner = ' || '''' || 'SYSADM' || '''' || ')' ;
  v_select_statement := v_select_statement || ' AND (i.column_name IN (' || '''' ||
                                              replace( piosColumnName,',','''' ||',' ||'''')   ||''''|| ') )' ;
  v_select_statement := v_select_statement || ' AND (i.column_position = 1) ';
  v_select_statement := v_select_statement || ' AND (table_owner = ' || '''' || 'SYSADM' || '''' || ')' ;
  v_select_statement := v_select_statement || ' AND (object_name = table_name) ' ;
  v_select_statement := v_select_statement || ' AND (object_name NOT IN ( SELECT dbx_table_name FROM dbx_tables';
  v_select_statement := v_select_statement || ' WHERE dbx_schema_name = ' || '''' || 'SYSADM' || '''' || '))';
  -- Do exclude table in state DROPPED of 10g
  v_select_statement := v_select_statement || ' AND (object_name IN ( SELECT table_name FROM dba_tables';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' where owner = ' || '''' || 'SYSADM' || '''' || ')) ';
  -- Exclude tables depending on column list
--v_select_statement := v_select_statement || ' AND (object_name NOT IN ( SELECT table_name FROM dba_tab_columns';
  v_select_statement := v_select_statement || ' AND (object_name NOT IN ( SELECT table_name FROM dba_ind_columns';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
--v_select_statement := v_select_statement || ' where owner = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' where table_owner = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' and   index_owner = ' || '''' || 'SYSADM' || '''';
-- Do not exclude tables with LONG Columns here
--v_select_statement := v_select_statement || ' and (  data_length = 0 OR column_name IN (';
  v_select_statement := v_select_statement || ' and ( column_name IN (';
  v_select_statement := v_select_statement || '''' || replace(piosColumnExcludeList,',','''' ||',' ||'''') || '''';
  v_select_statement := v_select_statement || ' ) and (column_position = 1 ';
  v_select_statement := v_select_statement || ')))' ;
  v_select_statement := v_select_statement || ' ) ';

  v_select_statement := v_select_statement || ' ORDER BY o.owner,o.object_name,i.index_name DESC ';

  ShowStatement(v_select_statement);


  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_object_owner, v_object_name , v_object_type , v_status ,v_index_name ,v_index_column_name ;
      EXIT WHEN cur%NOTFOUND;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_object_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE(' || v_object_name || ') ' || ' INDEX( ' || v_object_name ;
      str_create := str_create || ' ' || v_index_name || ') ';
      str_create := str_create || '  */ * FROM ' || v_object_name ;
      str_create := str_create || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE ';
      str_where      := NULL;
      str_where_link := NULL;

      if piosColumnName = 'CUSTOMER_ID' OR piosColumnName = 'CUST_ID_ASSIGN' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create || ' WHERE CUSTOMER_ID ';
         str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         str_create := str_create ||  ')';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_create := str_create || ' AND ' || v_index_column_name ;
            str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         IF v_object_name = 'FEES' AND EXP_FEES_PERIOD_ZERO = '1' AND EXIST_FEES_PERIOD = 'Y' THEN
            str_create := str_create || ' AND PERIOD NOT IN (0) ';
         END IF;
         IF v_object_name = 'CUSTOMER_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_create := str_create || ' AND (TU_DATE > sysdate - ' || TURNOVERDATE || ')';
         END IF;
         IF v_object_name = 'DCH_EVENT_CTRL' THEN
            IF ordhdrentdate is not null and ordhdrentdate != '0' and ordhdrentdate != '-1' then
               str_create := str_create || ' AND ENTRY_DATE >= TO_DATE(' || '''' || vohentdate_string || '''' || ','
                                                                         || '''' || 'YYYY-MM-DD' || '''' || ')';
            END IF;
            IF ordhdrentdate is not null and ordhdrentdate = '-1' then
               str_create := 'EXCLUDE';
            END IF;
         END IF;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where := str_where || ' AND ' || v_index_column_name ;
            str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         IF v_object_name = 'FEES' AND EXP_FEES_PERIOD_ZERO = '1' AND EXIST_FEES_PERIOD = 'Y' THEN
            str_where := str_where || ' AND PERIOD NOT IN (0) ';
         END IF;
         IF v_object_name = 'CUSTOMER_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_where := str_where || ' AND ( TU_DATE > sysdate - ' || TURNOVERDATE || ')';
         END IF;
         IF v_object_name = 'DCH_EVENT_CTRL' THEN
            IF ordhdrentdate is not null and ordhdrentdate != '0' and ordhdrentdate != '-1' then
               str_where := str_where || ' AND ENTRY_DATE >= TO_DATE(' || '''' || vohentdate_string || '''' || ','
                                                                       || '''' || 'YYYY-MM-DD' || '''' || ')';
            END IF;
            IF ordhdrentdate is not null and ordhdrentdate = '-1' then
               str_where := 'EXCLUDE';
            END IF;
         END IF;

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where_link := str_where_link || ' AND ' || v_index_column_name ;
            str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         IF v_object_name = 'FEES' AND EXP_FEES_PERIOD_ZERO = '1' AND EXIST_FEES_PERIOD = 'Y' THEN
            str_where_link := str_where_link || ' AND PERIOD NOT IN (0) ';
         END IF;
         IF v_object_name = 'CUSTOMER_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_where_link := str_where_link || ' AND (TU_DATE > sysdate - ' || TURNOVERDATE || ')';
         END IF;
         IF v_object_name = 'DCH_EVENT_CTRL' THEN
            IF ordhdrentdate is not null and ordhdrentdate != '0' and ordhdrentdate != '-1' then
               str_where_link := str_where_link ||
                                 ' AND ENTRY_DATE >= TO_DATE(' || '''' || vohentdate_string || '''' || ','
                                                               || '''' || 'YYYY-MM-DD' || '''' || ')';
            END IF;
            IF ordhdrentdate is not null and ordhdrentdate = '-1' then
               str_where_link := 'EXCLUDE';
            END IF;
         END IF;

      end if;

      -- CUST_INFO_CUSTOMER_ID,ENTRY_DATE_TIMESTAMP or START_TIME_TIMESTAMP (6.00) = udrentdate
      if piosColumnName = 'CUST_INFO_CUSTOMER_ID' then
        /*
         * Check if column ENTRY_DATE_TIMESTAMP exists
         */
         str := 'SELECT count(*) FROM dba_tab_columns';
         if vialink = 'Y' then
            str := str ||  '@' || vdbBSCS;
         end if;
         str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
         str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
         str := str || ' AND   column_name = ' || '''' || 'ENTRY_DATE_TIMESTAMP' || '''';

         ShowStatement(str);
         execute immediate str into count_timestamp;
         if count_timestamp = 1 then
            v_date_column_name := 'ENTRY_DATE_TIMESTAMP';
         else
           /*
            * Check if column START_TIME_TIMESTAMP exists
            */
            str := 'SELECT count(*) FROM dba_tab_columns';
            if vialink = 'Y' then
               str := str ||  '@' || vdbBSCS;
            end if;
            str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
            str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
            str := str || ' AND   column_name = ' || '''' || 'START_TIME_TIMESTAMP' || '''';

            ShowStatement(str);
            execute immediate str into count_timestamp;
            if count_timestamp = 1 then
               v_date_column_name := 'START_TIME_TIMESTAMP';
            end if;
         end if;

        /*
         * Check if column CUST_INFO_BILL_CYCLE exists
         */
         str := 'SELECT count(*) FROM dba_tab_columns';
         if vialink = 'Y' then
            str := str ||  '@' || vdbBSCS;
         end if;
         str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
         str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
         str := str || ' AND   column_name = ' || '''' || 'CUST_INFO_BILL_CYCLE' || '''';

         ShowStatement(str);
         execute immediate str into count_billcycle;

         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create || ' WHERE CUSTOMER_ID ';
         str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         str_create := str_create ||  ')';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_create := str_create || ' AND ' || v_index_column_name ;
            str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_create := str_create || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;
         if count_billcycle = 1 then
            str_create := str_create || ' AND CUST_INFO_BILL_CYCLE IN (';
            str_create := str_create || ' SELECT BILLCYCLE FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
            if viadbxlink = 'Y' then
               str_create := str_create ||  '@' || vdbDBXHOME ;
            end if;
            str_create := str_create || ' WHERE BILLCYCLE IS NOT NULL';
            str_create := str_create || ' GROUP BY BILLCYCLE)';
         end if;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where := str_where || ' AND ' || v_index_column_name ;
            str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_where := str_where || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;
         if count_billcycle = 1 then
            str_where  := str_where  || ' AND CUST_INFO_BILL_CYCLE IN (';
            str_where  := str_where  || ' SELECT BILLCYCLE FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
            str_where  := str_where  || ' WHERE BILLCYCLE IS NOT NULL';
            str_where  := str_where  || ' GROUP BY BILLCYCLE)';
         end if;

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where_link := str_where_link || ' AND ' || v_index_column_name ;
            str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_where_link := str_where_link || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;
         if count_billcycle = 1 then
            str_where_link := str_where_link || ' AND CUST_INFO_BILL_CYCLE IN (';
            str_where_link := str_where_link || ' SELECT BILLCYCLE FROM DBX_CUSTOMER';
            str_where_link := str_where_link ||  '@' || vdbDBXHOME ;
            str_where_link := str_where_link || ' WHERE BILLCYCLE IS NOT NULL';
            str_where_link := str_where_link || ' GROUP BY BILLCYCLE)';
         end if;

      end if;

      if piosColumnName = 'CONTRACT_ID' OR piosColumnName = 'CO_ID' OR piosColumnName = 'RLH_PROCESS_CONTR_ID' OR
         piosColumnName = 'SB_CUST_INFO_CONTRACT_ID' OR piosColumnName = 'BI_BALAN_IDENT_CONTRACT_ID' OR
         piosColumnName = 'RDR_PROCESS_CONTR_ID' OR piosColumnName = 'CUST_INFO_PARENT_CONTRACT_ID' then

         if piosColumnName = 'CUST_INFO_PARENT_CONTRACT_ID' then
           /*
            * Check if column ENTRY_DATE_TIMESTAMP exists
            */
            str := 'SELECT count(*) FROM dba_tab_columns';
            if vialink = 'Y' then
               str := str ||  '@' || vdbBSCS;
            end if;
            str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
            str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
            str := str || ' AND   column_name = ' || '''' || 'ENTRY_DATE_TIMESTAMP' || '''';

            execute immediate str into count_timestamp;
            if count_timestamp = 1 then
               v_date_column_name := 'ENTRY_DATE_TIMESTAMP';
            end if;
         end if;

         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';
         IF USE_CONTRACT_ID_RANGE = 'Y' THEN
            str_create := str_create || ' AND ' || v_index_column_name ;
            str_create := str_create || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
         END IF;
         IF v_object_name = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_create := str_create || ' AND (CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')';
         END IF;
         IF v_object_name = 'FUP_ACCOUNTS_HIST' AND EXIST_FUPACCHIST_ACCSTARTDATE = 'Y' AND vfupaccstartdate_string IS NOT NULL
            AND FUPACCHISTSTARTDATE NOT IN ('0') AND FUPACCHISTSTARTDATE IS NOT NULL THEN
            str_create := str_create || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                  '- ' || FUPACCHISTSTARTDATE ;
         END IF;
         if count_timestamp = 1 then
            str_create := str_create || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT )';
         IF USE_CONTRACT_ID_RANGE = 'Y' THEN
            str_where := str_where || ' AND ' || v_index_column_name ;
            str_where := str_where || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
         END IF;
         IF v_object_name = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_where := str_where || ' AND (CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')';
         END IF;
         IF v_object_name = 'FUP_ACCOUNTS_HIST' AND EXIST_FUPACCHIST_ACCSTARTDATE = 'Y' AND vfupaccstartdate_string IS NOT NULL
            AND FUPACCHISTSTARTDATE NOT IN ('0') AND FUPACCHISTSTARTDATE IS NOT NULL THEN
            str_where  := str_where  || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                  '- ' || FUPACCHISTSTARTDATE ;
         END IF;
         if count_timestamp = 1 then
            str_where := str_where || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
         IF USE_CONTRACT_ID_RANGE = 'Y' THEN
            str_where_link := str_where_link || ' AND ' || v_index_column_name ;
            str_where_link := str_where_link || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
         END IF;
         IF v_object_name = 'CONTR_TURNOVER' AND TURNOVERDATE IS NOT NULL AND LENGTH(TRIM(TURNOVERDATE)) > 0   THEN
            str_where_link := str_where_link || ' AND ( CT_TU_DATE > sysdate - ' || TURNOVERDATE || ')' ;
         END IF;
         IF v_object_name = 'FUP_ACCOUNTS_HIST' AND EXIST_FUPACCHIST_ACCSTARTDATE = 'Y' AND vfupaccstartdate_string IS NOT NULL
            AND FUPACCHISTSTARTDATE NOT IN ('0') AND FUPACCHISTSTARTDATE IS NOT NULL THEN
            str_where_link  := str_where_link  || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                  '- ' || FUPACCHISTSTARTDATE ;
         END IF;
         if count_timestamp = 1 then
            str_where_link := str_where_link || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;
      end if;

      if piosColumnName = 'OHXACT' OR piosColumnName = 'OTXACT' then

       if ordhdrentdate IS NOT NULL AND TO_NUMBER(ordhdrentdate) < 0 then
         str_create      := 'EXCLUDE';
         str_where       := 'EXCLUDE';
         str_where_link  := 'EXCLUDE';
       else
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';
         IF USE_OHXACT_RANGE = 'Y' THEN
            str_create := str_create || ' AND ' || v_index_column_name ;
            str_create := str_create || ' BETWEEN ' || OHXACT_MIN      || ' AND ' || OHXACT_MAX      ;
         END IF;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM ' || vuserDBXHOME || '.DBX_ORDERHDR )';
         IF USE_OHXACT_RANGE = 'Y' THEN
            str_where := str_where || ' AND ' || v_index_column_name ;
            str_where := str_where || ' BETWEEN ' || OHXACT_MIN      || ' AND ' || OHXACT_MAX      ;
         END IF;

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ OHXACT FROM DBX_ORDERHDR';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
         IF USE_OHXACT_RANGE = 'Y' THEN
            str_where_link := str_where_link || ' AND ' || v_index_column_name ;
            str_where_link := str_where_link || ' BETWEEN ' || OHXACT_MIN      || ' AND ' || OHXACT_MAX  ;
         END IF;
       end if;
      end if;

      if piosColumnName = 'FFCODE' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FFCODE FROM ' || vuserDBXHOME || '.DBX_FFCODE';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FFCODE FROM ' || vuserDBXHOME || '.DBX_FFCODE   )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FFCODE FROM DBX_FFCODE';
         str_where_link := str_where_link || '@' || vdbDBXHOME  || ' )' ;
      end if;

      -- DOCUMENT_ID
      if piosColumnName = 'DOCUMENT_ID' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM ' || vuserDBXHOME || '.DBX_DOCUMENT';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM ' || vuserDBXHOME || '.DBX_DOCUMENT )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DOCUMENT_ID FROM DBX_DOCUMENT';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
      end if;

      -- DN_ID
      if piosColumnName = 'DN_ID' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM ' || vuserDBXHOME || '.DBX_DN';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM ' || vuserDBXHOME || '.DBX_DN )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DN_ID FROM DBX_DN';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
      end if;

      if piosColumnName = 'ACCOUNT_ID' and IS_BSCS6_FUP_ACCOUNT_ID = 'Y' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM ' || vuserDBXHOME || '.DBX_FUP_ACCOUNTS';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM ' || vuserDBXHOME || '.DBX_FUP_ACCOUNTS   )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM DBX_FUP_ACCOUNTS';
         str_where_link := str_where_link || '@' || vdbDBXHOME  || ' )' ;
      end if;

  -- Release IX 3
      IF TO_NUMBER(IsRelease) >= 11
      and piosColumnName = 'PAYMENT_ID'
      and v_object_name NOT IN ('PAYMENTTYPE_ALL') then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAYMENT_ID FROM ' || vuserDBXHOME || '.DBX_PAYMENT';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAYMENT_ID FROM ' || vuserDBXHOME || '.DBX_PAYMENT )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAYMENT_ID FROM DBX_PAYMENT';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
      end if;

      IF TO_NUMBER(IsRelease) >= 11
      and piosColumnName = 'PAY_PROPOSAL_ID' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAY_PROPOSAL_ID FROM ' || vuserDBXHOME || '.DBX_PAYMENT_PROP';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAY_PROPOSAL_ID FROM ' || vuserDBXHOME || '.DBX_PAYMENT_PROP )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ PAY_PROPOSAL_ID FROM DBX_PAYMENT_PROP';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
      end if;

  -- Release IX 1
  --  IF TO_NUMBER(IsRelease) >= 9 AND piosColumnName = 'REQUEST_ID' then
      IF (TO_NUMBER(IsRelease) >= 9 OR ( TO_NUMBER(IsRelease) >= 7 AND PORTING_INDEX_AVAILABLE = 'Y' ) ) AND piosColumnName = 'REQUEST_ID' THEN
       if v_object_name LIKE 'PORTING%' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_PORTREQ';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_PORTREQ )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_PORTREQ';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
       end if;
      END IF;
  -- Release IX 1 +2 +3
      IF TO_NUMBER(IsRelease) >= 10 AND piosColumnName = 'REQUEST_ID' then
       if v_object_name LIKE 'CRP%' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CRPREQ';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CRPREQ )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_CRPREQ';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
       end if;
       if v_object_name LIKE 'CDS%' OR v_object_name LIKE 'CDH%' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CDSREQ';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CDSREQ )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_CDSREQ';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
       end if;
      END IF;
      IF TO_NUMBER(IsRelease) >= 7 AND piosColumnName = 'REQUEST_ID' THEN
       if v_object_name LIKE 'CRH%' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CRH_REQUEST';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_CRH_REQUEST )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_CRH_REQUEST';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
       end if;
      END IF;
      IF piosColumnName in ('REQUEST','REQUEST_ID') then
       IF v_object_name LIKE 'GMD%' then
        if trim(EXCLUDE_GMD_REQUESTS) IS NULL or trim(EXCLUDE_GMD_REQUESTS) != '1' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_REQUEST';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM ' || vuserDBXHOME || '.DBX_REQUEST )';
         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ REQUEST_ID FROM DBX_REQUEST';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
        else
         str_create     := 'EXCLUDE';
         str_where      := 'EXCLUDE';
         str_where_link := 'EXCLUDE';
        end if;
       end if;
      END IF;

      if piosColumnName = 'TRANSACTION_ID' and v_object_name like 'LC_%' THEN
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ TRANSACTION_ID FROM ' || vuserDBXHOME || '.DBX_LCTRANSACTION';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ TRANSACTION_ID FROM ' || vuserDBXHOME || '.DBX_LCTRANSACTION )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ TRANSACTION_ID FROM DBX_LCTRANSACTION';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' )' ;
      end if;
      if TO_NUMBER(IsRelease) > 8 then
        if piosColumnName = 'BILLSEQNO' AND  v_object_name NOT IN ('MPULKIXD')  then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO FROM ' || vuserDBXHOME || '.DBX_BILLSEQNO';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO FROM ' || vuserDBXHOME || '.DBX_BILLSEQNO )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLSEQNO FROM DBX_BILLSEQNO';
         str_where_link := str_where_link || '@' || vdbDBXHOME  || ' )' ;
        end if;
      end if;

      -- BILLING_ACCOUNT_ID
      if piosColumnName = 'BILLING_ACCOUNT_ID' then
         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLING_ACCOUNT_ID FROM BILLING_ACCOUNT';
         str_create := str_create ||  '@' || vdbBSCS ;
         str_create := str_create || ' WHERE CUSTOMER_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create || ' WHERE CUSTOMER_ID ';
         str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         str_create := str_create ||  ')';
         str_create := str_create ||  ')';

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLING_ACCOUNT_ID FROM BILLING_ACCOUNT';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         str_where := str_where || ' )';

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ BILLING_ACCOUNT_ID FROM BILLING_ACCOUNT';
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         str_where_link := str_where_link || ' )';
      end if;

      -- CUSTOMER_SET_ID
      if piosColumnName = 'CUSTOMER_SET_ID' AND  v_object_name NOT IN ('CUSTOMER_SET','BCH_CONTROL','CUSTOMER_SET_APP_INSTANCE','CUSTOMER_SET_WORKLOAD')  then
        count_is_part_cuid := 0;
        count_is_part_coid := 0;
        /*
         * Check if column CUSTOMER_ID is part of that index
         */
         str := 'SELECT count(*) FROM dba_ind_columns';
         if vialink = 'Y' then
            str := str ||  '@' || vdbBSCS;
         end if;
         str := str || ' WHERE table_owner = ' || '''' || 'SYSADM' || '''';
         str := str || ' AND   index_name = ' || '''' || v_index_name  || '''';
         str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
         str := str || ' AND   column_name = ' || '''' || 'CUSTOMER_ID' || '''';
         execute immediate str into count_is_part_cuid;

         if count_is_part_cuid = 0 then
            str := 'SELECT count(*) FROM dba_ind_columns';
            if vialink = 'Y' then
               str := str ||  '@' || vdbBSCS;
            end if;
            str := str || ' WHERE table_owner = ' || '''' || 'SYSADM' || '''';
            str := str || ' AND   index_name = ' || '''' || v_index_name  || '''';
            str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
            str := str || ' AND   column_name = ' || '''' || 'CONTRACT_ID' || '''';
            execute immediate str into count_is_part_coid;
         end if;

         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DISTINCT CUSTOMER_SET_ID FROM CUSTOMER_BASE';
         str_create := str_create || '@' || vdbBSCS;
         str_create := str_create || ' WHERE CUSTOMER_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create || ' WHERE CUSTOMER_ID ';
         str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         str_create := str_create ||  ')';
         str_create := str_create ||  ')';
         if count_is_part_cuid = 1 then
            str_create := str_create || ' AND CUSTOMER_ID  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
            if viadbxlink = 'Y' then
               str_create := str_create ||  '@' || vdbDBXHOME ;
            end if;
            str_create := str_create || ' WHERE CUSTOMER_ID ';
            str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
            str_create := str_create ||  ')';
         end if;
         if count_is_part_coid = 1 then
            str_create :=  str_create || ' AND CONTRACT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT';
            if viadbxlink = 'Y' then
               str_create := str_create ||  '@' || vdbDBXHOME ;
            end if;
            IF USE_CONTRACT_ID_RANGE = 'Y' THEN
               str_create := str_create || ' WHERE CO_ID ';
               str_create := str_create || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
            END IF;
            str_create := str_create ||  ')';
         end if;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DISTINCT CUSTOMER_SET_ID FROM CUSTOMER_BASE';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         str_where := str_where || ' )';
         if count_is_part_cuid = 1 then
            str_where := str_where || ' AND CUSTOMER_ID ';
            str_where := str_where || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
            str_where := str_where || ' WHERE CUSTOMER_ID ';
            str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         end if;
         if count_is_part_coid = 1 then
            str_where  :=  str_where  || ' AND CONTRACT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT';
            IF USE_CONTRACT_ID_RANGE = 'Y' THEN
               str_where  := str_where  || ' WHERE CO_ID ';
               str_where  := str_where  || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
            END IF;
            str_where := str_where || ' )';
         end if;


         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ DISTINCT CUSTOMER_SET_ID FROM CUSTOMER_BASE';
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         str_where_link := str_where_link || ' )';
         if count_is_part_cuid = 1 then
            str_where_link := str_where_link || ' AND CUSTOMER_ID ';
            str_where_link := str_where_link || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
            str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
            str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
            str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         end if;
         if count_is_part_coid = 1 then
            str_where_link  :=  str_where_link  || ' AND CONTRACT_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM ' || vuserDBXHOME || '.DBX_CONTRACT';
            str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
            IF USE_CONTRACT_ID_RANGE = 'Y' THEN
               str_where_link  := str_where_link || ' WHERE CO_ID ';
               str_where_link  := str_where_link || ' BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX ;
            END IF;
            str_where_link := str_where_link || ' )';
         end if;
      end if;


      -- START_TIME_TIMESTAMP or INITIAL_START_TIME_TIMESTAMP
      if piosColumnName = 'START_TIME_TIMESTAMP' or piosColumnName = 'INITIAL_START_TIME_TIMESTAMP'
         OR piosColumnName = 'DR_ASS_DOC_CR_DATE_TIMESTAMP' THEN
         str_create := str_create || v_index_column_name ||  ' >= sysdate - ' || udrentdate ;

         str_where :=  v_index_column_name ||  ' >= sysdate - ' || udrentdate ;

         str_where_link := v_index_column_name ||  ' >= sysdate - ' || udrentdate ;
      end if;

-- DR_ADDRESSEE_CUSTOMER_ID DR_ASS_DOC_CR_DATE_TIMESTAMP
      if piosColumnName = 'DR_ADDRESSEE_CUSTOMER_ID' then
        /*
         * Check if column DR_ASS_DOC_CR_DATE_TIMESTAMP exists
         */
         str := 'SELECT count(*) FROM dba_tab_columns';
         if vialink = 'Y' then
            str := str ||  '@' || vdbBSCS;
         end if;
         str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
         str := str || ' AND   table_name = ' || '''' || v_object_name || '''';
         str := str || ' AND   column_name = ' || '''' || 'DR_ASS_DOC_CR_DATE_TIMESTAMP' || '''';

         execute immediate str into count_timestamp;
         if count_timestamp = 1 then
            v_date_column_name := 'DR_ASS_DOC_CR_DATE_TIMESTAMP';
         end if;

         str_create := str_create || v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER';
         if viadbxlink = 'Y' then
            str_create := str_create ||  '@' || vdbDBXHOME ;
         end if;
         str_create := str_create || ' WHERE CUSTOMER_ID ';
         str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         str_create := str_create ||  ')';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_create := str_create || ' AND ' || v_index_column_name ;
            str_create := str_create || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_create := str_create || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;

         str_where :=  v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM ' || vuserDBXHOME || '.DBX_CUSTOMER ';
         str_where := str_where || ' WHERE CUSTOMER_ID ';
         str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where := str_where || ' AND ' || v_index_column_name ;
            str_where := str_where || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_where := str_where || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;

         str_where_link := v_index_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CUSTOMER_ID FROM DBX_CUSTOMER';
         str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
         str_where_link := str_where_link || ' WHERE CUSTOMER_ID ';
         str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX || ' ) ';
         IF USE_CUSTOMER_ID_RANGE = 'Y' THEN
            str_where_link := str_where_link || ' AND ' || v_index_column_name ;
            str_where_link := str_where_link || ' BETWEEN ' || CUSTOMER_ID_MIN || ' AND ' || CUSTOMER_ID_MAX ;
         END IF;
         if count_timestamp = 1 then
            str_where_link := str_where_link || ' AND ' || v_date_column_name || ' >= sysdate - ' || udrentdate ;
         end if;

      end if;

      if piosColumnName in ('CUST_INFO_CUSTOMER_ID','CUST_INFO_PARENT_CONTRACT_ID','START_TIME_TIMESTAMP',
                            'INITIAL_START_TIME_TIMESTAMP','DR_ASS_DOC_CR_DATE_TIMESTAMP') then
        /*
         * Check is Public synonym exists
         */
         str := 'SELECT count(*) FROM all_synonyms';
         if vialink = 'Y' then
            str := str ||  '@' || vdbBSCS;
         end if;
         str := str || ' WHERE owner = ' || '''' || 'PUBLIC' || '''';
         str := str || ' AND table_name = ' || '''' || v_object_name || '''';
         str := str || ' AND table_owner = ' || '''' || 'SYSADM' || '''';
         str := str || ' AND db_link IS NULL';
         ShowStatement(str);
         execute immediate str into count_synonym;
        /*
         * Check is synonym fits to storage_id
         */
         if piosStorageIDList != 'NULL' and count_synonym > 0  then
           /*
            * Get number of matching context_keys
            */
            str := 'SELECT count(*) FROM uds_context';
            if vialink_BSCS = 'Y' then
               str := str ||  '@' || vdbBSCS_BSCS;
            end if;
            str := str || ' c ';
            str := str || ',all_synonyms';
            if vialink = 'Y' then
               str := str ||  '@' || vdbBSCS;
            end if;
            str := str || ' a ';
            str := str || ' WHERE c.uds_context_key = a.synonym_name ';
            str := str || ' AND c.uds_storage_id  IN (' || piosStorageIDList || ')' ;
            str := str || ' AND a.owner = ' || '''' || 'PUBLIC' || '''';
            str := str || ' AND a.table_name = ' || '''' || v_object_name || '''';
            str := str || ' AND a.table_owner = ' || '''' || 'SYSADM' || '''';
            str := str || ' AND a.db_link IS NULL';
            ShowStatement(str);
            execute immediate str into count_context;
         end if;
        /*
         * Check if synonym fits to context key list
         */
         if piosContextKeyList != 'NULL' and count_synonym > 0 then
           /*
            * Get number of matching context_keys
            */
            v_ContextKeyList := replace(piosContextKeyList,' ');
            v_ContextKeyList := upper(v_ContextKeyList);
            str := 'SELECT count(*) FROM uds_context';
            if vialink_BSCS = 'Y' then
               str := str ||  '@' || vdbBSCS_BSCS;
            end if;
            str := str || ' c ';
            str := str || ',all_synonyms';
            if vialink = 'Y' then
               str := str ||  '@' || vdbBSCS;
            end if;
            str := str || ' a ';
            str := str || ' WHERE c.uds_context_key = a.synonym_name ';
            str := str || ' AND a.owner = ' || '''' || 'PUBLIC' || '''';
            str := str || ' AND a.table_name = ' || '''' || v_object_name || '''';
            str := str || ' AND a.table_owner = ' || '''' || 'SYSADM' || '''';
            str := str || ' AND a.db_link IS NULL ';
            str := str || ' AND c.uds_context_key IN (' || '''' ||
                          replace(v_ContextKeyList,',','''' || ',' || '''') ||
                          '''' || ')';
            ShowStatement(str);
            execute immediate str into count_context;
         end if;
      end if;

      if piosColumnName in ('CUST_INFO_CUSTOMER_ID','CUST_INFO_PARENT_CONTRACT_ID','START_TIME_TIMESTAMP',
                            'INITIAL_START_TIME_TIMESTAMP','DR_ASS_DOC_CR_DATE_TIMESTAMP') and (
         ( count_timestamp = 0 and piosTIMESTAMP = 'Y' and piosColumnName in ('CUST_INFO_CUSTOMER_ID') ) -- no timestamp column
         OR ( count_synonym = 0 )                                  -- no Public Synonym
         OR ( piosStorageIDList  != 'NULL' and count_context = 0 )           -- Context Key (Public Synonym) not for this Storage ID
         OR ( piosContextKeyList != 'NULL' and count_context = 0 ) -- Context Key (Public Synonym) not for this Context Keys
         ) then
         null;
         CallInfo(piosColumnName ||': ' || v_object_name || ' not selected. (Pub.Syn.:' || TO_CHAR(count_synonym) ||
                                   ',Context:' || TO_CHAR(count_context) || ',TS:' || TO_CHAR(count_timestamp) ||')' );
      else
         --
         -- Use the first Index which was found for this object
         --
        if str_where IS NOT NULL then
         if v_object_name != l_object_name then
           if str_create != 'EXCLUDE' then
            CallInfo(piosColumnName ||': ' || v_object_name || ' ' || v_index_name || ' ' || v_index_column_name );
            SetDBXWhereClause( piosTableName => v_object_name
                              ,piosColumnName => v_index_column_name
                              ,piosCreateClause => str_create
                              ,piosWhereClause => str_where
                              ,piosWhereLinkClause => str_where_link
                              ,piosHint => ' '
                              ,piosStorage => ' '
                              ,piosDBConnect => vdbconBSCS );
           else
            Exclude_Tab ( piosTableName => v_object_name ,piosExclude   => 'Y' ,piosOverwrite => 'Y');
           end if;
         end if;
        end if;
      end if;
      l_object_name := v_object_name;
  END LOOP;
  CLOSE cur;


EXCEPTION WHEN OTHERS THEN
  callinfo('WhereAll: Error:');
  if SQLCODE = -1652 then
     CallInfo('Define a temporary tablespace for user SYSADM, which is large enough.');
     CallInfo('Connect as user SYS at the DBX database and the DBX HOME database and execute:');
     CallInfo('SQL> alter user SYSADM temporary tablespace <name>;');
  end if;
  CallError(SQLERRM);
END WhereAll;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXFfcode
   **
   ** Purpose:   Fille Table DBX_Ffcode
   **
   **=====================================================
*/


PROCEDURE FillTableDBXFfcode
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 MPUFFTAB_HINT             VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 vmpufftab                 VARCHAR2(1);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  MPUFFTAB_HINT            := GetParameter('MPUFFTAB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  vmpufftab                := GetParameter ('vmpufftab');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

  /* First CUSTOMER_ID_HIGH */

  str := 'INSERT /*+  APPEND */ INTO dbx_ffcode ';
  str := str || '(FFCODE) ';
  str := str || 'SELECT /*+ ' || GLOBAL_HINT || ' ' || MPUFFTAB_HINT ;
  if vmpufftab = '1' then
     str := str || ' */ FFCODE ';
  else
     str := str || ' */ DISTINCT FFCODE ';   -- add distinct for PN 296127 for PK: FFCODE,VSCODE
  end if;
  str := str || 'FROM MPUFFTAB';

  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;

  str := str || ' WHERE CO_ID IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ CO_ID FROM DBX_CONTRACT';
--   if vialink = 'Y' then
--      str := str ||  '@' || vdbBSCS;
--   end if;
  str := str || ' ) ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND   CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_FFCODE'
               ,piosActionName => 'FillTableDBXFfcode'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXFfcode: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_FFCODE'
               ,piosActionName => 'FillTableDBXFfcode'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXFfcode: Start:' || start_time );
  callinfo('FillTableDBXFfcode: Stop: ' || stop_time );

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXFfcode: Error:');
  CallError(SQLERRM);
END FillTableDBXFfcode;



 /*
   **=====================================================
   **
   ** Procedure: Mark_long
   **
   ** Purpose:   Exclude tables with long or long raw columns
   **
   **=====================================================
*/


PROCEDURE Mark_long
AS
 v_update_statement        VARCHAR2(30000);
 vdbBSCS                      VARCHAR2(4000);
 vialink                   VARCHAR2(1);

BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');

  v_update_statement := 'UPDATE dbx_tables SET dbx_long_column = ' || '''' || 'Y' || ''''  ;
  v_update_statement := v_update_statement || ' WHERE dbx_schema_name = ' || '''' || 'SYSADM' || '''';
  v_update_statement := v_update_statement || '   AND dbx_table_name IN (';
  v_update_statement := v_update_statement || ' SELECT table_name FROM dba_tab_columns';
  if vialink = 'Y' then
     v_update_statement := v_update_statement ||  '@' || vdbBSCS;
  end if;
  v_update_statement := v_update_statement || ' WHERE data_type IN (' ||''''|| 'LONG RAW' || '''' ;
  v_update_statement := v_update_statement || ',' || ''''|| 'LONG' || '''' || ') ' ;
  v_update_statement := v_update_statement || ' AND owner = ' ||''''|| 'SYSADM' || '''' || ')' ;

  ShowStatement(v_update_statement);
  execute immediate v_update_statement;
  callinfo('Mark_long: Rows updated ' || to_CHAR(SQL%ROWCOUNT) );

EXCEPTION WHEN OTHERS THEN
  callinfo('Mark_long: Error:');
  CallError(SQLERRM);
END Mark_long;




 /*
   **=====================================================
   **
   ** Procedure: Mark_iot_table
   **
   ** Purpose:   Exclude tables which are Index organized or Partitioned or Clustered
   **
   **=====================================================
*/


PROCEDURE Mark_iot_table
(
    -- DB Link
     piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
)
AS
 v_update_statement        VARCHAR2(30000);
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);

BEGIN

  vdbBSCS                  := GetParameter(piosDBLink);
  vialink                  := GetParameter(piosVIALink);

  CallInfo('REM INFO: vdbBSCS ' || vdbBSCS || ' ' || vialink );

  v_update_statement := 'UPDATE dbx_tables SET dbx_iot_table = ' || '''' || 'Y' || ''''  ;
  v_update_statement := v_update_statement || ' WHERE dbx_iot_table = ' || '''' || 'N' || '''';
  v_update_statement := v_update_statement || ' AND   dbx_schema_name = ' || '''' || 'SYSADM' || '''';
  v_update_statement := v_update_statement || ' AND   dbx_table_name IN (';
  v_update_statement := v_update_statement || ' SELECT table_name FROM dba_tables';
  if vialink = 'Y' then
     v_update_statement := v_update_statement ||  '@' || vdbBSCS;
  end if;
  v_update_statement := v_update_statement || ' WHERE ( iot_type IN (' ||''''|| 'IOT' || '''' || ')' ;
  v_update_statement := v_update_statement || ' OR    ( partitioned = ' || '''' || 'YES' || '''' || ')';
  v_update_statement := v_update_statement || ' OR    ( cluster_name IS NOT NULL ) )';
  v_update_statement := v_update_statement || ' AND   iot_name IS NULL ' ;
  v_update_statement := v_update_statement || ' AND   owner = ' || '''' || 'SYSADM' || '''' || ' )';

  ShowStatement(v_update_statement);
  execute immediate v_update_statement;
  callinfo('Mark_iot_table: Rows updated ' || to_CHAR(SQL%ROWCOUNT) );

EXCEPTION WHEN OTHERS THEN
  callinfo('Mark_iot_table: Error:');
  CallError(SQLERRM);
END Mark_iot_table;





 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSTables
   **
   ** Purpose:   Copy BSCS tables from BSCS DB and UDR DB to DBX DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSTables
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 str                       VARCHAR2(30000);
 v_str                     VARCHAR2(30000); -- Required for ORACLE 8.1.6
 str_cv                    VARCHAR2(30000);
 str_ct                    VARCHAR2(30000);
 v_table_name              VARCHAR2(30);
 start_date                VARCHAR2(30);
 stop_date                 VARCHAR2(30);
 diff_time                 INTEGER;
 vdbBSCS                   VARCHAR2(4000);
 vdbUDR                    VARCHAR2(4000);
 vdbBILL                   VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 viadbxlink                VARCHAR2(1);
 USE_DRIVING_SITE_VIEW     VARCHAR2(1);
 vuserBSCS                 VARCHAR2(4000);
 vuserUDR                  VARCHAR2(4000);
 vuserBILL                 VARCHAR2(4000);
 step                      INTEGER := 0;
 vdbconBSCS                VARCHAR2(4000);
 vdbconUDR                 VARCHAR2(4000);
 vdbconBILL                VARCHAR2(4000);
 vdbconDBXHOME             VARCHAR2(4000);
 vdbconDBX                 VARCHAR2(4000);
 v_dbx_db_connect          VARCHAR2(4000);
 v_dblink                  VARCHAR2(4000);
 v_user                    VARCHAR2(4000);
 v_checkdblink             INTEGER := 0;
 v_checkdblink_BSCS        INTEGER := 1;
 v_checkdblink_BILL        INTEGER := 1;
 v_checkdblink_UDR         INTEGER := 1;
 v_checkdblink_DBX         INTEGER := 1;
 firstTableStart           INTEGER := 0;
 firstTableStop            INTEGER := 0;
 firstTable                VARCHAR2(60);
 secondTableStart          INTEGER := 0;
 secondTableStop           INTEGER := 0;
 secondTable               VARCHAR2(60);
begin
  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbUDR                   := GetParameter('vdbUDR');
  vdbBILL                  := GetParameter('vdbBILL');
  vialink                  := GetParameter('vialink');
  viadbxlink               := GetParameter('viadbxlink');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  vuserBSCS                := GetParameter('vuserBSCS');
  vuserUDR                 := GetParameter('vuserUDR');
  vuserBILL                := GetParameter('vuserBILL');
  USE_DRIVING_SITE_VIEW    := GetParameter('USE_DRIVING_SITE_VIEW');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbconUDR                := GetParameter('vdbconUDR');
  vdbconBILL               := GetParameter('vdbconBILL');
  vdbconDBXHOME            := GetParameter('vdbconDBXHOME');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  vdbconDBX                := GetParameter('vdbconDBX');

 /*
  * check vdbDBXHOME at BSCS DB
  */
  if upper(vdbconDBXHOME) = upper(vdbconBSCS) then
     v_checkdblink_BSCS := 0;
  end if;
  v_dblink := vdbBSCS;
 /*
  * check vdbDBXHOME at UDR  DB
  */
  if upper(vdbconDBXHOME) = upper(vdbconUDR) then
     v_checkdblink_UDR  := 0;
  end if;
 /*
  * check vdbDBXHOME at BILL DB
  */
  if upper(vdbconDBXHOME) = upper(vdbconBILL) then
     v_checkdblink_BILL  := 0;
  end if;
 /*
  * check vdbDBXHOME at DBX DB
  */
  if upper(vdbconDBXHOME) = upper(vdbconDBX) then
     v_checkdblink_DBX   := 0;
  end if;


 /*
  * Fetch all tables
  */
  v_select_statement := 'SELECT   dbx_table_name , dbx_create_clause, dbx_db_connect ';
  v_select_statement := v_select_statement || 'FROM    DBX_TABLES ';
  v_select_statement := v_select_statement || 'WHERE   dbx_long_column = ' || '''' || 'N' || '''';
  v_select_statement := v_select_statement || ' AND    dbx_iot_table   = ' || '''' || 'N' || '''';
  v_select_statement := v_select_statement || ' AND    dbx_exclude = ' || '''' || 'N' || '''';
  v_select_statement := v_select_statement || ' AND    dbx_schema_name = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' AND   dbx_table_name NOT IN (SELECT table_name FROM user_tables)';
  v_select_statement := v_select_statement || ' ORDER BY dbx_storage, dbx_table_name ASC';
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , str ,v_dbx_db_connect ;
      EXIT WHEN cur%NOTFOUND;

      begin
         step := 0;
         start_date := TO_CHAR(sysdate,'YYYYMMDDHH24MISS');
         v_str := str;
         IF lower(trim(USE_DRIVING_SITE_VIEW)) = '1' THEN
            IF upper(trim(v_dbx_db_connect)) = upper(trim(vdbconBSCS)) THEN
               v_dblink      := vdbBSCS;
               v_user        := vuserBSCS;
               v_checkdblink := v_checkdblink_BSCS;
            ELSE
               IF upper(trim(v_dbx_db_connect)) = upper(trim(vdbconUDR)) THEN
                  v_dblink      := vdbUDR;
                  v_user        := vuserUDR;
                  v_checkdblink := v_checkdblink_UDR;
               ELSE
                  IF upper(trim(v_dbx_db_connect)) = upper(trim(vdbconBILL)) THEN
                     v_dblink      := vdbBILL;
                     v_user        := vuserBILL;
                     v_checkdblink := v_checkdblink_BILL;
                  END IF;
               END IF;
            END IF;

            /*
             * Adjust view dbx_driving_view at the BSCSDB with current SELECT
             */
            str_cv := 'begin ' ||
    --                '  ' || vuserBSCS || '.dbx_driving_view_proc@' || v_dblink || ' ( ' || '''' ||
                      '  ' || 'dbx_driving_view_proc@' || v_dblink || ' ( ' || '''' ||
                      replace(replace(replace(replace(substr(str,instr(str,'AS SELECT')+3)
                                                     ,'@' || vdbBSCS  )
                                             ,'@' || vdbBILL  )
                                     ,'@' || vdbUDR   )
                             ,'''','''''') ||
                      '''' || ' ); ' || chr(10) ||
                      'end;';
           /*
            * check if vdbDBXHOME exists at DB and remove from str_cv if not
            */
            if 0 != instr(str,'@' || vdbDBXHOME ) then
               if v_checkdblink = 0 then
                  str_cv := replace(str_cv,'@' || vdbDBXHOME);
               end if;
            end if;


           /*
            * check if vdbDBXHOME exists at DBX DB and add DBLINK to DBXHOME DB
            */
            if v_checkdblink_DBX = 0 then
            -- str_cv := replace(str_cv,'@' || vdbDBXHOME);
               firstTableStart  := 0;
               firstTableStop   := 0;
               secondTableStart := 0;
               secondTableStop  := 0;
               firstTable       := '';
               secondTable      := '';
               firstTableStart  := instr(upper(str_cv),'.DBX_',1,1);
               secondTableStart := instr(upper(str_cv),'.DBX_',1,2);
               if firstTableStart > 0 then
                  firstTableStop  :=instr(replace(str_cv,')',' ')||' ' ,' ',firstTableStart,1);
                  firstTable      := substr(str_cv,firstTableStart,firstTableStop-firstTableStart);
               end if;
               if secondTableStart > 0 then
                  secondTableStop := instr(replace(str_cv,')',' ')||' ' ,' ',secondTableStart,1);
                  secondTable     := substr(str_cv,secondTableStart,secondTableStop-secondTableStart);
               end if;
               if firstTableStart > 0 and firstTableStop > firstTableStart then
                  str_cv := replace(str_cv,firstTable,firstTable || '@' || vdbDBXHOME);
               end if;
               if secondTableStart > 0 and secondTableStop > secondTableStart and firstTable != secondTable then
                  str_cv := replace(str_cv,secondTable,secondTable || '@' || vdbDBXHOME);
               end if;
            end if;

            step := 1;
            DBXLogStart ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-av'
                         ,piosMessage    => 'ALTER VIEW'
                         ,piosPara1      => str_cv);
            execute immediate str_cv;
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-av'
                         ,piosMessage    => 'SUCCESS');
           /*
            * Use view dbx_driving_view from the BSCSDB instead of current SELECT
            */
            step := 2;
            str_ct := substr(str,1,instr(str,'AS SELECT')+2 ) ||
                      ' SELECT * FROM ' || v_user    || '.dbx_driving_view@' || v_dblink;
            DBXLogStart ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-ct'
                         ,piosMessage    => 'CREATE TABLE FROM VIEW'
                         ,piosPara1      => str_ct);
            execute immediate str_ct;
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-ct'
                         ,piosMessage    => 'SUCCESS');
         ELSE

            step := 3;
            DBXLogStart ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables'
                         ,piosMessage    => 'CREATE TABLE'
                         ,piosPara1      => v_str);
            execute immediate v_str;
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables'
                         ,piosMessage    => 'SUCCESS');
         END IF;
         stop_date  := TO_CHAR(sysdate,'YYYYMMDDHH24MISS');

         diff_time := TO_NUMBER(stop_date) - TO_NUMBER(start_date);
         CallInfo('Table:     ' || v_table_name);
         CallInfo('Duration : ' || TO_CHAR(diff_time) );

       exception when others then
         CallInfo('Error for Table: ' || v_table_name );
         CallInfo(substr(SQLERRM,1,250));
         CallInfo(substr(str,1,250) );
         CallInfo(substr(str,251,500) );
         CallInfo(substr(str_cv,1,250) );
         CallInfo(substr(str_cv,251,500) );
         -- Error for missing privilege to create a table
         if SQLCODE = -1031 then
            CallInfo('Connect as user SYS at the DBX database and grant "CREATE TABLE" privilege to SYSADM');
            CallInfo('SQL> grant CREATE TABLE to SYSADM;');
         end if;
         if SQLCODE = -942  then
            CallInfo('The SELECT privilege on the table to be copied seems to be missing');
            CallInfo('May use user SYSADM for the database links to connect to the BSCS DBs');
         end if;
         if step = 1 then
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-av'
                         ,piosMessage    => 'ERROR'
                         ,piosPara2      => SQLERRM);
         end if;
         if step = 2 then
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables-ct'
                         ,piosMessage    => 'ERROR'
                         ,piosPara2      => SQLERRM);
         end if;
         if step = 3 then
            DBXLogStop  ( piosTableName => v_table_name
                         ,piosActionName => 'CreateBSCSTables'
                         ,piosMessage    => 'ERROR'
                         ,piosPara2      => SQLERRM);
         end if;
       end;
       commit;
  END LOOP;
  CLOSE cur;

exception when others then
  callinfo('CreateBSCSTables Error:');
  CallInfo(v_select_statement);
  CallInfo(str);
  if SQLCODE = -942  then
     CallInfo('Check synonym for dbx_tables');
  end if;
  CallError(SQLERRM);

END CreateBSCSTables;

 /*
   **=====================================================
   **
   ** Procedure: CreateDatabaseLink
   **
   ** Purpose:   Create a database link
   **
   **=====================================================
*/


PROCEDURE CreateDatabaseLink
(
     -- DB Link
     piosDBLink        IN VARCHAR2
     -- DB User
    ,piosDBUser        IN VARCHAR2
     -- DB Password
    ,piosDBPW          IN VARCHAR2
     -- DB Connect String
    ,piosDBConnect     IN VARCHAR2
     -- DB Connect String Current
    ,piosDBConnectCurr IN VARCHAR2
)
AS
 i      integer;
 str    VARCHAR2(2000);
 v_user VARCHAR2(60);
begin

 /*
  * Get current username
  */
  SELECT username INTO v_user FROM user_users;

 /*
  * Is DBX DB equal BSCS DB
  */
  if piosDBConnect  = piosDBConnectCurr then
     CallInfo('No database link required');
  else
     CallInfo('Database link required');
     CallInfo('DROP DATABASE LINK ' || piosDBLink );
     begin
        execute immediate 'drop database link ' || piosDBLink;
        CallInfo('DATABASE LINK ' || piosDBLink || ' dropped.');
     exception when others then
        if SQLCODE = -2024 then
           CallInfo('Database link ' || nvl(piosDBLink,'NULL') || ' does not exist');
        else
           CallInfo('DROP DATABASE LINK ' || piosDBLink || substr(SQLERRM,1,200) );
        end if;
     end;
     CallInfo('CREATE DATABASE LINK ' || piosDBLink );
     begin
        str := 'CREATE DATABASE LINK ' || piosDBLink ||
               ' CONNECT TO ' || piosDBUser ||
               ' IDENTIFIED BY ' || piosDBPW ||
               ' USING ' || '''' || piosDBConnect || '''';
        execute immediate str;
        CallInfo(str);
        CallInfo('DATABASE LINK ' || piosDBLink || ' created.');
     exception when others then
        if SQLCODE = -2011 then
            CallInfo('DATABASE LINK ' || piosDBLink || ' already exists');
        else
            if SQLCODE = -1031  then
               CallInfo('INFO> The user used to create a database link may not have CREATE DATABASE LINK privilege');
               CallInfo('INFO> Connect to database ' ||  piosDBConnectCurr  || ' as user SYS and execute:');
               CallInfo('SQL> grant CREATE DATABASE LINK to ' || v_user || ';');
            end if;
            CallError('CREATE DATABASE LINK ' || piosDBLink || substr(SQLERRM,1,200) );
        end if;
     end;
     str := 'select count(*) from dba_tables@' || piosDBLink || ' where owner = ' || '''' || 'SYSADM' || '''';
     execute immediate str into i ;
     CallInfo('Test DBLink> No of tables:' || TO_CHAR(i) );
     str := 'select count(*) from dba_users@' || piosDBLink ;
     execute immediate str into i ;
     CallInfo('Test DBLink> No of users :' || TO_CHAR(i) );

  end if;

exception when others then
  callinfo('CreateDatabaseLink: Error:');
  CallInfo(str);
  -- Error for missing db link
  if SQLCODE = -2019 then
            CallInfo('Database link ' || nvl(piosDBLink,'NULL') || ' is missing');
  end if;
  -- Error ORA-12505: TNS:listener does not currently know of SID given in connect
  if SQLCODE = -12505 then
            CallInfo('The connect string of the database link ' || nvl(piosDBLink,'NULL') ||
                     ' can not be resolved in database (' || NVL(piosDBConnectCurr,'NULL') || ')' );
            CallInfo('You may use a full qualified connect string for the database link (see output of tnsping)');
  end if;
  if SQLCODE = -1017  then
            CallInfo('The combination of username and password of the database link ' || nvl(piosDBLink,'NULL') ||
                     ' in database ' || piosDBConnectCurr ||
                     ' is wrong for database '  || NVL(piosDBConnect,'NULL') || ' ' );
            CallInfo('Connect to database ' || piosDBConnect || ' as user SYS and execute:');
            CallInfo('SQL> create user ' || piosDBUser || ' identified by ' || piosDBPW || ';');
            CallInfo('SQL> alter  user ' || piosDBUser || ' identified by ' || piosDBPW || ';');
            CallInfo('SQL> grant  create session to ' || piosDBUser || ';');
            CallInfo('SQL> grant  SELECT ANY DICTIONARY to ' || piosDBUser || ';');
            CallInfo('SQL> grant  BSCS_ROLE      to ' || piosDBUser || ';');
  end if;
  if SQLCODE = -1045 then
            CallInfo('The username of the database link ' || nvl(piosDBLink,'NULL') ||
                     ' in database (' ||  NVL(piosDBConnectCurr,'NULL') || ') ' ||
                     ' does not have connect privilege for database (' || NVL(piosDBConnect,'NULL') || ')' );
            CallInfo('Connect to database ' || piosDBConnect || ' as user SYS and execute:');
            CallInfo('SQL> grant CONNECT to ' || piosDBUser || ';');
  end if;
  if SQLCODE = -942  then
            CallInfo('The user ' || piosDBUser || ' of the database link ' || nvl(piosDBLink,'NULL') ||
                     ' in database ' || piosDBConnectCurr ||
                     ' may not have SELECT ANY DICTIONARY privilege for database ' || piosDBConnect ||
                     ' (See data dictionary view DBA_USERS) ');
            CallInfo('Connect to database ' || piosDBConnect || ' as user SYS and execute:');
            CallInfo('SQL> grant SELECT ANY DICTIONARY to ' || piosDBUser || ';');
  end if;
  if SQLCODE = -12154 then
            CallInfo('The connect string of the database link ' || nvl(piosDBLink,'NULL') ||
                     ' in database (' ||  NVL(piosDBConnectCurr,'NULL') || ') ' ||
                     ' can not be resolved. (connect string: ' || NVL(piosDBConnect,'NULL') || ')' );
            CallInfo('Check connect string in dbextract.cfg or default.cfg.');
            CallInfo('When connect string is correct:');
            CallInfo(' - login to the database server');
            CallInfo(' - set the ORACLE environment for the database.');
            CallInfo(' - test the connect string with tnsping.');
  end if;
  CallError(SQLERRM);
END CreateDatabaseLink;


 /*
   **=====================================================
   **
   ** Procedure: DropTable
   **
   ** Purpose:   Drop a DBX Table
   **
   **=====================================================
*/

PROCEDURE DropTable
(
     -- Table Name
     piosTableName  IN VARCHAR2
)
AS
 i   integer;
 str VARCHAR2(2000);
 v   integer := 0;
begin

  -- Check if table exists
  str := 'select count(*) from user_tables where table_name = upper(' ||
         '''' || piosTableName ||'''' || ')';
  begin
    execute immediate str into i;
  exception when others then
    CallInfo(str);
    CallError('DropTable:' || SQLERRM);
  end;

  -- Check if Oracle Version 10
  str := 'select count(*) from dictionary where table_name = upper(' ||
         '''' || 'USER_DATAPUMP_JOBS' ||'''' || ')';
  begin
    execute immediate str into v;
  exception when others then
    v := 0;
    CallInfo(str);
    CallError('DropTable:' || SQLERRM);
  end;

  -- Drop Table
  if i > 0 then
    str := 'DROP TABLE ' || piosTableName ;
    if v > 0 then
       str := str || ' PURGE' ;
    end if;
    begin
       execute immediate str;
       CallInfo('Table ' ||  piosTableName || ' dropped.');
    exception when others then
       CallInfo(str);
       CallError('DropTable:' || SQLERRM);
    end;
  end if;

exception when others then
  callinfo('DropTable: Error:');
  CallError('DropTable:' || SQLERRM);
END DropTable;


 /*
   **=====================================================
   **
   ** Procedure: DropSynonym
   **
   ** Purpose:   Drop a DBX Synonym
   **
   **=====================================================
*/

PROCEDURE DropSynonym
(
     -- Synonym Name
     piosSynonymName  IN VARCHAR2
    ,piosSynonymOwner IN VARCHAR2 DEFAULT NULL
)
AS
 i   integer;
 str VARCHAR2(2000);
begin

  if piosSynonymOwner IS NULL then
     str := 'select count(*) from user_synonyms where synonym_name = upper(' ||
            '''' || piosSynonymName ||'''' || ')';
  else
     str := 'select count(*) from all_synonyms where synonym_name = upper(' ||
            '''' || piosSynonymName ||'''' || ')' ||
            ' and owner = ' || '''' || piosSynonymOwner || '''';
  end if;
  begin
    execute immediate str into i;
  exception when others then
    CallInfo(str);
    CallError('DropSynonym:' || SQLERRM);
  end;

  if i > 0 then
    if piosSynonymOwner IS NULL then
       str := 'DROP SYNONYM ' || piosSynonymName ;
    else
       str := 'DROP PUBLIC SYNONYM ' || piosSynonymName ;
    end if;
    begin
       execute immediate str;
       CallInfo('Synonym ' ||  piosSynonymName || ' dropped.');
    exception when others then
       CallInfo(str);
       CallError('DropSynonym:' || SQLERRM);
    end;
  end if;

exception when others then
  callinfo('DropSynonym: Error:');
  CallError('DropSynonym:' || SQLERRM);
END DropSynonym;




 /*
   **=====================================================
   **
   ** Procedure: CreateTable
   **
   ** Purpose:   Create a Table
   **
   **=====================================================
*/

PROCEDURE CreateTable
(
     -- Table Name
     piosTableName  IN VARCHAR2
    ,piosStatement  IN VARCHAR2
)
AS
 i   integer;
 str VARCHAR2(2000);
begin

  str := 'select count(*) from user_tables where table_name = upper(' ||
         '''' || piosTableName ||'''' || ')';
  begin
    execute immediate str into i;
  exception when others then
    CallInfo(str);
    CallError('CreateTable:' || SQLERRM);
  end;

  if i = 0 then
    str := piosStatement;
    begin
       execute immediate str;
       CallInfo('Table ' ||  piosTableName || ' created.');

       str := 'GRANT select ON ' || piosTableName || ' TO PUBLIC';
       execute immediate str;
       CallInfo('Table ' ||  piosTableName || ' grant SELECT to PUBLIC.');

    exception when others then
       CallInfo(str);
       -- Error for missing privilege to create a table
       if SQLCODE = -1031 then
          CallInfo('Connect as user SYS at the DBX database and the DBX HOME database and execute:');
          CallInfo('SQL> grant CREATE TABLE to SYSADM;');
       end if;
       if SQLCODE = -1950 then
          CallInfo('Define a default database other than SYSTEM or SYSAUX for user SYSADM ');
          CallInfo('and define an unlimited quota for user SYSADM. ');
          CallInfo('Connect as user SYS at the DBX database and the DBX HOME database and execute:');
          CallInfo('SQL> alter user SYSADM default tablespace <name>;');
          CallInfo('SQL> alter user SYSADM quota unlimited on <name>;');
          CallInfo('SQL> alter user SYSADM temporary tablespace <name>;');
       end if;
       CallError('CreateTable:' || SQLERRM);
    end;
  end if;

exception when others then
  callinfo('CreateTable: Error:');
  CallError('CreateTable:' || SQLERRM);
END CreateTable;


 /*
   **=====================================================
   **
   ** Procedure: CreateDBXTables
   **
   ** Purpose:   Create a Table
   **
   **=====================================================
*/

PROCEDURE CreateDBXTables
(
     -- OrganisationType
     piosOrgType   IN VARCHAR2
)
AS
 i                 integer;
 str               VARCHAR2(30000);
 storage           VARCHAR2(200) := ' ';
 tsp               VARCHAR2(30);
begin

  storage := piosOrgType;

  if trim(upper(storage)) = 'INDEX' then
     str := 'select DEFAULT_TABLESPACE from user_users';
     begin
       execute immediate str into tsp;
       storage := storage || ' OVERFLOW TABLESPACE ' || tsp;
     exception when others then
       CallInfo(str);
       CallError('CreateTable:' || SQLERRM);
     end;
  end if;

  DropSynonym('dbx_parameter');
  DropTable('dbx_parameter');
  str := 'create table dbx_parameter (';
  str := str || ' dbx_parameter_name   varchar2(30)';
  str := str || ',dbx_parameter_value  varchar2(4000) DEFAULT '' '' ';
  str := str || ',CONSTRAINT pk_dbx_parameter PRIMARY KEY (dbx_parameter_name) ';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter',str);

  DropSynonym('dbx_tables');
  DropTable('dbx_tables');
  str := 'create table dbx_tables (';
  str := str || ' dbx_table_name           varchar2(30)';
  str := str || ',dbx_column_name          varchar2(30)';
  str := str || ',dbx_create_clause        varchar2(4000)';
  str := str || ',dbx_where_condition      varchar2(4000)';
  str := str || ',dbx_where_link_condition varchar2(4000)';
  str := str || ',dbx_hint                 varchar2(4000)';
--str := str || ',dbx_storage              varchar2(4000)';
  str := str || ',dbx_storage              NUMBER  ';
  str := str || ',dbx_db_connect           varchar2(4000)';
  str := str || ',dbx_long_column          varchar2(1) DEFAULT ''N'' ';
  str := str || ',dbx_iot_table            varchar2(1) DEFAULT ''N'' ';
  str := str || ',dbx_exclude              varchar2(1) DEFAULT ''N'' ';
--str := str || ',dbx_start_date           VARCHAR2(30)';
--str := str || ',dbx_stop_date            VARCHAR2(30)';
  str := str || ',dbx_ptype                varchar2(30)';
  str := str || ',dbx_pkeycol              varchar2(30)';
  str := str || ',dbx_stype                varchar2(30)';
  str := str || ',dbx_skeycol              varchar2(30)';
  str := str || ',dbx_schema_name          varchar2(30) DEFAULT ''SYSADM'' ';
  str := str || ',CONSTRAINT pk_dbx_tables PRIMARY KEY (dbx_table_name,dbx_schema_name)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_tables',str);

  DropSynonym('dbx_part');
  DropTable('dbx_part');
  str := 'create table dbx_part (';
  str := str || ' dbx_table_name           varchar2(30)';
  str := str || ',dbx_schema_name          varchar2(30) DEFAULT ''SYSADM'' ';
  str := str || ',dbx_partition_name       varchar2(30)';
  str := str || ',dbx_check_diff           number default (0)';
  str := str || ',CONSTRAINT pk_dbx_part   PRIMARY KEY (dbx_table_name,dbx_schema_name,dbx_partition_name)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_part',str);

  DropSynonym('dbx_part_pvalues');
  DropTable('dbx_part_pvalues');
  str := 'create table dbx_part_pvalues (';
  str := str || ' dbx_table_name           varchar2(30)';
  str := str || ',dbx_schema_name          varchar2(30) DEFAULT ''SYSADM'' ';
  str := str || ',dbx_partition_name       varchar2(30)';
  str := str || ',dbx_high_value           varchar2(4000)';
  str := str || ',dbx_high_value_num       number';
  str := str || ',dbx_prev_high_value_num  number';
  str := str || ',dbx_high_value_date      date';
  str := str || ',dbx_prev_high_value_date date';
  str := str || ',CONSTRAINT pk_dbx_part_pvalues   PRIMARY KEY (dbx_table_name,dbx_schema_name,dbx_partition_name)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_part_pvalues',str);

  DropSynonym('dbx_part_svalues');
  DropTable('dbx_part_svalues');
  str := 'create table dbx_part_svalues (';
  str := str || ' dbx_table_name           varchar2(30)';
  str := str || ',dbx_partition_name       varchar2(30)';
  str := str || ',dbx_subpartition_name    varchar2(30)';
  str := str || ',dbx_high_value           varchar2(4000)';
  str := str || ',dbx_high_value_num       number';
  str := str || ',dbx_prev_high_value_num  number';
  str := str || ',dbx_high_value_date      date';
  str := str || ',dbx_prev_high_value_date date';
  str := str || ',CONSTRAINT pk_dbx_part_svalues PRIMARY KEY (dbx_table_name,dbx_partition_name,dbx_subpartition_name)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_part_svalues',str);

  DropSynonym('dbx_customer');
  DropTable('dbx_customer');
  str := 'create table dbx_customer (';
  str := str || ' CUSTOMER_ID      integer';
  str := str || ',CUSTOMER_ID_HIGH integer';
  str := str || ',CSLEVEL          varchar2(10)';
  str := str || ',CSTYPE           varchar2(10)';
  str := str || ',CSRESELLER       varchar2(10)';
  str := str || ',BILLCYCLE        varchar2(4) ';       -- Istanbul for UDR_LT
  str := str || ',PARTY_ROLE_ID    integer     ';       -- Istanbul for Prepaid
  str := str || ',CONSTRAINT PK_DBX_CUSTOMER PRIMARY KEY (CUSTOMER_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_customer',str);

  DropSynonym('dbx_dealer');
  DropTable('dbx_dealer');
  str := 'create table dbx_dealer   (';
  str := str || ' CUSTOMER_ID      integer';
  str := str || ',CUSTOMER_ID_HIGH integer';
  str := str || ',CSLEVEL          varchar2(10)';
  str := str || ',CSTYPE           varchar2(10)';
  str := str || ',CSRESELLER       varchar2(10)';
  str := str || ',BILLCYCLE        varchar2(4) ';       -- Istanbul for UDR_LT
  str := str || ',CONSTRAINT PK_DBX_DEALER   PRIMARY KEY (CUSTOMER_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_dealer',str);

  DropSynonym('dbx_contract_ohxact');
  DropTable('dbx_contract_ohxact');
  str := 'create table dbx_contract_ohxact (';
  str := str || ' OHXACT           integer';
  str := str || ',CONSTRAINT PK_DBX_CONTRACT_OHXACT PRIMARY KEY (OHXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_contract_ohxact',str);

  DropSynonym('dbx_orderhdr_customer');
  DropTable('dbx_orderhdr_customer');
  str := 'create table dbx_orderhdr_customer (';
  str := str || ' CUSTOMER_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_ORDERHDR_CUSTOMER PRIMARY KEY (CUSTOMER_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_orderhdr_customer',str);

  DropSynonym('dbx_contract');
  DropTable('dbx_contract');
  str := 'create table dbx_contract (';
  str := str || ' CO_ID            integer';
  str := str || ',CUSTOMER_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_CONTRACT PRIMARY KEY (CO_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_contract',str);

  DropSynonym('dbx_tickler');
  DropTable('dbx_tickler');
  str := 'create table dbx_tickler (';
  str := str || ' CUSTOMER_ID      integer';
  str := str || ',TICKLER_NUMBER   integer';
  str := str || ',CREATED_DATE     date ';
  str := str || ',CONSTRAINT PK_DBX_TICKLER PRIMARY KEY (CUSTOMER_ID,TICKLER_NUMBER,CREATED_DATE)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_tickler',str);

  DropSynonym('dbx_request');
  DropTable('dbx_request');
  str := 'create table dbx_request (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request',str);

  DropSynonym('dbx_request_1');
  DropTable('dbx_request_1');
  str := 'create table dbx_request_1 (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST_1 PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request_1',str);

  DropSynonym('dbx_request_2');
  DropTable('dbx_request_2');
  str := 'create table dbx_request_2 (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST_2 PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request_2',str);

  DropSynonym('dbx_request_3');
  DropTable('dbx_request_3');
  str := 'create table dbx_request_3 (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST_3 PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request_3',str);

  DropSynonym('dbx_request_4');
  DropTable('dbx_request_4');
  str := 'create table dbx_request_4 (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST_4 PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request_4',str);

  DropSynonym('dbx_request_5');
  DropTable('dbx_request_5');
  str := 'create table dbx_request_5 (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_REQUEST_5 PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_request_5',str);

  DropSynonym('dbx_port');
  DropTable('dbx_port');
  str := 'create table dbx_port (';
  str := str || ' PORT_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_PORT PRIMARY KEY (PORT_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_port',str);

  DropSynonym('dbx_dn');
  DropTable('dbx_dn');
  str := 'create table dbx_dn (';
  str := str || ' DN_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_DN PRIMARY KEY (DN_ID) ';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_dn',str);

  DropSynonym('dbx_sm');
  DropTable('dbx_sm');
  str := 'create table dbx_sm (';
  str := str || ' SM_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_SM PRIMARY KEY (SM_ID) ';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_sm',str);

  DropSynonym('dbx_orderhdr');
  DropTable('dbx_orderhdr');
  str := 'create table dbx_orderhdr (';
  str := str || ' OHXACT    INTEGER';
  str := str || ',OHENTDATE DATE         DEFAULT (SYSDATE)';
  str := str || ',OHBILLSEQNO INTEGER ';
  str := str || ',CONSTRAINT PK_DBX_ORDERHDR PRIMARY KEY (OHXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_orderhdr',str);
  execute immediate 'CREATE INDEX i_dbx_orderhdr_ohentdate  ON dbx_orderhdr (ohentdate,ohxact) NOLOGGING';

  DropSynonym('dbx_orderhdr_1');
  DropTable('dbx_orderhdr_1');
  str := 'create table dbx_orderhdr_1 (';
  str := str || ' OHXACT    INTEGER';
  str := str || ',OHENTDATE DATE         DEFAULT (SYSDATE)';
  str := str || ',OHBILLSEQNO INTEGER ';
  str := str || ',CONSTRAINT PK_DBX_ORDERHDR_1 PRIMARY KEY (OHXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_orderhdr_1',str);
  execute immediate 'CREATE INDEX i_dbx_orderhdr_ohentdate_1  ON dbx_orderhdr_1 (ohentdate,ohxact) NOLOGGING';

  DropSynonym('dbx_orderhdr_2');
  DropTable('dbx_orderhdr_2');
  str := 'create table dbx_orderhdr_2 (';
  str := str || ' OHXACT    INTEGER';
  str := str || ',OHENTDATE DATE         DEFAULT (SYSDATE)';
  str := str || ',OHBILLSEQNO INTEGER ';
  str := str || ',CONSTRAINT PK_DBX_ORDERHDR_2 PRIMARY KEY (OHXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_orderhdr_2',str);


  DropSynonym('dbx_cashreceipts');
  DropTable('dbx_cashreceipts');
  str := 'create table dbx_cashreceipts (';
  str := str || ' CUSTOMER_ID  INTEGER';
  str := str || ',CAXACT       INTEGER';
  str := str || ',CONSTRAINT PK_DBX_cashreceipts PRIMARY KEY (CUSTOMER_ID,CAXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_cashreceipts',str);

  execute immediate 'CREATE INDEX i_dbx_cashreceipts_caxact ON dbx_cashreceipts (caxact) NOLOGGING';

  DropSynonym('dbx_cashdetail');
  DropTable('dbx_cashdetail');
  str := 'create table dbx_cashdetail   (';
  str := str || ' CADXACT      INTEGER';
  str := str || ',CADOXACT     INTEGER';
  str := str || ',CONSTRAINT PK_DBX_cashdetail PRIMARY KEY (CADXACT,CADOXACT)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_cashdetail',str);

  execute immediate 'CREATE INDEX i_dbx_cashdetail_cado ON DBX_CASHDETAIL (CADOXACT,CADXACT) NOLOGGING';


  DropSynonym('dbx_parameter_value');
  DropTable('dbx_parameter_value');
  str := 'create table dbx_parameter_value  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value',str);

  DropSynonym('dbx_parameter_value_1');
  DropTable('dbx_parameter_value_1');
  str := 'create table dbx_parameter_value_1  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_1 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_1',str);

  DropSynonym('dbx_parameter_value_2');
  DropTable('dbx_parameter_value_2');
  str := 'create table dbx_parameter_value_2  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_2 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_2',str);

  DropSynonym('dbx_parameter_value_3');
  DropTable('dbx_parameter_value_3');
  str := 'create table dbx_parameter_value_3  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_3 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_3',str);

  DropSynonym('dbx_parameter_value_4');
  DropTable('dbx_parameter_value_4');
  str := 'create table dbx_parameter_value_4  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_4 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_4',str);

  DropSynonym('dbx_parameter_value_5');
  DropTable('dbx_parameter_value_5');
  str := 'create table dbx_parameter_value_5  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_5 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_5',str);

  DropSynonym('dbx_parameter_value_6');
  DropTable('dbx_parameter_value_6');
  str := 'create table dbx_parameter_value_6  (';
  str := str || ' PRM_VALUE_ID INTEGER';
  str := str || ',CONSTRAINT PK_DBX_parameter_value_6 PRIMARY KEY (PRM_VALUE_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_parameter_value_6',str);

  DropSynonym('dbx_mpurhtab');
  DropTable('dbx_mpurhtab');
  str := 'create table dbx_mpurhtab  (';
  str := str || ' HPLCODE  INTEGER    ';
  str := str || ',VPLCODE  INTEGER    ';
  str := str || ',FLSQN    INTEGER    ';
  str := str || ',CONSTRAINT pk_dbx_mpurhtab PRIMARY KEY (HPLCODE,VPLCODE,FLSQN)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_mpurhtab',str);

  DropSynonym('dbx_document_1');
  DropTable('dbx_document_1');
  str := 'create table dbx_document_1  (';
  str := str || ' CUSTOMER_ID  INTEGER';
  str := str || ',DOCUMENT_ID  INTEGER';
  str := str || ',OHXACT       INTEGER';
  str := str || ',CONSTRAINT PK_DBX_DOCUMENT_1 PRIMARY KEY (DOCUMENT_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_document_1',str);

  execute immediate 'CREATE INDEX i_dbx_document_1_ohxact ON DBX_DOCUMENT_1 (OHXACT) NOLOGGING';

  DropSynonym('dbx_document');
  DropTable('dbx_document');
  str := 'create table dbx_document  (';
  str := str || ' CUSTOMER_ID  INTEGER';
  str := str || ',DOCUMENT_ID  INTEGER';
  str := str || ',OHXACT       INTEGER';
  str := str || ',CONSTRAINT PK_DBX_DOCUMENT PRIMARY KEY (DOCUMENT_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_document',str);

  DropSynonym('dbx_ffcode');
  DropTable('dbx_ffcode');
  str := 'create table dbx_ffcode    (';
  str := str || ' FFCODE       INTEGER';
  str := str || ',CONSTRAINT PK_DBX_FFCODE PRIMARY KEY (FFCODE)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_ffcode',str);

  DropSynonym('dbx_billseqno');
  DropTable('dbx_billseqno');
  str := 'create table dbx_billseqno (';
  str := str || 'BILLSEQNO INTEGER ';
  str := str || ',CONSTRAINT PK_DBX_BILLSEQNO PRIMARY KEY (BILLSEQNO)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_billseqno',str);

  DropSynonym('dbx_crh_request');
  DropTable('dbx_crh_request');
  str := 'create table dbx_crh_request (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_CRH_REQUEST PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_crh_request',str);

  DropSynonym('dbx_thufitab');
  DropTable('dbx_thufitab');
  str := 'create table dbx_thufitab (';
  str := str || ' FILE_ID      integer';
  str := str || ',FT_ID        integer';
  str := str || ') ';

  CreateTable('dbx_thufitab',str);

  execute immediate 'CREATE INDEX i_dbx_thufitab_file_id ON DBX_THUFITAB (FILE_ID) NOLOGGING';

  DropSynonym('dbx_thufotab');
  DropTable('dbx_thufotab');
  str := 'create table dbx_thufotab (';
  str := str || ' FILE_ID      integer';
  str := str || ',FT_ID        integer';
  str := str || ') ';

  CreateTable('dbx_thufotab',str);

  execute immediate 'CREATE INDEX i_dbx_thufotab_file_id ON DBX_THUFOTAB (FILE_ID) NOLOGGING';

  DropSynonym('dbx_log');
  DropTable('dbx_log');
  str := 'CREATE TABLE dbx_log (';
  str := str || ' dbx_table_name   varchar2(90)';
  str := str || ',action           varchar2(90)   DEFAULT ''CreateBSCSTables'' ';
  str := str || ',message          varchar2(4000) DEFAULT '' '' ';
  str := str || ',para1            varchar2(4000) DEFAULT '' '' ';
  str := str || ',para2            varchar2(4000) DEFAULT '' '' ';
  str := str || ',para3            varchar2(4000) DEFAULT '' '' ';
  str := str || ',starttime        date';
  str := str || ',finishtime       date';
  str := str || ',CONSTRAINT pk_dbx_log PRIMARY KEY (dbx_table_name,action) ';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_log',str);

  DropSynonym('dbx_lctransaction');
  DropTable('dbx_lctransaction');
  str := 'create table dbx_lctransaction (';
  str := str || ' TRANSACTION_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_LCTRANSACTION PRIMARY KEY (TRANSACTION_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_lctransaction',str);

  DropSynonym('dbx_payment');
  DropTable('dbx_payment');
  str := 'create table dbx_payment (';
  str := str || ' PAYMENT_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_PAYMENT PRIMARY KEY (PAYMENT_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_payment',str);

  DropSynonym('dbx_portreq');
  DropTable('dbx_portreq');
  str := 'create table dbx_portreq (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_PORTREQ PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_portreq',str);

  DropSynonym('dbx_crpreq');
  DropTable('dbx_crpreq');
  str := 'create table dbx_crpreq (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_CRPREQ PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_crpreq',str);

  DropSynonym('dbx_cdsreq');
  DropTable('dbx_cdsreq');
  str := 'create table dbx_cdsreq (';
  str := str || ' REQUEST_ID      integer';
  str := str || ',CONSTRAINT PK_DBX_CDSREQ PRIMARY KEY (REQUEST_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_cdsreq',str);

  DropSynonym('dbx_payment_prop');
  DropTable('dbx_payment_prop');
  str := 'create table dbx_payment_prop (';
  str := str || ' PAY_PROPOSAL_ID integer';
  str := str || ',CONSTRAINT PK_DBX_PAYMENT_PROP PRIMARY KEY (PAY_PROPOSAL_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_payment_prop',str);

  DropSynonym('dbx_prm_mech_set');
  DropTable('dbx_prm_mech_set');
  str := 'create table dbx_prm_mech_set (';
  str := str || ' MECH_SET_ID integer';
  str := str || ',CONSTRAINT PK_DBX_PRM_MECH_SET PRIMARY KEY (MECH_SET_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_prm_mech_set',str);

  DropSynonym('dbx_fup_accounts');
  DropTable('dbx_fup_accounts');
  str := 'create table dbx_fup_accounts (';
  str := str || ' ACCOUNT_ID integer';
  str := str || ',CONSTRAINT PK_DBX_FUP_ACCOUNTS PRIMARY KEY (ACCOUNT_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_fup_accounts',str);

  DropSynonym('dbx_billcycle_group');
  DropTable('dbx_billcycle_group');
  str := 'create table dbx_billcycle_group (';
  str := str || ' BILLCYCLE_GROUP_ID   integer';
  str := str || ',CONSTRAINT PK_DBX_BILLCYCLE_GROUP PRIMARY KEY (BILLCYCLE_GROUP_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_billcycle_group',str);

  DropSynonym('dbx_family_group');
  DropTable('dbx_family_group');
  str := 'create table dbx_family_group (';
  str := str || ' FAMILY_GROUP_ID   integer';
  str := str || ',CONSTRAINT PK_DBX_FAMILY_GROUP PRIMARY KEY (FAMILY_GROUP_ID)';
  str := str || ') ORGANIZATION ' || storage;

  CreateTable('dbx_family_group',str);

exception when others then
  callinfo('CreateDBXTables: Error:');
  CallError('CreateDBXTables: ' || SQLERRM);
END CreateDBXTables;


 /*
   **=====================================================
   **
   ** Procedure: GatherTableStat
   **
   ** Purpose:   Gather Table statistics for DBX tables
   **
   **=====================================================
*/


PROCEDURE GatherTableStat
(
     -- OrganisationType
     piosSchema    IN VARCHAR2
    ,piosTablename IN VARCHAR2
)
AS
 str                  VARCHAR2(30000);
 SynonymTranslation   EXCEPTION;
 pragma exception_init (SynonymTranslation, -980);
begin
       str := 'begin ' || chr(10);
       str :=  str || '  SYS.DBMS_STATS.GATHER_TABLE_STATS (ownname => ' || '''' || piosSchema || '''' ||
                                                        ' , tabname => ' || '''' || piosTablename || '''' ||
                                                        ' , granularity => ''ALL'', cascade => TRUE);' || chr(10);
       str :=  str || 'end;';
       ShowStatement(str);
       begin
          execute immediate str;
       exception
       when SynonymTranslation then -- Error found in Oracle 11 with DBMS_STAT package Oracle bugfix 3431498
            begin
               DropSynonym ( piosSynonymName => 'EXTRACT'   , piosSynonymOwner => 'PUBLIC');
               DropSynonym ( piosSynonymName => 'EXISTSNODE', piosSynonymOwner => 'PUBLIC');
               DropSynonym ( piosSynonymName => 'XMLCONCAT' , piosSynonymOwner => 'PUBLIC');
               ShowStatement(str);
               execute immediate str;
               callinfo('GatherTableStat: DBMS_STATS at ' ||  piosTablename || ' executed.');
            exception
            when others then
               callinfo('GatherTableStat: DBMS_STATS at ' ||  piosTablename || ':' || substr(SQLERRM,1,20) );
               str := 'analyze table ' || piosTablename || ' compute statistics';
               ShowStatement(str);
               execute immediate str;
            end;
       when others then
          callinfo('GatherTableStat: DBMS_STATS at ' ||  piosTablename || ':' || substr(SQLERRM,1,20) );
          str := 'analyze table ' || piosTablename || ' compute statistics';
          ShowStatement(str);
          execute immediate str;
       end;

exception when others then
  callinfo('GatherTableStat: Schema: ' || piosSchema );
  callinfo('GatherTableStat: Table: ' || piosTablename );
  callinfo('GatherTableStat: Error:' || substr(SQLERRM,1,200) );
  CallError('GatherTableStat: ' || SQLERRM);
END GatherTableStat;




 /*
   **=====================================================
   **
   ** Procedure: GatherDBXTableStat
   **
   ** Purpose:   Gather Table statistics for DBX tables
   **
   **=====================================================
*/

PROCEDURE GatherDBXTableStat
(
     -- OrganisationType
     piosSchema    IN VARCHAR2
    ,piosTablename IN VARCHAR2 DEFAULT NULL
)
AS
 i                 integer;
 losSchema         VARCHAR2(30);
 losTablename      VARCHAR2(30);
 losGather         VARCHAR2(300);
 str               VARCHAR2(30000);
begin
 losGather    := GetParameter('GATHER_DBX_TABLE_STAT');

 if trim(losGather) = '1' then
    losSchema    := trim(upper(piosSchema));
    losTablename := trim(upper(piosTablename));
    str := 'select count(*) +1 from DBX_LOG where ACTION like ''GatherTableStat%'' ';
    execute immediate str into i;

    if piosTablename is not null then
       DBXLogStart ( piosTableName  => piosTablename
                    ,piosActionName => 'GatherTableStat_' || trim(TO_CHAR(i,'09999'))
                    ,piosMessage    => 'Gather Statistic'
                    ,piosPara1      => 'SYS.DBMS_STATS.GATHER_TABLE_STATS'
                    ,piosPara2      => piosSchema
                    ,piosPara3      => piosTablename);
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       DBXLogStop  ( piosTableName  => piosTablename
                    ,piosActionName => 'GatherTableStat_' || trim(TO_CHAR(i,'09999'))
                    ,piosMessage    => 'SUCCESS' );
    else
       DBXLogStart ( piosTableName  => 'GatherTableStatALL'
                    ,piosActionName => 'GatherTableStatALL_' || trim(TO_CHAR(i,'09999'))
                    ,piosMessage    => 'Statistic'
                    ,piosPara1      => 'SYS.DBMS_STATS.GATHER_TABLE_STATS');
       losTablename := 'DBX_BILLSEQNO';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CASHDETAIL';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CASHRECEIPTS';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CDSREQ';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CONTRACT';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CONTRACT_OHXACT';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CRH_REQUEST';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CRPREQ';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_CUSTOMER';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_DEALER';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_DN';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_DOCUMENT';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_DOCUMENT_1';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_FFCODE';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
--     begin
--        losTablename := 'DBX_INVALID_OBJECTS';
--        GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
--     exception when others then null;
--     end;
--     losTablename := 'DBX_LOG';
--     GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_MPURHTAB';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_ORDERHDR';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_ORDERHDR_1';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_ORDERHDR_2';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_ORDERHDR_CUSTOMER';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PART';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PART_PVALUES';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PART_SVALUES';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
--     losTablename := 'DBX_PARAMETER';
--     GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_1';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_2';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_3';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_4';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_5';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PARAMETER_VALUE_6';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PAYMENT';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PAYMENT_PROP';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PORT';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PORTREQ';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_REQUEST_1';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_REQUEST_2';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_REQUEST_3';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_REQUEST_4';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_REQUEST_5';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_SM';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_TABLES';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_TICKLER';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_THUFITAB';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_THUFOTAB';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
--     begin
--        losTablename := 'DBX_SELECTION';
--        GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
--     exception when others then null;
--     end;
       losTablename := 'DBX_LCTRANSACTION';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_PRM_MECH_SET';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_FUP_ACCOUNTS';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_BILLCYCLE_GROUP';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       losTablename := 'DBX_FAMILY_GROUP';
       GatherTableStat(piosSchema => losSchema , piosTablename => losTablename);
       DBXLogStop  ( piosTableName  => 'GatherTableStatALL'
                    ,piosActionName => 'GatherTableStatALL_' || trim(TO_CHAR(i,'09999'))
                    ,piosMessage    => 'SUCCESS' );
    end if;
 else
    callinfo('GatherDBXTableStat: Disabled!');
 end if;

exception when others then
  if piosTablename is not null then
     callinfo('GatherDBXTableStat: Table: ' || piosTablename );
  else
     callinfo('GatherDBXTableStat: Schema: ' || piosSchema );
  end if;
  callinfo('GatherDBXTableStat: Error:' || substr(SQLERRM,1,200) );
  callinfo('A workaround for this error could be to set parameter GATHER_DBX_TABLE_STAT to 0 in dbextract.cfg or default.cfg');
  CallError('GatherDBXTableStat: ' || SQLERRM);
END GatherDBXTableStat;



 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSColumnDefaults
   **
   ** Purpose:   Copy BSCS default column values from BSCS DB and UDR DB to DBX DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSColumnDefaults
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 v_table_name              VARCHAR2(30);
 v_column_name             VARCHAR2(30);
 v_default                 VARCHAR2(25000);
 str                       VARCHAR2(30000);
 v_count                   INTEGER;
begin

  v_select_statement := 'SELECT  c.table_name,c.column_name,c.DATA_DEFAULT ';
  v_select_statement := v_select_statement || 'FROM    dba_tab_columns';
  v_select_statement := v_select_statement || '@' || piosLinkName || ' c,';
  v_select_statement := v_select_statement || '        dba_tables';
  v_select_statement := v_select_statement || '@' || piosLinkName || ' t ';
  v_select_statement := v_select_statement || 'WHERE   data_default IS NOT NULL ';
  v_select_statement := v_select_statement || ' AND  c.OWNER      = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' AND  c.OWNER      = t.OWNER ';
  v_select_statement := v_select_statement || ' AND  c.TABLE_NAME = t.TABLE_NAME ';
/*
 FOR ORACLE 8.1.6
  v_select_statement := v_select_statement || 'AND     table_name IN (';
  v_select_statement := v_select_statement || ' SELECT table_name FROM dba_tables u, dbx_tables d ';
  v_select_statement := v_select_statement || ' WHERE  u.table_name = d.dbx_table_name ';
  v_select_statement := v_select_statement || ' AND    u.owner = ' || '''' || 'SYSADM' || '''' ;
  v_select_statement := v_select_statement || ' AND    upper(d.dbx_db_connect) = upper(' ||
                                              '''' || piosDBConnect || '''' || ')  )';
*/
  v_select_statement := v_select_statement || 'ORDER BY table_name';
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , v_column_name , v_default ;
      EXIT WHEN cur%NOTFOUND;

      str := 'ALTER TABLE ' || v_table_name || ' MODIFY ( ' || v_column_name || ' DEFAULT ' || v_default || ' )';
      begin
         execute immediate str;
         CallInfo('Default for Column: ' || v_table_name || '.' || v_column_name );
       exception when others then
         if sqlcode != -942 then
            CallInfo('Error for Column: ' || v_table_name || '.' || v_column_name );
            CallInfo(substr(SQLERRM,1,250));
            CallInfo(substr(str,1,250) );
         else
            CallInfo('Does not exist    : ' || v_table_name || '.' || v_column_name );
         end if;
       end;

  END LOOP;
  CLOSE cur;

exception when others then
  callinfo('CreateBSCSColumnDefaults: Error:');
  CallError(SQLERRM);
END CreateBSCSColumnDefaults;


 /*
   **=====================================================
   **
   ** Procedure: SetStorage
   **
   ** Purpose:   Set Storage in DBX_TABLES and CreateClauses
   **
   **=====================================================
*/


PROCEDURE SetStorage
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 v_owner                   VARCHAR2(30);
 v_table_name              VARCHAR2(30);
 v_column_name             VARCHAR2(30);
 v_default                 VARCHAR2(25000);
 str                       VARCHAR2(30000);
 vialink                   VARCHAR2(1);
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 v_where                   VARCHAR2(4000);
 v_storage                 NUMBER;
 vspace                    NUMBER;
 cpctsize                  NUMBER;
 PCTSIZE                   VARCHAR2(4000);
 SPCTSIZE                  VARCHAR2(4000);
 ostorage                  VARCHAR2(4000);
 dbx_mode                  VARCHAR2(4000);
begin


  vialink                  := GetParameter(piosVIALink);
  vdbBSCS                  := GetParameter(piosLinkName);
  vdbconBSCS               := GetParameter(piosDBConnect);
  PCTSIZE                  := GetParameter('PCTSIZE');
  SPCTSIZE                 := GetParameter('SPCTSIZE');
  dbx_mode                 := GetParameter('DBX_MODE');
--CallInfo('SetStorage: ' || vdbBSCS || '.' ||  piosLinkName  );
--CallInfo('SetStorage: ' || vdbconBSCS || '.' ||  piosDBConnect  );
--CallInfo('SetStorage: ' || PCTSIZE    || '-' ||  SPCTSIZE       );

IF trim(upper(dbx_mode)) = 'D' THEN
 CallInfo('SetStorage: dbx_mode: ' || dbx_mode );
 IF PCTSIZE != '0' OR SPCTSIZE != '0' THEN
 /*
  * All Normal Tables and Partitioned Tables
  */
  v_select_statement := 'SELECT DBA.owner,DBA.table_name,DBX.DBX_WHERE_CONDITION,DBA.bytes ';
  v_select_statement := v_select_statement || 'FROM ( ' ;
  v_select_statement := v_select_statement || 'SELECT  TAB.owner, TAB.table_name,  sum(SEG.bytes) Bytes ' ;
  v_select_statement := v_select_statement || 'FROM    dba_tables' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' TAB ';
  v_select_statement := v_select_statement || ' ,      dba_segments' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' SEG ';
  v_select_statement := v_select_statement || ' WHERE TAB.TABLE_NAME = SEG.SEGMENT_NAME ' ;
  v_select_statement := v_select_statement || '   AND TAB.OWNER = SEG.OWNER ' ;
  v_select_statement := v_select_statement || '   AND TAB.OWNER = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || '   AND TAB.IOT_TYPE IS NULL ';
  v_select_statement := v_select_statement || ' GROUP BY TAB.owner,TAB.table_name ';
  v_select_statement := v_select_statement || ' HAVING sum(SEG.bytes) > 100000 ) DBA ';
  v_select_statement := v_select_statement || ' ,      dbx_tables' ;
  v_select_statement := v_select_statement || ' DBX ';
  v_select_statement := v_select_statement || 'WHERE DBA.OWNER      = DBX.DBX_SCHEMA_NAME ';
  v_select_statement := v_select_statement || 'AND   DBA.TABLE_NAME = DBX.DBX_TABLE_NAME ';
  v_select_statement := v_select_statement || 'AND   DBX.DBX_STORAGE IS NULL ';
  v_select_statement := v_select_statement || 'AND   DBX.DBX_DB_CONNECT = ' || '''' || vdbconBSCS || '''' || ' ' ;
  v_select_statement := v_select_statement || 'AND (DBX.DBX_WHERE_CONDITION IS NULL OR ';
  v_select_statement := v_select_statement || ' 0 = INSTR(DBX.DBX_WHERE_CONDITION,' ||''''||'1 = 2'||''''|| ') ) ';
  v_select_statement := v_select_statement || 'AND     DBX.dbx_exclude = ' ||''''||'N'||''''|| ' ';
  v_select_statement := v_select_statement || ' ORDER BY DBA.owner,DBA.table_name ';

  showStatement(v_select_statement);

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_owner,v_table_name , v_where, v_storage ;
      EXIT WHEN cur%NOTFOUND;

      ostorage := NULL;
      IF v_where IS NULL then
           IF trim(spctsize) != '0' THEN
           /*
            * Full tables
            */
            cpctsize := TO_NUMBER(TRIM(spctsize));
            v_storage :=  v_storage * cpctsize/100 ;
             -- If 100 % then no change
            IF cpctsize = 100 THEN
              vspace := v_storage;
            ELSE
              vspace := round(v_storage , - length(TO_CHAR(round(v_storage) )) +1 ) ;
            END IF;

            -- If less than 100000 no storage output
            IF vspace > 100000 AND vspace < 1048576 THEN
               ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
                  ' NEXT ' || TO_CHAR(vspace) || ' ) ';
            END IF;
            IF vspace >= 1048576 THEN
               vspace := round(vspace/1048576);
               ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
                           ' M NEXT ' || TO_CHAR(vspace) || ' M ) ';
            END IF;
           END IF;

      ELSE
           IF trim(pctsize) != '0' THEN
           /*
            * Size reduced tables
            */
            cpctsize := TO_NUMBER(TRIM(pctsize));
            v_storage :=  v_storage * cpctsize/100 ;
             -- If 100 % then no change
            IF cpctsize = 100 THEN
              vspace := v_storage;
            ELSE
              vspace := round(v_storage , - length(TO_CHAR(round(v_storage) )) +1 ) ;
            END IF;

            -- If less than 100000 no storage output
            IF vspace > 100000 AND vspace < 1048576 THEN
               ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
                  ' NEXT ' || TO_CHAR(vspace) || ' ) ';
            END IF;
            IF vspace >= 1048576 THEN
               vspace := round(vspace/1048576);
               ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
                           ' M NEXT ' || TO_CHAR(vspace) || ' M ) ';
            END IF;
           END IF;

      END IF;

     /*
      * Update DBX_TABLES
      */
      IF ostorage IS NOT NULL THEN
       str := 'UPDATE dbx_tables SET DBX_STORAGE = ' || TO_CHAR(v_storage) || ' ';
       str := str || ', DBX_CREATE_CLAUSE = REPLACE(DBX_CREATE_CLAUSE,' || '''' || ' UNRECOVERABLE ' || '''';
       str := str || ',' || '''' ||' UNRECOVERABLE ' || ostorage  || '''' ||  ')';
       str := str || ' WHERE DBX_TABLE_NAME = ' || '''' || v_table_name || '''' ;
       str := str || '   AND DBX_SCHEMA_NAME = ' || '''' || v_owner      || '''' ;

       begin
          execute immediate str;
          CallInfo('Default storage: ' || v_owner || '.' || v_table_name || ':' || TO_CHAR(vspace) );
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      END IF;


      commit;
  END LOOP;
  CLOSE cur;

 /*
  * All IOT  Tables and Partitioned IOT Tables
  * Ignore OVERFLOW Segments
  */
-- Not required because IOT tables are not copied in direct mode , they are always exported / imported
--v_select_statement := 'SELECT  TAB.owner,TAB.table_name, DBX.DBX_WHERE_CONDITION, sum(SEG.bytes) ';
--v_select_statement := v_select_statement || 'FROM    dba_tables' ;
--if vialink = 'Y' then
--   v_select_statement :=  v_select_statement || '@' || vdbBSCS;
--end if;
--v_select_statement := v_select_statement || ' TAB ';
--v_select_statement := v_select_statement || ',       dba_indexes' ;
--if vialink = 'Y' then
--   v_select_statement :=  v_select_statement || '@' || vdbBSCS;
--end if;
--v_select_statement := v_select_statement || ' IND ';
--v_select_statement := v_select_statement || ' ,      dba_segments' ;
--if vialink = 'Y' then
--   v_select_statement :=  v_select_statement || '@' || vdbBSCS;
--end if;
--v_select_statement := v_select_statement || ' SEG ';
--v_select_statement := v_select_statement || ' ,      dbx_tables' ;
--v_select_statement := v_select_statement || ' DBX ';
--v_select_statement := v_select_statement || 'WHERE   TAB.TABLE_NAME = IND.TABLE_NAME ';
--v_select_statement := v_select_statement || 'AND     TAB.OWNER      = DBX.DBX_SCHEMA_NAME ';
--v_select_statement := v_select_statement || 'AND     IND.INDEX_NAME = SEG.SEGMENT_NAME ';
--v_select_statement := v_select_statement || 'AND     TAB.TABLE_NAME = DBX.DBX_TABLE_NAME ';
--v_select_statement := v_select_statement || 'AND     TAB.IOT_TYPE IS NOT NULL  ';
--v_select_statement := v_select_statement || 'AND     TAB.TABLESPACE_NAME IS NULL  ';
--v_select_statement := v_select_statement || 'AND     DBX.DBX_STORAGE IS NULL ';
--v_select_statement := v_select_statement || 'AND     DBX.DBX_DB_CONNECT = ' || '''' || vdbconBSCS || '''' || ' ' ;
--v_select_statement := v_select_statement || 'AND (DBX.DBX_WHERE_CONDITION IS NULL OR ';
--v_select_statement := v_select_statement || ' 0 = INSTR(DBX.DBX_WHERE_CONDITION,' ||''''||'1 = 2'||''''|| ') ) ';
--v_select_statement := v_select_statement || 'AND     DBX.dbx_exclude = ' ||''''||'N'||''''|| ' ';
--v_select_statement := v_select_statement || ' GROUP BY TAB.owner, TAB.table_name, DBX.DBX_WHERE_CONDITION ';
--
--showStatement(v_select_statement);
--
--OPEN cur FOR v_select_statement ;
--LOOP
--    FETCH cur INTO v_owner, v_table_name , v_where, v_storage ;
--    EXIT WHEN cur%NOTFOUND;
--
--    ostorage := NULL;
--    IF v_where IS NULL then
--         IF trim(spctsize) != '0' THEN
--         /*
--          * Full tables
--          */
--          cpctsize := TO_NUMBER(TRIM(spctsize));
--          v_storage :=  v_storage * cpctsize/100 ;
--           -- If 100 % then no change
--          IF cpctsize = 100 THEN
--            vspace := v_storage;
--          ELSE
--            vspace := round(v_storage , - length(TO_CHAR(round(v_storage) )) +1 ) ;
--          END IF;
--
--          -- If less than 50000 no storage output
--          IF vspace > 50000 AND vspace < 1048576 THEN
--             ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
--                ' NEXT ' || TO_CHAR(vspace) || ' ) ';
--          END IF;
--          IF vspace >= 1048576 THEN
--             vspace := round(vspace/1048576);
--             ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
--                         ' M NEXT ' || TO_CHAR(vspace) || ' M ) ';
--          END IF;
--         END IF;
--    ELSE
--         IF trim(pctsize) != '0' THEN
--         /*
--          * Size reduced tables
--          */
--          cpctsize := TO_NUMBER(TRIM(pctsize));
--          v_storage :=  v_storage * cpctsize/100 ;
--           -- If 100 % then no change
--          IF cpctsize = 100 THEN
--            vspace := v_storage;
--          ELSE
--            vspace := round(v_storage , - length(TO_CHAR(round(v_storage) )) +1 ) ;
--          END IF;
--
--          -- If less than 50000 no storage output
--          IF vspace > 50000 AND vspace < 1048576 THEN
--             ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
--                ' NEXT ' || TO_CHAR(vspace) || ' ) ';
--          END IF;
--          IF vspace >= 1048576 THEN
--             vspace := round(vspace/1048576);
--             ostorage := 'STORAGE (INITIAL ' || TO_CHAR(vspace) ||
--                         ' M NEXT ' || TO_CHAR(vspace) || ' M ) ';
--          END IF;
--         END IF;
--
--    END IF;
--
--   /*
--    * Update DBX_TABLES
--    */
--    IF ostorage IS NOT NULL THEN
--     str := 'UPDATE dbx_tables SET DBX_STORAGE = ' || TO_CHAR(v_storage) || ' ';
--     str := str || ', DBX_CREATE_CLAUSE = REPLACE(DBX_CREATE_CLAUSE,' || '''' || ' UNRECOVERABLE ' || '''';
--     str := str || ',' || '''' ||' UNRECOVERABLE ' || ostorage  || '''' ||  ')';
--     str := str || ' WHERE DBX_TABLE_NAME = ' || '''' || v_table_name || '''' ;
--     str := str || '   AND DBX_SCHEMA_NAME = ' || '''' || v_owner      || '''' ;
--
--     begin
--        execute immediate str;
--        CallInfo('Default storage: ' || v_owner || '.' || v_table_name || ':' || TO_CHAR(vspace) );
--     exception when others then
--        CallInfo(substr(SQLERRM,1,250));
--        CallInfo(substr(str,1,250) );
--     end;
--    END IF;
--
--
--    commit;
--END LOOP;
--CLOSE cur;


 END IF;
ELSE
  CallInfo('SetStorage: dbx_mode: ' || dbx_mode );
END IF;

exception when others then
  callinfo('SetStorage: Error:');
  CallError(SQLERRM);
END SetStorage;


 /*
   **=====================================================
   **
   ** Procedure: CreateBSCSUser
   **
   ** Purpose:   Copy Database User from BSCS DB
   **
   **=====================================================
*/


PROCEDURE CreateBSCSUser
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);
 v_select                  VARCHAR2(30000);

 str                       VARCHAR2(30000);
 v_user_name               VARCHAR2(30);
 v_password                VARCHAR2(60);
 start_date                VARCHAR2(30);
 stop_date                 VARCHAR2(30);
 diff_time                 INTEGER;
 DEFTSP                    VARCHAR2(30);
 TMPTSP                    VARCHAR2(30);
 vdbBSCS                   VARCHAR2(4000);
 tester                    INTEGER;
begin

  vdbBSCS          := GetParameter('vdbBSCS');

  -- Get current settings in DBX HOME
  str := 'SELECT DEFAULT_TABLESPACE DEFTSP ,TEMPORARY_TABLESPACE ';
  str := str || ' FROM DBA_USERS';
  str := str || ' WHERE  USERNAME = ' || '''' || 'SYSADM' || '''' ;

  begin
     execute immediate str into DEFTSP , TMPTSP;
  exception when others then
     callinfo('CreateBSCSUser: Error:');
     CallInfo(str);
     -- Error for missing privilege to select dictionary view
     if SQLCODE = -942 then
        CallInfo('Connect as user SYS at the DBX database and grant "SELECT ANY DICTIONARY" privilege to SYSADM');
        CallInfo('SQL> grant SELECT ANY DICTIONARY to SYSADM;');
     end if;
     CallError(SQLERRM);
  end;

  -- 11 g database ?
  v_select := 'SELECT count(*) FROM DBA_USERS WHERE username = (select username from user_users) and password is null ';
  execute immediate v_select into tester;

  -- Get users from BSCS DB
  if tester = 1 then
     -- 11 g database
     v_select_statement := 'SELECT   d.username , u.password ';
     v_select_statement := v_select_statement || 'FROM    DBA_USERS@' || vdbBSCS || ' D ' ;
     v_select_statement := v_select_statement || '       ,sys.user$@' || vdbBSCS || ' U ' ;
     v_select_statement := v_select_statement || 'WHERE   D.username NOT IN (';
     v_select_statement := v_select_statement || '''' || 'SYS' || '''' || ',';
     v_select_statement := v_select_statement || '''' || 'SYSTEM' || '''' || ',';
     v_select_statement := v_select_statement || '''' || 'SYSADM' || '''' || ') ';
     v_select_statement := v_select_statement || 'AND     D.username = U.name ';
     v_select_statement := v_select_statement || 'AND     D.username NOT IN (';
     v_select_statement := v_select_statement || 'SELECT username FROM dba_users) ';
     v_select_statement := v_select_statement || ' ORDER BY username';
  else
     v_select_statement := 'SELECT   username , password ';
     v_select_statement := v_select_statement || 'FROM    DBA_USERS@' || vdbBSCS || ' ' ;
     v_select_statement := v_select_statement || 'WHERE   username NOT IN (';
     v_select_statement := v_select_statement || '''' || 'SYS' || '''' || ',';
     v_select_statement := v_select_statement || '''' || 'SYSTEM' || '''' || ',';
     v_select_statement := v_select_statement || '''' || 'SYSADM' || '''' || ') ';
     v_select_statement := v_select_statement || 'AND     username NOT IN (';
     v_select_statement := v_select_statement || 'SELECT username FROM dba_users) ';
     v_select_statement := v_select_statement || ' ORDER BY username';
  end if;

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_user_name , v_password ;
      EXIT WHEN cur%NOTFOUND;

      begin
         str := 'CREATE USER "' || v_user_name || '" IDENTIFIED BY VALUES ' || '''' || v_password || '''' ;
         execute immediate str;

         str := 'ALTER  USER "' || v_user_name || '"  DEFAULT TABLESPACE ' || DEFTSP ;
         execute immediate str;

         str := 'ALTER  USER "' || v_user_name || '"  TEMPORARY TABLESPACE ' || TMPTSP ;
         execute immediate str;

         CallInfo('User:      ' || v_user_name);

       exception when others then
         CallInfo('Error for User: ' || v_user_name );
         CallInfo(substr(SQLERRM,1,250));
         CallInfo(substr(str,1,250) );
         -- Error for missing privilege to create or alter a user
         if SQLCODE = -1031 then
            CallInfo('Connect as user SYS at the DBX database and grant "CREATE/ALTER USER" privileges to SYSADM');
            CallInfo('SQL> grant CREATE USER to SYSADM;');
            CallInfo('SQL> grant ALTER  USER to SYSADM;');
         end if;

       end;

  END LOOP;
  CLOSE cur;

exception when others then
  callinfo('CreateBSCSUser: Error:');
  CallInfo(v_select_statement);
  -- Error for missing db link
  if SQLCODE = -2019 then
            CallInfo('Database link ' || nvl(vdbBSCS,'NULL') || ' is missing');
  end if;
  -- Error ORA-12505: TNS:listener does not currently know of SID given in connect
  if SQLCODE = -12505 then
            CallInfo('The connect string of the database link ' || nvl(vdbBSCS,'NULL') ||
                     ' can not be resolved in DBX database');
            CallInfo('You may use a full qualified connect string for the database link (see output of tnsping)');
  end if;
  if SQLCODE = -1017  then
            CallInfo('The username and password of the database link ' || nvl(vdbBSCS,'NULL') ||
                     ' is wrong in DBX database');
  end if;
  if SQLCODE = -1045 then
            CallInfo('The username of the database link ' || nvl(vdbBSCS,'NULL') ||
                     ' in DBX database does not have connect privilege to BSCS database');
  end if;
  if SQLCODE = -942  then
            CallInfo('The username of the database link ' || nvl(vdbBSCS,'NULL') ||
                     ' in DBX database does not have SELECT ANY DICTIONARY privilege in BSCS database' ||
                     ' (See data dictionary view DBA_USERS) ');
  end if;
  CallError(SQLERRM);
end CreateBSCSUser;


 /*
   **=====================================================
   **
   ** Procedure: IsRelease
   **
   ** Purpose:   check BSCS Version
   **
   **=====================================================
*/


PROCEDURE IsRelease (
    -- ReleaseNo
    piosReleaseNo IN OUT INTEGER
)
AS
 vdbBSCS              VARCHAR2(4000);
 EXP_FEES_PERIOD_ZERO VARCHAR2(4000);
 vialink              VARCHAR2(1);
 str                  VARCHAR2(32000);
 vcount               INTEGER;
 counter              INTEGER;
 count_fupacc1        INTEGER;
 count_fupacc2        INTEGER;
 count_fupacc3        INTEGER;
BEGIN

 vdbBSCS              := GetParameter('vdbBSCS');
 vialink              := GetParameter('vialink');
 EXP_FEES_PERIOD_ZERO := GetParameter('EXP_FEES_PERIOD_ZERO');

 if EXP_FEES_PERIOD_ZERO = '1' then
    str := 'SELECT count(*) FROM dba_tab_columns';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   table_name = ' || '''' || 'FEES' || '''' ;
    str := str || ' AND   column_name = ' || '''' || 'PERIOD' || '''' ;
    ShowStatement(str);
    execute immediate str into counter;
    callinfo('IsRelease: Check for column FEES.PERIOD:' || TO_CHAR(counter) );
    if counter = 1 then
      dbx.SetParameter ( piosParameterName => 'EXIST_FEES_PERIOD',piosParameterValue => 'Y' );
    else
      dbx.SetParameter ( piosParameterName => 'EXIST_FEES_PERIOD',piosParameterValue => 'N' );
    end if;
 else
    dbx.SetParameter ( piosParameterName => 'EXIST_FEES_PERIOD',piosParameterValue => 'N' );
 end if;

 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'FUP_ACCOUNTS_HIST' || '''' ;
 str := str || ' AND   column_name = ' || '''' || 'ACCOUNT_START_DATE' || '''' ;
 ShowStatement(str);
 execute immediate str into counter;
 callinfo('IsRelease: Check for column FUP_ACCOUNTS_HIST.ACCOUNT_START_DATE:' || TO_CHAR(counter) );
 if counter = 1 then
   dbx.SetParameter ( piosParameterName => 'EXIST_FUPACCHIST_ACCSTARTDATE',piosParameterValue => 'Y' );
 else
   dbx.SetParameter ( piosParameterName => 'EXIST_FUPACCHIST_ACCSTARTDATE',piosParameterValue => 'N' );
 end if;

 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'FUP_ACCOUNTS_HIST' || '''' ;
 str := str || ' AND   column_name = ' || '''' || 'CO_ID' || '''' ;
 ShowStatement(str);
 execute immediate str into counter;
 callinfo('IsRelease: Check for column FUP_ACCOUNTS_HIST.CO_ID:' || TO_CHAR(counter) );
 if counter = 1 then
   dbx.SetParameter ( piosParameterName => 'EXIST_FUPACCHIST_COID',piosParameterValue => 'Y' );
 else
   dbx.SetParameter ( piosParameterName => 'EXIST_FUPACCHIST_COID',piosParameterValue => 'N' );
 end if;

 -- BSCS 5.21
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name IN ( ' || '''' || 'MPDNDTAB' || '''' || ','
                                         || '''' || 'WKZENTRY_ALL' || '''' || ')' ;

 ShowStatement(str);
 execute immediate str into vcount;

 -- BSCS 5.21
 if vcount = 1 then
    piosReleaseNo := 5;
    callinfo('IsRelease: Check BSCS 5.21  (5):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );
 end if;
 -- BSCS 5.1
 if vcount = 2 then
    piosReleaseNo := 4;
    callinfo('IsRelease: Check BSCS 5.10  (4):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );
 end if;

 if piosReleaseNo = 4 then
    str := 'SELECT count(*) FROM dba_tables';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   table_name IN ( ' || '''' || 'CONTR_SERVICES_CAP' || '''' || ')' ;
    ShowStatement(str);
    execute immediate str into counter;
    if counter = 0 then
      dbx.SetParameter ( piosParameterName => 'BSCS523',piosParameterValue => 'Y' );
      callinfo('IsRelease: Check BSCS 5.23  (4):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );
    else
      dbx.SetParameter ( piosParameterName => 'BSCS523',piosParameterValue => 'N' );
    end if;
 else
    dbx.SetParameter ( piosParameterName => 'BSCS523',piosParameterValue => 'N' );
 end if;


 -- BSCS 6.00
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'UDS_STORAGE' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 1 then
    piosReleaseNo := 6;
 end if;
 callinfo('IsRelease: Check BSCS 6.00  (6):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 dbx.SetParameter ( piosParameterName => 'IS_BSCS6_FUP_ACCOUNT_ID',piosParameterValue => 'N' );
 if piosReleaseNo = 6 then
    str := 'SELECT count(*) FROM dba_tab_columns';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   table_name IN ( ' || '''' || 'FUP_ACCOUNTS_HEAD' || '''' || ')' ;
    str := str || ' AND   column_name IN ( ' || '''' || 'ACCOUNT_ID' || '''' || ',' || '''' || 'CO_ID' || '''' || ')' ;
    ShowStatement(str);
    execute immediate str into count_fupacc1;
    if count_fupacc1 = 2 then
       str := 'SELECT count(*) FROM dba_tab_columns';
       if vialink = 'Y' then
          str := str ||  '@' || vdbBSCS ;
       end if;
       str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
       str := str || ' AND   table_name IN ( ' || '''' || 'FUP_ACCOUNTS_HIST' || '''' || ')' ;
       str := str || ' AND   column_name IN ( ' || '''' || 'ACCOUNT_ID' || '''' || ',' || '''' || 'BALANCE_TYPE' || '''' || ')' ;
       ShowStatement(str);
       execute immediate str into count_fupacc2;
       if count_fupacc2 = 2 then
          str := 'SELECT count(*) FROM dba_tab_columns';
          if vialink = 'Y' then
             str := str ||  '@' || vdbBSCS ;
          end if;
          str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
          str := str || ' AND   table_name IN ( ' || '''' || 'FUP_ACCOUNTS_HIST' || '''' || ')' ;
          str := str || ' AND   column_name IN ( ' || '''' || 'CO_ID' || '''' || ')' ;
          ShowStatement(str);
          execute immediate str into count_fupacc3;
          if count_fupacc3 = 0 then
             dbx.SetParameter ( piosParameterName => 'IS_BSCS6_FUP_ACCOUNT_ID',piosParameterValue => 'Y' );
             callinfo('IsRelease: Check IS_BSCS6_FUP_ACCOUNT_ID: Y');
          end if;
       end if;
    end if;
 end if;

 -- BSCS 7.00
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'PROFILE_SERVICE' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 1 then
    piosReleaseNo := 7;
 end if;
 callinfo('IsRelease: Check BSCS 7.00  (7):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 -- BSCS 8.01
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'UDS_ITEM_LISTS' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 1 and piosReleaseNo = 7 then
    piosReleaseNo := 8;
 end if;
 callinfo('IsRelease: Check BSCS 8.01  (8):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 -- BSCS Havana
 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'BSCSPROJECT_ALL' || '''' ;
 str := str || ' AND   column_name = ' || '''' || 'PRODUCT_LABEL' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 1 and ( piosReleaseNo = 7 or piosReleaseNo = 8 ) then
    piosReleaseNo := 9;
 end if;
 callinfo('IsRelease: Check BSCS IX 1  (9):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 -- BSCS Istanbul
 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'CUSTOMER_ALL' || '''' ;
 str := str || ' AND   column_name = ' || '''' || 'BILLCYCLE' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 0 and ( piosReleaseNo = 9 ) then
    piosReleaseNo := 10;
 end if;
 if vcount = 1 then
    dbx.SetParameter ( piosParameterName => 'IsCustBillCycle',piosParameterValue => 'Y' );
 else
    dbx.SetParameter ( piosParameterName => 'IsCustBillCycle',piosParameterValue => 'N' );
 end if;
 callinfo('IsRelease: Check BSCS IX 2 (10):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 -- BSCS Jerez
 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'BSCSPROJECT_ALL' || '''' ;
 str := str || ' AND   column_name in (' || '''' || 'INSTANCE_ID' || '''' || ',' || '''' || 'RERATING' || '''' || ')';

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 2 and ( piosReleaseNo = 10 ) then
    piosReleaseNo := 11;
 end if;
 callinfo('IsRelease: Check BSCS IX 3 (11):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );

 -- BSCS CBIO
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'PREPAID_PROFILE' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;

 if vcount = 0 and ( piosReleaseNo = 11 ) then
    piosReleaseNo := 12;
 end if;
 callinfo('IsRelease: Check BSCS IX 4 (12):' || TO_CHAR(piosReleaseNo) || ':' || TO_CHAR(vcount) );


-- End

 dbx.SetParameter ( piosParameterName => 'IsRelease',piosParameterValue => TO_CHAR(piosReleaseNo) );
 callinfo('IsRelease: Result :' || TO_CHAR( piosReleaseNo ) );

 -- NII specific PN 324195
 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'RESOURCE_LINK' || '''' ;
 str := str || ' AND   column_name in (' || '''' || 'SM_ID' || '''' || ',' || '''' || 'EQUIPMENT_ID' || '''' || ')';

 ShowStatement(str);
 execute immediate str into vcount;
 if vcount = 2 and piosReleaseNo = 10 then
    dbx.SetParameter ( piosParameterName => 'IS_NII_SPECIFIC',piosParameterValue => 'Y' );
    callinfo('IsNIISpecific: Result :' || TO_CHAR( vcount ) );
 else
    dbx.SetParameter ( piosParameterName => 'IS_NII_SPECIFIC',piosParameterValue => 'N' );
    callinfo('IsSpecific: Result :' || TO_CHAR( vcount ) );
 end if;


-- PN 326620 dbx_lctransaction for LC_TABLES
 str := 'SELECT count(*) FROM dba_tab_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'PREPAID_LC_ATTR_HIST' || '''' ;
 str := str || ' AND   column_name in (' || '''' || 'CO_ID' || '''' || ',' || '''' || 'TRANSACTION_ID' || '''' || ')';

 ShowStatement(str);
 execute immediate str into vcount;
 if vcount = 2 then
    dbx.SetParameter ( piosParameterName => 'IS_LCTRANS',piosParameterValue => 'Y' );
    callinfo('IS_LCTRANS: Result :' || TO_CHAR( vcount ) );
 else
    dbx.SetParameter ( piosParameterName => 'IS_LCTRANS',piosParameterValue => 'N' );
    callinfo('IS_LCTRANS: Result :' || TO_CHAR( vcount ) );
 end if;

-- PN 387901
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name = ' || '''' || 'DOCUMENT_REFERENCE' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;
 if vcount = 1 then
    dbx.SetParameter ( piosParameterName => 'EXIST_DOCUMENT_REFERENCE',piosParameterValue => 'Y' );
    callinfo('EXIST_DOCUMENT_REFERENCE: Result :' || TO_CHAR( vcount ) );
 else
    dbx.SetParameter ( piosParameterName => 'EXIST_DOCUMENT_REFERENCE',piosParameterValue => 'N' );
    callinfo('EXIST_DOCUMENT_REFERENCE: Result :' || TO_CHAR( vcount ) );
 end if;

-- PN 393922
 str := 'SELECT count(*) FROM dba_ind_columns';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE table_owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name  = ' || '''' || 'PARAMETER_VALUE' || '''' ;
 str := str || ' AND   column_name = ' || '''' || 'CO_ID' || '''' ;
 str := str || ' AND   column_position = 1';

 ShowStatement(str);
 execute immediate str into vcount;
 if vcount >= 1 then
    dbx.SetParameter ( piosParameterName => 'EXIST_PARAMETER_VALUE_COID_IDX',piosParameterValue => 'Y' );
    callinfo('EXIST_PARAMETER_VALUE_COID_IDX: Result :' || TO_CHAR( vcount ) );
 else
    dbx.SetParameter ( piosParameterName => 'EXIST_PARAMETER_VALUE_COID_IDX',piosParameterValue => 'N' );
    callinfo('EXIST_PARAMETER_VALUE_COID_IDX: Result :' || TO_CHAR( vcount ) );
 end if;

EXCEPTION WHEN OTHERS THEN
  callinfo('IsRelease: Error:');
  CallError(SQLERRM);
END IsRelease;


 /*
   **=====================================================
   **
   ** Procedure: IsMPURHTAB6
   **
   ** Purpose:   check MPURHTAB structure
   **
   **=====================================================
*/


PROCEDURE IsMPURHTAB6 (
    -- Release No for structure
    piosReleaseNo IN OUT INTEGER
)
AS
 vdbBSCS           VARCHAR2(4000);
 vialink           VARCHAR2(1);
 str               VARCHAR2(32000);
 vcount            INTEGER;
 vcounter          INTEGER;
BEGIN

 vdbBSCS             := GetParameter('vdbBSCS');
 vialink             := GetParameter('vialink');

 -- Check if table exists for Adapter Release
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name  = ' || '''' || 'MPURHTAB' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;
 callinfo('MPURHTAB exits:' || TO_CHAR(vcount) );

 if vcount = 1 then

    -- BSCS 6.00
    str := 'SELECT count(*) FROM dba_tab_columns';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   table_name  = ' || '''' || 'MPURHTAB' || '''' ;
    str := str || ' AND   column_name = ' || '''' || 'PLCODE' || '''' ;

    ShowStatement(str);
    execute immediate str into vcount;
    callinfo('MPURHTAB:PLCODE:' || TO_CHAR(vcount) );

    if vcount = 1 then
       piosReleaseNo := 6;
    else
       piosReleaseNo := 7;
    end if;

    -- BSCS 5.23
    str := 'SELECT count(*) FROM dba_tab_columns';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   table_name  = ' || '''' || 'MPURHTAB' || '''' ;
    str := str || ' AND   column_name = ' || '''' || 'VPLCODE' || '''' ;

    ShowStatement(str);
    execute immediate str into vcounter;
    callinfo('MPURHTAB:VPLCODE:' || TO_CHAR(vcounter) );

    if vcount = 0 and vcounter = 0 then
       piosReleaseNo := 4;
    end if;
    if piosReleaseNo = 4 then
       callinfo('MPURHTAB structure 5.23 ');
    end if;
    if piosReleaseNo = 6 then
       callinfo('MPURHTAB structure 6.00 ');
    end if;
    if piosReleaseNo = 7 then
       callinfo('MPURHTAB structure 7.00 or higher');
    end if;
 else
    callinfo('Table MPURHTAB does not exist');
    piosReleaseNo := 0;
 end if;

EXCEPTION WHEN OTHERS THEN
  callinfo('IsMPURHTAB6: Error:');
  CallError(SQLERRM);
END IsMPURHTAB6;



 /*
   **=====================================================
   **
   ** Procedure: IsMPUFFTAB
   **
   ** Purpose:   check MPUFFTAB structure
   **
   **=====================================================
*/


PROCEDURE IsMPUFFTAB
AS
 vdbBSCS           VARCHAR2(4000);
 vialink           VARCHAR2(1);
 str               VARCHAR2(32000);
 vcount            INTEGER;
BEGIN

 vdbBSCS             := GetParameter('vdbBSCS');
 vialink             := GetParameter('vialink');

 -- Check if table exists for Adapter Release
 str := 'SELECT count(*) FROM dba_tables';
 if vialink = 'Y' then
    str := str ||  '@' || vdbBSCS ;
 end if;
 str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
 str := str || ' AND   table_name  = ' || '''' || 'MPUFFTAB' || '''' ;

 ShowStatement(str);
 execute immediate str into vcount;
 callinfo('MPUFFTAB: exits:' || TO_CHAR(vcount) );

 if vcount = 1 then

    -- MPUFFTAB is unique
    str := 'SELECT count(*) FROM dba_cons_columns';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || ' AND   constraint_name  = (select constraint_name ';
    str := str || '                           from   dba_constraints';
    if vialink = 'Y' then
       str := str ||  '@' || vdbBSCS ;
    end if;
    str := str || '                           where  owner = ' || '''' || 'SYSADM' || '''' ;
    str := str || '                           and    table_name = ' || '''' || 'MPUFFTAB' || '''' ;
    str := str || '                           and    constraint_type = ' || '''' || 'P' || '''' ;
    str := str || ')';

    ShowStatement(str);
    execute immediate str into vcount;
    callinfo('IsMPUFFTAB:' || TO_CHAR(vcount) );
 else
    callinfo('Table MPUFFTAB does not exist');
 end if;
 SetParameter('vmpufftab',to_char(vcount) );

EXCEPTION WHEN OTHERS THEN
  callinfo('IsMPUFFTAB: Error:');
  CallError(SQLERRM);
END IsMPUFFTAB;




 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Thufitab
   **
   ** Purpose:   Define Where condition for THUFITAB
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Thufitab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CONTRACT_ID_MIN           INTEGER;
 CONTRACT_ID_MAX           INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM ' || vuserDBXHOME || '.DBX_THUFITAB';
  if viadbxlink = 'Y' then
     str_create := str_create || '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName ||' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM ' || vuserDBXHOME || '.DBX_THUFITAB)';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM DBX_THUFITAB@' || vdbDBXHOME  ;
  str_where_link := str_where_link || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Thufitab: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Thufitab;

 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_Thufotab
   **
   ** Purpose:   Define Where condition for THUFOTAB
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Thufotab
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 CONTRACT_ID_MIN           INTEGER;
 CONTRACT_ID_MAX           INTEGER;
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName;
  str_create := str_create || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM ' || vuserDBXHOME ||
 '.DBX_THUFOTAB';
  if viadbxlink = 'Y' then
     str_create := str_create || '@' || vdbDBXHOME ;
  end if;
  str_create := str_create || ' ) ';

  str_where := piosColumnName ||' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM ' || vuserDBXHOME || '.DBX_THUFOTAB)';

  str_where_link := piosColumnName || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ FILE_ID FROM DBX_THUFOTAB@' || vdbDBXHOME  ;
  str_where_link := str_where_link || ' )';

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Thufotab: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Thufotab;


 /*
   **=====================================================
   **
   ** Procedure: WherePKALL
   **
   ** Purpose:   Define a WHERE condition on all tables with FK to piosTableName
   **            where FK column is also PK column of that table
   **
   **=====================================================
*/


PROCEDURE WherePKAll
(
     -- Table Name
     piosTableName      IN VARCHAR2
     -- Table Name
    ,piosConstraintName IN VARCHAR2
     -- Column Name
    ,piosColumnName IN VARCHAR2
     -- DBX Table Name
    ,piosDBXTableName   IN VARCHAR2
     -- DB Link
    ,piosDBLink      IN VARCHAR2  DEFAULT 'vdbBSCS'
     -- DB Connect
    ,piosDBConnect   IN VARCHAR2  DEFAULT 'vdbconBSCS'
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);

 v_owner                   VARCHAR2(30);
 v_table_name              VARCHAR2(30);
 v_column_name             VARCHAR2(30);
 v_constraint_name         VARCHAR2(30);
 v_pk_name                 VARCHAR2(30);

 count_timestamp           INTEGER := 1;
 vuserDBXHOME              VARCHAR2(60) := ' ';

BEGIN

  vdbBSCS                  := GetParameter(piosDBLink);
  vdbconBSCS               := GetParameter(piosDBConnect);
  vialink                  := GetParameter(piosVIALink);

  CallInfo('REM INFO: vdbBSCS ' || vdbBSCS || ' ' || vdbconBSCS || ' ' || vialink );
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  vuserDBXHOME             := GetParameter('vuserDBXHOME');

  -- Get all tables and columns referencing the PK
  v_select_statement := 'select col.owner,col.table_name,col.column_name,col.constraint_name ' ;
  v_select_statement := v_select_statement || ' FROM sys.dba_cons_columns';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' col ';
  v_select_statement := v_select_statement || ' where  col.position = 1 ';
  v_select_statement := v_select_statement || ' and    (col.owner,col.constraint_name,col.table_name) IN ( ';
  v_select_statement := v_select_statement || '  select owner,constraint_name,table_name ';
  v_select_statement := v_select_statement || '  from   dba_constraints';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '  where  constraint_type = ' || '''' || 'R' || '''' || ' ';
  v_select_statement := v_select_statement || '  and    (r_owner,r_constraint_name) in ( ';
  v_select_statement := v_select_statement || '      select owner,constraint_name ';
  v_select_statement := v_select_statement || '      from   dba_constraints';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '      where  table_name      = ' || '''' || piosTableName || '''';
  v_select_statement := v_select_statement || '      and    owner           = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || '      and    constraint_type IN (''U'',''P'') ';
  v_select_statement := v_select_statement || '      and    constraint_name = ' || '''' || piosConstraintName || '''';
  v_select_statement := v_select_statement || '      ) ';
  v_select_statement := v_select_statement || '  ) ';
  v_select_statement := v_select_statement || ' and (col.owner,col.table_name,col.COLUMN_NAME ) IN ';
  v_select_statement := v_select_statement || ' (select pcol.owner,pcol.table_name,pcol.column_name ';
  v_select_statement := v_select_statement || '  from   dba_cons_columns';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' pcol ';
--  v_select_statement := v_select_statement || ' where  pcol.position in (1,2) ';
  v_select_statement := v_select_statement || ' where  pcol.owner = col.owner ';
  v_select_statement := v_select_statement || ' and    (pcol.table_name , pcol.constraint_name) in ( ';
  v_select_statement := v_select_statement || '    select table_name,constraint_name ';
  v_select_statement := v_select_statement || '    from   dba_constraints';
  if vialink = 'Y' then
     v_select_statement := v_select_statement ||  '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || '    where  table_name      = col.table_name ';
  v_select_statement := v_select_statement || '    and    owner           = col.owner ';
  v_select_statement := v_select_statement || '    and    constraint_type = ''P'') ';
  v_select_statement := v_select_statement || ' ) ';
  v_select_statement := v_select_statement || ' ORDER BY col.owner,col.table_name,col.column_name ';

  ShowStatement(v_select_statement);

  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_owner, v_table_name , v_column_name , v_constraint_name ;
      EXIT WHEN cur%NOTFOUND;

     /*
      * Get the name of the PK
      */
      str := 'SELECT constraint_name FROM dba_constraints';
      if vialink = 'Y' then
         str := str ||  '@' || vdbBSCS;
      end if;
      str := str || ' WHERE owner = ' || '''' || 'SYSADM' || '''';
      str := str || ' AND   table_name = ' || '''' || v_table_name || '''';
      str := str || ' AND   constraint_type = ' || '''' || 'P' || '''';

      begin
      execute immediate str into v_pk_name;
      ShowStatement(str);
      exception when others then
         callinfo('WherePKAll: Subselect Error:');
         v_pk_name := ' ';
      end;

      str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || v_table_name || ' UNRECOVERABLE ' ;
      str_create := str_create || ' AS ';
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE(' || v_table_name || ') ' || ' INDEX( ' || v_table_name ;
      str_create := str_create || ' ' || v_pk_name || ') ';
      str_create := str_create || '  */ * FROM ' || v_table_name ;
      str_create := str_create || '@' || vdbBSCS ;
      str_create := str_create || ' WHERE ' || v_column_name || ' IN (' ;
      str_create := str_create || 'SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ' || piosColumnName || ' FROM ' || vuserDBXHOME || '.' || piosDBXTableName;
      if viadbxlink = 'Y' then
         str_create := str_create ||  '@' || vdbDBXHOME ;
      end if;
      str_create := str_create || ' )';

      str_where :=  v_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ' || piosColumnName || ' FROM '|| vuserDBXHOME || '.' || piosDBXTableName;
      str_where := str_where || ' ) ';

      str_where_link := v_column_name || ' IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ' || piosColumnName || ' FROM ' || piosDBXTableName;
      str_where_link := str_where_link || '@' || vdbDBXHOME || ' ' ;
      str_where_link := str_where_link || ' ) ';

      CallInfo(piosColumnName ||': ' || v_table_name || ' ' || v_pk_name || ' ' || v_column_name  );
      SetDBXWhereClause( piosTableName => v_table_name
                        ,piosColumnName => v_column_name
                        ,piosCreateClause => str_create
                        ,piosWhereClause => str_where
                        ,piosWhereLinkClause => str_where_link
                        ,piosHint => ' '
                        ,piosStorage => ' '
                        ,piosDBConnect => vdbconBSCS );

  END LOOP;
  CLOSE cur;


EXCEPTION WHEN OTHERS THEN
  callinfo('WherePKAll: Error:');
  if SQLCODE = -1652 then
     CallInfo('Define a temporary tablespace for user SYSADM, which is large enough.');
     CallInfo('Connect as user SYS at the DBX database and the DBX HOME database and execute:');
     CallInfo('SQL> alter user SYSADM temporary tablespace <name>;');
  end if;
  CallError(SQLERRM);
END WherePKAll;


 /*
   **=====================================================
   **
   ** Procedure: SetPartition
   **
   ** Purpose:   Set Storage in DBX_TABLES and CreateClauses
   **
   **=====================================================
*/


PROCEDURE SetPartition
(
     -- Link Name
     piosLinkName  IN VARCHAR2
     -- DB   Connect
    ,piosDBConnect IN VARCHAR2
     -- VIA Link
    ,piosVIALink     IN VARCHAR2  DEFAULT 'vialink'
)
AS
 /* Type Declaration */
 TYPE DynCurTyp IS REF CURSOR;

 /* Variable Declaration */
 cur                       DynCurTyp;
 v_select_statement        VARCHAR2(30000);

 v_table_name              VARCHAR2(30);
 V_PTYPE                   VARCHAR2(30);
 V_PKEYCOL                 VARCHAR2(30);
 V_STYPE                   VARCHAR2(30);
 V_SKEYCOL                 VARCHAR2(30);
 v_partition_name          VARCHAR2(30);
 v_subpartition_name       VARCHAR2(30);
 v_high_value              VARCHAR2(4000);
 v_PHIGHVALUE              VARCHAR2(4000);
 v_SHIGHVALUE              VARCHAR2(4000);
 v_PNAME                   VARCHAR2(30);
 v_SNAME                   VARCHAR2(30);
 l_table_name              VARCHAR2(30);
 check_date                NUMBER;
 check_bill                NUMBER;
 check_int_low             NUMBER := 0;
 check_int_high            NUMBER := 0;
 check_int_diff            NUMBER := 0;
 v_udrentdate              VARCHAR2(4000);
 v_DBX_PART                INTEGER := 0;
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 OHXACT_MIN                VARCHAR2(4000);
 OHXACT_MAX                VARCHAR2(4000);
 PRM_VALUE_ID_MIN          VARCHAR2(4000);
 PRM_VALUE_ID_MAX          VARCHAR2(4000);
 CAXACT_MIN                VARCHAR2(4000);
 CAXACT_MAX                VARCHAR2(4000);
 CADXACT_MIN               VARCHAR2(4000);
 CADXACT_MAX               VARCHAR2(4000);
 REQUEST_MIN               VARCHAR2(4000);
 REQUEST_MAX               VARCHAR2(4000);
 CRH_REQUEST_MIN           VARCHAR2(4000);
 CRH_REQUEST_MAX           VARCHAR2(4000);
 tckrccredate              VARCHAR2(4000);
 PRUNE_PARTITION           VARCHAR2(4000);
 BILLCYCLE_GROUP_ID_MIN    VARCHAR2(4000);
 BILLCYCLE_GROUP_ID_MAX    VARCHAR2(4000);
 DATAPUMP_DBXDB_NWMODE     VARCHAR2(4000);

 v_column_name             VARCHAR2(30);
 v_default                 VARCHAR2(25000);
 str                       VARCHAR2(30000);
 vialink                   VARCHAR2(1);
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 v_where                   VARCHAR2(4000);
 v_storage                 NUMBER;
 vspace                    NUMBER;
 counter                   INTEGER;
 do_subpart                INTEGER := 0;
 i                         INTEGER := 0;
 i_end                     INTEGER := 0;
 v_PREV_HIGH_VALUE_NUM     NUMBER;
 v_HIGH_VALUE_NUM          NUMBER;
 v_PART_ROW_COUNTER        INTEGER := 0;
 v_PART_ActionName         VARCHAR2(90);
 v_PART_Message3           VARCHAR2(4000);
 v_PREV_HIGH_VALUE_DATE    DATE;
 v_HIGH_VALUE_DATE         DATE;
 v_COMPARE_DATE            DATE;
begin

  vialink                  := GetParameter(piosVIALink);
  vdbBSCS                  := GetParameter(piosLinkName);
  vdbconBSCS               := GetParameter(piosDBConnect);
  v_udrentdate             := GetParameter('udrentdate');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  OHXACT_MIN               := GetParameter('OHXACT_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  PRM_VALUE_ID_MIN         := GetParameter('PRM_VALUE_ID_MIN');
  PRM_VALUE_ID_MAX         := GetParameter('PRM_VALUE_ID_MAX');
  tckrccredate             := GetParameter('TCKRCCREDATE');
  CAXACT_MIN               := GetParameter('CAXACT_MIN');
  CAXACT_MAX               := GetParameter('CAXACT_MAX');
  CADXACT_MIN              := GetParameter('CADXACT_MIN');
  CADXACT_MAX              := GetParameter('CADXACT_MAX');
  REQUEST_MIN              := GetParameter('REQUEST_MIN');
  REQUEST_MAX              := GetParameter('REQUEST_MAX');
  CRH_REQUEST_MIN          := GetParameter('CRHREQUEST_MIN');
  CRH_REQUEST_MAX          := GetParameter('CRHREQUEST_MAX');
  PRUNE_PARTITION          := GetParameter('PRUNE_PARTITION');
  DATAPUMP_DBXDB_NWMODE    := GetParameter('DATAPUMP_DBXDB_NWMODE');

  BILLCYCLE_GROUP_ID_MIN   := GetParameter('BILLCYCLE_GROUP_ID_MIN');
  BILLCYCLE_GROUP_ID_MAX   := GetParameter('BILLCYCLE_GROUP_ID_MAX');

--CallInfo('SetStorage: ' || vdbBSCS || '.' ||  piosLinkName  );
--CallInfo('SetStorage: ' || vdbconBSCS || '.' ||  piosDBConnect  );
--CallInfo('SetStorage: ' || PCTSIZE    || '-' ||  SPCTSIZE       );

IF PRUNE_PARTITION = '1' AND DATAPUMP_DBXDB_NWMODE != 'd'  THEN

 /*
  * Get start value for counter i
  */
  str := 'select count(*) +1 from DBX_LOG where ACTION like ''SetPartition%'' ';
  execute immediate str into i;
 /*
  * Update partition columns in DBX_TABLES All Partitioned Tables
  */
  v_select_statement := 'SELECT pt.table_name,  ';
  v_select_statement := v_select_statement || '  pt.partitioning_type        PTYPE,' ;
  v_select_statement := v_select_statement || '  pk.column_name              PKEYCOL,' ;
  v_select_statement := v_select_statement || '  pt.subpartitioning_type     STYPE,' ;
  v_select_statement := v_select_statement || '  nvl(spk.column_name,''NONE'') SKEYCOL' ;
  v_select_statement := v_select_statement || ' FROM    dba_part_tables' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' pt ';
  v_select_statement := v_select_statement || ' ,      dba_part_key_columns' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' pk ';
  v_select_statement := v_select_statement || ' ,      dba_subpart_key_columns' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' spk ';
  v_select_statement := v_select_statement || 'WHERE   pt.OWNER       = ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     pt.owner = pk.owner ';
  v_select_statement := v_select_statement || 'AND     pt.table_name = pk.name ';
  v_select_statement := v_select_statement || 'AND     pt.table_name not like ' || '''' || 'BIN$%' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     pk.object_type = ''TABLE''  ';
  v_select_statement := v_select_statement || 'AND     pk.column_position = 1 ';
  v_select_statement := v_select_statement || 'AND     pt.owner = spk.owner (+) ';
  v_select_statement := v_select_statement || 'AND     pt.table_name = spk.name (+) ';
  v_select_statement := v_select_statement || 'AND     ''TABLE'' = spk.object_type (+) ';
  v_select_statement := v_select_statement || 'AND     1 =  spk.column_position (+) ';


  showStatement(v_select_statement);
  DBXLogStart ( piosTableName  => 'DBX_TABLES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'Update DBX_TABLES'
               ,piosPara1      => v_select_statement);
  i_end :=i;
  CallInfo('DBX_TABLES> Add partition Info');
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , V_PTYPE, V_PKEYCOL, V_STYPE, V_SKEYCOL ;
      EXIT WHEN cur%NOTFOUND;

     /*
      * Update DBX_TABLES
      */
      str := 'UPDATE dbx_tables SET DBX_PTYPE = ' || '''' || V_PTYPE || '''' || ' ';
      str := str || ', DBX_PKEYCOL = '  || '''' || V_PKEYCOL || '''' || ' ';
      str := str || ', DBX_STYPE   = '  || '''' || V_STYPE   || '''' || ' ';
      str := str || ', DBX_SKEYCOL = '  || '''' || V_SKEYCOL || '''' || ' ';
      str := str || ' WHERE DBX_TABLE_NAME = ' || '''' || v_table_name || '''' ;
      str := str || '   AND DBX_SCHEMA_NAME = ' || '''' || 'SYSADM' || '''' ;

       begin
          execute immediate str;
          CallInfo('DBX_TABLES> Table: ' || v_table_name );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_TABLES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;


      commit;
  END LOOP;
  CLOSE cur;
  DBXLogStop  ( piosTableName  => 'DBX_TABLES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i_end,'099999999'))
               ,piosMessage    => 'SUCCESS' );

 /*
  * Fill table dbx_part_pvalues
  */

  v_select_statement := 'SELECT p.table_name,  ';
  v_select_statement := v_select_statement || '  p.partition_name,' ;
  v_select_statement := v_select_statement || '  p.high_value ' ;
  v_select_statement := v_select_statement || ' FROM    dba_tab_partitions' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' p  ';
  v_select_statement := v_select_statement || ' ,    dba_tables' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' t  ';
  v_select_statement := v_select_statement || 'WHERE   p.table_owner   = ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     t.owner         = ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     t.table_name    = p.table_name  ';
  v_select_statement := v_select_statement || 'AND     t.iot_name   IS NULL ';
  v_select_statement := v_select_statement || 'ORDER BY p.table_name,p.partition_name ';

  showStatement(v_select_statement);

  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'Insert DBX_PART_PVALUES'
               ,piosPara1      => v_select_statement);
  i_end :=i;

  CallInfo('DBX_PART_PVALUES> Store all Partitions');
  l_table_name := '---';
  counter := 0;
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , v_partition_name, v_high_value ;
      EXIT WHEN cur%NOTFOUND;
      counter := counter + 1;
     /*
      * Show inserted rows of last table
      */
      if l_table_name != '---' and l_table_name != v_table_name then
         CallInfo('DBX_PART_PVALUES> Table: ' || l_table_name || ' (' || TO_CHAR(counter) || ')');
         counter := 1;
      end if;
     /*
      * Delete first time a table_name appears
      */
      if l_table_name = '---' or l_table_name != v_table_name then
         str := 'DELETE FROM dbx_part_pvalues WHERE dbx_table_name = ' || '''' || v_table_name || '''';
     --  showStatement(str);
         execute immediate str;
         if SQL%ROWCOUNT > 0 then
            CallInfo('DBX_PART_PVALUES> Delete: ' || v_table_name || ' ' || TO_CHAR(SQL%ROWCOUNT) );
         end if;
         i := i+1;
         DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => str);
      end if;

     /*
      * Insert DBX_PART_PVALUES
      */
      str := 'INSERT INTO dbx_part_pvalues (dbx_table_name,dbx_partition_name,dbx_high_value) ' ;
      str := str || 'VALUES (' ;
      str := str || '''' || v_table_name || '''' || ' ';
      str := str || ','  || '''' || v_partition_name   || '''' || ' ';
      str := str || ','  || '''' || replace(v_high_value,'''','''''')  || '''' || ') ';

       begin
      --  showStatement(str);
          execute immediate str;
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => str);
   --     showStatement('DBX_PART_PVALUES> Insert ' || v_table_name || ':' || v_partition_name  );
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      l_table_name := v_table_name;

      commit;
  END LOOP;
  CLOSE cur;
  if l_table_name != '---' then
     CallInfo('DBX_PART_PVALUES> Table: ' || l_table_name || ' (' || TO_CHAR(counter) || ')');
     counter := 1;
  end if;
  DBXLogStop  ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i_end,'099999999'))
               ,piosMessage    => 'SUCCESS' );

 /*
  * check if dbx_part_svalues can be filled
  */
  v_select_statement := 'SELECT count(*) ';
  v_select_statement := v_select_statement || ' from   dba_tab_columns' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' where  column_name = ' || '''' || 'HIGH_VALUE' || '''' ;
  v_select_statement := v_select_statement || ' and    table_name  = ' || '''' || 'DBA_TAB_SUBPARTITIONS' || '''' ;
  execute immediate v_select_statement into do_subpart;

  if do_subpart >= 1 then
 /*
  * Fill table dbx_part_svalues
  */

  v_select_statement := 'SELECT p.table_name,  ';
  v_select_statement := v_select_statement || '  p.partition_name,' ;
  v_select_statement := v_select_statement || '  p.subpartition_name,' ;
  v_select_statement := v_select_statement || '  p.high_value ' ;
  v_select_statement := v_select_statement || ' FROM    dba_tab_subpartitions' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' p  ';
  v_select_statement := v_select_statement || ' ,    dba_tables' ;
  if vialink = 'Y' then
     v_select_statement :=  v_select_statement || '@' || vdbBSCS;
  end if;
  v_select_statement := v_select_statement || ' t  ';
  v_select_statement := v_select_statement || 'WHERE   p.table_owner   = ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     t.owner         = ' || '''' || 'SYSADM' || '''' || ' ';
  v_select_statement := v_select_statement || 'AND     t.table_name    = p.table_name  ';
  v_select_statement := v_select_statement || 'AND     t.iot_name   IS NULL ';
  v_select_statement := v_select_statement || 'ORDER BY p.table_name,p.partition_name,p.subpartition_name ';

  showStatement(v_select_statement);
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_SVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'Insert DBX_PART_SVALUES'
               ,piosPara1      => v_select_statement);
  i_end :=i;

  CallInfo('DBX_PART_SVALUES> Store all Subpartitions');
  counter := 0;
  l_table_name := '---';
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , v_partition_name, v_subpartition_name, v_high_value ;
      EXIT WHEN cur%NOTFOUND;
      counter := counter + 1;
     /*
      * Show inserted rows of last table
      */
      if l_table_name != '---' and l_table_name != v_table_name then
         CallInfo('DBX_PART_SVALUES> Table: ' || l_table_name || ' (' || TO_CHAR(counter) || ')');
         counter := 1;
      end if;
     /*
      * Delete first time a table_name appears
      */
      if l_table_name = '---' or l_table_name != v_table_name then
         str := 'DELETE FROM dbx_part_svalues WHERE dbx_table_name = ' || '''' || v_table_name || '''';
--       showStatement(str);
         execute immediate str;
         if SQL%ROWCOUNT > 0 then
            CallInfo('DBX_PART_SVALUES> Delete: ' || v_table_name || ' ' || TO_CHAR(SQL%ROWCOUNT) );
         end if;
         i := i+1;
         DBXLogStart ( piosTableName  => 'DBX_PART_SVALUES'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => str);
      end if;

     /*
      * Insert DBX_PART_SVALUES
      */
      str := 'INSERT INTO dbx_part_svalues (dbx_table_name,dbx_partition_name,dbx_subpartition_name,dbx_high_value) ' ;
      str := str || 'VALUES (' ;
      str := str || '''' || v_table_name || '''' || ' ';
      str := str || ','  || '''' || v_partition_name   || '''' || ' ';
      str := str || ','  || '''' || v_subpartition_name   || '''' || ' ';
      str := str || ','  || '''' || replace(v_high_value,'''','''''')  || '''' || ') ';

       begin
--        showStatement(str);
          execute immediate str;
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_SVALUES'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => str);
--        showStatement('DBX_PART_SVALUES> Insert ' || v_table_name || ':' || v_partition_name || ' ' ||v_subpartition_name  );
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;

      l_table_name := v_table_name;
      commit;
  END LOOP;
  CLOSE cur;
  if l_table_name != '---' then
     CallInfo('DBX_PART_SVALUES> Table: ' || l_table_name || ' (' || TO_CHAR(counter) || ')');
     counter := 1;
  end if;
  end if; -- end if do_subpart
  DBXLogStop  ( piosTableName  => 'DBX_PART_SVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i_end,'099999999'))
               ,piosMessage    => 'SUCCESS' );

  CallInfo('CUSTOMER_ID  (min/max):' || CUSTOMER_ID_MIN  || ' ' || CUSTOMER_ID_MAX  );
  CallInfo('CONTRACT_ID  (min/max):' || CONTRACT_ID_MIN  || ' ' || CONTRACT_ID_MAX  );
  CallInfo('OHXACT       (min/max):' || OHXACT_MIN       || ' ' || OHXACT_MIN       );
  CallInfo('PRM_VALUE_ID (min/max):' || PRM_VALUE_ID_MIN || ' ' || PRM_VALUE_ID_MAX );
  CallInfo('CAXACT       (min/max):' || CAXACT_MIN       || ' ' || CAXACT_MAX       );
  CallInfo('CADXACT      (min/max):' || CADXACT_MIN      || ' ' || CADXACT_MAX      );
  CallInfo('REQUEST      (min/max):' || REQUEST_MIN      || ' ' || REQUEST_MAX      );
  CallInfo('CRHREQUEST   (min/max):' || CRH_REQUEST_MIN  || ' ' || CRH_REQUEST_MAX  );
  CallInfo('BILLCYC_GROUP(min/max):' || BILLCYCLE_GROUP_ID_MIN   || ' ' || BILLCYCLE_GROUP_ID_MAX );
  CallInfo('TICKLER/TU   (min)    :' || tckrccredate  );
  CallInfo('TIMESTAMP    (min)    :' || v_udrentdate  );

  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'CUSTOMER_ID  (min/max):' || CUSTOMER_ID_MIN  || ' ' || CUSTOMER_ID_MAX );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'CONTRACT_ID  (min/max):' || CONTRACT_ID_MIN  || ' ' || CONTRACT_ID_MAX  );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'OHXACT       (min/max):' || OHXACT_MIN       || ' ' || OHXACT_MIN       );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'PRM_VALUE_ID (min/max):' || PRM_VALUE_ID_MIN || ' ' || PRM_VALUE_ID_MAX );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'CAXACT       (min/max):' || CAXACT_MIN       || ' ' || CAXACT_MAX       );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'CADXACT      (min/max):' || CADXACT_MIN      || ' ' || CADXACT_MAX       );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'REQUEST      (min/max):' || REQUEST_MIN      || ' ' || REQUEST_MAX       );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'CRHREQUEST   (min/max):' || CRH_REQUEST_MIN   || ' ' || CRH_REQUEST_MAX       );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'BILLCYC_GROUP(min/max):' || BILLCYCLE_GROUP_ID_MIN   || ' ' || BILLCYCLE_GROUP_ID_MAX );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'TICKLER/TU   (min)    :' || tckrccredate  );
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => 'TIMESTAMP    (min)    :' || v_udrentdate  );

-- JK

     /*
      * Update DBX_PART_PVALUES
      */
      str := 'UPDATE dbx_part_pvalues  SET DBX_HIGH_VALUE_NUM = TO_NUMBER (dbx_high_value) ';
      str := str || 'WHERE dbx_high_value NOT IN (' || '''' || 'MAXVALUE' || '''' || ',' || '''' || 'DEFAULT' || '''' || ') ';
      str := str || '  AND dbx_high_value IS NOT NULL ';
      str := str || '  AND dbx_table_name IN (';
      str := str || ' SELECT dbx_table_name FROM dbx_tables ';
      str := str || '  WHERE dbx_ptype = ' || '''' || 'RANGE' || '''' ;
      str := str || '    AND DBX_PKEYCOL IN (' || '''' || 'CO_ID' || '''' ;
      str := str || '                       ,' || '''' || 'CUSTOMER_ID' || '''' ;
      str := str || '                       ,' || '''' || 'REQUEST' || '''' ;
      str := str || '                       ,' || '''' || 'REQUEST_ID' || '''' ;
      str := str || '                       ,' || '''' || 'OHXACT' || '''' ;
      str := str || '                       ,' || '''' || 'OTXACT' || '''' ;
      str := str || '                       ,' || '''' || 'PRM_VALUE_ID' || '''' ;
      str := str || '                       ,' || '''' || 'CADXACT' || '''' ;
      str := str || '                       ,' || '''' || 'CAXACT' || '''' ;
      str := str || '                       ,' || '''' || 'BILLCYCLE_GROUP' || '''' ;
      str := str || '                       ,' || '''' || 'BILLCYCLE_GROUP_ID' || '''' ;
      str := str || '))';

       begin
          execute immediate str;
          CallInfo('DBX_PART_PVALUES> Fill DBX_HIGH_VALUE_NUM rows: ' || TO_CHAR(SQL%ROWCOUNT) );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      commit;

     /*
      * Update DBX_PART_PVALUES
      */
      str := 'UPDATE dbx_part_pvalues PA SET DBX_PREV_HIGH_VALUE_NUM = (';
      str := str || ' SELECT max(PB.dbx_high_value_num) ';
      str := str || '   FROM dbx_part_pvalues PB' ;
      str := str || '  WHERE PA.dbx_table_name = PB.dbx_table_name ' ;
      str := str || '    AND PA.DBX_SCHEMA_NAME = PB.DBX_SCHEMA_NAME ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_NUM IS NOT NULL ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_NUM < PA.DBX_HIGH_VALUE_NUM ) ' ;

       begin
          execute immediate str;
          CallInfo('DBX_PART_PVALUES> Fill DBX_PREV_HIGH_VALUE_NUM rows: ' || TO_CHAR(SQL%ROWCOUNT) );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      commit;

     /*
      * Update DBX_PART_PVALUES for MAXVALUE
      */
      str := 'UPDATE dbx_part_pvalues PA SET DBX_PREV_HIGH_VALUE_NUM = (';
      str := str || ' SELECT max(PB.dbx_high_value_num) ';
      str := str || '   FROM dbx_part_pvalues PB' ;
      str := str || '  WHERE PA.dbx_table_name = PB.dbx_table_name ' ;
      str := str || '    AND PA.DBX_SCHEMA_NAME = PB.DBX_SCHEMA_NAME ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_NUM IS NOT NULL) ' ;
      str := str || ' WHERE 0 != instr(upper(PA.DBX_HIGH_VALUE),' || '''' || 'MAXVALUE' || '''' || ')';

       begin
          execute immediate str;
          CallInfo('DBX_PART_PVALUES> Fill DBX_PREV_HIGH_VALUE_NUM rows: ' || TO_CHAR(SQL%ROWCOUNT) );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      commit;

-- JK DATE
     /*
      * Update DBX_PART_PVALUES
      */
      v_select_statement := 'SELECT dbx_table_name,dbx_partition_name,dbx_high_value ';
      v_select_statement := v_select_statement || ' FROM dbx_part_pvalues ';
      v_select_statement := v_select_statement || ' WHERE dbx_high_value NOT IN (' || '''' || 'MAXVALUE' || '''' || ',' || '''' || 'DEFAULT' || '''' || ') ';
      v_select_statement := v_select_statement || '   AND dbx_high_value IS NOT NULL ';
      v_select_statement := v_select_statement || '   AND DBX_SCHEMA_NAME = ' || '''' || 'SYSADM' || '''';
      v_select_statement := v_select_statement || '   AND dbx_table_name IN (';
      v_select_statement := v_select_statement || ' SELECT dbx_table_name FROM dbx_tables ';
      v_select_statement := v_select_statement || '  WHERE dbx_ptype = ' || '''' || 'RANGE' || '''' ;
      v_select_statement := v_select_statement || '    AND DBX_PKEYCOL IN (' || '''' || 'ENTRY_DATE_TIMESTAMP' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'INITIAL_START_TIME_TIMESTAMP' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'START_TIME_TIMESTAMP' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'LBC_DATE' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'BI_BALAN_SNAP_DATE_TIMESTAMP' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'DR_ASS_DOC_CR_DATE_TIMESTAMP' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'CREATED_DATE' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'TU_DATE' || '''' ;
      v_select_statement := v_select_statement || '                       ,' || '''' || 'CT_TU_DATE' || '''' ;
      v_select_statement := v_select_statement || '))';
      showStatement(v_select_statement);
      i := i+1;
      DBXLogStart ( piosTableName  => 'DBX_PART'
                   ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                   ,piosMessage    => 'Update DBX_PART_PVALUES '
                   ,piosPara1      => v_select_statement);
      OPEN cur FOR v_select_statement ;
        LOOP
          FETCH cur INTO v_table_name , v_PNAME, v_PHIGHVALUE;
          EXIT WHEN cur%NOTFOUND;
             str := 'UPDATE dbx_part_pvalues  SET DBX_HIGH_VALUE_DATE = ' || v_PHIGHVALUE ;
             str := str || ' WHERE DBX_SCHEMA_NAME = ' || '''' || 'SYSADM' || '''';
             str := str || '   AND dbx_table_name  = ' || '''' || v_table_name || '''';
             str := str || '   AND dbx_partition_name  = ' || '''' || v_PNAME || '''';
             begin
                execute immediate str;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
                     ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                     ,piosMessage    => 'SUCCESS'
                     ,piosPara1      => str
                     ,piosPara2      => TO_CHAR(SQL%ROWCOUNT) );
             exception when others then
                CallInfo(substr(SQLERRM,1,250));
                CallInfo(substr(str,1,250) );
             end;
            commit;
      END LOOP;
      CLOSE cur;

     /*
      * Update DBX_PART_PVALUES
      */
      str := 'UPDATE dbx_part_pvalues PA SET DBX_PREV_HIGH_VALUE_DATE = (';
      str := str || ' SELECT max(PB.dbx_high_value_date) ';
      str := str || '   FROM dbx_part_pvalues PB' ;
      str := str || '  WHERE PA.dbx_table_name = PB.dbx_table_name ' ;
      str := str || '    AND PA.DBX_SCHEMA_NAME = PB.DBX_SCHEMA_NAME ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_DATE IS NOT NULL ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_DATE < PA.DBX_HIGH_VALUE_DATE ) ' ;

       begin
          execute immediate str;
          CallInfo('DBX_PART_PVALUES> Fill DBX_PREV_HIGH_VALUE_DATE rows: ' || TO_CHAR(SQL%ROWCOUNT) );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      commit;

     /*
      * Update DBX_PART_PVALUES for MAXVALUE
      */
      str := 'UPDATE dbx_part_pvalues PA SET DBX_PREV_HIGH_VALUE_DATE = (';
      str := str || ' SELECT max(PB.dbx_high_value_date) ';
      str := str || '   FROM dbx_part_pvalues PB' ;
      str := str || '  WHERE PA.dbx_table_name = PB.dbx_table_name ' ;
      str := str || '    AND PA.DBX_SCHEMA_NAME = PB.DBX_SCHEMA_NAME ' ;
      str := str || '    AND PB.DBX_HIGH_VALUE_DATE IS NOT NULL) ' ;
      str := str || ' WHERE 0 != instr(upper(PA.DBX_HIGH_VALUE),' || '''' || 'MAXVALUE' || '''' || ')';

       begin
          execute immediate str;
          CallInfo('DBX_PART_PVALUES> Fill DBX_PREV_HIGH_VALUE_DATE rows: ' || TO_CHAR(SQL%ROWCOUNT) );
          i := i+1;
          DBXLogStart ( piosTableName  => 'DBX_PART_PVALUES'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'SUCCESS'
               ,piosPara1      => str);
       exception when others then
          CallInfo(substr(SQLERRM,1,250));
          CallInfo(substr(str,1,250) );
       end;
      commit;



 /*
  * Check all partioned tables
  */

  v_select_statement := 'SELECT  t.dbx_table_name,  ';
  v_select_statement := v_select_statement || '  t.DBX_PKEYCOL ,' ;
  v_select_statement := v_select_statement || '  t.DBX_SKEYCOL ,' ;
  v_select_statement := v_select_statement || '  t.DBX_STYPE,  ' ;
  v_select_statement := v_select_statement || '  t.DBX_PTYPE,  ' ;
  v_select_statement := v_select_statement || '  p.DBX_high_value,  ' ;
  v_select_statement := v_select_statement || '  p.DBX_partition_name,  ' ;
  v_select_statement := v_select_statement || '  s.DBX_high_value,  ' ;
  v_select_statement := v_select_statement || '  s.DBX_subpartition_name,' ;
  v_select_statement := v_select_statement || '  t.DBX_COLUMN_NAME, ' ;
  v_select_statement := v_select_statement || '  p.DBX_HIGH_VALUE_NUM, ' ;
  v_select_statement := v_select_statement || '  p.DBX_PREV_HIGH_VALUE_NUM, ' ;
  v_select_statement := v_select_statement || '  p.DBX_HIGH_VALUE_DATE, ' ;
  v_select_statement := v_select_statement || '  p.DBX_PREV_HIGH_VALUE_DATE  ' ;
  v_select_statement := v_select_statement || ' FROM    dbx_tables t ' ;
  v_select_statement := v_select_statement || '        ,dbx_part_pvalues p' ;
  v_select_statement := v_select_statement || '        ,dbx_part_svalues s' ;
  v_select_statement := v_select_statement || ' WHERE   t.DBX_PKEYCOL IS NOT NULL ';
  v_select_statement := v_select_statement || ' AND     t.DBX_SCHEMA_NAME = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' AND    (  ( t.DBX_COLUMN_NAME = t.DBX_PKEYCOL )   ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUST_INFO_CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''ENTRY_DATE_TIMESTAMP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUST_INFO_CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''INITIAL_START_TIME_TIMESTAMP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUST_INFO_CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''START_TIME_TIMESTAMP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''(CUSTOMER_ID,TICKLER_NUMBER)''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''CREATED_DATE'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''(CADXACT,CADOXACT)''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''CADXACT'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CO_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''CT_TU_DATE'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''TU_DATE'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''LBC_DATE'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''BI_BALAN_IDENT_CONTRACT_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BI_BALAN_SNAP_DATE_TIMESTAMP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''DR_ADDRESSEE_CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''DR_ASS_DOC_CR_DATE_TIMESTAMP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP_ID'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CUSTOMER_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CO_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP_ID'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CO_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''RLH_PROCESS_CONTR_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''RDR_PROCESS_CONTR_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''BILLCYCLE_GROUP'' ) ';
  v_select_statement := v_select_statement || '  OR ( t.DBX_COLUMN_NAME =''CO_ID''  ';
  v_select_statement := v_select_statement || '        AND t.DBX_PKEYCOL = ''FAMILY_GROUP_ID'' ) ';
  v_select_statement := v_select_statement || '     )  ';
  v_select_statement := v_select_statement || ' AND   t.dbx_table_name = p.dbx_table_name         ';
  v_select_statement := v_select_statement || ' AND   p.dbx_table_name = s.dbx_table_name (+)     ';
  v_select_statement := v_select_statement || ' AND   p.dbx_partition_name = s.dbx_partition_name (+)     ';
  v_select_statement := v_select_statement || ' AND   t.dbx_exclude = ' || '''' || 'N' || '''' ;
  v_select_statement := v_select_statement || ' AND   (t.dbx_where_condition IS NULL OR t.dbx_where_condition != ' || '''' || '1 = 2' || '''' || ') ';
--v_select_statement := v_select_statement || ' order  by t.dbx_table_name,p.DBX_partition_name,s.DBX_partition_name ';
  v_select_statement := v_select_statement || ' order  by t.dbx_table_name';
  v_select_statement := v_select_statement || ',LPAD( REPLACE(p.DBX_high_value,' || '''' || 'MAXVALUE' || '''' ||
                                              ',LPAD(' || '''' || 'Z' || '''' || ',200,' || '''' || 'Z' || '''' || ')),200 ) ';
  v_select_statement := v_select_statement || ',LPAD( REPLACE(s.DBX_high_value,' || '''' || 'MAXVALUE' || '''' ||
                                              ',LPAD(' || '''' || 'Z' || '''' || ',200,' || '''' || 'Z' || '''' || ')),200 ) ';

  l_table_name := '---';
  showStatement(v_select_statement);
  i := i+1;
  DBXLogStart ( piosTableName  => 'DBX_PART'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
               ,piosMessage    => 'Insert DBX_PART'
               ,piosPara1      => v_select_statement);
  i_end :=i;
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , V_PKEYCOL,V_SKEYCOL,V_STYPE,V_PTYPE,v_PHIGHVALUE,v_PNAME,v_SHIGHVALUE,v_SNAME,v_column_name,v_HIGH_VALUE_NUM,v_PREV_HIGH_VALUE_NUM , v_HIGH_VALUE_DATE ,v_PREV_HIGH_VALUE_DATE ;
      EXIT WHEN cur%NOTFOUND;

      v_PART_ROW_COUNTER        := 0;
      check_int_diff            := 0;
     /*
      * Delete first time a table_name appears
      */
      if l_table_name = '---' or l_table_name != v_table_name then
         str := 'DELETE FROM dbx_part WHERE dbx_table_name = ' || '''' || v_table_name || '''';
--       showStatement(str);
         execute immediate str;
         i := i+1;
         DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => str);
         if SQL%ROWCOUNT > 0 then
            CallInfo('DBX_PART> Delete ' || v_table_name || ' ' || TO_CHAR(SQL%ROWCOUNT) );
         end if;
      end if;

--    showStatement('PARTITION ' || V_PKEYCOL || ' ' || V_PTYPE || ' ' || v_table_name || ':' || v_PNAME );
--    showStatement('HIGHVALUE ' || v_PHIGHVALUE);
      i := i+1;
      v_PART_ActionName := 'SetPartition_' || trim(TO_CHAR(i,'099999999')) ;
      DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => v_PART_ActionName
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'PARTITION ' || V_PKEYCOL || ' ' || V_PTYPE || ' ' || v_table_name || ':' || v_PNAME
              ,piosPara2      => 'HIGHVALUE ' || v_PHIGHVALUE );
      IF V_SKEYCOL != 'NONE' THEN
--         showStatement('/SUBPART  ' || V_SKEYCOL || ' ' || V_STYPE || ' ' || v_table_name || ':' || v_SNAME );
         i := i+1;
         DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => '/SUBPART  ' || V_SKEYCOL || ' ' || V_STYPE || ' ' || v_table_name || ':' || v_SNAME
              ,piosPara2      => '/HIGHVALUE ' || v_SHIGHVALUE);
--         showStatement('/HIGHVALUE ' || v_SHIGHVALUE);
      END IF;

      v_DBX_PART := 0;
     /*
      * Partiton Range ENTRY_DATE_TIMESTAMP DR_ADDRESSEE_CUSTOMER_ID DR_ASS_DOC_CR_DATE_TIMESTAMP
      */
      IF (V_PKEYCOL = 'ENTRY_DATE_TIMESTAMP' OR V_PKEYCOL = 'INITIAL_START_TIME_TIMESTAMP'
          OR V_PKEYCOL = 'START_TIME_TIMESTAMP' OR V_PKEYCOL = 'LBC_DATE'
          OR V_PKEYCOL = 'BI_BALAN_SNAP_DATE_TIMESTAMP' OR V_PKEYCOL = 'DR_ASS_DOC_CR_DATE_TIMESTAMP' )  AND
         V_PTYPE = 'RANGE'
      THEN
--       CallInfo('TIMESTAMP (min): sysdate - ' || v_udrentdate  );
         v_PART_Message3 := 'TIMESTAMP (min): sysdate - ' || v_udrentdate ;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_DATE IS NULL THEN
            v_DBX_PART := 1;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF v_udrentdate != '0'
          THEN
           /*
            *  Check DATE partition
            */
            v_COMPARE_DATE := sysdate - TO_NUMBER(NVL(v_udrentdate,'0'));
            IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
               str := 'select trunc( ' || v_PHIGHVALUE || ' - (sysdate - ' || v_udrentdate || ') ) from dual';
--             showStatement(str);
               execute immediate str into check_date;
               i := i+1;
               DBXLogStart ( piosTableName  => 'DBX_PART'
                 ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                 ,piosMessage    => 'SUCCESS'
                 ,piosPara1      => str
                 ,piosPara2      => TO_CHAR(check_date));
--             showStatement('CHECK: ' || TO_CHAR(check_date) );
               IF check_date > 0 then
                  v_DBX_PART := 1;
               ELSE
                  v_DBX_PART := 0;
               END IF;
            END IF;
-- range begin
            IF v_HIGH_VALUE_DATE IS NOT NULL OR v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                IF v_HIGH_VALUE_DATE IS NOT NULL AND v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                   IF v_COMPARE_DATE < v_HIGH_VALUE_DATE THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                   END IF;
                END IF;
                IF v_HIGH_VALUE_DATE IS NOT NULL AND v_PREV_HIGH_VALUE_DATE IS     NULL THEN
                   IF v_COMPARE_DATE < v_HIGH_VALUE_DATE THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                   END IF;
                END IF;
                IF v_HIGH_VALUE_DATE IS     NULL AND v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                END IF;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                 ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                 ,piosMessage    => 'SUCCESS'
                 ,piosPara1      => 'DBX_PART : ' || TO_CHAR(v_DBX_PART) || ' CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff)
                 ,piosPara2      => 'HIGHVAL_DATE  : ' || TO_CHAR(v_HIGH_VALUE_DATE,'YYMMDD HH24:MI:SS')
                 ,piosPara3      => 'COMPARE_DATE  : ' || TO_CHAR(v_COMPARE_DATE,'YYMMDD HH24:MI:SS'));
            END IF;
-- range end
          END IF;
         END IF;
      END IF;
     /*
      * Partiton Range   ENTRY_DATE_TIMESTAMP
      * Subpartiton List CUST_INFO_BILL_CYCLE
      */
      IF (V_PKEYCOL = 'ENTRY_DATE_TIMESTAMP' OR V_PKEYCOL = 'INITIAL_START_TIME_TIMESTAMP'
          OR V_PKEYCOL = 'LBC_DATE' OR V_PKEYCOL = 'BI_BALAN_SNAP_DATE_TIMESTAMP' )  AND
         (V_SKEYCOL = 'CUST_INFO_BILL_CYCLE' OR V_SKEYCOL = 'BILLCYCLE' OR V_SKEYCOL = 'BI_BALAN_IDENT_BILL_CYCLE')  AND
         V_STYPE = 'LIST' AND
         V_PTYPE = 'RANGE' AND
         (v_column_name = 'CUST_INFO_CUSTOMER_ID' or v_column_name = 'BI_BALAN_IDENT_CONTRACT_ID' ) AND
         v_DBX_PART = 1 THEN
--       showStatement('/BILLCYCLE: ' || v_SHIGHVALUE );
         i := i+1;
         DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => '/BILLCYCLE: ' || v_SHIGHVALUE );
        /*
         * Check BILLCYCLE LIST SUB Partition
         */
         IF 0 != instr(upper(v_SHIGHVALUE),'MAXVALUE') THEN
            v_DBX_PART := 1;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF v_udrentdate != '0'
          THEN
             IF v_SHIGHVALUE = 'DEFAULT' THEN
                v_DBX_PART := 1;
             ELSE
                IF 0 = instr(v_SHIGHVALUE,',') THEN
                   str := 'SELECT count(*) ';
                   str := str || ' FROM  dbx_customer ';
                   str := str || ' WHERE billcycle IS NOT NULL ';
                   str := str || ' AND   upper(billcycle) = upper(' || v_SHIGHVALUE || ')' ;
                ELSE
                  str := 'SELECT count(*) ';
                   str := str || ' FROM  dbx_customer ';
                   str := str || ' WHERE billcycle IS NOT NULL ';
                   str := str || ' AND   instr(upper(' || '''' || replace(v_SHIGHVALUE,'''','''''') || '''' || '),'
                              || '''''' || '''''' || ' || upper(billcycle) || ' || '''''' || '''''' || ') != 0 ';
                END IF;

             -- showStatement(str);
                execute immediate str into check_bill;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => '/CHECK: ' || TO_CHAR(check_bill) );
--              showStatement('/CHECK: ' || TO_CHAR(check_bill) );
                IF check_bill > 0 then
                   v_DBX_PART := 1;
                ELSE
                   v_DBX_PART := 0;
                END IF;
             END IF;
          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range CREATED_DATE TICKLER_RECORDS,TU_DATE CUSTOMER_TAKEOVER,CT_TU_DATE CONTRACT_TAKEOVER
      */
      IF (V_PKEYCOL = 'CREATED_DATE' OR V_PKEYCOL = 'TU_DATE' OR V_PKEYCOL = 'CT_TU_DATE' )  AND V_PTYPE = 'RANGE'
-- AND   v_column_name = '(CUSTOMER_ID,TICKLER_NUMBER)'
      THEN
--       CallInfo('TIMESTAMP (min): sysdate - ' || tckrccredate  );
         v_PART_Message3 := 'TIMESTAMP (min): sysdate - ' || tckrccredate ;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_DATE IS NULL THEN
            v_DBX_PART := 1;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF tckrccredate != '0'
          THEN
           /*
            *  Check DATE partition
            */
            v_COMPARE_DATE := sysdate - TO_NUMBER(NVL(tckrccredate,'0'));
            IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
               str := 'select trunc( ' || v_PHIGHVALUE || ' - (sysdate - ' || tckrccredate || ') ) from dual';
--             showStatement(str);
               execute immediate str into check_date;
--             showStatement('CHECK: ' || TO_CHAR(check_date) );
               i := i+1;
               DBXLogStart ( piosTableName  => 'DBX_PART'
                                ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                                ,piosMessage    => 'SUCCESS'
                                ,piosPara1      => str
                                ,piosPara2      => '/CHECK: ' || TO_CHAR(check_date) );
               IF check_date > 0 then
                  v_DBX_PART := 1;
               ELSE
               v_DBX_PART := 0;
               END IF;
            END IF;
-- range begin
            IF v_HIGH_VALUE_DATE IS NOT NULL OR v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                IF v_HIGH_VALUE_DATE IS NOT NULL AND v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                   IF v_COMPARE_DATE < v_HIGH_VALUE_DATE THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                   END IF;
                END IF;
                IF v_HIGH_VALUE_DATE IS NOT NULL AND v_PREV_HIGH_VALUE_DATE IS     NULL THEN
                   IF v_COMPARE_DATE < v_HIGH_VALUE_DATE THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                   END IF;
                END IF;
                IF v_HIGH_VALUE_DATE IS     NULL AND v_PREV_HIGH_VALUE_DATE IS NOT NULL THEN
                      v_DBX_PART := 1;
                      check_int_diff := 0;
                END IF;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                 ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                 ,piosMessage    => 'SUCCESS'
                 ,piosPara1      => 'DBX_PART : ' || TO_CHAR(v_DBX_PART) || ' CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff)
                 ,piosPara2      => 'HIGHVAL_DATE  : ' || TO_CHAR(v_HIGH_VALUE_DATE,'YYMMDD HH24:MI:SS')
                 ,piosPara3      => 'COMPARE_DATE  : ' || TO_CHAR(v_COMPARE_DATE,'YYMMDD HH24:MI:SS'));
            END IF;
-- range end
          END IF;
         END IF;
      END IF;


     /*
      * Partiton Range CUSTOMER_ID
      */
      IF (V_PKEYCOL = 'CUSTOMER_ID' OR V_PKEYCOL = 'CUST_INFO_CUSTOMER_ID' OR V_PKEYCOL = 'CUST_ID_ASSIGN'
          OR V_PKEYCOL = 'DR_ADDRESSEE_CUSTOMER_ID' )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CUSTOMER_ID  (min/max):' || CUSTOMER_ID_MIN  || ' ' || CUSTOMER_ID_MAX  );
         v_PART_Message3 := 'CUSTOMER_ID  (min/max):' || CUSTOMER_ID_MIN  || ' ' || CUSTOMER_ID_MAX ;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( CUSTOMER_ID_MAX != '0' OR CUSTOMER_ID_MAX != '0' )
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || CUSTOMER_ID_MIN ;
            str := str || ', ' || v_PHIGHVALUE || '-' || CUSTOMER_ID_MAX || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;
-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  dbx_customer ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE customer_id < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND customer_id >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE customer_id < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE customer_id >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end
          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range CONTRACT_ID
      */
      IF (V_PKEYCOL = 'CONTRACT_ID' OR V_PKEYCOL = 'CO_ID' OR V_PKEYCOL = 'BI_BALAN_IDENT_CONTRACT_ID'
          OR V_PKEYCOL = 'SB_CUST_INFO_CONTRACT_ID'  )  AND
         V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CONTRACT_ID  (min/max):' || CONTRACT_ID_MIN  || ' ' || CONTRACT_ID_MAX  );
         v_PART_Message3 := 'CONTRACT_ID  (min/max):' || CONTRACT_ID_MIN  || ' ' || CONTRACT_ID_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( CONTRACT_ID_MIN != '0' OR CONTRACT_ID_MAX != '0' )
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || CONTRACT_ID_MIN ;
            str := str || ', ' || v_PHIGHVALUE || '-' || CONTRACT_ID_MAX || '  from dual';
    --      showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;
-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  dbx_contract ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE co_id < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND co_id >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE co_id < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE co_id >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end

          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range OHXACT
      */
      IF (V_PKEYCOL = 'OHXACT' OR V_PKEYCOL = 'OTXACT' )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('OHXACT       (min/max):' || OHXACT_MIN       || ' ' || OHXACT_MAX       );
         v_PART_Message3 := 'OHXACT       (min/max):' || OHXACT_MIN       || ' ' || OHXACT_MAX ;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( OHXACT_MIN != '0' OR OHXACT_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || OHXACT_MIN      ;
            str := str || ', ' || v_PHIGHVALUE || '-' || OHXACT_MAX      || '  from dual';
  --        showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;
-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  dbx_orderhdr ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE ohxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND ohxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE ohxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE ohxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end

          END IF;
         END IF;
      END IF;


     /*
      * Partiton Range CAXACT
      */
      IF (V_PKEYCOL = 'CAXACT' )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CAXACT       (min/max):' || CAXACT_MIN       || ' ' || CAXACT_MAX       );
         v_PART_Message3 := 'CAXACT       (min/max):' || CAXACT_MIN       || ' ' || CAXACT_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( CAXACT_MIN != '0' OR CAXACT_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || CAXACT_MIN      ;
            str := str || ', ' || v_PHIGHVALUE || '-' || CAXACT_MAX      || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;
-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_CASHRECEIPTS ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE caxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND caxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE caxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE caxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end

          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range CADXACT
      */
      IF (V_PKEYCOL = 'CADXACT' )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CADXACT      (min/max):' || CADXACT_MIN       || ' ' || CADXACT_MAX       );
         v_PART_Message3 := 'CADXACT      (min/max):' || CADXACT_MIN       || ' ' || CADXACT_MAX ;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( CAXACT_MIN != '0' OR CAXACT_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || CADXACT_MIN      ;
            str := str || ', ' || v_PHIGHVALUE || '-' || CADXACT_MAX      || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;
-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_CASHDETAIL ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE cadxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND cadxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE cadxact      < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE cadxact      >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end


          END IF;
         END IF;
      END IF;


     /*
      * Partiton Range PRM_VALUE_ID
      */
      IF (V_PKEYCOL = 'PRM_VALUE_ID' )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('PRM_VALUE_ID (min/max):' || PRM_VALUE_ID_MIN || ' ' || PRM_VALUE_ID_MAX );
         v_PART_Message3 := 'PRM_VALUE_ID (min/max):' || PRM_VALUE_ID_MIN || ' ' || PRM_VALUE_ID_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( PRM_VALUE_ID_MIN != '0' OR PRM_VALUE_ID_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || PRM_VALUE_ID_MIN;
            str := str || ', ' || v_PHIGHVALUE || '-' || PRM_VALUE_ID_MAX || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;

-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_PARAMETER_VALUE ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE PRM_VALUE_ID < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND PRM_VALUE_ID >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE PRM_VALUE_ID < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE PRM_VALUE_ID >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end

          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range REQUEST_ID or REQUEST
      */
      IF (V_PKEYCOL = 'REQUEST_ID' OR V_PKEYCOL = 'REQUEST') AND 0 = instr(V_TABLE_NAME,'CRH')         --  V_TABLE_NAME NOT LIKE 'CRH%'
                                                             AND 0 = instr(V_TABLE_NAME,'PORTING_REQ') -- V_TABLE_NAME NOT LIKE 'PORTING_REQ%'
                                                             AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('REQUEST_ID   (min/max):' || REQUEST_MIN || ' ' || REQUEST_MAX );
         v_PART_Message3 := 'REQUEST_ID   (min/max):' || REQUEST_MIN || ' ' || REQUEST_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( REQUEST_MIN != '0' OR REQUEST_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || REQUEST_MIN;
            str := str || ', ' || v_PHIGHVALUE || '-' || REQUEST_MAX || '  from dual';
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;

-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_REQUEST         ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE REQUEST_ID  < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND REQUEST_ID  >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE REQUEST_ID  < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE REQUEST_ID  >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end

          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range REQUEST_ID of CRH request
      */
      IF (V_PKEYCOL = 'REQUEST_ID' AND 1 = instr(V_TABLE_NAME,'CRH')        -- V_TABLE_NAME LIKE 'CRH%'
                                   AND 0 = instr(V_TABLE_NAME,'PORTING_REQ') -- V_TABLE_NAME NOT LIKE 'PORTING_REQ%'
          )  AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CRH REQUEST_ID (min/max):' || CRH_REQUEST_MIN || ' ' || CRH_REQUEST_MAX );
         v_PART_Message3 := 'CRH REQUEST_ID (min/max):' || CRH_REQUEST_MIN || ' ' || CRH_REQUEST_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( CRH_REQUEST_MIN != '0' OR CRH_REQUEST_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || CRH_REQUEST_MIN ;
            str := str || ', ' || v_PHIGHVALUE || '-' || CRH_REQUEST_MAX || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;

-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_CRH_REQUEST     ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE REQUEST_ID  < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND REQUEST_ID  >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE REQUEST_ID  < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE REQUEST_ID  >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end


          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range BILLCYCLE_GROUP_ID
      */
      IF (V_PKEYCOL = 'BILLCYCLE_GROUP_ID' OR V_PKEYCOL = 'BILLCYCLE_GROUP' ) AND V_PTYPE = 'RANGE'
      THEN
--       CallInfo('CRH BILLCYCLE_GROUP_ID (min/max):' || BILLCYCLE_GROUP_ID_MIN || ' ' || BILLCYCLE_GROUP_ID_MAX );
         v_PART_Message3 := 'CRH BILLCYCLE_GROUP_ID (min/max):' || BILLCYCLE_GROUP_ID_MIN || ' ' || BILLCYCLE_GROUP_ID_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') AND v_PREV_HIGH_VALUE_NUM IS NULL THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( BILLCYCLE_GROUP_ID_MIN != '0' OR BILLCYCLE_GROUP_ID_MAX != '0')
          THEN
           IF 0 = instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
           /*
            *  Check int  partition
            */
            str := 'select ' || v_PHIGHVALUE || '-' || BILLCYCLE_GROUP_ID_MIN;
            str := str || ', ' || v_PHIGHVALUE || '-' || BILLCYCLE_GROUP_ID_MAX || '  from dual';
--          showStatement(str);
            execute immediate str into check_int_low, check_int_high;
--          showStatement('CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high)  );
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'CHECK (low/high): ' || TO_CHAR(check_int_low) || '/' || TO_CHAR(check_int_high) );
            IF check_int_low > 0 then
               v_DBX_PART := 1;
            ELSE
               v_DBX_PART := 0;
            END IF;

            IF check_int_high > 0 then
               check_int_diff := check_int_high;
            ELSE
               check_int_diff := 0;
            END IF;
           END IF;

-- range begin
            IF v_HIGH_VALUE_NUM IS NOT NULL OR v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                str := 'SELECT count(*) ';
                str := str || ' FROM  DBX_BILLCYCLE_GROUP  ';
                --
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE BILLCYCLE_GROUP_ID < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                   str := str || ' AND BILLCYCLE_GROUP_ID >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS NOT NULL AND v_PREV_HIGH_VALUE_NUM IS     NULL THEN
                   str := str || ' WHERE BILLCYCLE_GROUP_ID < ' || TO_CHAR(v_HIGH_VALUE_NUM) ;
                END IF;
                IF v_HIGH_VALUE_NUM IS     NULL AND v_PREV_HIGH_VALUE_NUM IS NOT NULL THEN
                   str := str || ' WHERE BILLCYCLE_GROUP_ID >= ' || TO_CHAR(v_PREV_HIGH_VALUE_NUM) ;
                END IF;
                --
             -- showStatement(str);
                execute immediate str into v_PART_ROW_COUNTER;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => 'ROWS: ' || TO_CHAR(v_PART_ROW_COUNTER)
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
                IF v_PART_ROW_COUNTER > 0 then
                   v_DBX_PART := 1;
                   check_int_diff := 0;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;
-- range end


          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range BILLCYCLE_GROUP_ID
      */
      IF (V_PKEYCOL = 'BILLCYCLE_GROUP_ID' OR V_PKEYCOL = 'BILLCYCLE_GROUP' ) AND V_PTYPE = 'LIST'
      THEN
--       CallInfo('CRH BILLCYCLE_GROUP_ID (min/max):' || BILLCYCLE_GROUP_ID_MIN || ' ' || BILLCYCLE_GROUP_ID_MAX );
         v_PART_Message3 := 'CRH BILLCYCLE_GROUP_ID (min/max):' || BILLCYCLE_GROUP_ID_MIN || ' ' || BILLCYCLE_GROUP_ID_MAX;

         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
          IF ( BILLCYCLE_GROUP_ID_MIN != '0' OR BILLCYCLE_GROUP_ID_MAX != '0')
          THEN
           /*
            *  Check int  partition
            */
            IF v_PHIGHVALUE = 'DEFAULT' THEN
                v_DBX_PART := 1;
            ELSE
                str := 'SELECT count(*) ';
                str := str || ' FROM  dbx_billcycle_group ';
                str := str || ' WHERE billcycle_group_id = ' || v_PHIGHVALUE ;

                showStatement(str);
                execute immediate str into check_bill;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => '/CHECK: ' || TO_CHAR(check_bill) );
--              showStatement('/CHECK: ' || TO_CHAR(check_bill) );
                IF check_bill > 0 then
                   v_DBX_PART := 1;
                ELSE
                   v_DBX_PART := 0;
                END IF;
            END IF;


          END IF;
         END IF;
      END IF;

     /*
      * Partiton Range FAMILY_GROUP_ID
      */
      IF (V_PKEYCOL = 'FAMILY_GROUP_ID' ) AND V_PTYPE = 'LIST'
      THEN
         IF 0 != instr(upper(v_PHIGHVALUE),'MAXVALUE') THEN
            v_DBX_PART := 1;
            check_int_diff := 1000000000;
--          showStatement('CHECK: MAXVALUE');
            i := i+1;
            DBXLogStart ( piosTableName  => 'DBX_PART'
              ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
              ,piosMessage    => 'SUCCESS'
              ,piosPara1      => 'CHECK: MAXVALUE');
         ELSE
           /*
            *  Check int  partition
            */
            IF v_PHIGHVALUE = 'DEFAULT' THEN
                v_DBX_PART := 1;
            ELSE
                str := 'SELECT count(*) ';
                str := str || ' FROM  dbx_family_group ';
                str := str || ' WHERE family_group_id = ' || v_PHIGHVALUE ;

                showStatement(str);
                execute immediate str into check_bill;
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => '/CHECK: ' || TO_CHAR(check_bill) );
--              showStatement('/CHECK: ' || TO_CHAR(check_bill) );
                IF check_bill > 0 then
                   v_DBX_PART := 1;
                ELSE
                   v_DBX_PART := 0;
                END IF;
             END IF;
         END IF;
      END IF;

     /*
      * Takeover Partition
      */
      IF v_DBX_PART = 1 THEN
        /*
         * Insert DBX_PART
         */
         str := 'INSERT INTO dbx_part (dbx_table_name,dbx_partition_name,dbx_check_diff) ' ;
         str := str || 'VALUES (' ;
         str := str || '''' || v_table_name || '''' || ' ';
         IF v_SNAME IS NOT NULL THEN
            str := str || ','  || '''' || v_SNAME  || '''' || ' ';
         ELSE
            str := str || ','  || '''' || v_PNAME  || '''' || ' ';
         END IF;
         str := str || ','  || TO_CHAR(check_int_diff) || ') ';

         begin
--           showStatement(str);
             execute immediate str;
             IF v_SNAME IS NOT NULL THEN
                i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => v_table_name || ':' || v_SNAME
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
             -- showStatement('DBX_PART> Insert ' ||  v_table_name || ':' || v_SNAME  );
             ELSE
               i := i+1;
                DBXLogStart ( piosTableName  => 'DBX_PART'
                             ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                             ,piosMessage    => 'SUCCESS'
                             ,piosPara1      => str
                             ,piosPara2      => v_table_name || ':' || v_PNAME
                             ,piosPara3      => 'CHECK_INT_DIFF: ' || TO_CHAR(check_int_diff) );
--              showStatement('DBX_PART> Insert ' ||  v_table_name || ':' || v_PNAME  );
             END IF;
         exception when others then
             CallInfo(substr(SQLERRM,1,250));
             CallInfo(substr(str,1,250) );
         end;

      END IF;

      DBXLogStop  ( piosTableName  => 'DBX_PART'
                   ,piosActionName => v_PART_ActionName
                   ,piosPara3      => v_PART_Message3 );

      l_table_name := v_table_name;
      check_int_diff := 0;
      check_int_high := 0;
      check_int_low  := 0;
      v_PART_Message3 := '';
  END LOOP;
  CLOSE cur;
  DBXLogStop  ( piosTableName  => 'DBX_PART'
               ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i_end,'099999999'))
               ,piosMessage    => 'SUCCESS' );

 /*
  * Delete upper partitions (dbx_check_diff > 0 ) which are not required
  */
  str := 'delete from dbx_part p1 ';
  str := str || 'where p1.dbx_check_diff > 0 ';
  str := str || 'and p1.dbx_check_diff NOT IN (select min(dbx_check_diff)  ';
  str := str || '                              from dbx_part p2 ';
  str := str || '                              where p2.dbx_table_name = p1.dbx_table_name ';
  str := str || '                              and   p2.dbx_check_diff > 0) ';

  begin
--    showStatement(str);
      execute immediate str;
      CallInfo('DBX_PART> Delete higher partitions which are not required: ' || TO_CHAR(SQL%ROWCOUNT) );
      i := i+1;
      DBXLogStart ( piosTableName  => 'DBX_PART'
                   ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                   ,piosMessage    => 'SUCCESS'
                   ,piosPara1      => str);
  exception when others then
      CallInfo(substr(SQLERRM,1,250));
      CallInfo(substr(str,1,250) );
  end;


 /*
  * Show all partitioned tables where valid partitions were found
  */
  v_select_statement := 'SELECT  dbx_table_name, count(*) tcounter  ';
  v_select_statement := v_select_statement || ' FROM dbx_part ' ;
  v_select_statement := v_select_statement || ' GROUP BY dbx_table_name ' ;
  v_select_statement := v_select_statement || ' ORDER BY dbx_table_name ' ;

  CallInfo('DBX_PART> Partitioned tables with valid partitions');
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name , counter ;
      EXIT WHEN cur%NOTFOUND;
      CallInfo('DBX_PART> Table: ' || v_table_name || ' (' || TO_CHAR(counter) || ')');
      i := i+1;
      DBXLogStart ( piosTableName  => 'DBX_PART'
                   ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                   ,piosMessage    => 'SUCCESS'
                   ,piosPara1      => 'VALID PARTITIONS:' ||  v_table_name || ' (' || TO_CHAR(counter) || ')'  );
  END LOOP;
  CLOSE cur;

 /*
  * Show all partitioned tables where no valid partitions were found
  */
  v_select_statement := 'SELECT  dbx_table_name  ';
  v_select_statement := v_select_statement || ' FROM dbx_tables ' ;
  v_select_statement := v_select_statement || ' WHERE dbx_column_name is not null ' ;
  v_select_statement := v_select_statement || ' AND   dbx_schema_name = ' || '''' || 'SYSADM' || '''';
  v_select_statement := v_select_statement || ' AND   dbx_pkeycol is not null ' ;
  v_select_statement := v_select_statement || ' AND   dbx_exclude = ' || '''' || 'N' || '''' ;
  v_select_statement := v_select_statement || ' AND    (dbx_where_condition is null or dbx_where_condition != ' ||
                                              '''' || '1 = 2' || '''' || ') ';
  v_select_statement := v_select_statement || ' AND   dbx_table_name in (  ' ;
  v_select_statement := v_select_statement || ' select dbx_table_name from dbx_part_pvalues ' ;
  v_select_statement := v_select_statement || ' MINUS' ;
  v_select_statement := v_select_statement || ' select dbx_table_name from dbx_part) ' ;
  v_select_statement := v_select_statement || ' ORDER BY dbx_table_name ' ;

  CallInfo('DBX_PART> Partitioned tables without valid partitions');
  OPEN cur FOR v_select_statement ;
  LOOP
      FETCH cur INTO v_table_name ;
      EXIT WHEN cur%NOTFOUND;
      CallInfo('DBX_PART> Table: ' || v_table_name );
      i := i+1;
      DBXLogStart ( piosTableName  => 'DBX_PART'
                   ,piosActionName => 'SetPartition_' || trim(TO_CHAR(i,'099999999'))
                   ,piosMessage    => 'SUCCESS'
                   ,piosPara1      => 'NO VALID PARTITIONS:' ||  v_table_name || ' (' || TO_CHAR(counter) || ')'  );
  END LOOP;
  CLOSE cur;

ELSE
  CallInfo('Partition pruning disabled!');
END IF;

exception when others then
  callinfo('SetPartition: Error:');
  CallError(SQLERRM);
END SetPartition;


 /*
   **=====================================================
   **
   ** Procedure: CheckRangeID
   **
   ** Purpose:   Verify if between clauses should be used
   **
   **=====================================================
*/


PROCEDURE CheckRangeID (
        piosRangeName IN VARCHAR2
)
AS
 vdbBSCS                   VARCHAR2(4000);
 CUSTOMER_ID_MAX           VARCHAR2(4000);
 CUSTOMER_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 OHXACT_MAX                VARCHAR2(4000);
 OHXACT_MIN                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 counter                   INTEGER;
 CUSTOMER_ID_MAX_NUM       INTEGER;
 CUSTOMER_ID_MIN_NUM       INTEGER;
 CONTRACT_ID_MAX_NUM       INTEGER;
 CONTRACT_ID_MIN_NUM       INTEGER;
 OHXACT_MAX_NUM            INTEGER;
 OHXACT_MIN_NUM            INTEGER;

BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  CUSTOMER_ID_MAX          := GetParameter('CUSTOMER_ID_MAX');
  CUSTOMER_ID_MIN          := GetParameter('CUSTOMER_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  OHXACT_MAX               := GetParameter('OHXACT_MAX');
  OHXACT_MIN               := GetParameter('OHXACT_MIN');

  IF piosRangeName = 'CUSTOMER_ID_MAX' THEN
     begin
        CUSTOMER_ID_MAX_NUM := TO_NUMBER(CUSTOMER_ID_MAX);
        CUSTOMER_ID_MIN_NUM := TO_NUMBER(CUSTOMER_ID_MIN);
     exception when others then
        SetParameter ('USE_CUSTOMER_ID_RANGE','Y');
     end;

     str := 'SELECT NVL(MAX(CUSTOMER_ID),0) FROM CUSTOMER_ALL';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDCustomer'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str into counter;
     callinfo('CheckRangeIDCustomer: Max ID:' || to_CHAR(counter) );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDCustomer'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;

     IF counter > 5 * (CUSTOMER_ID_MAX_NUM - CUSTOMER_ID_MIN_NUM) THEN
        SetParameter ('USE_CUSTOMER_ID_RANGE','Y');
        callinfo('CheckRangeIDohxact: USE_CUSTOMER_ID_RANGE: Y');
     ELSE
        SetParameter ('USE_CUSTOMER_ID_RANGE','N');
        callinfo('CheckRangeIDohxact: USE_CUSTOMER_ID_RANGE: N');
     END IF;

  END IF;

  IF piosRangeName = 'CONTRACT_ID_MAX' THEN
     begin
        CONTRACT_ID_MAX_NUM := TO_NUMBER(CONTRACT_ID_MAX);
        CONTRACT_ID_MIN_NUM := TO_NUMBER(CONTRACT_ID_MIN);
     exception when others then
        SetParameter ('USE_CONTRACT_ID_RANGE','Y');
     end;


     str := 'SELECT NVL(MAX(CO_ID),0) FROM CONTRACT_ALL';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDContract'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str into counter;
     callinfo('CheckRangeIDContract: Max ID:' || to_CHAR(counter) );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDContract'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;

     IF counter > 5 * (CONTRACT_ID_MAX_NUM - CONTRACT_ID_MIN_NUM) THEN
        SetParameter ('USE_CONTRACT_ID_RANGE','Y');
        callinfo('CheckRangeIDohxact: USE_CONTRACT_ID_RANGE: Y');
     ELSE
        SetParameter ('USE_CONTRACT_ID_RANGE','N');
        callinfo('CheckRangeIDohxact: USE_CONTRACT_ID_RANGE: N');
     END IF;

  END IF;

  IF piosRangeName = 'OHXACT_MAX' THEN

     begin
        OHXACT_MAX_NUM := TO_NUMBER(OHXACT_MAX);
        OHXACT_MIN_NUM := TO_NUMBER(OHXACT_MIN);
     exception when others then
        SetParameter ('USE_OHXACT_RANGE','Y');
     end;

     str := 'SELECT NVL(MAX(OHXACT),0) FROM ORDERHDR_ALL';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;

     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDohxact'
                  ,piosMessage    => 'FILL'
                  ,piosPara1      => str);
     execute immediate str into counter;
     callinfo('CheckRangeIDohxact: Max ID:' || to_CHAR(counter) );
     DBXLogStop  ( piosTableName  => 'DBX_CUSTOMER'
                  ,piosActionName => 'CheckRangeIDohxact'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     commit;

     IF counter > 5 * (OHXACT_MAX_NUM - OHXACT_MIN_NUM) THEN
        SetParameter ('USE_OHXACT_RANGE','Y');
        callinfo('CheckRangeIDohxact: USE_OHXACT_RANGE: Y');
     ELSE
        SetParameter ('USE_OHXACT_RANGE','N');
        callinfo('CheckRangeIDohxact: USE_OHXACT_RANGE: N');
     END IF;

  END IF;


  stop_time := GetTime;
  callinfo('CheckRangeID: Start:' || start_time );
  callinfo('CheckRangeID: Stop: ' || stop_time );


EXCEPTION WHEN OTHERS THEN
  callinfo('CheckRangeID: Error:');
  CallError(SQLERRM);
END CheckRangeID;

 /*
   **=====================================================
   **
   ** Procedure: FillTableLCTransaction
   **
   ** Purpose:   Fill Table DBX_LCTransaction
   **
   **=====================================================
*/


PROCEDURE FillTableDBXLCTransaction
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 minval                    NUMBER;
 maxval                    NUMBER;
 IS_LCTRANS                VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  IS_LCTRANS               := GetParameter('IS_LCTRANS');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');

IF IS_LCTRANS = 'Y' THEN

  str := 'INSERT /*+  APPEND */ INTO dbx_lctransaction ';
  str := str || '(TRANSACTION_ID) ';
  str := str || 'SELECT /*+ index (tab PK_PREPAID_LC_ATTR_HIST) ';
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ DISTINCT tab.TRANSACTION_ID FROM PREPAID_LC_ATTR_HIST';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';

  str := str || ' WHERE tab.TRANSACTION_ID IS NOT NULL ';

  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_LCTRANSACTION'
               ,piosActionName => 'FillTableDBXLCTrans'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXLCTransaction: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_LCTRANSACTION'
               ,piosActionName => 'FillTableDBXLCTrans'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXLCTransaction: Start:' || start_time );
  callinfo('FillTableDBXLCTransaction: Stop: ' || stop_time );

END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXLCTransaction: Error:');
  CallError(SQLERRM);
END FillTableDBXLCTransaction;


 /*
   **=====================================================
   **
   ** Procedure: FillTableDBXFupAccounts
   **
   ** Purpose:   Fill Table DBX_FUP_ACCOUNTS
   **
   **=====================================================
*/


PROCEDURE FillTableDBXFupAccounts
AS
 vdbBSCS                   VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 str                       VARCHAR2(32000);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 CONTRACT_ID_MIN           VARCHAR2(4000);
 CONTRACT_ID_MAX           VARCHAR2(4000);
 start_time                VARCHAR2(30);
 stop_time                 VARCHAR2(30);
 minval                    NUMBER;
 maxval                    NUMBER;
 IS_BSCS6_FUP_ACCOUNT_ID   VARCHAR2(4000);
 USE_CONTRACT_ID_RANGE     VARCHAR2(4000);
 EXIST_FUPACCHIST_ACCSTARTDATE VARCHAR2(4000);
 EXIST_FUPACCHIST_COID     VARCHAR2(4000);
 FUPACCHISTSTARTDATE       VARCHAR2(4000);
 vfupaccstartdate_string   VARCHAR2(30);
BEGIN

  start_time               := GetTime;
  vdbBSCS                  := GetParameter('vdbBSCS');
  vialink                  := GetParameter('vialink');
  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  CONTRACT_ID_MIN          := GetParameter('CONTRACT_ID_MIN');
  CONTRACT_ID_MAX          := GetParameter('CONTRACT_ID_MAX');
  IS_BSCS6_FUP_ACCOUNT_ID  := GetParameter('IS_BSCS6_FUP_ACCOUNT_ID');
  USE_CONTRACT_ID_RANGE    := GetParameter('USE_CONTRACT_ID_RANGE');
  EXIST_FUPACCHIST_ACCSTARTDATE := GetParameter('EXIST_FUPACCHIST_ACCSTARTDATE');
  EXIST_FUPACCHIST_COID    := GetParameter('EXIST_FUPACCHIST_COID');
  FUPACCHISTSTARTDATE      := GetParameter('FUPACCHISTSTARTDATE');

  SetParameter ('vfupaccstartdate_string','N');
IF IS_BSCS6_FUP_ACCOUNT_ID = 'Y' THEN

  str := 'INSERT /*+  APPEND */ INTO dbx_fup_accounts ';
  str := str || '(ACCOUNT_ID) ';
  str := str || 'SELECT /*+ index (tab FK_FUP_ACC_HEAD_COID) ';
  if vialink = 'Y' then
     str := str || ' DRIVING_SITE(tab) ';
  end if;
  str := str || ' */ DISTINCT tab.account_id     FROM FUP_ACCOUNTS_HEAD';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' tab , DBX_CONTRACT dbx';
  str := str || ' WHERE tab.ACCOUNT_ID     IS NOT NULL ';
  str := str || ' AND  tab.CO_ID = dbx.CO_ID ';
  IF USE_CONTRACT_ID_RANGE = 'Y' THEN
     str := str || ' AND  tab.CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
  END IF;

  ShowStatement(str);
  DBXLogStart ( piosTableName  => 'DBX_FUP_ACCOUNTS'
               ,piosActionName => 'FillTableDBXFupAcc'
               ,piosMessage    => 'FILL'
               ,piosPara1      => str);
  execute immediate str;
  callinfo('FillTableDBXFupAccounts: Rows inserted ' || to_CHAR(SQL%ROWCOUNT) );
  DBXLogStop  ( piosTableName  => 'DBX_FUP_ACCOUNTS'
               ,piosActionName => 'FillTableDBXFupAcc'
               ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
  commit;

  stop_time := GetTime;
  callinfo('FillTableDBXFupAccounts: Start:' || start_time );
  callinfo('FillTableDBXFupAccounts: Stop: ' || stop_time );

  IF EXIST_FUPACCHIST_COID = 'N' AND trim(FUPACCHISTSTARTDATE) NOT IN ('0') AND FUPACCHISTSTARTDATE IS NOT NULL AND LENGTH(TRIM(FUPACCHISTSTARTDATE)) > 0 THEN
     str := 'select TO_CHAR(nvl(max(account_start_date),sysdate)' || ',' || '''' || 'YYYY-MM-DD' || '''' ||') from fup_accounts_hist';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' WHERE balance_type in (''F'',''I'') and account_id IN (SELECT account_id FROM DBX_FUP_ACCOUNTS)';
     DBXLogStart ( piosTableName  => 'DBX_FUP_ACCOUNTS'
                  ,piosActionName => 'FillTableDBXFupAccMax'
                  ,piosMessage    => 'MAXVALUE'
                  ,piosPara1      => str);
     ShowStatement(str);
     execute immediate str into vfupaccstartdate_string;
     DBXLogStop  ( piosTableName  => 'DBX_FUP_ACCOUNTS'
                  ,piosActionName => 'FillTableDBXFupAccMax'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     SetParameter ('vfupaccstartdate_string',vfupaccstartdate_string);
     start_time := stop_time;
     stop_time := GetTime;
     callinfo('FillTableDBXFupAccounts MaxStartDate Start:' || start_time );
     callinfo('FillTableDBXFupAccounts MaxStartDate Stop: ' || stop_time );
     callinfo('FillTableDBXFupAccounts MaxStartDate:' || vfupaccstartdate_string );
  END IF;


ELSE
  IF EXIST_FUPACCHIST_COID = 'Y' AND trim(FUPACCHISTSTARTDATE) NOT IN ('0') AND FUPACCHISTSTARTDATE IS NOT NULL  AND LENGTH(TRIM(FUPACCHISTSTARTDATE)) > 0 THEN
     str := 'select TO_CHAR(nvl(max(account_start_date),sysdate)' || ',' || '''' || 'YYYY-MM-DD' || '''' ||') from fup_accounts_hist';
     if vialink = 'Y' then
        str := str ||  '@' || vdbBSCS;
     end if;
     str := str || ' WHERE CO_ID IN (SELECT CO_ID FROM DBX_CONTRACT)';
     IF USE_CONTRACT_ID_RANGE = 'Y' THEN
        str := str || ' AND  CO_ID BETWEEN ' || CONTRACT_ID_MIN || ' AND ' || CONTRACT_ID_MAX;
     END IF;
     ShowStatement(str);
     DBXLogStart ( piosTableName  => 'DBX_FUP_ACCOUNTS'
                  ,piosActionName => 'FillTableDBXFupAccMax'
                  ,piosMessage    => 'MAXVALUE'
                  ,piosPara1      => str);
     execute immediate str into vfupaccstartdate_string;
     DBXLogStop  ( piosTableName  => 'DBX_FUP_ACCOUNTS'
                  ,piosActionName => 'FillTableDBXFupAccMax'
                  ,piosMessage    => 'SUCCESS'||'('|| TO_CHAR(SQL%ROWCOUNT) ||')' );
     SetParameter ('vfupaccstartdate_string',vfupaccstartdate_string);
     stop_time := GetTime;
     callinfo('FillTableDBXFupAccounts MaxStartDate Start:' || start_time );
     callinfo('FillTableDBXFupAccounts MaxStartDate Stop: ' || stop_time );
     callinfo('FillTableDBXFupAccounts MaxStartDate:' || vfupaccstartdate_string );
  END IF;

END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('FillTableDBXFupAccounts: Error:');
  CallError(SQLERRM);
END FillTableDBXFupAccounts;


 /*
   **=====================================================
   **
   ** Procedure: SetDBX_Where_FUP_ACCOUNTS_HIST
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/


PROCEDURE SetDBX_Where_FupAccountsHist
(
    piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 viadbxlink                VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 IS_BSCS6_FUP_ACCOUNT_ID   VARCHAR2(4000);
 EXIST_FUPACCHIST_ACCSTARTDATE VARCHAR2(4000);
 FUPACCHISTSTARTDATE       VARCHAR2(4000);
 vfupaccstartdate_string   VARCHAR2(60);
 vuserDBXHOME              VARCHAR2(60) := ' ';
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  viadbxlink               := GetParameter('viadbxlink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  str_hint                 := GetParameter(piosHint);
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  IS_BSCS6_FUP_ACCOUNT_ID  := GetParameter('IS_BSCS6_FUP_ACCOUNT_ID');
  EXIST_FUPACCHIST_ACCSTARTDATE := GetParameter('EXIST_FUPACCHIST_ACCSTARTDATE');
  FUPACCHISTSTARTDATE      := GetParameter('FUPACCHISTSTARTDATE');
  vfupaccstartdate_string  := GetParameter('vfupaccstartdate_string');

IF IS_BSCS6_FUP_ACCOUNT_ID = 'Y' AND EXIST_FUPACCHIST_ACCSTARTDATE = 'Y' THEN
  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.FUP_ACCOUNTS_HIST UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( FUP_ACCOUNTS_HIST ) ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM FUP_ACCOUNTS_HIST'|| '@' || vdbBSCS ;
  str_create := str_create || ' WHERE BALANCE_TYPE IN (''F'',''I'') AND account_id  IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM ' ||
                               vuserDBXHOME ||  '.DBX_FUP_ACCOUNTS';
  if viadbxlink = 'Y' then
     str_create := str_create ||  '@' || vdbDBXHOME;
  end if;
  str_create := str_create || ' ) ';
  if trim(vfupaccstartdate_string) not in ('N') and vfupaccstartdate_string is not null  AND LENGTH(TRIM(vfupaccstartdate_string)) > 0 then
      str_create := str_create || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                  '- ' || FUPACCHISTSTARTDATE ;
  end if;

  str_where := 'BALANCE_TYPE IN (''F'',''I'') AND account_id IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM ' || vuserDBXHOME || '.DBX_FUP_ACCOUNTS)';
  if trim(vfupaccstartdate_string) not in ('N') and vfupaccstartdate_string is not null AND LENGTH(TRIM(vfupaccstartdate_string)) > 0 then
      str_where  := str_where || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                 '- ' || FUPACCHISTSTARTDATE ;
  end if;

  str_where_link := 'BALANCE_TYPE IN (''F'',''I'') AND account_id IN (SELECT /*+ ' || GLOBAL_SUB_HINT || ' */ ACCOUNT_ID FROM DBX_FUP_ACCOUNTS@' || vdbDBXHOME || ' )';
  if trim(vfupaccstartdate_string) not in ('N') and vfupaccstartdate_string is not null AND LENGTH(TRIM(vfupaccstartdate_string)) > 0 then
      str_where_link := str_where_link || ' and account_start_date >= TO_DATE(' || '''' || vfupaccstartdate_string || '''' || ',' || '''' || 'YYYY-MM-DD' || '''' || ')' ||
                                          '- ' || FUPACCHISTSTARTDATE ;
  end if;

  SetDBXWhereClause( piosTableName => 'FUP_ACCOUNTS_HIST'
                    ,piosColumnName => 'ACCOUNT_ID'
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include FUP_ACCOUNTS_HIST ACCOUNT_ID ' || str_hint );

END IF;

EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_FupAccountsHist: Error:');
  CallError(SQLERRM);
END SetDBX_Where_FupAccountsHist;


 /*
   **=====================================================
   **
   ** Function : SetDBX_Where_Entdate
   **
   ** Purpose:   Define a WHERE condition
   **
   **=====================================================
*/

PROCEDURE SetDBX_Where_Entdate
(
     -- Table Name
     piosTableName IN VARCHAR2
     -- Column Name
    ,piosColumnName  IN VARCHAR2  DEFAULT NULL
     -- Where Clause
    ,piosHint        IN VARCHAR2  DEFAULT NULL
)
AS
 vdbBSCS                   VARCHAR2(4000);
 vdbconBSCS                VARCHAR2(4000);
 vdbDBXHOME                VARCHAR2(4000);
 vialink                   VARCHAR2(1);
 GLOBAL_HINT               VARCHAR2(4000);
 GLOBAL_SUB_HINT           VARCHAR2(4000);
 ENTRYDATE                 VARCHAR2(4000);
 str                       VARCHAR2(30000);
 str_create                VARCHAR2(30000);
 str_where                 VARCHAR2(30000);
 str_where_link            VARCHAR2(30000);
 str_storage               VARCHAR2(30000) := ' ';
 str_hint                  VARCHAR2(4000) := ' ';
 vuserDBXHOME              VARCHAR2(60) := ' ';
 counter                   INTEGER;
BEGIN

  vdbBSCS                  := GetParameter('vdbBSCS');
  vdbconBSCS               := GetParameter('vdbconBSCS');
  vdbDBXHOME               := GetParameter('vdbDBXHOME');
  vialink                  := GetParameter('vialink');

  GLOBAL_HINT              := GetParameter('GLOBAL_HINT');
  GLOBAL_SUB_HINT          := GetParameter('GLOBAL_SUB_HINT');
  if piosHint IS NOT NULL and length(trim(piosHint)) > 0 then
     str_hint                 := GetParameter(piosHint);
  else
     str_hint := '';
  end if;
  vuserDBXHOME             := GetParameter('vuserDBXHOME');
  ENTRYDATE                := GetParameter('ENTRYDATE');

  str := 'select count(*) from dba_tab_columns';
  if vialink = 'Y' then
     str := str ||  '@' || vdbBSCS;
  end if;
  str := str || ' where table_name = ' || '''' || piosTableName || '''' ||
         ' and column_name = ' || '''' || piosColumnName || '''' ||
         ' and owner  = ' || '''' || 'SYSADM' || '''';

  execute immediate str into counter;

 if counter = 1 and ENTRYDATE is not null and length(trim(ENTRYDATE)) > 0 and trim(ENTRYDATE) != '0'  then
  str_create := 'CREATE /*+  APPEND */ TABLE SYSADM.' || piosTableName || ' UNRECOVERABLE ' || str_storage ;
  str_create := str_create || ' AS ';
  str_create := str_create || 'SELECT /*+ ' || GLOBAL_HINT ||' DRIVING_SITE( ' || piosTableName || ') ' || ' ' || str_hint ;
  str_create := str_create || '  */ * FROM ' || piosTableName || '@' || vdbBSCS ;
  str_create := str_create || ' WHERE ' || piosColumnName || ' > sysdate - ' || ENTRYDATE ;

  str_where := piosColumnName || ' > sysdate - ' || ENTRYDATE;

  str_where_link := piosColumnName || ' > sysdate - ' || ENTRYDATE;

  SetDBXWhereClause( piosTableName => piosTableName
                    ,piosColumnName => piosColumnName
                    ,piosCreateClause => str_create
                    ,piosWhereClause => str_where
                    ,piosWhereLinkClause => str_where_link
                    ,piosHint => str_hint
                    ,piosStorage => str_storage
                    ,piosDBConnect => vdbconBSCS );

  CallInfo('Include ' || piosTableName || ' ' || piosColumnName || ' ' || str_hint );
 end if;
EXCEPTION WHEN OTHERS THEN
  callinfo('SetDBX_Where_Entdate: Error:');
  CallError(SQLERRM);
END SetDBX_Where_Entdate;

 /*
   **=====================================================
   **
   ** Function : PrintBodyVersion
   **
   ** Purpose:   return the version control information
   **
   **=====================================================
*/


   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN '/main/231' || ' | ' || '2013/04/15' || ' | ' || 'lhs_global/lhs/database/tools/dbextract/scripts/dbx_spp.sql, , DBEXTRACT_2.00, DBEXTRACT_2.00_130710';
   END PrintBodyVersion;


END DBX;
/
