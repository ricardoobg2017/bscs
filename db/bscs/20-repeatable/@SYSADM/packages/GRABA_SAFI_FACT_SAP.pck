CREATE OR REPLACE PACKAGE GRABA_SAFI_FACT_SAP IS
 
  -- 10189
  -- SIS Luis Flores
  -- PDS Fernando Ortega
  -- RGT Ney Miranda
  -- Ultimo Cambio: 28/07/2015
  -- Nueva logica de Facturacion para SAP

  PROCEDURE GRABA_SAFI_PRINCIPAL(PDFECHCIERREPERIODO IN DATE,
                                 pv_producto         IN VARCHAR2, -- NCA 'DTH' solo DTH, 'VOZ' todos los productos exceptio DTH, nulo para procesar voz y dth.  
                                 pv_ejecucion        IN VARCHAR2, -- tipo de ejecucion 'C' commit 'CG' control group 
                                 RESULTADO           OUT VARCHAR2,
                                 PV_MSJ_TRAMA        OUT VARCHAR2);

  PROCEDURE CREAR_SUMARIZADO_SAFI_SAP(PDFECHCIERREPERIODO   IN DATE,
                                      PV_PRODUCTO           IN VARCHAR,
                                      LVFECHANTERIOR        IN VARCHAR,
                                      ID_POLIZA_SIERRA_DTH  IN NUMBER,
                                      ID_POLIZA_SIERRA_BSCS IN NUMBER,
                                      ID_POLIZA_COSTA_DTH   IN NUMBER,
                                      ID_POLIZA_COSTA_BSCS  IN NUMBER,
                                      FECHACIERREP          IN DATE,
                                      LNANOFINAL            IN NUMBER,
                                      LNMESFINAL            IN NUMBER,
                                      FECHACIERRER          IN DATE,
                                      LNDIAFINAL            IN VARCHAR2,
                                      PV_MSJ_TRAMA2         IN OUT VARCHAR2);

  --
  PROCEDURE CREA_ASIENTOS(TIPO_FACTURACION IN NUMBER,
                          FECHACIERRER2    IN VARCHAR2,
                          FECHACIERREP     IN DATE,
                          LNANOFINAL       IN NUMBER,
                          LNMESFINAL       IN NUMBER,
                          FECHACIERRER     IN DATE,
                          GLOSA            IN VARCHAR2,
                          DOC_TYPE         IN VARCHAR2,
                          LVFECHA          IN VARCHAR2,
                          LNDIAFINAL       IN VARCHAR2,
                          LVFECHACTUAL     IN VARCHAR2,
                          CODIGO_ERROR     OUT NUMBER,
                          RESULTADO        IN OUT VARCHAR2);

  --

  --
  FUNCTION VERIFICA_TIPO_FACTURA(PD_PERIODO_FIN IN DATE,
                                 PV_CUSTOMER_ID IN INTEGER) RETURN NUMBER;
  --
  FUNCTION GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;

  --
  FUNCTION GVF_OBTENER_SECUENCIAS(PV_CODIGO IN VARCHAR2) RETURN NUMBER;
  --
  FUNCTION FN_GLOSA_DTH(PV_NOMBRE_CTA IN VARCHAR2,
                        PV_CTA_CTBLE  IN VARCHAR2,
                        PV_FECHACTUAL IN VARCHAR2,
                        PV_GLOSA      IN VARCHAR2,
                        PV_TIPO       IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION FN_IMPUESTO_DTH_IVA_ICE(PV_NOMBRE    IN VARCHAR2,
                                   PV_CTA_CTBLE IN VARCHAR2,
                                   PV_TIPO_IMP  IN VARCHAR2) RETURN NUMBER;
  --
  PROCEDURE GEN_RUBROS_NEG_DTH(FECHACIERREP IN DATE,
                               LV_ERROR     IN OUT VARCHAR2);
  --

  --

  FUNCTION OBTENER_VALOR_PARAMETRO_SAP(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;

  ---

  FUNCTION CREAR_TRAMA(PDFECHCIERREPERIODO IN DATE, --DD/MM/YYYY
                       PV_PRODUCTO         IN VARCHAR2,--VOZ/DTH
                       PV_REGION           IN VARCHAR2, -- COSTA/SIERRA
                       PV_IDENTIFICADOR    OUT VARCHAR2, 
                       ERROR               OUT VARCHAR2) RETURN CLOB;
  --
  PROCEDURE CALCULAR_BASE_IMPONIBLE(PV_FECHA_CIERRE   IN DATE,--DD/MM/YYYY
                                    PV_PRODUCTO       VARCHAR2,--VOZ / DTH / NULL 
                                    PV_REGION         VARCHAR2, -- COSTA / SIERRA
                                    PN_VALOR          OUT NUMBER,
                                    PN_DESCUENTO      OUT NUMBER,
                                    PN_BASE_IMPONIBLE OUT NUMBER,
                                    PV_RESULTADO_BASE OUT VARCHAR2);
  --

  PROCEDURE ENVIA_TRAMAS_SAFI_SAP(PDFECHCIERREPERIODO IN DATE, --FECHA DE CORTE
                                  PV_PRODUCTO         IN VARCHAR2, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
                                  PV_EJECUCION        IN VARCHAR2, -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP 
                                  PV_RESULTADO        OUT VARCHAR2);
  --

  PROCEDURE REENVIA_POLIZAS(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2);

  --
  PROCEDURE anula_polizas(pd_fecha     IN DATE,--DD/MM/YYYY
                          pv_producto  IN VARCHAR2,--VOZ / DTH
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2);
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2);
  PROCEDURE reporte_valores_omitidos(ld_fecha IN DATE,
                                     pv_error OUT VARCHAR2);
  FUNCTION valida_dth_rep_omitidos(pdfechcierreperiodo in date) return number;
END GRABA_SAFI_FACT_SAP;
/
CREATE OR REPLACE PACKAGE BODY GRABA_SAFI_FACT_SAP IS
  pn_id_transaccion NUMBER := sc_id_transaccion_sap.nextval;

  PROCEDURE GRABA_SAFI_PRINCIPAL(PDFECHCIERREPERIODO IN DATE, --FECHA DE CORTE
                                 PV_PRODUCTO         IN VARCHAR2, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
                                 PV_EJECUCION        IN VARCHAR2, -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP 
                                 RESULTADO           OUT VARCHAR2,
                                 PV_MSJ_TRAMA        OUT VARCHAR2) IS
    --
    --=====================================================================================--
    -- Versi�n:  1.0.0
    -- Descripci�n: Creaci�n de Asiento Contables para Facturaci�n F�sica y Electr�nica
    --=====================================================================================--
    -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Arturo Gamboa
    -- Fecha de creaci�n: 14/05/2012 9:42:15
    -- Proyecto: [5328] Facturaci�n Electr�nica
    --=====================================================================================--
    --=====================================================================================--
    -- Modificado por: CLS Luis Rivera (LRI)
    -- L�der proyecto: Ing. Diego Ocampo
    -- PDS: CLS Ing. Allan Endara C.
    -- Fecha de modificaci�n: 15/01/2012 12:10:20
    -- Proyecto: [8708] � Modificaci�n Proceso BSCS JGE_ACC_EN
    --=====================================================================================--
    --=====================================================================================--
    -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SUD SRE)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Cristhian Acosta Ch.
    -- Fecha de creaci�n: 28/05/2013 11:57:50
    -- Proyecto: [8693] VENTA DTH POSTPAGO
    --=====================================================================================--
    --=====================================================================================--
    -- Modificado por: SUD Norman Castro (SUD NCA)
    -- L�der proyecto: SIS Paola Carvajal
    -- Lider PDS:      SUD Cristhian Acosta Chambers
    -- Fecha de modificaci�n: 04/07/2013
    -- Proyecto: [8693] VENTA DTH POSTPAGO
    -- Proposito: Se agrega parametro de simulacion 'CG' o commit 'C', se a�ade el producto a procesar
    --             solo DTH, solo VOZ o todos los productos VOZ y DTH.
    --=====================================================================================-- 
    --=====================================================================================--
    -- Modificado por: SUD Cristhian Acosta Chambers (SUD CAC)
    -- L�der proyecto: SIS Xavier Trivi�o
    -- Lider PDS:      SUD Cristhian Acosta Chambers
    -- Fecha de modificaci�n: 31/07/2014
    -- Proyecto: [9587] - Facturaci�n Electr�nica DTH
    -- Proposito: Generaci�n de asiento contable DTH de Factura Electr�nica
    --=====================================================================================-- 
    --
  
    --=====================================================================================--
    -- Modificado por: RGT Ney Miranda T.
    -- L�der proyecto: SIS Luis Flores
    -- Lider PDS:      RGT Fernando Ortega
    -- Fecha de modificaci�n: 24/04/2015
    -- Proyecto:  Proyecto: [10189] API BSCS SAP
    -- Proposito: Generaci�n de asiento contable  sumarizados por cuenta contable para el envio de polizas a SAP
    --=====================================================================================-- 
    --
  
    ID_POLIZA_COSTA_DTH   NUMBER := 0;
    ID_POLIZA_COSTA_BSCS  NUMBER := 0;
    ID_POLIZA_SIERRA_DTH  NUMBER := 0;
    ID_POLIZA_SIERRA_BSCS NUMBER := 0;
    PV_MSJ_TRAMA2         VARCHAR2(200) := '';
    --  LV_ENVIA_TRAMA        VARCHAR2(20) := '';
    LV_SQL              VARCHAR2(200);
    LC_VALIDA_ID_POLIZA SYS_REFCURSOR;
    LN_CUENTA_PRODUCTO  NUMBER;
    LVSENTENCIA         VARCHAR2(18000);
    LNMESFINAL          NUMBER;
    LNANOFINAL          NUMBER;
    LNDIAFINAL          VARCHAR2(2);
    LVFECHA             VARCHAR2(10);
    LVFECHANTERIOR      VARCHAR(12);
    LVFECHACTUAL        VARCHAR(12);
    GLOSA               VARCHAR2(200);
    FECHACIERREP        DATE;
    FECHACIERRER        DATE;
    FECHACIERRER2       VARCHAR2(20);
    CODIGO_ERROR        NUMBER := 0;
    PV_APLICACION       VARCHAR2(50) := 'GRABA_SAFI_FACT_SAP.GRABA_SAFI_PRINCIPAL';
    MY_ERROR EXCEPTION;
    RESULTADO2        VARCHAR2(5500) := NULL;
    DOC_TYPE          VARCHAR2(100) := NULL;
    pv_resultado_resp VARCHAR2(3500) := NULL;
    -- NCA [8693] 04/07/2013
    LV_ERROR VARCHAR2(2000) := NULL;
    LE_ERROR EXCEPTION;
    --
     -- INICIO [10183] RGT Ney Miranda T.  
    lv_resultado varchar2(2000);
    LE_ERROR_RESPALDA EXCEPTION; 
      -- FIN [10183] RGT Ney Miranda T.  
    CURSOR DESCX IS
      SELECT R.CTAPSOFT,
             R.CTA_DSCTO,
             R.CONTRAPARTIDA_GYE,
             R.CONTRAPARTIDA_UIO
        FROM COB_SERVICIOS R;
  
    --TIPOS DE FACTURACI�N CON SUS RESPECTIVAS GLOSAS Y DOC_TYPE - SUD SRE
    CURSOR C_TIPOS_FACTURAS IS
      SELECT S.*
        FROM TIPOS_FACTURAS_SAFI S
       WHERE S.ESTADO = 'A'
       ORDER BY 1;
    --
    --RECOGE CUENTAS POST PAGO DEL PRODUCTO DTH - SUD SRE
    CURSOR C_CTAPOST IS
      SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
        FROM PRUEBAFINSYS_SAP D
       WHERE D.CTA_CTRAP IS NULL
         AND D.PRODUCTO = 'DTH';
    --
    --
    --CURSOR PARA VALIDAR SI YA HAN SIDO GENERADAS POLIZAS CON ESA FECHA DE CORTE Y ESCENARIO
    CURSOR c_valida_poliza_existente(cv_fecha DATE, cv_producto VARCHAR2) IS
      SELECT COUNT(*)
        FROM gsib_tramas_sap d
       WHERE d.referencia LIKE
             '%' || 'FACTURACION ' ||
             decode(cv_producto,
                    'VOZ',
                    'BSCS',
                    'DTH',
                    'DTH',
                    NULL,
                    'FACTURACION') || '%'
         AND d.fecha_corte = cv_fecha
         AND d.estado NOT IN ('ERROR', 'ANULADA');
    lc_valida_poliza_existente NUMBER := 0;
    --cursor para saber si existen productos dth y poder respaldar los datos
    sql_text_dth  VARCHAR2(100) := ' SELECT COUNT(*)  FROM CO_FACT_' ||
                                   to_char(pdfechcierreperiodo, 'DDMMYYYY') ||
                                   '  WHERE PRODUCTO = ''DTH''AND ROWNUM <= 1'; --1 SI EXISTE DTH , 0 SI NO ESXISTE
    cur_dth       SYS_REFCURSOR;
    lv_existe_dth NUMBER := 0;
  BEGIN
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           '****INICIO GENERA ASIENTOS FACTURACION ' ||
                           pv_producto || ' FECHA DE CORTE :' ||
                           TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    -- [8693] - INI SUD NCA
    IF PV_EJECUCION NOT IN ('C', 'CG') THEN
      LV_ERROR := 'El parametro PV_EJECUCION es erroneo. Ingrese C (COMMIT) � CG (CONTROL GROUP) => ';
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      RAISE LE_ERROR;
    END IF;
    IF PV_PRODUCTO IS NOT NULL THEN
      IF PV_PRODUCTO NOT IN ('DTH', 'VOZ') THEN
        LV_ERROR := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ => ';
        gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
        RAISE LE_ERROR;
      END IF;
    END IF;
    OPEN c_valida_poliza_existente(pdfechcierreperiodo, pv_producto);
    FETCH c_valida_poliza_existente
      INTO lc_valida_poliza_existente;
    CLOSE c_valida_poliza_existente;
    IF lc_valida_poliza_existente > 0 THEN
      lv_error := 'Ya existen polizas Generadas para este producto . ' ||
                  pv_producto;
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      RAISE le_error;
    END IF;
    -- [8693] - FIN SUD NCA
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    --
    FECHACIERREP   := TO_DATE(PDFECHCIERREPERIODO, 'DD/MM/YYYY');
    FECHACIERRER   := TO_DATE(SYSDATE, 'DD/MM/YYYY');
    FECHACIERRER2  := TRIM(TO_CHAR(SYSDATE, 'YYYYMMDD'));
    LNMESFINAL     := TO_NUMBER(TO_CHAR(PDFECHCIERREPERIODO, 'MM'));
    LNDIAFINAL     := TO_CHAR(PDFECHCIERREPERIODO, 'DD');
    LNANOFINAL     := TO_NUMBER(TO_CHAR(PDFECHCIERREPERIODO, 'YYYY'));
    LVFECHANTERIOR := TO_CHAR(ADD_MONTHS(PDFECHCIERREPERIODO, -1),
                              'DD/MM/YYYY');
    LVFECHACTUAL   := TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY');
    LVSENTENCIA    := 'TRUNCATE  TABLE PRUEBAFINSYS_SAP';
    LVFECHA        := TO_CHAR(PDFECHCIERREPERIODO, 'DDMMYYYY');
    EXEC_SQL(LVSENTENCIA);
    --
    SELECT DECODE(LNDIAFINAL,
                  '02',
                  '04',
                  '08',
                  '02',
                  '15',
                  '03',
                  '24',
                  '01')
      INTO LNDIAFINAL
      FROM DUAL;
    COMMIT;
    LVSENTENCIA := NULL;
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Inicio de carga masiva de datos en la tabla PRUEBAFINSYS_SAP ',
                           pn_id_transaccion);
  
    --CARGA DATOS DE LAS CUENTAS POR TIPO DE FACTURACION
    -- INI - 8693 - VENTA DTH POST PAGO
    --CAMBIO EN LA ESTRUCTURA DEL SELECT SEGUN EL PRODUCTO QUE SE NECESITA PROCESAR - SUD NCA
    LVSENTENCIA := 'INSERT INTO PRUEBAFINSYS_SAP  ' ||
                   ' SELECT /* + RULE */  H.TIPO,H.NOMBRE,H.PRODUCTO,SUM(H.VALOR) VALOR ,SUM(H.DESCUENTO) DESCUENTO,H.COST_DESC COMPA�IA , ' ||
                   ' DECODE(H.PRODUCTO,''DTH'',H.CTACLBLEP,MAX(H.CTACLBLEP)), ' || --SUD SRE DECODE PARA DTH
                   ' NULL  CUENTA_DESC, GRABA_SAFI_FACT_SAP.VERIFICA_TIPO_FACTURA(TO_DATE(''' ||
                   FECHACIERREP ||
                   ''',''DD/MM/YYYY''),H.CUSTOMER_ID) TIPO_FACTURACION,NULL,NULL,NULL FROM CO_FACT_' ||
                   LVFECHA || ' H' ||
                   ' WHERE H.CUSTOMER_ID NOT  IN (SELECT CUSTOMER_ID FROM CUSTOMER_TAX_EXEMPTION CTE WHERE CTE.EXEMPT_STATUS = ''A'') ';
  
    gsib_api_sap.reinicia_secuencia(pn_id_transaccion); -- VALIDO SI YA PASO UN ANIO Y REINICIO LA SEQUENCIA
  
    --ASIGNO LOS ID POLIZAS Y EL QUERY SEGUN EL PRODUCTO SELECCIONADO
    IF PV_PRODUCTO = 'DTH' THEN
      --SOLO DTH - RUBRO POSITIVOS
      LVSENTENCIA          := LVSENTENCIA ||
                              ' AND H.PRODUCTO = ''DTH'' AND H.VALOR > 0 ';
      ID_POLIZA_COSTA_DTH  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_DTH := ID_POLIZA_SAP.NEXTVAL;
    ELSIF PV_PRODUCTO = 'VOZ' THEN
      --SOLO VOZ
      LVSENTENCIA           := LVSENTENCIA || ' AND H.PRODUCTO <> ''DTH'' ';
      ID_POLIZA_COSTA_BSCS  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_BSCS := ID_POLIZA_SAP.NEXTVAL;
    ELSIF PV_PRODUCTO IS NULL THEN
      --SI EL PRODUCTO ES NULO TODOS LOS PRODUCTOS
      -- VOZ - DTH RUBROS POSITIVOS
      LVSENTENCIA := LVSENTENCIA ||
                     ' AND (H.PRODUCTO <> ''DTH'' OR (H.PRODUCTO = ''DTH'' AND H.VALOR > 0)) ';
    
      LV_SQL := 'select COUNT(*) from co_fact_' ||
                to_char(PDFECHCIERREPERIODO, 'DDMMYYYY') ||
                ' d where d.producto=''DTH''';
    
      OPEN LC_VALIDA_ID_POLIZA FOR LV_SQL;
      FETCH LC_VALIDA_ID_POLIZA
        INTO LN_CUENTA_PRODUCTO;
      CLOSE LC_VALIDA_ID_POLIZA;
    
      IF LN_CUENTA_PRODUCTO > 0 THEN
        ID_POLIZA_COSTA_DTH  := ID_POLIZA_SAP.NEXTVAL;
        ID_POLIZA_SIERRA_DTH := ID_POLIZA_SAP.NEXTVAL;
      END IF;
    
      ID_POLIZA_COSTA_BSCS  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_BSCS := ID_POLIZA_SAP.NEXTVAL;
    
    END IF;
    LVSENTENCIA := LVSENTENCIA ||
                   ' GROUP BY H.TIPO,H.NOMBRE,H.PRODUCTO,H.COST_DESC, GRABA_SAFI_FACT_SAP.VERIFICA_TIPO_FACTURA(TO_DATE(''' ||
                   FECHACIERREP ||
                   ''',''DD/MM/YYYY''),H.CUSTOMER_ID),H.CTACLBLEP';
    --
    EXEC_SQL(LVSENTENCIA);
    COMMIT;
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Fin de carga masiva de datos en la tabla PRUEBAFINSYS_SAP ',
                           pn_id_transaccion);
  
    --
    --ASIGNA CUENTAS CONTRAPARTIDAS SEGUN LA COMPA�IA - SUD SRE
    FOR RD IN DESCX LOOP
      UPDATE PRUEBAFINSYS_SAP F
         SET F.CTA_DESC  = RD.CTA_DSCTO,
             F.CTA_CTRAP = DECODE(F.COMPA�IA,
                                  'Guayaquil',
                                  RD.CONTRAPARTIDA_GYE,
                                  'Quito',
                                  RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
       WHERE F.CTACTBLE = RD.CTAPSOFT;
    END LOOP;
    COMMIT;
  
    --ACTUALIZA CUENTAS POST PAGADAS CON LA SU CONTRAPARTIDA - SUD SRE
    FOR AZ IN C_CTAPOST LOOP
      UPDATE PRUEBAFINSYS_SAP A
         SET A.CTA_CTRAP =
             (SELECT DECODE(A.COMPA�IA,
                            'Guayaquil',
                            D.CONTRAPARTIDA_GYE,
                            'Quito',
                            D.CONTRAPARTIDA_UIO)
                FROM COB_SERVICIOS D
               WHERE D.CTAPOST = AZ.CTACTBLE
                 AND D.NOMBRE = AZ.NOMBRE)
       WHERE A.ROWID = AZ.ROWID;
    END LOOP COMMIT;
    --
    --CONTROL RUBROS NEGATIVOS DTH
    IF PV_PRODUCTO IN ('DTH') OR PV_PRODUCTO IS NULL THEN
      GEN_RUBROS_NEG_DTH(FECHACIERREP => FECHACIERREP,
                         LV_ERROR     => LV_ERROR);
    
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    END IF;
    -- FIN - 8693 - VENTA DTH POST PAGO
    --INSERTO LOS SERVICIOS PREPAGADOS A TABLA DE DEVENGACION -SUD NCA
    IF PV_EJECUCION = 'C' THEN
      -- SI ES TIPO DE EJECUCION COMMIT GRABA EN SERVICIOS_PREPAGADOS_DTH_SAP
      IF PV_PRODUCTO IN ('DTH') OR PV_PRODUCTO IS NULL THEN
        LVSENTENCIA := NULL;
      
        GSIB_SECURITY.dotraceh(PV_APLICACION,
                               'Inicio de carga masiva de datos hacia la tabla SERVICIOS_PREPAGADOS_DTH_SAP',
                               pn_id_transaccion);
      
        LVSENTENCIA := 'insert into servicios_prepagados_dth_SAP (ciclo, dia, mes, anio, tipo, nombre, valor, ctactble, cta_ctrap, compa�ia, shdes ) ' ||
                       ' select (select f.id_ciclo from fa_ciclos_bscs f' ||
                       ' where f.dia_ini_ciclo = extract(day from to_date(''' ||
                       FECHACIERREP || ''',''dd/mm/yyyy''))) ciclo, ' ||
                       ' extract(day from to_date(''' || FECHACIERREP ||
                       ''',''dd/mm/yyyy'')) dia,' ||
                       ' extract(month from to_date(''' || FECHACIERREP ||
                       ''',''dd/mm/yyyy'')) mes, ' ||
                       ' extract(year from to_date(''' || FECHACIERREP ||
                       ''',''dd/mm/yyyy'')) anio,' ||
                       ' a.tipo, a.nombre, round(a.valor,3), b.ctapost, a.cta_ctrap, a.compa�ia, c.shdes' ||
                       ' from PRUEBAFINSYS_SAP a, COB_SERVICIOS b, mpusntab c' ||
                       ' where a.ctactble <> b.ctapost' ||
                       ' and a.nombre = b.nombre' ||
                       ' and a.producto = ''DTH'' and b.nombre = c.des';
        EXEC_SQL(LVSENTENCIA);
        COMMIT;
      
        GSIB_SECURITY.dotraceh(PV_APLICACION,
                               'Fin de carga masiva de datos hacia la tabla SERVICIOS_PREPAGADOS_DTH_SAP',
                               pn_id_transaccion);
      
      END IF;
    END IF;
    --
    LVSENTENCIA := NULL;
  
    LVSENTENCIA := 'TRUNCATE TABLE GSI_FIN_SAFI_SAP';
    EXEC_SQL(LVSENTENCIA);
    GSIB_SECURITY.dotracem(PV_APLICACION,
                           'Se elimino registros de la tabla temporal GSI_FIN_SAFI_SAP ',
                           pn_id_transaccion);
    COMMIT;
    LVSENTENCIA := ' TRUNCATE TABLE PS_PR_JGEN_ACCT_EN_TMP_SAP ';
    EXEC_SQL(LVSENTENCIA);
    COMMIT;
    GSIB_SECURITY.dotracem(PV_APLICACION,
                           'Se elimino registros de la tabla temporal PS_PR_JGEN_ACCT_EN_TMP_SAP ',
                           pn_id_transaccion);
  
    LVSENTENCIA := 'TRUNCATE TABLE GSI_FIN_SAFI_EXCENTOS_SAP';
    EXEC_SQL(LVSENTENCIA);
    COMMIT;
    GSIB_SECURITY.dotracem(PV_APLICACION,
                           'Se elimino registros de la tabla temporal GSI_FIN_SAFI_EXCENTOS_SAP ',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Inicio del proceso que crea los diferentes asientos contables detallados para UIO Y GYE ',
                           pn_id_transaccion);
  
    --LLAMAR AL PROCESO QUE CREA LOS ASIENTOS
    FOR X IN C_TIPOS_FACTURAS LOOP
      --
      --
      GLOSA    := X.GLOSA;
      DOC_TYPE := X.DOC_TYPE;
      --
      IF GLOSA IS NULL THEN
        RESULTADO := 'GLOSA NO CONFIGURADA PARA ' || X.DESCRIPCION;
        gsib_security.dotraceh(pv_aplicacion, resultado, pn_id_transaccion);
        RAISE MY_ERROR;
      END IF;
      IF DOC_TYPE IS NULL THEN
        RESULTADO := 'DOC_TYPE NO CONFIGURADO PARA ' || X.DESCRIPCION;
        gsib_security.dotraceh(pv_aplicacion, resultado, pn_id_transaccion);
        RAISE MY_ERROR;
      END IF;
      --
      GLOSA := REPLACE(REPLACE(GLOSA, '[%%FECHANTERIOR%%]', LVFECHANTERIOR),
                       '[%%FECHACTUAL%%]',
                       LVFECHACTUAL);
      --
      --CREA LOS DIFERENTES ASIENTOS CONTABLES PARA UIO Y GYE
      GRABA_SAFI_FACT_SAP.CREA_ASIENTOS(TIPO_FACTURACION => X.ID_TIPO, --SE MANEJAN LOS TIPOS DE FACTURACION EXISTENTES
                                        FECHACIERRER2    => FECHACIERRER2,
                                        FECHACIERREP     => FECHACIERREP,
                                        LNANOFINAL       => LNANOFINAL,
                                        LNMESFINAL       => LNMESFINAL,
                                        FECHACIERRER     => FECHACIERRER,
                                        GLOSA            => GLOSA,
                                        DOC_TYPE         => DOC_TYPE,
                                        LVFECHA          => LVFECHA,
                                        LNDIAFINAL       => LNDIAFINAL,
                                        LVFECHACTUAL     => LVFECHACTUAL,
                                        CODIGO_ERROR     => CODIGO_ERROR,
                                        RESULTADO        => RESULTADO);
    
      IF CODIGO_ERROR = 1 THEN
        RAISE MY_ERROR;
      END IF;
    
      RESULTADO2 := RESULTADO2 || ' ' || RESULTADO;
      --
    END LOOP;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Fin del proceso que crea los diferentes asientos contables detallados para UIO Y GYE ',
                           pn_id_transaccion);
  
    --CREA LOS DIFERENTES ASIENTOS CONTABLES SUPARIZADOS POR CUENTA CONTABLE PARA UIO Y GYE
    GRABA_SAFI_FACT_SAP.CREAR_SUMARIZADO_SAFI_SAP(PDFECHCIERREPERIODO,
                                                  PV_PRODUCTO,
                                                  LVFECHANTERIOR,
                                                  ID_POLIZA_SIERRA_DTH,
                                                  ID_POLIZA_SIERRA_BSCS,
                                                  ID_POLIZA_COSTA_DTH,
                                                  ID_POLIZA_COSTA_BSCS,
                                                  FECHACIERREP,
                                                  LNANOFINAL,
                                                  LNMESFINAL,
                                                  FECHACIERRER,
                                                  LNDIAFINAL,
                                                  PV_MSJ_TRAMA2);
  
    IF CODIGO_ERROR = 2 THEN
      /*cls lri ini*/
    
      BEGIN
        IF pv_ejecucion = 'C' THEN
        
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Inicio de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_FINSYS',
                                 pn_id_transaccion);
        
          INSERT INTO ps_jgen_acct_entry_finsys
            (BUSINESS_UNIT,
             TRANSACTION_ID,
             TRANSACTION_LINE,
             LEDGER_GROUP,
             LEDGER,
             ACCOUNTING_DT,
             APPL_JRNL_ID,
             BUSINESS_UNIT_GL,
             FISCAL_YEAR,
             ACCOUNTING_PERIOD,
             JOURNAL_ID,
             JOURNAL_DATE,
             JOURNAL_LINE,
             ACCOUNT,
             ALTACCT,
             DEPTID,
             OPERATING_UNIT,
             PRODUCT,
             FUND_CODE,
             CLASS_FLD,
             PROGRAM_CODE,
             BUDGET_REF,
             AFFILIATE,
             AFFILIATE_INTRA1,
             AFFILIATE_INTRA2,
             CHARTFIELD1,
             CHARTFIELD2,
             CHARTFIELD3,
             PROJECT_ID,
             CURRENCY_CD,
             STATISTICS_CODE,
             FOREIGN_CURRENCY,
             RT_TYPE,
             RATE_MULT,
             RATE_DIV,
             MONETARY_AMOUNT,
             FOREIGN_AMOUNT,
             STATISTIC_AMOUNT,
             MOVEMENT_FLAG,
             DOC_TYPE,
             DOC_SEQ_NBR,
             DOC_SEQ_DATE,
             JRNL_LN_REF,
             LINE_DESCR,
             IU_SYS_TRAN_CD,
             IU_TRAN_CD,
             IU_ANCHOR_FLG,
             GL_DISTRIB_STATUS,
             PROCESS_INSTANCE)
            SELECT BUSINESS_UNIT,
                   TRANSACTION_ID,
                   TRANSACTION_LINE,
                   LEDGER_GROUP,
                   LEDGER,
                   ACCOUNTING_DT,
                   APPL_JRNL_ID,
                   BUSINESS_UNIT_GL,
                   FISCAL_YEAR,
                   ACCOUNTING_PERIOD,
                   JOURNAL_ID,
                   JOURNAL_DATE,
                   JOURNAL_LINE,
                   ACCOUNT,
                   ALTACCT,
                   DEPTID,
                   OPERATING_UNIT,
                   PRODUCT,
                   FUND_CODE,
                   CLASS_FLD,
                   PROGRAM_CODE,
                   BUDGET_REF,
                   AFFILIATE,
                   AFFILIATE_INTRA1,
                   AFFILIATE_INTRA2,
                   CHARTFIELD1,
                   CHARTFIELD2,
                   CHARTFIELD3,
                   PROJECT_ID,
                   CURRENCY_CD,
                   STATISTICS_CODE,
                   FOREIGN_CURRENCY,
                   RT_TYPE,
                   RATE_MULT,
                   RATE_DIV,
                   MONETARY_AMOUNT,
                   FOREIGN_AMOUNT,
                   STATISTIC_AMOUNT,
                   MOVEMENT_FLAG,
                   DOC_TYPE,
                   DOC_SEQ_NBR,
                   DOC_SEQ_DATE,
                   JRNL_LN_REF,
                   substr(LINE_DESCR, 1, 100),
                   IU_SYS_TRAN_CD,
                   IU_TRAN_CD,
                   IU_ANCHOR_FLG,
                   GL_DISTRIB_STATUS,
                   PROCESS_INSTANCE
              FROM GSI_FIN_SAFI_SAP;
          COMMIT;
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Fin de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_FINSYS',
                                 pn_id_transaccion);
        END IF;
      EXCEPTION
      
        WHEN OTHERS THEN
          RESULTADO := PV_APLICACION || ' ' || SQLERRM;
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Error al realizar la carga masiva hacia la tabla PS_JGEN_ACCT_ENTRY_FINSYS ' ||
                                 SQLERRM,
                                 pn_id_transaccion);
        
      END;
    
      BEGIN
      
        IF pv_ejecucion = 'C' THEN
        
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Inicio de carga masiva de datos hacia la tabla PS_PR_JGEN_ACCT_EN_FINSYS',
                                 pn_id_transaccion);
        
          INSERT INTO PS_PR_JGEN_ACCT_EN_FINSYS
            (BUSINESS_UNIT,
             TRANSACTION_ID,
             TRANSACTION_LINE,
             LEDGER_GROUP,
             LEDGER,
             PR_JRNL_LN_REF2,
             PR_JRNL_TIPO_IVA,
             PR_JRNL_CICLO,
             PR_ID_CIA_REL,
             PR_JRNL_LN_REF3,
             PR_LINE_DESC2,
             PR_JRNL_LN_REF_INT,
             PR_FEC_PROV_INTERC,
             PR_RUC_PROV_INTERC,
             PR_SERVICIO_INTERC,
             PR_ESTADO_INTERC,
             PR_ID_PROV_INTERC,
             PR_TIPO_SERV,
             ID_AGRUPACION,
             PR_FEC_INI_DEVEN,
             PR_FEC_FIN_DEVEN)
            SELECT BUSINESS_UNIT,
                   TRANSACTION_ID,
                   TRANSACTION_LINE,
                   LEDGER_GROUP,
                   LEDGER,
                   PR_JRNL_LN_REF2,
                   PR_JRNL_TIPO_IVA,
                   PR_JRNL_CICLO,
                   ' ',
                   ' ',
                   ' ',
                   ' ',
                   trunc(SYSDATE),
                   ' ',
                   ' ',
                   ' ',
                   ' ',
                   ' ',
                   ' ',
                   trunc(SYSDATE),
                   trunc(SYSDATE)
              FROM PS_PR_JGEN_ACCT_EN_TMP_SAP;
          COMMIT;
        
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Fin de carga masiva de datos hacia la tabla PS_PR_JGEN_ACCT_EN_FINSYS',
                                 pn_id_transaccion);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          RESULTADO := PV_APLICACION || ' ' || SQLERRM;
        
          GSIB_SECURITY.dotraceh(PV_APLICACION,
                                 'Error al realizar la carga masiva hacia la tabla PS_PR_JGEN_ACCT_EN_FINSYS ' ||
                                 SQLERRM,
                                 pn_id_transaccion);
        
          RAISE MY_ERROR;
      END;
      /*cls lri fin*/
    
      IF pv_ejecucion = 'C' THEN
      
        /*      REGUN.SEND_MAIL.MAIL@COLECTOR('gsibilling@claro.com.ec',
                                      'mgavilanes@conecel.com,lmedina@conecel.com',
                                      'tescalante@conecel.com',
                                      'GSIFacturacion@conecel.com',
                                      'Actualizaci�n de cuentas SAP del cierre de facturaci�n a fecha  ' ||
                                      lvfechactual,
                                      'Saludos,' || CHr(13) || CHr(13) ||
                                      'Se informa, se actualiz� los registros y se generaron los asientos contables del cierre de facturaci�n a la fecha ' ||
                                      lvfechactual || CHr(13) || CHr(13) ||
                                      CHr(13) || CHr(13) ||
                                      'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).' ||
                                      chr(13) ||
                                      'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||
                                      CHr(13) || CHr(13) || 'Atentamente,' ||
                                      CHr(13) || CHr(13) ||
                                      'SIS GSI-Billing.' || CHr(13) ||
                                      CHr(13) ||
                                      'Conecel.S.A - America Movil.');
        COMMIT;
        */
        GSIB_SECURITY.dotracem(PV_APLICACION,
                               'Se envio un e-mail a los funcionales informando la creacion del asiento ',
                               pn_id_transaccion);
      ELSE
        GSIB_SECURITY.dotracem(PV_APLICACION,
                               'No se envio el e-mail a los funcionales por que el parametro de ejecucion no es C ''COMMIT''',
                               pn_id_transaccion);
      
      END IF;
    
      --Si existen productos DTH hago el respado en la tabla SERVICIOS_PREPAGADOS_YYYYMMDD
      OPEN cur_dth FOR sql_text_dth;
      FETCH cur_dth
        INTO lv_existe_dth;
      /* IF lv_existe_dth = 1 THEN
        respaldo_servicios_dth(pdfechcierreperiodo, pv_resultado_resp);
      END IF;*/
      RESULTADO    := RESULTADO2;
      pv_msj_trama := pv_msj_trama2 || pv_resultado_resp;
      gsib_security.dotraceh(pv_aplicacion, resultado, pn_id_transaccion);
      gsib_security.dotraceh(pv_aplicacion,
                             pv_msj_trama,
                             pn_id_transaccion);
    
    END IF;
    --
     -- INICIO 	[10183] RGT Ney Miranda T. 
     --RESPALDA LAS TABLA DE SERVICIOS_PREPAGADOS_DTH_SAP AL TERMINAR LA EJECUCION    
  GSIB_API_SAP.respaldo_servicios_dth(PDFECHCIERREPERIODO,
                                   'FACTURACION',
                                   pn_id_transaccion,
                                   Lv_resultado);
    IF  Lv_resultado IS NOT NULL THEN
      RESULTADO:= Lv_resultado;
      RAISE LE_ERROR_RESPALDA;
    END IF;    
  -- FIN 	[10183] RGT Ney Miranda T.   
   
    
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           '****FIN GENERA ASIENTOS FACTURACION ' ||
                           pv_producto || ' FECHA DE CORTE :' ||
                           TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN LE_ERROR THEN
      RESULTADO    := LV_ERROR || ' ' || PV_APLICACION;
      pv_msj_trama := pv_msj_trama2 || pv_resultado_resp;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             lower(pv_msj_trama),
                             pn_id_transaccion);
      gsib_security.dotraceh(pv_aplicacion,
                             '****FIN GENERA ASIENTOS FACTURACION ' ||
                             pv_producto || ' FECHA DE CORTE :' ||
                             TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
    
    WHEN MY_ERROR THEN
      RESULTADO    := RESULTADO || ' ' || PV_APLICACION;
      pv_msj_trama := pv_msj_trama2 || pv_resultado_resp;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             lower(pv_msj_trama),
                             pn_id_transaccion);
      gsib_security.dotraceh(pv_aplicacion,
                             '****FIN GENERA ASIENTOS FACTURACION ' ||
                             pv_producto || ' FECHA DE CORTE :' ||
                             TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
   -- INICIO [10183] RGT Ney Miranda T.         
   -- Si hubo algun error al respaldar la tabla lo presenta     
    when LE_ERROR_RESPALDA then 
       RESULTADO    := RESULTADO ;  
    gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);                         
        gsib_security.dotraceh(pv_aplicacion,
                             '****FIN GENERA ASIENTOS FACTURACION ' ||
                             pv_producto || ' FECHA DE CORTE :' ||
                             TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);                      
   -- FIN [10183] RGT Ney Miranda T.  
    WHEN OTHERS THEN
      RESULTADO    := 'ERROR ' || SQLERRM || ' ' || PV_APLICACION;
      pv_msj_trama := pv_msj_trama2 || pv_resultado_resp;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             lower(pv_msj_trama),
                             pn_id_transaccion);
      gsib_security.dotraceh(pv_aplicacion,
                             '****FIN GENERA ASIENTOS FACTURACION ' ||
                             pv_producto || ' FECHA DE CORTE :' ||
                             TO_CHAR(PDFECHCIERREPERIODO, 'DD/MM/YYYY') ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
  END GRABA_SAFI_PRINCIPAL;

  --
  PROCEDURE CREA_ASIENTOS(TIPO_FACTURACION IN NUMBER,
                          FECHACIERRER2    IN VARCHAR2,
                          FECHACIERREP     IN DATE,
                          LNANOFINAL       IN NUMBER,
                          LNMESFINAL       IN NUMBER,
                          FECHACIERRER     IN DATE,
                          GLOSA            IN VARCHAR2,
                          DOC_TYPE         IN VARCHAR2,
                          LVFECHA          IN VARCHAR2,
                          LNDIAFINAL       IN VARCHAR2,
                          LVFECHACTUAL     IN VARCHAR2,
                          CODIGO_ERROR     OUT NUMBER,
                          RESULTADO        IN OUT VARCHAR2) IS
  
    --
    --=====================================================================================--
    -- Versi�n:  1.0.0
    -- Descripci�n: Creaci�n de Asiento Contables por tipos de Documento
    --=====================================================================================--
    -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Arturo Gamboa
    -- Fecha de creaci�n: 14/05/2012 9:42:15
    -- Proyecto: [5328] Facturaci�n Electr�nica
    --=====================================================================================--
    --=====================================================================================--
    -- Versi�n:  1.0.1
    -- Descripci�n: Creaci�n de Asiento Contables por tipos de Documento
    --=====================================================================================--
    -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Cristhian Acosta Ch.
    -- Fecha de creaci�n: 28/05/2013 11:57:50
    -- Proyecto: [8693] VENTA DTH POSTPAGO
    --=====================================================================================--
    --
    --=====================================================================================--
    -- Modificado por: RGT Ney Miranda T.
    -- L�der proyecto: SIS Luis Flores
    -- Lider PDS:      RGT Fernando Ortega
    -- Fecha de modificaci�n: 24/04/2015
    -- Proyecto:  Proyecto: [10189] API BSCS SAP
    -- Proposito: Se modifico los parametros de PEOPLE para que sean utilizados los correspondientes a SAP
    --=====================================================================================-- 
    --
  
    LV_CUADRE     NUMBER;
    LV_CUADRE2    NUMBER;
    TOTU          NUMBER := 0;
    TOTG          NUMBER := 0;
    SECU          NUMBER;
    SECU2         NUMBER;
    LVSENTENCIA   VARCHAR2(18000);
    LV_CODIGO_GYE VARCHAR2(3);
    LV_CODIGO_UIO VARCHAR2(3);
    LV_DOC_TYPE   VARCHAR2(8);
    MY_ERROR EXCEPTION;
    PV_APLICACION VARCHAR2(50) := 'GRABA_SAFI_FACT_SAP.CREA_ASIENTOS';
    --
    -- --INI [8693] - CURSOR SUMA POR CONTRAPARTIDAS --
    CURSOR C_SUM_CTRAP(CV_COMPANIA VARCHAR2, CN_TIPOFACT NUMBER) IS
      SELECT X.CTA_CTRAP, nvl(SUM(X.SUMA_IVA_DTH), 0) VALOR
        FROM (
              --TODOS LOS SERVICIOS
              SELECT NVL(decode(D.PRODUCTO,
                                 'DTH',
                                 GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                                 DECODE(D.COMPA�IA,
                                                                                        'Guayaquil',
                                                                                        'GYE_' ||
                                                                                        D.CTA_CTRAP ||
                                                                                        '_IMP',
                                                                                        'Quito',
                                                                                        'UIO_' ||
                                                                                        D.CTA_CTRAP ||
                                                                                        '_IMP')),
                                 D.CTA_CTRAP),
                          D.CTA_CTRAP) CTA_CTRAP,
                      nvl(SUM(D.VALOR), 0) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
               GROUP BY D.CTA_CTRAP, D.PRODUCTO, D.COMPA�IA
              UNION
              --TODOS LOS DESCUENTOS
              SELECT NVL(decode(D.PRODUCTO,
                                'DTH',
                                GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                                DECODE(D.COMPA�IA,
                                                                                       'Guayaquil',
                                                                                       'GYE_' ||
                                                                                       D.CTA_CTRAP ||
                                                                                       '_IMP',
                                                                                       'Quito',
                                                                                       'UIO_' ||
                                                                                       D.CTA_CTRAP ||
                                                                                       '_IMP')),
                                D.CTA_CTRAP),
                         D.CTA_CTRAP) CTA_CTRAP,
                     (nvl(SUM(D.DESCUENTO), 0) * -1) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
                 AND D.DESCUENTO > 0
               GROUP BY D.CTA_CTRAP, D.PRODUCTO, D.COMPA�IA
              UNION
              --VALOR DEL IVA A SUMAR A LAS CUENTAS DE INTALACI�N - SOLO APLICA A DTH
              SELECT D.CTA_CTRAP,
                     NVL(nvl(SUM(D.VALOR), 0) *
                         GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                     D.CTACTBLE,
                                                                     'IVA'),
                         0) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND D.TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
                 AND D.CTA_CTRAP =
                     GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_IVA_',
                                                                            'Quito',
                                                                            'UIO_IVA_',
                                                                            NULL) ||
                                                                     D.CTA_CTRAP)
                 AND D.PRODUCTO = 'DTH'
               GROUP BY D.CTA_CTRAP,
                        GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                    D.CTACTBLE,
                                                                    'IVA')
              UNION
              --VALOR DEL IVA A RESTAR DEL TOTAL - SOLO APLICA A DTH
              SELECT GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_' ||
                                                                            D.CTA_CTRAP ||
                                                                            '_RIVA',
                                                                            'Quito',
                                                                            'UIO_' ||
                                                                            D.CTA_CTRAP ||
                                                                            '_RIVA',
                                                                            NULL)),
                     NVL((nvl(SUM(D.VALOR), 0) *
                         GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                      D.CTACTBLE,
                                                                      'IVA')) * -1,
                         0) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND D.TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
                 AND D.CTA_CTRAP =
                     GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_IVA_',
                                                                            'Quito',
                                                                            'UIO_IVA_',
                                                                            NULL) ||
                                                                     D.CTA_CTRAP)
                 AND D.PRODUCTO = 'DTH'
               GROUP BY D.CTA_CTRAP,
                        D.COMPA�IA,
                        GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                    D.CTACTBLE,
                                                                    'IVA')
              UNION
              --VALOR DEL ICE A SUMAR A LAS CUENTAS  - SOLO APLICA A DTH
              SELECT D.CTA_CTRAP,
                     NVL(nvl(SUM(D.VALOR), 0) *
                         GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                     D.CTACTBLE,
                                                                     'ICE'),
                         0) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND D.TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
                 AND D.CTA_CTRAP =
                     GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_ICE_',
                                                                            'Quito',
                                                                            'UIO_ICE_',
                                                                            NULL) ||
                                                                     D.CTA_CTRAP)
                 AND D.PRODUCTO = 'DTH'
               GROUP BY D.CTA_CTRAP,
                        GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                    D.CTACTBLE,
                                                                    'ICE')
              UNION
              --VALOR DEL ICE A RESTAR DEL TOTAL - SOLO APLICA A DTH
              SELECT GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_' ||
                                                                            D.CTA_CTRAP ||
                                                                            '_RICE',
                                                                            'Quito',
                                                                            'UIO_' ||
                                                                            D.CTA_CTRAP ||
                                                                            '_RICE',
                                                                            NULL)),
                     NVL((nvl(SUM(D.VALOR), 0) *
                         GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                      D.CTACTBLE,
                                                                      'ICE')) * -1,
                         0) SUMA_IVA_DTH
                FROM PRUEBAFINSYS_SAP D
               WHERE D.COMPA�IA = CV_COMPANIA
                 AND D.TIPO_FACTURA = CN_TIPOFACT
                 AND D.TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
                 AND D.CTA_CTRAP =
                     GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                     DECODE(D.COMPA�IA,
                                                                            'Guayaquil',
                                                                            'GYE_ICE_',
                                                                            'Quito',
                                                                            'UIO_ICE_',
                                                                            NULL) ||
                                                                     D.CTA_CTRAP)
                 AND D.PRODUCTO = 'DTH'
               GROUP BY D.CTA_CTRAP,
                        D.COMPA�IA,
                        GRABA_SAFI_FACT_SAP.FN_IMPUESTO_DTH_IVA_ICE(D.NOMBRE,
                                                                    D.CTACTBLE,
                                                                    'ICE')) X
       GROUP BY X.CTA_CTRAP;
    -- FIN [8693] - CURSOR SUMA POR CONTRAPARTIDAS --
  BEGIN
    --
    --
    LV_CODIGO_GYE := GRABA_SAFI_FACT_SAP.GVF_OBTENER_VALOR_PARAMETRO(5328,
                                                                     'CODIGO_GYE_F');
    --
    IF LV_CODIGO_GYE IS NULL THEN
      RESULTADO := 'PARAMETRO CODIGO_GYE_F NO CONFIGURADO EN LA GV_PARAMTROS ID_TIPO_PARAMETROS 5328';
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
      RAISE MY_ERROR;
    END IF;
    --
    LV_CODIGO_UIO := GRABA_SAFI_FACT_SAP.GVF_OBTENER_VALOR_PARAMETRO(5328,
                                                                     'CODIGO_UIO_F');
    --
    IF LV_CODIGO_UIO IS NULL THEN
      RESULTADO := 'PARAMETRO CODIGO_UIO_F NO CONFIGURADO EN LA GV_PARAMTROS ID_TIPO_PARAMETROS 5328';
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
      RAISE MY_ERROR;
    END IF;
    --
    LV_DOC_TYPE := DOC_TYPE;
    --
    -----------GYE--NORMAL------------
    SECU := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_GYE || FECHACIERRER2,
             ROWNUM + SECU,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU,
             TRIM(G.CTACTBLE),
             ' ',
             ' ',
             'GYE_999',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(decode(g.producto,
                         'DTH',
                         FN_GLOSA_DTH(g.nombre,
                                      g.ctactble,
                                      lvfechactual,
                                      glosa,
                                      TIPO_FACTURACION),
                         glosa)) --SRE
            ,
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Guayaquil'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.CTACTBLE <> '4201011001';
  
    COMMIT;
  
    SECU := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_GYE || FECHACIERRER2,
             ROWNUM + SECU,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU,
             TRIM(G.CTACTBLE),
             ' ',
             ' ',
             'GYE_999',
             '000003',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(GLOSA),
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Guayaquil'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.PRODUCTO IN ('TARIFARIO', 'BULK', 'PYMES')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.CTACTBLE = '4201011001';
    COMMIT;
  
    SECU := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_GYE || FECHACIERRER2,
             ROWNUM + SECU,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU,
             TRIM(G.CTACTBLE),
             ' ',
             ' ',
             'GYE_999',
             '000002',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(GLOSA),
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Guayaquil'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.PRODUCTO = 'AUTOCONTROL'
         AND G.CTACTBLE = '4201011001';
    COMMIT;
  
    --DESCUENTOS
    SECU := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_GYE || FECHACIERRER2,
             ROWNUM + SECU,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU,
             TRIM(G.CTA_DESC),
             ' ',
             ' ',
             'GYE_999',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (G.DESCUENTO) VALOR,
             (G.DESCUENTO) VALOR,
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(decode(g.producto,
                         'DTH',
                         'DESC ' ||
                         FN_GLOSA_DTH(g.nombre,
                                      g.ctactble,
                                      lvfechactual,
                                      glosa,
                                      TIPO_FACTURACION),
                         'DESC ' || glosa)), --SRE
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Guayaquil'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.DESCUENTO > 0;
    COMMIT;
  
    --DESCUENTOS
    /*CLS LRI aumento nvl antes del into*/
    -----------------
    --INI [8693] - SUMA DE CONTRAPARTIDAS GYE
    /*SELECT nvl((SUM(FOREIGN_AMOUNT)),0) INTO TOTG FROM  GSI_FIN_SAFI WHERE OPERATING_UNIT = 'GYE_999'
    AND DOC_TYPE = LV_DOC_TYPE;--5328 PARA DIFERENCIAR LAS CUENTAS POR TIPO DE FACTURACI�N;*/
    FOR AZ IN C_SUM_CTRAP('Guayaquil', TIPO_FACTURACION) LOOP
      ---
      SECU := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_GYE);
      ---
      INSERT INTO GSI_FIN_SAFI_SAP
        SELECT 'CONEC',
               LV_CODIGO_GYE || FECHACIERRER2,
               ROWNUM + SECU,
               'REAL',
               'LOCAL',
               TRIM(FECHACIERREP),
               'BSCS',
               'CONEC',
               TRIM(LNANOFINAL),
               TRIM(LNMESFINAL),
               'NEXT',
               TRIM(FECHACIERRER),
               ROWNUM + SECU,
               AZ.CTA_CTRAP,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               'USD',
               ' ',
               'USD',
               'CRRNT',
               1,
               1,
               (AZ.VALOR),
               (AZ.VALOR),
               0,
               ' ',
               LV_DOC_TYPE,
               ' ',
               TRIM(FECHACIERREP),
               ' ',
               TRIM(decode(TIPO_FACTURACION,
                           7,
                           REPLACE(glosa, '[%%SERVICIO%%]', NULL),
                           8,
                           REPLACE(glosa, '[%%SERVICIO%%]', NULL),
                           glosa)), --SRE
               ' ',
               ' ',
               ' ',
               'N',
               0
          FROM DUAL;
    END LOOP;
    --FIN [8693] - SUMA DE CONTRAPARTIDAS GYE
    COMMIT;
  
    --------------------------------
    ---------UIO--NORMAL---------
    SECU2 := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_UIO || FECHACIERRER2,
             ROWNUM + SECU2,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU2,
             G.CTACTBLE,
             ' ',
             ' ',
             'UIO_999',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(decode(g.producto,
                         'DTH',
                         FN_GLOSA_DTH(g.nombre,
                                      g.ctactble,
                                      lvfechactual,
                                      glosa,
                                      TIPO_FACTURACION),
                         glosa)) --SRE
            ,
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Quito'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.CTACTBLE <> '4201011001';
    COMMIT;
  
    SECU2 := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_UIO || FECHACIERRER2,
             ROWNUM + SECU2,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU2,
             G.CTACTBLE,
             ' ',
             ' ',
             'UIO_999',
             '000003',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(GLOSA),
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Quito'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.PRODUCTO IN ('TARIFARIO', 'BULK', 'PYMES')
         AND G.CTACTBLE = '4201011001';
    COMMIT;
  
    SECU2 := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_UIO || FECHACIERRER2,
             ROWNUM + SECU2,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU2,
             G.CTACTBLE,
             ' ',
             ' ',
             'UIO_999',
             '000002',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (VALOR * -1),
             (VALOR * -1),
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(GLOSA),
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Quito'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.PRODUCTO = 'AUTOCONTROL'
         AND G.CTACTBLE = '4201011001';
    COMMIT;
  
    --DESCUENTOS
    SECU2 := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
    ---
    INSERT INTO GSI_FIN_SAFI_SAP
      SELECT 'CONEC',
             LV_CODIGO_UIO || FECHACIERRER2,
             ROWNUM + SECU2,
             'REAL',
             'LOCAL',
             TRIM(FECHACIERREP),
             'BSCS',
             'CONEC',
             TRIM(LNANOFINAL),
             TRIM(LNMESFINAL),
             'NEXT',
             TRIM(FECHACIERRER),
             ROWNUM + SECU2,
             G.CTA_DESC,
             ' ',
             ' ',
             'UIO_999',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             ' ',
             'USD',
             ' ',
             'USD',
             'CRRNT',
             1,
             1,
             (G.DESCUENTO) VALOR,
             (G.DESCUENTO) VALOR,
             0,
             ' ',
             LV_DOC_TYPE,
             ' ',
             TRIM(FECHACIERREP),
             ' ',
             TRIM(decode(g.producto,
                         'DTH',
                         'DESC ' ||
                         FN_GLOSA_DTH(g.nombre,
                                      g.ctactble,
                                      lvfechactual,
                                      glosa,
                                      TIPO_FACTURACION),
                         'DESC ' || glosa)), --SRE
             ' ',
             ' ',
             ' ',
             'N',
             0
        FROM PRUEBAFINSYS_SAP G
       WHERE COMPA�IA = 'Quito'
         AND TIPO NOT IN ('006 - CREDITOS', '005 - CARGOS')
         AND G.TIPO_FACTURA = TIPO_FACTURACION --5328 DIFERENCIAR TIPOS
         AND G.DESCUENTO > 0;
  
    COMMIT;
  
    --DESCUENTOS
    /*CLS LRI aumento nvl antes del into*/
    --INI [8693] - SUMA DE CONTRAPARTIDAS UIO
  
    /*SELECT nvl((SUM(Y.FOREIGN_AMOUNT)),0) INTO TOTU FROM  GSI_FIN_SAFI Y WHERE OPERATING_UNIT = 'UIO_999'
    AND DOC_TYPE = LV_DOC_TYPE;--5328 PARA DIFERENCIAR LAS CUENTAS POR TIPO DE FACTURACI�N
    */
    FOR AZ IN C_SUM_CTRAP('Quito', TIPO_FACTURACION) LOOP
      ---
      SECU2 := GRABA_SAFI_FACT_SAP.GVF_OBTENER_SECUENCIAS(LV_CODIGO_UIO);
      ---
      INSERT INTO GSI_FIN_SAFI_SAP
        SELECT 'CONEC',
               LV_CODIGO_UIO || FECHACIERRER2,
               ROWNUM + SECU2,
               'REAL',
               'LOCAL',
               TRIM(FECHACIERREP),
               'BSCS',
               'CONEC',
               TRIM(LNANOFINAL),
               TRIM(LNMESFINAL),
               'NEXT',
               TRIM(FECHACIERRER),
               ROWNUM + SECU2,
               AZ.CTA_CTRAP,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               'USD',
               ' ',
               'USD',
               'CRRNT',
               1,
               1,
               (AZ.VALOR),
               (AZ.VALOR),
               0,
               ' ',
               LV_DOC_TYPE,
               ' ',
               TRIM(FECHACIERREP),
               ' ',
               TRIM(decode(TIPO_FACTURACION,
                           7,
                           REPLACE(glosa, '[%%SERVICIO%%]', NULL),
                           8,
                           REPLACE(glosa, '[%%SERVICIO%%]', NULL),
                           glosa)), --SRE
               ' ',
               ' ',
               ' ',
               'N',
               0
          FROM DUAL;
    END LOOP;
    --FIN [8693] - SUMA DE CONTRAPARTIDAS UIO
    COMMIT;
  
    --OBTENGO EL VALOR PARA EL CUADRE
    /*CLS LRI aumento nvl antes del into*/
  
    SELECT nvl((SUM(MONETARY_AMOUNT)), 0)
      INTO LV_CUADRE
      FROM GSI_FIN_SAFI_SAP T
     WHERE T.DOC_TYPE = LV_DOC_TYPE;
    --COPIO A FINNACIERO
    RESULTADO := LV_CUADRE || ' <--> ' || LV_CUADRE2;
    --VALIDO QUE NO EXISTAN DIFERENCIAS
    IF LV_CUADRE = 0 THEN
      --NORMALES
      --SOLO IVA 12%
      LVSENTENCIA := NULL;
      LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_' ||
                     LVFECHA;
      LVSENTENCIA := LVSENTENCIA ||
                     ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS''))';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE ';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      --SOLO CICLO
      LVSENTENCIA := NULL;
      LVSENTENCIA := ' insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, ''' ||
                     LNDIAFINAL || '''  PR_JRNL_CICLO';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA'; -- (cuenta_contable)
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE';
      EXEC_SQL(LVSEntencia);
      COMMIT;
    
      --IVA 12% + CICLO
      LVSENTENCIA := NULL;
      LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, ''' ||
                     LNDIAFINAL || '''  PR_JRNL_CICLO ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA '; -- (cuenta_contable)
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo =''Y'') ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_' ||
                     LVFECHA;
      LVSENTENCIA := LVSENTENCIA ||
                     ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS'')) ';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE ';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      --CARGOS Y CREDITOS   + CICLO
      LVSENTENCIA := NULL;
      LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, ''V1''  PR_JRNL_TIPO_IVA, ''' ||
                     LNDIAFINAL || '''  PR_JRNL_CICLO ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA '; -- (cuenta_contable)
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and t.account in (select /* + rule */ K.CTACLBLEP from co_fact_' ||
                     LVFECHA;
      LVSENTENCIA := LVSENTENCIA ||
                     ' k where k.tipo   in ( ''005 - CARGOS'',''006 - CREDITOS'')) ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from PS_PR_JGEN_ACCT_EN_TMP_SAP x)';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE ';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      --- SOLO IVA 12%
      LVSENTENCIA := NULL;
      LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from PS_PR_JGEN_ACCT_EN_TMP_SAP x)';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE ';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      --- SOLO CICLO
      LVSENTENCIA := NULL;
      LVSENTENCIA := ' insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, ''' ||
                     LNDIAFINAL || '''  PR_JRNL_CICLO';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA'; -- (cuenta_contable)
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and (t.transaction_id,t.transaction_line) not in (select X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp_SAP x)';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      --IVA 12% + CICLO
      LVSENTENCIA := NULL;
      LVSENTENCIA := 'insert into  ps_pr_jgen_acct_en_tmp_SAP ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,';
      LVSENTENCIA := LVSENTENCIA ||
                     ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, ''' ||
                     lndiaFinal || '''  PR_JRNL_CICLO ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gsi_fin_safi_SAP t where t.account in ( select /* + rule */ A.CUENTA '; -- (cuenta_contable)
      LVSENTENCIA := LVSENTENCIA ||
                     ' from gl_parametros_ctas_contab a where a.setid = ''PORTA'' and a.usa_tipo_iva = ''Y'' and a.usa_ciclo =''Y'') ';
      LVSENTENCIA := LVSENTENCIA ||
                     ' and (t.transaction_id,t.transaction_line) not in (SELECT X.TRANSACTION_ID,X.TRANSACTION_LINE from ps_pr_jgen_acct_en_tmp_SAP x)';
      LVSENTENCIA := LVSENTENCIA || ' and t.doc_type = ''' || LV_DOC_TYPE || ''''; --5328 para diferenciar las cuentas por tipo de facturaci�n
      LVSENTENCIA := LVSENTENCIA || ' order by TRANSACTION_LINE ';
      EXEC_SQL(LVSENTENCIA);
      COMMIT;
    
      -- GRABA DATOS A FINANCIERO
    
      /*  INSERT INTO PS_JGEN_ACCT_ENTRY_sap
      SELECT * FROM GSI_FIN_SAFI T;
      
      INSERT INTO SYSADM.PS_PR_JGEN_ACCT_EN@FINSYS
      SELECT PR.*, ' ', ' ' FROM PS_PR_JGEN_ACCT_EN_TMP_SAP PR;*/
    
      -- GRABA DATOS A FINANCIERO
    
      --****************************
      COMMIT;
      RESULTADO    := RESULTADO ||
                      'SE COPIO CORRECTAMENTE LAS CUENTAS NORMALES ;) TIPO DE CUENTAS: ' ||
                      LV_DOC_TYPE;
      RESULTADO    := RESULTADO || ' SE COPIO LA INFORMACION CORRECTAMENTE';
      CODIGO_ERROR := 2;
    
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
    
    ELSE
    
      RESULTADO    := 'NO SE PUDO COPIAR A LA TABLA PS_JGEN_ACCT_ENTRY POR QUE NO CUADRO  RESULTADO = ' ||
                      LV_CUADRE;
      RESULTADO    := RESULTADO || 'ERROR REVISE POR FAVOR NO CUADRO';
      CODIGO_ERROR := 1;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
    
    END IF;
  
  EXCEPTION
    WHEN MY_ERROR THEN
      CODIGO_ERROR := 1;
      RESULTADO    := RESULTADO || ' ' || PV_APLICACION;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
    
    WHEN OTHERS THEN
      RESULTADO := 'ERROR ' || SQLERRM || ' ' || PV_APLICACION;
      gsib_security.dotraceh(pv_aplicacion,
                             lower(resultado),
                             pn_id_transaccion);
    
      CODIGO_ERROR := 1;
    
  END CREA_ASIENTOS;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE CREAR_SUMARIZADO_SAFI_SAP(PDFECHCIERREPERIODO   IN DATE, --DD/MM/YYYY
                                      PV_PRODUCTO           IN VARCHAR, --VOZ , DTH
                                      LVFECHANTERIOR        IN VARCHAR,
                                      ID_POLIZA_SIERRA_DTH  IN NUMBER,
                                      ID_POLIZA_SIERRA_BSCS IN NUMBER,
                                      ID_POLIZA_COSTA_DTH   IN NUMBER,
                                      ID_POLIZA_COSTA_BSCS  IN NUMBER,
                                      FECHACIERREP          IN DATE,
                                      LNANOFINAL            IN NUMBER,
                                      LNMESFINAL            IN NUMBER,
                                      FECHACIERRER          IN DATE,
                                      LNDIAFINAL            IN VARCHAR2,
                                      PV_MSJ_TRAMA2         IN OUT VARCHAR2) IS
  
    LV_MSJ_CREA_TABLA VARCHAR(5500);
    lv_error_html     VARCHAR(2500);
    LV_FECHA_ACTUAL   VARCHAR(100) := to_char(SYSDATE,
                                              'dd/mm/yyyy hh24:mi:ss');
    LVSENTENCIA       VARCHAR(100);
    lv_html           VARCHAR2(2500);
    LV_IDENTIFICADOR  VARCHAR2(15);
    T_COSTA_BSCS      CLOB := NULL;
    T_SIERRA_BSCS     CLOB := NULL;
    T_COSTA_DTH       CLOB := NULL;
    T_SIERRA_DTH      CLOB := NULL;
    ERROR             VARCHAR2(4000);
    ERROR_TRAMA       VARCHAR2(4000);
    MSJ_TRAMAS        VARCHAR2(2500);
    LV_EXISTE_VOZ     NUMBER := 0;
    LV_EXISTE_DTH     NUMBER := 0;
    LV_TRAMA          VARCHAR(100) := NULL;
    LV_ENVIA_TRAMA    VARCHAR(10);
    LV_APLICACION     VARCHAR(100) := 'GRABA_SAFI_FACT_SAP.CREAR_SUMARIZADO_SAFI_SAP';
    LE_ERROR_TRAMA EXCEPTION;
    LV_MSJ_CREA_HTML VARCHAR(5500);
    --ASIGNA IVA
    CURSOR ASIGNA_IVA IS
      SELECT DISTINCT (P.CTA_SAP), P.TIPO_IVA FROM gsi_fin_cebes P;
  
    --ASIGNA LOS CENTROS DE BENEFICIO
    CURSOR ASIGNA_CEBES IS
      SELECT R.CTA_SAP, R.CEBE_UIO, R.CEBE_GYE FROM GSI_FIN_CEBES R;
    --cursor para saber si existen productos dth     
    SQL_Text_Dth VARCHAR2(100) := ' SELECT COUNT(*)  FROM CO_FACT_' ||
                                  TO_CHAR(PDFECHCIERREPERIODO, 'DDMMYYYY') ||
                                  '  WHERE PRODUCTO = ''DTH''AND ROWNUM <= 1'; --1 SI EXISTE DTH , 0 SI NO ESXISTE
    CUR_DTH      SYS_REFCURSOR;
  
    --cursor para saber si existen productos Voz 
    SQL_Text_Voz VARCHAR2(100) := ' SELECT COUNT(*)  FROM CO_FACT_' ||
                                  TO_CHAR(PDFECHCIERREPERIODO, 'DDMMYYYY') ||
                                  '  WHERE PRODUCTO <> ''DTH''AND ROWNUM <= 1'; --1 SI EXISTE VOZ , 0 SI NO ESXISTE
    CUR_VOZ      SYS_REFCURSOR;
  
    -- CREA LA VISUALIZACION DE LAS POLIZAS GENERADA                     
    CURSOR LC_CREA_HTML(LC_FECHA_CORTE DATE) IS
    
      SELECT G.ID_POLIZA
        FROM GSIB_TRAMAS_SAP G
       WHERE G.FECHA_CORTE = LC_FECHA_CORTE
         AND TRUNC(G.FECHA) = TRUNC(SYSDATE);
  
  BEGIN
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Inicio del proceso que crea los asientos contables sumarizados por cuenta contable para UIO Y GYE',
                           pn_id_transaccion);
  
    -- TRUNCO LA TABLA GSIB_SUMARIZADO_SAP
    LVSENTENCIA := 'TRUNCATE TABLE GSIB_SUMARIZADO_SAP';
    EXEC_SQL(LVSENTENCIA);
    GSIB_SECURITY.dotracem(LV_APLICACION,
                           'Se elimino registros de la tabla temporal GSIB_SUMARIZADO_SAP ',
                           pn_id_transaccion);
  
    --INSERTO EN LA TABLA GSIB_SUMARIZADO_SAP
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Inicio carga masiva de datos hacia la tabla GSIB_SUMARIZADO_SAP ',
                           pn_id_transaccion);
  
    BEGIN
      INSERT INTO GSIB_SUMARIZADO_SAP
        SELECT 'Fact Consumo Celular ' || REPLACE(LVFECHANTERIOR, '/', '') || ' ' ||
               REPLACE(FECHACIERREP, '/', '') TEXTO,
               D.ACCOUNT,
               SUM(D.MONETARY_AMOUNT) MONETARY,
               SUM(D.FOREIGN_AMOUNT) FOREING,
               'BSCS' || LNANOFINAL ||
               DECODE(SUBSTR(D.TRANSACTION_ID, 1, 2),
                      'BG',
                      DECODE(D.DOC_TYPE,
                             'CBSCSE',
                             LPAD(ID_POLIZA_COSTA_BSCS, 3, 0),
                             'CBSDTHE',
                             LPAD(ID_POLIZA_COSTA_DTH, 3, 0)),
                      'BU',
                      DECODE(D.DOC_TYPE,
                             'CBSCSE',
                             LPAD(ID_POLIZA_SIERRA_BSCS, 3, 0),
                             'CBSDTHE',
                             LPAD(ID_POLIZA_SIERRA_DTH, 3, 0))) IDENTIFICADOR,
               'EC02' SOCIEDAD,
               FECHACIERRER FECHA_CONTA,
               
               DECODE(D.DOC_TYPE, 'CBSCSE', 'BSCS', 'CBSDTHE', 'DTH') ||
               DECODE(SUBSTR(D.TRANSACTION_ID, 1, 2),
                      'BG',
                      ' COSTA',
                      'BU',
                      ' SIERRA') REFERENCIA,
               FECHACIERRER FECHA_DOC,
               DECODE(D.DOC_TYPE, 'CBSCSE', 'I0', 'CBSDTHE', 'I1') CLASE_DOC,
               'FACTURACION CICLO ' || LNDIAFINAL ||
               DECODE(SUBSTR(D.TRANSACTION_ID, 1, 2),
                      'BG',
                      ' R2',
                      'BU',
                      ' R1') TEXTO_CABECERA,
               LNANOFINAL ANIO_FINAL,
               LNMESFINAL MES_FINAL,
               'USD' MONEDA,
               ' ' IVA,
               ' ' CLAVE_CONTA,
               ' ' CENTRO_DE_BENEFICIO,
               LNDIAFINAL CICLO
          FROM GSI_FIN_SAFI_SAP D
        
         GROUP BY D.ACCOUNT, D.TRANSACTION_ID, D.DOC_TYPE;
    
      --ASIGNA LOS CENTROS DE BENEFICIO
      FOR CE IN ASIGNA_CEBES LOOP
        UPDATE GSIB_SUMARIZADO_SAP F
           SET F.CENTRO_BENEFICIO = DECODE(SUBSTR(F.TEXTO_CABECERA,
                                                  LENGTH(F.TEXTO_CABECERA) - 1,
                                                  3),
                                           'R2', --R2 COSTA
                                           CE.CEBE_GYE,
                                           'R1', --R1 SIERRA
                                           CE.CEBE_UIO)
         WHERE F.ACCOUNT = CE.CTA_SAP;
      END LOOP;
    
      --ASIGNA LAS CLAVES DE CONTABILIZACION
      UPDATE GSIB_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 40
       WHERE F.MONETARY_AMOUNT > 0;
    
      UPDATE GSIB_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 50
       WHERE F.MONETARY_AMOUNT < 0;
    
      --ASIGNA EL TIPO DE IVA A LAS CTAS
      FOR IV IN ASIGNA_IVA LOOP
        UPDATE GSIB_SUMARIZADO_SAP S
           SET S.IVA = IV.TIPO_IVA
         WHERE S.ACCOUNT = IV.CTA_SAP;
      END LOOP;
    
      COMMIT;
      LV_MSJ_CREA_TABLA := 'Se creo correctamente la tabla GSIB_SUMARIZADO_SAP ;';
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Fin de carga masiva de datos hacia la tabla GSIB_SUMARIZADO_SAP ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        LV_MSJ_CREA_TABLA := 'ERROR. Al crear la tabla GSIB_SUMARIZADO_SAP => ' ||
                             SQLERRM;
        GSIB_SECURITY.dotraceh(LV_APLICACION,
                               'Error al realizar la carga masiva hacia la tabla GSIB_SUMARIZADO_SAP ',
                               pn_id_transaccion);
      
    END;
  
    BEGIN
    
      LV_ENVIA_TRAMA := GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                        'ENVIA_TRAMA');
    
      IF PV_PRODUCTO = 'DTH' THEN
        --SOLO DTH - RUBRO POSITIVOS
      
        T_COSTA_DTH := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                   pv_producto         => 'DTH',
                                   pv_region           => 'COSTA',
                                   PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                   ERROR               => ERROR_TRAMA);
      
        IF ERROR_TRAMA IS NOT NULL THEN
          MSJ_TRAMAS  := 'ERROR AL CREAR LA TRAMA DTH COSTA => ' ||
                         ERROR_TRAMA;
          ERROR_TRAMA := NULL;
        
          RAISE LE_ERROR_TRAMA;
        ELSE
          INSERT INTO GSIB_TRAMAS_SAP
            (ID_POLIZA,
             FECHA,
             trama,
             estado,
             REFERENCIA,
             FECHA_CORTE,
             OBSERVACION)
          VALUES
            (LV_IDENTIFICADOR,
             SYSDATE,
             T_COSTA_DTH,
             'CREADA',
             'FACTURACION DTH COSTA',
             PDFECHCIERREPERIODO,
             ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
          COMMIT;
        END IF;
        -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
        IF LV_ENVIA_TRAMA = 'S' THEN
        
          gsib_envia_tramas_sap.consume_webservice(T_COSTA_DTH, error);
        
          IF error IS NOT NULL THEN
            MSJ_TRAMAS := MSJ_TRAMAS ||
                          'Error al enviar a SAP DTH COSTA =>' || error;
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'NO ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                   LV_FECHA_ACTUAL ||
                                   ' ERROR al tratar de envia a SAP ' ||
                                   error || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
            error := NULL;
          ELSE
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                   LV_FECHA_ACTUAL || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
          END IF;
        END IF;
      
        T_SIERRA_DTH := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                    pv_producto         => 'DTH',
                                    pv_region           => 'SIERRA',
                                    PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                    ERROR               => ERROR_TRAMA);
      
        IF ERROR_TRAMA IS NOT NULL THEN
          MSJ_TRAMAS  := MSJ_TRAMAS ||
                         ' ERROR AL CREAR LA TRAMA DTH SIERRA =>' ||
                         ERROR_TRAMA;
          ERROR_TRAMA := NULL;
          RAISE LE_ERROR_TRAMA;
        
        ELSE
          INSERT INTO GSIB_TRAMAS_SAP
            (ID_POLIZA,
             FECHA,
             trama,
             estado,
             REFERENCIA,
             FECHA_CORTE,
             OBSERVACION)
          VALUES
            (LV_IDENTIFICADOR,
             SYSDATE,
             T_SIERRA_DTH,
             'CREADA',
             'FACTURACION DTH SIERRA',
             PDFECHCIERREPERIODO,
             ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        
          COMMIT;
        END IF;
      
        -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
        IF LV_ENVIA_TRAMA = 'S' THEN
        
          gsib_envia_tramas_sap.consume_webservice(T_SIERRA_DTH, error);
        
          IF error IS NOT NULL THEN
            MSJ_TRAMAS := MSJ_TRAMAS ||
                          ' Error al enviar a SAP DTH SIERRA =>' || error;
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'NO ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                   LV_FECHA_ACTUAL ||
                                   ' ERROR al tratar de envia a SAP ' ||
                                   error || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
            error := NULL;
          ELSE
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                   LV_FECHA_ACTUAL || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
          
          END IF;
        END IF;
      
        MSJ_TRAMAS := 'Se crearon las tramas DTH =>' || MSJ_TRAMAS;
      
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      ELSIF PV_PRODUCTO = 'VOZ' THEN
        --SOLO VOZ
        T_COSTA_BSCS := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                    pv_producto         => 'BSCS',
                                    pv_region           => 'COSTA',
                                    PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                    ERROR               => ERROR_TRAMA);
      
        IF ERROR_TRAMA IS NOT NULL THEN
          MSJ_TRAMAS  := 'ERROR AL CREAR LA TRAMA BSCS COSTA =>' ||
                         ERROR_TRAMA;
          ERROR_TRAMA := NULL;
          RAISE LE_ERROR_TRAMA;
        ELSE
          INSERT INTO GSIB_TRAMAS_SAP
            (ID_POLIZA,
             FECHA,
             trama,
             estado,
             REFERENCIA,
             FECHA_CORTE,
             OBSERVACION)
          VALUES
            (LV_IDENTIFICADOR,
             SYSDATE,
             T_COSTA_BSCS,
             'CREADA',
             'FACTURACION BSCS COSTA',
             PDFECHCIERREPERIODO,
             ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        
          COMMIT;
        END IF;
        -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
        IF LV_ENVIA_TRAMA = 'S' THEN
        
          gsib_envia_tramas_sap.consume_webservice(T_COSTA_BSCS, error);
        
          IF error IS NOT NULL THEN
            MSJ_TRAMAS := MSJ_TRAMAS ||
                          'Error al enviar a SAP BSCS COSTA =>' || error;
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'NO ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                   LV_FECHA_ACTUAL ||
                                   ' ERROR al tratar de envia a SAP ' ||
                                   error || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
            error := NULL;
          ELSE
          
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                   LV_FECHA_ACTUAL || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
          
          END IF;
        END IF;
      
        T_SIERRA_BSCS := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                     pv_producto         => 'BSCS',
                                     pv_region           => 'SIERRA',
                                     PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                     ERROR               => ERROR_TRAMA);
      
        IF ERROR_TRAMA IS NOT NULL THEN
          MSJ_TRAMAS  := MSJ_TRAMAS ||
                         'ERROR AL CREAR LA TRAMA BSCS SIERRA =>' ||
                         ERROR_TRAMA;
          ERROR_TRAMA := NULL;
          RAISE LE_ERROR_TRAMA;
        ELSE
          INSERT INTO GSIB_TRAMAS_SAP
            (ID_POLIZA,
             FECHA,
             trama,
             estado,
             REFERENCIA,
             FECHA_CORTE,
             OBSERVACION)
          VALUES
            (LV_IDENTIFICADOR,
             SYSDATE,
             T_SIERRA_BSCS,
             'CREADA',
             'FACTURACION BSCS SIERRA',
             PDFECHCIERREPERIODO,
             ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        
          COMMIT;
        END IF;
        -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
        IF LV_ENVIA_TRAMA = 'S' THEN
        
          gsib_envia_tramas_sap.consume_webservice(T_SIERRA_BSCS, error);
        
          IF error IS NOT NULL THEN
            MSJ_TRAMAS := MSJ_TRAMAS ||
                          ' Error al enviar a SAP BSCS SIERRA =>' || error;
            --COD_ERROR_SAP := FALSE;
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'NO ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                   LV_FECHA_ACTUAL ||
                                   ' ERROR al tratar de envia a SAP ' ||
                                   error || chr(13)
            
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
            error := NULL;
            --  RAISE MY_ERROR;
          ELSE
            UPDATE GSIB_TRAMAS_SAP F
               SET F.ESTADO      = 'ENVIADA',
                   F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                   LV_FECHA_ACTUAL || chr(13)
             WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
            COMMIT;
          
          END IF;
        END IF;
      
        MSJ_TRAMAS := 'Se crearon las tramas de BSCS => ' || MSJ_TRAMAS;
        --COD_ERROR_SAP := FALSE;
      
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      ELSIF PV_PRODUCTO IS NULL THEN
        --SI EL PRODUCTO ES NULO TODOS LOS PRODUCTOS VOZ - DTH RUBROS POSITIVOS
        OPEN CUR_VOZ FOR SQL_Text_Voz;
        FETCH CUR_VOZ
          INTO LV_EXISTE_VOZ;
      
        IF LV_EXISTE_VOZ = 1 THEN
        
          T_COSTA_BSCS := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                      pv_producto         => 'BSCS',
                                      pv_region           => 'COSTA',
                                      PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                      ERROR               => ERROR_TRAMA);
        
          IF ERROR_TRAMA IS NOT NULL THEN
            MSJ_TRAMAS := 'ERROR AL CREAR LA TRAMA BSCS COSTA =>' ||
                          ERROR_TRAMA;
          
            ERROR_TRAMA := NULL;
            RAISE LE_ERROR_TRAMA;
          ELSE
            INSERT INTO GSIB_TRAMAS_SAP
              (ID_POLIZA,
               FECHA,
               trama,
               estado,
               REFERENCIA,
               FECHA_CORTE,
               OBSERVACION)
            VALUES
              (LV_IDENTIFICADOR,
               SYSDATE,
               T_COSTA_BSCS,
               'CREADA',
               'FACTURACION BSCS COSTA',
               PDFECHCIERREPERIODO,
               ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
          
            COMMIT;
          END IF;
        
          -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
          IF LV_ENVIA_TRAMA = 'S' THEN
          
            gsib_envia_tramas_sap.consume_webservice(T_COSTA_BSCS, error);
          
            IF error IS NOT NULL THEN
              MSJ_TRAMAS := MSJ_TRAMAS ||
                            ' Error al enviar a SAP BSCS COSTA =>' || error;
              --COD_ERROR_SAP := FALSE;
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'NO ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                     LV_FECHA_ACTUAL ||
                                     ' ERROR al tratar de envia a SAP ' ||
                                     error || chr(13)
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
              error := NULL;
            ELSE
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                     LV_FECHA_ACTUAL || chr(13)
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
            END IF;
          END IF;
        
          T_SIERRA_BSCS := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                       pv_producto         => 'BSCS',
                                       pv_region           => 'SIERRA',
                                       PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                       ERROR               => ERROR_TRAMA);
          IF ERROR_TRAMA IS NOT NULL THEN
            MSJ_TRAMAS := MSJ_TRAMAS ||
                          'ERROR AL CREAR LA TRAMA BSCS SIERRA =>' ||
                          ERROR_TRAMA;
          
            ERROR_TRAMA := NULL;
            RAISE LE_ERROR_TRAMA;
          ELSE
            INSERT INTO GSIB_TRAMAS_SAP
              (ID_POLIZA,
               FECHA,
               trama,
               estado,
               REFERENCIA,
               FECHA_CORTE,
               OBSERVACION)
            VALUES
              (LV_IDENTIFICADOR,
               SYSDATE,
               T_SIERRA_BSCS,
               'CREADA',
               'FACTURACION BSCS SIERRA',
               PDFECHCIERREPERIODO,
               ' =>' || ' Creada el ' || LV_FECHA_ACTUAL || chr(13));
          
            COMMIT;
          END IF;
        
          -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
          IF LV_ENVIA_TRAMA = 'S' THEN
          
            gsib_envia_tramas_sap.consume_webservice(T_SIERRA_BSCS, error);
            IF error IS NOT NULL THEN
              MSJ_TRAMAS := MSJ_TRAMAS ||
                            ' Error al enviar a SAP BSCS SIERRA =>' ||
                            error;
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'NO ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                     LV_FECHA_ACTUAL ||
                                     ' ERROR al tratar de envia a SAP ' ||
                                     error || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
              error := NULL;
              -- RAISE MY_ERROR;
            ELSE
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                     LV_FECHA_ACTUAL || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
            END IF;
          END IF;
          LV_TRAMA := 'BSCS ';
        ELSE
          LV_TRAMA := LV_TRAMA || 'NO SE ENCONTRO PRODUCTOS VOZ';
        
        END IF;
      
        OPEN CUR_DTH FOR SQL_Text_Dth;
        FETCH CUR_DTH
          INTO LV_EXISTE_DTH;
      
        IF LV_EXISTE_DTH = 1 THEN
        
          T_COSTA_DTH := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                     pv_producto         => 'DTH',
                                     pv_region           => 'COSTA',
                                     PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                     ERROR               => ERROR_TRAMA);
        
          IF ERROR_TRAMA IS NOT NULL THEN
            MSJ_TRAMAS  := MSJ_TRAMAS ||
                           ' ERROR AL CREAR LA TRAMA DTH COSTA =>' ||
                           ERROR_TRAMA;
            ERROR_TRAMA := NULL;
            RAISE LE_ERROR_TRAMA;
          ELSE
            INSERT INTO GSIB_TRAMAS_SAP
              (ID_POLIZA,
               FECHA,
               trama,
               estado,
               REFERENCIA,
               FECHA_CORTE,
               OBSERVACION)
            VALUES
              (LV_IDENTIFICADOR,
               SYSDATE,
               T_COSTA_DTH,
               'CREADA',
               'FACTURACION DTH COSTA',
               PDFECHCIERREPERIODO,
               ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
          
            COMMIT;
          END IF;
        
          -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
          IF LV_ENVIA_TRAMA = 'S' THEN
          
            gsib_envia_tramas_sap.consume_webservice(T_COSTA_DTH, error);
            IF error IS NOT NULL THEN
              MSJ_TRAMAS := MSJ_TRAMAS ||
                            ' Error al enviar a SAP DTH COSTA =>' || error;
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'NO ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                     LV_FECHA_ACTUAL ||
                                     ' ERROR al tratar de envia a SAP ' ||
                                     error || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
              error := NULL;
            ELSE
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                     LV_FECHA_ACTUAL || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
            END IF;
          END IF;
        
          T_SIERRA_DTH := crear_trama(PDFECHCIERREPERIODO => PDFECHCIERREPERIODO,
                                      pv_producto         => 'DTH',
                                      pv_region           => 'SIERRA',
                                      PV_IDENTIFICADOR    => LV_IDENTIFICADOR,
                                      ERROR               => ERROR_TRAMA);
          IF ERROR_TRAMA IS NOT NULL THEN
            MSJ_TRAMAS  := MSJ_TRAMAS ||
                           ' ERROR AL CREAR LA TRAMA DTH SIERRA =>' ||
                           ERROR_TRAMA;
            ERROR_TRAMA := NULL;
            RAISE LE_ERROR_TRAMA;
          ELSE
            INSERT INTO GSIB_TRAMAS_SAP
              (ID_POLIZA,
               FECHA,
               trama,
               estado,
               REFERENCIA,
               FECHA_CORTE,
               OBSERVACION)
            VALUES
              (LV_IDENTIFICADOR,
               SYSDATE,
               T_SIERRA_DTH,
               'CREADA',
               'FACTURACION DTH SIERRA',
               PDFECHCIERREPERIODO,
               ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
            COMMIT;
          END IF;
        
          -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
          IF LV_ENVIA_TRAMA = 'S' THEN
          
            gsib_envia_tramas_sap.consume_webservice(T_SIERRA_DTH, error);
            IF error IS NOT NULL THEN
              MSJ_TRAMAS := MSJ_TRAMAS ||
                            ' Error al enviar a SAP DTH SIERRA =>' || error;
            
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'NO ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => ' ||
                                     LV_FECHA_ACTUAL ||
                                     ' ERROR al tratar de envia a SAP ' ||
                                     error || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
              error := NULL;
            
            ELSE
              UPDATE GSIB_TRAMAS_SAP F
                 SET F.ESTADO      = 'ENVIADA',
                     F.OBSERVACION = F.OBSERVACION || ' => Enviada el : ' ||
                                     LV_FECHA_ACTUAL || chr(13)
              
               WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
              COMMIT;
            END IF;
          END IF;
        
          LV_TRAMA := LV_TRAMA || ', DTH';
        ELSE
          LV_TRAMA := LV_TRAMA || ',NO SE ENCONTRO PRODUCTOS DTH';
        END IF;
      
        LV_TRAMA := 'Se crearon las tramas =>' || LV_TRAMA;
      
      END IF;
      --generos los reportes en html
      genera_reporte_en_html(pdfechcierreperiodo, lv_html);
    
      IF LV_ENVIA_TRAMA = 'S' THEN
        MSJ_TRAMAS := LV_TRAMA || MSJ_TRAMAS;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      ELSE
        MSJ_TRAMAS := LV_TRAMA || MSJ_TRAMAS ||
                      '; El envio SAP no esta Habilitado';
      
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      END IF;
    
    EXCEPTION
    
      WHEN LE_ERROR_TRAMA THEN
        MSJ_TRAMAS := MSJ_TRAMAS || SQLERRM;
      
      WHEN OTHERS THEN
      
        MSJ_TRAMAS := 'ERROR AL CREAR LA TRAMA Y ENVIAR A SAP ; ' ||
                      SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
    END;
  
    FOR D IN LC_CREA_HTML(PDFECHCIERREPERIODO) LOOP
      GSIB_API_SAP.GENERA_HTML(D.ID_POLIZA, LV_MSJ_CREA_HTML);
    END LOOP;
  
    --genero el reporte de valores omitidos 
    reporte_valores_omitidos(pdfechcierreperiodo, lv_error_html);
    PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                     lv_msj_crea_html || lv_error_html;
    gsib_security.dotraceh(lv_aplicacion,
                           pv_msj_trama2 || lv_error_html,
                           pn_id_transaccion);
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Fin del proceso que crea los asientos contables sumarizados por cuenta contable para UIO Y GYE',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                       lv_msj_crea_html || ' ' || lv_error_html || ' ' ||
                       SQLERRM || ' - ' ||
                       dbms_utility.format_error_backtrace;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_msj_trama2,
                             pn_id_transaccion);
    
  END;

  ---
  FUNCTION VERIFICA_TIPO_FACTURA(PD_PERIODO_FIN IN DATE,
                                 PV_CUSTOMER_ID IN INTEGER) RETURN NUMBER IS
    --
    --=====================================================================================--
    -- Versi�n:  1.0.0
    -- Descripci�n: Verifica el Tipo de Facturaci�n al que pertenece la Factura(Fisica o Electr�nica)
    --=====================================================================================--
    -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Arturo Gamboa
    -- Fecha de creaci�n: 14/05/2012 9:42:15
    -- Proyecto: [5328] Facturaci�n Electr�nica
    --=====================================================================================--
    -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Cristhian Acosta Chambers
    -- Fecha de Modificaci�n: 13/06/2013
    -- Proyecto: [8693] - Venta DTH Post Pago
    --=====================================================================================--
    --=====================================================================================--
    -- Modificado por  :  SUD Cristhian Acosta. (CAC)
    -- Lider proyecto  : SIS Xavier Trivi�o.
    -- Lider PDS       : SUD Cristhian Acosta Chambers
    -- Fecha de Modificacion: 31/07/2014
    -- Proyecto: [9587] - Factura Electronica DTH
    --=====================================================================================--
    --=====================================================================================--
    -- Modificado por  :  SUD Cristhian Acosta. (CAC)
    -- Lider proyecto  : SIS Ma. Elizabeth Estevez
    -- Lider PDS       : SUD Cristhian Acosta Chambers
    -- Fecha de Modificacion: 30/10/2010
    -- Proyecto: [8968] - Activaci�n Masiva de Factura Electronica
    --=====================================================================================--
    --
    CURSOR C_TIPOS_FACTURAS(CN_CUSTOMER_ID INTEGER, CD_PERIODO_FIN DATE) IS
      SELECT X.ESTADO, X.FECHA_FIN
        FROM DOC1.CON_MARCA_FACT_ELECT X
       WHERE X.CUSTOMER_ID = CN_CUSTOMER_ID
         AND X.FECHA_INICIO =
             (SELECT MAX(X.FECHA_INICIO)
                FROM DOC1.CON_MARCA_FACT_ELECT X
               WHERE X.CUSTOMER_ID = CN_CUSTOMER_ID
                 AND X.FECHA_INICIO <
                     TO_DATE(TO_CHAR(CD_PERIODO_FIN, 'DD/MM/YYYY'),
                             'DD/MM/YYYY'))
       ORDER BY X.FECHA_FIN DESC;
    --
    --SUD SRE CURSOR QUE OBTIENE EL ESTADO Y FECHA DE LAS CUENTAS DTH
    CURSOR C_TIPOS_FACTURAS_DTH(CN_CUSTOMER_ID INTEGER) IS
      SELECT C.PRGCODE
        FROM CUSTOMER_ALL C
       WHERE C.CUSTOMER_ID = CN_CUSTOMER_ID
         AND C.PRGCODE = 7;
    --
    LC_C_TIPOS_FACTURAS   C_TIPOS_FACTURAS%ROWTYPE;
    LN_TIPO_FACTURA       NUMBER;
    TIPO_FACTURA          NUMBER := 1; --POR DEFECTO FACT. FISICA
    LV_TIPOS_FACTURAS_DTH VARCHAR2(10);
  BEGIN
    --8968 - INI - SUD CAC
    --ESQUEMA PARA QUE APLICA A TODO FACTURA ELECTR�NICA
    IF NVL(GRABA_SAFI_FACT_SAP.GVF_OBTENER_VALOR_PARAMETRO(8968,
                                                           'GV_APLICA_TIPO_FACTURA'),
           'N') = 'S' THEN
      -- Verifica el Cuentas DTH - prgcode 7
      OPEN C_TIPOS_FACTURAS_DTH(PV_CUSTOMER_ID);
      FETCH C_TIPOS_FACTURAS_DTH
        INTO LV_TIPOS_FACTURAS_DTH;
      CLOSE C_TIPOS_FACTURAS_DTH;
      --
      IF LV_TIPOS_FACTURAS_DTH = '7' THEN
        --FACTURA DTH ELECTR�NICA - 8
        TIPO_FACTURA := NVL(GRABA_SAFI_FACT_SAP.GVF_OBTENER_VALOR_PARAMETRO(8968,
                                                                            'EXTRAE_FACT_ELECT_DTH'),
                            8);
        RETURN TIPO_FACTURA;
      END IF;
      --PARA CUENTAS DE VOZ
      --VALIDACI�N TODOS COMO ELECTRONICA VOZ - 4
      TIPO_FACTURA := NVL(GRABA_SAFI_FACT_SAP.GVF_OBTENER_VALOR_PARAMETRO(8968,
                                                                          'EXTRAE_FACT_ELECT'),
                          4);
      RETURN TIPO_FACTURA;
      --
    END IF;
    --8968 - FIN - SUD CAC
    --ESQUEMA FISICA O ELECTRONICA DEPENDIENTE DE LA ACTIVACION DE F.E.
    -- Busca clientes con marca Factura Electronica
    OPEN C_TIPOS_FACTURAS(PV_CUSTOMER_ID, PD_PERIODO_FIN);
    FETCH C_TIPOS_FACTURAS
      INTO LC_C_TIPOS_FACTURAS;
    CLOSE C_TIPOS_FACTURAS;
    -- Verifica el Cuentas DTH - prgcode 7
    OPEN C_TIPOS_FACTURAS_DTH(PV_CUSTOMER_ID);
    FETCH C_TIPOS_FACTURAS_DTH
      INTO LV_TIPOS_FACTURAS_DTH;
    CLOSE C_TIPOS_FACTURAS_DTH;
    --INI - [9587] - FACTURA ELECTRONICA DTH 
    IF LV_TIPOS_FACTURAS_DTH = '7' THEN
      TIPO_FACTURA := 7; --FACTURA DTH FISICA
      IF LC_C_TIPOS_FACTURAS.ESTADO = 'A' THEN
        TIPO_FACTURA := 8; --FACTURA DTH ELECTR�NICA
      END IF;
      RETURN TIPO_FACTURA;
    END IF;
    --FIN - [9587] - FACTURA ELECTRONICA DTH 
    --Por defecto TIPO_FACTURA es 1 Factura F�sica VOZ
    IF LC_C_TIPOS_FACTURAS.ESTADO = 'A' THEN
      TIPO_FACTURA := 4; --FACTURA ELECTRONICA VOZ
    END IF;
    --
    RETURN TIPO_FACTURA;
  
  END VERIFICA_TIPO_FACTURA;
  --
  FUNCTION GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
    --
    --=====================================================================================--
    -- Versi�n:  1.0.0
    -- Descripci�n: Recoge los Datos configurados desde la gv_parametros
    --=====================================================================================--
    -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Arturo Gamboa
    -- Fecha de creaci�n: 14/05/2012 9:42:15
    -- Proyecto: [5328] Facturaci�n Electr�nica
    --=====================================================================================--
    --
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER,
                       CV_ID_PARAMETRO      VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END GVF_OBTENER_VALOR_PARAMETRO;
  --
  FUNCTION GVF_OBTENER_SECUENCIAS(PV_CODIGO IN VARCHAR2) RETURN NUMBER IS
    --
    --=====================================================================================--
    -- Versi�n:  1.0.0
    -- Descripci�n: Obtiene las secuencias
    --=====================================================================================--
    -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Arturo Gamboa
    -- Fecha de creaci�n: 14/05/2012 9:42:15
    -- Proyecto: [5328] Facturaci�n Electr�nica
    --=====================================================================================--
    --
  
    CURSOR C_SECUENCIA(CV_CODIGO VARCHAR2) IS
      SELECT MAX(ROWNUM)
        FROM GSI_FIN_SAFI_SAP G
       WHERE SUBSTR(G.TRANSACTION_ID, 1, 2) = CV_CODIGO; --5328 DIFERENCIAR TIPOS
  
    LN_VALOR_SECUENCIA INTEGER := 0;
  
  BEGIN
  
    OPEN C_SECUENCIA(PV_CODIGO);
    FETCH C_SECUENCIA
      INTO LN_VALOR_SECUENCIA;
    CLOSE C_SECUENCIA;
  
    RETURN NVL(LN_VALOR_SECUENCIA, 0);
  
  END GVF_OBTENER_SECUENCIAS;
  --
  FUNCTION FN_GLOSA_DTH(PV_NOMBRE_CTA IN VARCHAR2,
                        PV_CTA_CTBLE  IN VARCHAR2,
                        PV_FECHACTUAL IN VARCHAR2,
                        PV_GLOSA      IN VARCHAR2,
                        PV_TIPO       IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2 IS
  
    --=====================================================================================--
    -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Cristhian Acosta Chambers
    -- Fecha de Creaci�n: 13/06/2013
    -- Proyecto: [8693] - Venta DTH Post Pago
    -- Motivo: Arma la glosa correspondiente para el producto DTH
    --=====================================================================================--    
    --=====================================================================================--
    -- Modificado por  :  SUD Cristhian Acosta. (CAC)
    -- Lider proyecto  : SIS Xavier Trivi�o.
    -- Lider PDS       : SUD Cristhian Acosta Chambers
    -- Fecha de Modificacion: 31/07/2014
    -- Proyecto: [9587] - Factura Electronica DTH
    -- Motivo: Arma la glosa correspondiente para el producto DTH por tipo de Facturaci�n
    --=====================================================================================--                   
  
    LN_EXISTE NUMBER;
    LE_ERROR EXCEPTION;
    LB_FOUND          BOOLEAN := FALSE;
    GLOSA             VARCHAR2(6000);
    LV_FECHAPOSTERIOR VARCHAR2(20);
  
    CURSOR C_ES_POST(CV_CTA_CTBLE VARCHAR2, CV_NOMBRE_CTA VARCHAR2) IS
      SELECT DISTINCT 1
        FROM COB_SERVICIOS C
       WHERE C.CTAPSOFT = CV_CTA_CTBLE
         AND C.NOMBRE = CV_NOMBRE_CTA
         AND C.CTAPOST IS NULL;
    --
    CURSOR C_ES_POST_1(CV_CTA_CTBLE VARCHAR2, CV_NOMBRE_CTA VARCHAR2) IS
      SELECT DISTINCT 1
        FROM COB_SERVICIOS B
       WHERE B.NOMBRE = CV_NOMBRE_CTA
         AND B.CTAPOST = CV_CTA_CTBLE;
  
    CURSOR C_TIPOS_FACTURAS IS
      SELECT S.GLOSA
        FROM TIPOS_FACTURAS_SAFI S
       WHERE S.ESTADO = 'A'
         AND S.ID_TIPO IN (PV_TIPO) --[9587] - SUD CAC
       ORDER BY 1;
  
  BEGIN
  
    IF PV_CTA_CTBLE IS NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    IF PV_NOMBRE_CTA IS NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    IF PV_FECHACTUAL IS NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    IF PV_GLOSA IS NULL THEN
      RAISE LE_ERROR;
    END IF;
    --
    OPEN C_ES_POST(PV_CTA_CTBLE, PV_NOMBRE_CTA);
    FETCH C_ES_POST
      INTO LN_EXISTE;
    LB_FOUND := C_ES_POST%FOUND;
    CLOSE C_ES_POST;
    --
    IF NOT LB_FOUND THEN
      OPEN C_ES_POST_1(PV_CTA_CTBLE, PV_NOMBRE_CTA);
      FETCH C_ES_POST_1
        INTO LN_EXISTE;
      LB_FOUND := C_ES_POST_1%FOUND;
      CLOSE C_ES_POST_1;
    END IF;
    --
    IF LB_FOUND THEN
      RETURN REPLACE(PV_GLOSA, '[%%SERVICIO%%]', PV_NOMBRE_CTA);
    ELSE
      OPEN C_TIPOS_FACTURAS;
      FETCH C_TIPOS_FACTURAS
        INTO GLOSA;
      LB_FOUND := C_TIPOS_FACTURAS%FOUND;
      CLOSE C_TIPOS_FACTURAS;
      --
      IF LB_FOUND THEN
        LV_FECHAPOSTERIOR := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHACTUAL,
                                                        'DD/MM/YYYY'),
                                                1),
                                     'DD/MM/YYYY');
        RETURN REPLACE(REPLACE(REPLACE(GLOSA,
                                       '[%%SERVICIO%%]',
                                       PV_NOMBRE_CTA),
                               '[%%FECHANTERIOR%%]',
                               PV_FECHACTUAL),
                       '[%%FECHACTUAL%%]',
                       LV_FECHAPOSTERIOR);
      ELSE
        RAISE LE_ERROR;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      RETURN 'Error al Obtener la glosa DTH';
    WHEN OTHERS THEN
      RETURN 'Error al Obtener la glosa DTH';
    
  END FN_GLOSA_DTH;
  --
  FUNCTION FN_IMPUESTO_DTH_IVA_ICE(PV_NOMBRE    IN VARCHAR2,
                                   PV_CTA_CTBLE IN VARCHAR2,
                                   PV_TIPO_IMP  IN VARCHAR2) RETURN NUMBER IS
  
    --=====================================================================================--
    -- Desarrollado por:  SUD Silvia Stefan�a Remache. (SRE)
    -- Lider proyecto: Ing. Paola Carvajal.
    -- PDS: SUD Cristhian Acosta Chambers
    -- Fecha de Creaci�n: 13/06/2013
    -- Proyecto: [8693] - Venta DTH Post Pago
    -- Motivo: Calculo de Impuestos IVA - ICE
    --=====================================================================================--                                  
  
    CURSOR C_OBT_IVA(CV_NOMBRE_SERV VARCHAR2, CV_CTA_CTBLE VARCHAR2) IS
      SELECT DISTINCT B.TIPO_IVA
        FROM COB_SERVICIOS B
       WHERE B.NOMBRE = CV_NOMBRE_SERV
         AND B.CTAPSOFT = CV_CTA_CTBLE;
    /**/
    CURSOR C_OBT_IVA_POST(CV_NOMBRE_SERV VARCHAR2, CV_CTA_CTBLE VARCHAR2) IS
      SELECT DISTINCT B.TIPO_IVA
        FROM COB_SERVICIOS B
       WHERE B.NOMBRE = CV_NOMBRE_SERV
         AND B.CTAPOST = CV_CTA_CTBLE;
  
    CURSOR C_OBT_VALOR_IVA(CN_VALOR NUMBER) IS
      SELECT T.TAXRATE / 100
        FROM TAX_CATEGORY T
       WHERE T.TAXCAT_ID = CN_VALOR;
  
    LV_OBT_IVA       VARCHAR2(30);
    LN_OBT_VALOR_IVA NUMBER(4, 2);
    LE_ERROR EXCEPTION;
    LB_FOUND BOOLEAN := FALSE;
  
  BEGIN
    --  
    IF PV_NOMBRE IS NULL THEN
      RAISE LE_ERROR;
    END IF;
    IF PV_CTA_CTBLE IS NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    OPEN C_OBT_IVA(PV_NOMBRE, PV_CTA_CTBLE);
    FETCH C_OBT_IVA
      INTO LV_OBT_IVA;
    LB_FOUND := C_OBT_IVA%NOTFOUND;
    CLOSE C_OBT_IVA;
    --
    IF LB_FOUND THEN
      OPEN C_OBT_IVA_POST(PV_NOMBRE, PV_CTA_CTBLE);
      FETCH C_OBT_IVA_POST
        INTO LV_OBT_IVA;
      LB_FOUND := C_OBT_IVA_POST%NOTFOUND;
      CLOSE C_OBT_IVA_POST;
    END IF;
    --
    IF LV_OBT_IVA IS NULL THEN
      RETURN 0;
    END IF;
    --
    IF PV_TIPO_IMP = 'IVA' THEN
      IF LV_OBT_IVA = 'I12' THEN
        OPEN C_OBT_VALOR_IVA(3);
        FETCH C_OBT_VALOR_IVA
          INTO LN_OBT_VALOR_IVA;
        CLOSE C_OBT_VALOR_IVA;
        IF LN_OBT_VALOR_IVA IS NULL THEN
          RETURN 0;
        END IF;
      ELSIF LV_OBT_IVA = 'I27' THEN
        OPEN C_OBT_VALOR_IVA(8);
        FETCH C_OBT_VALOR_IVA
          INTO LN_OBT_VALOR_IVA;
        CLOSE C_OBT_VALOR_IVA;
        IF LN_OBT_VALOR_IVA IS NULL THEN
          RETURN 0;
        END IF;
      END IF;
    ELSIF PV_TIPO_IMP = 'ICE' THEN
      IF LV_OBT_IVA = 'I27' THEN
        OPEN C_OBT_VALOR_IVA(4);
        FETCH C_OBT_VALOR_IVA
          INTO LN_OBT_VALOR_IVA;
        CLOSE C_OBT_VALOR_IVA;
        IF LN_OBT_VALOR_IVA IS NULL THEN
          RETURN 0;
        END IF;
      END IF;
    END IF;
    RETURN LN_OBT_VALOR_IVA;
  EXCEPTION
    WHEN LE_ERROR THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;
  END FN_IMPUESTO_DTH_IVA_ICE;
  --
  PROCEDURE GEN_RUBROS_NEG_DTH(FECHACIERREP IN DATE,
                               LV_ERROR     IN OUT VARCHAR2) IS
  
    --=====================================================================================--
    -- Creador por:    SUD Cristhian Acosta Chambers
    -- L�der proyecto: SIS Paola Carvajal
    -- Lider PDS:      SUD Cristhian Acosta Chambers
    -- Proyecto:       [8693] - VENTA DTH POSTPAGO
    -- Proposito:      Control de Rubros Negativos DTH.
    --=====================================================================================--           
    --CONTROL RUBROS NEGATIVOS 
    CURSOR C_RUBRO_NEG_DTH IS
      SELECT Y.*, ROWID LROWID FROM PRUEBAFINSYS_DTH_SAP Y;
    --ACTUALIZA RUBROS NEGATIVOS
    CURSOR C_CTAPOST_NEG(CV_PERIODO VARCHAR2) IS
      SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
        FROM PRUEBAFINSYS_DTH_SAP D
       WHERE D.CTA_CTRAP IS NULL
         AND D.PRODUCTO = 'DTH'
         AND D.FECHA_CORTE = TO_DATE(CV_PERIODO, 'DD/MM/YYYY');
    --
    CURSOR DESCX(CV_PERIODO VARCHAR2) IS
      SELECT DISTINCT R.CTAPSOFT,
                      R.CTA_DSCTO,
                      R.CONTRAPARTIDA_GYE,
                      R.CONTRAPARTIDA_UIO
        FROM COB_SERVICIOS R
       WHERE R.CTAPSOFT IN
             (SELECT DISTINCT D.CTACTBLE
                FROM PRUEBAFINSYS_DTH_SAP D
               WHERE D.CTA_CTRAP IS NULL
                 AND D.PRODUCTO = 'DTH'
                 AND D.FECHA_CORTE = TO_DATE(CV_PERIODO, 'DD/MM/YYYY'));
    --
    CURSOR CONSULTA_RUBRO(CV_TIPO         PRUEBAFINSYS_DTH_SAP.TIPO%TYPE,
                          CV_NOMBRE       PRUEBAFINSYS_DTH_SAP.NOMBRE%TYPE,
                          CV_PRODUCTO     PRUEBAFINSYS_DTH_SAP.PRODUCTO%TYPE,
                          CV_COMPA�IA     PRUEBAFINSYS_DTH_SAP.COMPA�IA%TYPE,
                          CV_CTACTBLE     PRUEBAFINSYS_DTH_SAP.CTACTBLE%TYPE,
                          CV_TIPO_FACTURA PRUEBAFINSYS_DTH_SAP.TIPO_FACTURA%TYPE) IS
      SELECT 'X'
        FROM PRUEBAFINSYS_SAP S
       WHERE S.TIPO = CV_TIPO --LC_RUBRO_NEG_DTH.TIPO
         AND S.NOMBRE = CV_NOMBRE --LC_RUBRO_NEG_DTH.NOMBRE
         AND S.PRODUCTO = CV_PRODUCTO --LC_RUBRO_NEG_DTH.PRODUCTO
         AND S.COMPA�IA = CV_COMPA�IA --LC_RUBRO_NEG_DTH.COMPA�IA
         AND S.CTACTBLE = CV_CTACTBLE --LC_RUBRO_NEG_DTH.CTACTBLE
         AND S.TIPO_FACTURA = CV_TIPO_FACTURA --LC_RUBRO_NEG_DTH.TIPO_FACTURA
      ;
    LV_EXISTE_RUBRO VARCHAR2(2) := NULL;
    LB_BOOLEAN_EXI  BOOLEAN := FALSE;
    --
    --RECOGE CUENTAS POST PAGO DEL PRODUCTO DTH
    CURSOR C_CTAPOST IS
      SELECT D.CTACTBLE, D.NOMBRE, D.ROWID
        FROM PRUEBAFINSYS_SAP D
       WHERE D.CTA_CTRAP IS NULL
         AND D.PRODUCTO = 'DTH';
    --
    CURSOR DESCX_2 IS
      SELECT DISTINCT R.CTAPSOFT,
                      R.CTA_DSCTO,
                      R.CONTRAPARTIDA_GYE,
                      R.CONTRAPARTIDA_UIO
        FROM COB_SERVICIOS R
       WHERE R.CTAPSOFT IN (SELECT DISTINCT D.CTACTBLE
                              FROM PRUEBAFINSYS_SAP D
                             WHERE D.CTA_CTRAP IS NULL
                               AND D.PRODUCTO = 'DTH');
    --
    TYPE RC_CURSOR IS REF CURSOR;
    LC_CURSOR        RC_CURSOR;
    LC_RUBRO_NEG_DTH C_RUBRO_NEG_DTH%ROWTYPE;
    --                              
    LVSENTENCIA    VARCHAR2(18000) := NULL;
    LVFECHA        VARCHAR2(10) := TO_CHAR(FECHACIERREP, 'DDMMYYYY');
    PV_FECHACIERRE VARCHAR2(70) := TO_CHAR(FECHACIERREP, 'DD/MM/YYYY');
    LE_ERROR EXCEPTION;
    LE_ERROR2 EXCEPTION;
    --
  BEGIN
    LVSENTENCIA := NULL;
    LVSENTENCIA := ' DELETE  /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH_SAP X WHERE X.FECHA_CORTE = TO_DATE(''' ||
                   PV_FECHACIERRE || ''',''DD/MM/YYYY'')';
    BEGIN
      EXECUTE IMMEDIATE LVSENTENCIA;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := 'ERROR => ' || SQLERRM;
        RAISE LE_ERROR;
    END;
    --RECOGE LOS RUBROS NEGATIVOS DEL PRODUCTO DTH
    LVSENTENCIA := NULL;
    LVSENTENCIA := ' INSERT INTO PRUEBAFINSYS_DTH_SAP ';
    LVSENTENCIA := LVSENTENCIA ||
                   ' SELECT /* + RULE */  H.TIPO,H.NOMBRE,H.PRODUCTO,SUM(H.VALOR) VALOR ,SUM(H.DESCUENTO) DESCUENTO,H.COST_DESC COMPA�IA , ' ||
                   ' DECODE(H.PRODUCTO,''DTH'',H.CTACLBLEP,MAX(H.CTACLBLEP)), ' ||
                   ' NULL  CUENTA_DESC, GRABA_SAFI_FACT_SAP.VERIFICA_TIPO_FACTURA(TO_DATE(''' ||
                   PV_FECHACIERRE ||
                   ''',''DD/MM/YYYY''),H.CUSTOMER_ID) TIPO_FACTURACION, NULL ';
    LVSENTENCIA := LVSENTENCIA || ' ,H.CUSTCODE, ''' || PV_FECHACIERRE ||
                   ''', NULL , H.CUSTOMER_ID ';
    LVSENTENCIA := LVSENTENCIA || ' FROM CO_FACT_' || LVFECHA || ' H ' ||
                   ' WHERE H.CUSTOMER_ID NOT  IN (SELECT CUSTOMER_ID FROM CUSTOMER_TAX_EXEMPTION CTE WHERE CTE.EXEMPT_STATUS = ''A'') ';
    --LVSENTENCIA:= LVSENTENCIA||' AND H.PRODUCTO = ''DTH'' AND H.VALOR < 0 AND SERVICIO NOT IN (46,38) '; --9578 - Se cambia la validaci�n para excluir por tipo
    LVSENTENCIA := LVSENTENCIA ||
                   ' AND H.PRODUCTO = ''DTH'' AND H.VALOR < 0 AND TIPO NOT IN (''006 - CREDITOS'',''005 - CARGOS'') ';
    LVSENTENCIA := LVSENTENCIA ||
                   ' GROUP BY H.TIPO,H.NOMBRE,H.PRODUCTO,H.COST_DESC, GRABA_SAFI_FACT_SAP.VERIFICA_TIPO_FACTURA(TO_DATE(''' ||
                   PV_FECHACIERRE ||
                   ''',''DD/MM/YYYY''),H.CUSTOMER_ID),H.CTACLBLEP ,H.CUSTCODE, H.CUSTOMER_ID ';
    --
    BEGIN
      EXECUTE IMMEDIATE LVSENTENCIA;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := 'ERROR => ' || SQLERRM;
        RAISE LE_ERROR;
    END;
    --
    LVSENTENCIA := NULL;
    LVSENTENCIA := ' SELECT  /*+INDEX (X IDX_FINSYS_3)+*/ X.*, ROWID FROM PRUEBAFINSYS_DTH_SAP X WHERE X.FECHA_CORTE = TO_DATE(''' ||
                   PV_FECHACIERRE || ''',''DD/MM/YYYY'')';
    OPEN LC_CURSOR FOR LVSENTENCIA;
    --SE REVISA Y ACTUALIZA SI EL RUBRO APLICA A SER CONTABILIZADO
    LOOP
      LC_RUBRO_NEG_DTH := NULL;
      FETCH LC_CURSOR
        INTO LC_RUBRO_NEG_DTH;
      EXIT WHEN LC_CURSOR%NOTFOUND;
      --
      LVSENTENCIA := NULL;
      LVSENTENCIA := ' UPDATE PRUEBAFINSYS_DTH_SAP X ' ||
                     ' SET X.ESTADO = NVL((SELECT ''S'' FROM ORDERHDR_ALL D WHERE D.CUSTOMER_ID IN (X.CUSTOMER_ID) ' ||
                     ' AND D.OHENTDATE = TO_DATE(''' || PV_FECHACIERRE ||
                     ''',''DD/MM/YYYY'') ' ||
                     ' AND D.OHSTATUS = ''IN'' AND D.OHINVAMT_DOC > 0),''N'') ' ||
                     ' WHERE X.ROWID = ''' || LC_RUBRO_NEG_DTH.LROWID || '''';
      -- 
      BEGIN
        EXECUTE IMMEDIATE LVSENTENCIA;
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := 'ERROR => ' || SQLERRM;
          RAISE LE_ERROR;
      END;
    END LOOP;
    CLOSE LC_CURSOR;
    --
    LVSENTENCIA := NULL;
    LVSENTENCIA := ' SELECT /*+INDEX (X IDX_FINSYS_3)+*/ S.TIPO TIPO,S.NOMBRE NOMBRE,S.PRODUCTO PRODUCTO,SUM(S.VALOR) VALOR,SUM(S.DESCUENTO) DESCUENTO, ' ||
                   ' S.COMPA�IA COMPA�IA,S.CTACTBLE CTACTBLE,NULL CTA_DESC,S.TIPO_FACTURA TIPO_FACTURA, ' ||
                   ' NULL CTA_CTRAP, NULL CUSTCODE ,NULL FECHA_CORTE,NULL ESTADO,NULL CUSTOMER_ID, NULL ' ||
                   ' FROM PRUEBAFINSYS_DTH_SAP S WHERE S.ESTADO <>''N'' AND S.FECHA_CORTE = TO_DATE(''' ||
                   PV_FECHACIERRE || ''',''DD/MM/YYYY'') ' ||
                   ' GROUP BY S.TIPO,S.NOMBRE,S.PRODUCTO,S.COMPA�IA,S.TIPO_FACTURA,S.CTACTBLE ';
    --                                 
    OPEN LC_CURSOR FOR LVSENTENCIA;
    LOOP
      LC_RUBRO_NEG_DTH := NULL;
      FETCH LC_CURSOR
        INTO LC_RUBRO_NEG_DTH;
      EXIT WHEN LC_CURSOR%NOTFOUND;
      --                     
      BEGIN
        --
        LV_EXISTE_RUBRO := NULL;
        LB_BOOLEAN_EXI  := FALSE;
        OPEN CONSULTA_RUBRO(LC_RUBRO_NEG_DTH.TIPO,
                            LC_RUBRO_NEG_DTH.NOMBRE,
                            LC_RUBRO_NEG_DTH.PRODUCTO,
                            LC_RUBRO_NEG_DTH.COMPA�IA,
                            LC_RUBRO_NEG_DTH.CTACTBLE,
                            LC_RUBRO_NEG_DTH.TIPO_FACTURA);
        --                           
        FETCH CONSULTA_RUBRO
          INTO LV_EXISTE_RUBRO;
        LB_BOOLEAN_EXI := CONSULTA_RUBRO%FOUND;
        CLOSE CONSULTA_RUBRO;
        --
        IF LB_BOOLEAN_EXI THEN
          --SE ACTUALIZAN LOS VALORES
          UPDATE PRUEBAFINSYS_SAP S
             SET S.VALOR     = S.VALOR + LC_RUBRO_NEG_DTH.VALOR,
                 S.DESCUENTO = S.DESCUENTO + LC_RUBRO_NEG_DTH.DESCUENTO
           WHERE S.TIPO = LC_RUBRO_NEG_DTH.TIPO
             AND S.NOMBRE = LC_RUBRO_NEG_DTH.NOMBRE
             AND S.PRODUCTO = LC_RUBRO_NEG_DTH.PRODUCTO
             AND S.COMPA�IA = LC_RUBRO_NEG_DTH.COMPA�IA
             AND S.CTACTBLE = LC_RUBRO_NEG_DTH.CTACTBLE
             AND S.TIPO_FACTURA = LC_RUBRO_NEG_DTH.TIPO_FACTURA;
          --
          IF SQL%FOUND THEN
            COMMIT;
          ELSIF SQL%NOTFOUND THEN
            LV_ERROR := 'ERROR => ' ||
                        'No se actualizo rubro negativo Tipo:' ||
                        LC_RUBRO_NEG_DTH.TIPO || ' - Nombre: ' ||
                        LC_RUBRO_NEG_DTH.NOMBRE || ' - ' ||
                        LC_RUBRO_NEG_DTH.PRODUCTO;
            RAISE LE_ERROR2;
          END IF;
        ELSE
          --SE INSERTAN LOS RUBROS
          INSERT INTO PRUEBAFINSYS_SAP
            (VALOR,
             DESCUENTO,
             TIPO,
             NOMBRE,
             PRODUCTO,
             COMPA�IA,
             CTACTBLE,
             TIPO_FACTURA)
          VALUES
            (LC_RUBRO_NEG_DTH.VALOR,
             LC_RUBRO_NEG_DTH.DESCUENTO,
             LC_RUBRO_NEG_DTH.TIPO,
             LC_RUBRO_NEG_DTH.NOMBRE,
             LC_RUBRO_NEG_DTH.PRODUCTO,
             LC_RUBRO_NEG_DTH.COMPA�IA,
             LC_RUBRO_NEG_DTH.CTACTBLE,
             LC_RUBRO_NEG_DTH.TIPO_FACTURA);
        END IF;
        --      
      EXCEPTION
        WHEN LE_ERROR2 THEN
          RAISE LE_ERROR;
        WHEN OTHERS THEN
          LV_ERROR := 'ERROR => ' || SQLERRM;
          RAISE LE_ERROR;
      END;
    END LOOP;
    CLOSE LC_CURSOR;
    --   
    --ACTUALIZA EN BITACORA LAS CUENTAS CONTRAPARTIDAS
    FOR RD IN DESCX(PV_FECHACIERRE) LOOP
      UPDATE /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH_SAP F
         SET F.CTA_DESC  = RD.CTA_DSCTO,
             F.CTA_CTRAP = DECODE(F.COMPA�IA,
                                  'Guayaquil',
                                  RD.CONTRAPARTIDA_GYE,
                                  'Quito',
                                  RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
       WHERE F.CTACTBLE = RD.CTAPSOFT
         AND F.FECHA_CORTE = TO_DATE(PV_FECHACIERRE, 'DD/MM/YYYY');
    END LOOP;
    COMMIT;
    --ACTUALIZA CUENTAS EN BITACORA POST PAGADAS CON LA SU CONTRAPARTIDA - SUD NCA
    FOR AX IN C_CTAPOST_NEG(PV_FECHACIERRE) LOOP
      UPDATE /*+INDEX (X IDX_FINSYS_3)+*/ PRUEBAFINSYS_DTH_SAP A
         SET A.CTA_CTRAP =
             (SELECT DECODE(A.COMPA�IA,
                            'Guayaquil',
                            D.CONTRAPARTIDA_GYE,
                            'Quito',
                            D.CONTRAPARTIDA_UIO)
                FROM COB_SERVICIOS D
               WHERE D.CTAPOST = AX.CTACTBLE
                 AND D.NOMBRE = AX.NOMBRE)
       WHERE A.ROWID = AX.ROWID;
    END LOOP COMMIT;
    --
    --ACTUALIZA CUENTAS POST PAGADAS CON LA SU CONTRAPARTIDA - SUD SRE
    FOR AZ IN C_CTAPOST LOOP
      UPDATE PRUEBAFINSYS_SAP A
         SET A.CTA_CTRAP =
             (SELECT DECODE(A.COMPA�IA,
                            'Guayaquil',
                            D.CONTRAPARTIDA_GYE,
                            'Quito',
                            D.CONTRAPARTIDA_UIO)
                FROM COB_SERVICIOS D
               WHERE D.CTAPOST = AZ.CTACTBLE
                 AND D.NOMBRE = AZ.NOMBRE)
       WHERE A.ROWID = AZ.ROWID;
    END LOOP COMMIT;
    --
    --ASIGNA CUENTAS CONTRAPARTIDAS SEGUN LA COMPA�IA - SUD SRE
    FOR RD IN DESCX_2 LOOP
      UPDATE PRUEBAFINSYS_SAP F
         SET F.CTA_DESC  = RD.CTA_DSCTO,
             F.CTA_CTRAP = DECODE(F.COMPA�IA,
                                  'Guayaquil',
                                  RD.CONTRAPARTIDA_GYE,
                                  'Quito',
                                  RD.CONTRAPARTIDA_UIO) --ASIGNA CUENTA DE CONTRAPARTIDA
       WHERE F.CTACTBLE = RD.CTAPSOFT;
    END LOOP;
    COMMIT;
    --
  EXCEPTION
    WHEN LE_ERROR THEN
      LV_ERROR := 'GEN_RUBROS_NEG_DTH - ' || LV_ERROR;
      IF LC_CURSOR%ISOPEN THEN
        CLOSE LC_CURSOR;
      END IF;
    WHEN OTHERS THEN
      LV_ERROR := 'GEN_RUBROS_NEG_DTH - ERROR => ' || SQLERRM;
      IF LC_CURSOR%ISOPEN THEN
        CLOSE LC_CURSOR;
      END IF;
  END GEN_RUBROS_NEG_DTH;
  --

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION OBTENER_VALOR_PARAMETRO_SAP(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER,
                       CV_ID_PARAMETRO      VARCHAR2) IS
      SELECT VALOR
        FROM GSIB_PARAMETROS_SAP
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END OBTENER_VALOR_PARAMETRO_SAP;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION CREAR_TRAMA(PDFECHCIERREPERIODO IN DATE,
                       PV_PRODUCTO         IN VARCHAR2,
                       PV_REGION           IN VARCHAR2,
                       PV_IDENTIFICADOR    OUT VARCHAR2,
                       ERROR               OUT VARCHAR2) RETURN CLOB IS
  
    PV_APLICACION  VARCHAR2(100) := 'GRABA_SAFI_FACT_SAP.CREAR_TRAMA';
    LV_IVA         VARCHAR2(10);
    ACU            NUMBER := 0;
    LC_CABECERA    CLOB;
    LC_CUERPO_TEMP CLOB;
    LC_CUERPO_FIN  CLOB;
    RESULTADO      VARCHAR2(100);
    LV_NODO_RAIZ   VARCHAR2(100);
    LV_SOCIEDAD    VARCHAR2(10);
    MY_ERROR EXCEPTION;
    PN_BASE_IMPONIBLE NUMBER;
    PV_RESULTADO_BASE VARCHAR2(1000);
    PN_VALOR          NUMBER := 0;
    PN_DESCUENTO      NUMBER := 0;
    PV_BASE_IMPONIBLE VARCHAR2(30);
    --OBTENGO TODOS LOS REGISTROS DEACUERDO AL PRODUCTO Y A LA REGION
    CURSOR C_PROD_REG IS
      SELECT *
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
       ORDER BY G.CLAVE_CONTA;
  
    --OBTENGO EL IDENTIFICADOR UNICO POR CADA PRODUCTO Y  REGION
    CURSOR C_IDENTIFICADOR IS
      SELECT DISTINCT (G.IDENTIFICADOR)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_IDENTIFICADOR GSIB_SUMARIZADO_SAP.IDENTIFICADOR%TYPE;
  
    --OBTENGO LA FECHA DEL REGISTRO CONTABLE
    CURSOR C_REGISTRO_CONTABLE IS
      SELECT DISTINCT (G.FECHA_CONTABILIZACION)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_REGISTRO_CONTABLE GSIB_SUMARIZADO_SAP.FECHA_CONTABILIZACION%TYPE;
  
    --OBTENGO EL TIPO DE DOCUMENTO
    CURSOR C_CLASE_DOC IS
      SELECT DISTINCT (G.CLASE_DOC)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_CLASE_DOC GSIB_SUMARIZADO_SAP.CLASE_DOC%TYPE;
  
    --OBTENGO LA REFERENCIA
    CURSOR C_REFERENCIA IS
      SELECT DISTINCT (G.REFERENCIA)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_REFERENCIA GSIB_SUMARIZADO_SAP.REFERENCIA%TYPE;
  
    --OBTENGO TEXTO CABECERA
    CURSOR C_TXT_CABECERA IS
      SELECT DISTINCT (G.TEXTO_CABECERA)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_TXT_CABECERA GSIB_SUMARIZADO_SAP.TEXTO_CABECERA%TYPE;
  
    --OBTENGO ANIO FISCAL
    CURSOR C_ANIO_FISCAL IS
      SELECT DISTINCT (G.ANIO_FINAL)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_ANIO_FISCAL GSIB_SUMARIZADO_SAP.ANIO_FINAL%TYPE;
  
    --OBTENGOEL MES FISCAL
    CURSOR C_MES_FISCAL IS
      SELECT DISTINCT (G.MES_FINAL)
        FROM GSIB_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND ROWNUM = 1;
    LV_MES_FISCAL GSIB_SUMARIZADO_SAP.MES_FINAL%TYPE;
  
  BEGIN
  
    IF PV_PRODUCTO IS NULL OR
       PV_PRODUCTO <> 'BSCS' AND PV_PRODUCTO <> 'DTH' THEN
      RESULTADO := 'El parametro PV_PRODUCTO no es correcto. Debe ingresar BSCS,DTH => ';
      RAISE MY_ERROR;
    END IF;
    IF PV_REGION IS NULL OR PV_REGION <> 'COSTA' AND PV_REGION <> 'SIERRA' THEN
      RESULTADO := 'El parametro PV_REGION no es correcto. Debe ingresar COSTA,SIERRA => ';
      RAISE MY_ERROR;
    END IF;
  
    LC_CABECERA := GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                   'CABECERA');
    IF LC_CABECERA IS NULL THEN
      RESULTADO := 'El parametro CABECERA no existe en la GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LV_NODO_RAIZ := GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                    'NODO_RAIZ');
    IF LV_NODO_RAIZ IS NULL THEN
      RESULTADO := 'El parametro NODO_RAIZ no existe en la GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LC_CABECERA := REPLACE(LC_CABECERA, '<%N_RAIZ%>', LV_NODO_RAIZ);
  
    --
    OPEN C_IDENTIFICADOR;
    FETCH C_IDENTIFICADOR
      INTO LV_IDENTIFICADOR;
    CLOSE C_IDENTIFICADOR;
  
    IF LV_IDENTIFICADOR IS NULL THEN
      RESULTADO := 'IDENTIFICADOR PARA EL PRODUCTO ' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
  
    PV_IDENTIFICADOR := LV_IDENTIFICADOR;
    LC_CABECERA      := REPLACE(LC_CABECERA,
                                '<%ID_POLIZA%>',
                                REPLACE(LV_IDENTIFICADOR, ' ', ''));
  
    --
    LV_SOCIEDAD := GRABA_SAFI_FACT_SAP.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                   'SOCIEDAD');
    IF LV_SOCIEDAD IS NULL THEN
      RESULTADO := 'PARAMETRO SOCIEDAD NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%SOCIEDAD%>', LV_SOCIEDAD);
  
    --
    OPEN C_REGISTRO_CONTABLE;
    FETCH C_REGISTRO_CONTABLE
      INTO LV_REGISTRO_CONTABLE;
    CLOSE C_REGISTRO_CONTABLE;
  
    IF LV_REGISTRO_CONTABLE IS NULL THEN
      RESULTADO := 'REGISTRO CONTABLE PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%FECHA%>',
                           TO_CHAR(LV_REGISTRO_CONTABLE, 'YYYYMMDD'));
  
    --
  
    OPEN C_CLASE_DOC;
    FETCH C_CLASE_DOC
      INTO LV_CLASE_DOC;
    CLOSE C_CLASE_DOC;
  
    IF LV_CLASE_DOC IS NULL THEN
      RESULTADO := 'LA CLASE DE DOCUMENTO PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%DOCUMENTO%>', LV_CLASE_DOC);
  
    --
  
    OPEN C_REFERENCIA;
    FETCH C_REFERENCIA
      INTO LV_REFERENCIA;
    CLOSE C_REFERENCIA;
  
    IF LV_REFERENCIA IS NULL THEN
      RESULTADO := 'LA REFERENCIA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%REFERENCIA_CIUDAD%>',
                           LV_REFERENCIA);
  
    --
  
    OPEN C_TXT_CABECERA;
    FETCH C_TXT_CABECERA
      INTO LV_TXT_CABECERA;
    CLOSE C_TXT_CABECERA;
  
    IF LV_TXT_CABECERA IS NULL THEN
      RESULTADO := 'EL TEXTO CABECERA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%CICLO%>', LV_TXT_CABECERA);
  
    --
  
    OPEN C_ANIO_FISCAL;
    FETCH C_ANIO_FISCAL
      INTO LV_ANIO_FISCAL;
    CLOSE C_ANIO_FISCAL;
  
    IF LV_ANIO_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%ANIO%>', LV_ANIO_FISCAL);
  
    --
  
    OPEN C_MES_FISCAL;
    FETCH C_MES_FISCAL
      INTO LV_MES_FISCAL;
    CLOSE C_MES_FISCAL;
  
    IF LV_MES_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%MES%>', LV_MES_FISCAL); -- +1 solo para pruebas
  
    -----
  
    ---
  
    FOR REC IN C_PROD_REG LOOP
      ACU            := ACU + 1;
      LC_CUERPO_TEMP := OBTENER_VALOR_PARAMETRO_SAP(10189, 'CUERPO');
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%POSICION%>', ACU);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CLAVE_CONTA%>',
                                REC.CLAVE_CONTA);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%FECHA%>',
                                TO_CHAR(REC.FECHA_CONTABILIZACION,
                                        'YYYYMMDD'));
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CUENTABSCS%>',
                                REC.ACCOUNT);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CENTRO_BENEFICIO%>',
                                REC.CENTRO_BENEFICIO);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%MONTO%>',
                                GSIB_API_SAP.ABS_NUMERO(REC.MONETARY_AMOUNT));
    
      IF REC.IVA = 'V2' THEN
        LV_IVA := REC.IVA;
      ELSIF REC.IVA = 'V1' THEN
        LV_IVA := REC.IVA;
      ELSE
        LV_IVA := '^';
      END IF;
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%IMPUESTO%>', LV_IVA);
    
      IF REC.ACCOUNT = '2104021000' THEN
        CALCULAR_BASE_IMPONIBLE(PDFECHCIERREPERIODO,
                                PV_PRODUCTO,
                                PV_REGION,
                                PN_VALOR,
                                PN_DESCUENTO,
                                PN_BASE_IMPONIBLE,
                                PV_RESULTADO_BASE);
      
        IF PN_BASE_IMPONIBLE IS NULL THEN
          RESULTADO := 'ERROR AL OBTENER LA BASE IMPONIBLE';
          RAISE MY_ERROR;
        END IF;
      
        PV_BASE_IMPONIBLE := GSIB_API_SAP.ABS_NUMERO(PN_BASE_IMPONIBLE);
      
      ELSE
        PV_BASE_IMPONIBLE := '^';
      END IF;
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%VALOR_IMPUESTO%>',
                                PV_BASE_IMPONIBLE);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%IMPUESTO%>', LV_IVA);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%ASIGNACION%>',
                                'FACTURACION');
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%TEXTO%>', REC.TEXTO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<CICLO_FACTURACION>',
                                REC.CICLO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%MONEDA%>', REC.MONEDA);
    
      LC_CUERPO_FIN := LC_CUERPO_FIN || LC_CUERPO_TEMP;
    END LOOP;
  
    RETURN(LC_CABECERA || LC_CUERPO_FIN || ']');
  
  EXCEPTION
  
    WHEN MY_ERROR THEN
      ERROR := RESULTADO || ' ' || PV_APLICACION || '. ' ||
               DBMS_UTILITY.format_error_backtrace;
      RETURN NULL;
    WHEN OTHERS THEN
      ERROR := 'ERROR ' || SQLERRM || ' ' || PV_APLICACION || '. ' ||
               DBMS_UTILITY.format_error_backtrace;
      RETURN NULL;
    
  END CREAR_TRAMA;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE CALCULAR_BASE_IMPONIBLE(PV_FECHA_CIERRE   IN DATE, --FECHA DEL CORTE DD/MM/YYYY
                                    PV_PRODUCTO       VARCHAR2, --BSCS,DTH
                                    PV_REGION         VARCHAR2, --COSTA, SIERRA
                                    PN_VALOR          OUT NUMBER,
                                    PN_DESCUENTO      OUT NUMBER,
                                    PN_BASE_IMPONIBLE OUT NUMBER,
                                    PV_RESULTADO_BASE OUT VARCHAR2) IS
  
    LV_SQL            VARCHAR2(1000);
    LC_BASE_IMPONIBLE SYS_REFCURSOR;
    PV_APLICACION     VARCHAR2(200) := ' GRABA_SAFI_FACT_SAP.CALCULAR_BASE_IMPONIBLE';
    LE_ERROR_BASE EXCEPTION;
  BEGIN
  
    IF PV_PRODUCTO = 'BSCS' THEN
      LV_SQL := 'select SUM(D.VALOR),SUM(D.DESCUENTO),SUM(D.VALOR)-SUM(D.DESCUENTO)
                 from co_fact_' ||
                TO_CHAR(PV_FECHA_CIERRE, 'DDMMYYYY') || ' D
                 WHERE DECODE(D.producto,''DTH'',''DTH'',''AUTOCONTROL'',''BSCS'',''TARIFARIO'',''BSCS'',''BSCS'') =''' ||
                PV_PRODUCTO ||
                ''' AND DECODE(D.COST_DESC,''Guayaquil'',''COSTA'',''Quito'',''SIERRA'') =''' ||
                PV_REGION ||
                ''' AND D.tipo in  (''002 - ADICIONALES'') AND D.VALOR>0';
    
      OPEN LC_BASE_IMPONIBLE FOR LV_SQL;
      FETCH LC_BASE_IMPONIBLE
        INTO PN_VALOR, PN_DESCUENTO, PN_BASE_IMPONIBLE;
      CLOSE LC_BASE_IMPONIBLE;
    
    ELSIF PV_PRODUCTO = 'DTH' THEN
      LV_SQL := 'SELECT SUM(D.MONETARY_AMOUNT)*-1
                 FROM GSI_FIN_SAFI_SAP D, GSI_FIN_CEBES G
                 WHERE D.ACCOUNT = G.CTA_SAP
                 AND DECODE(D.ACCOUNT,''2104020584'',''V2'',G.TIPO_IVA)  = ''V2''
                 AND G.CTA_SAP <>''2104021000''
                 AND DECODE(SUBSTR(D.TRANSACTION_ID,0,2),''BU'',''SIERRA'',''BG'',''COSTA'')=''' ||
                PV_REGION || '''
                 AND D.DOC_TYPE=''CBSDTHE'' 
                 AND TO_CHAR(D.ACCOUNTING_DT,''DD/MM/YYYY'') =''' ||
                TO_CHAR(PV_FECHA_CIERRE, 'DD/MM/YYYY') || '''';
    
      OPEN LC_BASE_IMPONIBLE FOR LV_SQL;
      FETCH LC_BASE_IMPONIBLE
        INTO PN_BASE_IMPONIBLE;
      CLOSE LC_BASE_IMPONIBLE;
    
    END IF;
  
    PV_RESULTADO_BASE := 'BASE IMPONIBLE ' || PV_PRODUCTO || ' ' ||
                         PV_REGION || ' ES : ' || PN_BASE_IMPONIBLE;
  
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE ENVIA_TRAMAS_SAFI_SAP(PDFECHCIERREPERIODO IN DATE, --FECHA DE CORTE
                                  PV_PRODUCTO         IN VARCHAR2, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
                                  PV_EJECUCION        IN VARCHAR2, -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP 
                                  PV_RESULTADO        OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(200) := 'GRABA_SAFI_FACT_SAP.ENVIA_TRAMAS_SAFI_SAP';
  
  BEGIN
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****INICIO ENVIA TRAMAS FACTURACION ' ||
                           pv_producto || 'FECHA DE CORTE :' ||
                           PDFECHCIERREPERIODO || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    GSIB_API_SAP.envia_tramas(PDFECHCIERREPERIODO,
                              'F', --f por facturacion 
                              PV_PRODUCTO,
                              PV_EJECUCION,
                              pn_id_transaccion,
                              PV_RESULTADO);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****FIN ENVIA TRAMAS FACTURACION ' ||
                           pv_producto || 'FECHA DE CORTE :' ||
                           PDFECHCIERREPERIODO || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/2015 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE REENVIA_POLIZAS(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2) IS
    LV_APLICACION VARCHAR2(200) := 'GRABA_SAFI_FACT_SAP.REENVIA_POLIZAS';
    CURSOR busca_id_poliza(cv_poliza VARCHAR2) IS
      SELECT * FROM GSIB_TRAMAS_SAP t WHERE t.id_poliza = cv_poliza;
  BEGIN
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****INICIO RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    gsib_api_sap.reenvio_tramas(pv_id_polizas,
                                'F',
                                pn_id_transaccion,
                                pv_resultado);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****FIN RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  END;
  /*PROCEDURE REPROCESO_ASIENTOS_CONTABLES(PDFECHCIERREPERIODO IN DATE, --FECHA DE CORTE
                                   PV_PRODUCTO         IN VARCHAR2, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH.
                                   RESULTADO           OUT VARCHAR2) IS
  LV_APLICACION VARCHAR2(150):='GRABA_SAFI_FACT_SAP.REPROCESO_ASIENTOS_CONTABLES';
  LE_ERROR exception;
  LV_ERROR VARCHAR2(150);
  ln_dia number:= to_number(to_char(PDFECHCIERREPERIODO,'dd'));
  ln_mes number:=to_number(to_char(PDFECHCIERREPERIODO,'mm'));
  ln_anio number:=to_number(to_char(PDFECHCIERREPERIODO,'yyyy'));
   CURSOR C_VALIDA_POLIZAS(CV_FECHA     DATE,
                     cv_escenario VARCHAR2,
                     cv_producto  VARCHAR2) IS
      SELECT COUNT(*)   FROM GSIB_TRAMAS_SAP D
       WHERE (D.ESTADO = 'CREADA' OR D.ESTADO = 'NO ENVIADA')
         AND D.REFERENCIA LIKE 'FACTURACION '||DECODE(cv_producto,
                           'VOZ',
                           'BSCS',
                           'DTH',
                           'DTH',
                           null) || '%'
         AND D.FECHA_CORTE = CV_FECHA;
   LN_VALIDA_POLIZAS NUMBER:=0;
    cursor c_valida_dth (cv_dia number,cv_mes number , cv_anio number) IS
     select count(*) from servicios_prepagados_dth_SAP D
  WHERE D.DIA =  cv_dia
     AND   D.MES=  cv_mes
      AND  D.ANIO= cv_anio  ;
    LN_VALIDA_DTH number:=0;
    BEGIN
     GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****INICIO REPROCESO ASIENTOS CONTABLES FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',pn_id_transaccion);
     IF PV_PRODUCTO IS NOT NULL THEN
        IF PV_PRODUCTO NOT IN ('DTH', 'VOZ') THEN
          LV_ERROR := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar NULL, DTH,VOZ => ';
          GSIB_SECURITY.dotraceh(LV_APLICACION, LV_ERROR,pn_id_transaccion);
          RAISE LE_ERROR;
        END IF;
          END IF;
    IF LN_VALIDA_POLIZAS = 0 THEN
      RESULTADO:='No existen polizas del corte del '||PDFECHCIERREPERIODO||'que reprocesar para este escenario ';
      GSIB_SECURITY.dotraceh(LV_APLICACION,RESULTADO,pn_id_transaccion);
    ELSE
      --CAMBIO EL ESTADO A TODAS LAS TRAMAS ERRONEAS GENERADAS HASTA ESE MOMENTO PARA ESE CORTE
     update gsib_tramas_sap d
     set d.estado      = 'ERROR',
         d.observacion = d.observacion|| ' => El : ' ||
                         TO_DATE(SYSDATE, 'dd/mm/yyyy hh:mi:ss') ||
                         ' se le cambio el estado por reproceso '||chr(13)
      where d.fecha_corte= PDFECHCIERREPERIODO
      and  D.REFERENCIA LIKE
             '%FACTURACION%'
         AND D.REFERENCIA LIKE
             '%' || DECODE(Pv_producto,
                           'VOZ',
                           'BSCS',
                           'DTH',
                           'DTH',
                           null,
                           'FACTURACION') || '%';
       GSIB_SECURITY.dotraceh(LV_APLICACION,'Se cambio el estado a todas las tramas erroneas generadas para ese corte',pn_id_transaccion);
   end if;
   open c_valida_dth(ln_dia,ln_mes,ln_anio);
   fetch c_valida_dth
   into ln_valida_dth;
   close c_valida_dth;
   if ln_valida_dth = 0 then
     RESULTADO:='No existen registros DTH del :'||PDFECHCIERREPERIODO||' en la tabla SERVICIOS_PREPAGADOS_DTH_SAP' ;
      GSIB_SECURITY.dotraceh(LV_APLICACION,RESULTADO,pn_id_transaccion);
   else
   --ELIMINO LOS REGISTROS DTH DE ESE CORTE, PARA QUE NO AFECTE EL DEVENGAMIENTO
  DELETE servicios_prepagados_dth_SAP D
  WHERE D.DIA =  extract(DAY FROM PDFECHCIERREPERIODO)
     AND   D.MES=  extract(MONTH FROM PDFECHCIERREPERIODO)
      AND  D.ANIO=   extract(YEAR FROM PDFECHCIERREPERIODO);
   GSIB_SECURITY.dotraceh(LV_APLICACION,'Se elimino los datos dth para que no afecte el devengamiento',pn_id_transaccion);
  end if;
        COMMIT;
   RESULTADO :='OK :Se completo ';
       GSIB_SECURITY.dotraceh(LV_APLICACION,RESULTADO,pn_id_transaccion);
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****FIN REPROCESO ASIENTOS CONTABLES FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',pn_id_transaccion);
      EXCEPTION
        WHEN LE_ERROR THEN
          RESULTADO  :=LV_ERROR;
       GSIB_SECURITY.dotraceh(LV_APLICACION,RESULTADO,pn_id_transaccion);
     GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****FIN REPROCESO ASIENTOS CONTABLES FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',pn_id_transaccion);
        WHEN OTHERS THEN
        RESULTADO :='ERROR : '||SQLCODE||' - '||SQLERRM;
           GSIB_SECURITY.dotraceh(LV_APLICACION,RESULTADO,pn_id_transaccion);
     GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****FIN REPROCESO ASIENTOS CONTABLES FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',pn_id_transaccion);
        ROLLBACK;
    END ;*/
  /*PROCEDURE respaldo_servicios_dth(pd_fecha_corte IN DATE,
                                   pv_resultado   OUT VARCHAR2) IS
    lv_aplicacion            VARCHAR2(200) := ' GRABA_SAFI_FACT_SAP.RESPALDO_SERVICIOS_DTH';
    lv_sentencia_crear_table VARCHAR2(2000);
    lv_sentencia_drop_table  VARCHAR2(2000);
    lv_nombre_tabla          VARCHAR2(100);
    CURSOR lc_valida_table(cv_nombre VARCHAR2) IS
      SELECT COUNT(*) FROM all_all_tables WHERE table_name = cv_nombre;
    lv_valida_table NUMBER := 0;
    lb_found        BOOLEAN;
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '****INICIO RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
    lv_nombre_tabla := 'SERVICIOS_PREPAGADOS_' ||
                       to_char(pd_fecha_corte, 'yyyymmdd');
    OPEN lc_valida_table(lv_nombre_tabla);
    FETCH lc_valida_table
      INTO lv_valida_table;
    CLOSE lc_valida_table;
    --si ya existe la tabla la elimino
    IF lv_valida_table >= 1 THEN
      lv_sentencia_drop_table := 'DROP TABLE ' || lv_nombre_tabla;
      EXECUTE IMMEDIATE lv_sentencia_drop_table;
      pv_resultado := 'La tabla ' || lv_nombre_tabla ||
                      ' ya existe , se procedio a eliminarla';
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
    END IF;
    lv_sentencia_crear_table := 'CREATE TABLE ' || lv_nombre_tabla ||
                                ' AS SELECT * FROM  servicios_prepagados_dth_SAP ';
    EXECUTE IMMEDIATE lv_sentencia_crear_table;
    pv_resultado := 'Se Creo correctamente la tabla ' || lv_nombre_tabla;
    gsib_security.dotraceh(lv_aplicacion, pv_resultado, pn_id_transaccion);
    gsib_security.dotraceh(lv_aplicacion,
                           '****INICIO RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'ERROR: ' || SQLERRM || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
      gsib_security.dotraceh(lv_aplicacion,
                             '****FIN RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
  END;*/
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 05/07/2015 11:29:08
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE anula_polizas(pd_fecha     IN DATE,
                          pv_producto  IN VARCHAR2,
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2) IS
    lv_aplicacion VARCHAR2(200) := 'GRABA_SAFI_FACT_SAP.ANULA_POLIZAS';
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '**** INICIO ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
    gsib_api_sap.anula_polizas(pd_fecha,
                               'FACTURACION',
                               pv_producto,
                               pn_id_transaccion,
                               pv_resultado,
                               pv_polizas);
    gsib_security.dotraceh(lv_aplicacion,
                           '**** FIN ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 05/07/2015 14:56:48
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2) IS
    --ciclos que tienen provision
    CURSOR c_ciclo IS
      SELECT DISTINCT ciclo,
                      decode(ciclo,
                             '01',
                             'Primer',
                             '02',
                             'Segundo',
                             '03',
                             'Tercer',
                             '04',
                             'Cuarto',
                             '05',
                             'Quinto',
                             '06',
                             'Sexto',
                             '07',
                             'S�ptimo',
                             '08',
                             'Octavo',
                             '09',
                             'Noveno',
                             '10',
                             'D�cimo',
                             'NUEVO CICLO A INGRESAR') des_ciclo
        FROM gsib_sumarizado_sap;
    --ciudad GYE/UIO
    CURSOR c_referencia IS
      SELECT DISTINCT referencia
        FROM gsib_sumarizado_sap
       ORDER BY referencia ASC;
    --datos para el reporte
    CURSOR c_datos(cv_referencia VARCHAR2, cv_ciclo VARCHAR2) IS
      SELECT s.texto,
             s.account,
             s.monetary_amount,
             s.identificador,
             s.fecha_contabilizacion,
             s.referencia,
             s.texto_cabecera,
             s.iva,
             s.clave_conta,
             s.centro_beneficio,
             s.ciclo
        FROM gsib_sumarizado_sap s
       WHERE referencia = cv_referencia
         AND ciclo = cv_ciclo
       ORDER BY s.clave_conta ASC;
    CURSOR lc_id_reporte IS
      SELECT nvl(MAX(id_reporte), 0) + 1 FROM gsib_reportes_html;
    ln_id_reporte     NUMBER := 0;
    lc_html           CLOB;
    lv_nombre_excel   VARCHAR2(1000);
    lv_ruta_escenario VARCHAR2(1000);
    lv_aplicacion     VARCHAR2(1000) := 'GRABA_SAFI_FACT_SAP.GENERA_REPORTE_EN_HTML';
  BEGIN
    lv_ruta_escenario := obtener_valor_parametro_sap(10189,
                                                     'RUTA_REPORTE_FACTURACION');
    FOR c IN c_ciclo LOOP
      FOR r IN c_referencia LOOP
        lv_nombre_excel := lower(lv_ruta_escenario || 'Facturacion_' ||
                                 REPLACE(r.referencia, ' ', '_') || '_' ||
                                 to_char(ld_fecha, 'ddmmyyyy') || '_' ||
                                 c.ciclo || '.xls');
        --cabecera
        lc_html := '<HTML>';
        lc_html := lc_html || '<HEAD>';
        lc_html := lc_html || '</HEAD>';
        lc_html := lc_html || '<BODY>';
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TABLE BORDER =1>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>ACCOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>MONETARY AMOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IDENTIFICADOR</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>FECHA CONTABILIZACION</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>REFERENCIA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO CABECERA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IVA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CLAVE CONTA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CENTRO BENEFICIO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CICLO</h4></STRONG></TD>';
        lc_html := lc_html || '</TR>';
        FOR d IN c_datos(r.referencia, c.ciclo) LOOP
          lc_html := lc_html || '<TR>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.account ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                     d.monetary_amount || '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.identificador ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.fecha_contabilizacion ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.referencia ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto_cabecera ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.iva ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.clave_conta ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.centro_beneficio ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.ciclo ||
                     '</STRONG></TD>';
          lc_html := lc_html || '</TR>';
        END LOOP;
        ln_id_reporte := NULL;
        lc_html       := lc_html || '</table>';
        OPEN lc_id_reporte;
        FETCH lc_id_reporte
          INTO ln_id_reporte;
        CLOSE lc_id_reporte;
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD><STRONG>ID REPORTE : </STRONG></TD>';
        lc_html := lc_html || '<TD>' || ln_id_reporte || '</TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '</table></html>';
        INSERT INTO gsib_reportes_html
          (id_reporte,
           nombre_reporte,
           fecha_corte,
           fecha_generacion,
           reporte_html)
        VALUES
          (sc_id_reporte.nextval,
           lv_nombre_excel,
           ld_fecha,
           SYSDATE,
           lc_html);
        COMMIT;
      END LOOP;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error : ' || SQLERRM || lv_aplicacion;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 12/07/2015 16:05:14
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reporte_valores_omitidos(ld_fecha IN DATE,
                                     pv_error OUT VARCHAR2) IS
    lv_aplicacion VARCHAR2(100) := 'GRABA_SAFI_FACT_SAP.REPORTE_VALORES_OMITODOS';
    CURSOR lc_tipo(cv_fecha DATE) IS
      SELECT DISTINCT (f.tipo)
        FROM pruebafinsys_dth_sap f
       WHERE f.fecha_corte = cv_fecha -- to_date('15/06/2015','dd/mm/yyyy')
         AND f.estado = 'N'
         AND compa�ia IN ('Quito', 'Guayaquil')
         AND f.tipo <> '006 - CREDITOS'
       ORDER BY f.tipo ASC;
    CURSOR lc_nombres(cv_fecha DATE, cv_tipo VARCHAR2) IS
      SELECT DISTINCT (f.nombre)
        FROM pruebafinsys_dth_sap f
       WHERE f.fecha_corte = cv_fecha --to_date('15/06/2015','dd/mm/yyyy')
         AND f.estado = 'N'
         AND tipo <> '006 - CREDITOS'
         AND f.tipo = cv_tipo
       ORDER BY f.nombre ASC;
    CURSOR lc_valor(cv_fecha    DATE,
                    cv_compania VARCHAR2,
                    cv_tipo     VARCHAR2,
                    cv_nombre   VARCHAR2) IS
      SELECT SUM(f.valor)
        FROM pruebafinsys_dth_sap f
       WHERE f.fecha_corte = cv_fecha --to_date('15/06/2015','dd/mm/yyyy')
         AND f.estado = 'N'
         AND f.compa�ia = cv_compania
         AND f.tipo <> '006 - CREDITOS'
         AND f.tipo = cv_tipo
         AND f.nombre = cv_nombre;
    CURSOR lc_id_reporte IS
      SELECT nvl(MAX(id_reporte), 0) + 1 FROM gsib_reportes_html;
    ln_id_reporte        NUMBER := 0;
    lv_htlm              CLOB;
    ln_valor_gye         NUMBER := 0;
    ln_valor_uio         NUMBER := 0;
    ln_suma              NUMBER := 0;
    ln_total_general_gye NUMBER := 0;
    ln_total_general_uio NUMBER := 0;
    ln_total_general     NUMBER := 0;
    lv_ruta_escenario    VARCHAR2(250);
    lv_nombre_excel      VARCHAR2(250);
  BEGIN
    lv_htlm := '<HTML>';
    lv_htlm := lv_htlm || '<HEAD>';
    lv_htlm := lv_htlm || '</HEAD>';
    lv_htlm := lv_htlm || '<BODY>';
    lv_htlm := lv_htlm || '<TABLE BORDER =1>';
    lv_htlm := lv_htlm || '<TR>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2><STRONG>TIPO</STRONG></TD>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2><STRONG>NOMBRE</STRONG></TD>';
    lv_htlm := lv_htlm ||
               '<TD COLSPAN=2 bgcolor="#FF0000" ><font color="white"><STRONG>Guayaquil</STRONG></TD>';
    lv_htlm := lv_htlm ||
               '<TD COLSPAN=2 bgcolor="#0000B8"><font color="white"><STRONG>Quito</STRONG></TD>';
    lv_htlm := lv_htlm ||
               '<TD COLSPAN=2><STRONG>Total General</STRONG></TD>';
    lv_htlm := lv_htlm || '</TR>';
    FOR i IN lc_tipo(ld_fecha) LOOP
      FOR f IN lc_nombres(ld_fecha, i.tipo) LOOP
        OPEN lc_valor(ld_fecha, 'Guayaquil', i.tipo, f.nombre);
        FETCH lc_valor
          INTO ln_valor_gye;
        CLOSE lc_valor;
        OPEN lc_valor(ld_fecha, 'Quito', i.tipo, f.nombre);
        FETCH lc_valor
          INTO ln_valor_uio;
        CLOSE lc_valor;
        ln_suma              := nvl(ln_valor_gye, 0) + nvl(ln_valor_uio, 0);
        lv_htlm              := lv_htlm || '<TR>';
        lv_htlm              := lv_htlm || '<TD COLSPAN=2>' || i.tipo ||
                                '</TD>';
        lv_htlm              := lv_htlm || '<TD COLSPAN=2>' || f.nombre ||
                                '</TD>';
        lv_htlm              := lv_htlm || '<TD COLSPAN=2 align="right">' ||
                                ln_valor_gye || '</TD>';
        lv_htlm              := lv_htlm || '<TD COLSPAN=2 align="right">' ||
                                ln_valor_uio || '</TD>';
        lv_htlm              := lv_htlm || '<TD COLSPAN=2 align="right">' ||
                                ln_suma || '</TD>';
        lv_htlm              := lv_htlm || '</TR>';
        ln_total_general_gye := nvl(ln_total_general_gye, 0) +
                                nvl(ln_valor_gye, 0);
        ln_total_general_uio := nvl(ln_total_general_uio, 0) +
                                nvl(ln_valor_uio, 0);
        ln_total_general     := nvl(ln_total_general, 0) + nvl(ln_suma, 0);
      END LOOP;
    END LOOP;
    lv_htlm := lv_htlm || '<TR>';
    lv_htlm := lv_htlm ||
               '<TD COLSPAN=2><STRONG>Total General</STRONG></TD>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2></TD>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2 align="right">' ||
               ln_total_general_gye || '</TD>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2 align="right">' ||
               ln_total_general_uio || '</TD>';
    lv_htlm := lv_htlm || '<TD COLSPAN=2 align="right">' ||
               ln_total_general || '</TD>';
    lv_htlm := lv_htlm || '</TR>';
    lv_htlm := lv_htlm || '</table>';
    OPEN lc_id_reporte;
    FETCH lc_id_reporte
      INTO ln_id_reporte;
    CLOSE lc_id_reporte;
    lv_htlm           := lv_htlm || '<TABLE BORDER =0>';
    lv_htlm           := lv_htlm || '<TR></TR>';
    lv_htlm           := lv_htlm || '<TR></TR>';
    lv_htlm           := lv_htlm || '<TR></TR>';
    lv_htlm           := lv_htlm || '<TR></TR>';
    lv_htlm           := lv_htlm || '<TR>';
    lv_htlm           := lv_htlm ||
                         '<TD><STRONG>ID REPORTE : </STRONG></TD>';
    lv_htlm           := lv_htlm || '<TD>' || ln_id_reporte || '</TD>';
    lv_htlm           := lv_htlm || '</TR>';
    lv_htlm           := lv_htlm || '</table></html>';
    lv_ruta_escenario := obtener_valor_parametro_sap(10189,
                                                     'RUTA_REPORTE_FACTURACION');
    lv_nombre_excel   := lower(lv_ruta_escenario ||
                               'Reporte_valores_omitidos' || '_' ||
                               to_char(ld_fecha, 'ddmmyyyy') || '.xls');
    INSERT INTO gsib_reportes_html
      (id_reporte,
       nombre_reporte,
       fecha_corte,
       fecha_generacion,
       reporte_html)
    VALUES
      (sc_id_reporte.nextval, lv_nombre_excel, ld_fecha, SYSDATE, lv_htlm);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR : ' || ' ' || SQLERRM || ' ' || lv_aplicacion;
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 20/10/2015 15:00:14
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- ProcedimiEnto que permite validar si existe productos dth para generar y enviar el reporte de valores omitidos
  --===================================================================================
  FUNCTION valida_dth_rep_omitidos(pdfechcierreperiodo IN DATE) RETURN NUMBER IS
    -- pdfechcierreperiodo DATE:=TO_dATE('02/10/2015','DD/MM/YYYY');
    cur_dth      SYS_REFCURSOR;
    sql_text_dth VARCHAR2(100) := ' SELECT COUNT(*)  FROM CO_FACT_' ||
                                  to_char(pdfechcierreperiodo, 'DDMMYYYY') ||
                                  '  WHERE PRODUCTO = ''DTH''AND ROWNUM <= 1'; --1 SI EXISTE DTH , 0 SI NO ESXISTE
  
    lv_existe_dth NUMBER := 0;
  
  BEGIN
    OPEN cur_dth FOR sql_text_dth;
    FETCH cur_dth
      INTO lv_existe_dth;
  
    RETURN lv_existe_dth;
  
  END;

END GRABA_SAFI_FACT_SAP;
/
