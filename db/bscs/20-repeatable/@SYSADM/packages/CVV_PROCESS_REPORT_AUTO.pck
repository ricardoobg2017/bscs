create or replace package CVV_PROCESS_REPORT_AUTO is
  procedure reporte_formapago(pd_fechaini in date, pd_fechafin in date);

end CVV_PROCESS_REPORT_AUTO;
/
create or replace package body CVV_PROCESS_REPORT_AUTO is

  procedure reporte_formapago(pd_fechaini in date, pd_fechafin in date) IS
   
   --DECLARACION DE VARIABLES
   lcidciclo                     Varchar2(2);
    HORA                          VARCHAR2(200);
    lb_cursor                     boolean := True;
    lb_regi_pagos_exceso          boolean := True;
    lb_regi_cartera_vencida       boolean := False;
    lb_regi_total_cartera_vencida boolean := False;
    lb_fecha_ciclo                boolean := False;
    lb_encontrar_region           boolean:=False;
    c                             number;
    R                             NUMBER;
    k                             number;
    -- 
    ln_conta_bita number;
    --
    LV_REGION                     VARCHAR2(50);
    cadena                        VARCHAR2(50);
  
    CLIENTES_TOTAL NUMBER := 0;
    clientes       NUMBER := 0;
    clientes_rv    NUMBER := 0;
    clientes_v     NUMBER := 0;
    clientes_pe    NUMBER := 0;
  
    CARTERA_TOTAl NUMBER := 0;
    cartera       NUMBER := 0;
    cartera_rv    NUMBER := 0;
    cartera_v     NUMBER := 0;
    cartera_pe    NUMBER := 0;
  
    PAGOS_TOTAL NUMBER := 0;
    pagos       NUMBER := 0;
    pagos_rv    NUMBER := 0;
    pagos_v     NUMBER := 0;
    pagos_pe    NUMBER := 0;
  
    DEBITOS_TOTAL NUMBER := 0;
    debitos       NUMBER := 0;
    debitos_rv    NUMBER := 0;
    debitos_v     NUMBER := 0;
    debitos_pe    NUMBER := 0;
  
    CREDITOS_TOTAL NUMBER := 0;
    creditos       NUMBER := 0;
    creditos_rv    NUMBER := 0;
    creditos_v     NUMBER := 0;
    creditos_pe    NUMBER := 0;
   
  
    LD_FECHA_CICLO DATE;
  
    recuperacion_rv NUMBER;
    RECUPERACION    NUMBER;
    LD_FECHA_GEN    DATE;
    lv_error        VARCHAR2(50);
-------------------------------------------------------------------------  
     cursor registro_hist(cv_region varchar2, cv_IdCiclo varchar2) is
      select /*+ rule */
       LV_REGION,
       lcidciclo,
       s.producto producto,
       mora rango,
       round(sum(total_deuda), 2) cartera,
       round(sum(TotPagos_Recuperado), 2) pagos,
       round(sum(DebitoRecuperado), 2) notas_debitos,
       round(sum(CreditoRecuperado), 2) notas_creditos,
       pd_fechaini,
       pd_fechafin,
       LD_FECHA_GEN,
       USER
      
        from SYSADM.ope_cuadre_DWL s, SYSADM.customer_all_DWL r
       where s.region = cv_region
         and s.customer_id = r.customer_id
         and tipo = '5_R'
         and mora > 0
         and id_ciclo = cv_IdCiclo
       group by s.producto, mora;
-------------------------------------------------------------------------  
    
    
      cursor regi (cv_region varchar2) is
      select /*+ rule */
       mora rango,
       count(s.customer_id) clientes,
       round(sum(total_deuda), 2) cartera,
       0 saldos,
       round(sum(TotPagos_Recuperado), 2) pagos,
       round(sum(DebitoRecuperado), 2) notas_debitos,
       round(sum(CreditoRecuperado), 2) notas_creditos,
       decode(sum(total_deuda),
              0,
              0,
              round((sum(TotPagos_Recuperado) / sum(total_deuda)) * 100, 2)) recuperacion
        from SYSADM.ope_cuadre_DWL s, SYSADM.customer_all_DWL r
       where s.customer_id = r.customer_id
         and s.region=cv_region
         and mora = 0
         and total_deuda >= 0
         and id_ciclo = lcidciclo
       group by mora;
  
--------------------------------------------------------------------------
    cursor regi_pagos_exceso(cv_region varchar2) is
      select /*+ rule */
       tipo,
       mora rango,
       count(s.customer_id) clientes,
       round(sum(total_deuda), 2) cartera,
       0 saldos,
       round(sum(TotPagos_Recuperado), 2) pagos,
       round(sum(DebitoRecuperado), 2) notas_debitos,
       round(sum(CreditoRecuperado), 2) notas_creditos,
       0 recuperacion
        from ope_cuadre_DWL s, customer_all_DWL r
       where s.customer_id = r.customer_id
         and tipo = '3_VNE'
         and s.region=cv_region
         and id_ciclo = lcidciclo
       group by tipo, mora;
---------------------------------------------------------------------  
    cursor regi_cartera_vencida(cv_region varchar2) is
      select /*+ rule */
       tipo,
       mora rango,
       count(s.customer_id) clientes,
       round(sum(total_deuda), 2) cartera,
       
       0 saldos,
       round(sum(TotPagos_Recuperado), 2) pagos,
       round(sum(DebitoRecuperado), 2) notas_debitos,
       round(sum(CreditoRecuperado), 2) notas_creditos,
       decode(sum(total_deuda),
              0,
              0,
              round((sum(TotPagos_Recuperado) / sum(total_deuda)) * 100, 2)) recuperacion
        from ope_cuadre_DWL s, customer_all_DWL r
       where s.customer_id = r.customer_id
        and s.region =cv_region
        and tipo = '5_R'
         and mora > 0
         and id_ciclo = lcidciclo
       group by tipo, mora;
-------------------------------------------------------------------- 
    cursor regi_total_cartera_vencida(cv_region varchar2) is
      select /*+ rule */
       tipo,
       count(s.customer_id) clientes,
       round(sum(total_deuda), 2) cartera,
       0 saldos,
       round(sum(TotPagos_Recuperado), 2) pagos,
       round(sum(DebitoRecuperado), 2) notas_debitos,
       round(sum(CreditoRecuperado), 2) notas_creditos,
        decode(sum(total_deuda),
              0,
              0,
              round((sum(TotPagos_Recuperado) / sum(total_deuda)) * 100, 2)) recuperacion
        from ope_cuadre_DWL s, customer_all_DWL r
       where s.customer_id = r.customer_id
         and s.region =cv_region
         and tipo = '5_R'
         and mora > 0
         and id_ciclo = lcidciclo
       group by tipo;
  
-------------------------------------------------------------------   
    Cursor c_fecha_ciclo is
      select distinct b.lrstart
        from bch_history_table b
       where b.lrstart < pd_fechaini
         and b.lrstart > (pd_fechaini - 31);
------------------------------------------------------------------- 
    cursor c_secuencia_ciclo is
      select seq_cvvresumen.nextval from dual;
-------------------------------------------------------------------    
    cursor c_encontrar_ciclo (pcd_fecha date)is
    select id_ciclo
         from fa_ciclos_bscs
         where dia_ini_ciclo = substr(pcd_fecha, 1, 2);
 
----------------------------------------------------------------     
------------------------------------------------------------------    
   cursor c_encontrar_region is
   select distinct op.region from ope_cuadre_DWL op;
------------------------------------------------------------------  
 
  
   
    lregion                     c_encontrar_region%ROWTYPE;
    lfecha_ciclo                c_fecha_ciclo%ROWTYPE;
    lr_reg                      regi%ROWTYPE; --  variable del cursor
    lregi_pagos_exceso          regi_pagos_exceso%ROWTYPE; --  variable del cursor
    lregi_cartera_vencida       regi_cartera_vencida%ROWTYPE; --  variable del cursor
    lregi_total_cartera_vencida regi_total_cartera_vencida%ROWTYPE; --  variable del cursor
    lsecuencia_ciclo            NUMBER;
    
  
  
  
  
  begin
-----------------------LOL----------------------------------  
----------------INICIO PROCEDIMIENTO------------------------  
-----------------------LOL----------------------------------
 
------------------FECHA CICLO
   open c_fecha_ciclo;
    fetch c_fecha_ciclo
      into lfecha_ciclo;
    IF c_fecha_ciclo%FOUND THEN
      lb_fecha_ciclo := true;
    END IF;
    close c_fecha_ciclo;
--------------------------------   
 
--------------------REGION

   OPEN c_encontrar_region;
    FETCH c_encontrar_region
    into LV_REGION;
    IF c_encontrar_region%FOUND THEN
      lb_encontrar_region := true;
    END IF;
    close c_encontrar_region;
--------------------------------    
    
IF  lb_encontrar_region THEN 
 for r in c_encontrar_region loop   
   If lb_fecha_ciclo then
      for c in c_fecha_ciclo loop
        LD_FECHA_CICLO := c.lrstart;
        CLIENTES_TOTAL := 0;
    clientes       := 0;
    clientes_rv    := 0;
    clientes_v     := 0;
    clientes_pe    := 0;
  
    CARTERA_TOTAl := 0;
    cartera       := 0;
    cartera_rv    := 0;
    cartera_v     := 0;
    cartera_pe    := 0;
  
    PAGOS_TOTAL := 0;
    pagos       := 0;
    pagos_rv    := 0;
    pagos_v     := 0;
    pagos_pe    := 0;
  
    DEBITOS_TOTAL := 0;
    debitos       := 0;
    debitos_rv    := 0;
    debitos_v     := 0;
    debitos_pe    := 0;
  
    CREDITOS_TOTAL := 0;
    creditos       := 0;
    creditos_rv    := 0;
    creditos_v     := 0;
    creditos_pe    := 0;
  
    recuperacion_rv:=0;
    RECUPERACION:=0;
  

     
        open c_secuencia_ciclo;
        fetch c_secuencia_ciclo
          into lsecuencia_ciclo;
        close c_secuencia_ciclo;
      
        --Valida ciclo   
    sysadm.QC_TRACE_OPE_CUADRE('RP_QC_3',
                                   'QUERY_VALIDA_CICLO',
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                   null);
    open c_encontrar_ciclo(LD_FECHA_CICLO);
    fetch c_encontrar_ciclo
    into  lcidciclo;
    close c_encontrar_ciclo;
     
     sysadm.QC_TRACE_OPE_CUADRE('RP_QC_3',
                                   'QUERY_VALIDA_CICLO',
                                   null,
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
       
        ---------------------------------------------
        --  Verifica si existen datos en el cursor
        ---------------------------------------------
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_4',
                                   'QUERY_REGI',
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                   null);
        
        
        OPEN regi(LV_REGION);
        FETCH regi
          INTO lr_reg;
        IF regi%FOUND THEN
          lb_cursor := true;
        END IF;
        CLOSE regi;
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_4',
                                   'QUERY_REGI',
                                   null,
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
       
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_6',
                                   'QUERY_REGI_PAGOS_EXCESO',
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                   null);
        OPEN regi_pagos_exceso(LV_REGION);
        FETCH regi_pagos_exceso
          INTO lregi_pagos_exceso;
        IF regi_pagos_exceso%FOUND THEN
          lb_regi_pagos_exceso := true;
        END IF;
        CLOSE regi_pagos_exceso;
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_6',
                                   'QUERY_REGI_PAGOS_EXCESO',
                                   null,
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_7',
                                   'QUERY_REGI_CARTERA_VENCIDA',
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                   null);
        OPEN regi_cartera_vencida(lv_region);
        FETCH regi_cartera_vencida
          INTO lregi_cartera_vencida;
        IF regi_cartera_vencida%FOUND THEN
          lb_regi_cartera_vencida := true;
        END IF;
        CLOSE regi_cartera_vencida;
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_7',
                                   'QUERY_REGI_CARTERA_VENCIDA',
                                   null,
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_8',
                                   'QUERY_REGI_TOTAL_CARTERA_VENCIDA',
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                   null);
        OPEN regi_total_cartera_vencida(LV_REGION);
        FETCH regi_total_cartera_vencida
          INTO lregi_total_cartera_vencida;
        IF regi_total_cartera_vencida%FOUND THEN
          lb_regi_total_cartera_vencida := true;
        END IF;
        CLOSE regi_total_cartera_vencida;
        sysadm.QC_TRACE_OPE_CUADRE('RP_QC_8',
                                   'QUERY_REGI_TOTAL_CARTERA_VENCIDA',
                                   null,
                                   to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
        HORA := to_char(sysdate, 'HH24:MI');
      
        lv_error := 'Error prueba';
      
        SYSADM.COK_PROCESS_REPORT_DWL1.init_recuperado('ope_cuadre_DWL',
                                                      lv_error);
      
        SYSADM.COK_PROCESS_REPORT_DWL1.llena_pagorecuperado(to_char(LD_FECHA_CICLO,
                                                                   'dd/mm/yyyy'),
                                                           to_char(pd_fechafin,
                                                                   'dd/mm/yyyy'),
                                                           'ope_cuadre_DWL',
                                                           lv_error);
      
        SYSADM.COK_PROCESS_REPORT_DWL1.llena_creditorecuperado(to_char(LD_FECHA_CICLO,
                                                                      'dd/mm/yyyy'),
                                                              to_char(pd_fechafin,
                                                                      'dd/mm/yyyy'),
                                                              'ope_cuadre_DWL',
                                                              lv_error);
      
        SYSADM.COK_PROCESS_REPORT_DWL1.llena_cargorecuperado(to_char(LD_FECHA_CICLO,
                                                                    'dd/mm/yyyy'),
                                                            to_char(pd_fechafin,
                                                                    'dd/mm/yyyy'),
                                                            'ope_cuadre_DWL',
                                                            lv_error);
      
      
       
-- LOL  INI [9407] EXTRAE RESULTADOS CARTERA VIGENTE     
      If lb_cursor then
          for k in regi(lv_region) loop
            --
            clientes     := nvl(k.CLIENTES, 0);
            cartera      := nvl(k.CARTERA, 0);
            pagos        := nvl(k.PAGOS, 0);
            debitos      := nvl(k.NOTAS_DEBITOS, 0);
            creditos     := nvl(k.NOTAS_CREDITOS, 0);
            recuperacion := nvl(k.RECUPERACION, 0);
          
          end loop;
          INSERT INTO CVV_RESUMENCARTERA
          VALUES
            (lsecuencia_ciclo,
             TRUNC(SYSDATE) || '-' || 'VIGENTE',
             LD_FECHA_CICLO,
             pd_fechafin,
             LV_REGION,
             lcidciclo,
             'subtotal',
             clientes,
             cartera,
             pagos,
             debitos,
             creditos,
             RECUPERACION);
          commit;
        
        end if;
-- LOL  FIN [9407] EXTRAE RESULTADOS CARTERA VIGENTE        
      
        If lb_regi_cartera_vencida then
          for k in regi_cartera_vencida(lv_region) loop
            --
            IF K.RANGO >= 0 and K.RANGO <= 30 THEN
              cadena := 'VENCIM.  0  - 30';
            ELSIF K.RANGO >= 31 and K.RANGO <= 60 THEN
              cadena := 'VENCIM.  31 - 60';
            ELSIF K.RANGO >= 61 and K.RANGO <= 90 THEN
              cadena := 'VENCIM.  61 - 90';
            ELSIF K.RANGO >= 91 and K.RANGO <= 120 THEN
              cadena := 'VENCIM. 91 - 120';
            ELSIF K.RANGO >= 121 and K.RANGO <= 150 THEN
              cadena := 'VENCIM. 121 - 150';
            ELSIF K.RANGO >= 151 and K.RANGO <= 180 THEN
              cadena := 'VENCIM. 151 - 180';
            ELSIF K.RANGO >= 181 and K.RANGO <= 210 THEN
              cadena := 'VENCIM. 181 - 210';
            ELSIF K.RANGO >= 211 and K.RANGO <= 240 THEN
              cadena := 'VENCIM. 211 - 240';
            ELSIF K.RANGO >= 241 and K.RANGO <= 270 THEN
              cadena := 'VENCIM. 241 - 270';
            ELSIF K.RANGO >= 271 and K.RANGO <= 300 THEN
              cadena := 'VENCIM. 271 - 300';
            ELSIF K.RANGO >= 301 and K.RANGO <= 330 THEN
              cadena := 'VENCIM. 301 - 330';
            end if;
          
            clientes_rv     := nvl(k.CLIENTES, 0);
            cartera_rv      := nvl(k.CARTERA, 0);
            pagos_rv        := nvl(k.PAGOS, 0);
            debitos_rv      := nvl(k.NOTAS_DEBITOS, 0);
            creditos_rv     := nvl(k.NOTAS_CREDITOS, 0);
            RECUPERACION_rv := nvl(k.RECUPERACION, 0);
          
            INSERT INTO CVV_RESUMENCARTERA
            VALUES
              (lsecuencia_ciclo,
               TRUNC(SYSDATE) || '-' || 'VENCIDA',
               LD_FECHA_CICLO,
               pd_fechafin,
               LV_REGION,
               lcidciclo,
               cadena,
               clientes_rv,
               cartera_rv,
               pagos_rv,
               debitos_rv,
               creditos_rv,
               RECUPERACION_rv);
            commit;
          
          end loop;
        
          
        
          BEGIN
            LD_FECHA_GEN := SYSDATE;
          
            sysadm.QC_TRACE_OPE_CUADRE('RP_QC_16',
                                       'QUERY CONSULTA REGION',
                                       to_char(sysdate,
                                               'dd/mm/yyyy hh24:mi:ss'),
                                       null);
            select count(*)
              into ln_conta_bita
              from sysadm.provi_bita_cvv a
             where a.region = LV_REGION
               AND A.ID_CICLO = lcidciclo
               AND A.FECHA_INI = LD_FECHA_CICLO;
            
            sysadm.QC_TRACE_OPE_CUADRE('RP_QC_16',
                                       'QUERY CONSULTA REGION',
                                       null,
                                       to_char(sysdate,
                                               'dd/mm/yyyy hh24:mi:ss'));
          
            IF ln_conta_bita > 0 THEN
              sysadm.QC_TRACE_OPE_CUADRE('RP_QC_17',
                                         'ELIMINACION DE REGISTRO DE CAMPO region DE TABLA provi_bita_cvv',
                                         to_char(sysdate,
                                                 'dd/mm/yyyy hh24:mi:ss'),
                                         null);
              delete from sysadm.provi_bita_cvv a
               where a.region = LV_REGION
                 AND A.ID_CICLO = lcidciclo
                 AND A.FECHA_INI = LD_FECHA_CICLO;
              commit;
              sysadm.QC_TRACE_OPE_CUADRE('RP_QC_17',
                                         'ELIMINACION DE REGISTRO DE CAMPO region DE TABLA provi_bita_cvv',
                                         null,
                                         to_char(sysdate,
                                                 'dd/mm/yyyy hh24:mi:ss'));
            END IF;
          
            for h in registro_hist(LV_REGION, lcidciclo) loop
              INSERT INTO sysadm.PROVI_BITA_CVV
                (REGION,
                 ID_CICLO,
                 PRODUCTOS,
                 MORA,
                 TOTAL_DEUDA,
                 TOTPAGOS_RECUPERADO,
                 DEBITORECUPERADO,
                 CREDITORECUPERADO,
                 FECHA_INI,
                 FECHA_FIN,
                 FECHA_GEN,
                 USUARIO_GEN)
              
              values
                (h.LV_REGION,
                 h.lcidciclo,
                 h.producto,
                 h.rango,
                 h.cartera,
                 h.pagos,
                 h.notas_debitos,
                 h.notas_creditos,
                 LD_FECHA_CICLO,
                 pd_fechafin,
                 LD_FECHA_GEN,
                 USER);
            
            end loop;
          
          EXCEPTION
          
            WHEN OTHERS THEN
            
              dbms_output.put_line(sqlerrm);
            
          END;
        
        end if;
      
        If lb_regi_total_cartera_vencida then
          for k in regi_total_cartera_vencida(lv_region) loop
            clientes_v   := nvl(k.CLIENTES, 0);
            cartera_v    := nvl(k.CARTERA, 0);
            pagos_v      := nvl(k.PAGOS, 0);
            debitos_v    := nvl(k.NOTAS_DEBITOS, 0);
            creditos_v   := nvl(k.NOTAS_CREDITOS, 0);
            recuperacion := nvl(k.RECUPERACION, 0);
            ------LOL       
            INSERT INTO CVV_RESUMENCARTERA
            VALUES
              (lsecuencia_ciclo,
               TRUNC(SYSDATE) || '-' || 'VENCIDA',
               LD_FECHA_CICLO,
               pd_fechafin,
               LV_REGION,
               lcidciclo,
               'subtotxCob',
               clientes_v,
               cartera_v,
               pagos_v,
               debitos_v,
               creditos_v,
               RECUPERACION);
            commit;
            -------
          end loop;
        
        end if;
      
        If lb_regi_pagos_exceso then
          for k in regi_pagos_exceso(LV_REGION) loop
            clientes_pe  := nvl(k.CLIENTES, 0);
            cartera_pe   := nvl(k.CARTERA, 0);
            pagos_pe     := nvl(k.PAGOS, 0);
            debitos_pe   := nvl(k.NOTAS_DEBITOS, 0);
            creditos_pe  := nvl(k.NOTAS_CREDITOS, 0);
            recuperacion := nvl(k.RECUPERACION, 0);
          
          end loop;
          --LOL   
          INSERT INTO CVV_RESUMENCARTERA
          VALUES
            (lsecuencia_ciclo,
             TRUNC(SYSDATE) || '-' || 'VENCIDA',
             LD_FECHA_CICLO,
             pd_fechafin,
             LV_REGION,
             lcidciclo,
             'subtot. Fav',
             clientes_pe,
             cartera_pe,
             pagos_pe,
             debitos_pe,
             creditos_pe,
             RECUPERACION);
          commit;
          ------
        end if;
      
        CLIENTES_TOTAL := clientes + clientes_v + clientes_pe;
        CARTERA_TOTAl  := cartera + cartera_v + cartera_pe;
        PAGOS_TOTAL    := pagos + pagos_v + pagos_pe;
        DEBITOS_TOTAL  := debitos + debitos_v + debitos_pe;
        CREDITOS_TOTAL := creditos + creditos_v + creditos_pe;
      
        IF CARTERA_TOTAl = 0 THEN
          RECUPERACION := 0;
        ELSE
          RECUPERACION := (PAGOS_TOTAL / CARTERA_TOTAl) * 100;
        
        END IF;
      
        --LOL   
        INSERT INTO CVV_RESUMENCARTERA
        VALUES
          (lsecuencia_ciclo,
           TRUNC(SYSDATE) || '-' || 'VENCIDA',
           LD_FECHA_CICLO,
           pd_fechafin,
           LV_REGION,
           lcidciclo,
           'TOTAL CARTERA',
           round(CLIENTES_TOTAL, 2),
           round(CARTERA_TOTAl, 2),
           round(PAGOS_TOTAL, 2),
           round(DEBITOS_TOTAL, 2),
           round(CREDITOS_TOTAL, 2),
           round(RECUPERACION, 2));
        commit;
        ------
      end loop;
    
    end if;
end loop;
END IF; 
  EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line(sqlerrm);
  end reporte_formapago;
 

end CVV_PROCESS_REPORT_AUTO;
/
