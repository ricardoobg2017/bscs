CREATE OR REPLACE PACKAGE FIN_GESTION_SHORTCODE IS

TYPE R_REGISTROS IS RECORD(
      LINEA    VARCHAR2(2000));
  TYPE TR_REGISTROS IS TABLE OF R_REGISTROS INDEX BY BINARY_INTEGER;

  PROCEDURE P_CARGA_CONFIG_SHORTCODE(PN_SHORTCODE      NUMBER,
                                     Pv_Mensaje        VARCHAR2,
                                     Pv_StatusSusp     VARCHAR2,
                                     Pn_MontoMin       NUMBER,
                                     Pn_MontoMax       NUMBER,
                                     Pn_PorcPagos      NUMBER,
                                     Pv_Financiamiento VARCHAR2,
                                     Pv_Ciclo          VARCHAR2,
                                     Pn_IdMensaje      OUT NUMBER,
                                     Pv_Error          OUT VARCHAR2);

  PROCEDURE P_CARGA_DATOS_SHORTCODE(Pv_Error OUT VARCHAR2);

  PROCEDURE P_ENVIA_MSJ_SHORTCODE(PN_HILO NUMBER, Pv_Error OUT VARCHAR2);
  
  PROCEDURE P_CARGA_PAGOS(Pv_Error          OUT VARCHAR2);
  
  PROCEDURE P_CONSULTA_SALDO_CUOTAS(Pv_Telef              VARCHAR2,
                                    Pn_CodError       OUT NUMBER,
                                    Pv_Error          OUT VARCHAR2);
  
  PROCEDURE P_CONSULTA_SALDO(Pv_Telef              VARCHAR2,
                             Pn_CodError       OUT NUMBER,
                             Pv_Error          OUT VARCHAR2);
  
  PROCEDURE P_REPORTE_SHORTCODE_USSD (Pv_Error OUT VARCHAR2);
  
  PROCEDURE P_CARGA_ARCHIVO(Pn_IdMensaje      NUMBER,
                            Pn_Ussd           NUMBER,
                            Pv_Mensaje        VARCHAR2,
                            Pv_Separador      VARCHAR2,
                            Pt_Cadena         TR_REGISTROS,
                            Pv_Error      OUT VARCHAR2);
  
  PROCEDURE P_ACTUALIZA_ESTADO (Pv_Error      OUT VARCHAR2);
  
END FIN_GESTION_SHORTCODE;
/
CREATE OR REPLACE PACKAGE BODY FIN_GESTION_SHORTCODE IS
  /*******************************************************************************************************************************
  LIDER SIS  :        ANTONIO MAYORGA
  LIDER IRO  :        Juan Romero Aguilar 
  PROYECTO   :        [11477]Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  CREADO POR :        Yorvi Pozo 
  Created    :        22/08/2017 
  COMENTARIO  :       Creacion de procedimientos para el env�o de mensajer�a por Shortcode o USSD a clientes 
                      seg�n la validaci�n de los siguientes criterios:
                      � Porcentaje m�nimo de deuda.
                      � Estatus de l�nea (80, 34, 33 o todos).
                      � Tipo de cliente.
                      � Categorizaci�n del cliente.
                      � Monto m�nimo y m�ximo de deuda.
                      � Financiamiento.   
  *********************************************************************************************************************************/
  /*******************************************************************************************************************************
  LIDER SIS  :        ANTONIO MAYORGA
  LIDER IRO  :        Juan Romero Aguilar 
  PROYECTO   :        [11603]Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  MODIFICADO POR :    Iro Andres Balladares 
  FECHA      :        19/10/2017 
  COMENTARIO  :       Ajuste de logica de extraccion de cuentas.  
  *********************************************************************************************************************************/
  /*******************************************************************************************************************************
  LIDER SIS  :        ANTONIO MAYORGA
  LIDER IRO  :        Juan Romero Aguilar 
  PROYECTO   :        [11603]Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
  MODIFICADO POR :    Iro Andres Balladares 
  FECHA      :        04/12/2017 
  COMENTARIO  :       Adicion de proceso de procedimiento para reporteria
                      Adicion de parametro de porcentaje de pagos en varios procedimientos.
                      Ajuste en la logica de extraccion para cuentas con financiamiento.  
  *********************************************************************************************************************************/
/*******************************************************************************************************************************
  LIDER SIS  :        ANTONIO MAYORGA
  LIDER IRO  :        Hugo Chavez 
  PROYECTO   :        [12018] Equipo �gil Procesos Cobranzas
  MODIFICADO POR :    Iro Andr�s Balladares 
  FECHA      :        16/10/2018 
  COMENTARIO  :       Adicion de proceso de procedimiento para cargar cuentas por medio de un archivo plano.  
  *********************************************************************************************************************************/
/*******************************************************************************************************************************
LIDER SIS  :        ANTONIO MAYORGA
LIDER HTS  :        Gloria Rojas
PROYECTO   :        [12392] Legados Cobranzas DBC y Reloj de Cob
MODIFICADO POR :    HTS Andres Balladares 
FECHA      :        31/07/2019
COMENTARIO :        Modificacion para restringir el tiempo de acceso al USSD una vez enviado el SMS al cliente.  
*********************************************************************************************************************************/
  --Procedimiento para realizar la insercion de la configuracion de un nuevo shortcode o USSD
  PROCEDURE P_CARGA_CONFIG_SHORTCODE(PN_SHORTCODE      NUMBER,
                                     Pv_Mensaje        VARCHAR2,
                                     Pv_StatusSusp     VARCHAR2,
                                     Pn_MontoMin       NUMBER,
                                     Pn_MontoMax       NUMBER,
                                     Pn_PorcPagos      NUMBER,
                                     Pv_Financiamiento VARCHAR2,
                                     Pv_Ciclo          VARCHAR2,
                                     Pn_IdMensaje      OUT NUMBER,
                                     Pv_Error          OUT VARCHAR2) IS
  
    Ln_IdMensaje NUMBER;
  
  BEGIN
  
    Ln_IdMensaje := SEQ_CODIGO_SHORTCODE.NEXTVAL;
  
    INSERT INTO FN_CONF_CAB_SHORTCODE
      (ID_MENSAJE,
       SHORTCODE,
       MENSAJE,
       STATUS_SUSP,
       MONTO_MINIMO,
       MONTO_MAXIMO,
       PORCENTAJE_PAGO,
       FINANCIAMIENTO,
       CICLO,
       ESTADO,
       USUARIO_INGRESO,
       FECHA_INGRESO)
    VALUES
      (Ln_IdMensaje,
       PN_SHORTCODE,
       Pv_Mensaje,
       Pv_StatusSusp,
       Pn_MontoMin,
       Pn_MontoMax,
       Pn_PorcPagos,
       Pv_Financiamiento,
       Pv_Ciclo,
       'A',
       USER,
       SYSDATE);
  
    Pn_IdMensaje := Ln_IdMensaje;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      Pn_IdMensaje := 0;
      Pv_Error     := 'P_CARGA_CONFIG_SHORTCODE -> ' ||
                      SUBSTR(SQLERRM, 1, 300);
  END P_CARGA_CONFIG_SHORTCODE;
  ----------------------------------------------------------------------------------------------------
  ----------------------------------------------------------------------------------------------------
  --Procedimiento para realizar la carga de las cuentas que cumplen con los criterios configurados.
  PROCEDURE P_CARGA_DATOS_SHORTCODE(PV_ERROR OUT VARCHAR2) IS
  
    ------------------------------- Cursores---------------------------------
    --Cursor para obtener la configuraci�n global del shortcode o USSD
    CURSOR C_ID_ENVIO IS
      SELECT C.ID_MENSAJE, C.SHORTCODE, C.MENSAJE, C.STATUS_SUSP,
             C.MONTO_MINIMO, C.MONTO_MAXIMO, C.PORCENTAJE_PAGO,
             C.FINANCIAMIENTO, C.CICLO
        FROM FN_CONF_CAB_SHORTCODE C
       WHERE C.ESTADO = 'A';
  
    -- Cursor para obtener el detalle de la configuraci�n de un shortcode o USSD.   
    CURSOR C_DETALLE(CV_IDMENSAJE NUMBER, CV_TIPO VARCHAR2) IS
      SELECT D.CODIGO
        FROM FN_CONF_DETALLE_SHORTCODE D
       WHERE D.ID_MENSAJE = CV_IDMENSAJE
         AND D.TIPO = CV_TIPO;
    
    -- Cursor para obtener la fecha de planificaci�n y el ciclo de un cliente.
    CURSOR C_FECHA_SUSPENSION(CN_CUSTOMER_ID NUMBER) IS
      SELECT TRUNC(A.INSERTIONDATE) FECHA_INGRESO, A.ADMINCYCLE
        FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS A
       WHERE A.CUSTOMER_ID = CN_CUSTOMER_ID
         AND A.CO_STATUS_A IN ('34', '80', '33')
         AND ROWNUM <= 1;
    LC_FECHA_SUSPENSION C_FECHA_SUSPENSION%ROWTYPE;
    LN_FECHA_SUSPENSION NUMBER := 0;
  
    -- Cursor para obtener la �ltima factura, el saldo y la fecha m�xima de pago de un cliente.
    CURSOR C_SALDO_CLIENTE(CN_CUSTOMER_ID NUMBER) IS
    SELECT SUM(C.OHOPNAMT_DOC) SALDO,
           MAX(C.OHDUEDATE) FECHA_MAX_PAGO
      FROM ORDERHDR_ALL C
     WHERE C.CUSTOMER_ID = CN_CUSTOMER_ID;
    LC_SALDO_CLIENTE C_SALDO_CLIENTE%ROWTYPE;
    
    CURSOR C_SERVICES_CAP (Cn_DnId NUMBER)IS
    SELECT B.CO_ID
      FROM CONTR_SERVICES_CAP B 
     WHERE B.DN_ID = Cn_DnId
       AND B.CS_DEACTIV_DATE IS NULL;
    
    CURSOR C_CUENTA (Cn_CoId  NUMBER)IS
    SELECT C.CUSTOMER_ID, D.CUSTCODE
      FROM CONTRACT_ALL C, CUSTOMER_ALL D
     WHERE C.CO_ID = Cn_CoId
       AND D.CUSTOMER_ID = C.CUSTOMER_ID;
    
    CURSOR C_STATUS(Cn_CoId  NUMBER)IS
    SELECT X.CH_STATUS
      FROM CONTRACT_HISTORY X
     WHERE X.CO_ID = Cn_CoId
       AND X.CH_SEQNO IN (SELECT MAX(Y.CH_SEQNO)
                            FROM CONTRACT_HISTORY Y
                           WHERE Y.CO_ID = Cn_CoId);
    
    Lc_ServicesCap C_SERVICES_CAP%ROWTYPE;
    Lc_Cuenta      C_CUENTA%ROWTYPE;
    
    TYPE TNDNID IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVDNNUM IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
    LT_DnId               TNDNID;
    LT_DnNum              TVDNNUM;
    
   ------------------------------------------------------------------------------------
   ----------------------------- Types -------------------------------------------------
    TYPE C_OBTIENE_BASE_CLIENTES IS REF CURSOR;
    TYPE TVSNCODE IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    OBTIENE_BASE_CLIENTES C_OBTIENE_BASE_CLIENTES;
    LT_SNCODE             TVSNCODE;
  
    -- Type principal
    TYPE TYP_RC_OPE_SHORTCODE IS RECORD(
      ID_MENSAJE          NUMBER,
      MENSAJE             VARCHAR2(160),
      CICLO               VARCHAR(3),
      CUSTOMER_ID         NUMBER,
      CUSTCODE            VARCHAR2(30),
      CO_ID               NUMBER,
      ID_SERVICIO         VARCHAR2(20),
      FINANCIAMIENTO      VARCHAR2(3),
      DEUDA_TOTAL         NUMBER,
      RESPUESTA           VARCHAR2(3000),
      ID_NEGOCIACION      NUMBER,
      FECHA_MAX_PAGO      DATE,
      FECHA_RESPUESTA     DATE,
      USUARIO_INGRESO     VARCHAR2(20),
      FECHA_INGRESO       DATE,
      FECHA_ACTUALIZACION DATE,
      ESTADO              VARCHAR2(3),
      OBSERVACION         VARCHAR2(50),
      SHORTCODE           NUMBER,
      PAGOS               NUMBER);
  
    TYPE TYPTR_RC_OPE_SHORTCODE IS TABLE OF TYP_RC_OPE_SHORTCODE INDEX BY BINARY_INTEGER;
    LT_TABLA_RC_OPE_SHORTCODE TYPTR_RC_OPE_SHORTCODE;
    TYPE RTYP_RC_OPE_SHORTCODE IS RECORD(
      T_CUSTOMER_ID NUMBER,
      T_CUSTCODE    VARCHAR2(30),
      T_CO_ID       NUMBER,
      T_DN_NUM      VARCHAR2(25),
      T_PAGO        NUMBER);
    TYPE TYPTB_RC_OPE_SHORTCODE IS TABLE OF RTYP_RC_OPE_SHORTCODE INDEX BY BINARY_INTEGER;
    LT_RC_OPE_SHORTCODE TYPTB_RC_OPE_SHORTCODE;
    -----------------------------------------------------------------------------------------
    --------------------------- Variables ---------------------------------------------------
    LV_SQLCARGA       VARCHAR2(4000);
    LV_SQLTIPO        VARCHAR2(1000);
    LV_SQLCATE        VARCHAR2(1000);
    LV_SQLSNCODE      VARCHAR2(1000);
    LN_COUNT          NUMBER;
    LV_FINANMANUAL    VARCHAR2(200);
    LV_FINANAUTO      VARCHAR2(200);
    LV_SQLFINANMANUAL VARCHAR2(4000);
    LV_SQLFINAN       LONG;
    LV_MES            VARCHAR2(3);
    LV_ANNIO          VARCHAR2(6);
    LV_CORTE          VARCHAR2(3);
    LV_FECHA_CORTE    VARCHAR2(15);
    LN_FINAN          NUMBER;
    LV_FINAN          VARCHAR2(3);
    LN_COUNT_TYPE     NUMBER := 0;
    LN_CUSTOMER_ID    NUMBER := 0;
    --LN_DEUDA          NUMBER := 0;
    LN_SALDO          NUMBER := 0;
    LD_FECHA_MAX_PAGO DATE;
    --LN_SALDO_MIN      NUMBER := 0;
    --LN_SALDO_MAX      NUMBER := 0;
    --Lv_SqlPagos       VARCHAR2(1000);
    --Ln_Pagos          NUMBER;
    Ln_PorcePago      NUMBER;
    Ln_Existe         NUMBER:=0;
    Ln_IdMensaje      NUMBER;
    Ln_ShortCode      NUMBER;
    Lv_Mensaje        VARCHAR2(1000);
    Lv_NumerosConf    VARCHAR2(1000);
    Lv_SqlUsuarios    VARCHAR2(10000);
    Lv_BanderaUsu     VARCHAR2(10);
    Le_Error          EXCEPTION;
  -------------------------------------------------------------------------------------------  
    Lv_Status         VARCHAR2(24);
    
  BEGIN
  
    LV_FINANAUTO   := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                           PV_ID_PARAMETRO      => 'OPE_FINAN_AUTOMATICO');
    LV_FINANMANUAL := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                           PV_ID_PARAMETRO      => 'OPE_FINAN_MANUAL');
    
    Lv_BanderaUsu := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11603,
                                                                          PV_ID_PARAMETRO      => 'USSD_BAND_USU');
    
    LV_SQLSNCODE := 'SELECT SNCODE
                    FROM CO_MAPEO_FINAN_CARGOS@AXIS
                   WHERE ID_TRANSACCION IN (<FINAN_AUTOMATICO>)
                     AND NUMERO_CUOTA >= 1';
    
    LV_SQLSNCODE := REPLACE(LV_SQLSNCODE,'<FINAN_AUTOMATICO>',LV_FINANAUTO);
    EXECUTE IMMEDIATE LV_SQLSNCODE BULK COLLECT INTO LT_SNCODE;
    IF LT_SNCODE.COUNT > 0 THEN
      
      FOR I IN LT_SNCODE.FIRST .. LT_SNCODE.LAST LOOP
        LV_SQLFINANMANUAL := LV_SQLFINANMANUAL || LT_SNCODE(I) || ',';
      END LOOP;
      
      IF LV_SQLFINANMANUAL IS NOT NULL THEN
        LV_SQLFINANMANUAL := SUBSTR(LV_SQLFINANMANUAL, 1, LENGTH(LV_SQLFINANMANUAL) - 1);
      END IF;
      
    END IF;
    
    P_CARGA_PAGOS(Pv_Error  => Pv_Error);
    
    IF Pv_Error IS NOT NULL THEN
       RAISE Le_Error;
    END IF;
    
    FOR I IN C_ID_ENVIO LOOP
      BEGIN
        
        LV_SQLCARGA := 'SELECT A.CUSTOMER_ID, A.CUSTCODE, A.CO_ID, A.DN_NUM, X.CACURAMT_PAY PAGO
                          FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS A, CUSTOMER_ALL B, SYSADM.FN_CARGA_PAGOS X
                         WHERE A.CH_TIMEACCIONDATE BETWEEN SYSDATE - 120 AND SYSDATE + 120
                           AND A.CO_STATUSTRX IN (''P'',''F'',''E'',''V'',''T'',''L'')
                           AND A.CO_STATUS_A IN (<STATUS_SUSP>)
                           AND A.ADMINCYCLE IN (<CICLO>)
                           AND X.CUSTOMER_ID = A.CUSTOMER_ID
                           AND B.CUSTOMER_ID = A.CUSTOMER_ID
                           AND B.CUSTOMER_ID = X.CUSTOMER_ID ';
        
        LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<STATUS_SUSP>', I.STATUS_SUSP);
        LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<CICLO>', I.CICLO);
        
        LN_COUNT   := 0;
        LV_SQLTIPO := NULL;
        LV_SQLCATE := NULL;
        
        FOR J IN C_DETALLE(I.ID_MENSAJE, 'T') LOOP
          IF J.CODIGO IS NOT NULL THEN
            LN_COUNT := LN_COUNT + 1;
            IF LN_COUNT = 1 THEN
              LV_SQLTIPO := 'AND B.PRGCODE IN (' || '''' || J.CODIGO || '''' || ',';
            ELSE
              LV_SQLTIPO := LV_SQLTIPO || '''' || J.CODIGO || '''' || ',';
            END IF;
          END IF;
        END LOOP;
        
        IF LV_SQLTIPO IS NOT NULL THEN
          LV_SQLTIPO  := SUBSTR(LV_SQLTIPO, 1, LENGTH(LV_SQLTIPO) - 1);
          LV_SQLTIPO  := LV_SQLTIPO || ') ';
          LV_SQLCARGA := LV_SQLCARGA || LV_SQLTIPO;
          LN_COUNT    := 0;
        END IF;
        
        FOR K IN C_DETALLE(I.ID_MENSAJE, 'C') LOOP
          IF K.CODIGO IS NOT NULL THEN
            LN_COUNT := LN_COUNT + 1;
            IF LN_COUNT = 1 THEN
              LV_SQLCATE := 'AND B.CSTRADECODE IN (' || '''' || K.CODIGO || '''' || ',';
            ELSE
              LV_SQLCATE := LV_SQLCATE || '''' || K.CODIGO || '''' || ',';
            END IF;
          END IF;
        END LOOP;
        
        IF LV_SQLCATE IS NOT NULL THEN
          LV_SQLCATE  := SUBSTR(LV_SQLCATE, 1, LENGTH(LV_SQLCATE) - 1);
          LV_SQLCATE  := LV_SQLCATE || ') ';
          LV_SQLCARGA := LV_SQLCARGA || LV_SQLCATE;
        END IF;
        LV_SQLCARGA := LV_SQLCARGA ||'  GROUP BY A.CUSTOMER_ID, A.CUSTCODE, A.CO_ID, A.DN_NUM, X.CACURAMT_PAY ORDER BY A.CUSTOMER_ID';
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      
      BEGIN
        OPEN OBTIENE_BASE_CLIENTES FOR LV_SQLCARGA;
        LOOP
          FETCH OBTIENE_BASE_CLIENTES BULK COLLECT
            INTO LT_RC_OPE_SHORTCODE LIMIT 1000;
          EXIT WHEN LT_RC_OPE_SHORTCODE.COUNT = 0;
          --LN_DEUDA       := NULL;
          LN_CUSTOMER_ID := 0;
          FOR J IN LT_RC_OPE_SHORTCODE.FIRST .. LT_RC_OPE_SHORTCODE.LAST LOOP
              LV_MES         := NULL;
              LV_CORTE       := NULL;
              LV_ANNIO       := NULL;
              LV_FECHA_CORTE := NULL;
              LV_SQLFINAN    := NULL;
              LN_FINAN       := NULL;
              LN_SALDO       := 0;
              Ln_PorcePago   :=0;
              
              OPEN C_STATUS(LT_RC_OPE_SHORTCODE(J).T_CO_ID);
              FETCH C_STATUS INTO Lv_Status;
              CLOSE C_STATUS;
              
              IF Lv_Status = 's' THEN
              IF LN_CUSTOMER_ID <> LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID THEN
                 LC_SALDO_CLIENTE := NULL;
                 LV_FINAN       := NULL;
                 
                 OPEN C_SALDO_CLIENTE(LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID);
                 FETCH C_SALDO_CLIENTE INTO LC_SALDO_CLIENTE;
                 CLOSE C_SALDO_CLIENTE;
                 
                 LN_SALDO          := LC_SALDO_CLIENTE.SALDO;
                 LN_SALDO          := LN_SALDO + LT_RC_OPE_SHORTCODE(J).T_PAGO ;
                 LD_FECHA_MAX_PAGO := LC_SALDO_CLIENTE.FECHA_MAX_PAGO;
                 
                 IF LN_SALDO > 0 THEN
                    Ln_PorcePago := (LT_RC_OPE_SHORTCODE(J).T_PAGO/LN_SALDO)*100;
                 END IF;
                 
                 OPEN C_FECHA_SUSPENSION(LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID);
                 FETCH C_FECHA_SUSPENSION INTO LC_FECHA_SUSPENSION;
                 LN_FECHA_SUSPENSION := C_FECHA_SUSPENSION%ROWCOUNT;
                 CLOSE C_FECHA_SUSPENSION;
                 
              END IF;
              
              IF I.FINANCIAMIENTO <> 'T' THEN
                 IF LN_CUSTOMER_ID <> LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID THEN
                    IF LN_FECHA_SUSPENSION > 0 THEN
                       LV_MES   := SUBSTR(TO_CHAR(LC_FECHA_SUSPENSION.FECHA_INGRESO,'DD/MM/RRRR'),4,2);
                       LV_ANNIO := SUBSTR(TO_CHAR(LC_FECHA_SUSPENSION.FECHA_INGRESO,'DD/MM/RRRR'),7,4);
                       IF LC_FECHA_SUSPENSION.ADMINCYCLE = '01' THEN
                          LV_CORTE := '24';
                          LV_MES   := SUBSTR(TO_CHAR(ADD_MONTHS(LC_FECHA_SUSPENSION.FECHA_INGRESO, -1),'DD/MM/RRRR'),4,2);
                          LV_ANNIO := SUBSTR(TO_CHAR(ADD_MONTHS(LC_FECHA_SUSPENSION.FECHA_INGRESO, -1),'DD/MM/RRRR'),7,4);
                       ELSIF LC_FECHA_SUSPENSION.ADMINCYCLE = '02' THEN
                          LV_CORTE := '08';
                       ELSIF LC_FECHA_SUSPENSION.ADMINCYCLE = '03' THEN
                          LV_CORTE := '15';
                       ELSIF LC_FECHA_SUSPENSION.ADMINCYCLE = '04' THEN
                          LV_CORTE := '02';
                       END IF;
                       LV_FECHA_CORTE := LV_CORTE || LV_MES || LV_ANNIO;
                       LV_SQLFINAN := 'SELECT COUNT(1) 
                                    FROM sysadm.CO_FACT_<FECHA> A
                                    WHERE A.TIPO = ''005 - CARGOS''
                                    AND (A.SERVICIO IN (SELECT to_char(SNCODE) FROM MPUSNTAB WHERE SNCODE IN (<FINAN_AUTOMATICO>))
                                         OR A.SERVICIO IN (''<FINAN_MANUAL>''))AND A.customer_id = <CUSTOMER_ID>';
                       
                       LV_SQLFINAN    := REPLACE(LV_SQLFINAN,'<FECHA>',LV_FECHA_CORTE);
                       LV_SQLFINAN    := REPLACE(LV_SQLFINAN,'<FINAN_AUTOMATICO>',LV_SQLFINANMANUAL);
                       LV_SQLFINAN    := REPLACE(LV_SQLFINAN,'<FINAN_MANUAL>',LV_FINANMANUAL);
                       LV_SQLFINAN    := REPLACE(LV_SQLFINAN,'<CUSTOMER_ID>',LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID);
                       
                       EXECUTE IMMEDIATE LV_SQLFINAN INTO LN_FINAN;
                       
                       IF LN_FINAN > 0 THEN
                          LV_FINAN := 'S';
                       ELSE
                          LV_FINAN := 'N';
                       END IF;
                    END IF;
                 END IF;
                  
               IF LN_SALDO > I.MONTO_MINIMO AND LN_SALDO < I.MONTO_MAXIMO AND LN_FECHA_SUSPENSION > 0 
                  AND I.FINANCIAMIENTO = LV_FINAN AND Ln_PorcePago >= I.PORCENTAJE_PAGO AND LC_SALDO_CLIENTE.SALDO > 0 THEN
                  
                  Ln_Existe := Ln_Existe +1;
                  Ln_IdMensaje := I.ID_MENSAJE;
                  Ln_ShortCode := I.SHORTCODE;
                  Lv_Mensaje   := I.MENSAJE;
                  
                  LN_COUNT_TYPE := LN_COUNT_TYPE + 1;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ID_MENSAJE := I.ID_MENSAJE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).MENSAJE := I.MENSAJE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CICLO := LC_FECHA_SUSPENSION.ADMINCYCLE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CUSTOMER_ID := LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CUSTCODE := LT_RC_OPE_SHORTCODE(J).T_CUSTCODE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CO_ID := LT_RC_OPE_SHORTCODE(J).T_CO_ID;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ID_SERVICIO := LT_RC_OPE_SHORTCODE(J).T_DN_NUM;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FINANCIAMIENTO := LV_FINAN;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).DEUDA_TOTAL := LN_SALDO;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FECHA_MAX_PAGO := LD_FECHA_MAX_PAGO;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).USUARIO_INGRESO := USER;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FECHA_INGRESO := SYSDATE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ESTADO := 'I';
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).SHORTCODE := I.SHORTCODE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).PAGOS := LT_RC_OPE_SHORTCODE(J).T_PAGO;
               END IF;
              ELSE
               IF LN_SALDO > I.MONTO_MINIMO AND LN_SALDO < I.MONTO_MAXIMO AND LN_FECHA_SUSPENSION > 0 
                  AND Ln_PorcePago >= I.PORCENTAJE_PAGO AND LC_SALDO_CLIENTE.SALDO > 0 THEN
                  
                  Ln_Existe := Ln_Existe +1;
                  Ln_IdMensaje := I.ID_MENSAJE;
                  Ln_ShortCode := I.SHORTCODE;
                  Lv_Mensaje   := I.MENSAJE;
                  
                  LN_COUNT_TYPE := LN_COUNT_TYPE + 1;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ID_MENSAJE := I.ID_MENSAJE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).MENSAJE := I.MENSAJE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CICLO := LC_FECHA_SUSPENSION.ADMINCYCLE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CUSTOMER_ID := LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CUSTCODE := LT_RC_OPE_SHORTCODE(J).T_CUSTCODE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).CO_ID := LT_RC_OPE_SHORTCODE(J).T_CO_ID;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ID_SERVICIO := LT_RC_OPE_SHORTCODE(J).T_DN_NUM;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FINANCIAMIENTO := LV_FINAN;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).DEUDA_TOTAL := LN_SALDO;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FECHA_MAX_PAGO := LD_FECHA_MAX_PAGO;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).USUARIO_INGRESO := USER;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).FECHA_INGRESO := SYSDATE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).ESTADO := 'I';
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).SHORTCODE := I.SHORTCODE;
                  LT_TABLA_RC_OPE_SHORTCODE(LN_COUNT_TYPE).PAGOS := LT_RC_OPE_SHORTCODE(J).T_PAGO;
               END IF;
              END IF;
              END IF;
              LN_CUSTOMER_ID := LT_RC_OPE_SHORTCODE(J).T_CUSTOMER_ID;
          END LOOP;
          
          LN_COUNT_TYPE := 0;
          IF LT_TABLA_RC_OPE_SHORTCODE.COUNT > 0 THEN
            FORALL K IN LT_TABLA_RC_OPE_SHORTCODE.FIRST .. LT_TABLA_RC_OPE_SHORTCODE.LAST
            
              INSERT INTO FN_GESTION_SHORTCODE(ID_MENSAJE,SHORTCODE,MENSAJE,CICLO,CUSTOMER_ID,CUSTCODE,CO_ID,ID_SERVICIO,FINANCIAMIENTO,
              DEUDA_TOTAL,PAGO,FECHA_MAX_PAGO,ESTADO,FECHA_INGRESO,USUARIO_INGRESO)
              VALUES
                (LT_TABLA_RC_OPE_SHORTCODE(K).ID_MENSAJE,
                 LT_TABLA_RC_OPE_SHORTCODE(K).SHORTCODE,
                 LT_TABLA_RC_OPE_SHORTCODE(K).MENSAJE,
                 LT_TABLA_RC_OPE_SHORTCODE(K).CICLO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).CUSTOMER_ID,
                 LT_TABLA_RC_OPE_SHORTCODE(K).CUSTCODE,
                 LT_TABLA_RC_OPE_SHORTCODE(K).CO_ID,
                 LT_TABLA_RC_OPE_SHORTCODE(K).ID_SERVICIO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).FINANCIAMIENTO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).DEUDA_TOTAL,
                 LT_TABLA_RC_OPE_SHORTCODE(K).PAGOS,
                 LT_TABLA_RC_OPE_SHORTCODE(K).FECHA_MAX_PAGO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).ESTADO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).FECHA_INGRESO,
                 LT_TABLA_RC_OPE_SHORTCODE(K).USUARIO_INGRESO);
            COMMIT;
          END IF;
          LT_TABLA_RC_OPE_SHORTCODE.DELETE;
        END LOOP;
        CLOSE OBTIENE_BASE_CLIENTES;
      EXCEPTION
        WHEN OTHERS THEN
          ---------  Cerrar cursores  ------------
          IF OBTIENE_BASE_CLIENTES%ISOPEN THEN
            CLOSE OBTIENE_BASE_CLIENTES;
          END IF;
          IF C_SALDO_CLIENTE%ISOPEN THEN
            CLOSE C_SALDO_CLIENTE;
          END IF;
          IF C_FECHA_SUSPENSION%ISOPEN THEN
            CLOSE C_FECHA_SUSPENSION;
          END IF;                    
          ----------------------------------------
          PV_ERROR := PV_ERROR ||'NO SE PUDO CARGAR EL ID MENSAJE DEBIDO AL SIGUIENTE ERROR '||SUBSTR(SQLERRM, 1, 200);       
      END;
    END LOOP;
    COMMIT;
    
    IF PV_ERROR IS NOT NULL THEN
       RAISE Le_Error;
    END IF;
    
    IF Ln_Existe > 0 AND NVL(Lv_BanderaUsu,'N') = 'S' THEN
       Lv_NumerosConf := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_NUM_USU',Pv_Error);
  
       IF Pv_Error IS NOT NULL OR Lv_NumerosConf IS NULL THEN
          Lv_NumerosConf := NVL(Lv_NumerosConf,'''593997199521'',''593986639517''');
          Pv_Error := NULL;
       END IF;
       
       Lv_SqlUsuarios := 'SELECT A.DN_ID, A.DN_NUM FROM DIRECTORY_NUMBER A WHERE A.DN_NUM IN (<NUMEROS>) AND A.DN_STATUS = ''a''';
       Lv_SqlUsuarios := REPLACE(Lv_SqlUsuarios,'<NUMEROS>',Lv_NumerosConf);
       
       EXECUTE IMMEDIATE Lv_SqlUsuarios BULK COLLECT INTO LT_DnId, LT_DnNum;
       
       FOR X IN LT_DnId.FIRST .. LT_DnId.LAST LOOP
           
           OPEN C_SERVICES_CAP(LT_DnId(X));
           FETCH C_SERVICES_CAP INTO Lc_ServicesCap;
           CLOSE C_SERVICES_CAP;
           
           OPEN C_CUENTA(Lc_ServicesCap.CO_ID);
           FETCH C_CUENTA INTO Lc_Cuenta;
           CLOSE C_CUENTA;
           
           IF Lc_ServicesCap.CO_ID IS NOT NULL AND Lc_Cuenta.CUSTOMER_ID IS NOT NULL AND Lc_Cuenta.CUSTCODE IS NOT NULL THEN
              INSERT INTO FN_GESTION_SHORTCODE(ID_MENSAJE, SHORTCODE, MENSAJE, CUSTOMER_ID, CUSTCODE,
                                               CO_ID, ID_SERVICIO, ESTADO,FECHA_INGRESO,USUARIO_INGRESO)
                                        VALUES(Ln_IdMensaje, Ln_ShortCode, Lv_Mensaje, Lc_Cuenta.CUSTOMER_ID, Lc_Cuenta.CUSTCODE,
                                               Lc_ServicesCap.CO_ID, LT_DnNum(x), 'I', SYSDATE, USER);
           END IF;
           
       END LOOP;
       COMMIT;
    END IF;
    
  EXCEPTION
    WHEN Le_Error THEN
      PV_ERROR := 'P_CARGA_DATOS_SHORTCODE -> ' || PV_ERROR;
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'P_CARGA_DATOS_SHORTCODE -> ' || PV_ERROR ||' - '||SUBSTR(SQLERRM, 1, 300);
  END P_CARGA_DATOS_SHORTCODE;
  -------------------------------------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------------------------------------
  -- Procedimiento para el envio de sms para cada registro de la tabla de trabajo
  PROCEDURE P_ENVIA_MSJ_SHORTCODE(PN_HILO NUMBER, Pv_Error OUT VARCHAR2) IS
  
    -- Cursor para obtener la informaci�n de la tabla de trabajo
    cursor C_OPE_SHORCODE(CN_HILO NUMBER, CN_MOD NUMBER) is
      select ID_SERVICIO,
             CUSTOMER_ID,
             CUSTCODE,
             ID_MENSAJE,
             MENSAJE,
             SHORTCODE,
             ESTADO,
             FECHA_INGRESO,
             OBSERVACION
        from FN_GESTION_SHORTCODE k
       where k.estado in ('I', 'E')
         and mod(substr(K.CUSTOMER_ID, 3), CN_MOD) = CN_HILO;
  
    -- Variables
    LN_MOD_HILO   VARCHAR2(20);
    ID_SERVICIO   VARCHAR2(20);
    LV_ASUNTO     VARCHAR2(50);
    LV_ERROR      VARCHAR2(4000);
    Lv_resulado   number;
    LV_SATELITE   varchar2(100);
    LV_TIPO       varchar2(5);
    LV_PUERTO     varchar2(20);
    LV_HOST       varchar2(50);
    Ln_secuencia  number;
    Lv_Bandera    VARCHAR2(100);
    Le_Error      EXCEPTION;
    
    -- Types
    TYPE TB_FN_GESTION_SHORECODE IS RECORD(
      ID_SERVICIO   VARCHAR2(20),
      CUSTOMER_ID   NUMBER,
      CUSTCODE      VARCHAR2(30),
      ID_MENSAJE    NUMBER,
      MENSAJE       VARCHAR2(160),
      SHORTCODE     NUMBER,
      ESTADO        VARCHAR2(3),
      FECHA_INGRESO DATE,
      OBSERVACION   VARCHAR2(3000));
        
    TYPE TY_FN_GESTION_SHORTCODE IS TABLE OF TB_FN_GESTION_SHORECODE INDEX BY BINARY_INTEGER; 
    LT_RC_OPE_SHORTCODE TY_FN_GESTION_SHORTCODE;
  
  BEGIN
    LV_SATELITE := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_COB_SATELITE');
    LN_MOD_HILO := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_NUM_HILO');
    LV_ASUNTO   := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_COB_ASUNTO');
    LV_TIPO     := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_COB_TIPO');
    LV_PUERTO   := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_COB_PUERTO');
    LV_HOST     := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11477,
                                                                        'SHORTCODE_COB_HOST');
    OPEN C_OPE_SHORCODE(PN_HILO, LN_MOD_HILO);
    LOOP
      FETCH C_OPE_SHORCODE BULK COLLECT
        INTO LT_RC_OPE_SHORTCODE LIMIT 1000;
      EXIT WHEN LT_RC_OPE_SHORTCODE.COUNT = 0;
    
      FOR I IN LT_RC_OPE_SHORTCODE.FIRST .. LT_RC_OPE_SHORTCODE.LAST LOOP
        ID_SERVICIO := SUBSTR(LT_RC_OPE_SHORTCODE(I).id_servicio, 5);
        Lv_resulado := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => LV_SATELITE,
                                                                 pd_fechaenvio      => TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY')||' '||'10:01:06', 'DD/MM/YYYY HH24:MI:SS'),
                                                                 pd_fechaexpiracion => sysdate + 1,
                                                                 pv_asunto          => LV_ASUNTO,
                                                                 pv_mensaje         => LT_RC_OPE_SHORTCODE(I).MENSAJE,
                                                                 pv_destinatario    => LT_RC_OPE_SHORTCODE(I).ID_SERVICIO,
                                                                 pv_remitente       => LT_RC_OPE_SHORTCODE(I).SHORTCODE,
                                                                 pv_tiporegistro    => LV_TIPO,
                                                                 pv_clase           => 'SCC',
                                                                 pv_puerto          => LV_PUERTO,
                                                                 pv_servidor        => LV_HOST,
                                                                 pv_maxintentos     => 5,
                                                                 pv_idusuario       => 'GSIOPER',
                                                                 pv_mensajeretorno  => LV_ERROR,
                                                                 pn_idsecuencia     => Ln_secuencia);
      
        IF LV_ERROR IS NOT NULL THEN
          LT_RC_OPE_SHORTCODE(I).ESTADO := 'E';
          LT_RC_OPE_SHORTCODE(I).OBSERVACION := LV_ERROR;
        ELSE
          LT_RC_OPE_SHORTCODE(I).ESTADO := 'P';
          LT_RC_OPE_SHORTCODE(I).OBSERVACION := 'OK';
        END IF;
      END LOOP;
      IF LT_RC_OPE_SHORTCODE.COUNT > 0 THEN
        FORALL A IN LT_RC_OPE_SHORTCODE.FIRST .. LT_RC_OPE_SHORTCODE.LAST
          UPDATE FN_GESTION_SHORTCODE B
             SET B.OBSERVACION         = LT_RC_OPE_SHORTCODE(A).OBSERVACION,
                 B.ESTADO              = LT_RC_OPE_SHORTCODE(A).ESTADO,
                 B.FECHA_ACTUALIZACION = SYSDATE
           WHERE ID_MENSAJE = LT_RC_OPE_SHORTCODE(A).ID_MENSAJE
             AND B.SHORTCODE = LT_RC_OPE_SHORTCODE(A).SHORTCODE
             AND B.ID_SERVICIO = LT_RC_OPE_SHORTCODE(A).ID_SERVICIO;
        COMMIT;
      END IF;
    END LOOP;
    CLOSE C_OPE_SHORCODE;
    
    Lv_Bandera := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(12392,
                                                                       'USSD_BAND_ACT_EST');
    
    IF NVL(Lv_Bandera,'N') = 'S' THEN
       FIN_GESTION_SHORTCODE.P_ACTUALIZA_ESTADO(Pv_Error => PV_ERROR);
       
       IF PV_ERROR IS NOT NULL THEN
          RAISE Le_Error;
       END IF;
       
    END IF;
    
  EXCEPTION
    WHEN Le_Error THEN
      PV_ERROR := 'P_ENVIA_MSJ_SHORTCODE -> ' || PV_ERROR;
    WHEN OTHERS THEN
      IF C_OPE_SHORCODE%ISOPEN THEN
        CLOSE C_OPE_SHORCODE;
      END IF;  
      PV_ERROR := 'P_ENVIA_MSJ_SHORTCODE -> ' || SUBSTR(SQLERRM, 1, 300);
  END P_ENVIA_MSJ_SHORTCODE;
------------------------------------------------------------------------
------------------------------------------------------------------------
  PROCEDURE P_CARGA_PAGOS(Pv_Error          OUT VARCHAR2) IS
    
    Lv_SqlCreaTabla      VARCHAR2(4000);
    Lv_SqlCreaIndice     VARCHAR2(1000);
    Lv_SqlDropTabla      VARCHAR2(1000);
    Lv_SqlCargaDatos     VARCHAR2(30000);
    
  BEGIN
    
    Lv_SqlCreaTabla := 'CREATE TABLE SYSADM.FN_CARGA_PAGOS
    (
    CUSTOMER_ID          NUMBER,
    CACURAMT_PAY         NUMBER
    )
    TABLESPACE DBX_DAT
      PCTFREE 10
      PCTUSED 40
      INITRANS 1
      MAXTRANS 255
      STORAGE
      (
       INITIAL 64K
       NEXT 1M
       MINEXTENTS 1
       MAXEXTENTS UNLIMITED
      )';
    
    Lv_SqlCreaIndice := 'CREATE INDEX SYSADM.IDX_FN_CARG_PAG ON FN_CARGA_PAGOS (CUSTOMER_ID)
    TABLESPACE DBX_DAT
     PCTFREE 10
     INITRANS 2
     MAXTRANS 255
     STORAGE
     (
      INITIAL 64K
      NEXT 1M
      MINEXTENTS 1
      MAXEXTENTS UNLIMITED
     )';
    
    BEGIN
      Lv_SqlDropTabla:= 'DROP TABLE SYSADM.FN_CARGA_PAGOS';
      EXECUTE IMMEDIATE Lv_SqlDropTabla;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    
    EXECUTE IMMEDIATE Lv_SqlCreaTabla;
    EXECUTE IMMEDIATE Lv_SqlCreaIndice;
    
    Lv_SqlCargaDatos := 
    'DECLARE
    CURSOR C_PAGOS IS
    SELECT C.CUSTOMER_ID, SUM(C.CACURAMT_PAY) CACURAMT_PAY
      FROM CASHRECEIPTS_ALL C
     WHERE C.CAENTDATE BETWEEN TO_DATE(TO_CHAR(SYSDATE - 1, ''DD/MM/RRRR'') || '' 00:00:00'', ''DD/MM/RRRR HH24:MI:SS'') AND 
                               TO_DATE(TO_CHAR(SYSDATE - 1, ''DD/MM/RRRR'') || '' 23:59:59'', ''DD/MM/RRRR HH24:MI:SS'')
     GROUP BY C.CUSTOMER_ID;
    
    TYPE Ty_CUSTOMERId   IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE Ty_CacuramtPay  IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    Lt_CustomerId        Ty_CUSTOMERId;
    Lt_CacuramtPay       Ty_CacuramtPay;
    
    BEGIN
      
    OPEN C_PAGOS;
    LOOP
      FETCH C_PAGOS BULK COLLECT INTO Lt_CustomerId, Lt_CacuramtPay LIMIT 1000;
      EXIT WHEN Lt_CustomerId.COUNT=0;
      
      IF Lt_CustomerId.COUNT > 0 THEN
         FORALL I IN Lt_CustomerId.FIRST .. Lt_CustomerId.LAST
         INSERT INTO SYSADM.FN_CARGA_PAGOS(CUSTOMER_ID, CACURAMT_PAY)
         VALUES (Lt_CustomerId(I), Lt_CacuramtPay(I));
         
         COMMIT;
      END IF;
    END LOOP;
    CLOSE C_PAGOS;
    END;';
    
    EXECUTE IMMEDIATE Lv_SqlCargaDatos;
    
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error    := 'P_CARGA_PAGOS -> ' || SUBSTR(SQLERRM, 1, 300);
  END P_CARGA_PAGOS;
------------------------------------------------------------------------
------------------------------------------------------------------------
  PROCEDURE P_CONSULTA_SALDO_CUOTAS(Pv_Telef              VARCHAR2,
                                    Pn_CodError       OUT NUMBER,
                                    Pv_Error          OUT VARCHAR2) IS
    
    CURSOR C_DATOS (Cv_Telef VARCHAR2)IS
    SELECT A.CUSTOMER_ID
      FROM FN_GESTION_SHORTCODE A
     WHERE A.ID_SERVICIO = Cv_Telef
       AND A.ESTADO = 'P';
    
    CURSOR C_SALDO_CLIENTE(CN_CUSTOMER_ID NUMBER) IS
    SELECT SUM(C.OHOPNAMT_DOC) SALDO
      FROM ORDERHDR_ALL C
     WHERE C.CUSTOMER_ID = CN_CUSTOMER_ID;
    
    Lc_SaldoCliente C_SALDO_CLIENTE%ROWTYPE;
    Ln_CustomerdId  NUMBER;
    Lv_Telf         VARCHAR2(20);
    Le_Error        EXCEPTION;
    
  BEGIN
    
    Lv_Telf := OBTIENE_TELEFONO_DNC_INT(PV_TELEFONO => Pv_Telef,
                                        PN_RED => 2,
                                        PV_VERSION_TELEFONO => NULL);
    
    OPEN C_DATOS(Lv_Telf);
    FETCH C_DATOS INTO Ln_CustomerdId;
    CLOSE C_DATOS;
    
    IF Ln_CustomerdId IS NULL THEN
       Pv_Error := 'No se encontro el numero: '||Pv_Telef||' en la tabla FN_GESTION_SHORTCODE';
       RAISE Le_Error;
    END IF;
    
    OPEN C_SALDO_CLIENTE(Ln_CustomerdId);
    FETCH C_SALDO_CLIENTE INTO Lc_SaldoCliente;
    CLOSE C_SALDO_CLIENTE;
    
    IF Lc_SaldoCliente.SALDO > 0 THEN
       Pv_Error := Lc_SaldoCliente.SALDO;
    ELSE
       Pv_Error := 'El cliente no posee deuda ';
       RAISE Le_Error;
    END IF;
    
    Pn_CodError := 0;
    
  EXCEPTION
    WHEN Le_Error THEN
      Pn_CodError := -1;
      Pv_Error    := 'P_CONSULTA_SALDO_CUOTAS -> ' || Pv_Error;
    WHEN OTHERS THEN
      Pn_CodError := -1;
      Pv_Error    := 'P_CONSULTA_SALDO_CUOTAS -> ' || SUBSTR(SQLERRM, 1, 300);
  END P_CONSULTA_SALDO_CUOTAS;
------------------------------------------------------------------------
------------------------------------------------------------------------
  PROCEDURE P_CONSULTA_SALDO(Pv_Telef              VARCHAR2,
                             Pn_CodError       OUT NUMBER,
                             Pv_Error          OUT VARCHAR2) IS
    
    CURSOR C_DATOS (Cv_Telef VARCHAR2)IS
    SELECT A.CUSTOMER_ID
      FROM FN_GESTION_SHORTCODE A
     WHERE A.ID_SERVICIO = Cv_Telef;
    
    CURSOR C_SALDO_CLIENTE(CN_CUSTOMER_ID NUMBER) IS
    SELECT SUM(C.OHOPNAMT_DOC) SALDO
      FROM ORDERHDR_ALL C
     WHERE C.CUSTOMER_ID = CN_CUSTOMER_ID;
    
    CURSOR C_FECHA_FACT (CN_CUSTOMER_ID NUMBER)IS
    SELECT MAX(X.OHENTDATE)
      FROM ORDERHDR_ALL X 
     WHERE X.CUSTOMER_ID = CN_CUSTOMER_ID
       AND X.OHSTATUS = 'IN';
    
    Lc_SaldoCliente  C_SALDO_CLIENTE%ROWTYPE;
    Ln_CustomerdId   NUMBER;
    Lv_MensajeDeuda  VARCHAR2(1000);
    Lv_MensajeNoDeu  VARCHAR2(1000);
    Lv_Telf          VARCHAR2(20);
    Ld_FechaIni      DATE;
    Ld_FechaFin      DATE;
    Le_Error         EXCEPTION;
    
  BEGIN
    
    Lv_MensajeDeuda := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11477,
                                                                            PV_ID_PARAMETRO      => 'SHORTCODE_MENSAJE_DEUDA');
    
    Lv_MensajeNoDeu := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11477,
                                                                            PV_ID_PARAMETRO      => 'SHORTCODE_MENSAJE_SIN_DEUDA');
    
    Lv_Telf := OBTIENE_TELEFONO_DNC_INT(PV_TELEFONO => Pv_Telef,
                                        PN_RED => 2,
                                        PV_VERSION_TELEFONO => NULL);
    
    OPEN C_DATOS(Lv_Telf);
    FETCH C_DATOS INTO Ln_CustomerdId;
    CLOSE C_DATOS;
    
    IF Ln_CustomerdId IS NULL THEN
       Pv_Error := 'No se encontro el numero: '||Pv_Telef||' en la tabla FN_GESTION_SHORTCODE';
       RAISE Le_Error;
    END IF;
    
    OPEN C_SALDO_CLIENTE(Ln_CustomerdId);
    FETCH C_SALDO_CLIENTE INTO Lc_SaldoCliente;
    CLOSE C_SALDO_CLIENTE;
    
    IF Lc_SaldoCliente.SALDO > 0 THEN
       
       OPEN C_FECHA_FACT(Ln_CustomerdId);
       FETCH C_FECHA_FACT INTO Ld_FechaIni;
       CLOSE C_FECHA_FACT;
       
       Ld_FechaFin := (Ld_FechaIni+INTERVAL'1'month)-1;
       
       Lv_MensajeDeuda := REPLACE(Lv_MensajeDeuda, '<SALDO>', Lc_SaldoCliente.SALDO);
       Lv_MensajeDeuda := REPLACE(Lv_MensajeDeuda, '<FECHA_INI>', Ld_FechaIni);
       Lv_MensajeDeuda := REPLACE(Lv_MensajeDeuda, '<FECHA_FIN>', Ld_FechaFin);
       
       Pv_Error := Lv_MensajeDeuda;
    ELSE
       Pv_Error := Lv_MensajeNoDeu;
    END IF;
    
    Pn_CodError := 0;
    
  EXCEPTION
    WHEN Le_Error THEN
      Pn_CodError := -1;
      Pv_Error    := 'P_CONSULTA_SALDO -> ' || Pv_Error;
    WHEN OTHERS THEN
      Pn_CodError := -1;
      Pv_Error    := 'P_CONSULTA_SALDO -> ' || SUBSTR(SQLERRM, 1, 300);
  END P_CONSULTA_SALDO;
------------------------------------------------------------------------
------------------------------------------------------------------------
PROCEDURE P_REPORTE_SHORTCODE_USSD (Pv_Error OUT VARCHAR2) IS
  
  CURSOR C_CUENTAS IS
  SELECT A.ID_MENSAJE, A.SHORTCODE, A.MENSAJE, A.CICLO, A.CUSTOMER_ID, A.CUSTCODE CUENTA, A.ID_SERVICIO, A.DEUDA_TOTAL,
         A.PAGO, A.USUARIO_INGRESO
    FROM FN_GESTION_SHORTCODE A 
   WHERE /*A.FECHA_INGRESO*/A.FECHA_ACTUALIZACION >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY')||' 00:00:01', 'DD/MM/YYYY HH24:MI:SS')
   ORDER BY A.ID_MENSAJE, A.CUSTOMER_ID;
  
  LF_OUTPUT          UTL_FILE.FILE_TYPE;
  Ln_Count           NUMBER:=0;
  Ln_Error           NUMBER;
  Ln_IdSecuencia     NUMBER;
  Lv_Directorio      VARCHAR2(100);
  Lv_NombreArch      VARCHAR2(100);
  Lv_IpFinan27       VARCHAR2(50);
  Lv_UsuFinan27      VARCHAR2(50);
  Lv_RutaRemota      VARCHAR2(1000);
  Lv_ErrorFtp        VARCHAR2(1000);
  Lv_Asunto          VARCHAR2(1000);
  Lv_Body            VARCHAR2(6000);
  Lv_Destinatarios   VARCHAR2(4000);
  Lv_Remitente       VARCHAR2(1000);
  Lv_Puerto          VARCHAR2(50);
  Lv_Servidor        VARCHAR2(50);
  Le_Error           EXCEPTION;
  
BEGIN
  
  Lv_Directorio := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_NOM_DIRE',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_NombreArch := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_NOM_ARCH',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_UsuFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_USU_FIN',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_IpFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_IP_FIN',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_RutaRemota := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_RUT_REPO',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_Asunto := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_ASUNTO_MAIL',Pv_Error);
  
  Lv_Asunto := Lv_Asunto||to_char(SYSDATE,'DDMMYYYY');
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_BODY_MAIL',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_Remitente := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_REM_MAIL',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_Destinatarios := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_DEST_MAIL',Pv_Error);
  
  IF Pv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_NombreArch := Lv_NombreArch||TO_CHAR(SYSDATE,'DDMMYYYY')||'.csv';
  
  FOR I IN C_CUENTAS LOOP
      
      Ln_Count := Ln_Count +1;
      
      IF Ln_Count = '1' THEN
         LF_OUTPUT := UTL_FILE.FOPEN(Lv_Directorio, Lv_NombreArch, 'W');
         UTL_FILE.PUT_LINE(LF_OUTPUT,'ID_MENSAJE|USSD|MENSAJE|CICLO|CUSTOMER_ID|CUENTA|ID_SERVICIO|DEUDA_TOTAL|PAGO|USUARIO');
      END IF;
      
      UTL_FILE.PUT_LINE(LF_OUTPUT,I.ID_MENSAJE||'|'||I.SHORTCODE||'|'||I.MENSAJE||'|'||I.CICLO||'|'||I.CUSTOMER_ID||'|'||
                                  I.CUENTA||'|'||I.ID_SERVICIO||CHR(9)||I.DEUDA_TOTAL||CHR(9)||I.PAGO||'|'||I.USUARIO_INGRESO);
      
  END LOOP;
  
  IF Ln_Count > 0 THEN
     
     UTL_FILE.FCLOSE(LF_OUTPUT);
     --Traspaso de archivo a ruta de finan27
     scp_dat.sck_ftp_gtw.scp_transfiere_archivo(pv_ip                => Lv_IpFinan27,
                                                pv_usuario           => Lv_UsuFinan27,
                                                pv_ruta_remota       => Lv_RutaRemota,
                                                pv_nombre_arch       => Lv_NombreArch,
                                                pv_ruta_local        => Lv_Directorio,
                                                pv_borrar_arch_local => 'N',
                                                pn_error             => Ln_Error,
                                                pv_error             => Lv_ErrorFtp);
     
     IF Lv_ErrorFtp IS NULL THEN
        
        Lv_RutaRemota := '</ARCHIVO1='||Lv_NombreArch||'|DIRECTORIO1='||Lv_RutaRemota||'|/>';
        
        --Envio del mail en caso de exito
        Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar (pv_nombresatelite  => 'P_REPORTE_SHORTCODE_USSD',
                                                               pd_fechaenvio      => SYSDATE,
                                                               pd_fechaexpiracion => SYSDATE+1,
                                                               pv_asunto          => Lv_Asunto,
                                                               pv_mensaje         => Lv_Body||' '||Lv_RutaRemota,
                                                               pv_destinatario    => Lv_Destinatarios,
                                                               pv_remitente       => Lv_Remitente,
                                                               pv_tiporegistro    => 'A',
                                                               pv_clase           => 'SCC',
                                                               pv_puerto          => Lv_Puerto,
                                                               pv_servidor        => Lv_Servidor,
                                                               pv_maxintentos     => 5,
                                                               pv_idusuario       => 'RELOJ',
                                                               pv_mensajeretorno  => PV_ERROR,
                                                               pn_idsecuencia     => Ln_IdSecuencia);
        
     ELSE
        
        Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_ERROR_BODY',Pv_Error);
                
     END IF;
  ELSE
     
     Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_VACIO_BODY',Pv_Error);
  END IF;
  
  IF Ln_Count = 0 OR Lv_ErrorFtp IS NOT NULL THEN
     --Envio del mail cuando no se obtuvo registros de suspension
     Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar (pv_nombresatelite  => 'P_REPORTE_SHORTCODE_USSD',
                                                            pd_fechaenvio      => SYSDATE,
                                                            pd_fechaexpiracion => SYSDATE+1,
                                                            pv_asunto          => Lv_Asunto,
                                                            pv_mensaje         => Lv_Body,
                                                            pv_destinatario    => Lv_Destinatarios,
                                                            pv_remitente       => Lv_Remitente,
                                                            pv_tiporegistro    => 'M',
                                                            pv_clase           => 'SCC',
                                                            pv_puerto          => Lv_Puerto,
                                                            pv_servidor        => Lv_Servidor,
                                                            pv_maxintentos     => 5,
                                                            pv_idusuario       => 'RELOJ',
                                                            pv_mensajeretorno  => PV_ERROR,
                                                            pn_idsecuencia     => Ln_IdSecuencia);
     
  END IF;
  
  IF PV_ERROR IS NOT NULL THEN
     PV_ERROR := 'Error en el envio del mail: '||Ln_Error||'-'||PV_ERROR;
     RAISE Le_Error;
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    PV_ERROR := 'P_REPORTE_SHORTCODE_USSD -> ' || PV_ERROR;
  WHEN OTHERS THEN
    Pv_Error := 'P_REPORTE_SHORTCODE_USSD -> '|| SUBSTR(SQLERRM, 1, 300);
END P_REPORTE_SHORTCODE_USSD;
------------------------------------------------------------------------
PROCEDURE P_CARGA_ARCHIVO(Pn_IdMensaje      NUMBER,
                          Pn_Ussd           NUMBER,
                          Pv_Mensaje        VARCHAR2,
                          Pv_Separador      VARCHAR2,
                          Pt_Cadena         TR_REGISTROS,
                          Pv_Error      OUT VARCHAR2)IS
  
  CURSOR C_DATOS_BSCS (Cv_Cuenta VARCHAR2)IS
  SELECT A.CUSTOMER_ID, B.CO_ID
    FROM CUSTOMER_ALL A, CONTRACT_ALL B
   WHERE A.CUSTCODE = Cv_Cuenta
     AND B.CUSTOMER_ID = A.CUSTOMER_ID
   ORDER BY B.CO_ID DESC;
  
  CURSOR C_CICLO (Cv_Cuenta VARCHAR2,
                  Cv_Servicio VARCHAR2)IS
  SELECT X.CUSTOMER_ID, X.CO_ID, X.ADMINCYCLE CICLO, X.DN_NUM 
    FROM RELOJ.TTC_CONTRACTPLANNIGDETAILS X, CUSTOMER_ALL Y
   WHERE Y.CUSTCODE = Cv_Cuenta
     AND X.CUSTOMER_ID = Y.CUSTOMER_ID
     AND X.DN_NUM = Cv_Servicio
     AND ROWNUM <= 1;
  
  CURSOR C_SALDO_CLIENTE(Cn_CustomerId NUMBER) IS
  SELECT SUM(C.OHOPNAMT_DOC) SALDO, MAX(C.OHDUEDATE) FECHA_MAX_PAGO
    FROM ORDERHDR_ALL C
   WHERE C.CUSTOMER_ID = Cn_CustomerId;
  
  CURSOR C_STATUS(Cn_CoId  NUMBER)IS
  SELECT X.CH_STATUS
    FROM CONTRACT_HISTORY X
   WHERE X.CO_ID = Cn_CoId
     AND X.CH_SEQNO IN (SELECT MAX(Y.CH_SEQNO)
                          FROM CONTRACT_HISTORY Y
                         WHERE Y.CO_ID = Cn_CoId);
  
  TYPE R_DATOS IS RECORD(
      IdMensaje        NUMBER,
      Ussd             NUMBER,
      Mensaje          VARCHAR2(1000),
      Ciclo            VARCHAR2(2),
      CustomerId       NUMBER,
      Cuenta           VARCHAR2(24),
      CoId             NUMBER,
      IdServicio       VARCHAR2(50),
      Financiamiento   VARCHAR2(3),
      DeudaTotal       NUMBER,
      Pago             NUMBER,
      FechaMaxPag      DATE,
      Estado           VARCHAR2(3),
      FechaIngreso     DATE,
      Usuario          VARCHAR2(100)
      );
      
  TYPE TR_DATOS IS TABLE OF R_DATOS INDEX BY BINARY_INTEGER;
  
  TYPE R_NO_PROCESADOS IS RECORD(
      IdMensaje        NUMBER,
      Ussd             NUMBER,
      Mensaje          VARCHAR2(1000),
      Ciclo            VARCHAR2(2),
      CustomerId       NUMBER,
      Cuenta           VARCHAR2(24),
      CoId             NUMBER,
      IdServicio       VARCHAR2(50),
      DeudaTotal       NUMBER,
      Observacion      VARCHAR2(1000),
      Usuario          VARCHAR2(100)
      );
      
  TYPE TR_NO_PROCESAR IS TABLE OF R_NO_PROCESADOS INDEX BY BINARY_INTEGER;
  
  Lt_Datos           TR_DATOS;
  Lt_NoProcesar      TR_NO_PROCESAR;
  Lc_DatosBscs       C_DATOS_BSCS%ROWTYPE;
  Lc_Ciclo           C_CICLO%ROWTYPE;
  Lc_Saldo           C_SALDO_CLIENTE%ROWTYPE;
  Ln_CountProces     NUMBER:=0;
  Ln_CountNoProc     NUMBER:=0;
  Ln_Error           NUMBER;
  Ln_IdSecuencia     NUMBER;
  Lv_Cuenta          VARCHAR2(24);
  Lv_IdServicio      VARCHAR2(50);
  Lv_Usuario         VARCHAR2(100);
  Lv_Status          VARCHAR2(24);
  Lv_Asunto          VARCHAR2(1000);
  Lv_Body            VARCHAR2(6000);
  Lv_Destinatarios   VARCHAR2(4000);
  Lv_Remitente       VARCHAR2(1000);
  Lv_NombreArch      VARCHAR2(100);
  Lv_IpFinan27       VARCHAR2(50);
  Lv_UsuFinan27      VARCHAR2(50);
  Lv_RutaRemota      VARCHAR2(1000);
  Lv_Directorio      VARCHAR2(100);
  Lv_Puerto          VARCHAR2(50);
  Lv_Servidor        VARCHAR2(50);
  Lv_ErrorFtp        VARCHAR2(1000);
  LF_OUTPUT          UTL_FILE.FILE_TYPE;
  
BEGIN
  
  FOR I IN Pt_Cadena.FIRST .. Pt_Cadena.LAST LOOP
      
      Lv_Cuenta := NULL;
      Lv_IdServicio := NULL;
      Lv_Usuario := NULL;
      Lc_DatosBscs := NULL;
      Lc_Ciclo := NULL;
      Lc_Saldo := NULL;
      Lv_Status := NULL;
      
      Lv_Cuenta := SUBSTR(Pt_Cadena(I).LINEA, INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,1)+1,
                   ((LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,1)) - 
                   (LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,2))-1));
      
      Lv_IdServicio := SUBSTR(Pt_Cadena(I).LINEA, INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,2)+1,
                    ((LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,2)) - 
                    (LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,3))-1));
      
      Lv_Usuario := SUBSTR(Pt_Cadena(I).LINEA, INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,3)+1,
                    ((LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,3)) - 
                    (LENGTH(Pt_Cadena(I).LINEA)-INSTR(Pt_Cadena(I).LINEA,Pv_Separador,1,4))-1));
      
      OPEN C_CICLO(Lv_Cuenta, Lv_IdServicio);
      FETCH C_CICLO INTO Lc_Ciclo;
      CLOSE C_CICLO;
      
      IF Lc_Ciclo.CUSTOMER_ID IS NULL THEN
         OPEN C_DATOS_BSCS(Lv_Cuenta);
         FETCH C_DATOS_BSCS INTO Lc_DatosBscs;
         CLOSE C_DATOS_BSCS;
      END IF;
      
      OPEN C_SALDO_CLIENTE(NVL(Lc_Ciclo.CUSTOMER_ID, Lc_DatosBscs.Customer_Id));
      FETCH C_SALDO_CLIENTE INTO Lc_Saldo;
      CLOSE C_SALDO_CLIENTE;
      
      OPEN C_STATUS(NVL(Lc_Ciclo.CO_ID, Lc_DatosBscs.Co_Id));
      FETCH C_STATUS INTO Lv_Status;
      CLOSE C_STATUS;
      
      IF Lv_Status = 's' AND Lc_Saldo.Saldo > 0 THEN
         
         Ln_CountProces := Ln_CountProces +1;
         
         Lt_Datos(Ln_CountProces).IdMensaje      := Pn_IdMensaje;
         Lt_Datos(Ln_CountProces).Ussd           := Pn_Ussd;
         Lt_Datos(Ln_CountProces).Mensaje        := Pv_Mensaje;
         Lt_Datos(Ln_CountProces).Ciclo          := Lc_Ciclo.Ciclo;
         Lt_Datos(Ln_CountProces).CustomerId     := NVL(Lc_Ciclo.CUSTOMER_ID, Lc_DatosBscs.Customer_Id);
         Lt_Datos(Ln_CountProces).Cuenta         := Lv_Cuenta;
         Lt_Datos(Ln_CountProces).CoId           := NVL(Lc_Ciclo.CO_ID, Lc_DatosBscs.Co_Id);
         Lt_Datos(Ln_CountProces).IdServicio     := Lv_IdServicio;
         Lt_Datos(Ln_CountProces).Financiamiento := 'T';
         Lt_Datos(Ln_CountProces).DeudaTotal     := Lc_Saldo.Saldo;
         Lt_Datos(Ln_CountProces).Pago           := 0;
         Lt_Datos(Ln_CountProces).FechaMaxPag    := Lc_Saldo.Fecha_Max_Pago;
         Lt_Datos(Ln_CountProces).Estado         := 'I';
         Lt_Datos(Ln_CountProces).FechaIngreso   := SYSDATE;
         Lt_Datos(Ln_CountProces).Usuario        := Lv_Usuario;
      ELSE
         Ln_CountNoProc := Ln_CountNoProc +1;
         Lt_NoProcesar(Ln_CountNoProc).IdMensaje  := Pn_IdMensaje;
         Lt_NoProcesar(Ln_CountNoProc).Ussd       := Pn_Ussd;
         Lt_NoProcesar(Ln_CountNoProc).Mensaje    := Pv_Mensaje;
         Lt_NoProcesar(Ln_CountNoProc).Ciclo      := Lc_Ciclo.Ciclo;
         Lt_NoProcesar(Ln_CountNoProc).CustomerId := NVL(Lc_Ciclo.CUSTOMER_ID, Lc_DatosBscs.Customer_Id);
         Lt_NoProcesar(Ln_CountNoProc).Cuenta     := Lv_Cuenta;
         Lt_NoProcesar(Ln_CountNoProc).CoId       := NVL(Lc_Ciclo.CO_ID, Lc_DatosBscs.Co_Id);
         Lt_NoProcesar(Ln_CountNoProc).IdServicio := Lv_IdServicio;
         Lt_NoProcesar(Ln_CountNoProc).DeudaTotal := Lc_Saldo.Saldo;
         Lt_NoProcesar(Ln_CountNoProc).Usuario    := Lv_Usuario;
         
         IF Lc_Saldo.Saldo <= 0 THEN
            Lt_NoProcesar(Ln_CountNoProc).Observacion := 'La cuenta no tiene deuda.';
         ELSE
            IF Lv_Status = 'a' THEN
               Lt_NoProcesar(Ln_CountNoProc).Observacion := 'La cuenta se encuentra activada.';
            ELSIF Lv_Status = 's' THEN
               Lt_NoProcesar(Ln_CountNoProc).Observacion := 'La cuenta se encuentra suspendida.';
            ELSIF Lv_Status = 'd' THEN
               Lt_NoProcesar(Ln_CountNoProc).Observacion := 'La cuenta se encuentra inactiva.';
            END IF;
         END IF;
      END IF;
      
  END LOOP;
  
  FORALL J IN Lt_Datos.FIRST .. Lt_Datos.LAST
         INSERT INTO FN_GESTION_SHORTCODE(ID_MENSAJE, SHORTCODE, MENSAJE, CICLO, CUSTOMER_ID, CUSTCODE, CO_ID, ID_SERVICIO,
                                          FINANCIAMIENTO, DEUDA_TOTAL, PAGO, FECHA_MAX_PAGO, ESTADO, FECHA_INGRESO, USUARIO_INGRESO)
         VALUES (Lt_Datos(J).IdMensaje, Lt_Datos(J).Ussd, Lt_Datos(J).Mensaje, Lt_Datos(J).Ciclo, Lt_Datos(J).CustomerId,
                 Lt_Datos(J).Cuenta, Lt_Datos(J).CoId, Lt_Datos(J).IdServicio, Lt_Datos(J).Financiamiento, Lt_Datos(J).DeudaTotal,
                 Lt_Datos(J).Pago, Lt_Datos(J).FechaMaxPag, Lt_Datos(J).Estado, Lt_Datos(J).FechaIngreso, Lt_Datos(J).Usuario);
  
  IF Ln_CountNoProc > 0 THEN
     
     Lv_Remitente := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_REM_MAIL',Pv_Error);
     
     Lv_Destinatarios := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_DEST_MAIL',Pv_Error);
     
     Lv_Asunto := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(12018,'USSD_ASUNT_ARCH_ERROR',Pv_Error);
     Lv_Asunto := Lv_Asunto||to_char(SYSDATE,'DDMMYYYY');
     
     Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(12018,'USSD_BODY_ARCH_ERROR',Pv_Error);
     
     Lv_Directorio := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_NOM_DIRE',Pv_Error);
     
     Lv_NombreArch := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(12018,'USSD_CTAS_NOM_ARCH',Pv_Error);
     Lv_NombreArch := Lv_NombreArch||TO_CHAR(SYSDATE,'DDMMYYYY')||'.csv';
     
     Lv_IpFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_IP_FIN',Pv_Error);
     
     Lv_UsuFinan27 := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_USU_FIN',Pv_Error);
     
     Lv_RutaRemota := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_RUT_REPO',Pv_Error);
     
     LF_OUTPUT := UTL_FILE.FOPEN(Lv_Directorio, Lv_NombreArch, 'W');
     UTL_FILE.PUT_LINE(LF_OUTPUT,'ID_MENSAJE|USSD|MENSAJE|CUSTOMER_ID|CUENTA|ID_SERVICIO|DEUDA_TOTAL|USUARIO|COMENTARIO');
     
     FOR K IN Lt_NoProcesar.FIRST .. Lt_NoProcesar.LAST LOOP
         UTL_FILE.PUT_LINE(LF_OUTPUT,Lt_NoProcesar(K).IdMensaje||'|'||Lt_NoProcesar(K).Ussd||'|'||Lt_NoProcesar(K).Mensaje
                                     ||'|'||Lt_NoProcesar(K).CustomerId||'|'||Lt_NoProcesar(K).Cuenta||'|'||
                                     Lt_NoProcesar(K).IdServicio||'|'||Lt_NoProcesar(K).DeudaTotal||'|'||
                                     Lt_NoProcesar(K).Usuario||'|'||Lt_NoProcesar(K).Observacion);
         
     END LOOP;
     
     UTL_FILE.FCLOSE(LF_OUTPUT);
     
     --Traspaso de archivo a ruta de finan27
     scp_dat.sck_ftp_gtw.scp_transfiere_archivo(pv_ip                => Lv_IpFinan27,
                                                pv_usuario           => Lv_UsuFinan27,
                                                pv_ruta_remota       => Lv_RutaRemota,
                                                pv_nombre_arch       => Lv_NombreArch,
                                                pv_ruta_local        => Lv_Directorio,
                                                pv_borrar_arch_local => 'N',
                                                pn_error             => Ln_Error,
                                                pv_error             => Lv_ErrorFtp);
     
     IF Lv_ErrorFtp IS NULL THEN
        
        Lv_RutaRemota := '</ARCHIVO1='||Lv_NombreArch||'|DIRECTORIO1='||Lv_RutaRemota||'|/>';
        
        --Envio del mail en caso de exito
        Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar (pv_nombresatelite  => 'P_CARGA_ARCHIVO',
                                                               pd_fechaenvio      => SYSDATE,
                                                               pd_fechaexpiracion => SYSDATE+1,
                                                               pv_asunto          => Lv_Asunto,
                                                               pv_mensaje         => Lv_Body||' '||Lv_RutaRemota,
                                                               pv_destinatario    => Lv_Destinatarios,
                                                               pv_remitente       => Lv_Remitente,
                                                               pv_tiporegistro    => 'A',
                                                               pv_clase           => 'SCC',
                                                               pv_puerto          => Lv_Puerto,
                                                               pv_servidor        => Lv_Servidor,
                                                               pv_maxintentos     => 5,
                                                               pv_idusuario       => 'RELOJ',
                                                               pv_mensajeretorno  => Pv_Error,
                                                               pn_idsecuencia     => Ln_IdSecuencia);
        
     ELSE
        
        Lv_Body := RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11603,'USSD_ERROR_BODY',Pv_Error);
        Lv_Body := Lv_Body||' - '||Lv_ErrorFtp;
        
        Ln_Error := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'P_CARGA_ARCHIVO',
                                                              pd_fechaenvio      => SYSDATE,
                                                              pd_fechaexpiracion => SYSDATE+1,
                                                              pv_asunto          => Lv_Asunto,
                                                              pv_mensaje         => Lv_Body,
                                                              pv_destinatario    => Lv_Destinatarios,
                                                              pv_remitente       => Lv_Remitente,
                                                              pv_tiporegistro    => 'M',
                                                              pv_clase           => 'SCC',
                                                              pv_puerto          => Lv_Puerto,
                                                              pv_servidor        => Lv_Servidor,
                                                              pv_maxintentos     => 5,
                                                              pv_idusuario       => 'RELOJ',
                                                              pv_mensajeretorno  => Pv_Error,
                                                              pn_idsecuencia     => Ln_IdSecuencia);
        
     END IF;
     
  END IF;
  
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    Pv_Error:= 'FIN_GESTION_SHORTCODE.P_CARGA_ARCHIVO => '||SQLERRM;
END P_CARGA_ARCHIVO;

PROCEDURE P_ACTUALIZA_ESTADO (Pv_Error      OUT VARCHAR2)IS
  
  Ln_DiasNeg   NUMBER;
  
BEGIN
  
  Ln_DiasNeg := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 12392,
                                                                     PV_ID_PARAMETRO      => 'USSD_DIAS_NEGO');
  
  UPDATE FN_GESTION_SHORTCODE X
     SET X.ESTADO = 'F',
         X.FECHA_ACTUALIZACION = SYSDATE
   WHERE X.ESTADO = 'P'
     AND X.FECHA_ACTUALIZACION + NVL(Ln_DiasNeg,0) < SYSDATE;
  
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    Pv_Error:= 'FIN_GESTION_SHORTCODE.P_ACTUALIZA_ESTADO => '||SQLERRM;
END P_ACTUALIZA_ESTADO;

END FIN_GESTION_SHORTCODE;
/
