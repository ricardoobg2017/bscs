create or replace package cvv_cartera_vigente is

  -- Author  : CLS William Pozo CLS Diana Huayamave CLS Luis Olvera
  -- Created : 24/01/2014 16:00:14
 -------------------------------------------------------------------------------
--  Creado por:  CLS Luis Olvera, CLS Diana Huayamave, CLS William Pozo
--  Objetivo  :  Insertar en la tabla CV_CONTROL_EJECUCION el nombre de proceso 
--               con el estado que genere, usuario, ciclo, Inicio de Ejecucion.             
--  Fecha     :  Diciembre 17/2013
-------------------------------------------------------------------------------
------------------------------------------------------------------------------- 

  
  
 procedure CVV_OBJ_CONTROL_EJECUCION(PN_ID_EJECUCION IN NUMBER,
                                                  PV_NOMBRE_PROCESO IN VARCHAR2,
                                                  PV_ESTADO IN VARCHAR2,
                                                  PV_USUARIO IN VARCHAR2,
                                                  PV_CICLO IN VARCHAR2,
                                                  PD_INI_EJECUCION IN DATE,
                                                  PV_ERROR OUT VARCHAR2);
         
-------------------------------------------------------------------------------
--  Objetivo  :  Actualizar en la tabla CV_CONTROL_EJECUCION con los estado de 
--               Terminado = 'T' o Error = 'E' e Insertar en la 
--               tabla CV_CONTROL_HISTORICO para llevar un control de cada 
--               ejecucion de proceso
--  Fecha     :  Diciembre 27/2013
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

                                           
 procedure CVV_OBJ_PROCESS_REPORT(pn_bandera in number,
                                                    pn_ejecucion in number,
                                                    pv_ciclo varchar2);




end cvv_cartera_vigente;
/
create or replace package body cvv_cartera_vigente is

 -- Author  : CLS William Pozo CLS Diana Huayamave CLS Luis Olvera
 -- Created : 24/01/2014 16:00:14

 -------------------------------------------------------------------------------
--  Objetivo  :  Insertar en la tabla CV_CONTROL_EJECUCION el nombre de proceso 
--               con el estado que genere, usuario, ciclo, Inicio de Ejecucion.             
--  Fecha     :  Diciembre 17/2013
-------------------------------------------------------------------------------
------------------------------------------------------------------------------- 

 procedure CVV_OBJ_CONTROL_EJECUCION(PN_ID_EJECUCION IN NUMBER,
                                                  PV_NOMBRE_PROCESO IN VARCHAR2,
                                                  PV_ESTADO IN VARCHAR2,
                                                  PV_USUARIO IN VARCHAR2,
                                                  PV_CICLO IN VARCHAR2,
                                                  PD_INI_EJECUCION IN DATE,
                                                  PV_ERROR OUT VARCHAR2) is
                                                  
                                                  

  PRAGMA AUTONOMOUS_TRANSACTION;


begin
  
 INSERT INTO CV_CONTROL_EJECUCION

    (ID_EJECUCION, 
     NOMBRE_PROCESO,
     ESTADO,
     USUARIO,
     CICLO,
     INI_EJECUCION
    )
  VALUES
    (PN_ID_EJECUCION, 
     PV_NOMBRE_PROCESO,
     PV_ESTADO, 
     PV_USUARIO,
     PV_CICLO,
     PD_INI_EJECUCION
);

  COMMIT;
EXCEPTION
    
    WHEN OTHERS THEN
      PV_ERROR :=  SUBSTR(SQLERRM, 1, 200);


end CVV_OBJ_CONTROL_EJECUCION;

-------------------------------------------------------------------------------
--  Objetivo  :  Actualizar en la tabla CV_CONTROL_EJECUCION con los estado de 
--               Terminado = 'T' o Error = 'E' e Insertar en la 
--               tabla CV_CONTROL_HISTORICO para llevar un control de cada 
--               ejecucion de proceso
--  Fecha     :  Diciembre 27/2013
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

 procedure CVV_OBJ_PROCESS_REPORT(pn_bandera in number,
                                                   pn_ejecucion in number,
                                                   pv_ciclo varchar2) is



nom_proceso varchar2(100);
seq_proceso number;
id_nombre_proc varchar2(50);
FECHA_INI date;
FEHC_FIN date;

cursor cvv_control_ejecu is 

 select ini_ejecucion, fin_ejecucion  from cv_control_ejecucion
 where id_ejecucion = pn_ejecucion ;
 
 lc_control_ejecu cvv_control_ejecu%rowtype;

begin

 select sq_id_proceso.nextval into seq_proceso from dual;

if pn_bandera=1 then
  

 update cv_control_ejecucion
    set estado = 'T', Fin_ejecucion = sysdate
  where id_ejecucion = pn_ejecucion;
  commit; 
 
 
 OPEN cvv_control_ejecu;
    FETCH cvv_control_ejecu INTO lc_control_ejecu;
    CLOSE cvv_control_ejecu;
 
 IF pn_ejecucion = 1 THEN 
    nom_proceso:='INIT_RECUPERADO';
    id_nombre_proc:='CV_HE_01';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 2 THEN 
    nom_proceso:='LLENA_PAGORECUPERADO'; 
    id_nombre_proc:='CV_HE_02';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 3 THEN 
    nom_proceso:='LLENA_CREDITORECUPERADO';
    id_nombre_proc:='CV_HE_03';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 4 THEN 
    nom_proceso:='LLENA_CARGORECUPERADO';
    id_nombre_proc:='CV_HE_04';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 END IF;


 
 INSERT INTO CV_CONTROL_HISTORICO
    VALUES
      (id_nombre_proc,seq_proceso,nom_proceso, 'T', USER, pv_ciclo,FECHA_INI, FEHC_FIN);
    COMMIT;




ELSIF PN_BANDERA=2 THEN
 update cv_control_ejecucion
         set estado = 'E', Fin_ejecucion = sysdate
  WHERE ID_EJECUCION = pn_ejecucion;
  commit;
  
  
  OPEN cvv_control_ejecu;
    FETCH cvv_control_ejecu INTO lc_control_ejecu;
    CLOSE cvv_control_ejecu;  
     
       
 IF pn_ejecucion = 1 THEN 
    nom_proceso:='INIT_RECUPERADO';
    id_nombre_proc:='CV_HE_01';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 2 THEN 
    nom_proceso:='LLENA_PAGORECUPERADO'; 
    id_nombre_proc:='CV_HE_02';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 3 THEN 
    nom_proceso:='LLENA_CREDITORECUPERADO';
    id_nombre_proc:='CV_HE_03';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 ELSIF pn_ejecucion = 4 THEN 
    nom_proceso:='LLENA_CARGORECUPERADO';
    id_nombre_proc:='CV_HE_04';
    FECHA_INI := lc_control_ejecu.ini_ejecucion;
    FEHC_FIN := lc_control_ejecu.fin_ejecucion;
 END IF;

 INSERT INTO CV_CONTROL_HISTORICO
    VALUES
      (id_nombre_proc,seq_proceso,nom_proceso, 'E', USER, pv_ciclo, FECHA_INI, FEHC_FIN);
    COMMIT;
     
      
END IF;
end CVV_OBJ_PROCESS_REPORT;

end cvv_cartera_vigente;
/
