CREATE OR REPLACE PACKAGE IDM_ACTUALIZA_USERS IS

  -- Author  : HCOBOO
  -- Created : 18/02/2010 9:55:08
  -- Purpose : replica los datos y permisos de usuario segun su grupo maestro a la tabla user_class  

  PROCEDURE crp_inserta_user_class(pv_cuenta VARCHAR2,
                                   pv_error  OUT VARCHAR2);
  PROCEDURE crp_elimina_user_class(pv_cuenta VARCHAR2,
                                   pv_error  OUT VARCHAR2);
END;
/
CREATE OR REPLACE PACKAGE BODY IDM_ACTUALIZA_USERS IS
  -- Author  : SIS HCO Ing. Hugo Mauricio Cobo Rojas
  -- Created : 18/02/2010 9:55:08
  -- Purpose : replica los datos y permisos de usuario segun su grupo maestro a la tabla user_class

  PROCEDURE crp_inserta_user_class(pv_cuenta VARCHAR2,
                                   pv_error  OUT VARCHAR2) IS
    --cursor que verifica si el usuario existe en la users
  
    CURSOR c_retorna_grupo(cv_cuenta VARCHAR2) IS
      SELECT a.group_user
        FROM users a
       WHERE a.username = cv_cuenta;
    --cursor que verifica que permisos tiene ese grupo(que el grupo es otro usuario) 
    CURSOR c_verifica_permisos(cv_grupo VARCHAR2) IS
      SELECT b.clcode
        FROM User_Class b
       WHERE b.username = cv_grupo;
    --verifica si ya existe en la user_class
    CURSOR c_actualiza_grupo(cv_cuenta VARCHAR2) IS
      SELECT a.username
        FROM user_class a
       WHERE a.username = cv_cuenta;
  
    --variables   
    le_error EXCEPTION;
    le_error2 EXCEPTION;
    lv_grupo    VARCHAR2(16);
    lv_cuenta   VARCHAR2(16);
    Lc_Proceso  c_verifica_permisos%ROWTYPE;
    Lc_Proceso2 c_actualiza_grupo%ROWTYPE;
    Lb_Found    BOOLEAN;
  
  BEGIN
  
    IF pv_cuenta IS NULL THEN
      RAISE le_error;
    END IF;
  
    OPEN c_retorna_grupo(pv_cuenta);
    FETCH c_retorna_grupo
      INTO lv_grupo;
    Lb_Found := c_retorna_grupo%NOTFOUND;
    CLOSE c_retorna_grupo;
  
    OPEN c_actualiza_grupo(pv_cuenta);
    FETCH c_actualiza_grupo
      INTO lv_cuenta;
    CLOSE c_actualiza_grupo;
  
    --Inserta en la tabla user_class segun los permisos q tenga ese grupo
    IF NOT Lb_Found THEN
      DELETE FROM user_class w
       WHERE w.username = pv_cuenta;
      --COMMIT;
      FOR lc_proceso IN c_verifica_permisos(lv_grupo) LOOP
        BEGIN
          INSERT INTO user_class
            (Username,
             Clcode,
             Entdate,
             Moddate,
             Userlastmod,
             Rec_Version)
          VALUES
            (pv_cuenta,
             lc_proceso.clcode,
             SYSDATE,
             SYSDATE,
             lv_grupo,
             1);
          --COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            pv_error := 'Error al insertar en la tabla user_class, ' || substr(SQLERRM, 1, 100);
        END;
      END LOOP;
    ELSE
      pv_error := NULL;
    
    END IF;
    
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error: ' || substr(SQLERRM, 1, 200);
    
  END crp_inserta_user_class;

  PROCEDURE crp_elimina_user_class(pv_cuenta VARCHAR2,
                                   pv_error  OUT VARCHAR2) IS
  
    CURSOR c_actualiza_grupo(cv_cuenta VARCHAR2) IS
      SELECT a.username
        FROM user_class a
       WHERE a.username = cv_cuenta
         AND rownum < 2;
  
    --declaraciones
    lv_cuenta VARCHAR2(16);
    Lb_Found  BOOLEAN;
  
  BEGIN
  
    OPEN c_actualiza_grupo(pv_cuenta);
    FETCH c_actualiza_grupo
      INTO lv_cuenta;
    Lb_Found := c_actualiza_grupo%NOTFOUND;
    CLOSE c_actualiza_grupo;
  
    --Elimina el usuario asociado al grupo en la tabla user_class 
    IF NOT Lb_Found THEN
      DELETE FROM user_class w
       WHERE w.username = lv_cuenta;
      COMMIT;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error: ' || substr(SQLERRM, 1, 200);
  END;
END;
/
