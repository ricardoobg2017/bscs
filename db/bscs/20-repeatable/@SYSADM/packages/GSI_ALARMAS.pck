CREATE OR REPLACE PACKAGE GSI_ALARMAS IS
  PROCEDURE EJECUTA_ALARMAS;
  PROCEDURE COLAS;
  PROCEDURE COLA_ESPECIFICA(PV_PROCESO IN VARCHAR2);
  PROCEDURE ALARMA_COLAS;
  PROCEDURE SEND_EMAIL(PV_MSG     IN VARCHAR2,
                       PV_SUBJECT IN VARCHAR2,
                       GRUPO      IN NUMBER);
END GSI_ALARMAS;
/
CREATE OR REPLACE PACKAGE BODY GSI_ALARMAS IS
  --========================================================================--
  -- Sistema de Alarmas
  --========================================================================--
  PROCEDURE EJECUTA_ALARMAS IS
    LN_ANTMON CHAR(1);
  BEGIN
    --Cambiar estado a 'A' en la tabla ALARM_CONFIG para volver al antiguo monitoreo
    SELECT T.ESTADO
      INTO LN_ANTMON
      FROM ALARM_CONFIG T
     WHERE T.DESCRIPCION =
           'Activar envio de alarmas directo (antiguo monitoreo)';
  
    IF LN_ANTMON = 'A' THEN
      ALARMA_COLAS();
    ELSE
      COLAS();
    END IF;
  END;
  --========================================================================--
  PROCEDURE COLAS IS
    CURSOR COLAS IS
      SELECT * FROM ALARM_COLAS WHERE ESTADO = 'A';
  
    LI_CURSOR    INTEGER;
    LI_CURSOR1   INTEGER;
    LV_SENTENCIA VARCHAR2(250);
    LN_COLA      NUMBER := 0;
    LB_COLA      BOOLEAN := FALSE;
  
  BEGIN
    FOR I IN COLAS LOOP
      --
      LB_COLA      := FALSE;
      LV_SENTENCIA := 'select count(1) conteo from ' || I.NOMBRE_TABLA;
      LI_CURSOR    := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(LI_CURSOR, LV_SENTENCIA, DBMS_SQL.NATIVE);
      DBMS_SQL.DEFINE_COLUMN(LI_CURSOR, 1, LN_COLA);
      LI_CURSOR1 := DBMS_SQL.EXECUTE(LI_CURSOR);
      LOOP
        EXIT WHEN DBMS_SQL.FETCH_ROWS(LI_CURSOR) = 0;
        DBMS_SQL.COLUMN_VALUE(LI_CURSOR, 1, LN_COLA);
      END LOOP;
    
      UPDATE ALARM_COLAS
         SET LAST_COUNT = LN_COLA, LAST_DATE = SYSDATE
       WHERE NOMBRE_TABLA = I.NOMBRE_TABLA;
    
      COMMIT;
      --
      DBMS_SQL.CLOSE_CURSOR(LI_CURSOR);
      -- 
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(SQLERRM);
  END COLAS;
  --========================================================================--
  PROCEDURE COLA_ESPECIFICA(PV_PROCESO IN VARCHAR2) IS
    CURSOR COLAS IS
      SELECT *
        FROM ALARM_COLAS
       WHERE ESTADO = 'A'
         AND PROCESO = PV_PROCESO;
  
    LI_CURSOR    INTEGER;
    LI_CURSOR1   INTEGER;
    LV_SENTENCIA VARCHAR2(250);
    LN_COLA      NUMBER := 0;
    LB_COLA      BOOLEAN := FALSE;
  
  BEGIN
    FOR I IN COLAS LOOP
      --
      LB_COLA      := FALSE;
      LV_SENTENCIA := 'select count(1) conteo from ' || I.NOMBRE_TABLA;
      LI_CURSOR    := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(LI_CURSOR, LV_SENTENCIA, DBMS_SQL.NATIVE);
      DBMS_SQL.DEFINE_COLUMN(LI_CURSOR, 1, LN_COLA);
      LI_CURSOR1 := DBMS_SQL.EXECUTE(LI_CURSOR);
      LOOP
        EXIT WHEN DBMS_SQL.FETCH_ROWS(LI_CURSOR) = 0;
        DBMS_SQL.COLUMN_VALUE(LI_CURSOR, 1, LN_COLA);
      END LOOP;
    
      UPDATE ALARM_COLAS
         SET LAST_COUNT = LN_COLA, LAST_DATE = SYSDATE
       WHERE NOMBRE_TABLA = I.NOMBRE_TABLA;
    
      COMMIT;
      --
      DBMS_SQL.CLOSE_CURSOR(LI_CURSOR);
      -- 
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(SQLERRM);
  END COLA_ESPECIFICA;
  --========================================================================--
  PROCEDURE ALARMA_COLAS IS
    CURSOR COLAS IS
      SELECT * FROM ALARM_COLAS WHERE ESTADO = 'A';
  
    CURSOR TELEFONOS IS
      SELECT TELEFONO FROM ALARMAS_TELEFONOS;
  
    LI_CURSOR    INTEGER;
    LI_CURSOR1   INTEGER;
    LV_SENTENCIA VARCHAR2(400);
    LV_MSG       VARCHAR2(2500);
    LN_COLA      NUMBER := 0;
    LB_COLA      BOOLEAN := FALSE;
    HORA         VARCHAR2(20);
  BEGIN
    FOR I IN COLAS LOOP
      --
      LB_COLA      := FALSE;
      LV_SENTENCIA := 'select count(1) from ' || I.NOMBRE_TABLA;
      LI_CURSOR    := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(LI_CURSOR, LV_SENTENCIA, DBMS_SQL.NATIVE);
      DBMS_SQL.DEFINE_COLUMN(LI_CURSOR, 1, LN_COLA);
      LI_CURSOR1 := DBMS_SQL.EXECUTE(LI_CURSOR);
      LOOP
        EXIT WHEN DBMS_SQL.FETCH_ROWS(LI_CURSOR) = 0;
        DBMS_SQL.COLUMN_VALUE(LI_CURSOR, 1, LN_COLA);
      END LOOP;
    
      HORA := TO_CHAR(SYSDATE, 'hh24:mi:ss');
      UPDATE ALARM_COLAS
         SET LAST_COUNT = LN_COLA, LAST_DATE = SYSDATE
       WHERE NOMBRE_TABLA = I.NOMBRE_TABLA;
      COMMIT;
    
      IF LN_COLA > I.MAX_TX_COLA /*AND HORA > TO_CHAR('07:30:00')*/ AND
         I.FRECUENCIA_EJECUCIOIN = 'T' THEN
        LV_MSG := 'ALARMA: BSCS';
        LV_MSG := LV_MSG || ' Tabla ' || UPPER(I.NOMBRE_TABLA) || ' tiene ' ||
                  LN_COLA || ' Transacciones en cola' || CHR(13) || CHR(13) ||
                  I.MENSAJE || CHR(13);
        GSI_ALARMAS.SEND_EMAIL(LV_MSG,
                                'Problemas de encolamiento BSCS -> ' ||
                                I.NOMBRE_TABLA,
                                I.GRUPO);
        FOR FONO IN TELEFONOS LOOP
          PORTA.SWK_SMS.SEND@AXIS('sms', FONO.TELEFONO, LV_MSG, LV_MSG);
        END LOOP;
      
      END IF;
    
      IF LN_COLA > I.MAX_TX_COLA /*AND HORA > TO_CHAR('07:30:00')*/ AND
         I.FRECUENCIA_EJECUCIOIN = 'P' THEN
        IF I.HORA_EJECUCION = TO_CHAR(SYSDATE, 'hh24:mi') THEN
          LV_MSG := 'ALARMA: BSCS';
          LV_MSG := LV_MSG || ' Tabla ' || UPPER(I.NOMBRE_TABLA) ||
                    ' tiene ' || LN_COLA || ' Transacciones en cola' ||
                    CHR(13) || CHR(13) || I.MENSAJE || CHR(13);
          GSI_ALARMAS.SEND_EMAIL(LV_MSG,
                                  'Problemas de encolamiento BSCS -> ' ||
                                  I.NOMBRE_TABLA,
                                  I.GRUPO);
          FOR FONO IN TELEFONOS LOOP
            SWK_SMS.SEND@AXIS('sms', FONO.TELEFONO, LV_MSG, LV_MSG);
          END LOOP;
        END IF;
      
      END IF;
      --
      DBMS_SQL.CLOSE_CURSOR(LI_CURSOR);
      -- 
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(SQLERRM);
  END ALARMA_COLAS;

  --========================================================================--

  PROCEDURE SEND_EMAIL(PV_MSG     IN VARCHAR2,
                       PV_SUBJECT IN VARCHAR2,
                       GRUPO      IN NUMBER) IS
  
    CURSOR EMAIL_ALARMA(LN_GRUPO NUMBER) IS
      SELECT TO1, CC, CCO FROM ALARM_EMAIL WHERE GRUPO = LN_GRUPO;
  
    LV_MSG     VARCHAR2(1000);
    LV_SUBJECT VARCHAR2(1000);
    LV_FIRMA   VARCHAR2(150) := CHR(13) || 'BSCS' || CHR(13) || '130.2.18.14' ||
                                CHR(13) || '@bscs' || CHR(13) ||
                                TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
  BEGIN
  
    LV_SUBJECT := PV_SUBJECT;
    LV_MSG     := PV_MSG || CHR(13) || CHR(13) || CHR(13) || LV_FIRMA;
    --
  
    FOR J IN EMAIL_ALARMA(GRUPO) LOOP
      --
      porta.send_mail.mail@axis(FROM_NAME => 'BSCS@conecel.com',
                     TO_NAME   => J.TO1,
                     CC        => J.CC,
                     CCO       => J.CCO,
                     SUBJECT   => LV_SUBJECT,
                     MESSAGE   => LV_MSG);
    END LOOP;
  
  END SEND_EMAIL;
  --========================================================================--
END GSI_ALARMAS;
/
