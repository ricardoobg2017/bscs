create or replace package COK_DATOS_REPLICA_NEG is

  PROCEDURE CO_REPLICA_DATOS(PV_ERROR OUT VARCHAR2, PN_ERROR OUT NUMBER);

end COK_DATOS_REPLICA_NEG;
/
create or replace package body COK_DATOS_REPLICA_NEG is

  PROCEDURE CO_REPLICA_DATOS(PV_ERROR OUT VARCHAR2, PN_ERROR OUT NUMBER) IS
  
    LN_ERROR NUMBER;
  
    LV_ERROR       VARCHAR2(4000);
    lv_programa    varchar2(200) := 'COK_DATOS_REPLICA_NEG.CO_REPLICA_DATOS';
    lv_sentencia   varchar2(2000) := null;
    Lv_descripcion varchar2(200) := null;
    lv_id_proceso  varchar2(50);
    SALIR EXCEPTION;
  
  BEGIN
  
    scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'SCP_QUERY_TRUNCATE_CLI_NEG',
                                                pv_id_proceso   => lv_id_proceso,
                                                pv_valor        => lv_sentencia,
                                                pv_descripcion  => Lv_descripcion,
                                                pn_error        => LN_ERROR,
                                                pv_error        => Lv_error);
  
    IF Lv_error IS NOT NULL THEN
      PN_ERROR := -1;
      PV_ERROR := 'Error  ' || Lv_error || ' - ' || lv_programa || ' - ' ||
                  'al extraer parametro SCP_QUERY_DELET_NEG  -';
      RAISE SALIR;
    
    END IF;
    execute immediate lv_sentencia;
  
    scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'SCP_QUERY_REP_NEGOCIACION',
                                                pv_id_proceso   => lv_id_proceso,
                                                pv_valor        => lv_sentencia,
                                                pv_descripcion  => Lv_descripcion,
                                                pn_error        => PN_ERROR,
                                                pv_error        => lv_error);
  
    IF Lv_error IS NOT NULL THEN
      PN_ERROR := -1;
      PV_ERROR := 'Error ' || Lv_error || ' - ' || lv_programa || ' - ' ||
                  ' al extraer parametro SCP_QUERY_REPLICACION -';
      RAISE SALIR;
    
    END IF;
    execute immediate lv_sentencia;
    COMMIT;
  EXCEPTION
    WHEN SALIR THEN
      COMMIT;
    
    WHEN OTHERS THEN
      PN_ERROR := -2;
      PV_ERROR := 'Error en el programa ' || lv_programa || ' - ' ||
                  sqlerrm;
    
  END CO_REPLICA_DATOS;
end COK_DATOS_REPLICA_NEG;
/
