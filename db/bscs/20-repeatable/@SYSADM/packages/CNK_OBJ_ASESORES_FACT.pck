CREATE OR REPLACE PACKAGE CNK_OBJ_ASESORES_FACT
/*
 ||   Name       : CNK_OBJ_ASESORES_FACT
 ||   Created on : 13-FEB-07
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */
 IS
    /*HELP
    Overview of CNK_OBJ_ASESORES_FACT
    HELP*/

    /*EXAMPLES
    Examples of test
    EXAMPLES*/

    /* Constants */

    /* Exceptions */

    /* Variables */

    /* Toggles */

    /* Windows */

    /* Programs */

    PROCEDURE CNP_HELP(CONTEXT_IN IN VARCHAR2 := NULL);

    PROCEDURE CNP_INSERTAR
    (
        PN_ID_SECUENCIA    IN OUT CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PN_ID_FUERZA_VENTA IN CN_ASESORES_FACTURAS.ID_FUERZA_VENTA%TYPE,
        PV_COD_ACTIVACION  IN CN_ASESORES_FACTURAS.COD_ACTIVACION%TYPE,
        PV_RUC             IN CN_ASESORES_FACTURAS.RUC%TYPE,
        PV_ANIO            IN CN_ASESORES_FACTURAS.ANIO%TYPE,
        PV_MES             IN CN_ASESORES_FACTURAS.MES%TYPE,
        PN_PARTICIPACION   IN CN_ASESORES_FACTURAS.PARTICIPACION%TYPE,
        PV_CODIGO_DOC      IN CN_ASESORES_FACTURAS.CODIGO_DOC%TYPE,
        PN_FACTURADO       IN CN_ASESORES_FACTURAS.FACTURADO%TYPE,
        PV_MSG_ERROR       IN OUT VARCHAR2
        --
    );
    --
    --
    PROCEDURE CNP_ACTUALIZAR
    (
        PN_ID_SECUENCIA    IN CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PN_ID_FUERZA_VENTA IN CN_ASESORES_FACTURAS.ID_FUERZA_VENTA%TYPE,
        PV_COD_ACTIVACION  IN CN_ASESORES_FACTURAS.COD_ACTIVACION%TYPE,
        PV_RUC             IN CN_ASESORES_FACTURAS.RUC%TYPE,
        PV_ANIO            IN CN_ASESORES_FACTURAS.ANIO%TYPE,
        PV_MES             IN CN_ASESORES_FACTURAS.MES%TYPE,
        PN_PARTICIPACION   IN CN_ASESORES_FACTURAS.PARTICIPACION%TYPE,
        PV_CODIGO_DOC      IN CN_ASESORES_FACTURAS.CODIGO_DOC%TYPE,
        PN_FACTURADO       IN CN_ASESORES_FACTURAS.FACTURADO%TYPE,
        PV_MSG_ERROR       IN OUT VARCHAR2
        --
    );

    --
    PROCEDURE CNP_ELIMINAR
    (
        PN_ID_SECUENCIA IN CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PV_MSG_ERROR    IN OUT VARCHAR2
    );
    --
--

END CNK_OBJ_ASESORES_FACT;
/
CREATE OR REPLACE PACKAGE BODY CNK_OBJ_ASESORES_FACT
/*
 ||   Name       : CNK_OBJ_ASESORES_FACT
 ||   Created on : 13-FEB-07
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by System Department CONECEL S.A. 2003
 */
 IS
    PROCEDURE CNP_HELP(CONTEXT_IN IN VARCHAR2 := NULL) IS
    BEGIN
        NULL;
    END CNP_HELP;

    PROCEDURE CNP_INSERTAR
    (
        PN_ID_SECUENCIA    IN OUT CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PN_ID_FUERZA_VENTA IN CN_ASESORES_FACTURAS.ID_FUERZA_VENTA%TYPE,
        PV_COD_ACTIVACION  IN CN_ASESORES_FACTURAS.COD_ACTIVACION%TYPE,
        PV_RUC             IN CN_ASESORES_FACTURAS.RUC%TYPE,
        PV_ANIO            IN CN_ASESORES_FACTURAS.ANIO%TYPE,
        PV_MES             IN CN_ASESORES_FACTURAS.MES%TYPE,
        PN_PARTICIPACION   IN CN_ASESORES_FACTURAS.PARTICIPACION%TYPE,
        PV_CODIGO_DOC      IN CN_ASESORES_FACTURAS.CODIGO_DOC%TYPE,
        PN_FACTURADO       IN CN_ASESORES_FACTURAS.FACTURADO%TYPE,
        PV_MSG_ERROR       IN OUT VARCHAR2
        --
    ) IS
        /*
        ||   Name       : CNP_INSERTAR
        ||   Created on : 13-FEB-07
        ||   Comments   : Standalone Delete Procedure
        ||   automatically generated using the PL/Vision building blocks
        ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
        ||   Adjusted by System Department CONECEL S.A. 2003
        */
        LV_APLICACION VARCHAR2(100) := 'CNK_OBJ_ASESORES_FACT.CNP_INSERTAR';
    BEGIN
    
        PV_MSG_ERROR := NULL;
        SELECT NVL(MAX(ID_SECUENCIA), 1) + 1
        INTO   PN_ID_SECUENCIA
        FROM   CN_ASESORES_FACTURAS;
    
        INSERT INTO CN_ASESORES_FACTURAS
            (ID_SECUENCIA,
             ID_FUERZA_VENTA,
             COD_ACTIVACION,
             RUC,
             ANIO,
             MES,
             PARTICIPACION,
             CODIGO_DOC,
             FACTURADO,
             FECHA_REG)
        VALUES
            (PN_ID_SECUENCIA,
             PN_ID_FUERZA_VENTA,
             PV_COD_ACTIVACION,
             PV_RUC,
             PV_ANIO,
             PV_MES,
             PN_PARTICIPACION,
             PV_CODIGO_DOC,
             PN_FACTURADO,
             SYSDATE);
    
    EXCEPTION
        -- Exception
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
    END CNP_INSERTAR;

    PROCEDURE CNP_ACTUALIZAR
    (
        PN_ID_SECUENCIA    IN CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PN_ID_FUERZA_VENTA IN CN_ASESORES_FACTURAS.ID_FUERZA_VENTA%TYPE,
        PV_COD_ACTIVACION  IN CN_ASESORES_FACTURAS.COD_ACTIVACION%TYPE,
        PV_RUC             IN CN_ASESORES_FACTURAS.RUC%TYPE,
        PV_ANIO            IN CN_ASESORES_FACTURAS.ANIO%TYPE,
        PV_MES             IN CN_ASESORES_FACTURAS.MES%TYPE,
        PN_PARTICIPACION   IN CN_ASESORES_FACTURAS.PARTICIPACION%TYPE,
        PV_CODIGO_DOC      IN CN_ASESORES_FACTURAS.CODIGO_DOC%TYPE,
        PN_FACTURADO       IN CN_ASESORES_FACTURAS.FACTURADO%TYPE,
        PV_MSG_ERROR       IN OUT VARCHAR2
    ) AS
        /*
        ||   Name       : CNP_ACTUALIZAR
        ||   Created on : 13-FEB-07
        ||   Comments   : Standalone Delete Procedure
        ||   automatically generated using the PL/Vision building blocks
        ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
        ||   Adjusted by System Department CONECEL S.A. 2003
        */
        CURSOR C_TABLE_CUR IS
            SELECT *
            FROM   CN_ASESORES_FACTURAS
            WHERE  ID_SECUENCIA = PN_ID_SECUENCIA
            FOR    UPDATE;
        LC_CURRENTROW CN_ASESORES_FACTURAS%ROWTYPE;
        LE_NODATAUPDATED EXCEPTION;
    
        LV_APLICACION VARCHAR2(100) := 'CNK_OBJ_ASESORES_FACT.CNP_ACTUALIZAR';
    BEGIN
    
        PV_MSG_ERROR := NULL;
        OPEN C_TABLE_CUR;
        FETCH C_TABLE_CUR
            INTO LC_CURRENTROW;
        IF C_TABLE_CUR%NOTFOUND THEN
            RAISE LE_NODATAUPDATED;
        END IF;
    
        UPDATE CN_ASESORES_FACTURAS
        SET    -- Revisar los campos que desea actualizar
               ID_FUERZA_VENTA = NVL(PN_ID_FUERZA_VENTA, ID_FUERZA_VENTA),
               COD_ACTIVACION  = NVL(PV_COD_ACTIVACION, COD_ACTIVACION),
               RUC             = NVL(PV_RUC, RUC),
               ANIO            = NVL(PV_ANIO, ANIO),
               MES             = NVL(PV_MES, MES),
               PARTICIPACION   = NVL(PN_PARTICIPACION, PARTICIPACION),
               CODIGO_DOC      = NVL(PV_CODIGO_DOC, CODIGO_DOC),
               FACTURADO       = NVL(PN_FACTURADO, FACTURADO),
               FECHA_REG       = SYSDATE
        WHERE  CURRENT OF C_TABLE_CUR;
    
        CLOSE C_TABLE_CUR;
    
    EXCEPTION
        -- Exception
        WHEN LE_NODATAUPDATED THEN
            PV_MSG_ERROR := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || LV_APLICACION;
            CLOSE C_TABLE_CUR;
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
            CLOSE C_TABLE_CUR;
    END CNP_ACTUALIZAR;

    PROCEDURE CNP_ELIMINAR
    (
        PN_ID_SECUENCIA IN CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE,
        PV_MSG_ERROR    IN OUT VARCHAR2
    ) AS
        /*
        ||   Name       : CNP_ELIMINAR
        ||   Created on : 13-FEB-07
        ||   Comments   : Standalone Delete Procedure
        ||   automatically generated using the PL/Vision building blocks
        ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
        ||   Adjusted by System Department CONECEL S.A. 2003
        */
        CURSOR C_TABLE_CUR IS
            SELECT *
            FROM   CN_ASESORES_FACTURAS
            WHERE  ID_SECUENCIA = PN_ID_SECUENCIA
            FOR    UPDATE;
        LC_CURRENTROW CN_ASESORES_FACTURAS%ROWTYPE;
        LE_NODATADELETED EXCEPTION;
        LV_APLICACION VARCHAR2(100) := 'CNK_OBJ_ASESORES_FACT.CNP_ELIMINAR';
    BEGIN
    
        PV_MSG_ERROR := NULL;
        OPEN C_TABLE_CUR;
        FETCH C_TABLE_CUR
            INTO LC_CURRENTROW;
        IF C_TABLE_CUR%NOTFOUND THEN
            RAISE LE_NODATADELETED;
        END IF;
    
        DELETE FROM CN_ASESORES_FACTURAS
        WHERE  CURRENT OF C_TABLE_CUR;
    
        CLOSE C_TABLE_CUR;
    
    EXCEPTION
        -- Exception
        WHEN LE_NODATADELETED THEN
            PV_MSG_ERROR := 'El registro que desea borrar no existe. ' || LV_APLICACION;
            CLOSE C_TABLE_CUR;
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
            CLOSE C_TABLE_CUR;
    END CNP_ELIMINAR;
    --

END CNK_OBJ_ASESORES_FACT;
/

