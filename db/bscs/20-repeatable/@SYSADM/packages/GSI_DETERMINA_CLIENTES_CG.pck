CREATE OR REPLACE package GSI_DETERMINA_CLIENTES_CG is

  -- Author  : Natividad del Rocio Mantilla L�pez.
  -- Created : 13/12/2007 10:52:32
  -- Purpose : Proceso que determina los clientes a correr  Control Group para cada corte
  

PROCEDURE PR_CARGA_INICIAL(Sesiones in number);
PROCEDURE PR_DISTRIBUYE (Sesiones in number);
PROCEDURE CLIENTES_ACTIVOS_ANTES_CORTE(Sesion in number);
PROCEDURE CLIENTES_CON_MOV_DENTRO_CORTE(Sesion in number);


end GSI_DETERMINA_CLIENTES_CG;
/
CREATE OR REPLACE package body GSI_DETERMINA_CLIENTES_CG is


PROCEDURE PR_CARGA_INICIAL(Sesiones in number) IS

  CURSOR C1 IS 
     SELECT /*+ rule */A.ROWID, C.ID_CICLO
     FROM  CHIO_CUENTAS A, 
           CUSTOMER_ALL B,
           fa_ciclos_axis_bscs C
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
     AND   B.BILLCYCLE = c.id_ciclo_admin;  


  numero decimal(10):=0;
  
  lvSentencia VARCHAR2(2000);
  lvMensErr   VARCHAR2(2000);
  

BEGIN
    
    lvSentencia := 'TRUNCATE TABLE CHIO_CUENTAS';        
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    lvSentencia := 'TRUNCATE TABLE CHIO_ESTATUS_ACTIVO';        
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    
    lvSentencia := 'TRUNCATE TABLE CLIENTES_CON_MOVIMIENTO_CORTE';        
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

   
    INSERT INTO CHIO_CUENTAS
    select /*+rule*/ customer_id, null,0,'X','X'
    from  customer_all a
    where a.csdeactivated is null;-- inserto todos los clientes que esten activos    
    COMMIT;
    
    ---Le actualizo el ciclo, le pongo 01,02,03
    FOR I IN C1 LOOP   
    
         Numero:= Numero + 1;
         
         UPDATE CHIO_CUENTAS
         SET    CICLO = I.ID_CICLO
         WHERE ROWID = I.ROWID;   
         
         if mod(numero,300)=0 then
             commit;
         end if;
        
    END LOOP;
   commit;
   
   -- Borro los que no tienen asignado un ciclo v�lido
   delete from CHIO_CUENTAS where ciclo is null;
   commit;    

   -- Hago la distribucion de clientes para N procesadores
   PR_DISTRIBUYE(Sesiones);
   commit;

END PR_CARGA_INICIAL;



PROCEDURE PR_DISTRIBUYE (Sesiones in number) IS

Cursor Clientes is
       select customer_id  from CHIO_CUENTAS;

Registros number:=0;
Contador number:=1;

Numero decimal(10):=0;

BEGIN

      select count(*) into Registros from CHIO_CUENTAS ;
      
      If Registros >0 then    
            
            For I in Clientes Loop
                                    
                Numero:= Numero + 1;
            
                Update CHIO_CUENTAS
                set    Proceso =Contador
                where  customer_id = I.customer_id;
                
                If Contador < Sesiones then
                   Contador :=Contador+1;
                else
                   Contador :=1;
                End if;
                            
                if mod(numero,300)=0 then
                    commit;
                end if;                
              
            End Loop;            
            commit;
            
      End if;

END PR_DISTRIBUYE;



PROCEDURE CLIENTES_ACTIVOS_ANTES_CORTE(Sesion in number) IS       

    CURSOR C1 IS 
           select /*+ rule */ b.co_id, a.ciclo, a.customer_id
           from   CHIO_CUENTAS A, CONTRACT_ALL B
           WHERE  a.customer_id = b.customer_id
           AND    A.proceso=Sesion and A.procesado='X';
                
                
    CURSOR C2(Pt_co_id integer) IS 
            select /*+ rule */b.co_id, b.sncode, b.status, b.valid_from_date 
            from  (select co_id, sncode, max(v.HISTNO) HISTNO 
                   from   pr_serv_status_hist v
                   where  co_id = Pt_co_id
                   group by co_id, sncode) a,
                   pr_serv_status_hist b
            where  a.co_id = b.co_id
            and    a.sncode  = b.sncode
            and    a.HISTNO= b.HISTNO;
    
    
Numero decimal(10):=0;     

BEGIN

  FOR I IN C1 LOOP           
        FOR J IN C2(i.co_id) LOOP               
                          
           IF (J.status = 'A') THEN
           
                 Numero:= Numero + 1;
           
                 if (I.CICLO = '01') then                                      
                 
                     if (J.valid_from_date <= to_Date('23/12/2007','dd/mm/yyyy')) then
                        insert into CHIO_ESTATUS_ACTIVO
                        values (I.CUSTOMER_ID, J.co_id, J.sncode, J.status, I.CICLO);                       
                     end if;
                                          
                 else       
                           
                     if (I.CICLO = '02') then                     
                         if (J.valid_from_date <= to_Date('07/12/2007','dd/mm/yyyy')) then
                            insert into CHIO_ESTATUS_ACTIVO
                            values (I.CUSTOMER_ID, J.co_id, J.sncode, J.status, I.CICLO);                       
                         end if;
                     else
                     
                          if (J.valid_from_date <= to_Date('14/12/2007','dd/mm/yyyy')) then
                              insert into CHIO_ESTATUS_ACTIVO
                              values (I.CUSTOMER_ID, J.co_id, J.sncode, J.status, I.CICLO);                       
                          end if;

                     end if;                                                                     
                 end if;                                  
            
                 if mod(numero,300)=0 then
                    commit;
                 end if;
             
           END IF;
                            
        END LOOP;
        
        
        update  CHIO_CUENTAS g
        set     procesado = sesion
        where   g.customer_id= I.customer_id;
        commit;        
                
  END LOOP;
  COMMIT;


END CLIENTES_ACTIVOS_ANTES_CORTE;


PROCEDURE CLIENTES_CON_MOV_DENTRO_CORTE(Sesion in number) IS

    CURSOR C1 IS 
           select /*+ rule */ b.co_id, a.ciclo, a.customer_id
           from   CHIO_CUENTAS A, CONTRACT_ALL B
           WHERE  a.customer_id = b.customer_id
           AND    a.proceso=Sesion and a.procesado2='X';
              
    ---Determino si el cliente tuvo algun movimiento de sus servicios dentro del periodo de facturacion            
    CURSOR C_Dia24(Pt_co_id integer) IS     
            select /*+RULE*/ DISTINCT CO_ID from pr_serv_status_hist k
            where k.co_id = Pt_co_id  
            and   k.valid_from_date >= to_date('24/11/2007','dd/mm/yyyy')
            and   k.valid_from_date <= to_date('23/12/2007','dd/mm/yyyy');

    CURSOR C_Dia08(Pt_co_id integer) IS     
        select /*+RULE*/ DISTINCT CO_ID from pr_serv_status_hist k
        where k.co_id = Pt_co_id  
        and   k.valid_from_date >= to_date('08/11/2007','dd/mm/yyyy')
        and   k.valid_from_date <= to_date('07/12/2007','dd/mm/yyyy');
    
    
    CURSOR C_Dia15(Pt_co_id integer) IS     
    select /*+RULE*/ DISTINCT CO_ID from pr_serv_status_hist k
    where k.co_id = Pt_co_id  
    and   k.valid_from_date >= to_date('15/11/2007','dd/mm/yyyy')
    and   k.valid_from_date <= to_date('14/12/2007','dd/mm/yyyy');
    
        
    Numero decimal(10):=0;     
    

BEGIN

  FOR I IN C1 LOOP          
          
      if I.CICLO = '01' then
    
            FOR J IN C_Dia24(i.co_id) LOOP              
            
                Numero:= Numero + 1;
         
                insert into CLIENTES_CON_MOVIMIENTO_CORTE
                VALUES (I.CUSTOMER_ID, J.co_id,I.CICLO);  
                
                if mod(numero,300)=0 then
                   commit;
                end if;    
                
            END LOOP;    
            COMMIT;
        
      else
      
         if (I.CICLO = '02') then      
         
             FOR K IN C_Dia08(i.co_id) LOOP              
             
                  Numero:= Numero + 1;
       
                  insert into CLIENTES_CON_MOVIMIENTO_CORTE
                  VALUES (I.CUSTOMER_ID, K.co_id,I.CICLO);  
                  
                  if mod(numero,300)=0 then
                     commit;
                  end if;    
              
             END LOOP;     
             COMMIT;   
            
         else    
             
             FOR L IN C_Dia15(i.co_id) LOOP              
             
                  Numero:= Numero + 1;
       
                  insert into CLIENTES_CON_MOVIMIENTO_CORTE
                  VALUES (I.CUSTOMER_ID, L.co_id,I.CICLO);  
                  
                  if mod(numero,300)=0 then
                     commit;
                  end if;    
              
             END LOOP;     
             COMMIT;            
         
         end if;      
      
      end if;    
 
      update  CHIO_CUENTAS g
      set     procesado2 = sesion
      where   g.customer_id= I.customer_id;
      commit;          
                 
        
  END LOOP;   
  COMMIT;

END CLIENTES_CON_MOV_DENTRO_CORTE;


end GSI_DETERMINA_CLIENTES_CG;
/

