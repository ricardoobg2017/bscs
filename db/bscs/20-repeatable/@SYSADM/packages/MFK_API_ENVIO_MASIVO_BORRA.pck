create or replace package MFK_API_ENVIO_MASIVO_BORRA is

 PROCEDURE MFP_ENVIO(PN_IDENVIO          MF_MENSAJES.IDENVIO%TYPE,
                     PN_SECUENCIA_ENVIO  MF_MENSAJES.SECUENCIA_ENVIO%TYPE);

                 

end MFK_API_ENVIO_MASIVO_BORRA;
/
create or replace package body MFK_API_ENVIO_MASIVO_BORRA is

PROCEDURE MFP_ENVIO(PN_IDENVIO          MF_MENSAJES.IDENVIO%TYPE,
                    PN_SECUENCIA_ENVIO  MF_MENSAJES.SECUENCIA_ENVIO%TYPE) IS

  ln_secuencia   NUMBER;
  lv_flag        VARCHAR2(1):=null;
  
  
  CURSOR c_Mensajes_Enviar IS
  
    SELECT M.DESTINATARIO, M.MENSAJE
    FROM  MF_MENSAJES_BORRAR M
    WHERE M.SECUENCIA_ENVIO = PN_SECUENCIA_ENVIO
    AND   M.IDENVIO = PN_IDENVIO
    AND   M.ESTADO  = 'A';

  lv_puerto varchar2(100);
  lv_servidor varchar2(100);
  lv_mensaje_retorno varchar2(100);
  ln_contador_mensajes number:=0;
  ld_fecha_envio      date;
  ln_minuto number:=0;  

BEGIN


         

         
         --Obtenemos el tipo de esquema configurado para el envio de SMS Grupal
         begin
           select valor 
             into lv_flag 
             from co_parametros 
            where id_parametro = 'SMS_GRUPAL_SCO';
         exception
           when others then
             lv_flag:='A';
         end;
         
         if lv_flag = 'I' then
            begin
              INSERT INTO SMG_ENVIO_SCO
                (SELECT SUBSTR(M.DESTINATARIO,5) TELEFONO, --TELEFONO
                        1,                                 --ID_THREAD
                        ln_secuencia,                      --ID_SMG_CONF
                        M.MENSAJE,                         --MENSAJE
                        --TRUNC(SYSDATE)+(9/24),
                        --Ya que el proceso de envio de mensajes tiene politicas y prioridades 
                        --en el envio se grabara con un a�o de retraso para que el proceso tome primero estos sms
                        (trunc(sysdate)-365)+(9/24),       --FECHA_ENVIO
                        1                                  --MEDIO_TRANS
                 FROM MF_MENSAJES_BORRAR M
                 WHERE M.SECUENCIA_ENVIO = PN_SECUENCIA_ENVIO
                 AND M.IDENVIO = PN_IDENVIO
                 AND M.ESTADO  = 'A');
            end;
         elsif lv_flag = 'A' then
            --===============================================================================
            -- Se agrega logica para que apunte al gestor de mensajes (<<)
            --===============================================================================
            begin
               ld_fecha_envio:=(trunc(sysdate))+(9/24);
               --si la fecha calculada ya paso, se deja para el siguiente dia
               if (ld_fecha_envio<sysdate) then ld_fecha_envio:=ld_fecha_envio+1; end if;
               for i in c_Mensajes_Enviar loop
                   notificaciones_dat.gvk_api_notificaciones.
                           gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio=> ld_fecha_envio,
                                                                     pd_fecha_expiracion =>ld_fecha_envio+2/24,
                                                                     pv_asunto => '.',
                                                                     pv_mensaje => i.mensaje,                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                                                                     pv_destinatario => i.destinatario,
                                                                     pv_remitente => 'PORTA',
                                                                     pv_tipo_registro => 'S',
                                                                     pv_clase => 'COB',
                                                                     pv_puerto => lv_puerto,
                                                                     pv_servidor => lv_servidor,
                                                                     pv_max_intentos => '3',
                                                                     pv_id_usuario => 'BSCS_COBRANZAS',
                                                                     pv_mensaje_retorno => lv_mensaje_retorno);
               

                    ln_contador_mensajes:= ln_contador_mensajes+1;
                    if (mod(ln_contador_mensajes,20000)=0) then
                          ln_minuto:=ln_minuto+10;
                    end if;
                    if  ( mod(ln_contador_mensajes,1000)=0) then 
                          commit;
                    end if;                   
               end loop;
               commit;
               begin
                   dbms_session.close_database_link(dblink => 'NOTIFICACIONES');
               exception when others then null;
               end;

            end;
            --===============================================================================
            -- Se agrega logica para que apunte al gestor de mensajes  (>>)
            --===============================================================================
         end if;
         
         COMMIT;
         
         UPDATE MF_MENSAJES_BORRAR M
         SET M.ESTADO = 'I'
         WHERE M.SECUENCIA_ENVIO =  PN_SECUENCIA_ENVIO
         AND   M.IDENVIO         =  PN_IDENVIO
         AND   M.ESTADO          = 'A';
         
         COMMIT;
       
             
END MFP_ENVIO;

end MFK_API_ENVIO_MASIVO_BORRA;
/
