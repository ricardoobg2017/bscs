CREATE OR REPLACE PACKAGE gsib_envia_tramas_sap IS

  -- 10189
  -- SIS Luis Flores
  -- PDS Fernando Ortega
  -- RGT Ney Miranda
  -- Ultimo Cambio: 30/07/2015 
  -- Nueva logica llamar a la logica para SAP

  PROCEDURE graba_safi_fact_sap(pdfechcierreperiodo IN DATE,
                                pv_producto         IN VARCHAR2, -- NCA 'DTH' solo DTH, 'VOZ' todos los productos exceptio DTH, nulo para procesar voz y dth.  
                                pv_ejecucion        IN VARCHAR2, -- tipo de ejecucion 'C' commit 'CG' control group 
                                pn_id_transaccion   IN NUMBER,
                                resultado           OUT VARCHAR2);

  PROCEDURE fin_provision_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2);

  PROCEDURE fin_provision_dth_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2);

  PROCEDURE dth_devengamiento_pre_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2);

  PROCEDURE reenvio_por_id_poliza(id_polizas IN VARCHAR2, pv_escenario VARCHAR2, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2);

  PROCEDURE consume_webservice(pv_trama IN CLOB, pv_error OUT VARCHAR2);

  --PROCEDURE envia_correo_xml(pc_xml IN CLOB);

  PROCEDURE notifica_envio_polizas(pv_escenario IN VARCHAR2, pv_producto IN VARCHAR2, pd_fecha_corte IN DATE);

END gsib_envia_tramas_sap;
/
CREATE OR REPLACE PACKAGE BODY gsib_envia_tramas_sap IS

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE graba_safi_fact_sap(pdfechcierreperiodo IN DATE,
                                pv_producto         IN VARCHAR2, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
                                pv_ejecucion        IN VARCHAR2, -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP 
                                pn_id_transaccion   IN NUMBER,
                                resultado           OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_ENVIA_TRAMAS_SAP.GRABA_SAFI_FACT_SAP';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
  
    le_error EXCEPTION;
    lv_error               VARCHAR2(5000);
    error                  VARCHAR2(3000);
    msj_tramas             VARCHAR2(5000);
    lv_producto_a_procesar VARCHAR2(100) := pv_producto;
    ln_contador            NUMBER := 0;
    lv_valida_envio        NUMBER := 0;
    --CURSOR OBTIENE TRAMAS DE FACTURACION DTH
    CURSOR c_obtiene_tramas_dth(pdfechcierreperiodo DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pdfechcierreperiodo
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'FACTURACION DTH%';
  
    --CURSOR OBTIENE TRAMAS DE FACTURACION BSCS
  
    CURSOR c_obtiene_tramas_voz(pdfechcierreperiodo DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pdfechcierreperiodo
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'FACTURACION BSCS%';
  
    --CURSOR OBTIENE TRAMAS DE FACTURACION BSCS Y DTH
  
    CURSOR c_obtiene_tramas_voz_dth(pdfechcierreperiodo DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pdfechcierreperiodo
         AND (t.estado = 'CREADA' OR t.estado = 'NO ENVIADA')
         AND t.referencia LIKE 'FACTURACION%';
  
  BEGIN
  
    IF lv_producto_a_procesar IS NULL THEN
      lv_producto_a_procesar := ' VOZ Y DTH (NULL)';
    END IF;
  
    gsib_security.dotraceh(pv_aplicacion,
                           'Se ejecuto el procedimiento que ENVIA las tramas del GRABA_SAFI_FACT_SAP con fecha de corte :' || pdfechcierreperiodo ||
                           ' ,para el producto : ' || lv_producto_a_procesar,
                           pn_id_transaccion);
  
    IF pv_producto IS NOT NULL THEN
      IF pv_producto NOT IN ('DTH', 'VOZ') THEN
        lv_error := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ => ';
        gsib_security.dotracem(pv_aplicacion, 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ', pn_id_transaccion);
      
        RAISE le_error;
      END IF;
    END IF;
    --SI EL PRODUCTO ES DTH PROCESO SOLO DTH
    IF pv_producto = 'DTH' THEN
    
      --OBTENGO TRAMAS DTH Y ENVIO
      FOR i IN c_obtiene_tramas_dth(pdfechcierreperiodo) LOOP
        ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      
        gsib_envia_tramas_sap.consume_webservice(i.trama, error);
      
        IF error IS NOT NULL THEN
          lv_valida_envio := lv_valida_envio + 1;
          msj_tramas      := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
          error := NULL;
          gsib_security.dotracem(pv_aplicacion,
                                 'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                                 pn_id_transaccion);
        
        ELSE
        
          msj_tramas := msj_tramas || '; Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
        
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
        
          gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
        
        END IF;
      
      END LOOP;
    
      --SI EL PRODUCTO ES VOZ PROCESO SOLO VOZ
    ELSIF pv_producto = 'VOZ' THEN
    
      FOR i IN c_obtiene_tramas_voz(pdfechcierreperiodo) LOOP
        ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      
        gsib_envia_tramas_sap.consume_webservice(i.trama, error);
      
        IF error IS NOT NULL THEN
          lv_valida_envio := lv_valida_envio + 1;
        
          msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
        
          gsib_security.dotracem(pv_aplicacion,
                                 'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                                 pn_id_transaccion);
        
          error := NULL;
        ELSE
        
          msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
        
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
          
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
          gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
        END IF;
      
      END LOOP;
    
      --SI EL PRODUCTO ES NULL PROCESO DTH Y VOZ
    ELSIF pv_producto IS NULL THEN
    
      FOR i IN c_obtiene_tramas_voz_dth(pdfechcierreperiodo) LOOP
      
        ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      
        gsib_envia_tramas_sap.consume_webservice(i.trama, error);
      
        IF error IS NOT NULL THEN
          lv_valida_envio := lv_valida_envio + 1;
        
          msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
        
          gsib_security.dotracem(pv_aplicacion,
                                 'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                                 pn_id_transaccion);
        
          error := NULL;
        ELSE
        
          msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' => ' || error;
        
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
          
           WHERE f.id_poliza = i.id_poliza;
        
          COMMIT;
          gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
        END IF;
      
      END LOOP;
    
    END IF;
    IF ln_contador > 0 THEN
      IF lv_valida_envio = 0 THEN
        IF pv_ejecucion = 'C' THEN
          notifica_envio_polizas('FACTURACION', pv_producto, pdfechcierreperiodo);
        
          -- SI ESTA HABILITADO EL ENVIO DE LAS TRAMAS A SAP , SE ENVIA UN CORREO A LOS FUNCIONALES CONFIRMADO LA CREACION  Y ENVIO DE LOS ASIENTOS    
        
          /* REGUN.SEND_MAIL.MAIL@COLECTOR('gsibilling@claro.com.ec',
                                           'mgavilanes@conecel.com,lmedina@conecel.com',
                                           'tescalante@conecel.com',
                                           'GSIFacturacion@conecel.com',
                                           'Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  ' ||
                                           to_char(pdfechcierreperiodo,
                                                   'DD/MM/YYYY'),
                                           'Saludos,' || chr(13) || chr(13) ||
                                           'Se informa, se envio a SAP los registros de financiero del cierre de facturaci�n a la fecha ' ||
                                           to_char(pdfechcierreperiodo,
                                                   'DD/MM/YYYY') || chr(13) ||
                                           chr(13) || chr(13) || chr(13) ||
                                           'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).' ||
                                           chr(13) ||
                                           'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||
                                           chr(13) || chr(13) ||
                                           'Atentamente,' || chr(13) ||
                                           chr(13) || 'SIS GSI-Billing.' ||
                                           chr(13) || chr(13) ||
                                           'Conecel.S.A - America Movil.');
             COMMIT;
           
          */
          msj_tramas := msj_tramas || ' Se envio un e-mail a los funcionales confirmando la creacion del asiento ';
          gsib_security.dotracem(pv_aplicacion, '; Se envio un e-mail a los funcionales confirmando la creacion del asiento ', pn_id_transaccion);
        ELSE
        
          msj_tramas := msj_tramas || '; No se envio el e-mail a los funcionales por que el parametro de ejecucion no es C ''COMMIT ''';
          gsib_security.dotracem(pv_aplicacion,
                                 'No se envio el e-mail a los funcionales por que el parametro de ejecucion no es C ''COMMIT ''',
                                 pn_id_transaccion);
        
        END IF;
      ELSE
      
        msj_tramas := msj_tramas || '; No se envio el e-mail a los funcionales por que ocurrio un problema al consumir el WEBSERVICE';
        gsib_security.dotracem(pv_aplicacion,
                               'No se envio el e-mail a los funcionales por que ocurrio un problema al consumir el WEBSERVICE',
                               pn_id_transaccion);
      
      END IF;
    ELSE
      msj_tramas := msj_tramas || ' NO se encontraron tramas que enviar con fecha de corte : ' || pdfechcierreperiodo;
      gsib_security.dotracem(pv_aplicacion, 'NO se encontraron tramas que enviar con fecha de corte : ' || pdfechcierreperiodo, pn_id_transaccion);
    END IF;
  
    resultado := msj_tramas;
  
    gsib_security.dotracem(pv_aplicacion, resultado, pn_id_transaccion);
  
  EXCEPTION
    WHEN le_error THEN
      resultado := lv_error || ' FF' || pv_aplicacion;
      gsib_security.dotraceh(pv_aplicacion, 'ERROR al enviar las Tramas', pn_id_transaccion);
    
    WHEN OTHERS THEN
      resultado := 'ERROR ' || SQLERRM || ' ' || pv_aplicacion;
      gsib_security.dotraceh(pv_aplicacion, 'ERROR al enviar las Tramas', pn_id_transaccion);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE fin_provision_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_ENVIA_TRAMAS_SAP.FIN_PROVISION_SAP';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    error           VARCHAR2(1000);
    msj_tramas      VARCHAR2(3000);
    ln_contador     NUMBER := 0;
    le_no_hay_tramas EXCEPTION;
  
    --CURSOR OBTIENE TRAMAS DE PROVISION BSCS
    CURSOR c_obtiene_tramas_prov_bscs(pd_fecha DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pd_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'PROVISION BSCS%';
  
  BEGIN
  
    gsib_security.dotraceh(pv_aplicacion,
                           'Se ejecuto el procedimiento que envia las tramas de FIN PROVISION_SAP con fecha de corte :' || pd_fecha ||
                           ' ,para el producto : BSCS',
                           pn_id_transaccion);
  
    FOR i IN c_obtiene_tramas_prov_bscs(pd_fecha) LOOP
      ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
    
      gsib_envia_tramas_sap.consume_webservice(i.trama, error);
    
      IF error IS NOT NULL THEN
        msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
        UPDATE gsib_tramas_sap f
           SET f.estado      = 'NO ENVIADA',
               f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
      
        gsib_security.dotracem(pv_aplicacion,
                               'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                               pn_id_transaccion);
      
        error := NULL;
      ELSE
      
        msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' => ' || error;
      
        UPDATE gsib_tramas_sap f
           SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
        
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
        gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
      END IF;
    
    END LOOP;
    IF ln_contador <= 0 THEN
      msj_tramas := 'No se encontramas Polizas que enviar con fecha de corte ' || pd_fecha;
      RAISE le_no_hay_tramas;
    
    ELSE
      notifica_envio_polizas('PROVISION', 'VOZ', pd_fecha);
      gsib_security.dotracem(pv_aplicacion, ' Se envio un e-mail a los funcionales confirmando la creacion del asiento ', pn_id_transaccion);
    
    END IF;
  
    pv_error := msj_tramas;
  
  EXCEPTION
    WHEN le_no_hay_tramas THEN
      pv_error := msj_tramas || ' ' || pv_aplicacion;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    WHEN OTHERS THEN
      pv_error := msj_tramas || ' ' || pv_aplicacion;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE fin_provision_dth_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_ENVIA_TRAMAS_SAP.FIN_PROVISION_DTH_SAP';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    error           VARCHAR2(1000);
    msj_tramas      VARCHAR2(3000);
    ln_contador     NUMBER := 0;
    le_no_hay_tramas EXCEPTION;
  
    --CURSOR OBTIENE TRAMAS DE PROVISION DTH
    CURSOR c_obtiene_tramas_prov_dth(pd_fecha DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pd_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'PROVISION DTH%';
  
  BEGIN
    gsib_security.dotraceh(pv_aplicacion,
                           'Se ejecuto el procedimiento que envia las tramas de FIN PROVISION_DTH_SAP con fecha de corte :' || pd_fecha ||
                           ' ,para el producto : DTH',
                           pn_id_transaccion);
  
    FOR i IN c_obtiene_tramas_prov_dth(pd_fecha) LOOP
      ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      gsib_envia_tramas_sap.consume_webservice(i.trama, error);
    
      IF error IS NOT NULL THEN
        msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
        UPDATE gsib_tramas_sap f
           SET f.estado      = 'NO ENVIADA',
               f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
        
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
      
        gsib_security.dotracem(pv_aplicacion,
                               'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                               pn_id_transaccion);
      
        error := NULL;
      ELSE
      
        msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' => ' || error;
      
        UPDATE gsib_tramas_sap f
           SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
        gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
      END IF;
    
    END LOOP;
  
    IF ln_contador <= 0 THEN
      msj_tramas := 'No se encontramas Polizas que enviar con fecha de corte ' || pd_fecha;
      RAISE le_no_hay_tramas;
    
    ELSE
      notifica_envio_polizas('PROVISION', 'DTH', pd_fecha);
      gsib_security.dotracem(pv_aplicacion, ' Se envio un e-mail a los funcionales confirmando la creacion del asiento ', pn_id_transaccion);
    
    END IF;
  
    pv_error := msj_tramas;
  
  EXCEPTION
    WHEN le_no_hay_tramas THEN
      pv_error := msj_tramas || ' ' || pv_aplicacion;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
    WHEN OTHERS THEN
      pv_error := msj_tramas || ' ' || pv_aplicacion;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE dth_devengamiento_pre_sap(pd_fecha DATE, pn_id_transaccion IN NUMBER, pv_error OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_ENVIA_TRAMAS_SAP.DTH_DEVENGAMIENTO_PRE_SAP';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    error           VARCHAR2(1000);
    msj_tramas      VARCHAR2(3000);
    ln_contador     NUMBER := 0;
    le_no_hay_tramas EXCEPTION;
    lv_valida_error NUMBER := 0;
    lv_error        VARCHAR2(1000);
    le_error EXCEPTION;
    lv_anio VARCHAR2(4) := NULL;
    lv_mes  VARCHAR2(2) := NULL;
  
    --CURSOR OBTIENE TRAMAS DE DEVENGO DTH
    CURSOR c_obtiene_tramas_dev_dth(pd_fecha DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pd_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'DEVENGO DTH%';
  
  BEGIN
    gsib_security.dotraceh(pv_aplicacion,
                           'Se ejecuto el procedimiento que envia las tramas de DTH_DEVENGAMIENTO_PRE_SAP con fecha de corte :' || pd_fecha ||
                           ' ,para el producto : DTH',
                           pn_id_transaccion);
  
    FOR i IN c_obtiene_tramas_dev_dth(pd_fecha) LOOP
      ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      gsib_envia_tramas_sap.consume_webservice(i.trama, error);
    
      IF error IS NOT NULL THEN
        lv_valida_error := lv_valida_error + 1;
        msj_tramas      := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error;
        UPDATE gsib_tramas_sap f
           SET f.estado      = 'NO ENVIADA',
               f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
      
        gsib_security.dotracem(pv_aplicacion,
                               'ERROR al enviar a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' ' || error,
                               pn_id_transaccion);
      
        error := NULL;
      ELSE
      
        msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia || ' => ' || error;
      
        UPDATE gsib_tramas_sap f
           SET f.estado = 'ENVIADA', f.observacion = f.observacion || ' => Enviada el : ' || lv_fecha_actual || chr(13)
        
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
        gsib_security.dotracem(pv_aplicacion, 'Se envio a SAP LA POLIZA ' || i.id_poliza || ' perteneciente a ' || i.referencia, pn_id_transaccion);
      END IF;
    
    END LOOP;
  
    -- si no hubo ningun error al enviar a SAP Gravo en la tabla Historica
    IF lv_valida_error = 0 THEN
    
      -------------------------------------------------------
      -- inserto los valor y servicios devengados a la tabla historica.
      BEGIN
        INSERT INTO dth_devengados_hist
          (ciclo, dia, mes, anio, tipo, nombre, valor, ctactble, cta_ctrap, compa�ia, devengado, shdes, fecha_devengado)
          SELECT a.ciclo, a.dia, a.mes, a.anio, a.tipo, a.nombre, a.valor, a.ctactble, a.cta_ctrap, a.compa�ia, b.valor_provisiona, a.shdes, SYSDATE
            FROM servicios_prepagados_dth_sap a, reporte_calc_prorrateo_dth_pre b
           WHERE upper(a.nombre) = b.servicio
             AND decode(a.compa�ia, 'Guayaquil', 'GYE', 'Quito', 'UIO') = b.ciudad
             AND a.ciclo = b.id_ciclo
             AND a.mes = b.mes
             AND a.ctactble = b.codigo_contable;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := SQLERRM;
          RAISE le_error;
      END;
    
      BEGIN
        lv_anio := extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr'));
        lv_mes  := extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
        INSERT INTO dth_bitacora_finan
          (anio, mes, fecha_journal, estado, comentario, fecha_replicacion)
        VALUES
          (lv_anio, lv_mes, pd_fecha, 'R', --replicado
           'Replicado a SAP', SYSDATE);
        -- borro registros de 1 a�o atras del atabla de bitacora
      
        DELETE FROM dth_bitacora_finan
         WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
           AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
        -- borro registros de 1 a�o atras de la tabla de servicios prepagados
        DELETE FROM servicios_prepagados_dth_sap
         WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
           AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := SQLERRM;
          RAISE le_error;
      END;
      COMMIT;
    
      ---------------------------------------------------------
    
    END IF;
  
    IF ln_contador <= 0 THEN
      msj_tramas := 'No se encontramas Polizas que enviar con fecha de corte ' || pd_fecha;
      RAISE le_no_hay_tramas;
    
    ELSE
      notifica_envio_polizas('DEVENGO', 'DTH', pd_fecha);
      gsib_security.dotracem(pv_aplicacion, ' Se envio un e-mail a los funcionales confirmando la creacion del asiento ', pn_id_transaccion);
    
    END IF;
  
    pv_error := msj_tramas;
  
  EXCEPTION
    WHEN le_no_hay_tramas THEN
      pv_error := msj_tramas;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
    WHEN le_error THEN
      pv_error := lv_error;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
    WHEN OTHERS THEN
      pv_error := msj_tramas;
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE reenvio_por_id_poliza(id_polizas        IN VARCHAR2, --polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                                  pv_escenario      VARCHAR2,
                                  pn_id_transaccion IN NUMBER,
                                  pv_error          OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_ENVIA_TRAMAS_SAP.REENVIO_POR_ID_POLIZA';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    ln_longitud     NUMBER := length(id_polizas);
    ln_contador     NUMBER := 0;
    lv_caracter     VARCHAR2(5);
    lv_id_poliza    VARCHAR2(12);
    lv_poliza       VARCHAR2(15);
    error_poliza EXCEPTION;
    error         VARCHAR2(5000);
    msj_tramas    VARCHAR2(3000);
    lv_found      BOOLEAN := FALSE;
    lv_referencia VARCHAR2(1000);
    contador      NUMBER := 0;
  
    CURSOR c_from_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'FROM_NAME';
    lv_from_name VARCHAR2(1000);
  
    CURSOR c_to_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'TO_NAME';
    lv_to_name VARCHAR2(1000);
  
    CURSOR c_subject IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'SUBJECT';
    lv_subject VARCHAR2(1000);
  
    CURSOR c_cc IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CC';
    lv_cc VARCHAR2(1000);
  
    CURSOR c_cco IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CCO';
    lv_cco VARCHAR2(1000);
  
    --CURSOR QUE BUSCA LA POLIZA A SER RE-ENVIADA
    CURSOR lc_busca_id_poliza(cv_poliza VARCHAR2) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.id_poliza = cv_poliza
         AND t.referencia LIKE
             '%' || decode(pv_escenario, 'F', 'FACTURACION', 'PV', 'PROVISION BSCS', 'PD', 'PROVISION DTH', 'D', 'DEVENGO DTH', '') || '%'
         AND t.estado <> 'ERROR';
  
    lv_busca_id_poliza lc_busca_id_poliza%ROWTYPE;
  
    CURSOR lc_escenario(lc_escenario VARCHAR2) IS
    
      SELECT decode(lc_escenario, 'F', 'FACTURACION', 'PV', 'PROVISION BSCS', 'PH', 'PROVISION DTH', 'D', 'DEVENGO DTH', '') FROM dual;
  
    lv_escenario VARCHAR2(300);
  
    CURSOR c_obtiene_referencia(cv_escenario VARCHAR2, cv_id_poliza VARCHAR2) IS
      SELECT d.id_poliza, d.estado, d.referencia
        FROM gsib_tramas_sap d
       WHERE d.id_poliza = cv_id_poliza
         AND d.referencia LIKE
             '%' || decode(cv_escenario, 'F', 'FACTURACION', 'PV', 'PROVISION BSCS', 'PD', 'PROVISION DTH', 'D', 'DEVENGO DTH', '') || '%'
            
         AND estado = 'RE-ENVIADA';
  
  BEGIN
  
    OPEN lc_escenario(pv_escenario);
    FETCH lc_escenario
      INTO lv_escenario;
    CLOSE lc_escenario;
  
    gsib_security.dotraceh(pv_aplicacion, 'Se ejecuto el procedimiento que reenvia las tramas por segun su ID_POLIZA', pn_id_transaccion);
  
    gsib_security.dotraceh(pv_aplicacion, 'Polizas que se pretenden enviar a SAP : ' || id_polizas, pn_id_transaccion);
  
    IF id_polizas IS NULL THEN
      pv_error := 'El parametro ID_POLIZAS no debe de ser NULO ';
      gsib_security.dotracem(pv_aplicacion, pv_error, pn_id_transaccion);
      RAISE error_poliza;
    
    END IF;
  
    FOR i IN 1 .. ln_longitud LOOP
    
      ln_contador := ln_contador + 1;
      lv_caracter := substr(id_polizas, ln_contador, 1);
      --     DBMS_OUTPUT.PUT_LINE(LV_CARACTER);
      lv_poliza := lv_poliza || lv_caracter;
    
      IF lv_caracter = ';' OR ln_contador = ln_longitud THEN
      
        lv_id_poliza := REPLACE(REPLACE(lv_poliza, ';', ''), ' ', '');
      
        OPEN lc_busca_id_poliza(lv_id_poliza);
        FETCH lc_busca_id_poliza
          INTO lv_busca_id_poliza;
        lv_found := lc_busca_id_poliza%NOTFOUND;
        CLOSE lc_busca_id_poliza;
      
        --   dbms_output.put_line('POLIZA COMPLETA ; ' || lv_id_poliza);
      
        IF lv_found = TRUE THEN
          pv_error := ' La poliza ' || lv_id_poliza || ' NO encontrada para el escenario de ' || lv_escenario;
          RAISE error_poliza;
        
        END IF;
      
        gsib_envia_tramas_sap.consume_webservice(lv_busca_id_poliza.trama, error);
      
        IF error IS NOT NULL THEN
          msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' || lv_busca_id_poliza.id_poliza || ' perteneciente a ' ||
                        lv_busca_id_poliza.referencia || ' ' || error;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ERROR. al tratar de envia a SAP ' || lv_fecha_actual || ' ' || error || chr(13)
           WHERE f.id_poliza = lv_busca_id_poliza.id_poliza;
        
          COMMIT;
        
          gsib_security.dotracem(pv_aplicacion,
                                 'ERROR al enviar a SAP LA POLIZA ' || lv_busca_id_poliza.id_poliza || ' perteneciente a ' ||
                                 lv_busca_id_poliza.referencia || ' ' || error,
                                 pn_id_transaccion);
        
          error := NULL;
        ELSE
        
          msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' || lv_busca_id_poliza.id_poliza || ' perteneciente a ' ||
                        lv_busca_id_poliza.referencia || ' => ' || error;
        
          UPDATE gsib_tramas_sap f
             SET f.estado = 'RE-ENVIADA', f.observacion = f.observacion || ' => Reintento de Envio el : ' || lv_fecha_actual || chr(13)
          
           WHERE f.id_poliza = lv_busca_id_poliza.id_poliza;
        
          FOR i IN c_obtiene_referencia(pv_escenario, id_polizas) LOOP
            lv_referencia := lv_referencia || i.id_poliza || '         ' || i.estado || '         ' || i.referencia || chr(13);
            contador      := contador + 1;
          END LOOP;
        
          IF contador > 0 THEN
          
            OPEN c_from_name;
            FETCH c_from_name
              INTO lv_from_name;
            CLOSE c_from_name;
          
            OPEN c_to_name;
            FETCH c_to_name
              INTO lv_to_name;
            CLOSE c_to_name;
          
            OPEN c_cc;
            FETCH c_cc
              INTO lv_cc;
            CLOSE c_cc;
          
            OPEN c_cco;
            FETCH c_cco
              INTO lv_cco;
            CLOSE c_cco;
          
            OPEN c_subject;
            FETCH c_subject
              INTO lv_subject;
            CLOSE c_subject;
          
            lv_subject := REPLACE(lv_subject, '<<ESCENARIO>>', pv_escenario);
          
            regun.send_mail.mail@colector(lv_from_name,
                                          lv_to_name,
                                          lv_cc,
                                          lv_cco,
                                          lv_subject,
                                          'Saludos,' || chr(13) || chr(13) || 'Se informa, que se procedio a realizar el Re-envio de la poliza ' ||
                                          id_polizas || chr(13) || chr(13) || 'Polizas que fueron enviadas :' || chr(13) || chr(13) || ' ID_POLIZA' ||
                                          '             ' || 'ESTADO' || '              ' || 'REFERENCIA' || chr(13) || chr(13) || lv_referencia ||
                                          chr(13) ||
                                          'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).' ||
                                          chr(13) || 'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||
                                          chr(13) || chr(13) || 'Atentamente,' || chr(13) || chr(13) || 'SIS GSI-Billing.' || chr(13) || chr(13) ||
                                          'Conecel.S.A - America Movil.');
            COMMIT;
          
          END IF;
        
          COMMIT;
          gsib_security.dotracem(pv_aplicacion,
                                 'Se envio a SAP LA POLIZA ' || lv_busca_id_poliza.id_poliza || ' perteneciente a ' || lv_busca_id_poliza.referencia,
                                 pn_id_transaccion);
        END IF;
      
        lv_id_poliza := NULL;
        lv_poliza    := NULL;
      
      END IF;
    
    END LOOP;
  
    pv_error := msj_tramas;
    gsib_security.dotraceh(pv_aplicacion, pv_error, pn_id_transaccion);
  
  EXCEPTION
    WHEN error_poliza THEN
      pv_error := pv_error || msj_tramas || ' ' || pv_aplicacion;
      gsib_security.dotraceh(pv_aplicacion, pv_error, pn_id_transaccion);
    
    WHEN OTHERS THEN
      pv_error := 'ERROR : ' || SQLERRM || ' ' || pv_aplicacion;
      gsib_security.dotraceh(pv_aplicacion, pv_error, pn_id_transaccion);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE consume_webservice(pv_trama IN CLOB, pv_error OUT VARCHAR2) IS
  
    --lv_xml_trama VARCHAR2(3500);
    lv_trama     CLOB;
    lv_xml_trama CLOB;
    lx_response  sys.xmltype;
    ln_id_req    NUMBER;
    le_error EXCEPTION;
    lv_fault_code          VARCHAR2(2000);
    lv_fault_string        VARCHAR2(2000);
    lv_resp_exito          VARCHAR2(2000);
    lv_id_tipo_transaccion VARCHAR2(100);
    lv_proceso             VARCHAR2(200) := ' GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE';
  
    CURSOR c_parametro(cn_id_tipo_parametro NUMBER, cv_id_parametro VARCHAR2) IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = cn_id_tipo_parametro
         AND id_parametro = cv_id_parametro;
  
  BEGIN
  
    OPEN c_parametro(10189, 'ID_REQUERIMIENTO');
    FETCH c_parametro
      INTO ln_id_req;
    CLOSE c_parametro;
  
    IF ln_id_req IS NULL THEN
      pv_error := 'Paraemtro ID_REQUERIMIENTO no existe en la GSIB_PARAMETROS_SAP ';
      RAISE le_error;
    END IF;
  
    OPEN c_parametro(10189, 'ID_TIPO_TRANSACCION');
    FETCH c_parametro
      INTO lv_id_tipo_transaccion;
    CLOSE c_parametro;
  
    IF lv_id_tipo_transaccion IS NULL THEN
      pv_error := 'Paraemtro ID_TIPO_TRANSACCION no existe en la GSIB_PARAMETROS_SAP ';
      RAISE le_error;
    END IF;
  
    lv_trama := pv_trama;
  
    /*  ln_id_req              := 79000100;
      lv_id_tipo_transaccion := 'Sap_ATR_0041_QA';
    */
    lv_xml_trama := 'Transaccion=' || lv_id_tipo_transaccion || '|CadenaParametros=' || lv_trama || '|';
  
    lx_response := scp_dat.sck_soap_clob_gtw.scp_consume_servicio(ln_id_req, lv_xml_trama, lv_fault_code, lv_fault_string);
  
    -- VERIFICACION DEL ERROR 
    IF lv_fault_string IS NOT NULL THEN
      pv_error := 'Error en consumo de webServices ' || lv_fault_string || ' - ' || lv_fault_code;
      RAISE le_error;
    END IF;
  
    pv_error := scp_dat.sck_soap_gtw.scp_obtener_valor(ln_id_req, lx_response, 'MENSAJE');
  
    IF pv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
    lv_resp_exito := scp_dat.sck_soap_gtw.scp_obtener_valor(ln_id_req, lx_response, 'Resultado');
  
    IF lv_resp_exito <> 'EXITO' THEN
      pv_error := lv_resp_exito;
      RAISE le_error;
    END IF;
    --
    -- Compuebo que no haya un Fault en el servicio
    IF lv_fault_code IS NOT NULL OR lv_fault_string IS NOT NULL THEN
      -- Lanzo la excepcion asociada al Fault en el servicio
      pv_error := substr(': Fault en servicio web. ID_REQUERIMIENTO=' || ln_id_req || 'PARAMETROS=' || lv_xml_trama, 1, 1000);
      pv_error := pv_error || ' - ' || substr(lv_fault_code || ' - ' || lv_fault_string, 1, 1000);
      RAISE le_error;
    
    ELSE
      -- Comprobamos que la respuesta no sea nula, de lo contrario lanza exception
      IF lx_response IS NULL THEN
        pv_error := 'Error al llamar paquete INK_TRX_TRANSF_AXIS_SAP: ' || pv_error;
        RAISE le_error;
      END IF;
    END IF;
  EXCEPTION
    WHEN le_error THEN
      ROLLBACK;
      pv_error := substr(pv_error, 1, 1000) || lv_proceso;
    
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := 'Error al despachar hacia SAP - ' || substr(SQLERRM, 1, 100) || lv_proceso;
  END consume_webservice;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  /*PROCEDURE envia_correo_xml(pc_xml IN CLOB) IS
  
  BEGIN
  
    regun.send_mail.send_files@colector(from_name => 'postmaster@claro.com.ec',
                                        to_name   => 'nmiranda@righttek.com;zmorejon@righttek.com',
                                        cc        => NULL,
                                        subject   => 'Previsualizacion HTML antes del envio a SAP',
                                        message   => pc_xml,
                                        max_size  => NULL,
                                        filename1 => NULL,
                                        filename2 => NULL,
                                        filename3 => NULL,
                                        debug     => NULL);
  
    COMMIT;
  END;*/

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE notifica_envio_polizas(pv_escenario IN VARCHAR2, pv_producto IN VARCHAR2, pd_fecha_corte IN DATE) IS
  
    CURSOR c_obtiene_referencia(cv_escenario VARCHAR2, cv_producto VARCHAR2, cv_fecha DATE) IS
      SELECT d.id_poliza, d.estado, d.referencia
        FROM gsib_tramas_sap d
       WHERE d.fecha_corte = cv_fecha
         AND d.referencia LIKE '%' || cv_escenario || ' ' || decode(cv_producto, 'VOZ', 'BSCS', 'DTH', 'DTH', NULL) || '%'
         AND estado = 'ENVIADA'
       ORDER BY d.id_poliza ASC;
  
    lv_referencia VARCHAR2(3000);
    contador      NUMBER := 0;
  
    CURSOR c_from_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'FROM_NAME';
    lv_from_name VARCHAR2(1000);
  
    CURSOR c_to_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'TO_NAME';
    lv_to_name VARCHAR2(1000);
  
    CURSOR c_subject IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'SUBJECT';
    lv_subject VARCHAR2(1000);
  
    CURSOR c_cc IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CC';
    lv_cc VARCHAR2(1000);
  
    CURSOR c_cco IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CCO';
    lv_cco VARCHAR2(1000);
  
    lv_mensaje VARCHAR2(3000);
  
  BEGIN
  
    FOR i IN c_obtiene_referencia(pv_escenario, pv_producto, pd_fecha_corte) LOOP
      lv_referencia := lv_referencia || i.id_poliza || '         ' || i.estado || '         ' || i.referencia || chr(13);
      contador      := contador + 1;
    END LOOP;
  
    IF contador > 0 THEN
    
      OPEN c_from_name;
      FETCH c_from_name
        INTO lv_from_name;
      CLOSE c_from_name;
    
      OPEN c_to_name;
      FETCH c_to_name
        INTO lv_to_name;
      CLOSE c_to_name;
    
      OPEN c_cc;
      FETCH c_cc
        INTO lv_cc;
      CLOSE c_cc;
    
      OPEN c_cco;
      FETCH c_cco
        INTO lv_cco;
      CLOSE c_cco;
    
      OPEN c_subject;
      FETCH c_subject
        INTO lv_subject;
      CLOSE c_subject;
    
      lv_subject := REPLACE(lv_subject, '<<ESCENARIO>>', pv_escenario);
    
      lv_mensaje := 'Saludos,' || chr(13) || chr(13) || 'Se informa, que se procedio a realizar el envio de los asientos Contables a la fecha ' ||
                    pd_fecha_corte || chr(13) || chr(13) || 'Polizas que fueron enviadas :' || chr(13) || chr(13) || ' ID_POLIZA' || '             ' ||
                    'ESTADO' || '              ' || 'REFERENCIA' || chr(13) || chr(13) || lv_referencia || chr(13) ||
                    'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).' || chr(13) ||
                    'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' || chr(13) || chr(13) ||
                    'Atentamente,' || chr(13) || chr(13) || 'SIS GSI-Billing.' || chr(13) || chr(13) || 'Conecel.S.A - America Movil.';
    
      regun.send_mail.mail@colector(lv_from_name, lv_to_name, lv_cc, lv_cco, lv_subject || pd_fecha_corte, lv_mensaje);
      COMMIT;
    
    END IF;
  END;

END gsib_envia_tramas_sap;
/
