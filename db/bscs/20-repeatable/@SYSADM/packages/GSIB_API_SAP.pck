CREATE OR REPLACE PACKAGE gsib_api_sap IS

  -- 10189
  -- SIS Luis Flores
  -- PDS Fernando Ortega
  -- RGT Ney Miranda 
  -- Ultimo Cambio: 30/07/2015
  -- Nueva logica de logs y tramas para SAP
  
  PROCEDURE genera_asientos(pd_fecha     IN DATE,
                            pv_escenario IN VARCHAR2,
                            pv_producto  IN VARCHAR2,
                            --pv_envia_trama IN VARCHAR2,
                            pv_ejecucion      IN VARCHAR2 DEFAULT NULL,
                            pn_id_transaccion IN NUMBER,
                            pv_resultado      OUT VARCHAR2);

  PROCEDURE envia_tramas(pd_fecha          IN DATE, --DD/MM/YYYY
                         pv_escenario      IN VARCHAR2, -- F, P, D
                         pv_producto       IN VARCHAR2, --VOZ, DTH
                         pv_ejecucion      IN VARCHAR2,
                         pn_id_transaccion IN NUMBER,
                         pv_resultado      OUT VARCHAR2);
  PROCEDURE reenvio_tramas(pv_id_polizas     IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'  
                           pv_escenario      VARCHAR2,
                           pn_id_transaccion IN NUMBER,
                           pv_resultado      OUT VARCHAR2);

  PROCEDURE genera_html(pv_id_poliza VARCHAR2, pv_error OUT VARCHAR2);

  FUNCTION abs_numero(valor NUMBER) RETURN VARCHAR2;
  PROCEDURE reinicia_secuencia(pn_id_transaccion IN NUMBER);
  PROCEDURE activa_shell;   -- [10183] RGT Ney Miranda T.  se elimina el parametro para ejecutarlo desde un JOBS  
  FUNCTION retorna_longitud(pv_id_poliza VARCHAR2) RETURN NUMBER;
  FUNCTION retorna_html_forma(pv_id_poliza IN VARCHAR2,
                              pn_inicio    IN NUMBER,
                              pn_fin       IN NUMBER) RETURN VARCHAR2;
  PROCEDURE anula_polizas(pd_fecha          IN DATE, --DD/MM/YYYY
                          pv_escenario      IN VARCHAR2, -- F, P, D
                          pv_producto       IN VARCHAR2, --VOZ, DTH
                          pn_id_transaccion IN NUMBER,
                          pv_resultado      OUT VARCHAR2,
                          pv_polizas        OUT VARCHAR2);
  FUNCTION retorna_longitud_reporte(pv_nombre_reporte IN VARCHAR2)
    RETURN NUMBER;
  FUNCTION retorna_reporte_html_forma(pv_nombre_reporte IN VARCHAR2,
                                      pn_inicio         IN NUMBER,
                                      pn_fin            IN NUMBER)
    RETURN VARCHAR2;

   -- INICIO [10183] RGT Ney Miranda T. 
    --Procediemto que ejecuta todos los escenarios desde un JOBS

  PROCEDURE crea_jobs(pv_resultado OUT VARCHAR2);
--Procedimiento que elimina los jobs creados
  PROCEDURE elimina_jobs;
  --Procedimeinto que respalda la tabla de Servicios_prepagados_dth al ejecutar el escenario de facturacion y el escenario de devengo Dth
  PROCEDURE respaldo_servicios_dth(pd_fecha_corte IN DATE,--dd/mm/yyyy
                                   pv_tipo        IN VARCHAR2,--FACTURACION / DEVENGO
                                               pn_id_transaccion IN NUMBER,
                                   pv_resultado   OUT VARCHAR2);
  -- FIN [10183] RGT Ney Miranda T.   				   
END gsib_api_sap;
/
CREATE OR REPLACE PACKAGE BODY gsib_api_sap IS
  lv_fmt_number VARCHAR2(50) := '99999999999999999990.00';

  /*
    Aplica para:
    GRABA_SAFI_FACT (Facturacion BSCS y DTH) F
    FIN_PROVISION (Provision BSCS) P
    FIN_PROVISION_DTH (Provision DTH) P
    DTH_DEVENGAMIENTO_PRE (Devengo DTH) D No Aplica, se debe llamar desde la Forma  
  */

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE genera_asientos(pd_fecha          IN DATE, --DD/MM/YYYY
                            pv_escenario      IN VARCHAR2, -- F, P, D
                            pv_producto       IN VARCHAR2, --VOZ, DTH
                            pv_ejecucion      IN VARCHAR2, -- 'C', 'CG'  C=COMMIT(GRABA EN EL HISTORICO)  ; CG CREA LOS ASIENTOS PERO NO GRABA EN LOS HISTORICOS
                            pn_id_transaccion IN NUMBER,
                            pv_resultado      OUT VARCHAR2) IS
  
    pv_msj_trama  VARCHAR2(3000);
    lv_error      VARCHAR2(3000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_API_SAP.GENERA_ASIENTOS';
    le_error_asiento EXCEPTION;
    pv_error      VARCHAR2(3000);
    lv_referencia VARCHAR2(100);
  
    --cursor para crear la vista previa de la poliza generada en html    
    CURSOR lc_crea_html(lc_fecha_corte DATE) IS
      SELECT g.id_poliza
        FROM gsib_tramas_sap g
       WHERE g.fecha_corte = lc_fecha_corte
         AND trunc(g.fecha) = trunc(SYSDATE);
  
    -- Bandera que habilita o eshabilita la generacion de asientos del mismo corte mas de una vez         
    CURSOR lc_permite_duplicados IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = '10189'
         AND id_parametro = 'BANDERA_DUPLICADOS';
  
    lv_permite_duplicados VARCHAR2(10);
  
    --cursor que valida si permite generar asientos del mismo corte mas de una vez 
    CURSOR lc_valida_corte(cd_fecha DATE, cv_escenario VARCHAR2) IS
      SELECT COUNT(*)
        FROM gsib_tramas_sap s
       WHERE s.fecha_corte = cd_fecha
         AND s.referencia LIKE
             '%' || decode(cv_escenario,
                           'F',
                           'FACTURACION',
                           'P',
                           'PROVISION',
                           'D',
                           'DEVENGO') || '%';
  
    ln_exsiste_registros NUMBER := 0;
  
  BEGIN
  
    --valida fecha
    IF pd_fecha IS NULL THEN
      lv_error := 'El parametro ingresado para PD_FECHA no es valido ';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
      RAISE le_error_asiento;
    END IF;
    -- valida escenario
    IF pv_escenario IS NULL OR
       (pv_escenario <> 'F' AND pv_escenario <> 'P' AND pv_escenario <> 'D') THEN
      lv_error := 'El parametro ingresado para PV_ESCENARIO no es valido debe de ser : F = FACTURACION ; P = PROVICION ; D = DEVENGO .';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
      RAISE le_error_asiento;
    END IF;
  
    --valida los productos permitidos para cada escenario
    IF pv_escenario = 'F' THEN
    
      IF pv_producto <> 'VOZ' AND pv_producto <> 'DTH' THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO del escenario FACTURACION no es valido debe de ser : VOZ , DTH o NULL para ambos casos .';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    IF pv_escenario = 'D' THEN
      IF pv_producto <> 'DTH' THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO del escenario DEVENGO no es valido debe de ser : DTH ';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    IF pv_escenario = 'P' THEN
      IF pv_producto IS NULL OR
         (pv_producto <> 'VOZ' AND pv_producto <> 'DTH') THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO  del escenario de PROVISION no es valido debe de ser : VOZ o DTH .';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    IF pv_ejecucion NOT IN ('C', 'CG') OR pv_ejecucion IS NULL THEN
      lv_error := 'El parametro PV_EJECUCION es erroneo. Ingrese C (COMMIT) � CG (CONTROL GROUP) => ';
      RAISE le_error_asiento;
    END IF;
  
    OPEN lc_valida_corte(pd_fecha, pv_escenario);
    FETCH lc_valida_corte
      INTO ln_exsiste_registros;
    CLOSE lc_valida_corte;
  
    OPEN lc_permite_duplicados;
    FETCH lc_permite_duplicados
      INTO lv_permite_duplicados;
    CLOSE lc_permite_duplicados;
  
    IF pv_escenario = 'F' THEN
      lv_referencia := 'FACTURACION';
    ELSE
      IF pv_escenario = 'P' THEN
        lv_referencia := 'PROVISION';
      ELSE
        IF pv_escenario = 'D' THEN
          lv_referencia := 'DEVENGO';
        END IF;
      END IF;
    END IF;
  
    --valido si ya existen asientos generados con ese corte y la Bandera de dupliados  
    IF ln_exsiste_registros > 0 AND nvl(lv_permite_duplicados, 'N') = 'N' THEN
    
      gsib_security.dotraceh(lv_aplicacion,
                             '**** INICIO GENERA ASIENTOS ' ||
                             lv_referencia || ' ' || pv_producto ||
                             ' FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '**** ',
                             pn_id_transaccion);
    
      lv_error := 'Polizas referentes a ' || lv_referencia ||
                  ' Costa / Sierra con fecha fecha de corte ' || pd_fecha ||
                  ' ya han sido generadas anteriormente.';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
    
      gsib_security.dotraceh(lv_aplicacion,
                             '**** FIN GENERA ASIENTOS ' || lv_referencia || ' ' ||
                             pv_producto || ' FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '**** ',
                             pn_id_transaccion);
    
      RAISE le_error_asiento;
    
    ELSE
    
      --LLAMO AL PROCEDIENTO ENCARGADO DE GENERAR ASIENTOS DE FACTURACION 
      IF pv_escenario = 'F' THEN
        gsib_security.dotraceh(lv_aplicacion,
                               '**** INICIO GENERA ASIENTOS FACTURACION ' ||
                               pv_producto || ' FECHA DE CORTE :' ||
                               pd_fecha || ' FECHA EJECUCION : ' ||
                               to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                               '**** ',
                               pn_id_transaccion);
      
        graba_safi_fact_sap.graba_safi_principal(pd_fecha,
                                                 pv_producto,
                                                 pv_ejecucion,
                                                 pv_resultado,
                                                 pv_msj_trama);
      
        pv_resultado := pv_resultado || ' - ' || pv_msj_trama;
      
        gsib_security.dotraceh(lv_aplicacion,
                               '**** FIN GENERA ASIENTOS FACTURACION ' ||
                               pv_producto || ' FECHA DE CORTE :' ||
                               pd_fecha || ' FECHA EJECUCION : ' ||
                               to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                               '**** ',
                               pn_id_transaccion);
      
        --LLAMO AL PROCEDIENTO ENCARGADO DE GENERAR ASIENTOS DE PROVISION
      ELSIF pv_escenario = 'P' THEN
        IF pv_producto = 'VOZ' THEN
          BEGIN
          
            gsib_security.dotraceh(lv_aplicacion,
                                   '**** INICIO GENERA ASIENTOS PROVISION BSCS ' ||
                                   'FECHA DE CORTE :' || pd_fecha ||
                                   ' FECHA EJECUCION : ' ||
                                   to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                                   '**** ',
                                   pn_id_transaccion);
          
            fin_provision_sap.p_genera_prorateo(pv_error); --GENERO PRORRATEO
            pv_resultado := pv_error;
          
            fin_provision_sap.p_gen_preasiento(pd_fecha, pv_error); --  CREO LOS ASIENTOS 
            pv_resultado := pv_resultado || pv_error;
            IF pv_ejecucion = 'C' THEN
              fin_provision_sap.replica_financiero(pd_fecha, pv_error); -- se replica la informacion en el historico
              pv_resultado := pv_resultado || pv_error;
            END IF;
            gsib_security.dotraceh(lv_aplicacion,
                                   '**** FIN GENERA ASIENTOS PROVISION BSCS ' ||
                                   'FECHA DE CORTE :' || pd_fecha ||
                                   ' FECHA EJECUCION : ' ||
                                   to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                                   '**** ',
                                   pn_id_transaccion);
          
          EXCEPTION
            WHEN OTHERS THEN
              pv_resultado := pv_error || SQLERRM;
          END;
        
        ELSIF pv_producto = 'DTH' THEN
        
          BEGIN
          
            gsib_security.dotraceh(lv_aplicacion,
                                   '**** INICIO GENERA ASIENTOS PROVISION DTH ' ||
                                   'FECHA DE CORTE :' || pd_fecha ||
                                   ' FECHA EJECUCION : ' ||
                                   to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                                   '**** ',
                                   pn_id_transaccion);
          
            fin_provision_dth_sap.p_genera_prorateo(pv_error); --GENERO PRORRATEO
            pv_resultado := pv_error;
          
            fin_provision_dth_sap.p_gen_preasiento(pd_fecha, pv_error); --  CREO LOS ASIENTOS 
            pv_resultado := pv_resultado || pv_error;
          
            IF pv_ejecucion = 'C' THEN
              fin_provision_dth_sap.replica_financiero(pd_fecha, pv_error); -- se replica la informacion en el historico
              pv_resultado := pv_resultado || pv_error;
            END IF;
            gsib_security.dotraceh(lv_aplicacion,
                                   '**** FIN GENERA ASIENTOS PROVISION DTH ' ||
                                   'FECHA DE CORTE :' || pd_fecha ||
                                   ' FECHA EJECUCION : ' ||
                                   to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                                   '**** ',
                                   pn_id_transaccion);
          
          EXCEPTION
            WHEN OTHERS THEN
              pv_resultado := pv_error || SQLERRM;
          END;
        
        END IF;
      
      ELSIF pv_escenario = 'D' THEN
        --DEVENGO
      
        gsib_security.dotraceh(lv_aplicacion,
                               '**** INICIO GENERA ASIENTOS DEVENGO DTH ' ||
                               'FECHA DE CORTE :' || pd_fecha ||
                               ' FECHA EJECUCION : ' ||
                               to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                               '**** ',
                               pn_id_transaccion);
      
        BEGIN
          dth_devengamiento_pre_sap.ajusta_prepago_dth(pd_fecha, pv_error); --GENERO PRORRATEO
          pv_resultado := pv_error;
        
          dth_devengamiento_pre_sap.p_gen_preasiento(pd_fecha, pv_error); --  CREO LOS ASIENTOS 
          pv_resultado := pv_resultado || pv_error;
          IF pv_ejecucion = 'C' THEN
            dth_devengamiento_pre_sap.replica_financiero(pd_fecha,
                                                         pv_error); -- se replica la informacion en el historico
            pv_resultado := pv_resultado || pv_error;
          END IF;
          gsib_security.dotraceh(lv_aplicacion,
                                 '**** FIN GENERA ASIENTOS DEVENGO DTH ' ||
                                 'FECHA DE CORTE :' || pd_fecha ||
                                 ' FECHA EJECUCION : ' ||
                                 to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                                 '**** ',
                                 pn_id_transaccion);
        
        EXCEPTION
          WHEN OTHERS THEN
            pv_resultado := pv_error || SQLERRM;
            gsib_security.dotraceh(lv_aplicacion,
                                   pv_resultado,
                                   pn_id_transaccion);
          
        END;
      END IF;
    
    END IF;
  
    FOR d IN lc_crea_html(pd_fecha) LOOP
      genera_html(d.id_poliza, pv_error);
    END LOOP;
  
  EXCEPTION
    WHEN le_error_asiento THEN
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    WHEN OTHERS THEN
      pv_resultado := 'ERROR : ' || SQLERRM || ' ' || lv_aplicacion;
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE envia_tramas(pd_fecha          IN DATE, --DD/MM/YYYY
                         pv_escenario      IN VARCHAR2, -- F, P, D
                         pv_producto       IN VARCHAR2, --VOZ, DTH
                         pv_ejecucion      IN VARCHAR2, --C commit
                         pn_id_transaccion IN NUMBER,
                         pv_resultado      OUT VARCHAR2) IS
  
    lv_error      VARCHAR2(3000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_API_SAP.ENVIA_TRAMAS';
    le_error_asiento EXCEPTION;
    pv_error VARCHAR2(3000);
  
    -- CURSOR VALIDA QUE NO SE TRATE DE ENVIAR 2 VECES LAS POLIZAS
    CURSOR lc_valida_envio(cd_fecha     DATE,
                           cv_escenario VARCHAR2,
                           cv_producto  VARCHAR2) IS
    
      SELECT COUNT(*)
        FROM gsib_tramas_sap s
       WHERE s.fecha_corte = cd_fecha
         AND s.referencia LIKE
             '%' || decode(cv_escenario,
                           'F',
                           'FACTURACION',
                           'P',
                           'PROVISION',
                           'D',
                           'DEVENGO') ||
             decode(cv_producto, 'VOZ', ' BSCS', 'DTH', ' DTH', NULL) || '%'
         AND s.estado = 'ENVIADA';
  
    ln_valida_envio NUMBER := 0;
    lv_referencia   VARCHAR2(50);
  
    -- CURSOR QUE VALIDA SI SE PERMITE ENVIAR DOS VECES LAS MISMAS TRAMAS SIN IMPORTAR EL ESTADO
  
    CURSOR lc_permite_duplicados IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = '10189'
         AND id_parametro = 'BANDERA_DUPLICADOS';
  
    lv_permite_duplicados VARCHAR2(10);
  
  BEGIN
  
    --VALIDO FECHA
    IF pd_fecha IS NULL THEN
      lv_error := 'El parametro ingresado para PD_FECHA no es valido ';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
    
      RAISE le_error_asiento;
    END IF;
  
    --VALIDA ESCENARIO
    IF pv_escenario IS NULL OR
       (pv_escenario <> 'F' AND pv_escenario <> 'P' AND pv_escenario <> 'D') THEN
      lv_error := 'El parametro ingresado para PV_ESCENARIO no es valido debe de ser : F = FACTURACION ; P = PROVICION ; D = DEVENGO .';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
    
      RAISE le_error_asiento;
    END IF;
  
    IF pv_escenario = 'F' THEN
    
      IF pv_producto <> 'VOZ' AND pv_producto <> 'DTH' THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO del escenario FACTURACION no es valido debe de ser : VOZ , DTH o NULL para ambos casos .';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
      
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    IF pv_escenario = 'D' THEN
      IF pv_producto <> 'DTH' THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO del escenario DEVENGO no es valido debe de ser : DTH ';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
      
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    IF pv_escenario = 'P' THEN
      IF pv_producto IS NULL OR
         (pv_producto <> 'VOZ' AND pv_producto <> 'DTH') THEN
        lv_error := 'El parametro ingresado para PV_PRODUCTO  del escenario de PROVISION no es valido debe de ser : VOZ o DTH .';
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
      
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    OPEN lc_valida_envio(pd_fecha, pv_escenario, pv_producto);
    FETCH lc_valida_envio
      INTO ln_valida_envio;
    CLOSE lc_valida_envio;
  
    OPEN lc_permite_duplicados;
    FETCH lc_permite_duplicados
      INTO lv_permite_duplicados;
    CLOSE lc_permite_duplicados;
  
    IF pv_escenario = 'F' THEN
      lv_referencia := 'FACTURACION';
    ELSE
      IF pv_escenario = 'P' THEN
        lv_referencia := 'PROVISION';
      ELSE
        IF pv_escenario = 'D' THEN
          lv_referencia := 'DEVENGO';
        END IF;
      END IF;
    END IF;
  
    --VALIDO SI EXISTEN POLIZAS QUE ENVIAR 
    IF ln_valida_envio > 0 AND ln_valida_envio <> 2 AND
       nvl(lv_permite_duplicados, 'N') = 'N' THEN
    
      lv_error := 'Polizas referentes a ' || lv_referencia ||
                  ' Costa / Sierra con fecha fecha de corte ' || pd_fecha ||
                  ' ya han sido ENVIADAS anteriormente.';
      gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
    
      RAISE le_error_asiento;
    
    ELSE
      gsib_security.dotraceh(lv_aplicacion,
                             'Se llama al proceso encargado de validar y enviar las tramas',
                             pn_id_transaccion);
    
      --LLAMO AL PROCEDIENTO ENCARGADO DE ENVIAR ASIENTOS DE FACTURACION 
    
      IF pv_escenario = 'F' THEN
        gsib_envia_tramas_sap.graba_safi_fact_sap(pd_fecha,
                                                  pv_producto, -- NCA 'DTH' SOLO DTH, 'VOZ' TODOS LOS PRODUCTOS EXCEPTIO DTH, NULO PARA PROCESAR VOZ Y DTH. 
                                                  pv_ejecucion, -- TIPO DE EJECUCION 'C' COMMIT 'CG' CONTROL GROUP 
                                                  pn_id_transaccion,
                                                  pv_resultado);
      
        pv_resultado := pv_resultado;
      
      ELSIF pv_escenario = 'P' THEN
        --LLAMO AL PROCEDIENTO ENCARGADO DE ENVIAR ASIENTOS DE PROVISION VOZ 
        IF pv_producto = 'VOZ' THEN
          BEGIN
          
            gsib_envia_tramas_sap.fin_provision_sap(pd_fecha,
                                                    pn_id_transaccion,
                                                    pv_error);
          
            pv_resultado := pv_error;
          
          EXCEPTION
            WHEN OTHERS THEN
              pv_resultado := pv_error || SQLERRM;
          END;
        
          --LLAMO AL PROCEDIENTO ENCARGADO DE ENVIAR ASIENTOS DE PROVISION DTH 
        ELSIF pv_producto = 'DTH' THEN
          BEGIN
          
            gsib_envia_tramas_sap.fin_provision_dth_sap(pd_fecha,
                                                        pn_id_transaccion,
                                                        pv_error);
            pv_resultado := pv_error;
          
          EXCEPTION
            WHEN OTHERS THEN
              pv_resultado := pv_error || SQLERRM;
          END;
        
        END IF;
      
        --LLAMO AL PROCEDIENTO ENCARGADO DE ENVIAR ASIENTOS DE DEVENGO DTH 
      ELSIF pv_escenario = 'D' THEN
        BEGIN
        
          gsib_envia_tramas_sap.dth_devengamiento_pre_sap(pd_fecha,
                                                          pn_id_transaccion,
                                                          pv_error);
          pv_resultado := pv_error;
        
        EXCEPTION
          WHEN OTHERS THEN
            pv_resultado := pv_error || SQLERRM;
        END;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN le_error_asiento THEN
      pv_resultado := lv_error || ' ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
    WHEN OTHERS THEN
      pv_resultado := 'ERROR : ' || SQLERRM || ' ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reenvio_tramas(pv_id_polizas     IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                           pv_escenario      VARCHAR2,
                           pn_id_transaccion IN NUMBER,
                           pv_resultado      OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(200) := 'GSIB_API_SAP.REENVIO_TRAMAS';
    pv_error      VARCHAR2(5000);
  
  BEGIN
    --LLAMO AL PROCEDIENTO ENCARGADO DE ENVIAR ASIENTOS SIN IMPORTAR EL ESTADO Y EL ESCENARIO , SOLO CON EL ID_POLIZA         
    gsib_envia_tramas_sap.reenvio_por_id_poliza(pv_id_polizas,
                                                pv_escenario,
                                                pn_id_transaccion,
                                                pv_error);
  
    pv_resultado := pv_error;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'ERROR : ' || SQLERRM || ' ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE genera_html(pv_id_poliza VARCHAR2, pv_error OUT VARCHAR2) IS
    lv_id_poliza VARCHAR2(100);
    lv_trama     CLOB;
    lv_cab_prev  CLOB;
    lv_cue_prev  CLOB;
    lv_html      CLOB;
  
    lv_estilo       VARCHAR2(1) := 'T'; --T=Texto; X=Tag; A=Ambos
    lv_esq_cuerpo   VARCHAR2(4000);
    lv_esq_cabecera VARCHAR2(4000);
  
    -- Cursor para cargar la trama
    CURSOR c_poliza(cv_id_poliza VARCHAR2) IS
      SELECT a.* FROM gsib_tramas_sap a WHERE a.id_poliza = cv_id_poliza;
    lc_poliza c_poliza%ROWTYPE;
    lb_found  BOOLEAN;
  
    -- Ciclos
    ln_cont    NUMBER := 0;
    lv_cad1    VARCHAR2(4000) := '';
    lv_cad2    VARCHAR2(4000) := '';
    lv_cad_sap VARCHAR2(50) := '';
  
    ln_pos1 NUMBER := 0;
    ln_pos2 NUMBER := 0;
  
    -- Aplicacion
    lv_application VARCHAR2(100) := 'GSIB_API_SAP.GENERA_HTML';
  
    -- Sumatoria
    ln_suma_debe  NUMBER := 0;
    ln_suma_haber NUMBER := 0;
    lv_debe_haber VARCHAR2(1) := '';
  
  BEGIN
    -- Abrir la poliza
    lv_id_poliza := pv_id_poliza;
  
    OPEN c_poliza(lv_id_poliza);
    FETCH c_poliza
      INTO lc_poliza;
    lb_found := c_poliza%FOUND;
    CLOSE c_poliza;
  
    IF NOT lb_found THEN
      pv_error := 'No se encontro la poliza. ' || lv_id_poliza;
      RETURN;
    END IF;
  
    lv_html := lv_html || '<html><head>
      <style>
        th {
          color: #ffffff;
          font-family: Verdana, Arial;
          font-size: 9pt;
          background-color: rgb(200,20,20);
        }
        
        td {
          color: #000000;
          font-family: Verdana, Arial;
          font-size: 9pt;
          background-color: rgb(204,204,204);
        }
        
        table {
          border-collapse: collapse;
          border-spacing:  3px;
        }
        
        table, th, td {
          border: 1px solid black;
        }
        
        p {
          font-family: Verdana, Arial;
          font-size: 12pt;
        }
      </style>
    
    
    </head><body><p>Fecha Generacion: <strong>' ||
               lc_poliza.fecha || '</strong></p>';
    --    lv_html := lv_html || '<div>'
    lv_html := lv_html || '<table>';
  
    -- Inicializar
    ln_cont := 0;
    ln_pos1 := 0;
    ln_pos2 := 0;
    lv_cad1 := '';
    lv_cad2 := '';
  
    -- Obtener el esquema para la cabecera    
    SELECT a.valor
      INTO lv_esq_cabecera
      FROM gsib_parametros_sap a
     WHERE a.id_tipo_parametro = 10189
       AND a.id_parametro = 'CABECERA_PREV';
  
    SELECT a.valor
      INTO lv_esq_cuerpo
      FROM gsib_parametros_sap a
     WHERE a.id_tipo_parametro = 10189
       AND a.id_parametro = 'CUERPO_PREV';
  
    -- Obtener la trama
    lv_cab_prev := lv_esq_cabecera;
    lv_trama    := substr(lc_poliza.trama, 1, instr(lc_poliza.trama, '['));
  
    WHILE length(lv_trama) > 0 LOOP
      ln_cont := ln_cont + 1;
      -- XML
      ln_pos2 := instr(lv_cab_prev, ';', 1);
      IF ln_pos2 = 0 THEN
        ln_pos2 := instr(lv_cab_prev, '[', 1);
      END IF;
      lv_cad2     := substr(lv_cab_prev, 1, ln_pos2 - 1);
      lv_cab_prev := substr(lv_cab_prev, ln_pos2 + 1);
      -- Trama
      ln_pos1 := instr(lv_trama, ';', 1);
      IF ln_pos1 = 0 THEN
        ln_pos1 := instr(lv_trama, '[', 1);
      END IF;
      lv_cad1  := substr(lv_trama, 1, ln_pos1 - 1);
      lv_trama := substr(lv_trama, ln_pos1 + 1);
      -- Reemplazar
      IF length(lv_cad1) > 0 AND instr(lv_cad1, '^') = 0 THEN
        IF lv_estilo = 'T' THEN
          lv_cad2 := substr(lv_cad2, instr(lv_cad2, '-') + 1);
        ELSIF lv_estilo = 'X' THEN
          lv_cad2 := substr(lv_cad2,
                            instr(lv_cad2, '*') + 1,
                            instr(lv_cad2, '-') - instr(lv_cad2, '*') - 1);
        END IF;
      
        lv_html := lv_html || '<tr>';
        lv_html := lv_html || '<th>';
        lv_html := lv_html || lv_cad2;
        lv_html := lv_html || '</th>';
        lv_html := lv_html || '<td>';
        lv_html := lv_html || lv_cad1;
        lv_html := lv_html || '</td>';
        lv_html := lv_html || '</tr>';
      END IF;
    END LOOP;
  
    lv_html := lv_html || '</table>';
  
    -- Inicializar
    ln_cont := 0;
    ln_pos1 := 0;
    ln_pos2 := 0;
    lv_cad1 := '';
    lv_cad2 := '';
  
    -- Obtener la trama
    lv_cue_prev := lv_esq_cuerpo;
    lv_trama    := substr(lc_poliza.trama, instr(lc_poliza.trama, '[') + 1);
  
    -- Pintar los titulos
    lv_html := lv_html || '<p>Detalles de la Poliza</p><table></thead>';
    WHILE length(lv_cue_prev) > 0 LOOP
      ln_cont := ln_cont + 1;
      -- XML
      ln_pos2 := instr(lv_cue_prev, ';', 1);
      IF ln_pos2 = 0 THEN
        ln_pos2 := instr(lv_cue_prev, ']', 1);
      END IF;
      lv_cad2     := substr(lv_cue_prev, 1, ln_pos2 - 1);
      lv_cue_prev := substr(lv_cue_prev, ln_pos2 + 1);
      -- Reemplazar
      IF length(lv_cad2) > 0 THEN
        IF lv_estilo = 'T' THEN
          lv_cad2 := substr(lv_cad2, instr(lv_cad2, '-') + 1);
        ELSIF lv_estilo = 'X' THEN
          lv_cad2 := substr(lv_cad2,
                            instr(lv_cad2, '*') + 1,
                            instr(lv_cad2, '-') - instr(lv_cad2, '*') - 1);
        END IF;
      
        IF instr(lv_cad2, '^') > 0 THEN
          lv_html := lv_html || '<th style="display: none;">';
          lv_html := lv_html || '';
          lv_html := lv_html || '</th>';
        ELSE
          lv_html := lv_html || '<th>';
          lv_html := lv_html || lv_cad2;
          lv_html := lv_html || '</th>';
        END IF;
      END IF;
    
    END LOOP;
  
    -- Inicializar para el cuerpo
    lv_html     := lv_html || '</thead></tbody>';
    ln_cont     := 0;
    lv_cue_prev := lv_esq_cuerpo;
  
    -- Pintar detalles
    WHILE length(lv_trama) > 0 LOOP
      ln_cont := ln_cont + 1;
      IF ln_cont = 1 THEN
        lv_html := lv_html || '<tr>';
      END IF;
    
      -- XML
      ln_pos2 := instr(lv_cue_prev, ';', 1);
      IF ln_pos2 = 0 THEN
        ln_pos2 := instr(lv_cue_prev, ']', 1);
      END IF;
      lv_cad2     := substr(lv_cue_prev, 1, ln_pos2 - 1);
      lv_cue_prev := substr(lv_cue_prev, ln_pos2 + 1);
      -- Trama
      ln_pos1 := instr(lv_trama, ';', 1);
      IF ln_pos1 = 0 THEN
        ln_pos1 := instr(lv_trama, ']', 1);
      END IF;
      lv_cad1  := substr(lv_trama, 1, ln_pos1 - 1);
      lv_trama := substr(lv_trama, ln_pos1 + 1);
    
      lv_cad_sap := substr(lv_cad2,
                           instr(lv_cad2, '*') + 1,
                           (instr(lv_cad2, '-') - instr(lv_cad2, '*') - 1));
    
      IF lv_cad_sap = 'NEWBS' THEN
        IF lv_cad1 = '40' THEN
          lv_debe_haber := 'D';
        ELSE
          lv_debe_haber := 'H';
        END IF;
      END IF;
    
      IF lv_cad_sap = 'WRBTR' THEN
        IF lv_debe_haber = 'D' THEN
          ln_suma_debe := ln_suma_debe +
                          to_number(lv_cad1, '99999999999999999990.00');
        ELSIF lv_debe_haber = 'H' THEN
          ln_suma_haber := ln_suma_haber +
                           to_number(lv_cad1, '99999999999999999990.00');
        END IF;
      END IF;
    
      -- Reemplazar
      IF length(lv_cad1) > 0 THEN
        IF lv_estilo = 'T' THEN
          lv_cad2 := substr(lv_cad2, instr(lv_cad2, '-') + 1);
        ELSIF lv_estilo = 'X' THEN
          lv_cad2 := substr(lv_cad2,
                            instr(lv_cad2, '*') + 1,
                            instr(lv_cad2, '-') - instr(lv_cad2, '*') - 1);
        END IF;
      
        IF instr(lv_cad1, '^') > 0 AND instr(lv_cad2, '^') > 0 THEN
          lv_html := lv_html || '<td style="display: none;">';
          lv_html := lv_html || '';
          lv_html := lv_html || '</td>';
        ELSE
          IF instr(lv_cad1, '^') > 0 THEN
            lv_cad1 := '';
          END IF;
          -- Dependiendo del TAG voy a organizar
          IF lv_cad_sap IN ('WRBTR', 'PRCTR', 'FWBAS') THEN
            lv_html := lv_html || '<td style="text-align: right;">';
          ELSE
            lv_html := lv_html || '<td style="text-align: center;">';
          END IF;
          --
          lv_html := lv_html || lv_cad1;
          lv_html := lv_html || '</td>';
        END IF;
      
      END IF;
    
      IF ln_cont = 34 THEN
        lv_debe_haber := '';
        ln_cont       := 0;
        lv_html       := lv_html || '</tr>';
        lv_cue_prev   := lv_esq_cuerpo;
      END IF;
    END LOOP;
  
    lv_html := lv_html || '</tbody></table>
      <p>Total Deber: <strong>' ||
               to_char(ln_suma_debe, lv_fmt_number) ||
               '</strong></p>
      <p>Total Haber: <strong>' ||
               to_char(ln_suma_haber, lv_fmt_number) || '</strong></p>      
    ';
  
    UPDATE gsib_tramas_sap a
       SET a.html = lv_html
     WHERE a.id_poliza = lv_id_poliza;
    COMMIT;
  
    -- Fin
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error. ' || lv_application || '. ' || SQLERRM || ' - ' ||
                  dbms_utility.format_error_backtrace || lv_application;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION abs_numero(valor NUMBER) RETURN VARCHAR2 IS
  BEGIN
    --SE FORMATEA LOS VALORES POR CONFIGURACION REGIONAL, NO DEBE DE IR ',' EN LAS TRAMAS 
    IF valor = 0 THEN
      RETURN '0';
    ELSIF valor < 1 AND valor > -1 AND valor != 0 THEN
      RETURN TRIM(REPLACE(to_char(abs(valor), '0.99'), ',', '.'));
    ELSE
      RETURN TRIM(REPLACE(to_char(abs(valor), '99999999999999999999.99'),
                          ',',
                          '.'));
    END IF;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reinicia_secuencia(pn_id_transaccion IN NUMBER) IS
  
    --CURSOR QUE OBTIENE EL ANIO DE LA ULTIMA POLIZA QUE SE GENERO
    CURSOR lc_valida_anio IS
      SELECT MAX(to_number(substr(id_poliza, 5, 4)))
        FROM gsib_tramas_sap d;
  
    ln_anio        NUMBER;
    ln_anio_actual NUMBER := to_number(to_char(SYSDATE, 'YYYY'));
  
    sql_cmd       VARCHAR2(500);
    pv_aplicacion VARCHAR2(200) := 'GSIB_API_SAP.REINICIA_SECUENCIA';
    pv_resultado  VARCHAR2(300);
  
  BEGIN
  
    gsib_security.dotracem(pv_aplicacion,
                           'Inicio de validacion del anio de la poliza',
                           pn_id_transaccion);
  
    OPEN lc_valida_anio;
    FETCH lc_valida_anio
      INTO ln_anio;
    CLOSE lc_valida_anio;
    --SI EL ANIO ACTUAL ES MAYOR AL ANIO DE LA ULTIMA POLIZA GENERADA REINICIO LA SECUENCIA
    IF ln_anio_actual > ln_anio THEN
      sql_cmd := 'DROP SEQUENCE ID_POLIZA_SAP';
      EXECUTE IMMEDIATE sql_cmd;
    
      sql_cmd := 'CREATE SEQUENCE ID_POLIZA_SAP START WITH 1 INCREMENT BY 1 nocache order maxvalue 999';
      EXECUTE IMMEDIATE sql_cmd;
      pv_resultado := 'Se reinicio la secuencia ID_POLIZA_SAP por que ya cambio el anio fiscal de ' ||
                      ln_anio || ' a ' || ln_anio_actual;
    ELSE
      pv_resultado := 'No se reinicio secuencia por que el anio fiscal es el mismo ' ||
                      ln_anio || '= ' || ln_anio_actual;
    END IF;
    gsib_security.dotraceh(pv_aplicacion, pv_resultado, pn_id_transaccion);
    gsib_security.dotracem(pv_aplicacion,
                           'Fin de validacion del anio de la poliza',
                           pn_id_transaccion);
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'Error al reiniciar la secuencia ID_POLIZA_SAP :' ||
                      SQLERRM || ' ' || pv_aplicacion;
      gsib_security.dotraceh(pv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 12/06/20115 15:46:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE activa_shell IS
    CURSOR c_verifica_estado IS
      SELECT *
        FROM gsib_activa_shell d
       WHERE d.estado = 'NO EJECUTADO'
       ORDER BY d.fecha_ingreso DESC;
    lv_aplicacion VARCHAR2(100) := 'GSIB_API_SAP.ACTIVA_SHELL';
    CURSOR c_id_transaccion IS
    --  SELECT TO_NUMBER(SC_ID_TRANSACCION_SAP.nextval) FROM DUAL;
      SELECT nvl(MAX(id_transaccion), 0) + 1 FROM gsib_novedades;
    ln_id_transaccion NUMBER := 0;
    lv_resultado      VARCHAR2(4000);
    lv_msj_tramas     VARCHAR2(4000);
    lb_encuentra      NUMBER := 0;
  BEGIN
    OPEN c_id_transaccion;
    FETCH c_id_transaccion
      INTO ln_id_transaccion;
    CLOSE c_id_transaccion;
    FOR i IN c_verifica_estado LOOP
    
      gsib_security.dotraceh(lv_aplicacion,
                             '**** Inicio activa shell ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             '');
      IF i.escenario = 'FACTURACION' THEN
        graba_safi_fact_sap.graba_safi_principal(i.fecha_corte,
                                                 i.producto,
                                                 'C',
                                                 lv_resultado,
                                                 lv_msj_tramas);
        --actualizo el estado
        UPDATE gsib_activa_shell d
           SET d.estado = 'EJECUTADO'
         WHERE d.fecha_corte = i.fecha_corte
           AND d.fecha_ingreso = i.fecha_ingreso
           AND d.escenario = i.escenario
           AND (d.producto = i.producto OR d.producto IS NULL);
        COMMIT;
        lb_encuentra := 1;
      ELSIF i.escenario = 'PROVISION' THEN
        IF i.producto = 'VOZ' THEN
          fin_provision.p_gen_preasiento(i.fecha_corte, lv_resultado);
          --actualizo el estado
          UPDATE gsib_activa_shell d
             SET d.estado = 'EJECUTADO'
           WHERE d.fecha_corte = i.fecha_corte
             AND d.fecha_ingreso = i.fecha_ingreso
             AND d.escenario = i.escenario
             AND d.producto = i.producto;
          COMMIT;
          lb_encuentra := 1;
        ELSIF i.producto = 'DTH' THEN
          fin_provision_dth.p_gen_preasiento(i.fecha_corte, lv_resultado);
          --actualizo el estado
          UPDATE gsib_activa_shell d
             SET d.estado = 'EJECUTADO'
           WHERE d.fecha_corte = i.fecha_corte
             AND d.fecha_ingreso = i.fecha_ingreso
             AND d.escenario = i.escenario
             AND d.producto = i.producto;
          COMMIT;
          lb_encuentra := 1;
        END IF;
      ELSIF i.escenario = 'DEVENGO' THEN
        IF i.producto = 'DTH' THEN
          dth_devengamiento_pre_sap.p_gen_preasiento(i.fecha_corte,
                                                     lv_resultado);
          --actualizo el estado
          UPDATE gsib_activa_shell d
             SET d.estado = 'EJECUTADO'
           WHERE d.fecha_corte = i.fecha_corte
             AND d.fecha_ingreso = i.fecha_ingreso
             AND d.escenario = i.escenario
             AND d.producto = i.producto;
          COMMIT;
          lb_encuentra := 1;
        END IF;
      END IF;
      gsib_security.dotraceh(lv_aplicacion,
                             '**** Fin activa shell ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             '');
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 12/06/20115 15:46:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  FUNCTION retorna_longitud(pv_id_poliza IN VARCHAR2) RETURN NUMBER IS
  
    CURSOR c_html(cv_id_poliza IN VARCHAR2) IS
      SELECT to_number(length(d.html)) tamanio
        FROM gsib_tramas_sap d
       WHERE d.id_poliza = cv_id_poliza;
    pn_longitud NUMBER := 0;
  BEGIN
    OPEN c_html(pv_id_poliza);
    FETCH c_html
      INTO pn_longitud;
    CLOSE c_html;
    RETURN pn_longitud;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 12/06/20115 15:46:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION retorna_html_forma(pv_id_poliza IN VARCHAR2,
                              pn_inicio    IN NUMBER,
                              pn_fin       IN NUMBER) RETURN VARCHAR2 IS
    CURSOR c_html(cv_id_poliza VARCHAR2) IS
      SELECT d.id_poliza, length(d.html) length_html, d.html
        FROM gsib_tramas_sap d
       WHERE d.id_poliza = cv_id_poliza;
    ln_longitud     NUMBER := 0;
    pv_porcion_html VARCHAR2(4000) := NULL;
    ln_inicio       NUMBER := 0;
    ln_fin          NUMBER := 0;
  BEGIN
    ln_inicio := pn_inicio;
    ln_fin    := pn_fin;
    FOR i IN c_html(pv_id_poliza) LOOP
      ln_longitud     := retorna_longitud(i.id_poliza);
      pv_porcion_html := substr(i.html, ln_inicio, ln_fin);
    END LOOP;
    RETURN pv_porcion_html;
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 27/06/20115 12:06:35
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE anula_polizas(pd_fecha          IN DATE, --DD/MM/YYYY
                          pv_escenario      IN VARCHAR2, -- FACTURACION, PROVISION, DEVENGO
                          pv_producto       IN VARCHAR2, --VOZ, DTH , NULL
                          pn_id_transaccion IN NUMBER,
                          pv_resultado      OUT VARCHAR2,
                          pv_polizas        OUT VARCHAR2) IS
    CURSOR c_obtiene_tramas(cv_fecha     DATE,
                            cv_escenario VARCHAR2,
                            cv_producto  VARCHAR2) IS
      SELECT t.estado, t.referencia, t.fecha_corte, t.id_poliza
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = cv_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE
             '%' || cv_escenario ||
             decode(cv_producto, 'VOZ', ' BSCS', 'DTH', ' DTH', '') || '%';
    ln_contador   NUMBER := 0;
    lv_aplicacion VARCHAR2(2250) := 'GSIB_API_SAP.C_OBTIENE_TRAMAS';
    lv_resul      VARCHAR2(1000);
    le_error EXCEPTION;
  BEGIN
    --valida fecha
    IF pd_fecha IS NULL THEN
      lv_resul := 'El parametro ingresado para PD_FECHA no es valido ';
      gsib_security.dotraceh(lv_aplicacion, lv_resul, pn_id_transaccion);
      RAISE le_error;
    END IF;
    -- valida escenario
    IF pv_escenario IS NULL OR
       (pv_escenario <> 'FACTURACION' AND pv_escenario <> 'PROVISION' AND
       pv_escenario <> 'DEVENGO') THEN
      lv_resul := 'El parametro ingresado para PV_ESCENARIO no es valido debe de ser : F = FACTURACION ; P = PROVICION ; D = DEVENGO .';
      gsib_security.dotraceh(lv_aplicacion, lv_resul, pn_id_transaccion);
      RAISE le_error;
    END IF;
    --valida los productos permitidos para cada escenario
    IF pv_escenario = 'FACTURACION' THEN
      IF pv_producto IS NOT NULL THEN
        IF pv_producto NOT IN ('DTH', 'VOZ') THEN
          lv_resul := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ,NULL => ';
          gsib_security.dotraceh(lv_aplicacion,
                                 lv_resul,
                                 pn_id_transaccion);
          RAISE le_error;
        END IF;
      END IF;
    END IF;
    IF pv_escenario = 'PROVISION' THEN
      IF pv_producto NOT IN ('DTH', 'VOZ') OR pv_producto IS NULL THEN
        lv_resul := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ => ';
        gsib_security.dotraceh(lv_aplicacion, lv_resul, pn_id_transaccion);
        RAISE le_error;
      END IF;
    END IF;
    IF pv_escenario = 'DEVENGO' THEN
      IF pv_producto NOT IN ('DTH') OR pv_producto IS NULL THEN
        lv_resul := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ => ';
        gsib_security.dotraceh(lv_aplicacion, lv_resul, pn_id_transaccion);
        RAISE le_error;
      END IF;
    END IF;
    FOR i IN c_obtiene_tramas(pd_fecha, pv_escenario, pv_producto) LOOP
      ln_contador := ln_contador + 1;
      UPDATE gsib_tramas_sap d
         SET d.estado      = 'ANULADA',
             d.observacion = d.observacion || chr(13) || ' => Anulada el : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss')
       WHERE d.id_poliza = i.id_poliza
         AND d.fecha_corte = i.fecha_corte
         AND d.referencia = i.referencia;
      COMMIT;
      pv_polizas := pv_polizas || ' ' || i.id_poliza || ' => ' ||
                    i.referencia || ' ;' || chr(13);
      gsib_security.dotraceh(lv_aplicacion,
                             'se actualizo la poliza : ' || i.id_poliza ||
                             ' => ' || i.referencia,
                             pn_id_transaccion);
    END LOOP;
    IF ln_contador > 0 THEN
      lv_resul := ln_contador || ' Registros Actualizados Correctamente';
    ELSE
      lv_resul := 'No se encontraron Registros que Actualizar';
    END IF;
    pv_resultado := lv_resul;
    gsib_security.dotraceh(lv_aplicacion, pv_resultado, pn_id_transaccion);
  EXCEPTION
    WHEN le_error THEN
      pv_resultado := lv_resul || ' ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
    WHEN OTHERS THEN
      pv_resultado := 'ERROR .' || SQLERRM || ' ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 12/06/20115 15:46:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION retorna_longitud_reporte(pv_nombre_reporte IN VARCHAR2)
    RETURN NUMBER IS
    CURSOR c_reporte_html(cv_nombre_reporte IN VARCHAR2) IS
      SELECT to_number(length(d.reporte_html)) tamanio
        FROM gsib_reportes_html d
       WHERE d.nombre_reporte = cv_nombre_reporte
         AND d.id_reporte =
             (SELECT MAX(c.id_reporte)
                FROM gsib_reportes_html c
               WHERE c.nombre_reporte = cv_nombre_reporte);
    pn_longitud NUMBER := 0;
  BEGIN
    OPEN c_reporte_html(lower(pv_nombre_reporte));
    FETCH c_reporte_html
      INTO pn_longitud;
    CLOSE c_reporte_html;
    RETURN pn_longitud;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 27/06/20115 14:54:23
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  FUNCTION retorna_reporte_html_forma(pv_nombre_reporte IN VARCHAR2,
                                      pn_inicio         IN NUMBER,
                                      pn_fin            IN NUMBER)
    RETURN VARCHAR2 IS
    CURSOR c_html_reporte(cv_nombre_reporte VARCHAR2) IS
      SELECT d.nombre_reporte,
             length(d.reporte_html) length_html,
             d.reporte_html
        FROM gsib_reportes_html d
       WHERE d.nombre_reporte = cv_nombre_reporte
         AND d.id_reporte =
             (SELECT MAX(c.id_reporte)
                FROM gsib_reportes_html c
               WHERE c.nombre_reporte = cv_nombre_reporte);
    ln_longitud     NUMBER := 0;
    pv_porcion_html VARCHAR2(4000) := NULL;
    ln_inicio       NUMBER := 0;
    ln_fin          NUMBER := 0;
  BEGIN
    ln_inicio := pn_inicio;
    ln_fin    := pn_fin;
    FOR i IN c_html_reporte(lower(pv_nombre_reporte)) LOOP
      ln_longitud := retorna_longitud_reporte(i.nombre_reporte);
    
      pv_porcion_html := substr(i.reporte_html, ln_inicio, ln_fin);
    END LOOP;
    RETURN pv_porcion_html;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 29/09/2015 11:05:24
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --===================================================================================

  PROCEDURE crea_jobs(pv_resultado OUT VARCHAR2) IS
    pv_aplicacion VARCHAR2(1000) := 'GSIB_API_SAP.CREA_JOBS';
    ln_job        NUMBER;
  
  BEGIN
    dbms_job.submit(job       => ln_job,
                    what      => 'GSIB_API_SAP.ACTIVA_SHELL;',
                    next_date => SYSDATE);
   -- dbms_job.RUN(ln_job, TRUE);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'Error : ' || SQLERRM || ' ' || pv_aplicacion;
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 20/10/2015 15:14:00
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --===================================================================================

  PROCEDURE elimina_jobs IS
  
    CURSOR c_elimina_job IS
      SELECT job FROM dba_jobs d WHERE upper(d.what) LIKE 'GSIB_API_SAP%';
  
  BEGIN
    FOR i IN c_elimina_job LOOP
      dbms_job.remove(i.job);
      COMMIT;
    END LOOP;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/10/2015 09:15:54
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --Procediemeinto que permite respaldar la tabla servicios_prepagados_dth_sAP
  --===================================================================================

  PROCEDURE respaldo_servicios_dth(pd_fecha_corte    IN DATE,
                                   pv_tipo           IN VARCHAR2,
                                   pn_id_transaccion IN NUMBER,
                                   pv_resultado      OUT VARCHAR2) IS
    lv_aplicacion            VARCHAR2(200) := ' GRABA_SAFI_FACT_SAP.RESPALDO_SERVICIOS_DTH';
    lv_sentencia_crear_table VARCHAR2(2000);
    lv_sentencia_drop_table  VARCHAR2(2000);
    lv_nombre_tabla          VARCHAR2(100);
    CURSOR lc_valida_table(cv_nombre VARCHAR2) IS
      SELECT COUNT(*)
        FROM all_objects a
       WHERE a.object_type = 'TABLE'
         AND upper(a.object_name) = upper(cv_nombre);
         
    CURSOR lc_nombre_tbl_respaldo(cv_nombre in varchar2) IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = '10183'
         AND id_parametro = cv_nombre;      
         
         
  
    lv_valida_table NUMBER := 0;
    -- lb_found        BOOLEAN;
    le_error EXCEPTION;
    lv_mensaje VARCHAR2(2000);
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '****INICIO RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    IF pv_tipo = 'FACTURACION' THEN
      
     OPEN lc_nombre_tbl_respaldo('TABLA_RESP_FACTURACION');
    FETCH lc_nombre_tbl_respaldo
      INTO lv_nombre_tabla;
    CLOSE lc_nombre_tbl_respaldo;
    
    
      lv_nombre_tabla := replace (lv_nombre_tabla,'<<<FECHA<>>',to_char(pd_fecha_corte, 'yyyymmdd'));
                         
    
    ELSIF pv_tipo = 'DEVENGO' THEN
    
      OPEN lc_nombre_tbl_respaldo('TABLA_RESP_DEVENGO');
    FETCH lc_nombre_tbl_respaldo
      INTO lv_nombre_tabla;
    CLOSE lc_nombre_tbl_respaldo;
    
          lv_nombre_tabla := replace (lv_nombre_tabla,'<<<FECHA<>>',to_char(pd_fecha_corte, 'yyyymmdd'));

    
    ELSE
      lv_mensaje := 'El tipo debe se "FACTURACION" O "DEVENGO" ';
      RAISE le_error;
    END IF;
  
    gsib_security.dotraceh(lv_aplicacion,
                           'Nombre tabla respaldo : ' || lv_nombre_tabla,
                           pn_id_transaccion);
  
    OPEN lc_valida_table(lv_nombre_tabla);
    FETCH lc_valida_table
      INTO lv_valida_table;
    CLOSE lc_valida_table;
  
    --si ya existe la tabla la elimino
    IF lv_valida_table >= 1 THEN
      lv_sentencia_drop_table := 'DROP TABLE ' || lv_nombre_tabla;
      gsib_security.dotraceh(lv_aplicacion,
                             lv_sentencia_drop_table,
                             pn_id_transaccion);
    
      EXECUTE IMMEDIATE lv_sentencia_drop_table;
    
      lv_sentencia_crear_table := 'CREATE TABLE ' || lv_nombre_tabla ||
                                  ' AS SELECT * FROM  servicios_prepagados_dth_SAP ';
      gsib_security.dotraceh(lv_aplicacion,
                             lv_sentencia_crear_table,
                             pn_id_transaccion);
    
      EXECUTE IMMEDIATE lv_sentencia_crear_table;
    
      lv_mensaje := 'La tabla ' || lv_nombre_tabla ||
                    ' ya existe , se procedio a eliminarla';
    
    ELSE
    
      lv_sentencia_crear_table := 'CREATE TABLE ' || lv_nombre_tabla ||
                                  ' AS SELECT * FROM  servicios_prepagados_dth_SAP ';
      EXECUTE IMMEDIATE lv_sentencia_crear_table;
    
      lv_mensaje := 'Se Creo correctamente la tabla ' || lv_nombre_tabla;
    
    END IF;
    gsib_security.dotraceh(lv_aplicacion, lv_mensaje, pn_id_transaccion);
  
    gsib_security.dotraceh(lv_aplicacion,
                           '****FIN RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  EXCEPTION
  
    WHEN le_error THEN
      pv_resultado := pv_resultado || ' ' || SQLERRM || ' ' ||
                      lv_aplicacion;
    
    WHEN OTHERS THEN
      pv_resultado := 'ERROR: ' || SQLERRM || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
      gsib_security.dotraceh(lv_aplicacion,
                             '****FIN RESPALDO SERVICIOS DTH FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
  END;

END gsib_api_sap;
/
