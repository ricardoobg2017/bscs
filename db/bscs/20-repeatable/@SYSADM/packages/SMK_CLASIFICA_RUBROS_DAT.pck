create or replace package SMK_CLASIFICA_RUBROS_DAT is

  -- Author  : Lorena Sellers
  -- Created : 12/11/2008 14:43:13
  -- Purpose : Clasifica los rubros de la tabla CO_FACT_ddmmyyyy en SMA y Otros
  
gv_co_fact   varchar2(30):= 'CO_FACT_SMA_';  
  
PROCEDURE SM_CLASIFICA
(         
          pd_fech_corte date,
          pn_hilo       number,
          pv_msg_error out varchar2
);
  
PROCEDURE CREA_TABLA_FACT_SMA
(         
          pd_fech_corte date,
          pv_msg_error out varchar2
);

end SMK_CLASIFICA_RUBROS_DAT;
/
CREATE OR REPLACE PACKAGE BODY SMK_CLASIFICA_RUBROS_DAT is


PROCEDURE SM_CLASIFICA (pd_fech_corte date,
                        pn_hilo       number,
                        pv_msg_error out varchar2) is

lvSentencia       varchar2(4000);
ln_existe_tabla   number;
ind               number;
pv_error          varchar2(1000);
le_error          exception;
ln_commit         number := 0;
pragma autonomous_transaction;

lt_customer_id cot_number := cot_number();
lt_ohrefnum cot_string := cot_string();
lt_valor_sma cot_number:= cot_number();
lt_valor_otros cot_number := cot_number();
lt_creditos cot_number := cot_number();
lt_cargos     cot_number := cot_number();
lt_custcode   cot_string := cot_string();
l_tabla_cofact varchar2(100);
l_tabla_cofactsma varchar2(100);

BEGIN--comentariado por pruebas
    l_tabla_cofact:='CO_FACT_'||to_char(pd_fech_corte,'ddMMyyyy');
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy'); 
  /*
 --[3815] Verifica si existe la tabla co_fact_ddmmyyyy del periodo enviado por par�metro -- LSE 
     lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from all_tables '||
                               ' where table_name ='''||l_tabla_cofact||''';
                               end;'; 
     execute immediate lvSentencia using out ln_existe_tabla;
           
     if ln_existe_tabla is null or ln_existe_tabla = 0 then
           pv_error := 'NO EXISTE LA TABLA '||l_tabla_cofact;
           raise le_error;
     end if; 
     
     ln_existe_tabla := null; 
     
     -- [3815] Verifica si existe la tabla co_fact_sma_ddmmyyyy
     -- Crea la tabla co_fact_sma_ddmmyyyy si no existe, si existe la trunca --LSE
       */
     lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from user_all_tables '||
                               ' where table_name ='''||l_tabla_cofactsma||''';
                     end;';
     execute immediate lvSentencia using out ln_existe_tabla;    
     commit; 
    l_tabla_cofact:='CO_FACT_'||to_char(pd_fech_corte,'ddMMyyyy');
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy');    
        
     if ln_existe_tabla is null or ln_existe_tabla = 0 then
           smk_clasifica_rubros_dat.CREA_TABLA_FACT_SMA(pd_fech_corte,pv_error);
    -- else
   --      lvSentencia := 'drop table '||gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy')||'';
     --    execute immediate lvSentencia;
      --   smk_clasifica_rubros_dat.CREA_TABLA_FACT_SMA(pd_fech_corte,pv_error);
     end if;
     
     --[3815] Inserta en la tabla co_fact_sma_ddmmyyyy los registros de la co_fact_ddmmyyyy --LSE
     
     lvSentencia := 'insert into '||l_tabla_cofactsma||
                          '(customer_id,
                          custcode,
                          ohrefnum,
                          nombre_cli,
                          apellido_cli,
                          producto,
                          cccity,
                          cost_id,
                          cost_desc,
                          credit_rat,
                          grupo,
                          valor,
                          descuento,
                          valor_total)
                          select f.customer_id,
                          f.custcode ,
                          f.ohrefnum ,
                          c.ccfname ,
                          c.cclname ,
                          f.producto ,
                          f.cccity ,
                          decode(f.cost_desc,''Guayaquil'',1,''Quito'',2), 
                          f.cost_desc,
                          cu.cstradecode,
                          cu.prgcode,
                          SUM(nvl(f.valor,0)),
                          SUM(nvl(f.descuento,0)),
                          SUM(nvl(f.valor,0)) - SUM(nvl(f.descuento,0))
                          from '||l_tabla_cofact||' f, customer_all cu, cob_servicios s, ccontact_all c
                          where cu.customer_id = c.customer_id 
                          and c.customer_id =f.customer_id 
                          and c.ccbill = ''X''
                          and f.servicio = s.servicio
                          and f.ctactble = s.ctactble 
                          and substr(f.tipo,1,3) not in (''006'')
                          AND f.servicio <> 45 
                          and substr(f.custcode,length(f.custcode),length(f.custcode)) = '||pn_hilo||
                          ' GROUP BY f.CUSTOMER_ID, f.CUSTCODE, f.ohrefnum,c.ccfname,c.cclname, f.producto,f.cccity,
                          decode(f.cost_desc,''Guayaquil'',1,''Quito'',2), f.cost_desc,cu.cstradecode,cu.prgcode';
                          
     
     execute immediate lvSentencia;
    commit; 
     
     --[3815] Extrae los valores de las factura si es SMA
       
     lvSentencia := 'begin
                          select f.customer_id, f.ohrefnum, SUM(nvl(f.valor,0)) - SUM(nvl(f.descuento,0))
                          bulk collect into :1, :2, :3
                          from '||l_tabla_cofact||' f, cob_servicios s, ccontact_all c
                          where c.customer_id =f.customer_id 
                          and c.ccbill = ''X''
                          and f.servicio = s.servicio
                          and f.ctactble = s.ctactble 
                          and substr(f.tipo,1,3) not in (''006'')  
                          AND f.servicio <> 45 
                          and s.sma = ''S''
                          and substr(f.custcode,length(f.custcode),length(f.custcode)) = '||pn_hilo||
                          ' GROUP BY f.CUSTOMER_ID, f.ohrefnum;
                    end;';
     
     execute immediate lvSentencia using out lt_customer_id, out lt_ohrefnum, out lt_valor_sma ;
     commit;
    
     --[3815] Inserta en la co_fact_sma_ddmmyyyy los valores SMA
     ln_commit := 0; 
     if lt_customer_id.count > 0 then
           for ind in lt_customer_id.first..lt_customer_id.last loop
                lvSentencia := 'update '||l_tabla_cofactsma||
                          ' set valor_sma = to_number('''||to_number(lt_valor_sma(ind)) ||''')'||
                          ' where ohrefnum= ''' ||lt_ohrefnum(ind)||'''';
                execute immediate lvSentencia;
           
            
           if ln_commit = 200 then
                 commit;
                 ln_commit:=0;
           else 
                ln_commit:=ln_commit+1;
           end if;
           
           end loop;
          
     end if;
     commit;
     
     
     
     lt_valor_sma.delete;
     lt_customer_id.delete;
     lt_ohrefnum.delete;
     
     
     --[3815] Extrae los valores de la factura que no son SMA
    
     
     lvSentencia := 'begin
                          SELECT c.customer_id, c.ohrefnum, c.valor_total - nvl(c.valor_sma,0)  
                          bulk collect into :1, :2 ,:3
                          FROM  '||l_tabla_cofactsma||' c
                          where c.valor_otros is null
                          and substr(c.custcode,length(c.custcode),length(c.custcode)) = '||pn_hilo||';
                    end;';
     execute immediate lvSentencia using out lt_customer_id, out lt_ohrefnum, out lt_valor_otros ;                
     
     ind := 0;
     --[3815] Inserta en la co_fact_sma_ddmmyyyy los valores que no son SMA
     ln_commit := 0;
     if lt_customer_id.count > 0 then
           for ind in lt_customer_id.first..lt_customer_id.last loop
                lvSentencia := 'update '||l_tabla_cofactsma||
                          ' set valor_otros = to_number('''||lt_valor_otros(ind) ||''')'||
                          ' where ohrefnum= ''' ||lt_ohrefnum(ind)||'''';
                execute immediate lvSentencia;
                
                if ln_commit = 200 then
                     commit;
                     ln_commit:=0;
                else 
                     ln_commit:=ln_commit+1;
                end if;
                
           end loop;
     end if;
     commit;
     
     lt_valor_otros.delete;
     lt_customer_id.delete;
     lt_ohrefnum.delete;
     
     
     --[3815] Extrae los cr�ditos aplicados a la factura
     
     lvSentencia := 'begin
                          select f.customer_id, f.ohrefnum, sum(f.valor) - sum(f.descuento) 
                          bulk collect into :1, :2, :3
                          from '||l_tabla_cofact||' f ,' ||l_tabla_cofactsma||' c
                          where c.ohrefnum = f.ohrefnum
                          and substr(f.tipo,1,3) in (''006'') 
                          and substr(f.custcode,length(f.custcode),length(f.custcode)) = '||pn_hilo||
                          ' GROUP BY f.CUSTOMER_ID, f.ohrefnum;
                    end;';
     
     execute immediate lvSentencia using out lt_customer_id, out lt_ohrefnum, out lt_creditos ;
     commit;
     
     ind := 0;
     --[3815] Inserta en la co_fact_sma_ddmmyyyy los cr�ditos aplicados a la factura
     ln_commit := 0;
     if lt_customer_id.count > 0 then
           for ind in lt_customer_id.first..lt_customer_id.last loop
              if lt_creditos(ind) <> 0 then  
                lvSentencia := 'update ' ||l_tabla_cofactsma||
                          ' set valor_creditos = to_number('''||lt_creditos(ind)||''')'||
                          ' where customer_id = '||lt_customer_id(ind)||
                          ' and ohrefnum= ''' ||lt_ohrefnum(ind)||'''';
                execute immediate lvSentencia;
                if ln_commit = 200 then
                     commit;
                     ln_commit:=0;
                else 
                     ln_commit:=ln_commit+1;
                end if;
             end if;
           end loop;
     end if;
     commit;
     
     lt_creditos.delete;
     lt_customer_id.delete;
     lt_ohrefnum.delete;
    
    ind:= 0; 
 ----------------------------------------------------------------------
  --[3815] Extrae los cargos aplicados a la factura
      lvSentencia := 'begin
                           select f.customer_id, 
                           f.ohrefnum,
                           (f.valor - f.descuento) 
                           bulk collect into :1, :2, :3
                           from '||l_tabla_cofact||' f,'||l_tabla_cofactsma||' c
                           where f.customer_id = c.customer_id
                           and f.ohrefnum = c.ohrefnum 
                           and f.servicio = 45 
                           and substr(f.custcode,length(f.custcode),length(f.custcode)) = '||pn_hilo||';
                     end;';
    
    execute immediate lvSentencia using out lt_customer_id, out lt_ohrefnum, out lt_cargos;
   
   
   if lt_customer_id.count > 0 then
           for ind in lt_customer_id.first..lt_customer_id.last loop
                lvSentencia := 'update '||l_tabla_cofactsma||
                          ' set valor_cargos = to_number('''||lt_cargos(ind)||''')'||
                          ' where ohrefnum= ''' ||lt_ohrefnum(ind)||'''';
                execute immediate lvSentencia;
                
                if ln_commit = 200 then
                     commit;
                     ln_commit:=0;
                else 
                     ln_commit:=ln_commit+1;
                end if;
                
           end loop;
     end if;
     commit;
     
     lt_customer_id.delete;
     lt_custcode.delete;
     lt_ohrefnum.delete;
     lt_cargos.delete;
   
        
EXCEPTION
     when le_error then
     pv_msg_error := pv_error;
     when others then
     pv_msg_error := 'ERROR EN EL PROC. SM_CLASIFICA: '||sqlerrm;

END SM_CLASIFICA;

 
PROCEDURE CREA_TABLA_FACT_SMA (pd_fech_corte date,
                               pv_msg_error out varchar2) is


lvSentencia      varchar2(1000);
l_tabla_cofactsma varchar2(100);                               
    BEGIN
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy');
           lvSentencia := 'create table '||l_tabla_cofactsma||
                                     ' (CUSTOMER_ID         NUMBER,'|| 
                                      ' CUSTCODE            VARCHAR2(24),'||
                                      ' OHREFNUM            VARCHAR2(30),'|| 
                                      ' NOMBRE_CLI          VARCHAR2(50),'|| 
                                      ' APELLIDO_CLI        VARCHAR2(50),'||
                                      ' CCCITY              VARCHAR2(40),'||
                                      ' PRODUCTO            VARCHAR2(30),'||
                                      ' VALOR               NUMBER(16,2),'||
                                      ' DESCUENTO           NUMBER(16,2),'||
                                      ' VALOR_TOTAL         NUMBER(16,2),'|| 
                                      ' VALOR_SMA           NUMBER(16,2),'||
                                      ' VALOR_OTROS         NUMBER(16,2),'||
                                      ' VALOR_CREDITOS      NUMBER(16,2),'||
                                      ' VALOR_CARGOS        NUMBER(16,2),'||
                                      ' COST_ID             NUMBER,'||
                                      ' COST_DESC           VARCHAR2(30),'||
                                      ' CREDIT_RAT          VARCHAR2(10),'||
                                      ' GRUPO               VARCHAR2(10))';
                             
                                     
           execute immediate lvsentencia;
           commit;
           
           lvSentencia := 'grant all on '||gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy')||' to public';
           execute immediate lvSentencia;
                     
    EXCEPTION
    when others then
       pv_msg_error := 'FUNCION CREA_TABLA_FACT_SMA: '||sqlerrm;
    
    END CREA_TABLA_FACT_SMA;
END SMK_CLASIFICA_RUBROS_DAT;
/
