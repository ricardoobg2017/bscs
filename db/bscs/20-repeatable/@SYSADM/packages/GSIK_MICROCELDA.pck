CREATE OR REPLACE package GSIK_MICROCELDA is

  -- Author  : BMORAE
  -- Created : 19/04/2006 11:13:41
  -- Purpose : Corrige el problema de redondeo en microcelda
  
    
    PROCEDURE ACTUALIZA_VISTA(Pv_Ciclo VARCHAR2,
                              Pv_error out varchar2);                         
                              
--    PROCEDURE ACTUALIZA_MICROCELDA(Pv_error out varchar2);                                  

  ---  PROCEDURE MAIN;



end GSIK_MICROCELDA;
/
CREATE OR REPLACE package body GSIK_MICROCELDA is

   
    PROCEDURE ACTUALIZA_VISTA(Pv_Ciclo VARCHAR2,
                              Pv_error out varchar2)IS 
    LV_NOMBRE_TABLA  VARCHAR2(10):= 'UDR_LT_';
    LV_NOMBRE_INDICE VARCHAR2(15):= 'IDX_MICROCELL_';
    LN_VALOR_TABLA   NUMBER :=0;
    LN_VALOR_INDICE NUMBER:=0;
    LV_TABLA_ACTUAL    VARCHAR2(80):= NULL;
    LV_INDICE_ACTUAL   VARCHAR2(80):= NULL;
    LV_TABLA_ANTERIOR  VARCHAR2(80):= NULL;                                   
    LV_INDICE_ANTERIOR VARCHAR2(80):= NULL;
    lvSentencia        VARChAR2(20000):= null;
    lvSentencia_final  VARChAR2(20000):= null;
    lvMensErr          VARCHAR2(2000):= null;
    
    CURSOR C_BUSCA_TABLA(cv_tabla VARCHAR2) IS
           SELECT COUNT(*) FROM ALL_TABLES
           WHERE TABLE_NAME = cv_tabla;
    
    CURSOR C_BUSCA_INDICE (cv_tabla VARCHAR2,cv_indice VARCHAR2) IS
           SELECT COUNT(*) FROM ALL_CONSTRAINTS
           WHERE TABLE_NAME = cv_tabla AND
                 CONSTRAINT_NAME = cv_indice; 
    
    BEGIN 
        LV_TABLA_ACTUAL := LV_NOMBRE_TABLA||TO_CHAR(SYSDATE,'YYYYMM');
        lv_indice_actual:= LV_NOMBRE_INDICE||TO_CHAR(SYSDATE,'MM');
        
        IF C_BUSCA_TABLA%ISOPEN THEN 
           CLOSE C_BUSCA_TABLA;
        END IF;
        
        OPEN C_BUSCA_TABLA(LV_TABLA_ACTUAL);
        FETCH C_BUSCA_TABLA INTO LN_VALOR_TABLA;
        IF LN_VALOR_TABLA = 0 THEN
           Pv_error := 'No se encuentra la tabla '||LV_TABLA_ACTUAL;
           CLOSE C_BUSCA_TABLA;
           RETURN;
        END IF;           
        CLOSE C_BUSCA_TABLA;
        
        IF C_BUSCA_INDICE%ISOPEN THEN 
           CLOSE C_BUSCA_INDICE;
        END IF;
        
        OPEN C_BUSCA_INDICE(LV_TABLA_ACTUAL,LV_INDICE_ACTUAL);
        FETCH C_BUSCA_INDICE INTO Ln_Valor_Indice;
        IF Ln_Valor_Indice = 0 THEN
           Pv_error := 'No se encuentra el indice '||Lv_Indice_Actual;
           CLOSE C_BUSCA_INDICE;
           RETURN;
        END IF;
        CLOSE C_BUSCA_INDICE;        
    
       lvSentencia := 'CREATE OR REPLACE VIEW MICROCELL_V AS'||
                      'select /*+ index('||
                      LV_TABLA_ACTUAL||
                      ','||
                      LV_INDICE_ACTUAL||
                      ')*/ '||
                      'rowid,rated_flat_amount from '||
                      LV_TABLA_ACTUAL||
                      ' where mc_scalefactor<1 and rated_flat_amount>0 and  length(rated_flat_amount) >6'; 
       lvSentencia_final:= lvSentencia;
       
       IF PV_CICLO = '02' THEN 
          LN_VALOR_TABLA := 0; 
          LN_VALOR_INDICE:= 0;           
          LV_TABLA_ANTERIOR := LV_NOMBRE_TABLA||TO_CHAR(ADD_MONTHS(SYSDATE,-1),'YYYYMM');
          lv_indice_ANTERIOR:= LV_NOMBRE_INDICE||TO_CHAR(ADD_MONTHS(SYSDATE,-1),'MM');
          
          IF C_BUSCA_TABLA%ISOPEN THEN 
             CLOSE C_BUSCA_TABLA;
          END IF;
            
          OPEN C_BUSCA_TABLA(LV_TABLA_ANTERIOR);
          FETCH C_BUSCA_TABLA INTO LN_VALOR_TABLA;
          IF LN_VALOR_TABLA = 0 THEN
             Pv_error := 'No se encuentra la tabla '||LV_TABLA_ANTERIOR;
             CLOSE C_BUSCA_TABLA;
             RETURN;
          END IF;
            CLOSE C_BUSCA_TABLA;
            
            IF C_BUSCA_INDICE%ISOPEN THEN 
               CLOSE C_BUSCA_INDICE;
            END IF;
            
            OPEN C_BUSCA_INDICE(LV_TABLA_ANTERIOR,LV_INDICE_ANTERIOR);
            FETCH C_BUSCA_INDICE INTO Ln_Valor_Indice;
            IF Ln_Valor_Indice = 0 THEN
               Pv_error := 'No se encuentra el indice '||Lv_Indice_ANTERIOR;
               CLOSE C_BUSCA_INDICE;
               RETURN;
            END IF;           
            CLOSE C_BUSCA_INDICE;                  
            
            lvSentencia := ' UNION ALL '||
                           'select /*+ index('||
                           LV_TABLA_ANTERIOR||
                           ','||
                           LV_INDICE_ANTERIOR||
                           ')*/ '||
                           'rowid,rated_flat_amount from '||
                           LV_TABLA_ANTERIOR||
                           ' where mc_scalefactor<1 and rated_flat_amount>0 and  length(rated_flat_amount) >6';             
       end if;                    
                           
            lvSentencia_FINAL := lvSentencia_FINAL + LVSENTENCIA;                     
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                          
--    END ACTUALIZA_VISTA;
    END;
    
    
    
                                                       
                              
--    PROCEDURE ACTUALIZA_MICROCELDA(Pv_error out varchar2);                                  

--    PROCEDURE MAIN;
END GSIK_MICROCELDA;
/*EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;        
    END ACTUALIZA_CMSEP;*/
/

