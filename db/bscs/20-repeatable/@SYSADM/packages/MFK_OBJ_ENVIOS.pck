CREATE OR REPLACE PACKAGE MFK_OBJ_ENVIOS
/* 
 ||   Name       : MFK_OBJ_ENVIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */
 IS
  /*HELP 
  Overview of MFK_OBJ_ENVIOS
  HELP*/

  /*EXAMPLES 
  Examples of test 
  EXAMPLES*/

  /* Constants */

  /* Exceptions */

  /* Variables */

  /* Toggles */

  /* Windows */

  /* Programs */
  /*
   ||   Created on : 20-06-2011 Cima
   ||   Comments   : Se agrego 3 nuevos campos 
   ||   [6092] Envio Masivo MBU   
  */

  PROCEDURE MFP_HELP(context_in IN VARCHAR2 := NULL);

  PROCEDURE MFP_INSERTAR(PV_DESCRIPCION          IN MF_ENVIOS.DESCRIPCION%TYPE,
                         Pv_Estado               IN MF_ENVIOS.ESTADO%TYPE,
                         PN_CANTIDAD_MENSAJES    IN MF_ENVIOS.CANTIDAD_MENSAJES%TYPE,
                         PN_HORAS_SEPARACION_MSN IN MF_ENVIOS.HORAS_SEPARACION_MSN%TYPE,
                         PV_FORMA_ENVIO          IN MF_ENVIOS.FORMA_ENVIO%TYPE,
                         PV_MENSAJE              IN MF_ENVIOS.MENSAJE%TYPE,
                         PN_MONTO_MINIMO         IN MF_ENVIOS.MONTO_MINIMO%TYPE,
                         PN_MONTO_MAXIMO         IN MF_ENVIOS.MONTO_MAXIMO%TYPE,
                         PV_REGION               IN MF_ENVIOS.REGION%TYPE,
                         PV_REQUIERE_EDAD_MORA   IN MF_ENVIOS.REQUIERE_EDAD_MORA%TYPE,
                         PN_TIPO_CLIENTE         IN MF_ENVIOS.TIPO_CLIENTE%TYPE,
                         PV_USUARIO_INGRESO      IN MF_ENVIOS.USUARIO_INGRESO%TYPE,
                         PD_FECHA_INGRESO        IN MF_ENVIOS.FECHA_INGRESO%TYPE,
                         PV_USUARIO_MODIFICACION IN MF_ENVIOS.USUARIO_MODIFICACION%TYPE,
                         PD_FECHA_MODIFICACION   IN MF_ENVIOS.FECHA_MODIFICACION%TYPE,
                         PN_IDENVIO              OUT MF_ENVIOS.IDENVIO%TYPE,
                         PV_MSG_ERROR            IN OUT VARCHAR2,
                         Pv_Ciclo                IN MF_ENVIOS.ID_CICLO%TYPE, --JRC
                         PV_TIPO_CLIENTE         IN VARCHAR2 DEFAULT NULL, --MBU
                         PV_CATEG_CLIENTE        IN VARCHAR2 DEFAULT NULL, --MBU
                         PV_SCORE                  IN VARCHAR2 DEFAULT 'N',                         
                         PV_TIPO_ENVIO           IN MF_ENVIOS.TIPO_ENVIO%TYPE DEFAULT Null,
                         PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL);
  --
  PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO              IN MF_ENVIOS.IDENVIO%TYPE,
                           PV_DESCRIPCION          IN MF_ENVIOS.DESCRIPCION%TYPE,
                           Pv_Estado               IN MF_ENVIOS.ESTADO%TYPE,
                           PN_CANTIDAD_MENSAJES    IN MF_ENVIOS.CANTIDAD_MENSAJES%TYPE,
                           PN_HORAS_SEPARACION_MSN IN MF_ENVIOS.HORAS_SEPARACION_MSN%TYPE,
                           PV_FORMA_ENVIO          IN MF_ENVIOS.FORMA_ENVIO%TYPE,
                           PV_MENSAJE              IN MF_ENVIOS.MENSAJE%TYPE,
                           PN_MONTO_MINIMO         IN MF_ENVIOS.MONTO_MINIMO%TYPE,
                           PN_MONTO_MAXIMO         IN MF_ENVIOS.MONTO_MAXIMO%TYPE,
                           PV_REGION               IN MF_ENVIOS.REGION%TYPE,
                           PV_REQUIERE_EDAD_MORA   IN MF_ENVIOS.REQUIERE_EDAD_MORA%TYPE,
                           PN_TIPO_CLIENTE         IN MF_ENVIOS.TIPO_CLIENTE%TYPE,
                           PV_USUARIO_INGRESO      IN MF_ENVIOS.USUARIO_INGRESO%TYPE,
                           PD_FECHA_INGRESO        IN MF_ENVIOS.FECHA_INGRESO%TYPE,
                           PV_USUARIO_MODIFICACION IN MF_ENVIOS.USUARIO_MODIFICACION%TYPE,
                           PD_FECHA_MODIFICACION   IN MF_ENVIOS.FECHA_MODIFICACION%TYPE,
                           --PN_TIPO_INGRESO            IN  MF_ENVIOS.TIPO_INGRESO%TYPE,                         
                           PV_MSG_ERROR     IN OUT VARCHAR2,
                           Pv_Ciclo         IN MF_ENVIOS.ID_CICLO%TYPE,
                           PV_TIPO_CLIENTE  IN VARCHAR2 DEFAULT NULL,
                           PV_CATEG_CLIENTE IN VARCHAR2 DEFAULT NULL,
                           PV_SCORE                  IN VARCHAR2 DEFAULT 'N',                           
                           PV_TIPO_ENVIO    IN MF_ENVIOS.TIPO_ENVIO%TYPE DEFAULT NULL,
                           PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL); --JRC 

  --
  PROCEDURE MFP_ELIMINAR(PN_IDENVIO   IN MF_ENVIOS.IDENVIO%TYPE,
                         PV_MSG_ERROR IN OUT VARCHAR2);
  --
  PROCEDURE MFP_ELIMINAR_LOGICO(PN_IDENVIO   IN MF_ENVIOS.IDENVIO%TYPE,
                                PV_MSG_ERROR IN OUT VARCHAR2);
  --
  PROCEDURE MFP_DETALLE_INSERTA(PN_ID_ENVIO IN MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                PV_TRAMA    IN VARCHAR2,
                                PV_TIPO     IN VARCHAR2,
                                Pv_Error    OUT VARCHAR2);

  PROCEDURE MFP_DETALLE_ACTUALIZA(PN_ID_ENVIO IN MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                  PV_TRAMA    IN VARCHAR2,
                                  PV_TIPO     IN VARCHAR2,
                                  Pv_Error    OUT VARCHAR2);

  PROCEDURE MFP_INSERTAR_BITACORA(PN_CUENTA_BSCS     MF_BITACORA_ENVIO_MSN.NUM_CUENTA_BSCS%TYPE,
                                  PV_NUM_CELULAR     MF_BITACORA_ENVIO_MSN.NUM_CELULAR%TYPE,
                                  PV_NOMBRE_CLIENTE  MF_BITACORA_ENVIO_MSN.NOMBRE_CLIENTE%TYPE,
                                  PV_CEDULA          MF_BITACORA_ENVIO_MSN.CEDULA%TYPE,
                                  PN_MONTO_DEUDA     MF_BITACORA_ENVIO_MSN.MONTO_DEUDA%TYPE,
                                  PD_FECHA_ENVIO     MF_BITACORA_ENVIO_MSN.FECHA_ENVIO%TYPE,
--                                  PD_HORA_ENVIO      MF_BITACORA_ENVIO_MSN.HORA_ENVIO%TYPE,
                                  PD_FECHA_REGISTRO MF_BITACORA_ENVIO_MSN.FECHA_REGISTRO%TYPE,
  ---                                PD_HORA_RECEPCION  MF_BITACORA_ENVIO_MSN.HORA_RECEPCION%TYPE,
                                  PV_MENSAJE         MF_BITACORA_ENVIO_MSN.MENSAJE%TYPE,
                                  PN_ID_ENVIO        MF_BITACORA_ENVIO_MSN.ID_ENVIO%TYPE,
                                  Pn_Secuencia       MF_BITACORA_ENVIO_MSN.SECUENCIA%TYPE,
                                  PN_HILO In Number,                                  
                                  Pv_Error           OUT VARCHAR2);
Procedure MFP_ACTUALIZA_FENVIO_BITACORA (PD_FECHA_ENVIO     MF_BITACORA_ENVIO_MSN.FECHA_ENVIO%TYPE,
                                  PN_ID_ENVIO        MF_BITACORA_ENVIO_MSN.ID_ENVIO%TYPE,
                                  Pn_Secuencia       MF_BITACORA_ENVIO_MSN.SECUENCIA%TYPE,
                                  PV_NUM_CELULAR MF_BITACORA_ENVIO_MSN.NUM_CELULAR%Type,
                                  Pv_Error           OUT VARCHAR2);                                  
END MFK_OBJ_ENVIOS;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_ENVIOS
/* 
 ||   Name       : MFK_OBJ_ENVIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */

 /*====================================================================================
 LIDER SIS :       ANTONIO MAYORGA
 Proyecto  :       [11334] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
 Modificado Por:	 IRO Andres Balladares.
 Fecha     :	     03/04/2017
 LIDER IRO :	     Juan Romero Aguilar 
 PROPOSITO :	     Considerar financiamiento para el modulo SMS, IVR y Gestores
#=====================================================================================*/
 IS
  PROCEDURE MFP_HELP(context_in IN VARCHAR2 := NULL) IS
  BEGIN
    NULL;
  END MFP_HELP;

  --modificado: SUD Jhon Reyes C
  --fecha: 05/11/2007
  --motivo: Permitir guardar el ciclo de facturacion en la tabla MF_ENVIOS
  --------------------------------------------------------------------------
  --------------------------------------------------------------------------

  --modificado: Cima  Mariuxi Buenaventura
  --fecha: 06/16/2011
  --motivo: [6092]Mejoras al M�dulo de Env�o de SMS Masivos
  
 
  PROCEDURE MFP_INSERTAR(PV_DESCRIPCION          IN MF_ENVIOS.DESCRIPCION%TYPE,
                         Pv_Estado               IN MF_ENVIOS.ESTADO%TYPE,
                         PN_CANTIDAD_MENSAJES    IN MF_ENVIOS.CANTIDAD_MENSAJES%TYPE,
                         PN_HORAS_SEPARACION_MSN IN MF_ENVIOS.HORAS_SEPARACION_MSN%TYPE,
                         PV_FORMA_ENVIO          IN MF_ENVIOS.FORMA_ENVIO%TYPE,
                         PV_MENSAJE              IN MF_ENVIOS.MENSAJE%TYPE,
                         PN_MONTO_MINIMO         IN MF_ENVIOS.MONTO_MINIMO%TYPE,
                         PN_MONTO_MAXIMO         IN MF_ENVIOS.MONTO_MAXIMO%TYPE,
                         PV_REGION               IN MF_ENVIOS.REGION%TYPE,
                         PV_REQUIERE_EDAD_MORA   IN MF_ENVIOS.REQUIERE_EDAD_MORA%TYPE,
                         PN_TIPO_CLIENTE         IN MF_ENVIOS.TIPO_CLIENTE%TYPE,
                         PV_USUARIO_INGRESO      IN MF_ENVIOS.USUARIO_INGRESO%TYPE,
                         PD_FECHA_INGRESO        IN MF_ENVIOS.FECHA_INGRESO%TYPE,
                         PV_USUARIO_MODIFICACION IN MF_ENVIOS.USUARIO_MODIFICACION%TYPE,
                         PD_FECHA_MODIFICACION   IN MF_ENVIOS.FECHA_MODIFICACION%TYPE,
                         PN_IDENVIO              OUT MF_ENVIOS.IDENVIO%TYPE,
                         PV_MSG_ERROR            IN OUT VARCHAR2,
                         Pv_Ciclo                IN MF_ENVIOS.ID_CICLO%TYPE, --JRC
                         PV_TIPO_CLIENTE         IN VARCHAR2 DEFAULT NULL, --MBU cima
                         PV_CATEG_CLIENTE        IN VARCHAR2 DEFAULT NULL,
                         PV_SCORE                  IN VARCHAR2 DEFAULT 'N',
                         PV_TIPO_ENVIO           IN MF_ENVIOS.TIPO_ENVIO%TYPE DEFAULT NULL,
                         PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL) Is

    /* 
    ||   Name       : MFP_INSERTAR
    ||   Created on : 11-ABR-05
    ||   Comments   : Standalone Delete Procedure 
    ||   automatically generated using the PL/Vision building blocks 
    ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
    ||   Adjusted by System Department CONECEL S.A. 2003
    */
    LV_APLICACION    VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_INSERTAR';
    LN_IDENVIO       MF_ENVIOS.IDENVIO%TYPE;
    LV_TIPO_CLIENTE  VARCHAR2(1000);
    LV_CATEG_CLIENTE VARCHAR2(1000);
    Lv_Error         VARCHAR2(1000);
    LE_ERROR EXCEPTION;
  BEGIN
  
    PV_MSG_ERROR     := NULL;
    LV_TIPO_CLIENTE  := PV_TIPO_CLIENTE;
    LV_CATEG_CLIENTE := PV_CATEG_CLIENTE;
  
    SELECT mfs_envios_sec.NEXTVAL
      INTO LN_IDENVIO
      FROM DUAL;
  
    INSERT INTO MF_ENVIOS
      (IDENVIO,
       DESCRIPCION,
       ESTADO,
       CANTIDAD_MENSAJES,
       HORAS_SEPARACION_MSN,
       FORMA_ENVIO,
       MENSAJE,
       MONTO_MINIMO,
       MONTO_MAXIMO,
       REGION,
       REQUIERE_EDAD_MORA,
       TIPO_CLIENTE,
       USUARIO_INGRESO,
       FECHA_INGRESO,
       USUARIO_MODIFICACION,
       FECHA_MODIFICACION,
       TIPO_ENVIO,
       SCORE,
       ID_CICLO,
       FINANCIAMIENTO) --JRC
    VALUES
      (LN_IDENVIO,
       PV_DESCRIPCION,
       Pv_Estado,
       PN_CANTIDAD_MENSAJES,
       PN_HORAS_SEPARACION_MSN,
       PV_FORMA_ENVIO,
       PV_MENSAJE,
       PN_MONTO_MINIMO,
       PN_MONTO_MAXIMO,
       PV_REGION,
       PV_REQUIERE_EDAD_MORA,
       PN_TIPO_CLIENTE,
       PV_USUARIO_INGRESO,
       PD_FECHA_INGRESO,
       PV_USUARIO_MODIFICACION,
       PD_FECHA_MODIFICACION,
       PV_TIPO_ENVIO,
       PV_SCORE,
       Pv_Ciclo,
       PV_FINANCIAMIENTO);
    PN_IDENVIO := LN_IDENVIO;
  
  EXCEPTION
    -- Exception 
    WHEN LE_ERROR THEN
      PV_MSG_ERROR := Lv_Error;
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
  END MFP_INSERTAR;

  --modificado: SUD Jhon Reyes C
  --fecha: 05/11/2207
  --motivo: Permitir actualizar el ciclo de facturacion en la tabla MF_ENVIOS

  /*
      modificado: Cima  Mariuxi Buenaventura
      fecha: 05/11/2011
      motivo: [6092]Mejoras al M�dulo de Env�o de SMS Masivos
  */
  PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO              IN MF_ENVIOS.IDENVIO%TYPE,
                           PV_DESCRIPCION          IN MF_ENVIOS.DESCRIPCION%TYPE,
                           Pv_Estado               IN MF_ENVIOS.ESTADO%TYPE,
                           PN_CANTIDAD_MENSAJES    IN MF_ENVIOS.CANTIDAD_MENSAJES%TYPE,
                           PN_HORAS_SEPARACION_MSN IN MF_ENVIOS.HORAS_SEPARACION_MSN%TYPE,
                           PV_FORMA_ENVIO          IN MF_ENVIOS.FORMA_ENVIO%TYPE,
                           PV_MENSAJE              IN MF_ENVIOS.MENSAJE%TYPE,
                           PN_MONTO_MINIMO         IN MF_ENVIOS.MONTO_MINIMO%TYPE,
                           PN_MONTO_MAXIMO         IN MF_ENVIOS.MONTO_MAXIMO%TYPE,
                           PV_REGION               IN MF_ENVIOS.REGION%TYPE,
                           PV_REQUIERE_EDAD_MORA   IN MF_ENVIOS.REQUIERE_EDAD_MORA%TYPE,
                           PN_TIPO_CLIENTE         IN MF_ENVIOS.TIPO_CLIENTE%TYPE,
                           PV_USUARIO_INGRESO      IN MF_ENVIOS.USUARIO_INGRESO%TYPE,
                           PD_FECHA_INGRESO        IN MF_ENVIOS.FECHA_INGRESO%TYPE,
                           PV_USUARIO_MODIFICACION IN MF_ENVIOS.USUARIO_MODIFICACION%TYPE,
                           PD_FECHA_MODIFICACION   IN MF_ENVIOS.FECHA_MODIFICACION%TYPE,
                           PV_MSG_ERROR            IN OUT VARCHAR2,
                           Pv_Ciclo                IN MF_ENVIOS.ID_CICLO%TYPE,
                           PV_TIPO_CLIENTE         IN VARCHAR2 DEFAULT NULL,
                           PV_CATEG_CLIENTE        IN VARCHAR2 DEFAULT NULL,
                           PV_SCORE                  IN VARCHAR2 DEFAULT 'N',                           
                           PV_TIPO_ENVIO           IN MF_ENVIOS.TIPO_ENVIO%TYPE DEFAULT NULL,
                           PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL) --JRC 
   AS
    /* 
    ||   Name       : MFP_ACTUALIZAR
    ||   Created on : 11-ABR-05
    ||   Comments   : Standalone Delete Procedure 
    ||   automatically generated using the PL/Vision building blocks 
    ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
    ||   Adjusted by System Department CONECEL S.A. 2003
    */
    CURSOR C_table_cur(cn_idEnvio NUMBER) IS
      SELECT *
        FROM MF_ENVIOS E
       WHERE E.IDENVIO = cn_idEnvio;
  
    Lc_CurrentRow MF_ENVIOS%ROWTYPE;
    Le_NoDataUpdated EXCEPTION;
    LE_ERROR EXCEPTION;
  
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_ACTUALIZAR';
    Lv_Error      VARCHAR2(1000);
  BEGIN
  
    PV_MSG_ERROR := NULL;
    OPEN C_table_cur(PN_IDENVIO);
    FETCH C_table_cur
      INTO Lc_CurrentRow;
    IF C_table_cur%NOTFOUND THEN
      RAISE Le_NoDataUpdated;
    END IF;
  
    UPDATE MF_ENVIOS
       SET -- Revisar los campos que desea actualizar
                   DESCRIPCION = NVL(PV_DESCRIPCION, DESCRIPCION),
           ESTADO               = NVL(Pv_Estado, ESTADO),
           CANTIDAD_MENSAJES    = NVL(PN_CANTIDAD_MENSAJES, CANTIDAD_MENSAJES),
           HORAS_SEPARACION_MSN = NVL(PN_HORAS_SEPARACION_MSN, HORAS_SEPARACION_MSN),
           FORMA_ENVIO          = NVL(PV_FORMA_ENVIO, FORMA_ENVIO),
           MENSAJE              = NVL(PV_MENSAJE, MENSAJE),
           MONTO_MINIMO         = NVL(PN_MONTO_MINIMO, MONTO_MINIMO),
           MONTO_MAXIMO         = NVL(PN_MONTO_MAXIMO, MONTO_MAXIMO),
           REGION               = NVL(PV_REGION, REGION),
           REQUIERE_EDAD_MORA   = NVL(PV_REQUIERE_EDAD_MORA, REQUIERE_EDAD_MORA),
           TIPO_CLIENTE         = NVL(PN_TIPO_CLIENTE, TIPO_CLIENTE),
           USUARIO_INGRESO      = NVL(PV_USUARIO_INGRESO, USUARIO_INGRESO),
           FECHA_INGRESO        = NVL(PD_FECHA_INGRESO, FECHA_INGRESO),
           USUARIO_MODIFICACION = NVL(PV_USUARIO_MODIFICACION, USUARIO_MODIFICACION),
           FECHA_MODIFICACION   = NVL(PD_FECHA_MODIFICACION, FECHA_MODIFICACION),
           TIPO_ENVIO           = NVL(PV_TIPO_ENVIO, TIPO_ENVIO) --MAB
           ,ID_CICLO             = NVL(Pv_Ciclo, ID_CICLO) 
           , SCORE                = NVL(PV_SCORE, SCORE)
           ,FINANCIAMIENTO     = NVL(PV_FINANCIAMIENTO,FINANCIAMIENTO)
     WHERE IDENVIO = PN_IDENVIO;
  
    CLOSE C_table_cur;
  
  EXCEPTION
    -- Exception 
    WHEN LE_ERROR THEN
      PV_MSG_ERROR := Lv_Error;
    WHEN Le_NoDataUpdated THEN
      PV_MSG_ERROR := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || LV_APLICACION;
      CLOSE C_table_cur;
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
      CLOSE C_table_cur;
  END MFP_ACTUALIZAR;

  --modificado: Cima  Mariuxi Buenaventura
  --fecha: 05/11/2011
  --motivo: [6092]Mejoras al M�dulo de Env�o de SMS Masivos
  PROCEDURE MFP_ELIMINAR(PN_IDENVIO   IN MF_ENVIOS.IDENVIO%TYPE,
                         PV_MSG_ERROR IN OUT VARCHAR2) AS
    /* 
    ||   Name       : MFP_ELIMINAR
    ||   Created on : 11-ABR-05
    ||   Comments   : Standalone Delete Procedure 
    ||   automatically generated using the PL/Vision building blocks 
    ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
    ||   Adjusted by System Department CONECEL S.A. 2003
    */
    CURSOR C_table_cur(cn_idEnvio NUMBER) IS
      SELECT *
        FROM MF_ENVIOS E
       WHERE E.IDENVIO = cn_idEnvio;
  
    Lc_CurrentRow MF_ENVIOS%ROWTYPE;
    Le_NoDataDeleted EXCEPTION;
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_ELIMINAR';
  BEGIN
  
    PV_MSG_ERROR := NULL;
    OPEN C_table_cur(PN_IDENVIO);
    FETCH C_table_cur
      INTO Lc_CurrentRow;
    IF C_table_cur%NOTFOUND THEN
      RAISE Le_NoDataDeleted;
    END IF;
  
    DELETE FROM MF_ENVIOS E
     WHERE e.idenvio = pn_idEnvio;
  
    DELETE FROM MF_DETALLES_ENVIOS D
     WHERE D.IDENVIO = pn_idEnvio;
  
    CLOSE C_table_cur;
  
  EXCEPTION
    -- Exception 
    WHEN Le_NoDataDeleted THEN
      PV_MSG_ERROR := 'El registro que desea borrar no existe. ' || LV_APLICACION;
      CLOSE C_table_cur;
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
      CLOSE C_table_cur;
  END MFP_ELIMINAR;
  ---------------------
  /* 
  ||   Name       : MFP_ELIMINAR_LOGICO
  ||   Created on : 12/07/2011
  ||   Comments   : Modificaciones [6092]Modificaciones envio SMS.
       Modificado : Cima MBU.
  */
  PROCEDURE MFP_ELIMINAR_LOGICO(PN_IDENVIO   IN MF_ENVIOS.IDENVIO%TYPE,
                                PV_MSG_ERROR IN OUT VARCHAR2) AS
  
    CURSOR C_table_cur(cn_idEnvio NUMBER) IS
      SELECT *
        FROM MF_ENVIOS E
       WHERE E.IDENVIO = cn_idEnvio;
  
    Lc_CurrentRow MF_ENVIOS%ROWTYPE;
    Le_NoDataDeleted EXCEPTION;
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_ELIMINAR';
  BEGIN
  
    PV_MSG_ERROR := NULL;
    OPEN C_table_cur(PN_IDENVIO);
    FETCH C_table_cur
      INTO Lc_CurrentRow;
    IF C_table_cur%NOTFOUND THEN
      RAISE Le_NoDataDeleted;
    END IF;
  
    UPDATE MF_ENVIOS E
       SET E.ESTADO = 'I'
     WHERE E.IDENVIO = pn_idEnvio;
  
    CLOSE C_table_cur;
  
  EXCEPTION
    -- Exception 
    WHEN Le_NoDataDeleted THEN
      PV_MSG_ERROR := 'El registro que desea borrar no existe. ' || LV_APLICACION;
      CLOSE C_table_cur;
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || LV_APLICACION;
      CLOSE C_table_cur;
  END MFP_ELIMINAR_LOGICO;
  --

  --------------------
  --Creado por: Cima  Mariuxi Buenaventura
  --fecha     : 05/07/2011
  --motivo    : [6092]Mejoras al M�dulo de Env�o de SMS Masivos

  PROCEDURE MFP_DETALLE_INSERTA(PN_ID_ENVIO IN MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                PV_TRAMA    IN VARCHAR2,
                                PV_TIPO     IN VARCHAR2,
                                Pv_Error    OUT VARCHAR2) AS
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_DETALLE_INSERTA';
    Lv_Error      VARCHAR2(1000);
    LE_ERROR EXCEPTION;
  
    LV_DATA         VARCHAR2(100) := PV_TRAMA;
    LV_DATA_TMP     VARCHAR2(100) := LV_DATA;
    LV_DATA_TEMP    VARCHAR2(100) := LV_DATA;
    LN_POSICION_ACT NUMBER(10) := 1;
    LV_ID           VARCHAR2(100);
    LN_AVANZA       NUMBER(10) := LENGTH(LV_DATA_TMP);
  BEGIN
    DELETE FROM MF_DETALLES_ENVIOS 
     WHERE IDENVIO = PN_ID_ENVIO
       AND TIPOENVIO = PV_TIPO;
    WHILE (LN_AVANZA > 1) LOOP
      LN_POSICION_ACT := INSTR(LV_DATA_TMP, '|', 1);
      LV_DATA_TEMP    := SUBSTR(LV_DATA_TMP, LN_POSICION_ACT + 1, LENGTH(LV_DATA_TMP));
      LV_ID           := SUBSTR(LV_DATA_TMP, 1, LN_POSICION_ACT - 1);
    
      IF LV_ID IS NOT NULL THEN
        BEGIN
          --INSERT EN MF_DETALLES_ENVIO
          INSERT INTO MF_DETALLES_ENVIOS
            (IDENVIO,
             IDCLIENTE,
             TIPOENVIO)
          VALUES
            (PN_ID_ENVIO,
             LV_ID,
             PV_TIPO);
        EXCEPTION
          WHEN OTHERS THEN
            Lv_Error := 'ERROR EN LA APLICACION ' || LV_APLICACION || '|EN LA INSERCION DEL DETALLE INTERNO ' || SUBSTR(SQLERRM, 1, 250);
            RAISE LE_ERROR;
        END;
      END IF;
      LV_DATA_TMP := LV_DATA_TEMP;
      LN_AVANZA   := LENGTH(LV_DATA_TMP);
    
    END LOOP;
    IF LENGTH(LV_DATA_TMP) > 0 THEN
      INSERT INTO MF_DETALLES_ENVIOS
        (IDENVIO,
         IDCLIENTE,
         TIPOENVIO)
      VALUES
        (PN_ID_ENVIO,
         LV_DATA_TMP,
         PV_TIPO);
    END IF;
  EXCEPTION
    WHEN LE_ERROR THEN
      Pv_Error := Lv_Error;
    WHEN OTHERS THEN
      Pv_Error := 'ERROR EN LA APLICACION ' || LV_APLICACION || '|' || SUBSTR(SQLERRM, 1, 250);
  END;
  --------------------
  --Creado por: Cima  Mariuxi Buenaventura
  --fecha     : 01/07/2011
  --motivo    : [6092]Mejoras al M�dulo de Env�o de SMS Masivos
  PROCEDURE MFP_DETALLE_ACTUALIZA(PN_ID_ENVIO IN MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                  PV_TRAMA    IN VARCHAR2,
                                  PV_TIPO     IN VARCHAR2,
                                  Pv_Error    OUT VARCHAR2) AS
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_DETALLE_ACTUALIZA';
    Lv_Error      VARCHAR2(1000);
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BEGIN
      DELETE FROM MF_DETALLES_ENVIOS
       WHERE IDENVIO = PN_ID_ENVIO
         AND TIPOENVIO = PV_TIPO;
    EXCEPTION
      WHEN OTHERS THEN
        Lv_Error := 'ERROR EN LA APLICACION ' || LV_APLICACION || '|EN LA ELIMINACION DEL DETALLE INTERNO ' || SUBSTR(SQLERRM, 1, 250);
        RAISE LE_ERROR;
    END;
  
    MFP_DETALLE_INSERTA(PN_ID_ENVIO, PV_TRAMA, PV_TIPO, Lv_Error);
  
  EXCEPTION
    WHEN LE_ERROR THEN
      Pv_Error := Lv_Error;
    WHEN OTHERS THEN
      Pv_Error := 'ERROR EN LA APLICACION ' || LV_APLICACION || '|' || SUBSTR(SQLERRM, 1, 250);
  END;
  --------------------
  --Creado por: Cima  Mariuxi Buenaventura
  --fecha     : 05/07/2011
  --motivo    : [6092]Mejoras al M�dulo de Env�o de SMS Masivos 
  PROCEDURE MFP_INSERTAR_BITACORA(PN_CUENTA_BSCS     MF_BITACORA_ENVIO_MSN.NUM_CUENTA_BSCS%TYPE,
                                  PV_NUM_CELULAR     MF_BITACORA_ENVIO_MSN.NUM_CELULAR%TYPE,
                                  PV_NOMBRE_CLIENTE  MF_BITACORA_ENVIO_MSN.NOMBRE_CLIENTE%TYPE,
                                  PV_CEDULA          MF_BITACORA_ENVIO_MSN.CEDULA%TYPE,
                                  PN_MONTO_DEUDA     MF_BITACORA_ENVIO_MSN.MONTO_DEUDA%TYPE,
                                  PD_FECHA_ENVIO     MF_BITACORA_ENVIO_MSN.FECHA_ENVIO%TYPE,
---                                  PD_HORA_ENVIO      MF_BITACORA_ENVIO_MSN.HORA_ENVIO%TYPE,
                                  PD_FECHA_REGISTRO MF_BITACORA_ENVIO_MSN.FECHA_REGISTRO%TYPE,
   ---                               PD_HORA_RECEPCION  MF_BITACORA_ENVIO_MSN.HORA_RECEPCION%TYPE,
                                  PV_MENSAJE         MF_BITACORA_ENVIO_MSN.MENSAJE%TYPE,
                                  PN_ID_ENVIO        MF_BITACORA_ENVIO_MSN.ID_ENVIO%TYPE,
                                  Pn_Secuencia       MF_BITACORA_ENVIO_MSN.SECUENCIA%TYPE,
                                  PN_HILO In Number,
                                  Pv_Error           OUT VARCHAR2) AS
  --************************************************************************************
  -- Proyecto:              [8646] - Ecarfo 4456.-  Envi� de SMS Masivos de Cobranzas
  -- Lider Conecel:       SIS Julia Rodas
  -- Lider Cls:              CLS Miguel Garcia
  -- Modificado Por:    CLS Freddy Beltr�n
  -- Fecha:                  08/07/2013
  -- Funcionalidad:     Procedure se cambio a modo dinamico.
  --************************************************************************************* 

    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_INSERTAR_BITACORA';
  BEGIN
      Begin
        execute immediate 'INSERT INTO MF_BITACORA_ENVIO_MSN '|| 
                                                   ' (NUM_CUENTA_BSCS, '||
                                                   ' NUM_CELULAR, '||
                                                   ' NOMBRE_CLIENTE, '||
                                                   ' CEDULA, '||
                                                   ' MONTO_DEUDA, '||
                                                   ' FECHA_ENVIO, '||
                                                   ' FECHA_REGISTRO, '||
                                                   ' ID_ENVIO, '||
                                                   ' SECUENCIA, '||
                                                   ' MENSAJE, '||
                                                   ' HILO) '||
                                                   ' VALUES '||
                                                       '( :1, '||
                                                       ' :2, '||
                                                       ' :3, '||
                                                       ' :4, '||
                                                       ' :5, '||
                                                       ' :6, '||
                                                       ' :7, '||
                                                       ' :8, '||
                                                       ' :9, '||
                                                       ' :10, '||
                                                       ' :11) '
        using PN_CUENTA_BSCS, PV_NUM_CELULAR, PV_NOMBRE_CLIENTE,
              PV_CEDULA, PN_MONTO_DEUDA, PD_FECHA_ENVIO,
              PD_FECHA_REGISTRO, PN_ID_ENVIO, PN_SECUENCIA,
              PV_MENSAJE , PN_HILO;
       Exception When Others Then Null;
      End;
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'ERROR EN EL APLICATIVO:' || LV_APLICACION || ' ERROR:' || SUBSTR(SQLERRM, 1, 250);
  END;
  
  Procedure MFP_ACTUALIZA_FENVIO_BITACORA (PD_FECHA_ENVIO     MF_BITACORA_ENVIO_MSN.FECHA_ENVIO%TYPE,
                                  PN_ID_ENVIO        MF_BITACORA_ENVIO_MSN.ID_ENVIO%TYPE,
                                  Pn_Secuencia       MF_BITACORA_ENVIO_MSN.SECUENCIA%TYPE,
                                  PV_NUM_CELULAR MF_BITACORA_ENVIO_MSN.NUM_CELULAR%Type,
                                  Pv_Error           OUT VARCHAR2) AS
  Begin 
    /*Update MF_BITACORA_ENVIO_MSN
    Set FECHA_ENVIO=PD_FECHA_ENVIO
    Where ID_ENVIO=PN_ID_ENVIO
           And SECUENCIA=Pn_Secuencia
           And NUM_CELULAR=PV_NUM_CELULAR;*/
           
    Begin
      execute immediate 'Update MF_BITACORA_ENVIO_MSN'||
                        ' Set FECHA_ENVIO= :1'||
                        ' Where ID_ENVIO = :2'||
                        ' and  SECUENCIA = :3'||
                        ' And NUM_CELULAR= :4'
      using PD_FECHA_ENVIO , PN_ID_ENVIO , Pn_Secuencia,PV_NUM_CELULAR;
    Exception When Others Then Null;
    End;
           
  Exception
     When Others Then
          Pv_Error:=substr(Sqlerrm,1,100);       
  End;
END MFK_OBJ_ENVIOS;
/
