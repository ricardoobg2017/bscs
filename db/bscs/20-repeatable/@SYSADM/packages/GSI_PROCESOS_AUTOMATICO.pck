create or replace package GSI_PROCESOS_AUTOMATICO is

   -- Desarrollado por : CIMA VCE   
   -- Lider            : CIMA Antonio Berechez 
   -- CRM              : SIS Wilson Pozo  
   -- Created          : 01/02/2016
   -- Purpose          : 10713 - Generacion de tablas tarifas en AXIS

   /*==================================================*\
   ==        ELIMINO Y CREO TABLAS TEMPRALES           ==
   \*==================================================*/

   PROCEDURE PR_INICIO_TARIFAS; 
   PROCEDURE PR_DISTRIBUYE_HILOS;

   /*==================================================*\
   ==   CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES   ==
   \*==================================================*/
  
   -- PARA EXTRAER LAS TARIFAS DE LOS PLANES NO BULK, NORMALES. 55 Servicio de voz
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_55(PN_HILO IN NUMBER);

   -- PARA EXTRAER LAS TARIFAS DE LOS PLANES NO BULK, NORMALES. 55 Servicio de voz   
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_55_BC(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS VPN: 539 - ONNET CLARO (PORTA).
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_539(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS 540- OFFNET (VPN), para obtener los destinos ALegro, Movistar, Locales
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_540(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS 540- OFFNET (VPN), para obtener los destinos ALegro, Movistar, Locales
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_540_BC(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS VPN INPOOL. 543 - VPN FORCED ON-NET
   PROCEDURE PR_EXTRAE_COSTOS_SNCODE_543(PN_HILO IN NUMBER);

   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 776. 988 - VPN 776 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_988_776(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 777. 998 - VPN 777 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_998_777(PN_HILO IN NUMBER);
  
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 778. 999 - VPN 778 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_999_778(PN_HILO IN NUMBER);
  
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 779. 1000 -VPN 779 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_1000_779(PN_HILO IN NUMBER);
   
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 780. 1307 -VPN 780 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_1307_780(PN_HILO IN NUMBER);  
   
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 781. 1308 -VPN 781 ----988,998,999,1000,1307,1308
   PROCEDURE PR_EXTRAE_COSTOS_1308_781(PN_HILO IN NUMBER);  

    /*=============================*\
    ==   CUADRES Y QC              ==
    \*=============================*/

   -- CUANDO TERMINENE LA EXTRACCIÓN, BORRAR LAS ZONAS DUMMIES
   PROCEDURE PR_DEPURA_TABLA_COSTOS;
   
   -- Debe salir la misma cantidad de registros entre los dos COUNT(*)
   PROCEDURE PR_QC_CUADRE_DE_REGISTROS_1; 

   --Hago QC de planes que tengan diferenetes tarifas, o RIs. No debe salir nada.
   PROCEDURE PR_QC_CUADRE_DE_REGISTROS_2;
  
   /*=======================================*\
   ==    GENERAR Y DEPURAR TABLAS FINALES   ==
   \*=======================================*/

   --Query para sacar los datos ya agrupados por destino, esto es para no hacer tabla dinamica
   PROCEDURE PR_GENERA_TABLA_FINAL(PV_FECHA_YYYYMMDD VARCHAR2);
   
   ----INSERTAR PLANE ABIERTOS BAM -- PARA INSERTAR LOS FALTANTES
   PROCEDURE PR_INSERTAR_EN_TABLA_FINAL(PV_FECHA_YYYYMMDD VARCHAR2);
   
   --Depura tablas antiguas
   PROCEDURE PR_DEPURA_TABLAS_TARIFAS(PN_DIAS_MANTIENE NUMBER DEFAULT NULL);


end GSI_PROCESOS_AUTOMATICO;
/
create or replace package body GSI_PROCESOS_AUTOMATICO is
-----------------------------
--- Correr en BSCSPROD 
-----------------------------
/*
-- Create table
create table GSI_PLANES_F16
(
  ID_PLAN VARCHAR2(20)
)
*/

PROCEDURE PR_INICIO_TARIFAS/*(pn_tabla in varchar2)*/ AS
   CURSOR C_INTERSECTA IS
   Select id_plan 
     From gsi_sle_planes_obt 
    Where sncode=539
Intersect
   Select id_plan 
     From gsi_sle_planes_obt 
    Where sncode=55 ;
   
   LC_INTERSECTA C_INTERSECTA %ROWTYPE;
   LE_INTERSECTA EXCEPTION;
  
  --LV_SALIDA         varchar2(1000);
  --lv_sentencia varchar2(32000);

BEGIN

   /*=================================================*\
   == Preparando a la tabla: gsi_planes_f16           ==
   \*=================================================*/

   --Ingreso en esta tabla los planes (BP-) a los que deseo obtener los costos.
   EXECUTE IMMEDIATE 'TRUNCATE TABLE gsi_planes_f16';
       
   insert into gsi_planes_f16(id_Plan) 
   select id_plan 
     from ge_planes@axis
    where id_Plan like 'BP-%' 
      and id_plan not in ('BP-0','BP-1','BP-10');
       
   update gsi_planes_f16 
      set id_plan = trim(id_plan);

   commit;
   
   /*=================================================*\
   == Preparando a la tabla: gsi_sle_planes_obt_pre   ==
   \*=================================================*/

   --Inicio el proceso, creo tabla de trabajo temporal
   --EXECUTE IMMEDIATE  'TRUNCATE TABLE GSI_SLE_PLANES_OBT_PRE';
   EXECUTE IMMEDIATE 'Drop Table gsi_sle_planes_obt_pre CASCADE CONSTRAINTS PURGE';

   EXECUTE IMMEDIATE 'Create Table gsi_sle_planes_obt_pre TABLESPACE TBS_TARIFAS_DAT As 
   Select Distinct a.id_plan, a.descripcion, b.id_detalle_plan, c.cod_bscs, b.id_clase, b.id_subproducto, nvl(a.tipo,''OLD'') TIPO
     From ge_planes@axis a, 
          ge_detalles_planes@axis b, 
          bs_planes@axis c
    Where a.id_plan=b.id_plan
      And b.id_detalle_plan= c.id_detalle_plan 
      And c.tipo=''O''
      And nvl(b.id_clase,''GSM'')!=''TDM'' 
      AND A.ID_PLAN IN (select id_plan from gsi_planes_f16)' ;
      
   --Identifico los planes que son Bulk y los No Bulk
   --A los planes no Bulk, los marco como TAR.
   Update gsi_sle_planes_obt_pre 
      Set tipo='TAR' 
    Where tipo!='BUL';
   
   commit;
   
   --Creo indice para mejorar rendimiento.
   EXECUTE IMMEDIATE 'Create Index ijasld On gsi_sle_planes_obt_pre(cod_bscs) TABLESPACE TBS_TARIFAS_IDX';

   /*=================================================*\
   == Preparando a la tabla: gsi_sle_planes_obt       ==
   \*=================================================*/

   --Creo tabla de tarifas temporal
   --EXECUTE IMMEDIATE 'INSERT INTO gsi_sle_planes_obt (id_plan, descripcion, tmcode, ricode, sncode)

   EXECUTE IMMEDIATE 'Drop Table gsi_sle_planes_obt CASCADE CONSTRAINTS PURGE';

   EXECUTE IMMEDIATE 'Create Table gsi_sle_planes_obt Tablespace TBS_TARIFAS_DAT As
   Select r.ID_PLAN, R.DESCRIPCION, j.TMCODE, k.RICODE, j.SNCODE
     From Mpulktmm j, Mpuritab k, Rateplan l, gsi_sle_planes_obt_pre r
    Where j.Tmcode In (Select cod_bscs From gsi_sle_planes_obt_pre)
      And j.Sncode In (55,540,543,539,988,998,999,1000,1308,1307) --91 para el servicio TDMA
      And r.cod_bscs=j.tmcode
      And j.Upcode In (1,2,5,4,7,8,16)
      And j.Vscode = (Select Max(h.Vscode)
                        From Mpulktmm h
                       Where h.Tmcode In (Select cod_bscs From gsi_sle_planes_obt_pre)
                         And h.Sncode In (55,540,543,539,988,998,999,1000,1308,1307) --91 para el servicio TDMA
                         And h.Upcode In (1,2,4,5,7,8,16)
                         And h.Tmcode = j.Tmcode
                     )
      And k.Ricode = j.Ricode
      And l.Tmcode = j.Tmcode';

   --Creo indice para mejorar rendimiento.
   EXECUTE IMMEDIATE 'Create Index idx_salkd  on gsi_sle_planes_obt(id_plan) Tablespace TBS_TARIFAS_IDX';
   EXECUTE IMMEDIATE 'Create Index idx_salkd2 on gsi_sle_planes_obt(sncode)  Tablespace TBS_TARIFAS_IDX';
   EXECUTE IMMEDIATE 'Create Index idx_salkd3 on gsi_sle_planes_obt(ricode)  Tablespace TBS_TARIFAS_IDX';

   --Se eliminan los servicios 55 de los planes BULK VPN
   -- para Bulk no es fiable el sncode 55
   Delete gsi_sle_planes_obt 
    Where id_Plan In (Select id_Plan From gsi_sle_planes_obt Where sncode=539)
      And sncode=55;

   commit;

   --Eliminar cualquier plan que no sea BULK VPN, tenga 539 - ONNET CLARO(VPN)
   delete 
     From gsi_sle_planes_obt 
    Where sncode=539 
      And id_plan In ('BP-1008','BP-878'); --BP de planes Asignados

   commit;

   --Verifica que los planes VPN con los normales no se crucen
   OPEN C_INTERSECTA;
   FETCH C_INTERSECTA INTO LC_INTERSECTA;
   IF C_INTERSECTA%FOUND THEN
     RAISE LE_INTERSECTA;
   END IF;
   CLOSE C_INTERSECTA;
   
   --Creo campo adicional para procesar por hilos
   EXECUTE IMMEDIATE 'Alter Table gsi_sle_planes_obt
                        Add hilo Number
                        Add procesado Varchar2(2)';
   
   PR_DISTRIBUYE_HILOS;
   
   -- Actualizo a Estado 'E' para los planes Controlados, pues esos no tiene
   -- tarifas en BSCS, tienen el RI GSM 0 0. Esos NO DEBEN procesarse.
   update gsi_sle_planes_obt
      set procesado = 'E'
    WHERE ID_PLAN  IN (SELECT ID_PLAN 
                         FROM gsi_sle_planes_obt_pre
                        WHERE TIPO != 'BUL'
                          AND  ID_SUBPRODUCTO = 'AUT'
                      );
                      
   commit;                      
                    
   update gsi_sle_planes_obt
      set procesado=null
    WHERE ID_PLAN IN ('BP-3837');
    
   commit;

   -----TOMAR EN CUENTA QUE ESTE PLAN NO SALDRA EN NINGUN REPORTE-----------
   ---ID_PLAN	ID_DETALLE_PLAN	OBSERVACION	ID_SUBPRODUCTO	FECHA_DESDE
   ---BP-3837	8428	Plan Llamada Corpo-3837	AUT	10/02/2012  Realizar un artificio para incluirlo
   ---Seria de hacer un update en una tabla ejecutar el script de creacion y vovlerlo a su estado original---
   ----POnerlo en el otro script
   --Trunco tabla FINAL donde van a estar los costos finales de los planes

   EXECUTE IMMEDIATE  'Truncate Table gsi_sle_tmp_ctos_plan_2';
   EXECUTE IMMEDIATE  'analyze table gsi_sle_tmp_ctos_plan_2 estimate statistics';
   EXECUTE IMMEDIATE  'analyze table gsi_sle_planes_obt estimate statistics';
     
   /*EXECUTE IMMEDIATE  'analyze table mpulkrim estimate statistics';
   EXECUTE IMMEDIATE  'analyze table rate_pack_element estimate statistics';
   EXECUTE IMMEDIATE  'analyze table rate_pack_parameter_value estimate statistics';
   EXECUTE IMMEDIATE  'analyze table rateplan estimate statistics';
   EXECUTE IMMEDIATE  'analyze table mpuritab estimate statistics';
   EXECUTE IMMEDIATE  'analyze table mpulktmm estimate statistics'; 
   EXECUTE IMMEDIATE  'analyze table mpuzntab estimate statistics';*/

EXCEPTION
  WHEN LE_INTERSECTA THEN
    dbms_output.put_line( 'Error: Verifica que los planes VPN con los normales no se crucen.');

  when others then
    dbms_output.put_line('Error : ' || sqlcode || ' - ' || substr(sqlerrm,1,200));
END;

----Distribuyo en 5 Hilos
PROCEDURE PR_DISTRIBUYE_HILOS IS
  
   CURSOR CLI IS
   SELECT J.ROWID, J.* 
     FROM GSI_SLE_PLANES_OBT J;
   
   LN_SESION NUMBER := 1;
   
BEGIN
   FOR I IN CLI LOOP
      UPDATE GSI_SLE_PLANES_OBT SET HILO = LN_SESION WHERE ROWID = I.ROWID;
      LN_SESION := LN_SESION + 1;
      IF LN_SESION > 5 THEN
         LN_SESION := 1;
         COMMIT;
      END IF;
   END LOOP;
   COMMIT;
END;

-- PARA EXTRAER LAS TARIFAS DE LOS PLANES NO BULK, NORMALES. 
--55	Servicio de voz
---CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_55(PN_HILO IN NUMBER) IS
   
   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a  
    Where sncode=55 
      And procesado Is Null 
      And ricode != 30 
      And hilo = cn_hilo;
      --And hilo=1;
  
Begin
   For i In reg(PN_HILO) Loop
    
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI,
             ri2.zncode, 
             znt.des DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe,
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas    
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (55)
                           ) 
         And tmm.sncode In (55);

      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;
         
      Commit;
   
   End Loop;
   
END PR_EXTRAE_COSTOS_SNCODE_55;

-- PARA EXTRAER LAS TARIFAS DE LOS PLANES NO BULK, NORMALES. 
--55	Servicio de voz
---CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_55_BC(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a  
    Where sncode=55 
      And procesado Is Null 
      And ricode != 30 
      And hilo = cn_hilo;
      
   cursor c_planes(lv_id_plan gsi_sle_planes_obt.id_plan%TYPE)IS
   Select /*+ first_rows */Distinct abc.id_plan, 
          tmm.ricode, 
          rt.des DES_RI,
          ri2.zncode, 
          znt.des DES_ZONE, 
          decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
          rpp.parameter_value_float, 
          round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
     From mpulkrim ri2, 
          rate_pack_element rpe,
          rate_pack_parameter_value rpp,
          rateplan rp, 
          mpuritab rt, 
          mpulktmm tmm, 
          mpuzntab znt, 
          gsi_sle_planes_obt abc
    Where ri2.ricode=abc.ricode
      And abc.id_plan = lv_id_plan
      And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
      And rpe.rate_pack_element_id=rpp.rate_pack_element_id
      And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas    
      And rpe.Chargeable_Quantity_Udmcode=10000 
      And rpp.parameter_seqnum=4
      And tmm.ricode=ri2.ricode 
      And tmm.tmcode=rp.tmcode 
      And rt.ricode=tmm.ricode 
      And znt.zncode=ri2.zncode
      And ri2.vscode=(Select Max(vscode) 
                        From mpulkrim h 
                       Where h.ricode=ri2.ricode 
                         And h.rate_type_id In (1,2)
                     )--(101,119,114,115,116))
      And tmm.vscode In (Select Max(vscode) 
                           From mpulktmm mt 
                          Where mt.tmcode=tmm.tmcode 
                            And mt.upcode In (1,2,4,7,8,16) 
                            And mt.sncode In (55)
                        ) 
      And tmm.sncode In (55);
      
   type t_id_plan  is table of gsi_sle_planes_obt.id_plan%type;
   type t_ricode   is table of mpulktmm.ricode%type;
   type t_des_ri   is table of mpuritab.des%type;
   type t_zncode   is table of mpulkrim.zncode%type;
   type t_des_zone is table of mpuzntab.des%type; 
   type t_tipo     is table of gsi_sle_tmp_ctos_plan_2.tipo%type;
   type t_float    is table of rate_pack_parameter_value.parameter_value_float%type;
   type t_costo    is table of number;

   l_id_plan   t_id_plan;
   l_ricode    t_ricode;
   l_des_ri    t_des_ri;   
   l_zncode    t_zncode;
   l_des_zone  t_des_zone;
   l_tipo      t_tipo;
   l_float     t_float;
   l_costo     t_costo;
  
Begin
   For i In reg(PN_HILO) Loop
   
      open c_planes(i.id_plan);
      loop
     fetch c_planes bulk collect into l_id_plan,l_ricode,l_des_ri,l_zncode,l_des_zone,l_tipo,l_float,l_costo limit 2000;

    forall j in 1 .. l_id_plan.count
    insert into gsi_sle_tmp_ctos_plan_2 
    values (l_id_plan(j),l_ricode(j),l_des_ri(j),l_zncode(j),l_des_zone(j),l_tipo(j),l_float(j),l_costo(j));
    --commit;
      exit when c_planes%NOTFOUND;
       end loop;
    commit;

     close c_planes;
     
    update gsi_sle_planes_obt
       set procesado='S'
     where Rowid=i.Rowid;
     commit;

   end loop;
exception 
   when others then
      rollback;
      dbms_output.put_line('Error -> ' || substr(sqlerrm,1,240));
END PR_EXTRAE_COSTOS_SNCODE_55_BC;


-- PARA EXTRAER COSTOS DE LOS VPN: 539 - ONNET CLARO (PORTA).
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_539(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=539 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;
  
Begin
   For i In reg(PN_HILO) Loop
     
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             znt.des DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (539)
                           ) 
         And tmm.sncode In (539);
    
      Update gsi_sle_planes_obt
		     Set procesado='S'
       Where Rowid=i.Rowid;    

      Commit;
   
  End Loop;

END PR_EXTRAE_COSTOS_SNCODE_539;

-- PARA EXTRAER COSTOS DE LOS 540- OFFNET (VPN), para obtener los destinos ALegro, Movistar, Locales
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_540(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=540 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
  
   For i In reg(PN_HILO) Loop
     
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             znt.des DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des!='CLARO'    
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (540)
                           ) 
         And tmm.sncode In (540);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_SNCODE_540;

-- PARA EXTRAER COSTOS DE LOS 540- OFFNET (VPN), para obtener los destinos ALegro, Movistar, Locales
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_540_BC(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=540 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;

   cursor c_planes(lv_id_plan gsi_sle_planes_obt.id_plan%TYPE)IS
   Select /*+ first_rows */Distinct abc.id_plan, 
          tmm.ricode, 
          rt.des DES_RI, 
          ri2.zncode, 
          znt.des DES_ZONE, 
          decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
          rpp.parameter_value_float, 
          round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
     From mpulkrim ri2, 
          rate_pack_element rpe, 
          rate_pack_parameter_value rpp,
          rateplan rp, 
          mpuritab rt, 
          mpulktmm tmm, 
          mpuzntab znt, 
          gsi_sle_planes_obt abc
    Where ri2.ricode=abc.ricode
      And abc.id_plan=lv_id_plan
      And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
      And rpe.rate_pack_element_id=rpp.rate_pack_element_id
      And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
      And znt.des!='CLARO'    
      And rpe.Chargeable_Quantity_Udmcode=10000 
      And rpp.parameter_seqnum=4
      And tmm.ricode=ri2.ricode 
      And tmm.tmcode=rp.tmcode 
      And rt.ricode=tmm.ricode 
      And znt.zncode=ri2.zncode
      And ri2.vscode=(Select Max(vscode) 
                        From mpulkrim h 
                       Where h.ricode=ri2.ricode 
                         And h.rate_type_id In (1,2)
                     )--(101,119,114,115,116))
      And tmm.vscode In (Select Max(vscode) 
                           From mpulktmm mt 
                          Where mt.tmcode=tmm.tmcode 
                            And mt.upcode In (1,2,4,7,8,16) 
                            And mt.sncode In (540)
                        ) 
      And tmm.sncode In (540);  

   type t_id_plan  is table of gsi_sle_planes_obt.id_plan%type;
   type t_ricode   is table of mpulktmm.ricode%type;
   type t_des_ri   is table of mpuritab.des%type;
   type t_zncode   is table of mpulkrim.zncode%type;
   type t_des_zone is table of mpuzntab.des%type; 
   type t_tipo     is table of gsi_sle_tmp_ctos_plan_2.tipo%type;
   type t_float    is table of rate_pack_parameter_value.parameter_value_float%type;
   type t_costo    is table of number;

   l_id_plan   t_id_plan;
   l_ricode    t_ricode;
   l_des_ri    t_des_ri;   
   l_zncode    t_zncode;
   l_des_zone  t_des_zone;
   l_tipo      t_tipo;
   l_float     t_float;
   l_costo     t_costo;      

begin

   For i In reg(PN_HILO) Loop
   
      open c_planes(i.id_plan);
      loop
     fetch c_planes bulk collect into l_id_plan,l_ricode,l_des_ri,l_zncode,l_des_zone,l_tipo,l_float,l_costo limit 2000;

    forall j in 1 .. l_id_plan.count
    insert into gsi_sle_tmp_ctos_plan_2 
    values (l_id_plan(j),l_ricode(j),l_des_ri(j),l_zncode(j),l_des_zone(j),l_tipo(j),l_float(j),l_costo(j));
    --commit;
      exit when c_planes%NOTFOUND;
       end loop;
    commit;

     close c_planes;
     
    update gsi_sle_planes_obt
       set procesado='S'
     where Rowid=i.Rowid;
     commit;

   end loop;
exception 
   when others then
      rollback;
      dbms_output.put_line('Error -> ' || substr(sqlerrm,1,240));
END PR_EXTRAE_COSTOS_SNCODE_540_BC;

  
-- PARA EXTRAER COSTOS DE LOS VPN INPOOL. 543 - VPN FORCED ON-NET
PROCEDURE PR_EXTRAE_COSTOS_SNCODE_543(PN_HILO IN NUMBER)IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=543 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
  
   For i In reg(PN_HILO) Loop
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO Inpool' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (543)
                           ) 
         And tmm.sncode In (543);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_SNCODE_543;
  
-- PARA EXTRAER COSTOS DE LOS VPN CLARO 776. 988 - VPN 776 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_988_776(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=988 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;
Begin

   For i In reg(PN_HILO) Loop
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 776' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (988)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (988);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_988_776;
  
-- PARA EXTRAER COSTOS DE LOS VPN CLARO 777. 998 - VPN 777 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_998_777(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=998 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
   For i In reg(PN_HILO) Loop
     
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 777' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (998)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (998);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;
    
    Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_998_777;
  
-- PARA EXTRAER COSTOS DE LOS VPN CLARO 778. 999 - VPN 778 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_999_778(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=999 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;
Begin
   For i In reg(PN_HILO) Loop
    
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 778' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (999)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (999);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_999_778;

-- PARA EXTRAER COSTOS DE LOS VPN CLARO 779. 1000 -VPN 779 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_1000_779(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=1000 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
   For i In reg(PN_HILO) Loop
     
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 779' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (1000)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (1000);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_1000_779;
  
-- PARA EXTRAER COSTOS DE LOS VPN CLARO 780. 1307 -VPN 780 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_1307_780(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=1307 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
   For i In reg(PN_HILO) Loop
      
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 780' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (1307)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (1307);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;

      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_1307_780;
  
-- PARA EXTRAER COSTOS DE LOS VPN CLARO 781. 1308 -VPN 781 ----988,998,999,1000,1307,1308
PROCEDURE PR_EXTRAE_COSTOS_1308_781(PN_HILO IN NUMBER) IS

   Cursor reg(cn_hilo number) Is
   Select a.rowid, a.* 
     From gsi_sle_planes_obt a 
    Where sncode=1308 
      And procesado Is Null 
      and ricode != 30 
      and hilo=cn_hilo;
      --And hilo=1;

Begin
   For i In reg(PN_HILO) Loop
     
      Insert Into gsi_sle_tmp_ctos_plan_2
      Select /*+ first_rows */Distinct abc.id_plan, 
             tmm.ricode, 
             rt.des DES_RI, 
             ri2.zncode, 
             'CLARO 781' DES_ZONE, 
             decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
             rpp.parameter_value_float, 
             round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
        From mpulkrim ri2, 
             rate_pack_element rpe, 
             rate_pack_parameter_value rpp,
             rateplan rp, 
             mpuritab rt, 
             mpulktmm tmm, 
             mpuzntab znt, 
             gsi_sle_planes_obt abc
       Where ri2.ricode=abc.ricode
         And abc.id_plan=i.id_plan
         And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
         And rpe.rate_pack_element_id=rpp.rate_pack_element_id
         And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
         And znt.des='CLARO'
         And rpe.Chargeable_Quantity_Udmcode=10000 
         And rpp.parameter_seqnum=4
         And tmm.ricode=ri2.ricode 
         And tmm.tmcode=rp.tmcode 
         And rt.ricode=tmm.ricode 
         And znt.zncode=ri2.zncode
         And ri2.vscode=(Select Max(vscode) 
                           From mpulkrim h 
                          Where h.ricode=ri2.ricode 
                            And h.rate_type_id In (1,2)
                        )--(101,119,114,115,116))
         And tmm.vscode In (Select Max(vscode) 
                              From mpulktmm mt 
                             Where mt.tmcode=tmm.tmcode 
                               And mt.upcode In (1,2,4,7,8,16) 
                               And mt.sncode In (1308)
                           )  ----988,998,999,1000,1307,1308
         And tmm.sncode In (1308);
    
      Update gsi_sle_planes_obt
         Set procesado='S'
       Where Rowid=i.Rowid;
      
      Commit;
    
  End Loop;

END PR_EXTRAE_COSTOS_1308_781;


-- CUANDO TERMINENE LA EXTRACCIÓN, BORRAR LAS ZONAS DUMMIES
PROCEDURE PR_DEPURA_TABLA_COSTOS IS
BEGIN
   Delete 
     From gsi_sle_tmp_ctos_plan_2 
    Where des_zone In ('Voice Mail','Extensiones','ZONA TARJETA','InterRegional','IntraRegional');
    
   commit;
   
   PR_QC_CUADRE_DE_REGISTROS_1;
   PR_QC_CUADRE_DE_REGISTROS_2;
   
exception

  when others then
    dbms_output.put_line('Error : Existieron inconvenientes al depurar la tabla: gsi_sle_tmp_ctos_plan_2');
    dbms_output.put_line('Error : ' || sqlcode || ' - ' || substr(sqlerrm,1,200));   
   
END PR_DEPURA_TABLA_COSTOS;

-- HACER CUADRE. Debe salir la misma cantidad de registros entre los dos COUNT(*)
PROCEDURE PR_QC_CUADRE_DE_REGISTROS_1 IS

   count_1 number;
   count_2 number;
   le_no_cuadra exception;
   
BEGIN
   Select count(*) 
     into count_1
     From gsi_sle_tmp_ctos_plan_2;

   Select Count(*) 
     into count_2
     From (Select b.descripcion, b.tmcode,a.*
             From gsi_sle_tmp_ctos_plan_2 a, 
                  (Select Distinct id_plan, descripcion, tmcode 
                     From gsi_sle_planes_obt
                  ) b
            Where a.plan=b.id_plan
          );
   
   if count_1 <> count_2 then
      raise le_no_cuadra;
   end if;
  
EXCEPTION
   WHEN le_no_cuadra THEN
      dbms_output.put_line('Error: Verifica que los count gsi_sle_tmp_ctos_plan_2 vs 
                            gsi_sle_tmp_ctos_plan_2 (gsi_sle_anes_obt) coincidan.');

END PR_QC_CUADRE_DE_REGISTROS_1;

--Hago QC de planes que tengan diferenetes tarifas, o RIs. No debe salir nada.
PROCEDURE PR_QC_CUADRE_DE_REGISTROS_2 IS

   CURSOR c_ris IS
   SELECT G.PLAN, G.DES_ZONE, G.TIPO, COUNT(*) 
     FROM gsi_sle_tmp_ctos_plan_2 G
    GROUP BY G.PLAN, G.DES_ZONE, G.TIPO
   HAVING COUNT(*) > 1;

   lc_ris c_ris %rowtype;
   le_no_cuadra exception;

BEGIN

   OPEN c_ris;
   FETCH c_ris INTO lc_ris;
   IF c_ris%FOUND THEN
      RAISE le_no_cuadra;
   END IF;
   CLOSE c_ris;   
  
EXCEPTION
   WHEN le_no_cuadra THEN
      dbms_output.put_line( 'Error: Verifica que no existan planes que tengan diferentes 
                             tarifas, o RIs en la tabla gsi_sle_tmp_ctos_plan_2.');

END PR_QC_CUADRE_DE_REGISTROS_2;


--Hago QC de planes que tengan diferenetes tarifas, o RIs. No debe salir nada.
PROCEDURE PR_GENERA_TABLA_FINAL(PV_FECHA_YYYYMMDD VARCHAR2) IS

   cursor c_existe_tabla(lv_table varchar2) is
   select count(*)
     from user_tables
    where table_name=lv_table;

   ln_existe number;
   lv_sentencia_create varchar(4000);

BEGIN
  
   open  c_existe_tabla('GSI_TARIFAS_BSCS_'||PV_FECHA_YYYYMMDD);
   fetch c_existe_tabla
   into  ln_existe; 
   close c_existe_tabla;

   if ln_existe <> 0 then
      execute immediate 'drop table GSI_TARIFAS_BSCS_'||PV_FECHA_YYYYMMDD ||' CASCADE CONSTRAINTS PURGE';
   end if;
   
   lv_sentencia_create := 'create table GSI_TARIFAS_BSCS_' || PV_FECHA_YYYYMMDD || 
' TABLESPACE TBS_TARIFAS_DAT as ' ||
'select id_plan,descripcion,tmcode,
sum("1700") "1700",
sum("1800") "1800",
sum(claro) claro,
sum(claro_inpool) claro_inpool,
sum(claro_776) claro_776,
sum(claro_777) claro_777,
sum(claro_778) claro_778,
sum(claro_779) claro_779,
sum(claro_780) claro_780,
sum(claro_781) claro_781,
sum(movistar) movistar,
sum(alegro) alegro,
sum(fija) fija,
sum(chile) chile,
sum(Cuba) Cuba,
sum(Espana) Espana,
sum(Italia) Italia,
sum(Canada) Canada,
sum(Usa) Usa,
sum(Europa) Europa,
sum(Japon) Japon,
sum(Maritima) Maritima,
sum(Mexico) Mexico,
sum(Mexico_Cel) Mexico_Cel,
sum(Pacto_Andino) Pacto_Andino,
sum(RestoAmerica) RestoAmerica,
sum(RestodelMundo) RestodelMundo,
sum(Zona_Especial) Zona_Especial,
sum(ALEMANIA) ALEMANIA,
sum(ARGENTINA) ARGENTINA,
sum(BOLIVIA) BOLIVIA,
sum(BRASIL) BRASIL,
sum(CHINA) CHINA,
sum(COLOMBIA) COLOMBIA,
sum(COSTA_RICA) COSTA_RICA,
sum(FRANCIA) FRANCIA
from (
Select a.plan id_plan, b.descripcion, b.tmcode, a.ricode, a.des_ri, 
decode(a.des_zone,''1700'',a.costo,0) "1700",
decode(a.des_zone,''1800'',a.costo,0) "1800",
decode(a.des_zone,''CLARO'',a.costo,0) claro,
decode(a.des_zone,''CLARO Inpool'',a.costo,0) claro_inpool,
decode(a.des_zone,''CLARO 776'',a.costo,0) claro_776,
decode(a.des_zone,''CLARO 777'',a.costo,0) claro_777,
decode(a.des_zone,''CLARO 778'',a.costo,0) claro_778,
decode(a.des_zone,''CLARO 779'',a.costo,0) claro_779,
decode(a.des_zone,''CLARO 780'',a.costo,0) claro_780,
decode(a.des_zone,''CLARO 781'',a.costo,0) claro_781,
decode(a.des_zone,''Bellsouth'',a.costo,0) movistar,
decode(a.des_zone,''Alegro'',a.costo,0) alegro,
decode(a.des_zone,''Local'',a.costo,0) fija,
decode(a.des_zone,''Chile'',a.costo,0) chile,
decode(a.des_zone,''CUBA'',a.costo,0) Cuba,
decode(a.des_zone,''Espana e Italia'',a.costo,0) Espana,
decode(a.des_zone,''ITALIA'',a.costo,0) Italia,
decode(a.des_zone,''Canada'',a.costo,0) Canada,
decode(a.des_zone,''USA'',a.costo,0) Usa,
decode(a.des_zone,''Europa'',a.costo,0) Europa,
decode(a.des_zone,''Japon'',a.costo,0) Japon,
decode(a.des_zone,''Maritima'',a.costo,0) Maritima,
decode(a.des_zone,''Mexico'',a.costo,0) Mexico,
decode(a.des_zone,''Mexico CEL'',a.costo,0) Mexico_Cel,
decode(a.des_zone,''Pacto Andino'',a.costo,0) Pacto_Andino,
decode(a.des_zone,''Resto de America'',a.costo,0) RestoAmerica,
decode(a.des_zone,''Resto del Mundo'',a.costo,0) RestodelMundo,
decode(a.des_zone,''ZONA ESPECIAL'',a.costo,0) Zona_Especial,
decode(a.des_zone,''ALEMANIA'',a.costo,0) "ALEMANIA",
decode(a.des_zone,''ARGENTINA'',a.costo,0) "ARGENTINA",
decode(a.des_zone,''BOLIVIA'',a.costo,0) "BOLIVIA",
decode(a.des_zone,''BRASIL'',a.costo,0) "BRASIL",
decode(a.des_zone,''CHINA'',a.costo,0) "CHINA",
decode(a.des_zone,''COLOMBIA'',a.costo,0) "COLOMBIA",
decode(a.des_zone,''COSTA RICA'',a.costo,0) "COSTA_RICA",
decode(a.des_zone,''FRANCIA'',a.costo,0) "FRANCIA"
From gsi_sle_tmp_ctos_plan_2 a, 
(Select Distinct x.id_plan, x.descripcion, x.tmcode, y.tipo , y.id_subproducto
From gsi_sle_planes_obt x, gsi_sle_planes_obt_pre y
Where x.id_plan=y.id_plan) b
Where a.plan=b.id_plan) group by id_plan,descripcion,tmcode';
  
   execute immediate lv_sentencia_create;
   
   EXECUTE IMMEDIATE 'create index idx_id_plan_'|| PV_FECHA_YYYYMMDD ||
                     '    on GSI_TARIFAS_BSCS_' || PV_FECHA_YYYYMMDD ||'(ID_PLAN) '||
                ' TABLESPACE TBS_TARIFAS_IDX';                     
   
exception

  when others then
    dbms_output.put_line('Error : Inconvenientes durante la generacion de la tabla: GSI_TARIFAS_BSCS_' || PV_FECHA_YYYYMMDD);
    dbms_output.put_line('Error : ' || sqlcode || ' - ' || substr(sqlerrm,1,200));   

END PR_GENERA_TABLA_FINAL;


PROCEDURE PR_INSERTAR_EN_TABLA_FINAL(PV_FECHA_YYYYMMDD VARCHAR2) IS

   lv_sentencia_insert varchar2(4000);  

BEGIN
  
   lv_sentencia_insert := 'insert into GSI_TARIFAS_BSCS_' || PV_FECHA_YYYYMMDD ||
' select b.ID_PLAN,
b.descripcion,
b.cod_bscs,
0 "1700",
0 "1800",
0 claro,
0 claro_inpool,
0 claro_776,
0 claro_777,
0 claro_778,
0 claro_779,
0 claro_780,
0 claro_781,
0 movistar,
0 alegro,
0 fija,
0 chile,
0 cuba,
0 espana,
0 italia,
0 canada,
0 usa,
0 europa,
0 japon,
0 maritima,
0 mexico,
0 mexico_cel,
0 pacto_andino,
0 restoamerica,
0 restodelmundo,
0 zona_especial,
0 alemania,
0 argentina,
0 bolivia,
0 brasil,
0 china,
0 colombia,
0 costa_rica,
0 francia
 from gsi_sle_planes_obt_pre b, CONTROL.CONT_PLUS_PLANES_DAT@PRUEBA c
where b.id_plan in (select a.id_plan
                      from gsi_sle_planes_obt a
                     where a.ricode = 30
                       and a.procesado is null
                   )
  and ''BP-'' || c.CODI_PLAN = +b.ID_PLAN
  and c.codi_plan_tecn = 250';

   execute immediate lv_sentencia_insert;
   commit;
  
exception

  when others then
    dbms_output.put_line('Error : Inconvenientes durante la insersion en la tabla: GSI_TARIFAS_BSCS_' || PV_FECHA_YYYYMMDD);
    dbms_output.put_line('Error : ' || sqlcode || ' - ' || substr(sqlerrm,1,200));   
  
END PR_INSERTAR_EN_TABLA_FINAL;

PROCEDURE PR_DEPURA_TABLAS_TARIFAS(PN_DIAS_MANTIENE NUMBER DEFAULT NULL) IS

   cursor c_tablas_antiguas(cn_dias_mantiene number) is
   select tbl.table_name
          --tbl.tablespace_name,
          --obj.object_type,
          --obj.created
     from user_tables  tbl,
          user_objects obj
    where tbl.table_name = obj.object_name
      and tbl.table_name like '%GSI_TARIFAS_BSCS_%'
      and tbl.tablespace_name = 'TBS_TARIFAS_DAT'
      and obj.created <= (sysdate-cn_dias_mantiene);
      --and obj.created <= (sysdate-30);
      
   ln_dias_mantiene number;

BEGIN

   if PN_DIAS_MANTIENE is null then
      ln_dias_mantiene:=30;
   else
      ln_dias_mantiene:=PN_DIAS_MANTIENE;
   end if;
  
   for i in c_tablas_antiguas(ln_dias_mantiene) loop
      --dbms_output.put_line('execute immediate drop table '|| i.table_name || ' CASCADE CONSTRAINTS PURGE');
      execute immediate 'drop table '|| i.table_name || ' CASCADE CONSTRAINTS PURGE';
   end loop;   

exception

  when others then
    dbms_output.put_line('Error : Inconvenientes durante la depuracion de tablas antiguas');
    dbms_output.put_line('Error : ' || sqlcode || ' - ' || substr(sqlerrm,1,200));   

END PR_DEPURA_TABLAS_TARIFAS;

end GSI_PROCESOS_AUTOMATICO;
/
