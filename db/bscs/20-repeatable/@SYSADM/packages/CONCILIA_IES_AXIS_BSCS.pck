CREATE OR REPLACE package concilia_ies_axis_bscs is
--
        Procedure Principal;
--
end concilia_ies_axis_bscs;
/
CREATE OR REPLACE package body concilia_ies_axis_bscs is
--
    Procedure Principal  is
      --
      lv_error           varchar2(1000);
      LvSentencia        varchar2(500);
      lv_telefono        varchar2(24);
      --
      Cursor  Datos_Bscs is 
              select ci.telefono, 
                     decode(pss.status,'D','I','A') estado_bscs,
                     ps.sncode                               sncode_bscs, 
                     psh.spcode                              spcode_bscs,
                     ca.tmcode                               tmcode_bscs,
                     ps.entry_date                           fecha_profi,
                     psh.entry_date                          fecha_serv_spcode, 
                     upper(m1.des)                           desc_servi,
                     sc.co_id                                co_id
              from concilia_revisa_ies ci, directory_number d, contr_services_cap sc, contract_all ca
              ,profile_service ps, pr_serv_spcode_hist psh, mpusntab  m1, pr_serv_status_hist pss
              where  d.dn_num           = '5939'||ci.telefono
              and    d.dn_id            =  sc.dn_id
              and    sc.cs_deactiv_date is null
              and    sc.co_id           =  ca.co_id
              and    sc.co_id           = ps.co_id
              and    ps.co_id           =    psh.co_id
              and    ps.spcode_histno   =  psh.histno
              and    ps.status_histno   = pss.histno
              and    ps.co_id           = pss.co_id
              and    ps.sncode          = pss.sncode
              and    m1.sncode          =  ps.sncode
              and    upper(m1.des)      like '%IES%'
              and    ci.telefono        = lv_telefono
              ;
      --
      Cursor Datos_Axis is
                  select --r.id_zona_bscs reg ,s.id_contrato contrato, 
                  s.id_servicio           telefono, 
                  ds.id_tipo_detalle_serv deta_serv_axis,
                  ds.descripcion          descr_deta_serv_axisd,
                  d.estado                estado_axis, 
                  d.fecha_desde           fecha_desde_axis, 
                  d.fecha_hasta           fecha_hasta_axis, 
                  --s.id_detalle_plan deta_plan, 
                  --s.id_clase tcno, 
                  --pa.cod_axis, pa.desc_paq_bscs, 
                  pa.sn_code              sn_code_axis,
                  pa.sp_code              sp_code_axis,
                  bs.cod_bscs             tm_code_axis                   
                  from cl_detalles_servicios@axis d , cl_servicios_contratados@axis s , cl_tipos_detalles_servicios@axis ds, cl_contratos@axis c,
                       ge_localidades@axis        l , ge_provincias@axis            r , bs_planes@axis                   bs, bs_servicios_paquete@axis pa 
                  where   
                       (d.id_tipo_detalle_serv, d.fecha_desde) in (select z.id_tipo_detalle_serv, max(z.fecha_desde) 
                                                                   from   cl_detalles_servicios@axis z
                                                                   where   z.id_servicio = d.id_servicio
                                                                   and     z.id_servicio = lv_telefono
                  --                                                 and    ( ( z.estado = 'A' and fecha_desde <= to_Date('24/01/2004','dd/mm/yyyy')) or
                  --                                                          ( z.estado = 'I' and fecha_hasta between to_Date('24/01/2004','dd/mm/yyyy') and to_Date('23/02/2004','dd/mm/yyyy'))
                  --                                                        )
                                                                   and       z.id_tipo_detalle_serv = d.id_tipo_detalle_serv 
                                                                  )
                  and ds.descripcion like '%I-ES%' 
                  and ds.descripcion not like '%SMS%' 
                  and  d.id_servicio                                = s.id_servicio
                  and  d.id_contrato                                = s.id_contrato
                  and  d.id_subproducto                             = s.id_subproducto
                  and  s.fecha_inicio = (select max(x.fecha_inicio) from cl_servicios_contratados@axis x where x.id_servicio = s.id_servicio)
                  and  ds.id_tipo_detalle_serv                      = d.id_tipo_detalle_serv
                  and  s.id_contrato                                = c.id_contrato
                  and  s.id_subproducto                             in ('TAR','AUT')
                  and  c.id_localidad                               = l.id_localidad 
                  and  l.id_provincia                               = r.id_provincia 
                  and  bs.id_detalle_plan                          =   s.id_detalle_plan
                  and  bs.id_clase                                 =   s.id_clase
                  and  bs.id_clase                                 =   pa.id_clase
                  and  substr(d.id_tipo_detalle_serv,5)            =  pa.cod_axis
                  and  d.id_servicio                               =  lv_telefono
                  ;
      -- Tabla que contiene todos los n�meros a ser revisados
      Cursor  celu_conciliar is
         select * 
         from concilia_revisa_ies
--         where rownum < 2
         ;
      --    
      --
    Begin
      -- Encero la tabla "concilia_ies_axis"
      LvSentencia := 'Truncate table concilia_ies_bscs';
      Execute Immediate LvSentencia;
      -- Encero la tabla "concilia_ies_axis"
      LvSentencia := 'Truncate table concilia_ies_axis';
      Execute Immediate LvSentencia;
       --
       --  Por cada Telefono a revisar se busca en Axis sus registros y se insertan
       --
       For i in celu_conciliar
       loop
          --
          lv_telefono := i.telefono;
             --
             For k in Datos_Axis
             loop
                 Begin
                    --
                    Insert into concilia_ies_axis (telefono, deta_serv_axis, descr_deta_serv_axisd, estado_axis, 
                                fecha_desde_axis, fecha_hasta_axis, sn_code_axis, sp_code_axis, tm_code_axis)
                          values( k.telefono, k.deta_serv_axis, k.descr_deta_serv_axisd, k.estado_axis, 
                                k.fecha_desde_axis, k.fecha_hasta_axis, k.sn_code_axis, k.sp_code_axis, k.tm_code_axis)
                    ;
                 Exception
                     When others then
                          lv_error := sqlerrm;
                 End;
             End Loop;
             commit;  -- x cada telefono
             --
       End Loop;
       --
       --
       --  Por cada Telefono a revisar se busca en BSCS sus registros y se insertan
       --
       For i in celu_conciliar
       loop
          --
          lv_telefono := i.telefono;
             --
             For k in Datos_BSCS
             loop
                 Begin
                    --
                    Insert into concilia_ies_bscs (telefono, estado_bscs, sncode_bscs, spcode_bscs, tmcode_bscs, 
                                                   fecha_profi, fecha_serv_spcode, desc_servi,  co_id )
                          values( k.telefono, k.estado_bscs, k.sncode_bscs, k.spcode_bscs, k.tmcode_bscs, 
                                  k.fecha_profi, k.fecha_serv_spcode, k.desc_servi, k.co_id )
                    ;
                 Exception
                     When others then
                          lv_error := sqlerrm;
                 End;
             End Loop;
             commit;  -- x cada telefono
             --
       End Loop;
       --
       commit;
    End;
--
end concilia_ies_axis_bscs;

/*
 *  Para crear la tabla por si no existe
 *
-- Create table
create table CONCILIA_REVISA_IES
(
  TELEFONO VARCHAR2(24)
)
tablespace DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 512K
    next 512K
    minextents 1
    maxextents 505
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on CONCILIA_REVISA_IES to PUBLIC;
*
*/

/*
 *  Para crear la tabla por si no existe
 *
-- Create table
create table CONCILIA_IES_BSCS
(
  TELEFONO          VARCHAR2(24),
  ESTADO_BSCS       VARCHAR2(1),
  SNCODE_BSCS       NUMBER not null,
  SPCODE_BSCS       NUMBER not null,
  TMCODE_BSCS       NUMBER,
  FECHA_PROFI       DATE not null,
  FECHA_SERV_SPCODE DATE not null,
  DESC_SERVI        VARCHAR2(30),
  CO_ID             NUMBER
)
tablespace DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents 505
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on CONCILIA_IES_BSCS to PUBLIC;
 */

 /*
  *
  *
 -- Create table
create table CONCILIA_REVISA_IES
(
  TELEFONO VARCHAR2(24)
)
tablespace DATA
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 512K
    next 512K
    minextents 1
    maxextents 505
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on CONCILIA_REVISA_IES to PUBLIC;
*/
/

