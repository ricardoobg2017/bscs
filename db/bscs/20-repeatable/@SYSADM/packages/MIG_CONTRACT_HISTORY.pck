CREATE OR REPLACE PACKAGE MIG_CONTRACT_HISTORY AS

PROCEDURE MAIN;

PROCEDURE Insert_CONTRACT_HISTORY (nCo_Id        IN CONTRACT_ALL.CO_ID%TYPE,
		  		   nseq_no       IN CONTRACT_HISTORY.CH_SEQNO%TYPE,
		  		   v_effective   IN CONTRACT_HISTORY.CH_VALIDFROM%TYPE,
				   v_last_status IN CONTRACT_HISTORY.CH_STATUS%TYPE,
				   v_reason      IN CONTRACT_HISTORY.CH_REASON%TYPE,
				   v_operator    IN CONTRACT_HISTORY.USERLASTMOD%TYPE,
				   nRequest_Id   IN GMD_REQUEST_BASE.REQUEST_ID%TYPE );

END MIG_CONTRACT_HISTORY;
/
CREATE OR REPLACE PACKAGE BODY MIG_CONTRACT_HISTORY AS


PROCEDURE MAIN IS

-- Global Variables

sError	  	   	   	VARCHAR2(100);
v_ch_seqno			NUMBER ;
v_status_ant			VARCHAR2(1) ;
v_ac_status 	   		VARCHAR2(1) ;
v_ac_seffec 			DATE ;
v_reason			NUMBER ;
v_status_curr			VARCHAR2(1) ;
v_status_bscs			VARCHAR2(2) ;
v_contador			NUMBER := 0 ;


CURSOR cHISTORY IS
   SELECT
      c.co_id,
      c.co_activated,
      c.co_userlastmod ,
      c.co_moddate,
      u.target_reached,
      c.co_crd_days,
      c.co_crd_clicks
   FROM customer_all u, contract_all c, mig_postmigration@cbill m
   WHERE c.customer_id    = u.customer_id
     AND c.sccode        != 6
	 and m.account = u.target_reached
	 and m.activnum = c.co_crd_days
     AND u.cslevel        = '40' ;


CURSOR c1 (v_target_reached  customer_all.target_reached%type,
           v_co_crd_days     contract_all.co_crd_days%type,
    	   v_co_moddate      date
	   ) IS
  SELECT m.status_bscs, r.reason_bscs, upper(h.operator) OPERATOR,
         h.effective, h.timeeffective
    FROM acthist@cbill h, mig_lkstatus@cbill m, mig_lkreason@cbill r
   WHERE h.account  = v_target_reached
     AND h.activnum = v_co_crd_days
     AND h.effective >= '2003/06/24'
     AND to_date(h.effective || ' ' || h.timeeffective, 'YYYY/MM/DD hh24:mi') < v_co_moddate
     AND m.status_cbill = h.status
     AND r.reason_cbill = h.reason
  ORDER BY h.effective, h.timeeffective ;

vcom			 cHISTORY%ROWTYPE;

BEGIN

  OPEN cHISTORY;

  IF cHISTORY%NOTFOUND THEN
    DBMS_OUTPUT.PUT_LINE('No data found');
  END IF;

  LOOP
    FETCH cHISTORY INTO vcom;
    EXIT WHEN cHISTORY%NOTFOUND;

    Insert_CONTRACT_HISTORY ( vcom.co_id,
	                      1,
	                      vcom.co_activated,
	                      'o',
			      50,
    			      vcom.co_userlastmod,
			      NULL );

    Insert_CONTRACT_HISTORY ( vcom.co_id,
	                      2,
	                      vcom.co_activated,
	     		      'a',
			      37,
			      vcom.co_userlastmod,
			      NULL );

    v_ch_seqno   := 3 ;
    v_status_ant := 'a' ;

    IF vcom.co_moddate >= to_date('2003/06/24', 'YYYY/MM/DD') THEN

      BEGIN

        SELECT m.status_bscs, to_date(ac.statuseffective, 'yyyy/mm/dd hh24:mi')
          into v_ac_status, v_ac_seffec
          FROM activ2@cbill ac, mig_lkstatus@cbill m
	 WHERE ac.account    = vcom.target_reached
	   AND ac.activnum   = vcom.co_crd_days
	   AND ac.status     = m.status_cbill
	   AND substr(ac.statuseffective,1,10) < '2003/06/24' ;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_ac_status := NULL ;
           v_ac_seffec := NULL ;
      END;

      IF v_ac_status != 'a' THEN

        IF v_ac_status = 'a' THEN
	  v_reason := 37;
	ELSIF v_ac_status = 's' THEN
	  v_reason := 49;
        ELSIF v_ac_status = 'd' THEN
	  v_reason := 48;
	END IF;

        Insert_CONTRACT_HISTORY ( vcom.co_id,
                                  v_ch_seqno,
                                  v_ac_seffec,
                                  v_ac_status,
                                  v_reason,
                                  vcom.co_userlastmod,
                                  NULL );

        v_ch_seqno   := v_ch_seqno + 1;
        v_status_ant := v_ac_status;

      END IF;

      FOR vReg IN c1 ( vcom.target_reached,  vcom.co_crd_days, vcom.co_moddate ) LOOP

	IF vReg.status_bscs != v_status_ant THEN

	  insert_CONTRACT_HISTORY ( vcom.co_id,
	                            v_ch_seqno,
                                    to_date(vReg.effective || ' ' || vReg.timeeffective, 'yyyy/mm/dd hh24:mi'),
                                    vReg.status_bscs,
                                    vReg.reason_bscs,
                                    vReg.operator,
                                    NULL ) ;
	  v_ch_seqno   := v_ch_seqno + 1;
	  v_status_ant := vReg.status_bscs;
	END IF;

      END LOOP;

    END IF;

    SELECT  status_bscs  INTO  v_status_curr
      FROM mig_lkstatus@cbill
     WHERE status_cbill = vcom.co_crd_clicks ;

    IF v_status_curr != v_status_ant THEN

      IF v_status_curr = 'a' THEN
        v_reason := 37;
      ELSIF v_status_curr = 's' THEN
        v_reason := 49;
      ELSIF v_status_curr = 'd' THEN
	v_reason := 48;
      END IF;

      insert_CONTRACT_HISTORY ( vcom.co_id,
                                v_ch_seqno,
                                vcom.co_moddate,
                                v_status_curr,
                                v_reason,
                                vcom.co_userlastmod,
                                NULL );

    END IF ;

    IF v_contador = 1000 THEN
      v_contador := 0;
      commit;
    END IF ;

    v_contador := v_contador + 1;

  END LOOP;

  CLOSE cHISTORY;

  COMMIT;

END MAIN;


PROCEDURE Insert_CONTRACT_HISTORY ( nCo_Id        IN CONTRACT_ALL.CO_ID%TYPE,
                                    nseq_no       IN CONTRACT_HISTORY.CH_SEQNO%TYPE,
	                            v_effective   IN CONTRACT_HISTORY.CH_VALIDFROM%TYPE,
	                            v_last_status IN CONTRACT_HISTORY.CH_STATUS%TYPE,
                                    v_reason      IN CONTRACT_HISTORY.CH_REASON%TYPE,
		                    v_operator    IN CONTRACT_HISTORY.USERLASTMOD%TYPE,
		                    nRequest_Id   IN GMD_REQUEST_BASE.REQUEST_ID%TYPE )
IS

-- Insert row in contract history table

sError		 	 			VARCHAR2(200);

BEGIN

  INSERT INTO contract_history
  (co_id,
   ch_seqno,
   ch_status,
   ch_reason,
   ch_validfrom,
   entdate,
   userlastmod,
   request,
   rec_version)
  VALUES
  (nco_id,
   nseq_no,
   v_last_status,
   v_reason,
   v_effective,
   v_effective,
   v_operator,
   null,
   0);

--  commit;


END Insert_CONTRACT_HISTORY;


END MIG_CONTRACT_HISTORY;
/

