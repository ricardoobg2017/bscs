create or replace package COK_REPORTE_DATACREDITO_V2 is

  ---==================================COK_REPORTE_DATACREDITO_V2=========================================--
  -- Versi�n:         1.0.0
  -- Descripci�n:     Paquete BSCS
  --      ..................................................
  --=====================================================================================--
  -- Desarrollado por:     Jhonny Morej�n
  -- Fecha de creaci�n:    22/Marzo/2006
  -- Proyecto:             [1556]Alcance Buro de cr�dito
  -- Autorizado por:       Juan Carlos Morales Carrillo
  --=====================================================================================--
  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.1
  -- Fecha:       5/04/2006
  -- Descripci�n: correcci�n de errores y mejoramiento de queries
  --=====================================================================================--

  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.2
  -- Fecha:       7/04/2006
  -- Descripci�n: Se crea una nueva tabla para llenarla con la informaci�n del reporte
  --        tal y como datacr�dito la va a recibir.  Con esto se evita realizar
  --        el union de los queries principales y hacer la consulta m�s r�pida
  --        Se optimizan los queries utilizando hints
  --=====================================================================================--
  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.3
  -- Fecha:       11/04/2006
  -- Descripci�n: Se crea una nueva tabla para bitacorizar errores a nivel de registros
  --        que no se pudieron insertar en co_union_clientes
  --=====================================================================================--
  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.4
  -- Fecha:       5/05/2006
  -- Descripci�n: Se cambia el nombre del archivo generado, con el objetivo que todos los
  --        meses se sobreescriban, evitando as� que queden acumulados en dicha ruta 
  --              archivos de meses pasados.  Esto debido a lo cr�tico que resulta el tema
  --              de almacenamiento en el servidor de bscs
  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.5
  -- Fecha:               19/07/2006, 
  -- Descripci�n: No insertamos los registros en la tabla si no tenemos identificaci�n, 
  --              ya que luego se generan inconsistencias en los sistemas de datacr�dito.
  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.6
  -- Fecha:               04/08/2006, 
  -- Descripci�n: Se aumenta un trim para evitar espacios en la identificaci�n

  TYPE cur_typ IS REF CURSOR;

  PROCEDURE COP_CALIFICA_CLIENTES_BSCS(pdfechcierreperiodo date,
                                       pv_error            out varchar2);

  PROCEDURE COP_INGRESA_VALORES_DATACRE(pv_nombre_tabla   in varchar2,
                                        pv_cuenta         in varchar2,
                                        pv_novedad        in varchar2,
                                        pv_adjetivo       in varchar2,
                                        pv_calificacion   in varchar2,
                                        pv_cuentas_mora   in varchar2,
                                        pv_edad_mora      in varchar2,
                                        pv_reclamo        in varchar2,
                                        pv_tipo_id        in varchar2,
                                        pv_tipo_persona   in varchar2,
                                        pv_identificacion varchar2,
                                        pn_codigo_error   out number,
                                        pv_error          out varchar2
                                        
                                        );

  PROCEDURE COP_CREA_TABLA(pv_nombre_tabla in varchar2,
                           pv_esquema      in varchar2,
                           pn_codigo_error out number,
                           pv_error        out varchar2);

  PROCEDURE COP_BITACORA_EJECUCION(Pv_nombreTabla    in varchar2,
                                   Pn_reg_exito      in number,
                                   Pn_reg_error      in number,
                                   Pv_descripcion    in varchar2,
                                   pv_estado_reporte in varchar2,
                                   pv_tipo_proceso   in varchar2,
                                   pv_consulta       in varchar2 default null,
                                   Pv_error          out varchar2,
                                   pv_tablaOrigen    in varchar2 default null);

  FUNCTION COF_VALIDA_IDENTIFICACON(pv_identificacion varchar2,
                                    pv_error          out varchar2)
    RETURN VARCHAR2;

  PROCEDURE COP_CALIFICA_CLIENTES_GYE;

  PROCEDURE COP_CALIFICA_CLIENTES_UIO;

  PROCEDURE COP_GENERA_ARCHIVO;

  PROCEDURE COP_ACTUALIZA_ESTADOS_BITACORA(pn_codigo in number,
                                           pv_estado in varchar2,
                                           pv_error  out varchar2);

  PROCEDURE COP_UNION_CLIENTES(pv_error out varchar2);

  PROCEDURE COP_DETALLE_BITACORA(pv_cuenta      varchar2,
                                 pv_descripcion varchar2);

  FUNCTION COF_CALIFICA_CUENTA(PV_CAMPO VARCHAR2) RETURN VARCHAR2;

  FUNCTION COF_RECLAMO(PV_CUENTA VARCHAR2) RETURN VARCHAR2;

  PROCEDURE COP_EXISTE_TABLA(PV_TABLA IN VARCHAR2, PV_ERROR OUT VARCHAR2);

  PROCEDURE COP_HABILITA_INDICE(pv_nombre_tabla IN varchar2,
                                pv_error        OUT varchar2);

  PROCEDURE COP_DESHABILITA_INDICE(pv_nombre_tabla IN varchar2,
                                   pv_error        OUT varchar2);

  PROCEDURE COP_CALIFICA_CLIENTES_BSCS_C(pdfechcierreperiodo date,
                                         pv_error            out varchar2);

  PROCEDURE COP_EJECUTA(pv_error OUT varchar2);

END COK_REPORTE_DATACREDITO_V2;
/
create or replace package body COK_REPORTE_DATACREDITO_V2 is
  ---MODIFICAR

  ---==================================COK_REPORTE_DATACREDITO_V2=========================================--
  -- Versi�n:         1.0.0
  -- Descripci�n:     Paquete BSCS
  --      ..................................................
  --=====================================================================================--
  -- Desarrollado por:     Jhonny Morej�n
  -- Fecha de creaci�n:    22/Marzo/2006
  -- Proyecto:             [1556]Alcance Buro de cr�dito
  -- Autorizado por:       Juan Carlos Morales Carrillo
  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.1
  -- Fecha:       5/04/2006
  -- Descripci�n: correcci�n de errores y mejoramiento de queries
  --=====================================================================================--

  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.2
  -- Fecha:       7/04/2006
  -- Descripci�n: Se crea una nueva tabla para llenarla con la informaci�n del reporte
  --        tal y como datacr�dito la va a recibir.  Con esto se evita realizar
  --        el union de los queries principales y hacer la consulta m�s r�pida
  --        Se optimizan los queries utilizando hints
  --=====================================================================================--

  --=====================================================================================--
  -- Actualizado por:   Jhonny Morej�n
  -- Versi�n:       1.0.3
  -- Fecha:       11/04/2006
  -- Descripci�n: Se crea una nueva tabla para bitacorizar errores a nivel de registros
  --        que no se pudieron INSERTar en co_union_clientes
  --=====================================================================================--

  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.4
  -- Fecha:       5/05/2006
  -- Descripci�n: Se cambia el nombre del archivo generado, con el objetivo que todos los
  --        meses se sobreescriban, evitando as� que queden acumulados en dicha ruta 
  --              archivos de meses pasados.  Esto debido a lo cr�tico que resulta el tema
  --              de almacenamiento en el servidor de bscs
  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.5
  -- Fecha:               19/07/2006, 
  -- Descripci�n: No INSERTamos los registros en la tabla si no tenemos identificaci�n, 
  --              ya que luego se generan inconsistencias en los sistemas de datacr�dito.
  --=====================================================================================--
  -- Actualizado por:   Juan Carlos Morales 
  -- Versi�n:       1.0.6
  -- Fecha:               04/08/2006, 
  -- Descripci�n: Se aumenta un trim para evitar espacios en la identificaci�n

  gn_commit number;
  gv_parametro CONSTANT co_parametro_repdatacre.parametro%TYPE := 'TRAN_COMMIT';
  gn_idFeature CONSTANT number := 1001;
  gn_valor     CONSTANT number := 41;
  gv_estado    CONSTANT varchar2(1) := 'A';
  gv_ohstatus  CONSTANT varchar2(2) := 'IN';

  --Comprueba si existe la tabla 
  CURSOR cg_existe_tabla(Cv_nombre_tabla varchar2) is
    SELECT 'X' existe
      FROM ALL_TABLES T
     WHERE T.TABLE_NAME = Cv_nombre_tabla;

  gr_existe_tabla cg_existe_tabla%rowtype;

  CURSOR gc_parametros(cv_parametro co_parametro_repdatacre.parametro%TYPE) IS
    SELECT nvl(valor, 500)
      FROM co_parametro_repdatacre
     WHERE parametro = cv_parametro;

  CURSOR gc_numero_moras(cv_cuenta varchar2, cv_ohstatus varchar2) is
    SELECT count(u.customer_id) Nu_Moras, cssocialsecno as IdentIFicacion
      FROM customer_all u, orderhdr_all o
     WHERE u.customer_id = o.customer_id
       AND u.custcode = cv_cuenta
       AND ohstatus = cv_ohstatus --'IN'
       AND ohopnamt_doc > 0
     group by cssocialsecno;

  PROCEDURE COP_CALIFICA_CLIENTES_BSCS(pdfechcierreperiodo date,
                                       pv_error            out varchar2) is
  
    lv_fecha        varchar2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
    ln_codigo_error number;
    lv_nombre_tabla varchar2(60) := 'CO_REPDATCRE_';
    lv_tabla_origen varchar2(60) := 'CO_REPCARCLI_' || lv_fecha;
    lv_consulta     varchar2(1000);
    lv_adjetivo     varchar2(2);
    lv_reclamo      varchar2(1);
    ln_reg_error    number := 0;
    ln_reg_exito    number := 0;
    ln_ruc          number := 0;
  
    ------*******    JPA  PROYECTO 3509    *******--------   
    lv_tipo_iden    varchar2(100);
    lv_tipo_persona varchar2(100);
  
    lv_cuenta        varchar2(25);
    lv_mayor_vencido varchar2(15);
    ln_nu_moras      number;
    lv_ruc           varchar2(20);
    ln_saldoDeuda    number;
    lv_novedad       VARCHAR2(5);
    lv_calificacion  CO_CALIFICACION_REPDATACRE.CALIFICACION%TYPE;
  
    lt_cuenta        cot_string := cot_string();
    lt_mayor_vencido cot_string := cot_string();
    lt_nu_moras      cot_number := cot_number();
    lt_ruc           cot_string := cot_string();
    lt_saldoDeuda    cot_number := cot_number();
    lt_novedad       cot_string := cot_string();
    lt_calificacion  cot_string := cot_string();
    lt_tipo_iden     cot_string := cot_string();
    lt_tipo_persona  cot_string := cot_string();
  
    ------*******    JPA  PROYECTO 3509    *******--------   
    --Variables del CURSOR
  
    le_error EXCEPTION;
    lv_programa varchar2(100) := 'COP_CALIFICA_CLIENTES_BSCS ';
  
  BEGIN
  
    -- Creo la tabla 
    --      lv_nombre_tabla:=lv_nombre_tabla||lv_fecha;    
    lv_nombre_tabla := lv_nombre_tabla || lv_fecha || '_T';
    pv_error        := NULL;
  
    -- El procedimiento pregunta si existe la tabla destino, Si la encuentra borrra los datos o sino la crea.
    -- Tambien deshabilita el indicie para bajar el tiempo del proceso 
    COP_EXISTE_TABLA(lv_nombre_tabla, pv_error);
  
    IF pv_error IS NOT NULL THEN
      raise le_error;
    END IF;
  
    lv_consulta := 'begin select cuenta,' ||
                   '(decode (balance_1,0,0,1) +  decode (balance_2,0,0,1) + decode (balance_3,0,0,1) + decode (balance_4,0,0,1) + ' ||
                   'decode (balance_5,0,0,1) +  decode (balance_6,0,0,1) + decode (balance_7,0,0,1) + decode (balance_8,0,0,1) + ' ||
                   'decode (balance_9,0,0,1) +  decode (balance_10,0,0,1) + decode (balance_11,0,0,1)) nu_moras, ' ||
                   'TOTALVENCIDA ln_saldoDeuda,' ||
                   'DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ) mayorvencido, ' ||
                   'DECODE (DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ), ''01'', ''01'', ''30'', ''06'',''60'',''07'',''90'', ''08'',''09'' ) NOVEDAD,' ||
                   'COK_REPORTE_DATACREDITO_V2.COF_CALIFICA_CUENTA( DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' ,''01'', mayorvencido )) evaluacion,' ||
                   'REPLACE(TRIM(ruc), ''-'', NULL) RUC,' ||
                   'DECODE(length(ruc),10,''CED'',13,''RUC'',''PAS'')TIPO_IDENTIFICACION,' ||
                   'DECODE(length(ruc),10,''N'',13,''J'',''N'') TIPO_PERSONA ' ||
                   'bulk collect into :1, :2, :3, :4, :5, :6, :7, :8, :9  ' ||
                  
                   'from ' || lv_tabla_origen || ' A;' || 'end;';
  
    EXECUTE IMMEDIATE lv_consulta
      using out lt_cuenta, out lt_nu_moras, out lt_saldoDeuda, out lt_mayor_vencido, out lt_novedad, out lt_calificacion, out lt_ruc, out lt_tipo_iden, out lt_tipo_persona;
  
    FOR a IN lt_cuenta.first .. lt_cuenta.last LOOP
    
      lv_cuenta        := lt_cuenta(a);
      ln_nu_moras      := lt_nu_moras(a);
      ln_saldoDeuda    := lt_saldoDeuda(a);
      lv_mayor_vencido := lt_mayor_vencido(a);
      lv_novedad       := lt_novedad(a);
      lv_calificacion  := lt_calificacion(a);
      lv_ruc           := lt_ruc(a);
      lv_tipo_iden     := lt_tipo_iden(a);
      lv_tipo_persona  := lt_tipo_persona(a);
    
      ---Valido tipo persona  
      --  
      pv_error := null;
    
      begin
        ln_ruc := TO_NUMBER(lv_ruc);
      exception
        when INVALID_NUMBER then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => pv_error);
        when others then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => pv_error);
        
      end;
    
      --Encuentro la autorizacion
      lv_reclamo := COF_RECLAMO(lv_cuenta);
    
      --Si el saldo de la deuda es menor que 0.5 entonces se pone al d�a 
      IF ln_saldoDeuda < 0.5 THEN
        lv_mayor_vencido := '01';
        lv_novedad       := '01'; --AL DIA 
        lv_adjetivo      := '00';
        ln_nu_moras      := 0;
        lv_calIFicacion  := 'A';
      END IF;
    
      --Valido el adjetivo  en base a la novedad  
      IF lv_novedad = '08' or lv_novedad = '09' THEN
        lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90
      ELSE
        IF lv_novedad = '06' or lv_novedad = '07' THEN
          lv_adjetivo := '11'; --Linea desconectada 
        ELSE
          lv_adjetivo := '00';
        END IF;
      END IF;
    
      --Ingreso la calIFicaci�n de cada cliente 
      pv_error        := NULL;
      ln_codigo_error := NULL;
    
      cop_ingresa_valores_datacre(pv_nombre_tabla   => lv_nombre_tabla,
                                  pv_cuenta         => lv_cuenta,
                                  pv_novedad        => lv_novedad,
                                  pv_adjetivo       => lv_adjetivo,
                                  pv_calIFicacion   => lv_calIFicacion,
                                  pv_cuentas_mora   => to_char(ln_nu_moras),
                                  pv_edad_mora      => lv_mayor_vencido,
                                  pv_reclamo        => lv_reclamo,
                                  pv_tipo_id        => lv_tipo_iden,
                                  pv_tipo_persona   => lv_tipo_persona,
                                  pv_identIFicacion => lv_ruc,
                                  pn_codigo_error   => ln_codigo_error,
                                  pv_error          => pv_error);
    
      IF ln_codigo_error = 0 THEN
        ln_reg_error := ln_reg_error + 1;
      ELSE
        ln_reg_exito := ln_reg_exito + 1;
      END IF;
    
      IF mod(ln_reg_exito, gn_commit) = 0 THEN
        commit;
      END IF;
    
    END LOOP;
  
    commit;
  
    pv_error := NULL;
    /* -- Habilito constraints
    COP_HABILITA_INDICE (lv_nombre_tabla,                                
                                                  pv_error);
    
    IF pv_error IS NOT NULL THEN 
        raise le_error;
        
    END IF;    
    ---*/
  
    cop_bitacora_ejecucion(Pv_nombreTabla    => lv_nombre_tabla,
                           pn_reg_exito      => ln_reg_exito,
                           pn_reg_error      => ln_reg_error,
                           pv_descripcion    => pv_error,
                           pv_estado_reporte => 'N',
                           pv_tipo_proceso   => 'P',
                           pv_consulta       => NULL,
                           pv_error          => pv_error,
                           pv_tablaOrigen    => lv_tabla_origen);
  
    -------------------------------------------------------------------  
  EXCEPTION
  
    WHEN le_error THEN
      pv_error := lv_programa || ' - ' || pv_error;
      cop_bitacora_ejecucion(Pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => pv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => pv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' - ' || sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => pv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => pv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    
  END COP_CALIFICA_CLIENTES_BSCS;

  PROCEDURE COP_CREA_TABLA(pv_nombre_tabla in varchar2,
                           pv_esquema      in varchar2,
                           pn_codigo_error out number,
                           pv_error        out varchar2) IS
  
    lv_sentecia varchar2(1000);
    le_error EXCEPTION;
    lv_programa varchar2(60) := 'COP_CREA_TABLA';
  
  BEGIN
    pv_error    := NULL;
    lv_sentecia := 'CREATE TABLE ' || pv_nombre_tabla || '(' ||
                   ' Cuenta                       varchar2(24),' ||
                   ' Novedad                      varchar(2),' ||
                   ' Adjetivo                     varchar2(2),' ||
                   ' CalIFicacion                 varchar2(2),' ||
                   ' Cuentas_mora                 varchar2(3),' ||
                   ' Edad_mora                    varchar2(3),' ||
                   ' Reclamo                      varchar(1),' ||
                   ' Tipo_ID                      varchar(3),' ||
                   ' Tipo_persona                  varchar(1),' ||
                   ' IdentIFicacion               varchar2(20)) ' ||
                   ' tablespace  REP_CRED_DAT pctfree 10 ' ||
                   '  pctused 40  initrans 1  maxtrans 255 ' ||
                   '   storage (initial 25M ' || '   next 10M ' ||
                   '   minextents 1 ' || '   maxextents 505 ' ||
                   '   pctincrease 0)' || ' NOLOGGING';
  
    BEGIN
      execute immediate lv_sentecia;
    
    EXCEPTION
      WHEN OTHERS THEN
        pv_error := 'Error al crear la tabla';
        raise le_error;
    END;
  
    --Crear el indice      
  
    lv_sentecia := 'create index idx_' || pv_nombre_tabla || ' on ' ||
                   pv_nombre_tabla || ' (CUENTA) ' ||
                   ' tablespace  REP_CRED_IDX ' || ' pctfree 10 ' ||
                   ' initrans 2 ' || ' maxtrans 255 ' || ' storage ' ||
                   ' ( initial 10M ' || '  next 5M ' || '  minextents 1 ' ||
                   '  maxextents unlimited ' || '  pctincrease 0 )';
  
    BEGIN
      execute immediate lv_sentecia;
    EXCEPTION
      WHEN OTHERS THEN
        pv_error := 'Error al crear el indice';
        raise le_error;
    END;
  
    --Permisos 
  
    lv_sentecia := 'grant all on ' || pv_nombre_tabla || ' to public';
    BEGIN
      execute immediate lv_sentecia;
    EXCEPTION
      WHEN OTHERS THEN
        pv_error := 'Error dar privelegios sobre la tabla';
        raise le_error;
    END;
  
    lv_sentecia := 'create public synonym ' || pv_nombre_tabla || ' for ' ||
                   pv_esquema || '.' || pv_nombre_tabla;
    BEGIN
      execute immediate lv_sentecia;
    EXCEPTION
      WHEN OTHERS THEN
        pv_error := 'Error al crear sinonimo de la tabla';
        raise le_error;
    END;
  
    pn_codigo_error := 1;
  
  EXCEPTION
    WHEN le_error THEN
      pn_codigo_error := 0;
      pv_error        := 'Error : ' || pv_error ||
                         ' en el siguiente programa ' || lv_programa;
    
    WHEN OTHERS THEN
      pn_codigo_error := 0;
      pv_error        := 'Error general en : ' || lv_programa;
    
  END COP_CREA_TABLA;

  PROCEDURE COP_INGRESA_VALORES_DATACRE(pv_nombre_tabla   in varchar2,
                                        pv_cuenta         in varchar2,
                                        pv_novedad        in varchar2,
                                        pv_adjetivo       in varchar2,
                                        pv_calIFicacion   in varchar2,
                                        pv_cuentas_mora   in varchar2,
                                        pv_edad_mora      in varchar2,
                                        pv_reclamo        in varchar2,
                                        pv_tipo_id        in varchar2,
                                        pv_tipo_persona   in varchar2,
                                        pv_identIFicacion varchar2,
                                        pn_codigo_error   out number,
                                        pv_error          out varchar2
                                        
                                        ) is
  
    lv_programa varchar2(60) := 'COP_INGRESA_VALORES_DATACREDITO';
    lv_sentecia varchar2(1000) := null;
  
    leError EXCEPTION;
    lvCtasMora varchar(1) := 'd';
  
  BEGIN
    -- jmo 19/07/2006, no INSERTamos los registros si no tenemos identIFicaci�n, ya que luego 
    -- se generan inconsistencias en los sistemas de datacr�dito.
    -- jmo 7/08/2006, aumentamos trim para evitar espacios en blanco.
    pv_error := NULL;
  
    IF trim(pv_identIFicacion) IS NOT NULL THEN
      lv_sentecia := '  INSERT INTO ' || pv_nombre_tabla || '(' ||
                     ' Cuenta,' || ' Novedad,' || ' Adjetivo,' ||
                     ' CalIFicacion,' || ' Cuentas_mora,' || ' Edad_mora,' ||
                     ' Reclamo,' || ' Tipo_ID,' || ' Tipo_persona,' ||
                     ' IdentIFicacion)' ||
                     ' values(:pv_cuenta, :pv_novedad, :pv_adjetivo, :pv_calIFicacion, :lvCtasMora, :pv_edad_mora, :pv_reclamo, :pv_tipo_id, :pv_tipo_persona, :pv_identIFicacion )';
    
      execute immediate lv_sentecia
        using pv_cuenta, pv_novedad, pv_adjetivo, pv_calIFicacion, lvCtasMora, pv_edad_mora, pv_reclamo, pv_tipo_id, pv_tipo_persona, pv_identIFicacion;
    
    END IF;
  
    pn_codigo_error := 1;
  
  EXCEPTION
    WHEN OTHERS THEN
      pn_codigo_error := 0;
      pv_error        := 'Error al insertar en la tabla ' ||
                         pv_nombre_tabla || ' en el siguiente programa : ' ||
                         lv_programa;
  END COP_INGRESA_VALORES_DATACRE;

  PROCEDURE COP_BITACORA_EJECUCION(Pv_nombreTabla    in varchar2 default null,
                                   Pn_reg_exito      in number default null,
                                   Pn_reg_error      in number default null,
                                   Pv_descripcion    in varchar2 default null,
                                   pv_estado_reporte in varchar2 default null,
                                   pv_tipo_proceso   in varchar2 default null,
                                   pv_consulta       in varchar2 default null,
                                   Pv_error          out varchar2,
                                   pv_tablaOrigen    in varchar2 default null) is
  
    -- pragma autonomous_transaction;
  
    lv_nombre_tabla varchar2(30) := Pv_nombreTabla;
    ln_codigo       number := 0;
    gr_existe_tabla cg_existe_tabla%rowtype;
    lb_notfound     boolean;
    lv_descripcion  varchar(500) := substr(Pv_descripcion, 1, 500);
  
  BEGIN
  
    IF pv_tipo_proceso = 'P' THEN
      OPEN cg_existe_tabla(lv_nombre_tabla);
      FETCH cg_existe_tabla
        INTO gr_existe_tabla;
      lb_notfound := cg_existe_tabla%NOTFOUND;
      CLOSE cg_existe_tabla;
    
      IF lb_notfound THEN
        lv_nombre_tabla := 'Tabla no creada';
      END IF;
    
    END IF;
  
    --Encuentro el siguiente c�digo de la bitacora 
    SELECT SC_BITACORA_REPDATCRE.NEXTVAL INTO ln_codigo FROM DUAL;
  
    INSERT INTO CO_BITACORA_REPDATACRE
      (COD_BITACORA_REPDATACRE,
       FECHA_EJECUCION,
       TABLA_ORIGEN,
       TABLA_DESTINO,
       REGISTROS_EXITO,
       REGISTROS_ERROR,
       DESCRIPCION,
       ESTADO_REPORTE,
       TIPO_PROCESO,
       CONSULTA)
    values
      (ln_codigo,
       sysdate,
       pv_tablaOrigen,
       lv_nombre_tabla,
       Pn_reg_exito,
       Pn_reg_error,
       substr(lv_descripcion, 1, 100),
       pv_estado_reporte,
       pv_tipo_proceso,
       pv_consulta);
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_error := 'Error al ingresar en la Bit�cora ' || sqlerrm;
    
  END COP_BITACORA_EJECUCION;

  ----
  FUNCTION COF_VALIDA_IDENTIFICACON(pv_identIFicacion varchar2,
                                    pv_error          OUT varchar2)
    RETURN VARCHAR2 is
  
    lv_programa       varchar2(60) := 'COF_VALIDA_IDENTIFICACON ';
    lv_identIFicacion varchar2(20) := pv_identIFicacion;
    ln_longitud       number := length(pv_identIFicacion);
    i                 number := 1;
    lv_valor          varchar2(1);
    lv_respuesta      varchar2(20) := null;
  BEGIN
    LOOP
      lv_valor := substr(lv_identIFicacion, i, 1);
    
      IF ascii(lv_valor) >= 48 AND ascii(lv_valor) <= 57 THEN
        lv_respuesta := lv_respuesta || lv_valor;
      END IF;
    
      i := i + 1;
    
      EXIT WHEN i > ln_longitud;
    END loop;
  
    RETURN lv_respuesta;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error al validar c�dula ' || lv_programa || sqlerrm;
  END COF_VALIDA_IDENTIFICACON;

  ---
  PROCEDURE COP_CALIFICA_CLIENTES_GYE IS
  
    /*  -- No se hace uso
    CURSOR c_datos is 
        SELECT  cuen_nume_cuen Cuenta,
                cuen_sald      Saldo,
                cuen_edad_mora Edad_mora,
                cuen_tipo_cart Tipo_cart
           FROM cart_cuentas_dat@FINGYE_BUROCRED a , CUSTOMER_ALL b
           WHERE a.cuen_nume_cuen= b.custcode;*/
  
    pdfechcierreperiodo date := sysdate;
    lv_fecha            varchar2(8) := to_char(pdfechcierreperiodo,
                                               'ddMMyyyy');
    lv_nombre_tabla     varchar2(60) := 'CO_REPDATCRE_GYE';
    lv_tabla_origen     varchar2(60) := 'CART_CUENTAS_DAT' ||
                                        '@FINGYE_BUROCRED';
    lv_novedad          varchar2(2);
    lv_adjetivo         varchar2(2);
    lv_calIFicacion     varchar2(1);
    lv_tipo_iden        varchar2(3);
    lv_tipo_persona     varchar2(1);
    lv_reclamo          varchar2(1);
    ln_reg_error        number := 0;
    ln_reg_exito        number := 0;
    ln_nu_moras         number;
    lv_ruc              varchar2(20);
  
    lgc_numero_moras gc_numero_moras%rowtype;
    lb_found         boolean;
    ln_codigo_error  number;
    ln_ruc           number;
    lv_error         varchar2(1000);
    le_error EXCEPTION;
    lv_programa  varchar2(60) := 'COP_CALIFICA_CLIENTES_INVEN ';
    lv_sentencia varchar(1000) := null;
  
    ----jose palma
  
    lv_cuenta   varchar2(20) := null;
    lv_saldo    varchar2(10) := null;
    lv_edadMora varchar2(10) := null;
    lv_tipoCart varchar2(10) := null;
  
    lt_tipoCart cot_string := cot_string();
    lt_cuenta   cot_string := cot_string();
    lt_saldo    cot_string := cot_string();
    lt_edadMora cot_number := cot_number();
  
    ------jose palma 
  BEGIN
    lv_nombre_tabla := lv_nombre_tabla || '_' || lv_fecha;
  
    lv_error := NULL;
    COP_EXISTE_TABLA(lv_nombre_tabla, lv_error);
  
    IF lv_error IS NOT NULL THEN
      raise le_error;
    END IF;
  
    lv_sentencia := 'begin select  cuen_nume_cuen Cuenta,' ||
                    ' cuen_sald      Saldo, ' ||
                    ' cuen_edad_mora Edad_mora,' ||
                    ' cuen_tipo_cart Tipo_cart ' ||
                    'bulk collect into :1, :2, :3, :4 ' || ' from ' ||
                    lv_tabla_origen || ', CUSTOMER_ALL b ' ||
                    ' WHERE cuen_nume_cuen= b.custcode' || ';' || 'end;';
  
    EXECUTE IMMEDIATE lv_sentencia
      using out lt_cuenta, out lt_saldo, out lt_edadMora, out lt_tipoCart;
  
    FOR a IN lt_cuenta.first .. lt_cuenta.last LOOP
    
      lv_cuenta   := lt_cuenta(a);
      lv_saldo    := lt_saldo(a);
      lv_edadMora := lt_edadMora(a);
      lv_tipoCart := lt_tipoCart(a);
    
      --Valida novedad 
      IF to_number(lv_saldo) > 0 AND lv_tipoCart = '0' THEN
        lv_novedad := '12'; --Cartera reclasIFicada 
      ELSE
        IF to_number(lv_saldo) > 0 AND lv_tipoCart = '1' THEN
          lv_novedad := '13'; --Cartera Castigada 
        END IF;
      END IF;
    
      --La edad de mora se calIFica en dias si es mayor a 12 meses se pone como 360 
      IF to_number(lv_edadMora) >= 12 THEN
        lv_edadMora := '360';
      ELSE
        lv_edadMora := to_char(to_number(lv_edadMora) * 30);
      END IF;
    
      --CalIFico la cuenta -- PASAR COMO COLUMNA DEL SELECT
      LV_CALIFICACION := COF_CALIFICA_CUENTA(lv_edadMora);
    
      -- N�mero de cuentas en mora de BSCS      
      OPEN gc_numero_moras(lv_cuenta, gv_ohstatus);
      FETCH gc_numero_moras
        INTO lgc_numero_moras;
      lb_found := gc_numero_moras%FOUND;
      CLOSE gc_numero_moras;
      IF lb_found THEN
        ln_nu_moras := lgc_numero_moras.Nu_Moras; --Autorizado por el cliente 
        lv_ruc      := lgc_numero_moras.IdentIFicacion; -- IdentIFicaci�n del cliente en BSCS 
      END IF;
    
      --Encuentro el tipo de persona 
      lv_reclamo := COF_RECLAMO(lv_cuenta);
    
      ---Valido tipo persona 
      begin
        ln_ruc := TO_NUMBER(lv_ruc);
      
      exception
        when INVALID_NUMBER then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => lv_error);
        when others then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => lv_error);
      end;
    
      --Valido la IdentIFicaci�n de acuerdo al n�mero de caracteres 
      IF (length(lv_ruc)) = 10 THEN
        lv_tipo_iden    := 'CED';
        lv_tipo_persona := 'N';
      ELSE
        IF (length(lv_ruc)) = 13 THEN
          lv_tipo_iden    := 'RUC';
          lv_tipo_persona := 'J';
        ELSE
          lv_tipo_iden    := 'PAS';
          lv_tipo_persona := 'N';
        END IF;
      END IF;
    
      --Para los clientes que cayeron en mora pero la cartera fue recuperada 
      IF to_number(nvl(lv_saldo, 0)) < 0.50 THEN
        lv_novedad      := '14'; --Cartera recuperada 
        lv_calIFicacion := 'A'; --Bajo riesgo 
        ln_nu_moras     := 0;
        lv_edadMora     := '0';
        lv_saldo        := '0'; --Saldo menor que 0.50 se hace 0 
      END IF;
    
      --Valido el adjetivo  en base a la novedad  
      IF lv_novedad = '08' or lv_novedad = '09' or lv_novedad = '12' or
         lv_novedad = '13' THEN
        lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90
      ELSE
        IF lv_novedad = '06' or lv_novedad = '07' THEN
          lv_adjetivo := '11'; --Linea desconectada 
        ELSE
          lv_adjetivo := '00';
        END IF;
      END IF;
    
      --Ingreso valores 
    
      cop_ingresa_valores_datacre(pv_nombre_tabla   => lv_nombre_tabla,
                                  pv_cuenta         => lv_cuenta,
                                  pv_novedad        => lv_novedad,
                                  pv_adjetivo       => lv_adjetivo,
                                  pv_calIFicacion   => lv_calIFicacion,
                                  pv_cuentas_mora   => ln_nu_moras,
                                  pv_edad_mora      => lv_edadMora,
                                  pv_reclamo        => lv_reclamo,
                                  pv_tipo_id        => lv_tipo_iden,
                                  pv_tipo_persona   => lv_tipo_persona,
                                  pv_identIFicacion => lv_ruc,
                                  pn_codigo_error   => ln_codigo_error,
                                  pv_error          => lv_error);
    
      IF ln_codigo_error = 0 THEN
        ln_reg_error := ln_reg_error + 1;
      
      ELSE
        ln_reg_exito := ln_reg_exito + 1;
      END IF;
    
      IF mod(ln_reg_exito, gn_commit) = 0 THEN
        commit;
      END IF;
    
    END LOOP;
  
    commit;
  
    -- habilito indices       
    lv_error := NULL;
    -- Habilito constraints
    COP_HABILITA_INDICE(lv_nombre_tabla, lv_error);
  
    IF lv_error IS NOT NULL THEN
      raise le_error;
    END IF;
    ---
  
    cop_bitacora_ejecucion(pv_nombretabla    => lv_nombre_tabla,
                           pn_reg_exito      => ln_reg_exito,
                           pn_reg_error      => ln_reg_error,
                           pv_descripcion    => lv_error,
                           pv_estado_reporte => 'N',
                           pv_tipo_proceso   => 'P',
                           pv_consulta       => NULL,
                           pv_error          => lv_error,
                           pv_tablaOrigen    => lv_tabla_origen);
  
  EXCEPTION
    WHEN le_error THEN
      lv_error := lv_error || ' ' || lv_programa || sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => lv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    WHEN OTHERS THEN
      lv_error := 'Error general ' || lv_programa || ': ' || sqlerrm;
    
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => lv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    
  END COP_CALIFICA_CLIENTES_GYE;

  PROCEDURE COP_CALIFICA_CLIENTES_UIO IS
  
    pdfechcierreperiodo date := sysdate;
    lv_fecha            varchar2(8) := to_char(pdfechcierreperiodo,
                                               'ddMMyyyy');
    lv_nombre_tabla     varchar2(60) := 'CO_REPDATCRE_UIO';
    /*lv_tabla_origen                 varchar2(60):= 'CART_CUENTAS_DAT'||'@B_BUROCRED';  */ -- 5628 CLS CJA 19/01/2012 SE COMENTA LA VARIABLE lv_tabla_origen
    --INI 5628 CLS CJA 19/01/2012 Se cambia el dblink de @B_BUROCRED a @FINGYE_BUROCRED
    lv_tabla_origen varchar2(60) := 'CART_CUENTAS_DAT' ||
                                    '@FINGYE_BUROCRED';
    --FIN 5628 CLS CJA 19/01/2012
    lv_novedad      varchar2(2);
    lv_adjetivo     varchar2(2);
    lv_calIFicacion varchar2(1);
    lv_tipo_iden    varchar2(3);
    lv_tipo_persona varchar2(1);
    lv_reclamo      varchar2(1);
    ln_reg_error    number := 0;
    ln_reg_exito    number := 0;
    ln_nu_moras     number;
    lv_ruc          varchar2(20);
  
    lgc_numero_moras gc_numero_moras%rowtype;
    --gr_existe_tabla                 cg_existe_tabla%rowtype;
    --lb_notfound                     boolean;
    lb_found        boolean;
    ln_codigo_error number;
    ln_ruc          number;
    lv_error        varchar2(1000);
    le_error EXCEPTION;
    lv_programa  varchar2(60) := 'COP_CALIFICA_CLIENTES_INVEN ';
    lv_sentencia varchar(1000) := null;
    --li_CURSOR                       integer:=0;
    --li_CURSOR1                      integer:=0;
    --lv_truncate                     varchar2(100);
  
    ----jose palma
    /*   v_cuenta                        varchar2(100);
    v_nu_moras                      varchar2(100);*/
  
    lv_cuenta   varchar2(20) := null;
    lv_saldo    varchar2(10) := null;
    lv_edadMora varchar2(10) := null;
    lv_tipoCart varchar2(10) := null;
    lt_tipoCart cot_string := cot_string();
    lt_cuenta   cot_string := cot_string();
    lt_saldo    cot_string := cot_string();
    lt_edadMora cot_number := cot_number();
  
    ------jose palma 
  BEGIN
  
    lv_nombre_tabla := lv_nombre_tabla || '_' || lv_fecha;
  
    COP_EXISTE_TABLA(lv_nombre_tabla, lv_error);
  
    IF lv_error IS NOT NULL THEN
      raise le_error;
    END IF;
  
    lv_sentencia := 'BEGIN SELECT  cuen_nume_cuen Cuenta,' ||
                    ' cuen_sald      Saldo, ' ||
                    ' cuen_edad_mora Edad_mora,' ||
                    ' cuen_tipo_cart Tipo_cart ' ||
                    'bulk collect INTO :1, :2, :3, :4 ' || ' FROM ' ||
                    lv_tabla_origen || ', CUSTOMER_ALL b ' ||
                    ' WHERE cuen_nume_cuen= b.custcode' || ';' || 'END;';
  
    EXECUTE IMMEDIATE lv_sentencia
      using out lt_cuenta, out lt_saldo, out lt_edadMora, out lt_tipoCart;
  
    FOR a IN lt_cuenta.first .. lt_cuenta.last LOOP
    
      lv_cuenta   := lt_cuenta(a);
      lv_saldo    := lt_saldo(a);
      lv_edadMora := lt_edadMora(a);
      lv_tipoCart := lt_tipoCart(a);
    
      --Valida novedad 
      IF to_number(lv_saldo) > 0 AND lv_tipoCart = '0' THEN
        lv_novedad := '12'; --Cartera reclasIFicada 
      ELSE
        IF to_number(lv_saldo) > 0 AND lv_tipoCart = '1' THEN
          lv_novedad := '13'; --Cartera Castigada 
        END IF;
      END IF;
    
      --La edad de mora se calIFica en dias si es mayor a 12 meses se pone como 360 
      IF to_number(lv_edadMora) >= 12 THEN
        lv_edadMora := '360';
      ELSE
        lv_edadMora := to_char(to_number(lv_edadMora) * 30);
      END IF;
    
      --CalIFico la cuenta              
      LV_CALIFICACION := COF_CALIFICA_CUENTA(lv_edadMora);
    
      -- N�mero de cuentas en mora de BSCS      
      OPEN gc_numero_moras(lv_cuenta, gv_ohstatus);
      FETCH gc_numero_moras
        INTO lgc_numero_moras;
      lb_found := gc_numero_moras%FOUND;
      CLOSE gc_numero_moras;
      IF lb_found THEN
        ln_nu_moras := lgc_numero_moras.Nu_Moras; --Autorizado por el cliente 
        lv_ruc      := lgc_numero_moras.IdentIFicacion; -- IdentIFicaci�n del cliente en BSCS 
      END IF;
    
      --Encuentro el tipo de persona 
      lv_reclamo := COF_RECLAMO(lv_cuenta);
    
      ---Valido tipo persona 
      begin
        ln_ruc := TO_NUMBER(lv_ruc);
      exception
        when INVALID_NUMBER then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => lv_error);
        when others then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => lv_error);
      end;
    
      --Valido la IdentIFicaci�n de acuerdo al n�mero de caracteres 
      IF (length(lv_ruc)) = 10 THEN
        lv_tipo_iden    := 'CED';
        lv_tipo_persona := 'N';
      ELSE
        IF (length(lv_ruc)) = 13 THEN
          lv_tipo_iden    := 'RUC';
          lv_tipo_persona := 'J';
        ELSE
          lv_tipo_iden    := 'PAS';
          lv_tipo_persona := 'N';
        END IF;
      END IF;
    
      --Para los clientes que cayeron en mora pero la cartera fue recuperada 
      IF to_number(nvl(lv_saldo, 0)) < 0.50 THEN
        lv_novedad      := '14'; --Cartera recuperada 
        lv_calIFicacion := 'A'; --Bajo riesgo 
        ln_nu_moras     := 0;
        lv_edadMora     := '0';
        lv_saldo        := '0'; --Saldo menor que 0.50 se hace 0 
      END IF;
    
      --Valido el adjetivo  en base a la novedad  
      IF lv_novedad = '08' or lv_novedad = '09' or lv_novedad = '12' or
         lv_novedad = '13' THEN
        lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90
      ELSE
        IF lv_novedad = '06' or lv_novedad = '07' THEN
          lv_adjetivo := '11'; --Linea desconectada 
        ELSE
          lv_adjetivo := '00';
        END IF;
      END IF;
    
      cop_ingresa_valores_datacre(pv_nombre_tabla   => lv_nombre_tabla,
                                  pv_cuenta         => lv_cuenta,
                                  pv_novedad        => lv_novedad,
                                  pv_adjetivo       => lv_adjetivo,
                                  pv_calIFicacion   => lv_calIFicacion,
                                  pv_cuentas_mora   => ln_nu_moras,
                                  pv_edad_mora      => lv_edadMora,
                                  pv_reclamo        => lv_reclamo,
                                  pv_tipo_id        => lv_tipo_iden,
                                  pv_tipo_persona   => lv_tipo_persona,
                                  pv_identIFicacion => lv_ruc,
                                  pn_codigo_error   => ln_codigo_error,
                                  pv_error          => lv_error);
    
      IF ln_codigo_error = 0 THEN
        ln_reg_error := ln_reg_error + 1;
      
      ELSE
        ln_reg_exito := ln_reg_exito + 1;
      END IF;
    
      IF mod(ln_reg_exito, gn_commit) = 0 THEN
        commit;
      END IF;
    
    END loop;
  
    commit;
    -- hBAILITAR INDICE
    lv_error := null;
    COP_HABILITA_INDICE(lv_nombre_tabla, lv_error);
  
    IF lv_error IS NOT NULL THEN
      raise le_error;
    END IF;
  
    cop_bitacora_ejecucion(pv_nombretabla    => lv_nombre_tabla,
                           pn_reg_exito      => ln_reg_exito,
                           pn_reg_error      => ln_reg_error,
                           pv_descripcion    => lv_error,
                           pv_estado_reporte => 'N',
                           pv_tipo_proceso   => 'P',
                           pv_consulta       => NULL,
                           pv_error          => lv_error,
                           pv_tablaOrigen    => lv_tabla_origen);
  
  EXCEPTION
    WHEN le_error THEN
      lv_error := lv_error || ' ' || lv_programa || sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => lv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    WHEN OTHERS THEN
      lv_error := 'Error general ' || lv_programa || ': ' || sqlerrm;
    
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => lv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    
  END COP_CALIFICA_CLIENTES_UIO;

  PROCEDURE COP_GENERA_ARCHIVO IS
  
    file_hANDle1  SYS.UTL_FILE.FILE_TYPE;
    path          varchar2(50) := '/home/gsioper/procesos/datacredito';
    lv_fecha      varchar2(10) := to_char(sysdate, 'mmyyyy');
    pv_nomb_arch1 varchar2(100) := 'rep_datacre.dat'; --SE MODIFICA NOMBRE PARA QUE NO INCLUYA FECHA
    lv_MensErr    varchar2(1000);
    le_error EXCEPTION;
  
    lv_sentencia     VARCHAR2(20000) := NULL;
    lv_cuentaReg     varchar2(20000) := null;
    lv_generaArchivo varchar2(20000) := null;
    lv_totalReg      varchar2(10);
    lv_totalNov      varchar2(10);
    li_CURSOR        integer;
    li_CURSOR1       integer;
    lv_datos         varchar2(1000);
    lv_error         varchar2(1000);
    wline            varchar2(2000);
    ln_reg_exito     number;
    ln_reg_error     number;
  
  BEGIN
  
    COP_UNION_CLIENTES(lv_error);
    IF lv_error is not null THEN
      raise le_error;
    END IF;
  
    lv_sentencia := 'SELECT datos_registro , novedad  FROM CO_UNION_CLIENTES';
  
    lv_cuentaReg := 'SELECT  count (*) Total_Reg, sum(to_number(novedad)) Total_novedad FROM(' ||
                    lv_sentencia || ')';
  
    --CUENTO LOS ARCHIVOS Y LAS NOVEDADES 
    BEGIN
      li_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(li_CURSOR, lv_cuentaReg, dbms_sql.v7);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 1, lv_totalReg, 10);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 2, lv_totalNov, 10);
      li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);
      loop
        exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 1, lv_totalReg);
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 2, lv_totalNov);
      END loop;
    
      DBMS_SQL.CLOSE_CURSOR(li_CURSOR);
    
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_SQL.CLOSE_CURSOR(li_CURSOR);
        lv_error := 'Error al contar registros en el archivo ' || sqlerrm;
        RAISE le_error;
      
    END;
    ---
    lv_totalReg := LPAD(to_char(to_number(lv_totalReg) + 2), 8, 0);
    lv_totalNov := LPAD(lv_totalNov, 7, 0);
    li_CURSOR   := 0;
    li_CURSOR1  := 0;
  
    --Consulta que genera el archivo 
    lv_generaArchivo := ' SELECT rpad((''00000000000000000023000423''||(to_char(sysdate,''yyyymm'')-1 ||''MM'')),600,''0'') as REGISTRO, ''00'' novedad FROM dual union all  ' ||
                        lv_sentencia || ' ' ||
                        'union all SELECT rpad(''ZZZZZZZZZZZZZZZZZZ''||to_char(sysdate,''yyyymmdd'')||''' ||
                        lv_totalReg || lv_totalNov ||
                        ''' ,600,''0'') as REGISTRO,''00'' novedad FROM dual ';
  
    li_CURSOR := DBMS_SQL.OPEN_CURSOR;
    DBMS_SQL.PARSE(li_CURSOR, lv_generaArchivo, dbms_sql.v7);
    DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 1, lv_datos, 1000);
    li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);
  
    --Abro el archivo 
    file_hANDle1 := SYS.UTL_FILE.FOPEN(path, pv_nomb_arch1, 'W');
  
    IF not SYS.UTL_FILE.IS_OPEN(file_hANDle1) THEN
      lv_error := 'Hubo error al abrir el archivo ' || sqlerrm;
      RAISE le_error;
    END IF;
  
    loop
      exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
    
      DBMS_SQL.COLUMN_VALUE(li_CURSOR, 1, wline);
      BEGIN
        SELECT replace(replace(replace(replace(replace(replace(upper(wline),
                                                               '�',
                                                               'A'),
                                                       '�',
                                                       'E'),
                                               '�',
                                               'I'),
                                       '�',
                                       'O'),
                               '�',
                               'U'),
                       '�',
                       'N')
          INTO wline
          FROM dual;
      
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := sqlerrm;
      END;
    
      --Escribo en el archivo y cuento lo que se generen con �xito o error 
      BEGIN
        SYS.UTL_FILE.PUT_LINE(file_hANDle1, wline);
        ln_reg_exito := ln_reg_exito + 1;
      EXCEPTION
        WHEN OTHERS THEN
          ln_reg_error := ln_reg_error + 1;
          lv_error     := sqlerrm;
      END;
    
    END loop;
  
    SYS.UTL_FILE.FCLOSE(file_hANDle1);
  
    --Ingreso a la bitacora que el reporte se genero con �xito 
    cop_bitacora_ejecucion(pv_nombreTabla    => null,
                           pn_reg_exito      => lv_totalReg,
                           pn_reg_error      => ln_reg_error,
                           pv_descripcion    => lv_error,
                           pv_estado_reporte => 'S', --estado generado con �xito 
                           pv_tipo_proceso   => 'R', --Indicador de reporte 
                           pv_consulta       => lv_generaArchivo,
                           pv_error          => lv_error);
  
    --Actualizo estados 
  
  EXCEPTION
    WHEN le_error THEN
      cop_bitacora_ejecucion(pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => 'Error al generar reporte :' ||
                                                  lv_error,
                             pv_estado_reporte => 'E', --Generado con error 
                             pv_tipo_proceso   => 'R',
                             pv_consulta       => lv_generaArchivo,
                             pv_error          => lv_error);
    
    WHEN OTHERS THEN
      IF SYS.UTL_FILE.IS_OPEN(file_hANDle1) THEN
        SYS.UTL_FILE.FCLOSE(file_hANDle1);
      END IF;
      lv_error := sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => 'Error al generar reporte :' ||
                                                  lv_error,
                             pv_estado_reporte => 'E', --Generado con error 
                             pv_tipo_proceso   => 'R',
                             pv_consulta       => lv_generaArchivo,
                             pv_error          => lv_error);
    
  END COP_GENERA_ARCHIVO;

  PROCEDURE COP_ACTUALIZA_ESTADOS_BITACORA(pn_codigo in number,
                                           pv_estado in varchar2,
                                           pv_error  out varchar2) IS
  
  BEGIN
  
    update CO_BITACORA_REPDATACRE
       set estado_reporte = pv_estado
     WHERE cod_bitacora_repdatacre = pn_codigo;
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := sqlerrm;
      rollback;
  END COP_ACTUALIZA_ESTADOS_BITACORA;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha ModIF.   : 19/11/2007     
     Solicitado por : SIS Guillermo Proa�o
     ModIFicado por : Anl. Eloy Carchi Rojas
     Motivo         : No debe haber c�digo quemado.
  =========================================================*/

  PROCEDURE COP_UNION_CLIENTES(pv_error out varchar2) IS
  
    lv_fecha   varchar2(10) := to_char(sysdate, 'mmyyyy');
    lv_MensErr varchar2(1000);
    le_error EXCEPTION;
  
    CURSOR c_nombre_tabla(pc_tabla varchar2) is
      SELECT cod_bitacora_repdatacre, tabla_origen, tabla_destino
        FROM CO_BITACORA_REPDATACRE
       WHERE cod_bitacora_repdatacre =
             (SELECT max(cod_bitacora_repdatacre)
                FROM CO_BITACORA_REPDATACRE BR
               WHERE br.estado_reporte in ('N', 'S')
                 AND br.fecha_ejecucion between sysdate - 30 AND sysdate
                 AND br.tabla_destino like '%' || pc_tabla || '%');
  
    lv_sentencia     VARCHAR2(20000) := NULL;
    lv_cuentaReg     varchar2(20000) := null;
    lv_generaArchivo varchar2(20000) := null;
    lv_totalReg      varchar2(10);
    lv_totalNov      varchar2(10);
    li_CURSOR        integer;
    li_CURSOR1       integer;
    lv_datos         varchar2(1000);
    lv_error         varchar2(1000);
    wline            varchar2(2000);
    lv_novedad       varchar2(10);
    lv_cuenta        varchar2(20);
    ln_reg_exito     number := 0;
    ln_reg_error     number := 0;
  
    --[2702] ECA
    /*lv_parametro_08  varchar2(50):='co_repdatcre_08';
    lv_parametro_24  varchar2(50):='co_repdatcre_24';*/
    lv_parametro_ciclo VARCHAR2(50);
  
    lv_parametro_gye varchar2(50) := 'co_repdatcre_gye';
    lv_parametro_uio varchar2(50) := 'co_repdatcre_uio';
  
    --[2702] ECA
    /*lv_repcarcli_08  varchar2(50);
    lv_repdacre_08   varchar2(50);
    lv_repcarcli_24  varchar2(50);
    lv_repdacre_24  varchar2(50);*/
    lv_repcarcli_ciclo VARCHAR2(50);
    lv_repdacre_ciclo  varchar2(50);
  
    lv_inven_gye varchar2(50);
    lv_inven_uio varchar2(50);
    lb_found     boolean;
  
    --[2702] ECA
    /*ln_cod08     number;
    ln_cod24     number;*/
    ln_cod_ciclo NUMBER;
    ln_codgye    number;
    ln_coduio    number;
  
    lc_nombre_tabla c_nombre_tabla%rowtype;
  
    CURSOR C_ProcesaCiclos IS
      SELECT * FROM FA_CICLOS_BSCS ORDER BY ID_CICLO ASC;
  
  BEGIN
    lv_sentencia := 'truncate table co_union_clientes';
    EJECUTA_SENTENCIA(lv_sentencia, lv_MensErr);
    IF lv_MensErr is not null THEN
      raise le_error;
    END IF;
  
    --[2702] INI ECA
    --Ciclo para procesar todos los ciclos existentes 
    FOR c IN C_ProcesaCiclos LOOP
      --Seteo variables
      lv_parametro_ciclo := 'co_repdatcre_' || c.dia_ini_ciclo;
      ln_reg_exito       := 0;
      ln_reg_error       := 0;
      --Encuentro los nombre de tablas
      OPEN c_nombre_tabla(lv_parametro_ciclo);
      FETCH c_nombre_tabla
        INTO lc_nombre_tabla;
      lb_found := c_nombre_tabla%FOUND;
      CLOSE c_nombre_tabla;
    
      IF lb_found THEN
        lv_repcarcli_ciclo := lc_nombre_tabla.tabla_origen;
        lv_repdacre_ciclo  := lc_nombre_tabla.tabla_destino;
        ln_cod_ciclo       := lc_nombre_tabla.cod_bitacora_repdatacre;
      END IF;
      lb_found := false;
    
      BEGIN
        lv_sentencia := '
                                    SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '') ||
                                     lpad(b.identIFicacion,13,''0'')||
                                     rpad(decode(instr(apellidos,'' ''),0,apellidos,NVL(substr(apellidos,1,instr(apellidos,'' '')),'' '')),15,'' '' )||
                                     rpad(decode(instr(apellidos,'' ''),0,'' '',NVL(substr(apellidos,instr(apellidos,'' '')+1,length(apellidos)),'' '')),15,'' '' )||
                                     rpad(decode(instr(nombres,'' ''),0,nombres,NVL(substr(nombres,1,instr(nombres,'' '')),'' '')),15,'' '' )||
                                     rpad(decode(instr(nombres,'' ''),0,'' '',NVL(substr(nombres,instr(nombres,'' '')+1,length(nombres)),'' '')),15,'' '' )||
                                     ''000000''||
                                     to_char(csactivated,''yyyymm'')||
                                     rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                                     lpad(round(decode(sign(balance_12),-1,0,balance_12)),10,0) ||
                                     b.novedad ||
                                     b.adjetivo||
                                     decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                                     ''0000000000'' ||
                                     lpad(round(decode(sign(a.totaladeuda),-1,0,a.totaladeuda)),10,0) ||
                                     lpad(round(a.totalvencida),10,0) ||
                                     ''0000000000''  ||
                                     ''142''         ||
                                     b.calIFicacion||
                                     rpad(nvl(a.canton,'' ''),20,'' '') ||
                                     rpad(nvl(a.direccion2,'' ''),120,'' '') ||
                                     ''0000000000000000000000000000000000000000000000000000000000000000000000'' ||
                                      rpad(nvl(a.direccion,'' ''),120,'' '')||
                                      ''000000000'' ||  --4.26   --4.27
                                      ''000''|| --4.28
                                      lpad(b.cuentas_mora,3,''0'')|| --4.29
                                      ''00000000000000000000000000000000000000000000'' ||
                                      ''1''|| --4.34
                                     lpad(b.edad_mora,3,0)|| --4.35
                                     ''00000000''||
                                     decode( b.reclamo,''S'',0,9)||
                                     ''000000000000000000000000000000'' as REGISTRO,
                                      b.novedad as novedad,
                                      a.cuenta as cuenta
                                     
                              FROM 
                              customer_all          c,
                              ' ||
                        lv_repcarcli_ciclo ||
                        ' a, 
                              ' || lv_repdacre_ciclo || ' b
                              WHERE a.cuenta = b.cuenta 
                              AND   b.cuenta= c.custcode ';
      
        li_CURSOR := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE(li_CURSOR, lv_sentencia, dbms_sql.v7);
        DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 1, lv_datos, 1000);
        DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 2, lv_novedad, 10);
        DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 3, lv_cuenta, 20);
        li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);
      
        loop
          exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
        
          DBMS_SQL.COLUMN_VALUE(li_CURSOR, 1, lv_datos);
          DBMS_SQL.COLUMN_VALUE(li_CURSOR, 2, lv_novedad);
          DBMS_SQL.COLUMN_VALUE(li_CURSOR, 3, lv_cuenta);
        
          BEGIN
            SELECT replace(replace(replace(replace(replace(replace(upper(lv_datos),
                                                                   '�',
                                                                   'A'),
                                                           '�',
                                                           'E'),
                                                   '�',
                                                   'I'),
                                           '�',
                                           'O'),
                                   '�',
                                   'U'),
                           '�',
                           'N')
              INTO lv_datos
              FROM dual;
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := sqlerrm;
          END;
        
          --Escribo en el archivo y cuento lo que se generen con �xito o error 
          BEGIN
            INSERT INTO CO_UNION_CLIENTES
              (datos_registro, novedad, CUENTA)
            VALUES
              (LV_DATOS, LV_NOVEDAD, lv_cuenta);
            ln_reg_exito := ln_reg_exito + 1;
          EXCEPTION
            WHEN OTHERS THEN
              ln_reg_error := ln_reg_error + 1;
              lv_error     := substr(sqlerrm, 1, 500);
              cop_detalle_bitacora(pv_cuenta      => lv_cuenta,
                                   pv_descripcion => lv_error);
          END;
        
          IF mod(ln_reg_exito, gn_commit) = 0 THEN
            commit;
          END IF;
        END loop;
      
        cop_bitacora_ejecucion(Pv_nombreTabla    => null,
                               pn_reg_exito      => ln_reg_exito,
                               pn_reg_error      => ln_reg_error,
                               pv_descripcion    => lv_error,
                               pv_estado_reporte => 'S',
                               pv_tipo_proceso   => 'U' ||
                                                    to_number(c.id_ciclo),
                               pv_consulta       => lv_sentencia,
                               pv_error          => lv_error);
      
      EXCEPTION
      
        WHEN OTHERS THEN
          lv_error := sqlerrm;
          cop_bitacora_ejecucion(pv_nombreTabla    => null,
                                 pn_reg_exito      => ln_reg_exito,
                                 pn_reg_error      => ln_reg_error,
                                 pv_descripcion    => 'Error al generar datos para corte del ' ||
                                                      c.dia_ini_ciclo || ' :' ||
                                                      lv_error,
                                 pv_estado_reporte => 'E', --Generado con error 
                                 pv_tipo_proceso   => 'U' ||
                                                      to_number(c.id_ciclo),
                                 pv_consulta       => lv_sentencia,
                                 pv_error          => lv_error);
        
      END;
      IF ln_cod_ciclo is not null THEN
        cop_actualiza_estados_bitacora(pn_codigo => ln_cod_ciclo,
                                       pv_estado => 'S',
                                       pv_error  => lv_error);
      END IF;
    END LOOP;
    --[2702] FIN ECA
  
    --Corte 08
    /*OPEN c_nombre_tabla(lv_parametro_08);
    FETCH c_nombre_tabla INTO lc_nombre_tabla;
          lb_found := c_nombre_tabla%FOUND;    
    CLOSE c_nombre_tabla;
    
    IF lb_found THEN 
    
       lv_repcarcli_08:=lc_nombre_tabla.tabla_origen;
       lv_repdacre_08:= lc_nombre_tabla.tabla_destino;
       ln_cod08:=lc_nombre_tabla.cod_bitacora_repdatacre;
       
    END IF;
    lb_found:=false;*/
  
    --Corte 24 
    /*OPEN c_nombre_tabla(lv_parametro_24);
    FETCH c_nombre_tabla INTO lc_nombre_tabla;
          lb_found := c_nombre_tabla%FOUND;    
    CLOSE c_nombre_tabla;
    
    IF lb_found THEN 
       
       lv_repcarcli_24:=lc_nombre_tabla.tabla_origen;
       lv_repdacre_24:= lc_nombre_tabla.tabla_destino;
       ln_cod24:=lc_nombre_tabla.cod_bitacora_repdatacre;
       
    END IF;
    lb_found:=false;*/
  
    --Inventario GYE   
    OPEN c_nombre_tabla(lv_parametro_gye);
    FETCH c_nombre_tabla
      INTO Lc_nombre_tabla;
    lb_found := c_nombre_tabla%FOUND;
    CLOSE c_nombre_tabla;
  
    IF lb_found THEN
      lv_inven_gye := lc_nombre_tabla.tabla_destino;
      ln_codgye    := lc_nombre_tabla.cod_bitacora_repdatacre;
    
    END IF;
    lb_found := false;
  
    --Inventario UIO 
    OPEN c_nombre_tabla(lv_parametro_uio);
    FETCH c_nombre_tabla
      INTO Lc_nombre_tabla;
    lb_found := c_nombre_tabla%FOUND;
    CLOSE c_nombre_tabla;
  
    IF lb_found THEN
      lv_inven_uio := lc_nombre_tabla.tabla_destino;
      ln_coduio    := lc_nombre_tabla.cod_bitacora_repdatacre;
    END IF;
  
    /*BEGIN                
           lv_sentencia:='
                              SELECT \*+ rule +*\ rpad(b.cuenta,18,'' '') ||
                               lpad(b.identIFicacion,13,''0'')||
                               rpad(decode(instr(apellidos,'' ''),0,apellidos,NVL(substr(apellidos,1,instr(apellidos,'' '')),'' '')),15,'' '' )||
                               rpad(decode(instr(apellidos,'' ''),0,'' '',NVL(substr(apellidos,instr(apellidos,'' '')+1,length(apellidos)),'' '')),15,'' '' )||
                               rpad(decode(instr(nombres,'' ''),0,nombres,NVL(substr(nombres,1,instr(nombres,'' '')),'' '')),15,'' '' )||
                               rpad(decode(instr(nombres,'' ''),0,'' '',NVL(substr(nombres,instr(nombres,'' '')+1,length(nombres)),'' '')),15,'' '' )||
                               ''000000''||
                               to_char(csactivated,''yyyymm'')||
                               rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                               lpad(round(decode(sign(balance_12),-1,0,balance_12)),10,0) ||
                               b.novedad ||
                               b.adjetivo||
                               decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                               ''0000000000'' ||
                               lpad(round(decode(sign(a.totaladeuda),-1,0,a.totaladeuda)),10,0) ||
                               lpad(round(a.totalvencida),10,0) ||
                               ''0000000000''  ||
                               ''142''         ||
                               b.calIFicacion||
                               rpad(nvl(a.canton,'' ''),20,'' '') ||
                               rpad(nvl(a.direccion2,'' ''),120,'' '') ||
                               ''0000000000000000000000000000000000000000000000000000000000000000000000'' ||
                                rpad(nvl(a.direccion,'' ''),120,'' '')||
                                ''000000000'' ||  --4.26   --4.27
                                ''000''|| --4.28
                                lpad(b.cuentas_mora,3,''0'')|| --4.29
                                ''00000000000000000000000000000000000000000000'' ||
                                ''1''|| --4.34
                               lpad(b.edad_mora,3,0)|| --4.35
                               ''00000000''||
                               decode( b.reclamo,''S'',0,9)||
                               ''000000000000000000000000000000'' as REGISTRO,
                                b.novedad as novedad,
                                a.cuenta as cuenta
                               
                        FROM 
                        customer_all          c,
                        '|| lv_repcarcli_08 ||' a, 
                        '||lv_repdacre_08||' b
                        WHERE a.cuenta = b.cuenta 
                        AND   b.cuenta= c.custcode ';
                                  
                        
    
       
       li_CURSOR := DBMS_SQL.OPEN_CURSOR;
       DBMS_SQL.PARSE(li_CURSOR, lv_sentencia, dbms_sql.v7);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,1,lv_datos,1000);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,2,lv_novedad,10);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,3,lv_cuenta,20);
       li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);  
      
        loop
        exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
        
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,1,lv_datos);
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,2,lv_novedad);
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,3,lv_cuenta);
    
                BEGIN 
                    SELECT 
                    replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N') 
                    INTO lv_datos
                    FROM  dual; 
          
                 EXCEPTION 
                           WHEN OTHERS THEN 
                              lv_error:=sqlerrm;
                 END;
              
                --Escribo en el archivo y cuento lo que se generen con �xito o error 
                BEGIN
                         INSERT INTO CO_UNION_CLIENTES (datos_registro,novedad,CUENTA) VALUES (LV_DATOS,LV_NOVEDAD,lv_cuenta);
                         ln_reg_exito := ln_reg_exito + 1;
                  EXCEPTION
                      WHEN OTHERS THEN
                         ln_reg_error:=ln_reg_error +1;  
                         lv_error:=substr(sqlerrm,1,500);
                         cop_detalle_bitacora(pv_cuenta => lv_cuenta,
                                              pv_descripcion => lv_error);
                 END; 
    
                 IF  mod (ln_reg_exito,gn_commit)= 0 THEN 
                     commit;
                 END IF;    
         END loop;
         
          cop_bitacora_ejecucion(    Pv_nombreTabla => null,
                                                             pn_reg_exito =>ln_reg_exito ,
                                                             pn_reg_error => ln_reg_error,
                                                             pv_descripcion => lv_error,
                                                             pv_estado_reporte => 'S' ,
                                                             pv_tipo_proceso => 'U1' ,
                                                             pv_consulta => lv_sentencia,
                                                             pv_error => lv_error
                                                             );  
           
    
    EXCEPTION 
      
    
       WHEN OTHERS THEN 
            lv_error:=sqlerrm; 
            cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                       pn_reg_exito =>ln_reg_exito ,
                                                       pn_reg_error => ln_reg_error,
                                                       pv_descripcion => 'Error al generar datos para corte del 08 :'||lv_error,
                                                       pv_estado_reporte => 'E' ,--Generado con error 
                                                       pv_tipo_proceso => 'U1' ,
                                                       pv_consulta => lv_sentencia,
                                                       pv_error => lv_error);  
         
     END;*/
    ----------------------------------
  
    /*ln_reg_exito:=0;
    ln_reg_error:=0;
    
    BEGIN 
       lv_sentencia:=' 
                             SELECT \*+ rule +*\ rpad(b.cuenta,18,'' '') ||
                               lpad(b.identIFicacion,13,''0'')||
                               rpad(decode(instr(apellidos,'' ''),0,apellidos,NVL(substr(apellidos,1,instr(apellidos,'' '')),'' '')),15,'' '' )||
                               rpad(decode(instr(apellidos,'' ''),0,'' '',NVL(substr(apellidos,instr(apellidos,'' '')+1,length(apellidos)),'' '')),15,'' '' )||
                               rpad(decode(instr(nombres,'' ''),0,nombres,NVL(substr(nombres,1,instr(nombres,'' '')),'' '')),15,'' '' )||
                               rpad(decode(instr(nombres,'' ''),0,'' '',NVL(substr(nombres,instr(nombres,'' '')+1,length(nombres)),'' '')),15,'' '' )||
                               ''000000''||
                               to_char(csactivated,''yyyymm'')||
                               rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                              lpad(round(decode(sign(balance_12),-1,0,balance_12)),10,0) ||
                               b.novedad ||
                               b.adjetivo||
                               decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                               ''0000000000'' ||
                               lpad(round(decode(sign(a.totaladeuda),-1,0,a.totaladeuda)),10,0) ||
                               lpad(round(a.totalvencida),10,0)||
                               ''0000000000''  ||
                               ''142''         ||
                               b.calIFicacion||
                               rpad(nvl(a.canton,'' ''),20,'' '') ||
                               rpad(nvl(a.direccion2,'' ''),120,'' '') ||
                               ''0000000000000000000000000000000000000000000000000000000000000000000000'' ||
                                rpad(nvl(a.direccion,'' ''),120,'' '')||
                                ''000000000'' ||
                                ''000''||
                                lpad(b.cuentas_mora,3,''0'')||
                                ''00000000000000000000000000000000000000000000'' ||
                                ''1''|| --4.34
                               lpad(b.edad_mora,3,0)||
                               ''00000000''||
                               decode( b.reclamo,''S'',0,9)||
                               ''000000000000000000000000000000'' as REGISTRO,
                                b.novedad as novedad,
                                a.cuenta as cuenta
                        FROM 
                        customer_all          c,
                         '|| lv_repcarcli_24 ||' a, 
                        '||  lv_repdacre_24||' b
                        WHERE a.cuenta = b.cuenta 
                        AND   b.cuenta= c.custcode';
                                  
                        
    
          
       li_CURSOR := DBMS_SQL.OPEN_CURSOR;
       DBMS_SQL.PARSE(li_CURSOR, lv_sentencia, dbms_sql.v7);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,1,lv_datos,1000);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,2,lv_novedad,10);
       DBMS_SQL.DEFINE_COLUMN(li_CURSOR,3,lv_cuenta,20);
       li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);  
      
        loop
        exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
        
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,1,lv_datos);
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,2,lv_novedad);
               DBMS_SQL.COLUMN_VALUE(li_CURSOR,3,lv_cuenta);
    
                BEGIN 
                    SELECT 
                    replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N') 
                    INTO lv_datos
                    FROM  dual; 
          
                 EXCEPTION 
                           WHEN OTHERS THEN 
                              lv_error:=sqlerrm;
                 END;
              
                --Escribo en el archivo y cuento lo que se generen con �xito o error 
                BEGIN
                         INSERT INTO CO_UNION_CLIENTES (datos_registro,novedad,CUENTA) VALUES (LV_DATOS,LV_NOVEDAD,lv_cuenta);
                         ln_reg_exito := ln_reg_exito + 1;
                  EXCEPTION
                      WHEN OTHERS THEN
                         ln_reg_error:=ln_reg_error +1;  
                         lv_error:=substr(sqlerrm,1,500);
                         cop_detalle_bitacora(pv_cuenta => lv_cuenta,
                                              pv_descripcion => lv_error);
                              
                 END; 
    
                   IF  mod (ln_reg_exito,gn_commit)= 0 THEN 
                     commit;
                 END IF;    
                 
         END loop;
          cop_bitacora_ejecucion(    Pv_nombreTabla => null,
                                                             pn_reg_exito =>ln_reg_exito ,
                                                             pn_reg_error => ln_reg_error,
                                                             pv_descripcion => lv_error,
                                                             pv_estado_reporte => 'S' ,
                                                             pv_tipo_proceso => 'U2' ,
                                                             pv_consulta => lv_sentencia,
                                                             pv_error => lv_error
                                                             );  
         
    
    EXCEPTION 
       WHEN OTHERS THEN 
            lv_error:=sqlerrm;
            cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                       pn_reg_exito =>ln_reg_exito ,
                                                       pn_reg_error => ln_reg_error,
                                                       pv_descripcion => 'Error al generar datos para corte del 24 :'||lv_error,
                                                       pv_estado_reporte => 'E' ,--Generado con error 
                                                       pv_tipo_proceso => 'U2' ,
                                                       pv_consulta => lv_sentencia,
                                                       pv_error => lv_error);  
         
     END;*/
  
    -----------------------------    
    ln_reg_exito := 0;
    ln_reg_error := 0;
  
    BEGIN
      lv_sentencia := ' 
                                               SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '')|| --Cuenta
                                                 lpad(b.identIFicacion,13,''0'')|| --Iden
                                                 rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )||
                                                 ''000000''||
                                                 to_char(csactivated,''yyyymm'')||
                                                 rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                                                 ''0000000000''||
                                                 b.novedad||
                                                 b.adjetivo||
                                                 decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                                                 ''0000000000'' ||
                                                 ''0000000000''|| --Saldo Deuda 
                                                 lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| --Saldo mora 
                                                 ''0000000000''||
                                                 ''142''         ||
                                                 b.calIFicacion||
                                                 rpad(nvl(d.cccity,'' ''),20,'' '')|| --Canton 
                                                 rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')||
                                                 ''0000000000000000000000000000000000000000000000000000000000000000000000''||
                                                  rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| --
                                                  ''000000000'' ||--4.26 -- 4.27 
                                                  ''000''|| --4.28
                                                  lpad(b.cuentas_mora,3,''0'')||
                                                  ''00000000000000000000000000000000000000000000''||
                                                  ''1''|| --4.34
                                                 lpad(b.edad_mora,3,0)||
                                                 ''00000000''||
                                                 decode(b.reclamo,''S'',0,9)||
                                                 ''000000000000000000000000000000'' as REGISTRO,
                                                  b.novedad as novedad,
                                                  b.cuenta as cuenta
                                              
                                          FROM 
                                          cart_cuentas_dat@FINGYE_BUROCRED a,
                                          sysadm.' ||
                      lv_inven_gye || ' b,
                                          customer_all c,
                                          ccontact_all d
                                          WHERE b.cuenta=a.cuen_nume_cuen
                                          AND   b.cuenta=c.custcode
                                          AND  c.customer_id=d.customer_id';
    
      li_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(li_CURSOR, lv_sentencia, dbms_sql.v7);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 1, lv_datos, 1000);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 2, lv_novedad, 10);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 3, lv_cuenta, 20);
      li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);
    
      loop
        exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
      
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 1, lv_datos);
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 2, lv_novedad);
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 3, lv_cuenta);
      
        BEGIN
          SELECT replace(replace(replace(replace(replace(replace(upper(lv_datos),
                                                                 '�',
                                                                 'A'),
                                                         '�',
                                                         'E'),
                                                 '�',
                                                 'I'),
                                         '�',
                                         'O'),
                                 '�',
                                 'U'),
                         '�',
                         'N')
            INTO lv_datos
            FROM dual;
        
        EXCEPTION
          WHEN OTHERS THEN
            lv_error := sqlerrm;
        END;
      
        --Escribo en el archivo y cuento lo que se generen con �xito o error 
        BEGIN
          INSERT INTO CO_UNION_CLIENTES
            (datos_registro, novedad, CUENTA)
          VALUES
            (LV_DATOS, LV_NOVEDAD, lv_cuenta);
          ln_reg_exito := ln_reg_exito + 1;
        
        EXCEPTION
          WHEN OTHERS THEN
            ln_reg_error := ln_reg_error + 1;
            lv_error     := substr(sqlerrm, 1, 500);
            cop_detalle_bitacora(pv_cuenta      => lv_cuenta,
                                 pv_descripcion => lv_error);
          
        END;
      
        IF mod(ln_reg_exito, gn_commit) = 0 THEN
          commit;
        END IF;
      
      END loop;
    
      cop_bitacora_ejecucion(Pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'S',
                             pv_tipo_proceso   => 'UG', --U3
                             pv_consulta       => lv_sentencia,
                             pv_error          => lv_error);
    
    EXCEPTION
      WHEN OTHERS THEN
      
        lv_error := sqlerrm;
        cop_bitacora_ejecucion(pv_nombreTabla    => null,
                               pn_reg_exito      => ln_reg_exito,
                               pn_reg_error      => ln_reg_error,
                               pv_descripcion    => 'Error al procesar datos de los clientes GYE :' ||
                                                    lv_error,
                               pv_estado_reporte => 'E', --Generado con error 
                               pv_tipo_proceso   => 'UG', --U3
                               pv_consulta       => lv_sentencia,
                               pv_error          => lv_error);
      
    END;
  
    ---------------------------
    ln_reg_exito := 0;
    ln_reg_error := 0;
  
    BEGIN
      /*lv_sentencia:=' 
          SELECT \*+ rule +*\ rpad(b.cuenta,18,'' '')|| --Cuenta
             lpad(b.identIFicacion,13,''0'')|| --Iden
             rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )||
             rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )||
             rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )||
             rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )||
             ''000000''||
             to_char(csactivated,''yyyymm'')||
             rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
             ''0000000000''||
             b.novedad||
             b.adjetivo||
             decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
             ''0000000000'' ||
             ''0000000000''|| --Saldo Deuda 
             lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| --Saldo mora 
             ''0000000000''||
             ''142''         ||
             b.calIFicacion||
             rpad(nvl(d.cccity,'' ''),20,'' '')|| --Canton 
             rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')||
             ''0000000000000000000000000000000000000000000000000000000000000000000000''||
              rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| --
              ''000000000'' ||--4.26 -- 4.27 
              ''000''|| --4.28
              lpad(b.cuentas_mora,3,''0'')||
              ''00000000000000000000000000000000000000000000''||
              ''1''|| --4.34
             lpad(b.edad_mora,3,0)||
             ''00000000''||
             decode(b.reclamo,''S'',0,9)||
             ''000000000000000000000000000000'' as REGISTRO,
              b.novedad as novedad,
              b.cuenta as cuenta
          
      FROM 
      cart_cuentas_dat@B_BUROCRED a,
      sysadm.'||lv_inven_uio||' b,
      customer_all c,
      ccontact_all d
      WHERE b.cuenta=a.cuen_nume_cuen
      AND   b.cuenta=c.custcode
      AND  c.customer_id=d.customer_id';*/ -- 5628 CLS CJA 19/01/2012 SE COMENTA VARIABLE LV_SENTENCIA PARA CAMBIAR EL DBLINK
    
      --INI 5628 CLS CJA 19/01/2012 SE CAMBIA DBLINK DE @B_BUROCRED A @FINGYE_BUROCRED
      lv_sentencia := ' 
                                              SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '')|| --Cuenta
                                                 lpad(b.identIFicacion,13,''0'')|| --Iden
                                                 rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )||
                                                 rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )||
                                                 ''000000''||
                                                 to_char(csactivated,''yyyymm'')||
                                                 rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                                                 ''0000000000''||
                                                 b.novedad||
                                                 b.adjetivo||
                                                 decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                                                 ''0000000000'' ||
                                                 ''0000000000''|| --Saldo Deuda 
                                                 lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| --Saldo mora 
                                                 ''0000000000''||
                                                 ''142''         ||
                                                 b.calIFicacion||
                                                 rpad(nvl(d.cccity,'' ''),20,'' '')|| --Canton 
                                                 rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')||
                                                 ''0000000000000000000000000000000000000000000000000000000000000000000000''||
                                                  rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| --
                                                  ''000000000'' ||--4.26 -- 4.27 
                                                  ''000''|| --4.28
                                                  lpad(b.cuentas_mora,3,''0'')||
                                                  ''00000000000000000000000000000000000000000000''||
                                                  ''1''|| --4.34
                                                 lpad(b.edad_mora,3,0)||
                                                 ''00000000''||
                                                 decode(b.reclamo,''S'',0,9)||
                                                 ''000000000000000000000000000000'' as REGISTRO,
                                                  b.novedad as novedad,
                                                  b.cuenta as cuenta
                                              
                                          FROM 
                                          cart_cuentas_dat@FINGYE_BUROCRED a,
                                          sysadm.' ||
                      lv_inven_uio || ' b,
                                          customer_all c,
                                          ccontact_all d
                                          WHERE b.cuenta=a.cuen_nume_cuen
                                          AND   b.cuenta=c.custcode
                                          AND  c.customer_id=d.customer_id';
      --FIN 5628 CLS CJA 19/01/2012
    
      li_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(li_CURSOR, lv_sentencia, dbms_sql.v7);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 1, lv_datos, 1000);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 2, lv_novedad, 10);
      DBMS_SQL.DEFINE_COLUMN(li_CURSOR, 3, lv_cuenta, 20);
      li_CURSOR1 := DBMS_SQL.EXECUTE(li_CURSOR);
    
      loop
        exit WHEN DBMS_SQL.FETCH_ROWS(li_CURSOR) = 0;
      
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 1, lv_datos);
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 2, lv_novedad);
        DBMS_SQL.COLUMN_VALUE(li_CURSOR, 3, lv_cuenta);
      
        BEGIN
          SELECT replace(replace(replace(replace(replace(replace(upper(lv_datos),
                                                                 '�',
                                                                 'A'),
                                                         '�',
                                                         'E'),
                                                 '�',
                                                 'I'),
                                         '�',
                                         'O'),
                                 '�',
                                 'U'),
                         '�',
                         'N')
            INTO lv_datos
            FROM dual;
        
        EXCEPTION
          WHEN OTHERS THEN
            lv_error := sqlerrm;
        END;
      
        --Escribo en el archivo y cuento lo que se generen con �xito o error 
        BEGIN
          INSERT INTO CO_UNION_CLIENTES
            (datos_registro, novedad, CUENTA)
          VALUES
            (LV_DATOS, LV_NOVEDAD, lv_cuenta);
          ln_reg_exito := ln_reg_exito + 1;
        
        EXCEPTION
          WHEN OTHERS THEN
            ln_reg_error := ln_reg_error + 1;
            lv_error     := substr(sqlerrm, 1, 500);
            cop_detalle_bitacora(pv_cuenta      => lv_cuenta,
                                 pv_descripcion => lv_error);
          
        END;
      
        IF mod(ln_reg_exito, gn_commit) = 0 THEN
          commit;
        END IF;
      
      END loop;
    
      cop_bitacora_ejecucion(Pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'S',
                             pv_tipo_proceso   => 'UQ', --U4
                             pv_consulta       => lv_sentencia,
                             pv_error          => lv_error);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := sqlerrm;
        cop_bitacora_ejecucion(pv_nombreTabla    => null,
                               pn_reg_exito      => ln_reg_exito,
                               pn_reg_error      => ln_reg_error,
                               pv_descripcion    => 'Error al procesar datos de los clientes UIO :' ||
                                                    lv_error,
                               pv_estado_reporte => 'E', --Generado con error 
                               pv_tipo_proceso   => 'UQ', --U4
                               pv_consulta       => lv_sentencia,
                               pv_error          => lv_error);
      
    END;
  
    -------------------------    
    --[2702] ECA
    /*IF ln_cod08 is not null THEN 
         cop_actualiza_estados_bitacora(pn_codigo => ln_cod08,
                                         pv_estado => 'S',
                                         pv_error => lv_error);            
    END IF;
    
    IF ln_cod24 is not null THEN 
         cop_actualiza_estados_bitacora(pn_codigo => ln_cod24,
                                         pv_estado => 'S',
                                         pv_error => lv_error);            
    END IF;*/
  
    IF ln_codgye is not null THEN
      cop_actualiza_estados_bitacora(pn_codigo => ln_codgye,
                                     pv_estado => 'S',
                                     pv_error  => lv_error);
    END IF;
  
    IF ln_coduio is not null THEN
      cop_actualiza_estados_bitacora(pn_codigo => ln_coduio,
                                     pv_estado => 'S',
                                     pv_error  => lv_error);
    END IF;
  
    commit;
  
  EXCEPTION
    WHEN le_error THEN
      lv_error := 'Error al truncar co_union_clientes favor truncarla manualmente :' ||
                  sqlerrm;
      pv_error := lv_error;
      cop_bitacora_ejecucion(pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => lv_error,
                             pv_estado_reporte => 'E', --Generado con error 
                             pv_tipo_proceso   => 'U1',
                             pv_consulta       => lv_sentencia,
                             pv_error          => lv_error);
    
    WHEN OTHERS THEN
      lv_error := sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => null,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => 'Error al generar union de proceso :' ||
                                                  lv_error,
                             pv_estado_reporte => 'E', --Generado con error 
                             pv_tipo_proceso   => 'U',
                             pv_consulta       => lv_generaArchivo,
                             pv_error          => lv_error);
    
  END COP_UNION_CLIENTES;
  ---------    
  PROCEDURE COP_DETALLE_BITACORA(pv_cuenta      varchar2,
                                 pv_descripcion varchar2) IS
  
    pragma autonomous_transaction;
  
  BEGIN
    INSERT INTO CO_DETALLE_BITACORA_REPDATACRE
      (cuenta, descripcion, fecha)
    VALUES
      (PV_CUENTA, PV_DESCRIPCION, sysdate);
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      null;
    
  END COP_DETALLE_BITACORA;

  ------*******    JPA  PROYECTO 3509    *******-------- 
  FUNCTION COF_CALIFICA_CUENTA(PV_CAMPO VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR C_CALIFICA_CUENTA(CV_EDAD_MORA VARCHAR2) is
      SELECT calIFicacion
        FROM co_calIFicacion_repdatacre
       WHERE to_number(cv_edad_mora) <= RANGO_MAX
         AND to_number(cv_edad_mora) >= RANGO_MIN;
  
    LV_CALIFICACION CO_CALIFICACION_REPDATACRE.CALIFICACION%TYPE;
  
  BEGIN
  
    OPEN C_CALIFICA_CUENTA(PV_CAMPO);
    FETCH C_CALIFICA_CUENTA
      INTO LV_CALIFICACION;
    CLOSE C_CALIFICA_CUENTA;
    RETURN LV_CALIFICACION;
  
  END COF_CALIFICA_CUENTA;

  FUNCTION COF_RECLAMO(PV_CUENTA VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR c_reclamo(lv_cuenta varchar2, cv_estado varchar2, cn_idFeature number, cn_valor number) is
    /* select 'x'
         from cl_contratos@axis a, 
              cb_features_contratos@axis b
         where a.id_contrato=b.id_contrato
         and a.codigo_doc=lv_cuenta 
         and a.estado= cv_estado--'a'
         and b.estado= cv_estado--'a'
         and b.valor=cn_valor --41
         and id_feature= cn_idfeature;--1001;*/
    
      SELECT 'X'
        FROM CO_PERMISO_DATACREDITO a
       WHERE a.id_cuenta = lv_cuenta;
  
    LC_RECLAMO VARCHAR2(1);
    LB_FOUND   BOOLEAN;
  
  BEGIN
    OPEN c_reclamo(PV_CUENTA, gv_estado, gn_idFeature, gn_valor);
    FETCH c_reclamo
      INTO lc_reclamo;
    lb_found := c_reclamo%FOUND;
    CLOSE c_reclamo;
  
    IF lb_found THEN
      RETURN 'S'; --Autorizado por el cliente 
    ELSE
      RETURN 'N'; --No autoriza el cliente 
    END IF;
  
  END COF_RECLAMO;

  PROCEDURE COP_EXISTE_TABLA(PV_TABLA IN VARCHAR2, PV_ERROR OUT VARCHAR2) IS
    --Comprueba si existe la tabla 
    CURSOR cg_existe_tabla(lv_nombre_tabla varchar2) is
      SELECT 'X' existe
        FROM ALL_TABLES T
       WHERE T.TABLE_NAME = lv_nombre_tabla;
  
    gr_existe_tabla cg_existe_tabla%rowtype;
    lb_found        boolean;
    lv_truncate     varchar2(100);
    le_error EXCEPTION;
    ln_codigo_error number;
    lv_programa     varchar2(60) := 'COP_EXISTE_TABLA';
  BEGIN
    PV_ERROR := null;
  
    OPEN cg_existe_tabla(PV_TABLA);
    FETCH cg_existe_tabla
      INTO gr_existe_tabla;
    lb_found := cg_existe_tabla%FOUND;
    CLOSE cg_existe_tabla;
  
    --Si encuentra la tabla borrra los datos o sino la crea 
    IF lb_found THEN
      BEGIN
        lv_truncate := 'truncate table ' || PV_TABLA;
        execute immediate lv_truncate;
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR := 'Error al truncar la tabla ' || sqlerrm;
          --raise le_error;   
          RETURN;
      END;
    
      --Se actualiza los que tienen el mismo nombre para no tomarlos en cuenta 
      BEGIN
      
        update co_bitacora_repdatacre
           set estado_reporte = 'E'
         WHERE tabla_destino like '%' || PV_TABLA || '%';
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR := 'Error actualizar co_bitacora_repdatacre ' || sqlerrm;
          RETURN;
          --                      raise le_error;    
      
      END;
    
      --
    ELSE
      -- CREA LA TABLA  
      COP_CREA_TABLA(PV_TABLA, 'sysadm', ln_codigo_error, PV_ERROR);
    END IF;
  
    IF PV_ERROR IS NOT NULL THEN
      RETURN;
    END IF;
  
    -- deshabilita el indicie
    /* COP_DESHABILITA_INDICE  (PV_TABLA,                                
                                                     PV_ERROR);
          
    IF  PV_ERROR IS NOT NULL THEN
        RETURN;
    END IF;   
                   */
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error general en : ' || lv_programa || ' - ' || sqlerrm;
  END COP_EXISTE_TABLA;

  PROCEDURE COP_HABILITA_INDICE(pv_nombre_tabla IN varchar2,
                                pv_error        OUT varchar2) IS
  
    lv_sentecia varchar2(1000);
  
    le_error EXCEPTION;
    lv_programa varchar2(60) := 'COP_HABILITA_INDICE';
  
  BEGIN
    pv_error    := null;
    lv_sentecia := 'create index idx_' || pv_nombre_tabla || ' on ' ||
                   pv_nombre_tabla || '(CUENTA)' /*||
                                ' tablespace  REP_CRED_IDX '|| 
                                ' pctfree 10 '||
                                ' initrans 2 '||
                                ' maxtrans 255 '||
                                ' storage '||
                                ' ( initial 10M '||
                                '  next 5M '||
                                '  minextents 1 '||
                                '  maxextents unlimited '||
                                '  pctincrease 0 )'*/
     ;
  
    execute immediate lv_sentecia;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error general  al crear el indice: idx_' ||
                  pv_nombre_tabla || ' en el programa ' || lv_programa;
  END COP_HABILITA_INDICE;

  PROCEDURE COP_DESHABILITA_INDICE(pv_nombre_tabla IN varchar2,
                                   pv_error        OUT varchar2) IS
  
    CURSOR Cl_ValidaIndice(Cv_indice Varchar2) is
      SELECT 'S' FROM ALL_INDEXES I WHERE I.INDEX_NAME = Cv_indice; --'IDX_'||NOMBRE_TABLE;
  
    lv_Encontro VARCHAR2(1) := 'N';
    lv_programa varchar2(60) := 'COP_DESHABILITA_INDICE';
    lv_sentecia varchar2(1000);
  
  BEGIN
    OPEN Cl_ValidaIndice('IDX_' || pv_nombre_tabla);
    FETCH Cl_ValidaIndice
      INTO lv_Encontro;
    IF Cl_ValidaIndice%NOTFOUND THEN
      lv_Encontro := 'N';
    END IF;
    CLOSE Cl_ValidaIndice;
  
    IF lv_Encontro = 'S' THEN
      lv_sentecia := 'DROP INDEX IDX_' || pv_nombre_tabla;
      execute immediate lv_sentecia;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error general  al eliminar  el indice: idx_' ||
                  pv_nombre_tabla || ' en el programa ' || lv_programa;
  END COP_DESHABILITA_INDICE;

  PROCEDURE COP_CALIFICA_CLIENTES_BSCS_C(pdfechcierreperiodo date,
                                         pv_error            out varchar2) is
  
    lv_fecha        varchar2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
    ln_codigo_error number;
    lv_nombre_tabla varchar2(60) := 'CO_REPDATCRE_';
    lv_tabla_origen varchar2(60) := 'CO_REPCARCLI_' || lv_fecha;
    lv_consulta     varchar2(1000);
    lv_adjetivo     varchar2(2);
    lv_reclamo      varchar2(1);
    ln_reg_error    number := 0;
    ln_reg_exito    number := 0;
    ln_ruc          number := 0;
    ------*******    JPA  PROYECTO 3509    *******--------   
    C_CURSOR_DINA    CUR_TYP;
    lv_cuenta        VARCHAR2(100);
    lv_mayor_vencido VARCHAR2(100);
    ln_nu_moras      VARCHAR2(100);
    lv_ruc           VARCHAR2(100);
    ln_saldoDeuda    VARCHAR2(100);
    lv_novedad       varchar2(2);
    lv_calificacion  varchar2(2);
    lv_tipo_iden     varchar(3);
    lv_tipo_persona  varchar2(1);
    ------*******    JPA  PROYECTO 3509    *******--------   
    le_error EXCEPTION;
    lv_programa varchar2(100) := 'COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS ';
  
  BEGIN
    -- Creo la tabla 
    lv_nombre_tabla := lv_nombre_tabla || lv_fecha || '_T';
    pv_error        := NULL;
  
    -- El procedimiento pregunta si existe la tabla destino, Si la encuentra borrra los datos o sino la crea.
    -- Tambien deshabilita el indicie para bajar el tiempo del proceso 
    COP_EXISTE_TABLA(lv_nombre_tabla, pv_error);
  
    IF pv_error IS NOT NULL THEN
      raise le_error;
    END IF;
  
    --Leo los datos del cliente con un cursor din�mico 
  
    lv_consulta := 'select cuenta,' ||
                   '(decode (balance_1,0,0,1) +  decode (balance_2,0,0,1) + decode (balance_3,0,0,1) + decode (balance_4,0,0,1) + ' ||
                   'decode (balance_5,0,0,1) +  decode (balance_6,0,0,1) + decode (balance_7,0,0,1) + decode (balance_8,0,0,1) + ' ||
                   'decode (balance_9,0,0,1) +  decode (balance_10,0,0,1) + decode (balance_11,0,0,1)) nu_moras, ' ||
                   'TOTALVENCIDA ln_saldoDeuda,' ||
                   'DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ) mayorvencido, ' ||
                   'DECODE (DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' , ''01'', mayorvencido ), ''01'', ''01'', ''30'', ''06'',''60'',''07'',''90'', ''08'',''09'' ) NOVEDAD,' ||
                   'COK_REPORTE_DATACREDITO_V2.COF_CALIFICA_CUENTA( DECODE(mayorvencido, ''V'',''01'',''''''-V'',''01'', ''-V'' ,''01'', mayorvencido )) evaluacion,' ||
                   'REPLACE(TRIM(ruc), ''-'', NULL) RUC,' ||
                   'DECODE(length(ruc),10,''CED'',13,''RUC'',''PAS'')TIPO_IDENTIFICACION,' ||
                   'DECODE(length(ruc),10,''N'',13,''J'',''N'') TIPO_PERSONA ' ||
                   'from ' || lv_tabla_origen;
  
    OPEN C_CURSOR_DINA FOR lv_consulta;
    LOOP
      FETCH C_CURSOR_DINA
        INTO lv_cuenta, ln_nu_moras, ln_saldoDeuda, lv_mayor_vencido, lv_novedad, lv_calificacion, lv_ruc, lv_tipo_iden, lv_tipo_persona;
      EXIT WHEN C_CURSOR_DINA%NOTFOUND;
    
      ---Valido tipo persona  
      pv_error := null;
    
      begin
        ln_ruc := TO_NUMBER(lv_ruc);
      exception
        when INVALID_NUMBER then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => pv_error);
        when others then
          lv_ruc := COF_VALIDA_IDENTIFICACON(pv_identificacion => lv_ruc,
                                             pv_error          => pv_error);
      end;
    
      --Encuentro el tipo de persona  
      lv_reclamo := COF_RECLAMO(lv_cuenta);
    
      --Si el saldo de la deuda es menor que 0.5 entonces se pone al d�a 
      IF ln_saldoDeuda < 0.5 THEN
        lv_mayor_vencido := '01';
        lv_novedad       := '01'; --AL DIA 
        lv_adjetivo      := '00';
        ln_nu_moras      := 0;
        lv_calIFicacion  := 'A';
      END IF;
    
      --Valido el adjetivo  en base a la novedad  
      IF lv_novedad = '08' or lv_novedad = '09' THEN
        lv_adjetivo := '11'; --Cuenta en cobrador mayor de 90
      ELSE
        IF lv_novedad = '06' or lv_novedad = '07' THEN
          lv_adjetivo := '11'; --Linea desconectada 
        ELSE
          lv_adjetivo := '00';
        END IF;
      END IF;
    
      --Ingreso la calIFicaci�n de cada cliente 
      pv_error        := NULL;
      ln_codigo_error := NULL;
    
      cop_ingresa_valores_datacre(pv_nombre_tabla   => lv_nombre_tabla,
                                  pv_cuenta         => lv_cuenta,
                                  pv_novedad        => lv_novedad,
                                  pv_adjetivo       => lv_adjetivo,
                                  pv_calIFicacion   => lv_calIFicacion,
                                  pv_cuentas_mora   => ln_nu_moras,
                                  pv_edad_mora      => lv_mayor_vencido,
                                  pv_reclamo        => lv_reclamo,
                                  pv_tipo_id        => lv_tipo_iden,
                                  pv_tipo_persona   => lv_tipo_persona,
                                  pv_identIFicacion => lv_ruc,
                                  pn_codigo_error   => ln_codigo_error,
                                  pv_error          => pv_error);
    
      IF ln_codigo_error = 0 THEN
        ln_reg_error := ln_reg_error + 1;
      ELSE
        ln_reg_exito := ln_reg_exito + 1;
      END IF;
    
      IF mod(ln_reg_exito, gn_commit) = 0 THEN
        commit;
      END IF;
    
    END LOOP;
  
    commit;
  
    pv_error := NULL;
    /* -- Habilito constraints
    COP_HABILITA_INDICE (lv_nombre_tabla,                                
                                                  pv_error);
    
    IF pv_error IS NOT NULL THEN 
        raise le_error;
    END IF;    
    ---*/
  
    cop_bitacora_ejecucion(Pv_nombreTabla    => lv_nombre_tabla,
                           pn_reg_exito      => ln_reg_exito,
                           pn_reg_error      => ln_reg_error,
                           pv_descripcion    => pv_error,
                           pv_estado_reporte => 'N',
                           pv_tipo_proceso   => 'P',
                           pv_consulta       => NULL,
                           pv_error          => pv_error,
                           pv_tablaOrigen    => lv_tabla_origen);
  
    -------------------------------------------------------------------  
  EXCEPTION
  
    WHEN le_error THEN
      pv_error := lv_programa || ' - ' || pv_error;
      cop_bitacora_ejecucion(Pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => pv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => pv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    WHEN OTHERS THEN
    
      pv_error := lv_programa || ' - ' || sqlerrm;
      cop_bitacora_ejecucion(pv_nombreTabla    => lv_nombre_tabla,
                             pn_reg_exito      => ln_reg_exito,
                             pn_reg_error      => ln_reg_error,
                             pv_descripcion    => pv_error,
                             pv_estado_reporte => 'E',
                             pv_tipo_proceso   => 'E',
                             pv_consulta       => NULL,
                             pv_error          => pv_error,
                             pv_tablaOrigen    => lv_tabla_origen);
    
  END COP_CALIFICA_CLIENTES_BSCS_C;

  ------*******    JPA  PROYECTO 3509    *******-------- 
  PROCEDURE COP_EJECUTA(pv_error OUT varchar2) IS
    lD_fecha date;
  BEGIN
  
    lD_fecha := to_date('24/01/2008', 'dd/mm/yyyy');
    --lD_fecha:= to_date('08/01/2007','dd/mm/yyyy');
  
    execute immediate 'Alter System flush shared_pool';
  
    -- inicio del proceso de BSCS con Bulk Collect
    cop_bitacora_ejecucion(pv_nombreTabla    => null,
                           pn_reg_exito      => 0,
                           pn_reg_error      => 0,
                           pv_descripcion    => 'INICIO BSC VERSION BC',
                           pv_estado_reporte => 'R', --estado generado con �xito 
                           pv_tipo_proceso   => 'PB', --Indicador de reporte 
                           pv_consulta       => NULL,
                           pv_error          => pv_error);
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en bitacrora INICIO ' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    COP_CALIFICA_CLIENTES_BSCS(lD_fecha, pv_error);
  
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en COP_CALIFICA_CLIENTES_BSCS VERSION BC ' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    -- FIN del proceso de BSCS con Bulk Collect
    cop_bitacora_ejecucion(pv_nombreTabla    => null,
                           pn_reg_exito      => 0,
                           pn_reg_error      => 0,
                           pv_descripcion    => 'FIN BSC VERSION BC',
                           pv_estado_reporte => 'R', --estado generado con �xito 
                           pv_tipo_proceso   => 'PB', --Indicador de reporte 
                           pv_consulta       => NULL,
                           pv_error          => pv_error);
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en bitacrora FIN' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    -- ELIMINA TABLA
    execute immediate 'drop table CO_REPDATCRE_24062008_T';
  
    ------------ VERSION CON CURSORES
  
    execute immediate 'Alter System flush shared_pool';
    -- inicio del proceso de BSCS con CURSORES
    cop_bitacora_ejecucion(pv_nombreTabla    => null,
                           pn_reg_exito      => 0,
                           pn_reg_error      => 0,
                           pv_descripcion    => 'INICIO BSC VERSION CURSORES',
                           pv_estado_reporte => 'R', --estado generado con �xito 
                           pv_tipo_proceso   => 'PC', --Indicador de reporte 
                           pv_consulta       => NULL,
                           pv_error          => pv_error);
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en bitacrora INICIO CURSORES ' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    COP_CALIFICA_CLIENTES_BSCS_C(lD_fecha, pv_error);
  
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en COP_CALIFICA_CLIENTES_BSCS VERSION CURSORES ' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    -- FIN del proceso de BSCS con Bulk Collect
    cop_bitacora_ejecucion(pv_nombreTabla    => null,
                           pn_reg_exito      => 0,
                           pn_reg_error      => 0,
                           pv_descripcion    => 'FIN BSC VERSION CURSORES',
                           pv_estado_reporte => 'R', --estado generado con �xito 
                           pv_tipo_proceso   => 'PC', --Indicador de reporte 
                           pv_consulta       => NULL,
                           pv_error          => pv_error);
    IF pv_error IS NOT NULL THEN
      DBMS_OUTPUT.put_line('error al registrar en bitacrora FIN CURSORES' ||
                           to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' ' ||
                           pv_error);
      pv_error := null;
    END IF;
  
    ---
  
    -- ELIMINA TABLA
    execute immediate 'drop table CO_REPDATCRE_24062008_T';
  
    /*  ------------ VERSION ORIGINAL
    
    execute immediate  'Alter System flush shared_pool';
       -- inicio del proceso de BSCS con VERSION ORIGINAL
    cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                    pn_reg_exito =>0 ,
                                                    pn_reg_error => 0,
                                                    pv_descripcion => 'INICIO BSC VERSION ORIGINAL',
                                                    pv_estado_reporte => 'R' , --estado generado con �xito 
                                                    pv_tipo_proceso => 'PO' , --Indicador de reporte 
                                                    pv_consulta => NULL,
                                                    pv_error => pv_error);
    IF pv_error IS NOT NULL THEN                                                          
        DBMS_OUTPUT.put_line('error al registrar en bitacrora INICIO ORIGINAL '||to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' '||pv_error );    
        pv_error:= null;
    END IF;
    
    COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(lD_fecha,pv_error );
    
    IF pv_error IS NOT NULL THEN                                                          
        DBMS_OUTPUT.put_line('error al registrar en COP_CALIFICA_CLIENTES_BSCS VERSION ORIGINAL '||to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' '||pv_error );    
        pv_error:= null;
    END IF;
    
    
    -- FIN del proceso de BSCS con ORIGINAL
    cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                    pn_reg_exito =>0 ,
                                                    pn_reg_error => 0,
                                                    pv_descripcion => 'FIN BSC VERSION ORIGINAL',
                                                    pv_estado_reporte => 'R' , --estado generado con �xito 
                                                    pv_tipo_proceso => 'PO' , --Indicador de reporte 
                                                    pv_consulta => NULL,
                                                    pv_error => pv_error);
    IF pv_error IS NOT NULL THEN                                                          
        DBMS_OUTPUT.put_line('error al registrar en bitacrora FIN CURSORES'||to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || ' '||pv_error );    
        pv_error:= null;
    END IF;
    
    
    */
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error general  en el programa COP_EJECUTA ' || sqlerrm;
    
  END COP_EJECUTA;

BEGIN
  OPEN gc_parametros(gv_parametro);
  FETCH gc_parametros
    INTO gn_commit;
  CLOSE gc_parametros;

END COK_REPORTE_DATACREDITO_V2;
/
