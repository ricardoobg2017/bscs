CREATE OR REPLACE PACKAGE PQ_RELOJ_MEDIOS_MAGNETICOS IS

PROCEDURE P_VALIDAR_CTAS (Pv_Cuenta     VARCHAR2,
                          Pn_Existe OUT NUMBER,--0 no existe 1 existe
                          Pv_Error  OUT VARCHAR2);

END PQ_RELOJ_MEDIOS_MAGNETICOS;
/
CREATE OR REPLACE PACKAGE BODY PQ_RELOJ_MEDIOS_MAGNETICOS IS
--=====================================================================================--
-- CREADO POR:     Andres Balladares.
-- FECHA MOD:      28/10/2022
-- PROYECTO:       [22311] Legados de cobranzas - Reloj validando medio
-- LIDER IRO:      HTSI Gloria Rojas
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Validacion de reloj de los pagos con error en medios magneticos.
--=====================================================================================--

PROCEDURE P_VALIDAR_CTAS (Pv_Cuenta     VARCHAR2,
                          Pn_Existe OUT NUMBER,--0 no existe 1 existe
                          Pv_Error  OUT VARCHAR2)IS
  
  CURSOR C_CTAS_MEDIOS (Cv_Cuentas VARCHAR2)IS
  SELECT COUNT(*)
    FROM PORTA.RC_MMAG_MEDIOS_GENERADOS_H@AXIS X, PORTA.RC_MMAG_RESP_BANCOS_DAT@AXIS Y
   WHERE X.ESTADO_RC = 'A'
     AND Y.CODIGO_ENTIDAD = X.CODI_ENTI_FINA
     AND Y.NOMBRE_ARCHIVO = X.ARCHIVO
     AND Y.CUENTA_AXIS = Cv_Cuentas;
  
  CURSOR C_CTAS_BSCS (Cv_Cuentas VARCHAR2)IS
  SELECT COUNT(*)
    FROM PIHTAB_ALL C
   WHERE C.CUSTCODE = Cv_Cuentas
     AND C.TRANSX_STATUS IN (1,3);--1 POR PROCESAR 3 ERROR
  
  CURSOR C_CTAS_EXISTE (Cv_Cuenta VARCHAR2, Cv_Detalle VARCHAR2)IS
  SELECT COUNT(*)
    FROM PORTA.RC_CTAS_ERROR_PAGOS@AXIS X
   WHERE X.CUENTA = Cv_Cuenta
     AND X.DETALLE = Cv_Detalle
     AND X.ESTADO = 'A';
  
  Ln_Count    NUMBER;
  Lv_Detalle  VARCHAR2(4000);
  Lb_Insert   BOOLEAN;
  
BEGIN
  
  OPEN C_CTAS_MEDIOS(Pv_Cuenta);
  FETCH C_CTAS_MEDIOS INTO Ln_Count;
  CLOSE C_CTAS_MEDIOS;
  
  IF Ln_Count > 0 THEN
     Lv_Detalle := 'LA CUENTA PRESENTA ERROR EN MEDIOS MAGNETICOS.';
  END IF;
  
  Ln_Count := 0;
  
  OPEN C_CTAS_BSCS (Pv_Cuenta);
  FETCH C_CTAS_BSCS INTO Ln_Count;
  CLOSE C_CTAS_BSCS;
  
  IF Ln_Count > 0 THEN
     Lv_Detalle := 'EL PAGO DE LA CUENTA NO HA SIDO PROCESADO EN BSCS.';
  END IF;
  
  Ln_Count := 0;
  
  OPEN C_CTAS_EXISTE(Pv_Cuenta, Lv_Detalle );
  FETCH C_CTAS_EXISTE INTO Ln_Count;
  CLOSE C_CTAS_EXISTE;
  
  IF Ln_Count = 0 AND Lv_Detalle IS NOT NULL THEN
     Lb_Insert := TRUE;
  ELSE
     Lb_Insert := FALSE;
  END IF;
  
  IF Lb_Insert THEN
     
     INSERT INTO PORTA.RC_CTAS_ERROR_PAGOS@AXIS(CUENTA, DETALLE, ESTADO, USUARIO_INGRESO, FECHA_INGRESO)
          VALUES (Pv_Cuenta, Lv_Detalle, 'A', USER, TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') );
     
     COMMIT;
     
  END IF;
  
  IF Lb_Insert THEN
     Pn_Existe := 1;
  ELSE
     Pn_Existe := 0;
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    Pv_Error := 'SYSADM.PQ_RELOJ_MEDIOS_MAGNETICOS.P_VALIDAR_CTAS => ' || SQLERRM;
END P_VALIDAR_CTAS;

END PQ_RELOJ_MEDIOS_MAGNETICOS;
/
