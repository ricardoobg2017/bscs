CREATE OR REPLACE package QC_FACTURAS_GENERAL is
  
  procedure DET_FACT_DETALLADA_PRUE(LwsCiclo varchar2);
  
  function Pl_Count(LwsSql varchar2) return integer;
                             
end QC_FACTURAS_GENERAL;
/
CREATE OR REPLACE package body QC_FACTURAS_GENERAL is

LwsSql     varchar2(2000);
--LwsCiclo   varchar2(2);
PROCEDURE DET_FACT_DETALLADA_PRUE (LwsCiclo varchar2) IS

cursor ll is
select CUENTA
from qc_proc_facturas
minus ( select cuenta from QC_FACTURAS where ciclo = LwsCiclo );
--where cuenta = '1.10049966';
lwiCont_Fact_Det number;
lwiCont_Cuenta_ESP number;
lwiCont_Ser_Tel number;
lwiSerie20000 number;
lwiSerie20100 number;
lwiSerie20200 number;
lwiSerie21100 number;
lv_error varchar2(500);

BEGIN

--cuentas con solo cargos o solo creditos (otro escenario que tengan solo $0.8)
--TRUNCAR TABLA
--TRUNCATE TABLE QC_FACTURAS
delete from QC_FACTURAS where ciclo = LwsCiclo;
delete from QC_FACTURAS_1 where ciclo = LwsCiclo;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (1,2,3,4,5)
and ciclo = LwsCiclo;
commit;
--INGRESAR FACTURAS CON COBRO DE CARGOS CREDITOS.
LwsSql:=' INSERT INTO QC_FACTURAS';
LwsSql:=LwsSql||' select DISTINCT cuenta, 0 PROCESO, 0 PROCESADOS, '||LwsCiclo;
LwsSql:=LwsSql||' from FACTURAS_CARGADAS_TMP'||LwsCiclo;
LwsSql:=LwsSql||' where codigo = 20200';
LwsSql:=LwsSql||' and campo_2 in (''Factura Detallada'',''Factura Detallada Plus'', ''Detalle de llamadas - no cobro'')';
execute immediate LwsSql;
commit;

--and cuenta = '5.36671';
--select * from FACTURAS_CARGADAS_TMP where cuenta = '5.36671'
--LAZO PRINCIPAL DE CUENTAS CON CARGOS O CREDITOS

FOR CURSOR1 IN ll LOOP
 begin

      LwsSql:='select count(*) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
      LwsSql:=LwsSql||' where codigo = 20200 and campo_2 in (''Factura Detallada'') '||
      'and cuenta = '''||CURSOR1.CUENTA||''';
      lwiCont_Fact_Det:=0;
      lwiCont_Ser_Tel:=Pl_count(LwsSql);

      LwsSql:='select count(*) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
      LwsSql:=LwsSql||' where codigo = ''41000'' and campo_2 = ''Plan:''';
      LwsSql:=LwsSql||' and CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES )and cuenta = '||'''CURSOR1.CUENTA'''';
      lwiCont_Cuenta_ESP:=0;
      lwiCont_Cuenta_ESP:=Pl_count(LwsSql);


--Servicios de telecomunicacion

      LwsSql:='select nvl(sum(to_number(nvl(campo_3,0))),0) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
      LwsSql:=LwsSql||' where codigo = 20000 and cuenta = '||CURSOR1.CUENTA;
      lwiSerie20000:=0;
      lwiSerie20000:=Pl_count(LwsSql);
--Otros Servicios de telecomunicacion
--No se toma encuenta la linea de factura detallada
     LwsSql:='select nvl(sum(to_number(nvl(campo_3,0))),0) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
     LwsSql:=LwsSql||' where codigo = 20200 and campo_2 <> ''Factura Detallada'' and cuenta = '||CURSOR1.CUENTA;
     lwiSerie20200:=0;
     lwiSerie20200:=Pl_count(LwsSql);
--Total servicios de telecomunicacion
     LwsSql:='select nvl(sum(to_number(nvl(campo_2,0))),0) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
     LwsSql:=LwsSql||' where codigo = 20100 and cuenta = '||CURSOR1.CUENTA;
     lwiSerie20100:=0;
     lwiSerie20100:=Pl_count(LwsSql);

     LwsSql:='select nvl(sum(to_number(nvl(campo_3,0))),0) from FACTURAS_CARGADAS_TMP'||LwsCiclo;
     LwsSql:=LwsSql||' where codigo IN (21100) and cuenta = '||CURSOR1.CUENTA;
     lwiSerie21100:=0;
     lwiSerie21100:=Pl_count(LwsSql);

   if lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0 then
      if lwiSerie20000 > 0 or lwiSerie20100 > 0 or lwiSerie20200 > 0 then
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,1, 'Factura Detallada 0.80',0,0,LwsCiclo);
       end if;
       
      if lwiSerie21100 > 0  then
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,3, 'Factura Detallada Cargos Creditos 0.80',0,0,LwsCiclo);
       end if;

      if lwiSerie21100 = 0 and lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0  then
         --comsumo por suspend
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,2, 'Cargar Factura Detallada por Suspend 0.80',0,0,LwsCiclo);
       end if;
   end if;

     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
 end;
 commit;
END LOOP;
-----------------SE REVISA FACTURAS DETALLADAS PLUS CON COBRO DE 0.8 ctvs.
LwsSql:=' INSERT INTO QC_RESUMEN_FAC ';
LwsSql:=LwsSql||' select CUENTA, 4, ''Factura detallada plus y $0.8'',0,0 from FACTURAS_CARGADAS_TMP'||LwsCiclo;
LwsSql:=LwsSql||' where codigo = 20200';
LwsSql:=LwsSql||' and campo_2 = ''Factura Detallada''';
LwsSql:=LwsSql||' and cuenta in (';
LwsSql:=LwsSql||'              select cuenta from FACTURAS_CARGADAS_TMP'||LwsCiclo;
LwsSql:=LwsSql||'              where codigo = 20200';
LwsSql:=LwsSql||'              and campo_2 in (''Factura Detallada Plus'', ''Detalle de llamadas - no cobro'' ))';
              
execute immediate LwsSql;
commit;

DELETE QC_FACTURAS;
execute immediate LwsSql;
DELETE QC_FACTURAS_1;
execute immediate LwsSql;
commit;
--INSERTAR CUENTAS ESPECIALES
LwsSql:=' INSERT INTO QC_FACTURAS';
LwsSql:=LwsSql||' select distinct cuenta, 0, 0 from FACTURAS_CARGADAS_TMP'||LwsCiclo;
LwsSql:=LwsSql||' where CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES )';
execute immediate LwsSql;
commit;

LwsSql:=' INSERT INTO QC_FACTURAS_1';
LwsSql:=LwsSql||' select distinct cuenta, 0, 0 from FACTURAS_CARGADAS_TMP'||LwsCiclo;
LwsSql:=LwsSql||' where cuenta in (select cuenta from QC_FACTURAS)';
LwsSql:=LwsSql||' and campo_2 = ''Plan:''';
LwsSql:=LwsSql||' and CAMPO_3 not in ( select cuenta from QC_CUENTAS_ESPECIALES )';
execute immediate LwsSql;
commit;
delete QC_FACTURAS where cuenta in ( select cuenta from QC_FACTURAS_1 );
execute immediate LwsSql;
commit;
--VERIFICAR CUENTAS CON FACTURA DETALLADA
LwsSql:=' INSERT INTO QC_RESUMEN_FAC';
LwsSql:=LwsSql||' select CUENTA, 5, ''Cuentas Especiales con factura detallada'',0,0 from FACTURAS_CARGADAS_TMP ';
LwsSql:=LwsSql||' where cuenta in ( select cuenta from QC_FACTURAS )';
LwsSql:=LwsSql||' AND  codigo = 20200';
LwsSql:=LwsSql||' and campo_2 = ''Factura Detallada''';
execute immediate LwsSql;
commit;

END;


function Pl_Count(LwsSql varchar2) return integer is Resultado integer;
begin
declare
 c_sql integer;
lwiValor integer;
  res integer;
  begin
    c_sql := dbms_sql.open_cursor;

      begin
      dbms_sql.parse(c_sql,LwsSql,dbms_sql.NATIVE);
      dbms_sql.define_column(c_sql, 1, lwiValor);
      res := dbms_sql.execute(c_sql);
        LOOP
          -- Fetch a row from the source table
          IF dbms_sql.fetch_rows(c_sql) > 0 THEN
          lwiValor:=0;
          dbms_sql.column_value(c_sql, 1, lwiValor);
          ELSE
            EXIT;
          END IF;
        END LOOP;
            Resultado:=lwiValor;
          return (Resultado);
        dbms_output.put_line (to_char(lwiValor));
      end;
   dbms_sql.close_cursor(c_sql);
  end;
end;
end;
/

