create or replace package sysadm.CRK_TRX_FUNCIONES_ENCRIPTA is

  FUNCTION COF_ENCRIPTA(Pv_Cadena   in varchar2,
                         Pn_Longitud in number) 
                         RETURN varchar2;                                   
                                     
  FUNCTION COF_DESENCRIPTA(Pv_Cadena in varchar2)
                           RETURN varchar2;
    
  FUNCTION COF_SOLO_NUMERO(PV_NUMERO IN VARCHAR2) 
                           RETURN VARCHAR2;

end CRK_TRX_FUNCIONES_ENCRIPTA;
/
create or replace package body sysadm.CRK_TRX_FUNCIONES_ENCRIPTA wrapped
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
8a6 34b
kDvBClAMjJQxXi150EikgfATKsowgzsr+kgFfC/Nkw9/eKECXoXe4Y7zdsOZzS8rWjcabs4U
js2+fWu10MCCH2Im/YkvftbZE0LQTmC5CotUpC93oACEakqMcGSGP8qy6DYI8/dA7ofDqULx
Z+Gz65DU4grc53dXXNhC5bR5n7pdKJjSTnK7fvgMaHPOkvi6o+VVnsNawxF9Ge7Jc95+5W/3
8uZwBmnfvdhfQV+kbFtWQCukymrH8FTLdPQ3Ok7FD5CM/yXppZMZEPruOFvYZKNPahuimmeS
f9cMW1EJIv7hDfNaJpyWEtzj7u0PnXjZQG36sGBbWeSIvgXBB74a6zLKQS8t5GI7ISpSL/ca
/t/oq06P8YnTdetKSgl2200GFz+vufN2kxNF+t1uVTVFLmqlUDxyzxqBP48uY9EgjomfgFYi
DeO6NWVZkjVfcXxZy5LIXwZaOw9dFVdRx+PHNSXpM65nueLRuDmFLrq5TYaFGcuirBdejTSl
oUv2+yWKZ2Sy/I2HI7npnp7Dl5fO5EhJa8MOUMoBZX9VXfRxAZYB4NjdK0LIc9vVa8xx+yBt
UVPl25AYbWwZnQPbEGDC5aSF5jqAvwQ/vJPdYqOWj/kAROSd/LEb32KaTWYfoQWWPq0bmxGL
eU0TiHrm8zHalz4JEyiBNnCYN369XOJduO8kG/QCH96w4kgJgsJK6rQKnusKcNSLO0VYY5xZ
dqQ2vDz6+pqDBx6ZPFU3zDJuRJaW9rYjiDzXaPfZ+04/iXuOAeSSCLoaYOYH8f0QTVCxp43P
rXGDOBqX5dH5kfdgVWLKsDoeOj0kLEMHJOh0GTc=
/
