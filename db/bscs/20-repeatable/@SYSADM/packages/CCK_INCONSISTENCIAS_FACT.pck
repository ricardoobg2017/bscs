CREATE OR REPLACE Package Cck_Inconsistencias_Fact Is

  -- Author  : DCHONILLO
  -- Created : 16/12/2004 09:24:55
  -- Purpose : REVISION DE INCONSISTENCIAS PARA LA FACTURACION
  Function ccf_obtiene_numero(pn_coid In Number,
                               pv_dn_num Out Varchar2) Return Number;
                               
  Function Ccf_Valida_Periodo(Pv_Periodo   In Varchar2,
                              Pv_Fecha_Ini Out Varchar2,
                              Pv_Fecha_Fin Out Varchar2,
                              Pv_Error     Out Varchar2) Return Number;
  Procedure Ccp_Valida_Cp_Contrato(Pv_Periodo   In Varchar2,
                                   Pv_Resultado Out Varchar2);
  Procedure Ccp_Valida_Cp_Eliminado(Pv_Periodo   In Varchar2,
                                    Pv_Resultado Out Varchar2);
  Procedure Ccp_Valida_Feat_Basicos(Pv_Resultado Out Varchar2);
  Procedure Ccp_Doble_Seguro_Equipo(Pv_Periodo   In Varchar2,Pv_sncode   In number,
                                    Pv_Resultado Out Varchar2);
  Procedure Ccp_Fechas_Traslapadas(Pv_code   In number,Pv_Resultado Out Varchar2);
  Procedure Ccp_Valida_Ies (pv_periodo In Varchar2, 
                                            Pv_Resultado Out Varchar2) ;

End Cck_Inconsistencias_Fact;
/
CREATE OR REPLACE Package Body Cck_Inconsistencias_Fact Is
  -- Author  :            DCHONILLO
  -- Created :            16/12/2004 09:24:55
  -- Purpose :            REVISION DE INCONSISTENCIAS PARA LA FACTURACION
  -- Version :            1.0.1
  -- Solicitado:          GSI-FACTURACION
  ---Modifiaci�n:         Se adiciona procedimiento FECHAS TRASLAPADAS (17/12/2004)
  ---------------------------------------------------------------------------------
  -- Version :            1.0.2  
  --Modifica:             DCH
  --Motivo:               Adiciona funcion para retornar n�mero tel�fono
  --fecha:                22/12/2004
  --Solicita              EAR
  ---------------------------------------------------------------------------------  
  -- Version :            1.0.3  
  --Modifica:             DCH
  --Motivo:               Sacar valor del cargo a paquete eliminado y la fecha valida.
  --fecha:                19/01/2005
  --Solicita              EAR
---------------------------------------------------------------------------------    
  -- Version :            1.0.4  
  --Modifica:             Diana Chonillo Luc�n
  --Motivo:               En cargo a paquete eliminado, revisar si el feature se marca como eliminado o se mantiene activo
  --fecha:                19/01/2005
  --Solicita              Patricio Chonillo
    
    
  
  Function ccf_obtiene_numero(pn_coid In Number,
                               pv_dn_num Out Varchar2) Return Number Is
  
  
  Begin
    Select Dn_Num
      Into Pv_Dn_Num
      From Directory_Number d, Contr_Services_Cap c
     Where c.Co_Id = Pn_Coid And d.Dn_Id = c.Dn_Id And
           c.Cs_Deactiv_Date Is Null And c.Main_Dirnum = 'X';
    Return 0;       
  Exception
    When no_data_found Then 
       pv_dn_num:='nulo';
       Return -1;
    When too_many_rows Then
       pv_dn_num:='doble';
       Return -1;
    When Others Then 
       pv_dn_num:='ERR ORA';
       Return -1;                           
  End;                             
                               
                               
  Function Ccf_Valida_Periodo(Pv_Periodo   In Varchar2,
                              Pv_Fecha_Ini Out Varchar2,
                              Pv_Fecha_Fin Out Varchar2,
                              Pv_Error     Out Varchar2) Return Number Is

  --Validaci�n de periodo ingresado, devuelve fecha inicio y fin del periodo 
    Lv_Error Varchar2(150);
    Le_Error Exception;
    Ln_Periodo Number;
    lv_a�o     Varchar2(4);
    ld_fecha_periodo       Date;
  
  Begin
    If Pv_Periodo Is Null Then
      Lv_Error := 'Debe ingresar periodo en formato mm';
      Raise Le_Error;
    End If;
    
    
    Begin
      Ln_Periodo := To_Number(Pv_Periodo);
    Exception
      When Others Then
        Lv_Error := 'Ingrese periodo en formato mm';
        Raise Le_Error;
    End;
  
    If Ln_Periodo < 1 Or Ln_Periodo > 12 Then
      Lv_Error := 'Periodos validos del 01 al 12';
      Raise Le_Error;
    End If;
    
    
    If  To_Char(Sysdate , 'dd') Between 24 And 31 Then
        ld_fecha_periodo:=Add_Months(Sysdate,1);
    Else    
        ld_fecha_periodo:=Sysdate;
    End If;    
    
    If to_char(ld_fecha_periodo,'mm') <pv_periodo Then
       lv_a�o:=to_char(ld_fecha_periodo,'yyyy')-1;
    Else 
       lv_a�o:=to_char(ld_fecha_periodo,'yyyy');   
    End If;
    
    Pv_Fecha_Fin := '24/' || Pv_Periodo || '/'||lv_a�o||' 00:01:00';
    Pv_Fecha_Ini := To_Char(Add_Months(To_Date(Pv_Fecha_Fin,
                                               'dd/mm/yyyy hh24:mi:ss'),
                                       -1),
                            'dd/mm/yyyy hh24:mi:ss');
  
    Return 0;
  Exception
    When Le_Error Then
      Pv_Error := Lv_Error;
      Return - 1;
  End;
  -----------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------
  Procedure Ccp_Valida_Cp_Contrato(Pv_Periodo   In Varchar2,
                                   Pv_Resultado Out Varchar2) Is
  ---Validaci�n del cargo a paquete a nivel de contrato hijo de los planes que lo soportan
    Cursor Plan_Cp Is
      Select a.Tmcode
        From Mpulktmb a ----, Rateplan b
       Where a.Sncode = 5 And
             a.Vscode =
             (Select Max(Vscode) From Mpulktmb Where Tmcode = a.Tmcode);
  
    Cursor No_Existe Is
      Select t.Co_Id
        From Cc_Tmp_Inconsistencia t
       Where t.Error = 0 And Not Exists
       (Select 'X'
                From Profile_Service
               Where Co_Id = t.Co_Id And Sncode = 5);
  
    Cursor No_Activo Is
      Select /*+ rule */
       t.Co_Id, Ph.Status
        From Cc_Tmp_Inconsistencia t,
             Profile_Service       p,
             Pr_Serv_Status_Hist   Ph
       Where t.Error = 0 And t.Ch_Estatus = 'a' And p.Co_Id = t.Co_Id And
             p.Sncode = 5 And Ph.Co_Id = p.Co_Id And Ph.Sncode = p.Sncode And
             Ph.Histno = p.Status_Histno And Ph.Status <> 'A' And
             t.Observacion Is Null;
  
    Cursor Suspend Is
      Select /*+ rule */
       t.Co_Id, t.Ch_Fecha, Ph.Status, Ph.Valid_From_Date
        From Cc_Tmp_Inconsistencia t,
             Profile_Service       p,
             Pr_Serv_Status_Hist   Ph
       Where t.Error = 0 And t.Ch_Estatus <> 'a' And p.Co_Id = t.Co_Id And
             p.Sncode = 5 And Ph.Co_Id = p.Co_Id And Ph.Sncode = p.Sncode And
             Ph.Histno = p.Status_Histno And t.Observacion Is Null;
  
    Le_Error Exception;
    Lv_Sentencia Varchar2(250);
    Lv_Error     Varchar2(200);
    Lv_Fecha_Fin Varchar2(20);
    Lv_Fecha_Ini Varchar2(20);
  
    Type Array_Obs Is Table Of Varchar2(100);
    Lr_Obs Array_Obs;
    Type Array_Coid Is Table Of Number;
    Lr_Coid Array_Coid;
    Type array_num Is Table Of Varchar2(24);
    lr_num array_num;    
  
  Begin
    If Cck_Inconsistencias_Fact.Ccf_Valida_Periodo(Pv_Periodo,
                                                   Lv_Fecha_Ini,
                                                   Lv_Fecha_Fin,
                                                   Lv_Error) <> 0 Then
      Raise Le_Error;
    End If;
  
    Lv_Sentencia := 'Delete Cc_Tmp_Inconsistencia where error=10';
    Begin
      Execute Immediate Lv_Sentencia;
      Commit;
    Exception
      When Others Then
        Lv_Error := Substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;
  
    For i In Plan_Cp Loop
      Begin
        Insert Into Cc_Tmp_Inconsistencia
          (Co_Id, Tmcode, Ch_Estatus, Ch_Fecha, Error)
          Select /*+ rule */
           c.Co_Id, c.Tmcode, Ch.Ch_Status, Ch.Ch_Validfrom, 0
            From Contract_All c, Contract_History Ch
           Where c.Tmcode = i.Tmcode And c.Plcode <> 3 And
                 Ch.Co_Id = c.Co_Id And
                 Ch.Ch_Seqno = (Select Max(Ch_Seqno)
                                  From Contract_History
                                 Where Co_Id = c.Co_Id) And
                 Ch.Ch_Status = 'a'
          Union
          Select /*+ rule */
           c.Co_Id, c.Tmcode, Ch.Ch_Status, Ch.Ch_Validfrom, 0
            From Contract_All c, Contract_History Ch
           Where c.Tmcode = i.Tmcode And c.Plcode <> 3 And
                 Ch.Co_Id = c.Co_Id And
                 Ch.Ch_Seqno = (Select Max(Ch_Seqno)
                                  From Contract_History
                                 Where Co_Id = c.Co_Id) And
                 Ch.Ch_Status <> 'a' And
                 Ch.Ch_Validfrom Between
                 To_Date(Lv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And
                 To_Date(Lv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss');
      
        Commit;
      
        For j In No_Existe Loop
          Update Cc_Tmp_Inconsistencia
             Set Observacion = 'No existe CP', Error = 10
           Where Co_Id = j.Co_Id;
        End Loop;
        Commit;
      
        For k In No_Activo Loop
          Lv_Sentencia := 'CP en estado ' || k.Status;
          Update Cc_Tmp_Inconsistencia
             Set Observacion = Lv_Sentencia, Error = 10
           Where Co_Id = k.Co_Id;
        End Loop;
        Commit;
      
        Lr_Coid := Array_Coid();
        Lr_Obs  := Array_Obs();
      
        For l In Suspend Loop
          If l.Status = 'A' Then
            Lr_Coid.Extend;
            Lr_Obs.Extend;
            Lr_Obs(Lr_Obs.Last) := 'Inconsistencia de estado';
          Elsif Trunc(l.Valid_From_Date) < Trunc(l.Ch_Fecha) Then
            Lr_Coid.Extend;
            Lr_Obs.Extend;
            Lr_Obs(Lr_Obs.Last) := 'Inconsistencia de fechas';
          End If;
        End Loop;
      
        If Lr_Coid.Count > 0 Then
          Forall b In Lr_Coid.First .. Lr_Coid.Last
            Update Cc_Tmp_Inconsistencia
               Set Observacion = Lr_Obs(b), Error = 10
             Where Co_Id = Lr_Coid(b);
        End If;
      
        Lv_Sentencia := 'Delete CC_TMP_INCONSISTENCIA where observacion is null and error=0';
        Begin
          Execute Immediate Lv_Sentencia;
        End;
        Commit;
      
      Exception
        When Others Then
          Lv_Error := Substr(Sqlerrm, 1, 150);
          Exit;
      End;
    End Loop;
  
    If Lv_Error Is Not Null Then
      Pv_Resultado := Lv_Error;
      Return;
    Else
      lr_num:=  array_num();
      Lr_Coid:= Array_Coid();
      For g In (Select co_id From cc_tmp_inconsistencia Where error=20) Loop
          Lr_Coid.Extend;
          lr_num.Extend;
          lr_coid(Lr_Coid.Last):=g.co_id;
          If cck_inconsistencias_fact.ccf_obtiene_numero (lr_coid(Lr_Coid.Last),lr_num(lr_num.Last)) <> 0 Then
             Null;
          End If;
      End Loop;
      
      If lr_num.Count>0 Then 
         Forall k In lr_num.First .. lr_num.Last 
            Update Cc_Tmp_Inconsistencia
               Set Dn_Num = Lr_Num(k)
             Where Co_Id = Lr_Coid(k);
      End If;
      
      Commit;    
      Pv_Resultado := 'Finalizado, ver los resultados en la tabla Cc_Tmp_Inconsistencia con error 10';
    End If;
  
  
  
  Exception
    When Le_Error Then
      Pv_Resultado := 'VALIDA_CP_CONTRATO: ' || Lv_Error;
    When Others Then
      Pv_Resultado := 'VALIDA_CP_CONTRATO: ' || Substr(Sqlerrm, 1, 150);
  End;
  -----------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------
  Procedure Ccp_Valida_Cp_Eliminado(Pv_Periodo   In Varchar2,
                                    Pv_Resultado Out Varchar2) Is
  ---Validaci�n del cargo a paquete eliminado cuando se realiza cambio de plan 
  ---a un nuevo plan que no soporta cargo a paquete.
  
    Cursor Plan_Cp Is
      Select a.Tmcode
        From Mpulktmb a ----, Rateplan b
       Where a.Sncode = 5 And
             a.Vscode =
             (Select Max(Vscode) From Mpulktmb Where Tmcode = a.Tmcode);
  
    Cursor Contratos(Cn_Tmcode Number, Cv_Fecha_Ini Varchar2, Cv_Fecha_Fin Varchar2) Is
      Select x.Co_Id,
             x.Tmcode,
             x.Tmcode_Date,
             x.Seqno
      From   Rateplan_Hist x
      Where  x.Tmcode = Cn_Tmcode And Exists
       (Select 'X'
        From   Rateplan_Hist
        Where  Co_Id = x.Co_Id And Seqno = x.Seqno + 1 And
               Tmcode_Date Between
               To_Date(Cv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') + 86339 / 86400 And
               To_Date(Cv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss') And
               Tmcode Not In
               (Select a.Tmcode
                From   Mpulktmb a
                Where  a.Sncode = 5 And
                       a.Vscode = (Select Max(Vscode)
                                         From   Mpulktmb
                                         Where  Tmcode = a.Tmcode)));
  
   Cursor fee Is 
        Select c.Co_Id,
                   c.Tmcode,
                   b.Accessfee
        From   Mpulktmb b,
                   Cc_Tmp_Inconsistencia c
        Where  c.Tmcode = b.Tmcode And b.Sncode = 5 And
                     b.Vscode = (Select Max(a.Vscode)
                                       From   Mpulktmb a
                                       Where  a.Sncode = 5 And a.Tmcode = c.Tmcode) And  c.Error = 20;
    
    Cursor estatus Is
      Select /*+ rule */
       c.Co_Id,
       h.Ch_Status,
       h.ch_validfrom
      From   Cc_Tmp_Inconsistencia c,
                  Contract_History h
      Where  c.Error = 20 And h.Co_Id = c.Co_Id And
                   h.Ch_Seqno = (Select Max(Ch_Seqno)
                                          From   Contract_History
                                          Where  Co_Id = c.Co_Id) And c.ch_estatus Is Null;

    Cursor c_cargo_paquete (cn_coid Number) Is                                                                                 
      Select  ps.delete_flag
      From Contract_Service    Cs,
           Profile_Service     Ps,
           Pr_Serv_Status_Hist Pst,
           Pr_Serv_Spcode_Hist Psp
      Where Cs.Co_Id = cn_coid And
            Cs.Sncode = 5 And
            Cs.Co_Id = Ps.Co_Id And
            Cs.Sncode = Ps.Sncode And
            Pst.Profile_Id = Ps.Profile_Id And
            Pst.Co_Id = Ps.Co_Id And
            Pst.Sncode = Ps.Sncode And
            Pst.Histno = Ps.Status_Histno And
            Psp.Profile_Id = Ps.Profile_Id And
            Psp.Co_Id = Ps.Co_Id And
            Psp.Sncode = Ps.Sncode And
            Psp.Histno = Ps.Spcode_Histno;
                                                      
                                            
    Ld_Fecha_Fin Date;
    Lv_Error     Varchar2(100);
    Lv_Fecha_Fin Varchar2(20);
    Lv_Fecha_Ini Varchar2(20);
    Le_Error Exception;
    Lv_Sentencia Varchar2(250);
    Type Array_Coid Is Table Of Number;
    Lr_Coid   Array_Coid;
    Type array_num Is Table Of Varchar2(24);
    lr_num array_num;
    
    lv_mensaje       Varchar2(2000);
    lb_found         Boolean;
    lc_cargo_paquete c_cargo_paquete%Rowtype;      
  
  Begin
  
    If Cck_Inconsistencias_Fact.Ccf_Valida_Periodo(Pv_Periodo,
                                                   Lv_Fecha_Ini,
                                                   Lv_Fecha_Fin,
                                                   Lv_Error) <> 0 Then
      Raise Le_Error;
    End If;
  
    Lv_Sentencia := 'Delete Cc_Tmp_Inconsistencia where error=20';
    Begin
      Execute Immediate Lv_Sentencia;
      Commit;
    Exception
      When Others Then
        Lv_Error := Substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;
  
    For i In Plan_Cp Loop
      For x In Contratos(i.Tmcode, Lv_Fecha_Ini, Lv_Fecha_Fin) Loop
        Begin
          Ld_Fecha_Fin := Null;
          lv_mensaje := Null;
          
          Open c_cargo_paquete(x.co_id);
          Fetch c_cargo_paquete Into lc_cargo_paquete;
          lb_found:= c_cargo_paquete%Found;
          Close c_cargo_paquete;
          
          If lb_found Then
            If lc_cargo_paquete.delete_flag Is Null Then
              lv_mensaje:='CP NO ESTA ELIMINADO LOGICAMENTE';
            End If;
          Else    
              lv_mensaje:='CP ESTA ELIMINADO';          
          End If;
          
          If lv_mensaje Is Not Null Then
              
           Select z.Tmcode_Date
            Into Ld_Fecha_Fin
            From Rateplan_Hist z
           Where Co_Id = x.Co_Id And Seqno = x.Seqno + 1;

           Insert Into Cc_Tmp_Inconsistencia
            (Co_Id, Tmcode, Ch_Fecha, Fecha, Observacion, Error)
           Values
            (x.Co_Id,
             x.Tmcode,
             x.Tmcode_Date,
             Ld_Fecha_Fin,
             lv_mensaje,
             20);
          End If; 
          
        Exception
          When Others Then
            Lv_Error := 'VALIDA_CP_ELIMINADO:' || Substr(Sqlerrm, 1, 100);
            Insert Into Cc_Tmp_Inconsistencia
              (Co_Id, Observacion, Error)
            Values
              (x.Co_Id, Lv_Error, -1);
        End;
      End Loop;
      Commit;
    End Loop;
    
    lr_num:=  array_num();
    Lr_Coid:= Array_Coid();
    For g In (Select co_id From cc_tmp_inconsistencia Where error=20) Loop
        Lr_Coid.Extend;
        lr_num.Extend;
        lr_coid(Lr_Coid.Last):=g.co_id;
        If cck_inconsistencias_fact.ccf_obtiene_numero (lr_coid(Lr_Coid.Last),lr_num(lr_num.Last)) <> 0 Then
           Null;
        End If;
    End Loop;
    
    If lr_num.Count>0 Then 
       Forall k In lr_num.First .. lr_num.Last 
          Update Cc_Tmp_Inconsistencia
             Set Dn_Num = Lr_Num(k)
           Where Co_Id = Lr_Coid(k);
    End If;
    
    Commit;    
    
   For i In fee Loop
        Update Cc_Tmp_Inconsistencia
        Set    Observacion = Observacion || '. Valor fee: ' || i.Accessfee
        Where  Co_Id = i.Co_Id And Tmcode = i.Tmcode And Error = 20;
    End Loop;
    
    For i In estatus Loop
        Update cc_tmp_inconsistencia
        Set ch_estatus=i.ch_status,
        observacion=observacion||'. Fecha contrato: '||to_char(i.ch_validfrom,'dd/mm/yyyy hh24:mi:ss')
        Where co_id=i.co_id;
    End Loop;
    
    Commit;          
        
    Pv_Resultado := 'Finalizado, ver los resultados en la tabla Cc_Tmp_Inconsistencia con error 20';
    
  Exception
    When Le_Error Then
      Pv_Resultado := 'VALID_CP_ELIMINADO:' || Lv_Error;
    When Others Then
      Pv_Resultado := 'VALID_CP_ELIMINADO:' || Substr(Sqlerrm, 1, 100);
  End;
  -----------------------------------------------------------------------------------------
  Procedure Ccp_Valida_Feat_Basicos(Pv_Resultado Out Varchar2) Is
  
  ---Validaci�n de los features b�sicos TARIFA BASICA Y SERVICIO DE VOZ para todos los 
  ---contratos hijos
    Cursor Planes Is
      Select * From Rateplan Where (Shdes Like 'TP%' Or Shdes Like 'GS%');
  
    Cursor Basicos(Cn_Tmcode Number) Is
      Select Mp.Sncode
        From Mpulktmb Mp
       Where Mp.Tmcode = Cn_Tmcode And
             Mp.Vscode =
             (Select Max(Vscode) From Mpulktmb Where Tmcode = Cn_Tmcode) And
             Mp.Sncode In ('2', '55', '91');
  
    Cursor No_Existe(Cn_Sncode Number) Is
      Select Cct.Co_Id, Cct.Ch_Fecha
        From Cc_Tmp_Inconsistencia Cct
       Where Cct.Error = 0 And Not Exists
       (Select 'X'
                From Contract_Service
               Where Co_Id = Cct.Co_Id And Sncode = Cn_Sncode);
  
    Cursor No_Activos(Cn_Sncode Number) Is
      Select /*+ rule */
       t.Co_Id, t.Ch_Fecha ---, Ph.Status, Ph.Valid_From_Date
        From Cc_Tmp_Inconsistencia t,
             Profile_Service       p,
             Pr_Serv_Status_Hist   Ph
       Where t.Error = 0 And p.Co_Id = t.Co_Id And p.Sncode = Cn_Sncode And
             Ph.Co_Id = p.Co_Id And Ph.Sncode = p.Sncode And
             Ph.Histno = p.Status_Histno And Ph.Status <> 'A';
  
    Lv_Sentencia Varchar2(200);
    Type Array_Obs Is Table Of Varchar2(150);
    Lr_Obs Array_Obs;
    Type Array_Coid Is Table Of Number;
    Lr_Coid   Array_Coid;
    Lr_Sncode Array_Coid;
    Type Array_Fecha Is Table Of Date;
    Lr_Fecha Array_Fecha;
    Type array_num Is Table Of Varchar2(24);
    lr_num array_num;
  
  Begin
    Lv_Sentencia := 'Delete cc_tmp_inconsistencia where error=30';
    Begin
      Execute Immediate Lv_Sentencia;
    End;
    Commit;
  
    For i In Planes Loop
      Insert Into Cc_Tmp_Inconsistencia
        (Co_Id, Tmcode, Error, Ch_Fecha)
        Select /*+ rule */
         c.Co_Id, c.Tmcode, 0, Ch.Ch_Validfrom
          From Contract_All c, Contract_History Ch
         Where c.Tmcode = i.Tmcode And c.Plcode <> 3 And Ch.Co_Id = c.Co_Id And
               Ch.Ch_Seqno = (Select Max(Ch_Seqno)
                                From Contract_History
                               Where Co_Id = c.Co_Id) And
               Ch.Ch_Status = 'a';
      Commit;
    
      Lr_Coid   := Array_Coid();
      Lr_Obs    := Array_Obs();
      Lr_Sncode := Array_Coid();
      Lr_Fecha  := Array_Fecha();
    
      For b In Basicos(i.Tmcode) Loop
        For j In No_Existe(b.Sncode) Loop
          Lr_Coid.Extend;
          Lr_Obs.Extend;
          Lr_Sncode.Extend;
          Lr_Fecha.Extend;
          Lr_Coid(Lr_Coid.Last) := j.Co_Id;
          Lr_Obs(Lr_Obs.Last) := 'No existe feature b�sico '; --||b.sncode;        
          Lr_Sncode(Lr_Sncode.Last) := b.Sncode;
          Lr_Fecha(Lr_Fecha.Last) := j.Ch_Fecha;
        End Loop;
        For k In No_Activos(b.Sncode) Loop
          Lr_Coid.Extend;
          Lr_Obs.Extend;
          Lr_Sncode.Extend;
          Lr_Fecha.Extend;
          Lr_Coid(Lr_Coid.Last) := k.Co_Id;
          Lr_Obs(Lr_Obs.Last) := 'No tiene activo feature b�sico '; ----||b.sncode;                     
          Lr_Sncode(Lr_Sncode.Last) := b.Sncode;
          Lr_Fecha(Lr_Fecha.Last) := k.Ch_Fecha;
        End Loop;
      End Loop;
    
      If Lr_Coid.Count > 0 Then
        Forall m In Lr_Coid.First .. Lr_Coid.Last
          Insert Into Cc_Tmp_Inconsistencia
            (Co_Id, Tmcode, Ch_Fecha, Sncode, Observacion, Error)
          Values
            (Lr_Coid(m),
             i.Tmcode,
             Lr_Fecha(m),
             Lr_Sncode(m),
             Lr_Obs(m),
             30);
      End If;
    
      --Con esto borro los que no tiene TB pero si tienen TB suspendida
      Delete Cc_Tmp_Inconsistencia
       Where Co_Id In
             (Select v.Co_Id
                From Cc_Tmp_Inconsistencia v
               Where v.Sncode = 2 And v.Error = 30 And v.Tmcode = i.Tmcode And
                     Exists
               (Select 'X'
                        From Pr_Serv_Status_Hist Pss, Profile_Service Ps
                       Where Ps.Co_Id = v.Co_Id And Ps.Co_Id = Pss.Co_Id And
                             Ps.Sncode = 4 And Pss.Histno = Ps.Status_Histno And
                             Ps.Sncode = Pss.Sncode And Pss.Status = 'A'));
      --Fin Borrar                                                       
    
      Lv_Sentencia := 'Delete cc_tmp_inconsistencia where error=0 and observacion is null';
      Begin
        Execute Immediate Lv_Sentencia;
      End;
      Commit;
    End Loop;
    
    lr_num:=  array_num();
    Lr_Coid:= Array_Coid();
    For g In (Select co_id From cc_tmp_inconsistencia Where error=30) Loop
        Lr_Coid.Extend;
        lr_num.Extend;
        lr_coid(Lr_Coid.Last):=g.co_id;
        If cck_inconsistencias_fact.ccf_obtiene_numero (lr_coid(Lr_Coid.Last),lr_num(lr_num.Last)) <> 0 Then
           Null;
        End If;
    End Loop;
    
    If lr_num.Count>0 Then 
       Forall k In lr_num.First .. lr_num.Last 
          Update Cc_Tmp_Inconsistencia
             Set Dn_Num = Lr_Num(k)
           Where Co_Id = Lr_Coid(k);
    End If;
    Commit;
    pv_resultado:='Finalizado, ver los resultados en la tabla Cc_Tmp_Inconsistencia con error 30';
  Exception
    When Others Then
      Pv_Resultado := 'VALID_FEAT_BASICOS:' || Substr(Sqlerrm, 1, 200);
  End;

  -----------------------------------------------------------------------------------------                             
  Procedure Ccp_Doble_Seguro_Equipo(Pv_Periodo   In Varchar2,
                                    Pv_sncode   In number,
                                    Pv_Resultado Out Varchar2) Is
    ---Diana Chonillo 02/09/04
    ---Procedimiento para verificar los seguros de equipo que se van a cobrar doble en un periodo
    ---Los datos son guardados en la tabla es_registros con codigo_error=50
    ---Consultar:       
    /*Select telefono, customer_id co_id_ultimo,  status status_ultimo,fecha fecha_ultima, 
    co_id co_id_anterior, observacion status_anterior,  fecha_axis_ini fecha_anterior  
    From  es_registros Where codigo_error=50 
    Order By telefono, fecha_anterior*/
  
    Cursor Seguro_Equipo(Cv_Fecha_Ini Varchar2, Cv_Fecha_Fin Varchar2) Is
      Select Distinct a.Co_Id,
                      a.Histno,
                      a.Valid_From_Date,
                      b.Dn_Id,
                      Dn.Dn_Num
        From Pr_Serv_Status_Hist a,
             Contr_Services_Cap  b,
             Directory_Number    Dn
--       Where a.Sncode =29 And a.Status = 'A' And
         Where a.Sncode =Pv_sncode And a.Status = 'A' And --mqu
             a.Valid_From_Date Between
             To_Date(Cv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And
             To_Date(Cv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss') And
             b.Co_Id = a.Co_Id And
             a.Valid_From_Date Between b.Cs_Activ_Date And
             Nvl(b.Cs_Deactiv_Date,
                 To_Date(Cv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss')) And
             Dn.Dn_Id = b.Dn_Id;
  
    Cursor Inactivado(Cn_Coid Number, Fecha Date) Is
      Select Pss.Valid_From_Date, Pss.Histno
        From Pr_Serv_Status_Hist Pss
--       Where Pss.Co_Id = Cn_Coid And Pss.Sncode =29 And
       Where Pss.Co_Id = Cn_Coid And Pss.Sncode =Pv_sncode And  --mqu
             Trunc(Pss.Valid_From_Date) = Fecha And Pss.Status = 'D';
  
    Cursor Estado_Actual(Cn_Coid Number) Is
      Select Pss.Valid_From_Date, Pss.Status
        From Pr_Serv_Status_Hist Pss, Profile_Service Ps
       Where Ps.Co_Id = Cn_Coid And Ps.Co_Id = Pss.Co_Id And
             Pss.Histno = Ps.Status_Histno And Ps.Sncode = Pss.Sncode And
--             Ps.Sncode = 29;
               Ps.Sncode = Pv_sncode;   --mqu
  
    Cursor Coid_Ante(Cn_Dnid Number, Cn_Coid Number, Ld_Fecha_Ini Date) Is
      Select Cse.Co_Id
        From Contr_Services_Cap Cse
       Where Cse.Dn_Id = Cn_Dnid And Cse.Co_Id <> Cn_Coid And
             Cse.Cs_Deactiv_Date Is Not Null And Not Exists
       (Select *
                From Contr_Services_Cap Csc
               Where Csc.Co_Id = Cse.Co_Id And Csc.Dn_Id <> Cn_Dnid And
                     Csc.Main_Dirnum = 'X' And
                     Nvl(Csc.Cs_Deactiv_Date, Ld_Fecha_Ini) >= Ld_Fecha_Ini);
  
    Cursor Otro_Seguro(Cn_Coid Number, Cv_Fecha_Ini Varchar2, Cv_Fecha_Fin Varchar2) Is
      Select Pss.Co_Id, Pss.Valid_From_Date, Pss.Status
        From Pr_Serv_Status_Hist Pss
--       Where Pss.Co_Id = Cn_Coid And Pss.Sncode =29 And Pss.Status = 'D' And
         Where Pss.Co_Id = Cn_Coid And Pss.Sncode =Pv_sncode  And Pss.Status = 'D' And
             Pss.Valid_From_Date >
             To_Date(Cv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And Not Exists
       (Select 'X'
                From Pr_Serv_Status_Hist h
               Where h.Co_Id = Pss.Co_Id And h.Sncode = Pss.Sncode And
                     h.Status = 'A' And
                     Trunc(h.Valid_From_Date) = Trunc(Pss.Valid_From_Date) And
                     h.Histno < Pss.Histno)
      Union
      Select Pss.Co_Id, Pss.Valid_From_Date, Pss.Status
        From Pr_Serv_Status_Hist Pss, Profile_Service Ps
       Where Ps.Co_Id = Cn_Coid And Ps.Co_Id = Pss.Co_Id And
             Pss.Histno = Ps.Status_Histno And Ps.Sncode = Pss.Sncode And
--             Ps.Sncode =29 And Pss.Status = 'A' And
             Ps.Sncode =Pv_sncode  And Pss.Status = 'A' And
             Pss.Valid_From_Date Between
             To_Date(Cv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And
             To_Date(Cv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss');
  
    Cursor Status_Ante(Ln_Coid Number, Lv_Status Varchar2, Cv_Fecha_Ini Varchar2, Cv_Fecha_Fin Varchar2) Is
      Select 'X'
        From Pr_Serv_Status_Hist
--       Where Co_Id = Ln_Coid And Status = Lv_Status And Sncode =29 And
       Where Co_Id = Ln_Coid And Status = Lv_Status And Sncode =Pv_sncode  And
             Valid_From_Date Between
             To_Date(Cv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And
             To_Date(Cv_Fecha_Fin, 'dd/mm/yyyy hh24:mi:ss') And Rownum = 1;
  
    ---cur_telefono                telefono%Rowtype;
    Cur_Inactivado  Inactivado%Rowtype;
    Cur_Otro_Seguro Otro_Seguro%Rowtype;
    ---cur_ultimo                  ultimo%Rowtype;
    Ln_Coid_Actual   Number;
    Ln_Coid_Ante     Number;
    Lv_Status_Actual Varchar2(1);
    Lv_Status_Ante   Varchar2(1);
    Ld_Fecha_Actual  Date;
    Ld_Fecha_Ante    Date;
    Lb_Found         Boolean;
    Lv_Telefono      Varchar2(24);
    Ln_Dn_Id         Number;
    Ld_Fecha         Date;
    Ld_Fecha_p       Date;
    Lb_Band          Boolean;
    Lv_Error         Varchar2(50);
    Le_Error Exception;
    Lv_Fecha_Fin Varchar2(20);
    Lv_Fecha_Ini Varchar2(20);
    Lv_Sentencia Varchar2(50);
    Ld_Fecha_Ini Date;
    Le_Siguiente Exception;
    Lv_Revisa Varchar2(1);
  
  Begin
    --Seguro_equipo, revisa todos los seguros de equipo en estado A,S,D en el periodo
  
  
    Lv_Sentencia := 'Delete Cc_Tmp_Inconsistencia where error=50';
    Begin
      Execute Immediate Lv_Sentencia;
      Commit;
    Exception
      When Others Then
        Lv_Error := Substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;
  
    if Pv_sncode = 29 then
              insert into cc_temporal values ('I-SEGURO OP29',NULL,null,null,null,sysdate); commit;
    else 
        IF  Pv_sncode = 169 then
              insert into cc_temporal values ('I-SEGURO OP169',NULL,null,null,null,sysdate); commit;    
        else 
              insert into cc_temporal values ('I-SEGURO OP211',NULL,null,null,null,sysdate); commit;    
        end if;
     end if;
    
    If Cck_Inconsistencias_Fact.Ccf_Valida_Periodo(Pv_Periodo,
                                                   Lv_Fecha_Ini,
                                                   Lv_Fecha_Fin,
                                                   Lv_Error) <> 0 Then
      Raise Le_Error;
    End If;
  
    Ld_Fecha_Ini := To_Date(Lv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss');
  
    For i In Seguro_Equipo(Lv_Fecha_Ini, Lv_Fecha_Fin) Loop
      Ln_Coid_Actual := i.Co_Id;
      Ld_Fecha_p     := Trunc(i.Valid_From_Date);
      Ld_Fecha       := i.Valid_From_Date;
      Lv_Telefono    := i.Dn_Num;
      Ln_Dn_Id       := i.Dn_Id;
    
      Open Inactivado(Ln_Coid_Actual, Ld_Fecha_p);
      Fetch Inactivado
        Into Cur_Inactivado;
      Lb_Found := Inactivado%Found;
      Close Inactivado;
    
      If Lb_Found Then
        If Cur_Inactivado.Histno > i.Histno Then
          Lb_Band := False;
        Else
          Lb_Band := True;
        End If;
      Else
        Lb_Band := True;
      End If;
    
      If Lb_Band Then
        Open Estado_Actual(Ln_Coid_Actual);
        Fetch Estado_Actual
          Into Ld_Fecha_Actual, Lv_Status_Actual;
        Close Estado_Actual;
      
        For k In Coid_Ante(Ln_Dn_Id, Ln_Coid_Actual, Ld_Fecha_Ini) Loop
          --Verifico si los co_id anteriores han tenido seguro de equipo en el periodo  
          Begin
            Open Otro_Seguro(k.Co_Id, Lv_Fecha_Ini, Lv_Fecha_Fin);
            Fetch Otro_Seguro
              Into Cur_Otro_Seguro;
            Lb_Found := Otro_Seguro%Found;
            Close Otro_Seguro;
          
            If Lb_Found Then
              Ln_Coid_Ante   := Cur_Otro_Seguro.Co_Id;
              Ld_Fecha_Ante  := Cur_Otro_Seguro.Valid_From_Date;
              Lv_Status_Ante := Cur_Otro_Seguro.Status;
            
              If Trunc(Ld_Fecha_Ante) = Trunc(Ld_Fecha_Ini) And
                 Lv_Status_Ante = 'D' Then
                If Ld_Fecha_p = Trunc(Ld_Fecha_Ini) Then
                  Raise Le_Siguiente;
                End If;
              End If;
            
              If Lv_Status_Ante = 'D' Then
                Lv_Revisa := Null;
                Open Status_Ante(Ln_Coid_Ante,
                                 'S',
                                 Lv_Fecha_Ini,
                                 Lv_Fecha_Fin);
                Fetch Status_Ante
                  Into Lv_Revisa;
                Close Status_Ante;
              
                If Lv_Revisa = 'X' Then
                  Lv_Revisa := Null;
                  Open Status_Ante(Ln_Coid_Ante,
                                   'A',
                                   Lv_Fecha_Ini,
                                   Lv_Fecha_Fin);
                  Fetch Status_Ante
                    Into Lv_Revisa;
                  Close Status_Ante;
                
                  If Lv_Revisa Is Null Then
                    Raise Le_Siguiente;
                  End If;
                End If;
              End If;
            
              Insert Into Cc_Tmp_Inconsistencia
                (Error,
                 Co_Id,
                 Sncode,
                 Ch_Estatus,
                 Estatus,
                 Ch_Fecha,
                 Fecha,
                 Dn_Num,
                 Observacion)
              Values
                (50,
                 Ln_Coid_Actual,
                 Ln_Coid_Ante,
                 Lv_Status_Actual,
                 Lv_Status_Ante,
                 Ld_Fecha_Actual,
                 Ld_Fecha_Ante,
                 Lv_Telefono,
                 'Doble seguro de equipo');
            End If;
            Commit;                  
          Exception
            When Le_Siguiente Then
              Null;
            When Others Then
              Lv_Error := Ln_Coid_Actual || ' ' || Substr(Sqlerrm, 1, 200);
              Raise Le_Error;
          End;
        End Loop;
      End If;
    End Loop;
    Pv_Resultado := 'Finalizado, ver los resultados en la tabla Cc_Tmp_Inconsistencia con error 50';
    
    if Pv_sncode = 29 then
              insert into cc_temporal values ('F-SEGURO OP29',NULL,null,null,null,sysdate); commit;
    else 
        IF  Pv_sncode= 169 then
              insert into cc_temporal values ('F-SEGURO OP169',NULL,null,null,null,sysdate); commit;    
        else 
              insert into cc_temporal values ('F-SEGURO OP211',NULL,null,null,null,sysdate); commit;    
        end if;
     end if;
            
  Exception
    When Le_Error Then
      Pv_Resultado := Lv_Error;
    When Others Then
      Pv_Resultado := Substr(Sqlerrm, 1, 200);
    
  End;

  Procedure Ccp_Fechas_Traslapadas(Pv_code   In number, Pv_Resultado Out Varchar2) Is

  ---validaci�n de fechas traslapadas en los servicios a nivel de contrato maestro
  
    Cursor Contratos Is
      Select /*+ rule +*/
       Ps.Co_Id,
       Ps.Sncode,
       Ps.Status_Histno,
       Pss.Status,
       Pss.Valid_From_Date
        From Contract_All Co, Profile_Service Ps, Pr_Serv_Status_Hist Pss
       Where Plcode = Pv_code And Ps.Co_Id = Co.Co_Id And Ps.Co_Id = Pss.Co_Id And
             Pss.Histno = Ps.Status_Histno And Ps.Profile_Id = 0 And
             Ps.Sncode = Pss.Sncode;
  --3 PARA CUENTA ---1 PARA SERVICIOS
    Cursor Sncode_Hist(Cn_Coid Number, Cn_Sncode Number, Cn_Histno Number, Cd_Date Date) Is
      Select h.Histno, h.Status, h.Valid_From_Date
        From Pr_Serv_Status_Hist h
       Where Co_Id = Cn_Coid And Sncode = Cn_Sncode And
             h.Histno < Cn_Histno And h.Valid_From_Date >= Cd_Date And
             h.Status <> 'O';
  
  lv_sentencia        Varchar2(100);
  lv_error            Varchar2(200);
  le_error            Exception;
  Type Array_Coid Is Table Of Number;
  Lr_Coid   Array_Coid;
  Type array_num Is Table Of Varchar2(24);
  lr_num array_num;
      
  Begin

    Lv_Sentencia := 'Delete Cc_Tmp_Inconsistencia where error=40';
    Begin
      Execute Immediate Lv_Sentencia;
      Commit;
    Exception
      When Others Then
        Lv_Error := Substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;
  
    if Pv_code = 3 then
              insert into cc_temporal values ('I-FECHA OP3',NULL,null,null,null,sysdate); commit;
    else
              insert into cc_temporal values ('I-FECHA OP1',NULL,null,null,null,sysdate); commit;    
    end if;
    
    For i In Contratos Loop
      For x In Sncode_Hist(i.Co_Id,
                           i.Sncode,
                           i.Status_Histno,
                           i.Valid_From_Date) Loop
          Insert Into Cc_Tmp_Inconsistencia
            (Co_Id,
             Tmcode,
             Ch_Estatus,
             Ch_Fecha,
             Estatus,
             Fecha,
             Observacion,
             Error)
          Values
            (i.Co_Id,
             i.Sncode,
             i.Status,
             i.Valid_From_Date,
             x.Status,
             x.Valid_From_Date,
             'Fechas traslapadas',
             40);
      End Loop;
    End Loop;
    Commit;

    lr_num:=  array_num();
    Lr_Coid:= Array_Coid();
    For g In (Select co_id From cc_tmp_inconsistencia Where error=40) Loop
        Lr_Coid.Extend;
        lr_num.Extend;
        lr_coid(Lr_Coid.Last):=g.co_id;
        If cck_inconsistencias_fact.ccf_obtiene_numero (lr_coid(Lr_Coid.Last),lr_num(lr_num.Last)) <> 0 Then
           Null;
        End If;
    End Loop;
    
    If lr_num.Count>0 Then 
       Forall k In lr_num.First .. lr_num.Last 
          Update Cc_Tmp_Inconsistencia
             Set Dn_Num = Lr_Num(k)
           Where Co_Id = Lr_Coid(k);
    End If;
    Commit;
    
   if Pv_code = 3 then
              insert into cc_temporal values ('F-FECHA OP3',NULL,null,null,null,sysdate); commit;
   else
              insert into cc_temporal values ('F-FECHA OP1',NULL,null,null,null,sysdate); commit;    
   end if;

        
    Pv_Resultado := 'Finalizado, ver los resultados en la tabla Cc_Tmp_Inconsistencia con error 40';
    
    
  Exception
    When le_error Then
      Pv_Resultado := 'Fechas Traslapadas :' || lv_error;
    When Others Then
      Pv_Resultado := 'Fechas Traslapadas :' || Substr(Sqlerrm, 1, 150);
  End;

-----------------------------------------------------------------------------------------------------------------------------------------
  Procedure Ccp_Valida_Ies (pv_periodo In Varchar2, 
                                            Pv_Resultado Out Varchar2) Is 
                                          
  Cursor c_sncodeies Is 
   Select Distinct c.Sn_Code
   From   Cl_Planes_Ies@Axis a, Cl_Tipos_Detalles_Servicios@Axis b, Bs_Servicios_Paquete@Axis c
   Where  a.Producto In ('TAR', 'AUT') And
          a.Tipo_Plan = 'C' And
          b.Id_Tipo_Detalle_Serv = a.Id_Plan And
          b.Id_Grupo_Tipo_Det_Servicio =
          b.Id_Subproducto || '-SERVIC' And
          c.Cod_Axis = Substr(a.Id_Plan, 5) And
          c.Sn_Code > -1 ;


  Type ar_num Is Table Of Number;          
  Type ar_date Is Table Of Date;
  lr_sncode            ar_num:=ar_num(); 
  lr_coid                ar_num:=ar_num();
  lr_fecha              ar_date:=ar_date();
  
  le_except          Exception;
  le_error            Exception;
  lv_fecha_ini       Varchar2(25);--:='24/05/2005';
  lv_fecha_fin       Varchar2(25);--:='24/06/2005';
  lv_error             Varchar2(300);
  
          
  Begin 
  
  
    If Cck_Inconsistencias_Fact.Ccf_Valida_Periodo(Pv_Periodo,
                                                                          Lv_Fecha_Ini,
                                                                          Lv_Fecha_Fin,
                                                                          Lv_Error) <> 0 Then
      Raise Le_Error;
    End If;
  
  insert into cc_temporal values ('I-VALIDAIES',NULL,null,null,null,sysdate); commit;
      
  --Cursor para obtener los sncode de ies 
    Open c_sncodeies;
    Fetch c_sncodeies Bulk Collect Into lr_sncode;
    Close c_sncodeies;
    
    If lr_sncode.Count  <=0 Then
       Raise le_except;
    End If;
  
    For i In lr_sncode.First .. lr_sncode.Last Loop
      Begin   
      --Obtiene los contratos activos con sncode ies lr_sncode(i)
        Select g.Co_Id, g.Valid_From_Date Bulk Collect
        Into   Lr_Coid, Lr_Fecha
        From   Pr_Serv_Status_Hist g, Profile_Service h
        Where g.Sncode = Lr_Sncode(i) And 
            g.Valid_From_Date Between To_Date(Lv_Fecha_Ini, 'dd/mm/yyyy hh24:mi:ss') And 
            To_Date(Lv_Fecha_Fin,'dd/mm/yyyy hh24:mi:ss') And 
            g.Status = 'A' And 
            g.Profile_Id = 0 And 
            h.Co_Id = g.Co_Id And 
            h.Sncode = g.Sncode And 
            h.Status_Histno = g.Histno And 
            h.Delete_Flag Is Null And 
            h.Profile_Id = g.Profile_Id;
        
        If lr_coid.Count <=0  Then
         Raise le_except;
        End If;
             
        Forall x In lr_coid.First .. lr_coid.Last 
          Insert Into Cc_Tmp_Inconsistencia
            (Co_Id,
             Sncode,
             Fecha,
             error )
          Values
            (Lr_Coid(x),
             Lr_Sncode(i),
             Lr_Fecha(x),
             6);
             
        lr_coid.Delete;
        lr_fecha.Delete;     
        Commit;
        --Obtiene el n�mero de tel�fono       
        Update Cc_Tmp_Inconsistencia d
        Set    Dn_Num = (Select DECODE (Substr(e.Dn_Num, 4,1),'8',Substr(e.Dn_Num, -8),Substr(e.Dn_Num, -7))
                                   From  Contr_Services_Cap f,Directory_Number e
                                   Where  f.Co_Id = d.Co_Id And
                                               f.Cs_Deactiv_Date Is Null And
                                               f.Dn_Id = e.Dn_Id) Where error=6;             
        
        --Obtiene los contratos que si tienen activos IES en AXIS y se los descarta
        Select t.Co_Id 
        Bulk Collect Into   Lr_Coid
        From   Cl_Detalles_Servicios@Axis s, Cc_Tmp_Inconsistencia t
        Where  t.Error = 6 And t.Dn_Num = s.Id_Servicio And
                Estado = 'A' And
                Id_Tipo_Detalle_Serv In
                (Select a.Id_Plan
                 From   Cl_Planes_Ies@Axis a, Cl_Tipos_Detalles_Servicios@Axis b
                 Where  a.Producto In ('TAR', 'AUT') And
                        a.Tipo_Plan = 'C' And
                        b.Id_Tipo_Detalle_Serv = a.Id_Plan And
                        b.Id_Grupo_Tipo_Det_Servicio =  b.Id_Subproducto || '-SERVIC');
                                
         If lr_coid.Count <=0 Then
          Raise le_except;
         End If;                       
                 
         Forall y In lr_coid.First .. lr_coid.Last 
           Delete cc_tmp_inconsistencia Where co_id=lr_coid(y) And error=6;                     
          
         Update cc_tmp_inconsistencia Set error=60, observacion='IES Activo en BSCS pero no en AXIS' Where error=6;
         lr_coid.Delete;  
         Commit;  
  
       Exception    
         When le_except Then
           Null;        
         When Others Then
           pv_resultado:='Error en sncode '||lr_sncode(i)||': '||Sqlerrm;
           Exit;                                      
       End;                                               
     End Loop;
     
     lr_sncode.Delete;
     Commit;

       insert into cc_temporal values ('F-VALIDAIES',NULL,null,null,null,sysdate); commit;
     
  Exception
    When Le_Error Then 
       pv_resultado:=lv_error;
    When Others Then 
       pv_resultado:='Error en general : '||Sqlerrm;       
  End;                                            
  
End Cck_Inconsistencias_Fact;
/

