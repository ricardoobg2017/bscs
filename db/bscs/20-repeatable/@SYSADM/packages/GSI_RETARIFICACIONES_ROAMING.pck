CREATE OR REPLACE PACKAGE GSI_RETARIFICACIONES_ROAMING IS

  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Realiza y controla el proceso de retarificacion en GPRS y SMS
  --=====================================================================================--
  -- Desarrollado por:  SUD. Diana Mero Morales
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 29/06/2012
  -- Proyecto: [6071] Mejoras al Servicios Internacional Roaming
  -- PV_CODIGOERROR 0 => Exito
  -- PV_CODIGOERROR 1 => Error de ingreso de datos
  -- PV_CODIGOERROR 2 => Error inesperado
  --=====================================================================================--
  
  CURSOR C_SIN_FONO IS
   SELECT A.ROWID, A.* 
     FROM GSI_ACTIVACIONES_PAQPORT A
    WHERE A.ESTADO = 'N';
   
  CURSOR C_VERIF_PROCESADO(CV_ID_PROCESO VARCHAR2, CD_FECHA_CORTE DATE)IS
   SELECT 'S' 
     FROM GSI_PROCESOS_BITACORA_ROA R
    WHERE R.FECHA_CORTE = CD_FECHA_CORTE
      AND R.ID_PROCESO  = CV_ID_PROCESO
      AND R.ESTADO      = 'PROCESADO';

  CURSOR C_MAX_FECH_PROCESADO(CV_ID_PROCESO VARCHAR2)IS
   SELECT MAX(R.FECHA_CORTE)
     FROM GSI_PROCESOS_BITACORA_ROA R
    WHERE R.ID_PROCESO=CV_ID_PROCESO
      AND R.ESTADO='PROCESADO';
 
  CURSOR C_GET_COID(CV_ID_SERVICIO VARCHAR2)IS    
   SELECT CO_ID
     FROM (SELECT A.DN_NUM, SC.CO_ID, MAX(SC.CS_ACTIV_DATE) INI
             FROM DIRECTORY_NUMBER A, CONTR_SERVICES_CAP SC
            WHERE A.DN_ID = SC.DN_ID
              AND A.DN_NUM = CV_ID_SERVICIO
            GROUP BY A.DN_NUM, SC.CO_ID
            ORDER BY INI DESC)
    WHERE ROWNUM = 1;
    
  CURSOR C_OBTIENE_RANGO_DIAS(CV_DIA VARCHAR2)IS
   SELECT C.DIA_INICIO,C.DIA_FIN,C.HORA_INICIO,C.HORA_FIN,C.INI_MES_ANT
     FROM GSI_CICLOS_CALCULO_ROAMING C
    WHERE C.DIA_CORTE = CV_DIA
      AND C.ESTADO = 'A';

  CURSOR C_GET_CUSTOMER_ID(CN_COID NUMBER)IS
   SELECT CUSTOMER_ID 
     FROM CONTRACT_ALL
    WHERE CO_ID = CN_COID;
   
  CURSOR C_INFO_OPERADORA(CV_OPERADORA VARCHAR2) IS
   SELECT CONTINENTE
     FROM GSI_MAESTRA_OPERADORAS
    WHERE TAP_SOURCE_CODE = CV_OPERADORA;

  CURSOR C_FEATURES_SERVICES (CV_SERVICIO VARCHAR2)IS
   SELECT * 
     FROM GSI_PAQROAMING_ACTIVADOS AC
    WHERE AC.ID_SERVICIO = CV_SERVICIO
    ORDER BY AC.FECHA_DESDE;
     
  CURSOR C_FEATURES_TMP(CN_NUM_FEAT_ACTUAL NUMBER)IS
   SELECT * 
     FROM GSI_FEATURES_TMP T 
    WHERE T.NUM_FEATURE = NVL(CN_NUM_FEAT_ACTUAL,T.NUM_FEATURE);
    
  CURSOR C_MAESTRA_ROAMING(CV_FEATURE VARCHAR2) IS
   SELECT M.*
     FROM GSI_MAESTRA_FEATURE_ROAMGRX M
    WHERE M.FEATURE = CV_FEATURE;
  
  --Almacenar mas de un registro   
  TYPE VAR_CURSOR IS REF CURSOR;
  --
    
  TYPE RECORD_UDR IS RECORD( ROWID               VARCHAR2(100),
                             FOLLOW_UP_CALL_TYPE NUMBER ,
                             EXPORT_FILE         VARCHAR2(20),
                             S_P_PORT_ADDRESS    VARCHAR2(100));
  
  TYPE RECORD_UDR_2 IS RECORD( S_P_NUMBER_ADDRESS  VARCHAR2(100),
                               O_P_NUMBER_ADDRESS  VARCHAR2(100),
                               S_P_PORT_ADDRESS    VARCHAR2(100),
                               FOLLOW_UP_CALL_TYPE NUMBER ,
                               EXPORT_FILE         VARCHAR2(100),
                               ROWID               VARCHAR2(100));
  
  CURSOR C_PARAMETROS(CV_TIPO_PARAM VARCHAR2,CV_PARAM VARCHAR2) IS
   SELECT P.VALOR
     FROM GV_PARAMETROS@RTX_TO_BSCS_LINK P
    WHERE P.ID_TIPO_PARAMETRO = CV_TIPO_PARAM
      AND P.ID_PARAMETRO      = CV_PARAM;
    
PROCEDURE GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          IN VARCHAR2,
                                      PV_ESTADO              IN VARCHAR2,
                                      PD_FECHA_CORTE         IN DATE,
                                      PD_FECHA_PROCESAMIENTO IN DATE,
                                      PV_REPROCESO           IN VARCHAR2,
                                      PV_NOMBRE_PROCESO      IN VARCHAR2,
                                      PV_ERROR               IN OUT VARCHAR2);
    
PROCEDURE GSI_GET_RANGO_DIAS(PD_FECHA_CORTE   IN DATE,
                             PD_FECHA_INI     OUT DATE,
                             PD_FECHA_FIN     OUT DATE,   
                             PB_INTERNACIONAL IN BOOLEAN,                          
                             PV_ERROR         OUT VARCHAR2);

PROCEDURE GSI_GET_FECHA_CORTE_ANT(PD_FECHA_CORTE      DATE,
                                  PD_FECHA_CORTE_ANT  OUT DATE,
                                  PV_ERROR            OUT VARCHAR2);

PROCEDURE GSI_GET_FECHA_CORTE_SIG(PD_FECHA_CORTE      DATE,
                                  PD_FECHA_CORTE_SIG  OUT DATE,
                                  PV_ERROR            OUT VARCHAR2) ;

PROCEDURE GSI_CONTROL_PROCESOS(PV_ID_PROCESO    VARCHAR2,
                               PD_FECHA_CORTE   DATE,
                               PV_REPROCESO     VARCHAR2,
                               PV_COD_ERROR     OUT VARCHAR2,
                               PV_ERROR         OUT VARCHAR2);


PROCEDURE GSI_DEPURA_INFO_ACTIVACIONES(PD_FECHA_CORTE  DATE,
                                       PV_REPROCESO    VARCHAR2,   
                                       PV_ERROR        OUT VARCHAR2);
 --GPRS                                 
PROCEDURE GSI_PROCESA_GPRS_PRINCIPAL(PD_FECHA_CORTE       IN DATE,
                                     PV_REPROCESO         IN VARCHAR2,
                                     PV_ERROR             OUT VARCHAR2);
                                 
PROCEDURE GSI_CARGA_CONSUMO_GPRS(PD_FECHA_CORTE IN DATE,
                                 PV_REPROCESO   IN VARCHAR2,
                                 PV_ACCION      IN OUT VARCHAR2,
                                 PV_COD_ERROR   OUT VARCHAR2,
                                 PV_ERROR       OUT VARCHAR2);
    
    
PROCEDURE GSI_RETARIFICA_GPRS(PD_FECHA_CORTE  DATE,
                              PV_ERROR        OUT VARCHAR2);

PROCEDURE GSI_RETARIFICA_GPRS_V2(PD_FECHA_CORTE  DATE,
                                 PV_ERROR        OUT VARCHAR2);
 
PROCEDURE GSI_BUSCAR_FEATURES(PN_NUM_FEAT_ACTUAL     IN OUT NUMBER,
                              PN_NUM_FEATURES        IN NUMBER,
                              PN_CONSUMO_BYTES       IN OUT NUMBER,
                              PD_FECHA_CONSUMO       IN DATE,
                              PV_REMARK_ADICIONAL    IN VARCHAR2,
                              PR_UDR                 IN OUT GSI_OBJ_RETARIFICACIONES.RECORD_UDR,
                              PN_COSTO_BYTE          IN OUT NUMBER,
                              PB_FEAT_FOUND          OUT BOOLEAN,
                              PN_KBREST              IN OUT NUMBER,
                              PC_FEATURES_TMP        OUT C_FEATURES_TMP%ROWTYPE,
                              PB_EXIT                OUT BOOLEAN,                              
                              PV_ERROR               OUT VARCHAR2) ;

PROCEDURE GSI_SET_EVENTOS_RMUNDO(PN_NUM_FEAT_ACTUAL     NUMBER,
                                 PN_NUM_FEATURES        NUMBER,
                                 PN_DATA_VOLUME_NEW     NUMBER,
                                 PD_FECHA_CONSUMO       DATE,
                                 PD_FEC_HASTA_FEAT_ACT  DATE,
                                 PV_REMARK_PAQUETE      IN VARCHAR2,
                                 PV_REMARK_EVENTOS      IN VARCHAR2,
                                 PV_REMARK_ADICIONALES  IN VARCHAR2,
                                 PR_UDR                 IN OUT GSI_OBJ_RETARIFICACIONES.RECORD_UDR,
                                 PN_COSTO_BYTE          NUMBER,
                                 PV_ERROR               OUT VARCHAR2);

PROCEDURE GSI_ACTUALIZA_UDR_MENSUAL(PD_FECHA_CORTE  DATE,
                                    PV_COD_ERROR    OUT VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2);
    
PROCEDURE GSI_PROCESA_SMS_PRINCIPAL(PD_FECHA_CORTE       IN  DATE,
                                    PV_REPROCESO         IN  VARCHAR2,
                                    PV_CODIGO_ERROR      OUT VARCHAR2,                                     
                                    PV_ERROR             OUT VARCHAR2);
    
PROCEDURE GSI_RETARIFICA_SMS(PD_FECHA_CORTE       DATE,
                             PV_RANGO_FECH_CORTE  VARCHAR2,
                             PV_FECHA_NEW_ESQ     VARCHAR2,
                             PV_FORMULA_FECH      VARCHAR2,
                             PV_TABLA             VARCHAR2,
                             PB_UPDATE_ACTI       BOOLEAN,
                             PV_ERROR             OUT VARCHAR2);
                              
PROCEDURE GSI_ACTUALIZA_PAQUETE_RECURR(PD_FECHA  DATE,
                                       PV_ERROR  OUT VARCHAR2);                              

PROCEDURE GSI_CLASIFICA_PRODUCTO_GPR(PV_ERROR OUT VARCHAR2);

PROCEDURE GSI_REGISTROS_DUPLI(PD_FECHA_CORTE   IN DATE, 
                              PV_FECHA_NEW_ESQ IN VARCHAR2,
                              PV_FORMULA_FECHA IN VARCHAR2,
                              PD_FECHA_INI     IN DATE,
                              PD_FECHA_FIN     IN DATE,
                              PV_TIPO          IN VARCHAR2,
                              PV_ERROR         OUT VARCHAR2) ;

PROCEDURE GSI_DEPURA_SMS_SIN_COSTO(PV_TABLA             IN VARCHAR2,
                                   PV_FORMULA_FECHA     IN VARCHAR2,
                                   PV_FECHA_NEW_ESQ     IN VARCHAR2, 
                                   PD_FECHA_INI         IN DATE,
                                   PD_FECHA_FIN         IN DATE,
                                   PV_ERROR             OUT VARCHAR2);

PROCEDURE GSI_CONTROL_TRX(PV_ERROR OUT VARCHAR2);
                                    
PROCEDURE GSI_CLASIFICA_EVEN_ADIC(PV_IDSERVICIO        IN VARCHAR2,
                                  PV_REMARK_EVENTOS    IN VARCHAR2,
                                  PV_REMARK_ADICIONAL  IN VARCHAR2,
                                  PV_COSTO_EVEN_AM     IN VARCHAR2,
                                  PV_COSTO_EVEN_RM     IN VARCHAR2,
                                  PV_ROWID             IN VARCHAR2,
                                  PV_ZONA_OPERADORA    IN VARCHAR2, 
                                  PD_FECHA_CONSUMO     IN DATE,
                                  PN_DATA_VOLUMEN      IN NUMBER,
                                  PN_KBREST            IN OUT NUMBER,
                                  PB_ADICIONALES       OUT BOOLEAN,
                                  PV_ERROR             OUT VARCHAR2);
                                  
PROCEDURE GSI_ARMA_MENSAJE(PV_MENSAJE IN OUT VARCHAR2,
                           PV_ERROR   OUT VARCHAR2) ;
                           
END GSI_RETARIFICACIONES_ROAMING;
/
CREATE OR REPLACE PACKAGE BODY GSI_RETARIFICACIONES_ROAMING is

--*====================================================================**--
--* CREADO POR : SUD. Diana Mero Morales
--* FECHA DE CRACION: 20/06/2012
--* OBJETIVO: Realiza y controla el proceso de retarificacion en GPRS y SMS
--*  PV_CODIGOERROR 0 => Exito
--*  PV_CODIGOERROR 1 => Error de ingreso de datos
--*  PV_CODIGOERROR 2 => Error inesperado
--*====================================================================**--

/***************************************************************
*Bitacoriza los procesos ejecutados para la retarificacion
***************************************************************/
PROCEDURE GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          IN VARCHAR2,
                                      PV_ESTADO              IN VARCHAR2,
                                      PD_FECHA_CORTE         IN DATE,
                                      PD_FECHA_PROCESAMIENTO IN DATE,
                                      PV_REPROCESO           IN VARCHAR2,
                                      PV_NOMBRE_PROCESO      IN VARCHAR2,
                                      PV_ERROR               IN OUT VARCHAR2)IS

 PRAGMA AUTONOMOUS_TRANSACTION;
 
 CURSOR C_DESCRIPCION_PROCESO(CV_ID_PROCESO VARCHAR2) IS
  SELECT P.DESCRIPCION
    FROM GSI_PROCESOS_RETARIFICA_ROA P
   WHERE P.ID_PROCESO = CV_ID_PROCESO
     AND P.ESTADO = 'A';
     
 LV_PROGRAMA    VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_BITACORIZA_PROCESOS_ROA';
 LV_PROCESO     GSI_PROCESOS_RETARIFICA_ROA.DESCRIPCION%TYPE;
 LV_DESCRIPCION VARCHAR2(500):=NULL;
 
BEGIN
  
  IF PV_ID_PROCESO IS NULL OR PV_ESTADO IS NULL OR PD_FECHA_CORTE IS NULL
     OR PD_FECHA_PROCESAMIENTO IS NULL OR PV_REPROCESO IS NULL THEN

     SELECT DESCRIPCION
       INTO LV_PROCESO
       FROM GSI_PROCESOS_RETARIFICA_ROA
      WHERE ID_PROCESO = PV_ID_PROCESO;

     PV_ERROR:='Error en programa:'||LV_PROGRAMA||'; Error: Ning�n par�metro puede ser nulo para bitacorizar proceso.'||
                  'Se intent� bitacorizar proceso: '||LV_PROCESO;
     RETURN;

  END IF;
  
   OPEN C_DESCRIPCION_PROCESO(PV_ID_PROCESO);
  FETCH C_DESCRIPCION_PROCESO INTO LV_DESCRIPCION;
  CLOSE C_DESCRIPCION_PROCESO;
  
  
  IF PV_ESTADO ='INICIADO' THEN
     INSERT INTO GSI_PROCESOS_BITACORA_ROA
       (ID_PROCESO,
        DESCRIPCION_PROCESO,
        NOMBRE_PROCESO_EJECUTADO,
        FECHA_CORTE,
        FECHA_PROCESAMIENTO,
        ESTADO,
        REPROCESO,
        ERROR)
     VALUES
       (PV_ID_PROCESO,
        LV_DESCRIPCION,
        PV_NOMBRE_PROCESO,
        PD_FECHA_CORTE,
        PD_FECHA_PROCESAMIENTO,
        PV_ESTADO,
        PV_REPROCESO,
        NULL);
        
  ELSIF PV_ESTADO='PROCESADO' THEN
      UPDATE GSI_PROCESOS_BITACORA_ROA R
         SET R.ESTADO         = PV_ESTADO,
             R.REPROCESO      = PV_REPROCESO,
             R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO    
       WHERE R.FECHA_CORTE    = PD_FECHA_CORTE
         AND R.ID_PROCESO     = PV_ID_PROCESO
         AND R.ESTADO         = 'INICIADO'
         AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO         
         AND R.FECHA_PROCESAMIENTO = (SELECT MAX(S.FECHA_PROCESAMIENTO)
                                        FROM GSI_PROCESOS_BITACORA_ROA S
                                       WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                                         AND S.ID_PROCESO  = PV_ID_PROCESO
                                         AND S.ESTADO      = 'INICIADO'
                                         AND S.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO);

       IF SQL%NOTFOUND THEN
          PV_ERROR:='Error en programa:'||LV_PROGRAMA||'; Error al actualizar tabla de bitacora';
          RETURN;
       END IF;

  ELSIF PV_ESTADO='ERROR' THEN
        UPDATE GSI_PROCESOS_BITACORA_ROA R
           SET R.REPROCESO = PV_REPROCESO,
               R.ERROR     = PV_ERROR,
               R.ESTADO    = PV_ESTADO
         WHERE R.FECHA_CORTE = PD_FECHA_CORTE
           AND R.ID_PROCESO  = PV_ID_PROCESO
           AND R.ESTADO      = 'INICIADO'
           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO              
           AND R.FECHA_PROCESAMIENTO = (SELECT MAX(S.FECHA_PROCESAMIENTO)
                                          FROM GSI_PROCESOS_BITACORA_ROA S
                                         WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                                           AND S.ID_PROCESO  = PV_ID_PROCESO
                                           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO                                              
                                           AND S.ESTADO      = 'INICIADO');                                    
       IF SQL%NOTFOUND THEN
          PV_ERROR:='Error en programa:'||LV_PROGRAMA||'; Error al actualizar tabla de bitacora';
          RETURN;
       END IF;
       
  ELSIF PV_ESTADO = 'WARNING' THEN
        UPDATE GSI_PROCESOS_BITACORA_ROA R
           SET R.REPROCESO = PV_REPROCESO,
               R.ERROR     = PV_ERROR,
               R.ESTADO    = PV_ESTADO
         WHERE R.FECHA_CORTE = PD_FECHA_CORTE
           AND R.ID_PROCESO  = PV_ID_PROCESO
           AND R.ESTADO      = 'INICIADO'
           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO              
           AND R.FECHA_PROCESAMIENTO = (SELECT MAX(S.FECHA_PROCESAMIENTO)
                                          FROM GSI_PROCESOS_BITACORA_ROA S
                                         WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                                           AND S.ID_PROCESO  = PV_ID_PROCESO
                                           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO                                              
                                           AND S.ESTADO      = 'INICIADO');                                    
       IF SQL%NOTFOUND THEN
          PV_ERROR:='Error en programa:'||LV_PROGRAMA||'; Error al actualizar tabla de bitacora';
          RETURN;
       END IF;  
  ELSIF PV_ESTADO = 'CANCELADO' THEN
        UPDATE GSI_PROCESOS_BITACORA_ROA R
           SET R.REPROCESO = PV_REPROCESO,
               R.ERROR     = PV_ERROR,
               R.ESTADO    = PV_ESTADO
         WHERE R.FECHA_CORTE = PD_FECHA_CORTE
           AND R.ID_PROCESO  = PV_ID_PROCESO
           AND R.ESTADO      = 'INICIADO'
           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO              
           AND R.FECHA_PROCESAMIENTO = (SELECT MAX(S.FECHA_PROCESAMIENTO)
                                          FROM GSI_PROCESOS_BITACORA_ROA S
                                         WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                                           AND S.ID_PROCESO  = PV_ID_PROCESO
                                           AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO                                              
                                           AND S.ESTADO      = 'INICIADO');                                    
       IF SQL%NOTFOUND THEN
          PV_ERROR:='Error en programa:'||LV_PROGRAMA||'; Error al actualizar tabla de bitacora';
          RETURN;
       END IF;  
  
  END IF;

  COMMIT; 
EXCEPTION 
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
  
END GSI_BITACORIZA_PROCESOS_ROA;

/***************************************************************
*Obtiene los dias a procesar segun el corte de facturacion
***************************************************************/
PROCEDURE GSI_GET_RANGO_DIAS(PD_FECHA_CORTE   IN DATE,
                             PD_FECHA_INI     OUT DATE,
                             PD_FECHA_FIN     OUT DATE,   
                             PB_INTERNACIONAL IN BOOLEAN,                          
                             PV_ERROR         OUT VARCHAR2)IS

  LV_PROGRAMA    VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.GSI_GET_RANGO_DIAS';
  LV_HORA_INI    VARCHAR2(8)   := NULL;
  LV_HORA_FIN    VARCHAR2(8)   := NULL;
  LB_RANGO       BOOLEAN;
  LE_ERROR       EXCEPTION;
  LC_RANGO       C_OBTIENE_RANGO_DIAS%ROWTYPE;
  
BEGIN
  --Obtengo el rango de fecha que voy a procesar  
   OPEN C_OBTIENE_RANGO_DIAS(SUBSTR(PD_FECHA_CORTE,1,2));
  FETCH C_OBTIENE_RANGO_DIAS INTO LC_RANGO;
  LB_RANGO:= C_OBTIENE_RANGO_DIAS%FOUND;
  CLOSE C_OBTIENE_RANGO_DIAS;

  IF LB_RANGO = FALSE THEN
     PV_ERROR:= 'Ingrese una fecha de corte v�lida';
     RAISE LE_ERROR;  
  END IF;

  PD_FECHA_INI := TO_DATE(LC_RANGO.DIA_INICIO||SUBSTR(PD_FECHA_CORTE,3,8)||' '||LC_RANGO.HORA_INICIO,'DD/MM/YYYY HH24:MI:SS');
  PD_FECHA_FIN := TO_DATE(LC_RANGO.DIA_FIN||SUBSTR(PD_FECHA_CORTE,3,8)||' '||LC_RANGO.HORA_FIN,'DD/MM/YYYY HH24:MI:SS');

  --Si el dia de inicio del c�lculo comienza el mes anterior
  IF NVL(LC_RANGO.INI_MES_ANT,'N') = 'S' THEN
    PD_FECHA_INI := ADD_MONTHS(PD_FECHA_INI,-1);
  END IF;

  --Si el rango de d�as es internacional
  IF PB_INTERNACIONAL THEN 
    
       OPEN C_PARAMETROS(6071,'HORA_INTER_INI');
      FETCH C_PARAMETROS INTO LV_HORA_INI;
      CLOSE C_PARAMETROS;

       OPEN C_PARAMETROS(6071,'HORA_INTER_FIN');
      FETCH C_PARAMETROS INTO LV_HORA_FIN;
      CLOSE C_PARAMETROS;

      PD_FECHA_INI := TO_DATE(SUBSTR(TO_CHAR(PD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS'),1,11)||LV_HORA_INI,'DD/MM/YYYY HH24:MI:SS');
      PD_FECHA_FIN := TO_DATE(SUBSTR(TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS'),1,11)||LV_HORA_FIN,'DD/MM/YYYY HH24:MI:SS')+1;

  END IF;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
    
END GSI_GET_RANGO_DIAS;

/*******************************************************************************
*Obtiene la fecha de corte anterior, dependiendo de la fecha de corte ingresada
********************************************************************************/
PROCEDURE GSI_GET_FECHA_CORTE_ANT(PD_FECHA_CORTE      DATE,
                                  PD_FECHA_CORTE_ANT  OUT DATE,
                                  PV_ERROR            OUT VARCHAR2) IS

  LV_PROGRAMA        VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_GET_FECHA_CORTE_ANT';
  LV_CICLO           VARCHAR2(2);
  LV_DIA_CORTE_ANT   VARCHAR2(2);
  LV_MES_ANT         VARCHAR2(1);
  LD_FECHA_CORTE_ANT DATE;
  LE_ERROR           EXCEPTION;
  
BEGIN

  SELECT R.ID_CICLO, R.INI_MES_ANT
    INTO LV_CICLO, LV_MES_ANT
    FROM GSI_CICLOS_CALCULO_ROAMING R 
   WHERE R.DIA_CORTE = SUBSTR(PD_FECHA_CORTE,1,2);

  IF LV_CICLO-1 = 0 THEN
     SELECT MAX(R.ID_CICLO)
       INTO LV_CICLO
       FROM GSI_CICLOS_CALCULO_ROAMING R;
  ELSE 
      LV_CICLO := '0'||(LV_CICLO-1);
  END IF;

  SELECT C.DIA_CORTE
    INTO LV_DIA_CORTE_ANT
    FROM GSI_CICLOS_CALCULO_ROAMING C
   WHERE C.ID_CICLO = LV_CICLO;

  LD_FECHA_CORTE_ANT := TO_DATE(LV_DIA_CORTE_ANT||SUBSTR(PD_FECHA_CORTE,3,10),'DD/MM/YYYY');

  IF LV_MES_ANT = 'S' THEN
     LD_FECHA_CORTE_ANT := ADD_MONTHS(LD_FECHA_CORTE_ANT,-1);
  END IF;

  PD_FECHA_CORTE_ANT := TO_CHAR(LD_FECHA_CORTE_ANT,'DD/MM/YYYY');

  IF PD_FECHA_CORTE_ANT IS NULL THEN
     PV_ERROR:= 'Error al obtener la fecha de corte anterior';
     RAISE LE_ERROR;
  END IF;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; D�a de corte no existe';  
  WHEN LE_ERROR THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_GET_FECHA_CORTE_ANT;

/*******************************************************************************
*Obtiene la fecha de corte siguiente, dependiendo de la fecha de corte ingresada
********************************************************************************/
PROCEDURE GSI_GET_FECHA_CORTE_SIG(PD_FECHA_CORTE      DATE,
                                  PD_FECHA_CORTE_SIG  OUT DATE,
                                  PV_ERROR            OUT VARCHAR2) IS

 LV_PROGRAMA       VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_GET_FECHA_CORTE_SIG';
 LV_CICLO          VARCHAR2(2);
 LV_DIA_CORTE_SIG  VARCHAR2(2);
 LV_MES_ANT        VARCHAR2(1);
 LN_MAX_CICLO      NUMBER       := 0;
 LN_CICLO          NUMBER       := 0;
 LE_ERROR          EXCEPTION;
 
BEGIN

 SELECT R.ID_CICLO
   INTO LV_CICLO
   FROM GSI_CICLOS_CALCULO_ROAMING R 
  WHERE R.DIA_CORTE = SUBSTR(PD_FECHA_CORTE,1,2);

 LN_CICLO := LV_CICLO + 1;

 SELECT MAX(R.ID_CICLO)
   INTO LN_MAX_CICLO
   FROM GSI_CICLOS_CALCULO_ROAMING R;

 IF LN_CICLO > LN_MAX_CICLO THEN
    LN_CICLO := 1;
 END IF;

 SELECT C.DIA_CORTE
   INTO LV_DIA_CORTE_SIG
   FROM GSI_CICLOS_CALCULO_ROAMING C
  WHERE C.ID_CICLO = '0' || LN_CICLO;

 PD_FECHA_CORTE_SIG := TO_DATE(LV_DIA_CORTE_SIG||SUBSTR(PD_FECHA_CORTE,3,10),'DD/MM/YYYY');

 SELECT R.INI_MES_ANT
   INTO LV_MES_ANT
   FROM GSI_CICLOS_CALCULO_ROAMING R 
  WHERE R.DIA_CORTE = SUBSTR(PD_FECHA_CORTE_SIG,1,2);

 IF LV_MES_ANT = 'S' THEN
    PD_FECHA_CORTE_SIG := ADD_MONTHS(PD_FECHA_CORTE_SIG,+1);
 END IF;

 IF PD_FECHA_CORTE_SIG IS NULL THEN
    PV_ERROR:= 'ERROR AL OBTENER LA FECHA DE CORTE SIGUIENTE';
    RAISE LE_ERROR;
 END IF;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; Error: D�a de corte no existe';
 WHEN LE_ERROR THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR; 
 WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_GET_FECHA_CORTE_SIG;

/***************************************************************************************
*Realiza las Validaciones respectivas al momento de realizar la transaccion del proceso. 
*Las validaciones depende de lo que existe en la bitacora
****************************************************************************************/
PROCEDURE GSI_CONTROL_PROCESOS (PV_ID_PROCESO       VARCHAR2,
                                PD_FECHA_CORTE      DATE,
                                PV_REPROCESO        VARCHAR2,
                                PV_COD_ERROR        OUT VARCHAR2,
                                PV_ERROR            OUT VARCHAR2 )IS

 LV_PROGRAMA VARCHAR2(100):='GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_PROCESOS';

 CURSOR C_INFOPROCESO(CV_ID_PROCESO VARCHAR2) IS
  SELECT P.DESCRIPCION,P.REPROCESABLE
    FROM GSI_PROCESOS_RETARIFICA_ROA P
   WHERE P.ID_PROCESO=CV_ID_PROCESO
     AND P.ESTADO='A';

 CURSOR C_PROCESO_DEPENDE(CV_ID_PROCESO VARCHAR2) IS
  SELECT R1.ID_PROCESO,R1.DESCRIPCION
    FROM GSI_PROCESOS_RETARIFICA_ROA R1,GSI_PROCESOS_RETARIFICA_ROA R2
   WHERE R1.ID_PROCESO=R2.ID_PROCESO_DEPENDE
     AND R2.ID_PROCESO=CV_ID_PROCESO;
  
 CURSOR C_HIJOS(CV_ID_PROCESO_PADRE VARCHAR2)IS
  SELECT *
    FROM GSI_PROCESOS_RETARIFICA_ROA
   WHERE ID_PROCESO_DEPENDE=CV_ID_PROCESO_PADRE
     AND ESTADO='A';
  
 LD_MAX_FECH_PROCESADO       DATE;
 LD_FECHA_CORTE_ANT          DATE;
 LD_FECHA_CORTE_SIG          DATE;
 LC_INFOPROCESO              C_INFOPROCESO%ROWTYPE;
 LV_NOM_PROCESO_DEPENDE      GSI_PROCESOS_RETARIFICA_ROA.DESCRIPCION%TYPE;
 LV_IDPROCESO_DEPENDE        GSI_PROCESOS_RETARIFICA_ROA.ID_PROCESO%TYPE;
 LV_VERIF_PROCESADO          VARCHAR2(1):=NULL;
 LV_ALTER_S                  VARCHAR2(100):=NULL;
 LD_MAX_FECH_PROCESADO_PADRE DATE;
 LB_CORTE_VALIDO             BOOLEAN;
 LC_CORTE_VALIDO             C_OBTIENE_RANGO_DIAS%ROWTYPE;
 LE_ERROR                    EXCEPTION;  
 LV_ERROR_ARMA_MSJ           VARCHAR2(4000):=NULL;
 
BEGIN
  
 LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY''';
 EXECUTE IMMEDIATE LV_ALTER_S;
  
  --Uso este cursor como auxiliar y as� comprobar que la fecha de corte sea v�lida  
  OPEN C_OBTIENE_RANGO_DIAS(substr(PD_FECHA_CORTE,1,2));
 FETCH C_OBTIENE_RANGO_DIAS INTO LC_CORTE_VALIDO;
 LB_CORTE_VALIDO:=C_OBTIENE_RANGO_DIAS%FOUND;
 CLOSE C_OBTIENE_RANGO_DIAS;
  
 IF LB_CORTE_VALIDO = FALSE THEN
    PV_ERROR:='Ingrese una fecha de corte v�lida';
    PV_COD_ERROR:='1';
    RETURN;    
 END IF;

   --Consulto ultima fecha de corte en que fue ejecutado
  OPEN C_MAX_FECH_PROCESADO(PV_ID_PROCESO);
 FETCH C_MAX_FECH_PROCESADO INTO LD_MAX_FECH_PROCESADO;
 CLOSE C_MAX_FECH_PROCESADO;

  --Extraigo nombre de proceso
  OPEN C_INFOPROCESO(PV_ID_PROCESO);
 FETCH C_INFOPROCESO INTO LC_INFOPROCESO;
 CLOSE C_INFOPROCESO;
  
  --Solo si es reproceso lo dejar� pasar para que proceso sea ejecutado nuevamente
 IF LD_MAX_FECH_PROCESADO = PD_FECHA_CORTE AND NVL(PV_REPROCESO,'N')<>'S' THEN
    
    IF NVL(LC_INFOPROCESO.REPROCESABLE,'N') = 'S' THEN
      PV_ERROR:= 'Proceso '||LC_INFOPROCESO.DESCRIPCION||' ya ha sido ejecutado con fecha de corte :'||to_char(PD_FECHA_CORTE,'dd/mm/yyyy') ||', digite "S", si desea reprocesar, caso contrario "N":';
      PV_COD_ERROR:= '1';
      RETURN;
    ELSE
      PV_ERROR:= 'Proceso '||LC_INFOPROCESO.DESCRIPCION||' ya ha sido ejecutado con fecha de corte :'||to_char(PD_FECHA_CORTE,'dd/mm/yyyy');
      PV_COD_ERROR:= '1';
      RETURN;
    END IF;
 
 END IF;

 IF PD_FECHA_CORTE < LD_MAX_FECH_PROCESADO THEN
    PV_COD_ERROR:='1';
    PV_ERROR:='La fecha de corte actual no puede ser menor a la �ltima fecha de ejecuci�n del proceso '||LC_INFOPROCESO.DESCRIPCION;
    RETURN;
 END IF;

 --Verifico si el proceso tiene dependencia (padre) de otro proceso
  OPEN C_PROCESO_DEPENDE(PV_ID_PROCESO);
 FETCH C_PROCESO_DEPENDE INTO LV_IDPROCESO_DEPENDE,LV_NOM_PROCESO_DEPENDE;
 CLOSE C_PROCESO_DEPENDE;

 --Si tiene (dependencia) padre
 IF LV_IDPROCESO_DEPENDE IS NOT NULL THEN

     --Consulto ultima fecha de corte de  ejecucion del padre
     OPEN C_MAX_FECH_PROCESADO(LV_IDPROCESO_DEPENDE);
    FETCH C_MAX_FECH_PROCESADO INTO LD_MAX_FECH_PROCESADO_PADRE;
    CLOSE C_MAX_FECH_PROCESADO;

    IF LD_MAX_FECH_PROCESADO_PADRE > NVL(LD_MAX_FECH_PROCESADO,LD_MAX_FECH_PROCESADO_PADRE-1)
       AND PD_FECHA_CORTE> LD_MAX_FECH_PROCESADO_PADRE THEN

       PV_ERROR:='Error: El proceso '||LC_INFOPROCESO.DESCRIPCION||' le corresponde procesar con fecha: '||TO_CHAR(LD_MAX_FECH_PROCESADO_PADRE,'DD/MM/YYYY');
       PV_COD_ERROR:='1';
       RETURN;
    END IF;

    IF (LD_MAX_FECH_PROCESADO_PADRE = LD_MAX_FECH_PROCESADO
      AND PD_FECHA_CORTE> LD_MAX_FECH_PROCESADO_PADRE) OR LD_MAX_FECH_PROCESADO_PADRE IS NULL THEN

      PV_ERROR:='Error: No puede ejecutar '||LC_INFOPROCESO.DESCRIPCION||'. '||
                'Primero debe ejecutar el proceso: '||LV_NOM_PROCESO_DEPENDE;
      PV_COD_ERROR:='1';
      RETURN;
    END IF;

 ELSE--Si no tiene padre
      
    IF LD_MAX_FECH_PROCESADO IS NULL THEN
       PV_COD_ERROR:='0'; --EXITO
       RETURN;--Si es la primera vez que se ejecuta salir
    END IF;

    --Aseguro que haya ejecutado los procesos hijos en la �ltima fecha de proceso
    FOR LC_HIJOS IN C_HIJOS(PV_ID_PROCESO) LOOP

        LV_VERIF_PROCESADO:=NULL;
          
         OPEN C_VERIF_PROCESADO(LC_HIJOS.ID_PROCESO,LD_MAX_FECH_PROCESADO);
        FETCH C_VERIF_PROCESADO INTO LV_VERIF_PROCESADO;
        CLOSE C_VERIF_PROCESADO;
        
        IF NVL(LV_VERIF_PROCESADO,'N')='N' AND NVL(PV_REPROCESO,'N')<>'S' THEN
            
           IF PV_ERROR IS NULL THEN
              PV_ERROR:= 'Tiene pendiente ejecutar los procesos: '||LC_HIJOS.DESCRIPCION;
           ELSE
              PV_ERROR:= PV_ERROR||', '||LC_HIJOS.DESCRIPCION;
           END IF;
          
        END IF;
        
    END LOOP;
      
    IF PV_ERROR IS NOT NULL THEN
       PV_ERROR := PV_ERROR||'. Con fecha de corte: '||LD_MAX_FECH_PROCESADO;
       PV_COD_ERROR := '1';
       RETURN;
    END IF;

    GSI_GET_FECHA_CORTE_ANT(PD_FECHA_CORTE     => PD_FECHA_CORTE,
                            PD_FECHA_CORTE_ANT => LD_FECHA_CORTE_ANT,
                            PV_ERROR           => PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_COD_ERROR:='2';
       RAISE LE_ERROR;       
    END IF;

    IF LD_MAX_FECH_PROCESADO < LD_FECHA_CORTE_ANT THEN
     
       GSI_GET_FECHA_CORTE_SIG(PD_FECHA_CORTE     => LD_MAX_FECH_PROCESADO,
                               PD_FECHA_CORTE_SIG => LD_FECHA_CORTE_SIG,
                               PV_ERROR           => PV_ERROR);
     
       IF PV_ERROR IS NOT NULL THEN
          PV_COD_ERROR:='2';
          RAISE LE_ERROR;            
       END IF;
         
       PV_ERROR:='Error: La �ltima fecha de corte procesada del proceso '||LC_INFOPROCESO.DESCRIPCION||' es: '||LD_MAX_FECH_PROCESADO||
                 '. Debe de procesar con fecha de corte:'||LD_FECHA_CORTE_SIG;
       PV_COD_ERROR:='1';
       RETURN;                                 
     
    END IF;

 END IF;

 PV_COD_ERROR:='0';

EXCEPTION
 WHEN LE_ERROR THEN
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);
 WHEN OTHERS THEN
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
    PV_COD_ERROR:='2';
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);
END GSI_CONTROL_PROCESOS;

/**************************************************************************************
*1.- Proceso principal activaciones.- Extrae de la tabla cl_detalles_servicios de axis 
     a todos los numeros que hallan activado paquetes Roaming
**************************************************************************************/

/**************************************************************************************
*Inserta los paquetes de los clientes que quedaron con un saldo pendiente del per�odo anterior
*Da de baja a los bytes por concepto de paquete recurrente
*Actualiza la fecha de inactivaci�n y estado de los paquetes que se arrastran
**************************************************************************************/
PROCEDURE GSI_DEPURA_INFO_ACTIVACIONES(PD_FECHA_CORTE DATE,
                                       PV_REPROCESO   VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2)IS

 LV_PROGRAMA VARCHAR2(100):='GSI_RETARIFICACIONES_ROAMING.GSI_DEPURA_INFO_ACTIVACIONES';

 CURSOR C_ARRASTRE_PERIODO_ANT IS
  SELECT A.ROWID, A.* 
    FROM GSI_PAQROAMING_ACTIVADOS A 
   WHERE PROCESADO = 'X' 
     AND FECHA_HASTA IS NULL;

 CURSOR C_GET_FECH_FIN_FEATURE(CV_ID_SERVICIO VARCHAR2,
                               CV_FEATURE     VARCHAR2,
                               CD_FECHA_DESDE DATE) IS
  SELECT FECHA_HASTA, ESTADO
    FROM CL_DETALLES_SERVICIOS@AXIS
   WHERE ID_SERVICIO = Cv_ID_SERVICIO
     AND ID_TIPO_DETALLE_SERV = CV_FEATURE
     AND FECHA_DESDE = CD_FECHA_DESDE;
    
 CURSOR C_PAQ_SOBRE_PAQ IS
  SELECT P.ID_SERVICIO,P.FEATURE, COUNT(P.FEATURE) 
    FROM GSI_PAQROAMING_ACTIVADOS P
   GROUP BY P.ID_SERVICIO, P.FEATURE
  HAVING COUNT(P.FEATURE) > 1;
    
 CURSOR C_FEAT_MAX_FEC_INI(CV_SERVICIO VARCHAR2,
                           CV_FEATURE VARCHAR2) IS
  SELECT MAX(P.FECHA_DESDE) FECHA_DESDE
    FROM GSI_PAQROAMING_ACTIVADOS P
   WHERE P.ID_SERVICIO = CV_SERVICIO
     AND P.FEATURE = CV_FEATURE;

 LD_FIN              DATE;
 LV_EST              VARCHAR2(1);
 LD_FECHA_INI        DATE;
 LD_FECHA_FIN        DATE;
 LD_FECHA_CORTE_ANT  DATE;
 LV_WHERE            VARCHAR2(200):=' WHERE FECHA_CORTE=TO_DATE('''||PD_FECHA_CORTE||''',''DD/MM/YYYY'') ';
 LD_FECHA_LAST_PAQ   DATE:=NULL;
 LE_ERROR            EXCEPTION;
 
BEGIN

 --Calcula la fecha de Corte Anterior
 GSI_RETARIFICACIONES_ROAMING.GSI_GET_FECHA_CORTE_ANT( PD_FECHA_CORTE,
                                                       LD_FECHA_CORTE_ANT,
                                                       PV_ERROR);
 
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;

 --
 GSI_RETARIFICACIONES_ROAMING.GSI_GET_RANGO_DIAS( PD_FECHA_CORTE,
                                                  LD_FECHA_INI,
                                                  LD_FECHA_FIN,
                                                  FALSE,
                                                  PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF;

 GSI_OBJ_RETARIFICACIONES.INSERT_PAQROAMING_PERIOD_ANT( LD_FECHA_CORTE_ANT,
                                                        LD_FECHA_INI,
                                                        PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF;
  
 --Da de baja a los bytes por concepto de paquete recurrente
 GSI_RETARIFICACIONES_ROAMING.GSI_ACTUALIZA_PAQUETE_RECURR( PD_FECHA => LD_FECHA_INI,
                                                            PV_ERROR => PV_ERROR);
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF; 

 --Actualizar la fecha de inactivaci�n de los paquetes que se arrastran
 FOR LC_ARRASTRE IN C_ARRASTRE_PERIODO_ANT LOOP

    OPEN C_GET_FECH_FIN_FEATURE(SUBSTR(LC_ARRASTRE.ID_SERVICIO,4,8),LC_ARRASTRE.FEATURE,LC_ARRASTRE.FECHA_DESDE);
   FETCH C_GET_FECH_FIN_FEATURE INTO LD_FIN, LV_EST;
   CLOSE C_GET_FECH_FIN_FEATURE;
        
   GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAMING_ACTIVADOS( LD_FIN,
                                                         LV_EST,
                                                         LC_ARRASTRE.ROWID,
                                                         PV_ERROR);
   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;      
   END IF;
    
 END LOOP;
 
 COMMIT;
 --Verifica registros que hallan realizado transacciones post_venta
 GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_TRX(PV_ERROR => PV_ERROR);
 
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF;
  
 --Para verificar aquellos clientes que tengan paquetes con una vigencia superior sus d�as vigencia.
 GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAMING_ACT_FECH_FIN( PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF;
   
 --Cuando es paquete sobre paquete debo setear la vigencia real al primer paquete  
 FOR VAR IN C_PAQ_SOBRE_PAQ LOOP
 
    OPEN C_FEAT_MAX_FEC_INI(VAR.ID_SERVICIO,VAR.FEATURE);
   FETCH C_FEAT_MAX_FEC_INI INTO LD_FECHA_LAST_PAQ;
   CLOSE C_FEAT_MAX_FEC_INI;
    
   GSI_OBJ_RETARIFICACIONES.UPDATE_FECH_FIN_PAQ_SOBRE_PAQ( VAR.ID_SERVICIO,
                                                           VAR.FEATURE,
                                                           LD_FECHA_LAST_PAQ,
                                                           PV_ERROR);
  IF PV_ERROR IS NOT NULL THEN
     RAISE LE_ERROR;     
  END IF;

 END LOOP;

 --Actualizar el estado de todos los paquetes a "sin procesar"
 GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAM_ACT_PROCESADO( 'N',
                                                        PV_ERROR);

  
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
 END IF;

 --Si es reproceso debo eliminar los respaldos que se hayan hecho de la fecha de corte actual
 IF NVL(UPPER(PV_REPROCESO),'N') = 'S' THEN
    GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE(' GSI_PAQROAMING_ACTIVADOS_HIST ',
                                                   LV_WHERE,
                                                   PV_ERROR);
                                                        
    IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;       
    END IF;
 END IF;
  
EXCEPTION
 WHEN LE_ERROR THEN 
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;    
 WHEN OTHERS THEN 
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_DEPURA_INFO_ACTIVACIONES;

/***********************************************************************************
*3.- Ejecuta el procedimiento que realiza la tasaci�n de los udrs de navegaci�n GPRS
************************************************************************************/
PROCEDURE GSI_PROCESA_GPRS_PRINCIPAL (PD_FECHA_CORTE       IN DATE,
                                      PV_REPROCESO         IN VARCHAR2,
                                      PV_ERROR             OUT VARCHAR2) IS

 LV_PROGRAMA            VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_PROCESA_GPRS_PRINCIPAL';
 LV_ID_PROCESO          VARCHAR2(3)  := '3';
 LE_ERROR               EXCEPTION;
 LV_PROGRAMA_RETARIFICA VARCHAR2(200);
 LN_ECONTRADO           VARCHAR2(1);
 LV_ERROR_ARMA_MSJ      VARCHAR2(4000):=NULL;
 LE_ERROR_ESPERADO      EXCEPTION;

BEGIN
  --Bitacorizo inicio de Proceso
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'INICIADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              NVL(PV_REPROCESO,'N'),
                              LV_PROGRAMA,
                              PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;

 --Depuro informaci�n GPRS


 --***Cierra DBLINK***--
     BEGIN
       --DBMS_SESSION.CLOSE_DATABASE_LINK('ESPEJO_RCC');
       --DBMS_SESSION.CLOSE_DATABASE_LINK('ESPEJO_RTX');
       DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
       COMMIT;
     EXCEPTION
      WHEN OTHERS THEN
        NULL;
     END;
 --***Cierra DBLINK***--
 
 GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_ACTIVACIONES_PAQPORT',
                                                 NULL,
                                                 PV_ERROR);
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
 
 GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_LOG_SERVICIOS_FALLIDOS',
                                                 'WHERE TIPO_SVA = ''GPRS''',
                                                 PV_ERROR);
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
   
 --Realizo la retarificaci�n GPRS
  OPEN C_PARAMETROS(6071,'PROG_RETARIFICA_GPRS');
 FETCH C_PARAMETROS INTO LV_PROGRAMA_RETARIFICA;
 CLOSE C_PARAMETROS;

 EXECUTE IMMEDIATE LV_PROGRAMA_RETARIFICA USING
 IN PD_FECHA_CORTE, OUT PV_ERROR;
 
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
                                                  
 GSI_RETARIFICACIONES_ROAMING.GSI_CLASIFICA_PRODUCTO_GPR(PV_ERROR => PV_ERROR);
 
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
 
 --Respaldo la tabla de trabajo
 GSI_OBJ_RETARIFICACIONES.GSI_RESPALDO_TAB_TRABAJO( PD_FECHA_CORTE => PD_FECHA_CORTE,
                                                     PV_ERROR       => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
  
 --Respaldo tabla de activaciones  
 GSI_OBJ_RETARIFICACIONES.GSI_RESPALDO_ACTIVACIONES( PD_FECHA_CORTE => PD_FECHA_CORTE,
                                                     PV_ERROR       => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
 
 COMMIT;
  
 --Bitacorizo a estado PROCESADO
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'PROCESADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              NVL(PV_REPROCESO,'N'),
                              LV_PROGRAMA,
                              PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;

EXCEPTION
 WHEN LE_ERROR_ESPERADO THEN
   ROLLBACK;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'WARNING',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(PV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);   
 WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;      
      GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                   'ERROR',
                                   PD_FECHA_CORTE,
                                   SYSDATE,
                                   NVL(PV_REPROCESO,'N'),
                                   LV_PROGRAMA,
                                   PV_ERROR);

      GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                     PV_ERROR => LV_ERROR_ARMA_MSJ);                    
 WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'ERROR',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(PV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);

    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR   => LV_ERROR_ARMA_MSJ);                                   
END GSI_PROCESA_GPRS_PRINCIPAL;

/********************************************************************
*2.- Llenar la tabla de trabajo con los registros gprs del per�odo
     correspondiente
*********************************************************************/
PROCEDURE GSI_CARGA_CONSUMO_GPRS (PD_FECHA_CORTE IN DATE,
                                  PV_REPROCESO   IN VARCHAR2,
                                  PV_ACCION      IN OUT VARCHAR2,
                                  PV_COD_ERROR   OUT VARCHAR2,
                                  PV_ERROR       OUT VARCHAR2)IS
 PRAGMA AUTONOMOUS_TRANSACTION;

 LV_PROGRAMA          VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.GSI_CARGA_CONSUMO_GPRS';
 LD_FECHA_INI         DATE;
 LD_FECHA_FIN         DATE;
 LV_FECHA_NEW_ESQUEMA VARCHAR2(25);
 LV_TABLA             VARCHAR2(100) := 'sysadm.Udr_Lt_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMM')||'_Grx';
 LV_TABLA_TRABAJO     VARCHAR2(100) := 'GSI_UDR_LT_GRX_ANALISIS';
 LV_SELECT            VARCHAR2(5000):= NULL;
 LV_FROM              VARCHAR2(500) := NULL;
 LV_WHERE             VARCHAR2(5000):= NULL;
 LV_SENTENCIA         VARCHAR2(4000):= NULL;
 C_CONTEO             VAR_CURSOR;
 LN_CONTEO            NUMBER:=0;
 LN_CONTEO_AUX        NUMBER:=0;
 LV_FORMULA_FECH      VARCHAR2(25)  := NULL;
 LV_SQL               VARCHAR2(100) := NULL;
 LE_ERROR             EXCEPTION;
 LV_ERROR_ARMA_MSJ    VARCHAR2(4000);
 LE_ERROR_ESPERADO    EXCEPTION;
 LV_ID_PROCESO        VARCHAR2(3)  := '2';
 LV_REPROCESO         VARCHAR2(3)  := PV_REPROCESO;
 
BEGIN

 LV_SQL := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
 EXECUTE IMMEDIATE LV_SQL;
  
 GSI_GET_RANGO_DIAS(PD_FECHA_CORTE  => PD_FECHA_CORTE,
                    PD_FECHA_INI    => LD_FECHA_INI,
                    PD_FECHA_FIN    => LD_FECHA_FIN,
                    PB_INTERNACIONAL=> TRUE,
                    PV_ERROR        => PV_ERROR);
                     
 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;

  OPEN C_PARAMETROS(6071,'FECHA_NEW_ESQUEMA');
 FETCH C_PARAMETROS INTO LV_FECHA_NEW_ESQUEMA;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'FORMULA_FECHA');
 FETCH C_PARAMETROS INTO LV_FORMULA_FECH;
 CLOSE C_PARAMETROS;

 IF PV_ACCION = 'INSERTAR' THEN
    GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_UDR_LT_GRX_ANALISIS',
                                                    NULL,
                                                    PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
    END IF;                                                    
 END IF;  
 
 IF PV_ACCION = 'CONTAR' THEN
    --Bitacorizo inicio de Proceso
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'INICIADO',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(LV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_COD_ERROR:='2';
       RAISE LE_ERROR;
    END IF;
     
    GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_PROCESOS( LV_ID_PROCESO,
                                                        PD_FECHA_CORTE,
                                                        LV_REPROCESO,
                                                        PV_COD_ERROR,
                                                        PV_ERROR);
    IF PV_COD_ERROR = '1' THEN
       RAISE LE_ERROR_ESPERADO;
    ELSIF PV_COD_ERROR = '2' THEN
       RAISE LE_ERROR;
    END IF;
        
    --Elimina Registros duplicados
    GSI_RETARIFICACIONES_ROAMING.GSI_REGISTROS_DUPLI( PD_FECHA_CORTE   => PD_FECHA_CORTE,
                                                     PV_FECHA_NEW_ESQ => LV_FECHA_NEW_ESQUEMA,
                                                     PV_FORMULA_FECHA => LV_FORMULA_FECH,
                                                     PD_FECHA_INI     => LD_FECHA_INI,
                                                     PD_FECHA_FIN     => LD_FECHA_FIN,
                                                     PV_TIPO          => 'Grx',
                                                     PV_ERROR         => PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
    END IF; 
    
    LV_SELECT := 'SELECT COUNT(*) ';
    
 ELSE 

   LV_SELECT:=
    'insert into '||LV_TABLA_TRABAJO||'
    select uds_stream_id,uds_record_id,uds_base_part_id,uds_charge_part_id,uds_free_unit_part_id,uds_special_purpose_part_id,
    cust_info_contract_id,cust_info_customer_id,cust_info_dn_id,cust_info_main_msisdn,cust_info_port_id,data_volume,data_volume_umcode,
    destination_field_id,downlink_volume_umcode,downlink_volume_volume,uplink_volume_umcode,uplink_volume_volume,entry_date_offset,
    entry_date_timestamp,event_umcode,follow_up_call_type,
    initial_start_time_timestamp + NVL ((initial_start_time_time_offset ' ||LV_FORMULA_FECH|| ' ), 0) fecha,
    initial_start_time_timestamp,initial_start_time_time_offset,mc_scalefactor,net_element_network_code,origin_field_id,
    o_p_normed_num_address,o_p_normed_num_int_acc_code,o_p_normed_num_number_plan,o_p_number_address,o_p_number_carrier_code,
    o_p_number_clir,o_p_number_numbering_plan,o_p_number_type_of_number,remark,rated_flat_amount,rated_flat_amount_currency,
    rated_flat_amount_gross_ind,rated_volume,rated_volume_umcode,rounded_volume,service_action_code,service_logic_code,s_p_home_loc_address,
    s_p_home_loc_numbering_plan,s_p_location_address,s_p_location_numbering_plan,s_p_number_address,s_p_number_network_code,
    s_p_number_numbering_plan,s_p_number_user_profile_id,s_p_port_address,s_p_port_user_profile_id,tariff_detail_chgbl_quantity,
    tariff_detail_ext_chrg_udmcode,tariff_detail_interconnect_ind,tariff_detail_rate_type_id,tariff_detail_rtx_charge_type,
    tariff_detail_ttcode,tariff_info_egcode,tariff_info_egversion,tariff_info_gvcode,tariff_info_rpcode,tariff_info_rpversion,
    tariff_info_sncode,tariff_info_spcode,tariff_info_tmcode,tariff_info_tmversion,tariff_info_tm_used_type,tariff_info_twcode,
    tariff_info_usage_ind,tariff_info_zncode,tariff_info_zpcode,tax_info_serv_cat,tax_info_serv_code,tax_info_serv_type,
    techn_info_prepay_ind,techn_info_sccode,techn_info_termination_ind,t_p_number_user_profile_id,udr_basepart_id,udr_chargepart_id,
    umts_qos_req_sdu_error_ratio,xfile_ind,zero_rated_volume_umcode,zero_rated_volume_volume,zero_rounded_volume_umcode,
    zero_rounded_volume_volume,export_file,s_pdp_address,xfile_base_charge_amount,data_volume data_volume_new,
    rated_flat_amount rated_flat_amount_new,null id_paquete,null des_paquete,null saldo_paquete,null zona,null remark_new, null producto';
 
 END IF;

 LV_FROM:= ' FROM '||LV_TABLA;

 LV_WHERE:= 
 
 ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset '||LV_FORMULA_FECH||' ), 0)>=to_date('''||LV_FECHA_NEW_ESQUEMA||''',''dd/mm/yyyy hh24:mi:ss'')
     and entry_date_timestamp >= TO_DATE('''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||
 ''',''DD/MM/YYYY HH24:MI:SS'' ) '|| 
          'and entry_date_timestamp<=TO_DATE('''||TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''' ,''DD/MM/YYYY HH24:MI:SS'') ';

 LV_SENTENCIA:=LV_SELECT||LV_FROM||LV_WHERE;

 --Si la fecha de inicio y fin de calculo es en el mismo mes ejecuto directamente la inserci�n/conteo
 IF TO_CHAR(LD_FECHA_INI,'MMYYYY') = TO_CHAR(LD_FECHA_FIN,'MMYYYY') THEN

    IF PV_ACCION='INSERTAR' THEN
       EXECUTE IMMEDIATE LV_SENTENCIA;
    ELSIF PV_ACCION='CONTAR' THEN
        OPEN C_CONTEO FOR LV_SENTENCIA;
       FETCH C_CONTEO INTO LN_CONTEO;
       CLOSE C_CONTEO;
    END IF;

 ELSE
   
    IF PV_ACCION = 'INSERTAR' THEN
       EXECUTE IMMEDIATE LV_SENTENCIA;
       

    ELSIF PV_ACCION = 'CONTAR' THEN
        OPEN C_CONTEO FOR LV_SENTENCIA;
       FETCH C_CONTEO INTO LN_CONTEO;
       CLOSE C_CONTEO;
       LN_CONTEO_AUX:=LN_CONTEO;

    END IF;
    
    --Ejecuto la tabla cuya fecha inicio es el mes anterior
    LV_FROM:= REPLACE(LV_FROM,TO_CHAR(PD_FECHA_CORTE,'YYYYMM'),TO_CHAR(ADD_MONTHS(PD_FECHA_CORTE,-1),'YYYYMM'));

    LV_WHERE:=' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset '||LV_FORMULA_FECH||' ), 0)>=to_date('''||LV_FECHA_NEW_ESQUEMA||''',''dd/mm/yyyy hh24:mi:ss'')
                and entry_date_timestamp>=TO_DATE('''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||
                ''',''DD/MM/YYYY HH24:MI:SS'' ) ';

    LV_SENTENCIA := LV_SELECT||LV_FROM||LV_WHERE;

    IF PV_ACCION = 'INSERTAR' THEN
       EXECUTE IMMEDIATE LV_SENTENCIA;
    ELSIF PV_ACCION = 'CONTAR' THEN
        OPEN C_CONTEO FOR LV_SENTENCIA;
       FETCH C_CONTEO INTO LN_CONTEO;
       CLOSE C_CONTEO;
       LN_CONTEO:=LN_CONTEO+LN_CONTEO_AUX;
    END IF;

 END IF;
    
 IF PV_ACCION = 'CONTAR' THEN
    PV_ACCION := TRIM(' '||LN_CONTEO);
    COMMIT;
 ELSIF PV_ACCION = 'INSERTAR' THEN
    COMMIT;
    PV_COD_ERROR := '0';
    --Bitacorizo a estado PROCESADO
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'PROCESADO',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(LV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_COD_ERROR := '2';
       RAISE LE_ERROR;
    END IF; 
 END IF;
 
EXCEPTION
  WHEN LE_ERROR_ESPERADO THEN
    ROLLBACK;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'WARNING',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(LV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);   
  WHEN LE_ERROR THEN
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;    
    PV_COD_ERROR:='2';
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'ERROR',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(PV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);
                                     
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);
  WHEN OTHERS THEN
    ROLLBACK;    
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
    PV_COD_ERROR:='2';
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'ERROR',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(PV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);   
                                  
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);   
END GSI_CARGA_CONSUMO_GPRS;

/***************************************************************
*Corrige mediante el IMSI los numeros de telefonos que llegan incorrectos y los
*que llegan nulll.
***************************************************************/


/***************************************************************
*Proceso que realiza la clasificacion en costo y remark para los registos GPRS
Utiliza Sergio
***************************************************************/
PROCEDURE GSI_RETARIFICA_GPRS(PD_FECHA_CORTE DATE,
                              PV_ERROR       OUT VARCHAR2) is

 LV_PROGRAMA VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_RETARIFICA_GPRS';

 CURSOR REG_CLI IS
  SELECT A.ROWID, A.* 
    FROM GSI_PAQROAMING_ACTIVADOS A 
   WHERE PROCESADO = 'N'
   ORDER BY ID_SERVICIO, FECHA_DESDE;

 CURSOR REG_UDR(NUMERO VARCHAR2, FECH_INI DATE, FECH_FIN DATE) IS
  SELECT A.ROWID, A.* 
    FROM GSI_UDR_LT_GRX_ANALISIS A 
   WHERE S_P_NUMBER_ADDRESS = NUMERO 
     AND FECHA >= FECH_INI
     AND FECHA <= NVL(FECH_FIN,PD_FECHA_CORTE)
   ORDER BY FECHA;

 LN_ULTNUM             GSI_PAQROAMING_ACTIVADOS%ROWTYPE;
 LN_KBREST             NUMBER;
 LN_DATAVNEW           NUMBER;
 LN_COSTOADIC          FLOAT;
 LN_RATEDN             FLOAT;
 LV_ZONA               VARCHAR2(20);
 LV_REM                VARCHAR2(500);
 LN_COMMIT             NUMBER:=0;
 LV_COMMIT             VARCHAR2(10);
 --*
 LV_COSTO_AME          VARCHAR2(10);
 LV_COSTO_RMU          VARCHAR2(10);
 LV_REMARK_EVENTOS     GV_PARAMETROS.VALOR%TYPE;
 LV_REMARK_ADICIONALES GV_PARAMETROS.VALOR%TYPE;
 LV_REMARK_PAQUETE     GV_PARAMETROS.VALOR%TYPE;
 LC_DATOS_FEATURE_AM   C_MAESTRA_ROAMING%ROWTYPE;
 LC_DATOS_FEATURE_RM   C_MAESTRA_ROAMING%ROWTYPE; 
 --*
 LV_FEATURE_ESPECIAL   VARCHAR2(20):= NULL;
 LV_COSTO_ESPECIAL     VARCHAR2(10);
 LE_ERROR              EXCEPTION;
  
BEGIN

  OPEN C_PARAMETROS(6071,'COMMIT_ACTIVACIONES');
 FETCH C_PARAMETROS INTO LV_COMMIT;
 CLOSE C_PARAMETROS;
 
  OPEN C_MAESTRA_ROAMING('EVENT-AME');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_AM;
 CLOSE C_MAESTRA_ROAMING;
  
  OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_RM;
 CLOSE C_MAESTRA_ROAMING; 
 
  OPEN C_PARAMETROS(6071,'REMARK_GPRS_EVENTOS');
 FETCH C_PARAMETROS INTO LV_REMARK_EVENTOS;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_ADICIONALES');
 FETCH C_PARAMETROS INTO LV_REMARK_ADICIONALES;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_PAQUETE');
 FETCH C_PARAMETROS INTO LV_REMARK_PAQUETE;
 CLOSE C_PARAMETROS;

 LV_COSTO_AME:= LC_DATOS_FEATURE_AM.COSTO_ROA_GPRS;
 LV_COSTO_RMU:= LC_DATOS_FEATURE_RM.COSTO_ROA_GPRS; 
 
 FOR I IN REG_CLI LOOP
  
   IF NVL(LN_ULTNUM.ID_SERVICIO,'LOL')!=I.ID_SERVICIO THEN
      IF LN_ULTNUM.ID_SERVICIO IS NULL THEN
         --GSA SOLO LA PRIMERA VEZ ENTRA POR AQUI 
         LN_ULTNUM.ID_SERVICIO:=I.ID_SERVICIO;
         LN_ULTNUM.FEATURE:=I.FEATURE;
         LN_ULTNUM.DESCRIPCION:=I.DESCRIPCION;
         LN_ULTNUM.ZONA:=I.ZONA;
      ELSE
         IF LN_ULTNUM.ZONA='AMERICA' THEN
            LN_COSTOADIC:=TO_NUMBER(LV_COSTO_AME); --Costo -> 0,000002374
         ELSE
            LN_COSTOADIC:=TO_NUMBER(LV_COSTO_RMU); --COSTO -> 0,000006666
         END IF;
         
         --Validaciones adicionales para paquetes con costo preferencial
         SELECT M.FEATURE, M.COSTO_ADIC_ESP
           INTO LV_FEATURE_ESPECIAL, LV_COSTO_ESPECIAL
           FROM GSI_MAESTRA_FEATURE_ROAMGRX M
          WHERE M.FEATURE = I.FEATURE
            AND M.COSTO_ADIC_ESP IS NOT NULL;

         IF LV_COSTO_ESPECIAL IS NOT NULL THEN
            LN_COSTOADIC := TO_NUMBER(LV_COSTO_ESPECIAL);
         END IF;
                
         IF LN_ULTNUM.ZONA='AMERIMUNDO' THEN
         
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW        = DATA_VOLUME, 
                   RATED_FLAT_AMOUNT_NEW  = ROUND(DATA_VOLUME*LN_COSTOADIC,5),
                   ID_PAQUETE             = NULL,
                   DES_PAQUETE            = LN_ULTNUM.DESCRIPCION,
                   ZONA                   = LN_ULTNUM.ZONA, 
                   REMARK_NEW             = LV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS     = LN_ULTNUM.ID_SERVICIO 
               AND REMARK_NEW IS NULL;
         ELSE
            UPDATE GSI_UDR_LT_GRX_ANALISIS
               SET DATA_VOLUME_NEW       = DATA_VOLUME,
                   RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC,5),
                   ID_PAQUETE            = LN_ULTNUM.FEATURE,
                   DES_PAQUETE           = LN_ULTNUM.DESCRIPCION,
                   ZONA                  = LN_ULTNUM.ZONA,
                   REMARK_NEW            = LV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = LN_ULTNUM.ID_SERVICIO
               AND REMARK_NEW IS NULL;
         END IF;
             COMMIT;
             LN_ULTNUM.ID_SERVICIO:=I.ID_SERVICIO;
             LN_ULTNUM.FEATURE:=I.FEATURE;
             LN_ULTNUM.DESCRIPCION:=I.DESCRIPCION;
             LN_ULTNUM.ZONA:=I.ZONA;
      END IF;
   ELSE
      LN_ULTNUM.ID_SERVICIO:= I.ID_SERVICIO;
      LN_ULTNUM.FEATURE    := I.FEATURE;
      LN_ULTNUM.DESCRIPCION:= I.DESCRIPCION;
      LN_ULTNUM.ZONA       := I.ZONA;
   END IF;
   
   LN_KBREST:=I.BYTES_PERIODO;
   
   IF I.ZONA='AMERICA' THEN
      LN_COSTOADIC:=TO_NUMBER(LV_COSTO_AME); --Costo -> 0,000002374 
   ELSE
      LN_COSTOADIC:=TO_NUMBER(LV_COSTO_RMU); --COSTO -> 0,000006666
   END IF;
 
   --Validaciones adicionales para paquetes con costo preferencial
   SELECT M.FEATURE, M.COSTO_ADIC_ESP
     INTO LV_FEATURE_ESPECIAL, LV_COSTO_ESPECIAL
     FROM GSI_MAESTRA_FEATURE_ROAMGRX M
    WHERE M.FEATURE = I.FEATURE
      AND M.COSTO_ADIC_ESP IS NOT NULL;

   IF LV_COSTO_ESPECIAL IS NOT NULL THEN
      LN_COSTOADIC := TO_NUMBER(LV_COSTO_ESPECIAL);
   END IF;   
                  
   FOR J IN REG_UDR(I.ID_SERVICIO, I.FECHA_DESDE, I.FECHA_HASTA) LOOP
    --Grabar en tabla de log los servicios cuando no registra operadora
      BEGIN
         LV_ZONA:=NULL;
         SELECT CONTINENTE 
           INTO LV_ZONA 
           FROM GSI_MAESTRA_OPERADORAS 
          WHERE TAP_SOURCE_CODE=SUBSTR(J.EXPORT_FILE,3,5);
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
           INSERT INTO GSI_LOG_SERVICIOS_FALLIDOS( 
            ID_SERVICIO,
            PROGRAMA,
            FECHA_CORTE,
            FECHA_PROCESAMIENTO,
            CORREGIDO,
            COD_OPERADORA_NO_FOUND,
            DESCRIPCION_ERROR,
            TIPO_SVA,
            IMSI)
           VALUES(
            I.ID_SERVICIO,
            LV_PROGRAMA,
            PD_FECHA_CORTE,
            SYSDATE,
            'N',
            SUBSTR(J.EXPORT_FILE, 3, 5),
            'NO SE ENCUENTRA C�DIGO DE OPERADORA.',
            'GPRS',
            J.S_P_PORT_ADDRESS);
      END;

      IF LV_ZONA IS NOT NULL THEN
         IF I.ZONA='AMERIMUNDO' THEN
            IF LV_ZONA='AMERICA' THEN
               LN_COSTOADIC:=TO_NUMBER(LV_COSTO_AME);  --Costo -> 0,000002374 
            ELSE
               LN_COSTOADIC:=TO_NUMBER(LV_COSTO_RMU); --COSTO -> 0,000006666
            END IF;
            
            --Validaciones adicionales para paquetes con costo preferencial
            SELECT M.FEATURE, M.COSTO_ADIC_ESP
              INTO LV_FEATURE_ESPECIAL, LV_COSTO_ESPECIAL
              FROM GSI_MAESTRA_FEATURE_ROAMGRX M
             WHERE M.FEATURE = I.FEATURE
               AND M.COSTO_ADIC_ESP IS NOT NULL;

            IF LV_COSTO_ESPECIAL IS NOT NULL THEN
               LN_COSTOADIC := TO_NUMBER(LV_COSTO_ESPECIAL);
            END IF; 
            
         END IF;

         IF NVL(J.REMARK_NEW,'LOL')!=LV_REMARK_PAQUETE THEN
            IF J.DATA_VOLUME_NEW>LN_KBREST THEN
               LN_DATAVNEW:=J.DATA_VOLUME_NEW-LN_KBREST;
               LN_RATEDN:=ROUND(LN_DATAVNEW*LN_COSTOADIC,5);
               LN_KBREST:=0;
               LV_REM:=LV_REMARK_ADICIONALES;
            ELSE
               LN_KBREST:=LN_KBREST-J.DATA_VOLUME_NEW;
               LN_DATAVNEW:=0;
               LN_RATEDN:=0;
               LV_REM:=LV_REMARK_PAQUETE;
            END IF;
            --SE ACTUALIZA EL REGISTRO DE COBRO
            UPDATE GSI_UDR_LT_GRX_ANALISIS
               SET DATA_VOLUME_NEW       = LN_DATAVNEW,
                   RATED_FLAT_AMOUNT_NEW = LN_RATEDN,
                   ID_PAQUETE            = I.FEATURE,
                   DES_PAQUETE           = I.DESCRIPCION,
                   SALDO_PAQUETE         = LN_KBREST,
                   ZONA                  = LV_ZONA,
                   REMARK_NEW            = LV_REM
             WHERE ROWID = J.ROWID;
         END IF;
         
         LN_COMMIT:= LN_COMMIT + 1;
         
         IF LN_COMMIT >= TO_NUMBER(LV_COMMIT) THEN
            COMMIT;
            LN_COMMIT:=0;
         END IF;
      END IF;
   
   END LOOP;

   UPDATE GSI_PAQROAMING_ACTIVADOS
      SET PROCESADO  = 'S', 
          BYTES_LEFT = LN_KBREST
    WHERE ROWID = I.ROWID;

   COMMIT;
 
 END LOOP;

 IF LN_ULTNUM.ZONA='AMERIMUNDO' THEN
    UPDATE GSI_UDR_LT_GRX_ANALISIS
       SET DATA_VOLUME_NEW       = DATA_VOLUME,
           RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
           ID_PAQUETE            = NULL,
           DES_PAQUETE           = LN_ULTNUM.DESCRIPCION,
           ZONA                  = LN_ULTNUM.ZONA,
           REMARK_NEW            = LV_REMARK_EVENTOS
     WHERE S_P_NUMBER_ADDRESS = LN_ULTNUM.ID_SERVICIO
       AND REMARK_NEW IS NULL;
 ELSE
      UPDATE GSI_UDR_LT_GRX_ANALISIS
         SET DATA_VOLUME_NEW       = DATA_VOLUME,
             RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
             ID_PAQUETE            = LN_ULTNUM.FEATURE,
             DES_PAQUETE           = LN_ULTNUM.DESCRIPCION,
             ZONA                  = LN_ULTNUM.ZONA,
             REMARK_NEW            = LV_REMARK_EVENTOS
       WHERE S_P_NUMBER_ADDRESS = LN_ULTNUM.ID_SERVICIO
         AND REMARK_NEW IS NULL;
 END IF;

 COMMIT; 
 
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_RETARIFICA_GPRS;

/*****************************************************************************
*Proceso que realiza la clasificacion en costo y remark para los registos GPRS
******************************************************************************/
PROCEDURE GSI_RETARIFICA_GPRS_V2( PD_FECHA_CORTE DATE,
                                  PV_ERROR       OUT VARCHAR2) is

 LV_PROGRAMA VARCHAR2(100):='GSI_RETARIFICACIONES_ROAMING.GSI_RETARIFICA_GPRS_V2';
 
 CURSOR C_SERVICIOS IS
  SELECT DISTINCT P.ID_SERVICIO 
    FROM GSI_PAQROAMING_ACTIVADOS P;
      
 CURSOR REG_UDR( NUMERO    VARCHAR2, 
                 FECH_INI  DATE,
                 FECH_FIN  DATE,
                 CD_FECHA_CORTE DATE) IS
  SELECT A.ROWID, A.* 
    FROM GSI_UDR_LT_GRX_ANALISIS A WHERE S_P_NUMBER_ADDRESS=NUMERO 
     AND FECHA >= FECH_INI
     AND FECHA <= NVL(FECH_FIN,CD_FECHA_CORTE)
     AND A.REMARK_NEW IS NULL     
   ORDER BY FECHA;

 CURSOR C_OPER_FALLIDA(CV_IMSI       VARCHAR2, 
                       CD_FECHA_CORT VARCHAR2, 
                       CV_COD_OPER   VARCHAR2, 
                       CV_TIPO_SVA   VARCHAR2, 
                       CV_ESTADO     VARCHAR2) IS 
  SELECT T.*
    FROM GSI_LOG_SERVICIOS_FALLIDOS T
   WHERE T.IMSI = CV_IMSI
     AND T.FECHA_CORTE = CD_FECHA_CORT
     AND T.COD_OPERADORA_NO_FOUND = CV_COD_OPER
     AND T.TIPO_SVA = CV_TIPO_SVA
     AND T.CORREGIDO = CV_ESTADO;  
     
 CURSOR C_REMARK_NULL IS
  SELECT S_P_NUMBER_ADDRESS,
        O_P_NUMBER_ADDRESS,
        S_P_PORT_ADDRESS,
        FOLLOW_UP_CALL_TYPE,
        EXPORT_FILE
   FROM GSI_UDR_LT_GRX_ANALISIS
  WHERE REMARK_NEW IS NULL;    
   
 LN_KBREST             NUMBER;
 LN_COSTOADIC          FLOAT;
 LN_NUM_FEATURES       NUMBER:= 0;
 LN_NUM_FEAT_ACTUAL    NUMBER:= 0;
 LB_SET_PROCESADO      BOOLEAN:= TRUE;
 LC_INFO_OPERADORA     C_INFO_OPERADORA%ROWTYPE;
 LC_FEATURES_TMP       C_FEATURES_TMP%ROWTYPE;
 LC_FEATURES_TMP_ANT   C_FEATURES_TMP%ROWTYPE;
 LR_UDR                GSI_OBJ_RETARIFICACIONES.RECORD_UDR;
 LV_COSTOADIC          VARCHAR2(25):= NULL;
 LB_SET_RMUNDO         BOOLEAN:= TRUE;--ES DENTRO DE PAQUETE MIENTRAS NO SE PRUEBE LO CONTRARIO
 LB_FEAT_FOUND         BOOLEAN:= FALSE;
 --*
 LV_FEATURE_ESPECIAL   VARCHAR2(20):= NULL;
 LN_COSTO_ESPECIAL     NUMBER; 
 --*
 LV_REMARK_EVENTOS     VARCHAR2(300):= NULL;
 LV_REMARK_ADICIONALES VARCHAR2(300):= NULL;
 LV_REMARK_PAQUETE     VARCHAR2(300):= NULL;
 LV_REMARK_EVE_NOPAQT  VARCHAR2(300):= NULL;
 LC_COSTOS_ROA         C_MAESTRA_ROAMING%ROWTYPE;
 --*
 LC_EXIST_REG           C_OPER_FALLIDA%ROWTYPE;
 LB_EXISTE              BOOLEAN:= FALSE;
 --*
 LN_COSTO_AM_EVEN      NUMBER      :=0;
 LN_COSTO_RM_EVEN      NUMBER      :=0;
 LN_COUNT_R            NUMBER      :=0;
 LC_DATOS_FEATURE_AM   C_MAESTRA_ROAMING%ROWTYPE;
 LC_DATOS_FEATURE_RM   C_MAESTRA_ROAMING%ROWTYPE;
 --*
 LE_ERROR              EXCEPTION;
 LB_ADICIONALES        BOOLEAN       := TRUE;
 LB_EXIT               BOOLEAN       := FALSE;
 LN_CANT_ZONA          NUMBER        :=0;
 
BEGIN

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_EVENTOS');
 FETCH C_PARAMETROS INTO LV_REMARK_EVENTOS;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_ADICIONALES');
 FETCH C_PARAMETROS INTO LV_REMARK_ADICIONALES;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_PAQUETE');
 FETCH C_PARAMETROS INTO LV_REMARK_PAQUETE;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'REMARK_GPRS_EVENTOSNOPAQ');
 FETCH C_PARAMETROS INTO LV_REMARK_EVE_NOPAQT;
 CLOSE C_PARAMETROS;
 
  OPEN C_MAESTRA_ROAMING('EVENT-AME');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_AM;
 CLOSE C_MAESTRA_ROAMING;
 
  OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_RM;
 CLOSE C_MAESTRA_ROAMING; 
 
 FOR I IN C_SERVICIOS LOOP
    
    LB_ADICIONALES := TRUE;
    
    GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_FEATURES_TMP',
                                                   NULL,
                                                   PV_ERROR);
     
    IF PV_ERROR IS NOT NULL THEN 
       RAISE LE_ERROR;
    END IF;

    --Inserto features en la temporal
    GSI_OBJ_RETARIFICACIONES.INSERT_GSI_FEATURES_TMP( I.ID_SERVICIO,
                                                      LN_NUM_FEATURES,
                                                      PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;       
    END IF;
     
    LN_NUM_FEAT_ACTUAL :=0;
    LC_FEATURES_TMP_ANT:=NULL;

    LOOP
    
      LB_EXIT            := FALSE;
      LN_NUM_FEAT_ACTUAL := LN_NUM_FEAT_ACTUAL+1;
      LV_FEATURE_ESPECIAL:= NULL;
      LN_COSTO_ESPECIAL  := NULL;

      EXIT WHEN LN_NUM_FEAT_ACTUAL > LN_NUM_FEATURES;

      LC_FEATURES_TMP_ANT := LC_FEATURES_TMP; --ASIGNACION DEL REGISTRO ANTERIOR
      
      --ME UBICO EN EL FEATURE
       OPEN C_FEATURES_TMP(LN_NUM_FEAT_ACTUAL);
      FETCH C_FEATURES_TMP INTO LC_FEATURES_TMP;
      CLOSE C_FEATURES_TMP;
         
      LN_COSTOADIC:= 0;
      LV_COSTOADIC:= NULL;
         
       OPEN C_MAESTRA_ROAMING(LC_FEATURES_TMP.FEATURE);
      FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
      CLOSE C_MAESTRA_ROAMING;
         
      IF LC_COSTOS_ROA.COSTO_ROA_GPRS IS NULL THEN 
         --Si no tiene costo normal, entonces tiene costo especial 
         LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ADIC_ESP;
      ELSE 
         LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ROA_GPRS;
      END IF;   
         
      IF LC_FEATURES_TMP.ZONA = 'AMERIMUNDO' THEN
         --Es decir, si no tiene costo especial, y se tomara su costo de la tabla de operadoras
         IF LV_COSTOADIC IS NULL THEN
            LN_COSTOADIC:= NULL;
         END IF; 
      ELSE
         LN_COSTOADIC:= TO_NUMBER(LV_COSTOADIC);    
      END IF;
             
      LN_KBREST           := LC_FEATURES_TMP.BYTES_LEFT; 
      LR_UDR.ZONA         := LC_FEATURES_TMP.ZONA;
      LR_UDR.ID_PAQUETE   := LC_FEATURES_TMP.FEATURE;
      LR_UDR.DES_PAQUETE  := LC_FEATURES_TMP.DESCRIPCION;
          
      FOR K IN REG_UDR(I.ID_SERVICIO, LC_FEATURES_TMP.FECHA_DESDE, LC_FEATURES_TMP.FECHA_HASTA, PD_FECHA_CORTE) LOOP

         LC_INFO_OPERADORA:= NULL;
         LB_SET_PROCESADO := TRUE;
                 
          OPEN C_INFO_OPERADORA(SUBSTR(K.EXPORT_FILE,3,5));
         FETCH C_INFO_OPERADORA INTO LC_INFO_OPERADORA;
         CLOSE C_INFO_OPERADORA;
             
         IF LC_INFO_OPERADORA.CONTINENTE IS NULL THEN
            
             OPEN C_OPER_FALLIDA(K.S_P_PORT_ADDRESS, TO_CHAR(PD_FECHA_CORTE,'DD/MM/YYYY'), SUBSTR(K.EXPORT_FILE, 3, 5), 'SMS','N');
            FETCH C_OPER_FALLIDA INTO LC_EXIST_REG;
            LB_EXISTE:= C_OPER_FALLIDA%FOUND;
            CLOSE C_OPER_FALLIDA;

            IF NOT LB_EXISTE THEN             
               INSERT INTO GSI_LOG_SERVICIOS_FALLIDOS( 
                           ID_SERVICIO,
                           PROGRAMA,
                           FECHA_CORTE,
                           FECHA_PROCESAMIENTO,
                           CORREGIDO,
                           COD_OPERADORA_NO_FOUND,
                           DESCRIPCION_ERROR,
                           TIPO_SVA,
                           IMSI)
                    VALUES(
                           I.ID_SERVICIO,
                           LV_PROGRAMA,
                           PD_FECHA_CORTE,
                           SYSDATE,
                           'N',
                           SUBSTR(K.EXPORT_FILE,3,5),
                           NULL,
                           'GPRS',
                           K.S_P_PORT_ADDRESS);
            
            ELSE
               UPDATE GSI_LOG_SERVICIOS_FALLIDOS F
                  SET F.FECHA_PROCESAMIENTO    = SYSDATE
                WHERE F.IMSI                   = K.S_P_PORT_ADDRESS
                  AND F.FECHA_CORTE            = PD_FECHA_CORTE
                  AND F.COD_OPERADORA_NO_FOUND = SUBSTR(K.EXPORT_FILE, 3, 5)
                  AND F.FECHA_PROCESAMIENTO    = LC_EXIST_REG.FECHA_PROCESAMIENTO
                  AND F.TIPO_SVA               = 'GPRS'
                  AND F.CORREGIDO              = 'N';              
            END IF;               
         END IF;
             
         IF LC_FEATURES_TMP.ZONA = 'AMERIMUNDO' OR LN_COSTOADIC IS NULL THEN 
            IF LC_INFO_OPERADORA.CONTINENTE = 'AMERICA' THEN
               LN_COSTOADIC := TO_NUMBER(LC_DATOS_FEATURE_AM.COSTO_ROA_GPRS);
            ELSE
               LN_COSTOADIC := TO_NUMBER(LC_DATOS_FEATURE_RM.COSTO_ROA_GPRS);              
            END IF;
         END IF;                

         --SI TENIENDO PAQUETES DE AMERICA, SE CONSTATA QUE HA NAVEGADO EN RMUNDO
         IF LC_FEATURES_TMP.ZONA = 'AMERICA' AND LC_INFO_OPERADORA.CONTINENTE = 'RMUNDO'
            AND LB_SET_RMUNDO THEN
             --BUSCAR SI TIENE OTRO PAQUETE DE RESTO DEL MUNDO PARA RESTARLE DE ESE PAQUETE
            GSI_SET_EVENTOS_RMUNDO(PN_NUM_FEAT_ACTUAL   => LN_NUM_FEAT_ACTUAL,
                                   PN_NUM_FEATURES      => LN_NUM_FEATURES,
                                   PN_DATA_VOLUME_NEW   => K.DATA_VOLUME_NEW,
                                   PD_FECHA_CONSUMO     => K.FECHA,
                                   PV_REMARK_PAQUETE    => LV_REMARK_PAQUETE,
                                   PV_REMARK_EVENTOS    => LV_REMARK_EVENTOS,
                                   PV_REMARK_ADICIONALES => LV_REMARK_ADICIONALES,                                   
                                   PD_FEC_HASTA_FEAT_ACT=> LC_FEATURES_TMP.FECHA_HASTA,
                                   PR_UDR               => LR_UDR,
                                   PN_COSTO_BYTE        => LN_COSTOADIC,--[DMM]
                                   PV_ERROR             => PV_ERROR);
                                   
            IF PV_ERROR IS NOT NULL THEN
               RAISE LE_ERROR;
            END IF;

         ELSE

            IF LN_KBREST >= K.DATA_VOLUME_NEW THEN
               --MIENTRAS EL PAQUETE TENGA BYTES SE RESTAN DEL MISMO
               LN_KBREST            := LN_KBREST - K.DATA_VOLUME_NEW;
               LR_UDR.SALDO_PAQUETE := LN_KBREST;

               LR_UDR.REMARK_NEW            := LV_REMARK_PAQUETE;
               LR_UDR.DATA_VOLUME_NEW       := 0;
               LR_UDR.RATED_FLAT_AMOUNT_NEW := 0;
               
               LR_UDR.ZONA         :=LC_FEATURES_TMP.ZONA;
               LR_UDR.ID_PAQUETE   :=LC_FEATURES_TMP.FEATURE;
               LR_UDR.DES_PAQUETE  :=LC_FEATURES_TMP.DESCRIPCION;
 
            ELSE--SI SE LE ACABAN LOS BYTES AL PAQUETE

               IF LN_NUM_FEAT_ACTUAL < LN_NUM_FEATURES THEN--

                  GSI_BUSCAR_FEATURES(PN_NUM_FEAT_ACTUAL   => LN_NUM_FEAT_ACTUAL,
                                      PN_NUM_FEATURES      => LN_NUM_FEATURES,
                                      PN_CONSUMO_BYTES     => K.DATA_VOLUME_NEW,
                                      PD_FECHA_CONSUMO     => K.FECHA,
                                      PV_REMARK_ADICIONAL  => LV_REMARK_ADICIONALES,
                                      PR_UDR               => LR_UDR,
                                      PN_COSTO_BYTE        => LN_COSTOADIC,
                                      PB_FEAT_FOUND        => LB_FEAT_FOUND,
                                      PN_KBREST            => LN_KBREST,
                                      PC_FEATURES_TMP      => LC_FEATURES_TMP,
                                      PB_EXIT              => LB_EXIT,                                      
                                      PV_ERROR             => PV_ERROR);
                   
                   IF PV_ERROR IS NOT NULL THEN
                     RAISE LE_ERROR;
                   ELSE
                     IF LB_EXIT THEN
                        LN_NUM_FEAT_ACTUAL := LN_NUM_FEAT_ACTUAL-1;
                        EXIT;
                     END IF;
                   END IF;                   
                      
               ELSE --SI NO TIENE M�S FEATURES
                 IF LC_INFO_OPERADORA.CONTINENTE = LC_FEATURES_TMP.ZONA THEN

                    LR_UDR.DATA_VOLUME_NEW      := K.DATA_VOLUME_NEW-LN_KBREST;
                    LR_UDR.RATED_FLAT_AMOUNT_NEW:= ROUND(LR_UDR.DATA_VOLUME_NEW*LN_COSTOADIC,5);
                    LR_UDR.REMARK_NEW           := LV_REMARK_ADICIONALES;
                    LN_KBREST                   := 0;
                    LR_UDR.SALDO_PAQUETE        := LN_KBREST;
                 ELSE 
                    GSI_RETARIFICACIONES_ROAMING.GSI_CLASIFICA_EVEN_ADIC(PV_IDSERVICIO         => I.ID_SERVICIO,
                                                                          PV_REMARK_EVENTOS    => LV_REMARK_EVENTOS,
                                                                          PV_REMARK_ADICIONAL  => LV_REMARK_ADICIONALES,
                                                                          PV_COSTO_EVEN_AM     => LC_DATOS_FEATURE_AM.COSTO_ROA_GPRS,
                                                                          PV_COSTO_EVEN_RM     => LC_DATOS_FEATURE_RM.COSTO_ROA_GPRS,
                                                                          PV_ROWID             => K.ROWID,
                                                                          PV_ZONA_OPERADORA    => LC_INFO_OPERADORA.CONTINENTE,
                                                                          PD_FECHA_CONSUMO     => K.FECHA,
                                                                          PN_DATA_VOLUMEN      => K.DATA_VOLUME_NEW,
                                                                          PN_KBREST            => LN_KBREST,
                                                                          PB_ADICIONALES       => LB_ADICIONALES,
                                                                          PV_ERROR             => PV_ERROR);
                 END IF;  
               END IF;
                   
            END IF;

        END IF;
        IF LB_ADICIONALES THEN 
           --Se actualiza el registro de cobro de --Paquete o Adicionales
           UPDATE GSI_UDR_LT_GRX_ANALISIS
              SET DATA_VOLUME_NEW       = LR_UDR.DATA_VOLUME_NEW,
                  RATED_FLAT_AMOUNT_NEW = LR_UDR.RATED_FLAT_AMOUNT_NEW,
                  ID_PAQUETE            = LR_UDR.ID_PAQUETE,
                  DES_PAQUETE           = LR_UDR.DES_PAQUETE,
                  SALDO_PAQUETE         = LR_UDR.SALDO_PAQUETE,
                  ZONA                  = LC_INFO_OPERADORA.CONTINENTE,
                  REMARK_NEW            = LR_UDR.REMARK_NEW
            WHERE ROWID = K.ROWID;
        ELSE           
            LB_ADICIONALES := TRUE;                                                      
        END IF; 
      END LOOP;

      IF LB_SET_PROCESADO THEN

         UPDATE GSI_FEATURES_TMP T
            SET T.BYTES_LEFT = LN_KBREST, 
                T.PROCESADO  = 'S'
          WHERE T.NUM_FEATURE = LN_NUM_FEAT_ACTUAL;
        
         LB_SET_PROCESADO:=FALSE;
      END IF;
    END LOOP;
     
    --Actualizo la tabla de features con datos de tabla de features tmp
    GSI_OBJ_RETARIFICACIONES.UPDATE_FEATURES_SERVICES(PV_ERROR);

    --Seteo la navegaci�n por eventos del �ltimo paquete contratado en caso de tener una sola zona
    SELECT COUNT(DISTINCT(P.ZONA))
      INTO LN_CANT_ZONA 
     FROM GSI_FEATURES_TMP P
     GROUP BY P.ID_SERVICIO;
         
   IF LN_CANT_ZONA = 1 AND LC_FEATURES_TMP.ZONA = 'AMERICA' THEN 
    UPDATE GSI_UDR_LT_GRX_ANALISIS
       SET DATA_VOLUME_NEW       = DATA_VOLUME,
           RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
           ID_PAQUETE            = LR_UDR.ID_PAQUETE,
           DES_PAQUETE           = LR_UDR.DES_PAQUETE,
           ZONA                  = LR_UDR.ZONA,
           SALDO_PAQUETE         = 0,
           REMARK_NEW            = LV_REMARK_EVENTOS
     WHERE S_P_NUMBER_ADDRESS = I.ID_SERVICIO
       AND SUBSTR(EXPORT_FILE, 3, 5) IN
                 (SELECT TAP_SOURCE_CODE
                    FROM GSI_MAESTRA_OPERADORAS 
                   WHERE CONTINENTE = LC_FEATURES_TMP.ZONA)
       AND REMARK_NEW IS NULL;
    ELSE    
      --Seteo la navegaci�n por eventos dependiendo del paquete y la operadora
      GSI_RETARIFICACIONES_ROAMING.GSI_CLASIFICA_EVEN_ADIC(PV_IDSERVICIO        => I.ID_SERVICIO,
                                                            PV_REMARK_EVENTOS    => LV_REMARK_EVENTOS,
                                                            PV_REMARK_ADICIONAL  => NULL,
                                                            PV_COSTO_EVEN_AM     => LC_DATOS_FEATURE_AM.COSTO_ROA_GPRS,
                                                            PV_COSTO_EVEN_RM     => LC_DATOS_FEATURE_RM.COSTO_ROA_GPRS,
                                                            PV_ROWID             => NULL,
                                                            PV_ZONA_OPERADORA    => NULL,
                                                            PD_FECHA_CONSUMO     => NULL,
                                                            PN_DATA_VOLUMEN      => NULL,
                                                            PN_KBREST            => LN_KBREST,
                                                            PB_ADICIONALES       => LB_ADICIONALES,
                                                            PV_ERROR             => PV_ERROR);         
    
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
     END IF;  
     COMMIT; 

 END LOOP;
 
  --**Eventos sin paquete**--  
 LN_COSTO_AM_EVEN := TO_NUMBER(LC_DATOS_FEATURE_AM.COSTO_ROA_GPRS);
 LN_COSTO_RM_EVEN := TO_NUMBER(LC_DATOS_FEATURE_RM.COSTO_ROA_GPRS);
  
 UPDATE GSI_UDR_LT_GRX_ANALISIS A
    SET RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME_NEW * LN_COSTO_AM_EVEN, 5),
        REMARK_NEW            = LV_REMARK_EVE_NOPAQT, 
        ZONA                  = 'AMERICA'
  WHERE ID_PAQUETE IS NULL
    AND SUBSTR(EXPORT_FILE, 3, 5) IN
         (SELECT TAP_SOURCE_CODE
            FROM GSI_MAESTRA_OPERADORAS
           WHERE CONTINENTE = 'AMERICA');
 
 UPDATE GSI_UDR_LT_GRX_ANALISIS A
    SET RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME_NEW * LN_COSTO_RM_EVEN, 5),
        REMARK_NEW            = LV_REMARK_EVE_NOPAQT,
        ZONA                  = 'RMUNDO'
  WHERE ID_PAQUETE IS NULL  
    AND SUBSTR(EXPORT_FILE, 3, 5) IN
         (SELECT TAP_SOURCE_CODE
            FROM GSI_MAESTRA_OPERADORAS
           WHERE CONTINENTE = 'RMUNDO');

 --Verificar operadoras no existentes
 BEGIN 
   FOR G IN C_REMARK_NULL LOOP
     
     SELECT COUNT(*)
       INTO LN_COUNT_R
       FROM GSI_MAESTRA_OPERADORAS
      WHERE TAP_SOURCE_CODE = SUBSTR(G.EXPORT_FILE, 3, 5);
     
     IF LN_COUNT_R = 0 THEN

         OPEN C_OPER_FALLIDA(G.S_P_PORT_ADDRESS, TO_CHAR(PD_FECHA_CORTE,'DD/MM/YYYY'), SUBSTR(G.EXPORT_FILE, 3, 5), 'GPRS','N');
        FETCH C_OPER_FALLIDA INTO LC_EXIST_REG;
        LB_EXISTE:= C_OPER_FALLIDA%FOUND;
        CLOSE C_OPER_FALLIDA;

        IF NOT LB_EXISTE THEN 
           --Si no se encuentra c�digo de operadora se registra en la tagbla de log
           INSERT INTO GSI_LOG_SERVICIOS_FALLIDOS
             (ID_SERVICIO,
              PROGRAMA,
              FECHA_CORTE,
              FECHA_PROCESAMIENTO,
              CORREGIDO,
              COD_OPERADORA_NO_FOUND,
              DESCRIPCION_ERROR,
              TIPO_SVA,
              IMSI)
           VALUES
             (G.S_P_NUMBER_ADDRESS,
              LV_PROGRAMA,
              PD_FECHA_CORTE,
              SYSDATE,
              'N',
              SUBSTR(G.EXPORT_FILE, 3, 5),
              'NO SE ENCUENTRA C�DIGO DE OPERADORA.',
              'GPRS',
              G.S_P_PORT_ADDRESS);
        
       ELSE
           UPDATE GSI_LOG_SERVICIOS_FALLIDOS F
              SET F.FECHA_PROCESAMIENTO    = SYSDATE
            WHERE F.IMSI                   = G.S_P_PORT_ADDRESS
              AND F.FECHA_CORTE            = PD_FECHA_CORTE
              AND F.COD_OPERADORA_NO_FOUND = SUBSTR(G.EXPORT_FILE, 3, 5)
              AND F.FECHA_PROCESAMIENTO    = LC_EXIST_REG.FECHA_PROCESAMIENTO
              AND F.TIPO_SVA               = 'GPRS'
              AND F.CORREGIDO              = 'N';
               
        END IF;
     END IF;

   END LOOP;
   
 EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR := 'Error Al tratar de ingresar la operadora no encontrada. '||SQLERRM;
     PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
 END;           
  
EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_RETARIFICA_GPRS_V2;

/*****************************************************************************
*Al numero que estoy retarificando en GPRS verifico si tiene otro feature para 
que salga del proceso y vuelva a ejecutar el otro feature
******************************************************************************/
PROCEDURE GSI_BUSCAR_FEATURES(PN_NUM_FEAT_ACTUAL     IN OUT NUMBER,
                              PN_NUM_FEATURES        IN NUMBER,
                              PN_CONSUMO_BYTES       IN OUT NUMBER,
                              PD_FECHA_CONSUMO       IN DATE,
                              PV_REMARK_ADICIONAL    IN VARCHAR2,
                              PR_UDR                 IN OUT GSI_OBJ_RETARIFICACIONES.RECORD_UDR,
                              PN_COSTO_BYTE          IN OUT NUMBER,
                              PB_FEAT_FOUND          OUT BOOLEAN,
                              PN_KBREST              IN OUT NUMBER,
                              PC_FEATURES_TMP        OUT C_FEATURES_TMP%ROWTYPE,
                              PB_EXIT                OUT BOOLEAN,
                              PV_ERROR               OUT VARCHAR2) IS
                              
 LV_PROGRAMA           VARCHAR2(100):='GSI_RETARIFICACIONES_ROAMING.GSI_BUSCAR_FEATURES';                           
 LD_FECHA_CONSUMO      DATE         := PD_FECHA_CONSUMO;
 LR_UDR                GSI_OBJ_RETARIFICACIONES.RECORD_UDR := PR_UDR;
 LC_FEATURES_TMP_ANT   C_FEATURES_TMP%ROWTYPE;
 LC_FEATURES_TMP       C_FEATURES_TMP%ROWTYPE;
 --*
 LV_FECHA_DESDE_ACT    VARCHAR2(50);
 LV_FECHA_HASTA_ACT    VARCHAR2(50);
 LE_ERROR              EXCEPTION;
 LV_ALTER_S            VARCHAR2(100):=NULL;
 
BEGIN
 
 LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY''';
 EXECUTE IMMEDIATE LV_ALTER_S;
  
 --Doy por finalizado el feature al que ya no le alcanzan los bytes
 UPDATE GSI_FEATURES_TMP
    SET BYTES_LEFT = 0,
        PROCESADO   = 'S'    
  WHERE NUM_FEATURE = PN_NUM_FEAT_ACTUAL;
  
 --Dejo listo el nuevo valor de consumo restandole lo poco que le quedaba 
 --al feature que no le alcanzan los bytes
 PN_CONSUMO_BYTES := PN_CONSUMO_BYTES - PN_KBREST;
 PB_FEAT_FOUND    := FALSE;

--Busco si el client tiene otro feature  
 WHILE PN_NUM_FEATURES > PN_NUM_FEAT_ACTUAL AND NOT PB_FEAT_FOUND LOOP
  
     PN_NUM_FEAT_ACTUAL := PN_NUM_FEAT_ACTUAL+1;
     LC_FEATURES_TMP_ANT := LC_FEATURES_TMP;
     
      OPEN C_FEATURES_TMP(PN_NUM_FEAT_ACTUAL);
     FETCH C_FEATURES_TMP INTO LC_FEATURES_TMP;
     CLOSE C_FEATURES_TMP;
     
      LV_FECHA_DESDE_ACT  := TO_CHAR(LC_FEATURES_TMP.FECHA_DESDE,'dd/mm/yyyy');
      LV_FECHA_HASTA_ACT  := TO_CHAR(LC_FEATURES_TMP.FECHA_HASTA,'dd/mm/yyyy');       
      
     IF LV_FECHA_DESDE_ACT <= LD_FECHA_CONSUMO AND
        LV_FECHA_HASTA_ACT >= LD_FECHA_CONSUMO THEN
            
        LR_UDR.DES_PAQUETE := LC_FEATURES_TMP.DESCRIPCION;
        LR_UDR.ID_PAQUETE  := LC_FEATURES_TMP.FEATURE;
        LR_UDR.ZONA        := LC_FEATURES_TMP.ZONA;        
        PB_EXIT            := TRUE;
      
     ELSE--Si el inicio de paquete no concilia con el tr�fico de datos

        LR_UDR.DATA_VOLUME_NEW       := PN_CONSUMO_BYTES;
        LR_UDR.RATED_FLAT_AMOUNT_NEW := ROUND(LR_UDR.DATA_VOLUME_NEW*PN_COSTO_BYTE,5);
        LR_UDR.REMARK_NEW            := PV_REMARK_ADICIONAL;  
        PN_KBREST                    := 0;
        LR_UDR.SALDO_PAQUETE         := PN_KBREST;
        LC_FEATURES_TMP              := LC_FEATURES_TMP_ANT;
        PN_NUM_FEAT_ACTUAL           := PN_NUM_FEAT_ACTUAL-1;
        PB_FEAT_FOUND                := TRUE;

     END IF;

 END LOOP;
  
 PC_FEATURES_TMP := LC_FEATURES_TMP;

 PR_UDR := LR_UDR;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;    
  WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_BUSCAR_FEATURES;

/*****************************************************************************
*Verifica si tiene algun paquete Rmundo para tasarlo al costo correspondiente
******************************************************************************/
PROCEDURE GSI_SET_EVENTOS_RMUNDO(PN_NUM_FEAT_ACTUAL     NUMBER,
                                 PN_NUM_FEATURES        NUMBER,
                                 PN_DATA_VOLUME_NEW     NUMBER,
                                 PD_FECHA_CONSUMO       DATE,
                                 PD_FEC_HASTA_FEAT_ACT  DATE,
                                 PV_REMARK_PAQUETE      IN VARCHAR2,
                                 PV_REMARK_EVENTOS      IN VARCHAR2,
                                 PV_REMARK_ADICIONALES  IN VARCHAR2,
                                 PR_UDR                 IN OUT GSI_OBJ_RETARIFICACIONES.RECORD_UDR,
                                 PN_COSTO_BYTE          NUMBER,
                                 PV_ERROR               OUT VARCHAR2)IS

 LV_PROGRAMA           VARCHAR2(100):='GSI_RETARIFICACIONES_ROAMING.GSI_SET_EVENTOS_RMUNDO';
 --*
 LN_NUM_FEAT_ACTUAL    NUMBER  := 0;
 LN_NUM_FEATURES       NUMBER  := 0;
 LN_DATA_VOLUME_NEW    NUMBER  := 0;
 LD_FECHA_CONSUMO      DATE    := NULL;
 LD_FEC_HASTA_FEAT_ACT DATE    := NULL;
 LN_COSTO_BYTE         NUMBER  := 0;
 LC_FEATURES_TMP       C_FEATURES_TMP%ROWTYPE;
 LB_PAQRMUNDO          BOOLEAN;
 LR_UDR                GSI_OBJ_RETARIFICACIONES.RECORD_UDR := NULL;
 LV_FEATURE            VARCHAR2(10);
 LV_COSTOADIC          VARCHAR2(25):= NULL;
 LC_COSTOS_ROA         C_MAESTRA_ROAMING%ROWTYPE;
 LB_FOUND_FEAT         BOOLEAN := FALSE;

 CURSOR C_VERIFICA_RMUNDO IS
  SELECT P.FEATURE
    FROM GSI_FEATURES_TMP P
    WHERE P.ZONA = 'RMUNDO';

 CURSOR C_ZONA_PAQUETE(CV_ZONA VARCHAR2)IS 
  SELECT P.*
    FROM GSI_FEATURES_TMP P
   WHERE P.ZONA = CV_ZONA;

 LB_FOUND_ZONA      BOOLEAN:=FALSE;     
 LC_ZONA_PAQUETE    C_ZONA_PAQUETE%ROWTYPE;
 
BEGIN
  
 LD_FEC_HASTA_FEAT_ACT:= PD_FEC_HASTA_FEAT_ACT;
 LN_NUM_FEAT_ACTUAL   := PN_NUM_FEAT_ACTUAL;
 LN_NUM_FEATURES      := PN_NUM_FEATURES;
 LN_DATA_VOLUME_NEW   := PN_DATA_VOLUME_NEW;
 LR_UDR               := PR_UDR;
 LD_FECHA_CONSUMO     := PD_FECHA_CONSUMO;
 LN_COSTO_BYTE        := PN_COSTO_BYTE;
 LB_PAQRMUNDO         := FALSE;

 IF LN_NUM_FEAT_ACTUAL >= LN_NUM_FEATURES THEN--SI NO TIENE OTRO FEATURE
    
       OPEN C_ZONA_PAQUETE('RMUNDO');
      FETCH C_ZONA_PAQUETE INTO LC_ZONA_PAQUETE;
      LB_FOUND_ZONA := C_ZONA_PAQUETE%FOUND;
      CLOSE C_ZONA_PAQUETE;
      
      IF LB_FOUND_ZONA THEN
          OPEN C_MAESTRA_ROAMING(LC_ZONA_PAQUETE.FEATURE);
         FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
         CLOSE C_MAESTRA_ROAMING;
         
         IF LC_COSTOS_ROA.COSTO_ROA_GPRS IS NULL THEN 
            LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ADIC_ESP;
         ELSE 
            LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ROA_GPRS;
         END IF;             
         
         IF LV_COSTOADIC IS NULL THEN
             OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
            FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
            CLOSE C_MAESTRA_ROAMING; 
            
            LN_COSTO_BYTE := TO_NUMBER(LC_COSTOS_ROA.COSTO_ROA_GPRS);  
         ELSE
            LN_COSTO_BYTE := TO_NUMBER(LV_COSTOADIC);
         END IF;    
       
         --SI NO TIENE PAQETE RMUNDO SETEAR NAVEGACI�N POR EVENTOS DIRECTAMENTE
         LR_UDR.REMARK_NEW      := PV_REMARK_EVENTOS;
         LR_UDR.DATA_VOLUME_NEW := LN_DATA_VOLUME_NEW;
         LR_UDR.RATED_FLAT_AMOUNT_NEW := ROUND(LR_UDR.DATA_VOLUME_NEW*LN_COSTO_BYTE,5);
         LR_UDR.SALDO_PAQUETE  := LC_ZONA_PAQUETE.BYTES_LEFT; 
         LR_UDR.ID_PAQUETE     := LC_ZONA_PAQUETE.FEATURE;
         LR_UDR.DES_PAQUETE    := LC_ZONA_PAQUETE.DESCRIPCION;        
         PR_UDR:= LR_UDR;   
       ELSE

          OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
         FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
         CLOSE C_MAESTRA_ROAMING; 
            
         LN_COSTO_BYTE := TO_NUMBER(LC_COSTOS_ROA.COSTO_ROA_GPRS);  
         
         --SI NO TIENE PAQETE RMUNDO SETEAR NAVEGACI�N POR EVENTOS PAQUETE RMUNDO SOLO AMERICA
         LR_UDR.REMARK_NEW      := PV_REMARK_EVENTOS;
         LR_UDR.DATA_VOLUME_NEW := LN_DATA_VOLUME_NEW;
         LR_UDR.RATED_FLAT_AMOUNT_NEW := ROUND(LR_UDR.DATA_VOLUME_NEW*LN_COSTO_BYTE,5);
         LR_UDR.SALDO_PAQUETE  := LC_ZONA_PAQUETE.BYTES_LEFT; 
         LR_UDR.ID_PAQUETE     := LC_ZONA_PAQUETE.FEATURE;
         LR_UDR.DES_PAQUETE    := LC_ZONA_PAQUETE.DESCRIPCION;        
         PR_UDR:= LR_UDR;         
       END IF;
      
    RETURN;
 END IF;
 
 LC_FEATURES_TMP := NULL;
 
 WHILE LN_NUM_FEAT_ACTUAL < LN_NUM_FEATURES AND NOT LB_PAQRMUNDO LOOP

    LN_NUM_FEAT_ACTUAL := LN_NUM_FEAT_ACTUAL+1;
       
     OPEN C_FEATURES_TMP(LN_NUM_FEAT_ACTUAL);
    FETCH C_FEATURES_TMP INTO LC_FEATURES_TMP;
    CLOSE C_FEATURES_TMP;
               
    IF LC_FEATURES_TMP.FECHA_DESDE <= LD_FEC_HASTA_FEAT_ACT AND
       LC_FEATURES_TMP.FECHA_DESDE <= LD_FECHA_CONSUMO      AND
       LC_FEATURES_TMP.FECHA_HASTA >= LD_FECHA_CONSUMO      AND
       LC_FEATURES_TMP.ZONA IN ('RMUNDO','AMERIMUNDO')      THEN
            
       IF LC_FEATURES_TMP.BYTES_LEFT >= LN_DATA_VOLUME_NEW THEN
     
          LB_PAQRMUNDO:= TRUE;

          UPDATE GSI_FEATURES_TMP
             SET BYTES_LEFT  = BYTES_LEFT - LN_DATA_VOLUME_NEW,
                 PROCESADO   = 'S'
           WHERE NUM_FEATURE = LN_NUM_FEAT_ACTUAL;

          LR_UDR.REMARK_NEW      := PV_REMARK_PAQUETE;
          LR_UDR.DATA_VOLUME_NEW := 0;
          LR_UDR.RATED_FLAT_AMOUNT_NEW := 0;
                                  
          LR_UDR.ID_PAQUETE   := LC_FEATURES_TMP.FEATURE;
          LR_UDR.DES_PAQUETE  := LC_FEATURES_TMP.DESCRIPCION;
          LR_UDR.SALDO_PAQUETE:= LC_FEATURES_TMP.BYTES_LEFT-LN_DATA_VOLUME_NEW;

       ELSE    
           OPEN C_VERIFICA_RMUNDO;
          FETCH C_VERIFICA_RMUNDO INTO LV_FEATURE;
          LB_FOUND_FEAT:= C_VERIFICA_RMUNDO%FOUND;
          CLOSE C_VERIFICA_RMUNDO;
          
          IF LB_FOUND_FEAT THEN

              OPEN C_MAESTRA_ROAMING(LV_FEATURE);
             FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
             CLOSE C_MAESTRA_ROAMING;
                 
             IF LC_COSTOS_ROA.COSTO_ROA_GPRS IS NULL THEN 
                --Si no tiene costo normal, entonces tiene costo especial 
                LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ADIC_ESP;
             ELSE 
                LV_COSTOADIC := LC_COSTOS_ROA.COSTO_ROA_GPRS;
             END IF;   
             
          END IF;
          IF LV_COSTOADIC IS NULL THEN
              OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
             FETCH C_MAESTRA_ROAMING INTO LC_COSTOS_ROA;
             CLOSE C_MAESTRA_ROAMING; 
             
             LV_COSTOADIC := TO_NUMBER(LC_COSTOS_ROA.COSTO_ROA_GPRS);              
          END IF;

          LN_COSTO_BYTE := TO_NUMBER(LV_COSTOADIC);
          
          --Debe ser por adicioanles ya que aun esta dentro del tiempo de vigencia
          LR_UDR.REMARK_NEW      := PV_REMARK_ADICIONALES;
          LR_UDR.DATA_VOLUME_NEW := LN_DATA_VOLUME_NEW;---LC_FEATURES_TMP.BYTES_LEFT;--CONSUMO FUERA DE PAQUETE
          LR_UDR.RATED_FLAT_AMOUNT_NEW := ROUND(LR_UDR.DATA_VOLUME_NEW*LN_COSTO_BYTE,5);
          LR_UDR.ID_PAQUETE   := LC_FEATURES_TMP.FEATURE;
          LR_UDR.DES_PAQUETE  := LC_FEATURES_TMP.DESCRIPCION;
          LR_UDR.SALDO_PAQUETE:= LC_FEATURES_TMP.BYTES_LEFT;          
       
          --EL PAQUETE SE QUEDA EN 0'               
          UPDATE GSI_FEATURES_TMP
             SET BYTES_LEFT  = 0,
                 PROCESADO   = 'S'             
           WHERE NUM_FEATURE = LN_NUM_FEAT_ACTUAL;

       END IF;
    ELSE
       --SI NO TIENE PAQETE RMUNDO SETEAR NAVEGACI�N POR EVENTOS DIRECTAMENTE
       LR_UDR.REMARK_NEW            := PV_REMARK_EVENTOS;
       LR_UDR.DATA_VOLUME_NEW       := LN_DATA_VOLUME_NEW;
       LR_UDR.RATED_FLAT_AMOUNT_NEW := ROUND(LR_UDR.DATA_VOLUME_NEW*LN_COSTO_BYTE,5);
    END IF;

 END LOOP;
         
 PR_UDR := LR_UDR;

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_SET_EVENTOS_RMUNDO;

/***********************************************************************************
*4.- Ejecuta el procedimiento que realiza la tasaci�n de los udrs de navegaci�n SMS
************************************************************************************/
PROCEDURE GSI_PROCESA_SMS_PRINCIPAL (PD_FECHA_CORTE       IN  DATE,
                                     PV_REPROCESO         IN  VARCHAR2,
                                     PV_CODIGO_ERROR      OUT VARCHAR2,                                     
                                     PV_ERROR             OUT VARCHAR2)IS
   
 LV_PROGRAMA          VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.GSI_PROCESA_SMS_PRINCIPAL';
 LV_ID_PROCESO        VARCHAR2(3)   := '4';
 LV_FECHA_NEW_ESQUEMA VARCHAR2(30)  := NULL;
 LD_FECHA_INI         DATE;
 LD_FECHA_FIN         DATE;
 LV_TABLA             VARCHAR2(100) := 'sysadm.Udr_Lt_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMM')||'_Sms';
 LV_FORMULA_FECH      VARCHAR2(15)  := NULL;
 LV_WHERE             VARCHAR2(5000):= NULL;
 LE_ERROR             EXCEPTION;
 LV_RANGO_FECH_CORTE  VARCHAR2(5000):= NULL;
 LV_ALTER_S           VARCHAR2(100) := NULL;
 LN_ECONTRADO         VARCHAR2(1); 
 LV_ERROR_ARMA_MSJ    VARCHAR2(4000);
 LE_ERROR_ESPERADO    EXCEPTION;
 
BEGIN

  --Bitacorizo inicio de Proceso
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'INICIADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              NVL(PV_REPROCESO,'N'),
                              LV_PROGRAMA,
                              PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;

 GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_PROCESOS( LV_ID_PROCESO,
                                                    PD_FECHA_CORTE,
                                                    PV_REPROCESO,
                                                    PV_CODIGO_ERROR,
                                                    PV_ERROR);

 IF PV_CODIGO_ERROR = '1' THEN
    RAISE LE_ERROR_ESPERADO;
 ELSIF PV_CODIGO_ERROR = '2' THEN
    RAISE LE_ERROR;
 END IF;
  
 LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
 EXECUTE IMMEDIATE LV_ALTER_S;

 --Depurar informacion de consumo sms
 GSI_GET_RANGO_DIAS(PD_FECHA_CORTE    => PD_FECHA_CORTE,
                    PD_FECHA_INI      => LD_FECHA_INI,
                    PD_FECHA_FIN      => LD_FECHA_FIN,
                    PB_INTERNACIONAL  => TRUE,
                    PV_ERROR          => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;

  OPEN C_PARAMETROS(6071,'FECHA_NEW_ESQUEMA');
 FETCH C_PARAMETROS INTO LV_FECHA_NEW_ESQUEMA;
 CLOSE C_PARAMETROS;

  OPEN C_PARAMETROS(6071,'FORMULA_FECHA');
 FETCH C_PARAMETROS INTO LV_FORMULA_FECH;
 CLOSE C_PARAMETROS;
 
 --Elimina Registros duplicados
 GSI_RETARIFICACIONES_ROAMING.GSI_REGISTROS_DUPLI( PD_FECHA_CORTE   => PD_FECHA_CORTE,
                                                   PV_FECHA_NEW_ESQ => LV_FECHA_NEW_ESQUEMA,
                                                   PV_FORMULA_FECHA => LV_FORMULA_FECH,
                                                   PD_FECHA_INI     => LD_FECHA_INI,
                                                   PD_FECHA_FIN     => LD_FECHA_FIN,
                                                   PV_TIPO          => 'Sms',
                                                   PV_ERROR         => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF; 
 
 GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_LOG_SERVICIOS_FALLIDOS',
                                                 'WHERE TIPO_SVA = ''SMS''',
                                                 PV_ERROR);
 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;
 
 GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_ACTIVACIONES_PAQPORT',
                                                 NULL,
                                                 PV_ERROR);
 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;
 
 --Pregunta si es el mismo Mes
 IF TO_CHAR(LD_FECHA_INI,'MMYYYY') = TO_CHAR(LD_FECHA_FIN,'MMYYYY') THEN  

    LV_WHERE:= ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset '||LV_FORMULA_FECH||' ), 0)>=to_date('''||LV_FECHA_NEW_ESQUEMA||''',''dd/mm/yyyy hh24:mi:ss'')
                 and entry_date_timestamp >= TO_DATE('''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'' ) '|| 
                'and entry_date_timestamp <= TO_DATE('''||TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''' ,''DD/MM/YYYY HH24:MI:SS'') '||
                'and (s_p_number_address=0 or s_p_number_address is null)';

    LV_RANGO_FECH_CORTE:= ' and entry_date_timestamp>='||'TO_DATE( '''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'')'||
                          ' and entry_date_timestamp<='||'TO_DATE( '''||TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'')';

   
    
    ---se debe de llamar a los que son sms gratis
    GSI_RETARIFICACIONES_ROAMING.GSI_DEPURA_SMS_SIN_COSTO( PV_TABLA         => LV_TABLA,
                                                           PV_FORMULA_FECHA => LV_FORMULA_FECH,
                                                           PV_FECHA_NEW_ESQ => LV_FECHA_NEW_ESQUEMA,
                                                           PD_FECHA_INI     => LD_FECHA_INI,
                                                           PD_FECHA_FIN     => LD_FECHA_FIN,
                                                           PV_ERROR         => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF; 
                          
    GSI_RETARIFICA_SMS(PD_FECHA_CORTE      => PD_FECHA_CORTE,
                       PV_RANGO_FECH_CORTE => LV_RANGO_FECH_CORTE,
                       PV_FECHA_NEW_ESQ    => LV_FECHA_NEW_ESQUEMA,
                       PV_FORMULA_FECH     => LV_FORMULA_FECH,
                       PV_TABLA            => LV_TABLA,
                       PB_UPDATE_ACTI      => TRUE,                       
                       PV_ERROR            => PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF;

 ELSE
     --Primero ejecuto la tabla cuya fecha inicio es el mes anterior
    LV_TABLA:= REPLACE(LV_TABLA,TO_CHAR(PD_FECHA_CORTE,'YYYYMM'),TO_CHAR(ADD_MONTHS(PD_FECHA_CORTE,-1),'YYYYMM'));

    LV_WHERE:= ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset '||LV_FORMULA_FECH||' ), 0)>=to_date('''||LV_FECHA_NEW_ESQUEMA||''',''dd/mm/yyyy hh24:mi:ss'')
                 and entry_date_timestamp>=TO_DATE('''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'' ) '||
               ' and (s_p_number_address=0 or s_p_number_address is null)';

    LV_RANGO_FECH_CORTE:= ' and entry_date_timestamp>='||
                          'TO_DATE( '''||TO_CHAR(LD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'')';

  
    
    ---se debe de llamar a los que son sms gratis
    GSI_RETARIFICACIONES_ROAMING.GSI_DEPURA_SMS_SIN_COSTO( PV_TABLA         => LV_TABLA,
                                                           PV_FORMULA_FECHA => LV_FORMULA_FECH,
                                                           PV_FECHA_NEW_ESQ => LV_FECHA_NEW_ESQUEMA,
                                                           PD_FECHA_INI     => LD_FECHA_INI,
                                                           PD_FECHA_FIN     => LD_FECHA_FIN,
                                                           PV_ERROR         => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF; 
    
    GSI_RETARIFICA_SMS(PD_FECHA_CORTE       => PD_FECHA_CORTE,
                       PV_RANGO_FECH_CORTE  => LV_RANGO_FECH_CORTE,
                       PV_FECHA_NEW_ESQ     => LV_FECHA_NEW_ESQUEMA,
                       PV_FORMULA_FECH      => LV_FORMULA_FECH,
                       PV_TABLA             => LV_TABLA,
                       PB_UPDATE_ACTI       => FALSE,                                              
                       PV_ERROR             => PV_ERROR);                             

    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF;

     --Segundo ejecuto la tabla cuya fecha fin es el mes actual
    LV_WHERE:= ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset '||LV_FORMULA_FECH||' ), 0)>=to_date('''||LV_FECHA_NEW_ESQUEMA||''',''dd/mm/yyyy hh24:mi:ss'')
                 and entry_date_timestamp<=TO_DATE('''||TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'' ) '||
               ' and (s_p_number_address=0 or s_p_number_address is null)';
                
    LV_TABLA:= REPLACE(LV_TABLA,TO_CHAR(ADD_MONTHS(PD_FECHA_CORTE,-1),'YYYYMM'),TO_CHAR(PD_FECHA_CORTE,'YYYYMM'));

   
     
    LV_RANGO_FECH_CORTE:= ' and entry_date_timestamp<='||
                          'TO_DATE( '''||TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''DD/MM/YYYY HH24:MI:SS'')';

        ---se debe de llamar a los que son sms gratis
    GSI_RETARIFICACIONES_ROAMING.GSI_DEPURA_SMS_SIN_COSTO( PV_TABLA         => LV_TABLA,
                                                           PV_FORMULA_FECHA => LV_FORMULA_FECH,
                                                           PV_FECHA_NEW_ESQ => LV_FECHA_NEW_ESQUEMA,
                                                           PD_FECHA_INI     => LD_FECHA_INI,
                                                           PD_FECHA_FIN     => LD_FECHA_FIN,
                                                           PV_ERROR         => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF; 
    
    GSI_RETARIFICA_SMS(PD_FECHA_CORTE      => PD_FECHA_CORTE,
                       PV_RANGO_FECH_CORTE => LV_RANGO_FECH_CORTE,
                       PV_FECHA_NEW_ESQ    => LV_FECHA_NEW_ESQUEMA,
                       PV_FORMULA_FECH     => LV_FORMULA_FECH,
                       PV_TABLA            => LV_TABLA,
                       PB_UPDATE_ACTI      => TRUE,                       
                       PV_ERROR            => PV_ERROR);                             

    IF PV_ERROR IS NOT NULL THEN
       PV_CODIGO_ERROR:= '2';
       RAISE LE_ERROR;
    END IF;

 END IF;
 
 COMMIT;
  
 --Bitacorizo a estado PROCESADO
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'PROCESADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              NVL(PV_REPROCESO,'N'),
                              LV_PROGRAMA,
                              PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    PV_CODIGO_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;

 PV_CODIGO_ERROR:='0';

EXCEPTION
 WHEN LE_ERROR_ESPERADO THEN
   ROLLBACK;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'WARNING',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 NVL(PV_REPROCESO,'N'),
                                 LV_PROGRAMA,
                                 PV_ERROR);   
 WHEN LE_ERROR THEN
   ROLLBACK;  
   PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR; 
   GSI_BITACORIZA_PROCESOS_ROA(LV_ID_PROCESO,
                               'ERROR',
                               PD_FECHA_CORTE,
                               SYSDATE,
                               NVL(PV_REPROCESO,'N'),
                               LV_PROGRAMA,
                               PV_ERROR);

   GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                  PV_ERROR => LV_ERROR_ARMA_MSJ);                                               
 WHEN OTHERS THEN
   ROLLBACK;
   PV_CODIGO_ERROR:= '2';
   PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
   GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                'ERROR',
                                PD_FECHA_CORTE,
                                SYSDATE,
                                NVL(PV_REPROCESO,'N'),
                                LV_PROGRAMA,
                                PV_ERROR);

   GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                  PV_ERROR => LV_ERROR_ARMA_MSJ);   
END GSI_PROCESA_SMS_PRINCIPAL;

/*****************************************************************************
*Proceso que realiza la clasificacion en costo y remark para los registos SMS
******************************************************************************/
PROCEDURE GSI_RETARIFICA_SMS(PD_FECHA_CORTE       DATE,
                             PV_RANGO_FECH_CORTE  VARCHAR2,
                             PV_FECHA_NEW_ESQ     VARCHAR2,
                             PV_FORMULA_FECH      VARCHAR2,
                             PV_TABLA             VARCHAR2,
                             PB_UPDATE_ACTI       BOOLEAN,
                             PV_ERROR             OUT VARCHAR2) IS

 CURSOR REG_CLI IS
  SELECT A.ROWID, A.*
    FROM GSI_PAQROAMING_ACTIVADOS A
   ORDER BY ID_SERVICIO, FECHA_DESDE;
   
 CURSOR C_OPER_FALLIDA(CV_IMSI       VARCHAR2, 
                       CD_FECHA_CORT VARCHAR2, 
                       CV_COD_OPER   VARCHAR2, 
                       CV_TIPO_SVA   VARCHAR2, 
                       CV_ESTADO     VARCHAR2) IS 
  SELECT T.*
    FROM GSI_LOG_SERVICIOS_FALLIDOS T
   WHERE T.IMSI = CV_IMSI
     AND T.FECHA_CORTE = CD_FECHA_CORT
     AND T.COD_OPERADORA_NO_FOUND = CV_COD_OPER
     AND T.TIPO_SVA = CV_TIPO_SVA
     AND T.CORREGIDO = CV_ESTADO;

 CURSOR C_COUNT_ACTIVACION(CV_SERVICIO VARCHAR2, CN_CONTRATO NUMBER)IS 
  SELECT COUNT(DISTINCT(ZONA))
    FROM GSI_PAQROAMING_ACTIVADOS A
   WHERE A.ID_SERVICIO = CV_SERVICIO
     AND A.ID_CONTRATO = CN_CONTRATO;

 CURSOR C_ZONA_PAQUETE(CV_TELEFONO VARCHAR2, CV_ZONA VARCHAR2)IS 
  SELECT P.*
    FROM GSI_PAQROAMING_ACTIVADOS P
   WHERE P.ID_SERVICIO = CV_TELEFONO
     AND P.ZONA = CV_ZONA
   ORDER BY P.FECHA_DESDE DESC;             

 LV_PROGRAMA            VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_RETARIFICA_SMS';
 LV_SENTENCIA           VARCHAR2(5000):= NULL;
 LN_COSTO               NUMBER;
 LV_ZONA                VARCHAR2(20);
 LD_FECHA_INI           DATE;
 LD_FECHA_FIN           DATE;
 REG_UDR                VAR_CURSOR;
 LV_UPDATE              VARCHAR2(500) := NULL;
 J                      RECORD_UDR;
 G                      RECORD_UDR_2;
 LV_UPDATE_SMS_GEN      VARCHAR2(5000):= NULL;
 LV_REMARK_SMS_EVENTO   VARCHAR2(50)  := NULL;
 --**
 LC_DETALL_FEAT         C_MAESTRA_ROAMING%ROWTYPE;
 LV_SENTENCIA_2         VARCHAR2(5000):= NULL;
 --**
 LN_COUNT_R             NUMBER;
 LC_EXIST_REG           C_OPER_FALLIDA%ROWTYPE;
 LB_EXISTE              BOOLEAN:= FALSE;
 LN_COUNT_ZONA          NUMBER:=0;
 LC_DATOS_FEATURE_AM    C_MAESTRA_ROAMING%ROWTYPE;
 LC_DATOS_FEATURE_RM    C_MAESTRA_ROAMING%ROWTYPE;
 LE_ERROR               EXCEPTION;
 LV_REMARK_NEW          VARCHAR2(1000):=NULL; 
 LC_ZONA_PAQUETE        C_ZONA_PAQUETE%ROWTYPE;
 LB_FOUND_ZONA          BOOLEAN:=FALSE;
 
BEGIN

 GSI_GET_RANGO_DIAS(PD_FECHA_CORTE    => PD_FECHA_CORTE,
                    PD_FECHA_INI      => LD_FECHA_INI,
                    PD_FECHA_FIN      => LD_FECHA_FIN,
                    PB_INTERNACIONAL  => TRUE,
                    PV_ERROR          => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
   
 --Costo general de sms (Cuando no es enviado dentro de vegencia de paquete)
  OPEN C_PARAMETROS(6071,'REMARK_SMS_EVENTO');
 FETCH C_PARAMETROS INTO LV_REMARK_SMS_EVENTO;
 CLOSE C_PARAMETROS;
   
  OPEN C_MAESTRA_ROAMING('EVENT-AME');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_AM;
 CLOSE C_MAESTRA_ROAMING;

  OPEN C_MAESTRA_ROAMING('EVENT-RMUN');
 FETCH C_MAESTRA_ROAMING INTO LC_DATOS_FEATURE_RM;
 CLOSE C_MAESTRA_ROAMING;
 
 --***SMS EVENTOS***--
  BEGIN 
   LV_UPDATE_SMS_GEN:= 
    ' UPDATE '||PV_TABLA||
    ' SET rated_flat_amount=:costo_sms_evento,  remark='''||LV_REMARK_SMS_EVENTO||
    ''' where substr(export_file,3,5) in (select tap_source_code '||
                                        'from gsi_maestra_operadoras '||
                                        'where continente='||':continente'|| ')'||
    'and (initial_start_time_timestamp + NVL'|| 
    '((initial_start_time_time_offset '||PV_FORMULA_FECH||'), 0))>=to_date('''||PV_FECHA_NEW_ESQ||''',''DD/MM/YYYY HH24:MI:SS'')'||
      PV_RANGO_FECH_CORTE||
    'and follow_up_call_type=:TIPO_SMS_IN_OUT '||
    'and nvl(remark,'||'''L'''||') not in (select t.valor from gv_parametros@RTX_TO_BSCS_LINK t where t.id_tipo_parametro = 6071 '||
    'and t.nombre = '''||'REMARK_SMS_PUERTO'||''''||')';
    
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error al Actualizar Eventos ';
      PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;  
  END;
      
  --ENTRANTE - C.AMERICANO
  EXECUTE IMMEDIATE LV_UPDATE_SMS_GEN
  USING LC_DATOS_FEATURE_AM.COSTO_SMS_IN_ROA,'AMERICA',2;
  
  --ENTRANTE - R.MUNDO
  EXECUTE IMMEDIATE LV_UPDATE_SMS_GEN
  USING LC_DATOS_FEATURE_RM.COSTO_SMS_IN_ROA,'RMUNDO',2;

  --SALIENTE - C.AMERICANO
  EXECUTE IMMEDIATE LV_UPDATE_SMS_GEN
  USING LC_DATOS_FEATURE_AM.COSTO_SMS_OUT_ROA,'AMERICA',1;

  --SALIENTE - R.MUNDO
  EXECUTE IMMEDIATE LV_UPDATE_SMS_GEN
  USING LC_DATOS_FEATURE_RM.COSTO_SMS_OUT_ROA,'RMUNDO',1;
  
 --***SMS EVENTOS***--
 LV_SENTENCIA:=
  'select a.rowid, a.follow_up_call_type, a.Export_File, a.s_p_port_address
     from '||PV_TABLA||' a 
    where s_p_number_address=:numero
      and initial_start_time_timestamp + NVL 
      ((initial_start_time_time_offset '||PV_FORMULA_FECH||'), 0)>=to_date('''||PV_FECHA_NEW_ESQ||''',''dd/mm/yyyy hh24:mi:ss'')
      and (initial_start_time_timestamp + NVL 
      ((initial_start_time_time_offset '||PV_FORMULA_FECH||'), 0))>=to_date(:fech_ini,''DD/MM/YYYY HH24:MI:SS'')
      and (initial_start_time_timestamp + NVL 
      ((initial_start_time_time_offset '||PV_FORMULA_FECH||'), 0))<=nvl(to_date(:fech_fin,''DD/MM/YYYY HH24:MI:SS''),to_date(:fech_fin,''DD/MM/YYYY HH24:MI:SS'')) '
    ||PV_RANGO_FECH_CORTE||
    'and nvl(remark,'||'''L'''||') not in (select t.valor from gv_parametros@RTX_TO_BSCS_LINK t where t.id_tipo_parametro = 6071 '||
    'and t.nombre = '''||'REMARK_SMS_PUERTO'||''''||')'||   
    ' order by initial_start_time_timestamp';
   
 FOR I IN REG_CLI LOOP
   
   /*Verifica cuantos paquetes de diferente zona tiene en caso de tener 2 toma el valor del export_file*/
    OPEN C_COUNT_ACTIVACION(I.ID_SERVICIO, I.ID_CONTRATO);
   FETCH C_COUNT_ACTIVACION INTO LN_COUNT_ZONA;
   CLOSE C_COUNT_ACTIVACION; 
   
    OPEN REG_UDR FOR LV_SENTENCIA
   USING I.ID_SERVICIO,TO_CHAR(I.FECHA_DESDE,'DD/MM/YYYY HH24:MI:SS'),
         NVL(TO_CHAR(I.FECHA_HASTA,'DD/MM/YYYY HH24:MI:SS'),TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')),
         NVL(TO_CHAR(I.FECHA_HASTA,'DD/MM/YYYY HH24:MI:SS'),TO_CHAR(LD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS'));
   FETCH REG_UDR INTO J;

   LOOP

     EXIT WHEN REG_UDR%NOTFOUND;

     BEGIN
       LV_ZONA := NULL;
       LN_COSTO:= NULL;

       IF J.FOLLOW_UP_CALL_TYPE = 2 THEN -- 2-> SMS ENTRANTE
          SELECT A.CONTINENTE
            INTO LV_ZONA
            FROM GSI_MAESTRA_OPERADORAS A
           WHERE A.TAP_SOURCE_CODE = SUBSTR(J.EXPORT_FILE, 3, 5);           

           OPEN C_ZONA_PAQUETE(I.ID_SERVICIO, LV_ZONA);
          FETCH C_ZONA_PAQUETE INTO LC_ZONA_PAQUETE;
          LB_FOUND_ZONA := C_ZONA_PAQUETE%FOUND;
          CLOSE C_ZONA_PAQUETE;
          
          IF LB_FOUND_ZONA THEN
              OPEN C_MAESTRA_ROAMING(LC_ZONA_PAQUETE.FEATURE);
             FETCH C_MAESTRA_ROAMING INTO LC_DETALL_FEAT;
             CLOSE C_MAESTRA_ROAMING;
             --En caso de que algun Feature tiene costo SMS especial   
             IF NVL(LC_DETALL_FEAT.COSTO_SMS_IN_ROA,-1) >= 0 THEN 
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_IN_ROA;
             ELSE 
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_IN_ESP;             
             END IF;   
              
             IF NVL(LN_COSTO,-1) < 0 THEN
                IF LV_ZONA = 'AMERICA' THEN
                   LN_COSTO := LC_DATOS_FEATURE_AM.COSTO_SMS_IN_ROA;
                ELSE
                   LN_COSTO := LC_DATOS_FEATURE_RM.COSTO_SMS_IN_ROA; 
                END IF;
             END IF; 
          ELSE
              OPEN C_MAESTRA_ROAMING(I.FEATURE);
             FETCH C_MAESTRA_ROAMING INTO LC_DETALL_FEAT;
             CLOSE C_MAESTRA_ROAMING;

             IF NVL(LC_DETALL_FEAT.COSTO_SMS_IN_ESP,-1) >= 0 THEN
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_IN_ESP;
             ELSE
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_IN_ROA;
             END IF;             
             
             IF NVL(LN_COSTO,-1) < 0 THEN
                IF LC_DETALL_FEAT.ZONA = 'AMERICA' THEN
                   LN_COSTO := LC_DATOS_FEATURE_AM.COSTO_SMS_IN_ROA;
                ELSE
                   LN_COSTO := LC_DATOS_FEATURE_RM.COSTO_SMS_IN_ROA; 
                END IF;
             END IF; 
                          
          END IF;          
          LB_FOUND_ZONA:= FALSE;
          LC_DETALL_FEAT:=NULL;       
       ELSE  -- 1-> SMS SALIENTE

          SELECT A.CONTINENTE
            INTO LV_ZONA
            FROM GSI_MAESTRA_OPERADORAS A
           WHERE A.TAP_SOURCE_CODE = SUBSTR(J.EXPORT_FILE, 3, 5);           

           OPEN C_ZONA_PAQUETE(I.ID_SERVICIO, LV_ZONA);
          FETCH C_ZONA_PAQUETE INTO LC_ZONA_PAQUETE;
          LB_FOUND_ZONA := C_ZONA_PAQUETE%FOUND;
          CLOSE C_ZONA_PAQUETE;
          
          IF LB_FOUND_ZONA THEN
              OPEN C_MAESTRA_ROAMING(LC_ZONA_PAQUETE.FEATURE);
             FETCH C_MAESTRA_ROAMING INTO LC_DETALL_FEAT;
             CLOSE C_MAESTRA_ROAMING;
             --En caso de que algun Feature tiene costo SMS especial   
             IF NVL(LC_DETALL_FEAT.COSTO_SMS_OUT_ROA,-1) >= 0 THEN 
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_OUT_ROA;
             ELSE 
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_OUT_ESP;             
             END IF;   
              
             IF NVL(LN_COSTO,-1) < 0 THEN
                IF LV_ZONA = 'AMERICA' THEN
                   LN_COSTO := LC_DATOS_FEATURE_AM.COSTO_SMS_OUT_ROA;
                ELSE
                   LN_COSTO := LC_DATOS_FEATURE_RM.COSTO_SMS_OUT_ROA; 
                END IF;
             END IF; 
          ELSE
              OPEN C_MAESTRA_ROAMING(I.FEATURE);
             FETCH C_MAESTRA_ROAMING INTO LC_DETALL_FEAT;
             CLOSE C_MAESTRA_ROAMING;

             IF NVL(LC_DETALL_FEAT.COSTO_SMS_OUT_ESP,-1) >= 0 THEN
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_OUT_ESP;
             ELSE
                LN_COSTO := LC_DETALL_FEAT.COSTO_SMS_OUT_ROA;
             END IF;             
             
             IF NVL(LN_COSTO,-1) < 0 THEN
                IF LC_DETALL_FEAT.ZONA = 'AMERICA' THEN
                   LN_COSTO := LC_DATOS_FEATURE_AM.COSTO_SMS_OUT_ROA;
                ELSE
                   LN_COSTO := LC_DATOS_FEATURE_RM.COSTO_SMS_OUT_ROA; 
                END IF;
             END IF; 
                          
          END IF;          
             LB_FOUND_ZONA:= FALSE;
             LC_DETALL_FEAT:=NULL;   
       END IF;

       IF LV_ZONA = 'AMERICA' THEN
           OPEN C_PARAMETROS(6071,'REMARK_SMS_AMERICA');
          FETCH C_PARAMETROS INTO LV_REMARK_NEW;
          CLOSE C_PARAMETROS;
       ELSE 
           OPEN C_PARAMETROS(6071,'REMARK_SMS_RMUNDO');
          FETCH C_PARAMETROS INTO LV_REMARK_NEW;
          CLOSE C_PARAMETROS;     
       END IF;
       
       LV_UPDATE:='update '||PV_TABLA ||
                  ' set rated_flat_amount='||REPLACE(LN_COSTO,',','.')||', remark='''||LV_REMARK_NEW||''' 
                   where rowid='''||J.ROWID||''' ';

       EXECUTE IMMEDIATE LV_UPDATE;
     
     EXCEPTION
         --Si no se encuentra c�digo de operadora se registra en la tagbla de log
      WHEN NO_DATA_FOUND THEN
           INSERT INTO GSI_LOG_SERVICIOS_FALLIDOS
            (ID_SERVICIO,
             PROGRAMA,
             FECHA_CORTE,
             FECHA_PROCESAMIENTO,
             CORREGIDO,
             COD_OPERADORA_NO_FOUND,
             DESCRIPCION_ERROR,
             TIPO_SVA,
             IMSI)
           VALUES
            (I.ID_SERVICIO,
             LV_PROGRAMA,
             PD_FECHA_CORTE,
             SYSDATE,
             'N',
             SUBSTR(J.EXPORT_FILE, 3, 5),
             'NO SE ENCUENTRA C�DIGO DE OPERADORA.',
             'SMS',
             J.S_P_PORT_ADDRESS);
     END;

     FETCH  REG_UDR INTO J;

   END LOOP;
  
   IF PB_UPDATE_ACTI THEN
    UPDATE GSI_PAQROAMING_ACTIVADOS
       SET PROCESADO = 'F'
     WHERE ROWID = I.ROWID;
   END IF;

 END LOOP;

 BEGIN 
    LV_SENTENCIA_2 := 'Select s_p_number_address, 
                              o_p_number_address, 
                              s_p_port_address,
                              follow_up_call_type, 
                              export_file, 
                              rowid 
                       from '||PV_TABLA||
                      ' where (initial_start_time_timestamp + NVL '|| 
                      ' ((initial_start_time_time_offset '||PV_FORMULA_FECH||'), 0)) '||
                      ' >= to_date('''||PV_FECHA_NEW_ESQ||''',''DD/MM/YYYY HH24:MI:SS'') '||
                      PV_RANGO_FECH_CORTE||
                      ' and remark is null';
                      
   EXECUTE IMMEDIATE LV_SENTENCIA_2;
   
    OPEN REG_UDR FOR LV_SENTENCIA_2;
   FETCH REG_UDR INTO G;
   
   LOOP
     
     EXIT WHEN REG_UDR%NOTFOUND;

     SELECT COUNT(*)
       INTO LN_COUNT_R
       FROM GSI_MAESTRA_OPERADORAS
      WHERE TAP_SOURCE_CODE = SUBSTR(G.EXPORT_FILE, 3, 5);
     
     IF LN_COUNT_R = 0 THEN

         OPEN C_OPER_FALLIDA(G.S_P_PORT_ADDRESS, TO_CHAR(PD_FECHA_CORTE,'DD/MM/YYYY'), SUBSTR(G.EXPORT_FILE, 3, 5), 'SMS','N');
        FETCH C_OPER_FALLIDA INTO LC_EXIST_REG;
        LB_EXISTE:= C_OPER_FALLIDA%FOUND;
        CLOSE C_OPER_FALLIDA;

        IF NOT LB_EXISTE THEN 
           --Si no se encuentra c�digo de operadora se registra en la tagbla de log
           INSERT INTO GSI_LOG_SERVICIOS_FALLIDOS
             (ID_SERVICIO,
              PROGRAMA,
              FECHA_CORTE,
              FECHA_PROCESAMIENTO,
              CORREGIDO,
              COD_OPERADORA_NO_FOUND,
              DESCRIPCION_ERROR,
              TIPO_SVA,
              IMSI)
           VALUES
             (G.S_P_NUMBER_ADDRESS,
              LV_PROGRAMA,
              PD_FECHA_CORTE,
              SYSDATE,
              'N',
              SUBSTR(G.EXPORT_FILE, 3, 5),
              'NO SE ENCUENTRA C�DIGO DE OPERADORA.',
              'SMS',
              G.S_P_PORT_ADDRESS);
        
       ELSE
           UPDATE GSI_LOG_SERVICIOS_FALLIDOS F
              SET F.FECHA_PROCESAMIENTO    = SYSDATE
            WHERE F.IMSI                   = G.S_P_PORT_ADDRESS
              AND F.FECHA_CORTE            = PD_FECHA_CORTE
              AND F.COD_OPERADORA_NO_FOUND = SUBSTR(G.EXPORT_FILE, 3, 5)
              AND F.FECHA_PROCESAMIENTO    = LC_EXIST_REG.FECHA_PROCESAMIENTO
              AND F.TIPO_SVA               = 'SMS'
              AND F.CORREGIDO              = 'N';
               
        END IF;
     END IF;
     
     FETCH REG_UDR INTO G;
   END LOOP;
   
 EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR := 'Error Al tratar de ingresar la operadora no encontrada. '||SQLERRM;
     PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
   END;
 
EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_RETARIFICA_SMS;

/***********************************************************************************
*5.- Actualiza la tabla UDR desde la tabla de trabajo(Aplica solo para GPRS)
************************************************************************************/
PROCEDURE GSI_ACTUALIZA_UDR_MENSUAL(PD_FECHA_CORTE  DATE,
                                    PV_COD_ERROR    OUT VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2 ) IS

 LV_PROGRAMA          VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.GSI_ACTUALIZA_UDR_MENSUAL';
 LV_ID_PROCESO        VARCHAR2(3)   := '11';
 LV_TABLA             VARCHAR2(50)  := 'SYSADM.UDR_LT_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMM')||'_GRX';
 LV_SENT_CURSOR       VARCHAR2(9000):= NULL;
 LD_FECHA_INI         DATE;
 LD_FECHA_FIN         DATE;
 LD_AUX               DATE;
 LV_ALTER_S           VARCHAR2(100):= NULL;
 LV_ADD               VARCHAR2(300):= NULL;
 LE_ERROR             EXCEPTION;
 LV_TABLA_UDR_HIST    VARCHAR2(300) := NULL;
 LV_ERROR_ARMA_MSJ    VARCHAR2(4000):=NULL;
 LE_ERROR_ESPERADO    EXCEPTION;

BEGIN
 
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'INICIADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              'N',
                              LV_PROGRAMA,
                              PV_ERROR );

 IF PV_ERROR IS NOT NULL THEN
    PV_COD_ERROR:='2';
    RAISE LE_ERROR;
 END IF;

 GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_PROCESOS( LV_ID_PROCESO,
                                                     PD_FECHA_CORTE,
                                                     'N',
                                                     PV_COD_ERROR,
                                                     PV_ERROR);
 IF PV_COD_ERROR = '1' THEN
    RAISE LE_ERROR_ESPERADO;
 ELSIF PV_COD_ERROR = '2' THEN
    RAISE LE_ERROR;
 END IF;

 LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
 EXECUTE IMMEDIATE LV_ALTER_S;

 GSI_GET_RANGO_DIAS( PD_FECHA_CORTE    => PD_FECHA_CORTE,
                     PD_FECHA_INI      => LD_FECHA_INI,
                     PD_FECHA_FIN      => LD_FECHA_FIN,
                     PB_INTERNACIONAL  => TRUE,
                     PV_ERROR          => PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    PV_COD_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;
 
 LV_TABLA_UDR_HIST:= 'GSI_TRAFICO_GRX_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMMDD');
 
 LV_SENT_CURSOR:= 'select a.uds_stream_id, a.uds_record_id, a.uds_base_part_id, a.uds_charge_part_id, a.uds_free_unit_part_id, a.uds_special_purpose_part_id,
                   a.cust_info_contract_id, a.cust_info_customer_id, a.entry_date_timestamp, a.s_p_port_address, a.s_p_number_address, a.data_volume,
                   a.data_volume_new,a.rated_flat_amount, a.rated_flat_amount_new, a.id_paquete, a.des_paquete, a.saldo_paquete, a.zona, a.remark_new, a.remark, a.rowid
                   from '||LV_TABLA_UDR_HIST||' a
                   where a.Cust_Info_Dn_Id is null';
        
 IF TO_CHAR(LD_FECHA_INI,'MMYYYY') = TO_CHAR(LD_FECHA_FIN,'MMYYYY') THEN--Si es en el mismo mes

    GSI_OBJ_RETARIFICACIONES.UPDATE_UDR_MENSUAL( PV_TABLA       => LV_TABLA,
                                                 PV_SENT_CURSOR => LV_SENT_CURSOR,
                                                 PV_ERROR       => PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_COD_ERROR:= '2';
       RAISE LE_ERROR;
    END IF;

 ELSE
     --Primero proceso todo lo del mes anterior (mes inicio)
    LD_AUX:= LAST_DAY(  REPLACE(LD_FECHA_INI,TO_CHAR(LD_FECHA_INI,'HH24:MI:SS'),'23:59:59')  );
    LV_ADD:= ' and entry_date_timestamp>= to_date('''||LD_FECHA_INI||''',''dd/mm/yyyy hh24:mi:ss'')'||
             ' and entry_date_timestamp<= to_date('''||LD_AUX||''',''dd/mm/yyyy hh24:mi:ss'' )';
         
    LV_TABLA:= 'SYSADM.UDR_LT_'||TO_CHAR(LD_FECHA_INI,'YYYYMM')||'_GRX';

    GSI_OBJ_RETARIFICACIONES.UPDATE_UDR_MENSUAL( PV_TABLA       => LV_TABLA,
                                                 PV_SENT_CURSOR => LV_SENT_CURSOR||LV_ADD,
                                                 PV_ERROR       => PV_ERROR);

    IF PV_ERROR IS NOT NULL THEN
       PV_COD_ERROR:= '2';
       RAISE LE_ERROR;
    END IF;

    --Luego lo del mes actual (mes fin)
    LD_AUX:= TO_DATE('01'||TO_CHAR(LD_FECHA_FIN,'mm/yyyy')||' 00:00:00','dd/mm/yyyy hh24:mi:ss');
    LV_ADD:=' and entry_date_timestamp>= to_date('''||LD_AUX||''',''dd/mm/yyyy hh24:mi:ss'')'||
             ' and entry_date_timestamp<= to_date('''||LD_FECHA_FIN||''',''dd/mm/yyyy hh24:mi:ss'' )';

    lv_tabla:='SYSADM.UDR_LT_'||to_char(LD_FECHA_FIN,'yyyymm')||'_GRX';

    GSI_OBJ_RETARIFICACIONES.UPDATE_UDR_MENSUAL( PV_TABLA       => LV_TABLA,
                                                 PV_SENT_CURSOR => LV_SENT_CURSOR||LV_ADD,
                                                 PV_ERROR       => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR:= '2';
      RAISE LE_ERROR;
    END IF;

 END IF;

      --Bitacorizo a estado PROCESADO
 GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                              'PROCESADO',
                              PD_FECHA_CORTE,
                              SYSDATE,
                              'N',
                              LV_PROGRAMA,
                              PV_ERROR );

 IF PV_ERROR IS NOT NULL THEN
    PV_COD_ERROR:= '2';
    RAISE LE_ERROR;
 END IF;
 
 PV_COD_ERROR := '0';
 PV_ERROR     := NULL;
 
EXCEPTION
 WHEN LE_ERROR_ESPERADO THEN
   ROLLBACK;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'CANCELADO',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 'N',
                                 LV_PROGRAMA,
                                 PV_ERROR);    
  WHEN LE_ERROR THEN
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'ERROR',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 'N',
                                 LV_PROGRAMA,
                                 PV_ERROR);
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);
  WHEN OTHERS THEN
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
    PV_COD_ERROR:= '2';
    GSI_BITACORIZA_PROCESOS_ROA( LV_ID_PROCESO,
                                 'ERROR',
                                 PD_FECHA_CORTE,
                                 SYSDATE,
                                 'N',
                                 LV_PROGRAMA,
                                 PV_ERROR);
    GSI_RETARIFICACIONES_ROAMING.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                   PV_ERROR => LV_ERROR_ARMA_MSJ);                                 
END GSI_ACTUALIZA_UDR_MENSUAL;

/***********************************************************************************
*Utilizado para los paquetes Roaming Recurrentes se les dan los bytes totales 
************************************************************************************/
PROCEDURE GSI_ACTUALIZA_PAQUETE_RECURR( PD_FECHA  DATE, --Fecha Inicio de Corte
                                        PV_ERROR  OUT VARCHAR2) IS
 CURSOR REG IS 
  SELECT A.ROWID, A.*
    FROM GSI_PAQROAMING_ACTIVADOS A 
   WHERE FEATURE IN (SELECT FEATURE
                       FROM GSI_MAESTRA_FEATURE_ROAMGRX
                      WHERE CORTE = 'FAC');

 CURSOR CICLO_CLIENTE(CN_CONTRATO NUMBER) IS                      
  SELECT ID_CICLO
    FROM FA_CONTRATOS_CICLOS_BSCS@AXIS
   WHERE ID_CONTRATO = CN_CONTRATO
     AND FECHA_FIN IS NULL; --Es la forma de validar que este Activo
                      
 LV_PROGRAMA   VARCHAR2(500):= 'GSI_RETARIFICACIONES_ROAMING.ACTUALIZA_PAQUETE_RECURRENTE';
 LV_CICLO_CURR VARCHAR2(2)  := NULL;
 LV_CICLO_CLI  VARCHAR2(2)  := NULL;
 LV_SET        VARCHAR2(5000):= NULL;
 LV_WHERE      VARCHAR2(5000):= NULL;
 LE_ERROR      EXCEPTION;
 
BEGIN   
  
 IF TO_CHAR(PD_FECHA,'DD')= '24' THEN    --FECHA_CORTE 02 
    LV_CICLO_CURR:= '01';
 ELSIF TO_CHAR(PD_FECHA,'DD')= '08' THEN --FECHA_CORTE 15
    LV_CICLO_CURR:= '02';
 ELSIF TO_CHAR(PD_FECHA,'DD')= '15' THEN --FECHA_CORTE 24
    LV_CICLO_CURR:= '03';
 ELSIF TO_CHAR(PD_FECHA,'DD')= '02' THEN --FECHA_CORTE 08
    LV_CICLO_CURR:= '04';
 ELSE
    LV_CICLO_CURR:= '05';
 END IF;
 
 FOR I IN REG LOOP
   
    OPEN CICLO_CLIENTE(I.ID_CONTRATO);
   FETCH CICLO_CLIENTE INTO LV_CICLO_CLI;
   CLOSE CICLO_CLIENTE;
   
   IF LV_CICLO_CLI IS NULL THEN
      LV_CICLO_CLI:= '01';     
   END IF;  

   IF LV_CICLO_CURR = LV_CICLO_CLI THEN
      
      LV_SET := 'bytes_periodo = a.bytes_total, '||
                'bytes_left    = a.bytes_total, '||
                'observacion   = '''||'--RENOVADO--|'''||
                '|| a.observacion ';
      
      LV_WHERE:= 'rowid = '''||I.ROWID||'''';
      
      GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => 'GSI_PAQROAMING_ACTIVADOS A',
                                              PV_SET   => LV_SET,
                                              PV_WHERE => LV_WHERE,
                                              PV_ERROR => PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
         RAISE LE_ERROR;
      END IF;   
   END IF;
 END LOOP;

EXCEPTION
  WHEN LE_ERROR THEN
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := 'Error al Actualizar los Features Recurrentes '||LV_PROGRAMA||'; '||SQLERRM ;
END GSI_ACTUALIZA_PAQUETE_RECURR;

/***********************************************************************************
*Se clasifica el producto de cada UDR facturable en GPRS 
************************************************************************************/
PROCEDURE GSI_CLASIFICA_PRODUCTO_GPR(PV_ERROR OUT VARCHAR2) IS

 CURSOR C_REG IS
  SELECT ID_SERVICIO
    FROM GSI_PAQROAMING_ACTIVADOS A 
   WHERE A.ID_SUBPRODUCTO = 'AUT';
   
 TYPE LR_RECORD_PROD IS RECORD( S_P_NUMBER_ADDRESS     VARCHAR2(100),
                                PRODUCTO               VARCHAR2(5));   
	
 J  LR_RECORD_PROD;
 LV_FONO               VARCHAR2(20);
 LV_PROD               VARCHAR2(10);
 LV_FORMULA_FECH       VARCHAR2(30):= NULL;
 LV_FECHA_EVENTO       VARCHAR2(30):= NULL;
 LV_PROGRAMA           VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.CLASIFICA_PRODUCTO_GPR';
 --*
 LV_SET                VARCHAR2(5000):= NULL;
 LV_WHERE              VARCHAR2(5000):= NULL; 
 LV_SELECT             VARCHAR2(9000):= NULL;    
 REG_UDR               VAR_CURSOR;
 --*
 LV_ESQ_SERV           VARCHAR2(3) := NULL;
 LE_ERROR              EXCEPTION;
 
BEGIN
 
  OPEN C_PARAMETROS(6071,'FORMULA_FECHA');
 FETCH C_PARAMETROS INTO LV_FORMULA_FECH;
 CLOSE C_PARAMETROS; 
 
  OPEN C_PARAMETROS(6071,'FECHA_EVENTO');
 FETCH C_PARAMETROS INTO LV_FECHA_EVENTO;
 CLOSE C_PARAMETROS; 

  OPEN C_PARAMETROS(6071,'N_DIGITO_SERV');
 FETCH C_PARAMETROS INTO LV_ESQ_SERV;
 CLOSE C_PARAMETROS;   
       
 FOR I IN C_REG LOOP
     
     LV_SET := 'producto = '''||'AUT''';         
     
     LV_WHERE := ' s_p_number_address   = '''||I.ID_SERVICIO||''''||
                 ' and rated_flat_amount_new != 0 '||
                 ' and initial_start_time_timestamp + '||
                 ' nvl((initial_start_time_time_offset '||LV_FORMULA_FECH||' ),0) '||
                 ' >= to_date('''||LV_FECHA_EVENTO||''''||','||'''DD/MM/YYYY HH24:MI:SS'')';
                 
     GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => 'GSI_UDR_LT_GRX_ANALISIS A',
                                              PV_SET   => LV_SET,
                                              PV_WHERE => LV_WHERE,
                                              PV_ERROR => PV_ERROR);
     IF PV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
     END IF;     
 END LOOP;
 
 LV_SELECT := ' select distinct s_p_number_address, '||'''TAR'''||
              '   from gsi_udr_lt_grx_analisis'||
              '  where id_paquete is not null'||
              '    and rated_flat_amount_new != 0'||
              '    and remark_new in (select p.valor'||
              '                         from gv_parametros@RTX_TO_BSCS_LINK p'||
              '                        where p.id_tipo_parametro = 6071'||
              '                          and p.id_parametro in ('||
              '                      ''REMARK_GPRS_EVENTOS'''||','||'''REMARK_GPRS_ADICIONALES'''||'))'||
              '    and initial_start_time_timestamp + '||
              '        nvl((initial_start_time_time_offset'||LV_FORMULA_FECH||'),0) >='|| 
              '        to_date('''||LV_FECHA_EVENTO||''','||'''dd/mm/yyyy hh24:mi:ss'''||')'||
              '    and nvl(producto,'||'''TAR'''||') = '||'''TAR'''||
              ' union all'||
              ' select distinct s_p_number_address, '||'''TAR'''||
              '   from gsi_udr_lt_grx_analisis'||
              '  where id_paquete is null'||
              '    and rated_flat_amount_new != 0'||
              '    and remark_new in (select p.valor'||
              '                         from gv_parametros@RTX_TO_BSCS_LINK p'||
              '                        where p.id_tipo_parametro = 6071'||
              '                          and p.id_parametro in ('||
              '                      ''REMARK_GPRS_EVENTOSNOPAQ'''||'))'||
              '    and initial_start_time_timestamp + '||
              '        nvl((initial_start_time_time_offset'||LV_FORMULA_FECH||'),0) >='|| 
              '        to_date('''||LV_FECHA_EVENTO||''','||'''dd/mm/yyyy hh24:mi:ss'''||')'||
              '    and nvl(producto,'||'''TAR'''||') = '||'''TAR'''||
              '    and s_p_number_address not in'||
              '    (select d.s_p_number_address from gsi_roam_lineas_directores d)';
 
  OPEN REG_UDR FOR LV_SELECT;
 FETCH REG_UDR INTO J;
 
 LOOP
     EXIT WHEN REG_UDR%NOTFOUND;
     
     LV_SET   := NULL;
     LV_WHERE := NULL;
     
     LV_FONO:= OBTIENE_TELEFONO_DNC(J.S_P_NUMBER_ADDRESS,LV_ESQ_SERV);
     LV_PROD:= 0;
     
     SELECT COUNT(*)
       INTO LV_PROD
       FROM PORTA.CL_SERVICIOS_CONTRATADOS@AXIS
      WHERE ID_SERVICIO = LV_FONO
        AND ESTADO = 'A'
        AND ID_SUBPRODUCTO = 'AUT';
     
     IF LV_PROD <> 0 THEN

        LV_SET := 'producto = '''||'AUT''';         
         
        LV_WHERE := ' s_p_number_address   = '''||J.S_P_NUMBER_ADDRESS||''''||
                    --' and id_paquete is not null '||
                    ' and rated_flat_amount_new != 0'||
                    ' and remark_new in (select p.valor'||
                    '                      from gv_parametros@RTX_TO_BSCS_LINK p'||
                    '                     where p.id_tipo_parametro = 6071'||
                    '                       and p.nombre in ('''||'REMARK_GPRS_SINPAQ'||'''))'||
                    ' and initial_start_time_timestamp + '||
                    ' nvl((initial_start_time_time_offset '||LV_FORMULA_FECH||' ),0) '||
                    ' >= to_date('''||LV_FECHA_EVENTO||''''||','||'''DD/MM/YYYY HH24:MI:SS'''||')'||
                    ' and nvl(producto,'''||'TAR'||''''||')='||'''TAR''';
                     
        GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => 'GSI_UDR_LT_GRX_ANALISIS A',
                                                PV_SET   => LV_SET,
                                                PV_WHERE => LV_WHERE,
                                                PV_ERROR => PV_ERROR);
        IF PV_ERROR IS NOT NULL THEN
           RAISE LE_ERROR;
        END IF;          
     END IF;
     
     FETCH  REG_UDR INTO J;
 END LOOP;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;    
  WHEN OTHERS THEN 
    PV_ERROR := 'Error al actualizar el producto '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_CLASIFICA_PRODUCTO_GPR;

/***********************************************************************************
*Registros Duplicados en GPRS y SMS 
************************************************************************************/
PROCEDURE GSI_REGISTROS_DUPLI( PD_FECHA_CORTE   IN DATE, 
                               PV_FECHA_NEW_ESQ IN VARCHAR2,
                               PV_FORMULA_FECHA IN VARCHAR2,
                               PD_FECHA_INI     IN DATE,
                               PD_FECHA_FIN     IN DATE,
                               PV_TIPO          IN VARCHAR2,
                               PV_ERROR         OUT VARCHAR2) IS
   
 CURSOR REG_DUPLI IS 
  SELECT * 
    FROM GSI_SVA_ROA_DUPLI D
   WHERE D.ESTADO = 'I';
    
 LV_PROGRAMA       VARCHAR2(150) := 'GSI_RETARIFICACIONES_ROAMING.GSI_REGISTROS_DUPLI';
 LV_TABLA_DUP      VARCHAR2(500) := 'GSI_SVA_ROA_DUPLI';
 LV_SENTENCIA_DUP  VARCHAR2(4000):= NULL;
 LV_TABLA          VARCHAR2(500) := NULL;
 LV_TIPO           VARCHAR2(5)   := NULL; 
 --*
 LV_WHERE_D        VARCHAR2(5000):= NULL; 
 --*
 LV_SET_U          VARCHAR2(5000):= NULL;
 LV_WHERE_U        VARCHAR2(5000):= NULL;
 LE_ERROR          EXCEPTION;
 
BEGIN 
   
 GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( LV_TABLA_DUP,
                                                NULL,
                                                PV_ERROR);

 IF PV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;
 END IF;
 
 LV_TIPO:= '_'||TRIM(PV_TIPO); --'_Grx', '_Sms'
 LV_TABLA:= 'sysadm.Udr_Lt_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMM')||LV_TIPO;
 
 --Si viene el corte 02 que utiliza 2meses viene primero el mes del corte y luego el anterior
 LV_SENTENCIA_DUP := 
 'insert into '||LV_TABLA_DUP||' '||
 'select /*+ PARALLEL(B,6) */
         cust_info_customer_id,
         cust_info_contract_id,
         uds_base_part_id,
         uds_charge_part_id,
         initial_start_time_timestamp,
         o_p_number_address,
         follow_up_call_type,
         durat_conn_volume,
         count(*) veces, '||
         '''I'''||
  ' from '||LV_TABLA||' '||
  'where initial_start_time_timestamp + nvl((initial_start_time_time_offset '||PV_FORMULA_FECHA||' ), 0)'||       
  '>= to_date('''||PV_FECHA_NEW_ESQ||''', ''dd/mm/yyyy hh24:mi:ss'')'||
  'and entry_date_timestamp >= to_date('''||TO_CHAR(PD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
  'and entry_date_timestamp <= to_date('''||TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
  'group by cust_info_customer_id,
            cust_info_contract_id,
            uds_base_part_id,
            uds_charge_part_id,
            initial_start_time_timestamp,
            o_p_number_address,
            follow_up_call_type,
            durat_conn_volume
  having count(*) > 1 ';
  
  --Si es el diferente mes necesito solo una udr
  IF TO_CHAR(PD_FECHA_INI,'MMYYYY') <> TO_CHAR(PD_FECHA_FIN,'MMYYYY') THEN
    
    EXECUTE IMMEDIATE LV_SENTENCIA_DUP;
   
    --Elimina los casos duplicados de la primera tabla
    FOR I IN REG_DUPLI LOOP
       /*******************************************
         *Elimnacion de Registros de registros Duplicados en UDR Original
       ********************************************/
       LV_WHERE_D := '     cust_info_customer_id        = '||I.CUST_INFO_CUSTOMER_ID||
                     ' and cust_info_contract_id        = '||I.CUST_INFO_CONTRACT_ID||
                     ' and uds_base_part_id             = '||I.UDS_BASE_PART_ID||
                     ' and uds_charge_part_id           = '||I.UDS_CHARGE_PART_ID||
                     ' and initial_start_time_timestamp = to_date('''||TO_CHAR(I.INITIAL_START_TIME_TIMESTAMP,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
                     ' and o_p_number_address           = '''||I.O_P_NUMBER_ADDRESS||''''||
                     ' and follow_up_call_type          = '||I.FOLLOW_UP_CALL_TYPE||
                     ' and rownum < '||I.VECES ;

       GSI_OBJ_RETARIFICACIONES.GSI_DELETE_DUP( PV_TABLA => LV_TABLA,
                                                PV_WHERE => LV_WHERE_D,
                                                PV_ERROR => PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN 
          RAISE LE_ERROR;
       END IF;
       /*******************************************
         *Actualizo tabla de Duplicados con estado Nuevo
       ********************************************/       
       LV_SET_U   := 'rd.estado = '||'''F''';
       
       LV_WHERE_U := '     rd.cust_info_customer_id        = '||I.CUST_INFO_CUSTOMER_ID||
                     ' and rd.cust_info_contract_id        = '||I.CUST_INFO_CONTRACT_ID||  
                     ' and rd.uds_base_part_id             = '||I.UDS_BASE_PART_ID||    
                     ' and rd.uds_charge_part_id           = '||I.UDS_CHARGE_PART_ID||    
                     ' and rd.initial_start_time_timestamp = '||'to_date('''||TO_CHAR(I.INITIAL_START_TIME_TIMESTAMP,'DD/MM/YYYY HH24:MI:SS')||''''||','||'''dd/mm/yyyy hh24:mi:ss'''||')'||
                     ' and rd.o_p_number_address           = '''||I.O_P_NUMBER_ADDRESS||''''||
                     ' and rd.follow_up_call_type          = '||I.FOLLOW_UP_CALL_TYPE;

       GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => 'GSI_SVA_ROA_DUPLI rd',
                                               PV_SET   => LV_SET_U,
                                               PV_WHERE => LV_WHERE_U,
                                               PV_ERROR => PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;        
       /*******************************************
         *fin de Actualizacion tabla de Duplicados con estado Nuevo
       ********************************************/                        
    END LOOP;  
            
    LV_TABLA := REPLACE(LV_TABLA,(TO_CHAR(PD_FECHA_CORTE,'YYYYMM')),TO_CHAR(ADD_MONTHS(PD_FECHA_CORTE,-1),'YYYYMM'));
     
    --El mes anterior  
    LV_SENTENCIA_DUP := 
     'insert into '||LV_TABLA_DUP||' '||
     'select /*+ PARALLEL(B,6) */
             cust_info_customer_id,
             cust_info_contract_id,
             uds_base_part_id,
             uds_charge_part_id,
             initial_start_time_timestamp,
             o_p_number_address,
             follow_up_call_type,
             durat_conn_volume,
             count(*) veces, '||
             '''I'''||
      ' from '||LV_TABLA||' '||
      'where initial_start_time_timestamp + nvl((initial_start_time_time_offset '||PV_FORMULA_FECHA||' ), 0)'||       
      '>= to_date('''||PV_FECHA_NEW_ESQ||''', ''dd/mm/yyyy hh24:mi:ss'')'||
      'and entry_date_timestamp >= to_date('''||TO_CHAR(PD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
      'and entry_date_timestamp <= to_date('''||TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
      'group by cust_info_customer_id,
                cust_info_contract_id,
                uds_base_part_id,
                uds_charge_part_id,
                initial_start_time_timestamp,
                o_p_number_address,
                follow_up_call_type,
                durat_conn_volume
      having count(*) > 1 ';   
    
 END IF;

    EXECUTE IMMEDIATE LV_SENTENCIA_DUP;   
    /*************************
     **Elimina los casos duplicados en el caso que solo se utilize una udr y 
     **para el udr final en el caso de utilizar 2
    **************************/
    FOR I IN REG_DUPLI LOOP
       
       /*******************************************
         *Elimnacion de Registros de registros Duplicados en UDR Original
       ********************************************/
       LV_WHERE_D := '     cust_info_customer_id        = '||I.CUST_INFO_CUSTOMER_ID||
                     ' and cust_info_contract_id        = '||I.CUST_INFO_CONTRACT_ID||
                     ' and uds_base_part_id             = '||I.UDS_BASE_PART_ID||
                     ' and uds_charge_part_id           = '||I.UDS_CHARGE_PART_ID||
                     ' and initial_start_time_timestamp = to_date('''||TO_CHAR(I.INITIAL_START_TIME_TIMESTAMP,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
                     ' and o_p_number_address           = '''||I.O_P_NUMBER_ADDRESS||''''||
                     ' and follow_up_call_type          = '||I.FOLLOW_UP_CALL_TYPE||
                     ' and rownum < '||I.VECES ;

       GSI_OBJ_RETARIFICACIONES.GSI_DELETE_DUP( PV_TABLA => LV_TABLA,
                                                PV_WHERE => LV_WHERE_D,
                                                PV_ERROR => PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN 
          RAISE LE_ERROR;
       END IF;
    
       /*******************************************
         *Actualizo tabla de Duplicados con estado Nuevo
       ********************************************/       
       LV_SET_U   := 'rd.estado = '||'''F''';
       
       LV_WHERE_U := '     rd.cust_info_customer_id        = '||I.CUST_INFO_CUSTOMER_ID||
                     ' and rd.cust_info_contract_id        = '||I.CUST_INFO_CONTRACT_ID||  
                     ' and rd.uds_base_part_id             = '||I.UDS_BASE_PART_ID||    
                     ' and rd.uds_charge_part_id           = '||I.UDS_CHARGE_PART_ID||    
                     ' and rd.initial_start_time_timestamp = '||'to_date('''||TO_CHAR(I.INITIAL_START_TIME_TIMESTAMP,'DD/MM/YYYY HH24:MI:SS')||''''||','||'''dd/mm/yyyy hh24:mi:ss'''||')'||
                     ' and rd.o_p_number_address           = '''||I.O_P_NUMBER_ADDRESS||''''||
                     ' and rd.follow_up_call_type          = '||I.FOLLOW_UP_CALL_TYPE;

       GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => 'GSI_SVA_ROA_DUPLI rd',
                                               PV_SET   => LV_SET_U,
                                               PV_WHERE => LV_WHERE_U,
                                               PV_ERROR => PV_ERROR);
       IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;          
       END IF;        
       /*******************************************
         *fin de Actualizacion tabla de Duplicados con estado Nuevo
       ********************************************/  
    END LOOP;  
   
EXCEPTION
  WHEN LE_ERROR THEN 
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_REGISTROS_DUPLI;

/*******************************************************************************
**Elimina de la UDR de SMS los regsitros que interactuan con puertos especiales
los cuales pueden tener costo 0 o mayor a 0
********************************************************************************/
PROCEDURE GSI_DEPURA_SMS_SIN_COSTO( PV_TABLA             IN VARCHAR2,
                                    PV_FORMULA_FECHA     IN VARCHAR2,
                                    PV_FECHA_NEW_ESQ     IN VARCHAR2,
                                    PD_FECHA_INI         IN DATE,
                                    PD_FECHA_FIN         IN DATE,
                                    PV_ERROR             OUT VARCHAR2)IS
  
 CURSOR C_PUERTOS IS
  SELECT * 
    FROM GSI_PUERTOS_FREE
   WHERE ESTADO = 'A';
   
 LV_PROGRAMA        VARCHAR2(80)  := 'GSI_RETARIFICACIONES_ROAMING.GSI_DEPURA_SMS_SIN_COSTO';
 LV_CONSULTA_REG    VARCHAR2(5000):= NULL;
 REG_UDR            VAR_CURSOR;
 LR_REG_DEPURA      RECORD_UDR_2;
 LV_ESQ_SERV        VARCHAR2(5)   := NULL;
 --*
 LV_SET             VARCHAR2(5000):= NULL;
 LV_WHERE           VARCHAR2(5000):= NULL;
 --*
 LV_REMARK_SINCOSTO VARCHAR2(100):= NULL;
 LV_REMARK_CONCOSTO VARCHAR2(100):= NULL;
 --*
 LE_ERROR           EXCEPTION;
 
BEGIN
 
  OPEN C_PARAMETROS(6071,'REMARK_SMS_CONCOSTO');
 FETCH C_PARAMETROS INTO LV_REMARK_CONCOSTO;
 CLOSE C_PARAMETROS; 

  OPEN C_PARAMETROS(6071,'REMARK_SMS_SINCOSTO');
 FETCH C_PARAMETROS INTO LV_REMARK_SINCOSTO;
 CLOSE C_PARAMETROS; 
 
  OPEN C_PARAMETROS(6071,'N_DIGITO_SERV');
 FETCH C_PARAMETROS INTO LV_ESQ_SERV;
 CLOSE C_PARAMETROS;
 
 FOR P IN C_PUERTOS LOOP
   LV_CONSULTA_REG:= 'Select  b.s_p_Number_Address, b.o_p_number_address, b.s_p_port_address, b.follow_up_call_type, export_file, b.rowid '||
                     ' from '|| PV_TABLA || 
                     ' b where o_p_Number_Address = '||
                     ''''|| P.PUERTO || '''' ||
                     ' and initial_start_time_timestamp + nvl((initial_start_time_time_offset '||PV_FORMULA_FECHA||' ), 0)'||       
                     ' >= to_date('''||PV_FECHA_NEW_ESQ||''', ''dd/mm/yyyy hh24:mi:ss'')'||
                     ' and entry_date_timestamp >= to_date('''||TO_CHAR(PD_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')'||
                     ' and entry_date_timestamp <= to_date('''||TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS')||''',''dd/mm/yyyy hh24:mi:ss'')';
   
    OPEN REG_UDR FOR LV_CONSULTA_REG;
   FETCH REG_UDR INTO LR_REG_DEPURA;
   
   LOOP 
      EXIT WHEN REG_UDR%NOTFOUND;

      LV_WHERE := NULL;
      LV_SET   := NULL;     
   
       IF LR_REG_DEPURA.FOLLOW_UP_CALL_TYPE = 2 THEN
         /*******************************************
           *Actualizo el Remark y el valor del mensaje
         ********************************************/       
         IF NVL(P.COSTO_IN,0) > 0 THEN 
            LV_SET   := ' s.remark = '''|| LV_REMARK_CONCOSTO ||''','||
                        ' s.rated_flat_amount ='''|| P.COSTO_IN||'''';
                 
         ELSE
            LV_SET   := ' s.remark = '''|| LV_REMARK_SINCOSTO ||''','||
                        ' s.rated_flat_amount ='|| P.COSTO_IN;
                                   
         END IF;      
            LV_WHERE := '       s.s_p_number_address = '''|| LR_REG_DEPURA.S_P_NUMBER_ADDRESS ||''''||
                        ' and s.o_p_number_address   = '''|| LR_REG_DEPURA.O_P_NUMBER_ADDRESS ||''''||
                        ' and s.follow_up_call_type  = '|| 2 ||
                        ' and s.rowid = '''||LR_REG_DEPURA.ROWID||'''';

            GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => PV_TABLA ||' s ',
                                                     PV_SET   => LV_SET,
                                                     PV_WHERE => LV_WHERE,
                                                     PV_ERROR => PV_ERROR);
            IF PV_ERROR IS NOT NULL THEN
               RAISE LE_ERROR;
            END IF;        
       ELSE
         IF NVL(P.COSTO_OUT,0) > 0 THEN             
           LV_SET   := ' s.remark = '''|| LV_REMARK_CONCOSTO ||''','||
                       ' s.rated_flat_amount ='''|| P.COSTO_OUT||'''';
         ELSE
           LV_SET   := ' s.remark = '''|| LV_REMARK_SINCOSTO ||''','||
                       ' s.rated_flat_amount ='|| P.COSTO_OUT;                 
         END IF;        
           LV_WHERE := '     s.s_p_number_address = '''|| LR_REG_DEPURA.S_P_NUMBER_ADDRESS ||''''||
                       ' and s.o_p_number_address   = '''|| LR_REG_DEPURA.O_P_NUMBER_ADDRESS ||''''||
                       ' and s.follow_up_call_type  = '|| 1 ||
                       ' and s.rowid = '''||LR_REG_DEPURA.ROWID||'''';

           GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL(PV_TABLA => PV_TABLA ||' s ',
                                                   PV_SET   => LV_SET,
                                                   PV_WHERE => LV_WHERE,
                                                   PV_ERROR => PV_ERROR);
           IF PV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
           END IF;        
           /*******************************************
             *Actualizo el Remark y el valor del mensaje
           ********************************************/                      
        END IF;           

      FETCH  REG_UDR INTO LR_REG_DEPURA;
   END LOOP;

 END LOOP;

EXCEPTION
  WHEN LE_ERROR THEN 
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;   
END GSI_DEPURA_SMS_SIN_COSTO;

/**************************************************************
*Para controlar el traspaso de linea y el cambio de producto
***************************************************************/
PROCEDURE GSI_CONTROL_TRX(PV_ERROR OUT VARCHAR2)IS
 
 CURSOR C_ACTIVACIONES IS
  SELECT P.ID_SERVICIO, COUNT(P.FEATURE) 
    FROM GSI_PAQROAMING_ACTIVADOS P
   GROUP BY P.ID_SERVICIO
  HAVING COUNT(P.FEATURE) > 1;
  
 CURSOR C_FEATURES_TMP(CN_POSICION NUMBER) IS
  SELECT F.*
    FROM GSI_FEATURES_TMP F
   WHERE F.NUM_FEATURE = CN_POSICION;

 CURSOR C_DIAS_VIGENCIAS(PV_FEATURE VARCHAR2) IS
  SELECT DIAS_VIGENCIAS
    FROM axis_mapeo_plan_cre@axisdesa
   WHERE PLAN_AXIS = PV_FEATURE;
       
 CURSOR C_DISTINT_FEATURE(CV_TELEFONO VARCHAR2, CN_CONTRATO NUMBER, CV_SUBPRODUCTO VARCHAR2)IS 
  SELECT DISTINCT(P.FEATURE) FEATURE
    FROM GSI_PAQROAMING_ACTIVADOS P
   WHERE P.ID_SERVICIO = CV_TELEFONO
     AND P.ID_CONTRATO = CN_CONTRATO
     AND P.ID_SUBPRODUCTO = CV_SUBPRODUCTO;
 
 LV_PROGRAMA        VARCHAR2(100):= 'GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_TRX';
 LN_NUM_FEATURES    NUMBER:= 0;
 LN_NUM_FEAT_ACTUAL NUMBER:= 0;
 LC_FEATURES_TMP    C_FEATURES_TMP%ROWTYPE;
 LC_FEATURES_TMP_2  C_FEATURES_TMP%ROWTYPE;
 LN_MAX_REG         NUMBER:= 0;
 LN_NUM_FEAT_TMP    NUMBER:= 0;
 LE_ERROR           EXCEPTION;
 LN_DIAS_VIGENCIA   NUMBER;
 LB_MODIFICA_FECHA  BOOLEAN:= FALSE;
 
BEGIN

 FOR I IN C_ACTIVACIONES LOOP
 
   GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE( 'GSI_FEATURES_TMP',
                                                   NULL,
                                                   PV_ERROR);
     
   IF PV_ERROR IS NOT NULL THEN 
      RAISE LE_ERROR;
   END IF;

   --Inserto features en la temporal
   GSI_OBJ_RETARIFICACIONES.INSERT_GSI_FEATURES_TMP( I.ID_SERVICIO,
                                                     LN_NUM_FEATURES,
                                                     PV_ERROR);

   IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

   LN_NUM_FEAT_ACTUAL :=0;
   LN_MAX_REG         :=0;
   
   LOOP
     LN_NUM_FEAT_ACTUAL:= LN_NUM_FEAT_ACTUAL + 1;
     
     EXIT WHEN LN_MAX_REG = LN_NUM_FEATURES;
     
     SELECT MAX(NUM_FEATURE)
       INTO LN_MAX_REG
       FROM GSI_FEATURES_TMP;
       
      OPEN C_FEATURES_TMP(LN_NUM_FEAT_ACTUAL);
     FETCH C_FEATURES_TMP INTO LC_FEATURES_TMP;
     CLOSE C_FEATURES_TMP;

     LN_NUM_FEAT_TMP:= LN_NUM_FEAT_ACTUAL;
                        
     LOOP
       LN_NUM_FEAT_TMP := LN_NUM_FEAT_TMP + 1;

       EXIT WHEN LN_NUM_FEAT_TMP > LN_NUM_FEATURES;
         
          OPEN C_FEATURES_TMP(LN_NUM_FEAT_TMP);
         FETCH C_FEATURES_TMP INTO LC_FEATURES_TMP_2;
         CLOSE C_FEATURES_TMP;

         IF LC_FEATURES_TMP.ID_CONTRATO <> LC_FEATURES_TMP_2.ID_CONTRATO THEN
            LB_MODIFICA_FECHA:= TRUE;
            
            DELETE FROM GSI_PAQROAMING_ACTIVADOS
             WHERE ID_SERVICIO = LC_FEATURES_TMP_2.ID_SERVICIO
               AND ID_CONTRATO = LC_FEATURES_TMP_2.ID_CONTRATO
               AND ID_SUBPRODUCTO = LC_FEATURES_TMP_2.ID_SUBPRODUCTO
               AND FEATURE     = LC_FEATURES_TMP_2.FEATURE
               AND ESTADO      = LC_FEATURES_TMP_2.ESTADO
               AND CO_ID       = LC_FEATURES_TMP_2.CO_ID
               AND CUSTOMER_ID = LC_FEATURES_TMP_2.CUSTOMER_ID;
            
         ELSE 
                     
           IF LC_FEATURES_TMP.ID_SUBPRODUCTO <> LC_FEATURES_TMP_2.ID_SUBPRODUCTO THEN
              LB_MODIFICA_FECHA:= TRUE;
                DELETE FROM GSI_PAQROAMING_ACTIVADOS
                 WHERE ID_SERVICIO        = LC_FEATURES_TMP_2.ID_SERVICIO
                   AND ID_CONTRATO        = LC_FEATURES_TMP_2.ID_CONTRATO
                   AND ID_SUBPRODUCTO     = LC_FEATURES_TMP_2.ID_SUBPRODUCTO
                   AND FEATURE            = LC_FEATURES_TMP_2.FEATURE
                   AND ESTADO             = LC_FEATURES_TMP_2.ESTADO
                   AND CO_ID              = LC_FEATURES_TMP_2.CO_ID
                   AND CUSTOMER_ID        = LC_FEATURES_TMP_2.CUSTOMER_ID;
                          
           END IF;
         END IF;
    END LOOP; 
    IF LB_MODIFICA_FECHA THEN
       FOR DF IN C_DISTINT_FEATURE(LC_FEATURES_TMP.ID_SERVICIO,LC_FEATURES_TMP.ID_CONTRATO,LC_FEATURES_TMP.ID_SUBPRODUCTO) LOOP 
            OPEN C_DIAS_VIGENCIAS(DF.FEATURE);
           FETCH C_DIAS_VIGENCIAS INTO LN_DIAS_VIGENCIA;
           CLOSE C_DIAS_VIGENCIAS;
           
           UPDATE GSI_PAQROAMING_ACTIVADOS
              SET FECHA_HASTA = FECHA_DESDE + LN_DIAS_VIGENCIA
            WHERE ID_SERVICIO = LC_FEATURES_TMP.ID_SERVICIO
              AND FEATURE = DF.FEATURE;
       END LOOP; 
       LB_MODIFICA_FECHA:= FALSE;
    END IF;     
   END LOOP;

 END LOOP;
 
EXCEPTION
  WHEN LE_ERROR THEN 
    ROLLBACK;
    PV_ERROR:= 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;   
END GSI_CONTROL_TRX;

/******************************************************************************
*Clasifica por eventos o adicionales segun se de el caso del numero que se este
 tasando
*******************************************************************************/
PROCEDURE GSI_CLASIFICA_EVEN_ADIC(PV_IDSERVICIO        IN VARCHAR2,
                                  PV_REMARK_EVENTOS    IN VARCHAR2,
                                  PV_REMARK_ADICIONAL  IN VARCHAR2,
                                  PV_COSTO_EVEN_AM     IN VARCHAR2,
                                  PV_COSTO_EVEN_RM     IN VARCHAR2,
                                  PV_ROWID             IN VARCHAR2,
                                  PV_ZONA_OPERADORA    IN VARCHAR2, 
                                  PD_FECHA_CONSUMO     IN DATE,
                                  PN_DATA_VOLUMEN      IN NUMBER,
                                  PN_KBREST            IN OUT NUMBER,
                                  PB_ADICIONALES       OUT BOOLEAN,
                                  PV_ERROR             OUT VARCHAR2) IS
 
 CURSOR C_DATOS(CV_IDSERVICIO VARCHAR2)IS
  SELECT UG.*,UG.ROWID 
    FROM GSI_UDR_LT_GRX_ANALISIS UG 
   WHERE UG.S_P_NUMBER_ADDRESS = CV_IDSERVICIO
     AND SUBSTR(EXPORT_FILE, 3, 5) IN
                 (SELECT TAP_SOURCE_CODE
                    FROM GSI_MAESTRA_OPERADORAS)
     AND UG.REMARK_NEW IS NULL; 

 CURSOR C_ZONA_PAQUETE(CV_TELEFONO VARCHAR2, CV_ZONA VARCHAR2)IS 
  SELECT P.*
    FROM GSI_PAQROAMING_ACTIVADOS P
   WHERE P.ID_SERVICIO = CV_TELEFONO
     AND P.ZONA = CV_ZONA;
     
 CURSOR C_DATOS_FEATURE(CV_FEATURE VARCHAR2)IS 
  SELECT M.* 
    FROM GSI_MAESTRA_FEATURE_ROAMGRX M
   WHERE FEATURE = CV_FEATURE;

 CURSOR C_PAQUETE_ACTIVO(CV_TELEFONO VARCHAR2) IS
  SELECT A.* 
    FROM GSI_PAQROAMING_ACTIVADOS A 
   WHERE A.ID_SERVICIO = CV_TELEFONO
   ORDER BY ID_SERVICIO, FECHA_DESDE DESC;
          
 LV_PROGRAMA        VARCHAR2(500):= 'GSI_RETARIFICACIONES_ROAMING.GSI_CLASIFICA_EVEN_ADIC';
 LV_ZONA_OPERADORA  VARCHAR2(100):= NULL;
 LB_FOUND_ZONA      BOOLEAN:=FALSE;
 LC_ZONA_PAQUETE    C_ZONA_PAQUETE%ROWTYPE;
 LC_DATOS_FEATURE   C_DATOS_FEATURE%ROWTYPE;
 LB_FEATURE_FOUND   BOOLEAN:=FALSE;
 LE_ERROR           EXCEPTION;
 LV_COSTOADIC       VARCHAR2(20):=NULL;
 LN_COSTOADIC       NUMBER;
 LC_PAQUETE_ACTIVO  C_PAQUETE_ACTIVO%ROWTYPE;
 LV_FECHA_DESDE_ACT VARCHAR2(50);
 LV_FECHA_HASTA_ACT VARCHAR2(50);
 LN_DATA_VOLUMEN_NEW                NUMBER;
   
BEGIN
  /* Buscaremos todos los registros del servicio que tengan el remark null y
    en caso de que tenga 2 paquetes se preguntara la zona de la operadora para hacer el cobro
    en caso de tener una sola zona y esta navegando por otra se hara el cobro de 
    eventos sin paquete a excepcion de que el paquete sea Rmundo y Amerimundo ya que 
    los 2 abarcan navegacion de america y rmundo.
  */
 IF PV_REMARK_ADICIONAL IS NULL THEN  
    FOR D IN C_DATOS(PV_IDSERVICIO) LOOP
      
      SELECT A.CONTINENTE
        INTO LV_ZONA_OPERADORA
        FROM GSI_MAESTRA_OPERADORAS A
       WHERE A.TAP_SOURCE_CODE = SUBSTR(D.EXPORT_FILE, 3, 5);
          
       OPEN C_ZONA_PAQUETE(PV_IDSERVICIO, LV_ZONA_OPERADORA);
      FETCH C_ZONA_PAQUETE INTO LC_ZONA_PAQUETE;
      LB_FOUND_ZONA := C_ZONA_PAQUETE%FOUND;
      CLOSE C_ZONA_PAQUETE;
      
      IF LB_FOUND_ZONA THEN
          OPEN C_DATOS_FEATURE(LC_ZONA_PAQUETE.FEATURE);
         FETCH C_DATOS_FEATURE INTO LC_DATOS_FEATURE; 
         LB_FEATURE_FOUND := C_DATOS_FEATURE%FOUND;
         CLOSE C_DATOS_FEATURE;
         
         IF NOT LB_FEATURE_FOUND THEN 
            PV_ERROR:= 'Feature Roaming no Existe en tabla Maestra.';
            RAISE LE_ERROR;
         END IF;
         
         IF LC_DATOS_FEATURE.COSTO_ROA_GPRS IS NULL THEN 
            LV_COSTOADIC := LC_DATOS_FEATURE.COSTO_ADIC_ESP;
         ELSE 
            LV_COSTOADIC := LC_DATOS_FEATURE.COSTO_ROA_GPRS;
         END IF;             
         
         IF LV_COSTOADIC IS NULL THEN
            IF LC_ZONA_PAQUETE.ZONA = 'AMERICA' THEN
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);
            ELSE 
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);  
            END IF;
         ELSE
            LN_COSTOADIC := TO_NUMBER(LV_COSTOADIC);
         END IF;
                            
         UPDATE GSI_UDR_LT_GRX_ANALISIS 
            SET DATA_VOLUME_NEW       = DATA_VOLUME,
                RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
                ID_PAQUETE            = LC_DATOS_FEATURE.FEATURE,
                DES_PAQUETE           = LC_DATOS_FEATURE.DESCRIPCION,
                ZONA                  = LC_DATOS_FEATURE.ZONA,
                SALDO_PAQUETE         = 0,
                REMARK_NEW            = PV_REMARK_EVENTOS
          WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
            AND EXPORT_FILE        = D.EXPORT_FILE
            AND ROWID = D.ROWID;      
      ELSE 
          OPEN C_PAQUETE_ACTIVO(PV_IDSERVICIO);
         FETCH C_PAQUETE_ACTIVO INTO LC_PAQUETE_ACTIVO;
         CLOSE C_PAQUETE_ACTIVO;
         
         IF LV_ZONA_OPERADORA = 'AMERICA' AND LC_PAQUETE_ACTIVO.ZONA = 'RMUNDO' THEN         
 
            LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);            
                                            
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = DATA_VOLUME,
                   RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_PAQUETE_ACTIVO.FEATURE,
                   DES_PAQUETE           = LC_PAQUETE_ACTIVO.DESCRIPCION,
                   ZONA                  = LV_ZONA_OPERADORA,--LC_DATOS_FEATURE.ZONA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND EXPORT_FILE        = D.EXPORT_FILE
               AND ROWID = D.ROWID;      
                        
         ELSIF LV_ZONA_OPERADORA = 'RMUNDO' AND LC_PAQUETE_ACTIVO.ZONA = 'AMERICA' THEN        
            
            LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);
            
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = DATA_VOLUME,
                   RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_PAQUETE_ACTIVO.FEATURE,
                   DES_PAQUETE           = LC_PAQUETE_ACTIVO.DESCRIPCION,                   
                   ZONA                  = LV_ZONA_OPERADORA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS--PV_REMARK_EVEN_SINPQ
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND EXPORT_FILE        = D.EXPORT_FILE
               AND ROWID = D.ROWID;

         ELSIF LV_ZONA_OPERADORA IN ('RMUNDO','AMERICA') AND LC_PAQUETE_ACTIVO.ZONA = 'AMERIMUNDO' THEN
            
             OPEN C_DATOS_FEATURE(LC_PAQUETE_ACTIVO.FEATURE);
            FETCH C_DATOS_FEATURE INTO LC_DATOS_FEATURE; 
            LB_FEATURE_FOUND := C_DATOS_FEATURE%FOUND;
            CLOSE C_DATOS_FEATURE;
                      
            IF LV_ZONA_OPERADORA = 'AMERICA' THEN
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);
            ELSE
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);
            END IF;   
                          
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = DATA_VOLUME,
                   RATED_FLAT_AMOUNT_NEW = ROUND(DATA_VOLUME * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_DATOS_FEATURE.FEATURE,
                   DES_PAQUETE           = LC_DATOS_FEATURE.DESCRIPCION,
                   ZONA                  = LV_ZONA_OPERADORA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND EXPORT_FILE        = D.EXPORT_FILE
               AND ROWID = D.ROWID;      
         END IF;
      END IF;
    END LOOP;
 ELSE
   --Para Adicionales
   PB_ADICIONALES := FALSE;       
       OPEN C_ZONA_PAQUETE(PV_IDSERVICIO, PV_ZONA_OPERADORA);
      FETCH C_ZONA_PAQUETE INTO LC_ZONA_PAQUETE;
      LB_FOUND_ZONA := C_ZONA_PAQUETE%FOUND;
      CLOSE C_ZONA_PAQUETE;
      
      IF LB_FOUND_ZONA THEN
         LV_FECHA_DESDE_ACT  := TO_CHAR(LC_ZONA_PAQUETE.FECHA_DESDE,'dd/mm/yyyy');
         LV_FECHA_HASTA_ACT  := TO_CHAR(LC_ZONA_PAQUETE.FECHA_HASTA,'dd/mm/yyyy');       
         
          OPEN C_DATOS_FEATURE(LC_ZONA_PAQUETE.FEATURE);
         FETCH C_DATOS_FEATURE INTO LC_DATOS_FEATURE; 
         LB_FEATURE_FOUND := C_DATOS_FEATURE%FOUND;
         CLOSE C_DATOS_FEATURE;
           
         IF NOT LB_FEATURE_FOUND THEN 
            PV_ERROR:= 'Feature Roaming no Existe en tabla Maestra.';
            RAISE LE_ERROR;
         END IF;
           
         IF LC_DATOS_FEATURE.COSTO_ROA_GPRS IS NULL THEN 
            LV_COSTOADIC := LC_DATOS_FEATURE.COSTO_ADIC_ESP;
         ELSE 
            LV_COSTOADIC := LC_DATOS_FEATURE.COSTO_ROA_GPRS;
         END IF;             
           
         IF LV_COSTOADIC IS NULL THEN
            IF LC_ZONA_PAQUETE.ZONA = 'AMERICA' THEN
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);
            ELSE 
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);  
            END IF;
         ELSE
            LN_COSTOADIC := TO_NUMBER(LV_COSTOADIC);
         END IF;   
           
         IF LV_FECHA_DESDE_ACT <= PD_FECHA_CONSUMO AND LV_FECHA_HASTA_ACT >= PD_FECHA_CONSUMO THEN
            
            LN_DATA_VOLUMEN_NEW := PN_DATA_VOLUMEN-PN_KBREST;
           UPDATE GSI_UDR_LT_GRX_ANALISIS
              SET DATA_VOLUME_NEW       = LN_DATA_VOLUMEN_NEW,
                  RATED_FLAT_AMOUNT_NEW = ROUND(LN_DATA_VOLUMEN_NEW*LN_COSTOADIC,5),
                  ID_PAQUETE            = LC_ZONA_PAQUETE.FEATURE,
                  DES_PAQUETE           = LC_ZONA_PAQUETE.DESCRIPCION,
                  SALDO_PAQUETE         = 0,
                  ZONA                  = PV_ZONA_OPERADORA,
                  REMARK_NEW            = PV_REMARK_ADICIONAL
            WHERE ROWID = PV_ROWID;        
         ELSE
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = PN_DATA_VOLUMEN,
                   RATED_FLAT_AMOUNT_NEW = ROUND(PN_DATA_VOLUMEN * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_DATOS_FEATURE.FEATURE,
                   DES_PAQUETE           = LC_DATOS_FEATURE.DESCRIPCION,
                   ZONA                  = LC_DATOS_FEATURE.ZONA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND ROWID = PV_ROWID; 
        END IF;        
      
      ELSE 
        /*No tiene activo ningun paquete de la zona que esta navegando pero se tiene que validar 
          en caso de tener paquete activo Rmundo y navegacion AMerica soporta asi que no puede ser eventos 
          sin paquete asi mismo para Amerimundo solo el costo de eventos.*/
          OPEN C_PAQUETE_ACTIVO(PV_IDSERVICIO);
         FETCH C_PAQUETE_ACTIVO INTO LC_PAQUETE_ACTIVO;
         CLOSE C_PAQUETE_ACTIVO;
         
         IF PV_ZONA_OPERADORA = 'AMERICA' AND LC_PAQUETE_ACTIVO.ZONA = 'RMUNDO' THEN         
            
            LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);            
                                 
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = PN_DATA_VOLUMEN,
                   RATED_FLAT_AMOUNT_NEW = ROUND(PN_DATA_VOLUMEN * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_PAQUETE_ACTIVO.FEATURE,
                   DES_PAQUETE           = LC_PAQUETE_ACTIVO.DESCRIPCION,
                   ZONA                  = PV_ZONA_OPERADORA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND ROWID = PV_ROWID;               
                        
         ELSIF PV_ZONA_OPERADORA = 'RMUNDO' AND LC_PAQUETE_ACTIVO.ZONA = 'AMERICA' THEN        
            
            LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);
            
            UPDATE GSI_UDR_LT_GRX_ANALISIS 
               SET DATA_VOLUME_NEW       = PN_DATA_VOLUMEN,
                   RATED_FLAT_AMOUNT_NEW = ROUND(PN_DATA_VOLUMEN * LN_COSTOADIC, 5),
                   ID_PAQUETE            = LC_PAQUETE_ACTIVO.FEATURE,
                   DES_PAQUETE           = LC_PAQUETE_ACTIVO.DESCRIPCION,                   
                   ZONA                  = PV_ZONA_OPERADORA,
                   SALDO_PAQUETE         = 0,
                   REMARK_NEW            = PV_REMARK_EVENTOS
             WHERE S_P_NUMBER_ADDRESS = PV_IDSERVICIO
               AND ROWID = PV_ROWID;

         ELSIF PV_ZONA_OPERADORA IN ('RMUNDO','AMERICA') AND LC_PAQUETE_ACTIVO.ZONA = 'AMERIMUNDO' THEN
            
             OPEN C_DATOS_FEATURE(LC_PAQUETE_ACTIVO.FEATURE);
            FETCH C_DATOS_FEATURE INTO LC_DATOS_FEATURE; 
            CLOSE C_DATOS_FEATURE;
                      
            IF PV_ZONA_OPERADORA = 'AMERICA' THEN
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_AM);
            ELSE
               LN_COSTOADIC := TO_NUMBER(PV_COSTO_EVEN_RM);
            END IF;   

           UPDATE GSI_UDR_LT_GRX_ANALISIS
              SET DATA_VOLUME_NEW       = PN_DATA_VOLUMEN,
                  RATED_FLAT_AMOUNT_NEW = ROUND(PN_DATA_VOLUMEN*LN_COSTOADIC,5),
                  ID_PAQUETE            = LC_DATOS_FEATURE.FEATURE,
                  DES_PAQUETE           = LC_DATOS_FEATURE.DESCRIPCION,
                  SALDO_PAQUETE         = 0,
                  ZONA                  = PV_ZONA_OPERADORA,
                  REMARK_NEW            = PV_REMARK_ADICIONAL
            WHERE ROWID = PV_ROWID;               
                    
         END IF;
      END IF;
 END IF;     

 EXCEPTION
  WHEN LE_ERROR THEN
    ROLLBACK;
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR; 
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := 'Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;   
END GSI_CLASIFICA_EVEN_ADIC;

/******************************************************************************
*Al generar el error puede tener mas de 250 caracteres y en el shell puede 
 ocasionar problemas por lo que se borrar la palabra Error en el Programa:
*******************************************************************************/
PROCEDURE GSI_ARMA_MENSAJE(PV_MENSAJE IN OUT VARCHAR2,
                           PV_ERROR   OUT VARCHAR2) IS
                           
    LV_CAD1      VARCHAR2(4000);
    LV_CAD2      VARCHAR2(4000);
    LV_MSJ       VARCHAR2(4000);
    LN_POS_PUNTO NUMBER;
    LN_POS_RAYA  NUMBER;
    LN_TAM       NUMBER;
    LN_ENCUENTRA NUMBER;
  
BEGIN
    LV_CAD1      := PV_MENSAJE;
    LN_POS_PUNTO := LENGTH(SUBSTR(LV_CAD1,(INSTR(LV_CAD1,'Error en programa:')),(INSTR(LV_CAD1,'a: ')+1)));
  
    WHILE (LN_POS_PUNTO <> 0) LOOP
    
      LN_TAM  := LENGTH(LV_CAD1);
      LN_TAM  := LN_TAM - LN_POS_PUNTO;
      LV_CAD1 := SUBSTR(LV_CAD1, -LN_TAM);
      LV_CAD1 := TRIM(LV_CAD1);
    
      LN_POS_RAYA := INSTR(LV_CAD1, ';');
      LN_POS_RAYA := LN_POS_RAYA - 1;
      LV_CAD2     := SUBSTR(LV_CAD1, 1, LN_POS_RAYA);
    
      IF LV_MSJ IS NOT NULL THEN
        LV_MSJ := LV_MSJ || '. ' || LV_CAD2;
      ELSE
        LV_MSJ := LV_CAD2;
      END IF;
      
      LN_ENCUENTRA:= INSTR(LV_CAD1,'Error en programa:');
      
      IF LN_ENCUENTRA > 0 THEN
         LN_POS_PUNTO := LENGTH(SUBSTR(LV_CAD1,(INSTR(LV_CAD1,'Error en programa:')),(INSTR(LV_CAD1,'a: ')+1)));        
      ELSE
        LN_POS_RAYA := INSTR(LV_CAD1, ';');
        LV_CAD1     := SUBSTR(LV_CAD1,INSTR(LV_CAD1,';'));
        LV_MSJ      := LV_MSJ|| '. ' || LV_CAD1;
        LN_POS_PUNTO:= 0;
      END IF;
       
    END LOOP;
  
    IF LV_MSJ IS NOT NULL THEN
      PV_MENSAJE := TRIM(LV_MSJ); 
    ELSE
      PV_MENSAJE := TRIM(LV_CAD1);
    END IF;
  
 EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR := 'Error ' || SQLCODE || ' ' || SQLERRM;
END GSI_ARMA_MENSAJE; 
END GSI_RETARIFICACIONES_ROAMING;
/
