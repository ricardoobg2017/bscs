create or replace package CBLK_TRX_PAGOS is

  /*--================================================================================================
  -- Descripcion:           Permite Consultar la deuda del cliente
  -- Lider TIC:             SIS Julia Rodas
  -- Lider PDS:             David de la Cruz
  -- Desarrollado por:      HTS Jose Pazmino
  -- Fecha de modificacion: 28/01/2020
  --===============================================================================================*/
 TYPE DATOSREPORTE IS RECORD(
ID_CUENTA Varchar2(50),
CUSTOMER_ID Number,
CO_ID Number,
PRGCODE Varchar2(5),
SERVICE Varchar2(8));

TYPE LV_DATOS IS TABLE OF DATOSREPORTE INDEX BY BINARY_INTEGER;

  Procedure Cblp_Consulta_Deuda(Pv_cuenta            in varchar2,
                                Pv_sub_producto      In Varchar2,
                                Pn_deuda             out number,
                                Pn_total_factura     Out Number,
                                Pn_IVA               Out Number,
                                Pn_ICE               Out Number,
                                Pn_deuda_anterior    Out Number,
                                Pd_fecha_max_pago    Out Date,
                                Pv_CodigoError       out varchar2);

  Procedure Cblp_Obtiene_Datos_Cuenta(Pv_Telefono         In Varchar2,
                                      Pn_Co_Id            Out Number,
                                      Pn_Customer_Id      Out Number,
                                      Pv_Cuenta           Out Varchar2,
                                      Pb_No_Encuentra     Out Boolean,
                                      Pb_Muchos_Registros Out Boolean,
                                      Pv_Estado           In Varchar2 default 'A');
                                      
  Procedure Cblp_Obtiene_Nombre_Cliente(Pn_Customer_Id      In Number,
                                        Pv_Cliente          Out Varchar2,
                                        Pb_No_Encuentra     Out Boolean);    
                                        
   Procedure Cblp_Obtiene_Cuenta_Ident(Pv_Identificacion  In  Varchar2,
                                      Pr_Datos_Cta       Out LV_DATOS,
                                      Pb_No_Encuentra    Out Boolean);  
                                      
   Procedure Cblp_Consulta_Cliente_Ident(Pv_Identificacion In Varchar2,
                                         Pv_Nombre_Cliente Out Varchar2,
                                         Pb_No_Encuentra   Out Boolean) ; 
                                         
    Procedure Cblp_Obtiene_Ident_Cliente(Pn_Customer_Id      In Number,
                                         Pv_Identificacion   Out Varchar2,
                                         Pb_No_Encuentra     Out Boolean); 
                                         
    Procedure Cblp_Obtiene_Nombre_Client_DTH(Pv_Identificacion In Varchar2,
                                             Pv_Nombre_Cliente Out Varchar2,
                                             Pb_No_Encuentra   Out Boolean);                                                                                                                                             

end CBLK_TRX_PAGOS;
/
create or replace package body CBLK_TRX_PAGOS is

  Function Cblf_Obtiene_Valor_Parametro(Pv_Id_Parametro In Varchar2)
    Return Varchar2 Is
    Lv_Proceso_Par_Scp     Varchar2(50);
    Lv_Descripcion_Par_Scp Varchar2(500);
    Lv_Valor_Parametro     Varchar2(4000);
    Ln_Error_Scp           Number;
    Lv_Error_Scp           Varchar2(3000);

  Begin

    Scp_Dat.Sck_Api.Scp_Parametros_Procesos_Lee(Pv_Id_Parametro => Pv_Id_Parametro,
                                                Pv_Id_Proceso   => Lv_Proceso_Par_Scp,
                                                Pv_Valor        => Lv_Valor_Parametro,
                                                Pv_Descripcion  => Lv_Descripcion_Par_Scp,
                                                Pn_Error        => Ln_Error_Scp,
                                                Pv_Error        => Lv_Error_Scp);
    Return Lv_Valor_Parametro;

  Exception
    When Others Then
      Return '';
  End;

  Procedure Cblp_Consulta_Deuda(Pv_cuenta         in varchar2,
                                Pv_sub_producto   In Varchar2,                  
                                Pn_deuda          out number,
                                Pn_total_factura  Out Number,
                                Pn_IVA            Out Number,
                                Pn_ICE            Out Number,
                                Pn_deuda_anterior Out Number,
                                Pd_fecha_max_pago Out Date,
                                Pv_CodigoError    out varchar2) IS
    /*
    Creado por    : Jose Pazmino - HTS_JPA
    Fecha creacion : 28-ENE-2020
    Observacion   : Obtener deuda vencida o deuda actual dependiento el per�odo en el que me encuentre
                    realizando la consulta
    ---------------------------------------------------------------------------------------------------
    Modificado por: Jose Pazmino - HTS_JPA
    Fecha modificacion : 04-MAR-2020
    Observacin   : Se agrega validacion de consulta de creditos, solicitado por TIC Victor Soria
    
     ---------------------------------------------------------------------------------------------------
    Modificado por: Jessenia Piloso - HTS_JPI
    Fecha modificacion : 22-MAR-2020
    Observacion   : Se agrega valores del detalle de las facturas con deuda, solicitado por TIC Edith Garaicoa
    -----------------------------------------------------------------------------------------------------
    Modificado por: Jessenia Piloso - HTS_JPI
    Fecha modificacion : 16-ABR-2020
    Observacion   : Se modifico la consulta de los datos del cliente  para considerar aquellos que tienen cuentas asociadas, solicitado por TIC Edith Garaicoa
    */
  
    cursor c_deuda_pendiente_actual(cv_cuenta varchar2) is
      SELECT /*+ rule +*/
       nvl(SUM(o.ohopnamt_doc), 0) deuda, max(o.ohduedate) fecha_max_pago
        FROM customer_all c, orderhdr_all o
       WHERE c.custcode = cv_cuenta
         AND c.customer_dealer = 'C'
         AND c.customer_id = o.customer_id
         AND o.ohstatus IN ('IN', 'CM', 'CO')
       GROUP BY c.custcode;
  
    --Se agrega consulta de creditos, solicitado por TIC Victor Soria
    cursor c_creditos(cv_cuenta varchar2) is
      SELECT /*+ rule +*/
       nvl(sum(amount), 0) creditos
        FROM customer_all a, fees b
       WHERE a.custcode = cv_cuenta
         AND a.customer_id = b.customer_id
         AND b.period > 0
         AND b.sncode = 46
         AND trunc(b.entdate) >= a.lbc_date;
  
    Cursor c_obtener_Iva(cv_cuenta Varchar2, cn_taxcat Number) Is
      select nvl(sum(i.taxamt_doc), 0) iva
        from ordertax_items i
       where i.otxact in (SELECT o.ohxact
                            FROM customer_all c, orderhdr_all o
                           WHERE c.custcode = cv_cuenta
                             AND c.customer_dealer = 'C'
                             AND c.customer_id = o.customer_id
                             AND o.ohstatus IN ('IN', 'CM', 'CO')
                             and o.ohopnamt_doc > 0)
         and i.taxcat_id in (3, cn_taxcat);
  
    Cursor c_obtener_ice(cv_cuenta Varchar2, cn_taxcat Number) Is
      select nvl(sum(i.taxamt_doc), 0) ice
        from ordertax_items i
       where i.otxact In (SELECT o.ohxact
                            FROM customer_all c, orderhdr_all o
                           WHERE c.custcode = cv_cuenta
                             AND c.customer_dealer = 'C'
                             AND c.customer_id = o.customer_id
                             AND o.ohstatus IN ('IN', 'CM', 'CO')
                             and o.ohopnamt_doc > 0)
         and taxcat_id in (cn_taxcat);
  
    Cursor c_obtener_total_factura(cv_cuenta Varchar2) Is
      select nvl(sum(i.otmerch_gross_doc), 0) total_factura
        from ordertrailer i
       where otxact in (SELECT o.ohxact
                          FROM customer_all c, orderhdr_all o
                         WHERE c.custcode = cv_cuenta
                           AND c.customer_dealer = 'C'
                           AND c.customer_id = o.customer_id
                           AND o.ohstatus IN ('IN', 'CM', 'CO')
                           and o.ohopnamt_doc > 0);
  
    lc_deuda_pendiente_actual c_deuda_pendiente_actual%rowtype;
    lc_creditos               c_creditos%rowtype; --HTS_JPA
    lv_band_credito           varchar2(1) := null;
    Lv_Saldo_Min_Por          varchar2(255);
    Lv_Saldo_Min              varchar2(255);
    Lv_CodigoError            varchar2(300);
    lv_cuenta                 varchar2(50);
    le_error_bscs             exception;
    ln_deuda                  number(10, 2) := 0;
    ln_creditos               number := 0;
    ln_saldo_real             number := 0;
    Ln_Saldo_Min              number;
    ln_por_impago             number;
    Ln_Saldo_Min_Por          number;
    ln_saldo_impago           number := 0;
    ln_taxcat_Iva             Number;
    ln_taxcat_Ice             Number;
    lb_notfound               boolean;
    lb_found                  boolean := false; --HTS_JPA
  
  begin
 
   
   If Pv_sub_producto Is Null Then 
       Lv_CodigoError := 'Ingresar Sub_Producto';
       Raise le_error_bscs;
     End If;
     
     ln_taxcat_Iva := to_number(Cblf_Obtiene_Valor_Parametro('ID_IVA_'||Pv_sub_producto));
     ln_taxcat_Ice := To_number(Cblf_Obtiene_Valor_Parametro('ID_ICE_'||Pv_sub_producto));
    
    lv_cuenta := Pv_cuenta;
  
    open c_deuda_pendiente_actual(lv_cuenta);
    fetch c_deuda_pendiente_actual
      into lc_deuda_pendiente_actual;
    lb_notfound := c_deuda_pendiente_actual%notfound;
    close c_deuda_pendiente_actual;
  
    if lb_notfound then
      Pn_deuda          := 0;
      Pn_total_factura  := 0;
      Pn_IVA            := 0;
      Pn_ICE            := 0;
      Pn_deuda_anterior := 0;
    else
      ln_deuda          := lc_deuda_pendiente_actual.deuda;
      Pd_fecha_max_pago := lc_deuda_pendiente_actual.fecha_max_pago;
    
      Open c_obtener_Iva(lv_cuenta, ln_taxcat_Iva);
      Fetch c_obtener_Iva
        Into Pn_IVA;
      Close c_obtener_Iva;
    
      Open c_obtener_ice(lv_cuenta, ln_taxcat_Ice);
      Fetch c_obtener_ice
        Into Pn_ICE;
      Close c_obtener_ice;
    
      Open c_obtener_total_factura(lv_cuenta);
      Fetch c_obtener_total_factura
        Into Pn_total_factura;
      Close c_obtener_total_factura;
    
      Pn_deuda_anterior := 0;
    
    end if;
  
    Lv_Saldo_Min := Cblf_Obtiene_Valor_Parametro('SALDO_MINIMO');
  
    Ln_Saldo_Min := to_number(Lv_Saldo_Min);
  
    Lv_Saldo_Min_Por := Cblf_Obtiene_Valor_Parametro('SALDO_MIN_POR');
  
    Ln_Saldo_Min_Por := to_number(Lv_Saldo_Min_Por);
  
    ln_por_impago := ((100 - Ln_Saldo_Min_Por) / 100);
  
    ln_saldo_impago := (ln_deuda * ln_por_impago);
  
    --Inicio HTS_JPA
    -- Obtener creditos realizados durante el mes y no aplicados
    lv_band_credito := Cblf_Obtiene_Valor_Parametro('BAND_CREDITOS_PAGOS');
  
    if nvl(lv_band_credito, 'N') = 'S' then
      OPEN c_creditos(lv_cuenta);
      FETCH c_creditos
        INTO lc_creditos;
      lb_found := c_creditos%FOUND;
      CLOSE c_creditos;
    end if;
  
    If lb_found Then
      ln_creditos := lc_creditos.creditos;
    Else
      ln_creditos := 0;
    End If;
    --Fin HTS_JPA
  
    ln_saldo_real := ln_deuda + ln_creditos;
  
    --No tiene deuda pendiente y puede continuar con la transaccion
    IF (ln_saldo_real <= Ln_Saldo_Min) OR
       (ln_saldo_real <= ln_saldo_impago) THEN
      Pn_deuda := ln_saldo_real;
    else
      --Tiene deuda
      Pn_deuda := ln_saldo_real;
    END IF;
  
  Exception
    When le_error_bscs then
      Pv_CodigoError := 'Cblf_Consulta_Deuda: ' || Lv_CodigoError;
    When Others then
      Pv_CodigoError := 'Cblf_Consulta_Deuda: ' || sqlerrm;
  end;

  Procedure Cblp_Obtiene_Datos_Cuenta(Pv_Telefono         In Varchar2,
                                      Pn_Co_Id            Out Number,
                                      Pn_Customer_Id      Out Number,
                                      Pv_Cuenta           Out Varchar2,
                                      Pb_No_Encuentra     Out Boolean,
                                      Pb_Muchos_Registros Out Boolean,
                                      Pv_Estado           In Varchar2 default 'A') is
  
  Begin
  
  If Pv_Estado = 'A' Then
    
      select nvl(coa2.co_id, csc.co_id) co_id,
            nvl(cua2.customer_id, cua.customer_id) customer_id,
            nvl(cua2.custcode, cua.custcode) custcode
       Into Pn_Co_Id, Pn_Customer_Id, Pv_Cuenta
       from directory_number dir
      Inner Join contr_services_cap csc
         On dir.dn_id = csc.dn_id
      Inner Join contract_all coa
         On csc.co_id = coa.co_id
      Inner Join customer_all cua
         On coa.customer_id = cua.customer_id
       Left Join customer_all cua2
         On cua.customer_id_high = cua2.customer_id
       Left Join contract_all coa2
         On cua2.customer_id = coa2.customer_id
      where dir.dn_num = Pv_Telefono
        and csc.cs_deactiv_date is null;
        
  Elsif Pv_Estado = 'I' Then
    
   select tab.co_id, tab.customer_id, tab.custcode
    Into Pn_Co_Id, Pn_Customer_Id, Pv_Cuenta
    from (select nvl(coa2.co_id, csc.co_id) co_id,
                 nvl(cua2.customer_id, cua.customer_id) customer_id,
                 nvl(cua2.custcode, cua.custcode) custcode
            from directory_number dir
           Inner Join contr_services_cap csc
              On dir.dn_id = csc.dn_id
           Inner Join contract_all coa
              On csc.co_id = coa.co_id
           Inner Join customer_all cua
              On coa.customer_id = cua.customer_id
            Left Join customer_all cua2
              On cua.customer_id_high = cua2.customer_id
            Left Join contract_all coa2
              On cua2.customer_id = coa2.customer_id
           where dir.dn_num = Pv_Telefono
             and csc.cs_deactiv_date is Not Null
           Order By csc.cs_deactiv_date Desc) tab
   where Rownum = 1
   and not exists (select nvl(coa2.co_id, csc.co_id) co_id,
                 nvl(cua2.customer_id, cua.customer_id) customer_id,
                 nvl(cua2.custcode, cua.custcode) custcode
            from directory_number dir
           Inner Join contr_services_cap csc
              On dir.dn_id = csc.dn_id
           Inner Join contract_all coa
              On csc.co_id = coa.co_id
           Inner Join customer_all cua
              On coa.customer_id = cua.customer_id
            Left Join customer_all cua2
              On cua.customer_id_high = cua2.customer_id
            Left Join contract_all coa2
              On cua2.customer_id = coa2.customer_id
           where dir.dn_num = Pv_Telefono
             and csc.cs_deactiv_date is null);
        
    End if;
  
  Exception
    When No_Data_Found Then
      Pb_No_Encuentra := True;
    When Too_Many_Rows Then
      Pb_Muchos_Registros := True;
    When Others Then
      Pb_No_Encuentra := True;
    
  End;
  
  Procedure Cblp_Obtiene_Nombre_Cliente(Pn_Customer_Id      In Number,
                                        Pv_Cliente          Out Varchar2,
                                        Pb_No_Encuentra     Out Boolean) Is
  
  Begin
    select cca.cclname || ' ' || cca.ccfname
      Into Pv_Cliente
      from ccontact_all cca
     where cca.customer_id = Pn_Customer_Id
       and cca.ccseq = (select max(t.ccseq) max_cceq
                          from ccontact_all t
                         where t.customer_id = Pn_Customer_Id);
  Exception
    When No_Data_Found Then
      Pb_No_Encuentra := True;
    When Others Then
      Pb_No_Encuentra := True;
  End;
  
  Procedure Cblp_Obtiene_Cuenta_Ident(Pv_Identificacion  In  Varchar2,
                                      Pr_Datos_Cta       Out LV_DATOS,
                                      Pb_No_Encuentra    Out Boolean) Is
   Lv_Service varchar2(12);      
   Begin
     Pr_Datos_Cta.DELETE;
     Pb_No_Encuentra := false; 
    For I In (Select ROWNUM IDX, Cust.Custcode, Contr.Customer_Id, Contr.Co_Id, cust.prgcode
      From Customer_All Cust, Contract_All Contr
     Where Cust.Cssocialsecno = Pv_Identificacion
       And Cust.Customer_Id_High Is Null
       And Cust.Customer_Id = Contr.Customer_Id
       And cust.prgcode <> 7)
     Loop
           
          Select substr(Dir.Dn_Num,-8) Into Lv_Service           
            From Directory_Number Dir
           Inner Join Contr_Services_Cap Csc
              On Dir.Dn_Id = Csc.Dn_Id
           Inner Join Contract_All Coa
              On Csc.Co_Id = Coa.Co_Id
           Inner Join Customer_All Cua
              On Coa.Customer_Id = Cua.Customer_Id
            Left Join Customer_All Cua2
              On Cua.Customer_Id_High = Cua2.Customer_Id
            Left Join Contract_All Coa2
              On Cua2.Customer_Id = Coa2.Customer_Id
           Where (Cua2.Custcode = i.custcode Or
                 Cua.Custcode = i.custcode)
             And Csc.Cs_Deactiv_Date Is Null
             And Dir.Dn_Status = 'a'
             And rownum =1;
     
           Pr_Datos_Cta(i.idx).Id_Cuenta := i.custcode;
           Pr_Datos_Cta(i.idx).Customer_Id := i.customer_id;
           Pr_Datos_Cta(i.idx).Co_Id := i.co_id;
           Pr_Datos_Cta(i.idx).Prgcode := i.prgcode;
           Pr_Datos_Cta(i.idx).Service:= Lv_Service;
           
           Lv_Service:='';
     End Loop; 

      If Pr_Datos_Cta.count <= 0 Then
         Pb_No_Encuentra := True;
      End If;
    
    Exception
    When No_Data_Found Then
      Pb_No_Encuentra := True;
    When Others Then
      Pb_No_Encuentra := True;                                 
    End;
    
 Procedure Cblp_Consulta_Cliente_Ident(Pv_Identificacion In Varchar2,
                                       Pv_Nombre_Cliente Out Varchar2,
                                       Pb_No_Encuentra   Out Boolean) Is
 Begin
   Select (select cca.cclname || ' ' || cca.ccfname
             from ccontact_all cca
            where cca.customer_id = Contr.Customer_Id
              and cca.ccseq =
                  (select max(t.ccseq) max_cceq
                     from ccontact_all t
                    where t.customer_id = Contr.Customer_Id)) customer_name
     Into Pv_Nombre_Cliente
     From Customer_All Cust, Contract_All Contr
    Where Cust.Cssocialsecno = Pv_Identificacion
      And Cust.Customer_Id_High Is Null
      And Cust.Customer_Id = Contr.Customer_Id
      And cust.prgcode <> 7
      And rownum = 1;
 
 Exception
   When No_Data_Found Then
     Pb_No_Encuentra := True;
   When Others Then
     Pb_No_Encuentra := True;
 End;
 
 Procedure Cblp_Obtiene_Ident_Cliente(Pn_Customer_Id      In Number,
                                      Pv_Identificacion   Out Varchar2,
                                      Pb_No_Encuentra     Out Boolean) Is
  /*Modificado por: Jessenia Piloso - HTS_JPI
    Fecha modificacion : 26-SEP-2021
    Observacion   : Se obtiene identificaci�n por medio del customer id para consulta deuda de servicio inactivo
    */
  Begin
    
    select c.cssocialsecno
    Into  Pv_Identificacion
    From customer_all c 
    Where c.customer_id = Pn_Customer_Id;
    
  Exception
    When No_Data_Found Then
      Pb_No_Encuentra := True;
    When Others Then
      Pb_No_Encuentra := True;
  End;
  
  Procedure Cblp_Obtiene_Nombre_Client_DTH(Pv_Identificacion In Varchar2,
                                           Pv_Nombre_Cliente Out Varchar2,
                                           Pb_No_Encuentra   Out Boolean) Is
 /*Modificado por: Jessenia Piloso - HTS_JPI
    Fecha modificacion : 26-SEP-2021
    Observacion   : Se obtiene nombre del cliente por medio de la identificaci�n para consultar si existe el cliente en 
                    Aplica Pago DTH convivencia.
    */                                      
 Begin
   Select (select cca.cclname || ' ' || cca.ccfname
             from ccontact_all cca
            where cca.customer_id = Contr.Customer_Id
              and cca.ccseq =
                  (select max(t.ccseq) max_cceq
                     from ccontact_all t
                    where t.customer_id = Contr.Customer_Id)) customer_name
     Into Pv_Nombre_Cliente
     From Customer_All Cust, Contract_All Contr
    Where Cust.Cssocialsecno = Pv_Identificacion
      And Cust.Customer_Id_High Is Null
      And Cust.Customer_Id = Contr.Customer_Id
      And cust.prgcode = 7
      And rownum = 1;
 
 Exception
   When No_Data_Found Then
     Pb_No_Encuentra := True;
   When Others Then
     Pb_No_Encuentra := True;
 End;

end CBLK_TRX_PAGOS;
/
