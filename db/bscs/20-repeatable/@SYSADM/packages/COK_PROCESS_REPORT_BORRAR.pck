create or replace package COK_PROCESS_REPORT_BORRAR is

  GN_COMMIT NUMBER:=5000;

  TYPE C_CURSOR IS REF CURSOR;
  TYPE ARR_NUM IS TABLE OF NUMBER;

  --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_id_bitacora_scp number:=0;
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_PROCESS_REPORT';
  lv_referencia_scp varchar2(100):='COK_PROCESS_REPORT.MAIN';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
  ---------------------------------------------------------------

  FUNCTION COF_EXTRAE_CARGOS(pv_customer_id in number, pvFechIni in varchar2, pvFechFin in varchar2) RETURN NUMBER;
  PROCEDURE CO_EDAD_REAL(pv_error out varchar2);
  PROCEDURE CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                   pdFechaCorteMaximo        date,
                                   PV_ERROR                  IN OUT VARCHAR2);
  PROCEDURE CUADRA;
  PROCEDURE SETEA_PLAN_IDEAL(pd_lvMensErr out varchar2);
  PROCEDURE INSERTA_OPE_CUADRE(pdNombre_tabla varchar2);
  PROCEDURE ACTUALIZA_OPE_CUADRE(pdFechCierrePeriodo date);
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  Procedure Llena_DatosCliente(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2);

  PROCEDURE UPD_FPAGO_PROD_CCOSTO(pdFechCierrePeriodo date,
                                  pdNombre_tabla      varchar2,
                                  pd_lvMensErr        out varchar2);

  PROCEDURE CONV_CBILL_TIPOFPAGO(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  PROCEDURE Upd_Disponible_Totpago(pdFechCierrePeriodo  date,
                                   pdNombre_tabla       varchar2,
                                   pdTabla_Cashreceipts varchar2,
                                   pdTabla_OrderHdr     varchar2,
                                   pd_lvMensErr         out varchar2);

  PROCEDURE Llena_NroFactura(pdFechCierrePeriodo date,
                             pdNombre_tabla      varchar2);
  PROCEDURE Credito_a_Disponible(pdFechCierrePeriodo  date,
                                 pdNombre_tabla       varchar2,
                                 --pdTabla_Cashreceipts varchar2,
                                 --pdTabla_OrderHdr     varchar2,
                                 --pdTabla_Customer     varchar2,
                                 pd_lvMensErr         out varchar2);

  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);

  PROCEDURE CALCULA_MORA; --(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  --[2702] ECA
  --Funcion para verificar si la cuenta realizo un cambio de ciclo
  --Devuelve True si se realizo un cambio de ciclo
  --Devuelve False si no se realizo un cambio de ciclo
  FUNCTION VERIFICA_CAMBIO_CICLO (Cn_CustomerId NUMBER) RETURN NUMBER;

  PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2);
  PROCEDURE ACTUALIZA_CMPER(pdFechCierrePeriodo in date,
                            pdNombre_tabla      in varchar2,
                            pNombre_orderhdr    in varchar2,
                            pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CONSMPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CREDTPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_PAGOSPER(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pdTabla_Cashreceipts in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_DESCUENTO(pdFechCierrePeriodo in date,
                                pdNombre_tabla      in varchar2,
                                pdMensErr           out varchar2);
  PROCEDURE LLENA_BALANCES(pdFechCierrePeriodo date,
                           pdNombre_tabla      varchar2,
                           pd_Tabla_OrderHdr   varchar2,
                           pd_lvMensErr        out varchar2);
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);

  PROCEDURE Copia_Fact_Actual(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO2(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2);
  PROCEDURE LLENA_CARGORECUPERADO2(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);
  PROCEDURE Llena_total_deuda_cierre(pdFechCierrePeriodo date,
                                     pdNombre_tabla      varchar2,
                                     pd_lvMensErr        out varchar2);

  PROCEDURE LLENATIPO(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  Procedure Llena_telefono(pdNombreTabla varchar2,
                           pd_lvMensErr  out varchar2);

  Procedure LLENA_BUROCREDITO(pdNombreTabla  varchar2,
                              pd_lvMensErr   out varchar2);

  PROCEDURE LLENA_FECH_MAX_PAGO(pdFechCierrePeriodo in date,
                                pdNombre_Tabla  in varchar2,
                                pd_lvMensErr   out varchar2);


  PROCEDURE PROCESA_CUADRE(pdFechaPeriodo       in date,
                           pdNombre_Tabla       in varchar2,
                           pdTabla_OrderHdr     in varchar2,
                           pdTabla_cashreceipts in varchar2,
                           pdTabla_Customer     in varchar2,
                           pd_lvMensErr         out varchar2);

  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2;

  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER;

  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2);

  PROCEDURE ACTUALIZA_CO_CUADRE (PN_PROCESADOS IN OUT NUMBER, PV_ERROR IN OUT VARCHAR2);

  --[2702] ECA
  --Funcion para crear la tabla mensual de la CO_CUADRE
  FUNCTION CREA_TABLA_CUADRE_MENSUAL(pdFechaPeriodo in date,
                                     pv_error       out varchar2,
                                     pvNombreTabla  in varchar2) RETURN NUMBER;
  --[2702] ECA
  --Actualiza la co_cuadre_mensual con el ciclo actual procesado para la co_cuadre
  PROCEDURE ACTUALIZA_CO_CUADRE_MENSUAL(pdFechaPeriodo in date,
                                        pv_error       out VARCHAR2);

  --[2702] ECA
  --Obtiene el codigo de ciclo de facturacion en base a la fecha de periodo
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2;

end COK_PROCESS_REPORT_BORRAR;
/
create or replace package body COK_PROCESS_REPORT_BORRAR is

  -- Variables locales
  gv_funcion varchar2(30);
  gv_mensaje varchar2(500);
  ge_error exception;

  FUNCTION COF_EXTRAE_CARGOS(pv_customer_id in number, pvFechIni in varchar2, pvFechFin in varchar2) RETURN NUMBER IS
    lvSentencia varchar2(2000);
    lnCargo     number;

  BEGIN

    lnCargo := 0;

    SELECT SUM( cargos.cargo ) into lnCargo
    FROM (
          select /*+ rule*/ cu.customer_id id_cliente, f.amount cargo
            from customer_all cu, co_fees_mv f
           where cu.customer_id = pv_customer_id
             AND f.customer_id = cu.customer_id
             and f.entdate BETWEEN
                 to_date(pvFechIni|| ' 00:00', 'dd/mm/yyyy hh24:mi') and
                 to_date(pvFechFin|| ' 23:59', 'dd/mm/yyyy hh24:mi')
             and cu.paymntresp = 'X'
             and f.amount > 0
             and f.sncode <> 103
          UNION ALL
          ---------CARGOS COMO OCCS NEGATIVOS CTAS hijas
          select /*+ rule*/ cu.customer_id id_cliente, f.amount cargo
            from customer_all cu, co_fees_mv f
           where cu.customer_id = pv_customer_id
             and f.customer_id = cu.customer_id
             and f.entdate BETWEEN
                 to_date(pvFechIni|| ' 00:00', 'dd/mm/yyyy hh24:mi') and
                 to_date(pvFechFin|| ' 23:59', 'dd/mm/yyyy hh24:mi')
             and cu.paymntresp is null
             and customer_id_high is not null
             and f.amount > 0
             and f.sncode <> 103
          ) cargos;

      return lnCargo;

  EXCEPTION
      when others then
          return 0;
  END COF_EXTRAE_CARGOS;

  PROCEDURE CO_EDAD_REAL(pv_error out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lvFechaCorteAnualAnterior varchar2(15);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;

    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CO_EDAD_REAL',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    PV_ERROR := NULL;
    -- actualizo la mora en la mora_real
    update co_cuadre set mora_real = mora;
    commit;

    -- se actualiza la mora de los clientes con 330
    select valor into lvFechaCorteAnualAnterior from co_parametros_cliente where campo = 'FECHA_RECLASIFICACION';
    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.total_deuda+b.balance_12, b.mora, b.customer_id ' ||
                     ' from co_repcarcli_'||lvFechaCorteAnualAnterior||
                     ' a, co_cuadre b ' ||
                     ' where a.id_cliente = b.customer_id' ||
                     ' and b.total_deuda > 0' ||
                     ' and b.mora = 330';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;

    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);

    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;

    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);

        execute immediate 'update co_cuadre'||
                          ' set mora_real = :1' ||
                          ' where customer_id = :2'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente;

        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;

      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;

    dbms_sql.close_cursor(source_cursor);

    commit;
    --[2702] ECA / El proceso termina luego del actualiza_co_cuadre
    --pv_error := 'Proceso terminado con Exito';
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CO_EDAD_REAL',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR CO_EDAD_REAL: ' || SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CO_EDAD_REAL',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;


  PROCEDURE CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                   pdFechaCorteMaximo        date,
                                   pv_error                  in out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;
    lnDiff         number;
    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;

  BEGIN

    PV_ERROR := NULL;

    BEGIN
      execute immediate 'create table co_repcarcli_' ||
                        to_char(pdFechaCorteMaximo, 'ddMMyyyy') ||
                        '_norecla as select * from co_repcarcli_' ||
                        to_char(pdFechaCorteMaximo, 'ddMMyyyy');

    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al respaldar tabla ' || 'co_repcarcli_' ||
                    to_char(pdFechaCorteMaximo, 'ddMMyyyy');
        RETURN;
    END;

    select months_between(pdFechaCorteMaximo, pdFechaCorteAnualAnterior)
      into lnDiff
      from dual;

    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.totaladeuda, b.mayorvencido, b.id_cliente ' ||
                     ' from co_repcarcli_' ||
                     to_char(pdFechaCorteAnualAnterior, 'ddMMyyyy') ||
                     ' a, co_repcarcli_' ||
                     to_char(pdFechaCorteMaximo, 'ddMMyyyy') || ' b ' ||
                     ' where a.id_cliente = b.id_cliente' ||
                     ' and b.Totalvencida > 0' ||
                     ' and b.mayorvencido = ''330''';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;

    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);

    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;

    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);

        execute immediate 'update co_repcarcli_' ||
                          to_char(pdFechaCorteMaximo, 'ddMMyyyy') || '' ||
                          ' set mayorvencido = :1' ||
                          ' where id_cliente = :2'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente;

        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;

      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;

    dbms_sql.close_cursor(source_cursor);

    commit;

    pv_error := 'Proceso terminado con Exito.... Error al actualizar = ' ||
                Ln_ContadorErr;

  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR GENERAL: ' || SQLERRM;
  END;

  PROCEDURE CUADRA IS

  pd_lvMensErr  VARCHAR2(200);

  BEGIN
       update co_cuadre
       set saldoant = (balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12) +nvl(pagosper,0)-nvl(credtper,0)-nvl(cmper,0)-nvl(consmper,0);
       commit;

  EXCEPTION
      WHEN OTHERS THEN
        pd_lvMensErr := 'CUADRA: ' || sqlerrm;
        --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CUADRA',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
  NULL;
  END;

  /* ACTUALIZA PLAN IDEAL */
  PROCEDURE SETEA_PLAN_IDEAL(pd_lvMensErr out varchar2) is

    type t_cuenta is table of number index by binary_integer;
    lII           number;
    LC_CUENTA     t_cuenta;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.SETEA_PLAN_IDEAL',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lII := 0;
    LC_CUENTA.DELETE;

    select nvl(cu.customer_id_high, cu.customer_id) codigo
      BULK COLLECT INTO LC_CUENTA
      from rateplan       rt,
           contract_all   co,
           curr_co_status cr,
           customer_all   cu
     where rt.tmcode = co.tmcode
       and co.co_id = cr.co_id
       and co.customer_id = cu.customer_id
       and cr.ch_status = 'a'
       and rt.tmcode >= 452
       and rt.tmcode <= 467
     group by cu.customer_id, cu.customer_id_high;

/*    IF LC_CUENTA.COUNT > 0 THEN
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
          execute immediate ' update co_cuadre' ||
                            ' set plan = ''IDEAL''' ||
                            ' where customer_id = :1'
            using LC_CUENTA(I);
        lII := lII + 1;
        if lII = GN_COMMIT then
           lII := 0;
           commit;
        end if;
       END LOOP;
    END IF;*/
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
          UPDATE co_cuadre
             SET plan = 'IDEAL'
           WHERE customer_id = LC_CUENTA(I);
    END IF;

    LC_CUENTA.DELETE;
    COMMIT;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.SETEA_PLAN_IDEAL',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'SETEA_PLAN_IDEAL: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SETEA_PLAN_IDEAL',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END SETEA_PLAN_IDEAL;


  PROCEDURE INSERTA_OPE_CUADRE(pdNombre_tabla varchar2) is
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

  BEGIN

    -- se llena trunca la tabla de reportes
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'truncate table ope_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre NOLOGGING (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade from ' ||
                   pdNombre_tabla;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

  END;

  PROCEDURE ACTUALIZA_OPE_CUADRE(pdFechCierrePeriodo date) is

    lnMesFinal     NUMBER;
    lvSentencia    VARCHAR2(1000);
    lvMensErr      VARCHAR2(3000);
    lII            NUMBER;
    source_cursor  INTEGER;
    rows_processed INTEGER;
    lnCustomerId   NUMBER;

  BEGIN

    -- se trunca la tabla destino
    lvSentencia := 'truncate table ope_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    -- se adjuntan datos de la tabla de cartera de detalle de clientes
    lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre NOLOGGING (custcode, customer_id, producto, canton, provincia, total_deuda,  mora, region, tipo, total_deuda_cierre)' ||
                   ' select cuenta, id_cliente, producto, canton, provincia, totaladeuda, decode(mayorvencido, ''V'', 0, ''''''-V'', 0, to_number(mayorvencido)), decode(compania, 1, ''Guayaquil'', ''Quito''), 0, balance_' ||
                   lnMesFinal || '  from co_repcarcli_' ||
                   to_char(pdFechCierrePeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    -- se actualiza el tipo
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id from ope_cuadre' || ' minus' ||
                     ' select customer_id from orderhdr_all where ohstatus = ''IN'' and  ohentdate = to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);

      -- actualizo los campos de la tabla final
      update ope_cuadre set tipo = '1_NF' where customer_id = lnCustomerId;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

    -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
    lnMesFinal := to_number(to_char(pdFechCierrePeriodo, 'MM'));
    update ope_cuadre set tipo = '3_VNE' where total_deuda_cierre < 0 and mora = 0;
    commit;

    -- Se actualiza campo tipo para saber que es Mora
    update ope_cuadre set tipo = '5_R' where mora > 0 and total_deuda_cierre >= 0;
    commit;

  END;

  ---------------------------------------------------------------------
  --  INSERTADATOS_CUSTOMER
  --  Extraer los codigos de las cuentas de la tabla customer_all
  --  para tener la referencia inicial de datos de clientes a procesar
  ---------------------------------------------------------------------
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(500);
    lvMensErr   varchar2(1000);
    ciclo       varchar2(2);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.InsertaDatosCustomer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lvSentencia := 'insert /*+ APPEND*/ into ' || pdNombre_tabla ||
                   ' NOLOGGING (custcode,customer_id) ' ||
                   ' select a.custcode, a.customer_id' ||
                   ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma' ||
                   ' where a.billcycle = ma.id_ciclo_admin' ||
                   ' and ci.id_ciclo = ma.id_ciclo' ||
                   ' and a.paymntresp  = ''X''' ||
                   ' and ci.dia_ini_ciclo = ''' ||
                   to_char(pdFechCierrePeriodo, 'dd') || '''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    pd_lvMensErr := lvMensErr;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.InsertaDatosCustomer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'InsertaDatosCustomer: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar InsertaDatosCustomer',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  ---------------------------------------------------------------------
  -- Llena_DatosCliente
  -- agrega informaci�n general del cliente de la vista DATOSCLIENTES
  ---------------------------------------------------------------------
  ---------------------------------------------------------------------
  -- Llena_DatosCliente
  -- agrega informaci�n general del cliente de la vista DATOSCLIENTES
  ---------------------------------------------------------------------
  Procedure Llena_DatosCliente(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2) is

   cursor C_DATOSCLI is
     select /*+ RULE */
       d.customer_id, f.ccfname, f.cclname, f.cssocialsecno, nvl(f.cccity, 'x') cccity,
       nvl(f.ccstate, 'x') ccstate, f.ccname, f.cctn, f.cctn2, f.ccline3 || f.ccline4 dir2, h.tradename
       from customer_all d, -- maestro de cliente
            ccontact_all f, -- informaci�n demografica de la cuente
            payment_all  g, --Forna de pago
            COSTCENTER   j, trade_all h
       where d.customer_id = f.customer_id
         and f.ccbill = 'X'
         and d.customer_id = g.customer_id
         and d.costcenter_id = j.cost_id
         and d.cstradecode = h.tradecode(+);

    type customer_id   is table of number index by binary_integer;
    type ccfname       is table of varchar2(1000) index by binary_integer;
    type cclname       is table of varchar2(1000) index by binary_integer;
    type cssocialsecno is table of varchar2(1000) index by binary_integer;
    type cccity        is table of varchar2(1000) index by binary_integer;
    type ccstate       is table of varchar2(1000) index by binary_integer;
    type ccname        is table of varchar2(1000) index by binary_integer;
    type dir2          is table of varchar2(1000) index by binary_integer;
    type cctn          is table of varchar2(1000) index by binary_integer;
    type cctn2         is table of varchar2(1000) index by binary_integer;
    type tradename     is table of varchar2(1000) index by binary_integer;

    LC_ccfname         ccfname;
    LC_cclname         cclname;
    LC_cssocialsecno   cssocialsecno;
    LC_cccity          cccity;
    LC_ccstate         ccstate;
    LC_ccname          ccname;
    LC_dir2            dir2;
    LC_cctn            cctn;
    LC_cctn2           cctn2;
    LC_tradename       tradename;
    LC_customer_id     customer_id;
    lv_sentencia_upd   varchar2(2000);
    lvMensErr          varchar2(2000);
    lII                number;
    ln_limit_bulk      number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Llena_DatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

    lII := 0;

    OPEN C_DATOSCLI;
    LOOP
      --
      LC_ccfname.DELETE;
      LC_cclname.DELETE;
      LC_cssocialsecno.DELETE;
      LC_cccity.DELETE;
      LC_ccstate.DELETE;
      LC_ccname.DELETE;
      LC_dir2.DELETE;
      LC_cctn.DELETE;
      LC_cctn2.DELETE;
      LC_tradename.DELETE;
      LC_customer_id.DELETE;
      --
      FETCH C_DATOSCLI BULK COLLECT INTO LC_customer_id, LC_ccfname, LC_cclname, 
            LC_cssocialsecno, LC_cccity, LC_ccstate, LC_ccname, LC_cctn, LC_cctn2, 
            LC_dir2, LC_tradename LIMIT ln_limit_bulk;
      EXIT WHEN LC_customer_id.COUNT = 0;
      --
      IF LC_customer_id.COUNT > 0 THEN
         FORALL I IN LC_customer_id.FIRST .. LC_customer_id.LAST
            UPDATE co_cuadre
               SET nombres = LC_ccfname(I), apellidos = LC_cclname(I), ruc = LC_cssocialsecno(I),
                   canton = LC_cccity(I), provincia = LC_ccstate(I), direccion = LC_ccname(I),
                   direccion2 = LC_dir2(I), cont1 = LC_cctn(I), cont2 = LC_cctn2(I), trade = LC_tradename(I)
             WHERE customer_id = LC_customer_id(I);
      END IF;
      --
      COMMIT;
      --
    END LOOP;
    --
    CLOSE C_DATOSCLI;
    
    LC_ccfname.DELETE;
    LC_cclname.DELETE;
    LC_cssocialsecno.DELETE;
    LC_cccity.DELETE;
    LC_ccstate.DELETE;
    LC_ccname.DELETE;
    LC_dir2.DELETE;
    LC_cctn.DELETE;
    LC_cctn2.DELETE;
    LC_tradename.DELETE;
    LC_customer_id.DELETE;

/*      IF LC_customer_id.COUNT > 0 THEN
         FOR I IN LC_customer_id.FIRST .. LC_customer_id.LAST LOOP
           EXECUTE IMMEDIATE
              'update ' || pdnombretabla || ' set' ||
                        ' nombres = :1,' || ' apellidos = :2,' ||
                        ' ruc = :3,' || ' canton = :4,' ||
                        ' provincia = :5,' || ' direccion = :6,' ||
                        ' direccion2 = :7,' || ' cont1 = :8,' ||
                        ' cont2 = :9,' || ' trade = :10' ||
                        ' where customer_id = :11'
           USING LC_ccfname(I), LC_cclname(I), LC_cssocialsecno(I), LC_cccity(I), LC_ccstate(I),
                 LC_ccname(I), LC_dir2(I), LC_cctn(I), LC_cctn2(I), LC_tradename(I), LC_customer_id(I);
          lII := lII + 1;
          if lII = GN_COMMIT then
             lII := 0;
             commit;
          end if;
         END LOOP;
      END IF;*/
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Llena_DatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'llena_datoscliente: ERROR:'|| sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Llena_DatosCliente',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END Llena_DatosCliente;

  -----------------------------------------------------
  --     UPD_FPAGO_PROD_CCOSTO
  --     Se llenan algunos datos generales del cliente
  --     como la forma de pago y tarjeta de credito
  -----------------------------------------------------
  PROCEDURE UPD_FPAGO_PROD_CCOSTO(pdFechCierrePeriodo date,
                                  pdNombre_tabla      varchar2,
                                  pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;

    TYPE customer_id is table of number index by binary_integer;
    TYPE bank_id is table of NUMBER index by binary_integer;
    TYPE bankname is table of varchar2(1000) index by binary_integer;
    TYPE producto is table of varchar2(1000) index by binary_integer;
    TYPE cost_desc is table of varchar2(1000) index by binary_integer;
    TYPE bankaccno is table of varchar2(1000) index by binary_integer;
    TYPE valid_thru_date is table of varchar2(1000) index by binary_integer;
    TYPE accountowner is table of varchar2(1000) index by binary_integer;
    TYPE csactivated is table of DATE index by binary_integer;
    TYPE cstradecode is table of varchar2(1000) index by binary_integer;


     LC_customer_id    customer_id;
     LC_bank_id        bank_id;
     LC_bankname       bankname;
     LC_producto       producto;
     LC_cost_desc      cost_desc;
     LC_bankaccno      bankaccno;
     LC_valid_thru_date  valid_thru_date;
     LC_accountowner     accountowner;
     LC_csactivated      csactivated;
     LC_cstradecode      cstradecode;

  BEGIN

     --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     ln_registros_error_scp:=ln_registros_error_scp+1;
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.UPD_FPAGO_PROD_CCOSTO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;

     lII := 0;

     LC_customer_id.DELETE;
     LC_bank_id.DELETE;
     LC_bankname.DELETE;
     LC_producto.DELETE;
     LC_cost_desc.DELETE;
     LC_bankaccno.DELETE;
     LC_valid_thru_date.DELETE;
     LC_accountowner.DELETE;
     LC_csactivated.DELETE;
     LC_cstradecode.DELETE;

      select /*+ rule */
       d.customer_id,
       k.bank_id,
       k.bankname,
       nvl(i.producto, 'x') producto,
       nvl(j.cost_desc, 'x') cost_desc,
       g.bankaccno,
       g.valid_thru_date,
       g.accountowner,
       d.csactivated,
       d.cstradecode
       BULK COLLECT INTO LC_customer_id,LC_bank_id,LC_bankname,LC_producto,LC_cost_desc,LC_bankaccno,LC_valid_thru_date,LC_accountowner,LC_csactivated,LC_cstradecode
        from customer_all    d,
             payment_all     g,
             read.COB_GRUPOS I,
             COSTCENTER      j,
             bank_all        k
       where d.customer_id = g.customer_id
         and act_used = 'X'
         and D.PRGCODE = I.PRGCODE
         and j.cost_id = d.costcenter_id
         and g.bank_id = k.bank_id;


      IF LC_customer_id.COUNT > 0 THEN
         --
         FOR I IN LC_customer_id.FIRST .. LC_customer_id.LAST LOOP
           EXECUTE IMMEDIATE
              ' update ' || pdNombre_tabla ||
              ' set forma_pago = :1,' || ' des_forma_pago = :2,' ||
              ' producto = :3,' || ' tarjeta_cuenta = :4,' ||
              ' fech_expir_tarjeta = :5,' || ' tipo_cuenta = :6,' ||
              ' fech_aper_cuenta = :7,' || ' grupo = :8,' ||
              ' region = :9' || ' where customer_id = :10'
           USING  LC_bank_id(I), LC_bankname(I), LC_producto(I), LC_bankaccno(I), LC_valid_thru_date(I), LC_accountowner(I), LC_csactivated(I), LC_cstradecode(I), LC_cost_desc(I), LC_customer_id(I);
          lII := lII + 1;
          if lII = GN_COMMIT then
            lII := 0;
            commit;
          end if;

         END LOOP;
      END IF;

     LC_customer_id.DELETE;
     LC_bank_id.DELETE;
     LC_bankname.DELETE;
     LC_producto.DELETE;
     LC_cost_desc.DELETE;
     LC_bankaccno.DELETE;
     LC_valid_thru_date.DELETE;
     LC_accountowner.DELETE;
     LC_csactivated.DELETE;
     LC_cstradecode.DELETE;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.UPD_FPAGO_PROD_CCOSTO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'upd_fpago_prod_ccosto: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar UPD_FPAGO_PROD_CCOSTO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ---------------------------------------------------------
  --     CONV_CBILL_TIPOFPAGO
  --     Coloca la forma de pago que se utilizaba en CBILL
  ---------------------------------------------------------
  PROCEDURE CONV_CBILL_TIPOFPAGO(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;

    CURSOR RPT_PAYMENT IS
      select rpt_cod_bscs, rpt_payment_id, rpt_nombrefp
        from ope_rpt_payment_type;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CONV_CBILL_TIPOFPAGO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- se actualizan los valores para tipo de forma de pago
    FOR b in RPT_PAYMENT LOOP

      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set tipo_forma_pago = :1,' ||
                        ' des_tipo_forma_pago = :2' ||
                        ' where forma_pago = :3'
        using b.rpt_payment_id, b.rpt_nombrefp, b.rpt_cod_bscs;
    END LOOP;
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CONV_CBILL_TIPOFPAGO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'conv_cbill_tipofpago: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CONV_CBILL_TIPOFPAGO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ------------------------------------------------------------------------
  --     UPD_DISPONIBLE_TOTPAGO
  --     Procediento que llena los siguientes campos
  --     totpagos: contiene el total de pagos realizados hasta el corte
  --     creditos: total de creditos realizados hasta el corte
  --     disponible: pagos + creditos utiles para el balanceo
  ------------------------------------------------------------------------
  PROCEDURE Upd_Disponible_Totpago(pdFechCierrePeriodo  date,
                                   pdNombre_tabla       varchar2,
                                   pdTabla_Cashreceipts varchar2,
                                   pdTabla_OrderHdr     varchar2,
                                   pd_lvMensErr         out varchar2) is

    lvSentencia    varchar2(20000);
    lvMensErr      varchar2(2000);
    Le_Error       exception;
    lnMes          number;
    lvPeriodos     varchar2(20000);--JHE 01/marxo/2007

    cursor c_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01'; --invoices since July

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Upd_Disponible_Totpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    ---------------------------
    --        PAGOS
    ---------------------------


    lvSentencia := 'delete co_pago_credito';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;


    lvSentencia   := ' insert /*+ APPEND*/ into co_pago_credito'||
                     ' NOLOGGING (customer_id, totpag) ' ||
                     ' select /*+ rule */ x.customer_id, sum(x.cachkamt_pay)  ' ||
                     ' from  ' || pdTabla_Cashreceipts ||' x , co_cuadre c'||
                     ' where  x.caentdate  < to_date(''' ||to_char(pdFechCierrePeriodo, 'yyyy/MM/dd')||''',''yyyy/MM/dd'')' ||
                     ' and x.customer_id = c.customer_id'||
                     ' and cachkamt_pay <> 0'||
                     ' group by x.customer_id';

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;

    lvSentencia := ' update ' || pdNombre_Tabla || ' c'||
                   ' set c.totpagos = nvl(c.totpagos,0) + (select totpag from co_pago_credito z where z.customer_id = c.customer_id)'||
                   '  ,c.disponible = nvl(c.disponible,0) + (select totpag from co_pago_credito z where z.customer_id = c.customer_id)'||
                   ' where c.customer_id = (select z.customer_id from co_pago_credito z where z.customer_id = c.customer_id)' ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;


    ---------------------------
    --        CREDITOS
    ---------------------------
    lvPeriodos := '';
    for i in c_periodos loop
      lvPeriodos := lvPeriodos || ' to_date(''' ||
                    to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                    ''',''dd/MM/yyyy''),';
    end loop;

    lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);
    --
    lvSentencia := 'delete co_pago_credito';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;


    lvSentencia   := ' insert /*+ APPEND*/ into co_pago_credito'||
                     ' NOLOGGING (customer_id, totcred) ' ||
                     ' select /*+ rule */ x.customer_id, sum(x.ohinvamt_doc)* -1 ' ||
                     ' from  ' || pdTabla_OrderHdr || ' x , co_cuadre c'||
                     ' where x.ohstatus  =''CM''' ||
                     ' and x.customer_id = c.customer_id'||
                     ' and not x.ohentdate in (' || lvPeriodos || ')' ||
                     ' group by x.customer_id' ||
                     ' having sum(x.ohinvamt_doc)<>0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;

    lvSentencia := ' update ' || pdNombre_Tabla || ' c'||
                   ' set c.creditos = nvl(c.creditos,0) + (select totcred from co_pago_credito z where z.customer_id = c.customer_id)'||
                   '  ,c.disponible = nvl(c.disponible,0) + (select totcred from co_pago_credito z where z.customer_id = c.customer_id)'||
                   ' where c.customer_id = (select z.customer_id from co_pago_credito z where z.customer_id = c.customer_id)' ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Upd_Disponible_Totpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    when Le_Error then
       pd_lvMensErr := 'upd_disponible_totpago: ' || lvMensErr;
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Upd_Disponible_Totpago',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'upd_disponible_totpago: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Upd_Disponible_Totpago',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  ------------------------------------------------------------
  --     Llena_NroFactura
  --     Setea el numero de la factura de la tabla de facturas
  ------------------------------------------------------------
  PROCEDURE Llena_NroFactura(pdFechCierrePeriodo date,
                             pdNombre_tabla      varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;

    type customer_id is table of number index by binary_integer;
    TYPE OHREFNUM is table of varchar2(50) index by binary_integer;
    LC_customer_id  customer_id;
    lc_OHREFNUM     OHREFNUM;
    pd_lvMensErr    VARCHAR2(200);


  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Llena_NroFactura',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lII := 0;

    lc_customer_id.delete;
    lc_OHREFNUM.delete;

    select customer_id, OHREFNUM
    bulk collect into lc_customer_id, lc_OHREFNUM
      from orderhdr_all
     where ohstatus = 'IN'
       and ohentdate = pdFechCierrePeriodo;


    IF LC_customer_id.COUNT > 0 THEN
       --
       FOR I IN LC_customer_id.FIRST .. LC_customer_id.LAST LOOP
         EXECUTE IMMEDIATE
             'update ' || pdNombre_Tabla || ' set FACTURA = :1' ||
                      ' where customer_id = :2'
         USING lc_OHREFNUM(I), LC_customer_id(I);
        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;

       END LOOP;
    END IF;

   lc_customer_id.delete;
   lc_OHREFNUM.delete;

   commit;
   --SCP:MENSAJE
   ----------------------------------------------------------------------
   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   ln_registros_error_scp:=ln_registros_error_scp+1;
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Llena_NroFactura',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
   ----------------------------------------------------------------------------
   COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'Llena_NroFactura: ERROR:'|| sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Llena_NroFactura',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ---------------------------------------------------------------
  --    Adiciona_credito_a_IN
  --    Procedimiento que coloca los creditos como parte de
  --    la factura para efectos de cuadratura ya que BSCS
  --    no lo estaba haciendo
  ---------------------------------------------------------------
  PROCEDURE Credito_a_Disponible(pdFechCierrePeriodo  date,
                                 pdNombre_tabla       varchar2,
                                 --pdTabla_Cashreceipts varchar2,
                                 --pdTabla_OrderHdr     varchar2,
                                 --pdTabla_Customer     varchar2,
                                 pd_lvMensErr         out varchar2) is

    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lvSentencia            varchar2(2000);
    lvMensErr              varchar2(2000);
    lnMes                  number;
    lII                    number:=0;
    lnCustomerId           number;
    lnValor                number;
    leError                exception;
    type t_cuenta is table of number index by binary_integer;
    type t_creditos is table of number index by binary_integer;
    LC_CUENTA              t_cuenta;
    LC_CREDITO             t_creditos;
    lv_fecha               varchar2(20);


  BEGIN

       --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Credito_a_Disponible',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

       ------------------------------------------
       -- se actualiza la informacion de creditos
       ------------------------------------------
       delete from co_creditos_cliente where fecha = pdFechCierrePeriodo;
       commit;

       lv_fecha := to_char(pdFechCierrePeriodo,'dd/mm/yyyy');

      lvSentencia := ' INSERT /*+ APPEND*/ INTO co_creditos_cliente' ||
                     ' NOLOGGING (customer_id ,fecha ,credito) ' ||
                     ' SELECT a.customer_id , to_date(' || '''' ||lv_fecha || '''' ||', ' || '''' || 'dd/mm/yyyy' || '''' ||'  ), nvl(sum(a.valor),0) - nvl(sum(a.descuento),0) ' ||
                     ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                     --' FROM cust_3_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                     ' WHERE a.tipo = ''006 - CREDITOS''' ||
                     ' GROUP BY a.customer_id';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

      if lvMensErr is not null then
        raise leError;
      end if;

      commit;

   /*------------------------------------------
   -- se actualiza la informacion para balancE
   ------------------------------------------*/

    LC_CUENTA.DELETE;
    LC_CREDITO.DELETE;

    select a.customer_id CUENTA, sum(a.credito) * -1 CREDITO
      BULK COLLECT INTO LC_CUENTA , LC_CREDITO
      from co_creditos_cliente a, co_cuadre b
     where a.customer_id = b.customer_id
     group by a.customer_id;


      IF LC_CUENTA.COUNT > 0 THEN
         --
         FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
           EXECUTE IMMEDIATE
              ' update co_cuadre set '||
              ' creditos = nvl(creditos,0) + :1,'||
              ' disponible = nvl(disponible,0) + :2 '||
              ' where customer_id=:3'
           USING  LC_CREDITO(I),LC_CREDITO(I), LC_CUENTA(I);
          lII := lII + 1;
          if lII = GN_COMMIT then
            lII := 0;
            commit;
          end if;

         END LOOP;
      END IF;

    LC_CUENTA.DELETE;
    LC_CREDITO.DELETE;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Credito_a_Disponible',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'credito_a_disponible: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Credito_a_Disponible',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  -------------------------------------------------------
  --     EJECUTA_SENTENCIA
  --     Funci�n general que ejecuta sentencias dinamicas
  -------------------------------------------------------
  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2) is
    x_cursor integer;
    z_cursor integer;
    name_already_used exception;
    pragma exception_init(name_already_used, -955);
  begin
    x_cursor := dbms_sql.open_cursor;
    DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
    z_cursor := DBMS_SQL.EXECUTE(x_cursor);
    DBMS_SQL.CLOSE_CURSOR(x_cursor);
  EXCEPTION
    WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
  END EJECUTA_SENTENCIA;

  -------------------------------
  --  CALCULA MORA
  --  Calcula las edades de mora
  -------------------------------
  -- [2574] Se modific� para corregir casos de
  -- edad de mora incorrecta reportados por SCO.
  -- Modificado por: Sis Nadia D�vila O.
  -- L�der: Galo Jer�z.
  -- Fecha: 07/09/2007.
  -----------------------------------
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Si en una migracion de ciclo el cliente
     tiene cartera vencida la mora se mantiene, si la cartera
     esta vigente la mora es 0.  Para el caso de cartera
     vigente se mantiene el mismo criterio para cualquier
     cambio de ciclo, no s�lo en el cambio de
     ciclo 1 al 2 como estaba anteriormente.
  =========================================================*/
  /*==============================================================
     Proyecto       : [3910] Cuarto Ciclo de Facturacion - 
                      Revisi�n de Cuentas Migradas al cuarto ciclo 
                      de facturaci�n
     Fecha Modif.   : 20/10/2008
     Solicitado por : SIS Romeo Cabrera
     Modificado por : Anl. Reyna Asencio
     Motivo         : Si hay una migraci�n de ciclo y la mora es vigente,
                      la cuenta pasa al nuevo ciclo de facturaci�n con mora
                      0 y si la cuenta tiene una mora vencida pasa al nuevo 
                      ciclo con la misma edad de mora, es decir la mora 
                      se mantiene.
    ==============================================================
  */
  PROCEDURE CALCULA_MORA is

    type t_cuenta is table of number index by binary_integer;
    type t_mora   is table of number index by binary_integer;
    --
    LC_CUENTA   t_cuenta;
    LC_MORA     t_mora;

    --variables
    ln_mora        number:=0;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
    lnDiff         number;
    lnRango        number;
    lnI            number;
    lnJ            number;
    source_cursor  integer;
    rows_processed integer;
    lvDia          varchar2(5);

    lnCustomerId number;
    lnBalance1   number;
    ldFecha1     date;
    lnBalance2   number;
    ldFecha2     date;
    lnBalance3   number;
    ldFecha3     date;
    lnBalance4   number;
    ldFecha4     date;
    lnBalance5   number;
    ldFecha5     date;
    lnBalance6   number;
    ldFecha6     date;
    lnBalance7   number;
    ldFecha7     date;
    lnBalance8   number;
    ldFecha8     date;
    lnBalance9   number;
    ldFecha9     date;
    lnBalance10  number;
    ldFecha10    date;
    lnBalance11  number;
    ldFecha11    date;
    lnBalance12  number;
    ldFecha12    date;
    pd_lvMensErr varchar2(200);
  
/*
    cursor cu_edades_migradas is
    select customer_id from co_cuadre
    where balance_1 = 0 and balance_2 = 0 and balance_3 = 0 and balance_4 = 0 and balance_5 = 0 and balance_6 = 0 and balance_7 = 0 and balance_8 = 0 and balance_9 = 0 and balance_10 = 0 and balance_11 > 0 and balance_12 > 0;
*/

  --[2702] ECA
  CURSOR C_CtasCarteraVigente IS
      SELECT a.customer_id
       FROM co_cuadre a
      WHERE a.balance_1 = 0 AND a.balance_2 = 0 AND a.balance_3 = 0 AND a.balance_4 = 0
        AND a.balance_5 = 0 AND a.balance_6 = 0 AND a.balance_7 = 0 AND a.balance_8 = 0
        AND a.balance_9 = 0 AND a.balance_10 = 0 AND a.balance_11 > 0 AND a.balance_12 > 0;
        --AND VERIFICA_CAMBIO_CICLO(a.customer_id)=1; --(1) Si se realizo cambio de ciclo

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CALCULA_MORA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lnI := 0;
    LC_CUENTA.DELETE;
    LC_MORA.DELETE;
    --
    SELECT /*+ RULE +*/ customer_id, months_between(Fecha_12, Fecha_1) * 30 MORA
    BULK COLLECT INTO LC_CUENTA, LC_MORA
    FROM co_cuadre
    WHERE balance_1 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_2) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_3) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_4) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_5) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_6) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_7) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_8) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_9) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_10) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_11) * 30 MORA
    FROM co_cuadre
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 = 0 AND balance_11 > 0
    ORDER BY MORA;
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
           ln_mora:=0;
           --
           if LC_MORA(I) < 0 then
              ln_mora:=0;
           elsif LC_MORA(I) = 0 then
              ln_mora:=0;
           elsif LC_MORA(I) > 330 then
              ln_mora:=330;
           else
              ln_mora:=LC_MORA(I);
           end if;
           ---------------------------------------------------------
           -- [3910] Cuarto Ciclo de Facturaci�n - SUD RAS
           -- Si hay una migraci�n le restamos un mes vencido 
           -- Ej: Si la migracion fue del ciclo 1 al ciclo 4 
           -- del 23 de Sep al 2 de Oct , el proceso asume q 
           -- la cuenta tiene un mes vencido porque tiene 
           -- un balance 12 y 11 cuando todav�a no ha pasado un mes.
           ---------------------------------------------------------
           IF VERIFICA_CAMBIO_CICLO(LC_CUENTA(I))=1 THEN
    	        ln_mora:=ln_mora-30;     
           END IF;
           ---------------------------------------------
           --
           EXECUTE IMMEDIATE ' update co_cuadre' ||
                             ' set mora = :1' ||
                             ' where customer_id = :2'
             USING ln_mora, LC_CUENTA(I);
           --
           lnI := lnI + 1;
           IF lnI = GN_COMMIT THEN
              lnI := 0;
              COMMIT;
           END IF;
       END LOOP;
    END IF;

    LC_CUENTA.DELETE;
    LC_MORA.DELETE;
    COMMIT;
/*
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, balance_1, fecha_1, balance_2, fecha_2, balance_3, fecha_3, balance_4, fecha_4, balance_5, fecha_5, balance_6, fecha_6, balance_7, fecha_7, balance_8, fecha_8, balance_9, fecha_9, balance_10, fecha_10, balance_11, fecha_11, balance_12, fecha_12' ||
                     ' from co_cuadre';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnBalance1);
    dbms_sql.define_column(source_cursor, 3, ldFecha1);
    dbms_sql.define_column(source_cursor, 4, lnBalance2);
    dbms_sql.define_column(source_cursor, 5, ldFecha2);
    dbms_sql.define_column(source_cursor, 6, lnBalance3);
    dbms_sql.define_column(source_cursor, 7, ldFecha3);
    dbms_sql.define_column(source_cursor, 8, lnBalance4);
    dbms_sql.define_column(source_cursor, 9, ldFecha4);
    dbms_sql.define_column(source_cursor, 10, lnBalance5);
    dbms_sql.define_column(source_cursor, 11, ldFecha5);
    dbms_sql.define_column(source_cursor, 12, lnBalance6);
    dbms_sql.define_column(source_cursor, 13, ldFecha6);
    dbms_sql.define_column(source_cursor, 14, lnBalance7);
    dbms_sql.define_column(source_cursor, 15, ldFecha7);
    dbms_sql.define_column(source_cursor, 16, lnBalance8);
    dbms_sql.define_column(source_cursor, 17, ldFecha8);
    dbms_sql.define_column(source_cursor, 18, lnBalance9);
    dbms_sql.define_column(source_cursor, 19, ldFecha9);
    dbms_sql.define_column(source_cursor, 20, lnBalance10);
    dbms_sql.define_column(source_cursor, 21, ldFecha10);
    dbms_sql.define_column(source_cursor, 22, lnBalance11);
    dbms_sql.define_column(source_cursor, 23, ldFecha11);
    dbms_sql.define_column(source_cursor, 24, lnBalance12);
    dbms_sql.define_column(source_cursor, 25, ldFecha12);
    rows_processed := Dbms_sql.execute(source_cursor);

    lnI := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnBalance1);
      dbms_sql.column_value(source_cursor, 3, ldFecha1);
      dbms_sql.column_value(source_cursor, 4, lnBalance2);
      dbms_sql.column_value(source_cursor, 5, ldFecha2);
      dbms_sql.column_value(source_cursor, 6, lnBalance3);
      dbms_sql.column_value(source_cursor, 7, ldFecha3);
      dbms_sql.column_value(source_cursor, 8, lnBalance4);
      dbms_sql.column_value(source_cursor, 9, ldFecha4);
      dbms_sql.column_value(source_cursor, 10, lnBalance5);
      dbms_sql.column_value(source_cursor, 11, ldFecha5);
      dbms_sql.column_value(source_cursor, 12, lnBalance6);
      dbms_sql.column_value(source_cursor, 13, ldFecha6);
      dbms_sql.column_value(source_cursor, 14, lnBalance7);
      dbms_sql.column_value(source_cursor, 15, ldFecha7);
      dbms_sql.column_value(source_cursor, 16, lnBalance8);
      dbms_sql.column_value(source_cursor, 17, ldFecha8);
      dbms_sql.column_value(source_cursor, 18, lnBalance9);
      dbms_sql.column_value(source_cursor, 19, ldFecha9);
      dbms_sql.column_value(source_cursor, 20, lnBalance10);
      dbms_sql.column_value(source_cursor, 21, ldFecha10);
      dbms_sql.column_value(source_cursor, 22, lnBalance11);
      dbms_sql.column_value(source_cursor, 23, ldFecha11);
      dbms_sql.column_value(source_cursor, 24, lnBalance12);
      dbms_sql.column_value(source_cursor, 25, ldFecha12);

      for lnJ in 1 .. 12 loop
        if lnJ = 1 and lnBalance1 > 0 then
          select months_between(ldFecha12, ldFecha1) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 2 and lnBalance2 > 0 then
          select months_between(ldFecha12, ldFecha2) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 3 and lnBalance3 > 0 then
          select months_between(ldFecha12, ldFecha3) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 4 and lnBalance4 > 0 then
          select months_between(ldFecha12, ldFecha4) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 5 and lnBalance5 > 0 then
          select months_between(ldFecha12, ldFecha5) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 6 and lnBalance6 > 0 then
          select months_between(ldFecha12, ldFecha6) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 7 and lnBalance7 > 0 then
          select months_between(ldFecha12, ldFecha7) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 8 and lnBalance8 > 0 then
          select months_between(ldFecha12, ldFecha8) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 9 and lnBalance9 > 0 then
          select months_between(ldFecha12, ldFecha9) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 10 and lnBalance10 > 0 then
          select months_between(ldFecha12, ldFecha10) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 11 and lnBalance11 > 0 then
          select months_between(ldFecha12, ldFecha11) * 30
            into lnDiff
            from dual;
          exit;
        else
          lnDiff := 0;
        end if;
      end loop;

      if lnDiff < 0 then
        lnRango := 0;
      elsif lnDiff = 0 then
        lnRango := 0;
      elsif lnDiff >= 1 and lnDiff <= 30 then
        lnRango := 30;
      elsif lnDiff >= 31 and lnDiff <= 60 then
        lnRango := 60;
      elsif lnDiff >= 61 and lnDiff <= 90 then
        lnRango := 90;
      elsif lnDiff >= 91 and lnDiff <= 120 then
        lnRango := 120;
      elsif lnDiff >= 121 and lnDiff <= 150 then
        lnRango := 150;
      elsif lnDiff >= 151 and lnDiff <= 180 then
        lnRango := 180;
      elsif lnDiff >= 181 and lnDiff <= 210 then
        lnRango := 210;
      elsif lnDiff >= 211 and lnDiff <= 240 then
        lnRango := 240;
      elsif lnDiff >= 241 and lnDiff <= 270 then
        lnRango := 270;
      elsif lnDiff >= 271 and lnDiff <= 300 then
        lnRango := 300;
      elsif lnDiff >= 301 and lnDiff <= 330 then
        lnRango := 330;
      else
        lnRango := 330;
      end if;

      execute immediate 'update co_cuadre' || ' set mora = :1' ||
                        ' where customer_id = :2'
        using lnRango, lnCustomerId;

      lnI := lnI + 1;
      if lnI = GN_COMMIT then
        lnI := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
*/
    -- se actualiza campo mora para balance_12 negativos y con mora mayor a cero
    update co_cuadre set mora = 0
    where balance_12 < 0
    and mora > 0;
    commit;

    -- se actualiza el campo mora_real_mig antes de cambiarlo
    update co_cuadre set mora_real_mig = mora;
    commit;

    -- se actualizan aquellas que hayan migrado
    --select to_char(fecha_12, 'dd') into lvDia from co_cuadre where rownum = 1;
    --
    --IF lvDia = '08' THEN
       lnI := 0;
       LC_CUENTA.DELETE;
       --
       /*SELECT customer_id
       BULK COLLECT INTO LC_CUENTA
         FROM co_cuadre
        WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0
          AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0
          AND balance_9 = 0 AND balance_10 = 0 AND balance_11 > 0 AND balance_12 > 0;*/

       -- Nuevo query que obtendr� solo las cuentas que se hayan cambiado de ciclo
       -- recientemente es decir que solo tengan una sola facturaci�n con fecha d�a '08'
       -- [2574] NDA 07/09/2007
       /*SELECT a.customer_id
         BULK COLLECT INTO LC_CUENTA
         FROM co_cuadre a
        WHERE a.balance_1 = 0 AND a.balance_2 = 0 AND a.balance_3 = 0 AND a.balance_4 = 0
          AND a.balance_5 = 0 AND a.balance_6 = 0 AND a.balance_7 = 0 AND a.balance_8 = 0
          AND a.balance_9 = 0 AND a.balance_10 = 0 AND a.balance_11 > 0 AND a.balance_12 > 0
          AND EXISTS (SELECT \*rule*\ c.customer_id, COUNT(*)
                        FROM co_consumos_cliente c
                       WHERE c.customer_id = A.CUSTOMER_ID
                         AND to_char(c.fecha,'dd') = '08'
                    GROUP BY c.customer_id
                      HAVING COUNT(*) = 1);*/

       --
/*       IF LC_CUENTA.COUNT > 0 THEN
          FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
              EXECUTE IMMEDIATE ' update co_cuadre' ||
                                ' set mora = 0' ||
                                ' where customer_id = :1'
              USING LC_CUENTA(I);
              --
              lnI := lnI + 1;
              IF lnI = GN_COMMIT THEN
                 lnI := 0;
                 COMMIT;
              END IF;
          END LOOP;
       END IF;*/
       
       -----------------------------------------------------
       --[3910] RAS comentado C_CtasCarteraVigente 
       --porque ya se est� considerando este escenario
       --tanto para la mora vigente y vencida
       -----------------------------------------------------
       --[2702] ECA
       --Se modifica el query para obtener cualquier cambio de ciclo
/*     OPEN C_CtasCarteraVigente;
       FETCH C_CtasCarteraVigente BULK COLLECT INTO LC_CUENTA;
       CLOSE C_CtasCarteraVigente;
       --
       IF LC_CUENTA.COUNT > 0 THEN
          FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
    			    IF VERIFICA_CAMBIO_CICLO(LC_CUENTA(I))=1 THEN
    	              UPDATE co_cuadre
    	                 SET mora = 0
    	               WHERE customer_id = LC_CUENTA(I);
              END IF;
          END LOOP;
       END IF;
       --
       LC_CUENTA.DELETE;
       COMMIT;*/ --[3910] RAS comentado C_CtasCarteraVigente 

    --END IF;
/*
    if lvDia = '08' then
        lnI := 0;
        for i in cu_edades_migradas loop
             update co_cuadre set mora = 0 where customer_id = i.customer_id;
             lnI := lnI + 1;
             if lnI = GN_COMMIT then
                 lnI := 0;
                 commit;
             end if;
        end loop
        commit;
    end if;
*/
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  ln_registros_error_scp:=ln_registros_error_scp+1;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CALCULA_MORA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'CALCULA_MORA: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CALCULA_MORA',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END CALCULA_MORA;

  --[2702] ECA
  --Funcion para verificar si la cuenta realizo un cambio de ciclo
  --Devuelve 1 si se realizo un cambio de ciclo
  --Devuelve 0 si no se realizo un cambio de ciclo
  FUNCTION VERIFICA_CAMBIO_CICLO (Cn_CustomerId NUMBER) RETURN NUMBER IS

  --Cursor para consultar las 2 ultimas facturas del cliente
  CURSOR C_Obtiene_Ultimas_Facturas IS
    SELECT LBC_DATE
      FROM (SELECT LBC_DATE
              FROM LBC_DATE_HIST
             WHERE CUSTOMER_ID = Cn_CustomerId
             ORDER BY LBC_DATE DESC)
     WHERE ROWNUM < 3;

  Ld_FechaFacturaActual   DATE;
  Ld_FechaFacturaAnterior DATE;
  Ln_CambioCiclo          NUMBER;

  BEGIN
       --Seteo Variables
       Ld_FechaFacturaActual:=NULL;
       Ld_FechaFacturaAnterior:=NULL;
       Ln_CambioCiclo:=0;
       --Obtengo la fecha de facturacion actual y la anterior
       FOR A IN C_Obtiene_Ultimas_Facturas LOOP
           IF Ld_FechaFacturaActual IS NULL THEN
               Ld_FechaFacturaActual:=A.LBC_DATE;
           ELSE
               Ld_FechaFacturaAnterior:=A.LBC_DATE;
           END IF;
       END LOOP;
       --Si la cuenta ha tenido mas de una facturacion
       IF Ld_FechaFacturaActual IS NOT NULL AND Ld_FechaFacturaAnterior IS NOT NULL THEN
           --Comparo los dias de las facturas para verificar el cambio de ciclo
           IF to_char(Ld_FechaFacturaActual,'dd')<>to_char(Ld_FechaFacturaAnterior,'dd') THEN
              Ln_CambioCiclo:=1;
           END IF;
       ELSE --Si la cuenta tiene solo una facturacion
           Ln_CambioCiclo:=0;
       END IF;
       RETURN (Ln_CambioCiclo);
  END;

  -----------------------
  --  BALANCEO
  ----------------------

  PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2) is
    -- variables
    val_fac_         varchar2(20);
    lv_sentencia     varchar2(2000);
    lv_sentencia_upd varchar2(2000);
    v_sentencia      varchar2(2000);
    lv_campos        varchar2(2000);
    mes              varchar2(2);
    nombre_campo     varchar2(20);
    lvMensErr        varchar2(2000) := null;

    wc_rowid       varchar2(100);
    wc_customer_id number;
    wc_disponible  number;
    val_fac_1      number;
    val_fac_2      number;
    val_fac_3      number;
    val_fac_4      number;
    val_fac_5      number;
    val_fac_6      number;
    val_fac_7      number;
    val_fac_8      number;
    val_fac_9      number;
    val_fac_10     number;
    val_fac_11     number;
    val_fac_12     number;
    --
    nro_mes             number;
    contador_mes        number;
    contador_campo      number;
    v_CursorId          number;
    v_cursor_asigna     number;
    v_Dummy             number;
    v_Row_Update        number;
    aux_val_fact        number;
    total_deuda_cliente number;
    leError exception;
  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.BALANCEO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- CBR: 27 Enero 2004
    -- Desde el 2004 se trabajaran siempre con las 12 columnas
    mes := 12;
    nro_mes := 12;

    if mes = '01' then
      lv_campos := 'balance_1';
    elsif mes = '02' then
      lv_campos := 'balance_1, balance_2';
    elsif mes = '03' then
      lv_campos := 'balance_1, balance_2, balance_3';
    elsif mes = '04' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4';
    elsif mes = '05' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
    elsif mes = '06' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
    elsif mes = '07' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
    elsif mes = '08' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
    elsif mes = '09' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
    elsif mes = '10' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
    elsif mes = '11' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
    elsif mes = '12' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
    end if;

    v_cursorId := 0;
    v_CursorId := DBMS_SQL.open_cursor;

    --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
    lv_sentencia := ' select c.rowid, customer_id, disponible, ' ||
                    lv_campos || ' from ' || pdNombre_tabla || ' c ';
    --' where customer_id in (450)';
    Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
    contador_campo := 0;
    contador_mes   := 1;
    dbms_sql.define_column(v_cursorId, 1, wc_rowid, 30);
    dbms_sql.define_column(v_cursorId, 2, wc_customer_id);
    dbms_sql.define_column(v_cursorId, 3, wc_disponible);

    -- define variables de salida dinamica
    if nro_mes >= 1 then
      dbms_sql.define_column(v_cursorId, 4, val_fac_1);
    end if;
    if nro_mes >= 2 then
      dbms_sql.define_column(v_cursorId, 5, val_fac_2);
    end if;
    if nro_mes >= 3 then
      dbms_sql.define_column(v_cursorId, 6, val_fac_3);
    end if;
    if nro_mes >= 4 then
      dbms_sql.define_column(v_cursorId, 7, val_fac_4);
    end if;
    if nro_mes >= 5 then
      dbms_sql.define_column(v_cursorId, 8, val_fac_5);
    end if;
    if nro_mes >= 6 then
      dbms_sql.define_column(v_cursorId, 9, val_fac_6);
    end if;
    if nro_mes >= 7 then
      dbms_sql.define_column(v_cursorId, 10, val_fac_7);
    end if;
    if nro_mes >= 8 then
      dbms_sql.define_column(v_cursorId, 11, val_fac_8);
    end if;
    if nro_mes >= 9 then
      dbms_sql.define_column(v_cursorId, 12, val_fac_9);
    end if;
    if nro_mes >= 10 then
      dbms_sql.define_column(v_cursorId, 13, val_fac_10);
    end if;
    if nro_mes >= 11 then
      dbms_sql.define_column(v_cursorId, 14, val_fac_11);
    end if;
    if nro_mes = 12 then
      dbms_sql.define_column(v_cursorId, 15, val_fac_12);
    end if;
    v_Dummy := Dbms_sql.execute(v_cursorId);

    Loop
      total_deuda_cliente := 0;
      lv_sentencia_upd    := '';
      lv_sentencia_upd    := 'update ' || pdNombre_tabla || ' set ';

      if dbms_sql.fetch_rows(v_cursorId) = 0 then
        exit;
      end if;
      --
      -- recupero valores en los campos  y disminuyo valores de factura segun monto disponible
      --
      dbms_sql.column_value(v_cursorId, 1, wc_rowid);
      dbms_sql.column_value(v_cursorId, 2, wc_customer_id);
      dbms_sql.column_value(v_cursorId, 3, wc_disponible);

      wc_disponible:=nvl(wc_disponible,0);

      if nro_mes >= 1 then
        dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_1, 0);
        if wc_disponible >= aux_val_fact then
          --  900        >   100
          val_fac_1     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
          --
        else
          --[2702] ECA - 09/01/2008
          --Si el disponible es negativo quiere decir que el cr�dito es mayor que los pagos
          --y ocasiona que se grabe un valor erroneo en el balance_1
          --Ejemplo: val_fac_1     := aux_val_fact (0) - wc_disponible (-0.69)
          if wc_disponible<0 then
             wc_disponible:=0;
          end if;
          --  900 dispo       <  1000 ene
          val_fac_1     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 1 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_1 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = ' || val_fac_1 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 2 then
        dbms_sql.column_value(v_cursorId, 5, val_fac_2); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_2, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_2     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_2     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 2 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_2 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = ' || val_fac_2 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 3 then
        dbms_sql.column_value(v_cursorId, 6, val_fac_3); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_3, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_3     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_3     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 3 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_3 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = ' || val_fac_3 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 4 then
        dbms_sql.column_value(v_cursorId, 7, val_fac_4); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_4, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_4     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_4     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 4 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_4 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = ' || val_fac_4 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 5 then
        dbms_sql.column_value(v_cursorId, 8, val_fac_5); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_5, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_5     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_5     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 5 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_5 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = ' || val_fac_5 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      if nro_mes >= 6 then
        dbms_sql.column_value(v_cursorId, 9, val_fac_6); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_6, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_6     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_6     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 6 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_6 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = ' || val_fac_6 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 7 then
        dbms_sql.column_value(v_cursorId, 10, val_fac_7); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_7, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_7     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_7     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 7 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_7 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = ' || val_fac_7 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 8 then
        dbms_sql.column_value(v_cursorId, 11, val_fac_8); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_8, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_8     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_8     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 8 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_8 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = ' || val_fac_8 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 9 then
        dbms_sql.column_value(v_cursorId, 12, val_fac_9); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_9, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_9     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_9     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 9 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_9 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = ' || val_fac_9 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 10 then
        dbms_sql.column_value(v_cursorId, 13, val_fac_10); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_10, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_10    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_10    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 10 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_10 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = ' ||
                            val_fac_10 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 11 then
        dbms_sql.column_value(v_cursorId, 14, val_fac_11); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_11, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_11    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_11    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 11 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_11 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = ' ||
                            val_fac_11 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes = 12 then
        dbms_sql.column_value(v_cursorId, 15, val_fac_12); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_12, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_12    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_12    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 12 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_12 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = ' ||
                            val_fac_12 || ' , '; -- Armando sentencia para UPDATE
      end if;
      -- Quito la coma y finalizo la sentencia
      lv_sentencia_upd := substr(lv_sentencia_upd,
                                 1,
                                 length(lv_sentencia_upd) - 2);
      lv_sentencia_upd := lv_sentencia_upd || ' where customer_id = ' ||
                          wc_customer_id;
      --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

      EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;

    End Loop; -- 1. Para extraer los datos del cursor
    commit;

    dbms_sql.close_cursor(v_CursorId);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.BALANCEO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := 'balanceo: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar BALANCEO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(v_CursorId) THEN
        DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      END IF;
      pd_lvMensErr := 'balanceo: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar BALANCEO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END BALANCEO;

  -----------------------------------------------------
  --    ACTUALIZA_CMPER
  --    Setea los valores por cm del periodo actual
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CMPER(pdFechCierrePeriodo in date,
                            pdNombre_tabla      in varchar2,
                            pNombre_orderhdr    in varchar2,
                            pdMensErr           out varchar2) is

    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CMPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, nvl(sum(ohinvamt_doc),0) valor' ||
                     ' from  ' || pNombre_orderhdr ||
                     ' where ohstatus  =''CM''' ||
                     ' and ohentdate  > to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')' ||
                     ' and ohentdate  < to_date(''' ||
                     to_char(pdFechCierrePeriodo, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' || ' group  by customer_id';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);

      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set cmper = nvl(cmper,0) + :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CMPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_CMPER',pdMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ACTUALIZA_CMPER;

  -----------------------------------------------------
  --    ACTUALIZA_CONSMPER
  --    Setea los valores facturados del perido
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CONSMPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2) is

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CONSMPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    source_cursor := DBMS_SQL.open_cursor;

    lvSentencia := 'SELECT a.customer_id, nvl(sum(a.valor),0), nvl(sum(a.descuento),0)' ||
                   ' FROM co_fact_' ||
                   --' FROM cust_3_' ||
                   to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                   pdNombre_tabla || ' b' ||
                   ' WHERE a.customer_id = b.customer_id and' ||
                   ' a.tipo != ''006 - CREDITOS''' ||
                   ' GROUP BY a.customer_id';

    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    dbms_sql.define_column(source_cursor, 3, lnDescuento);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);

      execute immediate 'update ' || pdNombre_tabla ||
                        ' set consmper = nvl(consmper,0) + :1 - :2' ||
                        ' where customer_id = :3'
        using lnValor, lnDescuento, lnCustomerId;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CONSMPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_CONSMPER',pdMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ACTUALIZA_CONSMPER;

  -----------------------------------------------------
  --    ACTUALIZA_CREDTPER
  --    Setea los valores de creditos del perido
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_CREDTPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2) is
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnValor        number;
    lnCustomerId   number;
    lnDescuento    number;

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CREDTPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'SELECT a.customer_id, nvl(sum(a.valor),0), nvl(sum(a.descuento),0)' ||
                     ' FROM co_fact_' ||
                     --' FROM cust_3_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                     pdNombre_tabla || ' b' ||
                     ' WHERE a.customer_id = b.customer_id and' ||
                     ' a.tipo = ''006 - CREDITOS''' ||
                     ' GROUP BY a.customer_id, a.cost_desc';

    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    dbms_sql.define_column(source_cursor, 3, lnDescuento);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);

      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set credtper = nvl(credtper,0) + :1 - :2' ||
                        ' where customer_id = :3'
        using lnValor, lnDescuento, lnCustomerId;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;

    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CREDTPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_CREDTPER',pdMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ACTUALIZA_CREDTPER;

  -----------------------------------------------------
  --    ACTUALIZA_PAGOSPER
  --    Setea los valores de pagos del periodo
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_PAGOSPER(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pdTabla_Cashreceipts in varchar2,
                               pdMensErr            out varchar2) is

    lvMaxPagoAgo    number;
    lvMaxPagoSep    number;
    lII             number;
    lvSentencia     varchar2(2000);
    lvMensErr       varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    lnCustomerId    number;
    lnValor         number;
    --pdFechaFinal    date;
    pdFechaAnterior date;
    ldMesCierrAnt   date;

    /*cursor cierres is
      select distinct lrstart
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;*/

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_PAGOSPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    select add_months(pdFechCierrePeriodo, -1) into ldMesCierrAnt from dual;
    pdFechaAnterior := to_date(to_char(pdFechCierrePeriodo, 'dd')||'/'||to_char(ldMesCierrAnt, 'MM')||'/'||to_char(ldMesCierrAnt, 'yyyy'),'dd/MM/yyyy');

    /*-- se extraen las dos ultimas fechas de cierre para el query posterior
    lII := 0;
    for i in cierres loop
      lII := lII + 1;
      if lII = 1 then
        pdFechaFinal := i.lrstart;
      elsif lII = 2 then
        pdFechaAnterior := i.lrstart;
        exit;
      end if;
    end loop;*/


    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select /*+ rule */ customer_id, decode(sum(cachkamt_pay),null,0,sum(cachkamt_pay)) TotPago' ||
                     ' from  ' || pdTabla_Cashreceipts ||
                     ' where  caentdate  >= to_date(''' ||
                     to_char(pdFechaAnterior, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' ||
                     ' and  caentdate  < to_date(''' ||
                     to_char(pdFechCierrePeriodo, 'yyyy/MM/dd') ||
                     ''',''yyyy/MM/dd'')' || ' group  by customer_id' ||
                     ' having sum(cachkamt_pay) <>0';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);

      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set pagosper = nvl(pagosper,0) + :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_PAGOSPER',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_PAGOSPER',pdMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ACTUALIZA_PAGOSPER;

  -----------------------------------------------
  --           ACTUALIZA_SALDOANT
  -----------------------------------------------
  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2) is

    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    lII            number;
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;

  BEGIN
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, cscurbalance ' || ' from  ' ||
                     pTablaCustomerAllAnt;
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);

      execute immediate 'update ' || pdNombre_Tabla || ' set saldoant = :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_SALDOANT;

  -----------------------------------------------------
  --    ACTUALIZA_DESCUENTO
  --    Llena los valores de la columna descuento
  --    CBR: 28 Noviembre
  -----------------------------------------------------
  PROCEDURE ACTUALIZA_DESCUENTO(pdFechCierrePeriodo in date,
                                pdNombre_tabla      in varchar2,
                                pdMensErr           out varchar2) is

    type t_cuenta    is table of number index by binary_integer;
    type t_descuento is table of number index by binary_integer;

    LC_CUENTA        t_cuenta;
    LC_DESCUENTO     t_descuento;

    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
/*
    cursor cur_descuento is
      select \*+ rule *\
       d.customer_id, sum(A.OTAMT_DISC_DOC) as descuento
        from ordertrailer  A, --items de la factura
             mpusntab      b, --maestro de servicios
             orderhdr_all  c, --cabecera de facturas
             customer_all  d, -- maestro de cliente
             ccontact_all  f, -- informaci�n demografica de la cuente
             payment_all   g, --Forna de pago
             COB_SERVICIOS h, --formato de reporte
             COB_GRUPOS    I, --NOMBRE GRUPO
             COSTCENTER    j
       where a.otxact = c.ohxact
         and c.ohentdate = pdFechCierrePeriodo
         and C.OHSTATUS IN ('IN', 'CM')
         AND C.OHUSERID IS NULL
         AND c.customer_id = d.customer_id
         and substr(otname,
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                    instr(otname,
                          '.',
                          instr(otname,
                                '.',
                                instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
             b.sncode
         and d.customer_id = f.customer_id
         and f.ccbill = 'X'
         and g.customer_id = d.customer_id
         and act_used = 'X'
         and h.servicio = b.sncode
         and h.ctactble = a.otglsale
         AND D.PRGCODE = I.PRGCODE
         and j.cost_id = d.costcenter_id
       group by d.customer_id
      having sum(A.OTAMT_DISC_DOC) > 0;
*/

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_DESCUENTO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lII := 0;

    LC_CUENTA.DELETE;
    LC_DESCUENTO.DELETE;

    select /*+ rule +*/
      d.customer_id, sum(A.OTAMT_DISC_DOC) as descuento
    BULK COLLECT INTO LC_CUENTA, LC_DESCUENTO
      from ordertrailer  A, --items de la factura
           mpusntab      b, --maestro de servicios
           orderhdr_all  c, --cabecera de facturas
           customer_all  d, -- maestro de cliente
           ccontact_all  f, -- informaci�n demografica de la cuente
           payment_all   g, --Forna de pago
           COB_SERVICIOS h, --formato de reporte
           COB_GRUPOS    I, --NOMBRE GRUPO
           COSTCENTER    j
     where a.otxact = c.ohxact
       and c.ohentdate = pdFechCierrePeriodo
       and C.OHSTATUS IN ('IN', 'CM')
       and C.OHUSERID IS NULL
       and c.customer_id = d.customer_id
       and substr(otname,
                  instr(otname,
                        '.',
                        instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                  instr(otname,
                        '.',
                        instr(otname,
                              '.',
                              instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                  instr(otname,
                        '.',
                        instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
           b.sncode
       and d.customer_id = f.customer_id
       and f.ccbill = 'X'
       and g.customer_id = d.customer_id
       and act_used = 'X'
       and h.servicio = b.sncode
       and h.ctactble = a.otglsale
       and D.PRGCODE = I.PRGCODE
       and j.cost_id = d.costcenter_id
     group by d.customer_id
    having sum(A.OTAMT_DISC_DOC) > 0;

/*    IF LC_CUENTA.COUNT > 0 THEN
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
           EXECUTE IMMEDIATE
                        ' Update ' || pdNombre_tabla ||
                        ' set descuento = descuento + :1' ||
                        ' where customer_id = :2'
             USING LC_DESCUENTO(I), LC_CUENTA(I);
           --
           lII := lII + 1;
           IF lII = GN_COMMIT THEN
              lII := 0;
              COMMIT;
           END IF;
       END LOOP;
    END IF;*/
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
          UPDATE co_cuadre
          SET descuento = nvl(descuento,0) + LC_DESCUENTO(I)
          WHERE customer_id = LC_CUENTA(I);
    END IF;
    --
    LC_CUENTA.DELETE;
    LC_DESCUENTO.DELETE;
    COMMIT;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_DESCUENTO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

/*
    for i in cur_descuento loop
      --lvSentencia := ' Update ' || pdNombre_tabla ||
      --               ' set descuento = descuento + ' || i.descuento ||
      --               ' where customer_id = ' || i.customer_id;
      --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      execute immediate ' Update ' || pdNombre_tabla ||
                        ' set descuento = descuento + :1' ||
                        ' where customer_id = :2'
        using i.descuento, i.customer_id;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
*/


  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_DESCUENTO',pdMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ACTUALIZA_DESCUENTO;


  -----------------------------------------
  --     LLENA_BALANCES
  -----------------------------------------
  -- [2574] Se modific� para corregir casos
  -- de cuentas con saldo a favor elevado.
  -- Modificado por: Sis Nadia D�vila O.
  -- L�der: Galo Jer�z.
  -- Fecha: 07/09/2007.
  -----------------------------------------
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Se quita la doble logica que se manejaba por ciclo 1
     y ciclo 2, queda una logica estandar para cualquier ciclo.
  =========================================================*/

  PROCEDURE LLENA_BALANCES(pdFechCierrePeriodo date,
                           pdNombre_tabla      varchar2,
                           pd_Tabla_OrderHdr   varchar2,
                           pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;

    nombre_campo       varchar2(20);
    nombre_fecha       varchar2(20);

    nro_mes        number;
    cuenta_mes     number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;

    leError        EXCEPTION;
    lv_fecha       varchar2(20);

    --[2574] NDA 07/09/2007
    lv_dia_cierre  VARCHAR2(2);
    Fecha          VARCHAR2(10);

    cursor cur_consumos_cliente is
      select /*+ rule */ a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre_borrar_ b
      where a.customer_id = b.customer_id
      order by a.customer_id, a.fecha desc;

    cursor cur_consumos_cliente2(pfecha date) is
      select /*+ rule */ a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre_borrar_ b
      where a.customer_id = b.customer_id
      and a.fecha = pfecha;

    cursor cur_consumos_cliente3(pfecha date) is
      select /*+ rule */a.customer_id, a.fecha, a.consumo
      from co_consumos_cliente a, co_cuadre_borrar_ b
      where a.customer_id = b.customer_id
      and a.fecha <= pfecha;

    cursor cierres is
      select distinct lrstart
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;

    --[2574] NDA 07/09/2007
    cursor cur_cons_ciclos_cliente2(pfecha DATE) is
      select /*+ rule */ a.customer_id, a.fecha, a.consumo
        from co_consumos_cliente a, co_cuadre_borrar_ b
       where a.customer_id = b.customer_id
         and fecha BETWEEN TRUNC(pfecha, 'MONTH') AND last_day(trunc(pfecha));

    --[2574] NDA 07/09/2007
    cursor cur_cons_ciclos_cliente3(pfecha date) is
      select /*+ rule */a.customer_id, SUM(a.consumo) consumo
        from co_consumos_cliente a, co_cuadre_borrar_ b
       where a.customer_id = b.customer_id
         and a.fecha <= pfecha
       GROUP BY a.customer_id;

    --[2574] NDA 07/09/2007
    CURSOR cierres_ciclos IS
      select distinct trunc(lrstart,'MONTH') fecha_ciclo,
             last_day(trunc(lrstart,'MONTH')) fecha_fin
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by 1 desc;

  BEGIN


         cuenta_mes := 12;
         lII:=0;
         lv_dia_cierre := to_char(pdFechCierrePeriodo,'dd');

         for i in cierres_ciclos LOOP

             if i.fecha_ciclo <= pdFechCierrePeriodo THEN

                 IF cuenta_mes < 1 then
                    nombre_campo := 'balance_1';
                    lII := 0;
                    for j in cur_cons_ciclos_cliente3(i.fecha_fin) loop
                      execute immediate 'update co_cuadre_borrar_ set '||
                                         nombre_campo || ' =  nvl(' || nombre_campo || ',0) + :1'||
                                        'where customer_id=:2'
                      using j.consumo, j.customer_id;
                      lII := lII + 1;
                      if lII = GN_COMMIT then
                        lII := 0;
                        commit;
                      end if;
                    end loop;
                    commit;
                    exit;
                 ELSE
                    nombre_campo := 'balance_'||cuenta_mes;
                    lII := 0;

                    for j in cur_cons_ciclos_cliente2(i.fecha_ciclo) loop
                       execute immediate 'update co_cuadre_borrar_ set '||
                                      nombre_campo || ' = nvl(' || nombre_campo || ',0) + :1'||
                                      'where customer_id=:2'
                       using j.consumo, j.customer_id;
                       lII := lII + 1;
                       if lII = GN_COMMIT then
                         lII := 0;
                         commit;
                       end if;
                    end loop;
                    commit;
                    IF cuenta_mes >= 1 THEN
                       Fecha:= lv_dia_cierre||'/'||to_char(i.fecha_ciclo, 'MM/yyyy');
                       execute immediate 'update co_cuadre_borrar_ set fecha_' ||
                                          cuenta_mes || ' = to_date(''' || Fecha || ''',''dd/MM/yyyy'')';

                       commit;
                    END IF;
                    cuenta_mes := cuenta_mes - 1;
                 END IF;
             END IF;
         END LOOP;
         COMMIT;
       --END IF;


    pd_lvMensErr := '';


  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := sqlerrm;

      COMMIT;

  END;


  --------------------------
  --   CalculaTotalDeuda
  --------------------------
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CalculaTotalDeuda',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lvSentencia := 'Update ' || pdNombre_tabla || ' set total_deuda = ';
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || ' BALANCE_' || lII || ' +';
    end loop;
    lvSentencia := substr(lvSentencia, 1, length(lvSentencia) - 1);

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    if lvMensErr is not null then
      raise leError;
    end if;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CalculaTotalDeuda',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;


  exception
    when leError then
      pd_lvMensErr := 'calculatotaldeuda: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CalculaTotalDeuda',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'calculatotaldeuda: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CalculaTotalDeuda',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End;

  --------------------------
  --   Copia_Fact_Actual
  --------------------------
  PROCEDURE Copia_Fact_Actual(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;
    --lnMesFinal number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Copia_Fact_Actual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lvSentencia := ' Update ' || pdNombre_tabla ||
                   ' t  set TOTAL_FACT_ACTUAL  = balance_12';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    if lvMensErr is not null then
      raise leError;
    end if;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Copia_Fact_Actual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  exception
    when leError then
      pd_lvMensErr := 'copia_fact_actual: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Copia_Fact_Actual',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'copia_fact_actual: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Copia_Fact_Actual',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End;

  ----------------------------------------------
  --   INIT_RECUPERADO
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2) is
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
  BEGIN
    lvSentencia := 'update ' || pdNombre_tabla ||
                   ' nologging set totPagos_recuperado = 0,' ||
                   ' CreditoRecuperado = 0, DebitoRecuperado = 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  END;

  PROCEDURE LLENA_PAGORECUPERADO2(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2) is
    -- variables
    lvMensErr      varchar2(2000);
    v_cursor       number;
    lvSentencia    varchar2(2000);
    lII            number;
    ldPeriodo      date;
    source_cursor  integer;
    rows_processed integer;
    rows_fetched   integer;
    lnCustomerId   number;
    lnValor        number;
    lnExisteTabla  number;

    -- cursor de pagos extraidos de la tabla online
    cursor /*+ rule*/ pagos is
      select customer_id, decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
        from cashreceipts_all
        where caentdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
         and catype in (1, 3, 9)
       group by customer_id;

  BEGIN

    ldperiodo := to_date('24/10/2005', 'dd/mm/yyyy');

    -- se valida la fecha de solicitud de cartera a recuperar
    if to_date(pvFechIni, 'dd/mm/yyyy') >= ldPeriodo then
      if to_date(pvFechFin, 'dd/mm/yyyy') >= ldPeriodo then
        lII := 0;
        for i in pagos loop
          execute immediate 'update ' || pdNombre_tabla ||
                            ' set totPagos_recuperado = :1 where customer_id = :2'
            using i.valor, i.customer_id;

          lII := lII + 1;
          if lII = GN_COMMIT then
            lII := 0;
            commit;
          end if;
        end loop;
      end if;
      commit;
    else
      begin
        lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CASHRECEIPTS_ALL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') || '''';
        source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
        rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

        if lnExisteTabla = 1 then
          lvSentencia := 'select /*+ rule*/ customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay))' ||
                         ' from cashreceipts_all_'||to_char(ldPeriodo, 'ddMMyyyy') ||
                         ' where caentdate >= to_date(''' || pvFechIni ||''', ''dd/mm/yyyy'')' ||
                         ' and catype <> ''2''' ||
                         ' and cachkdate >= to_date(''' || pvFechIni ||''', ''dd/mm/yyyy'')' ||
                         ' and cachkdate <= to_date(''' || pvFechFin ||''', ''dd/mm/yyyy'')' ||
                         ' group by customer_id';

          -- se toman los pagos de la ultima tabla de pagos respaldada
          source_cursor := DBMS_SQL.open_cursor;
          Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

          dbms_sql.define_column(source_cursor, 1, lnCustomerId);
          dbms_sql.define_column(source_cursor, 2, lnValor);
          rows_processed := Dbms_sql.execute(source_cursor);

          lII := 0;
          loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnValor);

            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using lnValor, lnCustomerId;
            lII := lII + 1;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end loop;
          dbms_sql.close_cursor(source_cursor);
          commit;
        else
          -- existe la posibilidad que no se encuentra la tabla
          -- respaldada de pagos entonces se toma de la online
          -- los datos, esto se da por tardanza en la facturacion
          -- ya que se encuentra un nuevo periodo en la orderhdr_all
          -- pero no se encuentran los datos que se requieren.
          lII := 0;
          for i in pagos loop
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using i.valor, i.customer_id;
            lII := lII + 1;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end loop;
          commit;
        end if;
      exception
        when others then
          lvMensErr := sqlerrm;
      end;
    end if;
  END;

  ----------------------------------------------
  --   LlenaPagoRecuperado
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2) is
    -- variables
    lvMensErr      varchar2(2000);
    v_cursor       number;
    lvSentencia    varchar2(2000);
    lII            number;
    ldPeriodo      date;
    source_cursor  integer;
    rows_processed integer;
    rows_fetched   integer;
    lnCustomerId   number;
    lnValor        number;
    lnExisteTabla  number;

    -- cursor de pagos extraidos de la tabla online
    cursor /*+ rule*/ pagos is
      select customer_id,
             decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
        from cashreceipts_all
       where cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
         and catype in (1, 3, 9)
       group by customer_id;

  BEGIN

    -- se busca el ultimo periodo vigente
    --SIS RCA 18/oct/2005: Eliminado para que tome el ciclo 1.
    /*select max(t.ohentdate)
     into ldPeriodo
     from orderhdr_all t
    where t.ohentdate is not null
      and t.ohstatus = 'IN';*/

    --quemado SIS RCABRERA!!! PARA QUE TOME EL CICLO 1!!!!
    --18/oct/2005
    --LUEGO DE LA CORRECCI�N, QUITAR!!
    ldperiodo := to_date('24/10/2005', 'dd/mm/yyyy');

    -- se valida la fecha de solicitud de cartera a recuperar
    if to_date(pvFechIni, 'dd/mm/yyyy') >= ldPeriodo then
      if to_date(pvFechFin, 'dd/mm/yyyy') >= ldPeriodo then
        lII := 0;
        for i in pagos loop
          execute immediate 'update ' || pdNombre_tabla ||
                            ' set totPagos_recuperado = :1 where customer_id = :2'
            using i.valor, i.customer_id;

          lII := lII + 1;
          if lII = GN_COMMIT then
            lII := 0;
            commit;
          end if;
        end loop;
      end if;
      commit;
    else
      begin
        lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CASHRECEIPTS_ALL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') || '''';
        source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
        rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

        if lnExisteTabla = 1 then
          lvSentencia := 'select  /*+ rule*/ customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay))' ||
                         ' from cashreceipts_all_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') ||
                         ' where catype <> ''2''' ||
                         ' and cachkdate >= to_date(''' || pvFechIni ||
                         ''', ''dd/mm/yyyy'')' ||
                         ' and cachkdate <= to_date(''' || pvFechFin ||
                         ''', ''dd/mm/yyyy'')' || ' group by customer_id';

          -- se toman los pagos de la ultima tabla de pagos respaldada
          source_cursor := DBMS_SQL.open_cursor;
          Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

          dbms_sql.define_column(source_cursor, 1, lnCustomerId);
          dbms_sql.define_column(source_cursor, 2, lnValor);
          rows_processed := Dbms_sql.execute(source_cursor);

          lII := 0;
          loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnValor);

            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using lnValor, lnCustomerId;
            lII := lII + 1;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end loop;
          dbms_sql.close_cursor(source_cursor);
          commit;
        else
          -- existe la posibilidad que no se encuentra la tabla
          -- respaldada de pagos entonces se toma de la online
          -- los datos, esto se da por tardanza en la facturacion
          -- ya que se encuentra un nuevo periodo en la orderhdr_all
          -- pero no se encuentran los datos que se requieren.
          lII := 0;
          for i in pagos loop
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using i.valor, i.customer_id;
            lII := lII + 1;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end loop;
          commit;
        end if;
      exception
        when others then
          lvMensErr := sqlerrm;
      end;
    end if;
  END;

  ------------------------------------------
  --    LLENA_CREDITORECUPERADO
  --    Funcion utilizada por forms
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;

    -------CREDITOS REGULARES
    cursor creditos is
      select customer_id id_cliente, ohinvamt_doc credito
        from orderhdr_all
       --where ohrefdate between
       where ohentdate BETWEEN  --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and to_char(ohrefdate, 'dd') <> '24'
        and substr(ohrefdate,1,2) <> substr(pvFechIni,1,2)
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount credito
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
        /* and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate between         --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and amount < 0
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id id_cliente, f.amount credito
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
         /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN     --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and f.amount < 0;

    /*=========================================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Modificado por : SUD. Reyna Asencio
     Motivo         : Obtener los creditos de los clientes que sean diferente
                      al dia de inicio de la fecha de facturacion
    ===========================================================================*/
    -------CREDITOS REGULARES
    cursor creditos2 is
      select  customer_id id_cliente, ohinvamt_doc credito
        from orderhdr_all
        where ohentdate BETWEEN  --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and substr(ohrefdate,1,2) <> substr(pvFechIni,1,2)
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select  cu.customer_id id_cliente, f.amount credito
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
       and f.entdate between         --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and amount < 0
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select  cu.customer_id id_cliente, f.amount credito
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
       and f.entdate BETWEEN     --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
        and cu.paymntresp is null
        and f.amount < 0;

  BEGIN

    lII := 0;
    for i in creditos2 loop
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set CreditoRecuperado = CreditoRecuperado + :1 where customer_id = :2'
        using i.credito, i.id_cliente;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;

    end loop;
    commit;

  END;


  PROCEDURE LLENA_CARGORECUPERADO2(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;
    lnCargo     number;

    cursor cuentas is
    select customer_id from ope_cuadre;

  BEGIN

    lII := 0;
    for i in cuentas loop
      update ope_cuadre set debitorecuperado = cof_extrae_cargos(i.customer_id, pvFechIni, pvFechFin)
      where customer_id = i.customer_id;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;

  END;

  ------------------------------------------
  --    LLENA_CARGORECUPERADO
  --    Funcion utilizada por forms
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2) is
    -- variables
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;

    cursor cargos is
    ---------CARGOS COMO OCCS NEGATIVOS CTAS PADRES
      select  cu.customer_id id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
         /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN   --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and f.amount > 0
         --and f.sncode <> 103 --cargos por facturaci�n --CBR 2006-05-19: para mejorar el rendimiento
      UNION ALL
      ---------CARGOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento
        from customer_all cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
         /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                     'dd/mm/yyyy hh24:mi') between
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN  --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and customer_id_high is not null
         and f.amount > 0;
         --and f.sncode <> 103; --cargos por facturaci�n --CBR 2006-05-19: para mejorar el rendimiento

  BEGIN

    lII := 0;
    for i in cargos loop
      if i.sncode <> 103 then --CBR 2006-05-19: para mejorar el rendimiento
      execute immediate 'update ' || pdNombre_tabla ||
                        ' set DebitoRecuperado = DebitoRecuperado + :1 where customer_id = :2'
        using i.cargo, i.id_cliente;
      end if;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;

  END;

  --------------------------
  --   Llena_total_deuda_cierre
  --------------------------
  PROCEDURE Llena_total_deuda_cierre(pdFechCierrePeriodo date,
                                     pdNombre_tabla      varchar2,
                                     pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);

  Begin

    lvSentencia := ' Update ' || pdNombre_tabla ||
                   ' t  set TOTAL_deuda_cierre  = total_deuda + total_fact_actual';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

  End;
  --------------------------
  --   LLENATIPO
  --------------------------

  PROCEDURE LLENATIPO(pdFechCierrePeriodo date, pdNombre_tabla varchar2) is

    lv_sentencia_upd varchar2(2000);
    lvMensErr        varchar2(2000);
    v_CursorUpdate   number;
    v_Row_Update     number;
    lnMesFinal       number;

    lII            number;
    lvSentencia    varchar2(2000);
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    pd_lvMensErr   VARCHAR2(200);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.LLENATIPO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- Son 5 tipos:
    --   = '1_NF'  Cartera vigente.
    --             Clientes que no tienen factura generada. Actualizado en LLENA BALANCE
    --   = '2_SF'  Se llenara cuando el tipo sea nulo... Pero debe ser realizado al final.
    --   = '3_VNE'. Cartera Vencida. Son valores Negativos o sea saldos a favor del cliente
    --   = '5_R'.   Esto es para los clientes que tienen Mora

    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id from ' || pdNombre_tabla ||
                     ' minus' ||
                     ' select customer_id from orderhdr_all where ohstatus = ''IN'' and  ohentdate = to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);

      -- actualizo los campos de la tabla final
      --lvSentencia := 'update ' || pdNombre_Tabla || ' set  tipo = ''1_NF''' ||
      --               ' where customer_id=' || lnCustomerId;
      --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set  tipo = ''1_NF''' ||
                        ' where customer_id = :1'
        using lnCustomerId;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

    -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
    --lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));
    lnMesFinal       := 12;
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''3_VNE''' || ' where  BALANCE_' ||
                        lnMesFinal || ' < 0  and  mora = 0 ';
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;

    -- Se actualiza campo tipo para saber que es Mora
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''5_R''' || ' where  mora > 0  and  BALANCE_' ||
                        lnMesFinal || ' >= 0 ';
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.LLENATIPO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'LLENATIPO: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar LLENATIPO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  ---------------------------------------------------------------------
  -- Llena_telefono_celular
  ---------------------------------------------------------------------

  Procedure Llena_telefono(pdNombreTabla varchar2,
                           pd_lvMensErr  out varchar2) is

    type t_cuenta   is table of number index by binary_integer;

    lv_telefono            varchar2(30);
    lv_customer            number:=0;
    lv_sentencia_upd       varchar2(2000);
    lvMensErr              varchar2(2000);
    lII                    number;
    LC_CUENTA              t_cuenta;

/*
   CURSOR CUR_TELEFONO (C_CUENTA VARCHAR2) IS
     select \*+ RULE +*\
       cu.customer_id, dn.dn_num
        from directory_number   dn,
             contr_services_cap cc,
             contract_all       co,
             customer_all       cu,
             contract_history   ch
       where co.customer_id = cu.customer_id
         and cc.co_id = co.co_id
         and cu.customer_id = C_CUENTA
         and cc.cs_deactiv_date is null
         and cc.seqno = (select max(cc1.seqno)
                           from contr_services_cap cc1
                          where cc1.co_id = cc.co_id)
         and dn.dn_id = cc.dn_id
         and ch.co_id = co.co_id
         and ch.ch_seqno = (select max(chl.ch_seqno)
                              from contract_history chl
                             where chl.co_id = ch.co_id)
       order by cu.customer_id, ch.ch_status;
*/

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Llena_telefono',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lv_telefono := 0;
    lII         := 0;
    LC_CUENTA.DELETE;
    --
    SELECT customer_id
    BULK COLLECT INTO LC_CUENTA
    FROM co_cuadre;
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FOR K IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
           lv_telefono:=0;
           --
           begin
             select /*+ RULE +*/ dn.dn_num
               into lv_telefono
               from directory_number   dn,
                    contr_services_cap cc,
                    contract_all       co,
                    customer_all       cu,
                    contract_history   ch
              where co.customer_id = cu.customer_id
                and cc.co_id = co.co_id
                and cu.customer_id = LC_CUENTA(K)
                and cc.cs_deactiv_date is null
                and cc.seqno = (select max(cc1.seqno)
                                  from contr_services_cap cc1
                                 where cc1.co_id = cc.co_id)
                and dn.dn_id = cc.dn_id
                and ch.co_id = co.co_id
                and ch.ch_seqno = (select max(chl.ch_seqno)
                                     from contract_history chl
                                    where chl.co_id = ch.co_id)
                and rownum <=1;
           exception
             when others then
               lv_telefono:=0;
           end;
           --
           IF lv_telefono <> 0 THEN
              UPDATE co_cuadre
              SET telefono = lv_telefono
              WHERE customer_id = LC_CUENTA(K);
              lII := lII + 1;
           END IF;
           --
           IF lII = GN_COMMIT THEN
              lII := 0;
              COMMIT;
           END IF;
       END LOOP;
    END IF;
    --
    LC_CUENTA.DELETE;
    COMMIT;

/*
   SELECT  C.Customer_Id BULK COLLECT INTO LC_CUENTA FROM CO_CUADRE C;

    IF LC_CUENTA.COUNT > 0 THEN
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
         FOR J IN CUR_TELEFONO (LC_CUENTA(I)) loop
           if nvl(lv_customer, 0) <> j.customer_id then
              execute immediate ' update ' || pdnombretabla ||
                                ' set telefono = :1' || ' where customer_id = :2'
                using J.dn_num, J.customer_id;
            lII := lII + 1;
            lv_customer := j.customer_id;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end if;
         END LOOP;
       END LOOP;
    END IF;
*/

  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  ln_registros_error_scp:=ln_registros_error_scp+1;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.Llena_telefono',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  exception
    when others then
      pd_lvMensErr := 'Llena_telefono: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Llena_telefono',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End Llena_telefono;

  Procedure LLENA_BUROCREDITO(pdNombreTabla varchar2,
                              pd_lvMensErr  out varchar2) is

    CURSOR CHK_BUROCREDITO IS
      SELECT I.CUSTOMER_ID, decode(I.CHECK01, 'X', 'S', I.CHECK01) check01
        FROM INFO_CUST_CHECK I
       ORDER BY I.customer_id;

    lv_customer number;
    ln_i        number := 0;


   type t_cuenta is table of number index by binary_integer;
   type t_CHECK is table of varchar2(10) index by binary_integer;
    LC_CUENTA              t_cuenta;
    LC_CHECK              t_CHECK;

  BEGIN

   --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
   --SCP:MENSAJE
   ----------------------------------------------------------------------
   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   ln_registros_error_scp:=ln_registros_error_scp+1;
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.LLENA_BUROCREDITO',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
   ----------------------------------------------------------------------------
   COMMIT;

   LC_CUENTA.DELETE;
   LC_CHECK.DELETE;

   SELECT I.CUSTOMER_ID, decode(I.CHECK01, 'X', 'S', I.CHECK01) check01
     BULK COLLECT INTO LC_CUENTA, LC_CHECK
      FROM INFO_CUST_CHECK I
     ORDER BY I.customer_id;

      IF LC_CUENTA.COUNT > 0 THEN
         --
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
        execute immediate ' update ' || pdnombretabla ||
                          ' set burocredito = :1' ||
                          ' where customer_id = :2'
          using LC_CHECK(I), LC_CUENTA(I);
          ln_i := ln_i + 1;
          if ln_i = GN_COMMIT then
            ln_i := 0;
            commit;
          end if;
         END LOOP;
      END IF;

   LC_CUENTA.DELETE;
   LC_CHECK.DELETE;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.LLENA_BUROCREDITO',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  exception
    when others then
      pd_lvMensErr := 'Llena_BuroCredito: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar LLENA_BUROCREDITO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  End;

  --------------------------------------------
  ---- Llena fecha maxima de pago  del cliente
  --------------------------------------------
  PROCEDURE LLENA_FECH_MAX_PAGO(pdFechCierrePeriodo in date,
                                pdNombre_Tabla      in varchar2,
                                pd_lvMensErr        out varchar2) is

    type t_cuenta is table of number index by binary_integer;
    type t_fecha  is table of date index by binary_integer;
    --
    LC_FECHA_MAX   t_fecha;
    LC_CUENTA      t_cuenta;
    --
    source_cursor  integer;
    lv_fecha_max   date;
    lv_fecha_max1  date;
    lvsentencia    varchar2(2000);
    lnCustomerId   number;
    rows_processed integer;
    lII            number;
    ln_termnet     number;
/*
    cursor cur_termcode is
      select termcode, termnet from terms_all;
*/

    BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.LLENA_FECH_MAX_PAGO',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    LC_CUENTA.DELETE;
    LC_FECHA_MAX.DELETE;
    --
    SELECT /*+ RULE +*/ c.customer_id, pdFechCierrePeriodo + t.termnet
    BULK COLLECT INTO LC_CUENTA, LC_FECHA_MAX
    FROM customer_all a, co_cuadre c, terms_all t
    WHERE a.customer_id = c.customer_id
    AND a.termcode = t.termcode;
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
       UPDATE co_cuadre
          SET fech_max_pago = LC_FECHA_MAX(I)
        WHERE customer_id = LC_CUENTA(I);
    END IF;

    LC_CUENTA.DELETE;
    LC_FECHA_MAX.DELETE;
    COMMIT;

/*
    for i in cur_termcode loop

      source_cursor := DBMS_SQL.open_cursor;
      --lvSentencia   := 'select customer_id from customer_all_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||' a'||
      lvSentencia := ' select \*+ RULE +*\ c.customer_id from customer_all a , ' ||pdNombre_Tabla||' c'||
                     ' where a.termcode = ' || i.termcode||
                     '   and a.customer_id = c.customer_id';

      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

      dbms_sql.define_column(source_cursor, 1, lnCustomerId);
      rows_processed := Dbms_sql.execute(source_cursor);
      lII            := 0;
      lv_fecha_max   := pdFechCierrePeriodo + i.termnet;
      loop
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnCustomerId);
        execute immediate 'update ' || pdNombre_Tabla ||
                          ' set fech_max_pago = to_date(''' ||
                          to_char(lv_fecha_max, 'dd/MM/yyyy') ||
                          ''',''dd/MM/yyyy'') where customer_id = :1'
          using lnCustomerId;

        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;
      end loop;
    end loop;
    commit;
*/
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  ln_registros_error_scp:=ln_registros_error_scp+1;
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.LLENA_FECH_MAX_PAGO',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'LLENA_FECH_MAX_PAGO: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar LLENA_FECH_MAX_PAGO',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END LLENA_FECH_MAX_PAGO;

  ---------------------------
  -- PROCESA_CUADRE
  ---------------------------
  PROCEDURE PROCESA_CUADRE(pdFechaPeriodo       in date,
                           pdNombre_Tabla       in varchar2,
                           pdTabla_OrderHdr     in varchar2,
                           pdTabla_cashreceipts in varchar2,
                           pdTabla_Customer     in varchar2,
                           pd_lvMensErr         out varchar2) is

    --variables
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    Saldo       number;
    vBalance    number;
    vDISPONIBLE number;
    leError exception;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.PROCESA_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- se insertan todos los cliente a la base de datos de la base de datos de producci�n
    COK_PROCESS_REPORT.InsertaDatosCustomer(pdFechaPeriodo,
                                            pdNombre_tabla,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- procedimiento que actualiza los campos balance de saldos
    COK_PROCESS_REPORT.llena_balances(pdFechaPeriodo,
                                      pdNombre_Tabla,
                                      pdTabla_OrderHdr,
                                      lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- se actualizan los valores de pago y el disponible
    COK_PROCESS_REPORT.Upd_Disponible_TotPago(pdFechaPeriodo,
                                              pdNombre_tabla,
                                              pdTabla_Cashreceipts,
                                              pdTabla_OrderHdr,
                                              lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- Creditos del 24 se colocan en disponible
    COK_PROCESS_REPORT.Credito_a_Disponible(pdFechaPeriodo,
                                            pdNombre_Tabla,
                                            --pdTabla_Cashreceipts,
                                            --pdTabla_OrderHdr,
                                            --pdTabla_Customer,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- Proceso que realiza el balanceo
    COK_PROCESS_REPORT.balanceo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- Llena campo Total_deuda
    COK_PROCESS_REPORT.CalculaTotalDeuda(pdFechaPeriodo,
                                         pdNombre_Tabla,
                                         lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- copia el valor de la factura del ultimos mes al campo "Valor_Fact_Actual"
    COK_PROCESS_REPORT.Copia_Fact_Actual(pdFechaPeriodo,
                                         pdNombre_Tabla,
                                         lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- Proceso que calcula la mora
    COK_PROCESS_REPORT.calcula_mora;

    -- Proceso que realiza LlenaTipo
    COK_PROCESS_REPORT.LlenaTipo(pdFechaPeriodo, pdNombre_tabla);

    -- ACTUALIZA LOS DATOS INFORMATIVOS
    COK_PROCESS_REPORT.llena_DatosCliente(pdNombre_Tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- se actualiza la forma de pago
    COK_PROCESS_REPORT.Llena_NroFactura(pdFechaPeriodo, pdNombre_tabla);

    -- se actualizan la forma de pago, el producto y centro de costo
    COK_PROCESS_REPORT.UPd_FPago_Prod_CCosto(pdFechaPeriodo,
                                             pdNombre_tabla,
                                             lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    -- se actualizan los valores para tipo de forma de pago. Seg�n CBILL para convertir
    COK_PROCESS_REPORT.Conv_Cbill_TipoFPago(pdFechaPeriodo,
                                            pdNombre_tabla,
                                            lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

   -- se actualizan los valores para telefono. segun tabla directory number
    COK_PROCESS_REPORT.llena_telefono(pdNombre_Tabla, lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.SETEA_PLAN_IDEAL(lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    --agrega indicador si el CustomerID tiene Buro de Credito asignado
    COK_PROCESS_REPORT.LLENA_BUROCREDITO(pdNombreTabla => pdNombre_Tabla,
                                         pd_lvMensErr  => lvMensErr);

    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.LLENA_FECH_MAX_PAGO(pdfechaperiodo,
                                           pdNombre_Tabla,
                                           pd_lvMensErr);

    if lvMensErr is not null then
      raise leError;
    end if;

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.PROCESA_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PROCESA_CUADRE',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'procesa_cuadre: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PROCESA_CUADRE',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END PROCESA_CUADRE;

  ---------------------------
  -- SET_CAMPOS_MORA
  ---------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2 IS
    lvSentencia varchar2(2000);
    lnMesFinal  number;
  BEGIN
    lnMesFinal  := to_number(to_char(pdFecha, 'MM'));
    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  END SET_CAMPOS_MORA;

  -----------------
  --  CREA_TABLA
  -----------------
  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER IS

    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CREA_TABLA_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura
                       COK_PROCESS_REPORT.set_campos_mora(pdFechaPeriodo) ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0)'||
                       '  tablespace DATA' ||
                       '  pctfree 10' || '  pctused 40' || '  initrans 1' ||
                       '  maxtrans 255' || '  storage' || '  (initial 256K' ||
                       '    next 256K' || '    minextents 1' ||
                       '    maxextents unlimited' || '    pctincrease 0)'; --JHE 28-03-2007 se cambio Unlimited
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea clave primaria
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PKCUSTOMER_ID' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                       '  primary key (CUSTOMER_ID)' || '  using index' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_CUST_IDX on CO_CUADRE (CUSTCODE)' ||
                       'tablespace IND' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    --  Crea indice
    lvSentenciaCrea := 'create index ID_CUSTOMER_ID_' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (CUSTOMER_ID, tipo)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CREA_TABLA_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 1;

  EXCEPTION
    when others THEN
      lvMensErr := sqlerrm;
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA_CUADRE',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;

  END CREA_TABLA_CUADRE;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2) IS

    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    -- variables
    lvSentencia            VARCHAR2(2000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    lvMensErr              VARCHAR2(2000) := null;
    lnExito                NUMBER;
    lv_nombre_tabla        varchar2(50);
    lv_Tabla_OrderHdr      varchar2(50);
    lv_Tabla_cashreceipts  varchar2(50);
    lv_Tabla_customer      varchar2(50);
    lv_TablaCustomerAllAnt varchar2(50);
    lnMes                  number;
    leError exception;

  BEGIN

       lvSentencia := 'grant select , references, index  on co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ') || '  to wfrec ';

       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    --CONTROL DE EJECUCION
    OPEN C_VERIFICA_CONTROL ('COK_PROCESS_REPORT.MAIN');
    FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
    LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
    CLOSE C_VERIFICA_CONTROL;

    IF LB_FOUND_CONTROL THEN
       UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
       WHERE TIPO_PROCESO='COK_PROCESS_REPORT.MAIN';
    ELSE
       INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
       VALUES ('COK_PROCESS_REPORT.MAIN',SYSDATE,'A');
    END IF;



    --

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    select valor into lv_nombre_tabla from co_parametros_cliente where campo = 'TABLA_PROCESO';
    select valor into lv_Tabla_OrderHdr from co_parametros_cliente where campo = 'TABLA_ORDERHDR';
    select valor into lv_Tabla_cashreceipts from co_parametros_cliente where campo = 'TABLA_PAGOS';
    select valor into lv_Tabla_customer from co_parametros_cliente where campo = 'TABLA_CLIENTE';
    select valor into lv_TablaCustomerAllAnt from co_parametros_cliente where campo = 'TABLA_CLIENTE_ANT';


    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''' ||
                     lv_nombre_tabla || '''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

    if lnExisteTabla = 1 then
      -- se trunca la tabla
      lvSentencia := 'truncate table ' || lv_nombre_tabla;
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;
    else
      --se crea la tabla
      lnExito := COK_PROCESS_REPORT.crea_tabla_cuadre(pdFechCierrePeriodo,
                                                      lvMensErr,
                                                      lv_nombre_tabla);
      if lvMensErr is not null then
        raise leError;
      end if;
    end if;

    -- se llama al procedure que procesa el cuadre de la tabla
    COK_PROCESS_REPORT.procesa_cuadre(pdFechCierrePeriodo,
                                      lv_nombre_tabla,
                                      lv_Tabla_OrderHdr,
                                      lv_Tabla_cashreceipts,
                                      lv_Tabla_customer,
                                      lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;


    COK_PROCESS_REPORT.actualiza_pagosper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lv_Tabla_cashreceipts,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_credtper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_cmper(pdFechCierrePeriodo,
                                       lv_nombre_tabla,
                                       lv_Tabla_OrderHdr,
                                       lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_consmper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_descuento(pdFechCierrePeriodo,
                                           lv_nombre_tabla,
                                           lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.CUADRA;

    -- se llama a procedimiento de calculo de edad real de mora
    COK_PROCESS_REPORT.CO_EDAD_REAL(lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    --[2702] ECA
    --Se actualiza la tabla co_cuadre_mensual
    COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE_MENSUAL(pdFechCierrePeriodo,lvMensErr);

    --CONTROL DE EJECUCION
    UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_PROCESS_REPORT.MAIN';
    COMMIT;
    --
    if lvMensErr is not null then
      raise leError;
    end if;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_PROCESS_REPORT.MAIN',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END MAIN;

--===============================================================================================-

  PROCEDURE ACTUALIZA_CO_CUADRE (PN_PROCESADOS IN OUT NUMBER, PV_ERROR IN OUT VARCHAR2) IS

  -- procedimiento que actualiza las nuevas
  -- cuentas que se hayan activado en la tabla
  -- temporal co_cuadre que sera usada con join
  -- de extraccion de informacion de plan ideal

  TYPE CuentasNuevas IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TAB_CuentasNuevas CuentasNuevas;

  lv_error       varchar2(5000):=null;
  ln_exito       number:=0;
  ln_contador    number:=0;
  lb_found       boolean:=false;
  lb_flag        boolean:=false;
  le_error       exception;

  cursor c_plan_ideal (cn_customer number) is
      select 1
      from rateplan rt, contract_all co, curr_co_status cr, customer_all cu
      where rt.tmcode = co.tmcode
      and co.co_id = cr.co_id
      and co.customer_id = cu.customer_id
      and cr.ch_status = 'a'
      and rt.tmcode >= 452
      and rt.tmcode <= 467
      and cu.customer_id = cn_customer;

  cursor c_cuentas is
      select cu.customer_id
      from customer_all cu
      where cu.paymntresp = 'X'
      and not exists (select 1
                      from co_cuadre co
                      where co.customer_id = cu.customer_id);

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;

    --[2702] ECA - SCP
    ln_total_registros_scp number:=0;
    lv_id_proceso_scp varchar2(100):='COK_PROCESS_REPORT';
    lv_referencia_scp varchar2(100):='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
    lv_unidad_registro_scp varchar2(30):='Cuentas';
    ln_id_bitacora_scp number:=0;
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_error_scp number:=0;
    ln_registros_procesados_scp number:=0;

  BEGIN

      --CONTROL DE EJECUCION
      OPEN C_VERIFICA_CONTROL ('COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE');
      FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
      LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
      CLOSE C_VERIFICA_CONTROL;

      IF LB_FOUND_CONTROL THEN
         UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
         WHERE TIPO_PROCESO='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
      ELSE
         INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
         VALUES ('COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',SYSDATE,'A');
      END IF;
      --
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:INICIO
      ----------------------------------------------------------------------------
      -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      if ln_error_scp <>0 then
         return;
      end if;

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',lv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      TAB_CuentasNuevas.DELETE;
      --
      ln_contador:=0;
      lv_error:=null;
      --
      begin
        OPEN c_cuentas;
        LOOP
          FETCH c_cuentas BULK COLLECT INTO TAB_CuentasNuevas;
          EXIT WHEN c_cuentas%NOTFOUND;
        END LOOP;
        CLOSE c_cuentas;
      exception
        when others then
          lv_error:='ERROR AL OBTENER CUENTAS DE CUSTOMER_ALL QUE FALTAN EN CO_CUADRE: '||SQLERRM;
          raise le_error;
      end;
      --
      IF TAB_CuentasNuevas.COUNT > 0 THEN
         FOR k in TAB_CuentasNuevas.first..TAB_CuentasNuevas.last LOOP
            BEGIN
        			 lb_found:=false;
               lb_flag:=false;
               ln_exito:=0;
               --
               open c_plan_ideal(TAB_CuentasNuevas(k));
               fetch c_plan_ideal into ln_exito;
                     lb_found:=c_plan_ideal%found;
               close c_plan_ideal;
               --
               if not lb_found then
                  ln_exito:=0;
               end if;
               --
            	 if ln_exito = 1 then
        	    		begin
                    insert into co_cuadre (customer_id, plan)
          	    		values (TAB_CuentasNuevas(k), 'IDEAL');
                    --
                    if SQL%ROWCOUNT > 0 then
                       lb_flag:=true;
                    else
                       lb_flag:=false;
                    end if;
                  exception
                    when others then
                      lb_flag:=false;
                  end;
            	 else
        	    		begin
                    insert into co_cuadre (customer_id)
          	    		values (TAB_CuentasNuevas(k));
                    --
                    if SQL%ROWCOUNT > 0 then
                       lb_flag:=true;
                    else
                       lb_flag:=false;
                    end if;
                  exception
                    when others then
                      lb_flag:=false;
                  end;
            	 end if;
               --
               if lb_flag then
                  ln_contador:=ln_contador+1;
               end if;
               --
               if mod(ln_contador, GN_COMMIT) = 0 then
                  commit;
           	   end if;
            EXCEPTION
              WHEN OTHERS THEN
                null;
            END;
         END LOOP;
      END IF;
      --
      TAB_CuentasNuevas.DELETE;
      --
      IF ln_contador > 0 THEN
         commit;
      END IF;
      --
      PV_ERROR:=null;
      PN_PROCESADOS:=ln_contador;

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',lv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --CONTROL DE EJECUCION
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
      COMMIT;
      --

  EXCEPTION
    WHEN le_error THEN
      TAB_CuentasNuevas.DELETE;
      PN_PROCESADOS:=ln_contador;
      PV_ERROR:=lv_error;
    WHEN OTHERS THEN
      TAB_CuentasNuevas.DELETE;
      PN_PROCESADOS:=ln_contador;
      PV_ERROR:='ERROR ACTUALIZA_CO_CUADRE: '||SQLERRM;

  END ACTUALIZA_CO_CUADRE;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 29/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Crear tabla mensual para la co_cuadre
  =========================================================*/
  FUNCTION CREA_TABLA_CUADRE_MENSUAL(pdFechaPeriodo in date,
                                     pv_error       out varchar2,
                                     pvNombreTabla  in varchar2) RETURN NUMBER IS

    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CREA_TABLA_CUADRE_MENSUAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  ID_CICLO            VARCHAR2(2),' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura
                       COK_PROCESS_REPORT.set_campos_mora(pdFechaPeriodo) ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0)'||
                       '  tablespace DATA' ||
                       '  pctfree 10' || '  pctused 40' || '  initrans 1' ||
                       '  maxtrans 255' || '  storage' || '  (initial 256K' ||
                       '    next 256K' || '    minextents 1' ||
                       '    maxextents unlimited' || '    pctincrease 0)'; --JHE 28-03-2007 se cambio Unlimited
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea clave primaria
    -- ECA / Como la tabla es mensual la clave primaria debe ser el customer_id y el ciclo
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PK_CUST_CICLO_ID' ||
                       '  primary key (CUSTOMER_ID,ID_CICLO)' || '  using index' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea indices
    lvSentenciaCrea := 'create index CO_REG_IDX on '|| pvNombreTabla ||'(REGION)' ||
                       'tablespace IND ' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_PLAN_IDX on '|| pvNombreTabla ||'(PLAN)' ||
                       'tablespace IND ' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_PROD_IDX on '|| pvNombreTabla ||'(PRODUCTO)' ||
                       'tablespace IND ' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_CICLO_IDX on ' ||
                       pvNombreTabla || ' (ID_CICLO)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_TIPO_IDX on ' ||
                       pvNombreTabla || ' (TIPO)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_MORA_IDX on ' ||
                       pvNombreTabla || ' (MORA)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea Sinonimo
    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea Permiso Publico
    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CREA_TABLA_CUADRE_MENSUAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 1;

  EXCEPTION
    when others THEN
      lvMensErr := sqlerrm;
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA_CUADRE_MENSUAL',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;

  END CREA_TABLA_CUADRE_MENSUAL;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 29/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Actualiza la co_cuadre_mensual con el
                      ciclo actual procesado para la co_cuadre
  =========================================================*/
  PROCEDURE ACTUALIZA_CO_CUADRE_MENSUAL(pdFechaPeriodo in date,
                                        pv_error       out VARCHAR2) IS

  --VARIABLES
  lvSentencia            VARCHAR2(2000);
  lvMensErr              VARCHAR2(2000) := null;
  lv_nombre_tabla        VARCHAR2(20):='CO_CUADRE_MENSUAL';
  lv_ciclo               VARCHAR2(2);
  source_cursor          INTEGER;
  rows_processed         INTEGER;
  rows_fetched           INTEGER;
  lnExisteTabla          NUMBER:=0;
  lnExito                NUMBER;
  lvFechaPeriodo         DATE:=pdFechaPeriodo;
  leError                EXCEPTION;

  BEGIN
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE_MENSUAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

       --Obtengo el codigo de ciclo de facturacion
       lv_ciclo:=get_ciclo(lvFechaPeriodo);

       --Verifico si la tabla co_cuadre_mensual existe
       lvSentencia   := 'select count(*) from user_all_tables where table_name = '''||lv_nombre_tabla||'''';
       source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
       dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
       dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
       rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
       rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
       dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
       dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

       --Solo Si no existe la tabla se la crea
       IF lnExisteTabla=0 THEN
           lnExito := COK_PROCESS_REPORT.crea_tabla_cuadre_mensual(pdFechaPeriodo,
                                                                       lvMensErr,
                                                                       lv_nombre_tabla);
           if lvMensErr is not null then
             raise leError;
           end if;
       END IF;

       --Borro la informacion del ciclo actual en la co_cuadre_mensual
       lvSentencia := 'delete from '|| lv_nombre_tabla ||' where id_ciclo='''||lv_ciclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;
       commit;

       --Inserto la informacion del ciclo actual de la co_cuadre en la co_cuadre_mensual
       lvSentencia := 'INSERT /*+ APPEND*/ into CO_CUADRE_MENSUAL NOLOGGING '||
                      '(custcode, customer_id, id_ciclo, region, provincia, canton, producto, nombres, apellidos, ruc,'||
                      'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                      'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                      'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                      'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                      'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                      'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                      'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                      'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                      'fecha_12, fech_max_pago, mora_real, mora_real_mig )' ||
                      'SELECT custcode, customer_id,'''||lv_ciclo||''', region, provincia, canton, producto, nombres, apellidos, ruc,'||
                      'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                      'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                      'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                      'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                      'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                      'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                      'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                      'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                      'fecha_12, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE where custcode is not null ';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;
       commit;
       pv_error := 'Proceso terminado con Exito';
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE_MENSUAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       commit;

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_CO_CUADRE_MENSUAL',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pv_error := 'ACTUALIZA_CO_CUADRE_MENSUAL: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ACTUALIZA_CO_CUADRE_MENSUAL',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 29/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Funcion para obtener el ciclo de
                      facturacion en base a una fecha
  =========================================================*/
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2 IS

  CURSOR C_CICLO IS
    SELECT ID_CICLO
      FROM FA_CICLOS_BSCS
     WHERE DIA_INI_CICLO = TO_CHAR(PD_FECHA, 'DD');

  LV_CICLO     VARCHAR2(2);

  BEGIN
       OPEN C_CICLO;
       FETCH C_CICLO INTO LV_CICLO;
       CLOSE C_CICLO;
       RETURN (LV_CICLO);
  END;

end COK_PROCESS_REPORT_BORRAR;
/
