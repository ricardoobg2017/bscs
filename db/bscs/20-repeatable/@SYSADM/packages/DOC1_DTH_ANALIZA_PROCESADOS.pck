CREATE OR REPLACE PACKAGE DOC1_DTH_ANALIZA_PROCESADOS IS
/*************************************************************************************
  DESARROLLADO POR  : SUD SILVIA STEFANIA REMACHE Y.
  LIDER PROYECTO    : SIS PAOLA CARVAJAL
  LIDER PDS         : SUD CRISTHIAN ACOSTA CHAMBERS
  FECHA             : 22/04/2013
  PROYECTO          : VENTA DTH POSTPAGO
  PROP�SITO         : LLEVAR UN CONTROL DE LAS CUENTAS DTH POSTPAGADAS       
**************************************************************************************/
  PROCEDURE DOC1_INSERT_CUENTA_POSTPAGO(PD_FECHA_CORTE DATE,
                                        PN_HILO        NUMBER,
                                        PN_CICLO       NUMBER,
                                        PV_ERROR       OUT VARCHAR2);

  /*ESTE PROCEDIMIENTO REALIZA EL INSERT EN LA TABLA CO_FACT_POST_(DDMMYYY)*/
  PROCEDURE DOC1_INSERT_CUENTA_DTH(PI_CO_ID          INTEGER,
                                   PI_CUSTOMER_ID    INTEGER,
                                   PV_SHDES          VARCHAR2,
                                   PI_SNCODE         INTEGER,
                                   PN_VALOR          NUMBER,
                                   PD_FECHA_CORTE    DATE,
                                   PD_FECHA_INGRESO  DATE,
                                   PN_HILO           NUMBER,
                                   PN_CICLO          NUMBER,
                                   PV_NUMERO_FACTURA VARCHAR2,
                                   PV_ERROR          OUT VARCHAR2);

  /*ESTE PROCEDIMIENTO REALIZA EL INSERT EN LA TABLA CO_FACT_POST_HIST*/  
  PROCEDURE DOC1_INSERT_CUENTA_HIST(PI_CO_ID          INTEGER,
                                    PI_CUSTOMER_ID    INTEGER,
                                    PV_SHDES          VARCHAR2,
                                    PI_SNCODE         NUMBER,
                                    PN_VALOR          NUMBER,
                                    PD_FECHA_CORTE    DATE,
                                    PD_FECHA_INGRESO  DATE,
                                    PN_HILO           NUMBER,
                                    PN_CICLO          NUMBER,
                                    PV_NUMERO_FACTURA VARCHAR2,
                                    PV_ERROR          OUT VARCHAR2);

  /*ESTE PROCEDIMIENTO REALIZA LA ELIMINACION DE TODOS LOS REGISTROS PROCESADOS DEL
  CICLO EN CURSO DE LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP*/
  PROCEDURE DOC1_DELETE_CUENTA_TMP(PD_FECHA_CORTE DATE,
                                   PN_HILO        NUMBER,
                                   PV_ESTADO      VARCHAR2,
                                   PV_ERROR       OUT VARCHAR2);
  /*SI EXISTEN DATOS IGUALES EN LA TABLA DE CO_FACT_POST_(DDMMYYYY) SE LOS ELIMINARAN DE LA MISMA TABLA*/
  PROCEDURE DOC1_DELETE_CUENTA_INACTIVA(PI_CO_ID       INTEGER,
                                        PI_CUSTOMER    INTEGER,
                                        PV_SHDES       VARCHAR2,
                                        PI_SNCODE      INTEGER,
                                        PD_FECHA_CORTE DATE,
                                        PN_HILO        INTEGER,
                                        PN_CICLO       INTEGER,
                                        PV_ERROR       OUT VARCHAR2);
 /*PROCESO QUE ACTUALIZA LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP EN CASO QUE EL REGISTRO ANALIZADO SEA 
 PROCESADO SE ACTUALIZARA CON ESTADO (I) Y SI OCURRIO ALGUN ERROR EL ESTADO (E) Y SE REGISTRARA EL ERROR*/
  PROCEDURE DOC1_UPDATE_CUENTA_TMP(PI_CO_ID       INTEGER,
                                   PI_CUSTOMER_ID INTEGER,
                                   PV_SHDES       VARCHAR2,
                                   PD_FECHA_CORTE DATE,
                                   PN_HILO        IN NUMBER,
                                   PN_CICLO       IN NUMBER,
                                   PV_ESTADO      IN VARCHAR2,
                                   PV_ERROR_TMP   IN VARCHAR2,
                                   PV_ERROR       OUT VARCHAR2);                                                                              
END DOC1_DTH_ANALIZA_PROCESADOS;
/
CREATE OR REPLACE PACKAGE BODY DOC1_DTH_ANALIZA_PROCESADOS IS
/*************************************************************************************
  DESARROLLADO POR  : SUD SILVIA STEFANIA REMACHE Y.
  LIDER PROYECTO    : SIS PAOLA CARVAJAL
  LIDER PDS         : SUD CRISTHIAN ACOSTA CHAMBERS
  FECHA             : 22/04/2013
  PROYECTO          : VENTA DTH POSTPAGO
  PROP�SITO         : LLEVAR UN CONTROL DE LAS CUENTAS DTH POSTPAGADAS       
**************************************************************************************/
  PROCEDURE DOC1_INSERT_CUENTA_POSTPAGO(PD_FECHA_CORTE DATE,
                                        PN_HILO        NUMBER,
                                        PN_CICLO       NUMBER,
                                        PV_ERROR       OUT VARCHAR2) IS
  
    /*OBTIENE LOS DATOS CARGADOS DE LA TABLA TEMPORAL*/  
    CURSOR C_CUENTAS_POSTPAGO_TMP(CD_FECHA_CORTE DATE, C_HILO NUMBER, C_CICLO NUMBER) IS
      SELECT T.* 
        FROM DOC1_SERVICIOS_POSTPAGO_TMP T
       WHERE T.FECHA_CORTE = CD_FECHA_CORTE
         AND T.HILO        = C_HILO
         AND T.CICLO       = C_CICLO
         AND T.STATUS      = 'A';  
    /*OBTIENE LOS SNCODE DE LA TABLA MPUSNTAB*/
    CURSOR C_EXTRAE_SNCODE(CV_SHDES VARCHAR2) IS
      SELECT M.SNCODE 
        FROM MPUSNTAB M
       WHERE M.SHDES = CV_SHDES; 
       
    LI_CO_ID           DOC1_SERVICIOS_POSTPAGO_TMP.CO_ID%TYPE;
    LI_CUSTOMER_ID     DOC1_SERVICIOS_POSTPAGO_TMP.CUSTOMER_ID%TYPE;
    LV_SHDES_1         DOC1_SERVICIOS_POSTPAGO_TMP.SHDES%TYPE;
    LI_SNCODE_1        CO_FACT_POST_HIST.SNCODE%TYPE;
    LN_VALOR           DOC1_SERVICIOS_POSTPAGO_TMP.VALOR%TYPE;
    LD_FECHA_CORTE     DOC1_SERVICIOS_POSTPAGO_TMP.FECHA_INGRESO%TYPE;
    LV_ESTADO          DOC1_SERVICIOS_POSTPAGO_TMP.STATUS%TYPE;
    LD_FECHA_INGRESO   DOC1_SERVICIOS_POSTPAGO_TMP.FECHA_INGRESO%TYPE;
    LN_HILO            DOC1_SERVICIOS_POSTPAGO_TMP.HILO%TYPE;
    LN_CICLO           DOC1_SERVICIOS_POSTPAGO_TMP.CICLO%TYPE;
    LV_NUMERO_FACTURA  DOC1_SERVICIOS_POSTPAGO_TMP.NO_FACTURA%TYPE;
     
    TYPE RC_CO_FACT_POST IS REF CURSOR;
     C_CO_FACT_POST RC_CO_FACT_POST;
                   
    LN_CUENTA        NUMBER         :=0;
    LI_SNCODE        INTEGER;
    LE_ERROR         EXCEPTION;
    LV_ERROR         VARCHAR2(4000);
    LV_PROCESO       VARCHAR2(80)   :='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_INSERT_CUENTA_POSTPAGO';
    LV_SENTENCIA_SQL VARCHAR2(6000) :=NULL;
    LB_FOUND         BOOLEAN        :=FALSE;
    PV_SHDES         VARCHAR2(10);
    
  BEGIN
        
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
        
    IF PD_FECHA_CORTE IS NULL THEN
      LV_ERROR := LV_PROCESO || ' NO SE ENCUENTRA LA FECHA DE CORTE';
      RAISE LE_ERROR;
    END IF;
        
    IF PN_HILO IS NULL THEN
      LV_ERROR := LV_PROCESO || ' NO SE ENCUENTRA EL NUMERO DE HILO';
      RAISE LE_ERROR;
    END IF;
              
    IF PN_CICLO IS NULL THEN
      LV_ERROR := LV_PROCESO ||' NO SE ENCUENTRA EL NUMERO DE CICLO';
      RAISE LE_ERROR;
    END IF;
                                                                             
    FOR I IN C_CUENTAS_POSTPAGO_TMP(PD_FECHA_CORTE,PN_HILO, PN_CICLO) LOOP                                        
           
       BEGIN      
         SAVEPOINT VALIDA_ERROR;                         
         LB_FOUND  := FALSE;
         LN_CUENTA := 0;
         PV_SHDES  := SUBSTR(I.SHDES,3,LENGTH(I.SHDES));                                                                                

         /* EXTRAE EL VALOR DEL SNCODE DE LA TABLA MPUSNTAB FILTRANDO POR EL SHDES */
         OPEN  C_EXTRAE_SNCODE(PV_SHDES);
         FETCH C_EXTRAE_SNCODE INTO LI_SNCODE;
         CLOSE C_EXTRAE_SNCODE;
         
         --REALIZAMOS EL SELECT DE LA TABLA CO_FACT_POST_DDMMYYYY PARA SABER SI EXISTEN DATOS IGUALES
         LV_SENTENCIA_SQL:=' SELECT A.CO_ID,
                             A.CUSTOMER_ID,
                             A.SHDES,
                             A.SNCODE,
                             A.VALOR,
                             A.FECHA_CORTE,
                             A.ESTADO,
                             A.FECHA_INGRESO,
                             A.HILO,
                             A.CICLO,
                             A.NUMERO_FACTURA '||
                           ' FROM CO_FACT_POST_'||TO_CHAR(PD_FECHA_CORTE,'DDMMYYYY')||' A '||
                           ' WHERE A.CO_ID        = '||I.CO_ID||
                           ' AND A.CUSTOMER_ID    = '||I.CUSTOMER_ID||
                           ' AND A.SHDES          = '''||PV_SHDES||''''||
                           ' AND A.SNCODE         = '||LI_SNCODE||
                           ' AND A.FECHA_CORTE    = TO_DATE('''||TO_CHAR(I.FECHA_CORTE,'DD/MM/YYYY')||''',''DD/MM/YYYY'')'||                          
                           ' AND A.ESTADO         = ''A'' '||
                           ' AND A.HILO           = '||I.HILO||
                           ' AND A.CICLO          = '||I.CICLO||
                           ' AND A.NUMERO_FACTURA = '''||I.NO_FACTURA||'''';                       
            
         OPEN C_CO_FACT_POST FOR LV_SENTENCIA_SQL;
           LOOP        
             -- SALIDA
             EXIT WHEN C_CO_FACT_POST%NOTFOUND = TRUE;
             LB_FOUND := C_CO_FACT_POST%FOUND;
             FETCH C_CO_FACT_POST INTO 
             LI_CO_ID,
             LI_CUSTOMER_ID,
             LV_SHDES_1,     
             LI_SNCODE_1,         
             LN_VALOR,          
             LD_FECHA_CORTE,
             LV_ESTADO,     
             LD_FECHA_INGRESO,
             LN_HILO,   
             LN_CICLO,          
             LV_NUMERO_FACTURA; 
             --EN CASO QUE EXISTAN REGISTRO IGUALES 
             IF LB_FOUND THEN
               --SE INSERTA EN LA TABLA CO_FACT_POST_HIST 
               DOC1_DTH_ANALIZA_PROCESADOS.DOC1_INSERT_CUENTA_HIST(PI_CO_ID         => LI_CO_ID,
                                                                   PI_CUSTOMER_ID    => LI_CUSTOMER_ID,
                                                                   PV_SHDES          => LV_SHDES_1,
                                                                   PI_SNCODE         => LI_SNCODE_1,
                                                                   PN_VALOR          => LN_VALOR,
                                                                   PD_FECHA_CORTE    => LD_FECHA_CORTE, 
                                                                   PD_FECHA_INGRESO  => LD_FECHA_INGRESO,
                                                                   PN_HILO           => LN_HILO,
                                                                   PN_CICLO          => LN_CICLO,
                                                                   PV_NUMERO_FACTURA => LV_NUMERO_FACTURA,
                                                                   PV_ERROR          => LV_ERROR);       
               IF LV_ERROR IS NOT NULL THEN
                 RAISE LE_ERROR;
               END IF;

               --BORRA EN LA TABLA CO_FACT_POST_(DDMMYYYY) EL REGISTRO CON LA MISMA CONSULTA
               DOC1_DTH_ANALIZA_PROCESADOS.DOC1_DELETE_CUENTA_INACTIVA(PI_CO_ID       => LI_CO_ID,
                                                                       PI_CUSTOMER    => LI_CUSTOMER_ID,
                                                                       PV_SHDES       => LV_SHDES_1,
                                                                       PI_SNCODE      => LI_SNCODE_1,
                                                                       PD_FECHA_CORTE => LD_FECHA_CORTE,
                                                                       PN_HILO        => LN_HILO,
                                                                       PN_CICLO       => LN_CICLO,
                                                                       PV_ERROR       => LV_ERROR);                                               
               IF LV_ERROR IS NOT NULL THEN
                 RAISE LE_ERROR;
               END IF;                        
             END IF;                           
           END LOOP;                                     
         CLOSE C_CO_FACT_POST;           
  
         -- INSERTA LAS NUEVAS CUENTAS POSPAGO EN LA TABLA CO_FACT_POST_(DDMMYYYY)
         DOC1_DTH_ANALIZA_PROCESADOS.DOC1_INSERT_CUENTA_DTH(PI_CO_ID          => I.CO_ID,
                                                            PI_CUSTOMER_ID    => I.CUSTOMER_ID,
                                                            PV_SHDES          => PV_SHDES,
                                                            PI_SNCODE         => LI_SNCODE,
                                                            PN_VALOR          => I.VALOR,
                                                            PD_FECHA_CORTE    => I.FECHA_CORTE,
                                                            PD_FECHA_INGRESO  => I.FECHA_INGRESO,  
                                                            PN_HILO           => I.HILO,
                                                            PN_CICLO          => I.CICLO,
                                                            PV_NUMERO_FACTURA => I.NO_FACTURA,
                                                            PV_ERROR          => LV_ERROR); 
                                                                                                                           
         IF LV_ERROR IS NOT NULL THEN
           RAISE LE_ERROR;
         END IF;
         
         --ACTUALIZA EL ESTADO DE LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP DE LAS CUENTAS PROCESADAS
         DOC1_DTH_ANALIZA_PROCESADOS.DOC1_UPDATE_CUENTA_TMP(PI_CO_ID         => I.CO_ID,
                                                             PI_CUSTOMER_ID  => I.CUSTOMER_ID,
                                                             PV_SHDES        => PV_SHDES,
                                                             PD_FECHA_CORTE  => I.FECHA_CORTE,
                                                             PN_HILO         => I.HILO,
                                                             PN_CICLO        => I.CICLO,
                                                             PV_ESTADO       => 'P',
                                                             PV_ERROR_TMP    => LV_ERROR,
                                                             PV_ERROR        => LV_ERROR);
                                                                                                                        
          IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
          END IF;
          
          --EN CASO QUE NO OCURRA NINGUN ERROR SE ACEPTARAN LAS TRASACCIONES ANTERIORES
          COMMIT;                      
          --
       EXCEPTION
        WHEN LE_ERROR THEN
          --EN CASO DE ALGUN ERROR SUSCITADO NO SE REALIZA NINGUNA TRANSACCION Y SE CONTINUARA CON EL 
          --SIGUIENTE REGISTRO
          ROLLBACK TO VALIDA_ERROR;          
          -- ACTUALIZA LA TABLA TEMPORAL DOC1_SERVICIOS_POSTPAGO_TMP PARA BITACORIZAR EL ERROR OCURRIDO
          DOC1_DTH_ANALIZA_PROCESADOS.DOC1_UPDATE_CUENTA_TMP(PI_CO_ID => I.CO_ID,
                                                             PI_CUSTOMER_ID => I.CUSTOMER_ID,
                                                             PV_SHDES =>  PV_SHDES,
                                                             PD_FECHA_CORTE => I.FECHA_CORTE,
                                                             PN_HILO => I.HILO,
                                                             PN_CICLO => I.CICLO,
                                                             PV_ESTADO => 'E',
                                                             PV_ERROR_TMP => LV_ERROR,
                                                             PV_ERROR => LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
          END IF;         
          --
              
        WHEN OTHERS THEN            
              
          LV_ERROR := SUBSTR(SQLERRM, 1, 2000);
              
          ROLLBACK TO VALIDA_ERROR;             
          -- ACTUALIZA LA TABLA TEMPORAL DOC1_SERVICIOS_POSTPAGO_TMP PARA BITACORIZAR EL ERROR OCURRIDO           
          DOC1_DTH_ANALIZA_PROCESADOS.DOC1_UPDATE_CUENTA_TMP(PI_CO_ID => I.CO_ID,
                                                             PI_CUSTOMER_ID => I.CUSTOMER_ID,
                                                             PV_SHDES =>  PV_SHDES,
                                                             PD_FECHA_CORTE => I.FECHA_CORTE,
                                                             PN_HILO => I.HILO,
                                                             PN_CICLO => I.CICLO,
                                                             PV_ESTADO => 'E',
                                                             PV_ERROR_TMP => LV_ERROR,
                                                             PV_ERROR => LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
          END IF;         
          --
      END;-- FIN DEL BEGIN PRINCIPAL

    END LOOP; 
    --EN CASO QUE NO OCURRA NINGUN ERROR SE ACEPTARAN LAS TRASACCIONES ANTERIORES
    COMMIT;            

    --ELIMINO DE LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP LAS CUENTAS POSTPAGADAS QUE YA FUERON PROCESADAS
    DOC1_DTH_ANALIZA_PROCESADOS.DOC1_DELETE_CUENTA_TMP(PD_FECHA_CORTE => PD_FECHA_CORTE,
                                                   PN_HILO => PN_HILO,
                                                   PV_ESTADO => 'P',
                                                   PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
    END IF;       
  EXCEPTION      
         WHEN LE_ERROR THEN                
            PV_ERROR := LV_ERROR;
         WHEN OTHERS THEN
            PV_ERROR := 'ERROR GENERAL - '||LV_PROCESO||'-'||SUBSTR(SQLERRM, 1, 3000);
  END DOC1_INSERT_CUENTA_POSTPAGO;
/*ESTE PROCEDIMIENTO REALIZA EL INSERT EN LA TABLA CO_FACT_POST_(DDMMYYY)*/
PROCEDURE DOC1_INSERT_CUENTA_DTH(PI_CO_ID INTEGER,
                                 PI_CUSTOMER_ID INTEGER,
                                 PV_SHDES    VARCHAR2,
                                 PI_SNCODE INTEGER,
                                 PN_VALOR NUMBER,
                                 PD_FECHA_CORTE DATE,
                                 PD_FECHA_INGRESO DATE,
                                 PN_HILO NUMBER,
                                 PN_CICLO NUMBER,
                                 PV_NUMERO_FACTURA VARCHAR2,
                                 PV_ERROR OUT VARCHAR2
                                 ) IS
                             
LV_PROCESO        VARCHAR2(70):='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_INSERT_CUENTA_DTH';
LV_SENTENCIA      VARCHAR2(2000);                             
BEGIN 
  LV_SENTENCIA:=
           ' INSERT INTO CO_FACT_POST_'||TO_CHAR(PD_FECHA_CORTE,'DDMMYYYY')         || 
           ' ( '                                                                    || 
           ' CO_ID ,'                                                               || 
           ' CUSTOMER_ID ,'                                                         || 
           ' SHDES  ,'                                                              ||           
           ' SNCODE ,'                                                              || 
           ' VALOR ,'                                                               || 
           ' FECHA_CORTE ,'                                                         || 
           ' ESTADO ,'                                                              || 
           ' FECHA_INGRESO, '                                                       || 
           ' HILO, '                                                                || 
           ' CICLO, '                                                               || 
           ' NUMERO_FACTURA '                                                       ||           
           ' )     '                                                                || 
           ' VALUES     '                                                           || 
           ' ('                                                                     || 
           ''||PI_CO_ID||','                                                        || 
           ''||PI_CUSTOMER_ID||','                                                  || 
           ''''||PV_SHDES||''','                                                    ||                      
           ''||PI_SNCODE||','                                                       || 
           'TO_NUMBER('''||PN_VALOR||'''),'                                         || 
           'TO_DATE('''||PD_FECHA_CORTE||''',''DD/MM/YYYY''),'                      || 
           '''A'','                                                                 || 
           'TO_DATE('''||PD_FECHA_INGRESO||''',''DD/MM/YYYY''),'                    || 
           ''||PN_HILO||','                                                         || 
           ''||PN_CICLO||','                                                        || 
           ''''||PV_NUMERO_FACTURA||''' '                                           ||                
           ' )';
  EXECUTE IMMEDIATE LV_SENTENCIA; 
  
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
       PV_ERROR:='ERROR AL INTENTAR INSERTAR REGISTRO DULPLICADO EN LA TABLA CO_FACT_POST_(DDMMYYYY)'||
                 LV_PROCESO;
  WHEN OTHERS THEN
       PV_ERROR:='ERROR AL INSERTAR EN LA TABLA DOC1_SERVICIOS_POSTPAGO_DTH'||
                  LV_PROCESO||'-'||SUBSTR(SQLERRM, 1, 2000);  
END DOC1_INSERT_CUENTA_DTH;
/*ESTE PROCEDIMIENTO REALIZA EL INSERT EN LA TABLA CO_FACT_POST_HIST*/
PROCEDURE DOC1_INSERT_CUENTA_HIST(PI_CO_ID INTEGER,
                                  PI_CUSTOMER_ID INTEGER,
                                  PV_SHDES VARCHAR2,
                                  PI_SNCODE NUMBER,
                                  PN_VALOR NUMBER,
                                  PD_FECHA_CORTE DATE,
                                  PD_FECHA_INGRESO DATE,                                
                                  PN_HILO NUMBER,
                                  PN_CICLO NUMBER,
                                  PV_NUMERO_FACTURA VARCHAR2,
                                  PV_ERROR OUT VARCHAR2
                                  ) IS

                         
LV_PROCESO        VARCHAR2(70):='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_INSERT_CUENTA_HIST';
                      
BEGIN 

          INSERT INTO CO_FACT_POST_HIST  
            ( 
            CO_ID ,
            CUSTOMER_ID ,
            SHDES,
            SNCODE ,
            VALOR ,
            FECHA_CORTE ,
            ESTADO ,
            FECHA_INGRESO, 
            FECHA_ACTUALIZACION,
            HILO ,
            CICLO,
            NUMERO_FACTURA
            )     
            VALUES     
            (
            PI_CO_ID ,
            PI_CUSTOMER_ID ,
            PV_SHDES,
            PI_SNCODE ,
            PN_VALOR ,
            PD_FECHA_CORTE,
            'I',
            PD_FECHA_INGRESO ,
            SYSDATE,
            PN_HILO,
            PN_CICLO,
            PV_NUMERO_FACTURA
            );           
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
       PV_ERROR:='ERROR AL INTENTAR INSERTAR REGISTRO DULPLICADO EN LA TABLA CO_FACT_POST_HIST'||
                  LV_PROCESO;
  WHEN OTHERS THEN
       PV_ERROR:='ERROR AL INSERTAR EN LA TABLA CO_FACT_POST_HIST'||
                  LV_PROCESO||'-'||SUBSTR(SQLERRM, 1, 2000);  
END DOC1_INSERT_CUENTA_HIST;

/*ESTE PROCEDIMIENTO REALIZA LA ELIMINACION DE TODOS LOS REGISTROS PROCESADOS DEL
CICLO EN CURSO DE LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP*/
PROCEDURE DOC1_DELETE_CUENTA_TMP(PD_FECHA_CORTE DATE,               
                                 PN_HILO NUMBER,
                                 PV_ESTADO VARCHAR2,
                                 PV_ERROR OUT VARCHAR2
                                 ) IS
LV_PROCESO        VARCHAR2(70):='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_DELETE_CUENTA_TMP';  
                                   
BEGIN 

DELETE FROM DOC1_SERVICIOS_POSTPAGO_TMP L
WHERE L.FECHA_CORTE=PD_FECHA_CORTE
AND L.STATUS=PV_ESTADO
AND L.HILO=PN_HILO;

COMMIT;
EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR AL ELIMINAR LA TABLA TEMPORAL DOC1_SERVICIOS_POSTPAGO_TMP.- '|| 
                  LV_PROCESO||' '||SUBSTR(SQLERRM, 1, 2000);    
END DOC1_DELETE_CUENTA_TMP;
  
/*SI EXISTEN DATOS IGUALES EN LA TABLA DE CO_FACT_POST_(DDMMYYYY) SE LOS ELIMINARAN DE LA MISMA TABLA*/
PROCEDURE DOC1_DELETE_CUENTA_INACTIVA(PI_CO_ID           INTEGER,
                                      PI_CUSTOMER        INTEGER,
                                      PV_SHDES           VARCHAR2,
                                      PI_SNCODE          INTEGER,
                                      PD_FECHA_CORTE     DATE,               
                                      PN_HILO            INTEGER,
                                      PN_CICLO           INTEGER,
                                      PV_ERROR       OUT VARCHAR2
                                      ) IS
LV_PROCESO        VARCHAR2(70):='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_DELETE_CUENTA_INACTIVA';  
LV_SENTENCIA VARCHAR2(2000);                   
LV_FORM_FECH  VARCHAR2(20);                
LV_NAME_TAB   VARCHAR2(20);
BEGIN 
LV_FORM_FECH:=TO_CHAR(PD_FECHA_CORTE,'DD/MM/YYYY');
LV_NAME_TAB:=TO_CHAR(PD_FECHA_CORTE,'DDMMYYYY');
LV_SENTENCIA:=
              'DELETE FROM CO_FACT_POST_'||LV_NAME_TAB||' L '||         
              ' WHERE L.CO_ID = '||PI_CO_ID||
              '   AND L.CUSTOMER_ID='||PI_CUSTOMER||
              '   AND L.SHDES='''||PV_SHDES||''''||
              '   AND L.SNCODE='||PI_SNCODE||
              '   AND L.FECHA_CORTE = TO_DATE('''||LV_FORM_FECH||''',''DD/MM/YYYY'')'||
              '   AND L.ESTADO      = ''A'''|| 
              '   AND L.CICLO       = '||PN_CICLO||             
              '   AND L.HILO        = '||PN_HILO;
              
EXECUTE IMMEDIATE LV_SENTENCIA;

EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_FACT_POST_(DDMMYYYY).- '|| 
                  LV_PROCESO||' '||SUBSTR(SQLERRM, 1, 2000);    
END DOC1_DELETE_CUENTA_INACTIVA;

/*PROCESO QUE ACTUALIZA LA TABLA DOC1_SERVICIOS_POSTPAGO_TMP EN CASO QUE EL REGISTRO ANALIZADO SEA 
PROCESADO SE ACTUALIZARA CON ESTADO (I) Y SI OCURRIO ALGUN ERROR EL ESTADO (E) Y SE REGISTRARA EL ERROR*/
PROCEDURE DOC1_UPDATE_CUENTA_TMP(PI_CO_ID     INTEGER,
                             PI_CUSTOMER_ID   INTEGER,
                             PV_SHDES         VARCHAR2,
                             PD_FECHA_CORTE   DATE,
                             PN_HILO          IN NUMBER,
                             PN_CICLO         IN NUMBER,
                             PV_ESTADO        IN VARCHAR2,
                             PV_ERROR_TMP     IN VARCHAR2,
                             PV_ERROR         OUT VARCHAR2
                             ) IS
LV_PROCESO  VARCHAR2(70):='DOC1_DTH_ANALIZA_PROCESADOS.DOC1_UPDATE_CUENTA_TMP';  
                           
BEGIN 

        UPDATE DOC1_SERVICIOS_POSTPAGO_TMP F
        SET F.STATUS = PV_ESTADO,
        F.ERROR=SUBSTR(NVL(PV_ERROR_TMP,NULL), 1, 2000)
        WHERE F.CO_ID=PI_CO_ID
        AND F.CUSTOMER_ID=PI_CUSTOMER_ID
        AND F.SHDES='PO'||PV_SHDES 
        AND F.FECHA_CORTE=PD_FECHA_CORTE
        AND F.HILO=PN_HILO
        AND F.CICLO=PN_CICLO
        AND F.STATUS='A';
EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR AL ACTUALIZAR LA TABLA TEMPORAL DOC1_SERVICIOS_POSTPAGO_TMP.- '|| 
                  LV_PROCESO||' '||SUBSTR(SQLERRM, 1, 2000);  
END DOC1_UPDATE_CUENTA_TMP;

END DOC1_DTH_ANALIZA_PROCESADOS;
/
