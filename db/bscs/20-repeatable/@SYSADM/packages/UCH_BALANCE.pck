CREATE OR REPLACE PACKAGE Uch_Balance
/*
**    @(#) but_bscs/bscs/database/scripts/oraproc/packages/uch_balance.sph, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/4, 24/04/02
**
**    References:
**    HLD uch/208316_uch_2 New Module UCH (Unapplied Cash Handler)
**
**    Author: Annegret Rauscher, LH Specifications GmbH
**    Date:   08/2001
**
**    Filenames: uch_balance.sph - Package header
**               uch_balance.spb - Package body
**
**    Major Modifications (when, who, what)
**    04/02 - Horst Szillat
**            New function PrintBodyVersion added
**
**/
IS

   /**************************************************************************
   *        P R O C E D U R E  do_balance                                    *
   *                                                                         *
   * Description:                                                            *
   *   This procedure balances debit and credit documents of preselected     *
   *   customers.                                                            *
   *                                                                         *
   * Parameters:                                                             *
   *   SourceId    IN   NUMBER                                               *
   *                    ID of the corresponding finance source entry         *
   *   DateLimit   IN   VARCHAR2                                             *
   *                    Documents older than the specified date are          *
   *                    processed                                            *
   *                    Date in the format DDMMYYYY                          *
   *   RunMode     IN   NUMBER                                               *
   *                    1: standard run                                      *
   *                    2: simulation run                                    *
   *   Result      OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   /**************************************************************************/
   PROCEDURE Do_Balance
   (
       -- ID of the corresponding finance source entry
       pionSourceID  IN  NUMBER,

       -- ID of the corresponding finance source entry
       piosDateLimit IN  VARCHAR2,

       -- mode of process run
       pionRunMode   IN  NUMBER,

       -- processing result
       poonResult    OUT NUMBER
   );

   /**************************************************************************
   *        F U N C T I O N  PrintBodyVersion                                *
   *                                                                         *
   * Description:                                                            *
   *   This function returns version control information.                    *
   *                                                                         *
   * Parameters:                                                             *
   *   none                                                                  *
   *                                                                         *
   /**************************************************************************/
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

END Uch_Balance;
/
CREATE OR REPLACE PACKAGE BODY Uch_Balance
/*
**    @(#) but_bscs/bscs/database/scripts/oraproc/packages/uch_balance.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/5, 22/01/03
**
**    References:
**    HLD uch/208316_uch_2 New Module UCH (Unapplied Cash Handler)
**
**    Author: Annegret Rauscher, LH Specifications GmbH
**    Date:   10/2001
**
**    Filenames: uch_balance.sph - Package header
**               uch_balance.spb - Package body
**
**    Major Modifications (when, who, what)
**    04/2002 - Horst Szillat
**              Feature 223242:
**              New function PrintBodyVersion added
**    05/2002 - Annegret Rauscher
**              Defect 224756:
**              Initialisation of the transaction number by selecting the
**              maximum from pihtab_all (<> 0 in case of recovery runs)
**    01/2003 - Annegret Rauscher
**              Defect 233467/d:
**              Cursor for debit documents:
**              Comparison of the ohrefdate without time with the date limit
**              (FCs entered in AR could not be balanced on the same day)
**
**
**/
IS
   /**************************************************************************
   *        P R O C E D U R E  do_balance                                    *
   *                                                                         *
   * Description:                                                            *
   *   This procedure balances debit and credit documents of preselected     *
   *   customers.                                                            *
   *   The debit documents can be of type invoice (IN) and further charge    *
   *   (FC); the credit documents can be of type credit memo (CM) and        *
   *   advance payment / credit customer adjustment (CO).                    *
   *                                                                         *
   * Parameters:                                                             *
   *   SourceId    IN   NUMBER                                               *
   *                    ID of the corresponding finance source entry         *
   *   DateLimit   IN   VARCHAR2                                             *
   *                    Documents older than the specified date are          *
   *                    processed                                            *
   *                    Date in the format DDMMYYYY                          *
   *   RunMode     IN   NUMBER                                               *
   *                    1: standard run                                      *
   *                    2: simulation run                                    *
   *   Result      OUT  NUMBER                                               *
   *                    0 : process finished with an unspecified error,      *
   *                        e.g. an Oracle Error                             *
   *                    1 : process finished without error                   *
   *                    >1: process finished with a specified error          *
   *                                                                         *
   /**************************************************************************/
   PROCEDURE Do_Balance
   (
       pionSourceID  IN  NUMBER,      -- ID of the finance source entry
       piosDateLimit IN  VARCHAR2,    -- Date Limit for the documents
       pionRunMode   IN  NUMBER,      -- mode of process run
       poonResult    OUT NUMBER       -- processing result
   )
   IS

      /*
      **  Variables section
      */
      -- USERLBL variables
      losComplaintSetting VARCHAR2(1);  -- setting from USERLBL
      losComplaintValue1  VARCHAR2(1);  -- 1st possible value for complaint flag
      losComplaintValue2  VARCHAR2(1);  -- 2nd possible value for complaint flag
      losComplaintValue3  VARCHAR2(1);  -- 3rd possible value for complaint flag
      losTransxSequence   VARCHAR2(1);  -- indicator for transaction sequence
      losCommitInterval   VARCHAR2(40); -- commit interval for customers

      -- Conversion variables (parameters, settings from table USERLBL)
      lodDateLimit        DATE;         -- date limit for the documents
      lonCommitInterval   NUMBER;       -- commit interval for customers

      -- Counter variables
      lonTransactionNo    NUMBER;       -- consecutive number to be used as
                                        -- part of the transaction number
      lonCustomerNo       NUMBER  := 0; -- counter for customers

      -- Variables to store temporary values
      lonCurrencyOld      ORDERHDR_ALL.document_currency%TYPE;
                          -- currency of privious documents of current customer
      lonDebAmountUsed    ORDERHDR_ALL.ohopnamt_doc%TYPE := 0;
                          -- amount of the current debit document already
                          -- used to balance credit documents
      lonCredAmountUsed   ORDERHDR_ALL.ohopnamt_doc%TYPE := 0;
                          -- amount of the current credit document already
                          -- used to balance debit documents
      lobFetchDebit       BOOLEAN := TRUE;  -- TRUE if next debit document
                                            -- has to be fetched
      lobFetchCredit      BOOLEAN := TRUE;  -- TRUE if next credit document
                                            -- has to be fetched

      -- Cursor Variables
      lonCurrentCustomer  ORDERHDR_ALL.customer_id%TYPE;
      lonDebKey           ORDERHDR_ALL.ohxact%TYPE;
      losDebRefnum        ORDERHDR_ALL.ohrefnum%TYPE;
      losDebStatus        ORDERHDR_ALL.ohstatus%TYPE;
      lonDebAmount        ORDERHDR_ALL.ohopnamt_doc%TYPE;
      lonDebCurrency      ORDERHDR_ALL.document_currency%TYPE;
      lonCredKey          ORDERHDR_ALL.ohxact%TYPE;
      losCredRefnum       ORDERHDR_ALL.ohrefnum%TYPE;
      losCredStatus       ORDERHDR_ALL.ohstatus%TYPE;
      lonCredAmount       ORDERHDR_ALL.ohopnamt_doc%TYPE;
      lonCredCurrency     ORDERHDR_ALL.document_currency%TYPE;

      -- PIHTAB values
      lonPIHTAB_ID        PIHTAB_ALL.pihtab_id%TYPE;
      losTransx_Seq       PIHTAB_ALL.transx_sequence%TYPE;
      losTransx_Status    PIHTAB_ALL.transx_status%TYPE;
      losTransx_Code      PIHTAB_ALL.transx_code%TYPE;
      losTransx_Refnum    PIHTAB_ALL.cachknum%TYPE;
      lonBalanceAmount    PIHTAB_ALL.cachkamt%TYPE;
      losCurrencyCode     PIHTAB_ALL.currency%TYPE;

      -- Error values
      lonWrongParameter   NUMBER := 10;
      lonWrongSetup       NUMBER := 11;
      lonWrongDocType     NUMBER := 12;


      /*
      **  Cursor section
      */
      -- Cursor to select the defined customers
      CURSOR locCustomers IS
         SELECT DISTINCT (customer_id)
           FROM UCH_CUSTOMER_SELECTION
          WHERE status IS NULL
       ORDER BY customer_id;

      -- Cursor to select all debit documents of the current customer
      CURSOR locDebitDocuments IS
         SELECT ohxact, ohrefnum, ohstatus,
                ohopnamt_doc, document_currency
           FROM ORDERHDR_ALL oh
          WHERE     oh.ohopnamt_doc > 0
                -- Defect 233467: truncate the time part from ohrefdate
                AND trunc(oh.ohrefdate) <= lodDateLimit
                AND oh.ohstatus    IN (SELECT doc_type_code
                                         FROM UCH_DOCUMENT_SELECTION
                                        WHERE doc_type_sign = 'D')
                AND oh.customer_id = lonCurrentCustomer
                AND NVL(oh.complaint, '0') IN (losComplaintValue1,
                                               losComplaintValue2,
                                               losComplaintValue3)
          ORDER BY ohrefdate;

      -- Cursor to select all credit documents of the current customer
      CURSOR locCreditDocuments IS
         SELECT ohxact, ohrefnum, ohstatus,
                ohopnamt_doc, document_currency
           FROM ORDERHDR_ALL oh
          WHERE     oh.ohopnamt_doc < 0
                AND oh.ohstatus    IN (SELECT doc_type_code
                                         FROM UCH_DOCUMENT_SELECTION
                                        WHERE doc_type_sign = 'C')
                AND oh.customer_id = lonCurrentCustomer
                AND NVL(oh.complaint, '0') IN (losComplaintValue1,
                                               losComplaintValue2,
                                               losComplaintValue3)
          ORDER BY ohrefdate;

   BEGIN

      poonResult := 0;

      -- Convert the date limit from string into date format
      lodDateLimit := TO_DATE(piosDateLimit, 'DDMMYYYY');

      -- Populate transaction status according to run mode
      IF pionRunMode = 1 THEN      -- normal run
         losTransx_Status := 1;
      ELSIF pionRunMode = 2 THEN   -- simulation run
         losTransx_Status := 4;
      ELSE  -- unknown mode
         poonResult := lonWrongParameter;
         RAISE_APPLICATION_ERROR( -1, 'Unknown run mode!');
      END IF;

      -- Retrieve configuration values
      -- (values for the complaint flag, transx sequence and commit size)
      SELECT ulcff3, ulcff4, ulnff1
        INTO losComplaintSetting,
             losTransxSequence,
             losCommitInterval
        FROM USERLBL
       WHERE ultag = 'UCH_SETTINGS';
      lonCommitInterval := TO_NUMBER(losCommitInterval);

      -- Determine the possible values for the complaint flag
      IF losComplaintSetting = 'A' THEN
         -- all values
         losComplaintValue1 := '0';
         losComplaintValue2 := 'F';
         losComplaintValue3 := 'X';
      ELSIF losComplaintSetting = 'N' THEN
         -- all values except for 'X' (complaint status)
         losComplaintValue1 := '0';
         losComplaintValue2 := 'F';
         losComplaintValue3 := '0';
      ELSE  -- wrong setup value
         poonResult := lonWrongSetup;
         RAISE_APPLICATION_ERROR( -1, 'Wrong setup value!');
      END IF;

      -- Check value for transaction sequence
      IF losTransxSequence <> 'N' AND
         losTransxSequence <> 'Y'
      THEN
         poonResult := lonWrongSetup;
         RAISE_APPLICATION_ERROR( -1, 'Wrong setup value!');
      END IF;

      -- Defect 224756:
      -- Determine initialisation value for the transaction number
      -- <> 0 in case of recovery runs
      SELECT NVL(MAX(transx_num), 0)
        INTO lonTransactionNo
        FROM PIHTAB_ALL
       WHERE source_id = pionSourceID;

      -- Open Customer cursor
      OPEN locCustomers;

      <<CustomerLoop>>
      LOOP
         FETCH locCustomers
          INTO lonCurrentCustomer;
         IF locCustomers%NOTFOUND THEN
            EXIT CustomerLoop;
         END IF;

         -- initialisations for new customer
         lonCurrencyOld   := NULL;   -- document currency
         losTransx_Seq    := 0;      -- new transaction
         lobFetchDebit    := TRUE;
         lobFetchCredit   := TRUE;
         SAVEPOINT customer;

         -- Close old document cursors; Open new document cursors
         IF lonCustomerNo > 0 THEN
            CLOSE locDebitDocuments;
            CLOSE locCreditDocuments;
         END IF;
         OPEN locDebitDocuments;
         OPEN locCreditDocuments;

         <<DocumentLoop>>
         LOOP
            -- Fetch debit document
            IF lobFetchDebit = TRUE THEN
               lonDebAmountUsed  := 0;
               FETCH locDebitDocuments
                INTO lonDebKey,
                     losDebRefnum,
                     losDebStatus,
                     lonDebAmount,
                     lonDebCurrency;
               /* if end of cursor exit loop */
               IF locDebitDocuments%NOTFOUND THEN
                  EXIT DocumentLoop;
               END IF;
            END IF;

            -- Fetch credit document
            IF lobFetchCredit = TRUE THEN
               lonCredAmountUsed := 0;
               FETCH locCreditDocuments
                INTO lonCredKey,
                     losCredRefnum,
                     losCredStatus,
                     lonCredAmount,
                     lonCredCurrency;
               /* if end of cursor exit loop */
               IF locCreditDocuments%NOTFOUND THEN
                  EXIT DocumentLoop;
               END IF;
            END IF;

            -- First documents of a customer:
            -- Store currency id, retrieve currency code
            IF lonCurrencyOld IS NULL THEN
               lonCurrencyOld := lonDebCurrency;
               SELECT fccode
                 INTO losCurrencyCode
                 FROM FORCURR
                WHERE fc_id = lonDebCurrency;
            END IF;

            -- Check that all documents of the customer have the same
            -- currency id
            IF lonDebCurrency  != lonCurrencyOld OR
               lonCredCurrency != lonCurrencyOld
            THEN
               ROLLBACK TO SAVEPOINT customer;
               UPDATE UCH_CUSTOMER_SELECTION
                  SET status = 'E'
                WHERE customer_id = lonCurrentCustomer AND
                      status IS NULL;
               EXIT DocumentLoop;
            END IF;

            -- Determine balance amount, used amount
            -- and cursor(s) to be fetched next
            IF ABS(lonDebAmount)  - lonDebAmountUsed  >
               ABS(lonCredAmount) - lonCredAmountUsed
            THEN
               lonBalanceAmount := ABS(lonCredAmount) - lonCredAmountUsed;
               lobFetchDebit    := FALSE;
               lobFetchCredit   := TRUE;
            ELSIF ABS(lonDebAmount)  - lonDebAmountUsed  <
                  ABS(lonCredAmount) - lonCredAmountUsed
            THEN
               lonBalanceAmount := ABS(lonDebAmount) - lonDebAmountUsed;
               lobFetchDebit    := TRUE;
               lobFetchCredit   := FALSE;
            ELSE  /* ABS(lonDebAmount)  - lonDebAmountUsed  =
                     ABS(lonCredAmount) - lonCredAmountUsed    */
               lonBalanceAmount := ABS(lonDebAmount) - lonDebAmountUsed;
               lobFetchDebit    := TRUE;
               lobFetchCredit   := TRUE;
            END IF;
            lonDebAmountUsed    := lonDebAmountUsed  + lonBalanceAmount;
            lonCredAmountUsed   := lonCredAmountUsed + lonBalanceAmount;

            -- Determine transaction code
            IF losDebStatus = 'IN' THEN
               IF losCredStatus     = 'CO' THEN
                  losTransx_Code   := 'CO2IN';
               ELSIF losCredStatus  = 'CM' THEN
                  losTransx_Code   := 'CM2IN';
               ELSE
                  poonResult := lonWrongDocType;
                  RAISE_APPLICATION_ERROR( -1, 'Unhandled document type: ' ||
                                           losCredStatus);
               END IF;
            ELSIF losDebStatus = 'FC' THEN
               IF losCredStatus     = 'CO' THEN
                  losTransx_Code   := 'CO2FC';
               ELSIF losCredStatus  = 'CM' THEN
                  losTransx_Code   := 'CM2FC';
               ELSE
                  poonResult := lonWrongDocType;
                  RAISE_APPLICATION_ERROR( -1, 'Unhandled document type: ' ||
                                           losCredStatus);
               END IF;
            ELSE
               poonResult := lonWrongDocType;
               RAISE_APPLICATION_ERROR( -1, 'Unhandled document type: ' ||
                                        losDebStatus);
            END IF;

            -- Enter balance transaction into PIHTAB_ALL
            Nextfree.getvalue('MAX_PIHTAB_ID', lonPIHTAB_ID);
            lonTransactionNo := lonTransactionNo + 1;
            losTransx_Refnum := 'UCH_' ||
                                TO_CHAR(pionSourceID) || '_' ||
                                TO_CHAR(lonTransactionNo);
            INSERT INTO PIHTAB_ALL
                   ( pihtab_id, source_id, transx_num, transx_sequence,
                     transx_status, transx_entrydate, transx_statusdate,
                     transx_code, cachknum, cachkdate, carecdate,
                     cachkamt, currency,
                     carem, causername,
                     ohrefnum, ohxact, customer_id,
                     ohrefnum2, ohxact2, cabatch)
              VALUES
                   ( lonPIHTAB_ID, pionSourceID, lonTransactionNo, losTransx_Seq,
                     losTransx_Status, SYSDATE, SYSDATE,
                     losTransx_Code, losTransx_Refnum, SYSDATE, SYSDATE,
                     lonBalanceAmount, losCurrencyCode,
                     'Automatically generated balance transaction', 'UCH',
                     losDebRefnum, lonDebKey, lonCurrentCustomer,
                     losCredRefnum, lonCredKey, pionSourceID);

            -- determine whether a transaction sequence should be created
            IF losTransxSequence = 'Y' AND losTransx_Seq = 0 THEN
               losTransx_Seq := 1;
            END IF;

         END LOOP DocumentLoop;

         -- update status of customer entry in selection table
         UPDATE UCH_CUSTOMER_SELECTION
            SET status = 'X'
          WHERE customer_id = lonCurrentCustomer AND
                status IS NULL;

         -- increase customer counter and check if commit size is reached
         lonCustomerNo := lonCustomerNo + 1;
         IF lonCustomerNo MOD lonCommitInterval = 0 THEN
            COMMIT;
         END IF;

      END LOOP CustomerLoop;

      -- close cursors; commit
      IF lonCustomerNo > 0 THEN
         CLOSE locDebitDocuments;
         CLOSE locCreditDocuments;
      END IF;
      CLOSE locCustomers;
      COMMIT;

      poonResult := 1;

      -- Handle exceptions
   EXCEPTION
      WHEN OTHERS THEN
           RAISE_APPLICATION_ERROR(-20000, SQLERRM);

   END Do_Balance;


   /**************************************************************************
   *        F U N C T I O N  PrintBodyVersion                                *
   *                                                                         *
   * Description:                                                            *
   *   This function returns version control information.                    *
   *   The placeholders (I, E, W) are replaced when extracting the           *
   *   package.                                                              *
   *                                                                         *
   * Parameters:                                                             *
   *   none                                                                  *
   *                                                                         *
   /**************************************************************************/
   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN '/main/5' || ' | ' || '22/01/03' || ' | ' || 'but_bscs/bscs/database/scripts/oraproc/packages/uch_balance.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605';
   END PrintBodyVersion;


END Uch_Balance;
/

