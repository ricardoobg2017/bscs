create or replace package QC_ESTATUS_REZAGO_CTA is

  -- Author  : XCIFUENTES
  -- Created : 15/04/2009
  -- Purpose : ACTUALIZACION ESTATUS REZAGO POR CUENTA
 

  FUNCTION VALIDA_TRAMA  (Pv_Trama varchar2) return boolean;   

  PROCEDURE qc_driverlicence (pv_fecha varchar2, pv_error out varchar2);

end QC_ESTATUS_REZAGO_CTA;
/
create or replace package body QC_ESTATUS_REZAGO_CTA is

  FUNCTION VALIDA_TRAMA  (Pv_Trama varchar2) return boolean is

 
 lv_region          varchar2(3);
 ln_prioridad       number;
 lv_estado          varchar2(1);
 ln_pos_esp1        number;
 ln_pos_esp2        number;
 
 lv_error           varchar2(3000);
 
 	BEGIN
  ln_pos_esp1 := Instr(Pv_Trama,' ',1,1);
  ln_pos_esp2 := Instr(Pv_Trama,' ',1,2);
  lv_region := substr(Pv_Trama,1,ln_pos_esp1 - 1);
  ln_prioridad := TO_NUMBER(substr(Pv_Trama,ln_pos_esp1 + 1,ln_pos_esp2 - (ln_pos_esp1+1)));
  lv_estado := substr(Pv_Trama,ln_pos_esp2 + 1); 
 
  IF lv_region IS NULL OR(lv_region != 'COS' AND lv_region != 'SIE') THEN
    RETURN FALSE;
  END IF;
  
  IF ln_prioridad IS NULL OR ln_prioridad < 1 THEN
    RETURN FALSE;
  END IF;
  
  IF lv_ESTADO IS NULL THEN
    RETURN FALSE;
  END IF;
   
  RETURN TRUE;
 		
 	EXCEPTION 		
 		WHEN OTHERS THEN
    RETURN FALSE;
 	END VALIDA_TRAMA;

 
procedure qc_driverlicence (pv_fecha varchar2, pv_error out varchar2) is

CURSOR actualiza_campo (cd_fecha date) is
  select w.* from ccontact_all w
  where w.LASTMODDATE >= cd_fecha or ccbill='X';
--  where w.ccentdate > cd_fecha;

CURSOR actualiza_campo_2 is
  select w.* from ccontact_all w
  where ccbill='X';
  
cursor c_custcode (cn_customer number) is
  select cu.custcode
  from ccontact_all co, customer_all cu
  where co.customer_id = cn_customer
  and cu.customer_id = co.customer_id;
 
ld_fecha date;
lv_custcode varchar2(24);
ln_contrato number;
lv_trama   varchar2(20);
lv_error   varchar2(2000);
ln_contador number:=0;

begin
 
 if pv_fecha is null then
     For t in actualiza_campo_2 loop
        begin
        SAVEPOINT B;
        if t.csdrivelicence is null or not valida_trama(t.csdrivelicence) then
        
            open c_custcode(t.customer_id);
            fetch c_custcode into lv_custcode;
            close c_custcode;
        
            CLP_RECUP_REGION_PRIORIDAD_REZ@axis(ln_contrato,lv_custcode,lv_trama,lv_error);
        
            update ccontact_ALL d
            set d.csdrivelicence=lv_trama
            where d.customer_id = t.customer_id;
             
            if ln_contador = 200 then
               ln_contador := 0;
               commit;
            else 
               ln_contador := ln_contador + 1;
            end if;
            
        end if;
        exception when others then
            rollback to B;
         end;
     end loop;
 else
     begin
       ld_fecha := to_date(pv_fecha||' 00:00:00','dd/mm/rrrr hh24:mi:ss');
     exception 
       when others then 
          pv_error := 'Fecha incorrecta'; 
          return;
     end;
     --
     ld_fecha := ld_fecha - 31;
     --
     For t in actualiza_campo (ld_fecha) loop
        begin
        SAVEPOINT A;
        if t.csdrivelicence is null or not valida_trama(t.csdrivelicence) then
        
            open c_custcode(t.customer_id);
            fetch c_custcode into lv_custcode;
            close c_custcode;
        
            CLP_RECUP_REGION_PRIORIDAD_REZ@axis(ln_contrato,lv_custcode,lv_trama,lv_error);
        
            update ccontact_ALL d
            set d.csdrivelicence=lv_trama
            where d.customer_id = t.customer_id;
             
            if ln_contador = 200 then
               ln_contador := 0;
               commit;
            else 
               ln_contador := ln_contador + 1;
            end if;
            
        end if;
        exception when others then
            rollback to A;
         end;
     end loop;
 end if;
     
 commit;
 
 exception 
   when others then 
     rollback;
     pv_error := sqlerrm;
 end qc_driverlicence;

  
end QC_ESTATUS_REZAGO_CTA;
/
