create or replace package GCK_TRX_SUS_REAC_INAC_MASIVA_L is

  --========================= =============================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Validacion de deuda de los servicios
--======================================================================================--
 
FUNCTION FC_GETPARAMETROS_RE(PV_COD_PARAMETRO  VARCHAR2) RETURN VARCHAR2;
PROCEDURE BLP_INSERTA_MQU_RELOJ(PV_CUENTA      IN VARCHAR2,
                                PN_DESPACHADOR IN NUMBER,
                                PV_MENSAJE     OUT VARCHAR2,
                                PN_ERROR       OUT NUMBER,
                                PV_ERROR       OUT VARCHAR2);
                                   
PROCEDURE BLP_VALIDA_DEUDA(PV_CUENTA_CRED        IN VARCHAR2,
                           PN_COD_ERROR_CRE     OUT NUMBER,
                           PV_RESULTADO_CRE     OUT VARCHAR2) ;
  
end GCK_TRX_SUS_REAC_INAC_MASIVA_L;
/
CREATE OR REPLACE PACKAGE BODY GCK_TRX_SUS_REAC_INAC_MASIVA_L IS

--======================================================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Validacion de deuda de los servicios
--======================================================================================--
FUNCTION FC_GETPARAMETROS_RE(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS

   LV_VALOR_PAR   VARCHAR2(1000);
   LV_PROCESO     VARCHAR2(1000);
   LV_DESCRIPCION VARCHAR2(1000);
   LN_ERROR       NUMBER;
   LV_ERROR       VARCHAR2(1000);
  
 BEGIN
  
    SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(PV_COD_PARAMETRO,
                                                LV_PROCESO,
                                                LV_VALOR_PAR,
                                                LV_DESCRIPCION,
                                                LN_ERROR,
                                                LV_ERROR);
    RETURN LV_VALOR_PAR;
  
  EXCEPTION
      WHEN OTHERS THEN
            RETURN NULL;
        
END FC_GETPARAMETROS_RE;

PROCEDURE BLP_INSERTA_MQU_RELOJ(PV_CUENTA      IN VARCHAR2,
                                   PN_DESPACHADOR IN NUMBER,
                                   PV_MENSAJE     OUT VARCHAR2,
                                   PN_ERROR       OUT NUMBER,
                                   PV_ERROR       OUT VARCHAR2) IS
   
    CURSOR C_MENSAJE (CV_CUENTA VARCHAR2) IS
       SELECT MENSAJE FROM BL_GSI_RELOJ WHERE CUENTA=CV_CUENTA;
     
    --DECLARACION DE VARIABLES
    LB_FOUND         BOOLEAN;
    LC_ENVIOS        C_MENSAJE%ROWTYPE;
    LV_ERROR         VARCHAR2(100);
    LE_MIERROR       EXCEPTION;
    LV_APLICACION    VARCHAR2(60) := 'GCK_TRX_SUS_REAC_INAC_MASIVA_L.BLP_INSERTA_MQU_RELOJ - ';
    
BEGIN
  
    --LLAMANDO AL PAQUETE GCK_OBJ_SUS_REAC_INAC_MASIVA_L PARA INSERT DEL LA TABLA BL_GSI_RELOJ. 
     GCK_OBJ_SUS_REAC_INAC_MASIVA_L.BLP_INSERT_BL_GSI_RELOJ(PV_CUENTA      => PV_CUENTA,
                                                            PN_DESPACHADOR => PN_DESPACHADOR,
                                                            PV_ERROR       => LV_ERROR);
     
     IF LV_ERROR IS NOT NULL THEN
        RAISE LE_MIERROR;
     END IF;

     --LLAMANDO AL PAQUETE GCK_TRX_CONCILIA_LINEA_MASIVA PARA UPDATE DEL LA TABLA BL_GSI_RELOJ.
     GCK_TRX_CONCILIA_LINEA_MASIVA.GCP_MQU_RELOJ(PN_DESPACHADOR);
 
     OPEN C_MENSAJE (PV_CUENTA);
     FETCH C_MENSAJE INTO LC_ENVIOS;
     LB_FOUND := C_MENSAJE%FOUND;
     CLOSE C_MENSAJE;
     
     IF LB_FOUND THEN
        PV_MENSAJE := LC_ENVIOS.MENSAJE;
        PN_ERROR :=0;  
     END IF;
      
     DELETE BL_GSI_RELOJ A WHERE A.CUENTA = PV_CUENTA;
     COMMIT;
  
 EXCEPTION
      WHEN LE_MIERROR THEN
          PV_ERROR := SUBSTR(LV_APLICACION || LV_ERROR, 1, 500);
          PN_ERROR:=-1;
      WHEN OTHERS THEN
          PV_ERROR := SUBSTR(LV_APLICACION || SQLERRM, 1, 500);
          PN_ERROR:=-2;

END BLP_INSERTA_MQU_RELOJ;

PROCEDURE BLP_VALIDA_DEUDA(PV_CUENTA_CRED      IN VARCHAR2,
                           PN_COD_ERROR_CRE   OUT NUMBER,
                           PV_RESULTADO_CRE   OUT VARCHAR2) IS
 
      CURSOR C_QUERY(PV_CUENTA VARCHAR2,PV_TIPO_CREDITO VARCHAR2) IS
          SELECT CUSTCODE, CSTRADECODE
            FROM CUSTOMER_ALL C
           WHERE CUSTCODE = PV_CUENTA
             AND INSTR(PV_TIPO_CREDITO, C.CSTRADECODE) > 0;
    --
    LV_RESULTADO_OUT                 LONG;
    LN_ID_REQ_EIS1                   NUMBER;
    LX_XML_EIS1                      SYS.XMLTYPE;
    LV_TRAMA_BIT_PARAMETRO_EIS1      VARCHAR2(3500);
    LV_FAULT_STRING                  VARCHAR2(4000):= NULL;
    LV_FAULT_CODE                    VARCHAR2(4000);
    LV_RESPUESTA_EIS1                VARCHAR2(4000);
    LV_APLICACION                    VARCHAR2(1000) := 'GCK_TRX_SUS_REAC_INAC_MASIVA_L.BLP_VALIDA_DEUDA';
    LC_CUSTCODE                      VARCHAR2(20);   
    LC_CSTRADECODE                   VARCHAR2(20);  
    LV_ERROR                         VARCHAR2(2000);  
    LB_EXISTEN_DATOS                 BOOLEAN:=FALSE;
    LE_ERROR                         EXCEPTION;
    LC_QUERY                         C_QUERY%ROWTYPE;
   
BEGIN
    
    PV_RESULTADO_CRE     := '';
    PN_COD_ERROR_CRE     := 1;
    -- EIS => OBTIENE LOS TIPOS DE CREDITOS EN AXIS
    LN_ID_REQ_EIS1:= TO_NUMBER(FC_GETPARAMETROS_RE('CONF_RC_TR_TC_0001'));
    LV_TRAMA_BIT_PARAMETRO_EIS1 :=  'dsId='||FC_GETPARAMETROS_RE('CONF_RC_TR_TC_0002')||';pnIdServicioInformacion='||TO_NUMBER(FC_GETPARAMETROS_RE('CONF_RC_TR_TC_0003'))||';'; 
                 
    LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                    => LN_ID_REQ_EIS1,
                                                            PV_PARAMETROS_REQUERIMIENTO  => LV_TRAMA_BIT_PARAMETRO_EIS1,
                                                            PV_FAULT_CODE                => LV_FAULT_CODE,
                                                            PV_FAULT_STRING              => LV_FAULT_STRING);
                  
    IF LV_FAULT_STRING IS NOT NULL THEN       
       LV_ERROR := 'ERROR EN EL CONSUMO DEL WEBSERVICE EIS DE AXIS: '||LV_FAULT_STRING;
       RAISE LE_ERROR;
    END IF;

    LV_RESPUESTA_EIS1   := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                                  PR_RESPUESTA        => LX_XML_EIS1,
                                                                  PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                                  PV_NAMESPACE        => NULL);
      
    IF LV_RESPUESTA_EIS1 IS NULL THEN
       LV_ERROR := ' NO SE OBTUVO EL PARAMETRO DEL EIS DE AXIS';
       RAISE LE_ERROR;
    END IF;
    
    LV_RESPUESTA_EIS1:=TRIM(',' FROM LV_RESPUESTA_EIS1);
    
    OPEN C_QUERY (PV_CUENTA_CRED,LV_RESPUESTA_EIS1);
    FETCH C_QUERY INTO LC_QUERY;
    LB_EXISTEN_DATOS:=C_QUERY%FOUND;
    CLOSE C_QUERY;
    
    LC_CUSTCODE:=LC_QUERY.CUSTCODE;
    LC_CSTRADECODE:=LC_QUERY.CSTRADECODE;
    LV_RESULTADO_OUT:='LC_CUSTCODE: '||LC_CUSTCODE||'| LC_LC_CSTRADECODE: '||LC_CSTRADECODE||'';
  
    IF LB_EXISTEN_DATOS THEN
       PV_RESULTADO_CRE     := LV_RESULTADO_OUT;
    ELSE 
       PV_RESULTADO_CRE     := '';
    END IF;
    
EXCEPTION
  WHEN LE_ERROR THEN
      PV_RESULTADO_CRE := SUBSTR(LV_ERROR, 1, 500);
      PN_COD_ERROR_CRE     := -1;
   WHEN OTHERS THEN
      PN_COD_ERROR_CRE     := -2;
      PV_RESULTADO_CRE := SUBSTR(LV_APLICACION || ' - ' || SUBSTR(SQLERRM, 0, 500), 1, 500);       
END BLP_VALIDA_DEUDA;
  
END GCK_TRX_SUS_REAC_INAC_MASIVA_L;
/
