CREATE OR REPLACE package SAK_CUENTAS_VIP is

  --===================================================
  -- Proyecto: Reportes de SAC
  -- Autor:    Ing. Jimmy Larrosa
  -- Fecha:    16/09/2003
  -- �ltima actualizaci�n: 10/03/2004
  -- JLA
  --===================================================
  
  procedure crea_cuentas_vip;
  
  procedure actualiza_datos(p_cuenta in varchar2,
                            p_ejecutivo in varchar2,
                            p_segmento in varchar2,
                            p_fecha in varchar2,
                            p_error out varchar2);
                            
  procedure cuentas_inactivas;
  
  procedure consulta_cuentas_vip(p_cuenta in varchar2,
                                 p_fecha in varchar2,
                                 p_ruc out varchar2,
                                 p_nombre out varchar2,
                                 p_region out varchar2,
                                 p_ciudad out varchar2,
                                 p_producto out varchar2,
                                 p_ejecutivo out varchar2,
                                 p_lineas out number,
                                 p_segmento out varchar2,
                                 p_consumo_actual out number,
                                 p_consumo_hace_un_mes out number,
                                 p_consumo_hace_dos_meses out number,
                                 p_consumo_promedio out number,
                                 p_pagos out number,
                                 p_porcentaje_recu out number,
                                 p_forma_pago out varchar2,
                                 p_tipo_segmento out varchar2,
                                 p_vendedor out varchar2,
                                 p_error out varchar2);
  
  procedure calcula_segmento_vip;
                                 
end;
/

