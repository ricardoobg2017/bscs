CREATE OR REPLACE PACKAGE OCK_TRX IS
 
 PROCEDURE Registra_occ(PV_CUSTOMER_CODE   IN VARCHAR2,
                        PV_SERVICIO        IN VARCHAR2 DEFAULT NULL,
                        PN_SNCODE          IN NUMBER,
                        PV_VALID_FROM      IN VARCHAR2,
                        PV_REMARK          IN VARCHAR2,
                        PN_AMOUNT          IN NUMBER,
                        PV_FEE_TYPE        IN VARCHAR2,
                        PN_FEE_CLASS       IN NUMBER,
                        PV_USERNAME        IN VARCHAR2,
                        PN_COID            IN OUT NUMBER,
                        PN_CUSTOMERID      OUT NUMBER,
                        PN_TMCODE          OUT NUMBER,
                        PN_PERIOD          OUT NUMBER,
                        PV_ERROR           OUT VARCHAR2,
                        PV_MESSAGE         OUT VARCHAR2);
 
 Function Procesa_Occ(pn_customer_id in number,
                       pn_co_id       in number,
                       pn_tmcode      in number,
                       pn_sncode      in number,
                       pn_period      in number,
                       pd_valid_from  in date,
                       pv_remark      in varchar2,
                       pn_amount      in number,
                       pv_fee_type    in varchar2,
                       pn_fee_class   in number,
                       pv_username    in varchar2,
                       Pv_Error       Out Varchar2,
                       Pd_Endate      in date default null) Return Number;

  Function Valida_Duplicado(Pn_Customerid In Number,
                            Pn_Coid       In Number,
                            Pn_Sncode     In Number,
                            Pn_Tmcode     In Number,
                            Pn_Amount     In Float,
                            Pv_Valid_From In date,
                            Pv_Remark     In Varchar2,
                            Pv_Error      Out Varchar2) Return Number;

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pd_lastmoddate     In date,
                        Pv_Error           Out Varchar2) Return Number;
   
  FUNCTION EXTRAE(CADENA   VARCHAR2,
                  PALABRA  VARCHAR2,
                  CARACTER VARCHAR2,
                  REP      NUMBER) RETURN NUMBER;
END OCK_TRX;
/
CREATE OR REPLACE PACKAGE BODY OCK_TRX IS
--=====================================================================================--  
-- CREADO POR:     Iro Andres Balladares
-- FECHA MOD:      24/11/2015
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         optimizacion de despacho masivo de OCC
--=====================================================================================--  
--=====================================================================================--
-- MODIFICADO POR: Johanna Guano JGU.
-- FECHA MOD:      30/11/2015
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Implementar el esquema SRE en el comando Crea_OCC para DTH
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Johanna Guano JGU.
-- FECHA MOD:      19/01/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Validacion del estado de la cuenta y nuevo parametro ENDATE
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Tomas Coronel.
-- FECHA MOD:      07/03/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Cambio de mensajes de notificaciones de errores
--=====================================================================================--
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano JGU.
  -- FECHA MOD:      13/04/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Verificar  si la cuenta en DTH, para encolar el co_id en la tabla fees
  --=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Johanna Guano JGU.
-- FECHA MOD:      18/04/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Obtener el pack_id para el sncode 48 de minutos portafolio.
--=====================================================================================--
 --=====================================================================================--
-- MODIFICADO POR: Johanna Guano JGU.
-- FECHA MOD:      22/04/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Validar status coid en cuenta DTH.
--=====================================================================================--
 
 cursor c_busca_cta_serv(cv_servicio varchar2) is
  select co_id, tmcode, customer_id
   from (select distinct dn.dn_num,
                         cu.custcode,
                         cc.cclname,
                         cc.ccfname,
                         cc.ccstreet,
                         cc.ccstreetno,
                         cc.cczip,
                         cc.cccity,
                         co.co_id,
                         co.subm_id,
                         co.plcode,
                         co.customer_id,
                         co.co_rel_type,
                         cap.vpn_id,
                         cap.cs_activ_date,
                         co.tmcode
           from curr_co_status     ch,
                ccontact           cc,
                customer           cu,
                contract_all       co,
                directory_number   dn,
                contr_services_cap cap,
                mputmtab           cmv
          where (co.co_id = cap.co_id)
            and (co.customer_id = cc.customer_id)
            and (co.svp_contract is null)
            and (dn.block_ind = 'N' or dn.dn_id is null)
            and (cap.dn_id = dn.dn_id)
            and (dn.dn_num = cv_servicio)
            and (cc.ccbill = 'X')
            and (co.customer_id = cu.customer_id)
            and (co.contract_template is null)
            and (co.co_id = ch.co_id)
            and (co.tmcode = cmv.tmcode)
          order by cap.cs_activ_date desc)
  where rownum <= 1;
 
 lv_sql                VARCHAR2(5000);
 resp                  NUMBER;
 --=====================================================================================--
  -- MODIFICADO POR: Katiuska Barreto.
  -- FECHA MOD:      29/09/2016
  -- PROYECTO:       [10417] Multipunto DTH TRX Postventa
  -- LIDER SUD:      SUD Jos� Bed�n
  -- LIDER SIS:      SIS Ricardo Arg�ello
  -- MOTIVO:         Modificaci�n de la inserci�n de occ de instalacci�n
  --                 para que se refleje el co_id del servicio en DTH Multipunto
  --=====================================================================================--
 PROCEDURE Registra_occ(PV_CUSTOMER_CODE  IN VARCHAR2,
                        PV_SERVICIO       IN VARCHAR2 DEFAULT NULL,
                        PN_SNCODE         IN NUMBER,
                        PV_VALID_FROM     IN VARCHAR2,
                        PV_REMARK         IN VARCHAR2,
                        PN_AMOUNT         IN NUMBER,  --10351 02/10/2015 cambio a valor numerico
                        PV_FEE_TYPE       IN VARCHAR2,
                        PN_FEE_CLASS      IN NUMBER,
                        PV_USERNAME       IN VARCHAR2,
                        PN_COID           IN OUT NUMBER,
                        PN_CUSTOMERID     OUT NUMBER,
                        PN_TMCODE         OUT NUMBER,
                        PN_PERIOD         OUT NUMBER,
                        PV_ERROR          OUT VARCHAR2,
                        PV_MESSAGE        OUT VARCHAR2) IS
  
    cursor c_verifica_cta(cv_custcode varchar2) is
      select x.customer_id customer_id_padre,
             y.customer_id customer_id_hijo,
             x.CSTYPE,
             x.CSDEACTIVATED,
             x.PRGCODE
        from customer_all x, customer_all y
       where x.custcode = cv_custcode
         and y.customer_id_high(+) = x.customer_id; 
  
    cursor c_verifica_cta_serv(cv_servicio varchar2, cn_customer_id number) is
      select c.co_id, c.tmcode, max(cs_activ_date) activ_date
        from directory_number a, contr_services_cap b, contract_all c
       where a.dn_num = cv_servicio
         and a.dn_id = b.dn_id
         and b.co_id = c.co_id
         and c.customer_id = cn_customer_id
         group by c.co_id, c.tmcode
         order by  activ_date desc;
         
    --10351 01/12/2015 Cursor para verificar que la co_id pertenezc a la cuenta
    CURSOR c_verifica_cta_coid(cn_customer NUMBER, cn_coid NUMBER) is
    SELECT 'X'
      FROM contract_all
     WHERE customer_id = cn_customer
       AND co_id = cn_coid;
       
    lc_verifica_cta_coid c_verifica_cta_coid%rowtype;
    --10351
    
    --10351 13/04/2016 Obtener co_id de la cuenta DTH
    CURSOR C_OBTIENE_COID_CTA (cn_customer NUMBER) IS
    SELECT CO_ID, FECHA, CH_STATUS
      FROM (SELECT Y.CO_ID, MAX(Z.CH_VALIDFROM) FECHA, Z.CH_STATUS
              FROM CUSTOMER_ALL X, CONTRACT_ALL Y, CURR_CO_STATUS Z
             WHERE X.CUSTOMER_ID = cn_customer
               AND Y.CUSTOMER_ID = X.CUSTOMER_ID
               AND Z.CO_ID = Y.CO_ID
               --AND Z.CH_STATUS <> 'o'
             GROUP BY Y.CO_ID, Z.CH_STATUS)
     ORDER BY FECHA DESC;
     
    LC_OBTIENE_COID_CTA  C_OBTIENE_COID_CTA%ROWTYPE;
    
    --10417 OBTENER EL CO_ID DEL SERVICIO DTH
     CURSOR C_OBTIENE_COID_SERV (CN_CUSTOMER NUMBER, CV_IDSERVICO VARCHAR) IS
      SELECT CO_ID, FECHA, CH_STATUS
        FROM (SELECT D.CO_ID, MAX(Z.CH_VALIDFROM) FECHA, Z.CH_STATUS
                FROM CUSTOMER_ALL   X,
                     CONTRACT_ALL  Y,
                     CURR_CO_STATUS Z, 
                     CL_SERVICIOS_CONTRATADOS@AXIS D
               WHERE X.CUSTOMER_ID =CN_CUSTOMER
                 AND Y.CUSTOMER_ID = X.CUSTOMER_ID           
                 AND D.ID_SERVICIO=CV_IDSERVICO
                 AND Z.CO_ID = Y.CO_ID
               GROUP BY D.CO_ID, Z.CH_STATUS);
               
    CURSOR C_CONTROL_MTP (CV_PARAMETRO VARCHAR , CN_TIPO_PARA NUMBER) IS
     SELECT G.VALOR FROM GV_PARAMETROS G
       WHERE G.ID_TIPO_PARAMETRO=CN_TIPO_PARA
       AND G.ID_PARAMETRO=CV_PARAMETRO;
              
    LC_OBTIENE_COID_SERV  C_OBTIENE_COID_SERV%ROWTYPE;
    LV_BAND_MTP               VARCHAR2(1); 
    
    --10417 
    
    lc_busca_cta_serv    c_busca_cta_serv%rowtype;
    lc_verifica_cta_serv c_verifica_cta_serv%rowtype;
    lc_verifica_cta      c_verifica_cta%rowtype;
  
    lb_existe_cta_serv boolean;
    lb_existe_serv     boolean;
    lb_verifica_cta    boolean;
    lb_es_cuenta_larga boolean := false;
    ln_period          number;
    ln_customer        number;
    ln_co_id           number;
    ln_tmcode          number;
    ln_formato_largo   number;
    lv_customer_code   varchar2(20);
    lv_servicio        varchar2(20);
    lv_error           varchar2(1000);
    Le_Error           exception;
    lb_existe_cta_coid boolean;--10351 JGU 01/12/2015
    
    ld_endate          date;
        
  BEGIN
    
    IF PV_CUSTOMER_CODE IS NOT NULL THEN
      --Verifico que no sea una cuenta larga
      IF PV_CUSTOMER_CODE NOT LIKE '1.%' then
        lb_es_cuenta_larga := true;
        ln_formato_largo   := instr(PV_CUSTOMER_CODE, '.', 1, 2);
      END IF;
    
      if nvl(ln_formato_largo, 0) > 0 then
        lv_customer_code := substr(PV_CUSTOMER_CODE, 1, instr(PV_CUSTOMER_CODE, '.', 1, 2) - 1);
      else
        lv_customer_code := PV_CUSTOMER_CODE;
      end if;
    END IF;
    
    if PV_CUSTOMER_CODE is not null then   
      open c_verifica_cta(lv_customer_code);
      fetch c_verifica_cta
        into lc_verifica_cta;
      lb_verifica_cta := c_verifica_cta%found;
      close c_verifica_cta;
    
      if not(lb_verifica_cta) then
        lv_error := 'No existe la cuenta en BSCS: Cuenta->' || lv_customer_code;
        raise Le_Error;
      else
        IF lb_es_cuenta_larga THEN
          ln_customer := lc_verifica_cta.customer_id_hijo;
        else
          ln_customer := lc_verifica_cta.customer_id_padre;
        end if;
        
        --10351 Se valida el estado de la cuenta si se encuentra inactiva se ingresa la fecha de desactivacion
        if lc_verifica_cta.CSTYPE = 'd' then 
          ld_endate := TRUNC(lc_verifica_cta.CSDEACTIVATED)-1;
        end if;
      end if;
      
           
      --Si recibe el servicio validar si posee la cuenta
     if PV_SERVICIO is not null then
       --10417 KBA VALIDA PARA QUE NO INGRESE CUANDO SEA DTH MULTIPUNTO
         OPEN C_CONTROL_MTP ('B_OCC_INST',10417);
         FETCH C_CONTROL_MTP INTO LV_BAND_MTP;
         CLOSE C_CONTROL_MTP;
         IF LV_BAND_MTP ='S' AND LC_VERIFICA_CTA.PRGCODE = 7 AND PN_COID = 1 THEN
          LN_TMCODE := 35;
          LN_CUSTOMER := LC_VERIFICA_CTA.CUSTOMER_ID_PADRE;
          OPEN C_OBTIENE_COID_SERV (LN_CUSTOMER,PV_SERVICIO);
          FETCH C_OBTIENE_COID_SERV INTO LC_OBTIENE_COID_SERV;
          CLOSE C_OBTIENE_COID_SERV;
          --10351 22/04/2016
          IF LC_OBTIENE_COID_SERV.CO_ID IS NOT NULL THEN
            IF LC_OBTIENE_COID_SERV.CH_STATUS = 'o' THEN
              IF (LC_OBTIENE_COID_SERV.FECHA >= SYSDATE-1) THEN
                ln_co_id:= LC_OBTIENE_COID_SERV.CO_ID;
              ELSE
                lv_error := 'El CO_ID '||LC_OBTIENE_COID_SERV.CO_ID|| ' se encuetra en status '||LC_OBTIENE_COID_SERV.CH_STATUS
                            || ' fecha: ' ||LC_OBTIENE_COID_SERV.FECHA;
                raise le_error;
              END IF;
            ELSE
              ln_co_id:= LC_OBTIENE_COID_SERV.CO_ID;
            END IF;
          ELSE
            lv_error := 'El Servicio  '||PV_SERVICIO||' no posee CO_ID en Bscs';
            raise le_error;
          END IF;          
      ELSE  
      --10417
        lv_servicio := obtiene_telefono_dnc_bs_int(PV_SERVICIO);
        
          OPEN c_verifica_cta_serv(lv_servicio, ln_customer);
          FETCH c_verifica_cta_serv
            INTO lc_verifica_cta_serv;
          lb_existe_cta_serv := c_verifica_cta_serv%found;
          CLOSE c_verifica_cta_serv;
        
          if lb_existe_cta_serv then
            ln_co_id  := lc_verifica_cta_serv.co_id;
            ln_tmcode := lc_verifica_cta_serv.tmcode;
          
            IF ln_co_id is null OR ln_tmcode is null THEN
              lv_error := 'No existe CO_ID/TMCODE para el servicio ingresado ' ||lv_servicio;
              RAISE le_error;
            END IF;
          else
            lv_error := 'El servicio no pertenece a la cuenta: Servicio->' || lv_servicio || ' Cuenta->' || PV_CUSTOMER_CODE;
            raise le_error;
          end if;
        end if;--10417
     else
        ln_tmcode := 35;
        ln_customer := lc_verifica_cta.customer_id_padre;
     end if;
    end if;
    
    --10351 JGU 01/12/2015 Si recibe el co_id validar que pertenezca a la cuenta DTH
    IF PV_SERVICIO IS NULL THEN
      IF PN_COID IS NOT NULL AND PN_COID <> 1 THEN
        OPEN c_verifica_cta_coid(ln_customer, PN_COID);
        FETCH c_verifica_cta_coid
         INTO lc_verifica_cta_coid;
        lb_existe_cta_coid := c_verifica_cta_coid%FOUND;
        CLOSE c_verifica_cta_coid;
        IF NOT(lb_existe_cta_coid) THEN
          lv_error := 'El CO_ID no pertenece a la cuenta: CO_ID->'||PN_COID||' Cuenta->'||lv_customer_code;
          raise le_error;
        ELSE
          ln_co_id := PN_COID;
        END IF;
      ---10351 13/04/2016 Verificar que la cuenta es DTH por PRGCODE 7
      ELSIF lc_verifica_cta.PRGCODE = 7 AND PN_COID = 1 THEN
        OPEN C_OBTIENE_COID_CTA (ln_customer);
        FETCH C_OBTIENE_COID_CTA INTO LC_OBTIENE_COID_CTA;
        CLOSE C_OBTIENE_COID_CTA;
        --10351 22/04/2016
        IF LC_OBTIENE_COID_CTA.CO_ID IS NOT NULL THEN
          IF LC_OBTIENE_COID_CTA.CH_STATUS = 'o' THEN
            IF (LC_OBTIENE_COID_CTA.FECHA >= SYSDATE-1) THEN
              ln_co_id:= LC_OBTIENE_COID_CTA.CO_ID;
            ELSE
              lv_error := 'El CO_ID '||LC_OBTIENE_COID_CTA.CO_ID|| ' se encuetra en status '||LC_OBTIENE_COID_CTA.CH_STATUS
                          || ' fecha: ' ||LC_OBTIENE_COID_CTA.FECHA;
              raise le_error;
            END IF;
          ELSE
            ln_co_id:= LC_OBTIENE_COID_CTA.CO_ID;
          END IF;
        ELSE
          lv_error := 'La cuenta  '||lv_customer_code||' no posee CO_ID en Bscs';
          raise le_error;
        END IF;
          
      END IF;
    END IF;
    --10351 FIN 01/12/2015    
 
    --solo recibe el telefono
    if PV_CUSTOMER_CODE IS NULL and PV_SERVICIO is not null then
      lv_servicio := obtiene_telefono_dnc_bs_int(PV_SERVICIO);

      open c_busca_cta_serv(lv_servicio);
      fetch c_busca_cta_serv
        into lc_busca_cta_serv;
      lb_existe_serv := c_busca_cta_serv%found;
      close c_busca_cta_serv;
    
      if lb_existe_serv then
        ln_customer := lc_busca_cta_serv.customer_id;
        ln_co_id    := lc_busca_cta_serv.co_id;
        ln_tmcode   := lc_busca_cta_serv.tmcode;
      
        IF ln_co_id is null OR ln_tmcode is null THEN
          lv_error := 'No existe CO_ID/TMCODE para el servicio ingresado ' ||lv_servicio;
          RAISE le_error;
        END IF;
      
      else
        lv_error := 'No existe datos para el servicio ingresado' ||lv_servicio;
        RAISE le_error;
      end if;
    end if;
        
    --si es R se envia -1 en Period, si es N se envia 1
    IF PV_FEE_TYPE = 'R' then
      ln_period := -1;
    ELSIF PV_FEE_TYPE = 'N' then
      ln_period := 1;
    Else
      lv_error := 'Fee_Type valido: (R)ecurrente o (N)o recurrente';
      Raise le_error;
    END IF;
     
      If Procesa_Occ(pn_customer_id => ln_customer,
                     pn_co_id       => ln_co_id,
                     pn_tmcode      => ln_tmcode,
                     pn_sncode      => PN_SNCODE,
                     pn_period      => ln_period,
                     pd_valid_from  => PV_VALID_FROM,
                     pv_remark      => PV_REMARK,
                     pn_amount      => PN_AMOUNT,--LN_AMOUNT,
                     pv_fee_type    => PV_FEE_TYPE,
                     pn_fee_class   => PN_FEE_CLASS,
                     pv_username    => PV_USERNAME,
                     Pv_Error       => lv_error,
                     Pd_Endate      => ld_endate) <> 0 Then
        RAISE Le_Error;
      End If;
    
    PN_COID   := ln_co_id;
    PN_CUSTOMERID :=  ln_customer;
    PN_TMCODE  := ln_tmcode;
    PN_PERIOD  := ln_period;
  
    PV_MESSAGE := 'Transaccion Existosa';
    PV_ERROR   := '0';
      
  Exception
    When Le_Error Then
      PV_MESSAGE := 'Registra_occ: '|| lv_error;
      PV_ERROR   := '-1';
    When Others Then
      PV_MESSAGE := 'Registra_occ: '||substr(sqlerrm, 1, 1000);
      PV_ERROR   := '-1';
          
  END Registra_occ;
 
 Function Procesa_Occ(pn_customer_id in number,
                      pn_co_id       in number,
                      pn_tmcode      in number,
                      pn_sncode      in number,
                      pn_period      in number,
                      pd_valid_from  in date,
                      pv_remark      in varchar2,
                      pn_amount      in number,
                      pv_fee_type    in varchar2,
                      pn_fee_class   in number,
                      pv_username    in varchar2,
                      Pv_Error       Out Varchar2,
                      Pd_Endate      in date default null) Return Number Is

   
    Le_Error exception;
    Lv_Error            Varchar2(300);
    Lv_Programa         Varchar2(60) := 'OCK_TRX.Procesa_Occ';
    Ln_Spcode           Mpulktmb.Spcode%Type;
    Lv_Accglcode        Mpulktmb.Accglcode%Type;
    Lv_Accserv_Catcode  Mpulktmb.Accserv_Catcode%Type;
    Lv_Accserv_Code     Mpulktmb.Accserv_Code%Type;
    Lv_Accserv_Type     Mpulktmb.Accserv_Type%Type;
    Lv_Accglcode_Disc   Mpulktmb.Accglcode_Disc%Type;
    Lv_Accglcode_Mincom Mpulktmb.Accglcode_Mincom%Type;
    Ln_Vscode           Mpulktmb.Vscode%Type;
    Ln_Evcode           Mpulkexn.Evcode%Type;
    Ln_Seqno            Fees.Seqno%Type;
    Ln_Currency         Forcurr.Fc_Id%Type := 19; ---'USD'
    
    --
    --10351 14/04/2016    
    CADENA 		VARCHAR2(500):=NULL;
    LN_FUPACKID 	NUMBER;
    LN_FUPVERSION      	NUMBER;
    LN_FUPSEQ          	NUMBER;
    LN_VERSION      	NUMBER;
    LN_FREEUNITSNUMBER 	NUMBER;
    --
    
    
  Begin

    If pn_Customer_Id Is Null Or pn_Tmcode Is Null Or pn_Sncode Is Null Or
       pn_Period Is Null Or pd_Valid_From Is Null Or pv_Remark Is Null Or
       (pn_Amount Is Null and pn_sncode <> 48) or pv_fee_type is null or pn_Fee_Class Is Null Then
       lv_error := 'Todos los campos de son obligatorios. Favor revisar: ' || Lv_Programa;
      Raise Le_Error;
    End If;
    
    --14/04/2016 Si es minutos portafolio obtener el pack_id
    IF pn_sncode = 48 THEN
      CADENA:= UPPER(TRIM(pv_Remark));
      LN_FUPACKID:= EXTRAE( CADENA, 'FU PACKAGE', ',', 1);
      IF NVL(LN_FUPACKID, 0) <> 0 THEN
        LN_FUPVERSION:= EXTRAE( CADENA, 'FU PACKAGE VERSION', ',', 2);
        LN_FUPSEQ:= EXTRAE( CADENA, 'FU ELEMENT', ',', 3);
        LN_VERSION:= EXTRAE( CADENA, 'FU ELEMENT VERSION', ',', 4);
        LN_FREEUNITSNUMBER := EXTRAE(CADENA, 'GRANTED', '.', 1);
        END IF;  
    END IF; 
      
    
      --Verifico si el occ a cargar en la fees esta duplicado
      If Valida_Duplicado(Pn_Customerid => pn_Customer_Id,
                          Pn_Coid       => pn_Co_Id,
                          Pn_Sncode     => pn_Sncode,
                          Pn_Tmcode     => pn_Tmcode,
                          Pn_Amount     => pn_Amount,
                          Pv_Valid_From => pd_Valid_From,
                          Pv_Remark     => pv_Remark,
                          Pv_Error      => Lv_Error) <> 0 Then
        Lv_Error :=  Lv_Error || ': ' || Lv_Programa;
        Raise Le_Error;
      End If;
    

    lv_sql := 'begin :response := ock_carga_fees.Obtiene_Modelo_Tarifa(Pn_Tmcode           => :Ln_Tmcode,' ||
              '                                    Pn_Sncode           => :Ln_Sncode,' ||
              '                                    Pn_Spcode           => :Ln_Spcode,' ||
              '                                    Pv_Accglcode        => :Lv_Accglcode,' ||
              '                                    Pv_Accserv_Catcode  => :Lv_Accserv_Catcode,' ||
              '                                    Pv_Accserv_Code     => :Lv_Accserv_Code,' ||
              '                                    Pv_Accserv_Type     => :Lv_Accserv_Type,' ||
              '                                    Pv_Accglcode_Disc   => :Lv_Accglcode_Disc,' ||
              '                                    Pv_Accglcode_Mincom => :Lv_Accglcode_Mincom,' ||
              '                                    Pn_Vscode           => :Ln_Vscode,' ||
              '                                    PV_ERROR            => :Lv_Error); end;';

    execute immediate lv_sql
      using out resp, pn_Tmcode, pn_Sncode, out Ln_Spcode, out Lv_Accglcode, out Lv_Accserv_Catcode, out Lv_Accserv_Code, out Lv_Accserv_Type, out Lv_Accglcode_Disc, out Lv_Accglcode_Mincom, out Ln_Vscode, out Lv_Error;

    IF resp <> 0 THEN
      Lv_Error := Lv_Error || ': ' || Lv_Programa;
      Raise Le_Error;
    END IF;

    lv_sql := 'begin :response := ock_carga_fees.Obtiene_Codigo_Evento(Pn_Sncode           => :Ln_Tmcode,' ||
              '                                    Pn_Evcode           => :Ln_Evcode,' ||
              '                                    PV_ERROR            => :Lv_Error); end;';

    execute immediate lv_sql
      using out resp, pn_Sncode, out Ln_Evcode, out Lv_Error;

    IF resp <> 0 THEN
      Lv_Error := Lv_Error || ': ' || Lv_Programa;
      Raise Le_Error;
    END IF;

    lv_sql := 'begin :response := ock_carga_fees.Obtiene_Secuencia_Fees(Pn_Customerid => :Ln_Customerid,' ||
              '                                     Pn_Seqno      => :Ln_Seqno,' ||
              '                                     PV_ERROR      => :Lv_Error); end;';

    execute immediate lv_sql
      using out resp, pn_Customer_Id, out Ln_Seqno, out Lv_Error;

    IF resp <> 0 THEN
      Lv_Error := Lv_Error || ': ' || Lv_Programa;
      Raise Le_Error;
    END IF;

    If Inserta_Fees(Pn_Customer_Id     => pn_Customer_Id,
                    Pn_Seqno           => Ln_Seqno,
                    Pv_Feetype         => pv_Fee_Type,
                    Pf_Amount          => pn_Amount,
                    Pv_Remark          => pv_Remark,
                    Pv_Glcode          => Lv_Accglcode,
                    Pd_Entdate         => nvl(Pd_Endate, Sysdate),
                    Pn_Period          => pn_Period,
                    Pv_Username        => pv_Username,
                    Pd_Validfrom       => pd_Valid_From,
                    Pn_Jobcost         => Null,
                    Pv_Billfmt         => Null,
                    Pv_Servcatcode     => Lv_Accserv_Catcode,
                    Pv_Servcode        => Lv_Accserv_Code,
                    Pv_Servtype        => Lv_Accserv_Type,
                    Pn_Coid            => pn_Co_Id,
                    Pf_Amountgross     => Null,
                    Pn_Currency        => Ln_Currency,
                    Pv_Glcodedisc      => Lv_Accglcode_Disc,
                    Pn_Jobcostiddisc   => Null,
                    Pv_Glcodemincom    => Lv_Accglcode_Mincom,
                    Pn_Jobcostidmincom => Null,
                    Pn_Recversion      => 0,
                    Pn_Cdrid           => Null,
                    Pn_Cdrsubid        => Null,
                    Pn_Udrbasepartid   => Null,
                    Pn_Udrchargepartid => Null,
                    Pn_Tmcode          => pn_Tmcode,
                    Pn_Vscode          => Ln_Vscode,
                    Pn_Spcode          => Ln_Spcode,
                    Pn_Sncode          => pn_Sncode,
                    Pn_Evcode          => Ln_Evcode,
                    Pn_Feeclass        => pn_Fee_Class,
                    Pn_Fupackid        => LN_FUPACKID,
                    Pn_Fupversion      => LN_FUPVERSION,
                    Pn_Fupseq          => LN_FUPSEQ,
                    Pn_Version         => LN_VERSION,
                    Pn_Freeunitsnumber => LN_FREEUNITSNUMBER,
                    Pd_lastmoddate     => Null,
                    Pv_Error           => Lv_Error) <> 0 Then
      Lv_Error := Lv_Error || ': ' || Lv_Programa;
      Raise Le_Error;
    End If;
    Return 0;

  Exception
    When Le_Error Then
      Pv_Error := Lv_Error;
      Return -1;
    When Others Then
      Pv_Error := 'Procesa_OCC: '|| substr(sqlerrm, 1, 200);
      Return -1;
  End Procesa_Occ;

  Function Valida_Duplicado(Pn_Customerid In Number,
                            Pn_Coid       In Number,
                            Pn_Sncode     In Number,
                            Pn_Tmcode     In Number,
                            Pn_Amount     In Float,
                            Pv_Valid_From In date,
                            Pv_Remark     In Varchar2,
                            Pv_Error      Out Varchar2) Return Number Is

    Ln_Duplicado number;
    Lv_Programa  Varchar2(65) := 'OCK_TRX.Valida_Duplicado';
  Begin
    if Pn_Coid is null then
      Select count(*)
        Into Ln_Duplicado
        From Fees
       Where Customer_Id = Pn_Customerid
         And Sncode = Pn_Sncode
         And Tmcode = Pn_Tmcode
         And amount = Pn_Amount
         and valid_from = Pv_Valid_From
         And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    else
     Select count(*)
        Into Ln_Duplicado
        From Fees
       Where Customer_Id = Pn_Customerid
         And Co_Id = Pn_Coid
         And Sncode = Pn_Sncode
         And Tmcode = Pn_Tmcode
         And amount = Pn_Amount
         and valid_from = Pv_Valid_From
         And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    end if;

    If Ln_Duplicado> 0 Then
      Pv_Error := 'El OCC ya se encuentra cargado en la tabla fees: ' || Lv_Programa ;
      Return 1;
    End If;
    Return 0;

  Exception
    When No_Data_Found Then
      Return 0;
    When Others Then
      Pv_Error := Substr(Sqlerrm, 1, 150) || ': ' || Lv_Programa;
      Return - 1;
  End Valida_Duplicado;
  
  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       In Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pd_lastmoddate     In date,
                        Pv_Error           Out Varchar2) Return Number Is
  
    Lv_Programa Varchar2(65) := 'OCK_TRX.INSERTA_FEES';
  
  Begin
  
    insert into fees
      (CUSTOMER_ID,
       SEQNO,
       FEE_TYPE,
       AMOUNT,
       REMARK,
       GLCODE,
       ENTDATE,
       PERIOD,
       USERNAME,
       VALID_FROM,
       JOBCOST,
       BILL_FMT,
       SERVCAT_CODE,
       SERV_CODE,
       SERV_TYPE,
       CO_ID,
       AMOUNT_GROSS,
       CURRENCY,
       GLCODE_DISC,
       JOBCOST_ID_DISC,
       GLCODE_MINCOM,
       JOBCOST_ID_MINCOM,
       REC_VERSION,
       CDR_ID,
       CDR_SUB_ID,
       UDR_BASEPART_ID,
       UDR_CHARGEPART_ID,
       TMCODE,
       VSCODE,
       SPCODE,
       SNCODE,
       EVCODE,
       FEE_CLASS,
       FU_PACK_ID,
       FUP_VERSION,
       FUP_SEQ,
       VERSION,
       FREE_UNITS_NUMBER,
       INSERTIONDATE,
       LASTMODDATE)
    values
      (Pn_Customer_Id,
       Pn_Seqno,
       Pv_Feetype,
       Pf_Amount,
       Pv_Remark,
       Pv_Glcode,
       Pd_Entdate,
       Pn_Period,
       Pv_Username,
       Pd_Validfrom,
       Pn_Jobcost,
       Pv_Billfmt,
       Pv_Servcatcode,
       Pv_Servcode,
       Pv_Servtype,
       Pn_Coid,
       Pf_Amountgross,
       Pn_Currency,
       Pv_Glcodedisc,
       Pn_Jobcostiddisc,
       Pv_Glcodemincom,
       Pn_Jobcostidmincom,
       Pn_Recversion,
       Pn_Cdrid,
       Pn_Cdrsubid,
       Pn_Udrbasepartid,
       Pn_Udrchargepartid,
       Pn_Tmcode,
       Pn_Vscode,
       Pn_Spcode,
       Pn_Sncode,
       Pn_Evcode,
       Pn_Feeclass,
       Pn_Fupackid,
       Pn_Fupversion,
       Pn_Fupseq,
       Pn_Version,
       Pn_Freeunitsnumber,
       sysdate,
       Pd_lastmoddate);
  
    Return 0;
  
  Exception
    When Others Then
      Pv_Error := Substr(Sqlerrm, 1, 150) || ': ' || Lv_Programa;--10351 07/10/2015
      Return - 1;
  End;

  FUNCTION EXTRAE(CADENA   VARCHAR2,
                  PALABRA  VARCHAR2,
                  CARACTER VARCHAR2,
                  REP      NUMBER) RETURN NUMBER IS

    RESULTADO NUMBER;
  BEGIN

    RESULTADO := SUBSTR(CADENA,
                        INSTR(CADENA, PALABRA) + LENGTH(PALABRA),
                        INSTR(CADENA, CARACTER, 1, REP) -
                        (INSTR(CADENA, PALABRA) + LENGTH(PALABRA)));

    RETURN RESULTADO;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END;

END OCK_TRX;
/
