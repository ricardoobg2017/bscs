CREATE OR REPLACE PACKAGE SMK_CONSULTA_CALIFICACIONES IS

  FUNCTION smf_getval(fv_trama VARCHAR2,
                      fv_campo VARCHAR2,
                      nth      NUMBER DEFAULT 1) RETURN VARCHAR2;

  FUNCTION smf_getparametros(pv_cod_parametro VARCHAR2) RETURN VARCHAR2;

  PROCEDURE smp_consulta_calificacion(pv_identificacion VARCHAR2,
                                      pv_gestion        VARCHAR2,
                                      pn_calificacion   OUT NUMBER,
                                      pv_categoria      OUT VARCHAR2,
                                      pv_procesa        OUT VARCHAR2,
                                      pv_error          OUT VARCHAR2);

  PROCEDURE smp_genera_rep_excel(pv_directorio IN VARCHAR2,
                                 pv_file       IN VARCHAR2,
                                 pv_query      IN VARCHAR2);
                                 
  PROCEDURE smp_envia_correo(pv_gestion   IN VARCHAR2,
                             pn_idenvio   IN NUMBER DEFAULT NULL,--SUD KBA
                             pv_categoria IN VARCHAR2,
                             pv_archivo   IN VARCHAR2,
                             pv_tipo      IN NUMBER, -- 0: exito / 1: error
                             pv_to        IN VARCHAR2 DEFAULT NULL,
                             pv_cc        IN VARCHAR2 DEFAULT NULL,
                             pv_error     IN OUT VARCHAR2);    
                                                          
  PROCEDURE smp_envia_correo_excel(pv_gestion   IN VARCHAR2,
                                   pv_categoria IN VARCHAR2,
                                   pv_tipo      IN NUMBER, -- 0: exito / 1: error
                                   pv_to        IN VARCHAR2 DEFAULT NULL,
                                   pv_cc        IN VARCHAR2 DEFAULT NULL,
                                   pv_error     OUT VARCHAR2); 
                                                               
END SMK_CONSULTA_CALIFICACIONES;
/
CREATE OR REPLACE PACKAGE BODY SMK_CONSULTA_CALIFICACIONES IS

  v_fh utl_file.file_type;
  
  --------------------------------------------------------------------------
  /** Creado por : SUD Dennise Pintado
  ** Fecha       : 01/07/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Funcion que obtiene el valor de la etiqueta XML 
                   despues del caracter igual
  -------------------------------------------------------------------------*/

  FUNCTION smf_getval(fv_trama VARCHAR2,
                      fv_campo VARCHAR2,
                      nth      NUMBER DEFAULT 1) RETURN VARCHAR2 IS
  
    ln_posini PLS_INTEGER;
    ln_posfin PLS_INTEGER;
    lv_dummy  VARCHAR2(1024);
  
  BEGIN
  
    IF nth = -1 THEN
      ln_posini := instr(fv_trama, fv_campo || '=', -1);
    ELSE
      ln_posini := instr(fv_trama, fv_campo || '=', 1, nth);
    END IF;
    IF ln_posini > 0 THEN
      lv_dummy  := substr(fv_trama, ln_posini);
      ln_posfin := instr(lv_dummy, ';');
      IF ln_posfin > 0 THEN
        lv_dummy := substr(lv_dummy, 1, ln_posfin - 1);
      END IF;
      lv_dummy := substr(lv_dummy, instr(lv_dummy, '=') + 1);
    END IF;
    RETURN lv_dummy;
  
  END;

  ------------------------------------------------------------------------
  /** Creado por : SUD Dennise Pintado
  ** Fecha       : 01/07/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera  
  ** Proposito   : Funcion que obtiene los parametros desde 
                   scp_dat.scp_parametros_procesos
  -----------------------------------------------------------------------*/

  FUNCTION smf_getparametros(pv_cod_parametro VARCHAR2) RETURN VARCHAR2 IS
  
    lv_valor_par   VARCHAR2(1000);
    lv_proceso     VARCHAR2(1000);
    lv_descripcion VARCHAR2(1000);
    ln_error       NUMBER;
    lv_error       VARCHAR2(1000);
  
  BEGIN
    scp_dat.sck_api.scp_parametros_procesos_lee(pv_cod_parametro,
                                                lv_proceso,
                                                lv_valor_par,
                                                lv_descripcion,
                                                ln_error,
                                                lv_error);
    RETURN lv_valor_par;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  ------------------------------------------------------------------------------
  /** Creado por : SUD Dennise Pintado
  ** Fecha       : 01/07/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera  
  ** Proposito   : Procedimiento para consultar la calificacion del cliente
                   desde AXIS
  ------------------------------------------------------------------------------*/

  PROCEDURE smp_consulta_calificacion(pv_identificacion VARCHAR2,
                                      pv_gestion        VARCHAR2,
                                      pn_calificacion   OUT NUMBER,
                                      pv_categoria      OUT VARCHAR2,
                                      pv_procesa        OUT VARCHAR2,
                                      pv_error          OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(300) := 'SMK_CONSULTA_CALIFICACIONES.SMP_CONSULTA_CALIFICACION ';
    --lv_error      VARCHAR2(3500);
    --lv_servicio_inf  VARCHAR2(20);
    --lv_req_parametro VARCHAR2(2000);
    --lx_respuestas    xmltype;
    --lv_fault_code    VARCHAR2(2000);
    --lv_fault_string  VARCHAR2(2000);
    --lv_resultado     VARCHAR2(2000);
    --le_error EXCEPTION;
    --lv_datasource VARCHAR2(100);
    --ln_id_req     NUMBER;
  
    cursor c_calificacion(cv_identificacion varchar2) is
      select calificacion, categoria_cli
        from dshare_bl.pr_cupo_precalificado
       where identificacion = cv_identificacion;
  
    lc_calificacion c_calificacion%ROWTYPE;
  
    cursor c_consulta_rango(cv_gestion varchar2, cv_calificacion number) is
      select a.tipo_rango, a.procesa
        from pr_asignacion_cupo a
       where a.id_gestion = cv_gestion
         and cv_calificacion >= a.valor_minimo
         and cv_calificacion <= a.valor_maximo
         and a.estado = 'A';
  
    lc_consulta_rango c_consulta_rango%ROWTYPE;
  
  BEGIN
  
    -- Consulto si el servicio est� activo an Axis
    /*ln_id_req       := to_number(smf_getparametros('GES_COB_PARAM_003'));
    lv_servicio_inf := smf_getparametros('GES_COB_PARAM_002');
    lv_datasource   := smf_getparametros('GES_COB_PARAM_001');
    
    lv_req_parametro := 'dsId=' || lv_datasource ||
                        ';pnIdServicioInformacion=' || lv_servicio_inf ||
                        ';pvParametroBind1=' || pv_identificacion;
    
    lx_respuestas := scp_dat.sck_soap_gtw.scp_consume_servicio(pn_id_req                   => ln_id_req,
                                                               pv_parametros_requerimiento => lv_req_parametro, --:req_parameters,
                                                               pv_fault_code               => lv_fault_code,
                                                               pv_fault_string             => lv_fault_string);
                                                                                              
    -- Verifica si hay error durante el consumo de servicio
    IF lv_fault_string IS NOT NULL THEN
      lv_error := 'Error en consumo de webServices ' || lv_fault_string ||
                  ' - ' || lv_fault_code;
      RAISE le_error;
    END IF;  
    
    lv_resultado := scp_dat.sck_soap_gtw.scp_obtener_valor(pn_id_req           => ln_id_req,
                                                           pr_respuesta        => lx_respuestas,
                                                           pv_nombre_parametro => 'pvresultadoOut');
    
    IF lv_resultado IS NULL THEN
      lv_error := 'No se puede obtener informacion del servicio';
      RAISE le_error;
    END IF;
    
    pn_calificacion := smf_getval(lv_resultado, 'CALIFICACION');
    pv_categoria := smf_getval(lv_resultado, 'CATEGORIA_CLI');*/
  
    -- Obtiene la calificaci�n del cliente
    open c_calificacion(pv_identificacion);
    fetch c_calificacion
      into lc_calificacion;
    close c_calificacion;
  
    -- Obtiene la categoria del cliente
    open c_consulta_rango(pv_gestion, lc_calificacion.calificacion);
    fetch c_consulta_rango
      into lc_consulta_rango;
    close c_consulta_rango;
  
    pn_calificacion := lc_calificacion.calificacion;
    pv_categoria := nvl(lc_consulta_rango.tipo_rango,'A'); --10798 SUD DPI
    pv_procesa := nvl(lc_consulta_rango.procesa,'S');
  
  EXCEPTION
    /*WHEN le_error THEN
    pv_error := lv_aplicacion || ' Error: ' || lv_error;*/
    WHEN OTHERS THEN
      pv_procesa := 'S';
      pv_error   := lv_aplicacion || ' Error: ' || substr(SQLERRM, 1, 200);
  END;

  ------------------------------------------------------------------------------------------
  /** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 12/10/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que sirve de plantilla para reporte Excel
  ------------------------------------------------------------------------------------------*/

  PROCEDURE smp_genera_rep_excel(pv_directorio IN VARCHAR2,
                                 pv_file       IN VARCHAR2,
                                 pv_query      IN VARCHAR2) IS
  
    --Inicio - Generaci�n del reporte
    PROCEDURE run_query(p_sql IN VARCHAR2) IS
    
      v_v_val VARCHAR2(4000);
      v_n_val NUMBER;
      v_d_val DATE;
      v_ret   NUMBER;
      c       NUMBER;
      d       NUMBER;
      col_cnt INTEGER;
      rec_tab dbms_sql.desc_tab;
    
    BEGIN
      c := dbms_sql.open_cursor;
      dbms_sql.parse(c, p_sql, dbms_sql.native);
      d := dbms_sql.execute(c);
      dbms_sql.describe_columns(c, col_cnt, rec_tab);
      -- bind variables to columns
      utl_file.put_line(v_fh, '<tr>');
      FOR j IN 1 .. col_cnt LOOP
        CASE rec_tab(j).col_type
          WHEN 1 THEN
            dbms_sql.define_column(c, j, v_v_val, 4000);
          WHEN 2 THEN
            dbms_sql.define_column(c, j, v_n_val);
          WHEN 12 THEN
            dbms_sql.define_column(c, j, v_d_val);
          ELSE
            dbms_sql.define_column(c, j, v_v_val, 4000);
        END CASE;
      END LOOP;
      -- Output the column headers
      FOR j IN 1 .. col_cnt LOOP
        utl_file.put_line(v_fh,
                          '<td width="120" align="center" bgcolor="#A4A4A4">');
        utl_file.put_line(v_fh, '<B>');
        utl_file.put_line(v_fh,
                          '<Type="String" font size="2" color="black" face="Book Antiqua" >' || rec_tab(j)
                          .col_name || '</font>');
        utl_file.put_line(v_fh, '</Type>');
        utl_file.put_line(v_fh, '</B>');
        utl_file.put_line(v_fh, '</td>');
      END LOOP;
      utl_file.put_line(v_fh, '</tr>');
      -- Output the data
      LOOP
        v_ret := dbms_sql.fetch_rows(c);
        EXIT WHEN v_ret = 0;
        utl_file.put_line(v_fh, '<tr>');
        FOR j IN 1 .. col_cnt LOOP
          CASE rec_tab(j).col_type
            WHEN 1 THEN
              dbms_sql.column_value(c, j, v_v_val);
              utl_file.put_line(v_fh, '<td width="120" align="center">');
              utl_file.put_line(v_fh,
                                '<Type="String" font   size="0"   color="black"   face="Book Antiqua" >' ||
                                v_v_val || '</font>');
              utl_file.put_line(v_fh, '</td>');
            WHEN 2 THEN
              dbms_sql.column_value(c, j, v_n_val);
              utl_file.put_line(v_fh, '<td width="90" align="center" >');
              utl_file.put_line(v_fh,
                                '<Type="Number"   font   size="2"   color="black"   face="Book Antiqua" >' ||
                                to_char(v_n_val) || '</:font>');
              utl_file.put_line(v_fh, '</td>');
            WHEN 12 THEN
              dbms_sql.column_value(c, j, v_d_val);
              utl_file.put_line(v_fh, '<td width="90" align="center">');
              utl_file.put_line(v_fh,
                                '<Type="DateTime"   font   size="2"   color="black"   face="Book Antiqua" >' ||
                                to_char(v_d_val, 'YYYY-MM-DD"T"HH24:MI:SS') ||
                                '</font>');
              utl_file.put_line(v_fh, '</td>');
            ELSE
              dbms_sql.column_value(c, j, v_v_val);
              utl_file.put_line(v_fh, '<td width="120" align="center">');
              utl_file.put_line(v_fh,
                                '<Type="String"   font   size="0"   color="black"   face="Book Antiqua" >' ||
                                v_v_val || '</font>');
              utl_file.put_line(v_fh, '</td>');
          END CASE;
        END LOOP;
        utl_file.put_line(v_fh, '</tr>');
      END LOOP;
      dbms_sql.close_cursor(c);
    END;
    --
    PROCEDURE start_workbook IS
    BEGIN
      utl_file.put_line(v_fh, '<Html>');
      utl_file.put_line(v_fh, '<Head>');
      utl_file.put_line(v_fh,
                        '</head> <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="APPLICATION/VND.MS-EXCEL">');
    
    END;
    PROCEDURE end_workbook IS
    BEGIN
      utl_file.put_line(v_fh, '</Html>');
    END;
    --
    PROCEDURE start_worksheet IS
    BEGIN
      utl_file.put_line(v_fh, '<body>');
      utl_file.put_line(v_fh, '<Table border="1">');
    END;
    PROCEDURE end_worksheet IS
    BEGIN
      utl_file.put_line(v_fh, '</Table>');
      utl_file.put_line(v_fh, '</body>');
    END;
    --
    PROCEDURE set_date_style IS
    BEGIN
    
      utl_file.put_line(v_fh,
                        '<tr><font NumberFormat   Format="dd/mm/yyyy\ hh:mm:ss"/</font></tr>');
    
    END;
    --Fin - Generaci�n del reporte                       
  BEGIN
    --
    v_fh := utl_file.fopen(upper(pv_directorio), pv_file, 'w', 32767);
    start_workbook;
    set_date_style;
    start_worksheet;
    run_query(pv_query);
    end_worksheet;
    end_workbook;
    utl_file.fclose(v_fh);
  
    BEGIN
      --Permisos a los archivos que se crean
      EXECUTE IMMEDIATE gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                             'PERMISO_ARCHIVO')
        USING IN pv_directorio, IN pv_file;
    EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line(substr('PROBLEMA PERMISOS: ' || SQLERRM,
                                    1,
                                    120));
    END;
  END;

  ------------------------------------------------------------------------------------------
  /** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 12/10/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que genera reportes en formato .csv y lo envia
  **               como adjunto por correo, si hay error envia un correo sin adjunto
  ------------------------------------------------------------------------------------------*/

  PROCEDURE smp_envia_correo(pv_gestion   IN VARCHAR2,
                             pn_idenvio   IN NUMBER DEFAULT NULL,--SUD KBA
                             pv_categoria IN VARCHAR2,
                             pv_archivo   IN VARCHAR2,
                             pv_tipo      IN NUMBER, -- 0: exito / 1: error
                             pv_to        IN VARCHAR2 DEFAULT NULL,
                             pv_cc        IN VARCHAR2 DEFAULT NULL,
                             pv_error     IN OUT VARCHAR2) IS
  
    lv_error         VARCHAR2(1000) := NULL;
    lv_resp          VARCHAR2(1000);
    lv_programa      VARCHAR2(100) := 'SMK_CONSULTA_CALIFICACIONES.SMP_ENVIA_CORREO';
    lv_asunto        VARCHAR2(100);
    lv_cuerpo_mail   VARCHAR2(5000);
    lv_from_name     VARCHAR2(100) := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                           'FROM_MAIL');
    lv_puerto        VARCHAR2(10) := NULL;
    lv_servidor_mail VARCHAR2(60) := NULL;
    lv_directorio    VARCHAR2(2000) := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                            'DIRECTORIO_REPORTE');
    lv_name_arch     VARCHAR2(2000);
    ln_valorretorno  NUMBER;
    lv_destinatario  VARCHAR2(2000);
    lv_cc            VARCHAR2(2000);
    lv_tiporegistro  VARCHAR2(5) := 'M';
    lv_dir_remoto    VARCHAR2(1500);
    lv_archivo_adj   VARCHAR2(1500);
    lv_user_remoto   VARCHAR2(50);
    lv_ip_remoto     VARCHAR2(50);
    ln_error         NUMBER;
    le_error         EXCEPTION;
    le_error2        EXCEPTION;
  
  BEGIN
  
    -- Toma valor del PV_TO en caso de que venga con datos
    IF pv_to IS NOT NULL THEN
      lv_destinatario := pv_to;
    ELSE
      lv_destinatario := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                              'MAIL_TO_' ||
                                                                              pv_gestion);
    END IF;
  
    -- Toma valor del PV_CC en caso de que venga con datos
    IF pv_cc IS NOT NULL THEN
      lv_cc := pv_cc;
    ELSE
      lv_cc := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                    'MAIL_CC_' ||
                                                                    pv_gestion);
    END IF;
  
    IF lv_destinatario IS NOT NULL THEN
      IF pv_tipo = 0 THEN
        lv_asunto      := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'ASUNTO_MAIL_OK');
        lv_cuerpo_mail := '[[' ||
                          gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'PLANTILLA_CORREO_OK') ||
                          ']]<<  ' || pv_categoria || ',' || pv_gestion ||
                          ' ,>>';
        lv_name_arch   := pv_archivo || '.Z';
        lv_directorio  := REPLACE(lv_directorio, '<PV_GESTION>', pv_gestion);
      
        --DESA:'/procesos/notificaciones/archivos' 
        --PROD: /procesos/gsioper/notificaciones/archivos
        lv_dir_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                              'DIR_REMOTO');
        --DESA:'192.168.37.141'
        --PROD:'130.2.18.27'
        lv_ip_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                             'IP_REMOTO');
      
        lv_user_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'USER_REMOTO');
        BEGIN
          EXECUTE IMMEDIATE 'begin
                                scp_dat.sck_ftp_gtw.SCP_TRANSFIERE_ARCHIVO(pv_ip => :1,
                                                                               pv_usuario =>  :2,
                                                                               pv_ruta_remota => :3,
                                                                               pv_nombre_arch => :4,
                                                                               pv_ruta_local => :5,
                                                                               pv_borrar_arch_local => :6,
                                                                               pn_error => :7,
                                                                               pv_error => :8); end;'
            USING lv_ip_remoto, --ipfinan27
                  lv_user_remoto, --usuario ftp
                  lv_dir_remoto, --dir remoto
                  lv_name_arch, --archivo
                  lv_directorio, --ruta donde se genera el archivo, Se debe dar permiso al Directorio a SCP_DAT, SCP_PRI, SCP_MIR
                  'N', 
                  OUT ln_error, 
                  OUT lv_error;
        
          IF lv_error IS NOT NULL THEN
            RAISE le_error2;
          ELSE
            lv_tiporegistro := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                    'TIPO_REPORTE');
            IF lv_tiporegistro = 'A' THEN
              lv_archivo_adj := '</ARCHIVO1=' || lv_name_arch ||
                                '|DIRECTORIO1=' || lv_dir_remoto || '|/>';
              lv_cuerpo_mail := lv_cuerpo_mail || '//' || lv_archivo_adj;
            END IF;
          END IF;
        
        EXCEPTION
          WHEN le_error2 THEN
            RAISE le_error;
          WHEN OTHERS THEN
            lv_error := substr(SQLERRM, 1, 200);
            RAISE le_error;
        END;
      ELSE
        lv_asunto      := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'ASUNTO_MAIL_ERR');
        lv_cuerpo_mail := '[[' ||
                          gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'PLANTILLA_CORREO_ERR') ||
                          ']]<<  ' || pv_categoria || ',' || pv_gestion || ',' || pv_error || --SUD KBA
                          ' ,>>';
      END IF;
      lv_asunto := REPLACE(lv_asunto, '<PV_GESTION>', pv_gestion);
      lv_asunto := REPLACE(lv_asunto, '<PN_IDENVIO>', pn_idenvio);--SUD KBA
    
      -- envio del correo con adjunto
      ln_valorretorno := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => lv_programa,
                                                                   pd_fechaenvio      => SYSDATE,
                                                                   pd_fechaexpiracion => SYSDATE +
                                                                                         1 / 24,
                                                                   pv_asunto          => lv_asunto,
                                                                   pv_mensaje         => lv_cuerpo_mail,
                                                                   pv_destinatario    => lv_destinatario || '//' ||
                                                                                         lv_cc,
                                                                   pv_remitente       => lv_from_name,
                                                                   pv_tiporegistro    => lv_tiporegistro,
                                                                   pv_clase           => 'IGC',
                                                                   pv_puerto          => lv_puerto,
                                                                   pv_servidor        => lv_servidor_mail,
                                                                   pv_maxintentos     => 2,
                                                                   pv_idusuario       => USER,
                                                                   pv_mensajeretorno  => lv_resp);
    
      IF lv_resp IS NOT NULL THEN
        lv_error := 'Error en el envio de correo electr�nico';
        RAISE le_error;
      END IF;
    ELSE
      lv_error := 'No se encontro Correo Destinatario ';
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_programa || ' Error: ' || substr(lv_error, 1, 100);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' Error: ' || substr(SQLERRM, 1, 200);
  END;

  ------------------------------------------------------------------------------------------
  /** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 12/10/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que genera reportes en formato .xls y lo envia
  **               como adjunto por correo, si hay error envia un correo sin adjunto
  ------------------------------------------------------------------------------------------*/

  PROCEDURE smp_envia_correo_excel(pv_gestion   IN VARCHAR2,
                                   pv_categoria IN VARCHAR2,
                                   pv_tipo      IN NUMBER, -- 0: exito / 1: error
                                   pv_to        IN VARCHAR2 DEFAULT NULL,
                                   pv_cc        IN VARCHAR2 DEFAULT NULL,
                                   pv_error     OUT VARCHAR2) IS

    lv_error         VARCHAR2(1000) := NULL;
    lv_resp          VARCHAR2(1000);
    lv_programa      VARCHAR2(100) := 'SMK_CONSULTA_CALIFICACIONES.SMP_ENVIA_CORREO_EXCEL';
    lv_asunto        VARCHAR2(100);
    lv_cuerpo_mail   VARCHAR2(5000);
    lv_from_name     VARCHAR2(100) := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                           'FROM_MAIL');
    lv_puerto        VARCHAR2(10) := NULL;
    lv_servidor_mail VARCHAR2(60) := NULL;
    lv_directorio    VARCHAR2(2000) := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                            'DIRECTORIO_REPORTE');
    lv_name_arch     VARCHAR2(2000);
    lv_sentencia     VARCHAR2(32650);
    ln_valorretorno  NUMBER;
    lv_destinatario  VARCHAR2(2000);
    lv_cc            VARCHAR2(2000);
    lv_tiporegistro  VARCHAR2(5) := 'M';
    lv_dir_remoto    VARCHAR2(1500);
    lv_archivo_adj   VARCHAR2(1500);
    lv_user_remoto   VARCHAR2(50);
    lv_ip_remoto     VARCHAR2(50);
    ln_error         NUMBER;
    le_error         EXCEPTION;
    le_error2        EXCEPTION;

  BEGIN

    -- Toma valor del PV_TO en caso de que venga con datos
    IF pv_to IS NOT NULL THEN
      lv_destinatario := pv_to;
    ELSE
      lv_destinatario := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                              'MAIL_TO_' ||
                                                                              pv_gestion);
    END IF;
    
    -- Toma valor del PV_CC en caso de que venga con datos
    IF pv_cc IS NOT NULL THEN
      lv_cc := pv_cc;
    ELSE
      lv_cc := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                    'MAIL_CC_' ||
                                                                    pv_gestion);
    END IF;

    IF lv_destinatario IS NOT NULL THEN
      IF pv_tipo = 0 THEN
        lv_asunto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                          'ASUNTO_MAIL_OK');
        lv_cuerpo_mail := '[[' ||
                          gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'PLANTILLA_CORREO_OK') ||
                          ']]<<  ' || pv_categoria || ',' || pv_gestion ||
                          ' ,>>';
        lv_name_arch := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                             'FILE_NAME');
        lv_name_arch := REPLACE(REPLACE(lv_name_arch,
                                        '<PV_GESTION>',
                                        pv_gestion),
                                '<FECHA>',
                                to_char(SYSDATE, 'yyyymmdd')) || '.xls';
        lv_sentencia := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                             'QUERY_REPOR_' ||
                                                                             pv_gestion);
        lv_directorio := REPLACE(lv_directorio, '<PV_GESTION>', pv_gestion);
      
        --genera reporte bitacora
        smp_genera_rep_excel(pv_directorio => lv_directorio,
                             pv_file       => lv_name_arch,
                             pv_query      => lv_sentencia);

        --DESA:'/procesos/notificaciones/archivos' 
        --PROD: /procesos/gsioper/notificaciones/archivos
        lv_dir_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                              'DIR_REMOTO');
        --DESA:'192.168.37.141'
        --PROD:'130.2.18.27'
        lv_ip_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                             'IP_REMOTO');
                                                                             
        lv_user_remoto := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'USER_REMOTO');
        BEGIN
          EXECUTE IMMEDIATE 'begin
                                  scp_dat.sck_ftp_gtw.SCP_TRANSFIERE_ARCHIVO(pv_ip => :1,
                                                                                 pv_usuario =>  :2,
                                                                                 pv_ruta_remota => :3,
                                                                                 pv_nombre_arch => :4,
                                                                                 pv_ruta_local => :5,
                                                                                 pv_borrar_arch_local => :6,
                                                                                 pn_error => :7,
                                                                                 pv_error => :8); end;'
            USING lv_ip_remoto, --ipfinan27
                  lv_user_remoto, --usuario ftp
                  lv_dir_remoto, --dir remoto
                  lv_name_arch, --archivo
                  lv_directorio, --ruta donde se genera el archivo, Se debe dar permiso al Directorio a SCP_DAT, SCP_PRI, SCP_MIR
                  'N', 
                  OUT ln_error, 
                  OUT lv_error;
          
          IF lv_error IS NOT NULL THEN
            RAISE le_error2;
          ELSE
            lv_tiporegistro := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                    'TIPO_REPORTE');
            IF lv_tiporegistro = 'A' THEN
              lv_archivo_adj := '</ARCHIVO1=' || lv_name_arch ||
                                '|DIRECTORIO1=' || lv_dir_remoto || '|/>';
              lv_cuerpo_mail := lv_cuerpo_mail || '//' || lv_archivo_adj;
            END IF;
          END IF;
        
        EXCEPTION
          WHEN le_error2 THEN
            RAISE le_error;
          WHEN OTHERS THEN
            lv_error := substr(SQLERRM, 1, 200);
            RAISE le_error;
        END;
      ELSE
        lv_asunto      := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'ASUNTO_MAIL_ERR');
        lv_cuerpo_mail := '[[' ||
                          gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                               'PLANTILLA_CORREO_ERR') ||
                          ']]<<  ' || pv_categoria || ',' || pv_gestion ||
                          ' ,>>';
      END IF;
      lv_asunto := REPLACE(lv_asunto, '<PV_GESTION>', pv_gestion);
      
      -- envio del correo con adjunto
      ln_valorretorno := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => lv_programa,
                                                                   pd_fechaenvio      => SYSDATE,
                                                                   pd_fechaexpiracion => SYSDATE +
                                                                                         1 / 24,
                                                                   pv_asunto          => lv_asunto,
                                                                   pv_mensaje         => lv_cuerpo_mail,
                                                                   pv_destinatario    => lv_destinatario || '//' ||
                                                                                         lv_cc,
                                                                   pv_remitente       => lv_from_name,
                                                                   pv_tiporegistro    => lv_tiporegistro,
                                                                   pv_clase           => 'IGC',
                                                                   pv_puerto          => lv_puerto,
                                                                   pv_servidor        => lv_servidor_mail,
                                                                   pv_maxintentos     => 2,
                                                                   pv_idusuario       => USER,
                                                                   pv_mensajeretorno  => lv_resp);
    
      IF lv_resp IS NOT NULL THEN
        lv_error := 'Error en el envio de correo electr�nico';
        RAISE le_error;
      END IF;
    ELSE
      lv_error := 'No se encontro Correo Destinatario ';
    END IF;

  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_programa || ' Error: ' || substr(lv_error, 1, 100);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' Error: ' || substr(SQLERRM, 1, 200);
  END;
    
END SMK_CONSULTA_CALIFICACIONES;
/
