create or replace package BLK_CARGA_OCC_TCF is

  -- Author  : RCABRERA
  -- Created : 16/03/2007 11:22:11
  -- Purpose :

  TYPE TVARCHAR IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR200 IS TABLE OF VARCHAR(200) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2;

  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean;

  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number;

  function BLF_obtiene_reg_procesados(pn_id_ejecucion number) return number ;

  procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number);

  procedure BLP_balancea_carga(pn_hilos integer);

  procedure BLP_carga_sndata;

  procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion number, pn_hilo number);

  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2);

  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null);

  procedure BLP_envia_sms(pv_destinatario varchar2, pv_mensaje varchar2);

  procedure BLP_envia_sms_grupo(pn_id_notificacion number,
                                pv_mensaje         varchar2);


  procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2);


  procedure BLP_detiene(pn_id_ejecucion number);

  procedure BLP_resume(pn_id_ejecucion number);

  procedure BLP_ejecuta(pv_usuario          varchar2,
                        pn_id_notificacion  number,
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar,
                        pd_entdate          date,
                        pv_respaldar        varchar,
                        pv_tabla_respaldo   varchar,
                        pn_id_ejecucion     out number);

end BLK_CARGA_OCC_TCF;
/
create or replace package body BLK_CARGA_OCC_TCF is

  tn_sncode          tnumber;
  tv_ACCGLCODE       tvarchar;
  tv_ACCSERV_CATCODE tvarchar;
  tv_ACCSERV_CODE    tvarchar;
  tv_ACCSERV_TYPE    tvarchar;
  tn_vscode          tnumber;
  tn_spcode          tnumber;
  tn_evcode          tnumber;

  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2 is
    lv_estado bl_carga_ejecucion.estado%type;

  begin

    select estado
      into lv_estado
      from bl_carga_ejecucion x
     where x.id_ejecucion = pn_id_ejecucion;
    return lv_estado;

  exception
    when others then

      return 'X';

  end;



  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean is
    ln_verifica number;
    ln_hilos    number;

  begin

    select nvl(count(*), 0)
      into ln_verifica
      from bl_carga_ejec_hilo_stat x
     where x.id_ejecucion = pn_id_ejecucion
       and estado = 'F';

    select cant_hilos
      into ln_hilos
      from bl_carga_ejecucion
     where id_ejecucion = pn_id_ejecucion;

    if ln_verifica = ln_hilos then
      return true;
    else
      return false;
    end if;

  exception
    when others then

      return false;

  end;

  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number is

    lv_tiempo varchar2(10);

    cursor c_obtiene is
      select substr(observacion, instr(observacion, ':') + 2)
        from bl_carga_ejecucion
       where id_ejecucion = pn_id_ejecucion;

  begin

    open c_obtiene;
    fetch c_obtiene
      into lv_tiempo;
    if c_obtiene%notfound then
      return - 1;
    end if;
    close c_obtiene;

    return to_number(lv_tiempo);

  exception when others then return
  0;

  end;

  function BLF_obtiene_reg_procesados(pn_id_ejecucion number) return number is

    ln_reg number;

    cursor c_obtiene is
      select reg_procesados
        from bl_carga_ejecucion
       where id_ejecucion = pn_id_ejecucion;

  begin

    open c_obtiene;
    fetch c_obtiene
      into ln_reg;
    if c_obtiene%notfound then
      return - 1;
    end if;
    close c_obtiene;

    return ln_reg;

  end;

  procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number) is

  --pragma autonomous_transaction;

  begin

    update bl_carga_ejecucion
       set observacion = substr(observacion, 1, instr(observacion, ':') + 1) ||
                         to_char(pn_valor)
     where id_ejecucion = pn_id_ejecucion;

     commit;

  end;

  procedure BLP_balancea_carga(pn_hilos integer) is

    --pragma autonomous_transaction;

    ln_hilo integer := 0;

  begin

    update BL_CARGA_OCC_TMP g
       set customer_id = (select customer_id
                            from contract_all
                           where co_id = g.co_id)
     where co_id is not null;

    update BL_CARGA_OCC_TMP j
       set customer_id = (select customer_id
                            from customer
                           where custcode = j.custcode)
     where custcode is not null;

    update BL_CARGA_OCC_TMP j
       set customer_id = (select customer_id
                            from customer
                           where customer_id = j.customer_id)
     where customer_id is not null
       and custcode is null;

    /*    update BL_CARGA_OCC_TMP g
       set customer_id = (select customer_id
                            from contract_all
                           where co_id = g.co_id)
     where customer_id is null;

    update BL_CARGA_OCC_TMP j
       set customer_id = (select customer_id
                            from customer
                           where customer_id = j.customer_id)
     where customer_id is not null;*/

    update BL_CARGA_OCC_TMP
       set status = 'E',
           error  = 'Problemas al obtener el customer_id de la tabla contract_all'
     where customer_id is null;

    update BL_CARGA_OCC_TMP
       set status = 'E', error = 'SNCode no existente'
     where sncode is null
        OR sncode not in (select sncode from mpusntab);

    for j in (select customer_id, count(*)
                from BL_CARGA_OCC_TMP
               where customer_id is not null
               group by customer_id
               order by 2 desc) loop
      update BL_CARGA_OCC_TMP
         set hilo = ln_hilo
       where customer_id = j.customer_id;
      ln_hilo := ln_hilo + 1;
      if ln_hilo = pn_hilos then
        ln_hilo := 0;
      end if;

    end loop;

    /*    lv_sql := 'update ' || pv_nombre_tabla || ' set hilo = mod(rownum, ' ||
              to_char(pn_hilos) || ') ';

    execute immediate lv_sql;*/

    commit;

  end;

  procedure BLP_carga_sndata is

    cursor x is
      select x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        from mpulktmb x, mpulkexn y;

    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;

  begin

    /*  lv_sql := '  select x.sncode,x.ACCGLCODE,x.ACCSERV_CATCODE, ' ||
           ' x.ACCSERV_CODE, x.ACCSERV_TYPE, ' ||
            ' x.VSCODE, x.SPCODE, y.EVCODE ' ||
         ' from mpulktmb x, mpulkexn y  where tmcode = 35 ' ||
          ' and x.sncode in (select distinct sncode from ' || pv_nombre_tabla ||
          ' ) and vscode = (select max(a.vscode)  from mpulktmb a ' ||
           ' where tmcode = 35  and sncode = 399)  and x.sncode = y.sncode ' ;

    OPEN LCV_CUR FOR lv_sql;*/
    open lcv_cur for
      select x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        from mpulktmb x, mpulkexn y
       where tmcode = 35
         and x.sncode in (select distinct sncode from BL_CARGA_OCC_TMP)
         and vscode = (select max(a.vscode)
                         from mpulktmb a
                        where tmcode = 35
                          and sncode = 399)
         and x.sncode = y.sncode;

    --LOOP
    FETCH LCV_CUR BULK COLLECT
      INTO tn_sncode, tv_ACCGLCODE, tv_ACCSERV_CATCODE, tv_ACCSERV_CODE, tv_ACCSERV_TYPE, tn_vscode, tn_spcode, tn_evcode;
    --EXIT WHEN LCV_CUR%NOTFOUND;
    --END LOOP;
    CLOSE LCV_CUR;

    FOR I in tn_sncode.first .. tn_sncode.last LOOP
      tv_ACCGLCODE(tn_sncode(i)) := tv_ACCGLCODE(i);
      tv_ACCSERV_CATCODE(tn_sncode(i)) := tv_ACCSERV_CATCODE(i);
      tv_ACCSERV_CODE(tn_sncode(i)) := tv_ACCSERV_CODE(i);
      tv_ACCSERV_TYPE(tn_sncode(i)) := tv_ACCSERV_TYPE(i);
      tn_vscode(tn_sncode(i)) := tn_vscode(i);
      tn_spcode(tn_sncode(i)) := tn_spcode(i);
      tn_evcode(tn_sncode(i)) := tn_evcode(i);

    END LOOP;

  end;

  procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion number, pn_hilo number) is

    --pragma autonomous_transaction;

    /*procedure BLP_Carga_Occ_Co_id(pv_usuario     varchar2,
    pv_recurrencia varchar2,
    pv_remark      varchar2,
    pd_entdate     date,
    pv_tabla       varchar2,
    pn_hilo        number,
    pn_limit       number) is*/
    cursor c_config is
      select *
        from bl_carga_ejecucion
       where id_ejecucion = pn_id_ejecucion;

    lc_config c_config%rowtype;

    cursor c_carga is
      select h.rowid, h.amount, h.sncode, h.co_id, h.customer_id
        from BL_CARGA_OCC_TMP H
       where status is null
         and hilo = pn_hilo
         and customer_id is not null;

    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;

    TV_ROWID       TVARCHAR;
    TN_AMOUNT      TNUMBER;
    TN_SNCODE      TNUMBER;
    TN_COID        TNUMBER;
    TN_CUSTOMER_ID TNUMBER;

    TN_SEQ_BY_CUSTID TNUMBER;
    TN_EXISTS_CUSTID TNUMBER;

    cursor c_seq(cn_customer_id number) is
      SELECT nvl(max(seqno), 0) seqno
        FROM fees
       WHERE customer_id = cn_customer_id;

    lc_seq c_seq%rowtype;

    cursor c_hilo_seq is
      SELECT nvl(max(seq), 0) seq
        FROM bl_carga_ejec_hilo_detail bc
       WHERE bc.id_ejecucion = pn_id_ejecucion
         and bc.hilo = pn_hilo;

    cursor c_cust_id_status(cn_customer_id number) is
      SELECT cstype, csdeactivated
        from customer
       where customer_id = cn_customer_id;

    lc_cust_id_status c_cust_id_status%rowtype;

    cursor c_co_id_status(cn_co_id number) is
      SELECT entdate
        from contract_history
       where ch_status = 'd'
         and co_id = cn_co_id;

    lc_co_id_status c_co_id_status%rowtype;

    ln_hilo_seq number;

    le_error exception;
    ln_period number;

    ld_entdate    date;
    ld_valid_from date;

    tn_sncode2            tnumber;
    tv_ACCGLCODE2         tvarchar;
    tv_ACCSERV_CATCODE2   tvarchar;
    tv_ACCSERV_CODE2      tvarchar;
    tv_ACCSERV_TYPE2      tvarchar;
    tn_vscode2            tnumber;
    tn_spcode2            tnumber;
    tn_evcode2            tnumber;
    tn_ln_seq             tnumber;
    td_entdate            tdate;
    td_valid_from         tdate;
    td_entdate_by_cust    tdate;
    td_valid_from_by_cust tdate;
    tv_error              tvarchar;

    ln_minutos         number;
    ln_registros_total number;

    lv_sql     varchar2(300);
    lv_estado  bl_carga_ejecucion.estado%type;
    ln_counter number := 0;
    lv_error   varchar2(200);
    lb_nohaydatos boolean;
    
 
  BEGIN

    lv_Sql := 'ALTER TABLE BL_CARGA_OCC_TMP PARALLEL (DEGREE 5)';

    open c_config;
    fetch c_config
      into lc_config;
    close c_config;

    open c_hilo_seq;
    fetch c_hilo_seq
      into ln_hilo_seq;
    close c_hilo_seq;

    ln_hilo_seq := ln_hilo_seq + 1;

    insert into bl_carga_ejec_hilo_detail
    values
      (pn_id_ejecucion, pn_hilo, 'P', 0, ln_hilo_seq, sysdate, null, null);

    update bl_carga_ejec_hilo_stat
       set estado = 'P'
     where id_ejecucion = pn_id_ejecucion
       and hilo = pn_hilo;

    commit;

    BLP_carga_sndata;

    open c_carga;

    loop

      TV_ROWID.delete;
      tv_error.delete;
      TN_AMOUNT.delete;
      TN_SNCODE.delete;
      TN_COID.delete;
      tn_sncode2.delete;
      tv_ACCGLCODE2.delete;
      tv_ACCSERV_CATCODE2.delete;
      tv_ACCSERV_CODE2.delete;
      tv_ACCSERV_TYPE2.delete;
      tn_vscode2.delete;
      tn_spcode2.delete;
      tn_evcode2.delete;
      tn_customer_id.delete;
      tn_ln_seq.delete;
      ln_counter := 0;

      lb_nohaydatos := true;

      fetch c_carga bulk collect
        into TV_ROWID, TN_AMOUNT, TN_SNCODE, TN_COID, TN_CUSTOMER_ID limit lc_config.cant_reg_mem;
      --        into TV_ROWID, TN_AMOUNT, TN_SNCODE, TN_COID, TN_CUSTOMER_ID limit 3000;

      if TV_ROWID.count > 0 THEN

      lb_nohaydatos := false;

        FOR I IN TV_ROWID.FIRST .. TV_ROWID.LAST

         LOOP

          tv_ACCGLCODE2(i) := tv_ACCGLCODE(tn_sncode(i));
          tv_ACCSERV_CATCODE2(i) := tv_ACCSERV_CATCODE(tn_sncode(i));
          tv_ACCSERV_CODE2(i) := tv_ACCSERV_CODE(tn_sncode(i));
          tv_ACCSERV_TYPE2(i) := tv_ACCSERV_TYPE(tn_sncode(i));
          tn_vscode2(i) := tn_vscode(tn_sncode(i));
          tn_spcode2(i) := tn_spcode(tn_sncode(i));
          tn_evcode2(i) := tn_evcode(tn_sncode(i));
          tv_error(i) := null;

          if not TN_SEQ_BY_CUSTID.exists(TN_CUSTOMER_ID(i)) then

            --Obtener el secuencial
            open c_seq(TN_CUSTOMER_ID(i));
            fetch c_seq
              into lc_seq;
            if c_seq%notfound then
              tn_ln_seq(i) := 1;
            else
              tn_ln_seq(i) := lc_seq.seqno + 1;
            end if;
            close c_seq;

            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := tn_ln_seq(i);

            --Determino si la cuenta est� desactivada
            open c_cust_id_status(TN_CUSTOMER_ID(i));
            fetch c_cust_id_status
              into lc_cust_id_status;

            if c_cust_id_status%notfound then
              tv_error(i) := 'Problemas al obtener el estado del customer_id';
              --TN_COID.delete(i);
              --close c_cust_id_status;
              --raise le_error;
            end if;

            if lc_cust_id_status.cstype = 'd' then
              --S� est� desactivada, entonces lo ingreso un d�a antes de fecha desact
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_cust_id_status.csdeactivated) - 1;
            else
              --Si no, tomo la fecha del param de entrada
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);
            end if;

            td_valid_from_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);

            close c_cust_id_status;

            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));

          else

            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) + 1;

            tn_ln_seq(i) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i));

            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));

          end if;

          -- if TN_COID.exists(i) then
          if TN_COID(i) is not null then
            --Cargos a nivel de CO_ID
            --Determino si el co_id esta desactivado
            open c_co_id_status(TN_COID(i));
            fetch c_co_id_status
              into lc_co_id_status;

            if c_co_id_status%notfound then
              close c_co_id_status;
            else
              td_entdate(i) := trunc(lc_co_id_status.entdate) - 1;
              close c_co_id_status;
            end if;

          end if;

          if lc_config.recurrencia = 'N' then
            ln_period := 1;
          else
            ln_period := 99;
          end if;

          ln_counter := ln_counter + 1;
          -- end if;

        end loop;
    
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          INSERT /*+ APPEND */ INTO Fees_Tcf
            (customer_id,
             seqno,
             fee_type,
             glcode,
             entdate,
             period,
             username,
             valid_from,
             servcat_code,
             serv_code,
             serv_type,
             co_id,
             amount,
             remark,
             currency,
             glcode_disc,
             glcode_mincom,
             tmcode,
             vscode,
             spcode,
             sncode,
             evcode,
             fee_class)
          values
            (tn_customer_id(m),
             tn_ln_seq(m),
             --null,
             lc_config.recurrencia,
             tv_ACCGLCODE2(M),
             td_entdate(m),
             ln_period,
             lc_config.usuario,
             td_valid_from(m),
             tv_ACCSERV_CATCODE2(M),
             tv_ACCSERV_CODE2(M),
             tv_ACCSERV_TYPE2(M),
             tn_coid(M),
             tn_amount(M),
             lc_config.remark,
             19,
             tv_ACCGLCODE2(M),
             tv_ACCGLCODE2(M),
             35,
             tn_VSCODE2(M),
             tn_SPCODE2(M),
             tn_sncode(M),
             tn_EVCODE2(M),
             3);
      
             
          --pv_error := substr(sqlerrm,1,200);
         
         -- INSERT DE PRUEBAS GSI BILLING       
         /*FORALL M IN TN_COID.FIRST .. TN_COID.LAST
          update blp_carga_occ_tmp
             set status = 'P', error = null
           where rowid = TV_ROWID(m);

        FORALL M IN tv_error.FIRST .. tv_error.LAST
          update blp_carga_occ_tmp
             set status = 'E', error = tv_error(m)
           where rowid = TV_ROWID(m)
             and tv_error(m) is not null;*/
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          update bl_carga_occ_tmp
             set status = 'P', error = null
           where rowid = TV_ROWID(m);

        update bl_carga_ejecucion
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion;

        update bl_carga_ejec_hilo_stat
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;

        update bl_carga_ejec_hilo_detail
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;


        If BLF_obtiene_estado(pn_id_ejecucion => pn_id_ejecucion) = 'U' then

          update bl_carga_ejec_hilo_stat
             set estado = 'U'
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo;

          update bl_carga_ejec_hilo_detail
             set estado = 'U', fecha_fin = sysdate
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo
             and seq = ln_hilo_seq;

          exit;

        end if;

      END IF;

      IF c_carga%NOTFOUND OR lb_nohaydatos then
        update bl_carga_ejec_hilo_stat
           set estado = 'F'
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;

        update bl_carga_ejec_hilo_detail
           set estado = 'F', fecha_fin = sysdate
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;

        commit;

        exit;

      end if;

      begin
        select trunc((sysdate - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          into ln_minutos, ln_registros_total
          from bl_carga_ejecucion where id_ejecucion = pn_id_ejecucion;

      exception
        when others then
          null;

      end;

    --ln_registros_total := BLF_obtiene_reg_procesados(pn_id_ejecucion => pn_id_ejecucion);
      begin
        select trunc((sysdate - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          into ln_minutos, ln_registros_total
          from bl_carga_ejecucion where id_ejecucion = pn_id_ejecucion;

      exception
        when others then
          null;

      end;

    --Notificaciones peri�dicas
    if trunc((sysdate - lc_config.fecha_inicio) * 24 * 60)  >=
       BLF_obtiene_ultima_notif(pn_id_ejecucion => pn_id_ejecucion) + lc_config.tiempo_notif then

      BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion,
                          pv_mensaje         => 'Avance proceso Carga OCC. Usuario: ' ||
                                                lc_config.usuario ||
                                                ' Minutos: ' ||
                                                to_char(ln_minutos) ||
                                                ' Total reg: ' ||
                                                ln_registros_total);

      Blp_Setea_Ultima_Notif(pn_id_ejecucion => pn_id_ejecucion ,pn_valor =>
        trunc((sysdate - lc_config.fecha_inicio) * 24 * 60) + lc_config.tiempo_notif );

    end if;

    commit;

    end loop;

    close c_carga;

    --Ya finaliz� el proceso?
    if BLF_verifica_finalizacion(pn_id_ejecucion => pn_id_ejecucion) then
      BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                    pd_fecha_fin    => sysdate,
                    pv_observacion  => 'Finalizado',
                    pv_estado       => 'F');

      if lc_config.respaldar in ('Y', 'S') then
        BLP_Respalda_tabla(pv_tabla_respaldar => lc_config.nombre_respaldo);
      end if;

      begin
        select trunc((sysdate - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          into ln_minutos, ln_registros_total
          from bl_carga_ejecucion where id_ejecucion = pn_id_ejecucion;

      exception
        when others then
          null;

      end;

      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion,
                          pv_mensaje         => 'Fin proceso Carga OCC. Usuario: ' ||
                                                lc_config.usuario ||' Remark: '||lc_config.remark||
                                                ' Minutos: ' ||
                                                to_char(ln_minutos) ||
                                                ' Total reg: ' ||
                                                ln_registros_total);


      BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion,
                          pv_mensaje         => 'Fin proceso Carga OCC. Usuario: ' ||
                                                lc_config.usuario ||
                                                ' Minutos: ' ||
                                                to_char(ln_minutos) ||
                                                ' Total reg: ' ||
                                                ln_registros_total);

    end if;

    commit;

  exception
    when others then
      lv_error := substr(sqlerrm, 1, 199);
      BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                    pd_fecha_fin    => sysdate,
                    pv_observacion  => lv_error,
                    pv_estado       => 'E');

      BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion,
                          pv_mensaje         => 'Error proceso Carga OCC: '
                          || substr(lv_error,1,100));

      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion,
                          pv_mensaje         => 'Error proceso Carga OCC: '
                          || sqlerrm);


      update bl_carga_ejec_hilo_stat
         set estado = 'E'
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo;

      update bl_carga_ejec_hilo_detail
         set estado = 'E', fecha_fin = sysdate, observacion = lv_error
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo
         and seq = ln_hilo_seq;

      commit;

  end BLP_Carga_Occ_Co_id;

  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2) is

    lv_sql varchar2(500);

  begin

    lv_sql := 'CREATE TABLE ' || pv_tabla_respaldar || ' AS ' ||
              ' select * from BL_CARGA_OCC_TMP ';

    execute immediate lv_sql;

  exception
    when others then
      null;

  end;

  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null) is

    --pragma autonomous_transaction;

  begin
    update bl_Carga_ejecucion
       set fecha_inicio   = nvl(pd_fecha_inicio, fecha_inicio),
           fecha_fin      = nvl(pd_fecha_fin, fecha_fin),
           estado         = nvl(pv_estado, estado),
           reg_procesados = nvl(pn_reg_procesados, reg_procesados),
           observacion    = nvl(pv_observacion, observacion)
     where id_ejecucion = pn_id_ejecucion;

    commit;

  end;

  procedure BLP_envia_sms(pv_destinatario varchar2, pv_mensaje varchar2) is

    pragma autonomous_transaction;

    ln_res number;
    lv_msg_error varchar2(200);

  begin

/*    ln_res := sysadm.sms_enviar_ies@bscs_to_rtx_link(pv_destinatario,
                                              replace(pv_mensaje, ' ', '_'));
*/
 /* swk_sms.send@axis(nombre_servidor => 'sms',
                 id_servciio => pv_destinatario,
                 pv_mensaje => pv_mensaje,
                 pv_msg_err => lv_msg_error);*/

    --Dbms_Output.put_line(pv_destinatario || replace(pv_mensaje, ' ', '_'));

    commit;

    exception when others then null;
  end;




  procedure BLP_envia_sms_grupo(pn_id_notificacion number,
                                pv_mensaje         varchar2) is

    cursor c_grupo_noti is
      select b.destinatario
        from bl_carga_grupo_notificacion a, bl_carga_notifica b
       where a.id_notifica = b.id_notifica
         and a.estado = 'A'
         and b.estado = 'A'
         and b.tipo = 'SMS'
         and a.id_notificacion = pn_id_notificacion;

  begin

    for j in c_grupo_noti loop
      BLP_envia_sms(j.destinatario, pv_mensaje);
    end loop;

  end;
 procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2) is
 pragma autonomous_transaction;
    cursor c_grupo_noti is
      select b.destinatario
        from bl_carga_grupo_notificacion a, bl_carga_notifica b
       where a.id_notifica = b.id_notifica
         and a.estado = 'A'
         and b.estado = 'A'
         and b.tipo='MAIL'
         and a.id_notificacion = pn_id_notificacion;

  lv_remitentes varchar2(4000):=null;
  begin

  for i in c_grupo_noti loop
      lv_remitentes:=lv_remitentes||i.destinatario||';';
  end loop;
  lv_remitentes:=substr(lv_remitentes,1,length(lv_remitentes)-1);
  
  send_mail.mail@axis(from_name => 'blk_carga_occ@conecel.com',
                 to_name => lv_remitentes,
                 cc => null,
                 cco => null,
                 subject => 'Proceso BLK_CARGA_OCC',
                 message => pv_mensaje);

  commit;
  exception when others then null;
  end;
  procedure BLP_detiene(pn_id_ejecucion number) is

    --pragma autonomous_transaction;

  begin

    BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                  pv_estado       => 'U',
                  pv_observacion  => 'En pausa. Detenido manualmente');

  end;

  procedure BLP_resume(pn_id_ejecucion number) is

    --pragma autonomous_transaction;

    cursor c_hilo is
      select cant_hilos
        from bl_carga_ejecucion
       where id_ejecucion = pn_id_ejecucion;

    ln_cantidad_hilos number;
    ln_job            number;
    lv_ejecutar       varchar2(400);

  begin

    open c_hilo;
    fetch c_hilo
      into ln_cantidad_hilos;
    close c_hilo;

    BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                  pv_estado       => 'P',
                  pv_observacion  => 'Procesando: 0');

    for i in 0 .. ln_cantidad_hilos - 1 loop
      lv_ejecutar := 'blk_carga_occ.blp_carga_occ_co_id(' ||
                     to_char(pn_id_ejecucion) || ',' || to_char(i) || ');';

      dbms_job.submit(job => ln_job, what => lv_ejecutar);

    end loop;

    commit;

  end;

  procedure BLP_ejecuta(pv_usuario          varchar2,
                        pn_id_notificacion  number,
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar,
                        pd_entdate          date,
                        pv_respaldar        varchar,
                        pv_tabla_respaldo   varchar,
                        pn_id_ejecucion     out number) is

    ln_job          number;
    lv_ejecutar     varchar2(500);
    ln_id_ejecucion number;

    cursor c_id_ejec is
      SELECT nvl(max(id_ejecucion), 0) id_ejecucion
        FROM bl_carga_ejecucion;

  begin

    open c_id_ejec;
    fetch c_id_ejec
      into ln_id_ejecucion;
    close c_id_ejec;

    ln_id_ejecucion := ln_id_ejecucion + 1;

    insert into bl_carga_ejecucion
    values
      (ln_id_ejecucion,
       pv_usuario,
       sysdate,
       null,
       'L',
       0,
       pn_id_notificacion,
       pn_tiempo_notif,
       pn_cantidad_hilos,
       pn_cantidad_reg_mem,
       pv_recurrencia,
       pv_remark,
       pd_entdate,
       pv_respaldar,
       pv_tabla_respaldo,
       null);


    BLP_envia_correo_grupo(pn_id_notificacion => pn_id_notificacion,
                          pv_mensaje         =>'Inicio del proceso Carga OCC. Usuario: ' ||
                                              pv_usuario ||' Remark: '||pv_Remark|| ' Fecha: ' ||
                                              to_char(sysdate,
                                                      'dd/mm/yyyy hh24:mi'));

    BLP_envia_sms_grupo(pn_id_notificacion => pn_id_notificacion,
                        pv_mensaje         => 'Inicio del proceso Carga OCC. Usuario: ' ||
                                              pv_usuario || ' Fecha: ' ||
                                              to_char(sysdate,
                                                      'dd/mm/yyyy hh24:mi'));

    BLP_actualiza(pn_id_ejecucion => ln_id_ejecucion,
                  pv_estado       => 'P',
                  pv_observacion  => 'Procesando: 0');

    BLP_balancea_carga(pn_hilos => pn_cantidad_hilos);

    for i in 0 .. pn_cantidad_hilos - 1 loop
      lv_ejecutar := 'blk_carga_occ_tcf.blp_carga_occ_co_id(' ||
                     to_char(ln_id_ejecucion) || ',' || to_char(i) || ');';

      dbms_job.submit(job => ln_job, what => lv_ejecutar);

      insert into bl_carga_ejec_hilo_stat
      values
        (ln_id_ejecucion, to_char(i), 'P', 0);
      commit;

    end loop;

    pn_id_ejecucion := ln_id_ejecucion;

  end;

end;
/
