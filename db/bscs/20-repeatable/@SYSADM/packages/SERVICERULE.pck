CREATE OR REPLACE PACKAGE ServiceRule
IS
/*
** @(#) but_bscs/bscs/database/scripts/oraproc/packages/rating/ServiceRule.sph, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/4, 22/04/02
**
**
**
** Reference : 81478_1, Mapped Component PDE
** Author: Inna Rivkin,Sema Telecom
**
** Filenames: ServiceRule.sph - Package header
**            ServiceRule.spb - Package body
**
** Overview:
**
** This package contains methods to add, delete rules (rows) to/from table MKT_SERVICE_CONSISTENCY_RULE
**
**
**
** Major Modifications (when, who, what)
**
** 07/01 -IR -  Initial Version
** 04/02 -HSZ- add PrintBodyVersion
*/

-- Basic procedures

   PROCEDURE InsertRule
   (
      ponMarket             IN mpdsctab.scslprefix%TYPE, -- char
      ponRatePlan           IN mkt_service_consistency_rule.tmcode%TYPE, -- integer
      ponRatePlanVersion    IN mkt_service_consistency_rule.vscode%TYPE, -- integer
      ponServicePackage     IN mkt_service_consistency_rule.spcode%TYPE, -- integer
      ponService            IN mkt_service_consistency_rule.sncode%TYPE, -- integer
      posDependType     IN mkt_service_consistency_rule.dependency_type%TYPE, -- char, values 'N' or 'P'
      ponDependentServicePackage
                            IN mkt_service_consistency_rule.dependent_spcode%TYPE, -- integer
      ponDependentService   IN mkt_service_consistency_rule.dependent_sncode%TYPE, -- integer
      -- Error indicator
   -- 0 - succes
   -- 1 - no data found
   -- 2 - duplicate value of the unique index
   -- 3 - wrong/missing parameters
   -- 4 - undefined error
      ponErrorCode          OUT NUMBER
    );

   PROCEDURE DeleteRule
   (
      ponMarket             IN mpdsctab.scslprefix%TYPE, -- char
      ponRatePlan           IN mkt_service_consistency_rule.tmcode%TYPE, -- integer
      ponRatePlanVersion    IN mkt_service_consistency_rule.vscode%TYPE, -- integer
      ponServicePackage     IN mkt_service_consistency_rule.spcode%TYPE, -- integer
      ponService            IN mkt_service_consistency_rule.sncode%TYPE, -- integer
      posDependType     IN mkt_service_consistency_rule.dependency_type%TYPE, -- char, values 'N' or 'P'
      ponDependentServicePackage
                            IN mkt_service_consistency_rule.dependent_spcode%TYPE, -- integer
      ponDependentService   IN mkt_service_consistency_rule.dependent_sncode%TYPE, -- integer
      -- Error indicator
   -- 0 - succes
   -- 1 - no data found
   -- 2 - duplicate value of the unique index
   -- 3 - wrong/missing parameters
   -- 4 - undefined error
      ponErrorCode          OUT NUMBER
    );

   /*
   **  return the version control information
   */
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

END ServiceRule;
/
CREATE OR REPLACE PACKAGE BODY ServiceRule
IS
/*
** @(#) but_bscs/bscs/database/scripts/oraproc/packages/rating/ServiceRule.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/3, 22/04/02
**
**
** Reference : 81478_1, Mapped Component PDE
** Author: Inna Rivkin,Sema Telecom
**
** Filenames: ServiceRule.sph - Package header
**            ServiceRule.spb - Package body
**
** Overview:
**
** This package contains methods to add, delete rules (rows) to/from table MKT_SERVICE_CONSISTENCY_RULE
**
**
** Major Modifications (when, who, what)
**
** 07/01 -IR -  Initial Version
** 04/02 -HSZ- add PrintBodyVersion
*/

   /*
   **  Global variables and exceptions
   */
 rec MKT_SERVICE_CONSISTENCY_RULE%ROWTYPE;
   /*
   **=====================================================
   **
   ** Procedure: InsertRule
   **
   ** Purpose: Insert of a row to MKT_SERVICE_CONSISTENCY_RULE table
   **
   **=====================================================
   */
   PROCEDURE InsertRule
   (
      ponMarket             IN mpdsctab.scslprefix%TYPE, -- char
      ponRatePlan           IN mkt_service_consistency_rule.tmcode%TYPE, -- integer
      ponRatePlanVersion    IN mkt_service_consistency_rule.vscode%TYPE, -- integer
      ponServicePackage     IN mkt_service_consistency_rule.spcode%TYPE, -- integer
      ponService            IN mkt_service_consistency_rule.sncode%TYPE, -- integer
      posDependType     IN mkt_service_consistency_rule.dependency_type%TYPE, -- char, values 'N' or 'P'
      ponDependentServicePackage
                            IN mkt_service_consistency_rule.dependent_spcode%TYPE, -- integer
      ponDependentService   IN mkt_service_consistency_rule.dependent_sncode%TYPE, -- integer
      -- Error indicator
   -- 0 - succes
   -- 1 - no data found
   -- 2 - duplicate value of the unique index
   -- 3 - wrong/missing parameters
   -- 4 - undefined error
      ponErrorCode          OUT NUMBER
    )
   IS
   -- Declare program variables as shown above
BEGIN

    IF              ponMarket IS NULL OR
                    ponRatePlan IS NULL OR
                    ponRatePlanVersion  IS NULL OR
                    ponServicePackage    IS NULL OR
                    posDependType     IS NULL OR
                   (posDependType != 'N' AND posDependType != 'P') OR
                    ponDependentServicePackage IS NULL
    THEN
         ponErrorCode:=3; -- wrong/missing parameters
    ELSE

        SELECT sccode INTO rec.sccode FROM mpdsctab WHERE
         scslprefix= ponMarket ;
        SELECT nvl(max(rule_id),0)+1 INTO rec.rule_id from mkt_service_consistency_rule;
        dbms_output.put_line(' market code='||to_char(rec.sccode));
        dbms_output.put_line(' rule='||to_Char(rec.rule_id));
        INSERT INTO mkt_service_consistency_rule
                (RULE_ID,
                SCCODE,
                TMCODE,
                VSCODE,
                SPCODE,
                SNCODE,
                DEPENDENCY_TYPE,
                DEPENDENT_SPCODE,
                DEPENDENT_SNCODE)
        VALUES
                (rec.rule_id           ,
                 rec.sccode            ,
                 ponRatePlan           ,
                 ponRatePlanVersion    ,
                 ponServicePackage     ,
                 ponService            ,
                 posDependType     ,
                 ponDependentServicePackage,
                 ponDependentService);

                    ponErrorCode:=0; --success
    END IF;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
        ponErrorCode:=1; -- no data found
    WHEN DUP_VAL_ON_INDEX THEN
        ponErrorCode:=2; -- duplicate value of unique index
    WHEN OTHERS
            THEN
            ponErrorCode:= 4; -- undefined error

   END InsertRule;

   /*
   **=====================================================
   **
   ** Procedure: DeleteRule
   **
   ** Purpose: Delete a row from MKT_SERVICE_CONSISTENCY_RULE table
   **
   **=====================================================
   */
   PROCEDURE DeleteRule
   (
      ponMarket             IN mpdsctab.scslprefix%TYPE, -- char
      ponRatePlan           IN mkt_service_consistency_rule.tmcode%TYPE, -- integer
      ponRatePlanVersion    IN mkt_service_consistency_rule.vscode%TYPE, -- integer
      ponServicePackage     IN mkt_service_consistency_rule.spcode%TYPE, -- integer
      ponService            IN mkt_service_consistency_rule.sncode%TYPE, -- integer
      posDependType     IN mkt_service_consistency_rule.dependency_type%TYPE, -- char, values 'N' or 'P'
      ponDependentServicePackage
                            IN mkt_service_consistency_rule.dependent_spcode%TYPE, -- integer
      ponDependentService   IN mkt_service_consistency_rule.dependent_sncode%TYPE, -- integer
      -- Error indicator
   -- 0 - succes
   -- 1 - no data found
   -- 2 - duplicate value of the unique index
   -- 3 - wrong/missing parameters
   -- 4 - undefined error
      ponErrorCode          OUT NUMBER
    )
   IS
   -- Declare program variables as shown above
BEGIN



        SELECT sccode INTO rec.sccode FROM mpdsctab WHERE
         scslprefix= ponMarket ;

        DELETE FROM mkt_service_consistency_rule
                WHERE
                SCCODE = rec.sccode      AND
                TMCODE = ponRatePlan     AND
                VSCODE = ponRatePlanVersion AND
                SPCODE = ponServicePackage AND
                SNCODE = ponService AND
                DEPENDENCY_TYPE = posDependType AND
                DEPENDENT_SPCODE = ponDependentServicePackage AND
                DEPENDENT_SNCODE = ponDependentService;

                    ponErrorCode:=0; --success

EXCEPTION
  WHEN NO_DATA_FOUND THEN
        ponErrorCode:=1; -- no data found
  WHEN OTHERS THEN
        ponErrorCode:= 4; -- undefined error
END DeleteRule;

   /*
   **  return the version control information
   */
   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN '/main/3' || ' | ' || '22/04/02' || ' | ' || 'but_bscs/bscs/database/scripts/oraproc/packages/rating/ServiceRule.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605';
   END PrintBodyVersion;

END ServiceRule;
/

