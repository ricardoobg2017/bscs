create or replace package dth_devengamiento_pre is

  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Creado POr : SUD Norman Castro
   Fecha      : 31/05/2013
   Descripci�n: Paquete de procesos que devengan los valores pagados por anticipado
                para el producto DTH.
  *******************************************************************************/
  gv_aplicacion varchar2(50):='DTH_DEVENGAMIENTO_PRE.';

  function f_userso return varchar2;

  procedure p_descripcion_cta ( pv_cta varchar2, pv_descripcion out varchar2);

  procedure p_gen_preasiento(pd_fecha date,pv_error out varchar2);

  procedure p_carga_servicios(pv_error out varchar2);

  procedure p_actualiza_ctas( pv_error out varchar2);

  procedure replica_financiero (pd_fecha in date, pv_error out varchar2);

  procedure ajusta_prepago_dth (pd_fecha in date, pv_error out varchar2);
end dth_devengamiento_pre;
/
create or replace package body dth_devengamiento_pre is
  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
	 Creado POr : SUD Norman Castro
   Fecha      : 31/05/2013
   Descripci�n: Paquete de procesos que devengan los valores pagados por anticipado
	              para el producto DTH.
  *******************************************************************************/

  function f_userso return varchar2 is
   /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Fecha      : 31/05/2013
   Descripci�n: Funcion que retorna el usuario del S.O. que se encuentra conectado
  *******************************************************************************/
    pv_usuario varchar2(60);
  Begin
     select upper(SYS_CONTEXT('USERENV','OS_USER'))
     into pv_usuario
     from dual;

     return pv_usuario;

  Exception
    When no_data_found then
    return user;
  end f_userso;


  procedure p_descripcion_cta ( pv_cta varchar2, pv_descripcion out varchar2) is

  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Fecha      : 31/05/2013
   Descripci�n: retorna la descripcion de una cta especifica
  *******************************************************************************/

    cursor c_cta is
       select descr
       from sysadm.PS_GL_ACCOUNT_TBL@finsys
       where setid='PORTA' and
             EFF_STATUS='A'and
             account = pv_cta
       order by EFFDT desc;


    lv_cta   varchar2 (200);
    lb_found boolean;

    begin

     /* open c_cta;
      fetch c_cta into lv_cta;
         lb_found:=c_cta%found;
      close c_cta;

      if not lb_found then
       lv_cta:='Cta. no configurada';
      end if;*/

--      pv_descripcion:=lv_cta;
      pv_descripcion:=null;

  end p_descripcion_cta;



procedure p_gen_preasiento(pd_fecha date, pv_error out varchar2) is

/******************************************************************************
Proyecto   : [8693] Venta DTH Post Pago
Lider Claro: SIS Paola Carvajal
Lider PDS  : SUD Cristhian Acosta Chambers
Fecha      : 31/05/2013
Descripci�n: Procedimiento que genera el preasiento en peoplesoft
*******************************************************************************/

cursor c_reporte (cv_ciclo varchar2, cv_mes varchar2, cv_anio varchar2) is
	select a.ciudad,  a.servicio, max(a.codigo_contable) codigo_contable,
				 decode (a.codigo_contable,'410102','000003',' ') producto_asi,
				 sum (round(a.valor_provisiona,2)) Valor, id_ciclo
	from reporte_calc_prorrateo_dth_pre a
	where a.id_ciclo = cv_ciclo
	and  a.valor_provisiona <> 0
	and a.mes = cv_mes
	and a.anio = cv_anio
	group by a.ciudad, a.servicio, decode (a.codigo_contable,'410102','000003',' ') , id_ciclo
	order by a.ciudad,codigo_contable,a.servicio;

cursor c_valida_ciclo (cv_mes varchar2, cv_anio varchar2)is
	select distinct id_ciclo
	from reporte_calc_prorrateo_dth_pre
	where mes = cv_mes
	and anio = cv_anio;

i              number;
ln_sec         number;
ln_sumgye      number:=0;
ln_sumuio      number:=0;
lv_transid     varchar2(30);
ln_transline   number;
ln_linea       number:=2;
lc_ctacontab   varchar2(200);
lv_referencia  varchar2(300);
lv_ciclo       varchar2(100);
ln_valor       number;

cursor c_verifica_cuentas(cv_cuenta varchar2) is
	select a.usa_tipo_iva,a.usa_ciclo
	from gl_parametros_ctas_contab a
	where a.cuenta = cv_cuenta
	and a.setid = 'PORTA';

lc_verif_ctas c_verifica_cuentas%rowtype;
lb_found_cta   boolean;
lv_ciclo_5359 varchar2(5);
lv_tipo_iva  varchar2(5);

lv_dia_inicio        varchar2(2):=null;
lv_mes               varchar2(2):=null;
lv_anio              varchar2(4):=null;
lv_mes_act           varchar2(2):=null;
lv_anio_act          varchar2(4):=null;
ld_fecha_ant         date;
lv_error             varchar2(2000):=null;

le_error             exception;
begin
	-- primero voy a generar del mes anterior
  ld_fecha_ant := add_months(pd_fecha, -1);
	lv_mes:=extract(month from ld_fecha_ant);
	lv_anio:=extract(year from ld_fecha_ant);
  ln_transline:=5;
  for j in c_valida_ciclo (lv_mes, lv_anio)loop --genera un asiento por ciclo
      ln_sumgye:=0;
      ln_sumuio:=0;
      ln_linea:=2;
			lv_dia_inicio:=null;

				-- saco el dia incicial del ciclo
			  select f.dia_ini_ciclo
				into lv_dia_inicio
        from fa_ciclos_bscs f
			  where f.id_ciclo = j.id_ciclo;

				for i in c_reporte (j.id_ciclo, lv_mes, lv_anio) loop
            ln_linea := ln_linea +1;
            If substr(i.codigo_contable,1,1) = '4' Then
               dth_devengamiento_pre.p_descripcion_cta (I.codigo_contable,lc_ctacontab);
               lv_referencia := lc_ctacontab||'DEVENGADO DTH FACTURACION '||i.servicio ||' DEL '
							               	||'01/'||to_char(pd_fecha,'mm/rrrr')||' HASTA 00:00 DEL '||lv_dia_inicio||'/'
										          ||to_char(pd_fecha,'mm/rrrr');
               If i.ciudad = 'GYE' then
                  ln_sumgye := ln_sumgye + i.valor;
                  lv_transid:='BG'||to_char(pd_fecha,'rrrrmmdd');
               else
                  ln_sumuio := ln_sumuio + i.valor;
                  lv_transid:='BU'||to_char(pd_fecha,'rrrrmmdd');
               End if;
               ln_valor := i.valor * (-1);
               Begin
                 insert into ps_jgen_acct_entry_dth_pre(business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                                        accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                                        journal_id,     journal_date,   journal_line,     account,
                                                        operating_unit, product,        currency_cd,      foreign_currency,
                                                        rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
                                                        doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
                                                        altacct,        deptid,         fund_code,        class_fld,
                                                        program_code,   budget_ref,     affiliate,        affiliate_intra1,
                                                        affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
                                                        project_id,     statistics_code,statistic_amount, movement_flag,
                                                        doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
                                                        iu_anchor_flg,  process_instance)
                                                 values('CONEC',        lv_transid,     ln_transline,     'REAL',       'LOCAL',
                                                        pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                                        'NEXT',         pd_fecha,       ln_linea,         i.codigo_contable,
                                                        i.ciudad||'_999',i.producto_asi,            'USD',            'USD',
                                                        'CRRNT',        1,              1,                ln_valor,       ln_valor,
                                                        'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            0,                ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            0 );

                 begin
                   insert into  ps_pr_jgen_acct_en_dth_pre (business_unit,
                                                            transaction_id,
                                                            transaction_line,
                                                            ledger_group,
                                                            ledger,
                                                            pr_jrnl_ln_ref2,
                                                            pr_jrnl_tipo_iva,
                                                            pr_jrnl_ciclo,
                                                            Pr_id_cia_rel,
                                                            pr_jrnl_ln_ref3,
                                                            pr_line_desc2,
                                                            pr_jrnl_ln_ref_int,
                                                            pr_fec_prov_interc,
                                                            pr_ruc_prov_interc,
                                                            pr_servicio_interc,
                                                            pr_estado_interc,
                                                            pr_id_prov_interc,
                                                            pr_tipo_serv,
                                                            id_agrupacion,
                                                            pr_fec_ini_deven,
                                                            pr_fec_fin_deven)
                                                    values ('CONEC',
                                                            lv_transid,
                                                            ln_transline,
                                                            'REAL',
                                                            'LOCAL',
                                                            ' ',
                                                            'PROV',
                                                            j.id_ciclo,
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            pd_fecha,
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            ' ',
                                                            pd_fecha,
                                                            pd_fecha);


                 exception
                   when others then
                      lv_error := 'Error 1, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_DTH_PRE ' || sqlerrm;
                      rollback;
                      raise le_error;
                 end;

                 ln_transline := ln_transline + 1;
               Exception
                  When others then
                     lv_error := 'Error al insertar el preasiento'||sqlerrm;
                       rollback;
                       raise le_error;
               End;
            End if;
    end loop;

    dth_devengamiento_pre.p_descripcion_cta ('280106',lc_ctacontab);
    lv_referencia := lc_ctacontab||'DEVENGADO PASIVO DIFERIDO DTH DEL 01/'
						         ||to_char(pd_fecha,'mm/rrrr')||' HASTA 00:00 DEL '||lv_dia_inicio
										 ||'/'||to_char(pd_fecha,'mm/rrrr');
    lv_transid:='BG'||to_char(pd_fecha,'rrrrmmdd');
    insert into ps_jgen_acct_entry_dth_pre (business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                            accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                            journal_id,     journal_date,   journal_line,     account,
                                            operating_unit, product,        currency_cd,      foreign_currency,
                                            rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
                                            doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
                                            altacct,        deptid,         fund_code,        class_fld,
                                            program_code,   budget_ref,     affiliate,        affiliate_intra1,
                                            affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
                                            project_id,     statistics_code,statistic_amount, movement_flag,
                                            doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
                                            iu_anchor_flg,  process_instance)
                                 values(    'CONEC',        lv_transid,     1,     'REAL',       'LOCAL',
                                            pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                            'NEXT',         pd_fecha,       1,                '280106',
                                            ' ',           ' ',           'USD',            'USD',
                                            'CRRNT',        1,              1,                ln_sumgye,  ln_sumgye,
                                            'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            0,                ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            0 );

    begin
    --verifico si la cuenta aplica tipo de iva y ciclo
    open c_verifica_cuentas('280106');
    fetch c_verifica_cuentas
    into lc_verif_ctas;
    lb_found_cta := c_verifica_cuentas%found;
    close c_verifica_cuentas;

     if lb_found_cta then
       lv_ciclo_5359:= ' ';
       lv_tipo_iva:=' ';
       if lc_verif_ctas.usa_ciclo ='Y' then
          lv_ciclo_5359 := 'PROV';
       end if;
       if lc_verif_ctas.usa_tipo_iva ='Y' then
          lv_tipo_iva := 'PROV';
       end if;
       insert into ps_pr_jgen_acct_en_dth_pre (business_unit,
                                               transaction_id,
                                               transaction_line,
                                               ledger_group,
                                               ledger,
                                               pr_jrnl_ln_ref2,
                                               pr_jrnl_tipo_iva,
                                               pr_jrnl_ciclo)
                                        VALUES ('CONEC',
                                               lv_transid,
                                               1,
                                               'REAL',
                                               'LOCAL',
                                               ' ',
                                               lv_tipo_iva,
                                               lv_ciclo_5359);

     end if;
    exception
      when others then
         lv_error := 'Error 2, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' || sqlerrm;
         rollback;
         raise le_error;
    end;
    dth_devengamiento_pre.p_descripcion_cta ('280106',lc_ctacontab);
    lv_referencia := lc_ctacontab||'DEVENGADO PASIVO DIFERIDO DTH DEL 01/'
   									||to_char(pd_fecha,'mm/rrrr')||' HASTA 00:00 DEL ' ||lv_dia_inicio||
										'/'||to_char(pd_fecha,'mm/rrrr');
    lv_transid:='BU'||to_char(pd_fecha,'rrrrmmdd');
    insert into ps_jgen_acct_entry_dth_pre  (business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                            accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                            journal_id,     journal_date,   journal_line,     account,
                                            operating_unit, product,        currency_cd,      foreign_currency,
                                            rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
                                            doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
                                            altacct,        deptid,         fund_code,        class_fld,
                                            program_code,   budget_ref,     affiliate,        affiliate_intra1,
                                            affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
                                            project_id,     statistics_code,statistic_amount, movement_flag,
                                            doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
                                            iu_anchor_flg,  process_instance)
                                 values(    'CONEC',        lv_transid,     2,     'REAL',       'LOCAL',
                                            pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                            'NEXT',         pd_fecha,       2,                '280106',
                                            ' ',           ' ',           'USD',            'USD',
                                            'CRRNT',        1,              1,                ln_sumuio,  ln_sumuio,
                                            'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            0,                ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            0 );

    begin
    --verifico si la cuenta aplica tipo de iva y ciclo
    open c_verifica_cuentas('280106');
    fetch c_verifica_cuentas
    into lc_verif_ctas;
    lb_found_cta := c_verifica_cuentas%found;
    close c_verifica_cuentas;

    if lb_found_cta then
       lv_ciclo_5359:= ' ';
       lv_tipo_iva:= ' ';
       if lc_verif_ctas.usa_ciclo ='Y' then
          lv_ciclo_5359 := 'PROV';
       end if;
       if lc_verif_ctas.usa_tipo_iva ='Y' then
          lv_tipo_iva := 'PROV';
       end if;
       insert into ps_pr_jgen_acct_en_dth_pre (business_unit,
                                              transaction_id,
                                              transaction_line,
                                              ledger_group,
                                              ledger,
                                              pr_jrnl_ln_ref2,
                                              pr_jrnl_tipo_iva,
                                              pr_jrnl_ciclo)
                                      values ('CONEC',
                                             lv_transid,
                                             2,
                                             'REAL',
                                             'LOCAL',
                                             ' ',
                                             lv_tipo_iva,
                                             lv_ciclo_5359);
       end if;
    exception
      when others then
          lv_error := 'Error 3, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' || sqlerrm;
          rollback;
          raise le_error;
    end;

    commit;
  End loop;
  lv_mes_act:=extract(month from pd_fecha);
	lv_anio_act:=extract(year from pd_fecha);
	for j in c_valida_ciclo (lv_mes_act, lv_anio_act)loop --genera un asiento por ciclo
    ln_sumgye:=0;
    ln_sumuio:=0;
    ln_linea:=2;
    lv_dia_inicio:=null;

    -- saco el dia incicial del ciclo
    select f.dia_ini_ciclo
    into lv_dia_inicio
    from fa_ciclos_bscs f
    where f.id_ciclo = j.id_ciclo;

    for i in c_reporte (j.id_ciclo, lv_mes_act, lv_anio_act) loop
       ln_linea := ln_linea +1;
       If substr(i.codigo_contable,1,1) = '4' Then
          dth_devengamiento_pre.p_descripcion_cta (I.CODIGO_CONTABLE,lc_ctacontab);
          lv_referencia := lc_ctacontab||'DEVENGADO DTH FACTURACION '||i.servicio ||' DEL '
                        ||lv_dia_inicio||'/'||to_char(pd_fecha,'mm/rrrr')||' HASTA 23:59 DEL '
                        ||to_char(pd_fecha,'dd/mm/rrrr');
          If i.ciudad = 'GYE' then
             ln_sumgye := ln_sumgye + i.valor;
             lv_transid:='BG'||to_char(pd_fecha,'rrrrmmdd');
          else
             ln_sumuio := ln_sumuio + i.valor;
             lv_transid:='BU'||to_char(pd_fecha,'rrrrmmdd');
          End if;
          ln_valor := i.valor * (-1);
          Begin
            insert into ps_jgen_acct_entry_dth_pre (business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                                    accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                                    journal_id,     journal_date,   journal_line,     account,
																										operating_unit, product,        currency_cd,      foreign_currency,
																										rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
																										doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
																										altacct,        deptid,         fund_code,        class_fld,
																										program_code,   budget_ref,     affiliate,        affiliate_intra1,
																										affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
																										project_id,     statistics_code,statistic_amount, movement_flag,
																										doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
																										iu_anchor_flg,  process_instance)
                                             values(    'CONEC',        lv_transid,     ln_transline,     'REAL',       'LOCAL',
                                                        pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                                        'NEXT',         pd_fecha,       ln_linea,         i.codigo_contable,
                                                        i.ciudad||'_999',i.producto_asi,            'USD',            'USD',
                                                        'CRRNT',        1,              1,                ln_valor,       ln_valor,
                                                        'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            ' ',            0,                ' ',
                                                        ' ',            ' ',            ' ',              ' ',
                                                        ' ',            0 );

            begin
              insert into  ps_pr_jgen_acct_en_dth_pre (business_unit,
                                                       transaction_id,
							    																			transaction_line,
																												ledger_group,
																												ledger,
																												pr_jrnl_ln_ref2,
																												pr_jrnl_tipo_iva,
																												pr_jrnl_ciclo,
																												Pr_id_cia_rel,
																												pr_jrnl_ln_ref3,
																												pr_line_desc2,
																												pr_jrnl_ln_ref_int,
																												pr_fec_prov_interc,
																												pr_ruc_prov_interc,
																												pr_servicio_interc,
																												pr_estado_interc,
																												pr_id_prov_interc,
																												pr_tipo_serv,
																												id_agrupacion,
																												pr_fec_ini_deven,
																												pr_fec_fin_deven)
                                                 values ('CONEC',
																												lv_transid,
																												ln_transline,
																												'REAL',
																												'LOCAL',
																												' ',
																												'PROV',
																												j.id_ciclo,
																												' ',
																												' ',
																												' ',
																												' ',
																												pd_fecha,
																												' ',
																												' ',
																												' ',
																												' ',
																												' ',
																												' ',
																												pd_fecha,
																												pd_fecha);

            exception
              when others then
                  lv_error := 'Error 1, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_DTH_PRE ' || sqlerrm;
                  rollback;
                  raise le_error;
            end;
            ln_transline := ln_transline + 1;
          Exception
            When others then
                lv_error := 'Error al insertar el preasiento'||sqlerrm;
                rollback;
                raise le_error;
          End;
       End if;
    end loop;
    dth_devengamiento_pre.p_descripcion_cta ('280106',lc_ctacontab);
    lv_referencia := lc_ctacontab||'DEVENGADO PASIVO DIFERIDO DTH DEL '
                     ||lv_dia_inicio||'/'||to_char(pd_fecha,'mm/rrrr')||
                     ' HASTA 23:59 DEL '||to_char(pd_fecha,'dd/mm/rrrr');
    lv_transid:='BG'||to_char(pd_fecha,'rrrrmmdd');
    insert into ps_jgen_acct_entry_dth_pre (business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                            accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                            journal_id,     journal_date,   journal_line,     account,
                                            operating_unit, product,        currency_cd,      foreign_currency,
                                            rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
                                            doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
                                            altacct,        deptid,         fund_code,        class_fld,
                                            program_code,   budget_ref,     affiliate,        affiliate_intra1,
                                            affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
                                            project_id,     statistics_code,statistic_amount, movement_flag,
                                            doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
                                            iu_anchor_flg,  process_instance)
                                 values(    'CONEC',        lv_transid,     3,     'REAL',       'LOCAL',
                                            pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                            'NEXT',         pd_fecha,       1,                '280106',
                                            ' ',           ' ',           'USD',            'USD',
                                            'CRRNT',        1,              1,                ln_sumgye,  ln_sumgye,
                                            'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            0,                ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            0 );

    begin
    --verifico si la cuenta aplica tipo de iva y ciclo
			open c_verifica_cuentas('280106');
			fetch c_verifica_cuentas
			into lc_verif_ctas;
			lb_found_cta := c_verifica_cuentas%found;
			close c_verifica_cuentas;

			if lb_found_cta then
				 lv_ciclo_5359:= ' ';
				 lv_tipo_iva:=' ';
			if lc_verif_ctas.usa_ciclo ='Y' then
				 lv_ciclo_5359 := 'PROV';
			end if;
			if lc_verif_ctas.usa_tipo_iva ='Y' then
				 lv_tipo_iva := 'PROV';
			end if;
			insert into ps_pr_jgen_acct_en_dth_pre(business_unit,
																						transaction_id,
																						transaction_line,
																						ledger_group,
																						ledger,
																						pr_jrnl_ln_ref2,
																						pr_jrnl_tipo_iva,
																						pr_jrnl_ciclo)
																		values ('CONEC',
																					 lv_transid,
																						1,
																						'REAL',
																						'LOCAL',
																						' ',
																						lv_tipo_iva,
																						lv_ciclo_5359);
			end if;
    exception
      when others then
         lv_error := 'Error 2, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' || sqlerrm;
         rollback;
         raise le_error;
    end;
    dth_devengamiento_pre.p_descripcion_cta ('280106',lc_ctacontab);
    lv_referencia := lc_ctacontab||'DEVENGADO PASIVO DIFERIDO DTH DEL '
                     ||lv_dia_inicio||'/'||to_char(pd_fecha,'mm/rrrr')||
                     ' HASTA 23:59 DEL '||to_char(pd_fecha,'dd/mm/rrrr');
    lv_transid:='BU'||to_char(pd_fecha,'rrrrmmdd');
    insert into ps_jgen_acct_entry_dth_pre  (business_unit,  transaction_id, transaction_line, ledger_group, ledger,
                                            accounting_dt,  appl_jrnl_id,   business_unit_gl, fiscal_year,  accounting_period,
                                            journal_id,     journal_date,   journal_line,     account,
                                            operating_unit, product,        currency_cd,      foreign_currency,
                                            rt_type,        rate_mult,      rate_div,         monetary_amount,  foreign_amount,
                                            doc_type,       doc_seq_date,   line_descr,       gl_distrib_status,
                                            altacct,        deptid,         fund_code,        class_fld,
                                            program_code,   budget_ref,     affiliate,        affiliate_intra1,
                                            affiliate_intra2, chartfield1,  chartfield2,      chartfield3,
                                            project_id,     statistics_code,statistic_amount, movement_flag,
                                            doc_seq_nbr,    jrnl_ln_ref,    iu_sys_tran_cd,   iu_tran_cd,
                                            iu_anchor_flg,  process_instance)
                                 values(    'CONEC',        lv_transid,     4,     'REAL',       'LOCAL',
                                            pd_fecha,       'BSCS',         'CONEC',          to_char(pd_fecha,'rrrr'),        to_char(pd_fecha,'mm'),
                                            'NEXT',         pd_fecha,       2,                '280106',
                                            ' ',           ' ',           'USD',            'USD',
                                            'CRRNT',        1,              1,                ln_sumuio,  ln_sumuio,
                                            'CBSCDDTH',       pd_fecha,       lv_referencia,    'N',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            ' ',            0,                ' ',
                                            ' ',            ' ',            ' ',              ' ',
                                            ' ',            0 );

    begin
    --verifico si la cuenta aplica tipo de iva y ciclo
      open c_verifica_cuentas('280106');
      fetch c_verifica_cuentas
      into lc_verif_ctas;
      lb_found_cta := c_verifica_cuentas%found;
      close c_verifica_cuentas;

      if lb_found_cta then
         lv_ciclo_5359:= ' ';
         lv_tipo_iva:= ' ';
      if lc_verif_ctas.usa_ciclo ='Y' then
         lv_ciclo_5359 := 'PROV';
      end if;
      if lc_verif_ctas.usa_tipo_iva ='Y' then
         lv_tipo_iva := 'PROV';
      end if;
      insert into ps_pr_jgen_acct_en_dth_pre(business_unit,
			    																		transaction_id,
																							transaction_line,
																							ledger_group,
																							ledger,
																							pr_jrnl_ln_ref2,
																							pr_jrnl_tipo_iva,
																							pr_jrnl_ciclo)
                                      VALUES ('CONEC',
																							lv_transid,
																							2,
																							'REAL',
																							'LOCAL',
																							' ',
																							lv_tipo_iva,
																							lv_ciclo_5359);
      end if;
    exception
      when others then
        lv_error := 'Error 3, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' || sqlerrm;
        raise le_error;
    end;

    commit;
 End loop;
Exception
when le_error then
 pv_error := lv_error;
 rollback;
 When others then
   pv_error := 'Error al insertar el preasiento'||sqlerrm;
   rollback;
end;


 procedure p_carga_servicios( pv_error out varchar2) is
  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Fecha      : 31/05/2013
   Descripci�n: Actualiza cuentas contables  de los rubros que no tienen
  *******************************************************************************/
   CURSOR C_servicios is
        select distinct cb.shdes, trim(upper(des)) SERVICIO
        from mpusntab cb
        where not exists
        (select cp.codigo_sh,cp.servicio from calculo_prorrateo cp where cb.shdes = cp.codigo_sh);

  lv_procedimiento varchar2(100);

  BEGIN
    pv_error:=NULL;
    gv_aplicacion:='DTH_DEVENGAMIENTO_PRE.';
    lv_procedimiento:='P_CARGA_SERVICIO -';
    FOR I IN C_SERVICIOS LOOP
        BEGIN --Llenar la tabla calculo_prorrateo con el codigo_sh
            INSERT INTO CALCULO_PRORRATEO  (servicio,
                                           codigo_sh)
            VALUES (i.servicio,
                    i.shdes);
        EXCEPTION
          WHEN OTHERS then
          ROLLBACK;
          PV_ERROR:= 'Error al insertar en CALCULO_PRORRATEO '||gv_aplicacion||lv_procedimiento||sqlerrm;
        END;
    END LOOP;
    commit;
   END;


 procedure p_actualiza_ctas( pv_error out varchar2) is

  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Fecha      : 31/05/2013
   Descripci�n: Actualiza cuentas contables  de los rubros que no tienen
  *******************************************************************************/
   i number;
   lv_procedimiento varchar2(100);

  --obtiene las cuentas contables de los servicios no configurados
  cursor c_cuentas is
    select a.codigo_sh, a.servicio,b.ctapsoft
      from calculo_prorrateo a, mpusntab c, cob_servicios b
      where a.codigo_contable is null
      and a.codigo_sh = c.shdes
      and c.sncode = b.servicio;

 begin
    pv_error:=NULL;
    gv_aplicacion:='DTH_DEVENGAMIENTO_PRE.';
    lv_procedimiento:='P_ACTUALIZA_CTAS -';

    --actualiza las cuentas contables en los servicios
    for i in c_cuentas loop
      begin
        update calculo_prorrateo x
        set x.codigo_contable = i.ctapsoft
        where x.codigo_sh = i.codigo_sh
        and x.codigo_contable is null;
      EXCEPTION
        WHEN OTHERS then
        ROLLBACK;
        PV_ERROR:= 'Error al actualizar cuentas en CALCULO_PRORRATEO '||gv_aplicacion||lv_procedimiento||sqlerrm;
      end;
    end loop;
    commit;
 END;

procedure replica_financiero (pd_fecha in date, pv_error out varchar2)is
   /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Creado Por : SUD Norman Castro
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Fecha      : 06/06/2013
   Descripci�n: Proceso que replica la informacion de la provision de DTH al area de Financiero
  *******************************************************************************/
lv_proceso        varchar2(60):='REPLICA_FINANCIERO';
--ld_fecha_dep      date:=add_years(pd_fecha, -1);
lv_anio   varchar2(4):=null;
lv_mes   varchar2(2):=null;
lv_error  varchar2(1000):=null;
le_error  exception;

begin
  if pd_fecha is null then
    lv_error := 'El parametro ingresado no es valido - ';
    raise le_error;
  end if;
  begin

		insert into
		ps_jgen_acct_entry@finsys
		select * from ps_jgen_acct_entry_dth_pre
		where journal_date = pd_fecha;

--		null;
  exception
    when dup_val_on_index then
     lv_error:='Existen registros duplicados a cargar a la tabla PS_JGEN_ACCT_ENTRY - ';
     raise le_error;
    when others then
      lv_error := sqlerrm;
     raise le_error;
  end;

	begin

		insert into
		sysadm.ps_pr_jgen_acct_en@finsys
		select * from ps_pr_jgen_acct_en_dth_pre
		where pr_fec_prov_interc = pd_fecha;

--		null;
  exception
    when dup_val_on_index then
     lv_error:='Existen registros duplicados a cargar a la tabla PS_PR_JGEN_ACCT_EN - ';
     raise le_error;
    when others then
      lv_error := sqlerrm;
     raise le_error;
  end;
	-- inserto los valor y servicios devengados a la tabla historica.
	begin
		insert into dth_devengados_hist (ciclo, dia, mes, anio, tipo, nombre, valor, ctactble, cta_ctrap, compa�ia, devengado, shdes, fecha_devengado)
		select a.ciclo, a.dia, a.mes, a.anio, a.tipo, a.nombre, a.valor, a.ctactble, a.cta_ctrap, a.compa�ia, b.valor_provisiona, a.shdes, sysdate
		from servicios_prepagados_dth a, reporte_calc_prorrateo_dth_pre b
		where upper(a.nombre) = b.servicio
		and decode(a.compa�ia,'Guayaquil','GYE', 'Quito', 'UIO') =  b.ciudad
		and a.ciclo = b.id_ciclo
		and a.mes = b.mes
    and a.Ctactble = b.Codigo_Contable;
  exception
    when others then
      lv_error := sqlerrm;
     raise le_error;
  end;
  begin
		lv_anio :=extract(year from to_date(pd_fecha,'dd/mm/rrrr'));
		lv_mes :=extract(month from to_date(pd_fecha,'dd/mm/rrrr'));
		insert into dth_bitacora_finan (anio, mes, fecha_journal, estado, comentario, fecha_replicacion)
		values (lv_anio,
					 lv_mes,
						pd_fecha,
						'R',--replicado
						'Replicado a Financiero',
						sysdate);
		-- borro registros de 1 a�o atras del atabla de bitacora

		delete from dth_bitacora_finan
		where anio = extract(year from to_date(pd_fecha,'dd/mm/rrrr')) -1
		and mes = extract(month from to_date(pd_fecha,'dd/mm/rrrr'));
		-- borro registros de 1 a�o atras de la tabla de servicios prepagados
		delete from servicios_prepagados_dth
		where anio = extract(year from to_date(pd_fecha,'dd/mm/rrrr')) -1
		and mes = extract(month from to_date(pd_fecha,'dd/mm/rrrr'));
	exception
		when others then
			lv_error :=sqlerrm;
			raise le_error;
  end;
  commit;
exception
  when le_error then
    pv_error:= lv_error ||gv_aplicacion||lv_proceso;
    rollback;
  when others then
    pv_error:=sqlerrm ||gv_aplicacion||lv_proceso;
    rollback;
end;

procedure ajusta_prepago_dth (pd_fecha in date, pv_error out varchar2)is
------------------------------------------------------------------------------------------------------------
-- Proyecto: [8693] DTH Venta DTH Postpago
-- Autor: SUD Norman Castro
-- Creado: 29/05/2013
-- Lider Conecel: SIS Paola Carvajal
-- Lider SUD: SUD Cristhian Acosta Chambers
-- Proposito:  Proceso que realiza el ajuste o la devengacion de los servicios pagados por adelantado para
--             el producto DTH
------------------------------------------------------------------------------------------------------------
cursor c_ciclos is
  select f.id_ciclo,f.dia_ini_ciclo,f.dia_fin_ciclo,f.descripcion
  from fa_ciclos_bscs f
;
cursor c_datos (cv_ciclo varchar2, cn_mes number, cn_anio number)is
  select tipo , nombre , valor, nvl(por_devengar,0) por_devengar, ctactble, cta_ctrap, compa�ia, decode(compa�ia,'Guayaquil','GYE','Quito','UIO') ciudad,
	       t.mes, t.anio, t.shdes
         ,rowid --sud cac - ajuste devengamiento 31/10/2014
  from servicios_prepagados_dth t
  where ciclo = cv_ciclo
  and mes = cn_mes
  and anio = cn_anio
  --and servicio = 1562
;

cursor cta_post (cv_codigo varchar2)is
select  a.ctapost
from cob_servicios a
where upper(a.nombre)= (select upper(des) from mpusntab where SHDES =cv_codigo)
;
-- cursor para ver si ha sido replicado el asiento a financiero
cursor c_verifica (cn_anio number, cn_mes number)is
select fecha_replicacion
from dth_bitacora_finan
where anio = cn_anio
and mes = cn_mes
and estado = 'R'
;
lv_proceso     varchar2(60):= 'AJUSTA_PREPAGO_DTH';
--ld_fecha       date:=trunc(sysdate);
ld_fecha_ejec  date:=null;
ln_ult_dia     number;
ln_valor_dia   number:=0;     -- valor x dia del servicio
ln_pro_act   number:=0;     -- valor prorrateado del servicio desde el ciclo hasta fin de mes.
ln_pro_ant   number:=0;     -- valor prorrateado del servicio desde el fin de mes hasta incio ciclo.
ln_dias      number:=0;
lv_sentencia varchar2(2000);
lv_cta_contable    varchar2(30):=null;
ld_fecha_ant date;
ln_valor     number;
lv_error      varchar2(2000):=null;
ld_fecha_rep  date:=null;
lb_found      boolean:=false;
ln_dias_ant   number:=null;
ln_valor_dia2   number:=0;     -- valor x dia del servicio para reproceso
ln_devengado    number:=0;
ln_por_devengar number:=0;

le_error      exception;
le_siguiente  exception;
begin
	if pd_fecha is null then
		lv_error:='Error, El parametro no puede ser nulo';
    raise le_error;
	else
	 if extract(day from pd_fecha) <> extract(day from (last_day(pd_fecha)))  then -- si no es el ultimo dia del mes
			lv_error:='Error, Debe ingresar el ultimo dia del mes';
			raise le_error;
	 else
			 ld_fecha_ejec := pd_fecha;
   end if;
	end if;

 ln_ult_dia := extract(day from ld_fecha_ejec);
 -- validacion de si ha sido replicado a financiero; de ser asi no se debe poder procesar nuevamente.
 open c_verifica (extract(year from ld_fecha_ejec),extract(month from ld_fecha_ejec));
 fetch c_verifica into ld_fecha_rep;
 lb_found := c_verifica%found;
 close c_verifica;
 if lb_found then
	 lv_error:='No es posible generar el reporte nuevamente, ya ha sido replicado al area Financiera el dia '||ld_fecha_rep;
	 raise le_error;
 end if;

 lv_sentencia:='truncate table reporte_calc_prorrateo_dth_pre';
 Begin
   Execute Immediate lv_sentencia;
 Exception
   When Others Then
      lv_error:=substr(Sqlerrm,1,200);
      Raise Le_Error;
 End;
	-- trunco las tablas de asientos contables locales
 lv_sentencia:='truncate table ps_jgen_acct_entry_dth_pre';
 Begin
		Execute Immediate lv_sentencia;
 Exception
		When Others Then
			lv_error:=substr(Sqlerrm,1,200);
			Raise Le_Error;
 End;

 lv_sentencia:='truncate table ps_pr_jgen_acct_en_dth_pre';
 Begin
		Execute Immediate lv_sentencia;
 Exception
		When Others Then
			lv_error:=substr(Sqlerrm,1,200);
			Raise Le_Error;
 End;

 for i in c_ciclos loop
   begin
   ln_dias := (ln_ult_dia - i.dia_ini_ciclo) + 1;
   for j in c_datos(i.id_ciclo,extract(month from ld_fecha_ejec),extract(year from ld_fecha_ejec) ) loop  -- sacando el valor prorrateado del mes en curso
     ln_valor_dia:=j.valor / ln_ult_dia;-- sacando el valor x dia
     ln_pro_act:=ln_valor_dia * ln_dias;
     ln_valor:=round((j.valor - ln_pro_act),3);
     -- actualizo el nuevo valor, le resto lo que ya se ha consumido
     update servicios_prepagados_dth set por_devengar = ln_valor,
		                                     devengado = round(ln_pro_act,3)
     where tipo = j.tipo
     and nombre = j.nombre
     and ciclo = i.id_ciclo
     and compa�ia = j.compa�ia
     and mes = extract(month from ld_fecha_ejec)
     and anio = extract(year from ld_fecha_ejec)
     and rowid = j.rowid;--sud cac - ajuste devengamiento 31/10/2014
     -- busco la cuenta contable de ese servicio
     lv_cta_contable:=null;
		 for k in cta_post(j.shdes) loop
				begin
				if k.ctapost is not null then
					lv_cta_contable := k.ctapost;
				else
					raise le_siguiente;
				end if;
				exception
					when le_siguiente then
						null;
					when others then
						lv_error := sqlerrm;
						raise le_error;
						end;
			end loop;
			if lv_cta_contable is null then
				 begin
						select distinct a.codigo_contable
						into lv_cta_contable
						from calculo_prorrateo a
						where a.servicio=upper(trim(j.nombre));
				 exception
						when too_many_rows then
							lv_cta_contable := null;
						when no_data_found then
							begin
								select t.codigo_contable
								into lv_cta_contable
								from prov_porc_llamada t
								where t.nombre=upper(trim(j.nombre))
								and   t.id_ciclo=i.id_ciclo;
							exception
								when no_data_found then
									lv_cta_contable := null;
							end;
				 end;
			 end if;
     -- inserto lo devengado en la tabla reporte_calc_prorrateo_dth_pre
     insert into reporte_calc_prorrateo_dth_pre(ciudad,
                                                codigo_contable,
                                                producto,
                                                servicio,
                                                valor,
                                                prorrateo,
                                                valor_provisiona,
                                                producto_asi,
                                                codigo_sh,
                                                id_ciclo,
																								mes,
																								anio)
	                                       values(j.ciudad,
                                                lv_cta_contable,
                                                'DTH',
                                                upper(trim(j.nombre)),
                                                round(ln_pro_act,3),
                                                0,
                                                round(ln_pro_act,3),
                                                '',
                                                '',
                                                i.id_ciclo,
																								j.mes,
																								j.anio);

   end loop;
   -- procesando lo que quedo del mes anterior.
   ld_fecha_ant := add_months(ld_fecha_ejec, -1);
   for j in c_datos(i.id_ciclo,extract(month from ld_fecha_ant),extract(year from ld_fecha_ant) ) loop  -- sacando el valor prorrateado del mes en curso
		 if j.por_devengar <> 0 then -- si por devengar no es 0 significa que es la primera vez que se ejecuta
			 ln_valor_dia:=j.por_devengar / i.dia_fin_ciclo;-- sacando el valor x dia
			 ln_pro_ant:=ln_valor_dia * i.dia_fin_ciclo;
			 ln_valor:=round((j.por_devengar - ln_pro_ant),3);
			 update servicios_prepagados_dth set por_devengar = ln_valor,
																					 devengado = round((devengado + ln_pro_ant),3)
			 where tipo = j.tipo
			 and nombre = j.nombre
			 and ciclo = i.id_ciclo
			 and compa�ia = j.compa�ia
			 and mes = extract(month from ld_fecha_ant)
			 and anio = extract(year from ld_fecha_ant)
       and rowid = j.rowid;--sud cac - ajuste devengamiento 31/10/2014
		 else                              -- si es 0 ya fue ejecutado antes
			 ln_ult_dia:=extract(day from (last_day(ld_fecha_ant)));     -- saco el ultimo dia del mes anterior
			 ln_dias_ant:= (ln_ult_dia - i.dia_ini_ciclo) + 1;
			 ln_valor_dia2:= j.valor / ln_ult_dia;
			 ln_devengado:= ln_valor_dia2 * ln_dias_ant;                 -- lo que se debio devengar el mes anterior
			 ln_por_devengar:= j.valor - ln_devengado;
			 ln_valor_dia:= ln_por_devengar / i.dia_fin_ciclo;
			 ln_pro_ant:=ln_valor_dia * i.dia_fin_ciclo;
			 ln_valor:=round((ln_por_devengar - ln_pro_ant),3);
			 update servicios_prepagados_dth set por_devengar = ln_valor,
																					 devengado = round((ln_devengado + ln_pro_ant),3)
			 where tipo = j.tipo
			 and nombre = j.nombre
			 and ciclo = i.id_ciclo
			 and compa�ia = j.compa�ia
			 and mes = extract(month from ld_fecha_ant)
			 and anio = extract(year from ld_fecha_ant)
       and rowid = j.rowid;--sud cac - ajuste devengamiento 31/10/2014
 		 end if;
     -- busco la cuenta contable de ese servicio
     lv_cta_contable:=null;
		 for l in cta_post(j.shdes) loop
				begin
				if l.ctapost is not null then
					lv_cta_contable := l.ctapost;
				else
					raise le_siguiente;
				end if;
				exception
					when le_siguiente then
						null;
					when others then
						lv_error := sqlerrm;
						raise le_error;
						end;
			end loop;
			if lv_cta_contable is null then
				 begin
						select distinct a.codigo_contable
						into lv_cta_contable
						from calculo_prorrateo a
						where a.servicio=upper(trim(j.nombre));
				 exception
						when too_many_rows then
							lv_cta_contable := null;
						when no_data_found then
							begin
								select t.codigo_contable
								into lv_cta_contable
								from prov_porc_llamada t
								where t.nombre=upper(trim(j.nombre))
								and   t.id_ciclo=i.id_ciclo;
							exception
								when no_data_found then
									lv_cta_contable := null;
							end;
				 end;
			end if;
     -- inserto lo devengado en la tabla reporte_calc_prorrateo_dth_pre
     insert into reporte_calc_prorrateo_dth_pre(ciudad,
                                                codigo_contable,
                                                producto,
                                                servicio,
                                                valor,
                                                prorrateo,
                                                valor_provisiona,
                                                producto_asi,
                                                codigo_sh,
                                                id_ciclo,
																								mes,
																								anio)
	                                       values(j.ciudad,
                                                lv_cta_contable,
                                                'DTH',
                                                upper(trim(j.nombre)),
                                                round(ln_pro_ant,3),
                                                0,
                                                round(ln_pro_ant,3),
                                                '',
                                                '',
                                                i.id_ciclo,
																								j.mes,
																								j.anio);
   end loop;
   exception
     when others then
       lv_error := sqlerrm;
       raise le_error;
   end;
 end loop;
 commit;

exception
when le_error then
     pv_error:=lv_error||' ' ||gv_aplicacion||lv_proceso;
when others then
     pv_error := 'Error en el proceso '||gv_aplicacion||lv_proceso||'-'||sqlerrm;
end;
end dth_devengamiento_pre;
/
