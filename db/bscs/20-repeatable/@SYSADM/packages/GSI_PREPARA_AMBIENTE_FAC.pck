CREATE OR REPLACE package GSI_PREPARA_AMBIENTE_FAC is

  -- Author  : LANCHUN
  -- Created : 15/08/2007 10:24:17
  -- Purpose : Preparar el ambiente previo a la facturaci�n
  
  -- Public type declarations
 PROCEDURE MAIN(paf_FechCierrePeriodo in date,
   paf_TablaFacturacion in varchar2,
   paf_TipoCierre  in varchar2,
   paf_Promociones in varchar2,
   paf_Encera in varchar2, 
   paf_lvMensErr   out varchar2);
   
   PROCEDURE ACT_TERMS_ALL (PAF_DIA IN VARCHAR2,paf_FechCierrePeriodo in date,PAF_CICLO IN VARCHAR2,
                            paf_lvMensErr out varchar2);
   PROCEDURE ACT_MPSCFTAB (paf_FechCierrePeriodo in date,paf_Promociones in varchar2,
                          paf_lvMensErr out varchar2);
   PROCEDURE ACT_SINONIMOS (paf_TipoCierre in varchar2,paf_lvMensErr out varchar2);
   PROCEDURE DEP_TABLAS_CONTOL_BCH (paf_lvMensErr out varchar2);
   PROCEDURE DEP_TABLAS_DOC1 (paf_lvMensErr out varchar2);
   PROCEDURE ACT_VARIAS (paf_TablaFacturacion IN VARCHAR2,paf_lvMensErr out varchar2);
   PROCEDURE INF_CONSISTENTENCIA_PADRE_HIJO (paf_lvMensErr out varchar2);
   PROCEDURE INF_INCONT_COSTCENTER_P_H  (paf_lvMensErr out varchar2);
   PROCEDURE VAL_FACTURA_DET (paf_TipoCierre in varchar2,
                           paf_FechCierrePeriodo in date,
                           paf_lvMensErr out varchar2);
              
   PROCEDURE VAL_CC_POS (paf_lvMensErr out varchar2);                   
   PROCEDURE VAL_CC_NEG (paf_lvMensErr out varchar2); 
   PROCEDURE VAL_BGH_TARIFF_PLAN_DOC1 (paf_lvMensErr out varchar2);
   PROCEDURE VAL_FORMAS_DE_PAGO (MAIN_CICLO IN VARCHAR2,paf_lvMensErr out varchar2);
   PROCEDURE VAL_VISTA_BCH_UDR_LT_ST (paf_lvMensErr out varchar2);
   PROCEDURE CON_DOC1_PARAM_INI (paf_FechINICIOPeriodo in date, paf_FechCierrePeriodo in date ,
                    PAF_TIPO IN VARCHAR2,paf_lvMensErr out varchar2);
   PROCEDURE ING_BITACORA (bit_proceso in varchar2,bit_TipoProceso  in varchar2,
                           bit_log in varchar2,bit_resultado in varchar2,
                           paf_lvMensErr out varchar2); 
   

end GSI_PREPARA_AMBIENTE_FAC;
/
CREATE OR REPLACE package body GSI_PREPARA_AMBIENTE_FAC is
/*
AUTOR     :SIS  LEONARDO ANCHUNDIA MEN�NDEZ
PREPARA AMBIENTE DE LA FACTURACI�N POSPAGO
FECHA     : 19/06/2007
ACTUALIZADO: 22/06/007 
*/
  -- Private type declarations
  GL_ERROR VARCHAR2(500):= NULL ;  
 /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  
   PROCEDURE MAIN(paf_FechCierrePeriodo in date,
   paf_TablaFacturacion in varchar2,   paf_TipoCierre in varchar2, 
     paf_Promociones in varchar2,   paf_Encera in varchar2, 
   paf_lvMensErr out varchar2) IS
      
    v_lvMensErr   VARCHAR2(2000) := null;
    V_CICLO         VARCHAR2(2);
    V_DIA           VARCHAR2(2);  
    V_ID_BITACORA   VARCHAR2(4);
    V_LG_USUARIO    VARCHAR2(50);
    V_IP_USUARIO    VARCHAR2(50);
    V_LVMENSERR_T   VARCHAR2(200);
    V_FECHA_V  date;
    V_proceso     VARCHAR2(50);
    w_salida  VARCHAR2(50);
    v_TAB  VARCHAR2(50);
    w_sql_1 VARCHAR2(500);
    w_sql_5 VARCHAR2(1000);
    leError exception;
   
    
    CURSOR USUARIO IS     
    SELECT SYS_CONTEXT('USERENV', 'OS_USER') LG_USUARIO, SYS_CONTEXT('USERENV', 'IP_ADDRESS') IP_USUARIO
    FROM dual;
     
    cursor_USUARIO USUARIO%rowtype;

    BEGIN
      
     update gsi_BITACORA_procESO
     set estado='E' 
     where estado='A';
     commit;
      
   
      
     open USUARIO;
     
     fetch USUARIO  into cursor_USUARIO;
           V_LG_USUARIO:=cursor_USUARIO.lg_USUARIO;
           V_IP_USUARIO:=cursor_USUARIO.IP_USUARIO;
     close USUARIO;
     
     V_DIA:= Substr(paf_FechCierrePeriodo,1,2);
     SELECT VALOR INTO V_CICLO FROM  GSI_PARAMETROS WHERE TIPO_PARAMETRO=V_DIA;
     
         
   /* inserci�n de bitacora*/
     insert into gsi_bitacora_proceso (ID_BITACORA,CICLO,FACTURACION,FECHA_CIERRE,FECHA_INICIO,USUARIO,Ip_Ejecucion,ESTADO,DIA) 
     values(format_cod(GSI_ID_BITACOR.NEXTVAL,4),V_CICLO,PAF_TIPOCIERRE,PAF_FECHCIERREPERIODO,SYSDATE,V_LG_USUARIO,V_IP_USUARIO,'A',V_DIA);
     COMMIT;
     
      select max(i.id_bitacora) into  V_ID_BITACORA  from  gsi_bitacora_proceso i; 
   
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;

           
     GSI_PREPARA_AMBIENTE_FAC.ACT_TERMS_ALL (V_DIA,paf_FechCierrePeriodo,V_CICLO,PAF_LVMENSERR);
    
     if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
     end if;   
 
     ACT_MPSCFTAB (paf_FechCierrePeriodo,paf_Promociones,V_LVMENSERR_T);
     V_LVMENSERR:=V_LVMENSERR_T ;  
                          
     if V_LVMENSERR_T IS NOT NULL  then
        raise leError;
    end if;     
    
    ACT_SINONIMOS (paf_TipoCierre,paf_lvMensErr);  
    
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;    
    INF_CONSISTENTENCIA_PADRE_HIJO (paf_lvMensErr);
    
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;
    
     select add_months(paf_FechCierrePeriodo,-1) into V_FECHA_V from dual;
    CON_DOC1_PARAM_INI (V_FECHA_V , paf_FechCierrePeriodo ,paf_TipoCierre ,paf_lvMensErr );
    
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;
    INF_INCONT_COSTCENTER_P_H  (paf_lvMensErr);
   
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;     
    v_TAB:='''' || 'CREA_VISTA_' || UPPER(paf_tablafacturacion) || '''';
    
    select id_proceso into V_proceso  from gsi_procesos where tarea= 'CREA_VISTA_UNION';
    
     if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;  
   /* LLAMO AL PAQUETE QUE CREA LA VISTA EN RTXPROD*/   
   
    SYSADM.crea_vistas_facturacion.main@bscs_to_rtx_link (paf_fechcierreperiodo,
                               paf_tablafacturacion,V_ID_BITACORA,
                               V_proceso);--, paf_lvmenserr);
                               
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;                           
    VAL_VISTA_BCH_UDR_LT_ST (paf_lvMensErr );
   
    IF UPPER(PAF_ENCERA)='S' THEN
       if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
       end if;
     DEP_TABLAS_DOC1 (paf_lvMensErr);
         if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
        end if;
      DEP_TABLAS_CONTOL_BCH (paf_lvMensErr);
        
    END IF;
    
     if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;             

     ACT_VARIAS (paf_TablaFacturacion,paf_lvMensErr);
     
      if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if; 
   /*  dbms_job.submit(job => w_salida,
                     what => 'INF_CONSISTENTENCIA_PADRE_HIJO;', --w_sentencia,
                    next_date => sysdate); --to_date('19/04/2006 12:30:00', 'dd/mm/yyyy hh24:mi:ss'),
                    --- interval => 'sysdate + 1/1440'); -- se ejecuta cada minuto
     commit;*/
     
     
  --VAL_FACTURA_DET (paf_TipoCierre, paf_FechCierrePeriodo,paf_lvMensErr);
       if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
          raise leError;
       end if; 
    /* PROCESOS SOLO PARA EL COMMIT*/
    IF UPPER(paf_TipoCierre)='F' THEN   
       INF_CONSISTENTENCIA_PADRE_HIJO (paf_lvMensErr);
       if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
       end if; 
       
       ING_BITACORA ('EJE_FIX_FP','I',w_sql_1,w_sql_5,paf_lvMensErr);
       fix_fp ( V_CICLO);
       if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
          raise leError;
       end if;  
       ING_BITACORA ('EJE_FIX_FP','A',w_sql_1,'EJECUCION EXITOSA DEL FIX FP',paf_lvMensErr);
        if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
         end if; 
     
        
 -- VAL_CC_POS (paf_lvMensErr); 
    
     if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if; 
    
 --VAL_CC_NEG (paf_lvMensErr); 
    
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;                     
       
       
         /* PROCESOS SOLO PARA EL COMMIT*/
       
    END IF;                    
  
   
    
    VAL_BGH_TARIFF_PLAN_DOC1(paf_lvMensErr);
    
     if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;
    
    VAL_FORMAS_DE_PAGO (V_CICLO,paf_lvMensErr);
    
   update  gsi_bitacora_proceso set FECHA_FIN= sysdate ,ESTADO='T' 
   where  ID_bitacora=format_cod(V_ID_BITACORA,4);
   COMMIT;
     
   EXCEPTION
    WHEN leError THEN
      paf_lvMensErr := v_lvMensErr || 'MAIN' || V_LVMENSERR;
      update  gsi_bitacora_proceso set FECHA_FIN= sysdate ,ESTADO='E' 
      where  ID_bitacora=format_cod(V_ID_BITACORA,4);
       COMMIT;

    WHEN OTHERS THEN
      paf_lvMensErr := sqlerrm || 'MAIN' || V_LVMENSERR;
      update  gsi_bitacora_proceso set FECHA_FIN= sysdate ,ESTADO='E' 
      where  ID_bitacora=format_cod(V_ID_BITACORA,4);
       COMMIT;

  END MAIN;
   
   /*********************************************************************
       ACtualiza termS all
   *********************************************************************/
   
   PROCEDURE ACT_TERMS_ALL (PAF_DIA IN VARCHAR2,paf_FechCierrePeriodo IN DATE,PAF_CICLO IN VARCHAR2,paf_lvMensErr out varchar2) IS
   
    V_DIA  VARCHAR2(2);
    V_MES VARCHAR2(2);
    V_MES_A VARCHAR2(2);
    V_ANIO VARCHAR2(4);
    V_ANIO_A VARCHAR2(4);
    V_FECHA  date;
    V_ESTADO  VARCHAR2(1);
    V_FECHA2 VARCHAR2(10);
    V_CICLO VARCHAR2(2);
    V_ID_PROCESO VARCHAR2(3);
    w_sql_1 VARCHAR2(500);
    w_sql_2 VARCHAR2(500);
    w_sql_3 VARCHAR2(500);
    w_sql_4 VARCHAR2(500);
    w_sql_5 VARCHAR2(1000);
    v_lvMensErr   VARCHAR2(2000) := null;
    leError exception;
    
    CURSOR c_terms_all IS
    select TERMNAME,paf_FechCierrePeriodo FECHA_CIERRE,termnet, paf_FechCierrePeriodo +termnet FECHA_CIERRE_PLUS_TERMNET
    FROM terms_all  WHERE termname LIKE '%C ' || to_number(PAF_DIA) ||'%';
    
     BEGIN
     V_CICLO:=PAF_CICLO;
     V_DIA:=PAF_DIA;
     V_FECHA:=paf_FechCierrePeriodo;
     ING_BITACORA ('ACTUALIZA_TERMS_ALL','I',w_sql_1,w_sql_5,paf_lvMensErr);
     
     SELECT to_char(ADD_MONTHS(V_FECHA,1),'MM') into V_MES from DUAL;
     SELECT to_char(ADD_MONTHS(V_FECHA,1),'YYYY') into V_ANIO from DUAL;
     SELECT to_char(V_FECHA,'MM') into V_MES_A from DUAL;
     SELECT to_char(V_FECHA,'YYYY') into V_ANIO_A from DUAL;
     --V_fecha2:='/'||V_MES ||'/' ||V_ANIO;
   
     IF V_CICLO='01' THEN
        IF NOT V_MES='02' THEN
             
                         
             UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES_A ||'/'|| V_ANIO_A,'dd/mm/yyyy') - V_FECHA
             where    not Substr(TERMNAME, 1, 2) < 24  and termname LIKE '%C ' ||V_DIA ||'%';
      
             commit;
             UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES ||'/'|| V_ANIO,'dd/mm/yyyy') - V_FECHA
             where    Substr(TERMNAME, 1, 2) <  24  and termname LIKE '%C ' || V_DIA ||'%';
             COMMIT;
             
             ELSE
               IF NOT V_MES='02' THEN
                  UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES_A ||'/'|| V_ANIO_A,'dd/mm/yyyy') - V_FECHA
                  where    not Substr(TERMNAME, 1, 2) < 24 AND  
                  NOT    Substr(TERMNAME, 1, 2) =30  AND TERMCODE<>10 AND termname LIKE '%C ' ||V_DIA ||'%';
      
                  commit;
                  UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES ||'/'|| V_ANIO,'dd/mm/yyyy') - V_FECHA
                  where    Substr(TERMNAME, 1, 2) <  24  and termname LIKE '%C ' || V_DIA ||'%';
                 COMMIT;
                 
                 w_sql_5:=              '**********************************************************************************************' ; 
                 w_sql_5:=   w_sql_5 || '* NO SE REALIZO  MODIFACION EN LA FECHA FIN DE MES PUES ESO DEBE SER APROBADO POR OPERACIONES*';
                w_sql_5:=   w_sql_5 || '***********************************************************************************************' ; 
      
               ELSE
                  raise leError;
               END IF;  
             
             END IF;
      
     ---  w_sql_1 := 'select Substr(TERMNAME, 1, 2) from terms_all ';
     w_sql_1 := 'UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)'|| '/'||  V_MES || '/' || V_ANIO ||',''dd/mm/yyyy'') - ' || V_FECHA ;
     -- w_sql_1 :=  w_sql_1 || '  WHERE Substr(TERMNAME, 1, 2)<> to_number(Substr(to_char(to_date(''' || V_FECHA ||''',''dd/mm/yyyy'')+termnet),1,2)) ';
      w_sql_1 :=  w_sql_1 || '  where    Substr(TERMNAME, 1, 2) < '|| V_DIA ;
      w_sql_1 :=  w_sql_1 || '  and termname LIKE ''%C '|| V_DIA ;
      w_sql_1 :=  w_sql_1 || '%''';
     
      --- EXECUTE IMMEDIATE w_sql_1;
   
    ELSE
      IF V_CICLO= '02' THEN
        IF NOT V_MES='02' THEN
         V_DIA:=to_number(V_DIA);
           UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES_A ||'/'|| V_ANIO_A,'dd/mm/yyyy') - V_FECHA
          where    termname LIKE '%C ' || V_DIA ||'%';
           commit;
          ELSE
            IF NOT V_MES='02' THEN
              UPDATE terms_all SET    termnet = to_date(Substr(TERMNAME,1,2)|| '/' || V_MES_A ||'/'|| V_ANIO_A,'dd/mm/yyyy') - V_FECHA
              where    TERMCODE<>11 AND termname LIKE '%C ' || V_DIA ||'%';
              commit;
              w_sql_5:=              '**********************************************************************************************' ; 
              w_sql_5:=   w_sql_5 || '* NO SE REALIZO  MODIFACION EN LA FECHA FIN DE MES PUES ESO DEBE SER APROBADO POR OPERACIONES*';
             w_sql_5:=   w_sql_5 || '***********************************************************************************************' ; 

              
            ELSE
                  raise leError;
             END IF;  
          END IF;
       
      
     
      ELSE
       w_sql_1 :='No se puede realizar la actualizacion no existe ese ciclo';
      END IF ;
      
   END IF;   

   w_sql_1 := w_sql_1 || w_sql_2;
  
   w_sql_5:=   w_sql_5 || '|' || 'FECHA_CIERRE' || '|'|| 'TERMNET' ; 
   w_sql_5:=   w_sql_5 || '|' || 'FECHA_CIERRE_PLUS_TERMNET'|| chr(13);
   
   
 
   
    FOR cur_term  IN c_terms_all LOOP
           w_sql_5:=  w_sql_5 || cur_term.TERMNAME || '|' || cur_term.FECHA_CIERRE || '|'|| cur_term.termnet; 
           w_sql_5:=  w_sql_5 || '|' || cur_term.FECHA_CIERRE_PLUS_TERMNET || chr(13);
     
   END LOOP;   
   
   
   ING_BITACORA ('ACTUALIZA_TERMS_ALL','A',w_sql_1,w_sql_5,paf_lvMensErr);
     
    if PAF_lvMensErr is not null OR GL_ERROR IS NOT NULL  then
        raise leError;
    end if;      
     

 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'ACT_TERMS_ALL';
      PAF_lvMensErr := sqlerrm || 'ACT_TERMS_ALL';
      w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
      dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          ING_BITACORA ('ACTUALIZA_TERMS_ALL','E',w_sql_4,w_sql_5,paf_lvMensErr);
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
          end ;
                            

END ACT_TERMS_ALL;
/* ACTUALIZO LA  TABLA MPSCFTAB CON O SIN PROMOCIONES */  
  
PROCEDURE ACT_MPSCFTAB (paf_FechCierrePeriodo in date,paf_Promociones in varchar2,
                        paf_lvMensErr out varchar2) IS
                        

V_FECHA2 VARCHAR2(10):= null;
V_CICLO VARCHAR2(2):= null;
w_sql_1 VARCHAR2(500):= null;
w_sql_2 VARCHAR2(500):= null;
w_sql_3 VARCHAR2(500):= null;
w_sql_4 VARCHAR2(500):= null;
w_sql_5 varchar2(1000):= null;
V_VALUE varchar2(500):= null;   
v_lvMensErr   VARCHAR2(2000) := null;
V_PROMO VARCHAR2(1):= null;
V_CFVALUE  VARCHAR2(50):= null;
leError exception;
 

BEGIN
    
    ING_BITACORA ('ACTUALIZA_MPSCFTAB','I',w_sql_1,w_sql_5,paf_lvMensErr);
     
     SELECT to_char(paf_FechCierrePeriodo,'YYYYMMDD') into V_FECHA2 from DUAL;
     V_PROMO:=paf_Promociones;
       
   
      IF UPPER(V_PROMO)='S' THEN
         V_VALUE:='''-R -C -d -v -t ' ||V_FECHA2 ;
         w_sql_1:='UPDATE  mpscftab SET  cfvalue  = ' || V_VALUE || ''' where cfcode=23 ';
         EXECUTE IMMEDIATE w_sql_1;
      ELSE 
        IF UPPER(V_PROMO)='N' THEN
           V_VALUE:='''-R -C -d -p -v -t ' ||V_FECHA2;
           w_sql_1:='UPDATE  mpscftab SET cfvalue  = ' || V_VALUE || ''' where cfcode=23 ';
           EXECUTE IMMEDIATE w_sql_1;
       
        ELSE
         w_sql_1 :='No escogio que si desea promociones o no';
        END IF ;
     END IF;   
       
     w_sql_5 := 'CFVALUE' || CHR(13);
     select CFVALUE INTO V_CFVALUE from mpscftab K where cfcode=23;
     w_sql_5 := w_sql_5 || V_CFVALUE;
     ING_BITACORA ('ACTUALIZA_MPSCFTAB','A',w_sql_1,w_sql_5,paf_lvMensErr);
  

 Exception when others then

      PAF_lvMensErr := v_lvMensErr ||'ACT_MPSCFTAB' || w_sql_1;
      PAF_lvMensErr := sqlerrm     || 'ACT_MPSCFTAB'|| w_sql_1;
      GL_ERROR:=PAF_lvMensErr;
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          ING_BITACORA ('ACTUALIZA_MPSCFTAB','E',w_sql_4,w_sql_5,paf_lvMensErr);
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  

END ACT_MPSCFTAB;

  
 /*ACTUALIZO LOS SIMONIMOS  DOCUMENT_ALL,DOCUMENT_INCLUDE,DOCUMENT_REFERENCE */
   
PROCEDURE ACT_SINONIMOS (paf_TipoCierre in varchar2,paf_lvMensErr out varchar2) IS

V_FECHA  date;
V_ESTADO  VARCHAR2(1):= null;
V_FECHA2 VARCHAR2(10):= null;
V_ID_PROCESO VARCHAR2(3):= null;
w_sql_1 VARCHAR2(1000):= null;
w_sql_2 VARCHAR2(500):= null;
w_sql_3 VARCHAR2(500):= null;
w_sql_4 VARCHAR2(500):= null;
w_sql_5 VARCHAR2(1000):= null;
w_sql_6 VARCHAR2(500):= null;
V_USUARIO_SINONIMO VARCHAR2(50):= null;
V_SINONIMO VARCHAR2(50):= null;
V_USUARIO_TABLA VARCHAR2(50):= null;
V_TABLA VARCHAR2(50):= null;
v_lvMensErr   VARCHAR2(2000) := null;
V_CIERRE VARCHAR2(1):= null;
leError exception;
 
 
BEGIN
    
     ING_BITACORA ('ACTUALIZA_SINONIMOS','I',w_sql_1,w_sql_5,paf_lvMensErr);
     
     V_CIERRE:=paf_TipoCierre;
       /* CUANDO ES CG*/
      IF UPPER(V_CIERRE)='P' THEN
         
         
            --DOCUMENT_ALL
            w_sql_2 := ' drop public synonym DOCUMENT_ALL ';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_2:=  w_sql_2 || chr(13);
            w_sql_3 :=  ' create public synonym DOCUMENT_ALL for SYSADM.DOCUMENT_ALL_CG';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_2 || w_sql_3;
            w_sql_2:=NULL;
            w_sql_3:=NULL;
               --DOCUMENT_INCLUDE
            w_sql_2 := 'drop public synonym DOCUMENT_INCLUDE';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_2:=  w_sql_2 || chr(13);
            w_sql_3 :=  ' create public synonym DOCUMENT_INCLUDE for SYSADM.DOCUMENT_INCLUDE_CG';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_1 ||w_sql_2 || w_sql_3;

          -- DOCUMENT_REFERENCE
            w_sql_2 := 'drop public synonym DOCUMENT_REFERENCE';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_2:=  w_sql_2 || chr(13);
             w_sql_3 :=  ' create public synonym DOCUMENT_REFERENCE for SYSADM.DOCUMENT_REFERENCE_CG';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_1 ||w_sql_2 || w_sql_3;
         
      ELSE
      
      /* CUANDO ES COMMIT*/
        IF UPPER(V_CIERRE)='F'   THEN
            --DOCUMENT_ALL
            w_sql_2 := ' drop public synonym DOCUMENT_ALL ';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_2:=  w_sql_2 || chr(13);
            w_sql_3 :=  ' create public synonym DOCUMENT_ALL for SYSADM.DOCUMENT_ALL';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_2 || w_sql_3;
            w_sql_2:=NULL;
            w_sql_3:=NULL;
            --DOCUMENT_INCLUDE
            w_sql_2 := 'drop public synonym DOCUMENT_INCLUDE';
            EXECUTE IMMEDIATE w_sql_2;
             w_sql_2:=  w_sql_2 || chr(13);
            w_sql_3 :=  ' create public synonym DOCUMENT_INCLUDE for SYSADM.DOCUMENT_INCLUDE';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_1 ||w_sql_2 || w_sql_3;
  
            -- DOCUMENT_REFERENCE
            w_sql_2 := 'drop public synonym DOCUMENT_REFERENCE';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_2:=  w_sql_2 || chr(13);
            w_sql_3 :=  ' create public synonym DOCUMENT_REFERENCE for SYSADM.DOCUMENT_REFERENCE';
            EXECUTE IMMEDIATE w_sql_3;
            w_sql_3:=  w_sql_3 || chr(13);
            w_sql_1 := w_sql_1 ||w_sql_2 || w_sql_3;
        ELSE
            w_sql_1 :='No escogio es commit o CG';
        END IF ;
     END IF;   
   
     w_sql_6 :='USUARIO_SINONIMO'||'|'||'SINONIMO'||'|'||'USUARIO_TABLA'||'|'||'TABLA'||chr(13);
     SELECT OWNER USUARIO_SINONIMO,  SYNONYM_NAME SINONIMO,
      TABLE_OWNER USUARIO_TABLA,   TABLE_NAME  TABLA 
     INTO  V_USUARIO_SINONIMO,V_SINONIMO,V_USUARIO_TABLA,V_TABLA 
     FROM ALL_SYNONYMS WHERE  OWNER = 'PUBLIC' AND SYNONYM_NAME = 'DOCUMENT_ALL';
     w_sql_5 := w_sql_6;
     w_sql_5 := w_sql_5 ||V_USUARIO_SINONIMO||'|'||V_SINONIMO||'|'||V_USUARIO_TABLA||'|'||V_TABLA||chr(13);
     V_USUARIO_SINONIMO:=null;
     V_SINONIMO:=null;
     V_USUARIO_TABLA:=null;
     V_TABLA:=null;
     SELECT OWNER USUARIO_SINONIMO,  SYNONYM_NAME SINONIMO,
      TABLE_OWNER USUARIO_TABLA,   TABLE_NAME  TABLA 
     INTO  V_USUARIO_SINONIMO,V_SINONIMO,V_USUARIO_TABLA,V_TABLA 
     FROM ALL_SYNONYMS WHERE  OWNER = 'PUBLIC' AND SYNONYM_NAME = 'DOCUMENT_INCLUDE';
     w_sql_5 := w_sql_5 || w_sql_6;
     w_sql_5 := w_sql_5 || V_USUARIO_SINONIMO||'|'||V_SINONIMO||'|'||V_USUARIO_TABLA||'|'||V_TABLA||chr(13);
     V_USUARIO_SINONIMO:=null;
     V_SINONIMO:=null;
     V_USUARIO_TABLA:=null;
     V_TABLA:=null;
     SELECT OWNER USUARIO_SINONIMO,  SYNONYM_NAME SINONIMO,
      TABLE_OWNER USUARIO_TABLA,   TABLE_NAME  TABLA 
     INTO  V_USUARIO_SINONIMO,V_SINONIMO,V_USUARIO_TABLA,V_TABLA 
     FROM ALL_SYNONYMS WHERE  OWNER = 'PUBLIC' AND SYNONYM_NAME = 'DOCUMENT_REFERENCE';
     w_sql_5 := w_sql_5 || w_sql_6;
     w_sql_5 := w_sql_5 || V_USUARIO_SINONIMO||'|'||V_SINONIMO||'|'||V_USUARIO_TABLA||'|'||V_TABLA||chr(13);
     
     w_sql_1 := w_sql_1 || w_sql_2;
   
     ING_BITACORA ('ACTUALIZA_SINONIMOS','A',w_sql_1,w_sql_5,paf_lvMensErr);
    
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'ACT_SIMONIMOS' ;
      PAF_lvMensErr := sqlerrm || 'ACT_SIMONIMOS' ;
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
         
          ING_BITACORA ('ACTUALIZA_SINONIMOS','E',w_sql_4,w_sql_5,paf_lvMensErr);
          
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
         end ;
  

END ACT_SINONIMOS; 
/*DEPURO LAS TABLAS DE CONTROL DE LOS BCH */

   
PROCEDURE DEP_TABLAS_CONTOL_BCH (paf_lvMensErr out varchar2) IS

w_sql_1 VARCHAR2(500) := null;
w_sql_2 VARCHAR2(500) := null;
w_sql_3 VARCHAR2(500) := null;
w_sql_4 VARCHAR2(500) := null;
w_sql_5 VARCHAR2(1000) := null;
V_COUNT VARCHAR2(10) := null;
v_lvMensErr   VARCHAR2(2000) := null;
leError exception;

BEGIN
            ING_BITACORA ('DEPURA_TABLAS_CONTROL_BCH','I',w_sql_1,w_sql_5,paf_lvMensErr);
            w_sql_2:=NULL;
            w_sql_2 := 'delete from  bch_process_cust';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 :=  w_sql_2|| chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'delete from  bch_process_contr';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2|| chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'delete from  bch_process_package';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'delete from  bch_control';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'delete from  bch_process';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'delete from  bch_monitor_table';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            SELECT COUNT(*) INTO V_COUNT from  bch_process_cust;
            w_sql_5 := 'TABLA bch_process_cust' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  bch_process_contr;
            w_sql_5 := w_sql_5 ||'TABLA bch_process_contr' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  bch_process_package;
            w_sql_5 := w_sql_5 ||'TABLA bch_process_package' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  bch_control;
            w_sql_5 := w_sql_5 ||'TABLA bch_control' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from bch_process;
            w_sql_5 := w_sql_5 ||'TABLA bch_process' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from bch_monitor_table;
            w_sql_5 := w_sql_5 ||'TABLA bch_monitor_table' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
                     
            ING_BITACORA ('DEPURA_TABLAS_CONTROL_BCH','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'DEP_TABLAS_CONTOL_BCH';
      PAF_lvMensErr := sqlerrm || 'DEP_TABLAS_CONTOL_BCH';
      
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('DEPURA_TABLAS_CONTROL_BCH','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  
END DEP_TABLAS_CONTOL_BCH; 
/*DEPURA LAS TABLAS DOC1*/
PROCEDURE DEP_TABLAS_DOC1 (paf_lvMensErr out varchar2) IS

w_sql_1 VARCHAR2(500):= null;
w_sql_2 VARCHAR2(500):= null;
w_sql_3 VARCHAR2(500):= null;
w_sql_4 VARCHAR2(500):= null;
w_sql_5 VARCHAR2(1000):= null;
V_COUNT VARCHAR2(10) := null;
v_lvMensErr   VARCHAR2(2000) := null;
leError exception;

BEGIN
            ING_BITACORA ('DEPURA_TABLAS_DOC1','I',w_sql_1,w_sql_5,paf_lvMensErr);
            w_sql_2 := 'truncate table DOC1.doc1_cuentas';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 :=  w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'truncate table DOC1.doc1_parametros_facturacion';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'truncate table DOC1.doc1_det_para_facturacion';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'truncate table DOC1.doc1_ciclos_bajados';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
            w_sql_2:=NULL;
	          w_sql_2 := 'truncate table DOC1.doc1_generate';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 := w_sql_1 || w_sql_2 || chr(13);
        
            SELECT COUNT(*) INTO V_COUNT from  DOC1.doc1_cuentas;
            w_sql_5 := 'TABLA bch_process_cust' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  DOC1.doc1_parametros_facturacion;
            w_sql_5 := w_sql_5 ||'TABLA DOC1.doc1_ciclos_bajados' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  DOC1.doc1_det_para_facturacion;
            w_sql_5 := w_sql_5 ||'TABLA DOC1.doc1_ciclos_bajados' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from  DOC1.doc1_ciclos_bajados;
            w_sql_5 := w_sql_5 ||'TABLA DOC1.doc1_ciclos_bajados' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
            V_COUNT:=NULL;
            SELECT COUNT(*) INTO V_COUNT from DOC1.doc1_generate;
            w_sql_5 := w_sql_5 ||'TABLA DOC1.doc1_generate' || chr(13)||'CANTIDAD'||V_COUNT|| chr(13);
        
            
            ING_BITACORA ('DEPURA_TABLAS_DOC1','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr ||'DEP_TABLAS_DOC1';
      PAF_lvMensErr := sqlerrm || 'DEP_TABLAS_DOC1';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('DEPURA_TABLAS_DOC1','E',w_sql_4,w_sql_5,paf_lvMensErr);
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  
END DEP_TABLAS_DOC1; 
/* VALIDACIONES VARIAS*/
PROCEDURE ACT_VARIAS (paf_TablaFacturacion IN VARCHAR2,paf_lvMensErr out varchar2) IS

w_sql_1 VARCHAR2(500):=NULL;
w_sql_2 VARCHAR2(500):=NULL;
w_sql_3 VARCHAR2(500):=NULL;
w_sql_4 VARCHAR2(500):=NULL;
w_sql_5 VARCHAR2(1000):=NULL;
V_udr_retrieval VARCHAR2(100):=NULL;
V_udr_streams   VARCHAR2(100):=NULL;
V_bch_callrecord_to_file VARCHAR2(50):=NULL;
v_lvMensErr   VARCHAR2(2000) := null;
leError exception;

BEGIN
            ING_BITACORA ('ACTUALIZA_VARIAS','I',w_sql_1,w_sql_5,paf_lvMensErr);
            w_sql_2 := 'update bscsproject set bch_callrecord_to_file =null';
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 :=  w_sql_2 || CHR(13);
            SELECT bch_callrecord_to_file INTO V_bch_callrecord_to_file from bscsproject;
            w_sql_5 := w_sql_5 ||'CAMPO bch_callrecord_to_file TABLA bscsproject |' ||  V_bch_callrecord_to_file  || ' |'  ||chr(13);
            w_sql_2:=NULL;
            IF UPPER(paf_TablaFacturacion)='POOL' THEN 
               w_sql_2 := 'update bscsproject_all set udr_retrieval=''4'',udr_streams=''X''';
            ELSE
               w_sql_2 := 'update bscsproject_all set udr_retrieval=''1'',udr_streams=''X''';
            END IF;
            EXECUTE IMMEDIATE w_sql_2;
            w_sql_1 :=  w_sql_2 || CHR(13);
            select udr_retrieval, udr_streams INTO V_udr_retrieval,V_udr_streams
            from bscsproject_all;
            w_sql_5 := w_sql_5 ||'UDR_RETRIEVAL' ||'|' ||'UDR_STREAMS' || chr(13);
            w_sql_5 := w_sql_5 ||V_udr_retrieval ||'|' ||V_udr_streams ||chr(13);
        
            
            ING_BITACORA ('ACTUALIZA_VARIAS','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'ACT_VARIAS';
      PAF_lvMensErr := sqlerrm || 'ACT_VARIAS'; 
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('ACTUALIZA_VARIAS','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  
END ACT_VARIAS; 

PROCEDURE INF_CONSISTENTENCIA_PADRE_HIJO  (paf_lvMensErr out varchar2) IS

w_sql_1 VARCHAR2(500):= null;
w_sql_2 VARCHAR2(500):= null;
w_sql_3 VARCHAR2(500):= null;
w_sql_4 VARCHAR2(500):= null;
w_sql_5 VARCHAR2(1000):= null;
/*paf_lvMensErr  VARCHAR2(1000):= null;*/
ind     number;
v_lvMensErr   VARCHAR2(2000) := null;
leError exception;

cursor incosistencia is
select a.custcode,a.customer_id cust_padre, b.customer_id cust_hijo, 
      a.prgcode   prg_padre,   b.prgcode prg_hijo,
      a.billcycle ciclo_padre, b.billcycle ciclo_hijo
from  customer_all a,customer_all b
where substr(a.custcode,1,1)<>'1'
and   b.customer_id_high=a.customer_id
and  (a.billcycle <> b.billcycle
      or a.prgcode <> b.prgcode);      


BEGIN
       ind:=0;
       ING_BITACORA ('INF_INCONSISTENCIA_BILLCYCLE_P_H','I',w_sql_1,w_sql_5,paf_lvMensErr);
       w_sql_5:=NULL;
       
       w_sql_5:= 'CUSTCODE' || '|' || 'CUST_PADRE' || '|'|| 'CUST_PADRE' ; 
       w_sql_5:=   w_sql_5 || '|' || 'PRG_PADRE' || '|'|| 'PRG_HIJO'|| '|' || 'CICLO_PADRE';
       w_sql_5:=   w_sql_5 || '|' || 'CICLO_HIJO '|| chr(13);
                    
       FOR cur_cos  IN incosistencia LOOP
           w_sql_5:=  w_sql_5 || cur_cos.custcode || '|' || cur_cos.cust_padre || '|'|| cur_cos.cust_hijo; 
           w_sql_5:=  w_sql_5 || '|' || cur_cos.prg_padre || '|'|| cur_cos.prg_hijo|| '|' || cur_cos.ciclo_padre;
           w_sql_5:=  w_sql_5 || '|' || cur_cos.ciclo_hijo || chr(13);
           ind:=ind+1;
       END LOOP;   
              
       if ind>0 then
           w_sql_1 := 'Se encontraron Diferencias en BILLCYCLE Y PRGCODE entre cuentas padre e Hijas Revizarlas en campo RESULTADOS';
       else
           w_sql_1 := 'No existen inconsistencias BILLCYCLE Y PRGCODE entre cuentas Padre e Hijas';
       end if;
                 
       ING_BITACORA ('INF_INCONSISTENCIA_BILLCYCLE_P_H','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'INF_INCONSISTENCIA_BILLCYCLE_P_H';
      PAF_lvMensErr := sqlerrm || 'INF_INCONSISTENCIA_BILLCYCLE_P_H';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('INF_INCONSISTENCIA_BILLCYCLE_P_H','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  
END INF_CONSISTENTENCIA_PADRE_HIJO; 

/*****
revisar si estan consistentes prgcode y costcenter_id
/****/


PROCEDURE INF_INCONT_COSTCENTER_P_H  (paf_lvMensErr out varchar2) IS

w_sql_1 VARCHAR2(500):= null;
w_sql_2 VARCHAR2(500):= null;
w_sql_3 VARCHAR2(500):= null;
w_sql_4 VARCHAR2(500):= null;
w_sql_5 VARCHAR2(1000):= null;
/*paf_lvMensErr  VARCHAR2(1000):= null;*/
ind     number;
v_lvMensErr   VARCHAR2(2000) := null;
leError exception;

cursor c_cost is
select a.custcode custcode_PADRE,
       B.custcode custcode_HIJO,
       a.customer_id cust_padre,
       b.customer_id cust_hijo,
       a.prgcode prg_padre,
       b.prgcode prg_hijo,
       a.costcenter_id cost_padre,
       b.costcenter_id cost_hijo
       from customer_all a,
      customer_all b
      where a.customer_id_high is null and
      b.customer_id_high  = a.customer_id and
      (a.prgcode<>b.prgcode OR  a.costcenter_id<>b.costcenter_id);      


BEGIN
       ind:=0;
       ING_BITACORA ('INF_INCONSISTENCIA_COSTCENTER_P_H','I',w_sql_1,w_sql_5,paf_lvMensErr);
       w_sql_5:=NULL;
       
       w_sql_5:= 'CUSTCODE_PADRE | CUSTCODE_HIJO | CUSTOMER_PADRE | CUSTOMER_HIJO |' ;
       w_sql_5:=   w_sql_5  || 'PRG_PADRE | PRG_HIJO | COST_PADRE | COST_HIJO'|| chr(13);
                         
       FOR C_COS  IN c_cost LOOP
           w_sql_5:=  w_sql_5 || c_cos.custcode_PADRE || '|' || c_cos.custcode_HIJO || '|'|| c_cos.cust_PADRE || '|'|| c_cos.cust_hijo; 
           w_sql_5:=  w_sql_5 || '|' || c_cos.prg_padre || '|'|| c_cos.prg_hijo|| '|' || c_cos.cost_padre;
           w_sql_5:=  w_sql_5 || '|' || c_cos.cOST_hijo || chr(13);
           ind:=ind+1;   
       END LOOP;   
              
       if ind>0 then
           w_sql_1 := 'Se encontraron Diferencias en COSTCENTER Y PRGCODE entre cuentas padre e Hijas Revizarlas en campo RESULTADOS';
       else
           w_sql_1 := 'No existen inconsistencias  COSTCENTER Y PRGCODE entre cuentas Padre e Hijas';
       end if;
                 
       ING_BITACORA ('INF_INCONSISTENCIA_COSTCENTER_P_H','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'INF_INCONSISTENCIA_COSTCENTER_P_H';
      PAF_lvMensErr := sqlerrm || 'INF_INCONSISTENCIA_COSTCENTER_P_H';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('INF_INCONSISTENCIA_COSTCENTER_P_H','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  
END  INF_INCONT_COSTCENTER_P_H ; 

/*****************************************************************************
* Validar y corregir doble cobro de Factura Detallada
/********************************************************************************/
   
PROCEDURE VAL_FACTURA_DET (paf_TipoCierre in varchar2, paf_FechCierrePeriodo in date,
                           paf_lvMensErr out varchar2) IS

   TYPE b_datos IS RECORD (
        b_customer_is  VARCHAR2(12),
        b_cantidad number(9));        
   B_datos1 b_datos;
   lv_cantidad NUMBER(1):=1;
   lv_sncode number(3):=103;
   lv_period NUMBER(1):=0;
   lv_fecha date:=paf_FechCierrePeriodo;
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_fees is
   select /*+ RULE +*/ customer_id cust,count(*) cuantos  from fees 
   where  sncode =lv_sncode  and period<>lv_period 
   and    valid_from < lv_fecha
   group by customer_id
   having count (*) >lv_cantidad;
   
BEGIN
  
   ING_BITACORA ('VAL_FACTURA_DOBLE_DET','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:='CUSTOMER_ID' || chr(13);
   
   
    FOR datos  IN C_fees LOOP
      w_sql_5:= w_sql_5 || datos.cust || chr(13);
   END LOOP;
   
   if     length(w_sql_5)<15 then
          w_sql_5:= 'No se encontraron registros duplicados en la fees de factura detallada';
   end if;
   
   ING_BITACORA ('VAL_FACTURA_DOBLE_DET','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_FACTURA_DOBLE_DET';
      PAF_lvMensErr := sqlerrm || 'VAL_FACTURA_DOBLE_DET';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_FACTURA_DOBLE_DET','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;

END VAL_FACTURA_DET; 


/*****************************************************************************
* Verificacion de cargos o creditos con signos cambiados SIGNO POSITIVO
/********************************************************************************/
   
PROCEDURE VAL_CC_POS (paf_lvMensErr out varchar2) IS

   lv_cantidad NUMBER(1):=1;
   lv_sncode number(3):=103;
   lv_period NUMBER(1):=0;
   lv_caracter varchar2(1):='A';
    lv_grupo varchar2(2):='01';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_fees is
   
   select /*+ RULE +*/ h.customer_id,h.amount,h.username,h.valid_from, h.sncode ,h.remark
   from fees h where h.sncode in (
   select p.valor from gsi_parametros p where p.estado=lv_caracter and p.id_grupo=lv_grupo)
   and h.amount >  lv_period 
   and h.period <>  lv_period;
   
   
BEGIN

   ING_BITACORA ('VAL_CC_POS','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;
   w_sql_5:= w_sql_5 || 'CUSTOMER_ALL | AMOUNT | USERNAME | VALID_FROM | SNCODE | REMARK ' || chr(13);  
   FOR cf  IN C_fees LOOP
     w_sql_5:= w_sql_5 ||  cf.customer_id || ' | ' ||cf.amount || ' | ' ||cf.username || ' | ' ||cf.valid_from || ' | ' ||cf .sncode || ' | ' ||cf.remark || chr(13);
   END LOOP;
   ING_BITACORA ('VAL_CC_POS','A',w_sql_1,w_sql_5,paf_lvMensErr);
  Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_CC_POS';
      PAF_lvMensErr := sqlerrm || 'VAL_CC_POS';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_CC_POS','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;


END VAL_CC_POS; 



/*****************************************************************************
* Verificacion de cargos o creditos con signos cambiados SIGNO NEGATIVO
/********************************************************************************/
   
PROCEDURE VAL_CC_NEG (paf_lvMensErr out varchar2)
IS

   lv_cantidad NUMBER(1):=1;
   lv_sncode number(3):=103;
   lv_period NUMBER(1):=0;
   lv_caracter varchar2(1):='A';
   lv_grupo varchar2(2):='01';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(9000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_fees is
   
   select /*+ RULE +*/ h.customer_id,h.amount,h.username,h.valid_from, h.sncode ,h.remark
   from fees h where h.sncode not  in (
   select p.valor from gsi_parametros p where p.estado=lv_caracter and p.id_grupo=lv_grupo)
   and h.amount <  lv_period 
   and h.period <>  lv_period;
  --- and not  h.username = 'WPP';
   
BEGIN

   ING_BITACORA ('VAL_CC_NEG','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;
   w_sql_5:= w_sql_5 || 'CUSTOMER_ALL |  AMOUNT | USERNAME | VALID_FROM | SNCODE | REMARK ' || chr(13); 
   FOR cf  IN C_fees LOOP
      w_sql_5:= w_sql_5 ||  cf.customer_id || ' | ' || cf.amount || ' | ' || cf.username || ' | ' || cf.valid_from || ' | ' || cf .sncode || ' | ' || cf.remark || chr(13);
   END LOOP;
   --ING_BITACORA ('VAL_CC_POS','A',w_sql_1,w_sql_5,paf_lvMensErr);
   ING_BITACORA ('VAL_CC_NEG','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_CC_NEG';
      PAF_lvMensErr := sqlerrm || 'VAL_CC_NEG';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_CC_NEG','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;


END VAL_CC_NEG; 

/*****************************************************************************
* Revisi�n de la tabla bgh_tariff_plan_doc1 
/********************************************************************************/
   
PROCEDURE VAL_BGH_TARIFF_PLAN_DOC1 (paf_lvMensErr out varchar2)IS 

   lv_caracter varchar2(1):='A';
   lv_grupo varchar2(2):='04';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;

   cursor C_tmcode is
   
   SELECT k.tmcode,k.des, K.SHDES from rateplan K WHERE K.TMCODE IN (
   select tmcode from rateplan
   where  upper(des)  not like ('%MAESTRO%')
   and    tmcode not in (select valor from gsi_parametros  where id_grupo=lv_grupo and
   estado=lv_caracter)
   --aqu� se incluyen planes especiales de Roaming que no se faturan
   minus
   select distinct(tmcode) from doc1.bgh_tariff_plan_doc1);
   
BEGIN

   ING_BITACORA ('VAL_BGH_TARIFF_PLAN_DOC1','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;
   
     w_sql_5:= w_sql_5 || 'TMCODE |  NOMBRE DEL PLAN              | SHDES ' || chr(13); 
    FOR cf  IN C_tmcode LOOP
      w_sql_5:= w_sql_5 ||  cf.tmcode || ' | ' ||cf.des || ' | ' ||cf.shdes ||  chr(13);
    END LOOP;
   ING_BITACORA ('VAL_BGH_TARIFF_PLAN_DOC1','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_BGH_TARIFF_PLAN_DOC1';
      PAF_lvMensErr := sqlerrm || 'VAL_BGH_TARIFF_PLAN_DOC1';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_BGH_TARIFF_PLAN_DOC1','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;

END VAL_BGH_TARIFF_PLAN_DOC1; 


/*****************************************************************************
* Validacion de formas de pago o Mantenimiento a fix_fp
/********************************************************************************/
   
PROCEDURE VAL_FORMAS_DE_PAGO (MAIN_CICLO IN VARCHAR2,paf_lvMensErr out varchar2)IS 

   lv_caracter varchar2(1):='A';
   lv_grupo varchar2(2):='05';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_bank_id is
    select   k.BANK_ID, k.bankname FROM BANK_ALL k where k.bank_id in  (
    SELECT  BANK_ID FROM BANK_ALL k
    WHERE BANK_ID not in 
    (select valor from gsi_parametros h where h.id_grupo=lv_grupo and h.estado=lv_caracter)--formas de pago no validas
    MINUS
    SELECT FP FROM QC_FP  
    WHERE id_ciclo = MAIN_CICLO);
    
BEGIN

   ING_BITACORA ('VAL_FORMAS_DE_PAGO','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;
   w_sql_5:= w_sql_5 || 'BANK_ID | BANKNAME '  ||  chr(13); 
   FOR cf  IN C_BANK_ID LOOP
      w_sql_5:= w_sql_5 ||  cf.BANK_ID || ' | ' ||cf.BANKNAME ||  chr(13);
   END LOOP;
   ING_BITACORA ('VAL_FORMAS_DE_PAGO','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_FORMAS_DE_PAGO';
      PAF_lvMensErr := sqlerrm || 'VAL_FORMAS_DE_PAGO';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_FORMAS_DE_PAGO','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;

END VAL_FORMAS_DE_PAGO; 


/*****************************************************************************
* Valida LAS TABLAS A LA QUE REFERENCIA LA VISTA BCH.UDR_LT_ST
/********************************************************************************/
   
PROCEDURE VAL_VISTA_BCH_UDR_LT_ST (paf_lvMensErr out varchar2)IS 

   lv_caracter varchar2(4):='VIEW';
   lv_OBJETO varchar2(10):='UDR_LT_ST';
   lv_OWNER varchar2(3):='BCH';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_VISTA is
   
   select K.OWNER,K.NAME,K.TYPE,K.REFERENCED_OWNER,K.REFERENCED_NAME,K.REFERENCED_TYPE 
   from dba_dependencies@BSCS_TO_RTX_link k
   where K.TYPE=lv_caracter AND K.NAME = lv_OBJETO  AND K.OWNER=lv_OWNER;
     
   
BEGIN

   ING_BITACORA ('VAL_VISTA_BCH_UDR_LT_ST','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;

     w_sql_5:= w_sql_5 || 'OWNER | NAME_VIEW | REFERENCED_OWNER | REFERENCED_NAME  | REFERENCED_TYPE'  ||chr(13); 
    FOR cf  IN C_VISTA LOOP
      w_sql_5:= w_sql_5 || cf.OWNER|| ' | ' ||cf.NAME || ' | ' || cf.REFERENCED_OWNER|| ' | ' ||cf.REFERENCED_NAME || ' | ' ||cf.REFERENCED_TYPE || chr(13);
    END LOOP;
 
   ING_BITACORA ('VAL_VISTA_BCH_UDR_LT_ST','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'VAL_VISTA_BCH_UDR_LT_ST';
      PAF_lvMensErr := sqlerrm || 'VAL_VISTA_BCH_UDR_LT_ST';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('VAL_VISTA_BCH_UDR_LT_ST','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;

END VAL_VISTA_BCH_UDR_LT_ST; 

/*****************************************************************************
* Configurar la tabla doc1_parametro_inicial con la fecha de la facturacion ACTUAL
/********************************************************************************/
   
   PROCEDURE CON_DOC1_PARAM_INI (paf_FechINICIOPeriodo in date, paf_FechCierrePeriodo in date ,
                    PAF_TIPO IN VARCHAR2,paf_lvMensErr out varchar2) IS  

   lv_caracter CHAR(1):='I';
   w_sql_1 VARCHAR2(500):= null;
   w_sql_2 VARCHAR2(500):= null;
   w_sql_3 VARCHAR2(500):= null;
   w_sql_4 VARCHAR2(500):= null;
   w_sql_5 VARCHAR2(5000):= null;
   V_EXISTE INT(1):=0;
   v_lvMensErr   VARCHAR2(2000) := null;
   leError exception;
   
   cursor C_doc1 is
   SELECT * FROM DOC1.doc1_parametro_inicial k 
   where k.periodo_inicio=paf_FechINICIOPeriodo  
   and k.periodo_final=paf_FechCierrePeriodo;
    Cc_doc1 C_doc1%rowtype;
    
   cursor C_doc2 is
   SELECT * FROM DOC1.doc1_parametro_inicial k 
   where k.periodo_inicio=paf_FechINICIOPeriodo 
   AND K.ESTADO=LV_CARACTER;
   
BEGIN
   ING_BITACORA ('CON_DOC1_PARAMETRO_INI','I',w_sql_1,w_sql_5,paf_lvMensErr);
   w_sql_5:=NULL;
   open c_doc1;
   fetch c_doc1  into cc_doc1;
   --w_sql_3:=cc_doc1.periodo_inicio;
   V_EXISTE:=1;
   close c_doc1;
   
   UPDATE DOC1.doc1_parametro_inicial SET ESTADO=lv_caracter;
  LV_CARACTER:='A';
   IF V_EXISTE =1 THEN 
      UPDATE DOC1.doc1_parametro_inicial H 
      SET H.ESTADO=lv_caracter,H.FECHA_REGISTRO= SYSDATE,H.TIPO=upper(PAF_TIPO)
      WHERE H.periodo_inicio=paf_FechINICIOPeriodo;
   ELSE
     INSERT INTO DOC1.doc1_parametro_inicial VALUES (SYSDATE,paf_FechINICIOPeriodo,paf_FechCierrePeriodo,lv_caracter,PAF_TIPO);
   END IF;
    
    w_sql_5 :=w_sql_5 || 'FECHA_REGISTRO | PERIODO_INICIO | PERIODO_FINAL | ESTADO  | TIPO'  || chr(13); 
    FOR cf  IN C_doc2 LOOP
      w_sql_5:= w_sql_5 || cf.FECHA_REGISTRO|| ' | ' ||cf.periodo_inicio || ' | ' || cf.periodo_final|| ' | '||cf.ESTADO || ' | '||cf.TIPO || chr(13);
    END LOOP;
   
   ING_BITACORA ('CON_DOC1_PARAMETRO_INI','A',w_sql_1,w_sql_5,paf_lvMensErr);
  
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'CON_DOC1_PARAMETRO_INI';
      PAF_lvMensErr := sqlerrm || 'CON_DOC1_PARAMETRO_INI';
      
       COMMIT;
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           ING_BITACORA ('CON_DOC1_PARAMETRO_INI','E',w_sql_4,w_sql_5,paf_lvMensErr); 
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;

END CON_DOC1_PARAM_INI; 

/**************************************************************************/
/*                          INGRESA DATOS A  LA BITACORA                  */
/**************************************************************************/
PROCEDURE ING_BITACORA (bit_proceso in varchar2,bit_TipoProceso  in varchar2,
                           bit_log in varchar2,bit_resultado in varchar2,
                           paf_lvMensErr out varchar2) IS


V_DIA  VARCHAR2(2);
V_FECHA  date;
V_ESTADO  VARCHAR2(1);
V_ID_BITACORA VARCHAR2(4);
V_ID_DET_BITACORA VARCHAR2(4);
V_CICLO VARCHAR2(2);
V_ID_PROCESO VARCHAR2(3);
w_sql_4 VARCHAR2(500);
v_lvMensErr   VARCHAR2(2000) := null;
V_LG_USUARIO    VARCHAR2(50);
V_IP_USUARIO    VARCHAR2(50);
leError exception;

cursor BITA is
select ID_BITACORA,
DIA,
FECHA_CIERRE,
ESTADO,
CICLO 
from GSI_BITACORA_PROCESO
where  ESTADO='A';
cursor_bita_TER BITA%rowtype;


cursor PROC is
select ID_PROCESO 
From GSI_PROCESOS P 
where  ESTADO='A' 
AND upper(P.TAREA)=bit_proceso;
cursor_PROC PROC%rowtype;

CURSOR USU IS     
SELECT SYS_CONTEXT('USERENV', 'OS_USER') LG_USUARIO_1, 
       SYS_CONTEXT('USERENV', 'IP_ADDRESS') IP_USUARIO_1
FROM dual;
     
cursor_USU USU%rowtype;
 
BEGIN
     
     select max(ID_DETALLE_BITACORA) into  V_ID_DET_BITACORA from  gsi_detalle_bitacora_proc ; 
     open BITA  ;
     fetch BITA  into cursor_bita_TER;
      V_ID_BITACORA:= cursor_bita_TER.ID_BITACORA;
      V_DIA:= cursor_bita_TER.DIA;
      V_FECHA:=cursor_bita_TER.FECHA_CIERRE;
      V_ESTADO:=cursor_bita_TER.ESTADO;
      V_CICLO:=cursor_bita_TER.CICLO;
     close BITA ;
     
     open PROC  ;
     fetch PROC into cursor_PROC;
      V_ID_PROCESO:= cursor_PROC.ID_PROCESO;
     close PROC ;
     
      open usu;
      fetch USU  into cursor_USU;
           V_LG_USUARIO:=cursor_USU.lg_USUARIO_1;
           V_IP_USUARIO:=cursor_USU.IP_USUARIO_1;
     close USU;
   
   if bit_tipoproceso='I' then
    insert into gsi_detalle_bitacora_proc 
                                          (ID_BITACORA,
                                           ID_DETALLE_BITACORA,
                                           ID_PROCESO,
                                           FECHA_INICIO,
                                           ESTADO,USUARIO,
                                           Ip_Ejecucion) 
     values(V_ID_BITACORA,
            format_cod(GSI_ID_DET_BITACORA.NEXTVAL,4),
            V_ID_PROCESO,
            SYSDATE,
            'A',
            V_LG_USUARIO,
            V_IP_USUARIO);
     COMMIT;
   else
       if bit_tipoproceso='A' then
           UPDATE gsi_detalle_bitacora_proc 
           SET LOG= bit_log ,
           RESULTADO= bit_resultado, 
           estado='T', 
           FECHA_FIN =SYSDATE
           WHERE ID_BITACORA=V_ID_BITACORA 
           AND ID_DETALLE_BITACORA=  format_cod(V_ID_DET_BITACORA ,4)
           AND  ID_PROCESO=  V_ID_PROCESO  ;
           commit;  
       else
           if bit_tipoproceso='E' then
              uPDATE gsi_detalle_bitacora_proc 
              SET error= w_sql_4, 
              estado='E',
              LOG= bit_log,
              RESULTADO= bit_resultado
              WHERE ID_BITACORA=V_ID_BITACORA 
              AND ID_DETALLE_BITACORA=format_cod(V_ID_DET_BITACORA,4)
              AND  ID_PROCESO=  V_ID_PROCESO ;
              commit;
           end if;
       end if;    
           
    end if;   

     
 Exception when others then

      PAF_lvMensErr := v_lvMensErr || 'ING_BITACORA';
      PAF_lvMensErr := sqlerrm ||'ING_BITACORA';
      w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           uPDATE gsi_detalle_bitacora_proc SET error= w_sql_4, estado='E',LOG= bit_log
           WHERE ID_BITACORA=V_ID_BITACORA AND ID_DETALLE_BITACORA=  format_cod(V_ID_DET_BITACORA ,4)
           AND  ID_PROCESO=  V_ID_PROCESO ;
            
           commit;
          Exception when others then
          dbms_output.put_line('error'|| sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
END ING_BITACORA; 


end GSI_PREPARA_AMBIENTE_FAC;
/

