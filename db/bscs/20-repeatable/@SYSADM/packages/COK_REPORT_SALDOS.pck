CREATE OR REPLACE package COK_REPORT_SALDOS is
    
    PROCEDURE CALCULA_TOTAL;   
    PROCEDURE CALCULA_MORA;   
    PROCEDURE BALANCE_PAGOS ( pdFechCierrePeriodo  date,
                              pd_lvMensErr       out  varchar2  );
    PROCEDURE BALANCEO(pdFech_ini in date,
                       pdFech_fin in date);
    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER;
    PROCEDURE MAIN (pdFech_ini in date,
                    pdFech_fin in date);                 

end COK_REPORT_SALDOS;
/
CREATE OR REPLACE package body COK_REPORT_SALDOS is

  -- Variables locales
  gv_funcion     varchar2(30);
  gv_mensaje     varchar2(500);
  ge_error       exception;
    
    PROCEDURE CALCULA_TOTAL is
    lvSentencia                 varchar2(500);
    lvMensErr                   varchar2(1000);
    
    BEGIN
         lvSentencia := 'update co_repsaldos '||
                        'set total = balance_1 + '||
                        'balance_2 + '||
                        'balance_3 + '||
                        'balance_4 + '||
                        'balance_5 + '||
                        'balance_6 + '||
                        'balance_7 + '||
                        'balance_8 + '||
                        'balance_9 + '||
                        'balance_10 + '||
                        'balance_11 + '||
                        'balance_12';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);                                                
         commit;    
    END CALCULA_TOTAL;                 
  
    PROCEDURE CALCULA_MORA is

    --variables
    --mes		                      varchar2(2);
    lvSentencia                 varchar2(2000);
    lvMensErr                   varchar2(2000);
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;
    lnDiff                      number;
    lnI                         number;
    source_cursor               integer;
    rows_processed              integer;
    
    lnCustomerId                number;
    lnBalance1                  number;
    ldFecha1                    date;
    lnBalance2                  number;
    ldFecha2                    date;
    lnBalance3                  number;
    ldFecha3                    date;
    lnBalance4                  number;
    ldFecha4                    date;
    lnBalance5                  number;
    ldFecha5                    date;
    lnBalance6                  number;
    ldFecha6                    date;
    lnBalance7                  number;
    ldFecha7                    date;
    lnBalance8                  number;
    ldFecha8                    date;
    lnBalance9                  number;
    ldFecha9                    date;
    lnBalance10                  number;
    ldFecha10                    date;    
    lnBalance11                 number;
    ldFecha11                   date;
    lnBalance12                 number;
    ldFecha12                   date;
    
    
    Begin

       source_cursor := DBMS_SQL.open_cursor;
       lvSentencia := 'select customer_id, balance_1, fecha_1, balance_2, fecha_2, balance_3, fecha_3, balance_4, fecha_4, balance_5, fecha_5, balance_6, fecha_6, balance_7, fecha_7, balance_8, fecha_8, balance_9, fecha_9, balance_10, fecha_10, balance_11, fecha_11, balance_12, fecha_12'||
                      ' from co_repsaldos';
       Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

       dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
       dbms_sql.define_column(source_cursor, 2,  lnBalance1);
       dbms_sql.define_column(source_cursor, 3,  ldFecha1);
       dbms_sql.define_column(source_cursor, 4,  lnBalance2);
       dbms_sql.define_column(source_cursor, 5,  ldFecha2);
       dbms_sql.define_column(source_cursor, 6,  lnBalance3);
       dbms_sql.define_column(source_cursor, 7,  ldFecha3);
       dbms_sql.define_column(source_cursor, 8,  lnBalance4);
       dbms_sql.define_column(source_cursor, 9,  ldFecha4);
       dbms_sql.define_column(source_cursor, 10,  lnBalance5);
       dbms_sql.define_column(source_cursor, 11,  ldFecha5);
       dbms_sql.define_column(source_cursor, 12,  lnBalance6);
       dbms_sql.define_column(source_cursor, 13,  ldFecha6);
       dbms_sql.define_column(source_cursor, 14,  lnBalance7);
       dbms_sql.define_column(source_cursor, 15,  ldFecha7);
       dbms_sql.define_column(source_cursor, 16,  lnBalance8);
       dbms_sql.define_column(source_cursor, 17,  ldFecha8);
       dbms_sql.define_column(source_cursor, 18,  lnBalance9);
       dbms_sql.define_column(source_cursor, 19,  ldFecha9);
       dbms_sql.define_column(source_cursor, 20,  lnBalance10);
       dbms_sql.define_column(source_cursor, 21,  ldFecha10);
       dbms_sql.define_column(source_cursor, 22,  lnBalance11);
       dbms_sql.define_column(source_cursor, 23,  ldFecha11);
       dbms_sql.define_column(source_cursor, 24,  lnBalance12);
       dbms_sql.define_column(source_cursor, 25,  ldFecha12);
       rows_processed := Dbms_sql.execute(source_cursor);
       
       lnI := 0;
       loop 
           if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
           end if;
           dbms_sql.column_value(source_cursor, 1, lnCustomerId);
           dbms_sql.column_value(source_cursor, 2, lnBalance1);
           dbms_sql.column_value(source_cursor, 3, ldFecha1);
           dbms_sql.column_value(source_cursor, 4,  lnBalance2);
           dbms_sql.column_value(source_cursor, 5,  ldFecha2);
           dbms_sql.column_value(source_cursor, 6,  lnBalance3);
           dbms_sql.column_value(source_cursor, 7,  ldFecha3);
           dbms_sql.column_value(source_cursor, 8,  lnBalance4);
           dbms_sql.column_value(source_cursor, 9,  ldFecha4);
           dbms_sql.column_value(source_cursor, 10,  lnBalance5);
           dbms_sql.column_value(source_cursor, 11,  ldFecha5);
           dbms_sql.column_value(source_cursor, 12,  lnBalance6);
           dbms_sql.column_value(source_cursor, 13,  ldFecha6);
           dbms_sql.column_value(source_cursor, 14,  lnBalance7);
           dbms_sql.column_value(source_cursor, 15,  ldFecha7);
           dbms_sql.column_value(source_cursor, 16,  lnBalance8);
           dbms_sql.column_value(source_cursor, 17,  ldFecha8);
           dbms_sql.column_value(source_cursor, 18,  lnBalance9);
           dbms_sql.column_value(source_cursor, 19,  ldFecha9);
           dbms_sql.column_value(source_cursor, 20,  lnBalance10);
           dbms_sql.column_value(source_cursor, 21,  ldFecha10);
           dbms_sql.column_value(source_cursor, 22,  lnBalance11);
           dbms_sql.column_value(source_cursor, 23,  ldFecha11);
           dbms_sql.column_value(source_cursor, 24,  lnBalance12);
           dbms_sql.column_value(source_cursor, 25,  ldFecha12);

            for lnJ in 1..12 loop
                if lnJ = 1 and lnBalance1 > 0 then
                   select round(months_between(ldFecha12,ldFecha1)*30,0)
                   into lnDiff from dual;
                    exit;
                elsif lnJ = 2 and lnBalance2 > 0 then
                   select round(months_between(ldFecha12,ldFecha2)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 3 and lnBalance3 > 0 then
                   select round(months_between(ldFecha12,ldFecha3)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 4 and lnBalance4 > 0 then
                   select  round(months_between(ldFecha12,ldFecha4)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 5 and lnBalance5 > 0 then
                   select  round(months_between(ldFecha12,ldFecha5)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 6 and lnBalance6 > 0 then
                   select round(months_between(ldFecha12,ldFecha6)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 7 and lnBalance7 > 0 then
                   select  round(months_between(ldFecha12,ldFecha7)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 8 and lnBalance8 > 0 then
                   select  round(months_between(ldFecha12,ldFecha8)*30,0) 
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 9 and lnBalance9 > 0 then
                   select  round(months_between(ldFecha12,ldFecha9)*30,0)
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 10 and lnBalance10 > 0 then
                   select  round(months_between(ldFecha12,ldFecha10)*30,0) 
                   into lnDiff from dual;
                   exit;
                elsif lnJ = 11 and lnBalance11 > 0 then
                   select  round(months_between(ldFecha12,ldFecha11)*30,0) 
                   into lnDiff from dual;
                   exit;
                else
                   lnDiff := 0;
                end if;
            end loop;

            execute immediate 'update co_repsaldos'||
                              ' set mora = :1'||
                              ' where customer_id = :2'
            using lnDiff, lnCustomerId;
            
            lnI:=lnI+1;
            if lnI = 500 then
               lnI := 0;
               commit;
            end if;
       end loop;
       dbms_sql.close_cursor(source_cursor);
       commit;      
       
    END CALCULA_MORA;

  ---------------------------------------------------------------------------
  --    BALANCE__PAGOS
  --    Solo balancea...
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_PAGOS ( pdFechCierrePeriodo  date,
                            pd_lvMensErr       out  varchar2  ) is

    val_fac_          varchar2(20);
    lv_sentencia	    varchar2(1000);
    lv_sentencia_upd  varchar2(1000);
    v_sentencia       varchar2(1000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    nombre_campo      varchar2(20);
    lvMensErr         varchar2(1000);
    lII               number;
    
    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    v_cursor_asigna   number;
    v_Dummy           number;
    v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;
    --
    Begin
       --
       if pdFechCierrePeriodo >= to_date('24/01/2004', 'dd/MM/yyyy') then
           mes := '12';
           nro_mes := 12;
       else
           mes := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'), 3, 2) ;
           nro_mes := to_number(mes);
       end if;
       if    mes = '01' then
             lv_campos := 'balance_1';
       elsif mes = '02' then
             lv_campos := 'balance_1, balance_2';
       elsif mes = '03' then
             lv_campos := 'balance_1, balance_2, balance_3';
       elsif mes = '04' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4';
       elsif mes = '05' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
       elsif mes = '06' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
       elsif mes = '07' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
       elsif mes = '08' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
       elsif mes = '09' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
       elsif mes = '10' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
       elsif mes = '11' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
       elsif mes = '12' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
       end if;

       v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;

       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       lv_sentencia:= ' select c.rowid, customer_id, disponible, '|| lv_campos||
                      ' from CO_REPSALDOS c';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30);
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id);
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible);

       if    nro_mes >= 1 then
             dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       end if;
       if    nro_mes >= 2 then
             dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       end if;
       if    nro_mes >= 3 then
             dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       end if;
       if    nro_mes >= 4 then
             dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       end if;
       if    nro_mes >= 5 then
             dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       end if;
       if    nro_mes >= 6 then
             dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       end if;
       if    nro_mes >= 7 then
             dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       end if;
       if    nro_mes >= 8 then
             dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       end if;
       if    nro_mes >= 9 then
             dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       end if;
       if    nro_mes >= 10 then
             dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       end if;
       if    nro_mes >= 11 then
             dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       end if;
       if    nro_mes = 12 then
             dbms_sql.define_column(v_cursorId, 15, val_fac_12);
       end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);
       
       lII := 0;
       Loop

          total_deuda_cliente := 0;
          lv_sentencia_upd := 'update CO_REPSALDOS set ';

          -- si no tiene datos sale
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;

          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );

          if    nro_mes >= 1 then
                 dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_1,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , ';
          end if;
          
          if    nro_mes >= 2 then
                 dbms_sql.column_value(v_cursorId, 5, val_fac_2);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_2,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 3 then
                 dbms_sql.column_value(v_cursorId, 6, val_fac_3);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_3,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
           end if;
           
           if    nro_mes >= 4 then
                 dbms_sql.column_value(v_cursorId, 7, val_fac_4);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_4,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
           end if;
           
           if    nro_mes >= 5 then
                 dbms_sql.column_value(v_cursorId, 8, val_fac_5);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_5,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
           end if;
           if    nro_mes >= 6 then
                 dbms_sql.column_value(v_cursorId, 9, val_fac_6);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_6,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 7 then
                 dbms_sql.column_value(v_cursorId, 10, val_fac_7);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_7,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE                 
           end if;
           --
           if    nro_mes >= 8 then
                 dbms_sql.column_value(v_cursorId, 11, val_fac_8);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_8,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 9 then
                 dbms_sql.column_value(v_cursorId, 12, val_fac_9);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_9,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 10 then
                 dbms_sql.column_value(v_cursorId, 13, val_fac_10);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_10,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 11 then
                 dbms_sql.column_value(v_cursorId, 14, val_fac_11);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_11,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes = 12 then
                 dbms_sql.column_value(v_cursorId, 15, val_fac_12);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_12,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
           end if;

           lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
           lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

          EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);

          lII:=lII+1;
          if lII = 5000 then
             lII := 0;
             commit;
          end if;          

       End Loop; -- 1. Para extraer los datos del cursor
       commit;

       dbms_sql.close_cursor(v_CursorId);

  END BALANCE_PAGOS;

  ------------------------------------------------------------------
  -- BALANCEO
  -- Balanceo de los datos de clientes
  ------------------------------------------------------------------

  PROCEDURE BALANCEO(pdFech_ini in date,
                             pdFech_fin in date) IS

    lvSentencia       VARCHAR2(5000);
    lvPeriodos        VARCHAR2(2000);
    source_cursor     INTEGER;
    rows_processed    INTEGER;
    lvMensErr         VARCHAR2(3000);
    lnTotCred         NUMBER;
    lnCustomerId      NUMBER;
    lII               NUMBER;

    lnMes              NUMBER;

     cursor cur_pagos is 
      select /*+ rule */ customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) TotPago
       from cashreceipts_all
       where cachkdate >= pdFech_ini
       and cachkdate <= pdFech_fin
       and catype in (1,3,9)
       group by customer_id
       having sum(cachkamt_pay)<>0;


     cursor c_periodos is
       select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy');
     
  BEGIN
      
       --------------------------------------------
       -- se llenan los pagos
       --------------------------------------------
       lII := 0;
       FOR B IN cur_pagos LOOP
           lvSentencia:='update CO_REPSALDOS'||
                        ' SET disponible = '||B.TotPago||
                        ' WHERE customer_id = '||B.CUSTOMER_ID;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lII:=lII+1;
           if lII = 2000 then
               lII := 0;
               commit;
           end if;
       end loop;
       commit;

       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       for i in c_periodos loop
           lvPeriodos := lvPeriodos||' to_date('''||to_char(i.cierre_periodo,'dd/MM/yyyy')||''',''dd/MM/yyyy''),'; 
       end loop;
       lvPeriodos := substr(lvPeriodos,1,length(lvPeriodos)-1);

       source_cursor := DBMS_SQL.open_cursor;
       lvSentencia := 'select /*+ rule */ customer_id,sum(ohinvamt_doc) as Totcred'||
                      ' from orderhdr_all'||
                      ' where ohstatus  =''CM'''||
                      ' and not ohentdate in ('||lvPeriodos||')'||
                      ' group by customer_id'||
                      ' having sum(ohinvamt_doc)<>0'; 
       Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

       dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
       dbms_sql.define_column(source_cursor, 2,  lnTotCred);
       rows_processed := Dbms_sql.execute(source_cursor);
       
       lII := 0;
       loop 
            if dbms_sql.fetch_rows(source_cursor) = 0 then
               exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnTotCred);

            lvSentencia := 'update CO_REPSALDOS'||
                           ' set creditos='||lnTotCred||','||
                           ' disponible = disponible + '||lnTotCred*-1||
                           ' where customer_id = '||lnCustomerId;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       dbms_sql.close_cursor(source_cursor);
       commit;
       
       -------------------------------------------
       -- se insertan los creditos de la vista
       -------------------------------------------
       for i in c_periodos loop
           lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
           begin
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := 'SELECT customer_id, sum(valor)'||
                          ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                          ' WHERE tipo = ''006 - CREDITOS'''||
                          ' GROUP BY customer_id';
           dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           dbms_sql.define_column(source_cursor, 2,  lnTotCred);
           rows_processed := Dbms_sql.execute(source_cursor);
           
           lII := 0;               
           loop 
              if dbms_sql.fetch_rows(source_cursor) = 0 then
                 exit;
              end if;
              dbms_sql.column_value(source_cursor, 1, lnCustomerId);
              dbms_sql.column_value(source_cursor, 2, lnTotCred);
              
              lvSentencia := 'update CO_REPSALDOS'||
                             ' set creditos = creditos + '||lnTotCred||','||
                             ' disponible = disponible + '||lnTotCred*-1||
                             ' where customer_id = '||lnCustomerId;
              EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
              lII:=lII+1;
              if lII = 3000 then
                 lII := 0;
                 commit;
              end if;    
           end loop;   
           commit;
           dbms_sql.close_cursor(source_cursor);           
           exception
           when others then
                null;           
           end;
       end loop;       
       
       
       --------------------------------------------
       -- se balancean dando prioridad a los pagos
       --------------------------------------------
       COK_REPORT_SALDOS.balance_pagos(pdFech_ini, lvMensErr);

  END BALANCEO;



    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);

    BEGIN
    
        lvSentencia := 'create table CO_REPSALDOS('||
        '  CUSTOMER_ID         NUMBER not null,'||
        '  CUSTCODE            VARCHAR2(24),'||
        '  APELLIDOS           VARCHAR2(40),'||
        '  NOMBRES             VARCHAR2(60),'|| ---se aumento
        '  CED_RUC             VARCHAR2(50),'|| ---se aumento
        '  PRODUCTO            VARCHAR2(30),'||
        '  PROVINCIA           VARCHAR2(40),'||
        '  CANTON              VARCHAR2(40),'||
        '  REGION              VARCHAR2(30),'||
        '  TELEFONO            VARCHAR2(63),'||---se aumento
        '  TELEFONO_CONTACTO1  VARCHAR2(15),'||---se aumento
        '  TELEFONO_CONTACTO2  VARCHAR2(15),'||---se aumento
        '  DIRECCION           VARCHAR2(70),'||
        '  DIRECCION2          VARCHAR2(200),'||
        '  FORMA_PAGO          NUMBER,'||
        '  DES_FORMA_PAGO      VARCHAR2(58),'||
        '  TIPO_FORMA_PAGO     NUMBER,'||
        '  DES_TIPO_FORMA_PAGO VARCHAR2(30),'||
        '  CSTRADECODE         VARCHAR2(30),'||
        '  TRADE               VARCHAR2(40),'||----se aumento
        '  DISPONIBLE          NUMBER default 0,'||
        '  CREDITOS            NUMBER default 0,'||
        '  BALANCE_1           NUMBER default 0,'||
        '  FECHA_1             DATE,'||
        '  BALANCE_2           NUMBER default 0,'||
        '  FECHA_2             DATE,'||
        '  BALANCE_3           NUMBER default 0,'||
        '  FECHA_3             DATE,'||
        '  BALANCE_4           NUMBER default 0,'||
        '  FECHA_4             DATE,'||
        '  BALANCE_5           NUMBER default 0,'||
        '  FECHA_5             DATE,'||
        '  BALANCE_6           NUMBER default 0,'||
        '  FECHA_6             DATE,'||
        '  BALANCE_7           NUMBER default 0,'||
        '  FECHA_7             DATE,'||
        '  BALANCE_8           NUMBER default 0,'||
        '  FECHA_8             DATE,'||
        '  BALANCE_9           NUMBER default 0,'||
        '  FECHA_9             DATE,'||
        '  BALANCE_10          NUMBER default 0,'||
        '  FECHA_10            DATE,'||
        '  BALANCE_11          NUMBER default 0,'||
        '  FECHA_11            DATE,'||
        '  BALANCE_12          NUMBER default 0,'||
        '  FECHA_12            DATE,'||
        '  TOTAL               NUMBER default 0,'||
        '  MORA                NUMBER default 0)'||
        '  tablespace DATA'||
        '  pctfree 10'||
        '  pctused 40'||
        '  initrans 1'||
        '  maxtrans 255'||
        '  storage'||
        '  ('||
        '    initial 9360K'||
        '    minextents 1'||
        '    maxextents unlimited'||
        '  )';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        
        -- Add comments to the columns 
        lvSentencia := 'comment on column CO_REPSALDOS.CUSTOMER_ID  is ''Código del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.CUSTCODE  is ''Cuenta del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.APELLIDOS  is ''Apellido del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.NOMBRES  is ''Nombre del cliente.''';---SE AUMENTO
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.CED_RUC  is ''Cedula R.U.C. del cliente.''';---SE AUMENTO
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.PRODUCTO  is ''Tarifario o autocontrol o bulk''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.CANTON  is ''Canton del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.REGION  is ''Guayaquil o Quito.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.TELEFONO  is ''Telefono celular cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.TELEFONO_CONTACTO1  is '' Telefono contacto1''';---SE AUMENTO
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.TELEFONO_CONTACTO2  is '' Telefono contacto2''';---SE AUMENTO
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.DIRECCION  is ''Direccion del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.DIRECCION2  is ''Direccion del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.CREDITOS  is ''Valores de los creditos desde el cierre hasta la fecha de recuperación.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.TOTAL  is ''Total de la deuda del cliente.''';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'comment on column CO_REPSALDOS.MORA  is ''Edad de mora del cliente.''';    
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

         lvSentencia := 'alter table CO_REPSALDOS'||
         '   add constraint PKCUSTOMERID_REPSALDOS primary key (CUSTOMER_ID)'||
                        '  using index'||
                        '  tablespace DATA'||
                        '  pctfree 10'||
                        '  initrans 2'||
                        '  maxtrans 255'||
                        '  storage'||
                        '  (initial 1M'||
                        '    next 1M'||
                        '    minextents 1'||
                        '    maxextents unlimited'||
                        '    pctincrease 0)';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        lvSentencia := 'create index CO_REPSALDOS_PR on CO_REPSALDOS (PRODUCTO)'||
        '  tablespace DATA'||
        '  pctfree 10'||
        '  initrans 2'||
        '  maxtrans 255'||
        '  storage'||
        '  ('||
        '    initial 120K'||
        '    minextents 1'||
        '    maxextents unlimited'||
        '  )';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         
        lvSentencia := 'create index CO_REPSALDOS_RG on CO_REPSALDOS (REGION)'||
        '  tablespace DATA'||
        '  pctfree 10'||
        '  initrans 2'||
        '  maxtrans 255'||
        '  storage'||
        '  ('||
        '    initial 120K'||
        '    minextents 1'||
        '    maxextents unlimited'||
        '  )';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        lvSentencia := 'create index CO_REPSALDOS_FP on CO_REPSALDOS (FORMA_PAGO)'||
        '  tablespace DATA'||
        '  pctfree 10'||
        '  initrans 2'||
        '  maxtrans 255'||
        '  storage'||
        '  ('||
        '    initial 120K'||
        '    minextents 1'||
        '    maxextents unlimited'||
        '  )';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        lvSentencia := 'create index CO_REPSALDOS_CS on CO_REPSALDOS (CSTRADECODE)'||
        '  tablespace DATA'||
        '  pctfree 10'||
        '  initrans 2'||
        '  maxtrans 255'||
        '  storage'||
        '  ('||
        '    initial 120K'||
        '    minextents 1'||
        '    maxextents unlimited'||
        '  )';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

         lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPSALDOS to PUBLIC';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPSALDOS to READ';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         
         return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;

/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFech_ini in date,
               pdFech_fin in date) IS

    -- variables
    lvSentencia     VARCHAR2(3000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;

BEGIN

        --search the table, if it exists...
        --lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPSALDOS_'||to_char(pdFech_ini,'ddMMyyyy')||'''';
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPSALDOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:=COK_REPORT_SALDOS.crea_tabla(pdFech_ini, lvMensErr);
        else
           --si no existe se trunca la tabla para colocar los datos nuevamente
           lvSentencia := 'truncate table CO_REPSALDOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;
        -- se agrega la información de la tabla de cuadre
        
        lvSentencia :='insert /*+ APPEND */ into CO_REPSALDOS  NOLOGGING (customer_id,custcode,apellidos,nombres,ced_ruc,producto,provincia,canton,region,telefono,telefono_contacto1,telefono_contacto2,direccion,direccion2,forma_pago,des_forma_pago,tipo_forma_pago,des_tipo_forma_pago,cstradecode,trade,balance_1,FECHA_1,balance_2,FECHA_2,balance_3,FECHA_3,balance_4,FECHA_4,balance_5,FECHA_5,balance_6,FECHA_6,balance_7,FECHA_7,balance_8,FECHA_8,balance_9,FECHA_9,balance_10,FECHA_10,balance_11,FECHA_11,balance_12,FECHA_12 ) '||
                      ' select a.customer_id,a.custcode,a.apellidos,a.nombres,a.RUC,a.producto,a.provincia,a.canton,a.region,a.telefono,a.CONT1,a.CONT2,a.direccion,a.direccion2,a.forma_pago,a.des_forma_pago,a.tipo_forma_pago,a.des_tipo_forma_pago,b.cstradecode,a.TRADE,a.balance_1,a.FECHA_1,a.balance_2,a.FECHA_2,a.balance_3,a.FECHA_3,a.balance_4,a.FECHA_4,a.balance_5,a.FECHA_5,a.balance_6,a.FECHA_6,a.balance_7,a.FECHA_7,a.balance_8,a.FECHA_8,a.balance_9,a.FECHA_9,a.balance_10,a.FECHA_10,a.balance_11,a.FECHA_11,a.balance_12,a.FECHA_12  from co_cuadre a,customer_all b  where a.customer_id = b.customer_id';
        /*lvSentencia :='insert /*+ APPEND */-- into CO_REPSALDOS NOLOGGING (customer_id,custcode,apellidos,producto,provincia,canton,region,direccion,direccion2,forma_pago,des_forma_pago,tipo_forma_pago,des_tipo_forma_pago,cstradecode,balance_1,FECHA_1,balance_2,FECHA_2,balance_3,FECHA_3,balance_4,FECHA_4,balance_5,FECHA_5,balance_6,FECHA_6,balance_7,FECHA_7,balance_8,FECHA_8,balance_9,FECHA_9,balance_10,FECHA_10,balance_11,FECHA_11,balance_12,FECHA_12 ) '||
                    --  ' select a.customer_id,a.custcode,a.apellidos,a.producto,a.provincia,a.canton,a.region,a.direccion,a.direccion2,a.forma_pago,a.des_forma_pago,a.tipo_forma_pago,a.des_tipo_forma_pago,b.cstradecode,a.balance_1,a.FECHA_1,a.balance_2,a.FECHA_2,a.balance_3,a.FECHA_3,a.balance_4,a.FECHA_4,a.balance_5,a.FECHA_5,a.balance_6,a.FECHA_6,a.balance_7,a.FECHA_7,a.balance_8,a.FECHA_8,a.balance_9,a.FECHA_9,a.balance_10,a.FECHA_10,a.balance_11,a.FECHA_11,a.balance_12,a.FECHA_12  from co_cuadre a, customer_all b where a.customer_id = b.customer_id';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        commit;        
        
        -- proceso de balance de deuda del cliente y ajuste de creditos
        COK_REPORT_SALDOS.balanceo(pdFech_ini, pdFech_fin);
        
        -- se calcula la edad de mora
        COK_REPORT_SALDOS.calcula_mora;                                               
        
        -- se calcula el total de la deuda del cliente
        COK_REPORT_SALDOS.calcula_total;

END MAIN;


end COK_REPORT_SALDOS;
/

