CREATE OR REPLACE Package OCK_RECLASIFICA_CUENTAS Is

  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Diana Chonillo
  -- PDS Leader: SUD Cristhian Acosta
  -- Created : 13/03/2014
  -- Purpose : Cargar OCC por Reclasificacion de Cartera a la tabla FEES

  -- Public type declarations
  Procedure Despacha_Cola(pi_bitacora  In Number,
                          pi_hilo      In Number,
                          pi_mod       In Number,
                          pi_total     Out Number,
                          pi_registros Out Number,
                          pi_commit    Out Number,
                          pi_error     Out Number,
                          pv_mensaje   Out Varchar2,
                          pv_error     Out Varchar2);
  Function Procesa_RegOCC(Pv_Rowid In Varchar2,
                          Pv_Cia   In Varchar2,
                          Pi_Total Out Number,
                          Pv_Error Out Varchar2) Return Number;
  
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Ma. Elizabeth Estevez
  -- PDS Leader: SUD Cristhian Acosta
  -- Created : 05/08/2014
  -- Purpose : Cargar OCC por despacho de cuotas para cuentas ya Reclasificadas a la tabla FEES
  Procedure Despacha_Cola2(pi_bitacora  In Number,
                           pi_hilo      In Number,
                           pi_mod       In Number,
                           pi_total     Out Number,
                           pi_registros Out Number,
                           pi_commit    Out Number,
                           pi_error     Out Number,
                           pv_mensaje   Out Varchar2,
                           pv_error     Out Varchar2);
  Function Procesa_RegOCC2(Pv_Rowid In Varchar2,
                           Pv_Cia   In Varchar2,
                           Pi_Total Out Number,
                           Pv_Error Out Varchar2) Return Number;
  -- 7399 - PROCESAMIENTO DE CUENTAS CON RECLASIFICACION PREVIA
  
  Function Obtener_Valor_Parametro(pn_id_tipo_parametro In Number,
                                   pv_id_parametro      In Varchar2)
    Return Varchar2;
  Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                                Pn_Coid       In Number,
                                Pn_Sncode     In Number,
                                Pn_Tmcode     In Number,
                                Pv_Fee_Type   In Varchar2,
                                Pn_Period     In Number,
                                Pv_Remark     In Varchar2,
                                Pv_Error      Out Varchar2) Return Number;

  Function Obtiene_Modelo_Tarifa(Pn_Tmcode           In Number,
                                 Pn_Sncode           In Number,
                                 Pn_Spcode           Out Number,
                                 Pv_Accglcode        Out Varchar2,
                                 Pv_Accserv_Catcode  Out Varchar2,
                                 Pv_Accserv_Code     Out Varchar2,
                                 Pv_Accserv_Type     Out Varchar2,
                                 Pv_Accglcode_Disc   Out Varchar2,
                                 Pv_Accglcode_Mincom Out Varchar2,
                                 Pn_Vscode           Out Integer,
                                 Pv_Error            Out Varchar2)
    Return Number;

  Function Obtiene_Codigo_Evento(Pn_Sncode In Number,
                                 Pn_Evcode Out Number,
                                 Pv_Error  Out Varchar2) Return Number;

  Function Obtiene_Secuencia_Fees(Pn_Customerid In Number,
                                  Pn_Seqno      Out Number,
                                  Pv_Error      Out Varchar2) Return Number;

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pv_Error           Out Varchar2) Return Number;

End OCK_RECLASIFICA_CUENTAS;
/
CREATE OR REPLACE Package Body OCK_RECLASIFICA_CUENTAS Is

  ------------------------------------------------------------------------------------ 
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Diana Chonillo
  -- PDS Leader: SUD Cristhian Acosta
  -- Created : 17/03/2014
  -- Purpose : Cargar OCC a la tabla FEES provenientes de AXIS
  ------------------------------------------------------------------------------------  

  Procedure Despacha_Cola(pi_bitacora  In Number,
                          pi_hilo      In Number,
                          pi_mod       In Number,
                          pi_total     Out Number,
                          pi_registros Out Number,
                          pi_commit    Out Number,
                          pi_error     Out Number,
                          pv_mensaje   Out Varchar2,
                          pv_error     Out Varchar2) Is
  
    Cursor c_cola_cartera Is
      select a.cia, max(fecha) fecha
        from co_cola_cartera a
       where a.tipo_cartera = 'R'
         and a.estado_ejecucion = 'P'
       group by a.cia;
  
    Cursor c_cuentas_cartera(Cv_Cia  Varchar2,
                             Cn_hilo Number,
                             Cn_mod  Number) Is
      Select b.rowid, b.cuenta, b.total
        From co_cartera_clientes b
       Where b.cia = Cv_Cia
         And b.proceso = 'P'
         And b.estado = 'V'
         And b.estado_cartera = 'R'
         And b.no_factura is not null
         And b.fecha_modificacion_cartera is not null
         And Mod(Substr(b.cuenta, 3), Cn_mod) = Cn_hilo;
  
    lv_error    Varchar2(8000);
    lv_mensaje  Varchar2(8000);
    lv_estado   Varchar2(1) := 'P'; --procesado;
    ln_mod      Number;
    ln_hilo     Number;
    ln_commit   Number;
    ln_contador Number;
    ln_commited Number;
    ln_contErr  Number;
    ln_contOK   Number;
    ln_total    Number;
    ln_suma     Number;
    le_error Exception;
    --
    lv_aplicacion       varchar2(100) := 'Ock_Reclasifica_Cuentas.Despacha_Cola';
    ln_detalle_bitacora number;
    ln_id_bitacora      number;
    ln_error_scp        number;
    lv_error_scp        varchar2(4000);
    lv_etapa_proc       varchar2(50) := 'Registro de OCC';
    ln_numerror         number := 0;
    le_general Exception;
  
  Begin
    If Nvl(pi_hilo, -1) = -1 Then
      Lv_error := 'No procede. Ingrese hilo de ejecucion.';
      raise le_general;
    End If;
    --
    If Nvl(pi_mod, -1) = -1 Then
      Lv_error := 'No procede. Ingrese parametro de ejecucion.';
      raise le_general;
    End If;
    --
    ln_hilo := pi_hilo;
    ln_mod  := pi_mod;
    --
    ln_commit := to_number(obtener_valor_parametro(7399, 'NUM_COMMIT_G'));
    --
    If Nvl(ln_commit, -1) = -1 Then
      Lv_error := 'No procede. Parametro no configurado.';
      raise le_general;
    End If;
    --
    ln_id_bitacora := pi_bitacora;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'Iniciando proceso - hilo #(' ||
                                                                       to_char(pi_hilo) || ') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'En proceso - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo),
                                              pv_cod_aux_2          => null,
                                              pv_cod_aux_3          => null,
                                              pn_cod_aux_4          => null,
                                              pn_cod_aux_5          => null,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error    := lv_aplicacion || '- ' || lv_error_scp;
      ln_numerror := -30;
      raise le_error;
    end if;
    --
    ln_contErr  := 0;
    ln_contOK   := 0;
    ln_commited := 0;
    ln_suma     := 0;
    --
    For x In c_cola_cartera Loop
      For y In c_cuentas_cartera(x.cia, ln_hilo, ln_mod) Loop
        Savepoint sp_procesaocc;
        lv_estado  := 'P';
        lv_mensaje := Null;
      
        If Procesa_RegOCC(Pv_Rowid => y.rowid,
                          Pv_Cia   => x.cia,
                          Pi_Total => ln_total,
                          Pv_Error => lv_error) <> 0 Then
          lv_estado  := 'E';
          lv_mensaje := lv_error;
        
          begin
            Update co_cartera_clientes f
               Set f.estado     = lv_estado,
                   f.comentario = Substr(lv_mensaje || ' - ' || f.comentario,
                                         1,
                                         2000)
             Where f.rowid = y.rowid;
            ln_contErr := ln_contErr + 1;
          exception
            when others then
              Rollback To sp_procesaocc;
              lv_error := 'Fallo actualizacion de tabla de trabajo:' ||
                          substr(sqlerrm, 1, 1000);
              Raise le_general;
          end;
        Else
          lv_estado  := 'D';
          lv_mensaje := 'CARGA EN FEES EXITOSA. ';
          begin
            Update co_cartera_clientes v
               Set v.estado     = lv_estado,
                   v.comentario = Substr(lv_mensaje || ' - ' || v.comentario,
                                         1,
                                         2000)
             Where v.rowid = y.rowid;
            ln_contOK := ln_contOK + 1;
            ln_suma   := ln_suma + ln_total;
          exception
            when others then
              Rollback To sp_procesaocc;
              lv_error := 'Fallo actualizacion de tabla de trabajo:' ||
                          substr(sqlerrm, 1, 1000);
              Raise le_general;
          end;
        End If;
        If ln_contador > ln_commit Then
          Commit;
          ln_contador := 0;
        End If;
        ln_contador := ln_contador + 1;
        ln_commited := ln_commited + 1;
      End Loop;
      Commit;
    End Loop;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'Finalizando proceso - hilo #(' ||
                                                                       to_char(pi_hilo) || ') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'En proceso - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo),
                                              pv_cod_aux_2          => ln_suma,
                                              pv_cod_aux_3          => ln_commited,
                                              pn_cod_aux_4          => ln_contOK,
                                              pn_cod_aux_5          => ln_contErr,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error    := lv_aplicacion || '- ' || lv_error_scp;
      ln_numerror := -30;
      raise le_error;
    end if;
  
    pi_total     := ln_suma;
    pi_registros := ln_commited;
    pi_commit    := ln_contOK;
    pi_error     := ln_contErr;
  
    pv_mensaje := 'Proceso de Carga a FEES => Se realizaron ' ||
                  ln_commited || ' registros.' || CHR(13) ||
                  'No. registros con Exito => ' || ln_contOK ||
                  ' <==> Total Valores Cargados => $' || ln_suma || CHR(13) ||
                  'No. registros con Error => ' || ln_contErr || CHR(13);
  
  Exception
    When le_error Then
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'Error de proceso en hilo #(' ||
                                                                         to_char(pi_hilo) ||
                                                                         ') [' ||
                                                                         lv_etapa_proc || ']',
                                                pv_mensaje_tecnico    => substr('En proceso - ' ||
                                                                                lv_aplicacion ||
                                                                                lv_error,
                                                                                1,
                                                                                4000),
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => ln_numerror,
                                                pv_cod_aux_1          => to_char(pi_hilo),
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
    When le_general Then
      --Rollback;
      pv_error := Substr('Error de ejecucion - ' || lv_error, 1, 1000);
    When Others Then
      --Rollback;
      pv_error := Substr('Error de ejecucion - ' || SQLERRM, 1, 1000);
  End;
  
  ------------------------------------------------------------------------------------ 
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Ma. Elizabeth Estevez
  -- PDS Leader: SUD Rosario Garcia
  -- Created : 05/08/2014
  -- Purpose : Cargar OCC de cuentas en 'bypass a la tabla FEES provenientes de AXIS
  ------------------------------------------------------------------------------------  
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Ma. Elizabeth Estevez
  -- PDS Leader: SUD Cristhian Acosta Chambers
  -- Created : 09/01/2015
  -- Purpose : Ajuste a proceso - Ejecución mientras exista agendamiento procesado
  ------------------------------------------------------------------------------------   
  Procedure Despacha_Cola2(pi_bitacora  In Number,
                           pi_hilo      In Number,
                           pi_mod       In Number,
                           pi_total     Out Number,
                           pi_registros Out Number,
                           pi_commit    Out Number,
                           pi_error     Out Number,
                           pv_mensaje   Out Varchar2,
                           pv_error     Out Varchar2) Is
  
    Cursor c_cola_cartera Is
      select a.cia, max(fecha) fecha
        from co_cola_cartera a
       where a.tipo_cartera = 'R'
         and a.estado_ejecucion = 'P'
         -- Ajuste - SUD MTR - 18/12/2014
         -- Para que se ejecute cuando exista un agendamiento en la cola
         -- Por reclasificación Procesada
          and trunc(sysdate-1) in (select max(trunc(fecha)) fecha
                                    from co_cola_cartera a1
                                    where a1.tipo_cartera = 'R'
                                    and a1.estado_ejecucion = 'P'
                                    and a1.cia = a.cia
                                  )
         -- Ajuste - SUD MTR - 18/12/2014
       group by a.cia;
  
    Cursor c_cuentas_cartera(Cv_Cia   Varchar2,
                             Cn_hilo  Number,
                             Cn_mod   Number,
                             Cn_total Number) Is
      Select b.rowid, b.cuenta, b.total
        From co_cartera_clientes b
       Where b.cia = Cv_Cia
         And b.proceso = 'P'
         And b.estado = 'V'
         And b.estado_cartera is null
         And b.no_factura is null
         And b.fecha_modificacion_cartera is null
         And b.mora Between 8 and 59
         And b.total Between 0.01 and Cn_total
         And Mod(Substr(b.cuenta, 3), Cn_mod) = Cn_hilo;
  
    lv_error    Varchar2(8000);
    lv_mensaje  Varchar2(8000);
    lv_estado   Varchar2(1) := 'P'; --procesado;
    ln_mod      Number;
    ln_hilo     Number;
    ln_commit   Number;
    ln_contador Number;
    ln_commited Number;
    ln_contErr  Number;
    ln_contOK   Number;
    ln_total    Number;
    ln_suma     Number;
    le_error Exception;
    --
    lv_aplicacion       varchar2(100) := 'Ock_Reclasifica_Cuentas.Despacha_Cola2';
    ln_detalle_bitacora number;
    ln_id_bitacora      number;
    ln_error_scp        number;
    lv_error_scp        varchar2(4000);
    lv_etapa_proc       varchar2(50) := 'Registro de OCC - BYPASS';
    ln_numerror         number := 0;
    le_general Exception;
    --
    ln_bypass  number:= to_number(Obtener_Valor_Parametro(7399,'BYPASS_BSCS'));
  
  Begin
    If Nvl(pi_hilo, -1) = -1 Then
      Lv_error := 'No procede. Ingrese hilo de ejecucion.';
      raise le_general;
    End If;
    --
    If Nvl(pi_mod, -1) = -1 Then
      Lv_error := 'No procede. Ingrese parametro de ejecucion.';
      raise le_general;
    End If;
    --
    ln_hilo := pi_hilo;
    ln_mod  := pi_mod;
    --
    ln_commit := to_number(obtener_valor_parametro(7399, 'NUM_COMMIT_G'));
    --
    If Nvl(ln_commit, -1) = -1 Then
      Lv_error := 'No procede. Parametro no configurado.';
      raise le_general;
    End If;
    --
    ln_id_bitacora := pi_bitacora;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'Iniciando proceso - hilo #(' ||
                                                                       to_char(pi_hilo) || ') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'En proceso - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo),
                                              pv_cod_aux_2          => null,
                                              pv_cod_aux_3          => null,
                                              pn_cod_aux_4          => null,
                                              pn_cod_aux_5          => null,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error    := lv_aplicacion || '- ' || lv_error_scp;
      ln_numerror := -30;
      raise le_error;
    end if;
    --
    ln_contErr  := 0;
    ln_contOK   := 0;
    ln_commited := 0;
    ln_suma     := 0;
    --
    For x In c_cola_cartera Loop
      For y In c_cuentas_cartera(x.cia, ln_hilo, ln_mod, ln_bypass) Loop
        Savepoint sp_procesaocc2;
        lv_estado  := 'P';
        lv_mensaje := Null;
      
        If Procesa_RegOCC2(Pv_Rowid => y.rowid,
                           Pv_Cia   => x.cia,
                           Pi_Total => ln_total,
                           Pv_Error => lv_error) <> 0 Then
          lv_estado  := 'E';
          lv_mensaje := lv_error;
        
          begin
            Update co_cartera_clientes f
               Set f.estado     = lv_estado,
                   f.comentario = Substr(lv_mensaje || ' - ' || f.comentario,
                                         1,
                                         2000)
             Where f.rowid = y.rowid;
            ln_contErr := ln_contErr + 1;
          exception
            when others then
              Rollback To sp_procesaocc2;
              lv_error := 'Fallo actualizacion de tabla de trabajo:' ||
                          substr(sqlerrm, 1, 1000);
              Raise le_general;
          end;
        Else
          lv_estado  := 'B'; -- Nuevo estado -- por BYPASS
          lv_mensaje := 'CARGA EN FEES [BYPASS DEUDA] EXITOSA. ';
          begin
            Update co_cartera_clientes v
               Set v.estado     = lv_estado,
                   v.comentario = Substr(lv_mensaje || ' - ' || v.comentario,
                                         1,
                                         2000)
             Where v.rowid = y.rowid;
            ln_contOK := ln_contOK + 1;
            ln_suma   := ln_suma + ln_total;
          exception
            when others then
              Rollback To sp_procesaocc2;
              lv_error := 'Fallo actualizacion de tabla de trabajo:' ||
                          substr(sqlerrm, 1, 1000);
              Raise le_general;
          end;
        End If;
        If ln_contador > ln_commit Then
          Commit;
          ln_contador := 0;
        End If;
        ln_contador := ln_contador + 1;
        ln_commited := ln_commited + 1;
      End Loop;
      Commit;
    End Loop;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'Finalizando proceso - hilo #(' ||
                                                                       to_char(pi_hilo) || ') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'En proceso - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo),
                                              pv_cod_aux_2          => ln_suma,
                                              pv_cod_aux_3          => ln_commited,
                                              pn_cod_aux_4          => ln_contOK,
                                              pn_cod_aux_5          => ln_contErr,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error    := lv_aplicacion || '- ' || lv_error_scp;
      ln_numerror := -30;
      raise le_error;
    end if;
  
    pi_total     := ln_suma;
    pi_registros := ln_commited;
    pi_commit    := ln_contOK;
    pi_error     := ln_contErr;
  
    pv_mensaje := 'Proceso de Carga a FEES => Se realizaron ' ||
                  ln_commited || ' registros.' || CHR(13) ||
                  'No. registros con Exito => ' || ln_contOK ||
                  ' <==> Total Valores Cargados => $' || ln_suma || CHR(13) ||
                  'No. registros con Error => ' || ln_contErr || CHR(13);
  
  Exception
    When le_error Then
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'Error de proceso en hilo #(' ||
                                                                         to_char(pi_hilo) ||
                                                                         ') [' ||
                                                                         lv_etapa_proc || ']',
                                                pv_mensaje_tecnico    => substr('En proceso - ' ||
                                                                                lv_aplicacion ||
                                                                                lv_error,
                                                                                1,
                                                                                4000),
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => ln_numerror,
                                                pv_cod_aux_1          => to_char(pi_hilo),
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
    When le_general Then
      --Rollback;
      pv_error := Substr('Error de ejecucion - ' || lv_error, 1, 1000);
    When Others Then
      --Rollback;
      pv_error := Substr('Error de ejecucion - ' || SQLERRM, 1, 1000);
  End;
  
  ------------------------------------------------------------------------------------ 
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Ma. Elizabeth Estevez
  -- PDS Leader: SUD Cristhian Acosta
  -- Modified : 22/07/2014
  -- Purpose : OCCs generados con fecha del d?-a anterior a la ejecuci??n
  ------------------------------------------------------------------------------------

  Function Procesa_RegOCC(Pv_Rowid In Varchar2,
                          Pv_Cia   In Varchar2,
                          Pi_Total Out Number,
                          Pv_Error Out Varchar2) Return Number Is
  
    Cursor c_cola_cartera(Cv_cia Varchar2) Is
      select 1
        from co_cola_cartera a
       where a.tipo_cartera = 'R'
         and a.estado_ejecucion = 'P'
         And a.cia = Cv_Cia
         and a.fecha in (select max(h.fecha)
                           from co_cola_cartera h
                          where h.tipo_cartera = a.tipo_cartera
                            and h.estado_ejecucion = a.estado_ejecucion
                            And h.cia = a.cia);
  
    Cursor c_Procesa_Occ(Cv_Rowid Varchar2) Is
      Select c.cuenta, c.total, d.customer_id
      /*Co_Id,
      Customer_Id,
      Tmcode,
      Sncode,
      Period,
      Valid_From,
      Remark,
      Amount,
      Fee_Type,
      Fee_Class,
      Username*/
        From co_cartera_clientes c, customer_all d
       Where c.Rowid = Cv_Rowid
         And c.cuenta = d.custcode;
  
    Lc_Procesa_Occ c_Procesa_Occ%Rowtype;
    Le_Error Exception;
    Lv_Error            Varchar2(4000);
    Lv_Programa         Varchar2(60) := 'OCK_RECLASIFICA_CUENTAS.CARGA_PRINCIPAL';
    Ln_Spcode           Mpulktmb.Spcode%Type;
    Lv_Accglcode        Mpulktmb.Accglcode%Type;
    Lv_Accserv_Catcode  Mpulktmb.Accserv_Catcode%Type;
    Lv_Accserv_Code     Mpulktmb.Accserv_Code%Type;
    Lv_Accserv_Type     Mpulktmb.Accserv_Type%Type;
    Lv_Accglcode_Disc   Mpulktmb.Accglcode_Disc%Type;
    Lv_Accglcode_Mincom Mpulktmb.Accglcode_Mincom%Type;
    Ln_Vscode           Mpulktmb.Vscode%Type;
    Ln_Evcode           Mpulkexn.Evcode%Type;
    Ln_Seqno            Fees.Seqno%Type;
    Ln_Currency         Forcurr.Fc_Id%Type := 19; ---'USD'
    --
    lc_cola        Number := 0;
    ln_tmcode      Number;
    ln_sncode      Number;
	ln_period	   Number;
    lv_usuario     Varchar2(8);
    lv_descripcion Varchar2(200);
    lv_pc          Varchar2(20);
    lv_mes         Varchar2(7);
    lv_dia         Varchar2(2);
  
  Begin
    --mtr
    Open c_cola_cartera(Pv_Cia);
    Fetch c_cola_cartera
      Into lc_cola;
    Close c_cola_cartera;
    If Nvl(lc_cola, 0) <> 0 Then
      ln_tmcode      := to_number(Obtener_Valor_Parametro(7399, 'TMCODE'));
      ln_sncode      := to_number(Obtener_Valor_Parametro(7399, 'SNCODE'));
      -- [7399] SUD MTR INI 12092014
      --lv_usuario     := Obtener_Valor_Parametro(7399, 'OCC_USER');
      If Pv_Cia = '1' Then
        lv_usuario := Obtener_Valor_Parametro(7399, 'OCC_USER_GYE');
      ElsIf Pv_Cia = '2' Then
        lv_usuario := Obtener_Valor_Parametro(7399, 'OCC_USER_UIO');
      End If;
      -- [7399] SUD MTR FIN 12092014
      lv_descripcion := Obtener_Valor_Parametro(7399, 'REMARK');
	    ln_period		   := to_number(Obtener_Valor_Parametro(7399, 'PERIOD'));
      --fin mtr
      Open c_Procesa_Occ(Pv_Rowid);
      Fetch c_Procesa_Occ
        Into Lc_Procesa_Occ;
      Close c_Procesa_Occ;
    
      --Verifico si est?!n todos los campos en la tabla occ_carga_tmp
      If Lc_Procesa_Occ.Customer_Id Is Null Or Nvl(ln_tmcode, 0) = 0 Or
         Nvl(ln_sncode, 0) = 0 Or ln_period Is Null Or
         lv_descripcion Is Null Or Lc_Procesa_Occ.total Is Null Then
        Lv_Error := Lv_Programa ||
                    ': Todos los campos de la tabla occ_carga_tmp son obligatorios, excluyendo dn_num. Favor revisar ';
        Raise Le_Error;
      End If;
      --Obtengo Modelo de Tarifa para el sncode(servicio) en ese tmcode(plan) en la tabla mpulktmb
      If Obtiene_Modelo_Tarifa(Pn_Tmcode           => ln_tmcode,
                               Pn_Sncode           => ln_sncode,
                               Pn_Spcode           => Ln_Spcode,
                               Pv_Accglcode        => Lv_Accglcode,
                               Pv_Accserv_Catcode  => Lv_Accserv_Catcode,
                               Pv_Accserv_Code     => Lv_Accserv_Code,
                               Pv_Accserv_Type     => Lv_Accserv_Type,
                               Pv_Accglcode_Disc   => Lv_Accglcode_Disc,
                               Pv_Accglcode_Mincom => Lv_Accglcode_Mincom,
                               Pn_Vscode           => Ln_Vscode,
                               Pv_Error            => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      If Obtiene_Codigo_Evento(Pn_Sncode => ln_sncode,
                               Pn_Evcode => Ln_Evcode,
                               Pv_Error  => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      If Obtiene_Secuencia_Fees(Pn_Customerid => Lc_Procesa_Occ.Customer_Id,
                                Pn_Seqno      => Ln_Seqno,
                                Pv_Error      => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      Select SYS_CONTEXT('USERENV', 'TERMINAL') into lv_pc From dual;
    
      lv_mes := to_char(sysdate, 'YYYY-MM');
    
      lv_dia := to_char(sysdate - 1, 'DD'); --to_char(sysdate, 'DD');
    
      lv_descripcion := Replace(Replace(Replace(lv_descripcion,
                                                '[%PCNAME%]',
                                                lv_pc),
                                        '[%MES%]',
                                        lv_mes),
                                '[%DIA%]',
                                lv_dia);
    
      If Inserta_Fees(Pn_Customer_Id     => Lc_Procesa_Occ.Customer_Id,
                      Pn_Seqno           => Ln_Seqno,
                      Pv_Feetype         => 'N', --
                      Pf_Amount          => Lc_Procesa_Occ.Total*(-1),
                      Pv_Remark          => lv_descripcion, --
                      Pv_Glcode          => Lv_Accglcode,
                      Pd_Entdate         => Sysdate - 1, -- Sysdate,
                      Pn_Period          => ln_period,
                      Pv_Username        => lv_usuario, --
                      Pd_Validfrom       => TRUNC(sysdate - 1), --TRUNC(sysdate),
                      Pn_Jobcost         => Null,
                      Pv_Billfmt         => Null,
                      Pv_Servcatcode     => Lv_Accserv_Catcode,
                      Pv_Servcode        => Lv_Accserv_Code,
                      Pv_Servtype        => Lv_Accserv_Type,
                      Pn_Coid            => Null,
                      Pf_Amountgross     => Null,
                      Pn_Currency        => Ln_Currency,
                      Pv_Glcodedisc      => Lv_Accglcode_Disc,
                      Pn_Jobcostiddisc   => Null,
                      Pv_Glcodemincom    => Lv_Accglcode_Mincom,
                      Pn_Jobcostidmincom => Null,
                      Pn_Recversion      => 1,
                      Pn_Cdrid           => Null,
                      Pn_Cdrsubid        => Null,
                      Pn_Udrbasepartid   => Null,
                      Pn_Udrchargepartid => Null,
                      Pn_Tmcode          => ln_tmcode,
                      Pn_Vscode          => Ln_Vscode,
                      Pn_Spcode          => Ln_Spcode,
                      Pn_Sncode          => ln_sncode,
                      Pn_Evcode          => Ln_Evcode,
                      Pn_Feeclass        => 3,
                      Pn_Fupackid        => Null,
                      Pn_Fupversion      => Null,
                      Pn_Fupseq          => Null,
                      Pn_Version         => Null,
                      Pn_Freeunitsnumber => Null,
                      Pv_Error           => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
      Pi_Total := Lc_Procesa_Occ.total;
      Return 0;
    End If; -- mtr
  
  Exception
    When Le_Error Then
      Pv_Error := Lv_Error;
      Return - 1;
    When Others Then
      Pv_Error := Sqlerrm;
      Return - 1;
  End Procesa_RegOCC;
  
  ------------------------------------------------------------------------------------ 
  -- Proyect : [7399] RECLASIFICACION AUTOMATICA DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Leader	 : SIS Ma. Elizabeth Estevez
  -- PDS Leader: SUD Cristhian Acosta
  -- Modified : 22/07/2014
  -- Purpose : OCCs generados con fecha del d?-a anterior a la ejecuci??n
  ------------------------------------------------------------------------------------

  Function Procesa_RegOCC2(Pv_Rowid In Varchar2,
                           Pv_Cia   In Varchar2,
                           Pi_Total Out Number,
                           Pv_Error Out Varchar2) Return Number Is
  
    Cursor c_cola_cartera(Cv_cia Varchar2) Is
      select 1
        from co_cola_cartera a
       where a.tipo_cartera = 'R'
         and a.estado_ejecucion = 'P'
         And a.cia = Cv_Cia
         and a.fecha in (select max(h.fecha)
                           from co_cola_cartera h
                          where h.tipo_cartera = a.tipo_cartera
                            and h.estado_ejecucion = a.estado_ejecucion
                            And h.cia = a.cia);
  
    Cursor c_Procesa_Occ(Cv_Rowid Varchar2) Is
      Select c.cuenta, c.total, d.customer_id
      /*Co_Id,
      Customer_Id,
      Tmcode,
      Sncode,
      Period,
      Valid_From,
      Remark,
      Amount,
      Fee_Type,
      Fee_Class,
      Username*/
        From co_cartera_clientes c, customer_all d
       Where c.Rowid = Cv_Rowid
         And c.cuenta = d.custcode;
  
    Lc_Procesa_Occ c_Procesa_Occ%Rowtype;
    Le_Error Exception;
    Lv_Error            Varchar2(4000);
    Lv_Programa         Varchar2(60) := 'OCK_RECLASIFICA_CUENTAS.CARGA_PRINCIPAL2';
    Ln_Spcode           Mpulktmb.Spcode%Type;
    Lv_Accglcode        Mpulktmb.Accglcode%Type;
    Lv_Accserv_Catcode  Mpulktmb.Accserv_Catcode%Type;
    Lv_Accserv_Code     Mpulktmb.Accserv_Code%Type;
    Lv_Accserv_Type     Mpulktmb.Accserv_Type%Type;
    Lv_Accglcode_Disc   Mpulktmb.Accglcode_Disc%Type;
    Lv_Accglcode_Mincom Mpulktmb.Accglcode_Mincom%Type;
    Ln_Vscode           Mpulktmb.Vscode%Type;
    Ln_Evcode           Mpulkexn.Evcode%Type;
    Ln_Seqno            Fees.Seqno%Type;
    Ln_Currency         Forcurr.Fc_Id%Type := 19; ---'USD'
    --
    lc_cola        Number := 0;
    ln_tmcode      Number;
    ln_sncode      Number;
	  ln_period	     Number;
    lv_usuario     Varchar2(8);
    lv_descripcion Varchar2(200);
    lv_pc          Varchar2(20);
    lv_mes         Varchar2(7);
    lv_dia         Varchar2(2);
  
  Begin
    --mtr
    Open c_cola_cartera(Pv_Cia);
    Fetch c_cola_cartera
      Into lc_cola;
    Close c_cola_cartera;
    If Nvl(lc_cola, 0) <> 0 Then
      ln_tmcode      := to_number(Obtener_Valor_Parametro(7399, 'TMCODE'));
      ln_sncode      := to_number(Obtener_Valor_Parametro(7399, 'SNCODE'));
      -- [7399] SUD MTR INI 12092014
      --lv_usuario     := Obtener_Valor_Parametro(7399, 'OCC_USER');
      If Pv_Cia = '1' Then
        lv_usuario := Obtener_Valor_Parametro(7399, 'OCC_USER_GYE');
      ElsIf Pv_Cia = '2' Then
        lv_usuario := Obtener_Valor_Parametro(7399, 'OCC_USER_UIO');
      End If;
      -- [7399] SUD MTR FIN 12092014
      lv_descripcion := Obtener_Valor_Parametro(7399, 'REMARK2');
	    ln_period		   := to_number(Obtener_Valor_Parametro(7399, 'PERIOD'));
      --fin mtr
      Open c_Procesa_Occ(Pv_Rowid);
      Fetch c_Procesa_Occ
        Into Lc_Procesa_Occ;
      Close c_Procesa_Occ;
    
      --Verifico si est?!n todos los campos en la tabla occ_carga_tmp
      If Lc_Procesa_Occ.Customer_Id Is Null Or Nvl(ln_tmcode, 0) = 0 Or
         Nvl(ln_sncode, 0) = 0 Or ln_period Is Null Or
         lv_descripcion Is Null Or Lc_Procesa_Occ.total Is Null Then
        Lv_Error := Lv_Programa ||
                    ': Todos los campos de la tabla occ_carga_tmp son obligatorios, excluyendo dn_num. Favor revisar ';
        Raise Le_Error;
      End If;
      --Obtengo Modelo de Tarifa para el sncode(servicio) en ese tmcode(plan) en la tabla mpulktmb
      If Obtiene_Modelo_Tarifa(Pn_Tmcode           => ln_tmcode,
                               Pn_Sncode           => ln_sncode,
                               Pn_Spcode           => Ln_Spcode,
                               Pv_Accglcode        => Lv_Accglcode,
                               Pv_Accserv_Catcode  => Lv_Accserv_Catcode,
                               Pv_Accserv_Code     => Lv_Accserv_Code,
                               Pv_Accserv_Type     => Lv_Accserv_Type,
                               Pv_Accglcode_Disc   => Lv_Accglcode_Disc,
                               Pv_Accglcode_Mincom => Lv_Accglcode_Mincom,
                               Pn_Vscode           => Ln_Vscode,
                               Pv_Error            => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      If Obtiene_Codigo_Evento(Pn_Sncode => ln_sncode,
                               Pn_Evcode => Ln_Evcode,
                               Pv_Error  => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      If Obtiene_Secuencia_Fees(Pn_Customerid => Lc_Procesa_Occ.Customer_Id,
                                Pn_Seqno      => Ln_Seqno,
                                Pv_Error      => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
      Select SYS_CONTEXT('USERENV', 'TERMINAL') into lv_pc From dual;
    
      lv_mes := to_char(sysdate, 'YYYY-MM');
    
      lv_dia := to_char(sysdate - 1, 'DD'); --to_char(sysdate, 'DD');
    
      lv_descripcion := Replace(Replace(Replace(lv_descripcion,
                                                '[%PCNAME%]',
                                                lv_pc),
                                        '[%MES%]',
                                        lv_mes),
                                '[%DIA%]',
                                lv_dia);
    
      If Inserta_Fees(Pn_Customer_Id     => Lc_Procesa_Occ.Customer_Id,
                      Pn_Seqno           => Ln_Seqno,
                      Pv_Feetype         => 'N', --
                      Pf_Amount          => Lc_Procesa_Occ.Total*(-1),
                      Pv_Remark          => lv_descripcion, --
                      Pv_Glcode          => Lv_Accglcode,
                      Pd_Entdate         => Sysdate - 1, -- Sysdate,
                      Pn_Period          => ln_period,
                      Pv_Username        => lv_usuario, --
                      Pd_Validfrom       => TRUNC(sysdate - 1), --TRUNC(sysdate),
                      Pn_Jobcost         => Null,
                      Pv_Billfmt         => Null,
                      Pv_Servcatcode     => Lv_Accserv_Catcode,
                      Pv_Servcode        => Lv_Accserv_Code,
                      Pv_Servtype        => Lv_Accserv_Type,
                      Pn_Coid            => Null,
                      Pf_Amountgross     => Null,
                      Pn_Currency        => Ln_Currency,
                      Pv_Glcodedisc      => Lv_Accglcode_Disc,
                      Pn_Jobcostiddisc   => Null,
                      Pv_Glcodemincom    => Lv_Accglcode_Mincom,
                      Pn_Jobcostidmincom => Null,
                      Pn_Recversion      => 1,
                      Pn_Cdrid           => Null,
                      Pn_Cdrsubid        => Null,
                      Pn_Udrbasepartid   => Null,
                      Pn_Udrchargepartid => Null,
                      Pn_Tmcode          => ln_tmcode,
                      Pn_Vscode          => Ln_Vscode,
                      Pn_Spcode          => Ln_Spcode,
                      Pn_Sncode          => ln_sncode,
                      Pn_Evcode          => Ln_Evcode,
                      Pn_Feeclass        => 3,
                      Pn_Fupackid        => Null,
                      Pn_Fupversion      => Null,
                      Pn_Fupseq          => Null,
                      Pn_Version         => Null,
                      Pn_Freeunitsnumber => Null,
                      Pv_Error           => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
      Pi_Total := Lc_Procesa_Occ.total;
      Return 0;
    End If; -- mtr
  
  Exception
    When Le_Error Then
      Pv_Error := Lv_Error;
      Return - 1;
    When Others Then
      Pv_Error := Sqlerrm;
      Return - 1;
  End Procesa_RegOCC2;

  -----------------------------
  Function Obtener_Valor_Parametro(pn_id_tipo_parametro In Number,
                                   pv_id_parametro      In Varchar2)
    Return Varchar2 Is
    --
  
    Cursor c_parametro(cn_id_tipo_parametro Number,
                       cv_id_parametro      Varchar2) Is
      Select valor
        From gv_parametros
       Where id_tipo_parametro = cn_id_tipo_parametro
         And id_parametro = cv_id_parametro;
  
    lv_valor gv_parametros.valor%Type;
  
  Begin
  
    Open c_parametro(pn_id_tipo_parametro, pv_id_parametro);
    Fetch c_parametro
      Into lv_valor;
    Close c_parametro;
  
    Return lv_valor;
  
  End Obtener_Valor_Parametro;
  ------------------------------------------------------------------------------------------------                                     
  --La validaci??n de duplicidad se la realiza especialmente verificando el REMARK.
  --Si el Remark ya existe entonces ese occ ya fue cargado. El REMARK debe venir
  --con un identificador ??nico para cada OCC
  Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                                Pn_Coid       In Number,
                                Pn_Sncode     In Number,
                                Pn_Tmcode     In Number,
                                Pv_Fee_Type   In Varchar2,
                                Pn_Period     In Number,
                                Pv_Remark     In Varchar2,
                                Pv_Error      Out Varchar2) Return Number Is
  
    Lv_Duplicado Varchar2(1);
    Lv_Programa  Varchar2(65) := 'OCK_RECLASIFICA_CUENTAS.VALIDA_OCC_DUPLICADO';
  Begin
    Select 'x'
      Into Lv_Duplicado
      From Fees
     Where Customer_Id = Pn_Customerid
       And Co_Id = Pn_Coid
       And Sncode = Pn_Sncode
       And Tmcode = Pn_Tmcode
       And Fee_Type = Pv_Fee_Type
       And Period = Pn_Period
       And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    If Lv_Duplicado Is Not Null Then
      Pv_Error := Lv_Programa ||
                  ': El OCC ya se encuentra cargado en la tabla fees';
      Return 1;
    End If;
  Exception
    When No_Data_Found Then
      Return 0;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End Valida_Occ_Duplicado;

  ----------------------------------------------------------------------------------------------------------------------
  Function Obtiene_Modelo_Tarifa(Pn_Tmcode           In Number,
                                 Pn_Sncode           In Number,
                                 Pn_Spcode           Out Number,
                                 Pv_Accglcode        Out Varchar2,
                                 Pv_Accserv_Catcode  Out Varchar2,
                                 Pv_Accserv_Code     Out Varchar2,
                                 Pv_Accserv_Type     Out Varchar2,
                                 Pv_Accglcode_Disc   Out Varchar2,
                                 Pv_Accglcode_Mincom Out Varchar2,
                                 Pn_Vscode           Out Integer,
                                 Pv_Error            Out Varchar2)
    Return Number Is
  
    Lv_Programa Varchar2(65) := 'OCK_RECLASIFICA_CUENTAS.OBTIENE_MODELO_TARIFA';
  
  Begin
    Select Accglcode,
           Accserv_Catcode,
           Accserv_Code,
           Accserv_Type,
           Vscode,
           Spcode,
           Accglcode_Disc,
           Accglcode_Mincom
      Into Pv_Accglcode,
           Pv_Accserv_Catcode,
           Pv_Accserv_Code,
           Pv_Accserv_Type,
           Pn_Vscode,
           Pn_Spcode,
           Pv_Accglcode_Disc,
           Pv_Accglcode_Mincom
      From Mpulktmb
     Where Tmcode = Pn_Tmcode
       And Sncode = Pn_Sncode
       And Vscode = (Select Max(a.Vscode)
                       From Mpulktmb a
                      Where Tmcode = Pn_Tmcode
                        And Sncode = Pn_Sncode);
    Return 0;
  Exception
    When No_Data_Found Then
      Pv_Error := Lv_Programa ||
                  ': No se encontraron datos para el tmcode ' || Pn_Tmcode ||
                  ' y el Sncode ' || Pn_Sncode || ' en la tabla Mpulktmb';
      Return - 1;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Obtiene_Codigo_Evento(Pn_Sncode In Number,
                                 Pn_Evcode Out Number,
                                 Pv_Error  Out Varchar2) Return Number Is
  
    Lv_Programa Varchar2(65) := 'OCK_RECLASIFICA_CUENTAS.OBTIENE_CODIGO_EVENTO';
  
  Begin
    Select Evcode Into Pn_Evcode From Mpulkexn Where Sncode = Pn_Sncode;
    Return 0;
  Exception
    When No_Data_Found Then
      Pv_Error := Lv_Programa ||
                  ': No se encontraron datos para el sncode ' || Pn_Sncode ||
                  ' en la tabla Mpulkexn';
      Return - 1;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Obtiene_Secuencia_Fees(Pn_Customerid In Number,
                                  Pn_Seqno      Out Number,
                                  Pv_Error      Out Varchar2) Return Number Is
    Lv_Programa Varchar2(65) := 'OCK_RECLASIFICA_CUENTAS.OBTIENE_SECUENCIA_FEES';
  
  Begin
    Select Nvl(Max(Seqno), 0) + 1
      Into Pn_Seqno
      From Fees
     Where Customer_Id = Pn_Customerid;
    Return 0;
  Exception
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pv_Error           Out Varchar2) Return Number Is
    Lv_Programa Varchar2(65) := 'OCK_RECLASIFICA_CUENTAS.INSERTA_FEES';
  
  Begin
    Insert Into Fees
      (Customer_Id,
       Seqno,
       Fee_Type,
       Amount,
       Remark,
       Glcode,
       Entdate,
       Period,
       Username,
       Valid_From,
       Jobcost,
       Bill_Fmt,
       Servcat_Code,
       Serv_Code,
       Serv_Type,
       Co_Id,
       Amount_Gross,
       Currency,
       Glcode_Disc,
       Jobcost_Id_Disc,
       Glcode_Mincom,
       Jobcost_Id_Mincom,
       Rec_Version,
       Cdr_Id,
       Cdr_Sub_Id,
       Udr_Basepart_Id,
       Udr_Chargepart_Id,
       Tmcode,
       Vscode,
       Spcode,
       Sncode,
       Evcode,
       Fee_Class,
       Fu_Pack_Id,
       Fup_Version,
       Fup_Seq,
       Version,
       Free_Units_Number)
    Values
      (Pn_Customer_Id,
       Pn_Seqno,
       Pv_Feetype,
       Pf_Amount,
       Pv_Remark,
       Pv_Glcode,
       Pd_Entdate,
       Pn_Period,
       Pv_Username,
       Pd_Validfrom,
       Pn_Jobcost,
       Pv_Billfmt,
       Pv_Servcatcode,
       Pv_Servcode,
       Pv_Servtype,
       Pn_Coid,
       Pf_Amountgross,
       Pn_Currency,
       Pv_Glcodedisc,
       Pn_Jobcostiddisc,
       Pv_Glcodemincom,
       Pn_Jobcostidmincom,
       Pn_Recversion,
       Pn_Cdrid,
       Pn_Cdrsubid,
       Pn_Udrbasepartid,
       Pn_Udrchargepartid,
       Pn_Tmcode,
       Pn_Vscode,
       Pn_Spcode,
       Pn_Sncode,
       Pn_Evcode,
       Pn_Feeclass,
       Pn_Fupackid,
       Pn_Fupversion,
       Pn_Fupseq,
       Pn_Version,
       Pn_Freeunitsnumber);
  
    Return 0;
  
  Exception
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End; 

End OCK_RECLASIFICA_CUENTAS;
/
