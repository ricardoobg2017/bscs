CREATE OR REPLACE PACKAGE GVK_PARAMETROS_GENERALES IS

  FUNCTION gvf_valida_valor_en_s(pn_id_tipo_parametro IN NUMBER,
                                 pv_id_parametro      IN VARCHAR2)
    RETURN BOOLEAN;

  FUNCTION gvf_obtener_valor_parametro(pn_id_tipo_parametro IN NUMBER,
                                       pv_id_parametro      IN VARCHAR2)
    RETURN VARCHAR2;

  PROCEDURE gvp_obtener_clase_parametro(pn_tipo            IN NUMBER,
                                        pv_nombre          IN VARCHAR2,
                                        pv_clase_parametro OUT VARCHAR2);

END GVK_PARAMETROS_GENERALES;
/
CREATE OR REPLACE PACKAGE BODY GVK_PARAMETROS_GENERALES IS

  FUNCTION gvf_valida_valor_en_s(pn_id_tipo_parametro IN NUMBER,
                                 pv_id_parametro      IN VARCHAR2)
    RETURN BOOLEAN IS
    lv_valor gv_parametros.valor%TYPE;
  BEGIN
    lv_valor := gvf_obtener_valor_parametro(pn_id_tipo_parametro,
                                            pv_id_parametro);
    RETURN nvl(lv_valor, 'N') = 'S';
  END gvf_valida_valor_en_s;

  FUNCTION gvf_obtener_valor_parametro(pn_id_tipo_parametro IN NUMBER,
                                       pv_id_parametro      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR c_parametro(cn_id_tipo_parametro NUMBER,
                       cv_id_parametro      VARCHAR2) IS
      SELECT valor
        FROM gv_parametros
       WHERE id_tipo_parametro = cn_id_tipo_parametro
         AND id_parametro = cv_id_parametro;
  
    lv_valor gv_parametros.valor%TYPE;
  
  BEGIN
  
    OPEN c_parametro(pn_id_tipo_parametro, pv_id_parametro);
    FETCH c_parametro
      INTO lv_valor;
    CLOSE c_parametro;
  
    RETURN lv_valor;
  
  END gvf_obtener_valor_parametro;

  /**************************************************************************************/
  /**/

  PROCEDURE gvp_obtener_clase_parametro(pn_tipo            IN NUMBER,
                                        pv_nombre          IN VARCHAR2,
                                        pv_clase_parametro OUT VARCHAR2) IS
  
  BEGIN
    SELECT clase
      INTO pv_clase_parametro
      FROM gv_parametros
     WHERE id_tipo_parametro = pn_tipo
       AND id_parametro = pv_nombre;
  
  EXCEPTION
    WHEN no_data_found THEN
      NULL;
    
  END gvp_obtener_clase_parametro;

END GVK_PARAMETROS_GENERALES;
/
