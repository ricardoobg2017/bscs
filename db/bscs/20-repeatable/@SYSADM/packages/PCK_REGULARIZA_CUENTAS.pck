create or replace package PCK_REGULARIZA_CUENTAS is

PROCEDURE EJECUTA_HILO(Pd_FechaCorte IN VARCHAR2, Pn_Hilo IN NUMBER);
PROCEDURE ASIGNA_HILO;
PROCEDURE INICIO(Pv_Corte          IN VARCHAR2,
                 Pv_DiaTopeCorte   IN VARCHAR2,
                 Pv_Error          OUT VARCHAR2);
end PCK_REGULARIZA_CUENTAS;
/
create or replace package body PCK_REGULARIZA_CUENTAS is
PROCEDURE EJECUTA_HILO(Pd_FechaCorte IN VARCHAR2, Pn_Hilo IN NUMBER) IS
--Proceso para la verificaci�n
  Cursor reg Is
  Select q.rowid, q.* 
  From gsi_cuentas_sin_mov_temp q 
  Where hilo=Pn_Hilo And 
        proceso Is Null;
  current_fecha Date:=TO_DATE(Pd_FechaCorte,'DD/MM/YYYY');--El �ltimo corte de facturaci�n del ciclo
  periodo_act  Date;
  periodo_ant1 Date;
  periodo_ant2 Date;
  periodo_ant3 Date;
  periodo_ant4 Date;
  periodo_ant5 Date;
  periodo_ant6 Date;
  Ld_maxord    Date;
Begin
   For i In reg Loop
      periodo_ant1:=Null;
      periodo_ant2:=Null;
      periodo_ant3:=Null;
      Select max(date_created) Into periodo_act  From document_reference h Where customer_id=i.customer_id And date_created=current_fecha;
      Select max(date_created) Into periodo_ant1 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-1);
      Select max(date_created) Into periodo_ant2 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-2);
      Select max(date_created) Into periodo_ant3 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-3);
      Select max(date_created) Into periodo_ant4 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-4);
      Select max(date_created) Into periodo_ant5 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-5);
      Select max(date_created) Into periodo_ant6 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-6);
      select max(ohentdate) into Ld_maxord from Orderhdr_All where customer_id=i.customer_id and ohstatus='IN';
      Insert Into gsi_sin_mov_temp Values
      (i.custcode, current_fecha, periodo_act, periodo_ant1, periodo_ant2,periodo_ant3,periodo_ant4,periodo_ant5,periodo_ant6, i.customer_id, Ld_maxord);
      Update gsi_cuentas_sin_mov_temp Set proceso='S' Where Rowid=i.rowid;
      Commit;
   End Loop;
End;

PROCEDURE ASIGNA_HILO IS
   CURSOR CLI IS
   SELECT J.ROWID, J.* FROM GSI_CUENTAS_SIN_MOV_TEMP  J;
   LN_SESION NUMBER:=1;
BEGIN
   FOR I IN CLI LOOP
      UPDATE GSI_CUENTAS_SIN_MOV_TEMP
      SET HILO=LN_SESION
      WHERE ROWID=I.ROWID;
      LN_SESION:=LN_SESION+1;
      IF LN_SESION>20 THEN
         LN_SESION:=1;
         COMMIT;
      END IF;
   END LOOP;
   COMMIT;
END ASIGNA_HILO;
/*
Descripci�n:Proceso que realizara la inactivacion de las cuentas que no hayan facturado por
            lo menos 6 meses continuos.
Par�metros: Pv_Corte        => N�mero de Corte a ejecutarse.
            Pv_DiaTopeCorte => Fecha de Corte de la ultima Facturaci�n ejecutada
            Pn_CicloInactivas => N�mero de ciclo a consultarse
Mapeo Realizado
  fup_account_period_id = 5 ---corte 04  --- ciclo inactivas 77 --dia 02
  fup_account_period_id = 1 ---corte 01  --- ciclo inactivas 23 --dia 24
  fup_account_period_id = 2 ---corte 02  --- ciclo inactivas 83 --dia 8 
  fup_account_period_id = 4 ---corte 03  --- ciclo inactivas 81 --dia 15
*/
PROCEDURE INICIO(Pv_Corte          IN VARCHAR2,
                 Pv_DiaTopeCorte   IN VARCHAR2,
                 Pv_Error          OUT VARCHAR2) IS
 Cursor C_Proceso is
  select  1 
  from gsi_cuentas_sin_mov_temp 
  where proceso is null
  and rownum < 2;
 Ln_Period_Id      billcycles.fup_account_period_id%type;
 Lv_CicloInactivas customer_all.billcycle%type;
 LVSENTENCIA     VARCHAR2(2000);
 ln_job NUMBER;
 Ln_Existe NUMBER;
 Ln_Error  NUMBER;
BEGIN
  IF Pv_Corte = '01'    THEN Ln_Period_Id := 1; Lv_CicloInactivas := '23';
  ELSIF Pv_Corte = '02' THEN Ln_Period_Id := 2; Lv_CicloInactivas := '83';
  ELSIF Pv_Corte = '03' THEN Ln_Period_Id := 4; Lv_CicloInactivas := '81';
  ELSIF Pv_Corte = '04' THEN Ln_Period_Id := 5; Lv_CicloInactivas := '77';
  ELSE Pv_Error := 'C�digo de corte no existe.'; Return;
  END IF;
  LVSENTENCIA := 'Drop Table gsi_cuentas_sin_mov_temp';
  EXECUTE IMMEDIATE LVSENTENCIA;
  LVSENTENCIA := 'Create Table gsi_cuentas_sin_mov_temp ' ||
                 'Tablespace billing_dat As ' ||
                 'Select /*+ index(h) +*/ customer_id, custcode, billcycle, lbc_date ' ||
                 'From customer_all h ' ||
                 'Where billcycle ' ||--='94' And lbc_date Is Not Null And customer_id_high Is Null
                 'In (Select billcycle ' ||
                      'From billcycles ' ||
                      'Where fup_account_period_id = ' || Ln_Period_Id ||') ' || --CAMBIAR PARAMETRO 
                 'and billcycle != '''|| Lv_CicloInactivas ||''' ' ||  --CAMBIAR PARAMETRO
                 'And lbc_date Is Not Null ' ||
                 'And customer_id_high Is Null';
  EXECUTE IMMEDIATE LVSENTENCIA;
  LVSENTENCIA :=  'Alter Table gsi_cuentas_sin_mov_temp ' ||
                  'Add hilo Number ' ||
                  'Add proceso Varchar2(1)';
  EXECUTE IMMEDIATE LVSENTENCIA;

  Ln_Error := 0;
  begin
    EXECUTE IMMEDIATE 'Truncate Table gsi_sin_mov_temp';
   exception
    when others then
     Ln_Error := sqlcode;
  end;
  IF Ln_Error = -942 THEN
    EXECUTE IMMEDIATE 'CREATE TABLE GSI_SIN_MOV_TEMP ' ||
                        '( ' ||
                          'CUENTA         VARCHAR2(40), ' ||
                          'FECHA          DATE, ' ||
                          'PERIODO_ACTUAL DATE, ' ||
                          'PERIODO_ANT_1  DATE, ' ||
                          'PERIODO_ANT_2  DATE, ' ||
                          'PERIODO_ANT_3  DATE, ' ||
                          'PERIODO_ANT_4  DATE, ' ||
                          'PERIODO_ANT_5  DATE, ' ||
                          'PERIODO_ANT_6  DATE, ' ||
                          'CUSTOMER_ID    NUMBER, ' ||
                          'ULT_ORDERHDR   DATE ' ||
                        ') ' ||
                        'tablespace BILLING_DAT ' ||
                          'pctfree 10 ' ||
                          'pctused 40 ' ||
                          'initrans 1 ' ||
                          'maxtrans 255 ' ||
                          'storage ' ||
                          '( ' ||
                            'initial 64K ' ||
                            'minextents 1 ' ||
                            'maxextents unlimited ' ||
                          ')';
  END IF;
  PCK_REGULARIZA_CUENTAS.ASIGNA_HILO;
  FOR I IN 0 .. 20 LOOP
      LVSENTENCIA := 'PCK_REGULARIZA_CUENTAS.EJECUTA_HILO(''' ||
                     Pv_DiaTopeCorte || ''',' || to_char(i) || ');';
      dbms_job.submit(job => ln_job, what => LVSENTENCIA);
  end loop;
  loop
   OPEN C_Proceso;
   FETCH C_Proceso INTO Ln_Existe;
   IF NVL(Ln_Existe,0) = 0 THEN
     exit;
   END IF;
   CLOSE C_Proceso;
  end loop;
  CLOSE C_Proceso;
  /*update Customer_All set billcycle= Lv_CicloInactivas
  where customer_id in ( select customer_id
                          from gsi_sin_mov_temp a 
                          where periodo_ant_1 is null
                            and periodo_ant_2 is null 
                            and periodo_ant_3 is null 
                            and periodo_ant_4 is null
                            and periodo_ant_5 is null 
                            and periodo_ant_6 is null 
                            and periodo_actual is null
                            and months_between(trunc(sysdate),nvl(ult_orderhdr,to_date('01/01/2005','dd/mm/yyyy')))>7);*/
END ;
end PCK_REGULARIZA_CUENTAS;
/
