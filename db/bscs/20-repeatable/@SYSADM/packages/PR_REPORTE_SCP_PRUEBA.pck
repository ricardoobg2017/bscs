CREATE OR REPLACE PACKAGE PR_REPORTE_SCP_PRUEBA IS
  PROCEDURE PR_PRINCIPAL_SCP(PV_FECHA        VARCHAR2,
                             PV_NOMBRE_VISTA OUT VARCHAR2,
                             PV_ERROR        OUT VARCHAR2);

  PROCEDURE PR_GENERA_INDICE(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_GENERA_TEMP_PR(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_CREA_VISTA_TMP(PV_FECHA        VARCHAR2,
                              PV_NOMBRE_VISTA OUT VARCHAR2,
                              PV_ERROR        OUT VARCHAR2);

  PROCEDURE PR_GEN_TEMP_PR_ANT_ACT(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_GENERA_TEMP_WORK_RP(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_GENERA_INDICE_RP(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_GENERA_REPORTE(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);
  -- Creaci�n de la tabla temporal, las que nos permite respaldar la informaci�n final de la tabla
  PROCEDURE PR_CPM_BUSCA_GRATIS_MES(PV_FECHA VARCHAR2,
                                    PV_ERROR OUT VARCHAR2);
  PROCEDURE PR_GENERA_RESPALDO_GSI(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2);

   PROCEDURE PR_GENERA_UPDATE_HILO(PV_FECHA VARCHAR2,
                                   PV_HILO NUMBER,                                   
                                   PV_ERROR OUT VARCHAR2);
                                       
  PROCEDURE PR_GENERA_BUSQUEDA_CLIENTE(PV_FECHA VARCHAR2,
                                       PV_HILO NUMBER,
                                       PV_ERROR OUT VARCHAR2) ;
                                       
  PROCEDURE PR_GENERA_UPDATE_GSI(PV_FECHA VARCHAR2, PV_HILO  NUMBER,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_GENERA_DELETE_GSI(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);
  
   
  PROCEDURE PR_GENERA_INSERT_GSI(PV_FECHA VARCHAR2,
                                 PV_HILO NUMBER,
                               PV_ERROR OUT VARCHAR2);
                               
  PROCEDURE PR_GENERA_BUSQUEDA_TMCODE(PV_FECHA VARCHAR2,
                                      PV_ERROR OUT VARCHAR2);
                                    
  PROCEDURE PR_GENERA_TEMPORAL_TMP_GSI(PV_FECHA VARCHAR2,
                                       PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_CREA_VISTA_TMP_MES(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);

END PR_REPORTE_SCP_PRUEBA;
/
CREATE OR REPLACE PACKAGE BODY PR_REPORTE_SCP_PRUEBA IS
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO PRINCIPAL, Genera reporte de llamadas gratis de los planes 
  --           (Numeros Favoritos & Mejor Amigo)
  --===============================================================================================--    

  CURSOR C_OBTIENE_SENTENCIA(CV_PARAMETRO VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS
     WHERE ID_TIPO_PARAMETRO = 8889
       AND NOMBRE = CV_PARAMETRO;
  -- cursor para obtener el mes de cada tabla 
  CURSOR C_OBTENER_MES(CV_MES VARCHAR2) IS
    SELECT DECODE(CV_MES,
                  '01',
                  'ENE',
                  '02',
                  'FEB',
                  '03',
                  'MAR',
                  '04',
                  'ABR',
                  '05',
                  'MAY',
                  '06',
                  'JUN',
                  '07',
                  'JUL',
                  '08',
                  'AGO',
                  '09',
                  'SEP',
                  '10',
                  'OCT',
                  '11',
                  'NOV',
                  '12',
                  'DIC')
      FROM DUAL;

  PROCEDURE PR_PRINCIPAL_SCP(PV_FECHA        VARCHAR2,
                             PV_NOMBRE_VISTA OUT VARCHAR2,
                             PV_ERROR        OUT VARCHAR2) IS
  
    LV_ERROR VARCHAR2(400);
    LV_HILO NUMBER;
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_PRINCIPAL';
  BEGIN
  
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'DEBE INGRESAR LA FECHA PARA GENERAR SCP';
      RETURN;
    END IF;
  
    -- Procediminto que genera indeces MC para las tablas UDR_LT
    PR_GENERA_INDICE(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      PV_ERROR := LV_ERROR;
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Procedimiento que genera tablas temporales PR
    PR_GENERA_TEMP_PR(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Crea las vistas TMP que contienen los datos de la las tablas temporales PR
    PR_CREA_VISTA_TMP(PV_FECHA        => PV_FECHA,
                      PV_NOMBRE_VISTA => PV_NOMBRE_VISTA,
                      PV_ERROR        => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Crea tablas temporales PR_(FECHA_ANTERIOR)_(FECHA_ACTUAL)
    PR_GEN_TEMP_PR_ANT_ACT(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Crea tablas temporales WORK_ & RP_
    PR_GENERA_TEMP_WORK_RP(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Genera indices PK para las tablas RP
    PR_GENERA_INDICE_RP(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Procedimiento que Genera Reporte
    PR_GENERA_REPORTE(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Genera tablas para mantener los datos finales Y l a tab�a 
    PR_GENERA_RESPALDO_GSI(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Genera los datos del campo hilo de la tablas temporales GSI
    PR_GENERA_UPDATE_HILO(PV_FECHA => PV_FECHA, PV_HILO => LV_HILO, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF; 
    -- Reaiza una busqueda de los clientes 
    PR_GENERA_BUSQUEDA_CLIENTE(PV_FECHA => PV_FECHA, PV_HILO => LV_HILO, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;  
    -- Genera el Update de las fechas segun los ciclos en las tablas temporales
    PR_GENERA_UPDATE_GSI(PV_FECHA => PV_FECHA, PV_HILO  => LV_HILO ,PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Procedimiento que inserta  datos GSI final
    PR_GENERA_INSERT_GSI(PV_FECHA => PV_FECHA, PV_HILO => LV_HILO, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Procedimiento que borra datos duplicados
    PR_GENERA_DELETE_GSI(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Genera busqueda del tmcode 
    PR_GENERA_BUSQUEDA_TMCODE(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
    -- Genera tablas temporales TMP_GSI
    PR_GENERA_TEMPORAL_TMP_GSI(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    -- Crea Vistas TMP_MES
    PR_CREA_VISTA_TMP_MES(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := SUBSTR(LV_APLICACION || LV_ERROR, 1, 4000);
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 4000);
  END PR_PRINCIPAL_SCP;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : Proceso que genera indices para la tabla UDR_LT 
  --===============================================================================================--

  PROCEDURE PR_GENERA_INDICE(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
  
    -- Declaracion de variables
    LV_NOMBRE_TABLA  VARCHAR2(2000);
    LV_NOMBRE_INDICE VARCHAR2(2000);
    LV_SENTENCIA     VARCHAR2(32650);
    LV_ERROR         VARCHAR2(2000);
    LN_EXISTE_TABLA  NUMBER;
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_INDICE - ';
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    FOR I IN 1 .. 4 LOOP
      LV_NOMBRE_INDICE := 'MC_' || SUBSTR(PV_FECHA, 7) ||
                          SUBSTR(PV_FECHA, 4, 2) || '_TAT_' || I;
    
      -- DBMS_OUTPUT.PUT_LINE(LV_NOMBRE_INDICE);
      LV_NOMBRE_TABLA := 'UDR_LT_' || SUBSTR(PV_FECHA, 7) ||
                         SUBSTR(PV_FECHA, 4, 2) || '_TAT_' || I;
    
      LV_SENTENCIA := 'SELECT COUNT(*) FROM ALL_TABLES WHERE TABLE_NAME = ''' ||
                      LV_NOMBRE_TABLA || ''' ';
    
      BEGIN
        EXECUTE IMMEDIATE LV_SENTENCIA
          INTO LN_EXISTE_TABLA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);
          IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE LE_EXCEPTION;
          END IF;
      END;
    
      -- si la tabla del cual consultamos no existe se hace un raise para no seguir
      IF LN_EXISTE_TABLA IS NULL OR LN_EXISTE_TABLA = 0 THEN
        LV_ERROR := 'NO EXISTE LA TABLA ' || LV_NOMBRE_TABLA ||
                    ', FAVOR REVISAR!';
        RAISE LE_EXCEPTION;
      END IF;
      --  tablespace BILLING_DAT2 
      LV_SENTENCIA:='create index 
                    :NOMBRE_INDICE  on 
                    :NOMBRE_TABLA (mc_scalefactor) 
                    tablespace BILLING_DAT2
                    Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
                    unlimited pctincrease 0)';
      -- DBMS_OUTPUT.PUT_LINE(LV_SENTENCIA);
      BEGIN
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_INDICE',
                                LV_NOMBRE_INDICE);
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA',
                                LV_NOMBRE_TABLA);
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);
          IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            --   RAISE LE_EXCEPTION;
            NULL;
          END IF;
      END;
    END LOOP;
  
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
    
  END PR_GENERA_INDICE;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO QUE GENERA TABLAS TEMPORALES PR_AAAAMM_TAT_C
  --===============================================================================================--

  PROCEDURE PR_GENERA_TEMP_PR(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
    -- Declaracion de Variables
    LV_NOMBRE_TABLA  VARCHAR2(2000);
    LV_NOMBRE_TABLA1 VARCHAR2(2000);
    LV_NOMBRE_TABLA2 VARCHAR2(2000);
    LV_SENTENCIA     VARCHAR2(32650);
    LV_ERROR         VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_TEMP_PR - ';
    LN_COUNT      NUMBER := 0;
  
  BEGIN
    FOR I IN 1 .. 4 LOOP
      LV_NOMBRE_TABLA := 'PR_PRUEBA_' || SUBSTR(PV_FECHA, 7) ||
                         SUBSTR(PV_FECHA, 4, 2) || '_TAT_' || I;
      OPEN C_OBTIENE_SENTENCIA('CREATE TABLE_PR');
      FETCH C_OBTIENE_SENTENCIA
        INTO LV_SENTENCIA;
      CLOSE C_OBTIENE_SENTENCIA;
      -- tablespace BILLING_DAT2
      BEGIN
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA',
                                LV_NOMBRE_TABLA);
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);
          IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE LE_EXCEPTION;
            NULL;
          END IF;
      END;
    END LOOP;
    LN_COUNT := 0;
    FOR I IN 1 .. 4 LOOP
      LV_NOMBRE_TABLA1 := 'PR_PRUEBA_' || SUBSTR(PV_FECHA, 7) ||
                          SUBSTR(PV_FECHA, 4, 2) || '_TAT_' || I;
      LV_NOMBRE_TABLA2 := 'UDR_LT_' || SUBSTR(PV_FECHA, 7) ||
                          SUBSTR(PV_FECHA, 4, 2) || '_TAT_' || I;
      OPEN C_OBTIENE_SENTENCIA('INSERT TABLE_PR');
      FETCH C_OBTIENE_SENTENCIA
        INTO LV_SENTENCIA;
      CLOSE C_OBTIENE_SENTENCIA;
      --     INSERT INTO gv_parametros ( id_tipo_parametro, id_parametro , nombre , descripcion, valor  , clase   ) 
      --     VALUES (7399, 'Evelyn', 'Evelyn', 'Evelyn', LV_SENTENCIA, 'CAR');
      --     COMMIT; 
      BEGIN
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA_PR',
                                LV_NOMBRE_TABLA1);
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_CAMPO',
                                '''' || LV_NOMBRE_TABLA2 || '''');
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA2',
                                LV_NOMBRE_TABLA2);
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);
          IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE LE_EXCEPTION;
            NULL;
          END IF;
      END;
      LN_COUNT := LN_COUNT + 1;
      IF LN_COUNT >= 100 THEN
        COMMIT;
        LN_COUNT := 0;
      END IF;
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GENERA_TEMP_PR;

  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : Proceso que crea vista TMP de las tablas temporales PR del mes actual y el anterior
  --===============================================================================================--

  PROCEDURE PR_CREA_VISTA_TMP(PV_FECHA        VARCHAR2,
                              PV_NOMBRE_VISTA OUT VARCHAR2,
                              PV_ERROR        OUT VARCHAR2) IS
    -- Declaracion de Variables
    LV_FECHA_ANT    VARCHAR2(2000);
    LV_FECHA_ACT    VARCHAR2(2000);
    LV_NOMBRE_TABLA VARCHAR2(2000);
    LV_UNION        VARCHAR2(2000);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_VALOR        VARCHAR2(32650);
    LV_TABLA2       VARCHAR2(2000);
    LV_TABLA3       VARCHAR2(2000);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_CREA_VISTA_TMP - ';
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ANT    := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'),
                                          -1),
                               'RRRRMM');
    LV_FECHA_ACT    := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_NOMBRE_TABLA := 'TMP_PR_PRUEBA_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    LV_UNION        := ' union all ';
    LV_SENTENCIA    := ' create or replace view ' || LV_NOMBRE_TABLA ||
                       ' as ';
    LV_TABLA2       := 'PR_PRUEBA_' || LV_FECHA_ANT || '_TAT_';
    LV_TABLA3       := 'PR_PRUEBA_' || LV_FECHA_ACT || '_TAT_';
    FOR I IN 1 .. 4 LOOP
      OPEN C_OBTIENE_SENTENCIA('CREATE VIEW_PR');
      FETCH C_OBTIENE_SENTENCIA
        INTO LV_VALOR;
      CLOSE C_OBTIENE_SENTENCIA;
      LV_SENTENCIA := LV_SENTENCIA || LV_VALOR || LV_TABLA2 || I;
      LV_SENTENCIA := LV_SENTENCIA || LV_UNION;
    
      LV_SENTENCIA := LV_SENTENCIA || LV_VALOR || LV_TABLA3 || I;
    
      IF (I <> 4) THEN
        LV_SENTENCIA := LV_SENTENCIA || LV_UNION;
      END IF;
    END LOOP;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          --RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    COMMIT;
    PV_NOMBRE_VISTA := LV_NOMBRE_TABLA;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_CREA_VISTA_TMP;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : Procedimiento que genera las tablas temporales PR_ANT_ACT, 
  --           Contiene informacion del campo REMARK= LLAMADAS DE PLAN DE DATOS
  --===============================================================================================--
  PROCEDURE PR_GEN_TEMP_PR_ANT_ACT(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS
    -- Declaracion  de variables 
    LV_NOMBRE_TABLA VARCHAR2(2000);
    LV_NOMBRE_VISTA VARCHAR2(2000);
    LV_FECHA_ANT    VARCHAR2(2000);
    LV_FECHA_ACT    VARCHAR2(2000);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GEN_TEMP_PR_ANT_ACT - ';
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ANT    := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'),
                                          -1),
                               'RRRRMM');
    LV_FECHA_ACT    := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_NOMBRE_VISTA := 'TMP_PR_PRUEBA_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    LV_NOMBRE_TABLA := 'PR_PRUEBA_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    OPEN C_OBTIENE_SENTENCIA('CREATE TABLE TEMP_PR');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
    --tablespace BILLING_DAT2
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA_PR2',
                              LV_NOMBRE_TABLA);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    OPEN C_OBTIENE_SENTENCIA('INSERT TABLE_TEMP');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
  
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA);
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_VISTA',
                              LV_NOMBRE_VISTA);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    COMMIT;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GEN_TEMP_PR_ANT_ACT;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA TABLAS TEMPORALES WORK & RP,
  --           contienen informacion 
  --===============================================================================================--
  -- Crea tablas temporales WORK_ & RP_
  PROCEDURE PR_GENERA_TEMP_WORK_RP(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS
    -- Declaracion de variables
    LV_NOMBRE_TABLA_WORK VARCHAR2(2000);
    LV_NOMBRE_TABLA_RP   VARCHAR2(2000);
    LV_FECHA_ANT         VARCHAR2(2000);
    LV_FECHA_ACT         VARCHAR2(2000);
    LV_ERROR             VARCHAR2(2000);
    LV_SENTENCIA         VARCHAR2(32650);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION  VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_TEMP_WORK_RP - ';
    LV_NOMBRE_WORK VARCHAR2(10);
    LV_NOMBRE_RP   VARCHAR2(10);
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ANT         := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA,
                                                       'DD/MM/RRRR'),
                                               -1),
                                    'RRRRMM');
    LV_FECHA_ACT         := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_NOMBRE_WORK       := 'WORK_PRUEBA_';
    LV_NOMBRE_RP         := 'RP_PRUEBA_';
    LV_NOMBRE_TABLA_WORK := LV_NOMBRE_WORK || '_' || LV_FECHA_ANT || '_' ||
                            LV_FECHA_ACT;
    LV_NOMBRE_TABLA_RP   := LV_NOMBRE_RP || '_' || LV_FECHA_ANT || '_' ||
                            LV_FECHA_ACT;
    -- tablespace BILLING_DAT2
    OPEN C_OBTIENE_SENTENCIA('CREATE TABLE_WR');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
  
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA_WORK);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    OPEN C_OBTIENE_SENTENCIA('CREATE TABLE_WR');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
    -- tablespace BILLING_DAT2
  
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA_RP);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GENERA_TEMP_WORK_RP;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO QUE GENERA INDICES PK PARA LAS TABLAS RP 
  --===============================================================================================--
  PROCEDURE PR_GENERA_INDICE_RP(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
  
    -- Declaracion de variables
    LV_NOMBRE_TABLA  VARCHAR2(2000);
    LV_NOMBRE_INDICE VARCHAR2(2000);
    LV_SENTENCIA     VARCHAR2(32650);
    LV_ERROR         VARCHAR2(2000);
    LV_FECHA_ANT     VARCHAR2(2000);
    LV_FECHA_ACT     VARCHAR2(2000);
    LN_EXISTE_TABLA  NUMBER;
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_INDICE_RP - ';
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ANT     := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'),
                                           -1),
                                'RRRRMM');
    LV_FECHA_ACT     := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_NOMBRE_INDICE := 'PK_RP_PRUEBA' || '_' || LV_FECHA_ANT || '_' ||
                        LV_FECHA_ACT;
  
    -- DBMS_OUTPUT.PUT_LINE(LV_NOMBRE_INDICE);
    LV_NOMBRE_TABLA := 'RP_PRUEBA' || '_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    --  DBMS_OUTPUT.PUT_LINE(LV_NOMBRE_TABLA);
  
    /*LV_SENTENCIA := 'SELECT COUNT(*) FROM ALL_TABLES@BSCS.CONECEL.COM WHERE TABLE_NAME = ''' || --Cambiar el dblink
    LV_NOMBRE_TABLA || '''';*/
    LV_SENTENCIA := 'SELECT COUNT(*) FROM ALL_TABLES WHERE TABLE_NAME = ''' || --Cambiar el dblink
                    LV_NOMBRE_TABLA || '''';
  
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA
        INTO LN_EXISTE_TABLA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
        END IF;
    END;
  
    -- si la tabla del cual consultamos no existe se hace un raise para no seguir
    IF LN_EXISTE_TABLA IS NULL OR LN_EXISTE_TABLA = 0 THEN
      LV_ERROR := 'NO EXISTE LA TABLA ' || LV_NOMBRE_TABLA ||
                  ', FAVOR REVISAR!';
      RAISE LE_EXCEPTION;
    END IF;
  
    --  tablespace BILLING_DAT2 
    OPEN C_OBTIENE_SENTENCIA('CREATE INDEX_RP');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
    -- DBMS_OUTPUT.PUT_LINE(LV_SENTENCIA);
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_INDICE',
                              LV_NOMBRE_INDICE);
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          --   RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
    
  END PR_GENERA_INDICE_RP;

  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA REPORTE 
  --===============================================================================================--
  PROCEDURE PR_GENERA_REPORTE(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
    -- Declaracion de variables
    LV_FECHA_ANT         VARCHAR2(2000);
    LV_FECHA_ACT         VARCHAR2(2000);
    LV_ERROR             VARCHAR2(2000);
    LV_SENTENCIA         VARCHAR2(32650);
    LV_NOMBRE_TABLA_RP   VARCHAR2(2000);
    LV_NOMBRE_TABLA_PR   VARCHAR2(2000);
    LV_NOMBRE_TABLA_WORK VARCHAR2(2000);
    LV_NOMBRE_VISTA      VARCHAR2(2000);
    LV_MES_ACT           VARCHAR2(10);
    LV_MES_ANT           VARCHAR2(10);
    LV_A�O               VARCHAR2(10);
    LV_A�O_ANT           VARCHAR2(10);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_REPORTE - ';
  
    --    CURSOR Obt_Ciclo(Cv_Dia_Ciclo VARCHAR2) IS
    --    SELECT Id_Ciclo, Dia_Ini_Ciclo
    --    FROM SYSADM.Fa_Ciclos_Bscs@BSCS.CONECEL.COM
    --    WHERE Dia_Ini_Ciclo = Cv_Dia_Ciclo;
  
    -- Lc_Obt_Ciclo Obt_Ciclo%ROWTYPE;
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ACT := TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'RRRRMM');
    LV_FECHA_ANT := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), -1),
                            'RRRRMM');
    LV_MES_ACT           := TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'MM');
    LV_MES_ANT           := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA,
                                                       'DD/MM/RRRR'),
                                               -1),
                                    'MM');
    LV_A�O               := TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'RRRR');
    LV_A�O_ANT           := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA,
                                                       'DD/MM/RRRR'),
                                               -1),
                                    'RRRR');
    LV_NOMBRE_TABLA_PR   := 'PR_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    LV_NOMBRE_TABLA_WORK := 'WORK_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    LV_NOMBRE_TABLA_RP   := 'RP_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    LV_SENTENCIA         := 'DELETE ' || LV_NOMBRE_TABLA_PR;
    LV_NOMBRE_VISTA      := 'TMP_PR_' || LV_FECHA_ANT || '_' ||
                            LV_FECHA_ACT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  
    LV_SENTENCIA := 'DELETE ' || LV_NOMBRE_TABLA_RP;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  
    LV_SENTENCIA := 'DELETE ' || LV_NOMBRE_TABLA_WORK;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    OPEN C_OBTIENE_SENTENCIA('INSERT TABLE_TEMP');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
  
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA_PR);
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_VISTA',
                              LV_NOMBRE_VISTA);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  
    COMMIT;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_PR ||
                    ' SET COMENTARIO1 = ''MEJOR AMIGO''
                   WHERE REMARK LIKE ''0.01 BFC%''';
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    --COMMIT;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_PR ||
                    ' SET COMENTARIO1 = ''NUMEROS FAVORITOS''
                 WHERE REMARK LIKE ''0.04 Mc%'''; --39sg
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    -- COMMIT;
  
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_PR ||
                    ' SET COMENTARIO1 = ''NUMEROS FAVORITOS''
                   WHERE REMARK IN (''6 DECIMALES MICROCELDA'', ''SIN PROCESAR'')'; --42sg
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    --  COMMIT;
    --identificar registros repetidos
  
    LV_SENTENCIA := ' DELETE ' || LV_NOMBRE_TABLA_PR ||
                    ' WHERE CUST_INFO_CUSTOMER_ID < 0';
    --  COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := 'INSERT /*+ append */
                    INTO ' || LV_NOMBRE_TABLA_WORK ||
                    ' SELECT A.*, B.*, C.*
                        FROM ' || LV_NOMBRE_TABLA_PR || ' A,
                             RP_CLIENTES        B, --tablas hay que actualizarla customer_all billcycles
                             
                             PLANES_BSCS C --tablas hay que actualizarla Pedir script mario Bosco
                       WHERE A.CUST_INFO_CUSTOMER_ID = B.CUSTOMER_ID
                         AND A.TARIFF_INFO_TMCODE = C.TMCODE';
  
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    COMMIT;
  
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_WORK ||
                    ' SET COMENTARIO2 = ''24/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || 'al 23/' || LV_MES_ACT || '/' || LV_A�O ||
                    ''' --20sg
                 WHERE ENTRY_DATE_TIMESTAMP >
                       TO_DATE(''24/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' 04:59:59'', ''dd/mm/yyyy hh24:mi:ss'')
                   AND ENTRY_DATE_TIMESTAMP <
                       TO_DATE(''24/' || LV_MES_ACT || '/' ||
                    LV_A�O ||
                    ' 05:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                   AND FUP_ACCOUNT_PERIOD_ID = 1';
    --COMMIT; 
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_WORK ||
                    ' SET COMENTARIO2 = ''8/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' al 7/' || LV_MES_ACT || '/' || LV_A�O ||
                    ''' --30sg
                   WHERE ENTRY_DATE_TIMESTAMP >
                         TO_DATE(''8/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' 04:59:59'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND ENTRY_DATE_TIMESTAMP <
                         TO_DATE(''8/' || LV_MES_ACT || '/' ||
                    LV_A�O ||
                    ' 05:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND FUP_ACCOUNT_PERIOD_ID = 2';
    --COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_WORK ||
                    ' SET COMENTARIO2 = ''15/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' al 14/' || LV_MES_ACT || '/' || LV_A�O ||
                    ''' --10sg
                   WHERE ENTRY_DATE_TIMESTAMP >
                         TO_DATE(''15/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' 04:59:59'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND ENTRY_DATE_TIMESTAMP <
                         TO_DATE(''15/' || LV_MES_ACT || '/' ||
                    LV_A�O ||
                    ' 05:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND FUP_ACCOUNT_PERIOD_ID = 4';
    --  COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_WORK ||
                    ' SET COMENTARIO2 = ''2/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' al 1/' || LV_MES_ACT || '/' || LV_A�O ||
                    ''' --10sg
                   WHERE ENTRY_DATE_TIMESTAMP >
                         TO_DATE(''2/' || LV_MES_ANT || '/' ||
                    LV_A�O_ANT || ' 04:59:59'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND ENTRY_DATE_TIMESTAMP <
                         TO_DATE(''2/' || LV_MES_ACT || '/' ||
                    LV_A�O ||
                    ' 05:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                     AND FUP_ACCOUNT_PERIOD_ID = 5';
    -- COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := ' INSERT INTO ' || LV_NOMBRE_TABLA_RP || --10sg
                    ' SELECT *
                      FROM ' || LV_NOMBRE_TABLA_WORK ||
                    ' WHERE TIPO = ''TAR''
                       AND BULK IS NULL
                       AND NOT COMENTARIO2 IS NULL';
    COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    --PR_CPM_BUSCA_GRATIS_MES; --procedimiento 
    PR_CPM_BUSCA_GRATIS_MES(PV_FECHA => PV_FECHA, PV_ERROR => LV_ERROR);
  
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_RP || ' G
                  SET REMARK = ''INCONSISTENCIA''
                 WHERE COMENTARIO1 = ''NUMEROS FAVORITOS''
                   AND REMARK <> ''INCONSISTENCIA''
                   AND UDS_CHARGE_PART_ID = 1
                   AND RATED_FLAT_AMOUNT = FREE_CHARGE_AMOUNT
                   AND ROUND((FREE_CHARGE_AMOUNT / DURAT_CONN_VOLUME) * 60, 2) < 0.04';
    -- COMMIT;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_SENTENCIA := ' UPDATE ' || LV_NOMBRE_TABLA_RP || ' G 
                  SET REMARK = ''INCONSISTENCIA''
                 WHERE COMENTARIO1 = ''NUMEROS FAVORITOS''
                   AND REMARK <> ''INCONSISTENCIA''
                   AND UDS_CHARGE_PART_ID = 1
                   AND RATED_FLAT_AMOUNT = FREE_CHARGE_AMOUNT
                   AND ROUND((FREE_CHARGE_AMOUNT / DURAT_CONN_VOLUME) * 60, 2) > 0.04';
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    -- COMMIT;
  
    COMMIT;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GENERA_REPORTE;
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : Proceso que busca llamadas gratis por mes
  --===============================================================================================--

  PROCEDURE PR_CPM_BUSCA_GRATIS_MES(PV_FECHA VARCHAR2,
                                    PV_ERROR OUT VARCHAR2) IS
    -- Declaracion de variables

    CUST_INFO_CUSTOMER_ID NUMBER;
    CUST_INFO_CONTRACT_ID NUMBER;
    UDS_STREAM_ID         NUMBER;
    UDS_RECORD_ID         NUMBER;
    COMENTARIO3           VARCHAR2(100);
  
    LV_FECHA_ANT       VARCHAR2(2000);
    LV_FECHA_ACT       VARCHAR2(2000);
    LV_ERROR           VARCHAR2(2000);
    LV_SENTENCIA       VARCHAR2(32650);
    LV_TABLA_UDR_ACT   VARCHAR2(2000);
    LV_TABLA_UDR_ANT   VARCHAR2(2000);
    LV_NOMBRE_TABLA_RP VARCHAR2(2000);
    -- CURSOR1            SYS_REFCURSOR;
  
    TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
  
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_CPM_BUSCA_GRATIS_MES - ';
  
    V_FREE_CHARGE_AMOUNT          FLOAT;
    V_FREE_RATED_VOLUME_VOLUME    FLOAT;
    V_FREE_UNITS_INFO_ACCOUNT_KEY NUMBER;
    V_FREE_UNITS_INFO_ACC_HIST_ID NUMBER;
    V_FREE_UNITS_INFO_CHG         VARCHAR2(1);
    V_FREE_UNITS_INFO_FU_PACK_ID  NUMBER;
  
    EXISTE  NUMBER;
    EXISTE2 NUMBER;
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    LV_FECHA_ANT       := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'),
                                             -1),
                                  'RRRRMM');
    LV_FECHA_ACT       := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_TABLA_UDR_ACT   := 'UDR_FU_' || LV_FECHA_ACT;
    LV_TABLA_UDR_ANT   := 'UDR_FU_' || LV_FECHA_ANT; 
    LV_NOMBRE_TABLA_RP := 'RP_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
  
    OPEN CURSOR1 FOR 'select cust_info_customer_id,
                     cust_info_contract_id,
                     uds_stream_id,
                     uds_record_id,
                     comentario3
                     from ' || LV_NOMBRE_TABLA_RP || ' WHERE comentario4 IS NULL ';
    /***/--EVELYN    
    
    LOOP
             cust_info_customer_id:=NULL;
             CUST_INFO_CONTRACT_ID:=NULL;
             UDS_STREAM_ID:=NULL;
             UDS_RECORD_ID:=NULL;
             COMENTARIO3:=NULL;
      FETCH CURSOR1 
        INTO CUST_INFO_CUSTOMER_ID, 
             CUST_INFO_CONTRACT_ID,  
             UDS_STREAM_ID,
             UDS_RECORD_ID,
             COMENTARIO3 /*LIMIT 1000*/;
        EXIT WHEN CURSOR1%NOTFOUND;
        --valida si existen datos:
        LV_SENTENCIA := ' select count(*)
                          from ' || LV_TABLA_UDR_ANT || '
                          where cust_info_customer_id = ' ||  
                       CUST_INFO_CUSTOMER_ID ||
                        ' and cust_info_contract_id =  ' ||  
                       CUST_INFO_CONTRACT_ID || ' and uds_stream_id =  ' ||  
                       UDS_STREAM_ID || ' and uds_record_id =  ' ||  
                       UDS_RECORD_ID || ' and uds_charge_part_id = 1';
        BEGIN
          EXECUTE IMMEDIATE LV_SENTENCIA INTO EXISTE;
        EXCEPTION
          WHEN OTHERS THEN
            LV_ERROR := SUBSTR(SQLERRM, 1, 200);
            IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
              NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
            ELSE
              --RAISE LE_EXCEPTION;
              NULL;
            END IF;
        END;
      
        IF EXISTE = 1 THEN
        
          LV_SENTENCIA := 'select free_charge_amount,
                           free_rated_volume_volume,
                           free_units_info_account_key,
                           free_units_info_chg_red_quota,
                           free_units_info_fu_pack_id,
                           FREE_UNITS_INFO_ACC_HIST_ID
                           from ' ||
                          LV_TABLA_UDR_ANT ||
                          ' where cust_info_customer_id = ' ||  
                         CUST_INFO_CUSTOMER_ID ||
                          ' and cust_info_contract_id = ' ||  
                         CUST_INFO_CONTRACT_ID || ' and uds_stream_id = ' ||  
                         UDS_STREAM_ID || ' and uds_record_id = ' ||  
                         UDS_RECORD_ID || ' and uds_charge_part_id = 1';
          BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA INTO V_FREE_CHARGE_AMOUNT,
                                                V_FREE_RATED_VOLUME_VOLUME,
                                                V_FREE_UNITS_INFO_ACCOUNT_KEY,
                                                V_FREE_UNITS_INFO_CHG,
                                                V_FREE_UNITS_INFO_FU_PACK_ID,
                                                V_FREE_UNITS_INFO_FU_PACK_ID;
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR(SQLERRM, 1, 200);
              IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
              ELSE
                RAISE LE_EXCEPTION;
                NULL;
              END IF;
          END;
          LV_SENTENCIA := 'update ' || LV_NOMBRE_TABLA_RP ||
                          'set comentario4                   = ''PROCESADO ''' ||
                          LV_TABLA_UDR_ANT || ',' ||
                          ' free_charge_amount            = ' ||
                          V_FREE_CHARGE_AMOUNT || ',' ||
                          ' free_rated_volume_volume      = ' ||
                          V_FREE_RATED_VOLUME_VOLUME || ',' ||
                          ' free_units_info_account_key   = ' ||
                          V_FREE_UNITS_INFO_ACCOUNT_KEY || ',' ||
                          ' free_units_info_chg_red_quota = ' ||
                          V_FREE_UNITS_INFO_CHG || ',' ||
                          ' free_units_info_fu_pack_id    = ' ||
                          V_FREE_UNITS_INFO_FU_PACK_ID || ',' ||
                          ' FREE_UNITS_INFO_ACC_HIST_ID   = ' ||
                          V_FREE_UNITS_INFO_ACC_HIST_ID ||
                          ' where cust_info_customer_id = ' ||  
                         CUST_INFO_CUSTOMER_ID ||
                          ' and cust_info_contract_id = ' ||  
                         CUST_INFO_CONTRACT_ID || ' and uds_stream_id = ' ||  
                         UDS_STREAM_ID || ' and uds_record_id = ' ||  
                         UDS_RECORD_ID;
          BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA;
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR(SQLERRM, 1, 200);
              IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
              ELSE
                RAISE LE_EXCEPTION;
                NULL;
              END IF;
          END;
          --   COMMIT;
        ELSE
          LV_SENTENCIA := ' select count(*)
                            from ' || LV_TABLA_UDR_ACT ||
                          ' where cust_info_customer_id = ' ||  
                         CUST_INFO_CUSTOMER_ID ||
                          ' and cust_info_contract_id = ' ||  
                         CUST_INFO_CONTRACT_ID || ' and uds_stream_id = ' ||  
                         UDS_STREAM_ID || ' and uds_record_id = ' ||  
                         UDS_RECORD_ID || ' and uds_charge_part_id = 1';
          BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA INTO EXISTE2 ;
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR(SQLERRM, 1, 200);
              IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
              ELSE
                RAISE LE_EXCEPTION;
                NULL;
              END IF;
          END;
          IF EXISTE2 = 1 THEN
            LV_SENTENCIA := ' select free_charge_amount,
                             free_rated_volume_volume,
                             free_units_info_account_key,
                             free_units_info_chg_red_quota,
                             free_units_info_fu_pack_id,
                             FREE_UNITS_INFO_ACC_HIST_ID
                            from ' ||
                            LV_TABLA_UDR_ACT ||
                            ' where cust_info_customer_id = ' ||  
                           CUST_INFO_CUSTOMER_ID ||
                            ' and cust_info_contract_id = ' ||  
                           CUST_INFO_CONTRACT_ID ||
                            ' and uds_stream_id = ' ||  
                           UDS_STREAM_ID || ' and uds_record_id = ' ||  
                           UDS_RECORD_ID || ' and uds_charge_part_id = 1';
            BEGIN
              EXECUTE IMMEDIATE LV_SENTENCIA INTO V_FREE_CHARGE_AMOUNT,
                                                  V_FREE_RATED_VOLUME_VOLUME,
                                                  V_FREE_UNITS_INFO_ACCOUNT_KEY,
                                                  V_FREE_UNITS_INFO_CHG,
                                                  V_FREE_UNITS_INFO_FU_PACK_ID,
                                                  V_FREE_UNITS_INFO_ACC_HIST_ID;
            EXCEPTION
              WHEN OTHERS THEN
                LV_ERROR := SUBSTR(SQLERRM, 1, 200);
                IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                  NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
                ELSE
                  RAISE LE_EXCEPTION;
                  NULL;
                END IF;
            END;
            LV_SENTENCIA := 'update ' || LV_NOMBRE_TABLA_RP ||
                            'set comentario4              = ''PROCESADO ''' ||
                            LV_TABLA_UDR_ACT || ',' ||
                            'free_charge_amount            = ' ||
                            V_FREE_CHARGE_AMOUNT || ',' ||
                            'free_rated_volume_volume      = ' ||
                            V_FREE_RATED_VOLUME_VOLUME || ',' ||
                            'free_units_info_account_key   = ' ||
                            V_FREE_UNITS_INFO_ACCOUNT_KEY || ',' ||
                            'free_units_info_chg_red_quota = ' ||
                            V_FREE_UNITS_INFO_CHG || ',' ||
                            'free_units_info_fu_pack_id    = ' ||
                            V_FREE_UNITS_INFO_FU_PACK_ID || ',' ||
                            'FREE_UNITS_INFO_ACC_HIST_ID   = ' ||
                            V_FREE_UNITS_INFO_ACC_HIST_ID ||
                            'where cust_info_customer_id = ' ||  
                           CUST_INFO_CUSTOMER_ID ||
                            'and cust_info_contract_id = ' ||  
                           CUST_INFO_CONTRACT_ID || 'and uds_stream_id = ' ||  
                           UDS_STREAM_ID || 'and uds_record_id = ' ||  
                           UDS_RECORD_ID || 'and uds_charge_part_id = 1';
            BEGIN
              EXECUTE IMMEDIATE LV_SENTENCIA;
            EXCEPTION
              WHEN OTHERS THEN
                LV_ERROR := SUBSTR(SQLERRM, 1, 200);
                IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                  NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
                ELSE
                  RAISE LE_EXCEPTION;
                  NULL;
                END IF;
            END;
          
            --    COMMIT;
          
          ELSE
            LV_SENTENCIA := ' update ' || LV_NOMBRE_TABLA_RP ||
                            ' set comentario4 = ''SIN UNIDADES GRATUITAS''
                             where cust_info_customer_id = ' ||  
                           CUST_INFO_CUSTOMER_ID ||
                            ' and cust_info_contract_id = ' ||  
                           CUST_INFO_CONTRACT_ID ||
                            ' and uds_stream_id = ' ||  
                           UDS_STREAM_ID || ' and uds_record_id = ' ||  
                           UDS_RECORD_ID;
            BEGIN
              EXECUTE IMMEDIATE LV_SENTENCIA;
            EXCEPTION
              WHEN OTHERS THEN
                LV_ERROR := SUBSTR(SQLERRM, 1, 200);
                IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
                  NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
                ELSE
                  RAISE LE_EXCEPTION;
                  NULL;
                END IF;
            END;
            -- commit;
          END IF;
        
        END IF;
      
    END LOOP;
    --FIN EVELYN
    COMMIT;
    CLOSE CURSOR1;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_CPM_BUSCA_GRATIS_MES;
 --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO PARA EXTAER INFROMACI�N DE LAS TABLAS PIVOTE Y LAS TABLAS FINALES GSI
  --===============================================================================================--
  -- Creaci�n de la tabla temporal, las que nos permite respaldar la informaci�n final de la tabla
  PROCEDURE PR_GENERA_RESPALDO_GSI(PV_FECHA VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS
  
    -- Declaracion de Variables
    LV_NOMBRE_TABLA   VARCHAR2(2000);
    LV_NOMBRE_INDICE  VARCHAR2(2000);
    LV_NOMBRE_INDICE2 VARCHAR2(2000);
    LV_NOMBRE_TABLA3  VARCHAR2(2000);
    LV_SENTENCIA      VARCHAR2(32650);
    LV_ERROR          VARCHAR2(2000);
    LV_MES            VARCHAR2(10);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_RESPALDO_GSI - ';
    --  LN_COUNT      NUMBER := 0;
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    ---
    LV_NOMBRE_TABLA  := 'GSI_RPT_PIVOTE_' || LV_MES;
    LV_NOMBRE_INDICE := 'GSI_TEMP' || SUBSTR(PV_FECHA, 7) ||
                          SUBSTR(PV_FECHA, 4, 2) ;
    LV_NOMBRE_INDICE2 := 'GSI_FINAL' || SUBSTR(PV_FECHA, 7) ||
                          SUBSTR(PV_FECHA, 4, 2) ;
 /*   LV_NOMBRE_TABLA2 := 'REGUN.PIVOTE_INICIAL_' || LV_MES||'@COLECTOR';*/
    LV_NOMBRE_TABLA3 := 'GSI_RPT_PIVOTE_DET_' || LV_MES;
    --    
    LV_SENTENCIA:='create table  :NOMBRE_TABLA
                  (
                    CUENTA          VARCHAR2(20),
                    TELEFONO        VARCHAR2(20),
                    CICLO           VARCHAR2(2),
                    OBSERVACION     VARCHAR2(200),
                    HILO            NUMBER,
                    FECHA_INI_CORTE DATE,
                    FECHA_FIN_CORTE DATE,
                    CUSTOMER_ID     NUMBER,
                    CONTROL         VARCHAR2(200)

                  )
                   tablespace billing_dat
                    pctfree 40
                    pctused 40
                    initrans 1
                    maxtrans 255
                    storage
                    (
                      initial 30M
                      next 5M
                      minextents 1
                      maxextents unlimited
                      pctincrease 0
                    )';
    
    BEGIN
      --
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA);
      --                              
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);     
          RAISE LE_EXCEPTION;        
    END;
    -- Crear indices a las tablas temporales
          --  tablespace BILLING_DAT2 
      LV_SENTENCIA:='create index 
                    :NOMBRE_INDICE  on 
                    :NOMBRE_TABLA (mc_scalefactor) 
                    tablespace BILLING_DAT2
                    Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
                    unlimited pctincrease 0)';
      -- DBMS_OUTPUT.PUT_LINE(LV_SENTENCIA);
      BEGIN
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_INDICE',
                                LV_NOMBRE_INDICE);
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA',
                                LV_NOMBRE_TABLA);
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);     
            RAISE LE_EXCEPTION;        
      END;
    ---
    /*OPEN C_OBTIENE_SENTENCIA('CREATE TABLE_GSI_FINAL');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;*/
    
    LV_SENTENCIA:='create table  :NOMBRE_TABLA
                  (
                    ID_SERVICIO      VARCHAR2(20) not null,
                    CO_ID            NUMBER(38),
                    ID_CONTRATO      NUMBER(10) not null,
                    ID_DETALLE_PLAN  NUMBER(10),
                    ID_PLAN          VARCHAR2(10) not null,
                    DESCRIPCION_PLAN VARCHAR2(60) not null,
                    FECHA_INICIO     DATE not null,
                    FECHA_FIN        DATE,
                    TIPO             VARCHAR2(20) not null,
                    ID_SUBPRODUCTO   VARCHAR2(20) not null,
                    ESTADO           VARCHAR2(1) not null,
                    ID_CATEGORIA_AMX VARCHAR2(3),
                    TMCODE           NUMBER(10),
                    CICLO            VARCHAR2(2),
                    PERFIL           NUMBER
                  )
                  tablespace billing_dat
                    pctfree 40
                    pctused 40
                    initrans 1
                    maxtrans 255
                    storage
                    (
                      initial 30M
                      next 5M
                      minextents 1
                      maxextents unlimited
                      pctincrease 0
                    )';
    
    -- tablespace BILLING_DAT2
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA3);
                                          
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);        
        RAISE LE_EXCEPTION;        
    END;
    LV_SENTENCIA:='create index 
              :NOMBRE_INDICE  on 
              :NOMBRE_TABLA (mc_scalefactor) 
              tablespace BILLING_DAT2
              Parallel 6 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents
              unlimited pctincrease 0)';
      -- DBMS_OUTPUT.PUT_LINE(LV_SENTENCIA);
      BEGIN
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_INDICE',
                                LV_NOMBRE_INDICE2);
        LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                                ':NOMBRE_TABLA',
                                LV_NOMBRE_TABLA3);
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);     
            RAISE LE_EXCEPTION;        
      END;
   LV_SENTENCIA := 'insert into ' || LV_NOMBRE_TABLA ||
                    ' (CUENTA, Telefono, Ciclo) select distinct cuenta, telefono, ciclo from gsi_pivote_view@rtx_to_bscs_link'; 
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
          RAISE LE_EXCEPTION;                
    END;
    
    COMMIT;
   
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);    
  END PR_GENERA_RESPALDO_GSI;
  
  -- Update de los hilos en las GSI_RPT_PIVOTE_MES
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO PARA GENERAR UPDATE DEL CAMPO HILO 
  --===============================================================================================--
  PROCEDURE PR_GENERA_UPDATE_HILO(PV_FECHA VARCHAR2,
                                  PV_HILO NUMBER,
                                  PV_ERROR OUT VARCHAR2) IS
  
  -- Declaracion de variables
    LV_NOMBRE_TABLA VARCHAR2(2000);  
    LV_SENTENCIA    VARCHAR2(32650);
    LV_SENTENCIA1   VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LV_MES          VARCHAR2(10);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_UPDATE_HILO - ';
    
    LN_HILO NUMBER:=0;
    LR_ROWID ROWID;
    
     TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
    
 BEGIN
      --
      IF PV_HILO IS NULL THEN
         --PV_ERROR := 'Ingrese Hilo';
         --RETURN;
         NULL;
      END IF;
      --
      OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
       FETCH C_OBTENER_MES INTO LV_MES;
      CLOSE C_OBTENER_MES;
      --      
      LV_NOMBRE_TABLA := 'GSI_RPT_PIVOTE_' || LV_MES;
      
      LV_SENTENCIA1:='SELECT ROWID FROM '||LV_NOMBRE_TABLA||' WHERE OBSERVACION = ''OK3'' AND control is null';
      
      OPEN CURSOR1 FOR LV_SENTENCIA1;     
      LOOP
        --
        FETCH CURSOR1 INTO LR_ROWID;
        EXIT WHEN CURSOR1%NOTFOUND;   
        
         LV_SENTENCIA := '  UPDATE ' || LV_NOMBRE_TABLA || '
                             SET hilo = '|| LN_HILO ||
                            ' WHERE ROWID = '''|| LR_ROWID ||'''';
          BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA;
            COMMIT;
          EXCEPTION
             WHEN OTHERS THEN   
                PV_ERROR:= SQLERRM;      
                RAISE LE_EXCEPTION;                          
          END;         
        --             
        LN_HILO:=LN_HILO+1;
        --
        IF LN_HILO = 20 THEN
           LN_HILO:=0;
        END IF;
        --
      END LOOP;
      
   /*   FOR I IN 0..9 LOOP
      --
        LV_SENTENCIA := '  UPDATE ' || LV_NOMBRE_TABLA || '
                             SET hilo = '|| I ||
                            ' WHERE SUBSTR(TELEFONO,-1) = '|| I ;
          BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA;
            COMMIT;
          EXCEPTION
             WHEN OTHERS THEN         
                RAISE LE_EXCEPTION;                          
          END;
      --      
      END LOOP;*/
      --
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GENERA_UPDATE_HILO;
  -- Busqueda de los clientes 
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA UNA BUSQUEDA DE LOS CLIENTES 
  --===============================================================================================--
  PROCEDURE PR_GENERA_BUSQUEDA_CLIENTE(PV_FECHA VARCHAR2,
                                       PV_HILO NUMBER,
                                       PV_ERROR OUT VARCHAR2) IS
  
    -- tipo record tabla GSI_RPT_PIVOTE_MES                             
   
      CUENTA          VARCHAR2(20);
      TELEFONO        VARCHAR2(20);
      CICLO           VARCHAR2(2);
      OBSERVACION     VARCHAR2(200);
      HILO            NUMBER;
      FECHA_INI_CORTE DATE;
      FECHA_FIN_CORTE DATE;
      CUSTOMER_ID     NUMBER;
      LV_ROWID        ROWID;    
    -- Crear un cursor para la tabla temporal GSI_RPT_PIVOTE_MES
  
    -- Declaracion de variables
    LV_NOMBRE_TABLA VARCHAR2(2000);
    ln_cont         number := 0;
    ln_commit       number := 100;
    lv_observacion  varchar2(1500);
    li_customer_id  varchar2(1500);
    LV_MES          VARCHAR2(10);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LE_EXCEPTION2 EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_BUSQUEDA - ';
  
    -- declaracion tipo cursor 
    TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
  
  BEGIN
    --
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    --
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    --
    LV_NOMBRE_TABLA := 'GSI_RPT_PIVOTE_' || LV_MES;
    --
    --FOR I IN 0..0 LOOP --PARA ACTUALIZAR EL CUSTOMER_ID POR HILO
    --
        -- abrimos el cursor
        OPEN CURSOR1 FOR ' select f.CUENTA,
                                    f.TELEFONO,
                                    f.CICLO,
                                    f.OBSERVACION,
                                    f.HILO,
                                    f.FECHA_INI_CORTE,
                                    f.FECHA_FIN_CORTE,
                                    f.CUSTOMER_ID,
                                    ROWID                            
                         from ' || LV_NOMBRE_TABLA || ' f ' || ' WHERE f.hilo = '||PV_HILO||'  AND f.customer_id is null ';

        LOOP
        BEGIN 
                  CUENTA:= NULL;          
                  TELEFONO:= NULL;        
                  CICLO:=NULL;           
                  OBSERVACION:=NULL;     
                  HILO:= NULL;
                  FECHA_INI_CORTE:=NULL; 
                  FECHA_FIN_CORTE:= NULL;
                  CUSTOMER_ID:= NULL;
                  LV_ROWID:= NULL;
              --
              FETCH CURSOR1 INTO  CUENTA,          
                                  TELEFONO,        
                                  CICLO,           
                                  OBSERVACION,     
                                  HILO,          
                                  FECHA_INI_CORTE, 
                                  FECHA_FIN_CORTE,
                                  CUSTOMER_ID,
                                  LV_ROWID; 
             --
             EXIT WHEN CURSOR1%NOTFOUND;  
             --   
                lv_observacion := '';
                li_customer_id := '';
                LV_SENTENCIA   := ' select customer_id  from customer_all
                                   where custcode= ' ||''''||CUENTA||'''';
                lv_observacion := 'OK3';
      
            BEGIN
              EXECUTE IMMEDIATE LV_SENTENCIA INTO li_customer_id;
            EXCEPTION
              WHEN no_data_found then
                lv_observacion := 'NO_OK3_FOUND';
                RAISE LE_EXCEPTION2;
              WHEN OTHERS THEN
                lv_observacion := 'NO_OK3_OTHERS';
                LV_ERROR       := SUBSTR(SQLERRM, 1, 200);      
                RAISE LE_EXCEPTION2;  
            END;
            /**/
            LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || '
                                set customer_id= ' ||
                            li_customer_id || ' , ' || ' observacion = ' ||''''|| 
                            lv_observacion ||''''|| ' where rowid = ' 
                            ||''''||LV_ROWID||'''';
            BEGIN
              EXECUTE IMMEDIATE LV_SENTENCIA;
            EXCEPTION                          
              WHEN no_data_found then
                lv_observacion := 'NO_OK3_FOUND';
                RAISE LE_EXCEPTION2 ;
              WHEN OTHERS THEN
                lv_observacion := 'NO_OK3_OTHERS';
                LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
                RAISE LE_EXCEPTION2 ;
            END;
            --
            ln_cont := ln_cont + 1;
            IF (ln_cont Mod ln_commit) = 0 then
              Commit;
            end if;
        --    
        EXCEPTION 
        when LE_EXCEPTION2  THEN
           LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
           LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set  observacion = ' ||''''|| 
                             SUBSTR(lv_observacion ||' => '||LV_ERROR||'''',1,200)||' where rowid = '''||LV_ROWID||'''';
           EXECUTE IMMEDIATE LV_SENTENCIA;
        when others then 
            LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
            LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set  observacion = ' ||''''|| 
            SUBSTR( lv_observacion ||' => '||LV_ERROR||'''',1,200)|| ' where rowid = '''||LV_ROWID||'''';
            EXECUTE IMMEDIATE LV_SENTENCIA;     
        END;   
        END LOOP;
        COMMIT;
        CLOSE CURSOR1;
                       
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR || CUENTA;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200) ||CUENTA;
    
  END PR_GENERA_BUSQUEDA_CLIENTE;
  -- Actualizacion de las TMP GSI segun la fecha 
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA LA INFORMACION PARA LLENAR A LA TABLAS FINAL GSI_RPT_PIVOTE_DET
  --===============================================================================================--
  PROCEDURE PR_GENERA_UPDATE_GSI(PV_FECHA VARCHAR2, 
                                 PV_HILO  NUMBER,
                                 PV_ERROR OUT VARCHAR2) IS
    -- Declarar un cursor para los ciclos
    CURSOR Obt_Ciclo  IS
    SELECT Id_Ciclo, Dia_Ini_Ciclo, Dia_fin_ciclo
    FROM SYSADM.Fa_Ciclos_Bscs@rtx_to_bscs_link
    WHERE ID_CICLO <> 05;       
    -- Declaracion de Variables
    LV_NOMBRE_TABLA VARCHAR2(2000);
    LV_SENTENCIA VARCHAR2(32650);
    LV_ERROR     VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_TEMP_PR - ';
    LN_COUNT      NUMBER := 0;
    LV_MES       VARCHAR2(10); 
    LV_MES_ACT           VARCHAR2(10);
    LV_MES_ANT           VARCHAR2(10);
    LV_A�O               VARCHAR2(10);
    LV_A�O_ANT           VARCHAR2(10);
  
  BEGIN
    --
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    
    LV_MES_ACT           := TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'MM');
    LV_MES_ANT           := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA,
                                                       'DD/MM/RRRR'),
                                               -1),
                                    'MM');
    LV_A�O               := TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'RRRR');
    LV_A�O_ANT           := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA,
                                                       'DD/MM/RRRR'),
                                               -1),
                                    'RRRR');
     OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
      FETCH C_OBTENER_MES
        INTO LV_MES;
      CLOSE C_OBTENER_MES; 
          
  -- Realizacion de updates a las tablas ( fecha_inicio_corte / fecha_fin_corte)
    FOR I IN Obt_Ciclo LOOP
    --
    LV_NOMBRE_TABLA := 'GSI_RPT_PIVOTE_' || LV_MES;
    --    
      LV_SENTENCIA := 'update '|| LV_NOMBRE_TABLA  ||
                      ' set fecha_ini_corte=to_date('''||I.Dia_Ini_Ciclo ||' / '|| LV_MES_ANT ||' / '|| LV_A�O_ANT ||''',''
                      dd / mm / yyyy ''), fecha_fin_corte=to_date(''
                      '||I.Dia_Fin_Ciclo ||' / '|| LV_MES_ACT ||' / '|| LV_A�O ||' 23 :59 :59 '','' dd / mm / yyyy hh24 :mi :ss '')
                      where HILO = '||PV_HILO||'AND ciclo= '||''''|| I.Id_Ciclo ||''''||'' ;
      BEGIN
        EXECUTE IMMEDIATE LV_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := SUBSTR(SQLERRM, 1, 200);         
            RAISE LE_EXCEPTION;                  
      END;
     --
      LN_COUNT := LN_COUNT + 1;
      IF LN_COUNT >= 100 THEN
        COMMIT;
        LN_COUNT := 0;
      END IF;      
    END LOOP;
    COMMIT;
    --
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
    
  END PR_GENERA_UPDATE_GSI;
  
    -- Obtener el tncde de la tablas finales
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA LA BUSQUEDA DEL CAMPO TMCODE
  --===============================================================================================--
  PROCEDURE PR_GENERA_BUSQUEDA_TMCODE(PV_FECHA VARCHAR2,
                                      PV_ERROR OUT VARCHAR2) IS
  
    -- tipo record tabla GSI_RPT_PIVOTE_MES   
                             
      ID_SERVICIO      VARCHAR2(20);
      CO_ID            NUMBER;
      ID_CONTRATO      NUMBER;
      ID_DETALLE_PLAN  NUMBER;
      ID_PLAN          VARCHAR2(10);
      DESCRIPCION_PLAN VARCHAR2(60);
      FECHA_INICIO     DATE;
      FECHA_FIN        DATE;
      TIPO             VARCHAR2(20);
      ID_SUBPRODUCTO   VARCHAR2(20);
      ESTADO           VARCHAR2(1);
      ID_CATEGORIA_AMX VARCHAR2(3);
      TMCODE           NUMBER;
      CICLO            VARCHAR2(2);
      PERFIL           NUMBER;
      LV_ROWID         ROWID;
  
    -- Declaracion de variables
    LV_NOMBRE_TABLA VARCHAR2(2000);
    ln_cont         number := 0;
    ln_commit       number := 100;
    lv_observacion  varchar2(1500);
    ln_tmcode       number(10);
    LV_MES          VARCHAR2(10);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LE_EXCEPTION2 EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_BUSQUEDA - ';

    -- declaracion tipo cursor 
    TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
  
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    LV_NOMBRE_TABLA := 'GSI_RPT_PIVOTE_DET_' || LV_MES;
  
    -- abrimos el cursor
    OPEN CURSOR1 FOR 'select f.ID_SERVICIO,
                                f.CO_ID,
                                f.ID_CONTRATO,
                                f.ID_DETALLE_PLAN,
                                f.ID_PLAN,
                                f.DESCRIPCION_PLAN,
                                f.FECHA_INICIO,
                                f.FECHA_FIN,
                                f.TIPO,
                                f.ID_SUBPRODUCTO,
                                f.ESTADO,
                                f.ID_CATEGORIA_AMX,
                                f.TMCODE,
                                f.CICLO,
                                f.PERFIL,
                                f.ROWID                             
                     from ' || LV_NOMBRE_TABLA || ' f  WHERE  f.tmcode is null ';

    LOOP
    BEGIN
      --
             ID_SERVICIO:= NULL;
             CO_ID:= NULL;
             ID_CONTRATO:= NULL;
             ID_DETALLE_PLAN:= NULL;
             ID_PLAN:= NULL;
             DESCRIPCION_PLAN:= NULL;
             FECHA_INICIO:= NULL;
             FECHA_FIN:= NULL;
             TIPO:= NULL;
             ID_SUBPRODUCTO:= NULL;
             ESTADO:= NULL;
             ID_CATEGORIA_AMX:= NULL;
             TMCODE:= NULL;
             CICLO:= NULL;
             PERFIL:= NULL;
             LV_ROWID:= NULL;
      FETCH CURSOR1 
      -- INTO DATOS1 LIMIT 1000;
        INTO ID_SERVICIO,
             CO_ID,
             ID_CONTRATO,
             ID_DETALLE_PLAN,
             ID_PLAN,
             DESCRIPCION_PLAN,
             FECHA_INICIO,
             FECHA_FIN,
             TIPO,
             ID_SUBPRODUCTO,
             ESTADO,
             ID_CATEGORIA_AMX,
             TMCODE,
             CICLO,
             PERFIL,
             LV_ROWID;
       EXIT WHEN CURSOR1%NOTFOUND;
        lv_observacion := '';
        ln_tmcode      := '';
        LV_SENTENCIA   := 'select cod_bscs  from bs_planes@AXIS b
                               where b.id_detalle_plan= ' || ID_DETALLE_PLAN;
      
        lv_observacion := 'OK4';

        BEGIN
          EXECUTE IMMEDIATE LV_SENTENCIA INTO ln_tmcode ;
        EXCEPTION
          WHEN no_data_found then
            lv_observacion := 'NO_OK4_FOUND';
            RAISE LE_EXCEPTION2;
          WHEN OTHERS THEN
            lv_observacion := 'NO_OK4_OTHERS';
            LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
            RAISE LE_EXCEPTION2;
       
        END;
        LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || '
                            set TMCODE= ' ||
                        ln_tmcode || ' , ' || ' observacion = ' ||''''||
                        lv_observacion ||''''|| ' where rowid = ' ||''''|| LV_ROWID||''''; 
        BEGIN
          EXECUTE IMMEDIATE LV_SENTENCIA;
        EXCEPTION
          WHEN no_data_found then
            lv_observacion := 'NO_OK4_FOUND';
            RAISE LE_EXCEPTION2;
          WHEN OTHERS THEN
            lv_observacion := 'NO_OK4_OTHERS';
            LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
            RAISE LE_EXCEPTION2;
        END;
        ln_cont := ln_cont + 1;
        IF (ln_cont Mod ln_commit) = 0 then
          Commit;
        end if;

   EXCEPTION 
    when LE_EXCEPTION2  THEN
       LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
       /*LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set  observacion = ' ||''''|| 
                         SUBSTR(lv_observacion ||' => '||LV_ERROR||'''',1,200)||' where rowid = '''||LV_ROWID||'''';
       EXECUTE IMMEDIATE LV_SENTENCIA;*/
    when others then 
        LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
        /*LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set  observacion = ' ||''''|| 
        SUBSTR( lv_observacion ||' => '||LV_ERROR||'''',1,200)|| ' where rowid = '''||LV_ROWID||'''';
        EXECUTE IMMEDIATE LV_SENTENCIA;  */   
    END;
    END LOOP;
    COMMIT;
    CLOSE CURSOR1;

  
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
    
  END PR_GENERA_BUSQUEDA_TMCODE;
  -- Eliminar duplicados
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO QUE ELIMINA INFORMACION DUPLICADA
  --===============================================================================================--
PROCEDURE PR_GENERA_DELETE_GSI(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2)IS
  -- Realizar un cursor
   -- tipo record tabla GSI_RPT_PIVOTE_MES                             
      ID_SERVICIO      VARCHAR2(20);
      ID_PLAN          VARCHAR2(10);
      FECHA_INICIO     DATE;
      ESTADO           VARCHAR2(1);
      LV_ROWID         ROWID;
      CONTEO           NUMBER;

  
    -- Declaracion de variables
    LV_NOMBRE_TABLA VARCHAR2(2000);
    LV_MES          VARCHAR2(10);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_SENTENCIA1    VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_DELETE_GSI - ';
    LV_ROWID      ROWID;
    --borrados number:=0;
    lv_rowid Varchar2(100);
    -- declaracion tipo cursor 
    TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
  BEGIN
  IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    LV_NOMBRE_TABLA := 'GSI_RPT_PIVOTE_DET_' || LV_MES;
        -- abrimos el cursor
    LV_SENTENCIA1:='select f.ID_SERVICIO,
                             f.ID_PLAN,
                             f.FECHA_INICIO,
                             f.ESTADO,
                             count(*) conteo                         
                     from ' || LV_NOMBRE_TABLA || ' f ' || '    GROUP BY f.id_servicio , 
                     f.id_plan , f.fecha_inicio , f.estado
                     HAVING COUNT(*)>1';
                     
       OPEN CURSOR1 FOR LV_SENTENCIA1;
       
       LOOP
       --
       ID_SERVICIO:= NULL;
       ID_PLAN:= NULL;
       FECHA_INICIO:= NULL;
       ESTADO:= NULL;
       CONTEO:= NULL;  
      --
      FETCH CURSOR1 
      -- INTO DATOS1 LIMIT 1000;
        INTO ID_SERVICIO,
             ID_PLAN,
             FECHA_INICIO,
             ESTADO,
             CONTEO;              
        --
        EXIT WHEN CURSOR1%NOTFOUND;
        --valida si existen datos:
        LV_SENTENCIA := ' Delete From ' || LV_NOMBRE_TABLA || '
                          Where id_servicio = '''||id_servicio||
                         '''  and id_plan = '''||id_plan||
                         '''  and fecha_inicio= to_date('''||fecha_inicio||''',''dd/mm/yyyy'')'||
                         '  and estado= ''' ||   estado ||
                         '''  And rownum < ' ||   conteo ;
        BEGIN
          EXECUTE IMMEDIATE LV_SENTENCIA;
        EXCEPTION
          WHEN OTHERS THEN
            LV_ERROR := SUBSTR(SQLERRM, 1, 200);
            IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
              NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
            ELSE
              --RAISE LE_EXCEPTION;
              NULL;
            END IF;
        END;      
    END LOOP;
    COMMIT;
    CLOSE CURSOR1;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
      rollback;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
      rollback;
END PR_GENERA_DELETE_GSI;
  -- Extrae informacion para la tabla final NO ESTOY SEGURA
  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA LA INFORMACION PARA LLENAR A LA TABLAS FINAL GSI_RPT_PIVOTE_DET
  --===============================================================================================--
  PROCEDURE PR_GENERA_INSERT_GSI(PV_FECHA VARCHAR2, 
                                 PV_HILO NUMBER, 
                                 PV_ERROR OUT VARCHAR2) IS
    /* -- Realizar cursor para la tabla temporal -- */    
    -- tipo record tabla GSI_RPT_PIVOTE_MES                            
      CUENTA          VARCHAR2(20);
      TELEFONO        VARCHAR2(20);
      CICLO           VARCHAR2(2);
      OBSERVACION     VARCHAR2(200);
      HILO            NUMBER;
      FECHA_INI_CORTE DATE;
      FECHA_FIN_CORTE DATE;
      CUSTOMER_ID     NUMBER;
      LV_ROWID        ROWID;
    -- declaracion tipo cursor 
    TYPE RC_TXDTLLAM IS REF CURSOR;
    CURSOR1 RC_TXDTLLAM;
    /* -- Realizar el cursor para la tabla final -- */
  cursor c_registros(lv_id_servicio varchar2, ld_fecha_ini_corte date, ld_fecha_fin_corte date) is
        select s.id_servicio, 
               s.co_id, 
               s.id_contrato, 
               s.id_detalle_plan, 
               g.id_plan, 
               p.descripcion DESCRIPCION_PLAN, 
               s.fecha_inicio, 
               s.fecha_fin, 
               c.descripcion TIPO, 
               s.id_subproducto,
               s.estado, 
               g.id_categoria_amx--este no va
          from CL_SERVICIOS_CONTRATADOS@AXIS S, 
               GE_DETALLES_PLANES@AXIS G, 
               GE_PLANES@AXIS P, 
               GE_CATEGORIAS_AMX@AXIS C
          where id_servicio=lv_id_servicio
          and s.id_subproducto <> 'PPA'
          and s.id_detalle_plan=g.id_detalle_plan
          and g.id_plan=p.id_plan
          and g.id_categoria_amx=c.id_categoria_amx
          AND 
          (
            fecha_fin between ld_fecha_ini_corte and ld_fecha_fin_corte
            or 
            (
               fecha_inicio < ld_fecha_ini_corte and nvl(fecha_fin,sysdate) > ld_fecha_fin_corte
            )
            or
            fecha_inicio between ld_fecha_ini_corte and ld_fecha_fin_corte
          )
          ORDER BY FECHA_INICIO;
    -- Declaracion de variables
    LV_NOMBRE_TABLA VARCHAR2(2000);
    LV_NOMBRE_TABLA2 VARCHAR2(2000);
    ln_cont         number := 0;
    ln_commit       number := 100;
    lv_observacion  varchar2(1500);
    lv_control      varchar2(1500);
    --li_customer_id  integer;
    LV_MES          VARCHAR2(10);
    LV_SENTENCIA    VARCHAR2(32650);
    LV_ERROR        VARCHAR2(2000);
    LE_EXCEPTION    EXCEPTION;
    LE_EXCEPTION2   EXCEPTION;
    LV_APLICACION   VARCHAR2(150) := 'PR_REPORTE_SCP.PR_GENERA_BUSQUEDA - ';
    
    --
      LV_ID_SERVICIO      VARCHAR2(20);
      ln_existe number:=0;
      old_id_servicio varchar2(20) ;
      old_co_id      number(38) ; 
      old_id_contrato number(10) ; 
      old_id_detalle_plan  number(10) ;
      old_id_plan varchar2(10) ;
      old_descripcion_plan varchar2(60) ;
      old_fecha_inicio  date ;
      old_fecha_fin  date ;
      old_tipo varchar2(20) ;
      old_id_subproducto   varchar2(20) ;
      old_estado  varchar2(1) ;
      old_id_categoria_amx varchar2(3);
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    LV_NOMBRE_TABLA  := 'GSI_RPT_PIVOTE_' || LV_MES;
    --LV_NOMBRE_TABLA2 := 'GSI_RPT_PIVOTE_DET_' || LV_MES;
    -- abrimos el primer cursor
    OPEN CURSOR1 FOR 'select f.CUENTA,
                                f.TELEFONO,
                                f.CICLO,
                                f.OBSERVACION,
                                f.HILO,
                                f.FECHA_INI_CORTE,
                                f.FECHA_FIN_CORTE,
                                f.CUSTOMER_ID,
                                f.ROWID                             
                     from ' || LV_NOMBRE_TABLA || ' f ' || ' WHERE f.hilo = '||pv_hilo||' AND f.observacion =''OK3''';
   
      -- Recorrer los cursores, llenar las gsi final
      LOOP
      --
         BEGIN
            FETCH CURSOR1      
              INTO CUENTA,
                   TELEFONO,
                   CICLO,
                   OBSERVACION,
                   HILO,
                   FECHA_INI_CORTE,
                   FECHA_FIN_CORTE,
                   CUSTOMER_ID,
                   LV_ROWID;
            --
            EXIT WHEN CURSOR1%NOTFOUND;

              lv_observacion:='';
              lv_control:='';
              old_id_plan:='';
              ln_existe:=0;
       
                FOR K IN c_registros(telefono, fecha_ini_corte, fecha_fin_corte) LOOP
                --
                  ln_existe:=1;
                  LV_NOMBRE_TABLA2 := 'GSI_RPT_PIVOTE_DET_' || LV_MES;
                  --
                  if k.id_plan <> old_id_plan and old_id_plan is not null then
                  
                  --
                  
                  -- Inserccion a las tablas Gsi final
                  LV_SENTENCIA:= 'insert into '|| LV_NOMBRE_TABLA2 ||' (id_servicio, co_id, id_contrato, id_detalle_plan,
                                                id_plan, descripcion_plan, fecha_inicio, fecha_fin, tipo, 
                                                id_subproducto, estado, id_categoria_amx )
                                   values ('
                                          || '''' ||old_id_servicio|| '''' ||', '||
                                          old_co_id       ||', '||
                                          old_id_contrato  ||', '||
                                          old_id_detalle_plan  ||', '||
                                          '''' ||old_id_plan|| ''', '||
                                          '''' ||old_descripcion_plan || ''', '||
                                          'to_date(' || '''' ||old_fecha_inicio || ''', ''dd-mm-yyyy'') ' ||', '||
                                          'to_date(' || '''' ||old_fecha_fin || ''', ''dd-mm-yyyy'') ' ||', '||
                                          '''' ||old_tipo || ''', '||
                                          '''' ||old_id_subproducto || ''', '||
                                          '''' ||old_estado || ''', '||
                                          '''' ||old_id_categoria_amx || '''' 
                                           || ') ';
                          BEGIN
                            EXECUTE IMMEDIATE LV_SENTENCIA;
                          EXCEPTION
                            WHEN OTHERS THEN
                              lv_control := 'NO_OK3_OTHERS';
                              LV_ERROR       := SUBSTR(SQLERRM, 1, 200);      
                              RAISE LE_EXCEPTION2;  
                          END;
                   end if;                
                   old_id_servicio := k.id_servicio;
                   old_co_id     := k.co_id; 
                   old_id_contrato := k.id_contrato; 
                   old_id_detalle_plan  := k.id_detalle_plan;
                   old_id_plan := k.id_plan;
                   old_descripcion_plan := k.descripcion_plan;
                   old_fecha_inicio  := k.fecha_inicio;
                   old_fecha_fin  := k.fecha_fin;
                   old_tipo := k.tipo;
                   old_id_subproducto   := k.id_subproducto;
                   old_estado  := k.estado;
                   old_id_categoria_amx :=k.id_categoria_amx;
                --                        
                END LOOP;
              
               if ln_existe=1 then
                   LV_SENTENCIA:= 'insert into '|| LV_NOMBRE_TABLA2 ||' (id_servicio, co_id, id_contrato, id_detalle_plan,
                                                  id_plan, descripcion_plan, fecha_inicio, fecha_fin, tipo, 
                                                  id_subproducto, estado, id_categoria_amx )
                                    values ('
                                            || '''' ||old_id_servicio|| '''' ||', '||
                                            old_co_id     ||', '||
                                            old_id_contrato  ||', '||
                                            old_id_detalle_plan  ||', '||
                                            '''' ||old_id_plan|| ''', '||
                                            '''' ||old_descripcion_plan || ''', '||
                                            'to_date(' || '''' ||old_fecha_inicio || ''', ''dd-mm-yyyy'') ' ||', '||
                                            'to_date(' || '''' ||old_fecha_fin || ''', ''dd-mm-yyyy'') ' ||', '||
                                            '''' ||old_tipo || ''', '||
                                            '''' ||old_id_subproducto || ''', '||
                                            '''' ||old_estado || ''', '||
                                            '''' ||old_id_categoria_amx || '''' 
                                             || ') ';
             --        
              BEGIN
                EXECUTE IMMEDIATE LV_SENTENCIA ;
              EXCEPTION
                WHEN OTHERS THEN
                  lv_observacion := 'NO_OK3_OTHERS';
                  LV_ERROR       := SUBSTR(SQLERRM, 1, 200);      
                  RAISE LE_EXCEPTION2;  
              END;
              lv_observacion :='2_OK';
                
              else
                lv_observacion:='2_NO_ROWS';
              end if;
             LV_SENTENCIA:= 'update '||LV_NOMBRE_TABLA||
                             ' set observacion= '|| ''''||lv_observacion||'''' ||
                              ' where rowid = '''||LV_ROWID||''''; 
                               
              BEGIN
                EXECUTE IMMEDIATE LV_SENTENCIA;
              EXCEPTION
                WHEN no_data_found then
                  lv_observacion := 'NO_OK2_FOUND';
                  RAISE LE_EXCEPTION2;
                WHEN OTHERS THEN
                  lv_observacion := 'NO_OK2_OTHERS';
                  LV_ERROR       := SUBSTR(SQLERRM, 1, 200);      
                  RAISE LE_EXCEPTION2;  
              END;
              --
              ln_cont := ln_cont + 1;
              IF (ln_cont Mod ln_commit) = 0 then
                Commit;
              end if;
              --  
      EXCEPTION 
      when LE_EXCEPTION2  THEN
         --LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
         LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set control = ' ||''''|| 
                           SUBSTR(lv_observacion ||' => '||LV_ERROR||'''',1,200)||' where rowid = '''||LV_ROWID||'''';
         EXECUTE IMMEDIATE LV_SENTENCIA;
      when others then 
          LV_ERROR       := SUBSTR(SQLERRM, 1, 200);
          LV_SENTENCIA := '  update ' || LV_NOMBRE_TABLA || ' set  control = ' ||''''|| 
          SUBSTR( 'PRINC_OTHERS' ||' => '||LV_ERROR||'''',1,200)|| ' where rowid = '''||LV_ROWID||'''';
          EXECUTE IMMEDIATE LV_SENTENCIA;     
      END;
    END LOOP;
    COMMIT;
    CLOSE CURSOR1;
    --
    EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);    
  END PR_GENERA_INSERT_GSI;

  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP 
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : PROCEDIMIENTO GENERA TABLAS TEMPORALES TMP_GSI 
  --===============================================================================================--

  PROCEDURE PR_GENERA_TEMPORAL_TMP_GSI(PV_FECHA VARCHAR2,
                                       PV_ERROR OUT VARCHAR2) IS
  
    -- Declaracion de Variables
    LV_NOMBRE_TABLA  VARCHAR2(2000);
    LV_NOMBRE_TABLA2 VARCHAR2(2000);
    LV_NOMBRE_TABLA3 VARCHAR2(2000);
    LV_SENTENCIA     VARCHAR2(32650);
    LV_ERROR         VARCHAR2(2000);
    LV_FECHA_ANT     VARCHAR2(2000);
    LV_FECHA_ACT     VARCHAR2(2000);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_CPM_BUSCA_GRATIS_MES - ';
    LV_MES        VARCHAR2(10);
  
  BEGIN
    LV_FECHA_ANT    := TO_CHAR(ADD_MONTHS(TO_DATE(PV_FECHA, 'DD/MM/RRRR'),
                                          -1),
                               'RRRRMM');
    LV_NOMBRE_TABLA := 'TMP_GSI_TAT_' || SUBSTR(PV_FECHA, 7) ||
                       SUBSTR(PV_FECHA, 4, 2) || '_PR';
  
    OPEN C_OBTIENE_SENTENCIA('CREATE TABLE_TEMP_GSI');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
    --tablespace TECNOMEN_DAT
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          --RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    LV_FECHA_ACT     := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);
    LV_NOMBRE_TABLA2 := 'RP_' || LV_FECHA_ANT || '_' || LV_FECHA_ACT;
    -- INSERTA los datos de rp en tabla final
    LV_SENTENCIA := 'insert into ' || LV_NOMBRE_TABLA || ' select * from ' ||
                    LV_NOMBRE_TABLA2;
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          --RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
    LV_NOMBRE_TABLA3 := 'gsi_rpt_pivote_det_' || LV_MES;
  
    LV_SENTENCIA := 'select a.*,
                    b.id_detalle_plan,
                    b.id_plan,
                    b.descripcion_plan,
                    b.fecha_inicio,
                    b.fecha_fin,
                    b.id_subproducto,
                    b.tmcode
                    from ' || LV_NOMBRE_TABLA || ' a, ' ||
                    LV_NOMBRE_TABLA3 || ' b
                    where
                    a.cust_info_contract_id=b.co_id and
                    a.tmcoder=b.tmcode';
    BEGIN
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          --RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_GENERA_TEMPORAL_TMP_GSI;

  --===============================================================================================--
  -- PROJECT : GENERA REPORTES SCP
  -- AUTHOR  : SUD Evelyn Gonzalez.
  -- CREATED : 25/04/2013 15:18:54
  -- CRM     : Ing Hector Ponce
  -- PURPOSE : Proceso que crea vista TMP por mes 
  --===============================================================================================--
  --este query sirve para obtener datos para tabular en excel
  PROCEDURE PR_CREA_VISTA_TMP_MES(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
  
    -- Declaracion de Variables
    LV_FECHA_ACT      VARCHAR2(2000);
    LV_NOMBRE_TABLA2  VARCHAR2(2000);
    LV_NOMBRE_TABLA3  VARCHAR2(2000);
    LV_SENTENCIA      VARCHAR2(32650);
    LV_ERROR          VARCHAR2(2000);
    LV_NOMBRE_REPORTE VARCHAR2(2000);
    LV_MES            VARCHAR2(10);
    LE_EXCEPTION EXCEPTION;
    LV_APLICACION VARCHAR2(150) := 'PR_REPORTE_SCP.PR_CREA_VISTA_TMP_MES - ';
  BEGIN
    IF PV_FECHA IS NULL THEN
      PV_ERROR := 'Ingrese fecha';
      RETURN;
    END IF;
  
    LV_NOMBRE_REPORTE := 'REPORTE_';
    LV_FECHA_ACT      := SUBSTR(PV_FECHA, 7) || SUBSTR(PV_FECHA, 4, 2);

    OPEN C_OBTENER_MES(SUBSTR(PV_FECHA, 4, 2));
    FETCH C_OBTENER_MES
      INTO LV_MES;
    CLOSE C_OBTENER_MES;
  
    LV_NOMBRE_REPORTE := LV_NOMBRE_REPORTE || LV_MES;
    LV_NOMBRE_TABLA2  := 'GSI_TAT_' || LV_FECHA_ACT || '_PR';
    LV_NOMBRE_TABLA3  := 'GSI_TAT_' || LV_FECHA_ACT || '_FU';
    OPEN C_OBTIENE_SENTENCIA('CREATE VIEW_GSI_TAT');
    FETCH C_OBTIENE_SENTENCIA
      INTO LV_SENTENCIA;
    CLOSE C_OBTIENE_SENTENCIA;
    BEGIN
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_REPORTE);
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA2);
      LV_SENTENCIA := REPLACE(LV_SENTENCIA,
                              ':NOMBRE_TABLA',
                              LV_NOMBRE_TABLA3);
      EXECUTE IMMEDIATE LV_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_EXCEPTION;
          NULL;
        END IF;
    END;
  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END PR_CREA_VISTA_TMP_MES;

END PR_REPORTE_SCP_PRUEBA;
/
