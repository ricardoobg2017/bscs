CREATE OR REPLACE PACKAGE OP_RECAUDADOR_DAS IS
  TYPE TYPE_DATOS_CLIENTE IS RECORD(
    CUENTA           VARCHAR2(24),
    ID_CLIENTE       NUMBER,
    PRODUCTO         VARCHAR2(30),
    CANTON           VARCHAR2(40),
    PROVINCIA        VARCHAR2(25),
    APELLIDOS        VARCHAR2(40),
    NOMBRES          VARCHAR2(40),
    RUC              VARCHAR2(20),
    ID_FORMA_PAGO    VARCHAR2(5),
    FORMA_PAGO       VARCHAR2(58),
    FECH_APER_CUENTA DATE,
    TELEFONO1        VARCHAR2(25),
    TELEFONO2        VARCHAR2(25),
    TELEFONO         VARCHAR2(63),
    DIRECCION        VARCHAR2(70),
    DIRECCION2       VARCHAR2(200),
    ID_SOLICITUD     VARCHAR2(10),
    LINEAS_ACT       VARCHAR2(10),
    LINEAS_INAC      VARCHAR2(10),
    LINEAS_SUSP      VARCHAR2(10),
    COD_VENDEDOR     VARCHAR2(10),
    VENDEDOR         VARCHAR2(2000),
    NUM_FACTURA      VARCHAR2(30),
    BALANCE_1        NUMBER,
    BALANCE_2        NUMBER,
    BALANCE_3        NUMBER,
    BALANCE_4        NUMBER,
    BALANCE_5        NUMBER,
    BALANCE_6        NUMBER,
    BALANCE_7        NUMBER,
    BALANCE_8        NUMBER,
    BALANCE_9        NUMBER,
    BALANCE_10       NUMBER,
    BALANCE_11       NUMBER,
    BALANCE_12       NUMBER,
    TOTALVENCIDA     NUMBER,
    TOTAL_DEUDA      NUMBER,
    MAYORVENCIDO     VARCHAR2(10),
    COMPANIA         NUMBER,
    FECH_MAX_PAGO    DATE,
    ID_PLAN          VARCHAR2(10),
    DETALLE_PLAN     VARCHAR2(4000),
    TIPO_ING_SOLIC   NUMBER);

  TYPE DATOS_CLIENTE IS TABLE OF TYPE_DATOS_CLIENTE INDEX BY BINARY_INTEGER;
  GN_ID_BITACORA_SCP     NUMBER;
  GN_BITACORA_ERROR      NUMBER := 0;
  GN_BITACORA_PROCESADOS NUMBER := 0;

  PROCEDURE CARGA(Pn_Hilo NUMBER, Pv_Reproceso varchar2 default null, Pv_Error OUT VARCHAR2);

  PROCEDURE P_CREACION_INDEX(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2);

  PROCEDURE P_TIPO_CLIENTE(PV_CUENTA      VARCHAR2,
                           PV_TIPOCLIENTE OUT VARCHAR2,
                           PV_ERROR       OUT VARCHAR2);

  PROCEDURE P_PAGOS_CREDITOS(PN_CUSTOMER_ID  NUMBER,
                             PD_FECHACORTE   DATE,
                             PN_DEUDA        NUMBER DEFAULT NULL,
                             PV_VERI_CREDITO VARCHAR2,
                             PN_SALDO        OUT NUMBER,
                             PN_PAGO         OUT NUMBER,
                             PN_CREDITO      OUT NUMBER,
                             PV_ERROR        OUT VARCHAR2);

  PROCEDURE P_CONFIRMAR_PROCESO(PV_PROCESO        VARCHAR2,
                                PV_NOMBRE_ARCHIVO VARCHAR2,
                                PV_ERROR          OUT VARCHAR2);

  PROCEDURE P_ACTUALIZA_PAGOS(PN_HILO NUMBER, PV_ERROR OUT VARCHAR2);

  PROCEDURE P_FINANCIAMIENTO(PV_CUENTA        VARCHAR2,
                             PV_FINANCIMIENTO OUT VARCHAR2,
                             PV_FECHA_CORTE   VARCHAR2,
                             Pv_Error         OUT VARCHAR2);

  PROCEDURE P_ACT_PAG_RECAUDADOR(Pn_Hilo NUMBER, Pv_Error OUT VARCHAR2);

  PROCEDURE P_ACT_PERIODOS_TERMINADOS(Pv_Error OUT VARCHAR2);

  PROCEDURE P_CARGA_ARCHIVO(PN_INTERACION NUMBER,
                            PV_LINEA      VARCHAR2,
                            PV_SEPARADOR  VARCHAR2,
                            PV_ERROR      OUT VARCHAR2);
  PROCEDURE P_ACTUALIZA_PAGOS_ELITE(PN_HILO NUMBER, PV_ERROR OUT VARCHAR2);
  
  PROCEDURE P_CALCULA_BALANCE(PN_ID_BALANCE    NUMBER,
                               PN_PAGO          NUMBER,
                               PN_BALANCE1      IN OUT NUMBER,
                               PN_BALANCE2      IN OUT NUMBER,
                               PN_BALANCE3      IN OUT NUMBER,
                               PN_BALANCE4      IN OUT NUMBER,
                               PN_BALANCE5      IN OUT NUMBER,
                               PN_BALANCE6      IN OUT NUMBER,
                               PN_BALANCE7      IN OUT NUMBER,
                               PN_BALANCE8      IN OUT NUMBER,
                               PN_BALANCE9      IN OUT NUMBER,
                               PN_BALANCE10     IN OUT NUMBER,
                               PN_BALANCE11     IN OUT NUMBER,
                               PN_BALANCE12     IN OUT NUMBER,
	                             PN_TOTAL_VENCIDO  OUT NUMBER,
                               PV_ERROR         OUT VARCHAR2);
END OP_RECAUDADOR_DAS;
/
CREATE OR REPLACE PACKAGE BODY OP_RECAUDADOR_DAS IS
  --=====================================================================================--
  -- CREADO POR:     Andres Balladares.
  -- FECHA MOD:      29/09/2016
  -- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Recaudador DAS - Recuperacion de cartera.
  --=====================================================================================--
  --=====================================================================================--
  -- ACTUALIZADO POR: Jordan Rodriguez.
  -- FECHA MOD:      14/10/2016
  -- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Recaudador DAS - Recuperacion de cartera, validacion de fecha de ejecucion.
  --=====================================================================================--
  --=====================================================================================--
  -- ACTUALIZADO POR: Andrés Balladares.
  -- FECHA MOD:      24/02/2017
  -- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Recaudador DAS - Asignación de recaudador y actualización de pagos diarios.
  --=====================================================================================--
  --=====================================================================================--
  -- MODIFICADO POR: JORDAN RODRIGUEZ.
  -- FECHA MOD:      14/06/2017
  -- PROYECTO:       [11334] CAMBIOS CONTINUO RELOJ DE COBRANZAS Y REACTIVACIONES
  -- LIDER IRO:      IRO JUAN ROMERO
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         RECAUDADOR DAS - SE AGREGAN LOS CLIENTES CATEGORIZADOS COMO VIP,PRIMIUM,ELITE, E INSIGNIA.
  --=====================================================================================--
    --=====================================================================================--
  -- MODIFICADO POR: JORDAN RODRIGUEZ.
  -- FECHA MOD:      23/06/2017
  -- PROYECTO:       [11334] CAMBIOS CONTINUO RELOJ DE COBRANZAS Y REACTIVACIONES
  -- LIDER IRO:      IRO JUAN ROMERO
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Se realiza ajuste en el calculo de los balances 
  --                 y calculo del saldo al momento de cargar un nuevo corte
  --=====================================================================================--
  /* =====================================================================================
  -- MODIFICADO POR: JORDAN RODRIGUEZ.
  -- FECHA MOD:      19/07/2017
  -- PROYECTO:       [11477] CAMBIOS CONTINUO RELOJ DE COBRANZAS Y REACTIVACIONES
  -- LIDER IRO:      IRO JUAN ROMERO
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Se agrega esquema de reproceso en el procedimiento de carga 
  --                 Se agrega ademas validaciones de fecha de actualización en 
  --                 los cursores de los proc: P_actualiza_pagos, P_act_pag_recuadador 
  --                 y p_actualiza_pagos_elite.
  ========================================================================================*/
  
  /* ======================================================================================
  -- MODIFICADO POR: HTS GENESIS LINDAO.
  -- FECHA MOD:      27/01/2020
  -- PROYECTO:       [12512] Legado Cobranzas DBC Reloj Cobra
  -- LIDER HTS:      HTS GLORIA ROJAS
  -- LIDER :         SIS ANTONIO MAYORGA
  -- MOTIVO:         Modificacion de longitud de campos de la tabla RC_OPE_RECAUDA. 
  =========================================================================================*/
  
  PROCEDURE CARGA(Pn_Hilo NUMBER, Pv_Reproceso varchar2 default null, Pv_Error OUT VARCHAR2) IS
  
    CURSOR C_TIPO_APROBACION(Cn_IdSolicitud NUMBER) IS
      SELECT T.DESCRIPCION
        FROM CR_SOLICITUDES@AXIS S, CR_TIPOS_APROBACIONES@AXIS T
       WHERE S.ID_SOLICITUD = Cn_IdSolicitud
         AND T.ID_TIPO_APROBACION = S.ID_TIPO_APROBACION;
  
    TYPE C_OBTIENE_BASE_CLIENTES IS REF CURSOR;
    OBTIENE_BASE_CLIENTES C_OBTIENE_BASE_CLIENTES;
    TYPE TAB_OPE_RECAUD IS TABLE OF RC_OPE_RECAUDA%ROWTYPE INDEX BY BINARY_INTEGER;
    Lr_TAB_OPE_RECAUD TAB_OPE_RECAUD;
  
    Tb_Datos           DATOS_CLIENTE;
    Lv_SqlCarga        VARCHAR2(10000);
    Lv_SqlCargaAxis    VARCHAR2(10000);
    Lv_Fecha           VARCHAR2(20);
    Lv_FechaCorte      VARCHAR2(20);
    Lv_Planes          VARCHAR2(4000);
    Lv_OpeTipoClient   VARCHAR2(4000);
    Lv_TipoCliente     VARCHAR2(50);
    Lv_DescAprobacion  VARCHAR2(4000);
    Lv_CreacionIndex   VARCHAR2(5);
    ln_parametro       NUMBER;
    Lv_BanderaEjec     VARCHAR2(1);
    Ln_ValorMinDeud    NUMBER;
    Ln_Count           NUMBER := 0;
    Ln_Pagos           NUMBER;
    Ln_Creditos        NUMBER;
    Ln_Saldo_Pendiente NUMBER;
    Ln_CountBand       NUMBER := 0;
    Ln_NumBandera      NUMBER;
    LV_ERROR_PAGOS     VARCHAR2(500);
    LV_ERROR_CLIENTE   VARCHAR2(500);
    LV_ERROR_IDEX      VARCHAR2(500);
    Le_Error EXCEPTION;
    LV_PROGRAMA_SCP    VARCHAR2(200) := 'OP_RECAUDADOR_DAS.CARGA';
    LV_UNIDAD_REGISTRO VARCHAR2(200) := 'BITACORIZAR';
    LN_ERROR_SCP       NUMBER := 0;
    LV_ERROR_SCP       VARCHAR2(500);
    LV_MENSAJE         VARCHAR2(4000);
    LV_MENSAJE_APL_SCP VARCHAR2(1000);
    LV_MENSAJE_TEC_SCP VARCHAR2(1000);
    LN_ID_DETALLE      NUMBER := 0;
    LD_FECHACORTE_ANT  DATE;
    LV_DIA             VARCHAR2(10);
    LV_MES_ANNO        VARCHAR(20);
    Lv_SqlQuery        VARCHAR(6000);
    Ln_CantHilo        NUMBER;
    --LN_EXITE           NUMBER;
    LE_ERROR_TABLA  EXCEPTION;
    LE_ERROR_EXITE1 EXCEPTION;
    LE_ERROR_EXITE2 EXCEPTION;
    Lv_Financiamiento VARCHAR2(10);
    Lv_ErrorFinan     VARCHAR2(500);
    Ld_FechaFinCiclo  DATE;
    LV_BANDERA_NS VARCHAR2(3);
    lV_ESTADO     VARCHAR2(3);
    LN_BALANCE    NUMBER:=0;
    LV_ERROR      VARCHAR2(300);
    
    CURSOR C_DETERMINA_CARGA(CV_FECHA_CORTE VARCHAR2, CN_HILO NUMBER) IS
     SELECT FECHA_INGRESO
       FROM (SELECT TRUNC(A.FECHA_INGRESO) FECHA_INGRESO
               FROM RC_OPE_RECAUDA A
              WHERE A.FECHA_CORTE = TO_DATE(CV_FECHA_CORTE, 'DD/MM/YYYY')
                AND A.HILO = CN_HILO
              ORDER BY A.FECHA_INGRESO DESC)
      WHERE ROWNUM < 2;
    LC_DETERMINA_CARGA C_DETERMINA_CARGA%ROWTYPE;
  BEGIN
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('OP_BITACORA_DAS',
                                              LV_PROGRAMA_SCP || '_' ||
                                              PN_HILO,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              0,
                                              LV_UNIDAD_REGISTRO,
                                              GN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    IF LN_ERROR_SCP <> 0 THEN
      LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' ||
                    LV_ERROR_SCP;
    END IF;
  
    Ln_NumBandera := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                   PV_IDPARAMETRO     => 'OPE_NUM_BAND',
                                                                   PV_ERROR           => Pv_Error);
  
    IF Pv_Error IS NOT NULL THEN
      RAISE Le_Error;
    END IF;
  
    Ln_ValorMinDeud := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                     PV_IDPARAMETRO     => 'OPE_VAL_DEUD',
                                                                     PV_ERROR           => Pv_Error);
  
    IF Pv_Error IS NOT NULL THEN
      RAISE Le_Error;
    END IF;
  
    Lv_Planes := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                               PV_IDPARAMETRO     => 'OPE_PLAN_EXCL',
                                                               PV_ERROR           => Pv_Error);
  
    IF Pv_Error IS NOT NULL THEN
      RAISE Le_Error;
    END IF;
  
    LV_BANDERA_NS := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                                   PV_IDPARAMETRO     => 'DAS_CONS_TIPO_CLIENTE',
                                                                   PV_ERROR           => Pv_Error);
  
    IF NVL(LV_BANDERA_NS, 'N') = 'S' THEN
      Lv_OpeTipoClient := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                                        PV_IDPARAMETRO     => 'DAS_DESCRIP_CLIENTE',
                                                                        PV_ERROR           => Pv_Error);
    ELSE
      Lv_OpeTipoClient := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                        PV_IDPARAMETRO     => 'OPE_TIP_CLIENT',
                                                                        PV_ERROR           => Pv_Error);
    END IF;
    IF Pv_Error IS NOT NULL THEN
      RAISE Le_Error;
    END IF;
  
  ln_parametro := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                PV_IDPARAMETRO     => 'OPE_NUM_DIAS',
                                                                PV_ERROR           =>  Pv_Error);
  LV_DIA        := TO_CHAR(SYSDATE, 'DD');
  LV_MES_ANNO    := TO_CHAR(SYSDATE, 'MM/YYYY');
  
    IF TO_NUMBER(LV_DIA) > (2 + NVL(ln_parametro, 5)) AND
       TO_NUMBER(LV_DIA) <= (8 + NVL(ln_parametro, 5)) THEN
      LV_MES_ANNO := '02/' || LV_MES_ANNO;
    ELSIF TO_NUMBER(LV_DIA) > (8 + NVL(ln_parametro, 5)) AND
          TO_NUMBER(LV_DIA) <= (15 + NVL(ln_parametro, 5)) THEN
      LV_MES_ANNO := '08/' || LV_MES_ANNO;
    ELSIF TO_NUMBER(LV_DIA) > (15 + NVL(ln_parametro, 5)) AND
          TO_NUMBER(LV_DIA) <= (24 + NVL(ln_parametro, 5)) THEN
      LV_MES_ANNO := '15/' || LV_MES_ANNO;
    ELSE
      IF TO_NUMBER(LV_DIA) BETWEEN 1 AND (2 + NVL(ln_parametro, 5)) THEN
        SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY')
          INTO LV_MES_ANNO
          FROM DUAL;
      END IF;
      LV_MES_ANNO := '24/' || LV_MES_ANNO;
    END IF;
  
    Lv_Fecha         := REPLACE(LV_MES_ANNO, '/', '');
    Lv_FechaCorte    := LV_MES_ANNO;
    Ld_FechaFinCiclo := (TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY') - 1) +
                        INTERVAL '1' MONTH;
    BEGIN
      Lv_SqlQuery := 'SELECT count(1) FROM SYSADM.CO_REPORTE_ADIC_<FECHA>';
      Lv_SqlQuery := REPLACE(Lv_SqlQuery, '<FECHA>', Lv_Fecha);
      EXECUTE IMMEDIATE Lv_SqlQuery
        INTO Ln_CantHilo;
    
      IF Ln_CantHilo IS NOT NULL THEN
        /*SELECT COUNT(1)
          INTO LN_EXITE
          FROM RC_OPE_RECAUDA A
         WHERE A.FECHA_CORTE = TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY')
         AND TRUNC(A.FECHA_INGRESO)=TRUNC(SYSDATE)
         AND A.HILO=Pn_Hilo;*/
         OPEN C_DETERMINA_CARGA(Lv_FechaCorte,Pn_Hilo);
         FETCH C_DETERMINA_CARGA INTO LC_DETERMINA_CARGA;
         CLOSE C_DETERMINA_CARGA;
         IF Pv_Reproceso='R' THEN
           DELETE FROM RC_OPE_RECAUDA A WHERE A.FECHA_CORTE=TO_DATE(Lv_FechaCorte,'DD/MM/RRRR')
           AND A.HILO =  Pn_Hilo;
           COMMIT;
         ELSIF TRUNC(SYSDATE) > LC_DETERMINA_CARGA.FECHA_INGRESO THEN
          RAISE LE_ERROR_EXITE1;
        END IF;
      END IF;
    EXCEPTION
      WHEN LE_ERROR_EXITE1 THEN
        RAISE LE_ERROR_EXITE2;
      WHEN OTHERS THEN
        RAISE LE_ERROR_TABLA;
    END;
  
    Lv_CreacionIndex := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                      PV_IDPARAMETRO     => 'OPE_BAND_INDEX',
                                                                      PV_ERROR           => Pv_Error);
  
    IF NVL(Lv_CreacionIndex, 'N') = 'S' AND PN_HILO = 1 THEN
      P_CREACION_INDEX(PV_FECHA => Lv_Fecha, PV_ERROR => LV_ERROR_IDEX);
    END IF;
  
    Lv_SqlCargaAxis := 'SELECT B.CUENTA, NULL ID_CLIENTE, NULL PRODUCTO, NULL CANTON, NULL PROVINCIA, NULL APELLIDOS, NULL NOMBRES, NULL RUC, B.FORMA_PAGO ID_FORMA_PAGO, 
                           NULL FORMA_PAGO, NULL FECH_APER_CUENTA, NULL TELEFONO1, NULL TELEFONO2, NULL TELEFONO, NULL DIRECCION, NULL DIRECCION2, B.ID_SOLICITUD, B.LINEAS_ACT, 
                           B.LINEAS_INAC, B.LINEAS_SUSP, B.COD_VENDEDOR, B.VENDEDOR, NULL NUM_FACTURA, NULL BALANCE_1, NULL BALANCE_2, NULL BALANCE_3, NULL BALANCE_4, NULL BALANCE_5,
                           NULL BALANCE_6, NULL BALANCE_7, NULL BALANCE_8, NULL BALANCE_9, NULL BALANCE_10, NULL BALANCE_11, NULL BALANCE_12, NULL TOTALVENCIDA, NULL TOTALADEUDA,
                           NULL MAYORVENCIDO, NULL COMPANIA, NULL FECH_MAX_PAGO, B.ID_PLAN, B.DETALLE_PLAN,B.TIPO_INGRESO
                    FROM SYSADM.CO_REPORTE_ADIC_<FECHA> B
                    WHERE B.ID_PLAN NOT IN (''<PLANES>'')
                    AND B.HILO =' || PN_HILO;
  
    Lv_SqlCargaAxis := REPLACE(Lv_SqlCargaAxis, '<FECHA>', Lv_Fecha);
    Lv_SqlCargaAxis := REPLACE(Lv_SqlCargaAxis, '<PLANES>', Lv_Planes);
  
    OPEN OBTIENE_BASE_CLIENTES FOR Lv_SqlCargaAxis;
    LOOP
      FETCH OBTIENE_BASE_CLIENTES BULK COLLECT
        INTO Tb_Datos LIMIT 1000;
      EXIT WHEN Tb_Datos.COUNT = 0;
    
      FOR I IN Tb_Datos.FIRST .. Tb_Datos.LAST LOOP
      
        LV_ERROR_CLIENTE := NULL;
        LV_ERROR_PAGOS   := NULL;
        Lv_ErrorFinan    := NULL;
        Lv_TipoCliente   := null;
        lV_ESTADO        := NULL;
        Ln_CountBand     := Ln_CountBand + 1;
      
        IF Ln_CountBand >= Ln_NumBandera THEN
        
          Lv_BanderaEjec := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                          PV_IDPARAMETRO     => 'OPE_BAND_EJE',
                                                                          PV_ERROR           => Pv_Error);
        
          IF NVL(Lv_BanderaEjec, 'N') = 'S' THEN
            Pv_Error := 'Abortado manualmente';
            RAISE Le_Error;
          END IF;
        
          Ln_CountBand := 0;
        END IF;
      
        BEGIN
          Lv_SqlCarga := 'SELECT A.ID_CLIENTE, A.PRODUCTO, A.CANTON, A.PROVINCIA, A.APELLIDOS, A.NOMBRES, A.RUC, A.FORMA_PAGO, A.FECH_APER_CUENTA,
                                  A.TELEFONO1, A.TELEFONO2, A.TELEFONO, A.DIRECCION, A.DIRECCION2, A.NUM_FACTURA, A.BALANCE_1, A.BALANCE_2, A.BALANCE_3, A.BALANCE_4,
                                  A.BALANCE_5, A.BALANCE_6, A.BALANCE_7, A.BALANCE_8, A.BALANCE_9, A.BALANCE_10, A.BALANCE_11, A.BALANCE_12, A.TOTALVENCIDA, A.TOTALADEUDA,
                                  A.MAYORVENCIDO, A.COMPANIA, A.FECH_MAX_PAGO
                           FROM SYSADM.CO_REPCARCLI_<FECHA> A
                           WHERE A.CUENTA= ''<CUENTA>''
                           AND A.RUC NOT IN (SELECT C.IDENTIFICACION FROM PORTA.CL_CLIENTES_ESPECIALES@AXIS C WHERE C.ESTADO = ''A'')';
        
          Lv_SqlCarga := REPLACE(Lv_SqlCarga, '<FECHA>', Lv_Fecha);
          Lv_SqlCarga := REPLACE(Lv_SqlCarga, '<CUENTA>', Tb_Datos(I).CUENTA);
          EXECUTE IMMEDIATE Lv_SqlCarga
            INTO Tb_Datos(I).ID_CLIENTE, Tb_Datos(I).PRODUCTO, Tb_Datos(I).CANTON, Tb_Datos(I).PROVINCIA, Tb_Datos(I).APELLIDOS, Tb_Datos(I).NOMBRES, Tb_Datos(I).RUC, Tb_Datos(I).FORMA_PAGO, Tb_Datos(I).FECH_APER_CUENTA, Tb_Datos(I).TELEFONO1, Tb_Datos(I).TELEFONO2, Tb_Datos(I).TELEFONO, Tb_Datos(I).DIRECCION, Tb_Datos(I).DIRECCION2, Tb_Datos(I).NUM_FACTURA, Tb_Datos(I).BALANCE_1, Tb_Datos(I).BALANCE_2, Tb_Datos(I).BALANCE_3, Tb_Datos(I).BALANCE_4, Tb_Datos(I).BALANCE_5, Tb_Datos(I).BALANCE_6, Tb_Datos(I).BALANCE_7, Tb_Datos(I).BALANCE_8, Tb_Datos(I).BALANCE_9, Tb_Datos(I).BALANCE_10, Tb_Datos(I).BALANCE_11, Tb_Datos(I).BALANCE_12, Tb_Datos(I).TOTALVENCIDA, Tb_Datos(I).TOTAL_DEUDA, Tb_Datos(I).MAYORVENCIDO, Tb_Datos(I).COMPANIA, Tb_Datos(I).FECH_MAX_PAGO;
        
        EXCEPTION
          WHEN OTHERS THEN
            LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.CARGA';
            LV_MENSAJE_TEC_SCP := 'SE EXCUYO LA CUENTA: ' || Tb_Datos(I)
                                 .CUENTA || ' ERROR: ' ||
                                  SUBSTR(SQLERRM, 1, 100);
            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                      LV_MENSAJE_APL_SCP,
                                                      LV_MENSAJE_TEC_SCP,
                                                      NULL,
                                                      0,
                                                      0,
                                                      NULL,
                                                      Tb_Datos(I).CUENTA,
                                                      0,
                                                      0,
                                                      'N',
                                                      LN_ID_DETALLE,
                                                      LN_ERROR_SCP,
                                                      LV_MENSAJE);
        END;
      
        IF Tb_Datos(I).RUC IS NOT NULL THEN
          IF NVL(LV_BANDERA_NS, 'N') = 'S' THEN
            PORTA.OP_REPORTEXCAPAS_SEGMENTADO.P_TIPO_CLIENTE@AXIS(PV_CUENTA      => Tb_Datos(I).CUENTA,
                                                                  PV_RUC         => Tb_Datos(I).RUC,
                                                                  PV_IDPLAN      => Tb_Datos(I).ID_PLAN,
                                                                  PN_TIPOINGSOL  => Tb_Datos(I).TIPO_ING_SOLIC,
                                                                  PV_TIPOCLIENTE => Lv_TipoCliente,
                                                                  PV_ERROR       => LV_ERROR_CLIENTE);
          ELSE
            P_TIPO_CLIENTE(PV_CUENTA      => Tb_Datos(I).CUENTA,
                           PV_TIPOCLIENTE => Lv_TipoCliente,
                           PV_ERROR       => LV_ERROR_CLIENTE);
          END IF;
          IF /*INSTR(Lv_OpeTipoClient, '|' || Lv_TipoCliente || '|', 1, 1) = 0  AND */ LV_ERROR_CLIENTE IS NULL THEN
            IF INSTR(Lv_OpeTipoClient, '|' || Lv_TipoCliente || '|', 1, 1) = 0 THEN
              lV_ESTADO := 'I';
            ELSE
              lV_ESTADO := 'C';
            END IF;
            P_PAGOS_CREDITOS(PN_CUSTOMER_ID  => Tb_Datos(I).ID_CLIENTE,
                             PD_FECHACORTE   => TO_DATE(Lv_FechaCorte,'DD/MM/YYYY'),
                             PN_DEUDA        => Tb_Datos(I).TOTAL_DEUDA,
                             PV_VERI_CREDITO => 'S',
                             PN_SALDO        => Ln_Saldo_Pendiente,
                             PN_PAGO         => Ln_Pagos,
                             PN_CREDITO      => Ln_Creditos,
                             PV_ERROR        => LV_ERROR_PAGOS);
          
            IF (Ln_Saldo_Pendiente > Ln_ValorMinDeud or lV_ESTADO = 'C') AND LV_ERROR_PAGOS IS NULL THEN
              IF LV_ESTADO = 'C' THEN
                CASE
                  WHEN Tb_Datos(I).BALANCE_1 > 0 THEN
                    LN_BALANCE := 1;
                  WHEN Tb_Datos(I).BALANCE_2 > 0 THEN
                    LN_BALANCE := 2;
                  WHEN Tb_Datos(I).BALANCE_3 > 0 THEN
                    LN_BALANCE := 3;
                  WHEN Tb_Datos(I).BALANCE_4 > 0 THEN
                    LN_BALANCE := 4;
                  WHEN Tb_Datos(I).BALANCE_5 > 0 THEN
                    LN_BALANCE := 5;
                  WHEN Tb_Datos(I).BALANCE_6 > 0 THEN
                    LN_BALANCE := 6;
                  WHEN Tb_Datos(I).BALANCE_7 > 0 THEN
                    LN_BALANCE := 7;
                  WHEN Tb_Datos(I).BALANCE_8 > 0 THEN
                    LN_BALANCE := 8;
                  WHEN Tb_Datos(I).BALANCE_9 > 0 THEN
                    LN_BALANCE := 9;
                  WHEN Tb_Datos(I).BALANCE_10 > 0 THEN
                    LN_BALANCE := 10;
                  WHEN Tb_Datos(I).BALANCE_11 > 0 THEN
                    LN_BALANCE := 11;
                  WHEN Tb_Datos(I).BALANCE_12 > 0 THEN
                    LN_BALANCE := 12;
                  ELSE
                    LN_BALANCE := 0;
                END CASE;
                IF LN_BALANCE > 0 THEN
                  P_CALCULA_BALANCE(PN_ID_BALANCE     => LN_BALANCE,
                                    PN_PAGO           => Tb_Datos(I).TOTAL_DEUDA-LN_SALDO_PENDIENTE,
                                    PN_BALANCE1       => Tb_Datos(I).BALANCE_1,
                                    PN_BALANCE2       => Tb_Datos(I).BALANCE_2,
                                    PN_BALANCE3       => Tb_Datos(I).BALANCE_3,
                                    PN_BALANCE4       => Tb_Datos(I).BALANCE_4,
                                    PN_BALANCE5       => Tb_Datos(I).BALANCE_5,
                                    PN_BALANCE6       => Tb_Datos(I).BALANCE_6,
                                    PN_BALANCE7       => Tb_Datos(I).BALANCE_7,
                                    PN_BALANCE8       => Tb_Datos(I).BALANCE_8,
                                    PN_BALANCE9       => Tb_Datos(I).BALANCE_9,
                                    PN_BALANCE10      => Tb_Datos(I).BALANCE_10,
                                    PN_BALANCE11      => Tb_Datos(I).BALANCE_11,
                                    PN_BALANCE12      => Tb_Datos(I).BALANCE_12,
                                    PN_TOTAL_VENCIDO  => Tb_Datos(I).TOTALVENCIDA,
                                    PV_ERROR          => LV_ERROR);
                END IF;
              END IF;
              OPEN C_TIPO_APROBACION(Tb_Datos(I).ID_SOLICITUD);
              FETCH C_TIPO_APROBACION INTO Lv_DescAprobacion;
              CLOSE C_TIPO_APROBACION;
            
              P_FINANCIAMIENTO(PV_CUENTA        => Tb_Datos(I).CUENTA,
                               PV_FINANCIMIENTO => Lv_Financiamiento,
                               PV_FECHA_CORTE   => Lv_FechaCorte,
                               Pv_Error         => Lv_ErrorFinan);
            
              IF Lv_ErrorFinan IS NOT NULL THEN
                GN_BITACORA_ERROR  := GN_BITACORA_ERROR + 1;
                LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_FINANCIAMIENTO';
                LV_MENSAJE_TEC_SCP := 'ERROR AL CONSULTAR EL FINANCIAMIENTO  CUENTA: ' || Tb_Datos(I)
                                     .CUENTA || ' ERROR: ' || Lv_ErrorFinan;
                SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                          LV_MENSAJE_APL_SCP,
                                                          LV_MENSAJE_TEC_SCP,
                                                          NULL,
                                                          0,
                                                          0,
                                                          Tb_Datos(I).CUENTA,
                                                          NULL,
                                                          0,
                                                          0,
                                                          'N',
                                                          LN_ID_DETALLE,
                                                          LN_ERROR_SCP,
                                                          LV_MENSAJE);
              END IF;
            
              GN_BITACORA_PROCESADOS := GN_BITACORA_PROCESADOS + 1;
            
              Ln_Count := Ln_Count + 1;
              Lr_TAB_OPE_RECAUD(Ln_Count).CUENTA := Tb_Datos(I).CUENTA;
              Lr_TAB_OPE_RECAUD(Ln_Count).ID_CLIENTE := Tb_Datos(I).ID_CLIENTE;
              Lr_TAB_OPE_RECAUD(Ln_Count).PRODUCTO := Tb_Datos(I).PRODUCTO;
              Lr_TAB_OPE_RECAUD(Ln_Count).CANTON := Tb_Datos(I).CANTON;
              Lr_TAB_OPE_RECAUD(Ln_Count).PROVINCIA := Tb_Datos(I).PROVINCIA;
              Lr_TAB_OPE_RECAUD(Ln_Count).APELLIDOS := Tb_Datos(I).APELLIDOS;
              Lr_TAB_OPE_RECAUD(Ln_Count).NOMBRES := Tb_Datos(I).NOMBRES;
              Lr_TAB_OPE_RECAUD(Ln_Count).RUC := Tb_Datos(I).RUC;
              Lr_TAB_OPE_RECAUD(Ln_Count).ID_FORMA_PAGO := Tb_Datos(I).ID_FORMA_PAGO;
              Lr_TAB_OPE_RECAUD(Ln_Count).FORMA_PAGO := Tb_Datos(I).FORMA_PAGO;
              Lr_TAB_OPE_RECAUD(Ln_Count).TIPO_CLIENTE := Lv_TipoCliente;
              Lr_TAB_OPE_RECAUD(Ln_Count).FECH_APER_CUENTA := Tb_Datos(I).FECH_APER_CUENTA;
              Lr_TAB_OPE_RECAUD(Ln_Count).TELEFONO1 := Tb_Datos(I).TELEFONO1;
              Lr_TAB_OPE_RECAUD(Ln_Count).TELEFONO2 := Tb_Datos(I).TELEFONO2;
              Lr_TAB_OPE_RECAUD(Ln_Count).TELEFONO := Tb_Datos(I).TELEFONO;
              Lr_TAB_OPE_RECAUD(Ln_Count).DIRECCION := Tb_Datos(I).DIRECCION;
              Lr_TAB_OPE_RECAUD(Ln_Count).DIRECCION2 := Tb_Datos(I).DIRECCION2;
              Lr_TAB_OPE_RECAUD(Ln_Count).ID_SOLICITUD := Tb_Datos(I).ID_SOLICITUD;
              Lr_TAB_OPE_RECAUD(Ln_Count).LINEAS_ACT := Tb_Datos(I).LINEAS_ACT;
              Lr_TAB_OPE_RECAUD(Ln_Count).LINEAS_INAC := Tb_Datos(I).LINEAS_INAC;
              Lr_TAB_OPE_RECAUD(Ln_Count).LINEAS_SUSP := Tb_Datos(I).LINEAS_SUSP;
              Lr_TAB_OPE_RECAUD(Ln_Count).COD_VENDEDOR := Tb_Datos(I).COD_VENDEDOR;
              Lr_TAB_OPE_RECAUD(Ln_Count).VENDEDOR := Tb_Datos(I).VENDEDOR;
              Lr_TAB_OPE_RECAUD(Ln_Count).NUM_FACTURA := Tb_Datos(I).NUM_FACTURA;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_1 := Tb_Datos(I).BALANCE_1;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_2 := Tb_Datos(I).BALANCE_2;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_3 := Tb_Datos(I).BALANCE_3;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_4 := Tb_Datos(I).BALANCE_4;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_5 := Tb_Datos(I).BALANCE_5;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_6 := Tb_Datos(I).BALANCE_6;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_7 := Tb_Datos(I).BALANCE_7;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_8 := Tb_Datos(I).BALANCE_8;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_9 := Tb_Datos(I).BALANCE_9;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_10 := Tb_Datos(I).BALANCE_10;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_11 := Tb_Datos(I).BALANCE_11;
              Lr_TAB_OPE_RECAUD(Ln_Count).BALANCE_12 := Tb_Datos(I).BALANCE_12;
              Lr_TAB_OPE_RECAUD(Ln_Count).TOTALVENCIDA := Tb_Datos(I).TOTALVENCIDA;
              Lr_TAB_OPE_RECAUD(Ln_Count).TOTAL_DEUDA := Tb_Datos(I).TOTAL_DEUDA;
              Lr_TAB_OPE_RECAUD(Ln_Count).SALDO_PENDIENTE := Ln_Saldo_Pendiente;
              Lr_TAB_OPE_RECAUD(Ln_Count).MAYORVENCIDO := Tb_Datos(I).MAYORVENCIDO;
              Lr_TAB_OPE_RECAUD(Ln_Count).COMPANIA := Tb_Datos(I).COMPANIA;
              Lr_TAB_OPE_RECAUD(Ln_Count).FECH_MAX_PAGO := Tb_Datos(I).FECH_MAX_PAGO;
              Lr_TAB_OPE_RECAUD(Ln_Count).ID_PLAN := Tb_Datos(I).ID_PLAN;
              Lr_TAB_OPE_RECAUD(Ln_Count).DETALLE_PLAN := Tb_Datos(I).DETALLE_PLAN;
              Lr_TAB_OPE_RECAUD(Ln_Count).DESC_APROBACION := Lv_DescAprobacion;
              Lr_TAB_OPE_RECAUD(Ln_Count).ESTADO := lV_ESTADO;
              Lr_TAB_OPE_RECAUD(Ln_Count).FECHA_CORTE := TO_DATE(Lv_FechaCorte,'DD/MM/YYYY');
              Lr_TAB_OPE_RECAUD(Ln_Count).USUARIO_INGRESO := USER;
              Lr_TAB_OPE_RECAUD(Ln_Count).FECHA_INGRESO := SYSDATE;
              Lr_TAB_OPE_RECAUD(Ln_Count).FINANCIAMIENTO := Lv_Financiamiento;
              Lr_TAB_OPE_RECAUD(Ln_Count).FECHA_FIN_CICLO := Ld_FechaFinCiclo;
            ELSIF LV_ERROR_PAGOS IS NOT NULL THEN
            
              GN_BITACORA_ERROR  := GN_BITACORA_ERROR + 1;
              LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_PAGOS_CREDITOS';
              LV_MENSAJE_TEC_SCP := 'ERROR AL CONSULTAR LOS PAGOS Y SALDOS  CUENTA: ' || Tb_Datos(I)
                                   .CUENTA || ' ERROR: ' || LV_ERROR_PAGOS;
              SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                        LV_MENSAJE_APL_SCP,
                                                        LV_MENSAJE_TEC_SCP,
                                                        NULL,
                                                        0,
                                                        0,
                                                        Tb_Datos(I).CUENTA,
                                                        NULL,
                                                        0,
                                                        0,
                                                        'N',
                                                        LN_ID_DETALLE,
                                                        LN_ERROR_SCP,
                                                        LV_MENSAJE);
            END IF;
          
          ELSIF LV_ERROR_CLIENTE IS NOT NULL THEN
            GN_BITACORA_ERROR  := GN_BITACORA_ERROR + 1;
            LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_TIPO_CLIENTE';
            LV_MENSAJE_TEC_SCP := 'ERROR AL CONSULTAR EL TIPO DE CLIENTE  CUENTA: ' || Tb_Datos(I)
                                 .CUENTA || ' ERROR: ' || LV_ERROR_CLIENTE;
            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                      LV_MENSAJE_APL_SCP,
                                                      LV_MENSAJE_TEC_SCP,
                                                      NULL,
                                                      0,
                                                      0,
                                                      NULL,
                                                      Tb_Datos(I).CUENTA,
                                                      0,
                                                      0,
                                                      'N',
                                                      LN_ID_DETALLE,
                                                      LN_ERROR_SCP,
                                                      LV_MENSAJE);
          END IF;
        END IF;
      END LOOP;
    
      FORALL J IN Lr_TAB_OPE_RECAUD.FIRST .. Lr_TAB_OPE_RECAUD.LAST
        INSERT INTO RC_OPE_RECAUDA
        VALUES
          (Lr_TAB_OPE_RECAUD(J).CUENTA,
           Lr_TAB_OPE_RECAUD(J).ID_CLIENTE,
           Lr_TAB_OPE_RECAUD(J).PRODUCTO,
           Lr_TAB_OPE_RECAUD(J).CANTON,
           Lr_TAB_OPE_RECAUD(J).PROVINCIA,
           Lr_TAB_OPE_RECAUD(J).APELLIDOS,
           Lr_TAB_OPE_RECAUD(J).NOMBRES,
           Lr_TAB_OPE_RECAUD(J).RUC,
           Lr_TAB_OPE_RECAUD(J).ID_FORMA_PAGO,
           Lr_TAB_OPE_RECAUD(J).FORMA_PAGO,
           Lr_TAB_OPE_RECAUD(J).TIPO_CLIENTE,
           Lr_TAB_OPE_RECAUD(J).FECH_APER_CUENTA,
           Lr_TAB_OPE_RECAUD(J).TELEFONO1,
           Lr_TAB_OPE_RECAUD(J).TELEFONO2,
           Lr_TAB_OPE_RECAUD(J).TELEFONO,
           Lr_TAB_OPE_RECAUD(J).DIRECCION,
           Lr_TAB_OPE_RECAUD(J).DIRECCION2,
           Lr_TAB_OPE_RECAUD(J).ID_SOLICITUD,
           Lr_TAB_OPE_RECAUD(J).LINEAS_ACT,
           Lr_TAB_OPE_RECAUD(J).LINEAS_INAC,
           Lr_TAB_OPE_RECAUD(J).LINEAS_SUSP,
           Lr_TAB_OPE_RECAUD(J).COD_VENDEDOR,
           Lr_TAB_OPE_RECAUD(J).VENDEDOR,
           Lr_TAB_OPE_RECAUD(J).NUM_FACTURA,
           Lr_TAB_OPE_RECAUD(J).BALANCE_1,
           Lr_TAB_OPE_RECAUD(J).BALANCE_2,
           Lr_TAB_OPE_RECAUD(J).BALANCE_3,
           Lr_TAB_OPE_RECAUD(J).BALANCE_4,
           Lr_TAB_OPE_RECAUD(J).BALANCE_5,
           Lr_TAB_OPE_RECAUD(J).BALANCE_6,
           Lr_TAB_OPE_RECAUD(J).BALANCE_7,
           Lr_TAB_OPE_RECAUD(J).BALANCE_8,
           Lr_TAB_OPE_RECAUD(J).BALANCE_9,
           Lr_TAB_OPE_RECAUD(J).BALANCE_10,
           Lr_TAB_OPE_RECAUD(J).BALANCE_11,
           Lr_TAB_OPE_RECAUD(J).BALANCE_12,
           Lr_TAB_OPE_RECAUD(J).TOTALVENCIDA,
           Lr_TAB_OPE_RECAUD(J).TOTAL_DEUDA,
           Lr_TAB_OPE_RECAUD(J).SALDO_PENDIENTE,
           Lr_TAB_OPE_RECAUD(J).MAYORVENCIDO,
           Lr_TAB_OPE_RECAUD(J).COMPANIA,
           Lr_TAB_OPE_RECAUD(J).FECH_MAX_PAGO,
           Lr_TAB_OPE_RECAUD(J).ID_PLAN,
           Lr_TAB_OPE_RECAUD(J).DETALLE_PLAN,
           Lr_TAB_OPE_RECAUD(J).DESC_APROBACION,
           Lr_TAB_OPE_RECAUD(J).ESTADO,
           Lr_TAB_OPE_RECAUD(J).FECHA_CORTE,
           PN_HILO,
           Lr_TAB_OPE_RECAUD(J).FINANCIAMIENTO,
           Lr_TAB_OPE_RECAUD(J).RECAUDADOR,
           Lr_TAB_OPE_RECAUD(J).FECHA_ASIGNACION,
           Lr_TAB_OPE_RECAUD(J).FECHA_FIN_CICLO,
           Lr_TAB_OPE_RECAUD(J).PAGOS,
           Lr_TAB_OPE_RECAUD(J).USUARIO_INGRESO,
           Lr_TAB_OPE_RECAUD(J).FECHA_INGRESO,
           SYSDATE);
    
      COMMIT;
    
      Lr_TAB_OPE_RECAUD.DELETE;
      Ln_Count := 0;
    
      EXIT WHEN OBTIENE_BASE_CLIENTES%NOTFOUND;
    END LOOP;
    CLOSE OBTIENE_BASE_CLIENTES;
    BEGIN
      SELECT ADD_MONTHS(TO_DATE(LV_FECHACORTE, 'DD/MM/RRRR'), -3) INTO LD_FECHACORTE_ANT
        FROM DUAL;
      DELETE FROM RC_OPE_RECAUDA A
       WHERE A.FECHA_CORTE = LD_FECHACORTE_ANT
         AND A.ESTADO = 'T'
         AND A.HILO=Pn_Hilo;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    BEGIN
      SELECT ADD_MONTHS(TO_DATE(LV_FECHACORTE, 'DD/MM/RRRR'), -12) INTO LD_FECHACORTE_ANT
        FROM DUAL;
      DELETE FROM RC_OPE_RECAUDA A
       WHERE A.FECHA_CORTE = LD_FECHACORTE_ANT
         AND A.ESTADO = 'C'
         AND A.HILO=Pn_Hilo;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    COMMIT;
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                              GN_BITACORA_PROCESADOS,
                                              GN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
  EXCEPTION
    WHEN LE_ERROR_TABLA THEN
      PV_ERROR := 'Error: LA TABLA CO_REPCARCLI_' || LV_FECHA ||
                  ' NO EXISTE';
    WHEN LE_ERROR_EXITE2 THEN
      PV_ERROR := 'Error: YA SE ENCUENTRA CARGADA LA TABLA RC_OPE_RECAUDA CON FECHA DE CORTE DEL ' ||
                  Lv_FechaCorte;
    WHEN Le_Error THEN
      Pv_Error := 'OP_RECAUDADOR_DAS.CARGA Error: ' || Pv_Error;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
      ROLLBACK;
      GN_BITACORA_ERROR  := GN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.CARGA';
      LV_MENSAJE_TEC_SCP := 'PROCESO ABORTADO MANUALMENTE: ';
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ABORTADO_MANUALMENTE',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                                GN_BITACORA_PROCESADOS,
                                                GN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    WHEN OTHERS THEN
      GN_BITACORA_ERROR := GN_BITACORA_ERROR + 1;
      Pv_Error          := 'OP_RECAUDADOR_DAS.CARGA Error: ' || SQLERRM;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
      ROLLBACK;
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.CARGA';
      LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. REVISAR LAS CUENTAS CORRESPONDIENTE A ESTE HILO EN LA TABLA CO_REPCARCLI_' ||
                            LV_FECHA || ' ' || SUBSTR(SQLERRM, 1, 100);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ERROR_PRINCIPAL',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(GN_ID_BITACORA_SCP,
                                                GN_BITACORA_PROCESADOS,
                                                GN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(GN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
  END CARGA;

  PROCEDURE P_CREACION_INDEX(PV_FECHA VARCHAR2, PV_ERROR OUT VARCHAR2) IS
  
    Lv_IndexRepCarCli  VARCHAR2(1000);
    Lv_IndexReportAdic VARCHAR2(1000);
    LV_MENSAJE_APL_SCP VARCHAR2(1000);
    LV_MENSAJE_TEC_SCP VARCHAR2(1000);
    LN_ERROR_SCP       NUMBER := 0;
    LV_MENSAJE         VARCHAR2(500);
    LN_ID_DETALLE      NUMBER := 0;
  
  BEGIN
  
    Lv_IndexRepCarCli  := 'CREATE INDEX SYSADM.IDX_CARCLI1_<FECHA>_3 ON SYSADM.CO_REPCARCLI_<FECHA> (RUC)';
    Lv_IndexReportAdic := 'CREATE INDEX SYSADM.IDX_ADICCLIENT<FECHA>_3 ON SYSADM.CO_REPORTE_ADIC_<FECHA> (ID_PLAN)';
  
    BEGIN
    
      Lv_IndexRepCarCli := REPLACE(Lv_IndexRepCarCli, '<FECHA>', PV_FECHA);
      EXECUTE IMMEDIATE Lv_IndexRepCarCli;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR           := PV_ERROR || ' ' || SUBSTR(SQLERRM, 1, 100);
        LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_CREACION_INDEX';
        LV_MENSAJE_TEC_SCP := 'ERROR AL CREACER EL INDEX: ' ||
                              Lv_IndexRepCarCli || ' ' ||
                              SUBSTR(SQLERRM, 1, 100);
      
        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  NULL,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
    END;
  
    BEGIN
    
      Lv_IndexReportAdic := REPLACE(Lv_IndexReportAdic, '<FECHA>', PV_FECHA);
      EXECUTE IMMEDIATE Lv_IndexReportAdic;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR           := PV_ERROR || ' ' || SUBSTR(SQLERRM, 1, 100);
        LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_CREACION_INDEX';
        LV_MENSAJE_TEC_SCP := 'ERROR AL CREACER EL INDEX: ' ||
                              Lv_IndexReportAdic || ' ' ||
                              SUBSTR(SQLERRM, 1, 100);
      
        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  NULL,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
    END;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR           := PV_ERROR || ' ' || SUBSTR(SQLERRM, 1, 100);
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_CREACION_INDEX';
      LV_MENSAJE_TEC_SCP := 'ERROR EN LA CREACION DE INDICES: ' ||
                            SUBSTR(SQLERRM, 1, 100);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                NULL,
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
  END P_CREACION_INDEX;

  PROCEDURE P_TIPO_CLIENTE(PV_CUENTA      VARCHAR2,
                           PV_TIPOCLIENTE OUT VARCHAR2,
                           PV_ERROR       OUT VARCHAR2) IS
  
    CURSOR C_TIPO_CREDITO(Cv_Cuenta VARCHAR2) IS
      SELECT C.CSTRADECODE
        FROM CUSTOMER_ALL C
       WHERE C.CUSTCODE = Cv_Cuenta;
  
    CURSOR C_TIPO_CLIENTE(Cv_Cuenta VARCHAR2) IS
      SELECT C.TIPO, C.PORCENTAJE, F.DESCRIPCION FORMA_PAGOS
        FROM CL_PERSONAS_EMPRESA@AXIS        A,
             CL_CONTRATOS@AXIS               B,
             PORTA.RC_PORCENTAJES_DEUDA@AXIS C,
             GE_FORMA_PAGOS@AXIS             F
       WHERE B.CODIGO_DOC = Cv_Cuenta
         AND B.ID_PERSONA = A.ID_PERSONA
         AND B.ID_FORMA_PAGO = F.ID_FORMA_PAGO
         AND C.ID_CLASE_CLIENTE =
             NVL((SELECT X.ID_CLASE_CLIENTE
                   FROM PORTA.RC_PORCENTAJES_DEUDA@AXIS X
                  WHERE X.ID_CLASE_CLIENTE = A.ID_CLASE_CLIENTE
                    AND X.TIPO <> 'I'),
                 'GEN')
         AND C.TIPO <> 'I';
  
    Lv_CsTradecode VARCHAR2(10);
    Lr_TipoCliente C_TIPO_CLIENTE%ROWTYPE;
  
  BEGIN
  
    OPEN C_TIPO_CREDITO(PV_CUENTA);
    FETCH C_TIPO_CREDITO
      INTO Lv_CsTradecode;
    CLOSE C_TIPO_CREDITO;
  
    OPEN C_TIPO_CLIENTE(PV_CUENTA);
    FETCH C_TIPO_CLIENTE
      INTO Lr_TipoCliente;
    CLOSE C_TIPO_CLIENTE;
  
    IF Lv_CsTradecode = 18 AND Lr_TipoCliente.TIPO <> 'V' THEN
      PV_TIPOCLIENTE := 'INMEDIATO';
    ELSE
      IF Lr_TipoCliente.TIPO = 'V' THEN
        IF Lr_TipoCliente.PORCENTAJE = 100 THEN
          PV_TIPOCLIENTE := 'INSIGNIA/INSI_REL';
        ELSE
          PV_TIPOCLIENTE := 'VIP';
        END IF;
      ELSIF Lr_TipoCliente.TIPO = 'C' THEN
        PV_TIPOCLIENTE := 'COORPORATIVOS/PYMENS';
      ELSE
        PV_TIPOCLIENTE := 'NORMAL';
      END IF;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := PV_ERROR || ' ' || SUBSTR(SQLERRM, 1, 100);
  END P_TIPO_CLIENTE;
  --=====================================================================================--
  -- CREADO POR:     Jordan Rodriguez.
  -- FECHA MOD:      14/10/2016
  -- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Antonio Mayorga
  -- MOTIVO:         Recaudador DAS - Recuperacion de cartera, procedimiento encargado de 
  --                 verificar pagos y creditos de una cuenta
  --=====================================================================================--
  PROCEDURE P_PAGOS_CREDITOS(PN_CUSTOMER_ID  NUMBER,
                             PD_FECHACORTE   DATE,
                             PN_DEUDA        NUMBER DEFAULT NULL,
                             PV_VERI_CREDITO VARCHAR2,
                             PN_SALDO        OUT NUMBER,
                             PN_PAGO         OUT NUMBER,
                             PN_CREDITO      OUT NUMBER,
                             PV_ERROR        OUT VARCHAR2) IS
  
    CURSOR C_PAGOS_CLIENTE(Cn_CustomerId NUMBER, Cd_FechaCorte DATE) IS
      SELECT SUM(C.CACURAMT_PAY)
        FROM CASHRECEIPTS_ALL C
       WHERE C.CAENTDATE >= CD_FECHACORTE
         AND C.CAENTDATE <= TRUNC(SYSDATE)
         AND C.CUSTOMER_ID = CN_CUSTOMERID;
  
    CURSOR C_CREDITOS(Cn_CustomerId NUMBER) IS
      SELECT /*+ RULE +*/
       NVL(SUM(AMOUNT), 0) CREDITOS
        FROM FEES B
       WHERE B.CUSTOMER_ID = Cn_CustomerId
         AND B.PERIOD > 0
         AND B.SNCODE = 46;
  
    Ln_Creditos NUMBER := NULL;
    Ln_Pagos    NUMBER;
  
  BEGIN
  
    OPEN C_PAGOS_CLIENTE(PN_CUSTOMER_ID, PD_FECHACORTE);
    FETCH C_PAGOS_CLIENTE
      INTO Ln_Pagos;
    CLOSE C_PAGOS_CLIENTE;
    IF PV_VERI_CREDITO = 'S' THEN
      OPEN C_CREDITOS(PN_CUSTOMER_ID);
      FETCH C_CREDITOS
        INTO Ln_Creditos;
      CLOSE C_CREDITOS;
    END IF;
  
    PN_CREDITO := NVL(Ln_Creditos, 0);
    PN_PAGO    := NVL(Ln_Pagos, 0);
    PN_SALDO   := NVL(PN_DEUDA, 0) - PN_PAGO;
    PN_SALDO   := PN_SALDO + PN_CREDITO;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_CREDITO := 0;
      PN_PAGO    := 0;
      PV_ERROR   := PV_ERROR || ' ' || SUBSTR(SQLERRM, 1, 100);
  END P_PAGOS_CREDITOS;

  PROCEDURE P_CONFIRMAR_PROCESO(PV_PROCESO        VARCHAR2,
                                PV_NOMBRE_ARCHIVO VARCHAR2,
                                PV_ERROR          OUT VARCHAR2) IS
  
    CURSOR C_DETALLE_BITACORA(CN_ID_BITACORA NUMBER) IS
      SELECT A.COD_AUX_2, A.COD_AUX_3, A.MENSAJE_TECNICO
        FROM SCP_DAT.SCP_DETALLES_BITACORA A
       WHERE A.ID_BITACORA = CN_ID_BITACORA
         AND A.FECHA_EVENTO BETWEEN TRUNC(SYSDATE) AND SYSDATE;
  
    CURSOR C_BITACORA(CV_ID_PROCESO VARCHAR2) IS
      SELECT A.ID_BITACORA, A.REGISTROS_ERROR
        FROM SCP_DAT.SCP_BITACORA_PROCESOS A
       WHERE A.FECHA_ACTUALIZACION BETWEEN TRUNC(SYSDATE) AND SYSDATE
         AND A.ID_PROCESO = CV_ID_PROCESO
         AND A.REGISTROS_ERROR > 0;
  
    TYPE R_CUENTA IS RECORD(
      CUENTA          VARCHAR2(30),
      ERROR           VARCHAR2(50),
      MENSAJE_TECNICO VARCHAR2(3000));
  
    TYPE T_CUENTA IS TABLE OF R_CUENTA INDEX BY BINARY_INTEGER;
  
    LT_CUENTA T_CUENTA;
  
    LN_COUNT      NUMBER := 0;
    LF_OUTPUT     UTL_FILE.FILE_TYPE;
    LV_DIRECTORIO VARCHAR2(50);
    LN_ERROR      NUMBER;
    LV_USUFINAN27 VARCHAR2(50);
    LV_IPFINAN27  VARCHAR2(50);
    LV_RUTAREMOTA VARCHAR2(1000);
    LV_ERRORFTP   VARCHAR2(1000);
    LV_MENSAJE    VARCHAR2(4000) := NULL;
    LV_ASUNTO     VARCHAR2(100);
    Le_Error EXCEPTION;
  
  BEGIN
  
    IF PV_PROCESO = 'OP_RECAUDADOR_DAS.CARGA' THEN
    
      LV_ASUNTO    := 'RELOJ DE COBRANZAS - INFORME DEL PROCESO DE RECAUDADOR DAS';
      LV_IPFINAN27 := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,
                                                                    'REP_IP_FIN',
                                                                    PV_ERROR);
    
      IF PV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    
      LV_USUFINAN27 := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,
                                                                     'REP_USU_FIN',
                                                                     PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    
      LV_RUTAREMOTA := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,
                                                                     'REP_RUT_REPO',
                                                                     PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    
      LV_DIRECTORIO := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695,
                                                                     'REP_NOM_DIRE',
                                                                     PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    
      FOR J IN C_BITACORA('OP_BITACORA_DAS') LOOP
      
        OPEN C_DETALLE_BITACORA(J.ID_BITACORA);
        FETCH C_DETALLE_BITACORA BULK COLLECT
          INTO LT_CUENTA;
        CLOSE C_DETALLE_BITACORA;
      
        IF LT_CUENTA.COUNT > 0 THEN
        
          FOR I IN LT_CUENTA.FIRST .. LT_CUENTA.LAST LOOP
          
            LN_COUNT := LN_COUNT + 1;
            IF LN_COUNT = 1 THEN
              LF_OUTPUT := UTL_FILE.FOPEN(LV_DIRECTORIO,
                                          PV_NOMBRE_ARCHIVO,
                                          'W');
              UTL_FILE.PUT_LINE(LF_OUTPUT, 'CUENTA' || CHR(9) || 'ERROR');
            END IF;
            IF LT_CUENTA(I).ERROR = 'ERROR_PRINCIPAL' OR LT_CUENTA(I)
               .ERROR = 'ABORTADO_MANUALMENTE' THEN
              UTL_FILE.PUT_LINE(LF_OUTPUT,
                                LT_CUENTA(I).ERROR || CHR(9) || LT_CUENTA(I)
                                .MENSAJE_TECNICO);
            ELSIF LT_CUENTA(I).CUENTA IS NOT NULL THEN
              UTL_FILE.PUT_LINE(LF_OUTPUT,
                                LT_CUENTA(I).CUENTA || CHR(9) || LT_CUENTA(I)
                                .MENSAJE_TECNICO);
            END IF;
          END LOOP;
        END IF;
      
      END LOOP;
    
      IF LN_COUNT > 0 THEN
        UTL_FILE.FCLOSE(LF_OUTPUT);
        LV_MENSAJE := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                                    'OPE_MENSAJE_GENERICO_ERROR',
                                                                    PV_ERROR);
        SCP_DAT.SCK_FTP_GTW.SCP_TRANSFIERE_ARCHIVO(PV_IP                => LV_IPFINAN27,
                                                   PV_USUARIO           => LV_USUFINAN27,
                                                   PV_RUTA_REMOTA       => LV_RUTAREMOTA,
                                                   PV_NOMBRE_ARCH       => PV_NOMBRE_ARCHIVO,
                                                   PV_RUTA_LOCAL        => LV_DIRECTORIO,
                                                   PV_BORRAR_ARCH_LOCAL => 'N',
                                                   PN_ERROR             => LN_ERROR,
                                                   PV_ERROR             => LV_ERRORFTP);
      
        IF Lv_ErrorFtp IS NULL THEN
          Lv_RutaRemota := '</ARCHIVO1=' || PV_NOMBRE_ARCHIVO ||
                           '|DIRECTORIO1=' || Lv_RutaRemota || '|/>';
        ELSE
          Lv_RutaRemota := NULL;
        END IF;
        sysadm.RC_TRX_UTILITIES.RC_NOTIFICACIONES(PN_ID_PROCESO       => 0,
                                                  PV_JOB              => NULL,
                                                  PV_SHELL            => 'RC_RECAUDADOR_DAS.SH',
                                                  PV_OBJETO           => 'OP_RECAUDADOR_DAS.CARGA',
                                                  PV_MENSAJE_NOTIF    => LV_MENSAJE,
                                                  PV_ASUNTO           => LV_ASUNTO,
                                                  PV_RUTAREMOTA       => LV_RUTAREMOTA,
                                                  PV_IDDESTINATARIO   => 'NRD',
                                                  PV_TipoNotificacion => 'ERROR',
                                                  PN_ERROR            => LN_ERROR,
                                                  PV_ERROR            => PV_ERROR);
      
      ELSE
        LV_MENSAJE := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10995,
                                                                    'OPE_MENSAJE_GENERICO_OK',
                                                                    PV_ERROR);
        sysadm.RC_TRX_UTILITIES.RC_NOTIFICACIONES(PN_ID_PROCESO       => 0,
                                                  PV_JOB              => NULL,
                                                  PV_SHELL            => 'RC_RECAUDADOR_DAS.SH',
                                                  PV_OBJETO           => 'OP_RECAUDADOR_DAS.CARGA',
                                                  PV_MENSAJE_NOTIF    => LV_MENSAJE,
                                                  PV_ASUNTO           => LV_ASUNTO,
                                                  PV_RUTAREMOTA       => NULL,
                                                  PV_IDDESTINATARIO   => 'NRD',
                                                  PV_TipoNotificacion => 'OK',
                                                  PN_ERROR            => LN_ERROR,
                                                  PV_ERROR            => PV_ERROR);
      END IF;
    END IF;
  
  EXCEPTION
    WHEN Le_Error THEN
      Pv_Error := 'ERROR: P_CONFIRMAR_PROCESO => ' || Pv_Error;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
      ROLLBACK;
    WHEN OTHERS THEN
      Pv_Error := 'ERROR: P_CONFIRMAR_PROCESO => ' ||
                  SUBSTR(SQLERRM, 1, 300);
  END P_CONFIRMAR_PROCESO;

  PROCEDURE P_ACTUALIZA_PAGOS(PN_HILO NUMBER, PV_ERROR OUT VARCHAR2) IS
    --=====================================================================================--
    -- CREADO POR:     Jordan Rodriguez.
    -- FECHA MOD:      14/10/2016
    -- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
    -- LIDER IRO:      IRO Juan Romero
    -- LIDER :         SIS Antonio Mayorga
    -- MOTIVO:         Recaudador DAS - Recuperacion de cartera, procedimiento encargado de 
    --                 verificar pagos diarios de las cuentas que se encuentran en la tabla
    --                 RC_OPE_RECAUDA.
    --=====================================================================================--
  
    LN_SALDO_PENDIENTE NUMBER;
    LN_PAGOS           NUMBER;
    LN_CREDITOS        NUMBER;
    LN_VALORMINDEUD    NUMBER;
    LV_ERROR_PAGOS     VARCHAR2(100);
    LE_ERROR EXCEPTION;
    LN_UPDATE    NUMBER := 0;
    LN_DELETE    NUMBER := 0;
    LV_SQL_CARGA VARCHAR2(300);
  
    TYPE C_OBTIENE_PAGOS IS REF CURSOR;
    OBTIENE_PAGOS C_OBTIENE_PAGOS;
  
    TYPE TY_PAGOS_BASE IS RECORD(
      T_CUENTA          VARCHAR2(30),
      T_ID_CLIENTE      NUMBER,
      T_FECHA_CORTE     DATE,
      T_FECHA_INGRESO   DATE,
      T_FECHA_ACTUALIZA DATE,
      T_TOTAL_DEUDA     NUMBER,
      T_SALDO_PENDIENTE NUMBER);
  
    TYPE TY_PAGOS_DELETE_UPDATE IS RECORD(
      T_CUENTA      VARCHAR2(30),
      T_SALDO_ACT   NUMBER,
      T_FECHA_CORTE DATE);
  
    TYPE T_PAGOS_BASE IS TABLE OF TY_PAGOS_BASE INDEX BY BINARY_INTEGER;
    TYPE T_PAGOS_DELETE_UPDATE IS TABLE OF TY_PAGOS_DELETE_UPDATE INDEX BY BINARY_INTEGER;
    LT_PAGOS_BASE       T_PAGOS_BASE;
    LT_PAGOS_UPDATE     T_PAGOS_DELETE_UPDATE;
    LT_PAGOS_DELETE     T_PAGOS_DELETE_UPDATE;
    LV_CUENTA_PROCESADA VARCHAR2(30) := 'CUENTA';
  
  BEGIN
    LN_VALORMINDEUD := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                     PV_IDPARAMETRO     => 'OPE_VAL_DEUD',
                                                                     PV_ERROR           => PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    LV_SQL_CARGA := 'SELECT A.CUENTA, A.ID_CLIENTE, A.FECHA_CORTE, A.FECHA_INGRESO, A.FECHA_ACTUALIZACION, A.TOTAL_DEUDA,
                       A.SALDO_PENDIENTE FROM RC_OPE_RECAUDA A
                       WHERE A.HILO =' || PN_HILO || '
                       AND A.ESTADO = ''I''
                       AND A.FECHA_ACTUALIZACION BETWEEN
                       TO_DATE(TO_CHAR(SYSDATE, ''DD/MM/RRRR'') || '' 00:00:00'', ''DD/MM/RRRR HH24:MI:SS'') - 390 AND
                       TO_DATE(TO_CHAR(SYSDATE, ''DD/MM/RRRR'') || '' 23:59:59'', ''DD/MM/RRRR HH24:MI:SS'') - 1
                       ORDER BY CUENTA,FECHA_CORTE ASC';
    OPEN OBTIENE_PAGOS FOR LV_SQL_CARGA;
    LOOP
      FETCH OBTIENE_PAGOS BULK COLLECT
        INTO LT_PAGOS_BASE LIMIT 1000;
      EXIT WHEN LT_PAGOS_BASE.COUNT = 0;
    
      FOR I IN LT_PAGOS_BASE.FIRST .. LT_PAGOS_BASE.LAST LOOP
        IF LV_CUENTA_PROCESADA <> LT_PAGOS_BASE(I).T_CUENTA THEN
          P_PAGOS_CREDITOS(PN_CUSTOMER_ID  => LT_PAGOS_BASE(I).T_ID_CLIENTE,
                           PD_FECHACORTE   => NVL(to_date(to_char(LT_PAGOS_BASE(I).T_FECHA_ACTUALIZA,'DD/MM/RRRR'),'dd/mm/rrrr'),
                                                  to_date(to_char(LT_PAGOS_BASE(I).T_FECHA_INGRESO,'DD/MM/RRRR'),'dd/mm/rrrr')),
                           PN_DEUDA        => LT_PAGOS_BASE(I).T_SALDO_PENDIENTE,
                           PV_VERI_CREDITO => 'N',
                           PN_SALDO        => LN_SALDO_PENDIENTE,
                           PN_PAGO         => LN_PAGOS,
                           PN_CREDITO      => LN_CREDITOS,
                           PV_ERROR        => LV_ERROR_PAGOS);
        END IF;
        IF LN_SALDO_PENDIENTE > LN_VALORMINDEUD AND LV_ERROR_PAGOS IS NULL THEN
          IF LN_SALDO_PENDIENTE <> LT_PAGOS_BASE(I).T_SALDO_PENDIENTE THEN
            LN_UPDATE := LN_UPDATE + 1;
            LT_PAGOS_UPDATE(LN_UPDATE).T_SALDO_ACT := LN_SALDO_PENDIENTE;
            LT_PAGOS_UPDATE(LN_UPDATE).T_FECHA_CORTE := LT_PAGOS_BASE(I)
                                                        .T_FECHA_CORTE;
            LT_PAGOS_UPDATE(LN_UPDATE).T_CUENTA := LT_PAGOS_BASE(I).T_CUENTA;
          END IF;
        ELSIF LN_SALDO_PENDIENTE <= LN_VALORMINDEUD AND
              LV_ERROR_PAGOS IS NULL THEN
          LN_DELETE := LN_DELETE + 1;
          LT_PAGOS_DELETE(LN_DELETE).T_SALDO_ACT := LT_PAGOS_BASE(I)
                                                    .T_SALDO_PENDIENTE;
          LT_PAGOS_DELETE(LN_DELETE).T_FECHA_CORTE := LT_PAGOS_BASE(I)
                                                      .T_FECHA_CORTE;
          LT_PAGOS_DELETE(LN_DELETE).T_CUENTA := LT_PAGOS_BASE(I).T_CUENTA;
        END IF;
        LV_CUENTA_PROCESADA := LT_PAGOS_BASE(I).T_CUENTA;
      END LOOP;
      IF LT_PAGOS_UPDATE.COUNT > 0 THEN
        FORALL I IN LT_PAGOS_UPDATE.FIRST .. LT_PAGOS_UPDATE.LAST
          UPDATE RC_OPE_RECAUDA A
             SET A.SALDO_PENDIENTE     = LT_PAGOS_UPDATE(I).T_SALDO_ACT,
                 A.FECHA_ACTUALIZACION = SYSDATE
           WHERE A.FECHA_CORTE = LT_PAGOS_UPDATE(I).T_FECHA_CORTE
             AND A.CUENTA = LT_PAGOS_UPDATE(I).T_CUENTA
             AND A.HILO = PN_HILO
             AND A.ESTADO = 'I';
      END IF;
    
      IF LT_PAGOS_DELETE.COUNT > 0 THEN
        FORALL J IN LT_PAGOS_DELETE.FIRST .. LT_PAGOS_DELETE.LAST
          DELETE FROM RC_OPE_RECAUDA A
           WHERE A.FECHA_CORTE = LT_PAGOS_DELETE(J).T_FECHA_CORTE
             AND A.CUENTA = LT_PAGOS_DELETE(J).T_CUENTA
             AND A.HILO = PN_HILO
             AND A.ESTADO = 'I';
      END IF;
      COMMIT;
      LN_UPDATE := 0;
      LN_DELETE := 0;
      LT_PAGOS_DELETE.DELETE;
      LT_PAGOS_UPDATE.DELETE;
    
    END LOOP;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'ERROR: P_ACTUALIZA_PAGOS => ' || SUBSTR(SQLERRM, 1, 300);
      ROLLBACK;
  END P_ACTUALIZA_PAGOS;

  PROCEDURE P_FINANCIAMIENTO(PV_CUENTA        VARCHAR2,
                             PV_FINANCIMIENTO OUT VARCHAR2,
                             PV_FECHA_CORTE   VARCHAR2,
                             Pv_Error         OUT VARCHAR2) IS
  
    CURSOR C_FINANCIAMIENTO(Cv_Cuenta VARCHAR2, Cv_Fecha_Corte VARCHAR2) IS
      SELECT COUNT(*)
        FROM FINANC_NOTIF_SMS_IVR_GES C
       WHERE C.CUENTA = Cv_Cuenta
         AND FECHA_CORTE = TO_DATE(Cv_Fecha_Corte, 'DD/MM/RRRR');
  
    Ln_Financiamiento NUMBER;
  
  BEGIN
  
    OPEN C_FINANCIAMIENTO(PV_CUENTA, PV_FECHA_CORTE);
    FETCH C_FINANCIAMIENTO
      INTO Ln_Financiamiento;
    CLOSE C_FINANCIAMIENTO;
  
    IF Ln_Financiamiento > 0 THEN
      PV_FINANCIMIENTO := 'SI';
    ELSE
      PV_FINANCIMIENTO := 'NO';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'ERROR: P_FINANCIAMIENTO => ' || SUBSTR(SQLERRM, 1, 300);
  END P_FINANCIAMIENTO;

  PROCEDURE P_ACT_PAG_RECAUDADOR(Pn_Hilo NUMBER, Pv_Error OUT VARCHAR2) IS
  
    CURSOR C_CORTES_PROCESAR(Cn_Hilo NUMBER) IS
      SELECT R.*
        FROM RC_OPE_RECAUDA R
       WHERE R.FECHA_FIN_CICLO >= SYSDATE - 1
         AND R.HILO = Cn_Hilo
         AND R.ESTADO = 'A'
         AND R.FECHA_ACTUALIZACION BETWEEN 
         TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') - 390 AND
         TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS') - 1;
  
    CURSOR C_PAGOS(Cn_CustomerId number,
                   Cd_FechaAsig  DATE,
                   Cd_FechaFin   DATE) IS
      SELECT B.CUSTOMER_ID, SUM(A.CADAMT_GL) VALOR
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE B.CUSTOMER_ID = Cn_CustomerId
         AND B.CAENTDATE BETWEEN
             TO_DATE(TO_CHAR(Cd_FechaAsig, 'DD/MM/RRRR') || ' 00:00:00',
                     'DD/MM/RRRR HH24:MI:SS') AND
             TO_DATE(TO_CHAR(Cd_FechaFin, 'DD/MM/RRRR') || ' 23:59:59',
                     'DD/MM/RRRR HH24:MI:SS')
         AND A.CADXACT = B.CAXACT
       GROUP BY B.CUSTOMER_ID;
  
    TYPE TYPE_PAGOS_RECAUDADOR IS RECORD(
      CUENTA                    VARCHAR2(30),
      SALDO_PENDIENTE           NUMBER,
      PAGOS_CLIENTES_RECAUDADOR NUMBER);
  
    TYPE T_PAGOS_RECAUDADOR IS TABLE OF TYPE_PAGOS_RECAUDADOR INDEX BY BINARY_INTEGER;
    Lt_PagosRecaudador T_PAGOS_RECAUDADOR;
  
    TYPE TAB_OPE_RECAUD IS TABLE OF RC_OPE_RECAUDA%ROWTYPE INDEX BY BINARY_INTEGER;
    Lr_TAB_OPE_RECAUD TAB_OPE_RECAUD;
  
    Lr_Pagos      C_PAGOS%ROWTYPE;
    Ln_Count      NUMBER := 0;
    Ln_Diferencia NUMBER;
  
  BEGIN
  
    OPEN C_CORTES_PROCESAR(Pn_Hilo);
    LOOP
      FETCH C_CORTES_PROCESAR BULK COLLECT
        INTO Lr_TAB_OPE_RECAUD LIMIT 1000;
      EXIT WHEN Lr_TAB_OPE_RECAUD.COUNT = 0;
    
      FOR I IN Lr_TAB_OPE_RECAUD.FIRST .. Lr_TAB_OPE_RECAUD.LAST LOOP
        Lr_Pagos := null;
        OPEN C_PAGOS(Lr_TAB_OPE_RECAUD(I).ID_CLIENTE,
                     Lr_TAB_OPE_RECAUD(I).FECHA_ASIGNACION,
                     Lr_TAB_OPE_RECAUD(I).FECHA_FIN_CICLO);
        FETCH C_PAGOS
          INTO Lr_Pagos;
        CLOSE C_PAGOS;
      
        IF Lr_Pagos.CUSTOMER_ID IS NOT NULL THEN
        
          IF Lr_TAB_OPE_RECAUD(I).PAGOS IS NULL THEN
            Ln_Count := Ln_Count + 1;
          
            Lt_PagosRecaudador(Ln_Count).CUENTA := Lr_TAB_OPE_RECAUD(I)
                                                   .CUENTA;
            Lt_PagosRecaudador(Ln_Count).SALDO_PENDIENTE := Lr_TAB_OPE_RECAUD(I)
                                                            .SALDO_PENDIENTE -
                                                             Lr_Pagos.VALOR;
            Lt_PagosRecaudador(Ln_Count).PAGOS_CLIENTES_RECAUDADOR := Lr_Pagos.VALOR;
          
          ELSIF Lr_Pagos.VALOR > Lr_TAB_OPE_RECAUD(I).PAGOS THEN
            Ln_Count      := Ln_Count + 1;
            Ln_Diferencia := Lr_Pagos.VALOR - Lr_TAB_OPE_RECAUD(I).PAGOS;
          
            Lt_PagosRecaudador(Ln_Count).CUENTA := Lr_TAB_OPE_RECAUD(I)
                                                   .CUENTA;
            Lt_PagosRecaudador(Ln_Count).SALDO_PENDIENTE := Lr_TAB_OPE_RECAUD(I)
                                                            .SALDO_PENDIENTE -
                                                             Ln_Diferencia;
            Lt_PagosRecaudador(Ln_Count).PAGOS_CLIENTES_RECAUDADOR := Lr_Pagos.VALOR;
          END IF;
        END IF;
      
      END LOOP;
    
      FORALL J IN Lt_PagosRecaudador.FIRST .. Lt_PagosRecaudador.LAST
        UPDATE RC_OPE_RECAUDA A
           SET A.SALDO_PENDIENTE     = Lt_PagosRecaudador(J).SALDO_PENDIENTE,
               A.PAGOS               = Lt_PagosRecaudador(J)
                                       .PAGOS_CLIENTES_RECAUDADOR,
               A.FECHA_ACTUALIZACION = SYSDATE
         WHERE A.CUENTA = Lt_PagosRecaudador(J).CUENTA;
    
      COMMIT;
    
      Lt_PagosRecaudador.DELETE;
      Ln_Count := 0;
    
      EXIT WHEN C_CORTES_PROCESAR%NOTFOUND;
    END LOOP;
    CLOSE C_CORTES_PROCESAR;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'ERROR: P_FINANCIAMIENTO => ' || SUBSTR(SQLERRM, 1, 300);
      ROLLBACK;
  END P_ACT_PAG_RECAUDADOR;

  PROCEDURE P_ACT_PERIODOS_TERMINADOS(Pv_Error OUT VARCHAR2) IS
  
    CURSOR C_CORTES_FECHAS_FIN_CICLO IS
      SELECT COUNT(*)
        FROM RC_OPE_RECAUDA T
       WHERE T.FECHA_FIN_CICLO <= SYSDATE - 3
         AND T.ESTADO = 'A';
  
    Ln_Existe NUMBER;
  
  BEGIN
  
    OPEN C_CORTES_FECHAS_FIN_CICLO;
    FETCH C_CORTES_FECHAS_FIN_CICLO
      INTO Ln_Existe;
    CLOSE C_CORTES_FECHAS_FIN_CICLO;
  
    IF Ln_Existe > 0 THEN
    
      UPDATE RC_OPE_RECAUDA T
         SET T.ESTADO = 'T', T.FECHA_ACTUALIZACION = SYSDATE
       WHERE T.FECHA_FIN_CICLO <= SYSDATE - 3
         AND T.ESTADO = 'A';
    
      COMMIT;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'ERROR: P_ACT_PERIDOS_TERMINADOS => ' ||
                  SUBSTR(SQLERRM, 1, 300);
  END P_ACT_PERIODOS_TERMINADOS;

  PROCEDURE P_CARGA_ARCHIVO(PN_INTERACION NUMBER,
                            PV_LINEA      VARCHAR2,
                            PV_SEPARADOR  VARCHAR2,
                            PV_ERROR      OUT VARCHAR2) IS
  
    Ln_contador   INT;
    Ln_inicio     INT;
    Ln_hasta      INT;
    Ln_inicio_a   INT;
    Ln_long_act   INT;
    ln_separado   number;
    Lv_valor      VARCHAR(60);
    Lv_cuenta     VARCHAR(20);
    Lv_recaudaror VARCHAR(20);
    Ld_fecha_asig DATE;
    LE_ERROR  EXCEPTION;
    LE_CADENA EXCEPTION;
    lv_sql varchar2(1000);
  BEGIN
    Ln_contador := 1;
    Ln_inicio_a := 0;
    Ln_long_act := 0;
  
    Ln_inicio := 1;
  
    Ln_hasta := instr(PV_LINEA, PV_SEPARADOR, Ln_inicio);
  
    WHILE Ln_contador <= 3 LOOP
    
      Ln_inicio_a := instr(PV_LINEA, PV_SEPARADOR, Ln_inicio);
      Ln_long_act := INSTR(PV_LINEA, PV_SEPARADOR, Ln_inicio_a + 1);
      IF Ln_long_act <> 0 THEN
      
        IF Ln_contador <> 1 THEN
          Ln_inicio := Ln_inicio_a + 1;
          Ln_hasta  := Ln_long_act - Ln_inicio_a;
        ELSE
          Ln_inicio := 1;
        END IF;
        Lv_valor := substr(PV_LINEA, Ln_inicio, Ln_hasta - 1);
      ELSE
      
        Ln_inicio := Ln_inicio_a + 1;
        Lv_valor  := substr(PV_LINEA, Ln_inicio);
      END IF;
    
      IF Ln_contador = 1 THEN
        Lv_cuenta := Lv_valor;
      ELSIF Ln_contador = 2 THEN
        Lv_recaudaror := Lv_valor;
      ELSIF Ln_contador = 3 THEN
        Ld_fecha_asig := TO_DATE(Lv_valor, 'DD/MM/YYYY');
      END IF;
    
      Ln_contador := Ln_contador + 1;
    END LOOP;
    IF PN_INTERACION = 1 THEN
      lv_sql := 'SELECT length(''<CADENA>'') - length(replace(''<CADENA>'',''<SEPARADOR>'')) from dual';
      lv_sql := REPLACE(lv_sql, '<CADENA>', PV_LINEA);
      lv_sql := REPLACE(lv_sql, '<SEPARADOR>', PV_SEPARADOR);
      EXECUTE IMMEDIATE lv_sql
        INTO ln_separado;
      IF LV_CUENTA IS NULL OR LV_RECAUDAROR IS NULL OR
         LD_FECHA_ASIG IS NULL OR ln_separado <> 2 THEN
        PV_ERROR := 'Se presento una inconsistencia en la cadena de caractenes enviada verifique que posea la CUENTA, RECUADADOR, FECHA_ASIGNACION separada por "|"';
        raise LE_CADENA;
      END IF;
    END IF;
    UPDATE RC_OPE_RECAUDA
       SET RECAUDADOR       = Lv_recaudaror,
           FECHA_ASIGNACION = Ld_fecha_asig,
           ESTADO           = 'A'
     WHERE CUENTA = Lv_cuenta;
  EXCEPTION
    WHEN LE_CADENA THEN
      PV_ERROR := 'ERROR: ' || PV_ERROR;
    WHEN OTHERS THEN
      lv_sql := 'SELECT length(''<CADENA>'') - length(replace(''<CADENA>'',''<SEPARADOR>'')) from dual';
      lv_sql := REPLACE(lv_sql, '<CADENA>', PV_LINEA);
      lv_sql := REPLACE(lv_sql, '<SEPARADOR>', PV_SEPARADOR);
      EXECUTE IMMEDIATE lv_sql
        INTO ln_separado;
      IF ln_separado <> 2 THEN
        PV_ERROR := 'Se presento una inconsistencia en la cadena de caractenes enviada verifique que posea la CUENTA, RECUADADOR, FECHA_ASIGNACION separada por "|"';
      ELSE
        PV_ERROR := SUBSTR(PV_ERROR, 1, 500);
      END IF;
  END P_CARGA_ARCHIVO;

  PROCEDURE P_ACTUALIZA_PAGOS_ELITE(PN_HILO NUMBER, PV_ERROR OUT VARCHAR2) IS
    --=====================================================================================--
    -- CREADO POR:     JORDAN RODRIGUEZ.
    -- FECHA MOD:      14/06/2017
    -- PROYECTO:       [11334] CAMBIOS CONTINUO RELOJ DE COBRANZAS Y REACTIVACIONES
    -- LIDER IRO:      IRO JUAN ROMERO
    -- LIDER :         SIS ANTONIO MAYORGA
    -- MOTIVO:         RECAUDADOR DAS - RECUPERACION DE CARTERA, PROCEDIMIENTO ENCARGADO DE 
    --                 VERIFICAR PAGOS DIARIOS DE LAS CUENTAS CATEGORIZADAS COMO CORPORTATIVOS
    --                 VIP, PRIMIUM E INSIGNIA QUE SE ENCUENTRAN EN LA TABLA RC_OPE_RECAUDA.
    --=====================================================================================--
    LE_ERROR EXCEPTION;
    LV_SQL_PAGOS VARCHAR2(4000);
  
    TYPE C_OBTIENE_PAGOS_ELITE IS REF CURSOR;
    OBTIENE_PAGOS_ELITE C_OBTIENE_PAGOS_ELITE;
  
    CURSOR C_FECHAS_CORTE(CV_CUENTA VARCHAR2) IS
      SELECT A.CUENTA,
             A.ID_CLIENTE,
             A.FECHA_CORTE,
             A.BALANCE_1,
             A.BALANCE_2,
             A.BALANCE_3,
             A.BALANCE_4,
             A.BALANCE_5,
             A.BALANCE_6,
             A.BALANCE_7,
             A.BALANCE_8,
             A.BALANCE_9,
             A.BALANCE_10,
             A.BALANCE_11,
             A.BALANCE_12,
             A.TOTALVENCIDA,
             A.TOTAL_DEUDA,
             A.SALDO_PENDIENTE,
             A.MAYORVENCIDO
        FROM RC_OPE_RECAUDA A
       WHERE A.CUENTA = CV_CUENTA
         AND A.FECHA_INGRESO BETWEEN
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 00:00:00',
                     'DD/MM/RRRR HH24:MI:SS') - 390 AND
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 23:59:59',
                     'DD/MM/RRRR HH24:MI:SS') - 1
          AND A.FECHA_ACTUALIZACION BETWEEN
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 00:00:00',
                     'DD/MM/RRRR HH24:MI:SS') - 390 AND
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 23:59:59',
                     'DD/MM/RRRR HH24:MI:SS') - 1
         AND A.ESTADO = 'C'
       GROUP BY A.CUENTA,
                A.ID_CLIENTE,
                A.FECHA_CORTE,
                A.BALANCE_1,
                A.BALANCE_2,
                A.BALANCE_3,
                A.BALANCE_4,
                A.BALANCE_5,
                A.BALANCE_6,
                A.BALANCE_7,
                A.BALANCE_8,
                A.BALANCE_9,
                A.BALANCE_10,
                A.BALANCE_11,
                A.BALANCE_12,
                A.TOTALVENCIDA,
                A.TOTAL_DEUDA,
                A.SALDO_PENDIENTE,
                A.MAYORVENCIDO
       ORDER BY A.CUENTA, A.FECHA_CORTE ASC;
    LC_BALANCES_MES C_FECHAS_CORTE%ROWTYPE;
    TYPE TY_PAGOS_BASE IS RECORD(
      T_ID_CLIENTE NUMBER,
      T_CUENTA     VARCHAR2(30),
      T_PAGO       NUMBER,
      T_FECHA_PAGO DATE);
      
    TYPE TY_BALANCES IS RECORD(
      T_CUENTA          VARCHAR2(30),
      T_FECHA_CORTE     DATE,
      T_BALANCE_1       NUMBER,
      T_BALANCE_2       NUMBER,
      T_BALANCE_3       NUMBER,
      T_BALANCE_4       NUMBER,
      T_BALANCE_5       NUMBER,
      T_BALANCE_6       NUMBER,
      T_BALANCE_7       NUMBER,
      T_BALANCE_8       NUMBER,
      T_BALANCE_9       NUMBER,
      T_BALANCE_10      NUMBER,
      T_BALANCE_11      NUMBER,
      T_BALANCE_12      NUMBER,
      T_TOTALVENCIDA    NUMBER,
      T_TOTAL_DEUDA     NUMBER,
      T_SALDO_PENDIENTE NUMBER,
      T_MAYORVENCIDO    VARCHAR2(10),
      T_PAGO            NUMBER);
    TYPE T_BALANCES IS TABLE OF TY_BALANCES INDEX BY BINARY_INTEGER;
    TYPE T_PAGOS_BASE IS TABLE OF TY_PAGOS_BASE INDEX BY BINARY_INTEGER;
    LT_BALANCES            T_BALANCES;
    LT_PAGOS_BASE          T_PAGOS_BASE;
    LN_DIA_INI             NUMBER;
    LN_DIASFIN             NUMBER;
    LN_MOD                 NUMBER;
    LV_ERROR               VARCHAR2(3000);
    LN_COUNT_TYPE          NUMBER := 0;
    LN_DIA_CORT            NUMBER := 0;
    LN_MES_CORT            NUMBER := 0;
    LN_DIAS                NUMBER := 0;
    LN_BALANCE             NUMBER := 0;
    LN_TOTAL_VENCIDO       NUMBER := 0;
    LN_TOTAL_DEUDA         NUMBER := 0;
    LV_PROGRAMA_SCP        VARCHAR2(200) := 'OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE';
    LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
    LN_ID_BITACORA_SCP     NUMBER := 0;
    LN_ERROR_SCP           NUMBER := 0;
    LV_ERROR_SCP           VARCHAR2(4000);
    LV_MENSAJE             VARCHAR2(4000);
    LV_MENSAJE_APL_SCP     VARCHAR2(4000);
    LV_MENSAJE_TEC_SCP     VARCHAR2(4000);
    LN_ID_DETALLE          NUMBER := 0;
    LN_BITACORA_PROCESADOS NUMBER := 0;
    LN_BITACORA_ERROR      NUMBER := 0;
  BEGIN
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('OP_BITACORA_DAS_ELITE',
                                              LV_PROGRAMA_SCP || '_' ||
                                              PN_HILO,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              0,
                                              LV_UNIDAD_REGISTRO,
                                              LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    IF LN_ERROR_SCP <> 0 THEN
      LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' ||
                    LV_ERROR_SCP;
    END IF;
    LN_DIA_INI := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                                PV_IDPARAMETRO     => 'DAS_DIAS_PAG',
                                                                PV_ERROR           => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LN_MOD := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                            PV_IDPARAMETRO     => 'DAS_HILO_PAGOS_ELITE',
                                                            PV_ERROR           => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LN_DIASFIN := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                                PV_IDPARAMETRO     => 'DAS_DIAS_PAG_FIN',
                                                                PV_ERROR           => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;                                                                   
    LV_SQL_PAGOS := 'SELECT B.CUSTOMER_ID,C.CUSTCODE, SUM(A.CADAMT_GL) VALOR, TRUNC(B.CAENTDATE) FECHA
                     FROM CASHDETAIL A, CASHRECEIPTS_ALL B, CUSTOMER_ALL C
                   WHERE B.CAENTDATE BETWEEN
                   TO_DATE(TO_CHAR(SYSDATE - <CN_DIAS>, ''DD/MM/RRRR'') || '' 00:00:00'',
                   ''DD/MM/RRRR HH24:MI:SS'') AND
                   TO_DATE(TO_CHAR(SYSDATE - <CN_DIASFIN>, ''DD/MM/RRRR'') || '' 23:59:59'',
                   ''DD/MM/RRRR HH24:MI:SS'')
                   AND A.CADXACT = B.CAXACT
                   AND B.CUSTOMER_ID=C.CUSTOMER_ID
                   AND MOD(SUBSTR(B.CUSTOMER_ID, 3), <CN_MOD>) = <CN_HILO>
                   GROUP BY B.CUSTOMER_ID,C.CUSTCODE, TRUNC(B.CAENTDATE)';
    LV_SQL_PAGOS := REPLACE(LV_SQL_PAGOS, '<CN_DIAS>', LN_DIA_INI);
    LV_SQL_PAGOS := REPLACE(LV_SQL_PAGOS, '<CN_DIASFIN>', LN_DIASFIN);
    LV_SQL_PAGOS := REPLACE(LV_SQL_PAGOS, '<CN_MOD>', LN_MOD);
    LV_SQL_PAGOS := REPLACE(LV_SQL_PAGOS, '<CN_HILO>', PN_HILO);
    OPEN OBTIENE_PAGOS_ELITE FOR LV_SQL_PAGOS;
    LOOP
      FETCH OBTIENE_PAGOS_ELITE BULK COLLECT INTO LT_PAGOS_BASE LIMIT 1000;
      EXIT WHEN LT_PAGOS_BASE.COUNT = 0;
      IF LT_PAGOS_BASE.COUNT > 0 THEN
        FOR J IN LT_PAGOS_BASE.FIRST .. LT_PAGOS_BASE.LAST LOOP
          BEGIN
            FOR I IN C_FECHAS_CORTE(LT_PAGOS_BASE(J).T_CUENTA) LOOP
              LN_TOTAL_VENCIDO                := 0;
              LN_TOTAL_DEUDA                  := 0;
              LN_BALANCE                      := 0;
              LC_BALANCES_MES.CUENTA          := I.CUENTA;
              LC_BALANCES_MES.ID_CLIENTE      := I.ID_CLIENTE;
              LC_BALANCES_MES.FECHA_CORTE     := I.FECHA_CORTE;
              LC_BALANCES_MES.BALANCE_1       := I.BALANCE_1;
              LC_BALANCES_MES.BALANCE_2       := I.BALANCE_2;
              LC_BALANCES_MES.BALANCE_3       := I.BALANCE_3;
              LC_BALANCES_MES.BALANCE_4       := I.BALANCE_4;
              LC_BALANCES_MES.BALANCE_5       := I.BALANCE_5;
              LC_BALANCES_MES.BALANCE_6       := I.BALANCE_6;
              LC_BALANCES_MES.BALANCE_7       := I.BALANCE_7;
              LC_BALANCES_MES.BALANCE_8       := I.BALANCE_8;
              LC_BALANCES_MES.BALANCE_9       := I.BALANCE_9;
              LC_BALANCES_MES.BALANCE_10      := I.BALANCE_10;
              LC_BALANCES_MES.BALANCE_11      := I.BALANCE_11;
              LC_BALANCES_MES.BALANCE_12      := I.BALANCE_12;
              LC_BALANCES_MES.TOTALVENCIDA    := I.TOTALVENCIDA;
              LC_BALANCES_MES.TOTAL_DEUDA     := I.TOTAL_DEUDA;
              LC_BALANCES_MES.SALDO_PENDIENTE := I.SALDO_PENDIENTE;
              LC_BALANCES_MES.MAYORVENCIDO    := I.MAYORVENCIDO;
              LN_DIA_CORT                     := TO_NUMBER(TO_CHAR(I.FECHA_CORTE, 'DD'));
              LN_MES_CORT                     := TO_NUMBER(TO_CHAR(I.FECHA_CORTE, 'MM'));
              CASE
                WHEN I.BALANCE_1 > 0 THEN
                  LN_BALANCE := 1;
                WHEN I.BALANCE_2 > 0 THEN
                  LN_BALANCE := 2;
                WHEN I.BALANCE_3 > 0 THEN
                  LN_BALANCE := 3;
                WHEN I.BALANCE_4 > 0 THEN
                  LN_BALANCE := 4;
                WHEN I.BALANCE_5 > 0 THEN
                  LN_BALANCE := 5;
                WHEN I.BALANCE_6 > 0 THEN
                  LN_BALANCE := 6;
                WHEN I.BALANCE_7 > 0 THEN
                  LN_BALANCE := 7;
                WHEN I.BALANCE_8 > 0 THEN
                  LN_BALANCE := 8;
                WHEN I.BALANCE_9 > 0 THEN
                  LN_BALANCE := 9;
                WHEN I.BALANCE_10 > 0 THEN
                  LN_BALANCE := 10;
                WHEN I.BALANCE_11 > 0 THEN
                  LN_BALANCE := 11;
                WHEN I.BALANCE_12 > 0 THEN
                  LN_BALANCE := 12;
                ELSE 
                  LN_BALANCE := 0;
              END CASE;
              IF LN_BALANCE > 0 THEN
                P_CALCULA_BALANCE(PN_ID_BALANCE     => LN_BALANCE,
                                   PN_PAGO           => LT_PAGOS_BASE(J).T_PAGO,
                                   PN_BALANCE1       => LC_BALANCES_MES.BALANCE_1,
                                   PN_BALANCE2       => LC_BALANCES_MES.BALANCE_2,
                                   PN_BALANCE3       => LC_BALANCES_MES.BALANCE_3,
                                   PN_BALANCE4       => LC_BALANCES_MES.BALANCE_4,
                                   PN_BALANCE5       => LC_BALANCES_MES.BALANCE_5,
                                   PN_BALANCE6       => LC_BALANCES_MES.BALANCE_6,
                                   PN_BALANCE7       => LC_BALANCES_MES.BALANCE_7,
                                   PN_BALANCE8       => LC_BALANCES_MES.BALANCE_8,
                                   PN_BALANCE9       => LC_BALANCES_MES.BALANCE_9,
                                   PN_BALANCE10      => LC_BALANCES_MES.BALANCE_10,
                                   PN_BALANCE11      => LC_BALANCES_MES.BALANCE_11,
                                   PN_BALANCE12      => LC_BALANCES_MES.BALANCE_12,
                                   PN_TOTAL_VENCIDO  => LN_TOTAL_VENCIDO,
                                   PV_ERROR          => LV_ERROR);
                IF LV_ERROR IS NULL THEN
                  LN_DIAS := TRUNC(SYSDATE) - LC_BALANCES_MES.FECHA_CORTE;
                  IF LN_MES_CORT = 2 THEN
                    LN_DIAS := LN_DIAS + 2;
                  END IF;
                  LN_COUNT_TYPE                   := LN_COUNT_TYPE + 1;
                  IF LN_DIAS >= 30 THEN
                    LT_BALANCES(LN_COUNT_TYPE).T_CUENTA := I.CUENTA;
                    LT_BALANCES(LN_COUNT_TYPE).T_FECHA_CORTE := I.FECHA_CORTE;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_1 := LC_BALANCES_MES.BALANCE_2;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_2 := LC_BALANCES_MES.BALANCE_3;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_3 := LC_BALANCES_MES.BALANCE_4;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_4 := LC_BALANCES_MES.BALANCE_5;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_5 := LC_BALANCES_MES.BALANCE_6;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_6 := LC_BALANCES_MES.BALANCE_7;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_7 := LC_BALANCES_MES.BALANCE_8;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_8 := LC_BALANCES_MES.BALANCE_9;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_9 := LC_BALANCES_MES.BALANCE_10;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_10 := LC_BALANCES_MES.BALANCE_11;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_11 := LC_BALANCES_MES.BALANCE_12;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_12 := 0;
                    LT_BALANCES(LN_COUNT_TYPE).T_TOTALVENCIDA := LN_TOTAL_VENCIDO;
                    LT_BALANCES(LN_COUNT_TYPE).T_TOTAL_DEUDA := I.TOTAL_DEUDA;
                    LT_BALANCES(LN_COUNT_TYPE).T_SALDO_PENDIENTE := I.SALDO_PENDIENTE-(LT_PAGOS_BASE(J).T_PAGO);
                    LT_BALANCES(LN_COUNT_TYPE).T_MAYORVENCIDO := I.MAYORVENCIDO;
                    LT_BALANCES(LN_COUNT_TYPE).T_PAGO := LT_PAGOS_BASE(J).T_PAGO;
                  ELSE
                    LT_BALANCES(LN_COUNT_TYPE).T_CUENTA := I.CUENTA;
                    LT_BALANCES(LN_COUNT_TYPE).T_FECHA_CORTE := I.FECHA_CORTE;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_1 := LC_BALANCES_MES.BALANCE_1;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_2 := LC_BALANCES_MES.BALANCE_2;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_3 := LC_BALANCES_MES.BALANCE_3;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_4 := LC_BALANCES_MES.BALANCE_4;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_5 := LC_BALANCES_MES.BALANCE_5;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_6 := LC_BALANCES_MES.BALANCE_6;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_7 := LC_BALANCES_MES.BALANCE_7;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_8 := LC_BALANCES_MES.BALANCE_8;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_9 := LC_BALANCES_MES.BALANCE_9;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_10 := LC_BALANCES_MES.BALANCE_10;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_11 := LC_BALANCES_MES.BALANCE_11;
                    LT_BALANCES(LN_COUNT_TYPE).T_BALANCE_12 := LC_BALANCES_MES.BALANCE_12;
                    LT_BALANCES(LN_COUNT_TYPE).T_TOTALVENCIDA := LN_TOTAL_VENCIDO;
                    LT_BALANCES(LN_COUNT_TYPE).T_TOTAL_DEUDA := I.TOTAL_DEUDA;
                    LT_BALANCES(LN_COUNT_TYPE).T_SALDO_PENDIENTE := I.SALDO_PENDIENTE-(LT_PAGOS_BASE(J).T_PAGO);
                    LT_BALANCES(LN_COUNT_TYPE).T_MAYORVENCIDO := I.MAYORVENCIDO;
                    LT_BALANCES(LN_COUNT_TYPE).T_PAGO := LT_PAGOS_BASE(J).T_PAGO;
                  END IF;
                ELSE
                  PV_ERROR:= PV_ERROR||' ERROR: ' || LV_ERROR;
                  LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
                  LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.P_CALCULA_BALANCE';
                  LV_MENSAJE_TEC_SCP := 'PRESENTO EL SIGUENTE ERROR EN EL BALANCE_' ||
                                        LN_BALANCE || 'DE LA CUENTA' ||
                                        I.CUENTA || ' ERROR: ' || LV_ERROR;
                  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                            LV_MENSAJE_APL_SCP,
                                                            LV_MENSAJE_TEC_SCP,
                                                            NULL,
                                                            0,
                                                            0,
                                                            NULL,
                                                            I.CUENTA,
                                                            0,
                                                            0,
                                                            'N',
                                                            LN_ID_DETALLE,
                                                            LN_ERROR_SCP,
                                                            LV_MENSAJE);
                END IF;
              END IF;
            END LOOP;
            LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
          EXCEPTION
            WHEN OTHERS THEN
              PV_ERROR:= PV_ERROR||' ERROR: ' || SUBSTR(SQLERRM, 1, 500);
              LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
              LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE';
              LV_MENSAJE_TEC_SCP := 'PRESENTO EL SIGUENTE EN LA CUENTA' || LT_PAGOS_BASE(J)
                                   .T_ID_CLIENTE || ' ERROR: ' ||
                                    SUBSTR(SQLERRM, 1, 500);
              SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                        LV_MENSAJE_APL_SCP,
                                                        LV_MENSAJE_TEC_SCP,
                                                        NULL,
                                                        0,
                                                        0,
                                                        NULL,
                                                        LT_PAGOS_BASE(J).T_ID_CLIENTE,
                                                        0,
                                                        0,
                                                        'N',
                                                        LN_ID_DETALLE,
                                                        LN_ERROR_SCP,
                                                        LV_MENSAJE);
          END;
        
        END LOOP;
      END IF;
      IF LT_BALANCES.COUNT > 0 THEN
        BEGIN
          FORALL I IN LT_BALANCES.FIRST .. LT_BALANCES.LAST
            UPDATE RC_OPE_RECAUDA A
               SET A.BALANCE_1       = LT_BALANCES(I).T_BALANCE_1,
                   A.BALANCE_2       = LT_BALANCES(I).T_BALANCE_2,
                   A.BALANCE_3       = LT_BALANCES(I).T_BALANCE_3,
                   A.BALANCE_4       = LT_BALANCES(I).T_BALANCE_4,
                   A.BALANCE_5       = LT_BALANCES(I).T_BALANCE_5,
                   A.BALANCE_6       = LT_BALANCES(I).T_BALANCE_6,
                   A.BALANCE_7       = LT_BALANCES(I).T_BALANCE_7,
                   A.BALANCE_8       = LT_BALANCES(I).T_BALANCE_8,
                   A.BALANCE_9       = LT_BALANCES(I).T_BALANCE_9,
                   A.BALANCE_10      = LT_BALANCES(I).T_BALANCE_10,
                   A.BALANCE_11      = LT_BALANCES(I).T_BALANCE_11,
                   A.BALANCE_12      = LT_BALANCES(I).T_BALANCE_12,
                   A.TOTALVENCIDA    = LT_BALANCES(I).T_TOTALVENCIDA,
                   A.TOTAL_DEUDA     = LT_BALANCES(I).T_TOTAL_DEUDA,
                   A.SALDO_PENDIENTE = LT_BALANCES(I).T_SALDO_PENDIENTE,
                   A.FECHA_ACTUALIZACION=SYSDATE
             WHERE A.FECHA_CORTE = LT_BALANCES(I).T_FECHA_CORTE
             AND   A.CUENTA=LT_BALANCES(I).T_CUENTA;
        EXCEPTION
          WHEN OTHERS THEN
            PV_ERROR:= PV_ERROR||' ERROR: ' || SUBSTR(SQLERRM, 1, 500);
            LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
            LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE';
            LV_MENSAJE_TEC_SCP := 'PRESENTO EL SIGUENTE AL EJECUTAR EL FORALL ERROR: ' ||
                                  SUBSTR(SQLERRM, 1, 500);
            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                      LV_MENSAJE_APL_SCP,
                                                      LV_MENSAJE_TEC_SCP,
                                                      NULL,
                                                      0,
                                                      0,
                                                      NULL,
                                                      0,
                                                      0,
                                                      0,
                                                      'N',
                                                      LN_ID_DETALLE,
                                                      LN_ERROR_SCP,
                                                      LV_MENSAJE);
        END;
      END IF;
      COMMIT;
      LT_BALANCES.DELETE;
      LN_COUNT_TYPE:=0;
    END LOOP;
    COMMIT;
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR           := 'ERROR: P_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE => ' || SUBSTR(SQLERRM, 1, 300);
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE';
      LV_MENSAJE_TEC_SCP := 'ERROR AL LEER UN DATO PARAMETRIZADO.';
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                0,
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                                LN_BITACORA_PROCESADOS,
                                                LN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
      ROLLBACK;   
    WHEN OTHERS THEN
      PV_ERROR           := 'ERROR: P_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE => ' || SUBSTR(SQLERRM, 1, 300);
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := 'PROCESO OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE';
      LV_MENSAJE_TEC_SCP := 'PRESENTO EL SIGUENTE AL EJECUTAR EL FORALL ERROR: ' ||
                            SUBSTR(SQLERRM, 1, 500);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                0,
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                                LN_BITACORA_PROCESADOS,
                                                LN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
      ROLLBACK;
  END P_ACTUALIZA_PAGOS_ELITE;
  
  PROCEDURE P_CALCULA_BALANCE(PN_ID_BALANCE    NUMBER,
                              PN_PAGO          NUMBER,
                              PN_BALANCE1      IN OUT NUMBER,
                              PN_BALANCE2      IN OUT NUMBER,
                              PN_BALANCE3      IN OUT NUMBER,
                              PN_BALANCE4      IN OUT NUMBER,
                              PN_BALANCE5      IN OUT NUMBER,
                              PN_BALANCE6      IN OUT NUMBER,
                              PN_BALANCE7      IN OUT NUMBER,
                              PN_BALANCE8      IN OUT NUMBER,
                              PN_BALANCE9      IN OUT NUMBER,
                              PN_BALANCE10     IN OUT NUMBER,
                              PN_BALANCE11     IN OUT NUMBER,
                              PN_BALANCE12     IN OUT NUMBER,
                              PN_TOTAL_VENCIDO OUT NUMBER,
                              PV_ERROR         OUT VARCHAR2) IS
    --=====================================================================================--
    -- CREADO POR:     JORDAN RODRIGUEZ.
    -- FECHA MOD:      14/06/2017
    -- PROYECTO:       [11334] CAMBIOS CONTINUO RELOJ DE COBRANZAS Y REACTIVACIONES
    -- LIDER IRO:      IRO JUAN ROMERO
    -- LIDER :         SIS ANTONIO MAYORGA
    -- MOTIVO:         RECAUDADOR DAS - CALCULO DE LOS BALANCES DE UN CLIENTE.
    --=====================================================================================--
    LN_INTERACION    NUMBER := 0;
    LN_SALDO_BALANCE NUMBER := 0;
    TYPE T_BALANCE IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    LT_BALANCE T_BALANCE;
    LN_CONT  NUMBER:=0;
    LN_NEXT_BALANCE NUMBER:=0;
  BEGIN
    LN_INTERACION := PN_ID_BALANCE;
    LT_BALANCE(1) := PN_BALANCE1;
    LT_BALANCE(2) := PN_BALANCE2;
    LT_BALANCE(3) := PN_BALANCE3;
    LT_BALANCE(4) := PN_BALANCE4;
    LT_BALANCE(5) := PN_BALANCE5;
    LT_BALANCE(6) := PN_BALANCE6;
    LT_BALANCE(7) := PN_BALANCE7;
    LT_BALANCE(8) := PN_BALANCE8;
    LT_BALANCE(9) := PN_BALANCE9;
    LT_BALANCE(10) := PN_BALANCE10;
    LT_BALANCE(11) := PN_BALANCE11;
    LT_BALANCE(12) := PN_BALANCE12;
    LN_SALDO_BALANCE := LT_BALANCE(PN_ID_BALANCE) - PN_PAGO;
    IF PN_PAGO > 0 THEN 
      WHILE (LN_INTERACION <= 12) LOOP
        LN_CONT:=LN_CONT+1;
        LN_NEXT_BALANCE:=0;
        IF LN_SALDO_BALANCE > 0 THEN
          LT_BALANCE(LN_INTERACION) := LN_SALDO_BALANCE;
        ELSIF LN_SALDO_BALANCE = 0 AND LN_CONT = 1 THEN
        LT_BALANCE(PN_ID_BALANCE):=LN_SALDO_BALANCE;
        ELSIF LN_SALDO_BALANCE < 0 THEN
          LT_BALANCE(LN_INTERACION) := 0;
          IF LN_INTERACION < 12 THEN
            LN_SALDO_BALANCE := LT_BALANCE(LN_INTERACION + 1) +
                                LN_SALDO_BALANCE;
            LN_NEXT_BALANCE:=1;
          ELSIF LN_INTERACION = 12 THEN
            LN_SALDO_BALANCE := 0;            
          END IF;
        END IF;
        IF LN_SALDO_BALANCE > 0 OR LN_SALDO_BALANCE = 0 THEN
          IF LN_NEXT_BALANCE > 0 THEN
            LT_BALANCE(LN_INTERACION + 1):=LN_SALDO_BALANCE;
          END IF;
          EXIT;
        END IF;
        LN_INTERACION:=LN_INTERACION+1;
      END LOOP;
    END IF;
    PN_BALANCE1      := LT_BALANCE(1);
    PN_BALANCE2      := LT_BALANCE(2);
    PN_BALANCE3      := LT_BALANCE(3);
    PN_BALANCE4      := LT_BALANCE(4);
    PN_BALANCE5      := LT_BALANCE(5);
    PN_BALANCE6      := LT_BALANCE(6);
    PN_BALANCE7      := LT_BALANCE(7);
    PN_BALANCE8      := LT_BALANCE(8);
    PN_BALANCE9      := LT_BALANCE(9);
    PN_BALANCE10     := LT_BALANCE(10);
    PN_BALANCE11     := LT_BALANCE(11);
    PN_BALANCE12     := LT_BALANCE(12);
    PN_TOTAL_VENCIDO := PN_BALANCE1 + PN_BALANCE2 + PN_BALANCE3 +
                        PN_BALANCE4 + PN_BALANCE5 + PN_BALANCE6 +
                        PN_BALANCE7 + PN_BALANCE8 + PN_BALANCE9 +
                        PN_BALANCE10 + PN_BALANCE11;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR EN EL PROCEDIMIENTO P_CALCULA_BALANCE '||SUBSTR(SQLERRM, 1, 500);
  END P_CALCULA_BALANCE;
END OP_RECAUDADOR_DAS;
/
