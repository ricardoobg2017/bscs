CREATE OR REPLACE PACKAGE CLK_MARCA_DTH IS

  PROCEDURE CRP_INSERTA_MARCA_MTP(PV_CUENTA IN VARCHAR2,
                                  PV_ERROR  OUT VARCHAR2);

END CLK_MARCA_DTH;
/
CREATE OR REPLACE PACKAGE BODY CLK_MARCA_DTH IS

  --==============================================================================--
  -- CREADO POR : SUD KLEBER ARREAGA
  -- LIDER SIS  : SIS OSCAR APOLINARIO
  -- LIDER PDS  : SUD CRISTHIAN ACOSTA
  -- FECHA DE CREACION: 21/01/2016
  -- PROYECTO   : [10417] - MULTIPUNTO DTH TRX POSTVENTA
  -- PURPOSE    : INSERCI�N DE LA MARCA DTHM PARA CUENTAS MULTIPUNTO
  --==============================================================================--

  --Inserta la marca en BSCS
  PROCEDURE CRP_INSERTA_MARCA_MTP(PV_CUENTA IN VARCHAR2,
                                  PV_ERROR  OUT VARCHAR2) IS
  
    --CURSOR PARA CONOCER SI HAY REGISTRO DE LA MARCA DTHM
    CURSOR C_EXISTE_INFO_CUST(CI_CUSTOMERID VARCHAR2) IS
      SELECT C.TEXT09 MARCA -- [10417] 29/02/2016
        FROM INFO_CUST_TEXT C
       WHERE CUSTOMER_ID = CI_CUSTOMERID;
  
    --Cursor que obtiene el CUSTOMER ID
    CURSOR C_OBTIENE_CUSTOMER_ID(CV_CUENTA VARCHAR2) IS
      SELECT CUSTOMER_ID FROM CUSTOMER_ALL WHERE CUSTCODE = CV_CUENTA;
  
    LC_FACT_ELECT_MARCA C_EXISTE_INFO_CUST%ROWTYPE;
    LB_EXISTE           BOOLEAN;
    LV_ERROR            VARCHAR(6000);
    LN_CUSTOMER_ID      VARCHAR2(100);
    ---
    LE_MIERROR EXCEPTION;
    LV_APLICACION VARCHAR2(100) := 'CLK_MARCA_DTH.CRP_INSERTA_MARCA_INFO_CUST';
  
  BEGIN
  
    IF PV_CUENTA IS NULL THEN
      LV_ERROR := 'LA CUENTA NO PUEDE SER NULL';
      RAISE LE_MIERROR;
    END IF;
  
    --OBTENGO EL CUSTOMER_ID
    OPEN C_OBTIENE_CUSTOMER_ID(PV_CUENTA);
    FETCH C_OBTIENE_CUSTOMER_ID
      INTO LN_CUSTOMER_ID;
    LB_EXISTE := C_OBTIENE_CUSTOMER_ID%FOUND;
    CLOSE C_OBTIENE_CUSTOMER_ID;
  
    IF NOT LB_EXISTE THEN
      LV_ERROR := 'NO EXISTE CUENTA EN BSCS';
      RAISE LE_MIERROR;
    END IF;
  
    -- 
    OPEN C_EXISTE_INFO_CUST(LN_CUSTOMER_ID);
    FETCH C_EXISTE_INFO_CUST
      INTO LC_FACT_ELECT_MARCA;
    LB_EXISTE := C_EXISTE_INFO_CUST%FOUND;
    CLOSE C_EXISTE_INFO_CUST;
  
    -- SI NO EXISTE EL CUSTOMER LO INSERTO Y PONGO LA MARCA   
    IF NOT LB_EXISTE THEN
      BEGIN
        --Inserto
        INSERT INTO INFO_CUST_TEXT
          (CUSTOMER_ID, TEXT09)
        VALUES
          (LN_CUSTOMER_ID, 'DTHM');
      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR := 'ERROR AL INSERTAR DATOS DE FACTURA ELECTRONICA EN BSCS. ACCION: ';
          RAISE LE_MIERROR;
      END;
    ELSE
      -- SI YA EXISTE EL CUSTOMER ACTUALIZO CON LA MARCA    
      BEGIN
        UPDATE INFO_CUST_TEXT
           SET TEXT09 = 'DTHM'
         WHERE CUSTOMER_ID = LN_CUSTOMER_ID;
      END;
    END IF;
  
  EXCEPTION
    WHEN LE_MIERROR THEN
      PV_ERROR := substr(LV_APLICACION || '. Error: ' || LV_ERROR, 1, 1000);
    WHEN OTHERS THEN
      PV_ERROR := substr(LV_APLICACION || ' - ' || SQLERRM, 1, 1000);
    
  END CRP_INSERTA_MARCA_MTP;

END CLK_MARCA_DTH;
/
