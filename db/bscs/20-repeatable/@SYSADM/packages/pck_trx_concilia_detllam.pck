create or replace package pck_trx_concilia_detllam is

  -- Author  : WMENDOZA
  -- Created : 07/09/2011 9:27:54
  -- Purpose : 
 /*procedure clp_obtiene_lineas ;*/
-- TYPE Tvarchar IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
TYPE Tcoid IS TABLE OF cl_concilia_detllam_temp.dn_num%TYPE INDEX BY BINARY_INTEGER;
 
 Function OBTIENE_TELEFONO_DNC (pv_telefono  In Varchar2
                               ) Return Varchar2;
 procedure clp_asigna_hilo;
 procedure clp_verifica_fees(pn_hilo in number,pvError out varchar2);
 procedure clp_verifica_axis;
end pck_trx_concilia_detllam;
/
create or replace package body pck_trx_concilia_detllam is

Function OBTIENE_TELEFONO_DNC (pv_telefono  In Varchar2
                               ) Return Varchar2 Is

   lv_telefono Varchar2(20);
Begin
   If length(Trim(pv_telefono)) >= 8 Then -- obtengo ultimos 8 digitos del numero enviado
      lv_telefono := substr(Trim(pv_telefono), length(Trim(pv_telefono))-7);
      If substr(lv_telefono,1,1) NOT IN ('9','8','6') Then -- si no viene de prefijo celular lo devuelvo tal cual
         lv_telefono := pv_telefono;
      Else
         If substr(lv_telefono,1,1) = '6' And substr(lv_telefono,2,1) NOT IN ('9','8') Then
            lv_telefono := pv_telefono;
         End If;
      End If;
   Elsif length(Trim(pv_telefono)) = 7 Then -- si llega en 7 digitos se agrega el prefijo 09
      lv_telefono := '9'||pv_telefono;
   Elsif length(Trim(pv_telefono)) = 6 Then -- si llega en 6 digitos se agrega el prefijo 09 y serie 0
      lv_telefono := '90'||pv_telefono;
   Else -- si llega de menos lo devuelvo tal cual
      lv_telefono := pv_telefono;
   End If;
   --
   Return lv_telefono;
End;
    
procedure clp_asigna_hilo is
  
cursor c_ttc_view_customers is
SELECT TYPE_DETLLAM(Customer_id,co_id )
              FROM TF_DETALLE_LLAMADA_TMP where fecha > sysdate -1 and hilo='XX';-- and rownum < 5000;

i_react               TYPE_DETLLAM_TBK;         
cont                  number:=0;     
cont_gen              number:=0;
PN_MAXTRX_EXE         number:=5000;
pvError               varchar2(30000);
lv_sql_create         varchar2(1000);
begin


lv_sql_create := 'insert into TF_DETALLE_LLAMADA_TMP (Select a.co_id,(Select /* +rule +*/b.customer_id From contract_all b Where b.co_id=a.co_id),''XX'' hilo,sysdate fecha
  From Pr_Serv_Status_Hist a
 Where a.Sncode = 13
   And a.Status =''A'')';

/*lv_sql_create := 'insert into TF_DETALLE_LLAMADA_TMP (select \*+ index(sh IX_PR_SERV_STATUS_STATUS) index(ca PKCONTRACT_ALL)  +*\
                  sh.co_id,ca.customer_id,''XX'' hilo,sysdate fecha
                  from pr_serv_status_hist sh, contract_all ca
                  where sh.co_id = ca.co_id
                  and sh.sncode = 13
                  and sh.status = ''A''
                  and ca.co_id >0)';*/
                  
    execute immediate lv_sql_create;

    commit;


 
  
 OPEN c_ttc_view_customers ;
 LOOP   
 FETCH c_ttc_view_customers
 BULK COLLECT INTO i_react LIMIT PN_MAXTRX_EXE;
     FOR rc_in IN (select * from table(cast( i_react as TYPE_DETLLAM_TBK)))  LOOP  
       BEGIN
       
       update TF_DETALLE_LLAMADA_TMP set hilo=cont where co_id=rc_in.co_id and customer_id=rc_in.customer_id and hilo = 'XX';
       
       if cont_gen = 5000 then         
         cont:=cont+1;
         cont_gen:=0;
         commit;
       end if;
        
       cont_gen:=cont_gen+1;
         
          EXCEPTION
          WHEN OTHERS THEN 
                   pvError := sqlerrm;
                   ROLLBACK;
        END;
        
        end loop; 
     --***********
     --COMMIT;
     --*********** 
     --EXIT WHEN LB_EXISTE_BCK = FALSE; 
     EXIT WHEN c_ttc_view_customers%NOTFOUND;  
 END LOOP;         
 commit;
 CLOSE c_ttc_view_customers;
 
end;  

procedure clp_verifica_fees (pn_hilo in number,pvError out varchar2) is  
  
cursor c_ttc_view_customers is
SELECT TYPE_DETLLAM(Customer_id,co_id )
              FROM TF_DETALLE_LLAMADA_TMP where hilo=pn_hilo;

--VIEW_CONCILIA_DETLLAM
CURSOR c_parametros (cn_tipo_parametro number,cv_id_parametro VARCHAR2) IS   
      SELECT P.VALOR   
        FROM GV_PARAMETROS P   
       WHERE P.ID_TIPO_PARAMETRO = cn_tipo_parametro   
         AND P.ID_PARAMETRO = cv_id_parametro;   

cursor c_verifica_detllam (cn_curtomer_id varchar2) is
select 'x'
  from fees
 where sncode = 598
   and customer_id = cn_curtomer_id
   And fee_type='R'
    and seqno > 0
   and period <> 0;

cursor c_obtiene_num (cn_coid varchar2) is
select di.dn_num from contr_services_cap co ,directory_number di 
where co.co_id=cn_coid--320122
and co.dn_id=di.dn_id;


i_react               TYPE_DETLLAM_TBK;
Lc_verifica_detllam   c_verifica_detllam%ROWTYPE; 
lc_obtiene_num        c_obtiene_num%rowtype;

LN_COUNT_COMMIT  NUMBER:=0;    
LN_COUNT_MAXTRX  NUMBER:=0;  
ln_cont_general  NUMBER:=0;
lv_exist_proc    varchar2(1):='S';
--LV_STOP varchar2(1):='S';

--ELIMINAR
PN_MAX_COMMIT  NUMBER:=0;  
PN_MAXTRX_EXE  NUMBER:=0;
pn_MaxRetenTrx NUMBER:=0;
LB_FOUND  BOOLEAN:=FALSE;
LB_EXISTE_BCK BOOLEAN:=true;


begin
  
--Obtengo los parametros  
OPEN c_parametros(6322,'CANT_MAX_COMMIT');   
FETCH c_parametros   
      INTO PN_MAX_COMMIT;   
CLOSE c_parametros; 

OPEN c_parametros(6322,'CANT_MAXTRX_EXE');   
FETCH c_parametros   
      INTO PN_MAXTRX_EXE;   
CLOSE c_parametros;

OPEN c_parametros(6322,'CANT_TRX_ROWNUM');   
FETCH c_parametros   
      INTO pn_MaxRetenTrx;   
CLOSE c_parametros;


OPEN c_ttc_view_customers;
 LOOP   
 FETCH c_ttc_view_customers
 BULK COLLECT INTO i_react LIMIT PN_MAXTRX_EXE;
     FOR rc_in IN (select * from table(cast( i_react as TYPE_DETLLAM_TBK)))  LOOP  
        BEGIN     
          
           ln_cont_general:=ln_cont_general+1;
           OPEN c_verifica_detllam(rc_in.Customer_id);      
           FETCH c_verifica_detllam      
             INTO Lc_verifica_detllam;      
           LB_FOUND := c_verifica_detllam%FOUND;      
           CLOSE c_verifica_detllam;   
           
           if LB_FOUND then
             
             OPEN c_obtiene_num(rc_in.co_id);      
             FETCH c_obtiene_num      
               INTO lc_obtiene_num;      
             LB_FOUND := c_obtiene_num%FOUND;      
             CLOSE c_obtiene_num;   
             
             insert into cl_concilia_detllam_temp (CURTOMER_ID, FECHA,co_id,dn_num)
             values (rc_in.Customer_id, sysdate,rc_in.co_id,lc_obtiene_num.dn_num);
           end if;  
               
          LN_COUNT_COMMIT:=LN_COUNT_COMMIT+1;     
          if LN_COUNT_COMMIT >= PN_MAX_COMMIT Then
                     COMMIT; 
                     LN_COUNT_COMMIT:=0;
          END IF;
            ----------------------------------------------------------------------
            --Cada cuantas trx se debe preguntar si el proceso continua arriba o si es que se realizo un bajada de proceso manual                        
          LN_COUNT_MAXTRX:=LN_COUNT_MAXTRX+1;
          IF LN_COUNT_MAXTRX >= PN_MAXTRX_EXE THEN
            
            OPEN c_parametros(6185,'EXISTE_PROCESO');   
            FETCH c_parametros   
                  INTO lv_exist_proc;   
            CLOSE c_parametros;
            IF lv_exist_proc = 'N' THEN
              LB_EXISTE_BCK:=false;
              EXIT;
            END IF;
            LN_COUNT_MAXTRX := 0;
          END IF;            
        EXCEPTION
          WHEN OTHERS THEN 
                   pvError := sqlerrm;
                   ROLLBACK;
        END;
            
     end loop; 
     --***********
     COMMIT;
     --*********** 
     EXIT WHEN LB_EXISTE_BCK = FALSE; 
     EXIT WHEN c_ttc_view_customers%NOTFOUND;  
 END LOOP;         
 CLOSE c_ttc_view_customers;
end; 

procedure clp_verifica_axis is  
  
             
cursor c_lineas_axis is
select distinct dn_num from cl_concilia_detllam_temp t where t.fecha > sysdate -2 ;

--DETALLES DE SERVICIO
cursor c_verifica_detllam(cv_idservicio varchar2) is
SELECT 'X'
 FROM porta.CL_DETALLES_SERVICIOS@axis A, porta.CL_TIPOS_DETALLES_SERVICIOS@axis B
WHERE A.ID_SERVICIO = cv_idservicio
  AND A.ID_TIPO_DETALLE_SERV = B.ID_TIPO_DETALLE_SERV
  AND B.ID_TIPO_DETALLE_SERV  = B.ID_SUBPRODUCTO||'-DTLLAM'
  and a.fecha_hasta is null;
   
Lc_Cursor           Tcoid;
lc_verifica_detllam        c_verifica_detllam%rowtype;
lvError             varchar2(3000);
lv_idservicio       varchar2(20);
LB_FOUND            boolean;
ln_cont             number :=0;
begin  
  
    OPEN c_lineas_axis;
    FETCH c_lineas_axis BULK COLLECT
      INTO Lc_Cursor;
    CLOSE c_lineas_axis;

    IF Lc_Cursor.COUNT > 0 THEN

      FOR i IN Lc_Cursor.FIRST .. Lc_Cursor.LAST LOOP
        BEGIN              
           lv_idservicio:=obtiene_telefono_dnc(pv_telefono => Lc_Cursor(i));
           
           OPEN c_verifica_detllam(lv_idservicio);      
           FETCH c_verifica_detllam      
             INTO Lc_verifica_detllam;      
           LB_FOUND := c_verifica_detllam%FOUND;      
           CLOSE c_verifica_detllam;   
           
           if LB_FOUND then             
             update cl_concilia_detllam_temp set estatus_axis='A' where dn_num=Lc_Cursor(i) and fecha > sysdate - 2;
           end if;
           
           ln_cont:=ln_cont+1;
           if ln_cont = 1000 then
             commit;
             ln_cont:=0;
           end if;
         
      EXCEPTION                             
          WHEN OTHERS THEN  
             lvError := sqlerrm;
        END;  
      END LOOP;
      commit;
    END IF;
end;
end pck_trx_concilia_detllam;
/
