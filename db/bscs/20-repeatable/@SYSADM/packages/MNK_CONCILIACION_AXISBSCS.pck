CREATE OR REPLACE package MNK_CONCILIACION_AXISBSCS is
  --===================================================
  -- Proyecto: Conciliaci�n de AXISvsBSCS
  -- Autores:    Alan Pab� R.
  -- Fecha:    21/Ago/2003
  -- Version:  1.0
  --===================================================
-- 1.2.1	Cuentas que tienen fecha de activaci�n diferentes entre AXIS y BSCS
-- 1.3.1	Cuentas activas en AXIS y se encuentran inactivas en BSCS
-- 1.3.2	Cuentas activas en BSCS y se encuentran inactivas en AXIS
-- 1.4.1	Cuentas activas en AXIS y no existen en BSCS
-- 1.4.2	Cuentas activas en BSCS y no existen en AXIS
-- 2.1.1	Tel�fonos que tienen fecha de activaci�n diferentes entre AXIS y BSCS
-- 2.2.1	Tel�fonos activos en AXIS y se encuentran inactivas en BSCS
-- 2.2.2	Tel�fonos activos en BSCS y se encuentran inactivas en AXIS
-- 2.3.1	Tel�fonos activos en AXIS y no existen en BSCS
-- 2.3.2	Tel�fonos activos en BSCS y no existen en AXIS
--
--============================== CUENTAS ================================--
--
  procedure MNP_CTA_ACT_FECH_DIF(p_fecha in varchar2,
  /*1.2.1.*/                     p_error out varchar2);
--
  procedure MNP_CTA_INACTBS_ACTAX(p_fecha in varchar2,
 /*1.3.1.*/                       p_error out varchar2);
--
 procedure MNP_CTA_ACTBS_INACTAX(p_fecha in varchar2,
 /*1.3.2.*/                      p_error out varchar2);
--
 procedure MNP_CTA_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*1.4.1.*/                       p_error out varchar2);
--
 procedure MNP_CTA_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*1.4.2.*/                       p_error out varchar2);
--
--============================== TELEFONOS ================================--
--
 procedure MNP_TELF_ACT_FECH_DIF(--p_telefono in varchar2,
                                 p_fecha in varchar2,
 /*2.1.1.*/                      p_error out varchar2);
--
 procedure MNP_TELF_INACTBS_ACTAX(p_fecha in varchar2,
 /*2.2.1.*/                       p_error out varchar2);
--
 procedure MNP_TELF_ACTBS_INACTAX(p_fecha in varchar2,
 /*2.2.2.*/                       p_error out varchar2);
--
 procedure MNP_TELF_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*2.3.1.*/                        p_error out varchar2);
--
 procedure MNP_TELF_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*2.3.2.*/                        p_error out varchar2);
--
end MNK_CONCILIACION_AXISBSCS;
/
CREATE OR REPLACE package body MNK_CONCILIACION_AXISBSCS is
lv_fecha          varchar2(20);
ln_contador       number;
ld_fecha          date;
  --===================================================
  -- Proyecto: Conciliaci�n de Base de Datos AXIS vs BSCS
  -- Autores:    Alan Pab� R.
  -- Fecha:    21/Agosto/2003
  -- Versi�n:  1.0
  --===================================================
--
--============================== CUENTAS ================================--
--
 procedure MNP_CTA_ACT_FECH_DIF(p_fecha in varchar2,
 /*1.2.1.*/                     p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Las cuentas activas en Axis y Bscs con
  --              fechas diferentes, son las que se muestran en el cursor.
  --
  --
  --===================================================
--
 cursor cuenta(p_fecha varchar2) is
   select /*+ RULE +*/ t.custcode cta_bscs, t.activa_cta activa_bscs,a.codigo_doc cta_axis, a.activa_cta activa_axis
   from   cta_act_bscs t, porta.cta_act_axis@axis a
   where  t.custcode = a.codigo_doc
   and    t.activa_cta <> a.activa_cta
   and    trunc(t.activa_cta) >= to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in cuenta(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_CTA_ACT_FECH_DIF;
--
 procedure MNP_CTA_INACTBS_ACTAX(p_fecha in varchar2,
 /*1.3.1.*/                      p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Las cuentas inactivas en BSCS
  --              vs las cuentas activas en AXIS, son las que se muestran en el cursor
  --
  --
  --===================================================
--
 cursor cuenta(p_fecha varchar2) is
   select t.custcode cta_bscs, t.cstype estado_bscs, a.codigo_doc cta_axis, a.estado estado_axis
   from   cta_act_bscs t, porta.cta_act_axis@axis a
   where  t.custcode = a.codigo_doc
   and    t.cstype = 'd'
   and    a.estado = 'A'
   and    trunc(t.activa_cta) >= to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in cuenta(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_CTA_INACTBS_ACTAX;
--
 procedure MNP_CTA_ACTBS_INACTAX(p_fecha in varchar2,
 /*1.3.2.*/                      p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Las cuentas activas en Bscs vs
  --              las inactivas en Axis, son las que se muestran en el cursor
  --
  --
  --===================================================
--
 cursor cuenta(p_fecha varchar2) is
   select t.custcode cta_bscs, t.cstype estado_bscs, a.codigo_doc cta_axis, a.estado estado_axis
   from   cta_act_bscs t, porta.cta_act_axis@axis a
   where  t.custcode = a.codigo_doc
   and    t.cstype = 'a'
   and    a.estado = 'I'
   and    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in cuenta(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_CTA_ACTBS_INACTAX;
--
 procedure MNP_CTA_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*1.4.1.*/                       p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Las cuentas activas en Axis
  --              que no existen en Bscs son las que muestra el cursor.
  --
  --
  --===================================================
--
 cursor cuenta(p_fecha varchar2) is
   select distinct a.codigo_doc cta_axis
   from   porta.cta_act_axis@axis a
   where  a.estado = 'A'
   and    trunc(a.activa_cta) = to_date(p_fecha,'dd/mm/yyyy')
   minus
   select distinct t.custcode cta_bscs
   from   cta_act_bscs t
   where    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in cuenta(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_CTA_NOEXISBS_ACTAX;
--
 procedure MNP_CTA_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*1.4.2.*/                       p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Las cuentas activas en Bscs
  --              y que no existen en Axis, son las que se muestran en el cursor
  --
  --
  --===================================================
--
 cursor cuenta(p_fecha varchar2) is
   select distinct t.custcode cta_bscs
   from   cta_act_bscs t
   where  t.cstype = 'a'
   and    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy')
   minus
   select distinct a.codigo_doc cta_axis
   from   porta.cta_act_axis@axis a
   where    trunc(a.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in cuenta(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_CTA_ACTBS_NOEXISAX;
--
--============================== TELEFONOS ================================--
--
 procedure MNP_TELF_ACT_FECH_DIF(--p_telefono in varchar2,
                                 p_fecha in varchar2,
 /*2.1.1.*/                      p_error out varchar2) is
   --===================================================
/*****#**********#************#**************#***************#*************#******/
/*  RETRICCION ANTES DE EJECUTAR ESTE PROCEDIMIENTO DEBE DESACTIVAR EL TRIGGER ***/
/*                                "CON_CONTRACT_ALL"                             */
/*  LUEGO VUELVA A ACTIVARLO.
/*****#**********#************#**************#***************#*************#******/
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Los tel�fonos activos en Axis y en Bscs
  --              con fechas diferentes, son los que se muestran en el cursor, para actualizar
  --              las fechas es necesario actualizar varias tablas cuentas, contratos y servicios
  --              se hace un update de los servicios, solo en el caso que el servicio tenga una fecha
  --              menor, esto con el fin de ser consistente, y no �perder las fechas ya registradas.
  --===================================================
--
 cursor telefono(p_fecha varchar2) is
   select /*+ RULE +*/ t.customer_id cod_cta,
                       t.co_id cod_key,
                       t.custcode cta_bscs,
                       t.activa_cta f_cta_bscs,
                       t.telefono telf_bscs,
                       t.activa_tel f_telf_bscs,
                       a.codigo_doc cta_axis,
                       a.activa_cta f_cta_axis,
                       a.id_servicio telf_axis,
                       a.activa_tef f_telf_axis
   from   telefonos_act_bscs t, porta.telefono_act_axis@axis a
   where  t.custcode = a.codigo_doc
   and    t.telefono = a.id_servicio
   and    trunc(t.activa_tel) <> trunc(a.activa_tef)
   and    t.ch_status = 'a'
   and    a.estado = 'A'
--   and    a.id_servicio = p_telefono --'9613524'
   and    trunc(a.activa_tef) >= to_date(p_fecha,'dd/mm/yyyy');
--   and    trunc(a.activa_tef) <= to_date('18/08/2003','dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in telefono(lv_fecha) loop
     begin
       ld_fecha := i.f_telf_axis;
--
----- CUENTA ---------
--
--
      update CUSTOMER_ALL
      set    CSACTIVATED        = i.f_cta_axis
      where  CUSTOMER_ID        = i.cod_cta
      and    trunc(CSACTIVATED) <> trunc(i.f_cta_axis);
--
      update CUSTOMER_ALL
      set    CSTYPE_DATE        = i.f_cta_axis
      where  CUSTOMER_ID        = i.cod_cta
      and    trunc(CSTYPE_DATE) < trunc(i.f_cta_axis);
--
      update CUSTOMER_ALL
      set    TMCODE_DATE        = i.f_cta_axis
      where  CUSTOMER_ID        = i.cod_cta
      and    trunc(TMCODE_DATE) < trunc(i.f_cta_axis);
--
      update CUSTOMER_ALL
      set    CSENTDATE          = i.f_cta_axis
      where  CUSTOMER_ID        = i.cod_cta
      and    trunc(CSENTDATE)   <> trunc(i.f_cta_axis);
--
      update CUSTOMER_ALL
      set    CSMODDATE          = i.f_cta_axis
      where  CUSTOMER_ID        = i.cod_cta
      and    trunc(CSMODDATE)   < trunc(i.f_cta_axis);
--
--
	     update CCONTACT
       set    CCENTDATE         = ld_fecha
       where  CUSTOMER_ID       = i.cod_cta
       and    trunc(CCENTDATE)  <> trunc(ld_fecha);
--	     and    ccseq < 0;
--
	     update CCONTACT
       set    CCMODDATE         = ld_fecha
       where  CUSTOMER_ID       = i.cod_cta
       and    trunc(CCMODDATE)  < trunc(ld_fecha);
--	     and    ccseq < 0;
--
----- CONTRATO --------
--
--
       update CONTRACT_ALL
       set    CO_SIGNED         = ld_fecha,
              CO_ACTIVATED      = ld_fecha,
              CO_ENTDATE        = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CO_ENTDATE) <> trunc(ld_fecha);
--
       update CONTRACT_ALL
       set    CO_MODDATE        = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CO_MODDATE) < trunc(ld_fecha);
--
       update CONTRACT_ALL
       set    CO_MODDATE        = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CO_MODDATE) < trunc(ld_fecha);
--
--
----- SERVICIOS -------
--
       update CONTR_SERVICES_CAP
       set    CS_ACTIV_DATE     = ld_fecha
       where  trunc(CS_ACTIV_DATE) < trunc(ld_fecha)
       and    SEQNO = 1
       and    CO_ID in ( select CO_ID from CONTRACT_ALL
                         where CUSTOMER_ID = i.cod_cta);
--
--
       update PROFILE_SERVICE
       set    ENTRY_DATE         = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(ENTRY_DATE)  < trunc(ld_fecha);
--
--
       update PR_SERV_STATUS_HIST
       set    VALID_FROM_DATE    = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(VALID_FROM_DATE) < trunc(ld_fecha);
--
       update PR_SERV_STATUS_HIST
       set    ENTRY_DATE         = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(ENTRY_DATE)  < trunc(ld_fecha);
--
--
       update PR_SERV_SPCODE_HIST
       set    VALID_FROM_DATE    = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(VALID_FROM_DATE) < trunc(ld_fecha);
--
--
       update PR_SERV_SPCODE_HIST
       set    ENTRY_DATE         = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(ENTRY_DATE)  < trunc(ld_fecha);
--
--
       update CONTR_DEVICES
       set    CD_ACTIV_DATE      = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CD_ACTIV_DATE)  < trunc(ld_fecha);
--
       update CONTR_DEVICES
       set    CD_VALIDFROM       = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CD_VALIDFROM)  < trunc(ld_fecha);

--
       update CONTR_DEVICES
       set    CD_ENTDATE         = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CD_ENTDATE)  < trunc(ld_fecha);
--
--
       update CONTRACT_HISTORY
       set    CH_VALIDFROM       = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(CH_VALIDFROM)< trunc(ld_fecha);
--
       update CONTRACT_HISTORY
       set    ENTDATE            = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(ENTDATE)     < trunc(ld_fecha);
--
--
    	 update RATEPLAN_HIST
       set    TMCODE_DATE        = ld_fecha
       where  CO_ID              = i.cod_key
       and    trunc(TMCODE_DATE) < trunc(ld_fecha);
--
--
	     update PARAMETER_VALUE
       set    PRM_VALID_FROM     = ld_fecha
       where  trunc(PRM_VALID_FROM) < trunc(ld_fecha)
       and    PRM_VALUE_ID in ( select PRM_VALUE_ID from PROFILE_SERVICE
                                where CO_ID  = i.cod_key );
--

       update PARAMETER_VALUE_BASE
       set    ENTRY_DATE         = ld_fecha
       where  trunc(ENTRY_DATE)  < trunc(ld_fecha)
       and    PRM_VALUE_ID in ( select PRM_VALUE_ID from PROFILE_SERVICE
                                where CO_ID  = i.cod_key );
--
--
	     update CONTR_VAS
       set    VALIDFROM         = ld_fecha
       where  CO_ID             = i.cod_key
       and    trunc(VALIDFROM)  < trunc(ld_fecha);
--
--
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 5) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_TELF_ACT_FECH_DIF;
--
 procedure MNP_TELF_INACTBS_ACTAX(p_fecha in varchar2,
 /*2.2.1.*/                       p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Los tel�fonos inactivos en Bscs vs
  --              los tel�fonos activos en Axis, son los que se muestran en el cursor.
  --
  --
  --===================================================
--
 cursor telefono(p_fecha varchar2) is
   select t.telefono telf_bscs, t.ch_status estado_bscs, a.id_servicio telf_axis, a.estado estado_axis
   from   telefonos_act_bscs t, porta.telefono_act_axis@axis a
   where  t.telefono = a.id_servicio
   and    t.ch_status = 'd'
   and    a.estado = 'A'
   and    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in telefono(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_TELF_INACTBS_ACTAX;
--
 procedure MNP_TELF_ACTBS_INACTAX(p_fecha in varchar2,
 /*2.2.2.*/                       p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Los tel�fonos activos en Bscs vs
  --              los tel�fonos inactivos en Axis, son los que se muestran en el cursor
  --
  --
  --===================================================
--
 cursor telefono(p_fecha varchar2) is
   select t.telefono telf_bscs, t.ch_status estado_bscs, a.id_servicio telf_axis, a.estado estado_axis
   from   telefonos_act_bscs t, porta.telefono_act_axis@axis a
   where  t.telefono = a.id_servicio
   and    t.ch_status = 'a'
   and    a.estado = 'I'
   and    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in telefono(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_TELF_ACTBS_INACTAX;
--
 procedure MNP_TELF_NOEXISBS_ACTAX(p_fecha in varchar2,
 /*2.3.1.*/                        p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Los tel�fonos activos en Axis
  --              que no existen en Bscs son los que se muestran en el cursor.
  --
  --
  --===================================================
--
 cursor telefono(p_fecha varchar2) is
   select distinct a.id_servicio telf_axis
   from   porta.telefono_act_axis@axis a
   where  a.estado = 'A'
   and    trunc(a.activa_cta) = to_date(p_fecha,'dd/mm/yyyy')
   minus
   select distinct t.telefono telf_bscs
   from   telefonos_act_bscs t
   where    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in telefono(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_TELF_NOEXISBS_ACTAX;
--
 procedure MNP_TELF_ACTBS_NOEXISAX(p_fecha in varchar2,
 /*2.3.2.*/                        p_error out varchar2) is
   --===================================================
  -- APA
  -- 21/Ago/2003
  -- Descripci�n: Conciliaci�n de BD AXIS vs BSCS. Los tel�fonos activos en Bscs que
  --              no existen en Axis, son los que muestra el cursor.
  --
  --
  --===================================================
--
 cursor telefono(p_fecha varchar2) is
   select distinct t.telefono telf_bscs
   from   telefonos_act_bscs t
   where  t.ch_status = 'a'
   and    trunc(t.activa_cta) = to_date(p_fecha,'dd/mm/yyyy')
   minus
   select distinct a.id_servicio telf_axis
   from   porta.telefono_act_axis@axis a
   where    trunc(a.activa_cta) = to_date(p_fecha,'dd/mm/yyyy');
--
 begin
   ln_contador := 0;
   lv_fecha := p_fecha;
   for i in telefono(lv_fecha) loop
     begin
       null;
/*     i.cta_bscs;
         i.activa_bscs;
         i.cta_axis;
         i.activa_axis;*/
       ln_contador := ln_contador + 1;
  		 if mod(ln_contador, 100) = 0 then
  			  commit;
  		 end if;
     exception
  	 when others then
  		 dbms_output.put_line('Ocurr�o un error al actulizar : ' || sqlerrm);
       p_error:=sqlerrm;
  	 end;
   end loop;
--
 commit;
 exception
   when others then
     p_error:=sqlerrm;
 end MNP_TELF_ACTBS_NOEXISAX;
end MNK_CONCILIACION_AXISBSCS;
/

