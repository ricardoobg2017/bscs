CREATE OR REPLACE package COK_REPORT_CAPAS_CBR is

    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_lvMensErr       out  varchar2);
    PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                         pdFech_fin in date);
    PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                         pdFech_fin in date,
                                         pd_lvMensErr out varchar2);
    PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date,
                             pd_lvMensErr out varchar2);
    FUNCTION RECUPERACICLO(pdfechacierreperiodo date) 
                           RETURN varchar2;
    FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                           pv_region      in varchar2,
                           pn_monto       out number,
                           pn_monto_favor out number,
                           pv_error       out varchar2)
                           RETURN NUMBER;
    PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date);
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_VALORFAVOR(pn_region in number,
                              pn_total   out number,
                              pv_error   out varchar2)
                              RETURN NUMBER;                            
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER;
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER;
    FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER;
    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER;
    PROCEDURE MAIN (pdFech_ini in date,
                    pdFech_fin in date);
    PROCEDURE INDEX2(pdFech_ini in date, pv_error out varchar2);

end COK_REPORT_CAPAS_CBR;
/
CREATE OR REPLACE package body COK_REPORT_CAPAS_CBR is

  -- Variables locales
  ge_error       exception;



    ------------------------------
    --     LLENA_BALANCES
    ------------------------------
    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;
    lJJ                    number;
    mes_recorrido_char          varchar2(2);
    nombre_campo                varchar2(20);

    
    mes_recorrido	              number;

    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lnDescuento            number;
    ldFecha                varchar2(20);
    lnMesEntre             number;
    lnDiff                 number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;

    BEGIN

       -- se llenan las facturas en los balances desde la mas vigente
       lnMes := 13;
       for i in cur_periodos loop

           if i.cierre_periodo <= pdFechCierrePeriodo then

               lnMes := lnMes - 1;
               if lnMes <= 0 then
                  lnMes := 1;
               end if;

               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then
                   source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor), sum(descuento)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);
                      dbms_sql.column_value(source_cursor, 3, lnDescuento);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||' - '||lnDescuento||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);
               else
                   source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);
               end if;
           end if;    -- if i.cierre_periodo <= pdFechCierrePeriodo
       end loop;
       
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';
       select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd')) into lnMesEntre from dual;
       lnMesEntre := lnMesEntre + 1;
       

       while ldFecha <= '2003/06/24'
       loop
           nombre_campo := 'balance_1';
           /*if pdFechCierrePeriodo <= to_date('2003/12/24','yyyy/MM/dd') then
               -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
               if  length(mes_recorrido) < 2 then
                   mes_recorrido_char := '0'||to_char(mes_recorrido);
               end if;
               nombre_campo    := 'balance_'|| mes_recorrido;
           else
               if (lnMesEntre-lJJ) >= 12 then
                  nombre_campo := 'balance_1';
               else
                  nombre_campo := 'balance_'||to_char(mes_recorrido-(lnMesEntre-12));
               end if;
           end if;*/

           -- se selecciona los saldos
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
                          ' from orderhdr_all'||
                          ' where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                          ' and   ohstatus   = ''IN''';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

           dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           dbms_sql.define_column(source_cursor, 2,  lnValor);
           rows_processed := Dbms_sql.execute(source_cursor);

           lII:=0;
           loop
              if dbms_sql.fetch_rows(source_cursor) = 0 then
                 exit;
              end if;
              dbms_sql.column_value(source_cursor, 1, lnCustomerId);
              dbms_sql.column_value(source_cursor, 2, lnValor);

              -- actualizo los campos de la tabla final
              lvSentencia:='update ' ||pdNombre_Tabla ||
                            ' set '||nombre_campo||'= '||nombre_campo||' + '||lnValor||
                            ' where customer_id='||lnCustomerId;
              EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
              lII:=lII+1;
              if lII = 3000 then
                 lII := 0;
                 commit;
              end if;
           end loop;
           dbms_sql.close_cursor(source_cursor);
           commit;

           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop;       
       
    END;

  PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                      pdFech_fin in date) is

    --val_fac_          varchar2(20);
    lv_sentencia	    varchar2(2000);
    lv_sentencia_upd  varchar2(2000);
    --v_sentencia       varchar2(2000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    --nombre_campo      varchar2(20);
    lvMensErr         varchar2(2000);
    lII               number;

    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    --v_cursor_asigna   number;
    v_Dummy           number;
    --v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;

    cursor cur_disponible(pCustomer_id number) is
    select fecha, valor, compania, tipo
    from co_disponible
    where customer_id = pCustomer_id
    order by customer_id, fecha, tipo desc;

    BEGIN

       if pdFech_ini >= to_date('24/01/2004', 'dd/MM/yyyy') then
           mes := '12';
           nro_mes := 12;
       else
           mes := substr(to_char(pdFech_ini,'ddmmyyyy'), 3, 2) ;
           nro_mes := to_number(mes);
       end if;
       lv_campos := '';
       for lII in 1..nro_mes loop
           lv_campos := lv_campos||' balance_'||lII||',';
       end loop;
       lv_campos := substr(lv_campos,1,length(lv_campos)-1);

       -- se trunca la tabla final de las transacciones para el reporte
       -- de capas en el form
       lv_sentencia := 'truncate table co_disponible2';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);
       lv_sentencia := 'truncate table co_disponible3';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);

       --v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;

       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       lv_sentencia:= ' select c.rowid, customer_id, 0, '|| lv_campos||
                      ' from CO_BALANCEO_CREDITOS c';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30 );
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id );
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible  );

       if    nro_mes >= 1 then
             dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       end if;
       if    nro_mes >= 2 then
             dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       end if;
       if    nro_mes >= 3 then
             dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       end if;
       if    nro_mes >= 4 then
             dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       end if;
       if    nro_mes >= 5 then
             dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       end if;
       if    nro_mes >= 6 then
             dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       end if;
       if    nro_mes >= 7 then
             dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       end if;
       if    nro_mes >= 8 then
             dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       end if;
       if    nro_mes >= 9 then
             dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       end if;
       if    nro_mes >= 10 then
             dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       end if;
       if    nro_mes >= 11 then
             dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       end if;
       if    nro_mes = 12 then
             dbms_sql.define_column(v_cursorId, 15, val_fac_12);
       end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);

       lII := 0;
       Loop

          total_deuda_cliente := 0;

          -- si no tiene datos sale
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;

          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );

          -- se asignan los valores de la factura
          if    nro_mes >= 1 then
               dbms_sql.column_value(v_cursorId, 4, val_fac_1);
          end if;
          if    nro_mes >= 2 then
               dbms_sql.column_value(v_cursorId, 5, val_fac_2);
          end if;
          if    nro_mes >= 3 then
               dbms_sql.column_value(v_cursorId, 6, val_fac_3);
          end if;
          if    nro_mes >= 4 then
               dbms_sql.column_value(v_cursorId, 7, val_fac_4);
          end if;
          if    nro_mes >= 5 then
               dbms_sql.column_value(v_cursorId, 8, val_fac_5);
          end if;
          if    nro_mes >= 6 then
               dbms_sql.column_value(v_cursorId, 9, val_fac_6);
          end if;
          if    nro_mes >= 7 then
               dbms_sql.column_value(v_cursorId, 10, val_fac_7);
          end if;
          if    nro_mes >= 8 then
               dbms_sql.column_value(v_cursorId, 11, val_fac_8);
          end if;
          if    nro_mes >= 9 then
               dbms_sql.column_value(v_cursorId, 12, val_fac_9);
          end if;
          if    nro_mes >= 10 then
               dbms_sql.column_value(v_cursorId, 13, val_fac_10);
          end if;
          if    nro_mes >= 11 then
               dbms_sql.column_value(v_cursorId, 14, val_fac_11);
          end if;
          if    nro_mes = 12 then
               dbms_sql.column_value(v_cursorId, 15, val_fac_12);
          end if;

          -- extraigo los creditos
          wc_disponible := 0;
          for i in cur_disponible(wc_customer_id) loop
              wc_disponible := wc_disponible + i.valor;

              -- se setea variable de actualizaci�n
              lv_sentencia_upd := 'update CO_BALANCEO_CREDITOS set ';

              if nro_mes >= 1 then
                     aux_val_fact   := nvl(val_fac_1,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact;
                         -- se coloca el credito en la tabla final si la fecha es
                         -- igual a la que se esta solicitando el reporte
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                               insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;                         
                         end if;
                         end if;
                         wc_disponible := 0;
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , ';
              end if;

              if nro_mes >= 2 then
                     aux_val_fact   := nvl(val_fac_2,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 3 then
                     aux_val_fact   := nvl(val_fac_3,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                    lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 4 then
                     aux_val_fact   := nvl(val_fac_4,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 5 then
                     aux_val_fact   := nvl(val_fac_5,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
               end if;
               if    nro_mes >= 6 then
                     aux_val_fact   := nvl(val_fac_6,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else   
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 7 then
                     aux_val_fact   := nvl(val_fac_7,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 7 then 
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 7 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 8 then
                     aux_val_fact   := nvl(val_fac_8,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 9 then
                     aux_val_fact   := nvl(val_fac_9,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 10 then
                     aux_val_fact   := nvl(val_fac_10,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 11 then
                     aux_val_fact   := nvl(val_fac_11,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes = 12 then
                     aux_val_fact   := nvl(val_fac_12,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                               -- se inserta los pagos a tabla de pagos que afectan a la capa
                               insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
               end if;

               lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
               lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';
               EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
               
               commit;

               -- si el valor de la factura del mes que se esta analizando las capas
               -- esta completamente saldado entonces ya no se buscan mas creditos
               -- y se procede a salir del bucle de los creditos para seguir con el
               -- siguiente cliente
               if nro_mes = 6 then
                  if val_fac_6 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 7 then
                  if val_fac_7 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 8 then
                  if val_fac_8 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 9 then
                  if val_fac_9 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 10 then
                  if val_fac_10 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 11 then
                  if val_fac_11 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 12 then
                  if val_fac_12 = 0 then
                     exit;
                  end if;
               end if;

          end loop;    -- fin del loop de los disponible en la tabla disponible

       end loop; -- 1. Para extraer los datos del cursor

       commit;

       dbms_sql.close_cursor(v_CursorId);


       -- se actualizan a positivo los creditos en la tabla final
       lv_sentencia := 'update co_disponible2 set valor = valor*-1 where valor < 0';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);
       commit;


  END BALANCE_EN_CAPAS_BALCRED;


  ---------------------------------------------------------------------------
  --    BALANCE_EN_CAPAS_CREDITOS
  --    Balanceo de creditos
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                       pdFech_fin in date,
                                       pd_lvMensErr out varchar2) IS
    lvSentencia     VARCHAR2(5000);
    lvMensErr       VARCHAR2(3000);
    lII             NUMBER;
    lnExisteTabla   NUMBER;
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;

    lnCustomerId           number;
    lnValor                number;
    lnCostDesc             number;
    ldFecha                date;
    lnTotCred              number;
    lnCompany              number;
    lvPeriodos             varchar2(2000);
    lnMes                  number;
    leError                exception;

    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and to_char(lrstart, 'dd') <> '01';    --invoices since July

  BEGIN

       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       for i in c_periodos loop
           lvPeriodos := lvPeriodos||' to_date('''||to_char(i.cierre_periodo,'dd/MM/yyyy')||''',''dd/MM/yyyy''),';
       end loop;
       lvPeriodos := substr(lvPeriodos,1,length(lvPeriodos)-1);

       source_cursor := DBMS_SQL.open_cursor;
       lvSentencia := 'select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company'||
                      ' from orderhdr_all o, customer_all d, costcenter j'||
                      ' where o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')'||
                      ' and o.ohstatus  = ''CM'''||
                      ' and o.customer_id = d.customer_id'||
                      ' and j.cost_id = d.costcenter_id'||
                      ' and not o.ohentdate in ('||lvPeriodos||')'||
                      ' group by j.cost_desc, o.customer_id, ohentdate'||
                      ' having sum(ohinvamt_doc)<>0';
       Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

       dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
       dbms_sql.define_column(source_cursor, 2,  ldFecha);
       dbms_sql.define_column(source_cursor, 3,  lnTotCred);
       dbms_sql.define_column(source_cursor, 4,  lnCompany);
       rows_processed := Dbms_sql.execute(source_cursor);

       lII := 0;
       loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
               exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2,  ldFecha);
            dbms_sql.column_value(source_cursor, 3,  lnTotCred);
            dbms_sql.column_value(source_cursor, 4,  lnCompany);
            
            insert into co_disponible values(lnCustomerId, ldFecha, lnTotCred, lnCompany,'C');
            
            lII:=lII+1;
            if lII = 2000 then
               lII := 0;
               commit;
            end if;
       end loop;
       dbms_sql.close_cursor(source_cursor);
       commit;


       ------------------------------------------------------
       -- se insertan los creditos de la vista
       -- son los creditos que el facturador los pone como 24
       ------------------------------------------------------
       for i in c_periodos loop
           lnMes := to_number(to_char(i.cierre_periodo, 'MM'));

           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := 'SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)'||
                          ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                          ' WHERE tipo = ''006 - CREDITOS'''||
                          ' GROUP BY customer_id, cost_desc';
           dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           dbms_sql.define_column(source_cursor, 2,  lnValor);
           dbms_sql.define_column(source_cursor, 3,  lnCostDesc);
           rows_processed := Dbms_sql.execute(source_cursor);

           lII := 0;
           loop
              if dbms_sql.fetch_rows(source_cursor) = 0 then
                 exit;
              end if;
              dbms_sql.column_value(source_cursor, 1, lnCustomerId);
              dbms_sql.column_value(source_cursor, 2, lnValor);
              dbms_sql.column_value(source_cursor, 3, lnCostDesc);
              
              insert into co_disponible values(lnCustomerId, i.cierre_periodo, lnValor, lnCostDesc,'C');
              
              lII:=lII+1;
              if lII = 2000 then
                 lII := 0;
                 commit;
              end if;
              
           end loop;
           commit;
           dbms_sql.close_cursor(source_cursor);

       end loop;

       -- se cambia el signo de los creditos para luego hacer el balanceo
       update CO_DISPONIBLE set valor = valor*-1 where valor < 0;
       commit;
       
       -------------------
       -- se balancea...
       -------------------
       COK_REPORT_CAPAS.balance_en_capas_balcred(pdFech_ini, pdFech_fin);
       
       
       -- se respalda la informacion para auditoria de pagos
       lvSentencia := 'create table co_cap_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy')||' as select * from co_disponible2';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;
       
  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
    
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'balance_en_capas_creditos: ' || sqlerrm;

  END BALANCE_EN_CAPAS_CREDITOS;

  ------------------------------------------------------------------
  -- BALANCE_EN_CAPAS
  -- Procedimiento principal para balanceo de los creditos
  -- para reportes en capas, actualiza pagos y creditos desde
  -- la deuda mas antigua hacia la mas nueva
  -- los creditos los realiza uno a uno para chequear el informe en capas
  ------------------------------------------------------------------

  PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date,
                             pd_lvMensErr out varchar2) IS

    lvSentencia            VARCHAR2(5000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    lnNumErr               NUMBER;
    lvMensErr              VARCHAR2(3000);
    lnExito                NUMBER;
    lnPagos                NUMBER;
    lnCreditos             NUMBER;
    lnSaldoPago            NUMBER;
    lnSaldoCredito         NUMBER;
    lnCustomerId           number;
    lnValor                number;
    
    ciclo               VARCHAR2(2);
    mes		              VARCHAR2(2);
    nro_mes		          NUMBER;
    anio                VARCHAR2(4);
    mes_recorrido       NUMBER;
    mes_recorrido_char  varchar2(2);
    nombre_campo        varchar2(20);
    lnFool              NUMBER;
    lII                 NUMBER;
    leError             exception;

  BEGIN
        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_BALANCEO_CREDITOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla = 1 then
           lvSentencia := 'truncate table CO_BALANCEO_CREDITOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        else
            --se crea la tabla
            lvSentencia := 'create table CO_BALANCEO_CREDITOS'||
                          '( CUSTOMER_ID         NUMBER,'||
                          COK_PROCESS_REPORT.set_campos_mora(pdFech_ini)||
                          '  FECHA_CANCELACION   DATE )'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia := 'alter table CO_BALANCEO_CREDITOS'||
                          '   add constraint PKCUIDBA  primary key (CUSTOMER_ID)'||
                              '  using index'||
                              '  tablespace DATA'||
                              '  pctfree 10'||
                              '  initrans 2'||
                              '  maxtrans 255'||
                              '  storage'||
                              '  (initial 1M'||
                              '    next 1M'||
                              '    minextents 1'||
                              '    maxextents unlimited'||
                              '    pctincrease 0)';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to PUBLIC';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to READ';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;
       
       --------------------------------------------
       -- se llena la informaci�n de los clientes
       --------------------------------------------
       lvSentencia := 'insert /*+ APPEND */ into CO_BALANCEO_CREDITOS'||
                      ' NOLOGGING (customer_id) '||
                      ' select id_cliente from co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy')||' where cuenta is not null';
       /*lvSentencia := 'insert into CO_BALANCEO_CREDITOS'||
                      ' NOLOGGING (customer_id) '||
                      ' select a.customer_id'|| 
                      ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma'||
                      ' where a.billcycle = ma.id_ciclo_admin'||
                      ' and ci.id_ciclo = ma.id_ciclo'||
                      ' and a.paymntresp  = ''X'''||
                      ' and ci.dia_ini_ciclo = '''||to_char(pdFech_ini,'dd')||'''';*/
       
       --ciclo := COK_REPORT_CAPAS.recuperaciclo(pdFech_ini);
       --lvSentencia := 'insert /*+ APPEND */ into CO_BALANCEO_CREDITOS NOLOGGING (customer_id) '||
       --            ' select customer_id from customer_all'||
       --            ' where paymntresp  = ''X''' ||
       --            ' and billcycle ='||ciclo;
                   
       --lvSentencia:='insert /*+ APPEND */ into CO_BALANCEO_CREDITOS NOLOGGING (customer_id) '||
       --             'select customer_id from customer_all a where a.paymntresp=''X''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;

       --------------------------------------------
       -- se llenan los campos de balances
       --------------------------------------------
       COK_REPORT_CAPAS.Llena_Balances(pdFech_ini,'co_balanceo_creditos',lvMensErr);

       
       -- se inicializa la tabla de disponible
       lvSentencia := 'truncate table CO_DISPONIBLE';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);       
       
       --------------------------------------------
       -- se llenan los pagos
       --------------------------------------------
       insert /*+ APPEND */ into CO_DISPONIBLE NOLOGGING (customer_id, fecha, valor, compania, tipo)
       select ca.customer_id, ca.caentdate, ca.cachkamt_pay, cu.costcenter_id, 'P'
       from   cashreceipts_all ca, customer_all cu
       where  ca.customer_id = cu.customer_id
       and    caentdate  < pdFech_fin
       and    cachkamt_pay <> 0;       
       commit;

       -------------------------------------------------------------------
       -- se llenan creditos en co_disponible y luego de balancean
       -------------------------------------------------------------------
       balance_en_capas_creditos(pdFech_ini, pdFech_fin,lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;

       lnFool := 1;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
    
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'balance_en_capas: ' || sqlerrm;

  END BALANCE_EN_CAPAS;

  ------------------------------------------------------
  -- LLENA_TEMPORAL: Usado por form para la impresi�n
  --                 de reporte
  ------------------------------------------------------
  PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date) IS
    lvSentencia     VARCHAR2(1000);
    lvMensErr       VARCHAR2(1000);
  BEGIN
       lvSentencia:='truncate table cobranzas_capas_tmp';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

       lvSentencia:='insert into cobranzas_capas_tmp '||
                    'select * from CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy');
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;


  END LLENA_TEMPORAL;

  -------------------------------------------------------------------
  -- Funci�n que retorna el billcyclo respectivo de acuerdo al cierre
  -------------------------------------------------------------------
  FUNCTION RECUPERACICLO(pdfechacierreperiodo date) return varchar2 is
 
  billciclo varchar2(2);
    
  BEGIN

    select billcycle into billciclo from co_billcycles where fecha_emision = pdfechacierreperiodo;
    
    return(billciclo);  
 
  END recuperaciclo;

  --------------------------------------------------------------
  -- Funci�n que calcula el total facturado por per�odo y regi�n
  --------------------------------------------------------------
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pn_monto_favor out number,
                         pv_error       out varchar2)
                         RETURN NUMBER IS

    lvSentencia     varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    rows_fetched    integer;
    lb_notfound     boolean;
    ln_retorno      number;
    lnMonto         number;
    lnMontoFavor    number;
    lnTotal         number;
    lvMensErr       varchar2(2000);
    lnExito         number;
    lvCampo         varchar2(2000);

    BEGIN

       -- se coloca el valor facturado para el reporte de capas de Junio
       if pd_facturacion = to_date('24/06/2003', 'dd/MM/yyyy') then
           if pv_region = 'GYE' then
              pn_monto :=  2746616.87;    -- valores entregados del FAS Junio
           else
              pn_monto :=  1798323.52;    -- valores entregados del FAS Junio
           end if;
           ln_retorno := 1;
           return(ln_retorno);
       else
           --CBR: 07/07/2004
           --se cambia para que el total facturado sea leido del detalle de clientes
           source_cursor := DBMS_SQL.open_cursor;
           if pd_facturacion = to_date('24/07/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_7';
           elsif pd_facturacion = to_date('24/08/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_8';
           elsif pd_facturacion = to_date('24/09/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_9';
           elsif pd_facturacion = to_date('24/10/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_10';
           elsif pd_facturacion = to_date('24/11/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_11';
           else
              lvCampo:='balance_12';              
           end if;
           
           lvSentencia := ' select sum('||lvCampo||')'||
                             ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                             ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                             ' and '||lvCampo||' > 0';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnMonto);
           rows_processed := Dbms_sql.execute(source_cursor);     
           rows_fetched := dbms_sql.fetch_rows(source_cursor);
           dbms_sql.column_value(source_cursor, 1, lnMonto);
           dbms_sql.close_cursor(source_cursor);

           -- se extraen los saldos a favor
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := ' select sum('||lvCampo||')'||
                         ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                         ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                         ' and '||lvCampo||' <= 0';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnMontoFavor);
           rows_processed := Dbms_sql.execute(source_cursor);     
           rows_fetched := dbms_sql.fetch_rows(source_cursor);
           dbms_sql.column_value(source_cursor, 1, lnMontoFavor);
           dbms_sql.close_cursor(source_cursor);
           
           -- se devuelve datos al exterior                                                
           pn_monto := lnMonto;
           pn_monto_favor := lnMontoFavor;
           ln_retorno := 1;
           return(ln_retorno);           
           
       end if;
      
       commit;
      
    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_FACTURA on no_data_found: '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_FACTURA on when others: '||sqlerrm;
    END TOTAL_FACTURA;

    ---------------------------------------------
    --  funci�n de totales de pagos en efectivo
    ---------------------------------------------
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS
    lnPago1    number;
    lnPago2    number;

    BEGIN

      select sum(valor)
      into pn_total
      from co_disponible2
      where  trunc(fecha) = pd_fecha
      and tipo = 'P'
      and compania = pn_region;      

      return 1;

    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
    END TOTAL_EFECTIVO;

    -------------------------------------------------
    --funci�n de totales de pagos en notas de credito
    -------------------------------------------------
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN

      select sum(valor)
      into pn_total
      from co_disponible2
      where fecha = pd_fecha
      and tipo = 'C'
      and compania = pn_region;

      return 1;
    EXCEPTION
        when no_data_found then
        pn_total := 0;
        return(0);
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
      when others then
        return(-1);
        pn_total := 0;
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
    END TOTAL_CREDITO;


    --funci�n de totales de pagos en efectivo
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN


      select /*+ rule +*/ sum(a.cachkamt_pay)
      into pn_total
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      and    t.ohduedate    >= pd_periodo
      and    a.cacostcent   = pn_region
      and    t.ohstatus     = 'CO'
      and    t.ohentdate    = pd_periodo
      --and    t.ohentdate    >= pd_periodo
      and    a.carecdate    >= pd_fecha;

      return 1;
    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
    END TOTAL_OC;

    
    -- funci�n que acumula los valores de valores a favor
    -- que son encontrados antes del inicio del periodo
    -- y que estaban afectando a la cabecera
    FUNCTION TOTAL_VALORFAVOR(pn_region in number,
                              pn_total   out number,
                              pv_error   out varchar2)
                              RETURN NUMBER IS
    BEGIN

      select sum(valor)
      into pn_total
      from co_disponible3
      where compania = pn_region;      

      return 1;

    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
    END TOTAL_VALORFAVOR;
    

    -- Funci�n que calcula el porcentaje de los pagos y notas de cr�dito
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER IS
    ln_retorno   number;

    BEGIN
      pn_porc    := ROUND((pn_amortizado*100)/pn_total,2);
      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when others then
        return(0);
        pv_error := 'FUNCION: PORCENTAJE '||sqlerrm;
    END PORCENTAJE;

    -- Funci�n que calcula la cantidad de facturas amortizadas diariamente
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS


    lvSentencia     varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    rows_fetched    integer;
                           
      ln_retorno   number;
      lb_notfound  boolean;
      ld_fecha     date;
      ln_cancelado number;
      ln_facturas  number;

    BEGIN
    
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia := 'select count(*)'||
                   ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                   ' where compania = '||pn_region||
                   ' and balance_12 > 0';
  
    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1,  ln_facturas);
    rows_processed := Dbms_sql.execute(source_cursor);     
    rows_fetched := dbms_sql.fetch_rows(source_cursor);
    dbms_sql.column_value(source_cursor, 1, ln_facturas);
    dbms_sql.close_cursor(source_cursor);
    
      select count(*) into ln_cancelado
      from   co_balanceo_creditos
      where  fecha_cancelacion >= pd_facturacion
      and    fecha_cancelacion <= pd_fecha
      and    compania = pn_region;
      
      pn_amortizado:=ln_facturas-ln_cancelado;
      
      return(1);

    EXCEPTION
      when no_data_found then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(0);
      when others then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(-1);
    END CANT_FACTURAS;

    FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(2000);

    BEGIN
           lvSentencia := 'create table CO_CABCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3), '||
                          '  PERIODO_FACTURACION DATE, '||
                          '  MONTO_FACTURACION   NUMBER, '||
                          '  CANT_FACTURAS       NUMBER, '||
                          '  SALDO_FAVOR         NUMBER, '||
                          '  PAGOS_FAVOR         NUMBER, '||
                          '  VALOR_FAVOR         NUMBER) '||
                          'tablespace DATA '||
                          '  pctfree 10 '||
                          '  pctused 40 '||
                          '  initrans 1 '||
                          '  maxtrans 255 '||
                          '  storage '||
                          '  (initial 4K '||
                          '    next 1K '||
                          '    minextents 1 '||
                          '    maxextents 2 '||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_CABCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_CABCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA_CAB;    
    

    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);

    BEGIN
           lvSentencia := 'create table CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3),'||
                          '  PERIODO_FACTURACION DATE,'||
                          '  FECHA		      DATE,'||
                          '  TOTAL_FACTURA       NUMBER,'||
                          '  MONTO               NUMBER,'||
                          '  PORC_RECUPERADO     NUMBER(8,2),'||
                          '  DIAS                NUMBER,'||
                          '  EFECTIVO            NUMBER,'||
                          '  PORC_EFECTIVO       NUMBER(8,2),'||
                          '  CREDITOS            NUMBER,'||
                          '  PORC_CREDITOS       NUMBER(8,2),'||
                          '  ACUMULADO           NUMBER,'||
                          '  PAGOS_CO            NUMBER)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.REGION is ''Centro de costo puede ser GYE o UIO.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PERIODO_FACTURACION is ''Cierre del periodo de facturaci�n, siempre 24.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.MONTO is ''Monto amortizado de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.EFECTIVO is ''Todos los pagos en efectivo.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.CREDITOS is ''Todos los pagos en notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_CREDITOS is ''Porcentaje de las notas de cr�dito vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.ACUMULADO is ''Total de pagos y notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PAGOS_CO is ''Overpayments.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'alter table CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          ' add constraint PK'||to_char(pdFechaPeriodo,'ddMMyyyy')||' primary key (REGION,FECHA) '||
                          'using index '|| 
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 120K '||
                          '  next 104K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);               
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_01 on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,REGION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_02 on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);


           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;

/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFech_ini in date,
               pdFech_fin in date) IS

    -- variables
    lvSentencia     VARCHAR2(2000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(2000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales facturados en el periodo
    lnTotalFavor    NUMBER;        --variable para totales facturados en el periodo a favor
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lnOc            NUMBER;
    lnValorFavor    NUMBER;

    -- cursores
    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart = pdFech_ini
       and to_char(lrstart, 'dd') <> '01';

BEGIN

    --loop for any period
    for p in c_periodos loop

        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:=COK_REPORT_CAPAS.crea_tabla(p.cierre_periodo, lvMensErr);
        else
           --si no existe se trunca la tabla para colocar los datos nuevamente
           lvSentencia := 'truncate table CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;

        -- proceso de balance de deuda del cliente y ajuste de creditos
        balance_en_capas(pdFech_ini, pdFech_fin,lvMensErr);

        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:=COK_REPORT_CAPAS.crea_tabla_cab(p.cierre_periodo, lvMensErr);
        else        
          -- se trunca tabla de cabecera
          lvSentencia := 'truncate table CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy');
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;

        -- loop for the costcenter
        for c in 1..2 loop
            if c=1 then
               lvCostCode:='GYE';
            else
               lvCostCode:='UIO';
            end if;

            -- se calcula el total facturado hasta el cierre
            lnExito:=COK_REPORT_CAPAS.total_factura(p.cierre_periodo, lvCostCode, lnTotal, lnTotalFavor, lvMensErr);
            
            -- se calcula los montos a favor que afectan a mi facturacion pero no se muestra en el detalle
            lnExito:=COK_REPORT_CAPAS.TOTAL_VALORFAVOR(c, lnValorFavor, lvMensErr);
            
            -- se inserta informaci�n en la cabecera para reporte en forms
            -- se valida a partir de Agosto2004 se agrega a monto el valor adelantado obtenido en lnValorFavor
            if p.cierre_periodo >= to_date('24/08/2004', 'dd/MM/yyyy') then
                lvSentencia := 'insert into CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' ('''||lvCostCode||''', to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||lnTotal||' + '||lnValorFavor||','||lnTotalFavor||','||lnValorFavor||')';
                
                lnTotal := lnTotal + lnValorFavor;
                                
            else
                lvSentencia := 'insert into CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' ('''||lvCostCode||''', to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||lnTotal||','||lnTotalFavor||','||lnValorFavor||')';
            end if;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            commit;
            
            -- se inicializan variables acumuladoras de efectivo y credito
            lnAcuEfectivo:=0;
            lnAcuCredito:=0;

            ldFech_dummy:=pdFech_ini;
            lnDia:=0;
            
            -- se inicializa el credito
            lnCredito := 0;
                                                                           
            -- loop for each date from the begining
            while ldFech_dummy <= pdFech_fin loop
                   lnDia:=lnDia+1;
                   -- se calcula el total de pagos en efectivo
                   lnExito:=COK_REPORT_CAPAS.total_efectivo(ldFech_dummy, c, p.cierre_periodo, lnEfectivo, lvMensErr);
                   lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo,0);
                   -- se valida para que el d�a del periodo no coja los creditos
                   -- ya que estan siendo tomados en cuenta a nivel de cabecera
                   if ldFech_dummy <> pdFech_ini then
                      -- se calcula el total de creditos
                      lnExito:=COK_REPORT_CAPAS.total_credito(ldFech_dummy, c, p.cierre_periodo, lnCredito, lvMensErr);
                   end if;
                   lnAcuCredito := lnAcuCredito + nvl(lnCredito,0);
                   -- se calcula la cantidad de facturas amortizadas
                   lnExito:=COK_REPORT_CAPAS.cant_facturas(p.cierre_periodo, c, ldFech_dummy, lnTotFact, lvMensErr);
                   -- se calcula el porcentaje recuperado del total
                   lnExito:=COK_REPORT_CAPAS.porcentaje(lnTotal, lnAcuEfectivo+lnAcuCredito, lnPorc, lvMensErr);
                   lnExito:=COK_REPORT_CAPAS.porcentaje(lnTotal, lnAcuEfectivo, lnPorcEfectivo, lvMensErr);
                   lnExito:=COK_REPORT_CAPAS.porcentaje(lnTotal, lnAcuCredito, lnPorcCredito, lvMensErr);
                   -- se calculan los oc
                   lnExito:=COK_REPORT_CAPAS.total_oc(ldFech_dummy, c, p.cierre_periodo, lnOc, lvMensErr);
                   -- se inserta a la tabla el primer registro
                   lnMonto:=lnTotal-lnAcuEfectivo-lnAcuCredito;
                   lnAcumulado:=lnAcuEfectivo+lnAcuCredito;
                   lvSentencia := 'insert into CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||' (region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) '||
                                  'values ('''||
                                  lvCostCode||''','||
                                  'to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                                  'to_date('''||to_char(ldFech_dummy,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                                  lnTotFact||','||
                                  lnMonto||','||
                                  lnPorc||','||
                                  lnDia||','||
                                  lnAcuEfectivo||','||
                                  lnPorcEfectivo||','||
                                  lnAcuCredito||','||
                                  nvl(lnPorcCredito,0)||','||
                                  lnAcumulado||','||
                                  nvl(lnOc,0)||')';
                   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                   commit;

                ldFech_dummy := ldFech_dummy + 1;
            end loop;
        end loop;

    end loop;

END MAIN;

--------------------------------------------------------------
-- INDEX
-- Procedimiento principal para ejecuci�n de reportes de capas
--------------------------------------------------------------

PROCEDURE INDEX2(pdFech_ini in date, pv_error out varchar2) IS

    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= pdFech_ini
       and to_char(lrstart, 'dd') <> '01';
           
      /*select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= pdFech_ini;*/
      
      lvMensErr       varchar2(1000);
      
BEGIN
     lvMensErr := '';
     if pdFech_ini < to_date('24/07/2003','dd/MM/yyyy') then
        lvMensErr := 'Ha ingresado una fecha menor a 24/07/2003';
        pv_error := lvMensErr;
        return;
     end if;
     
     for i in c_periodos loop
         COK_REPORT_CAPAS.main(i.cierre_periodo, sysdate-1);
     end loop;

END INDEX2;


end COK_REPORT_CAPAS_CBR;
/

