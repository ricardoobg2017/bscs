create or replace package GSI_CARGA_VISTAS is

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : Mejoramiento en el proceso de facturacion
  -----------------------------------------------------------------------------------------------------------

  PROCEDURE P_EJEC_HILOS(PN_ID_TABLA IN NUMBER,
                         PV_FECHA    IN VARCHAR2,
                         PV_ESTADO   IN VARCHAR2,
                         PV_ERROR    OUT VARCHAR2);

  PROCEDURE P_CICLO_INACT(PV_FECHA    IN VARCHAR2,
                          PV_ID_TABLA IN VARCHAR2,
                          PV_VALOR    OUT VARCHAR2,
                          PV_ERROR    OUT VARCHAR2);

  PROCEDURE P_VISTA_MAIL(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA      IN VARCHAR2,
                         PV_ERROR      OUT VARCHAR2);

  PROCEDURE P_DEPURA_SYN(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA      IN VARCHAR2,
                         PV_ERROR      OUT VARCHAR2);

  PROCEDURE P_ACT_PERIOD(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA IN VARCHAR2,
                         PV_ERROR OUT VARCHAR2);
  PROCEDURE P_EJEC_PROCESO	(PN_ID_TABLA IN NUMBER,
                             PV_FECHA    IN VARCHAR2,
                             PV_ESTADO   IN VARCHAR2,
                             PV_ERROR    OUT VARCHAR2);                         

end GSI_CARGA_VISTAS;
/
CREATE OR REPLACE PACKAGE BODY GSI_CARGA_VISTAS IS

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE EJECUTA EL SHELL PARA INVOCAR AL JAVA
  ----------------------------------------------------------------------------------------------------------

  PROCEDURE P_EJEC_HILOS(PN_ID_TABLA IN NUMBER,
                         PV_FECHA    IN VARCHAR2,
                         PV_ESTADO   IN VARCHAR2,
                         PV_ERROR    OUT VARCHAR2) IS
  
    LV_COMANDO    VARCHAR2(500);
    LV_SALIDA     VARCHAR2(500);
    LV_APLICACION VARCHAR2(100) := 'GSI_CARGA_VISTAS.P_EJEC_HILOS - ';
  
  BEGIN
  
    LV_COMANDO := '/bin/sh  /procesos/gsioper/otros/sh_consulta2.sh   ' ||
                  PN_ID_TABLA || '  ' || PV_FECHA || '  ' || PV_ESTADO;

      osutil_noreturn.RunOsCmd(LV_COMANDO, LV_SALIDA);

  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END P_EJEC_HILOS;
  
    -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE EJECUTA EL SHELL PARA INVOCAR AL JAVA
  ----------------------------------------------------------------------------------------------------------

  PROCEDURE P_EJEC_PROCESO	(PN_ID_TABLA IN NUMBER,
                             PV_FECHA    IN VARCHAR2,
                             PV_ESTADO   IN VARCHAR2,
                             PV_ERROR    OUT VARCHAR2) IS
                     

    CURSOR C_NOMBRE_TABLA(CN_ID_TABLA NUMBER)IS
     SELECT A.DESCRIPCION
      FROM REF_TABLAS_EJECUCION A
      WHERE A.ID_TABLA = CN_ID_TABLA
      AND A.ESTADO = 'A';                     
  
    LV_COMANDO    VARCHAR2(500);
    LV_SALIDA     VARCHAR2(500);
    LV_APLICACION VARCHAR2(100) := 'GSI_CARGA_VISTAS.P_EJEC_PROCESO - ';
    LV_NOMBRE_TABLA VARCHAR2(200):='EJECUTA_';
    LV_EJECUTA_TABLA  VARCHAR2(200);
    
  
  BEGIN
    
    OPEN C_NOMBRE_TABLA(PN_ID_TABLA);
    FETCH C_NOMBRE_TABLA INTO LV_EJECUTA_TABLA;
    CLOSE C_NOMBRE_TABLA;
    
    LV_NOMBRE_TABLA:= LV_NOMBRE_TABLA||LV_EJECUTA_TABLA;
    
    LV_COMANDO := '/bin/sh  /bscs/bscsprod/work/7312/'||LV_NOMBRE_TABLA||'.sh ' ||
                  PV_FECHA || '  ' || PV_ESTADO;


   osutil_noreturn.RunOsCmd(LV_COMANDO, LV_SALIDA);

  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END P_EJEC_PROCESO;

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE OBTIENE EL CICLO DE INACTIVAS
  -----------------------------------------------------------------------------------------------------------

  PROCEDURE P_CICLO_INACT(PV_FECHA    IN VARCHAR2,
                          PV_ID_TABLA IN VARCHAR2,
                          PV_VALOR    OUT VARCHAR2,
                          PV_ERROR    OUT VARCHAR2) IS
  
    CURSOR C_OBTIENE(CV_FECHA VARCHAR2) IS
      SELECT ID_CICLO
        FROM FA_CICLOS_BSCS
       WHERE DIA_INI_CICLO = SUBSTR(CV_FECHA, 1, 2);
  
    ---OBTIENE LA RESTRICCION DE LA TABLA QUE SE VA A PROCESAR---
    CURSOR C_RESTRICCIONES(CV_ID_TABLA VARCHAR2, CN_ID_CICLO NUMBER) IS
      SELECT B.VALOR
        FROM REF_TABLAS_EJECUCION A, REF_INACTIVOS_TABLAS B
       WHERE A.ID_TABLA = B.ID_TABLA
         AND B.CICLO = CN_ID_CICLO
         AND B.ID_TABLA = CV_ID_TABLA
         AND B.ESTADO = 'A';
  
    LC_OBTIENE     C_OBTIENE%ROWTYPE;
    LV_FECHA_CORTE VARCHAR2(20);
    LV_APLICACION  VARCHAR2(100) := 'GSI_CARGA_VISTAS.P_CICLO_INACT - ';
  
  BEGIN
  
    SELECT TO_CHAR(TO_DATE(PV_FECHA, 'YYYYMMDD'), 'DD/MM/YYYY')
      INTO LV_FECHA_CORTE
      FROM DUAL;
  
    OPEN C_OBTIENE(LV_FECHA_CORTE);
    FETCH C_OBTIENE
      INTO LC_OBTIENE;
    CLOSE C_OBTIENE;
  
    OPEN C_RESTRICCIONES(PV_ID_TABLA, LC_OBTIENE.ID_CICLO);
    FETCH C_RESTRICCIONES
      INTO PV_VALOR;
    CLOSE C_RESTRICCIONES;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END P_CICLO_INACT;

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE RENOMBRA LA TABLA CREADA, CREA LA VISTA Y ENVIA EL MAIL
  -----------------------------------------------------------------------------------------------------------

  PROCEDURE P_VISTA_MAIL(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA      IN VARCHAR2,
                         PV_ERROR      OUT VARCHAR2) IS
  
    ----CREACION DEL CURSOR DE CORREO
    CURSOR C_LEE_CORREO(C_ID_PARAMETRO VARCHAR2) IS
      SELECT B.VALOR EMAIL
        FROM GV_PARAMETROS B
       WHERE B.ID_TIPO_PARAMETRO = 7312
         AND B.ID_PARAMETRO = C_ID_PARAMETRO;
  
    ----CREACION DEL CURSOR DE FECHA
    CURSOR C_DIA_FIN_CICLO(FECHA1 VARCHAR2) IS
      SELECT DIA_FIN_CICLO, ID_CICLO
        FROM FA_CICLOS_BSCS
       WHERE DIA_INI_CICLO = FECHA1;
  
    LV_EMAIL      VARCHAR2(2000);
    LV_SERVIDOR   VARCHAR2(50) := 'maildesa';
    LV_CORREO     C_LEE_CORREO%ROWTYPE;
    LV_CORREO_CC  C_LEE_CORREO%ROWTYPE;
    LV_VISTA_TEMP VARCHAR2(100);
    LV_ERROR      VARCHAR2(500);
    LV_DIA        VARCHAR2(10);
    LV_MES        VARCHAR2(2);
    LV_ANIO       VARCHAR2(10);
    LV_SEN_VISTA  VARCHAR2(32650);
    LE_SALIDA EXCEPTION;
    LC_DIA_FIN_CICLO C_DIA_FIN_CICLO%ROWTYPE;
    LN_FOUND         NUMBER := 0;
    LV_TMES          VARCHAR(20);
    LD_FECHA_CORTE   DATE;
    LV_APLICACION    VARCHAR2(100) := 'GSI_CARGA_VISTAS.P_VISTA_MAIL - ';
  
  BEGIN
  
    --VALIDO LA FECHA DE INGRESO--
    IF PV_FECHACORTE IS NULL THEN
      PV_ERROR := 'Ingrese una Fecha Correcta';
      RETURN;
    END IF;
  
    --- VALIDO EL NOMBRE DE LA TABLA--
    IF PV_TABLA IS NULL THEN
      PV_ERROR := 'Ingrese Nombre de Tabla';
      RETURN;
    END IF;
  
    SELECT TO_DATE(TO_CHAR(TO_DATE(PV_FECHACORTE, 'YYYYMMDD'), 'DD/MM/YYYY'),
                   'DD/MM/YYYY')
      INTO LD_FECHA_CORTE
      FROM DUAL;
  
    BEGIN
      ----DESCOMPOSICION DE FECHA (DIA)
      LV_DIA  := TO_CHAR(TO_DATE(LD_FECHA_CORTE, 'DD/MM/YYYY'), 'DD');
      LV_MES  := TO_CHAR(TO_DATE(LD_FECHA_CORTE, 'DD/MM/YYYY'), 'MM');
      LV_ANIO := TO_CHAR(TO_DATE(LD_FECHA_CORTE, 'DD/MM/RRRR'), 'RRRR');
      IF LV_MES = '01' THEN
        LV_TMES := 'ENERO';
      ELSIF LV_MES = '02' THEN
        LV_TMES := 'FEBRERO';
      ELSIF LV_MES = '03' THEN
        LV_TMES := 'MARZO';
      ELSIF LV_MES = '04' THEN
        LV_TMES := 'ABRIL';
      ELSIF LV_MES = '05' THEN
        LV_TMES := 'MAYO';
      ELSIF LV_MES = '06' THEN
        LV_TMES := 'JUNIO';
      ELSIF LV_MES = '07' THEN
        LV_TMES := 'JULIO';
      ELSIF LV_MES = '08' THEN
        LV_TMES := 'AGOSTO';
      ELSIF LV_MES = '09' THEN
        LV_TMES := 'SEPTIEMBRE';
      ELSIF LV_MES = '10' THEN
        LV_TMES := 'OCTUBRE';
      ELSIF LV_MES = '11' THEN
        LV_TMES := 'NOVIEMBRE';
      ELSE
        LV_TMES := 'DICIEMBRE';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
        RETURN;
    END;
  
    ---PARAMETROS PARA ENVIO DE ALARMA A DESTINATARIO
    OPEN C_LEE_CORREO('MAIL_TO');
    FETCH C_LEE_CORREO
      INTO LV_CORREO;
    CLOSE C_LEE_CORREO;
  
    ---PARAMETROS PARA ENVIO DE ALARMA CON COPIA
    OPEN C_LEE_CORREO('MAIL_CC');
    FETCH C_LEE_CORREO
      INTO LV_CORREO_CC;
    CLOSE C_LEE_CORREO;
  
    ---DESCOMPONE LA FECHA PARA SACAR EL DIA DE CORTE
    OPEN C_DIA_FIN_CICLO(LV_DIA);
    FETCH C_DIA_FIN_CICLO
      INTO LC_DIA_FIN_CICLO;
    LN_FOUND := C_DIA_FIN_CICLO%ROWCOUNT;
    CLOSE C_DIA_FIN_CICLO;
    IF LN_FOUND = 0 THEN
      PV_ERROR := 'La fecha no se encuentra en la tabla: fa_ciclos_bscs_a';
      RAISE LE_SALIDA;
    END IF;
  
    --OBTIENE EL NOMBRE DE LA VISTA TEMPORAL
    IF LENGTH(PV_TABLA) BETWEEN 30 AND 32 THEN
      LV_VISTA_TEMP := REPLACE(PV_TABLA,
                               SUBSTR(PV_TABLA, (LENGTH(PV_TABLA) - 11), 3),
                               '_V_');
    ELSE
      LV_VISTA_TEMP := 'V_' || PV_TABLA;
    END IF;
  
    ---CREACION DE LA VISTA TEMPORAL
    LV_SEN_VISTA := 'CREATE VIEW sysadm.' || LV_VISTA_TEMP ||
                    ' AS(SELECT * FROM ' || PV_TABLA || ')';
    BEGIN
      EXECUTE IMMEDIATE LV_SEN_VISTA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE LE_SALIDA;
        END IF;
    END;
  
    LV_EMAIL := PORTA.SWK_MAIL.SEND_FILES_CC@AXIS(NOMBRE_SERVIDOR => LV_SERVIDOR,
                                                  FROM_NAME       => 'alarma_vista@claro.com.ec',
                                                  TO_NAME         => LV_CORREO.EMAIL,
                                                  CC              => LV_CORREO_CC.EMAIL,
                                                  SUBJECT         => ' QC de las Cargas de las vistas pertenecientes al ciclo: ' ||
                                                                     LC_DIA_FIN_CICLO.ID_CICLO ||
                                                                     ' Mes: ' ||
                                                                     LV_TMES ||
                                                                     ' Anio: ' ||
                                                                     LV_ANIO,
                                                  MESSAGE         => 'Se ejecuto correctamente. ' ||
                                                                     ' Se cre� la Vista ' ||
                                                                     LV_VISTA_TEMP ||
                                                                     ' correspondiente a la Tabla ' ||
                                                                     PV_TABLA ||
                                                                     ' del ultimo corte de facturacion: ' ||
                                                                     LD_FECHA_CORTE /*,*/ ---a Mensaje que se va a enviar.
                                                  /*FILENAME1       => LV_UBICACION || '/' ||
                                                                                                                                                                                                                                                                                                                                                                            WFILE)*/);
  
    IF LV_EMAIL IS NOT NULL THEN
      LV_ERROR := 'Envio fallido de alarma de mails';
      RAISE LE_SALIDA;
    END IF;
  
    commit;
  
  EXCEPTION
    WHEN LE_SALIDA THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
      LV_EMAIL := PORTA.SWK_MAIL.SEND_FILES_CC@AXIS(NOMBRE_SERVIDOR => LV_SERVIDOR,
                                                    FROM_NAME       => 'alarma_vista@claro.com.ec',
                                                    TO_NAME         => LV_CORREO.EMAIL,
                                                    CC              => LV_CORREO_CC.EMAIL,
                                                    SUBJECT         => ' QC de las Cargas de las vistas pertenecientes al ciclo: ' ||
                                                                       LC_DIA_FIN_CICLO.ID_CICLO ||
                                                                       ' Mes: ' ||
                                                                       LV_TMES ||
                                                                       ' Anio: ' ||
                                                                       LV_ANIO,
                                                    MESSAGE         => 'Error durante la ejecucion. No se pudo crear la vista correspondiente a la tabla ' ||
                                                                       PV_TABLA ||
                                                                       chr(13) ||
                                                                       'Error: ' ||
                                                                       PV_ERROR || '. ' ||
                                                                       chr(13) ||
                                                                       chr(13) ||
                                                                       'Favor Revisar!!! ' /*,*/ ---a Mensaje que se va a enviar.
                                                    /*FILENAME1       => LV_UBICACION || '/' ||
                                                                                                                                                                                                                                                                                                                                                                                        WFILE)*/);
      IF LV_EMAIL IS NOT NULL THEN
        PV_ERROR := LV_APLICACION || ' Envio fallido de alarma de mails - ' ||
                    SUBSTR(SQLERRM, 1, 200);
      END IF;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
      LV_EMAIL := PORTA.SWK_MAIL.SEND_FILES_CC@AXIS(NOMBRE_SERVIDOR => LV_SERVIDOR,
                                                    FROM_NAME       => 'alarma_vista@claro.com.ec',
                                                    TO_NAME         => LV_CORREO.EMAIL,
                                                    CC              => LV_CORREO_CC.EMAIL,
                                                    SUBJECT         => ' QC de las Cargas de las vistas pertenecientes al ciclo: ' ||
                                                                       LC_DIA_FIN_CICLO.ID_CICLO ||
                                                                       ' Mes: ' ||
                                                                       LV_TMES ||
                                                                       ' Anio: ' ||
                                                                       LV_ANIO,
                                                    MESSAGE         => 'Error durante la ejecucion. No se pudo crear la vista correspondiente a la tabla ' ||
                                                                       PV_TABLA ||
                                                                       chr(13) ||
                                                                       ' Error: ' ||
                                                                       PV_ERROR || '. ' ||
                                                                       chr(13) ||
                                                                       chr(13) ||
                                                                       'Favor Revisar!!! ' /*,*/ ---a Mensaje que se va a enviar.
                                                    /*FILENAME1       => LV_UBICACION || '/' ||
                                                                                                                                                                                                                                                                                                                                                                                        WFILE)*/);
      IF LV_EMAIL IS NOT NULL THEN
        PV_ERROR := LV_APLICACION || ' Envio fallido de alarma de mails - ' ||
                    SUBSTR(SQLERRM, 1, 200);
      END IF;
  END P_VISTA_MAIL;

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE ELIMINA LOS SYNONIMOS CREADOS PARA LAS TABLAS A FILTRAR
  -----------------------------------------------------------------------------------------------------------

  PROCEDURE P_DEPURA_SYN(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA      IN VARCHAR2,
                         PV_ERROR      OUT VARCHAR2) IS
  
    LV_SENTENCIA VARCHAR2(100);
    LV_DIA       VARCHAR2(10);
    LV_MES       VARCHAR2(10);
    LV_ANIO      VARCHAR2(10);
    LV_ERROR     VARCHAR2(1000);
    LE_ERROR EXCEPTION;
    LV_APLICACION VARCHAR2(30) := 'GSI_CARGA_VISTAS.P_DEPURA_SYN';
  BEGIN
  
    --VALIDO LA FECHA DE INGRESO--
    IF PV_FECHACORTE IS NULL THEN
      PV_ERROR := 'Ingrese una Fecha Correcta';
      RETURN;
    END IF;
  
    --VALIDO EL NOMBRE DE LA TABLA--
    IF PV_TABLA IS NULL THEN
      PV_ERROR := 'Ingrese el nombre de la tabla correcta';
      RETURN;
    END IF;
    ----DESCOMPOSICION DE FECHA (DIA)
    LV_DIA  := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/YYYY'), 'DD');
    LV_MES  := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/YYYY'), 'MM');
    LV_ANIO := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/RRRR'), 'RRRR');
    --ARMO LA SENTENCIA PARA BORRAR LOS SYNONIMOS CREADOS PARA BCH
    /*LV_SENTENCIA := 'DROP SYNONYM BCH.' || PV_TABLA || '_' || LV_ANIO ||
    LV_MES || LV_DIA;*/
    LV_SENTENCIA := 'DROP SYNONYM ' || PV_TABLA || '_' || LV_DIA || LV_MES ||
                    LV_ANIO;
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL;
        ELSE
          RAISE LE_ERROR;
        END IF;
    END;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_APLICACION || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
    
  END P_DEPURA_SYN;

  -----------------------------------------------------------------------------------------------------------
  --Proyecto  : (7312) Mejoras Procesos de Facturacion
  --Autor     : Sud Carla Duenas.
  --Creado    : 10/01/2013
  --L.Proyecto: SUD Ricardo Vergara
  --CRM       : SIS Mario Aycart
  --Purpose   : PROCESO QUE ACTUALIZA EL PERIODO EN LA TABLA FEES
  -----------------------------------------------------------------------------------------------------------

  PROCEDURE P_ACT_PERIOD(PV_FECHACORTE IN VARCHAR2,
                         PV_TABLA      IN VARCHAR2,
                         PV_ERROR      OUT VARCHAR2) IS
  
    /* LV_SENTENCIA VARCHAR2(100);*/
 
    ----OBTIENE EL PERIODO
    CURSOR C_OBT_PER IS
      SELECT CUSTOMER_ID, SEQNO, PERIOD FROM fees;
      
   LV_APLICACION        VARCHAR2(30):= 'GSI_CARGA_VISTAS.P_ACT_PERIOD'; 
   LV_SENTENCIA         VARCHAR2(100);
   LV_NOMBRE_TABLA      VARCHAR2(100);
   LV_DIA               VARCHAR2(10);
   LV_MES               VARCHAR2(10);
   LV_ANIO              VARCHAR2(10);
   LV_ERROR             VARCHAR2(200);
   LE_ERROR             EXCEPTION;
   
  BEGIN
  
    --VALIDO LA FECHA DE INGRESO--
    IF PV_FECHACORTE IS NULL THEN
      PV_ERROR := 'Ingrese una Fecha Correcta';
      RETURN;
    END IF;   ----DESCOMPOSICION DE FECHA (DIA)
    
    LV_DIA  := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/YYYY'), 'DD');
    LV_MES  := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/YYYY'), 'MM');
    LV_ANIO := TO_CHAR(TO_DATE(PV_FECHACORTE, 'DD/MM/RRRR'), 'RRRR');
  
  
    --VALIDO EL NOMBRE DE LA TABLA--
    IF PV_TABLA IS NULL THEN
      PV_ERROR := 'Ingrese el nombre de la tabla correcta';
      RETURN;
    END IF;
    
    LV_NOMBRE_TABLA:= PV_TABLA||'_'||lv_anio||lv_mes||lv_dia;
    
    LV_SENTENCIA:='rename bulk.'||lv_nombre_tabla||'  to bulk.fees'; 
    
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL;
        ELSE
          RAISE LE_ERROR;
        END IF;
    END;

    
    FOR I IN C_OBT_PER LOOP
      UPDATE sysadm.FEES
         SET PERIOD = I.PERIOD
       WHERE CUSTOMER_ID = I.CUSTOMER_ID
         AND SEQNO = I.SEQNO;
    END LOOP;
    COMMIT;
    
    lv_sentencia:='rename bulk.fees'||'to bulk.'|| lv_nombre_tabla; 
    
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := SUBSTR(SQLERRM, 1, 200);
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL;
        ELSE
          RAISE LE_ERROR;
        END IF;
    END;
   
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 200);
  END P_ACT_PERIOD;

END GSI_CARGA_VISTAS;
/
