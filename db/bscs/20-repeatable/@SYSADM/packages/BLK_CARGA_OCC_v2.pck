create or replace package BLK_CARGA_OCC_v2 is

  TYPE TVARCHAR IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR200 IS TABLE OF VARCHAR(200) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2;

  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean;

  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number;

  procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number);

  procedure BLP_balancea_carga(pn_id_req number);

  procedure BLP_carga_sndata(PN_ID_REQ IN NUMBER);

  procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion number,
                                
                                pn_hilo             number,
                                pn_id_requerimiento number,
                                pv_error            out varchar2);

  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2);

  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null);

  procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2,
                                   pv_error           out varchar2);

  procedure BLP_detiene(pn_id_ejecucion number);

  procedure BLP_ejecuta(pv_usuario          varchar2,
                        pn_id_notificacion  number,
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar2,
                        pv_entdate          varchar2,
                        pv_respaldar        varchar DEFAULT 'N',
                        pv_tabla_respaldo   varchar DEFAULT NULL,
                        pv_band_ejecucion   varchar2 DEFAULT 'A',
                        pn_id_requerimiento number,
                        pv_error            out varchar2);

  FUNCTION BLF_GETPARAMETROS_OCC(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2;
  procedure blp_occ_mail(pv_mensaje_remark   varchar2,
                         pv_usuario          varchar2,
                         pn_id_requerimiento number,
                         pv_fecha            varchar2,
                         pv_registros        varchar2,
                         pv_archivo          varchar2,
                         pv_error            out varchar2);

  PROCEDURE BLP_VALORES_PARAMETRIZADOS(PV_CODIGO      IN VARCHAR2,
                                       PV_DESCRIPCION IN VARCHAR2,
                                       PV_VALOR       OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2);
end BLK_CARGA_OCC_v2;
/
create or replace package body BLK_CARGA_OCC_v2 is

  tn_sncode          tnumber;
  tv_ACCGLCODE       tvarchar;
  tv_ACCSERV_CATCODE tvarchar;
  tv_ACCSERV_CODE    tvarchar;
  tv_ACCSERV_TYPE    tvarchar;
  tn_vscode          tnumber;
  tn_spcode          tnumber;
  tn_evcode          tnumber;

  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null) is
  
  begin
    update bl_Carga_ejecucion_v2
       set fecha_inicio   = nvl(pd_fecha_inicio, fecha_inicio),
           fecha_fin      = nvl(pd_fecha_fin, fecha_fin),
           estado         = nvl(pv_estado, estado),
           reg_procesados = nvl(pn_reg_procesados, reg_procesados),
           observacion    = nvl(pv_observacion, observacion)
     where id_ejecucion = pn_id_ejecucion;
  
    commit;
  
  end BLP_actualiza;

  procedure BLP_balancea_carga(pn_id_req number) is
  
  begin
  
    update BL_CARGA_OCC_TMP_V2 g
       set customer_id =
           (select customer_id from contract_all where co_id = g.co_id)
     where co_id is not null
       and id_requerimiento = pn_id_req;
  
    update BL_CARGA_OCC_TMP_V2 j
       set customer_id =
           (select customer_id from customer where custcode = j.custcode)
     where custcode is not null
       and id_requerimiento = pn_id_req;
  
    update BL_CARGA_OCC_TMP_V2 j
       set customer_id =
           (select customer_id
              from customer
             where customer_id = j.customer_id)
     where customer_id is not null
       and custcode is null
       and id_requerimiento = pn_id_req;
  
    update BL_CARGA_OCC_TMP_V2
       set status = 'E',
           error  = 'Problemas al obtener el customer_id de la tabla contract_all'
     where customer_id is null
       and id_requerimiento = pn_id_req;
  
    update BL_CARGA_OCC_TMP_V2
       set status = 'E', error = 'SNCode no existente'
     where (sncode is null OR sncode not in (select sncode from mpusntab))
       and id_requerimiento = pn_id_req;
  
    commit;
  
  end BLP_balancea_carga;

  procedure BLP_ejecuta(pv_usuario          varchar2, -- usuario del axis
                        pn_id_notificacion  number, -- 
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar2,
                        pv_entdate          varchar2, --eto 9820
                        pv_respaldar        varchar DEFAULT 'N',
                        pv_tabla_respaldo   varchar DEFAULT NULL,
                        pv_band_ejecucion   varchar2 DEFAULT 'A', --- [9820]CAUCANSHALA
                        pn_id_requerimiento number,
                        pv_error            out varchar2) is
  
    cursor c_id_ejec is
      SELECT nvl(max(id_ejecucion), 0) id_ejecucion
        FROM bl_carga_ejecucion_v2;
  
    LN_ID_EJECUCION NUMBER;
    LV_REMARK       VARCHAR2(1000);
    LV_FECHA        VARCHAR2(100);
    LV_REFERENCIA   VARCHAR2(100) := 'BLK_CARGA_OCC_2.BLP_EJECUTA';
    LV_ERROR        VARCHAR2(5000);
    LN_BITACORA     NUMBER := 0;
    LN_ERROR        NUMBER := 0;
    LN_DETALLE      NUMBER := 0;
    LD_FECHA        DATE;
    LE_ERROR EXCEPTION;
  
  begin
    --
    --
    Scp_Dat.sck_api.scp_bitacora_procesos_ins(pv_id_proceso        => 'SCP_CARGA_OCC_BSCS',
                                              pv_referencia        => LV_REFERENCIA,
                                              pv_usuario_so        => pv_usuario,
                                              pv_usuario_bd        => USER,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => 0,
                                              pv_unidad_registro   => 'PBX',
                                              pn_id_bitacora       => Ln_Bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => lv_error);
    --
    IF LV_ERROR IS NOT NULL THEN
      LV_ERROR := 'No se puede insertar en SCP - ' || SUBSTR(LV_ERROR, 469);
      RAISE LE_ERROR;
    END IF;
    --
    open c_id_ejec;
    fetch c_id_ejec
      into ln_id_ejecucion;
    close c_id_ejec;
  
    ln_id_ejecucion := ln_id_ejecucion + 1;
  
    lv_remark := replace(pv_remark, '*', ' ');
    lv_fecha  := replace(pv_entdate, '-', '/');
    lv_fecha  := trim(lv_fecha);
    lv_fecha  := lv_fecha || ' 00:00:01';
  
    ld_fecha := to_date(lv_fecha, 'dd/mm/rrrr HH24:mi:ss');
  
    insert into bl_carga_ejecucion_v2
    values
      (ln_id_ejecucion,
       pv_usuario,
       sysdate,
       null,
       'L',
       0,
       pn_id_notificacion,
       pn_tiempo_notif,
       pn_cantidad_hilos,
       pn_cantidad_reg_mem,
       pv_recurrencia,
       lv_remark,
       ld_fecha,
       pv_respaldar,
       pv_tabla_respaldo,
       null);
  
    IF pv_band_ejecucion = 'A' THEN
    
      BLP_actualiza(pn_id_ejecucion => ln_id_ejecucion,
                    pv_estado       => 'P',
                    pv_observacion  => 'Procesando: 0');
    
      BLP_balancea_carga(pn_id_req => pn_id_requerimiento);
    
    END IF;
    --
    --
    Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => Ln_Bitacora,
                                              Pv_Mensaje_Aplicacion => 'Proceso de datos - Modulo carga Occ',
                                              Pv_Mensaje_Tecnico    => 'Id Bitacora SCP de Axis ' ||
                                                                       pn_id_requerimiento,
                                              Pv_Mensaje_Accion     => 'Id ejecucion de bl_carga_ejecucion_v2 ' ||
                                                                       ln_id_ejecucion,
                                              Pn_Nivel_Error        => 0,
                                              Pv_Cod_Aux_1          => null,
                                              Pv_Cod_Aux_2          => null,
                                              Pv_Cod_Aux_3          => null,
                                              Pn_Cod_Aux_4          => Null,
                                              Pn_Cod_Aux_5          => Null,
                                              Pv_Notifica           => 'N',
                                              Pv_Id_Detalle         => Ln_Detalle,
                                              Pn_Error              => Ln_Error,
                                              Pv_Error              => Lv_Error);
  
    --
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    --
    Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Fin(Pn_Id_Bitacora => Ln_Bitacora,
                                              Pn_Error       => Ln_Error,
                                              Pv_Error       => Lv_Error);
    --
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    --
    COMMIT;
    --
  exception
    when le_error then
      pv_error := LV_ERROR;
    
    when others then
      pv_error := sqlerrm;
  end BLP_ejecuta;

  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2 is
  
    cursor c_estado_ejecucion(cn_id_ejecucion number) is
      select estado
        from bl_carga_ejecucion_v2 x
       where x.id_ejecucion = cn_id_ejecucion;
  
    lv_estado bl_carga_ejecucion_v2.estado%type;
  
  begin
  
    open c_estado_ejecucion(pn_id_ejecucion);
    fetch c_estado_ejecucion
      into lv_estado;
    close c_estado_ejecucion;
  
    return lv_estado;
  
  exception
    when others then
      return 'X';
    
  end BLF_obtiene_estado;

  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean is
  
    cursor c_verifica_ejecucion(cn_id_ejecucion number) is
      select nvl(count(*), 0)
        from bl_carga_ejec_hilo_stat_v2 x
       where x.id_ejecucion = cn_id_ejecucion
         and estado = 'F';
  
    cursor c_verifica_ejecucion1(cn_id_ejecucion number) is
      select cant_hilos
        from bl_carga_ejecucion_v2
       where id_ejecucion = cn_id_ejecucion;
  
    ln_verifica number;
    ln_hilos    number;
  
  begin
  
    open c_verifica_ejecucion(pn_id_ejecucion);
    fetch c_verifica_ejecucion
      into ln_verifica;
    close c_verifica_ejecucion;
  
    open c_verifica_ejecucion1(pn_id_ejecucion);
    fetch c_verifica_ejecucion1
      into ln_hilos;
    close c_verifica_ejecucion1;
  
    if ln_verifica = ln_hilos then
      return true;
    else
      return false;
    end if;
  
  exception
    when others then
      return false;
    
  end BLF_verifica_finalizacion;

  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number is
  
    cursor c_obtiene is
      select substr(observacion, instr(observacion, ':') + 2)
        from bl_carga_ejecucion_v2
       where id_ejecucion = pn_id_ejecucion;
  
    lv_tiempo varchar2(10);
  
  begin
  
    open c_obtiene;
    fetch c_obtiene
      into lv_tiempo;
    if c_obtiene%notfound then
      return - 1;
    end if;
    close c_obtiene;
  
    return to_number(lv_tiempo);
  
  exception
    when others then
      return 0;
    
  end BLF_obtiene_ultima_notif;

  procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number) is
  
  begin
  
    update bl_carga_ejecucion_v2
       set observacion = substr(observacion, 1, instr(observacion, ':') + 1) ||
                         to_char(pn_valor)
     where id_ejecucion = pn_id_ejecucion;
  
    commit;
  
  end BLP_setea_ultima_notif;

  procedure BLP_carga_sndata(PN_ID_REQ IN NUMBER) is
  
    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;
  
  begin
  
    open lcv_cur for
    
      select x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        from mpulktmb x, mpulkexn y
       where x.sncode = y.sncode
         AND EXISTS (select 'X'
                from BL_CARGA_OCC_TMP_V2 a
               WHERE sncode = x.sncode
                 AND a.id_requerimiento = PN_ID_REQ)
         and X.vscode = (select max(a.vscode)
                           from mpulktmb a
                          where tmcode = 35
                            and sncode = 399)
         and X.tmcode = 35;
  
    FETCH LCV_CUR BULK COLLECT
      INTO tn_sncode,
           tv_ACCGLCODE,
           tv_ACCSERV_CATCODE,
           tv_ACCSERV_CODE,
           tv_ACCSERV_TYPE,
           tn_vscode,
           tn_spcode,
           tn_evcode;
    CLOSE LCV_CUR;
  
    FOR I in tn_sncode.first .. tn_sncode.last LOOP
      tv_ACCGLCODE(tn_sncode(i)) := tv_ACCGLCODE(i);
      tv_ACCSERV_CATCODE(tn_sncode(i)) := tv_ACCSERV_CATCODE(i);
      tv_ACCSERV_CODE(tn_sncode(i)) := tv_ACCSERV_CODE(i);
      tv_ACCSERV_TYPE(tn_sncode(i)) := tv_ACCSERV_TYPE(i);
      tn_vscode(tn_sncode(i)) := tn_vscode(i);
      tn_spcode(tn_sncode(i)) := tn_spcode(i);
      tn_evcode(tn_sncode(i)) := tn_evcode(i);
    
    END LOOP;
  
  end blp_carga_sndata;

  procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion     number,
                                pn_hilo             number,
                                pn_id_requerimiento number,
                                pv_error            out varchar2) is
  
    cursor c_config is
      select cant_reg_mem,
             entdate,
             fecha_inicio,
             id_notificacion,
             nombre_respaldo,
             recurrencia,
             remark,
             respaldar,
             tiempo_notif,
             usuario
        from bl_carga_ejecucion_v2
       where id_ejecucion = pn_id_ejecucion;
  
    cursor c_carga(PN_ID_REQUERIMIENTO NUMBER) is
      select h.rowid,
             to_number(replace(replace(h.AMOUNT, ',', '.'), chr(13), '')) amount,
             h.sncode,
             h.co_id,
             h.customer_id ---/*-to_number(h.amount)*/
        from sysadm.BL_CARGA_OCC_TMP_V2 H
       where status is null
         and hilo = pn_hilo
         and customer_id is not null
         and h.id_requerimiento = PN_ID_REQUERIMIENTO; --9820 CAUCANSHALA
  
    cursor c_seq(cn_customer_id number) is
      SELECT nvl(max(seqno), 0) seqno
        FROM fees
       WHERE customer_id = cn_customer_id;
  
    cursor c_hilo_seq is
      SELECT nvl(max(seq), 0) seq
        FROM bl_carga_ejec_hilo_detail_v2 bc
       WHERE bc.id_ejecucion = pn_id_ejecucion
         and bc.hilo = pn_hilo;
  
    cursor c_cust_id_status(cn_customer_id number) is
      SELECT cstype, csdeactivated
        from customer
       where customer_id = cn_customer_id;
  
    cursor c_co_id_status(cn_co_id number) is
      SELECT entdate
        from contract_history
       where ch_status = 'd'
         and co_id = cn_co_id;
  
    cursor c_minutos(cn_id_ejecucion number) is
      select trunc((sysdate - fecha_inicio) * 24 * 60, 2) minutos,
             reg_procesados
        from bl_carga_ejecucion_v2
       where id_ejecucion = cn_id_ejecucion;
  
    tn_sncode2            tnumber;
    tv_ACCGLCODE2         tvarchar;
    tv_ACCSERV_CATCODE2   tvarchar;
    tv_ACCSERV_CODE2      tvarchar;
    tv_ACCSERV_TYPE2      tvarchar;
    tn_vscode2            tnumber;
    tn_spcode2            tnumber;
    tn_evcode2            tnumber;
    tn_ln_seq             tnumber;
    td_entdate            tdate;
    td_valid_from         tdate;
    td_entdate_by_cust    tdate;
    td_valid_from_by_cust tdate;
    tv_error              tvarchar;
    ln_minutos            number;
    ln_registros_total    number;
    ln_counter            number := 0;
    lv_error              varchar2(200);
    lb_nohaydatos         boolean;
    lc_config             c_config%rowtype;
    lc_co_id_status       c_co_id_status%rowtype;
    ln_hilo_seq           number;
    le_error exception;
    ln_period         number;
    lc_cust_id_status c_cust_id_status%rowtype;
    lc_seq            c_seq%rowtype;
    lv_error_mail     varchar2(1000);
    TV_ROWID          TVARCHAR;
    TN_AMOUNT         TNUMBER;
    TN_SNCODE         TNUMBER;
    TN_COID           TNUMBER;
    TN_CUSTOMER_ID    TNUMBER;
    TN_SEQ_BY_CUSTID  TNUMBER;
  
  BEGIN
  
    open c_config;
    fetch c_config
      into lc_config;
    close c_config;
  
    open c_hilo_seq;
    fetch c_hilo_seq
      into ln_hilo_seq;
    close c_hilo_seq;
  
    ln_hilo_seq := ln_hilo_seq + 1;
  
    insert into bl_carga_ejec_hilo_detail_v2
    values
      (pn_id_ejecucion, pn_hilo, 'P', 0, ln_hilo_seq, sysdate, null, null);
  
    update bl_carga_ejec_hilo_stat_v2
       set estado = 'P'
     where id_ejecucion = pn_id_ejecucion
       and hilo = pn_hilo;
  
    commit;
  
    BLP_carga_sndata(pn_id_requerimiento);
  
    open c_carga(pn_id_requerimiento);
    loop
    
      TV_ROWID.delete;
      tv_error.delete;
      TN_AMOUNT.delete;
      TN_SNCODE.delete;
      TN_COID.delete;
      tn_sncode2.delete;
      tv_ACCGLCODE2.delete;
      tv_ACCSERV_CATCODE2.delete;
      tv_ACCSERV_CODE2.delete;
      tv_ACCSERV_TYPE2.delete;
      tn_vscode2.delete;
      tn_spcode2.delete;
      tn_evcode2.delete;
      tn_customer_id.delete;
      tn_ln_seq.delete;
      ln_counter := 0;
    
      lb_nohaydatos := true;
    
      fetch c_carga bulk collect
        into TV_ROWID,
             TN_AMOUNT,
             TN_SNCODE,
             TN_COID,
             TN_CUSTOMER_ID limit lc_config.cant_reg_mem;
    
      if TV_ROWID.count > 0 THEN
      
        lb_nohaydatos := false;
      
        FOR I IN TV_ROWID.FIRST .. TV_ROWID.LAST
        
         LOOP
        
          tv_ACCGLCODE2(i) := tv_ACCGLCODE(tn_sncode(i));
          tv_ACCSERV_CATCODE2(i) := tv_ACCSERV_CATCODE(tn_sncode(i));
          tv_ACCSERV_CODE2(i) := tv_ACCSERV_CODE(tn_sncode(i));
          tv_ACCSERV_TYPE2(i) := tv_ACCSERV_TYPE(tn_sncode(i));
          tn_vscode2(i) := tn_vscode(tn_sncode(i));
          tn_spcode2(i) := tn_spcode(tn_sncode(i));
          tn_evcode2(i) := tn_evcode(tn_sncode(i));
          tv_error(i) := null;
        
          if not TN_SEQ_BY_CUSTID.exists(TN_CUSTOMER_ID(i)) then
          
            --Obtener el secuencial
            open c_seq(TN_CUSTOMER_ID(i));
            fetch c_seq
              into lc_seq;
            if c_seq%notfound then
              tn_ln_seq(i) := 1;
            else
              tn_ln_seq(i) := lc_seq.seqno + 1;
            end if;
            close c_seq;
          
            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := tn_ln_seq(i);
          
            --Determino si la cuenta est� desactivada
            open c_cust_id_status(TN_CUSTOMER_ID(i));
            fetch c_cust_id_status
              into lc_cust_id_status;
          
            if c_cust_id_status%notfound then
              tv_error(i) := 'Problemas al obtener el estado del customer_id';
            end if;
          
            if lc_cust_id_status.cstype = 'd' then
              --S� est� desactivada, entonces lo ingreso un d�a antes de fecha desact
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_cust_id_status.csdeactivated) - 1;
            else
              --Si no, tomo la fecha del param de entrada
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);
            end if;
          
            td_valid_from_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);
          
            close c_cust_id_status;
          
            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));
          
          else
          
            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) + 1;
          
            tn_ln_seq(i) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i));
          
            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));
          
          end if;
        
          if TN_COID(i) is not null then
            --Cargos a nivel de CO_ID
            --Determino si el co_id esta desactivado
            open c_co_id_status(TN_COID(i));
            fetch c_co_id_status
              into lc_co_id_status;
          
            if c_co_id_status%notfound then
              close c_co_id_status;
            else
              td_entdate(i) := trunc(lc_co_id_status.entdate) - 1;
              close c_co_id_status;
            end if;
          
          end if;
        
          if lc_config.recurrencia = 'N' then
            ln_period := 1;
          else
            ln_period := 99;
          end if;
        
          ln_counter := ln_counter + 1;
          -- end if;
        
        end loop;
      
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          INSERT /*+ APPEND */
          INTO fees
            (customer_id,
             seqno,
             fee_type,
             glcode,
             entdate,
             period,
             username,
             valid_from,
             servcat_code,
             serv_code,
             serv_type,
             co_id,
             amount,
             remark,
             currency,
             glcode_disc,
             glcode_mincom,
             tmcode,
             vscode,
             spcode,
             sncode,
             evcode,
             fee_class)
          values
            (tn_customer_id(m),
             tn_ln_seq(m),
             lc_config.recurrencia,
             tv_ACCGLCODE2(M),
             lc_config.entdate,
             ln_period,
             lc_config.usuario,
             td_valid_from(m),
             tv_ACCSERV_CATCODE2(M),
             tv_ACCSERV_CODE2(M),
             tv_ACCSERV_TYPE2(M),
             tn_coid(M),
             tn_amount(M),
             lc_config.remark,
             19,
             tv_ACCGLCODE2(M),
             tv_ACCGLCODE2(M),
             35,
             tn_VSCODE2(M),
             tn_SPCODE2(M),
             tn_sncode(M),
             tn_EVCODE2(M),
             3);
      
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          update bl_carga_occ_tmp_V2
             set status = 'P', error = null
           where rowid = TV_ROWID(M)
             AND id_requerimiento = pn_id_requerimiento
             and hilo = pn_hilo;
        COMMIT;
        update bl_carga_ejecucion_v2
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion;
      
        update bl_carga_ejec_hilo_stat_v2
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;
      
        update bl_carga_ejec_hilo_detail_v2
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;
      
        --
        If BLF_obtiene_estado(pn_id_ejecucion => pn_id_ejecucion) = 'U' then
        
          update bl_carga_ejec_hilo_stat_v2
             set estado = 'U'
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo;
        
          update bl_carga_ejec_hilo_detail_v2
             set estado = 'U', fecha_fin = sysdate
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo
             and seq = ln_hilo_seq;
        
          exit;
        
        end if;
      
      END IF;
    
      IF c_carga%NOTFOUND OR lb_nohaydatos then
        update bl_carga_ejec_hilo_stat_v2
           set estado = 'F'
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;
      
        update bl_carga_ejec_hilo_detail_v2
           set estado = 'F', fecha_fin = sysdate
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;
      
        commit;
      
        exit;
      
      end if;
    
      begin
        open c_minutos(pn_id_ejecucion);
        fetch c_minutos
          into ln_minutos, ln_registros_total;
        close c_minutos;
      
      exception
        when others then
          null;
        
      end;
    
      --Notificaciones peri�dicas
      if trunc((sysdate - lc_config.fecha_inicio) * 24 * 60) >=
         BLF_obtiene_ultima_notif(pn_id_ejecucion => pn_id_ejecucion) +
         lc_config.tiempo_notif then
      
        Blp_Setea_Ultima_Notif(pn_id_ejecucion => pn_id_ejecucion,
                               pn_valor        => trunc((sysdate -
                                                        lc_config.fecha_inicio) * 24 * 60) +
                                                  lc_config.tiempo_notif);
      
      end if;
    
      commit;
    
    end loop;
  
    close c_carga;
  
    --Ya finaliz� el proceso?
    if BLF_verifica_finalizacion(pn_id_ejecucion => pn_id_ejecucion) then
      BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                    pd_fecha_fin    => sysdate,
                    pv_observacion  => 'Finalizado',
                    pv_estado       => 'F');
    
      if lc_config.respaldar in ('Y', 'S') then
        BLP_Respalda_tabla(pv_tabla_respaldar => lc_config.nombre_respaldo);
      end if;
    
      begin
      
        open c_minutos(pn_id_ejecucion);
        fetch c_minutos
          into ln_minutos, ln_registros_total;
        close c_minutos;
      
      exception
        when others then
          null;
        
      end;
    
      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion,
                             pv_mensaje         => 'Fin proceso Carga OCC. Usuario: ' ||
                                                   lc_config.usuario ||
                                                   ' Remark: ' ||
                                                   lc_config.remark ||
                                                   ' Minutos: ' ||
                                                   to_char(ln_minutos) ||
                                                   ' Total reg: ' ||
                                                   ln_registros_total,
                             pv_error           => lv_error_mail);
    
    end if;
    commit;
  exception
    when others then
    
      if (sqlerrm = 'ORA-00001: unique constraint (SYSADM.PKFEES) violated') then
        blk_carga_occ_v2.blp_carga_occ_co_id(pn_id_ejecucion     => pn_id_ejecucion,
                                             pn_hilo             => pn_hilo,
                                             pn_id_requerimiento => pn_id_requerimiento,
                                             pv_error            => pv_error);
      
      end if;
    
      update bl_carga_ejec_hilo_stat_v2
         set estado = 'E'
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo;
    
      update bl_carga_ejec_hilo_detail_v2
         set estado = 'E', fecha_fin = sysdate, observacion = lv_error
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo
         and seq = ln_hilo_seq;
    
      commit;
    
  end BLP_Carga_Occ_Co_id;

  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2) is
    lv_sql varchar2(500);
  begin
    lv_sql := 'CREATE TABLE ' || pv_tabla_respaldar || ' AS ' ||
              ' select * from BL_CARGA_OCC_TMP_V2 ';
    execute immediate lv_sql;
  exception
    when others then
      null;
  end BLP_Respalda_tabla;

  procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2,
                                   pv_error           out varchar2) is
  
    cursor c_grupo_noti is
      select b.destinatario
        from bl_carga_grupo_notificacion_v2 a, bl_carga_notifica b
       where a.id_notifica = b.id_notifica
         and a.estado = 'A'
         and b.estado = 'A'
         and b.tipo = 'MAIL'
         and a.id_notificacion = pn_id_notificacion;
  
    lv_remitentes varchar2(4000) := null;
  
  begin
  
    for i in c_grupo_noti loop
      lv_remitentes := lv_remitentes || i.destinatario || ';';
    end loop;
  
    lv_remitentes := substr(lv_remitentes, 1, length(lv_remitentes) - 1);
  
    send_mail.mail@axis(from_name => 'blk_carga_occ@conecel.com',
                        to_name   => lv_remitentes,
                        cc        => lv_remitentes,
                        cco       => null,
                        subject   => 'Carga de valores a la factura mediante OCC',
                        message   => pv_mensaje);
    -- OJO QUITAR EL COMENTARIO
    commit;
  exception
    when others then
      pv_error := sqlerrm;
  end BLP_envia_correo_grupo;

  procedure BLP_detiene(pn_id_ejecucion number) is
  begin
    BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                  pv_estado       => 'U',
                  pv_observacion  => 'En pausa. Detenido manualmente');
  
  end;

  --9820 ETORRES funcion  para obtener los parametros de la tabla SCP_DAT.SCP_PARAMETROS_PROCESOS para cargas OCC 
  FUNCTION BLF_GETPARAMETROS_OCC(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS
  
    LV_VALOR_PAR   VARCHAR2(1000);
    LV_PROCESO     VARCHAR2(1000);
    LV_DESCRIPCION VARCHAR2(1000);
    LN_ERROR       NUMBER;
    LV_ERROR       VARCHAR2(1000);
  
  BEGIN
  
    SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(PV_COD_PARAMETRO,
                                                LV_PROCESO,
                                                LV_VALOR_PAR,
                                                LV_DESCRIPCION,
                                                LN_ERROR,
                                                LV_ERROR);
    RETURN LV_VALOR_PAR;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
    
  END BLF_GETPARAMETROS_OCC;

  --9820 ETORRES Procedimiento de envio de mail para cargas OCC 
  procedure blp_occ_mail(pv_mensaje_remark   varchar2, --9820 etorres
                         pv_usuario          varchar2,
                         pn_id_requerimiento number,
                         pv_fecha            varchar2,
                         pv_registros        varchar2,
                         pv_archivo          varchar2,
                         pv_error            out varchar2) is
  
    CURSOR C_SUM_FEES(CN_ID_REQUERIMIENTO NUMBER, cv_remark VARCHAR2) IS
      select sum(amount)
        from sysadm.fees D
       WHERE customer_id in (select customer_id
                               from sysadm.BL_CARGA_OCC_TMP_V2
                              where id_requerimiento = CN_ID_REQUERIMIENTO
                                and customer_id is not null)
         and username = pv_usuario
         AND remark = cv_remark;
    -- Proceso Automatico - Carga OCC  pruebas 28 10 20145 Fecha Facturacion: 20141028
    cursor C_OBTIENE_SNCODE(CN_ID_REQUERIMIENTO NUMBER) IS
      select distinct (sncode)
        from sysadm.BL_CARGA_OCC_TMP_V2
       where id_requerimiento = CN_ID_REQUERIMIENTO;
  
    CURSOR C_COUNT_FEES(CN_ID_REQUERIMIENTO NUMBER, cv_remark VARCHAR2) IS
      select count(*)
        from sysadm.fees D
       WHERE customer_id in (select customer_id
                               from sysadm.BL_CARGA_OCC_TMP_V2
                              where id_requerimiento = CN_ID_REQUERIMIENTO
                                and customer_id is not null)
         and username = pv_usuario
         AND remark = cv_remark;
  
    Cursor c_parametros is
      select (select valor1
                from occ_parametros_grales t
               where t.codigo = 'OCC_ASUNTO'
               and estado = 'A') asunto,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'OCC_REMITENTE'
               and estado = 'A') remitente,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'OCC_CLASE'
               and estado = 'A') clase,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'OCC_REINTENTOS'
               and estado = 'A') reintentos,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'OCC_ENCABEZADO'
               and estado = 'A') encabezado,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'DIRECTORY_FINAN_FTP'
               and estado = 'A') directorio_finan,
             (select valor1
                from occ_parametros_grales t
               where t.codigo = 'MAIL_CC_CCO'
               and estado = 'A') lv_mail_cc,
             (select valor2
                from occ_parametros_grales t
               where t.codigo = 'MAIL_CC_CCO'
               and estado = 'A') lv_mail_cco,
             (select valor1                 ---9820- PMERA - 24/08/2015  se agrega la ip de finan
               from occ_parametros_grales t
               where t.codigo='OCC_IP_FINAN'
               and estado = 'A') lv_ip_finan 
        from dual;
    Cursor c_tipo_envio(cv_parametro varchar2) is
      select valor1 from occ_parametros_grales where codigo = cv_parametro;
  
    lv_salto             VARCHAR2(10);
    ln_sum_amount        number := 0;
    ln_count_fees        number := 0;
    LN_SNCODE            NUMBER;
    ln_valor_descartados number := 0;
    lv_mensaje_remark    varchar2(1000) := null;
    lv_fecha             varchar(100);
    le_error exception;
    ln_result               number;
    lv_mensaje_retorno      varchar2(1000);
    ln_id_secuencia         number;
    lv_servidor             varchar2(100);
    lv_puerto               varchar2(100) := '';
    lv_archivo_adjunto      varchar2(1000);
    lv_asunto               varchar2(1000);
    lv_remitente            varchar2(1000);
    lv_tipo_envio           varchar2(1000);
    lv_clase                varchar2(1000);
    lv_reintentos           varchar2(1000);
    lv_encabezado           varchar2(1000);
    lv_mail_cc              varchar2(1000);
    lv_mail_cco             varchar2(1000);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_DIRECTORIO_FINAN     VARCHAR2(500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    LV_REFERENCIA           VARCHAR2(500) := 'BLK_CARGA_OCC_V2.BLP_OCC_MAIL';
    LN_BITACORA             NUMBER := 0;
    LN_ERROR                NUMBER := 0;
    LN_DETALLE              NUMBER := 0;
  
  begin
    --
    Scp_Dat.sck_api.scp_bitacora_procesos_ins(pv_id_proceso        => 'SCP_CARGA_OCC_BSCS',
                                              pv_referencia        => LV_REFERENCIA,
                                              pv_usuario_so        => pv_usuario,
                                              pv_usuario_bd        => USER,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => 0,
                                              pv_unidad_registro   => 'PBX',
                                              pn_id_bitacora       => Ln_Bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => lv_mensaje_retorno);
    --
    IF lv_mensaje_retorno IS NOT NULL THEN
      lv_mensaje_retorno := 'No se puede insertar en SCP Envio Mail - ' ||
                            SUBSTR(lv_mensaje_retorno, 469);
      RAISE LE_ERROR;
    END IF;
    --
    lv_mensaje_remark := replace(pv_mensaje_remark, '*', ' ');
    lv_salto          := CHR(13);
    lv_fecha          := replace(pv_fecha, '_', ' ');
    lv_fecha          := replace(lv_fecha, '-', '/');
  
    OPEN C_OBTIENE_SNCODE(pn_id_requerimiento);
    FETCH C_OBTIENE_SNCODE
      INTO LN_SNCODE;
    CLOSE C_OBTIENE_SNCODE;
  
    open C_COUNT_FEES(pn_id_requerimiento, lv_mensaje_remark);
    fetch C_COUNT_FEES
      into ln_count_fees;
    close C_COUNT_FEES;
  
    open C_SUM_FEES(pn_id_requerimiento, lv_mensaje_remark);
    fetch C_SUM_FEES
      into ln_sum_amount;
    close C_SUM_FEES;
  
    open c_parametros;
    fetch c_parametros
      into lv_asunto, lv_remitente, lv_clase, lv_reintentos, lv_encabezado, lv_directorio_finan, lv_mail_cc, lv_mail_cco,lv_servidor;
    if c_parametros%notfound then
      lv_asunto           := 'Cargas OCC';
      lv_remitente        := 'carga_occ@claro.com.ec';
      lv_directorio_finan := '/procesos/notificaciones/archivos';
      lv_mail_cc          := 'hponce@claro.com.ec;maycart@claro.com.ec;lflores@claro.com.ec';
      lv_mail_cco         := 'zquinonez@corlasosa.com;etorres@corlasosa.com;pmera@corlasosa.com';
      lv_servidor         := '130.2.18.27';
    end if;
    close c_parametros;
    if pv_registros > ln_count_fees then
      ln_valor_descartados := pv_registros - ln_count_fees;
      lv_archivo_adjunto   := '</ARCHIVO1=' || pv_archivo ||
                              '_rechazados_' || pn_id_requerimiento ||
                              '.csv' ||
                              '|DIRECTORIO1='|| lv_directorio_finan ||'|/>';
      open c_tipo_envio('TIPO_REGISTRO_A');
      fetch c_tipo_envio
        into lv_tipo_envio;
      close c_tipo_envio;
    else
      lv_archivo_adjunto := '';
      open c_tipo_envio('TIPO_REGISTRO_M');
      fetch c_tipo_envio
        into lv_tipo_envio;
      close c_tipo_envio;
    end if;
    if ln_sum_amount is null then
      ln_sum_amount := 0;
    end if;
  
    ---------------------------------------------------------------------------------
    -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
    ---------------------------------------------------------------------------------   
    LN_ID_REQ_EIS1          := TO_NUMBER(BLF_GETPARAMETROS_OCC('CONF_LLAMA_ID_REQ'));
    LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                               BLF_GETPARAMETROS_OCC('CONF_LLAMA_DATA_SOURCE') ||
                               ';pnIdServicioInformacion=' ||
                               TO_NUMBER(BLF_GETPARAMETROS_OCC('CONF_LLAMA_ID_SERV_INFORMACION')) ||
                               ';pvParametroBind1=' || pv_usuario || ';';
  
    LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                             PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                             PV_FAULT_CODE               => LV_FAULT_CODE,
                                                             PV_FAULT_STRING             => LV_FAULT_STRING);
  
    IF LV_FAULT_STRING IS NOT NULL THEN
      lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                            LV_FAULT_STRING;
      RAISE LE_ERROR;
    END IF;
  
    LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                              PR_RESPUESTA        => LX_XML_EIS1,
                                                              PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                              PV_NAMESPACE        => NULL);
  
    IF LV_DESTINATARIO IS NULL THEN
      lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
      RAISE LE_ERROR;
    END IF;
    LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
    LV_DESTINATARIO := TRIM(';' FROM LV_DESTINATARIO);
    lv_encabezado   := replace(lv_encabezado, '?', pv_archivo);
    ln_result       := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'blk_carga_occ_v2.blp_occ_mail',
                                                                 pd_fechaenvio      => sysdate,
                                                                 pd_fechaexpiracion => sysdate + 1,
                                                                 pv_asunto          => lv_asunto,
                                                                 pv_mensaje         => lv_encabezado ||
                                                                                       lv_salto ||
                                                                                       lv_salto ||
                                                                                       'Resumen de transacci�n:' ||
                                                                                       lv_salto ||
                                                                                       'Registros exit�sos: ' ||
                                                                                       ln_count_fees ||
                                                                                       lv_salto ||
                                                                                       'Registros descartados: ' ||
                                                                                       ln_valor_descartados ||
                                                                                       lv_salto ||
                                                                                       'Fecha hora de procesamiento: ' ||
                                                                                       to_char(sysdate,
                                                                                               'dd/mm/rrrr hh24:mi') ||
                                                                                       lv_salto ||
                                                                                       'Fecha de facturaci�n: ' ||
                                                                                       lv_fecha ||
                                                                                       lv_salto ||
                                                                                       'Cantidad d�lares procesados: $' ||
                                                                                       ln_sum_amount ||
                                                                                       lv_salto ||
                                                                                       'Sncode procesado: ' ||
                                                                                       LN_SNCODE ||
                                                                                       lv_salto ||
                                                                                       'Usuario: ' ||
                                                                                       pv_usuario||lv_archivo_adjunto,
                                                                 pv_destinatario    => lv_destinatario||'//'|| lv_mail_cc ||'//' ||lv_mail_cco,
                                                                 pv_remitente       => lv_remitente,
                                                                 pv_tiporegistro    => lv_tipo_envio,
                                                                 pv_clase           => lv_clase,
                                                                 pv_puerto          => lv_puerto,
                                                                 pv_servidor        => lv_servidor,
                                                                 pv_maxintentos     => lv_reintentos,
                                                                 pv_idusuario       => pv_usuario,
                                                                 pv_mensajeretorno  => lv_mensaje_retorno,
                                                                 pn_idsecuencia     => ln_id_secuencia);
  
    if lv_mensaje_retorno is not null or ln_result !=0 then
      --
      Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => Ln_Bitacora,
                                                Pv_Mensaje_Aplicacion => 'BLK_CARGA_OCC_V2.BLP_OCC_MAIL',
                                                Pv_Mensaje_Tecnico    => 'Error en porceso de envio de Mail del requerimiento N.-'|| pn_id_requerimiento,
                                                Pv_Mensaje_Accion     => lv_mensaje_retorno,
                                                Pn_Nivel_Error        => 3,
                                                Pv_Cod_Aux_1          => null,
                                                Pv_Cod_Aux_2          => null,
                                                Pv_Cod_Aux_3          => null,
                                                Pn_Cod_Aux_4          => Null,
                                                Pn_Cod_Aux_5          => Null,
                                                Pv_Notifica           => 'N',
                                                Pv_Id_Detalle         => Ln_Detalle,
                                                Pn_Error              => Ln_Error,
                                                Pv_Error              => lv_mensaje_retorno);
      --
      IF lv_mensaje_retorno IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
      --
    else   
      
      --- 9820 PMERA- 24/08/2015 SE AGREGA EL DETALLE CUANDO EL ENVIO DEL CORREO TIENE NIVEL_ERROR: 0
      --
      Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => Ln_Bitacora,
                                                Pv_Mensaje_Aplicacion => 'Proceso de datos - Modulo carga Occ - Envio Mail final',
                                                Pv_Mensaje_Tecnico    => 'Id Secuencia notificaciones ' ||
                                                                         ln_id_secuencia ||
                                                                         '. Req. Axis ' ||
                                                                         pn_id_requerimiento,
                                                Pv_Mensaje_Accion     => null,
                                                Pn_Nivel_Error        => 0,
                                                Pv_Cod_Aux_1          => null,
                                                Pv_Cod_Aux_2          => null,
                                                Pv_Cod_Aux_3          => null,
                                                Pn_Cod_Aux_4          => Null,
                                                Pn_Cod_Aux_5          => Null,
                                                Pv_Notifica           => 'N',
                                                Pv_Id_Detalle         => Ln_Detalle,
                                                Pn_Error              => Ln_Error,
                                                Pv_Error              => lv_mensaje_retorno);
    
      --
      IF lv_mensaje_retorno IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
      --
    end if;
    
    --
    Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Fin(Pn_Id_Bitacora => Ln_Bitacora,
                                              Pn_Error       => Ln_Error,
                                              Pv_Error       => lv_mensaje_retorno);
    --
    IF lv_mensaje_retorno IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    --
    COMMIT;
    --
  exception
    when le_error then
      pv_error := lv_mensaje_retorno;
    
    when others then
      pv_error := sqlerrm;
    
  end blp_occ_mail;
  ---
  PROCEDURE BLP_VALORES_PARAMETRIZADOS(PV_CODIGO      IN VARCHAR2, --CODIGO DEL ERROR
                                       PV_DESCRIPCION IN VARCHAR2, --PROCESO QUE ORIGINA EL ERROR
                                       PV_VALOR       OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2) IS
  
    CURSOR C_PARAMETRO(CV_DESCRIPCION IN VARCHAR2) IS
      SELECT P.VALOR1, P.VALOR2
        FROM OCC_PARAMETROS_GRALES P
       WHERE UPPER(TRIM(P.DESCRIPCION)) = UPPER(TRIM(CV_DESCRIPCION))
         AND P.ESTADO = 'A';
  
    LB_FOUND     BOOLEAN;
    LC_PARAMETRO C_PARAMETRO%ROWTYPE;
    LV_ERROR     VARCHAR2(2000);
    LE_ERROR EXCEPTION;
    LV_PROGRAM VARCHAR2(2000) := 'BLK_CARGA_OCC_v2.GCP_VALORES_PARAMETRIZADOS';
  BEGIN
    OPEN C_PARAMETRO(PV_CODIGO);
    FETCH C_PARAMETRO
      INTO LC_PARAMETRO;
    LB_FOUND := C_PARAMETRO%FOUND;
    CLOSE C_PARAMETRO;
    PV_VALOR := LC_PARAMETRO.VALOR1 || ' ' || LC_PARAMETRO.VALOR2 || ' -- ' ||
                PV_DESCRIPCION; --DEVUELVE EL MENSAJE TRADUCIDO DEL ERROR JUNTO CON LA DESCRIPCION DEL PROCESO QUE SE CAY�
    IF NOT LB_FOUND THEN
      LV_ERROR := 'NO SE ENCONTRO EL ERROR PARAMETRIZADO EN LA TABLA OCC_PARAMETROS_GRALES';
      RAISE LE_ERROR;
    END IF;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAM || ' ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR: ' || LV_PROGRAM || ' ' || SQLERRM;
    
  END BLP_VALORES_PARAMETRIZADOS;
end;
/
