CREATE OR REPLACE PACKAGE MFK_TRX_ENVIOS_GES IS

  TYPE gtr_categ_cliente IS RECORD(
    code     VARCHAR2(10),
    names    VARCHAR2(30),
    segmento VARCHAR2(10));
  TYPE gr_categ_cliente IS REF CURSOR RETURN gtr_categ_cliente;
  TYPE gt_categ_cliente IS TABLE OF gtr_categ_cliente INDEX BY BINARY_INTEGER;

  TYPE gtr_forma_pago IS RECORD(
    code  VARCHAR2(10),
    names VARCHAR2(70),
    grupo VARCHAR2(60));
  TYPE gr_forma_pago IS REF CURSOR RETURN gtr_forma_pago;
  TYPE gt_forma_pago IS TABLE OF gtr_forma_pago INDEX BY BINARY_INTEGER;

  TYPE gtr_planes IS RECORD(
    code  VARCHAR2(10),
    names VARCHAR2(70),
    grupo VARCHAR2(60));
  TYPE gr_planes IS REF CURSOR RETURN gtr_planes;
  TYPE gt_planes IS TABLE OF gtr_planes INDEX BY BINARY_INTEGER;

  PROCEDURE mfp_configura_envio(pn_idenvio        mf_envios_ges.idenvio%TYPE,
                                pd_fechaejecucion mf_envio_ejecuciones_ges.fecha_ejecucion%TYPE,
                                pn_secuencia      OUT mf_envio_ejecuciones_ges.secuencia%TYPE,
                                pv_msgerror       OUT VARCHAR2);

  PROCEDURE mfp_categoriza_cliente(pt_categ_cliente IN OUT gt_categ_cliente);

  PROCEDURE mfp_eliminar_logico(pn_idenvio   IN mf_envios_ges.idenvio%TYPE,
                                pv_msg_error IN OUT VARCHAR2);

END MFK_TRX_ENVIOS_GES;
/
CREATE OR REPLACE PACKAGE BODY MFK_TRX_ENVIOS_GES IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la configuracion de envios para GES 
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_configura_envio(pn_idenvio        mf_envios_ges.idenvio%TYPE,
                                pd_fechaejecucion mf_envio_ejecuciones_ges.fecha_ejecucion%TYPE,
                                pn_secuencia      OUT mf_envio_ejecuciones_ges.secuencia%TYPE,
                                pv_msgerror       OUT VARCHAR2) IS
  
    CURSOR c_envios(cn_idenvio NUMBER) IS
      SELECT e.estado, e.cantidad_mensajes, e.horas_separacion_msn
        FROM mf_envios_ges e
       WHERE e.idenvio = cn_idenvio;
  
    CURSOR c_criterios_x_envio(cn_idenvio NUMBER) IS
      SELECT 'X' FROM mf_criterios_x_envios_ges WHERE idenvio = cn_idenvio;
  
    lc_envios            c_envios%ROWTYPE;
    lb_found             BOOLEAN;
    le_error             EXCEPTION;
    ln_indice            PLS_INTEGER := 1;
    ld_fechaejecucion    DATE;
    lc_criterios_x_envio c_criterios_x_envio%ROWTYPE;
    lb_notfound          BOOLEAN;
  
  BEGIN
    
    OPEN c_envios(pn_idenvio);
    FETCH c_envios
      INTO lc_envios;
    lb_found := c_envios%FOUND;
    CLOSE c_envios;
  
    IF NOT lb_found THEN
      pv_msgerror := 'El Envio Nro:' || pn_idenvio || ' No Existe o no Fue Encontrado';
      RAISE le_error;
    END IF;
  
    IF lc_envios.estado != 'A' THEN
      pv_msgerror := 'El Envio Nro:' || pn_idenvio || ' se encuentra Inactivo';
      RAISE le_error;
    END IF;
  
    OPEN c_criterios_x_envio(pn_idenvio);
    FETCH c_criterios_x_envio
      INTO lc_criterios_x_envio;
    lb_notfound := c_criterios_x_envio%NOTFOUND;
    CLOSE c_criterios_x_envio;
  
    IF lb_notfound THEN
      pv_msgerror := 'Por favor verifique el Envio Nro:' || pn_idenvio || ', no tiene criterios asociados';
      RAISE le_error;
    END IF;
  
    ld_fechaejecucion := pd_fechaejecucion;
  
    WHILE (lc_envios.cantidad_mensajes >= ln_indice) LOOP
      mfk_obj_envio_ejecuciones_ges.mfp_insertar(pn_idenvio               => pn_idenvio,
                                                 pd_fecha_ejecucion       => ld_fechaejecucion,
                                                 pd_fecha_inicio          => NULL,
                                                 pd_fecha_fin             => NULL,
                                                 pv_mensaje_proceso       => NULL,
                                                 pn_tot_mensajes_enviados => NULL,
                                                 pn_secuencia             => pn_secuencia,
                                                 pv_msgerror              => pv_msgerror);
      ln_indice := ln_indice + 1;
      ld_fechaejecucion := ld_fechaejecucion +
                           lc_envios.horas_separacion_msn / 24;
    END LOOP;
    COMMIT;
  
  EXCEPTION
    WHEN le_error THEN
      RETURN;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la carga de subcriterios de categorias cliente  
  **               desde la tabla MF_CATEGORIAS_CLIENTE_GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_categoriza_cliente(pt_categ_cliente IN OUT gt_categ_cliente) IS
  
    CURSOR c_categ_cliente IS
      SELECT codigo AS code, descripcion AS names, segmento
        FROM mf_categorias_cliente_ges;
  
    ln_contador NUMBER;
  
  BEGIN
    
    ln_contador := 1;
    FOR i IN c_categ_cliente LOOP
      pt_categ_cliente(ln_contador).code := i.code;
      pt_categ_cliente(ln_contador).names := i.names;
      pt_categ_cliente(ln_contador).segmento := i.segmento;
      ln_contador := ln_contador + 1;
    END LOOP;
  
    BEGIN
      dbms_session.close_database_link('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      pt_categ_cliente(1).code := '0';
      pt_categ_cliente(1).names := 'SIN DATOS';
      pt_categ_cliente(1).segmento := 'SIN DATOS';
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la eliminacion de envios para GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_eliminar_logico(pn_idenvio   IN mf_envios_ges.idenvio%TYPE,
                                pv_msg_error IN OUT VARCHAR2) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER) IS
      SELECT * FROM mf_envios_ges e WHERE e.idenvio = cn_idenvio;
  
    lc_currentrow    mf_envios_ges%ROWTYPE;
    le_nodatadeleted EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_TRX_ENVIOS_GES.MFP_ELIMINAR_LOGICO';
    lv_error         VARCHAR2(1000);
    lb_found         BOOLEAN := FALSE;
    
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_table_cur(pn_idenvio);
    FETCH c_table_cur
      INTO lc_currentrow;
    lb_found := c_table_cur%FOUND;
    CLOSE c_table_cur;
  
    IF NOT lb_found THEN
      lv_error := 'El registro que desea borrar no existe. ' || lv_aplicacion;
      RAISE le_nodatadeleted;
    END IF;
  
    mfk_obj_envios_ges.mfp_eliminar_logico(pn_idenvio, lv_error);
    IF lv_error IS NOT NULL THEN
      RAISE le_nodatadeleted;
    END IF;
    COMMIT;
  
  EXCEPTION
    WHEN le_nodatadeleted THEN
      ROLLBACK;
      pv_msg_error := lv_error;
    WHEN OTHERS THEN
      ROLLBACK;
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

END MFK_TRX_ENVIOS_GES;
/
