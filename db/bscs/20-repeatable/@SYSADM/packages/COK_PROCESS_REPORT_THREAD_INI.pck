create or replace package COK_PROCESS_REPORT_THREAD_INI is

  GN_COMMIT NUMBER:=5000;

  TYPE C_CURSOR IS REF CURSOR;
  TYPE ARR_NUM IS TABLE OF NUMBER;

  --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_id_bitacora_scp number:=0;
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_PROCESS_REPORT';
  lv_referencia_scp varchar2(100):='COK_PROCESS_REPORT_THREAD_INI.MAIN';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
    type gr_array is table of varchar2(500) index by binary_integer;  
  ---------------------------------------------------------------

  PROCEDURE EjecutaSentencia (pv_sentencia in varchar2,
                              pn_errorcode out number,
                              pv_error     out varchar2);

  PROCEDURE ObtieneIniParams(pvTablaCuadre          out varchar2,
                            pvTableSpace            out varchar2,
                            pvCreaTabla             out varchar2,
                            pvTablaProceso          out varchar2,
                            pvTablaOrderHdr         out varchar2,
                            pvTablaPagos            out varchar2,                            
                            pnNroDespachadores      out number,
                            pdFechaCorte            out date,
                            pnIdBitacora            out number,
                            pv_error                out varchar2) 
                           ; -- (0) OK (!=0) ERROR}

  FUNCTION  GetControlProceso(pvProceso     varchar2,
                              pnDespachador number) 
                              return varchar2; --estado
                              

  PROCEDURE  SetControlProceso(pvProceso     varchar2,
                               pvEstado      varchar2,
                               pnDespachador number) ;



  PROCEDURE SetProcessParams(pnNroDespachadores   number,
                             pnIdBitacora         number,
                             pv_error             out varchar2);                                                        
                            
  FUNCTION SetCamposMora RETURN VARCHAR2;
  

  FUNCTION CreaTablaCuadre(pdFechaPeriodo in date,
                           pvNombreTabla  in varchar2,
                           pdNro_despachadores number,
                           pvNombreTableSpace in varchar2,
                           pv_error       out varchar2) RETURN NUMBER;      

  FUNCTION TruncaTablaCuadre(pvNombreTabla  in varchar2,
                             pv_error       out varchar2) RETURN NUMBER;                                                         

  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pdNro_despachadores number,
                                 pd_lvMensErr        out varchar2);


  PROCEDURE Main(pdFechCierrePeriodo in date, 
                 pvReproceso  varchar2,
                 pnBitacora out number, 
                 pd_lvMensErr out varchar2);


end COK_PROCESS_REPORT_THREAD_INI;
/
create or replace package body COK_PROCESS_REPORT_THREAD_INI is

  -- Variables locales
  gv_funcion varchar2(30);
  gv_mensaje varchar2(500);
  ge_error exception;

/*
  MODIFICADO POR     : CLS Jenniffer Cevallos --JCE
  LIDER SIS          : SIS Bosco Mora
  LIDER PDS          : CLS Geovanny Barrera 
  Motivo             : [A57]-Nuevos ajustes en procedimiento InsertaDatosCustomer 
                       para tomar las cuentas que cambiaron de ciclo
  Fecha Modificacion : 21/12/2016
  */

 -------------------------------------------------------
  --     EJECUTA_SENTENCIA
  --     Funci�n general que ejecuta sentencias dinamicas
  -------------------------------------------------------
  procedure EjecutaSentencia(pv_sentencia in varchar2,
                             pn_errorcode out number,
                             pv_error     out varchar2) is
    x_cursor integer;
    z_cursor integer;
    name_already_used exception;
    pragma exception_init(name_already_used, -955);
  begin
    x_cursor := dbms_sql.open_cursor;
    DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
    z_cursor := DBMS_SQL.EXECUTE(x_cursor);
    DBMS_SQL.CLOSE_CURSOR(x_cursor);
    pn_errorcode := 0;
  EXCEPTION
    WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := 'YA EXISTE EL OBJETO:';
      pn_errorcode := 1;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
      pn_errorcode := -1;
  END EjecutaSentencia;

--Prepara los valores de la configuraci�n de los procesos
--para que todos los procesos queden como pendientes
--tanto los que son un solo hilo como los multihilos
  PROCEDURE SetProcessParams(pnNroDespachadores number,
                             pnIdBitacora       number,
                             pv_error       out varchar2) is
  
  CURSOR C_ProcesosM is--(multihilo)                             
    select * from co_control_procesos where tipo='M';

  CURSOR C_ProcesosU is--(un solo hilo) 
    select * from co_control_procesos where tipo='U';
    
    lv_estados    varchar2(50):='';
    myException exception;  
    lv_error    varchar2(500);       
  begin
    
    for i in 1 .. pnNroDespachadores loop
        lv_estados := lv_estados||'P'||',';
    end loop;
    lv_estados := substr(lv_estados,1,length(lv_estados)-1);
  
    for i in C_ProcesosM loop
        update co_control_procesos
        set    valor = lv_estados
        where  campo = i.campo;
    end loop;
  
    for i in C_ProcesosU loop
        update co_control_procesos
        set    valor = 'P'
        where  campo = i.campo;
    end loop;
    
    update co_parametros_cliente
    set    valor =  to_char(pnIdBitacora)
    where  campo =  'ID_BITACORA_ACTUAL';
       
      EXCEPTION 
          WHEN OTHERS THEN
               lv_error := sqlerrm;
               lv_error := substr(lv_error,1,200);
               pv_error := lv_error;    
  
  end;
  


  PROCEDURE ObtieneIniParams(pvTablaCuadre          out  varchar2,
                            pvTableSpace            out  varchar2,
                            pvCreaTabla             out  varchar2,
                            pvTablaProceso          out  varchar2,
                            pvTablaOrderHdr         out  varchar2,
                            pvTablaPagos            out  varchar2,
                            pnNroDespachadores      out  number,
                            pdFechaCorte            out  date,
                            pnIdBitacora            out  number,
                            pv_error                out  varchar2) 
                             -- (0) OK (!=0) ERROR 
                            is

   CURSOR C_Parametro(pv_parametro varchar2) is
      select valor from co_parametros_cliente where campo=pv_parametro;                           
   
   LB_NOTFOUND boolean;   
   myException exception;  
   lv_fechacorte varchar2(10);
   lv_bitacora   varchar2(30);
   lv_error    varchar2(500);                    
   begin
      --parametro para la tabla del proceso
      OPEN   C_Parametro('TABLA_HILOS');
      FETCH  C_Parametro into pvTablaCuadre;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro TABLA_HILOS';
         raise myException;
      END IF;

      --parametro para la tablespace del proceso
      OPEN   C_Parametro('TABLESPACE');
      FETCH  C_Parametro into pvTableSpace;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro TABLESPACE';
         raise myException;
      END IF;

      --parametro que indica si la tabla debe crearse o truncarse
      OPEN   C_Parametro('CREA_TABLA_HILOS');
      FETCH  C_Parametro into pvCreaTabla;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro CREA_TABLA_HILOS';
         raise myException;
      END IF;


      --parametro que indica la tabla del proceso, utlima instancia
      OPEN   C_Parametro('TABLA_PROCESO');
      FETCH  C_Parametro into pvTablaProceso;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro TABLA_PROCESO';
         raise myException;
      END IF;
      
      
      --parametro que indica  la tabla orderhdr
      OPEN   C_Parametro('TABLA_ORDERHDR');
      FETCH  C_Parametro into pvTablaOrderHdr;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro TABLA_ORDERHDR';
         raise myException;
      END IF;
      
      
      --parametro que indica la tabla de pagos
      OPEN   C_Parametro('TABLA_PAGOS');
      FETCH  C_Parametro into pvTablaPagos;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro TABLA_PAGOS';
         raise myException;
      END IF;            



      --parametro para los hilos de la tabla del proceso
      OPEN   C_Parametro('NRO_HILOS');
      FETCH  C_Parametro into pnNroDespachadores;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro NRO_HILOS';
         raise myException;
      END IF;

      --parametro para los hilos de la tabla del proceso
      OPEN   C_Parametro('FECHA_CORTE');
      FETCH  C_Parametro into lv_fechacorte;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;

      pdFechaCorte := to_date(lv_fechacorte,'dd/mm/yyyy');     
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro FECHA_CORTE';
         raise myException;
      END IF;
  
      --parametro para los hilos de la tabla del proceso
      OPEN   C_Parametro('ID_BITACORA_ACTUAL');
      FETCH  C_Parametro into lv_bitacora;
      LB_NOTFOUND:=C_Parametro%NOTFOUND;
      CLOSE  C_Parametro;

      pnIdBitacora:=to_number(lv_bitacora);
      
      IF LB_NOTFOUND THEN
         lv_error := 'No se encuenta el parametro ID_BITACORA_ACTUAL';
         raise myException;
      END IF;  
  
      
      EXCEPTION 
          WHEN myException THEN
               lv_error := substr(lv_error,1,200);
               pv_error := lv_error;
          WHEN OTHERS THEN
               lv_error := sqlerrm;
               lv_error := substr(lv_error,1,200);
               pv_error := lv_error;    
   end;                            
 
   ---------------------------
  -- SET_CAMPOS_MORA
  ---------------------------
  FUNCTION SetCamposMora RETURN VARCHAR2 IS
    lvSentencia varchar2(2000);
    lnMesFinal  number;
  BEGIN
    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  END SetCamposMora;

 -----------------
  --  TRUNCA_TABLA
  -----------------

  FUNCTION TruncaTablaCuadre(pvNombreTabla  in varchar2,
                             pv_error       out varchar2) RETURN NUMBER IS                                                         
    lvSentenciaCrea VARCHAR2(20000);
    lnErrorCode     number;
    lvMensErr       VARCHAR2(2000);
    myException     exception;
    
   BEGIN
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD_INI.TruncaTablaCuadre',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------   
    lvSentenciaCrea := 'truncate table '||pvNombreTabla;

    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);


    if lvMensErr is not null then
       lvMensErr:='TRUNCA TABLA: '||lvMensErr;
       raise myException;
    end if;


    lvSentenciaCrea:='begin dbms_stats.gather_table_stats(ownname=> ''SYSADM'', tabname=> ''CO_CUADRE_BORRAR'', partname=> NULL , estimate_percent=> 80 , cascade=>TRUE ); end;';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);


    if lvMensErr is not null then
       lvMensErr:='ACTUALIZANDO ESTADISTICAS: '||lvMensErr;
       raise myException;
    end if;


    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD_INI.TruncaTablaCuadra',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    return 0;
  EXCEPTION
    when myException THEN
       pv_error:= substr(lvMensErr,1,200);
       return 1;
    when others THEN
      lvMensErr := sqlerrm;
      pv_error:= substr(lvMensErr,1,200);
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar TruncaTablaCuadre',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      return 2;

         
   END;

 -----------------
  --  CREA_TABLA
  -----------------
  FUNCTION CreaTablaCuadre  (pdFechaPeriodo in date,
                             pvNombreTabla  in varchar2,
                             pdNro_despachadores number,
                             pvNombreTableSpace in varchar2,
                             pv_error       out varchar2) RETURN NUMBER IS

    lvSentenciaCrea VARCHAR2(20000);
    lvSentenciaDrop VARCHAR2(200);
    lnErrorCode     number;
    lvMensErr       VARCHAR2(2000);
    myException     exception;

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD_INI.CreaTablaCuadre',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------


    

    lvSentenciaCrea := '';
    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura
                       COK_PROCESS_REPORT_THREAD_INI.SetCamposMora ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0,'||
                       '  COSTCENTER_ID       NUMBER,'||
                       '  CSTRADECODE         VARCHAR2(10),'||
                       '  PRGCODE             VARCHAR2(10),'||
                       '  TERMCODE            VARCHAR2(10),'||
                       '  csactivated         date, ' ||                       
                       '  DESPACHADOR         NUMBER(2),'||
                       '  ETAPA_PROCESO       VARCHAR2(3)'||
                       '  ) partition by range (DESPACHADOR) (';
                       for i in 1 .. pdNro_despachadores loop
                           lvSentenciaCrea := lvSentenciaCrea||
                                              '  partition P'||pvNombreTabla||i||' values less than ('||(i+1)||')'||
                                              '  tablespace '|| pvNombreTableSpace||
                                              '  pctfree 10  pctused 40 initrans 1' ||
                                              '  maxtrans 255 storage '||
                                              '  ('||
                                              '    initial 256K      next 256K   minextents 1' ||
                                              '    maxextents unlimited     pctincrease 0 ' ||
                                              '  ),'     ;                                         
                       end loop;
    lvSentenciaCrea := substr(lvSentenciaCrea,1,length(lvSentenciaCrea)-1);                  
    lvSentenciaCrea := lvSentenciaCrea||')';
                       
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);
    
    If ( lnErrorCode = 1 ) Then --ya existe la tabla la dropeo y vuelvo a intentar
        lvSentenciaDrop := 'drop table '||pvNombreTabla;
        lnErrorCode := 0;
        lvMensErr := null;
        EjecutaSentencia(lvSentenciaDrop, lnErrorCode,lvMensErr);
        If lvMensErr Is Not Null Then
           lvMensErr:='DROP TABLA: '||lvMensErr;
           raise myException;
        End if;  
        
        EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);  
    End If;


    if lvMensErr is not null then
       lvMensErr:='CREACION TABLA: '||lvMensErr;
       raise myException;
    end if;



 /*   -- Crea clave primaria
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PKCUSTOMER_ID' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                       '  primary key (CUSTOMER_ID)' || '  using index' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION CLAVE PRIMARIA: '||lvMensErr;
       raise myException;
    end if;
*/

    lvSentenciaCrea := 'create index CO_CUSTID_IDX'||
                        to_char(pdFechaPeriodo, 'ddMMyyyy')|| ' on  '||
                        pvNombreTabla ||' (CUSTOMER_ID) LOCAL' ||
                       ' tablespace '||pvNombreTableSpace|| ' pctfree 10' || 
                       ' initrans 2' ||' maxtrans 255' || ' storage' || 
                       ' (initial 504K' || '  next 520K' ||
                       '   minextents 1' || '  maxextents unlimited' ||
                       '   pctincrease 0)';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);


    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE1: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;



    lvSentenciaCrea := 'create index CO_CUSTCO_IDX'||
                        to_char(pdFechaPeriodo, 'ddMMyyyy')|| ' on  '||
                        pvNombreTabla ||' (CUSTCODE) LOCAL' ||
                       ' tablespace '||pvNombreTableSpace|| ' pctfree 10' || 
                       ' initrans 2' ||' maxtrans 255' || ' storage' || 
                       ' (initial 504K' || '  next 520K' ||
                       '   minextents 1' || '  maxextents unlimited' ||
                       '   pctincrease 0)';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);


    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE2: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;


    --  Crea indice
    lvSentenciaCrea := 'create index CO_CUSTI_IDX' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (CUSTOMER_ID, tipo) LOCAL' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE3: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;
    
    --  Crea indices necesarios para despachadores y control de ejecucion
  /*  lvSentenciaCrea := 'create index ID_CUST_DESP_ETAP' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (CUSTOMER_ID, DESPACHADOR,ETAPA_PROCESO)' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE4: '||lvMensErr;
       raise myException;
    end if;*/


    --  Crea indices necesarios para actualizar la forma de pago cbill
    lvSentenciaCrea := 'create index CO_FORMPAG_IDX' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (FORMA_PAGO) LOCAL' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE4: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;


    --  Crea indices necesarios para actualizar la forma de pago cbill
    lvSentenciaCrea := 'create index CO_PRGCODE_IDX' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (PRGCODE) LOCAL' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE5: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'alter PACKAGE COK_PROCESS_REPORT_THREAD COMPILE BODY';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);
    

    if lvMensErr is not null then
       lvMensErr:='RECOMPILA PROCEDIMIENTO: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);

    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EjecutaSentencia(lvSentenciaCrea, lnErrorCode,lvMensErr);

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD_INI.CreaTablaCuadre',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 0;

  EXCEPTION
    when myexception THEN
       pv_error:= lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaTablaCuadre',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       return 1;
    when others THEN
      lvMensErr := sqlerrm;
      pv_error:= lvMensErr;

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaTablaCuadre',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

  END CreaTablaCuadre;
 
 
  ---------------------------------------------------------------------
  --  INSERTADATOS_CUSTOMER
  --  Extraer los codigos de las cuentas de la tabla customer_all
  --  para tener la referencia inicial de datos de clientes a procesar
  ---------------------------------------------------------------------
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pdNro_despachadores number,
                                 pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);--[A57] JCE 
    lvMensErr   varchar2(1000);
    ciclo       varchar2(2);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT.InsertaDatosCustomer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    --INI [A57] JCE 
    /*lvSentencia := 'insert \*+ APPEND*\ into ' || pdNombre_tabla ||
                   ' NOLOGGING (custcode,customer_id,costcenter_id,cstradecode,termcode,prgcode,csactivated,despachador) ' ||
                   ' select a.custcode, a.customer_id,a.costcenter_id,a.cstradecode,a.termcode,a.prgcode,a.csactivated,ceil(dbms_random.value(0,'||pdNro_despachadores||')) despachador' ||
                   ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma' ||
                   ' where a.billcycle = ma.id_ciclo_admin' ||
                   ' and ci.id_ciclo = ma.id_ciclo' ||
                   ' and a.paymntresp  = ''X''' ||
                   ' and ci.dia_ini_ciclo = ''' ||
                   to_char(pdFechCierrePeriodo, 'dd') || '''';*/
                   
    
    lvSentencia := 'insert /*+ APPEND*/ into ' || pdNombre_tabla ||
                   ' NOLOGGING (custcode,customer_id,costcenter_id,cstradecode,termcode,prgcode,csactivated,despachador) ' ||
                   ' select a.custcode, a.customer_id,a.costcenter_id,a.cstradecode,a.termcode,a.prgcode,a.csactivated,ceil(dbms_random.value(0,'||pdNro_despachadores||')) despachador' ||
                   ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma' ||
                   ' where a.billcycle = ma.id_ciclo_admin' ||
                   ' and ci.id_ciclo = ma.id_ciclo' ||
                   ' and a.paymntresp  = ''X''' ||
                   ' and (ci.dia_ini_ciclo = '''||to_char(pdFechCierrePeriodo, 'dd') || ''''||
                   ' or a.lbc_date =TO_DATE('''||pdFechCierrePeriodo||''','||'''DD/MM/YYYY' ||''''||'))';

    --FIN [A57] JCE 
    
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    pd_lvMensErr := lvMensErr;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT.InsertaDatosCustomer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'InsertaDatosCustomer: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar InsertaDatosCustomer',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;


  

 
 
 

  -------------------------------
  --  CALCULA MORA
  --  Calcula las edades de mora
  -------------------------------
  -- [2574] Se modific� para corregir casos de
  -- edad de mora incorrecta reportados por SCO.
  -- Modificado por: Sis Nadia D�vila O.
  -- L�der: Galo Jer�z.
  -- Fecha: 07/09/2007.
  -----------------------------------
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Si en una migracion de ciclo el cliente
     tiene cartera vencida la mora se mantiene, si la cartera
     esta vigente la mora es 0.  Para el caso de cartera
     vigente se mantiene el mismo criterio para cualquier
     cambio de ciclo, no s�lo en el cambio de
     ciclo 1 al 2 como estaba anteriormente.
  =========================================================*/
  /*==============================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion - 
                      Revisi�n de Cuentas Migradas al cuarto ciclo 
                      de facturaci�n
     Fecha Modif.   : 20/10/2008
     Solicitado por : SIS Romeo Cabrera
     Modificado por : Anl. Reyna Asencio
     Motivo         : Si hay una migraci�n de ciclo y la mora es vigente,
                      la cuenta pasa al nuevo ciclo de facturaci�n con mora
                      0 y si la cuenta tiene una mora vencida pasa al nuevo 
                      ciclo con la misma edad de mora, es decir la mora 
                      se mantiene.
    ==============================================================
  */
  
  
--=====================================================================================--

  function StringToArray(fv_cadena  in varchar2,  --cadena de respuesta del comando IDL
                         fv_split   in varchar2,  --caracter separador de campos
                         fr_arreglo in out nocopy gr_array,  --arreglo de tipo varchar2
                         fv_error   out varchar2) return number is --0:Exito 1:Error

  lv_salida   varchar2(3000):=null;
  lv_temp     varchar2(500):=null;
  lv_error    varchar2(250):=null;
  ln_cont     number:=0;
  le_error    exception;

  begin

    lv_salida:= fv_cadena || fv_split;
    lv_temp:= substr(lv_salida, 1, instr(lv_salida, fv_split, 1, 1) - 1);
    fr_arreglo(0):= lv_temp;
    ln_cont:= 1;

    begin
      while length(lv_temp) > 0 loop
            lv_temp:=
            substr(lv_salida,
            instr(lv_salida, fv_split, 1, ln_cont) + 1,
            (instr(lv_salida, fv_split, 1, ln_cont + 1) -
            instr(lv_salida, fv_split, 1, ln_cont) - 1));
            --
            if lv_temp is not null then
               fr_arreglo(ln_cont):= lv_temp;
               ln_cont:= ln_cont+1;
            end if;
      end loop;
    exception
      when others then
        lv_error:=substr(sqlerrm,1,250);
        raise le_error;
    end;

    fv_error:=null;
    return 0;

  exception
    when le_error then
      fv_error:=lv_error;
      return 1;
    when others then
      fv_error:=substr(sqlerrm,1,250);
      return 1;
  end StringToArray;

--=====================================================================================--

--========================================================================
--========================================================================
-- Maneja la tabla co_control_procesos
-- Realiza consultas a la tabla para verificar el estado del proceso
-- en determinado despachador, la idea es que el proceso principal consulte
-- esta funci�n para determinar si el proceso est� pendiente en un despachador X,
-- de la misma forma hay un procedimiento para setear el estado de un procedimiento
-- en un determinado despachador indicando los parametros adecuados -> SetControlProceso
-- Estructura de la tabla:
-- campo := Nombre del Proceso EJ: LlenaDatosCliente
-- valor := Estado de los Despachadores 
--          EJ: P,E,P,F,P (indica/DESP1 estado P
--                                DESP2 estado E
--                                DESP3 estado P
--                                DESP4 estado F
--                                DESP5 estado P) los estados de los despachadores
--                                                de forma secuencial
-- descripcion := se explica solo
  FUNCTION  GetControlProceso(pvProceso     varchar2,
                              pnDespachador number) 
                              return varchar2 is --estado
  CURSOR C_Procesos is
    select valor
    from co_control_procesos 
    where campo=pvProceso;
  lv_valor     varchar2(50);
  lr_estados   gr_array;
  lb_not_found boolean;
  lv_error     varchar2(500);
  lv_estado    varchar2(1);

  begin

     Open C_Procesos;
     Fetch C_Procesos into lv_valor;
     lb_not_found:=C_Procesos%NOTFOUND;
     Close C_Procesos;
     If lb_not_found Then
        return 'N';
     End If;

     
    If (StringToArray(lv_valor,',',lr_estados,lv_error) =1  ) Then
       return 'N';                                            
    End If;
  
  
    If lr_estados.count > 0 Then
          lv_estado := lr_estados(pnDespachador-1); 
          If lv_estado = 'E' Then          
             lv_estado:='P'; --para que coja todos los que son P o E
          End If;
          return lv_estado;
    End If;
     
     return 'N';
     EXCEPTION WHEN OTHERS THEN
         return 'N';
  end;
  
  PROCEDURE  SetControlProceso(pvProceso     varchar2,
                               pvEstado      varchar2,
                               pnDespachador number) is  
                                                                
  CURSOR C_Procesos is
    select valor
    from co_control_procesos 
    where campo=pvProceso;
  lv_valor     varchar2(50);
  lv_valor_new varchar2(50):='';
  lr_estados   gr_array;
  lb_not_found boolean;
  lv_error     varchar2(500);                                                         
 
 PRAGMA AUTONOMOUS_TRANSACTION;    
  begin
     Open C_Procesos;
     Fetch C_Procesos into lv_valor;
     lb_not_found:=C_Procesos%NOTFOUND;
     Close C_Procesos;
     If not lb_not_found Then
         If ( StringToArray(lv_valor,',',lr_estados,lv_error) =0  ) Then
            If (lr_estados.count > 0 And lr_estados.count>=pnDespachador) Then
                  lr_estados(pnDespachador-1):=pvEstado;
                  For i In 0 .. lr_estados.count-1 Loop
                      lv_valor_new:=lv_valor_new||lr_estados(i)||',';
                  End Loop; 
                  lv_valor_new:=substr(lv_valor_new,1,length(lv_valor_new)-1);
                  Update co_control_procesos
                         Set valor=lv_valor_new
                  Where campo=pvProceso;
                  Commit;                     
            End if;  
         End If;
      End If;
   
    EXCEPTION WHEN OTHERS THEN
              null;
  end;
--========================================================================  
--========================================================================
  
 


  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE Main(pdFechCierrePeriodo in date, 
                 pvReproceso  varchar2, 
                 pnBitacora out number, 
                 pd_lvMensErr out varchar2 ) IS

    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    -- variables
    lvSentencia            VARCHAR2(2000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    lvMensErr              VARCHAR2(2000) := null;
    lnExito                NUMBER;
    lv_nombre_tabla        varchar2(50);
    lv_tabla_cuadre        varchar2(50);
    lv_tablespace          varchar2(50);    
    lv_creatabla           varchar2(1);    
    ln_hilos               number;
    lv_Tabla_OrderHdr      varchar2(50);
    lv_Tabla_cashreceipts  varchar2(50);
    lv_Tabla_customer      varchar2(50);
    lv_TablaCustomerAllAnt varchar2(50);
    lnMes                  number;
    ld_fechaper               date;
    leError                exception;

  BEGIN

       lvSentencia := 'grant select , references, index  on co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ') || '  to wfrec ';

       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --

    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT_THREAD_INI.Main',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    


    --Obteniendo los parametros iniciales
    ObtieneIniParams(pvTablaCuadre      => lv_tabla_cuadre,
                     pvTableSpace       => lv_tablespace,
                     pnNroDespachadores => ln_hilos,
                     pvTablaProceso     => lv_nombre_tabla,
                     pvTablaOrderHdr    => lv_Tabla_OrderHdr,
                     pvTablaPagos       => lv_Tabla_cashreceipts,
                     pvCreaTabla        => lv_creatabla,
                     pdFechaCorte       => ld_fechaper, --este parametro no se usa dentro del main
                     pnIdBitacora       => pnBitacora,
                     pv_error           => lvMensErr);
                               
    If (lvMensErr is not null) Then
       raise leError;
    End If;    
    
     pnBitacora:= ln_id_bitacora_scp;
    
     If pvReproceso='N' Then
    
       SetProcessParams(pnNroDespachadores => ln_hilos,
                        pnIdBitacora       => ln_id_bitacora_scp,
                        pv_error           => lvMensErr);

        commit;
        if lvMensErr is not null then
          raise leError;
        end if;  
        commit;                               
     End If;

    If GetControlProceso('CreaTruncaTablaCuadre',1)='P' Then
        If lv_creatabla='S' Then
           lnExito:= CreaTablaCuadre(pdFechaPeriodo      => pdFechCierrePeriodo,
                                     pvNombreTabla       => lv_tabla_cuadre,
                                     pdNro_despachadores => ln_hilos,
                                     pvNombreTableSpace  => lv_tablespace,
                                     pv_error            => lvMensErr);
                                       
        Else
           lnExito:= TruncaTablaCuadre(pvNombreTabla => lv_tabla_cuadre,
                                       pv_error      => lvMensErr);    
        End If;
        If (lnExito != 0) Then
           SetControlProceso('CreaTruncaTablaCuadre','E',1);
           raise leError;
        End If; 
        SetControlProceso('CreaTruncaTablaCuadre','F',1);
     End if;

    If GetControlProceso('InsertaDatosCustomer',1)='P' Then
        InsertaDatosCustomer(pdFechCierrePeriodo => pdFechCierrePeriodo,
                             pdNombre_tabla      => lv_tabla_cuadre,
                             pdNro_despachadores => ln_hilos,
                             pd_lvMensErr        => lvMensErr);

        if lvMensErr is not null then
          SetControlProceso('InsertaDatosCustomer','E',1);
          raise leError;
        end if;
        SetControlProceso('InsertaDatosCustomer','F',1);
    End If;
      

      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. 
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_PROCESS_REPORT_THREAD_INI.Main',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

      --SCP:FIN Se finalizara a lo que termine el proceso completo
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
 --     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
 --     scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el Main',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el Main',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;

  END Main;

--===============================================================================================-
                                                   
  

end COK_PROCESS_REPORT_THREAD_INI;
/
