CREATE OR REPLACE PACKAGE GSI_CONF_MATRICES_QC_PLANES IS

--====================================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 15212 Automatizacion de QC Matrices de Configuracion
  -- Fecha         : 17/11/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizacion de Validacion de Matrices COM vs las Plataformas
--====================================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 15212 Automatizacion de QC Matrices de Configuracion
  -- Fecha         : 19/12/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizacion de Validacion de Matrices COM vs las Plataformas
--====================================================================================================--  
   --
   FUNCTION FMQ_OBTENGO_PARAMETROS (PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2;
   --
   PROCEDURE PMQ_TARIFAS_ANTERIOR (PD_CIERRE_ACTUAL   IN DATE,
                                   PD_CIERRE_ANTERIOR OUT DATE,
                                   PV_ERROR           OUT VARCHAR2);
   --                                    
   PROCEDURE PMQ_TARIFAS_FECHA_ACTUAL (PD_CIERRE_ACTUAL  OUT DATE,
                                       PV_ERROR          OUT VARCHAR2);
   --
   PROCEDURE PMQ_QUITA_ESPACIOS_TABLA (PV_ERROR OUT VARCHAR);
   --
   PROCEDURE PMQ_CARGA_TABLA_RESUMEN (PV_ERROR OUT VARCHAR2);
   --
   PROCEDURE PMQ_CARGA_ID_DETALLE_PLAN (PV_ERROR OUT VARCHAR2); 
   --
   PROCEDURE PMQ_EJECUTA_SENTENCIAS (PV_FECHA IN VARCHAR2,PV_ERROR OUT VARCHAR2); 
   --
   PROCEDURE PMQ_GENERERA_ARCHIVO (PV_ARCHIVO     OUT VARCHAR2,
                                   PV_ERROR       OUT VARCHAR2);
   --
   PROCEDURE PMQ_TRANSFIERE_ARCHIVO (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                     PN_ERROR           OUT NUMBER,
                                     PV_ERROR           OUT VARCHAR2); 
   --
   PROCEDURE PMQ_ENVIA_REPORTE_MAIL (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                     PV_ERROR           OUT VARCHAR2);   
                                     
   --
   PROCEDURE PMQ_EJECUTA_PROCESO_MATRIZ (PV_FECHA IN VARCHAR2,
                                         PV_ERROR OUT VARCHAR2);   
                                         
   --
   PROCEDURE PMQ_FEATURE_FIJO_PLAN (PN_ID_PROMOCION IN NUMBER,
                                    PV_COD_SEQ      IN VARCHAR2,
                                    PV_TRANSACCION  IN VARCHAR2,
                                    PV_MENSAJE      IN VARCHAR2,
                                    PV_USUARIO      IN VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2);
   --
   PROCEDURE PMQ_PROMOCIONES_MEGAS (PL_CADENA IN LONG, PV_ERROR OUT VARCHAR2);                                                                                                                               
   -- INI PMERA - 08/11/2016 Se crea un proceso para ejecute los procesos de Qc Planes y Validaciones de Matrices mediante un JOB   
    PROCEDURE EJECUTA_PROCESOS_QC_JOBS (PV_NOM_PROCESO IN VARCHAR2,
                                        PV_FECHA       IN VARCHAR2,
                                        PV_ERROR       OUT VARCHAR2);
                                    
END GSI_CONF_MATRICES_QC_PLANES;
/
CREATE OR REPLACE PACKAGE BODY GSI_CONF_MATRICES_QC_PLANES IS
--====================================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 15212 Automatizacion de QC Matrices de Configuracion
  -- Fecha         : 17/11/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizacion de Validacion de Matrices COM vs las Plataformas
--====================================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 15212 Automatizacion de QC Matrices de Configuracion
  -- Fecha         : 19/12/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizacion de Validacion de Matrices COM vs las Plataformas
--====================================================================================================-- 
 FUNCTION FMQ_OBTENGO_PARAMETROS(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS

  --
  CURSOR OBTIENE_PARAMETRO (CN_TIPO_PARAMETRO IN NUMBER,CV_ID_PARAMETRO IN VARCHAR2)IS
  SELECT T.VALOR
    FROM GV_PARAMETROS T
   WHERE T.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
     AND T.ID_PARAMETRO = CV_ID_PARAMETRO;
  -- 
     LV_VALOR_PAR   VARCHAR2(1000);
   BEGIN
    
      OPEN OBTIENE_PARAMETRO (15212,PV_COD_PARAMETRO);
      FETCH OBTIENE_PARAMETRO INTO LV_VALOR_PAR;
      CLOSE OBTIENE_PARAMETRO;
       RETURN LV_VALOR_PAR;
    EXCEPTION
     WHEN OTHERS THEN
     RETURN NULL;
      
 END FMQ_OBTENGO_PARAMETROS;
 
 PROCEDURE PMQ_TARIFAS_ANTERIOR (PD_CIERRE_ACTUAL   IN DATE,
                                 PD_CIERRE_ANTERIOR OUT DATE,
                                 PV_ERROR           OUT VARCHAR2) IS
  --
  CURSOR CAMBIO_FECHA (CV_DIA VARCHAR2)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE FA.DIA_INI_CICLO=CV_DIA; 
  --
  LV_DIA              VARCHAR2(2);
  LV_FECHA_ANT        VARCHAR2(2); 
  LV_ERROR            VARCHAR2(2000);         
  LE_ERROR            EXCEPTION;
  LC_FECHA_ANT        CAMBIO_FECHA%ROWTYPE;  
  LB_FOUND            BOOLEAN;
  --
  BEGIN
    --
     -- EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';--POR PRUEBAS
    --
   LV_DIA:= EXTRACT (DAY FROM  PD_CIERRE_ACTUAL);
   
   IF LV_DIA =  2  THEN
     LV_FECHA_ANT:='24';
     --
   ELSIF LV_DIA = 8  THEN
     LV_FECHA_ANT:='02';
   ELSIF LV_DIA = 15 THEN
     LV_FECHA_ANT:='08';
   ELSIF LV_DIA = 24 THEN
     LV_FECHA_ANT:= '15';
   END IF;
   --
    OPEN CAMBIO_FECHA (LV_FECHA_ANT);
         FETCH CAMBIO_FECHA INTO LC_FECHA_ANT;
         LB_FOUND:=CAMBIO_FECHA%FOUND;
         CLOSE CAMBIO_FECHA;
        --
         IF LB_FOUND THEN
           IF LV_FECHA_ANT = '24' THEN
           PD_CIERRE_ANTERIOR:=ADD_MONTHS(TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR'),-1);
           ELSE
           PD_CIERRE_ANTERIOR:=TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR');  
           END IF;
         ELSE
           LV_ERROR:='No se obtuvo la fecha del ciclo anterior para la tablas tarifas';
           RAISE LE_ERROR;
         END IF;
   --                                         
   EXCEPTION
   WHEN LE_ERROR THEN
     PV_ERROR := LV_ERROR;
   WHEN OTHERS THEN
     PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);         

 END PMQ_TARIFAS_ANTERIOR;                                
 
 PROCEDURE PMQ_TARIFAS_FECHA_ACTUAL (PD_CIERRE_ACTUAL         OUT DATE,
                                     PV_ERROR                 OUT VARCHAR2)IS

 -- 
  CURSOR FECHA (CV_CICLO VARCHAR)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE ID_CICLO =CV_CICLO;   
 
   LV_ERROR            VARCHAR2(2000);
   LV_CICLO            VARCHAR2(2);
   LE_ERROR_CICLO      EXCEPTION;
   LC_FECHA            FECHA%ROWTYPE;  
   LB_FOUND            BOOLEAN;
   LN_DIA              VARCHAR2(2);
 --
 BEGIN
    
    --
     -- EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';--POR PRUEBAS
    --

     LN_DIA:= EXTRACT (DAY FROM  SYSDATE);
     --
     IF LN_DIA >=2  AND LN_DIA< 8 THEN 
      LV_CICLO:='04';
     ELSIF LN_DIA >= 8 AND LN_DIA <= 14 THEN
     
     LV_CICLO:='02';
    
     ELSIF LN_DIA >= 15 AND LN_DIA < 24 THEN
     
      LV_CICLO:='03';
     
     ELSIF (LN_DIA >= 24 AND LN_DIA <= 31) OR LN_DIA =1   THEN
     
      LV_CICLO:='01';
     
     
     END IF; 
         OPEN FECHA (LV_CICLO);
         FETCH FECHA INTO LC_FECHA;
         LB_FOUND:=FECHA%FOUND;
         CLOSE FECHA;
        --
         IF LB_FOUND THEN
           PD_CIERRE_ACTUAL:=TO_DATE(LC_FECHA.FECHA_INICIO,'DD/MM/RRRR');
         ELSE
           LV_ERROR:='No se obtuvo el ciclo admistrativo de la fecha ' || TO_CHAR(SYSDATE,'DD/MM/RRRR');
           RAISE LE_ERROR_CICLO;
         END IF;
       
 EXCEPTION
  WHEN LE_ERROR_CICLO THEN
  PV_ERROR := LV_ERROR;
 WHEN OTHERS THEN
  PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);         

END   PMQ_TARIFAS_FECHA_ACTUAL;           
 
 --
 PROCEDURE PMQ_QUITA_ESPACIOS_TABLA (PV_ERROR OUT VARCHAR) IS
 --
 LV_QUERY        LONG;
 LN_CONT         NUMBER:=1;
 LN_NUM_PLANES   NUMBER;
 LN_NUM_PROMO    NUMBER; 
 LV_TRAMA        VARCHAR2(4000);
 LV_USUARIO      VARCHAR2(50);
 BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
   --
   INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
   VALUES ('MQ_LIMP','Inicio Quitar espacios en blancos de las tablas de Trabajo de la carga principal',sysdate,'F','',LV_USUARIO);
   -- 
   LN_NUM_PLANES:=TO_NUMBER(FMQ_OBTENGO_PARAMETROS('CONFG_NUM_PAQUETES'));
   LN_NUM_PROMO :=TO_NUMBER(FMQ_OBTENGO_PARAMETROS('CONFG_NUM_PROMO'));
   --
    
    LV_QUERY:='UPDATE GSI_PLANES_CARGA_INICIAL T
               SET T.ID_PLAN= UPPER(TRIM(ID_PLAN))';
    WHILE LN_CONT <= LN_NUM_PLANES LOOP
     
     LV_TRAMA:=LV_TRAMA||', T.COSTO_PAQ'||TO_CHAR(LN_CONT)||'= TRIM(T.COSTO_PAQ'||TO_CHAR(LN_CONT)||'),'
               ||' T.CODIGO_PAQ'||TO_CHAR(LN_CONT)||'= TRIM(T.CODIGO_PAQ'||TO_CHAR(LN_CONT)||'),'
               ||' T.TIPO'||TO_CHAR(LN_CONT)||'= UPPER(TRIM(T.TIPO'||TO_CHAR(LN_CONT)||')),'
               ||' T.OCULTO_ROL'||TO_CHAR(LN_CONT)||'= UPPER(TRIM(T.OCULTO_ROL'||TO_CHAR(LN_CONT)||')),'
               ||' T.PCRF_'||TO_CHAR(LN_CONT)||'= UPPER(TRIM(T.PCRF_'||TO_CHAR(LN_CONT)||'))';
   
    
     LN_CONT := LN_CONT + 1;
    END LOOP;
    LV_QUERY:=LV_QUERY||LV_TRAMA;
    LV_QUERY:=LV_QUERY||',T.PRODUCTO=UPPER(TRIM(T.PRODUCTO)),T.TIPO_PLAN=UPPER(TRIM(T.TIPO_PLAN)),T.CATEGORIA_PLAN=UPPER(TRIM(T.CATEGORIA_PLAN)),T.CUOTA_MENSUAL=TRIM(T.CUOTA_MENSUAL),'
                      ||'T.TB_VOZ=TRIM(T.TB_VOZ),T.PRECIO_INPOOL=TRIM(T.PRECIO_INPOOL),T.PRECIO_CLARO=TRIM(T.PRECIO_CLARO),T.PRECIO_MOVISTAR=TRIM(T.PRECIO_MOVISTAR),T.PRECIO_ALEGRO=TRIM(T.PRECIO_ALEGRO),T.PRECIO_FIJAS=TRIM(T.PRECIO_FIJAS),'
                      ||'T.TOLL=UPPER(TRIM(T.TOLL)),T.VIGENCIA_ADENDUM=TRIM(T.VIGENCIA_ADENDUM)';
    
    --
    EXECUTE IMMEDIATE LV_QUERY;
    --
    COMMIT;
    --
    LV_TRAMA:='';
    LN_CONT:=1;
    --
    LV_QUERY:='UPDATE GSI_TABLA_PROMO_INICIAL T
               SET T.ID_PLAN= UPPER(TRIM(ID_PLAN))';
    
    
    WHILE LN_CONT <= LN_NUM_PROMO LOOP
     
     LV_TRAMA:=LV_TRAMA||', T.ID_PROMO'||TO_CHAR(LN_CONT)||'= TRIM(T.ID_PROMO'||TO_CHAR(LN_CONT)||')';
   
     IF LN_CONT < 6 THEN
      LV_TRAMA:=LV_TRAMA||', T.TIPO_PROMO'||TO_CHAR(LN_CONT)||'= UPPER(TRIM(T.TIPO_PROMO'||TO_CHAR(LN_CONT)||'))';
      
     END IF; 
     
     LN_CONT := LN_CONT + 1;
     
    END LOOP;
    LV_QUERY:=LV_QUERY||LV_TRAMA;
    LV_QUERY:=LV_QUERY||',T.FEATURE_SMS=TRIM(T.FEATURE_SMS),T.FEATURE_MEGAS_NO_FIJO=TRIM(T.FEATURE_MEGAS_NO_FIJO),T.CUPO_MEGAS_NO_FIJO=TRIM(T.CUPO_MEGAS_NO_FIJO)';

    --
    EXECUTE IMMEDIATE LV_QUERY;
    
    --
    COMMIT;
    -- 
    LV_TRAMA:='';
    LN_CONT:=1;
    --
    LV_QUERY:='UPDATE GSI_FEATURES_A_ELIMINAR T
               SET T.ID_PLAN= UPPER(TRIM(ID_PLAN))';
    
    WHILE LN_CONT <= LN_NUM_PLANES LOOP
     
     LV_TRAMA:=LV_TRAMA||', T.CODIGO'||TO_CHAR(LN_CONT)||'= TRIM(T.CODIGO'||TO_CHAR(LN_CONT)||')';
   
    
     LN_CONT := LN_CONT + 1;
    END LOOP;
    --
    LV_QUERY:=LV_QUERY||LV_TRAMA;
    EXECUTE IMMEDIATE LV_QUERY;
    --
    COMMIT;
    --           
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES ('MQ_LIMP','Fin Quitar espacios en blancos de las tablas de Trabajo de la carga principal',sysdate,'F','',LV_USUARIO);
    --
    COMMIT;          
 EXCEPTION
   WHEN OTHERS THEN
    --
    ROLLBACK;
    PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);   
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES ('MQ_LIMP','ERROR Quitar espacios en blancos de las tablas de Trabajo de la carga principal',sysdate,'F',PV_ERROR,LV_USUARIO);
    --
    COMMIT;
 END  PMQ_QUITA_ESPACIOS_TABLA; 
 --
 PROCEDURE PMQ_CARGA_TABLA_RESUMEN (PV_ERROR OUT VARCHAR2) IS
   --
  
   
   --
   CURSOR TIPO_PROMO IS
   SELECT A.ROWID,  B.TIPO_PROMO 
   FROM   GSI_TABLA_FINAL_PROMO A, PORTA.MK_PROMOCIONES_MAESTRA_AUT@AXIS B
   WHERE (A.TIPO_PROMO IS NULL)
   AND    A.ID_PROMO = B.ID_PROMOCION; 
   --
   CURSOR FEATURE_SMS IS 
   SELECT T2.ROWID,T1.FEATURE_SMS  FROM 
   (SELECT T.ID_PLAN, T.FEATURE_SMS FROM GSI_TABLA_PROMO_INICIAL T
    WHERE T.FEATURE_SMS IS NOT NULL
    AND T.ID_PLAN IN (SELECT B.ID_PLAN FROM GSI_TABLA_FINAL_PROMO B
                            WHERE B.TIPO_PROMO='PROMO_SMS')) T1 ,GSI_TABLA_FINAL_PROMO T2
    WHERE T2.ID_PLAN=T1.ID_PLAN            
    AND  T2.TIPO_PROMO='PROMO_SMS';   
   --
   CURSOR FEATURE_MEGAS IS
   SELECT T2.ROWID,T1.FEATURE_MEGAS_NO_FIJO,T1.CUPO_MEGAS_NO_FIJO FROM (SELECT T.ID_PLAN ,T.FEATURE_MEGAS_NO_FIJO,T.CUPO_MEGAS_NO_FIJO
   FROM GSI_TABLA_PROMO_INICIAL T 
   WHERE T.FEATURE_MEGAS_NO_FIJO IS NOT NULL  OR T.CUPO_MEGAS_NO_FIJO IS NOT NULL  
   AND T.ID_PLAN IN (SELECT B.ID_PLAN FROM GSI_TABLA_FINAL_PROMO B
                       WHERE B.TIPO_PROMO='PROMO_BANDA_ANCHA')) T1 ,GSI_TABLA_FINAL_PROMO T2
   WHERE T2.ID_PLAN=T1.ID_PLAN            
   AND  T2.TIPO_PROMO='PROMO_BANDA_ANCHA';    
   --
   LN_NUM_PLANES           NUMBER;
   LN_NUM_PROMO            NUMBER;
   LV_USUARIO              VARCHAR2(50);
   LV_QUERY_PL             LONG;
   LV_QUERY_PR             LONG;
   CONT                    NUMBER:=1;
   LE_ERROR                EXCEPTION;
   LV_ERROR                VARCHAR(1000);
   LV_TIPO_PROMO           VARCHAR2(50);
 BEGIN
   -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
   --
   INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
   VALUES ('MQ_RESU','Inicio Carga las Tablas de Trabajo para la Matriz Configuracion',sysdate,'F',NULL,LV_USUARIO);
   COMMIT; 
   --
   LN_NUM_PLANES:=TO_NUMBER(FMQ_OBTENGO_PARAMETROS('CONFG_NUM_PAQUETES'));
   LN_NUM_PROMO :=TO_NUMBER(FMQ_OBTENGO_PARAMETROS('CONFG_NUM_PROMO'));
   WHILE CONT <= LN_NUM_PLANES LOOP
    LV_QUERY_PL:=LV_QUERY_PL||'SELECT T.ID_PLAN,T.CODIGO_PAQ'||TO_CHAR(CONT)||
        ' AS CODIGO ,T.COSTO_PAQ'||TO_CHAR(CONT)||' AS COSTO ,T.TIPO'||TO_CHAR(CONT)||
        ' AS TIPO ,T.OCULTO_ROL'||TO_CHAR(CONT)||
        ' AS OCULTO_ROL ,T.PCRF_'||TO_CHAR(CONT)||' AS FEATURE_PCRF FROM GSI_PLANES_CARGA_INICIAL T <UNION>';
    
    IF CONT = LN_NUM_PLANES THEN
     LV_QUERY_PL:=REPLACE(LV_QUERY_PL,'<UNION>','');
    ELSE 
     LV_QUERY_PL:=REPLACE(LV_QUERY_PL,'<UNION>','UNION ALL ');
    END IF; 

    --      
    CONT := CONT + 1;
    END LOOP; 
    BEGIN
       EXECUTE IMMEDIATE 'INSERT INTO GSI_TABLA_RESUMEN (ID_PLAN,CODIGO,COSTO,TIPO,OCULTO_ROL,FEATURE_PCRF) '|| LV_QUERY_PL;
       --
       DELETE FROM GSI_TABLA_RESUMEN WHERE (CODIGO IS NULL OR CODIGO = '0');
       --
       COMMIT;
       --
      
     EXCEPTION
     WHEN OTHERS THEN
          PV_ERROR:='ERROR - al cargar la tabla GSI_TABLA_RESUMEN '|| SUBSTR(SQLERRM,1,500);
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_RESU','Error Cargar la Tabla Resumen de Planes ',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --   
     END;
     CONT:=1;
     WHILE CONT <= LN_NUM_PROMO LOOP
     
      LV_QUERY_PR:=LV_QUERY_PR||'SELECT T.ID_PLAN,T.ID_PROMO'||TO_CHAR(CONT)||
          ' AS ID_PROMO,<TIPO_PROMO> AS TIPO_PROMO ' ||
          ' FROM GSI_TABLA_PROMO_INICIAL T <UNION>';
      
      IF CONT < 6 THEN
      LV_TIPO_PROMO:='T.TIPO_PROMO'||TO_CHAR(CONT);
      LV_QUERY_PR:=REPLACE(LV_QUERY_PR,'<TIPO_PROMO>',LV_TIPO_PROMO);
      ELSE
       LV_QUERY_PR:=REPLACE(LV_QUERY_PR,'<TIPO_PROMO>','NULL'); 
      END IF;
        
    --
    
    IF CONT = LN_NUM_PROMO THEN
     LV_QUERY_PR:=REPLACE(LV_QUERY_PR,'<UNION>','');
    ELSE 
     LV_QUERY_PR:=REPLACE(LV_QUERY_PR,'<UNION>','UNION ALL ');
    END IF; 
    --      
    CONT := CONT + 1;
    END LOOP; 
    
    BEGIN
       EXECUTE IMMEDIATE 'INSERT INTO GSI_TABLA_FINAL_PROMO (ID_PLAN,ID_PROMO,TIPO_PROMO) '|| LV_QUERY_PR;
       --
       DELETE FROM GSI_TABLA_FINAL_PROMO WHERE (ID_PROMO IS NULL OR ID_PROMO ='0');
       --
       COMMIT;
       -- ACTUALIZO EL CAMPO DE TIPO PROMO DE LA TABLA FINAl de PROMOCIONES
        FOR I IN TIPO_PROMO LOOP
        UPDATE GSI_TABLA_FINAL_PROMO
        SET    TIPO_PROMO = I.TIPO_PROMO
        WHERE  ROWID= I.ROWID;
        COMMIT;
        END LOOP;
        --
        FOR J IN FEATURE_SMS LOOP
        UPDATE GSI_TABLA_FINAL_PROMO 
        SET FEATURE_SMS = J.FEATURE_SMS
        WHERE ROWID=J.ROWID;
        COMMIT;
        END LOOP;
        --
        FOR K IN FEATURE_MEGAS LOOP
        UPDATE GSI_TABLA_FINAL_PROMO 
        SET FEATURE_MEGAS_NO_FIJO=K.FEATURE_MEGAS_NO_FIJO,CUPO_MEGAS_NO_FIJO=K.CUPO_MEGAS_NO_FIJO
        WHERE ROWID=K.ROWID;
        COMMIT;
        END LOOP;
       
       
     EXCEPTION
     WHEN OTHERS THEN
          PV_ERROR:='ERROR - al cargar la tabla GSI_TABLA_FINAL_PROMO '|| SUBSTR(SQLERRM,1,500);
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_RESU','Error Cargar la Tabla Resumen de Promociones ',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --   
     END;
     -- 
     INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_RESU','Fin Carga la Tabla Resumen',sysdate,'F',NULL,LV_USUARIO); 
     --
     COMMIT;                   
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_RESU','Error Carga a las Tablas Finales de las Matriz CONF.',sysdate,'E','ERROR - '|| LV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_RESU','Error Carga a las Tablas Finales de las Matriz CONF.',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
 END  PMQ_CARGA_TABLA_RESUMEN;  
 
 PROCEDURE PMQ_CARGA_ID_DETALLE_PLAN (PV_ERROR OUT VARCHAR2) IS
   --
   CURSOR DETALLE_PLAN IS 
     SELECT A.ROWID , B.ID_DETALLE_PLAN FROM  GSI_TABLA_RESUMEN A, AXISFAC.GE_DETALLES_PLANES@AXIS B
     WHERE A.ID_PLAN = B.ID_PLAN;
   --
   CURSOR DETALLE_PROMO IS
    SELECT A.ROWID , B.ID_DETALLE_PLAN FROM  GSI_TABLA_FINAL_PROMO A, AXISFAC.GE_DETALLES_PLANES@AXIS B
     WHERE A.ID_PLAN = B.ID_PLAN;
   --
   LV_USUARIO              VARCHAR2(50);
   LE_ERROR                EXCEPTION;
   LV_ERROR                VARCHAR(1000);  
 BEGIN
   -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
   --
   INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
   VALUES ('MQ_DEPL','Inicio Se carga el id_detalle_plan a la Tabla Resumen - Tabla Final Promociones',sysdate,'F',NULL,LV_USUARIO);
   --
   COMMIT; 
   --
   FOR I IN DETALLE_PLAN LOOP
       UPDATE GSI_TABLA_RESUMEN
       SET   ID_DETALLE_PLAN = I.ID_DETALLE_PLAN
       WHERE  ROWID = I.ROWID;
      
    END LOOP;
    --
    FOR M IN DETALLE_PROMO LOOP
      UPDATE GSI_TABLA_FINAL_PROMO 
      SET ID_DETALLE_PLAN = M.ID_DETALLE_PLAN
      WHERE ROWID = M.ROWID;
    END LOOP;
    --
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES ('MQ_DEPL','Fin Se carga el id_detalle_plan a la Tabla Resumen - Tabla Final Promociones',sysdate,'F',NULL,LV_USUARIO);
    --
    COMMIT;
    --
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('COF_DEPL','Error en cargar el id_detalle_plan a la Tabla Resumen - Tabla Final Promociones',sysdate,'E','ERROR - '|| LV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_DEPL','Error en cargar el id_detalle_plan a la Tabla Resumen - Tabla Final Promociones',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --   
 END PMQ_CARGA_ID_DETALLE_PLAN;
 
 PROCEDURE PMQ_EJECUTA_SENTENCIAS (PV_FECHA IN VARCHAR2, PV_ERROR OUT VARCHAR2) IS
 --
 CURSOR EJECUTA_SENTENCIA IS
 SELECT T.COD_SEQ,
        T.DESCRIPCION,
        T.SENTENCIA_PROCESOS
   FROM GSI_SENTENCIAS_MATRICESQC T
   WHERE T.ESTADO='A'
   AND (T.COD_ACCION ='CONF_MATRIZ_PL' OR T.COD_ACCION='CONF_MATRIZ_PR')
   AND T.COD_SEQ NOT IN ('MQ_022','MQ_023')
  ORDER BY COD_SEQ ASC;

 --
 CURSOR C_PARAMETROS (CN_ID_PARAMETRO NUMBER,CV_PARAMETRO VARCHAR2) IS
 SELECT T.VALOR
   FROM GV_PARAMETROS T
  WHERE T.ID_TIPO_PARAMETRO = CN_ID_PARAMETRO
    AND SUBSTR(T.ID_PARAMETRO, 1, 6) = CV_PARAMETRO
    ORDER BY T.VALOR;
 --
  CURSOR VERIFICA_DATOS IS
  SELECT COUNT(*) FROM GSI_SENTENCIAS_MATRICESQC;
 --
 CURSOR DIF_MINIMA (CN_ID_PARAMETRO NUMBER,CV_PARAMETRO VARCHAR2) IS
 SELECT T.VALOR
 FROM GV_PARAMETROS T
 WHERE T.ID_TIPO_PARAMETRO= CN_ID_PARAMETRO
 AND T.ID_PARAMETRO= CV_PARAMETRO;  
 --
 LV_VALORES      VARCHAR2(4000);
 LV_TRANSACCION  VARCHAR2(4000);
 LV_ERROR        VARCHAR2(4000);
 LE_ERROR        EXCEPTION;
 LV_PARAMETROS   VARCHAR2(4000);
 LV_COD_SEQ      VARCHAR2(20);
 LV_SENTENCIA    LONG;
 LV_USUARIO      VARCHAR2(50);
 LN_DATOS        NUMBER;
 LV_COSTO        VARCHAR2(10);
 LV_FECHA        VARCHAR2(50);
 LV_MENSAJE      VARCHAR2(4000);
 BEGIN
    LV_FECHA:=PV_FECHA;
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
   
   --
   OPEN VERIFICA_DATOS;
    FETCH VERIFICA_DATOS INTO LN_DATOS;
    CLOSE VERIFICA_DATOS;
    IF LN_DATOS > 0 THEN  
    
   --
   FOR I IN EJECUTA_SENTENCIA LOOP
    --
    FOR M IN C_PARAMETROS (15212,I.COD_SEQ) 
      LOOP
         LV_VALORES:=LV_VALORES||M.VALOR||'|';
    END LOOP;
     LV_COD_SEQ    :=I.COD_SEQ;
     LV_TRANSACCION:=I.DESCRIPCION;
     LV_SENTENCIA  :=I.SENTENCIA_PROCESOS;
     LV_PARAMETROS :=LV_COD_SEQ||'|'||LV_USUARIO||'|'||LV_TRANSACCION||'|'||LV_VALORES;
      
      IF LV_COD_SEQ ='MQ_001' THEN
        -- SE INVOCA AL PROCESO QUE CARGA LA TABLA RESUMEN 
         GSI_CONF_MATRICES_QC_PLANES.PMQ_CARGA_TABLA_RESUMEN (PV_ERROR => LV_ERROR);
     
           IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
            END IF;
         -- SE INVOCA AL PROCESO QUE CARGA EL ID_DETALLE_PLAN A LA TABLA RESUMEN
         GSI_CONF_MATRICES_QC_PLANES.PMQ_CARGA_ID_DETALLE_PLAN(PV_ERROR => LV_ERROR);
     
         IF LV_ERROR IS NOT NULL THEN
           RAISE LE_ERROR;
         END IF;      
      END IF;
      --
      INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
      VALUES (LV_COD_SEQ,'Inicio '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
      --
      COMMIT; 
      --
         IF  LV_COD_SEQ = 'MQ_013' THEN
           -- SE INVOCA AL PROCESO DE QC_PLANES
           -- INI PMERA Se bitacoriza la ejecucion del proceso QC.Planes en la tabla GSI 13/01/2016
           INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
           VALUES ('MQ_QCP','Inicio se ejecuta el proceso de QC. Planes',sysdate,'F',NULL,LV_USUARIO);
           --
           COMMIT;
           --
           GSI_PLANESQC.PLP_PRINCIPAL_QC(PV_FECHA_ACTUAL => LV_FECHA,PV_ERROR => LV_ERROR);
           
           IF LV_ERROR IS NOT NULL THEN
             --
             INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
             VALUES ('MQ_QCP','Error en la ejecucion del proceso QC. Planes',sysdate,'E',NULL,LV_USUARIO);
             --
             COMMIT; 
             --
           ELSE
             INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
             VALUES ('MQ_QCP','Fin se ejecuta el proceso de QC. Planes',sysdate,'F',NULL,LV_USUARIO);
             --
             COMMIT;
             --
           END IF;
           -- FIN PMERA Se bitacoriza la ejecucion del proceso QC.Planes en la tabla GSI 13/01/2016
         END IF;
         
         IF LV_COD_SEQ ='MQ_011' OR LV_COD_SEQ = 'MQ_012' THEN
           --
           OPEN DIF_MINIMA (15212,'CONFG_COSTO_MINIMO');
           FETCH DIF_MINIMA INTO LV_COSTO;
           CLOSE DIF_MINIMA;
           
           LV_PARAMETROS:=LV_PARAMETROS||LV_COSTO||'|'||LV_FECHA||'|';
             
         END IF;  
         --
         IF LV_PARAMETROS IS NULL THEN
           LV_ERROR:='No se obtuvieron los parametros de iniciales para ejecutar el proceso'||LV_COD_SEQ;
           RAISE LE_ERROR;
         END IF;  
         --
         IF LV_COD_SEQ='MQ_021' THEN 
           GSI_CONF_MATRICES_QC_PLANES.PMQ_PROMOCIONES_MEGAS(PL_CADENA => LV_PARAMETROS,
                                                             PV_ERROR =>  LV_ERROR);
         ELSE  
         -- 
         BEGIN
            EXECUTE IMMEDIATE LV_SENTENCIA
            USING LV_PARAMETROS;
        
            EXCEPTION 
             WHEN OTHERS THEN 
              LV_MENSAJE:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
              --
              INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
              VALUES (LV_COD_SEQ,'Error en la ejecucion del QC - '||LV_TRANSACCION ,sysdate,'E',LV_MENSAJE,LV_USUARIO);
              --
              COMMIT; 
              --   
          END;    
         END IF;
         --
         LV_PARAMETROS:='';
         LV_VALORES:='';
     
   END LOOP;
   ELSE
    LV_ERROR:='NO TENGO DATOS PARA EJECUTAR LAS SENTENCIAS';
    RAISE LE_ERROR;
   
   END IF;
   --
   INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
   VALUES ('EJE_SENT','Fin '||'Se ejecuta los procedimientos de Matrices QC',sysdate,'F',NULL,LV_USUARIO);
   --
   COMMIT;
   --
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('EJE_SENT','Error en ejecutar los procedimientos de Matrices QC',sysdate,'E','ERROR - '|| LV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('EJE_SENT','Error en ejecutar los procedimientos de Matrices QC',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --   
 END   PMQ_EJECUTA_SENTENCIAS;
 
 --Genera el archivo excel
  PROCEDURE PMQ_GENERERA_ARCHIVO (PV_ARCHIVO     OUT VARCHAR2,
                                  PV_ERROR       OUT VARCHAR2) IS
    --
    CURSOR OBSERVACION (LV_COD VARCHAR2) IS
    SELECT T.OBSERVACION
    FROM GSI_LOG_VALIDA_MATRICESQC T
    WHERE T.COD_SEQ = LV_COD AND t.TRANSACCION LIKE '%Fin%' AND T.FECHA_EVENTO =(SELECT MAX(S.FECHA_EVENTO)
    FROM GSI_LOG_VALIDA_MATRICESQC S
    WHERE S.COD_SEQ = LV_COD);
  
    TYPE CURSOR_TYPE_T   IS REF CURSOR;
    LC_REGISTROS         CURSOR_TYPE_t;
    LF_FILE              SYS.UTL_FILE.FILE_TYPE;
    LV_DIRECTORIO        VARCHAR2(50); 
    LV_NOMBRE_ARCHIVO    VARCHAR2(50);  
    LV_LINEA             CLOB;
    LV_ERROR             VARCHAR2(1000);
    LE_ERROR             EXCEPTION;
    LD_FECHA             DATE;
    LL_VAR_AUX_1         LONG;
    LN_POSPIPE           PLS_INTEGER := 1;
    LN_POSNEXT           PLS_INTEGER := 1;
    LV_CADENA            LONG;
    LV_QUERY             LONG; 
    LL_VAR_AUX           LONG;
    LL_VALOR             LONG;
    LN_POSPIPE_REG       PLS_INTEGER := 1;
    LN_POSNEXT_REG       PLS_INTEGER := 1;
    LV_NOMBRE_PROGRAMA   VARCHAR2(100):='GSI_CONF_MATRICES_QC_PLANES.PMQ_GENERERA_ARCHIVO';
    LN_CONTADOR          NUMBER:=0;
    LV_QUERY_QC          VARCHAR2(4000);
    LV_OBSE_TARIFAS      VARCHAR2(4000);
    LV_OBSERVACION       VARCHAR2(4000);
    LV_USUARIO           VARCHAR2(50);
    LL_QUERY_SENT        LONG;
    LN_NUM_SENT          NUMBER:=1;
    LN_TOPE              NUMBER:=2; 
    LV_ACCION            VARCHAR2(50);
    TYPE CURSOR_TYPE     IS REF CURSOR;
    CV_CURSOR_SENTENCIA  CURSOR_TYPE;
    LV_CURSOR_SENT       GSI_SENTENCIAS_MATRICESQC%ROWTYPE;

    
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    --
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES ('MQ_ARCH','Inicio Generacion del Archivo de los Matrices QC',sysdate,'F',PV_ERROR,LV_USUARIO);
    --
    COMMIT;
    --
    LV_DIRECTORIO:=FMQ_OBTENGO_PARAMETROS('CONFG_DIRECTORIO');
    
    IF LV_DIRECTORIO IS NULL THEN
      LV_ERROR:='No se obtuvo el directorio';
      RAISE LE_ERROR;
    END IF;
    --
     LV_NOMBRE_ARCHIVO:=FMQ_OBTENGO_PARAMETROS('CONFG_NOMBRE_ARCHIVO');
    
    IF LV_NOMBRE_ARCHIVO IS NULL THEN
      LV_ERROR:='No se obtuvo el nombre del archivo';
      RAISE LE_ERROR;
    END IF;
    --
     LD_FECHA:=TO_DATE(SYSDATE,'DD/MM/RRRR');    
    --
    LV_NOMBRE_ARCHIVO:= REPLACE(LV_NOMBRE_ARCHIVO,'<fecha>',TO_CHAR(LD_FECHA,'DDMMYYYY'));
    --
     LF_FILE := SYS.UTL_FILE.FOPEN(LV_DIRECTORIO, LV_NOMBRE_ARCHIVO, 'W');

     LV_LINEA := '<html><head><title>Automaticacion_de_planes_Qc</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
     
     LV_LINEA := '<style>
                .sombra      {background-color:#CCCCCC;}
                .cabecera    {font-weight:bold}
                .observacion2 {background-color:#004080; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .observacion {background-color:#FFFF33; font-weight:bold; text-align:center; vertical-align:center;}
                .celdavacia  {background-color:#EAEAEA;}
                .letra       {background-color:#00CCFF; color:#FFFFFF; font-weight:bold;font-size:13.0pt; text-align:center; vertical-align:center;}
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .dettitulo2  {background-color:#000000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .col1_defi   {color:black; font-family:"Tahoma"; font-size:8.0pt; mso-font-charset:0; text-align:center; vertical-align:middle; white-space:normal;}
               ';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

     LV_LINEA := '<BODY>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
  --DETALLES DEL REPORTE GENERADO (FECHA)
     LV_LINEA := '<TABLE>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

          LV_LINEA := '<TR></TR>
           <TR>
           <TD></TD>
           <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7> AUTOMATIZACION DE VALIDACION DE MATRICES COM VS LAS PLATAFORMAS </TD>
           </TR>
           <TR>
           <TD></TD><TD ALIGN="RIGTH" COLSPAN=7><B> Fecha de Generaci&oacute;n:  '||to_char(sysdate, 'dd/mm/yyyy')||'</TD>
           </TR>
           <TR></TR>
           <TR><TD></TD>
           <TD ALIGN="CENTER" CLASS="LETRA" COLSPAN=7><B>  QC CONFIGURACIONES EN PLATAFORMAS (AXIS, BSCS, TN3) VS MATRIZ ENVIADA POR COM </TD>
           </TR>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
    
     
    WHILE LN_NUM_SENT <= LN_TOPE  
    LOOP 
      --
      LL_QUERY_SENT:='SELECT *
                     FROM GSI_SENTENCIAS_MATRICESQC T
                     WHERE T.ESTADO = ''A'' 
                     AND T.COD_ACCION =''<COD_ACCION>'''||
                     ' ORDER BY COD_SEQ  ASC';
      --
      IF LN_NUM_SENT =1 THEN
         LV_ACCION:='CONF_MATRIZ_PL';
         LL_QUERY_SENT:=REPLACE (LL_QUERY_SENT,'<COD_ACCION>',LV_ACCION);      
      END IF;
    
     IF LN_NUM_SENT = 2 THEN 
        LV_ACCION:='CONF_MATRIZ_PR';
        LL_QUERY_SENT:=REPLACE (LL_QUERY_SENT,'<COD_ACCION>',LV_ACCION);
        LV_LINEA := '<TABLE><TR></TR><TR><TD></TD>
                    <TD ALIGN="CENTER" CLASS="LETRA" COLSPAN=7><B> QC CONFIGURACION DE PROMOCIONES VS MATRIZ ENVIADA POR COM </TD>
                    </TR></TABLE>';
        SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
     END IF;
     
     --CABECERA DEL REPORTE (TITULOS DE LOS CAMPOS DEL REPORTE)
     OPEN CV_CURSOR_SENTENCIA FOR LL_QUERY_SENT;
     
     LOOP
     FETCH CV_CURSOR_SENTENCIA INTO LV_CURSOR_SENT;
     EXIT WHEN CV_CURSOR_SENTENCIA%NOTFOUND;

       LN_CONTADOR:=0;
        
         --
       --TITULO DEL PASO DEL REPORTE
            LV_LINEA := '<TABLE><TR></TR><TR></TR>
             <TR>
             <TD></TD>
             <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7>'||REPLACE(REPLACE(REPLACE(REPLACE(LV_CURSOR_SENT.DESCRIPCION, '�', '&Ntilde;'), '�', '&ntilde;'), '�', '&oacute;'), '�', '&Oacute;')||'</td>
             </TR><TR></TR></TABLE>';
       SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
         --
         OPEN OBSERVACION (LV_CURSOR_SENT.COD_SEQ);
         FETCH OBSERVACION INTO LV_OBSE_TARIFAS;
         CLOSE OBSERVACION;
         
         IF LV_CURSOR_SENT.COD_SEQ='MQ_011' OR LV_CURSOR_SENT.COD_SEQ='MQ_012' THEN  
           LV_LINEA := '<TABLE>
              <TR>
              <TD></TD>
              <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7>' ||LV_OBSE_TARIFAS|| '</td>
              </TR><TR></TR></TABLE>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
        END IF;
         --
         LV_QUERY:=LV_CURSOR_SENT.SENTENCIA_SQL;
         OPEN LC_REGISTROS FOR LV_QUERY;
         --
         LOOP
            FETCH LC_REGISTROS
              INTO LL_VALOR;
            IF   LC_REGISTROS%ROWCOUNT < 1 THEN
             BEGIN 
             LV_OBSERVACION:='';
             
             LV_QUERY_QC:='SELECT T.OBSERVACION
                           FROM GSI_LOG_VALIDA_MATRICESQC T
                           WHERE  T.COD_SEQ=''' ||LV_CURSOR_SENT.COD_SEQ ||''' AND T.ESTADO = ''E''
                           AND T.FECHA_EVENTO = (SELECT MAX(S.FECHA_EVENTO) 
                                                 FROM GSI_LOG_VALIDA_MATRICESQC S WHERE S.COD_SEQ = '''||LV_CURSOR_SENT.COD_SEQ||''')'; 
             EXECUTE IMMEDIATE (LV_QUERY_QC) INTO LV_OBSERVACION;
             EXCEPTION 
                  WHEN OTHERS THEN
                    NULL;
                END;
           --
           IF LV_OBSERVACION IS NULL THEN
              
              LV_OBSERVACION:=' No hay Casos ';
              
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                lv_linea := '<TR><td></td>
                          <td  class=''observacion2'' style="border-style: solid;" > OBSERVACI&Oacute;N </td>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA); 
                LV_LINEA := '</TR></TABLE>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                --
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA :='<TR><td></td>
                <td class=''celdavacia2'' align=''center'' style="border-style: solid;"> '||LV_OBSERVACION||' </td>
                </TR><TR></TR>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
               LV_LINEA := '</TABLE>';
               SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
              
            ELSE
             
                 LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                lv_linea := '<TR><td></td>
                          <td  class=''dettitulo'' style="border-style: solid;" > OBSERVACI&Oacute;N </td>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA); 
                LV_LINEA := '</TR></TABLE>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                --
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA :='<TR><td></td>
                <td class=''observacion'' align=''center'' style="border-style: solid;"> '||LV_OBSERVACION||' </td>
                </TR><TR></TR>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
               LV_LINEA := '</TABLE>';
               SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
           END IF;
               EXIT;
           ELSE
             LN_CONTADOR:=LN_CONTADOR+1;
             IF LN_CONTADOR = 1 THEN
              LV_CADENA:=LV_CURSOR_SENT.ETIQUETAS;
              LN_POSPIPE := INSTR(LV_CADENA, '|', 1, 1);
              LV_LINEA := '<TABLE ><TD></TD>';
              SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
              LL_VAR_AUX_1:='';
              IF LN_POSPIPE > 0 THEN
               WHILE LN_POSPIPE > 0 LOOP
               
                LL_VAR_AUX_1 := TRIM(SUBSTR(LV_CADENA, LN_POSNEXT, LN_POSPIPE - LN_POSNEXT));
                IF LL_VAR_AUX_1 IS NOT NULL THEN
                 LL_VAR_AUX_1 := SUBSTR(LV_CADENA, LN_POSNEXT, LN_POSPIPE - LN_POSNEXT);
                ELSE
                 LL_VAR_AUX_1 := 'NULL';
                END IF;
                    --  
                    LV_LINEA := '<td  class=''dettitulo'' style="border-style: solid;" >'||LL_VAR_AUX_1||'</td>';
                    SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);                           
                  LN_POSNEXT := LN_POSPIPE + 1;
                  LN_POSPIPE := INSTR(LV_CADENA, '|', LN_POSNEXT, 1);
                 END LOOP;
                END IF;    
                
                LV_LINEA := '</TABLE>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_CADENA:='';
                LN_POSNEXT:=1;
                LN_POSPIPE:=1;
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
              END IF;
              --
              LL_VAR_AUX:='';
              LV_LINEA :='<TR><TD></TD>';
              SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
             LN_POSPIPE_REG := INSTR(LL_VALOR, '|', 1, 1);
           
               IF LN_POSPIPE_REG > 0 THEN
                 WHILE LN_POSPIPE_REG > 0 LOOP
             
                 LL_VAR_AUX := TRIM(SUBSTR(LL_VALOR, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG));
                 IF LL_VAR_AUX IS NOT NULL THEN
                   LL_VAR_AUX := SUBSTR(LL_VALOR, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG);
                 ELSE
                   LL_VAR_AUX := 'NULL';
                 END IF;
                   --
                   LV_LINEA := '<td class=''celdavacia2'' align=''center'' style="border-style: solid;">  '||LL_VAR_AUX||' </td>';
                   SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                   --      
                   LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
                   LN_POSPIPE_REG := INSTR(LL_VALOR, '|', LN_POSNEXT_REG, 1);
                 END LOOP;
                END IF;  
                 LV_LINEA :='</TR>';
                 SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                 LL_VALOR:='';
                 LN_POSNEXT_REG:=1;
                LN_POSPIPE_REG:=1;
            EXIT WHEN LC_REGISTROS%NOTFOUND;
           END IF;
          END LOOP;
           IF  LC_REGISTROS%ISOPEN THEN
          CLOSE LC_REGISTROS;
          END IF;
          --
     END LOOP;
       CLOSE CV_CURSOR_SENTENCIA;

     LN_NUM_SENT:=LN_NUM_SENT+1;
     LV_QUERY_QC:='';       
    END LOOP;
   LV_LINEA := '</TABLE>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   LV_LINEA:='</BODY></HTML>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   
   UTL_FILE.FCLOSE(LF_FILE);
   PV_ARCHIVO := LV_NOMBRE_ARCHIVO; 
   --
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES ('MQ_ARCH','Fin Generacion del Archivo de los Matrices QC',sysdate,'F',PV_ERROR,LV_USUARIO);
  EXCEPTION
    	WHEN LE_ERROR THEN
			PV_ERROR := 'ERROR - '||LV_ERROR;
      --
      IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
      --
      INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
      VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E','ERROR - '||LV_ERROR,LV_USUARIO);
		  COMMIT;
     WHEN UTL_FILE.INVALID_PATH THEN 
      PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'RUTA NO VALIDA: '||LV_DIRECTORIO; 
      --
      IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
      --
      INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
      VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',LV_NOMBRE_PROGRAMA||'-->'||'RUTA NO VALIDA: '||'PLANES_QC_DIR',LV_USUARIO);
      COMMIT;
      --
     WHEN UTL_FILE.INVALID_FILEHANDLE THEN 
      PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'NOMBRE ARCHIVO NO VALIDO: '||LV_NOMBRE_ARCHIVO; 
      --
      IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
      --
      INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
      VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',LV_NOMBRE_PROGRAMA||'-->'||'NOMBRE ARCHIVO NO VALIDO: '||LV_NOMBRE_ARCHIVO,LV_USUARIO);
      COMMIT;
       
     WHEN UTL_FILE.INVALID_OPERATION THEN 
        PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'OPERACION NO VALIDA: '||LV_NOMBRE_ARCHIVO; 
        --
        IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
        --
        INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
        VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',LV_NOMBRE_PROGRAMA||'-->'||'OPERACION NO VALIDA: '||LV_NOMBRE_ARCHIVO,LV_USUARIO);
        COMMIT;
       WHEN UTL_FILE.WRITE_ERROR THEN 
         PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE ESCRITURA: '||LV_NOMBRE_ARCHIVO||' '||SQLERRM; 
         --
         IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
         --
         INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
         VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',PV_ERROR,LV_USUARIO);
         COMMIT;
      WHEN UTL_FILE.READ_ERROR THEN 
         PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE LECTURA DEL ARCHIVO: '||LV_NOMBRE_ARCHIVO; 
         --
         IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;
         --
         INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
         VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE LECTURA DEL ARCHIVO: '||LV_NOMBRE_ARCHIVO,LV_USUARIO);
         COMMIT;
 
    WHEN OTHERS THEN 
         PV_ERROR:=LV_NOMBRE_PROGRAMA||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500);                                
         --
         IF CV_CURSOR_SENTENCIA%ISOPEN THEN
          CLOSE CV_CURSOR_SENTENCIA;
         END IF;

         
         --
         INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
         VALUES ('MQ_ARCH','Error Generacion del Archivo de los Matrices QC',sysdate,'E',PV_ERROR,LV_USUARIO);
         COMMIT;
  
  END  PMQ_GENERERA_ARCHIVO;
  
   --Transfiere el archivo al servidor FINAN_27
  PROCEDURE PMQ_TRANSFIERE_ARCHIVO (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                    PN_ERROR           OUT NUMBER,
                                    PV_ERROR           OUT VARCHAR2)IS
  
    LV_IP                 VARCHAR2(100);
    LV_USUARIO_FTP        VARCHAR2(100);
    LV_RUTA_REMOTA        VARCHAR2(1000);
    LV_BORRAR_ARCH_LOCAL  VARCHAR2(3);
    LV_ERROR              VARCHAR2(1000);
    LN_ERROR              EXCEPTION;
    LV_DIRECTORIO         VARCHAR2(1000);
    LE_ERROR              EXCEPTION;
    LV_COMANDO            VARCHAR2(500);
    LV_SALIDA             VARCHAR2(500);
    LV_RUTA_LOCAL         VARCHAR2(100);
    LV_USUARIO            VARCHAR2(50);
   
   BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    --
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_TRANS','Inicio Transfiere Archivo al servidor de FINAN',sysdate,'F',NULL,LV_USUARIO);
     --
     COMMIT;
     ------------------------
      -- LEO LOS PARAMETROS
     ------------------------
        LV_IP:=FMQ_OBTENGO_PARAMETROS('CONFG_IP_SERVIDOR');

        IF LV_IP IS NULL THEN 
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO IP_SERVIDOR';
          RAISE LE_ERROR;
        END IF;
       --
        LV_USUARIO_FTP:=FMQ_OBTENGO_PARAMETROS('CONFG_USUARIO');

        IF LV_USUARIO_FTP IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO USUARIO DEL SERVIDOR';
          RAISE LE_ERROR;
        END IF; 
       --
        LV_RUTA_REMOTA:=FMQ_OBTENGO_PARAMETROS('CONFG_RUTA_REMOTA');
          
        IF LV_RUTA_REMOTA IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE RUTA DEL REMOTA';
          RAISE LE_ERROR;
        END IF;
       --
        LV_DIRECTORIO:=FMQ_OBTENGO_PARAMETROS('CONFG_DIRECTORIO');

        IF LV_DIRECTORIO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL NOMBRE DEL DIRECTORIO';
          RAISE LE_ERROR;
        END IF;
       --
        LV_BORRAR_ARCH_LOCAL:=FMQ_OBTENGO_PARAMETROS('CONFG_BORRA_ARCHIVO');
          
        IF LV_BORRAR_ARCH_LOCAL IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE BORRAR O NO BORRAR EL ARCHIVO';
          RAISE LE_ERROR;
        END IF;
      --
       LV_RUTA_LOCAL:=FMQ_OBTENGO_PARAMETROS('CONFG_RUTA_LOCAL');
          
        IF LV_RUTA_LOCAL IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA RUTA LOCAL';
          RAISE LE_ERROR;
        END IF;  
      --
      --LV_COMANDO := 'chmod 777 '||LV_RUTA_LOCAL||PV_NOMBRE_ARCHIVO;
    
      --Osutil.Runoscmd(Lv_Comando, Lv_Salida);
      --
      COMMIT;--POR PRUEBAS
      --  
      scp_dat.sck_ftp_gtw.scp_transfiere_archivo(PV_IP =>                 LV_IP,
                                                 PV_USUARIO =>            LV_USUARIO_FTP,
                                                 PV_RUTA_REMOTA =>        LV_RUTA_REMOTA,
                                                 PV_NOMBRE_ARCH =>        PV_NOMBRE_ARCHIVO,
                                                 PV_RUTA_LOCAL =>         LV_DIRECTORIO,
                                                 PV_BORRAR_ARCH_LOCAL =>  LV_BORRAR_ARCH_LOCAL,
                                                 PN_ERROR =>              PN_ERROR,
                                                 PV_ERROR =>              LV_ERROR);
    
    
     IF LV_ERROR IS NOT NULL OR PN_ERROR != 0 THEN
        RAISE LE_ERROR;
     END IF;
     --
     INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_TRANS','Fin Transfiere Archivo al servidor de FINAN',sysdate,'F',NULL,LV_USUARIO);
     --
     COMMIT;
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_TRANS','Error Transfiere Archivo al servidor de FINAN',sysdate,'E','ERROR - '||LV_ERROR,LV_USUARIO);
          COMMIT;
          --
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_TRANS','Error Transfiere Archivo al servidor de FINAN',sysdate,'E',PV_ERROR,LV_USUARIO);
          COMMIT;
          
  END PMQ_TRANSFIERE_ARCHIVO;
  --ENVIO DE CORREO CON ARCHIVO ADJUNTO
  PROCEDURE PMQ_ENVIA_REPORTE_MAIL (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2) IS
    --
    LV_RUTA              VARCHAR2(1000);
    LV_ASUNTO            VARCHAR2(100);
    LV_MENSAJE           VARCHAR2(1000);
    LV_DESTINATARIO      VARCHAR2(1000);
    LV_REMITENTE         VARCHAR2(1000);
    LV_TIPO_REGISTRO     VARCHAR2(1000);
    LV_CLASE             VARCHAR2(1000);
    LV_PUERTO            VARCHAR2(1000);
    LV_SERVIDOR          VARCHAR2(1000);
    LV_MAXINTENTOS       VARCHAR2(1000);
    LV_PROGRAM           VARCHAR2(100):='GSI_CONF_MATRICES_QC_PLANES.PMQ_ENVIA_REPORTE_MAIL';
    LV_ERROR             VARCHAR2(1000);
    LE_ERROR             EXCEPTION;
    LN_ID_SECUENCIA      NUMBER;
    LV_SALTO             VARCHAR2(5);
    LN_RESULT            NUMBER;
    LV_USUARIO           VARCHAR2(50);
   
    
 
   BEGIN 
     --
     LV_SALTO :=CHR(13);                                   
     -- USUARIO
     SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
     FROM DUAL ;
     
     INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_MAIL','Inicio Envio de Mail Adjunto',sysdate,'F',NULL,LV_USUARIO);
     --
     COMMIT;
     ------------------------
        -- LEO LOS PARAMETROS
     ------------------------
       --
        LV_RUTA:=FMQ_OBTENGO_PARAMETROS('CONFG_RUTA_REMOTA');

        IF LV_RUTA IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER LA RUTA DEL DIRECTORIO DE FINAN';
          RAISE LE_ERROR;
        END IF;
       --
         LV_ASUNTO:=FMQ_OBTENGO_PARAMETROS('CONFG_ASUNTO_CORREO');

        IF LV_ASUNTO IS NULL THEN 
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE ASUNTO';
          RAISE LE_ERROR;
        END IF;
           
       --
        LV_MENSAJE:=FMQ_OBTENGO_PARAMETROS('CONFG_MENSAJE');

        IF LV_MENSAJE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE MENSAJE';
          RAISE LE_ERROR;
        END IF; 
       --
        LV_DESTINATARIO:=FMQ_OBTENGO_PARAMETROS('CONFG_DEST_CORREO');
          
        IF LV_DESTINATARIO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE DESTINATARIO';
          RAISE LE_ERROR;
        END IF;
          
       --
        LV_REMITENTE:=FMQ_OBTENGO_PARAMETROS('CONFG_REMT_CORREO');

        IF LV_REMITENTE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE REMITENTE';
          RAISE LE_ERROR;
        END IF;
       --
        LV_TIPO_REGISTRO:=FMQ_OBTENGO_PARAMETROS('CONFG_TIPO_ADJUNTO');
          
        IF LV_TIPO_REGISTRO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE TIPO DE REGISTRO DE MAIL ADJUNTO';
          RAISE LE_ERROR;
        END IF;
      --
        LV_CLASE:=FMQ_OBTENGO_PARAMETROS('CONFG_CLASE_MAIL');
          
        IF LV_CLASE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA CLASE';
          RAISE LE_ERROR;
        END IF;
      --
        LV_MAXINTENTOS:=FMQ_OBTENGO_PARAMETROS('CONFG_MAXIMO_INTENT');
          
        IF LV_MAXINTENTOS IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE MAXIMO DE INTENTOS';
          RAISE LE_ERROR;
        END IF;
      --
       LV_MENSAJE:=REPLACE(LV_MENSAJE,'<lv_usuario>',LV_USUARIO);
       LV_MENSAJE:=REPLACE(LV_MENSAJE,'<fecha>',TO_CHAR(SYSDATE,'DD/MM/YYYY'));
       LV_MENSAJE:=LV_MENSAJE||LV_SALTO||LV_SALTO||'Saludos cordiales,'||LV_SALTO||LV_SALTO||
                   '------------------------'||LV_SALTO||
                   '::Este mensaje e-mail fue enviado automaticamente::';
       
     
      --POR PRUEBA EN DESARROLLO
      LV_SERVIDOR:=NULL;
      LV_PUERTO:=NULL;
      --          
        LN_RESULT:= scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => LV_PROGRAM,
                                                                        pd_fechaenvio      => sysdate,
                                                                        pd_fechaexpiracion => sysdate+1,
                                                                        pv_asunto          => LV_ASUNTO,
                                                                        pv_mensaje         => LV_MENSAJE||'</ARCHIVO1='||PV_NOMBRE_ARCHIVO||'|DIRECTORIO1='||LV_RUTA||'|/>',
                                                                        pv_destinatario    => LV_DESTINATARIO,
                                                                        pv_remitente       => LV_REMITENTE,
                                                                        pv_tiporegistro    => LV_TIPO_REGISTRO,
                                                                        pv_clase           => LV_CLASE,
                                                                        pv_puerto          => LV_PUERTO,   
                                                                        pv_servidor        => LV_SERVIDOR, 
                                                                        pv_maxintentos     => TO_NUMBER(LV_MAXINTENTOS),
                                                                        pv_idusuario       => USER,
                                                                        pv_mensajeretorno  => LV_ERROR,
                                                                        pn_idsecuencia     => LN_ID_SECUENCIA);                            
       
      
      
           -- se maneja el error del envio del correo
                IF LV_ERROR IS NOT NULL OR LN_RESULT != 0 THEN
                 RAISE LE_ERROR;
                END IF;
           --
           INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
           VALUES ('MQ_MAIL','Fin Envio de Mail Adjunto',sysdate,'F',NULL,LV_USUARIO);
           COMMIT;
   
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:=LV_PROGRAM||'-->'||'ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_MAIL','Error Envio de Mail Adjunto',sysdate,'E','ERROR - '||LV_PROGRAM||'-->'||LV_ERROR,LV_USUARIO);
          COMMIT;
        
          WHEN OTHERS THEN 
          PV_ERROR:=LV_PROGRAM||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500);                                
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_MAIL','Error Envio de Mail Adjunto',sysdate,'E',PV_ERROR||LV_ERROR,LV_USUARIO);
          COMMIT;
          
  END PMQ_ENVIA_REPORTE_MAIL;
  -- EJECUTA TODO LOS PROCESOS PARA LA CONFIGURACION DE MATRICES
  PROCEDURE PMQ_EJECUTA_PROCESO_MATRIZ (PV_FECHA IN VARCHAR2,
                                        PV_ERROR OUT VARCHAR2) IS
                                        
  
  LV_ERROR            VARCHAR2(4000);
  LE_ERROR            EXCEPTION;
  LV_USUARIO          VARCHAR2(50);
  LV_NOM_ARCHIVO      VARCHAR2(1000);
  LN_ERROR            NUMBER;
  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    --
     INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_PRIN','Inicio Ejecucion del proceso de configuracion de Matrices - QC. Planes',sysdate,'F','',LV_USUARIO);
    
    BEGIN
    -- SE INSERTA LOS REGISTRO A LA TABLA HISTORICA
      INSERT INTO GSI_BITACORA_MATRICESQC_HIST 
      SELECT * FROM GSI_BITACORA_MATRICESQC;
      -- SE ELIMINA LOS REGISTRO DE LAS TABLAS RESUMEN 
      DELETE FROM GSI_TABLA_RESUMEN;
      DELETE FROM GSI_TABLA_FINAL_PROMO;
      -- SE ELIMINA LOS REGISTRO DE LA TABLA BITACORA
      DELETE FROM GSI_BITACORA_MATRICESQC;
      --
       COMMIT;
      --
     EXCEPTION
      WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_PRIN','Error en limpiar las tablas de trabajo',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
     END;   
     
      -- SE INVOCA AL PROCESO QUE ELIMINA LOS ESPACIOS EN BLANCO DE LA TABLA INCIAL DE TRABAJO
      GSI_CONF_MATRICES_QC_PLANES.PMQ_QUITA_ESPACIOS_TABLA(PV_ERROR => LV_ERROR);
   
      IF LV_ERROR IS NOT NULL THEN
        
        RAISE LE_ERROR;
      END IF;
     
     -- SE INVOCA AL PROCESO QUE EJECUTA TODOS LOS BLOQUES Q CONTIENE
     GSI_CONF_MATRICES_QC_PLANES.PMQ_EJECUTA_SENTENCIAS(PV_FECHA => PV_FECHA,
                                                        PV_ERROR => LV_ERROR);
     
     IF LV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF; 
     -- SE INVOCA AL PROCESO QUE GENERA EL ARCHIVO CON EXTENSION .XLS
     GSI_CONF_MATRICES_QC_PLANES.PMQ_GENERERA_ARCHIVO(PV_ARCHIVO => LV_NOM_ARCHIVO,
                                                      PV_ERROR   =>  LV_ERROR);
     IF LV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF; 
     -- SE INVOCA AL PROCESO QUE TRANSFIERE EL ARCHIVO AL SERVIDOR DE FINAN
     GSI_CONF_MATRICES_QC_PLANES.PMQ_TRANSFIERE_ARCHIVO(PV_NOMBRE_ARCHIVO => LV_NOM_ARCHIVO, 
                                                        PN_ERROR          => LN_ERROR, 
                                                        PV_ERROR          => LV_ERROR);
                                                        
      IF LV_ERROR IS NOT NULL OR LN_ERROR != 0 THEN
        RAISE LE_ERROR;
     END IF;
     -- SE INVOCA AL PROCESO QUE ENVIA EL CORREO ADJUNTO
     GSI_CONF_MATRICES_QC_PLANES.PMQ_ENVIA_REPORTE_MAIL (PV_NOMBRE_ARCHIVO => LV_NOM_ARCHIVO, 
                                                         PV_ERROR => LV_ERROR);
      
       IF LV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF;                                                    
  
     INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
     VALUES ('MQ_PRIN','Fin Ejecucion del proceso de configuracion de Matrices - QC. Planes',sysdate,'F','',LV_USUARIO);
     --
     COMMIT;
     --
  EXCEPTION
   
    
   WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_PRIN','Error Ejecucion del proceso de configuracion de Matrices - QC. Planes',sysdate,'E','ERROR - '|| LV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          --
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES ('MQ_PRIN','Error Ejecucion del proceso de configuracion de Matrices - QC. Planes',sysdate,'E',PV_ERROR,LV_USUARIO);
          --
          COMMIT; 
          --  
  
        
  END  PMQ_EJECUTA_PROCESO_MATRIZ;    
  --
  PROCEDURE PMQ_FEATURE_FIJO_PLAN (PN_ID_PROMOCION IN NUMBER,
                                   PV_COD_SEQ      IN VARCHAR2,
                                   PV_TRANSACCION  IN VARCHAR2,
                                   PV_MENSAJE      IN VARCHAR2,
                                   PV_USUARIO      IN VARCHAR2,
                                   PV_ERROR        OUT VARCHAR2) IS
    
   --
   CURSOR FEATURE_FIJO (CN_ID_PROMOCION IN NUMBER)IS
   SELECT B.ID_PLAN,A.ID_PROMOCION, A.ID_PARAMETRO AS ID_DETALLE_PLAN, SUBSTR(A.VALOR,5) AS FEATURE_MEGAS_CONF_FIJO, B.CODIGO AS FEATURE_MEGAS_FIJO_COM
        FROM PORTA.MK_PARAM_GEN_AUT@AXIS A, GSI_TABLA_RESUMEN B
   WHERE  A.ID_PARAMETRO = B.ID_DETALLE_PLAN
   AND    A.ID_PARAMETRO2='FEATURE'
   AND    B.TIPO = 'FIJO'
   AND    B.MARCA_FEATURE_MB = 'MB'
   AND    SUBSTR(A.VALOR,5) <> B.CODIGO
   AND    A.ID_PROMOCION  = CN_ID_PROMOCION
   ORDER BY B.ID_PLAN;
  --
  
  BEGIN
   
   FOR I IN FEATURE_FIJO (PN_ID_PROMOCION)
   LOOP 
      INSERT INTO GSI_BITACORA_MATRICESQC T (T.COD_SEQ, 
                                             T.ID_PLAN,
                                             T.ID_PROMO,
                                             T.ID_DETALLE_PLAN,
                                             T.CANTIDAD,
                                             T.VALOR_OMISION,
                                             T.MENSAJE_TECNICO,
                                             T.USUARIO, 
                                             T.ESTADO, 
                                             T.FECHA_EVENTO)
                                      VALUES (PV_COD_SEQ,
                                              I.ID_PLAN,
                                              I.ID_PROMOCION,
                                              I.ID_DETALLE_PLAN,
                                              I.FEATURE_MEGAS_CONF_FIJO,
                                              I.FEATURE_MEGAS_FIJO_COM,
                                              PV_MENSAJE,
                                              PV_USUARIO,
                                              'A',
                                              SYSDATE);
    END LOOP;
    --
   
    COMMIT;
    --
   EXCEPTION
  
   WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:=SUBSTR(SQLERRM,1,500);
    --
    INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
    VALUES (PV_COD_SEQ,'Error '||PV_TRANSACCION,sysdate,'E','ERROR - '|| PV_ERROR,PV_USUARIO);
    --
    COMMIT;
  END PMQ_FEATURE_FIJO_PLAN;
  --
  PROCEDURE PMQ_PROMOCIONES_MEGAS (PL_CADENA IN LONG, PV_ERROR OUT VARCHAR2) IS
   
   --
   CURSOR PROMO_BANDA_ANCHA IS 
   SELECT  T.ID_PROMO FROM GSI_TABLA_FINAL_PROMO T
   WHERE TIPO_PROMO = 'PROMO_BANDA_ANCHA';
   --
   CURSOR  CONFG_MEGAS (CN_PROMOCION NUMBER)IS
   SELECT VALOR, COUNT(*) FROM PORTA.MK_PARAM_PROMO_AUT@AXIS
   WHERE ID_PROMOCION = CN_PROMOCION
   AND ID_PARAMETRO = 'USA_FEATURE_FIJO'
   GROUP BY VALOR;
   --
   CURSOR FEATURE_PROMOCIONAL (CN_ID_PROMOCION IN NUMBER) IS
    SELECT B.ID_PLAN,A.ID_PROMOCION, A.ID_PARAMETRO AS ID_DETALLE_PLAN, SUBSTR(A.VALOR, 5) AS FEATURE_MEGAS_CONF_NO_FIJO,
           B.FEATURE_MEGAS_NO_FIJO AS FEATURE_MEGAS_NO_FIJO_COM
    FROM  PORTA.MK_PARAM_GEN_AUT@AXIS A,
          (SELECT TO_CHAR(A.ID_DETALLE_PLAN) AS ID_DETALLE_PLAN, FEATURE_MEGAS_NO_FIJO,A.ID_PLAN
           FROM   GSI_TABLA_FINAL_PROMO A
           WHERE   A.TIPO_PROMO = 'PROMO_BANDA_ANCHA') B
    WHERE A.ID_PARAMETRO = B.ID_DETALLE_PLAN
    AND   A.ID_PARAMETRO2='FEATURE'
    AND  SUBSTR(A.VALOR, 5) <> B.FEATURE_MEGAS_NO_FIJO
    AND    A.ID_PROMOCION  = CN_ID_PROMOCION;
    --
    CURSOR CUPO_PROMOCIONAL (CN_ID_PROMOCION IN NUMBER )IS
    SELECT B.ID_PLAN,A.ID_PROMOCION, A.ID_PARAMETRO AS ID_DETALLE_PLAN, (A.VALOR/1024/1024) AS CUPO_MEGAS_CONF_NO_FIJO,
                B.CUPO_MEGAS_NO_FIJO AS CUPO_MEGAS_NO_FIJO_COM
    FROM  PORTA.MK_PARAM_GEN_AUT@AXIS A,
          (SELECT TO_CHAR(A.ID_DETALLE_PLAN) AS ID_DETALLE_PLAN, A.CUPO_MEGAS_NO_FIJO, (A.CUPO_MEGAS_NO_FIJO*1024*1024) AS CUPO_KB_FEATURE_PROMO,A.ID_PLAN
          FROM   GSI_TABLA_FINAL_PROMO A
          WHERE   A.TIPO_PROMO = 'PROMO_BANDA_ANCHA') B
    WHERE A.ID_PARAMETRO = B.ID_DETALLE_PLAN
    AND   A.ID_PARAMETRO2='KILOBYTES'
    AND   A.VALOR <> B.CUPO_KB_FEATURE_PROMO
    AND   A.ID_PROMOCION  = CN_ID_PROMOCION;          
   --
   CURSOR TRANSACCION (CV_CODSEQ IN VARCHAR2) IS
   SELECT T.COD_SEQ FROM GSI_SENTENCIAS_MATRICESQC T
   WHERE T.COD_SEQ=CV_CODSEQ;
   --VARIABLES---
   LV_ERROR                VARCHAR(1000); 
   LN_POSPIPE_REG          PLS_INTEGER := 1;
   LN_POSNEXT_REG          PLS_INTEGER := 1;
   LV_COD_SEQ              VARCHAR2(20);
   LV_USUARIO              VARCHAR2(50);
   LV_TRANSACCION          VARCHAR2(1000);     
   LV_MENSAJE_ERROR        VARCHAR2(1000);   
   LV_CADENA               VARCHAR2(4000);
   LB_CONF_MEGAS           BOOLEAN;
   LC_CONF_MEGAS           CONFG_MEGAS%ROWTYPE;
   CONT                    NUMBER;
   BEGIN
     LV_CADENA:=PL_CADENA;
   --
    LN_POSPIPE_REG := INSTR(LV_CADENA, '|', 1, 1);
      IF LN_POSPIPE_REG > 0 THEN
      
      WHILE LN_POSPIPE_REG > 0  LOOP
        --
        LV_COD_SEQ :=TRIM(SUBSTR(LV_CADENA, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG));  
        LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
        LN_POSPIPE_REG := INSTR(LV_CADENA, '|', LN_POSNEXT_REG, 1);
        --
        LV_USUARIO :=TRIM(SUBSTR(LV_CADENA, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG));  
        LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
        LN_POSPIPE_REG := INSTR(LV_CADENA, '|', LN_POSNEXT_REG, 1);
        --
        LV_TRANSACCION :=TRIM(SUBSTR(LV_CADENA, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG));  
        LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
        LN_POSPIPE_REG := INSTR(LV_CADENA, '|', LN_POSNEXT_REG, 1);
        --
        LV_MENSAJE_ERROR :=TRIM(SUBSTR(LV_CADENA, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG)); 
        LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
        LN_POSPIPE_REG := INSTR(LV_CADENA, '|', LN_POSNEXT_REG, 1);
        --
        IF LV_MENSAJE_ERROR IS NULL THEN
          LV_CADENA:=NULL;
          LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
          LN_POSPIPE_REG := INSTR(LV_CADENA, '|', LN_POSNEXT_REG, 1);
        END IF;  
        --
      END LOOP;
     END IF;
     -- Tomar todas las promociones de Banda Ancha y buscar su configuracion en la tabla: MK_PARAM_PROMO_AUT
     CONT:=1; 
     FOR I IN  PROMO_BANDA_ANCHA
     LOOP  
        
      BEGIN 
         -- Verificar en la tabla de Promociones, el tipo de Configuracion para la promoci�n de Megas 
         OPEN CONFG_MEGAS (I.ID_PROMO);
         FETCH CONFG_MEGAS INTO LC_CONF_MEGAS;
         LB_CONF_MEGAS:=CONFG_MEGAS%NOTFOUND;
         CLOSE CONFG_MEGAS;
         
         IF LB_CONF_MEGAS THEN
           -- Se invoca al proceso donde Verifica que la Promoci�n de Megas, tenga asociado correctamente el Feature Fijo del Plan
           PMQ_FEATURE_FIJO_PLAN(PN_ID_PROMOCION => I.ID_PROMO,
                                 PV_COD_SEQ => LV_COD_SEQ, 
                                 PV_TRANSACCION => LV_TRANSACCION,
                                 PV_MENSAJE => LV_MENSAJE_ERROR,
                                 PV_USUARIO => LV_USUARIO,
                                 PV_ERROR   => LV_ERROR);
              IF LV_ERROR IS NOT NULL THEN
              PV_ERROR:=LV_ERROR;
              EXIT;
             END IF;                    
         END IF;
         --
         IF LC_CONF_MEGAS.VALOR = 'S' THEN
           -- Se invoca al proceso donde Verifica que la Promoci�n de Megas, tenga asociado correctamente el Feature Fijo del Plan
           PMQ_FEATURE_FIJO_PLAN(PN_ID_PROMOCION => I.ID_PROMO,
                                 PV_COD_SEQ => LV_COD_SEQ, 
                                 PV_TRANSACCION => LV_TRANSACCION,
                                 PV_MENSAJE => LV_MENSAJE_ERROR,
                                 PV_USUARIO => LV_USUARIO,
                                 PV_ERROR   => LV_ERROR);
            IF LV_ERROR IS NOT NULL THEN
              PV_ERROR:=LV_ERROR;
              EXIT;
           END IF;                           
         END IF;  
         IF CONT = 1 THEN
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES (LV_COD_SEQ,'Fin '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
         END IF;
         
         IF LC_CONF_MEGAS.VALOR = 'N' THEN
           BEGIN
            IF CONT = 1 THEN
            -- Verifica que la Promoci�n de Megas, tenga asociado correctamente el Feature Promocional indicado por COM
            LV_COD_SEQ:='MQ_022';
            OPEN TRANSACCION (LV_COD_SEQ);
            FETCH TRANSACCION INTO LV_TRANSACCION;
            CLOSE TRANSACCION;
            LV_MENSAJE_ERROR:=FMQ_OBTENGO_PARAMETROS('MQ_022_FEATURE_PROMOCIONAL');
            --
            INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
            VALUES (LV_COD_SEQ,'Inicio '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
            END IF;
            --
             FOR M IN FEATURE_PROMOCIONAL (I.ID_PROMO)
             LOOP 
                 INSERT INTO GSI_BITACORA_MATRICESQC T (T.COD_SEQ, 
                                                        T.ID_PLAN,
                                                        T.ID_PROMO,
                                                        T.ID_DETALLE_PLAN,
                                                        T.CANTIDAD,
                                                        T.FEATURE_MEGAS_NO_FIJO,
                                                        T.MENSAJE_TECNICO,
                                                        T.USUARIO, 
                                                        T.ESTADO, 
                                                        T.FECHA_EVENTO)
                                                VALUES (LV_COD_SEQ,
                                                        M.ID_PLAN,
                                                        M.ID_PROMOCION,
                                                        M.ID_DETALLE_PLAN,
                                                        M.FEATURE_MEGAS_CONF_NO_FIJO,
                                                        M.FEATURE_MEGAS_NO_FIJO_COM,
                                                        LV_MENSAJE_ERROR,
                                                        LV_USUARIO,
                                                        'A',
                                                        SYSDATE);
             END LOOP;
             IF CONT = 1 THEN
             --
             INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
             VALUES (LV_COD_SEQ,'Fin '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
             --
             COMMIT;
             -- Verifica que la Promoci�n de Megas, tenga asociado correctamente el CUPO Promocional indicado por COM 
             LV_COD_SEQ:='MQ_023';
             OPEN TRANSACCION (LV_COD_SEQ);
             FETCH TRANSACCION INTO LV_TRANSACCION;
             CLOSE TRANSACCION;
             LV_MENSAJE_ERROR:=FMQ_OBTENGO_PARAMETROS('MQ_023_CUPO_PROMOCIONAL');
             --
             INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
             VALUES (LV_COD_SEQ,'Inicio '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
             END IF;
             --
             FOR L IN CUPO_PROMOCIONAL (I.ID_PROMO)
             LOOP
                 INSERT INTO GSI_BITACORA_MATRICESQC T (T.COD_SEQ, 
                                                        T.ID_PLAN,
                                                        T.ID_PROMO,
                                                        T.ID_DETALLE_PLAN,
                                                        T.CANTIDAD,
                                                        T.CUPO_MEGAS_NO_FIJO,
                                                        T.MENSAJE_TECNICO,
                                                        T.USUARIO, 
                                                        T.ESTADO, 
                                                        T.FECHA_EVENTO)
                                                VALUES (LV_COD_SEQ,
                                                        L.ID_PLAN,
                                                        L.ID_PROMOCION,
                                                        L.ID_DETALLE_PLAN,
                                                        L.CUPO_MEGAS_CONF_NO_FIJO,
                                                        L.CUPO_MEGAS_NO_FIJO_COM,
                                                        LV_MENSAJE_ERROR,
                                                        LV_USUARIO,
                                                        'A',
                                                        SYSDATE);
             END LOOP;
              IF CONT = 1 THEN 
               INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
               VALUES (LV_COD_SEQ,'FIN '||LV_TRANSACCION,sysdate,'F',NULL,LV_USUARIO);
             END IF;
             EXCEPTION
                WHEN OTHERS THEN
                ROLLBACK;
                PV_ERROR:=SUBSTR(SQLERRM,1,500);
                --
                INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
                VALUES (LV_COD_SEQ,'Error '||LV_TRANSACCION,sysdate,'E','ERROR - '|| PV_ERROR,LV_USUARIO);
                --
                COMMIT;
              
           END;
           
         END IF;  
         CONT:=CONT+1;      
       EXCEPTION
        
         WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR:=SUBSTR(SQLERRM,1,500);
          --
          INSERT INTO GSI_LOG_VALIDA_MATRICESQC (COD_SEQ, TRANSACCION, FECHA_EVENTO, ESTADO, OBSERVACION, USUARIO)
          VALUES (LV_COD_SEQ,'Error '||LV_TRANSACCION,sysdate,'E','ERROR - '|| LV_ERROR,LV_USUARIO);
          --
          COMMIT;
              
             
      END;
     END LOOP;
     --
    EXCEPTION

   WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR:=SUBSTR(SQLERRM,1,500);
    
      
  END PMQ_PROMOCIONES_MEGAS;  
  -- INI PMERA - 08/11/2016 Se crea un proceso para ejecute los procesos de Qc Planes y Validaciones de Matrices mediante un JOB   
  PROCEDURE EJECUTA_PROCESOS_QC_JOBS (PV_NOM_PROCESO IN VARCHAR2,
                                      PV_FECHA       IN VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2) IS
                                      
  
  LV_SENTENCIA VARCHAR2(4000);
  LV_ERROR     VARCHAR2(4000);
  LE_ERROR     EXCEPTION;
  LV_PARAMETRO VARCHAR2(100);
  LV_JOB       VARCHAR2(4000);
  LV_FECHA     VARCHAR2(50);
 
     
  
  BEGIN
     LV_FECHA:=PV_FECHA;
      IF PV_NOM_PROCESO IS NULL THEN
         LV_ERROR := 'Debe ingresar el nombre del proceso que desea ejecutar';
         RAISE LE_ERROR;
      
      ELSE  
      --
      IF PV_NOM_PROCESO = 'GSI_PLANESQC.PLP_PRINCIPAL_QC' THEN
        
         LV_PARAMETRO:='PV_FECHA_ACTUAL';
      ELSE
         LV_PARAMETRO:='PV_FECHA'; 
           
      END IF;   
      IF LV_FECHA IS NULL THEN
        
        LV_FECHA:='LV_FECHA;';
      ELSE
        LV_FECHA := ''''||LV_FECHA||''';';
      END IF;  
      LV_SENTENCIA:='DECLARE 
                     LV_FECHA VARCHAR2(50);
                     LV_ERROR  VARCHAR2(4000);
                     BEGIN 
                       
                     LV_FECHA:='||LV_FECHA||' '||
                     PV_NOM_PROCESO||'(<PV_PARAMETRO>=>LV_FECHA,PV_ERROR=>LV_ERROR);
                     END;';
      LV_SENTENCIA:=REPLACE(LV_SENTENCIA,'<PV_PARAMETRO>',LV_PARAMETRO);

      SYS.DBMS_JOB.SUBMIT(JOB => LV_JOB,
      WHAT =>LV_SENTENCIA,

      NEXT_DATE => SYSDATE,
      INTERVAL => NULL);
      COMMIT;
      END IF;
       
       DBMS_OUTPUT.PUT_LINE(LV_JOB);
    EXCEPTION
        
         WHEN LE_ERROR THEN
          ROLLBACK; 
          PV_ERROR:=LV_ERROR;
         WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR:=SUBSTR(SQLERRM,1,500);
          
                 
  END   EJECUTA_PROCESOS_QC_JOBS;                                  
   -- FIN PMERA - 08/11/2016                    
END GSI_CONF_MATRICES_QC_PLANES;
/
