CREATE OR REPLACE PACKAGE API_HOMOLOGACION_BSCS IS

 --====================================================================================                                           
  -- Proposito:    Procedure que muestra los rubros de la factura del cliente
  -- Autor:        CIMA: Azucena Moran 
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        28/01/2016
  -- Proyecto:     [10540] Homologación de Mailing SAC 527. 
  --====================================================================================   
  -- PURPOSE : OBTENER LOS RUBROS ADICIONALES Y CARGOS DE FACTURA DEL CLIENTE DESDE BSCS PARA PASARLOS A AXIS

  PROCEDURE P_OBTIENE_RUBROS (PV_CUENTA      VARCHAR2,
                             PV_FECHA_CORTE VARCHAR2,
                             PV_RUBROS      OUT VARCHAR2,
                             PV_CREDITOS    OUT VARCHAR2,
                             PV_IMPUESTOS   OUT VARCHAR2,
                             PV_DESCUENTOS  OUT VARCHAR2,
                             PN_ERROR       OUT VARCHAR2,
                             PV_ERROR       OUT VARCHAR2,
                             PV_NUM_FACTURA OUT VARCHAR2,
                             PV_ADICIONALES OUT VARCHAR2);
                             
END API_HOMOLOGACION_BSCS;
/
CREATE OR REPLACE PACKAGE BODY API_HOMOLOGACION_BSCS IS
  
--====================================================================================                                           
  -- Proposito:    Procedure que muestra los rubros de la factura del cliente
  -- Autor:        CIMA: Azucena Moran 
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        28/01/2016
  -- Proyecto:     [10540] Homologación de Mailing SAC 527. 
  --====================================================================================   
  
  --====================================================================================                                           
  -- Proposito:    Obtener rubros exentos de la factura
  -- Autor:        CIMA: Azucena Moran 
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        22/03/2016
  -- Proyecto:     [10540] Homologación de Mailing SAC 527. 
  --====================================================================================   
    --====================================================================================                                           
  -- Proposito:    Obtención de los rubros de cargo y créditos a facturación.
  --               Obtención de valores a pagar por rangos de ciclos de facturación.
  -- Autor:        CIMA: Azucena Moran 
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        01/04/2016
  -- Proyecto:     [10540] Homologación de Mailing SAC 527. 
  --====================================================================================   
  PROCEDURE P_OBTIENE_RUBROS(PV_CUENTA      VARCHAR2,
                             PV_FECHA_CORTE VARCHAR2,
                             PV_RUBROS      OUT VARCHAR2,
                             PV_CREDITOS    OUT VARCHAR2,
                             PV_IMPUESTOS   OUT VARCHAR2,
                             PV_DESCUENTOS  OUT VARCHAR2,
                             PN_ERROR       OUT VARCHAR2,
                             PV_ERROR       OUT VARCHAR2,
                             PV_NUM_FACTURA OUT VARCHAR2,
                             PV_ADICIONALES OUT VARCHAR2) IS
  
    --DECLARACION DE VARIABLES     
    --CREDITOS A FACTURACION  
    --nos aseguramos que los rubros tomados sean por credito a facturacion   
    LV_CONSULTA_CREDITOS        VARCHAR2(4000) := 'SELECT SUM(VALOR) FROM <TABLA_BSCS>
                                                          ,MPUSNTAB
                                                     WHERE CUSTCODE = :1
                                                      AND TIPO = ''006 - CREDITOS''
                                                      AND SHDES = ''EV006'''; -->'Crédito a Facturación'
                                                
   --CARGOS
   --nos aseguramos que los rubros tomados sean por cargo a facturacion 
    LV_CONSULTA_CARGOS          VARCHAR2(4000) := 'SELECT SUM(VALOR) 
                                                          FROM <TABLA_BSCS>
                                                          ,MPUSNTAB           
                                                    WHERE CUSTCODE = :1
                                                    AND TIPO = ''005 - CARGOS''
                                                    AND SHDES = ''EV006'''; -->'Cargo a Facturación'
                                                
    LV_CONSULTA_IMPUESTOS       VARCHAR2(4000) := 'SELECT SUM(VALOR) 
                                                          FROM <TABLA_BSCS>
                                                     WHERE CUSTCODE = :1
                                                     AND TIPO = ''003 - IMPUESTOS''';
              
    LV_CONSULTA_RUBROS          VARCHAR2(4000) := 'SELECT SUM(VALOR)
                                                    FROM <TABLA_BSCS>
                                                     WHERE CUSTCODE = :1
                                                    AND TIPO = ''002 - ADICIONALES''';
  
    LV_CONSULTA_DESCUENTOS      VARCHAR2(4000) := 'SELECT SUM(DESCUENTO) 
                                                    FROM <TABLA_BSCS>
                                                      WHERE CUSTCODE = :1
                                                    AND TIPO = ''002 - ADICIONALES''';
                                               
   LV_CONSULTA_ADICIONALES      VARCHAR2(4000) :=  'SELECT SUM(VALOR) 
                                                      FROM <TABLA_BSCS>
                                                    WHERE CUSTCODE = :1
                                                     AND TIPO = ''004 - EXENTO''
                                                    GROUP BY VALOR, NOMBRE';      
                                                    
    
   LV_CONSULTA_NUM_FACTURA      VARCHAR(4000):='SELECT DISTINCT(OHREFNUM) NUM_FACTURA 
                                                       FROM <TABLA_BSCS> 
                                                       WHERE CUSTCODE = :1';                                                                                                                                                                        
  
    LV_TABLA_BSCS         VARCHAR2(50) := ('CO_FACT_');
    LV_USUARIO            VARCHAR2(50) := 'SYSADM';
    LV_CARGOS_ADICIONALES VARCHAR2(25):='0,00';
    LV_CREDITOS           VARCHAR2(25):='0,00';
    LV_IMPUESTOS          VARCHAR2(25):='0,00';
    LV_DESCUENTOS         VARCHAR2(25):='0,00';
    LV_CARGOS             VARCHAR2(25):='0,00';
    LV_NUMERO_FACTURA     VARCHAR2(50):='0,00';
    LV_RUBROS             VARCHAR2(50):='0,00';
    
  BEGIN
  
    LV_TABLA_BSCS := LV_USUARIO || '.' || LV_TABLA_BSCS || PV_FECHA_CORTE;
  
    -- CREDITOS
  
    LV_CONSULTA_CREDITOS := REPLACE(LV_CONSULTA_CREDITOS, '<TABLA_BSCS>',
                                    LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_CREDITOS
        INTO LV_CREDITOS
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_CREDITOS := LV_CREDITOS;
   
   -- CARGOS
   LV_CONSULTA_CARGOS := REPLACE(LV_CONSULTA_CARGOS, '<TABLA_BSCS>',
                                    LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_CARGOS
        INTO LV_CARGOS
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
   -- PV_CARGOS := LV_CARGOS;
   
   
    -- IMPUESTOS
  
    LV_CONSULTA_IMPUESTOS := REPLACE(LV_CONSULTA_IMPUESTOS, '<TABLA_BSCS>',
                                     LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_IMPUESTOS
        INTO LV_IMPUESTOS
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_IMPUESTOS := LV_IMPUESTOS;
  
    -- ADICIONALES **
  
    LV_CONSULTA_ADICIONALES := REPLACE(LV_CONSULTA_ADICIONALES,
                                       '<TABLA_BSCS>', LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_ADICIONALES
        INTO LV_CARGOS_ADICIONALES
        USING PV_CUENTA;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_ADICIONALES := LV_CARGOS_ADICIONALES;
    
       -- RUBROS **
  
    LV_CONSULTA_RUBROS := REPLACE(LV_CONSULTA_RUBROS,
                                  '<TABLA_BSCS>', LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_RUBROS
        INTO LV_RUBROS
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_RUBROS := LV_RUBROS;
  
    -- DESCUENTOS
  
    LV_CONSULTA_DESCUENTOS := REPLACE(LV_CONSULTA_DESCUENTOS, '<TABLA_BSCS>',
                                      LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_DESCUENTOS
        INTO LV_DESCUENTOS
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_DESCUENTOS := LV_DESCUENTOS;
    
    -- NUMERO DE FACTURA 
    LV_CONSULTA_NUM_FACTURA := REPLACE(LV_CONSULTA_NUM_FACTURA, '<TABLA_BSCS>',
                                       LV_TABLA_BSCS);
  
    BEGIN
      EXECUTE IMMEDIATE LV_CONSULTA_NUM_FACTURA
        INTO LV_NUMERO_FACTURA
        USING PV_CUENTA;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := SUBSTR(SQLERRM, 300);
    END;
  
    PV_NUM_FACTURA := LV_NUMERO_FACTURA;
  
    PN_ERROR      := 0;
    PV_ERROR      := 'Exito';
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_ERROR := -1;
      PV_ERROR := (SQLERRM);
  END;

END API_HOMOLOGACION_BSCS;
/
