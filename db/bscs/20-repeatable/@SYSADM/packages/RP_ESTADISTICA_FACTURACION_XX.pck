CREATE OR REPLACE package rp_estadistica_facturacion_XX is
  ----------------------------------------------------------------------
Procedure GrabaBitacora(pv_tipo	       	VARCHAR2,
                  			pv_fecha       	DATE,
                  			pv_msg_error   	VARCHAR2,
                  			pv_usuario     	VARCHAR2,
                  			pv_prg		      VARCHAR2,
                  			pv_comentario	  VARCHAR2 );
  ----------------------------------------------------------------------
  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);
  ----------------------------------------------------------------------
  PROCEDURE Depuracion_IVA_ICE_y_dupli (pd_lvMensErr           out varchar2  );
  ----------------------------------------------------------------------
  PROCEDURE Duplicado (pd_lvMensErr OUT VARCHAR2  );
  ----------------------------------------------------------------------
  Procedure  Obtiene_fechas   ( pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 );
  ----------------------------------------------------------------------
  Procedure Obtiene_fechas_periodo  (   pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 );
  ----------------------------------------------------------------------
  FUNCTION CREA_TABLA (pv_tabla    varchar2,
                       pvMensErr   out varchar2) RETURN NUMBER;
  ----------------------------------------------------------------------
  FUNCTION CREA_REGISTROS (pv_tabla    varchar2,
                          pvMensErr   out varchar2) RETURN NUMBER;
  ----------------------------------------------------------------------
  PROCEDURE Procesa_x_Telefono (pvTabla         varchar2,
                                pvMensErr   out varchar2);
  ----------------------------------------------------------------------
  Procedure Guarda_resumen_MINUTOS (pv_fechaFinal      varchar2,
                                    pvMensErr   out varchar2);
  ----------------------------------------------------------------------
  PROCEDURE Guarda_total_minuto_factura ( pv_fechaFinal      varchar2,
                                        pvMensErr      out varchar2) ;
  ----------------------------------------------------------------------
    Procedure  FacturacionSaliente (pv_Fechafinal     varchar2,
                                    pvMens_Error      out varchar2 );
  ----------------------------------------------------------------------
  PROCEDURE MAIN ; 
  ----------------------------------------------------------------------
  PROCEDURE DATOS_AXIS;
  ----------------------------------------------------------------------
  PROCEDURE Retorna_Informacion_PLAN ( pv_plan          VARCHAR2,
                                     pv_tipo          OUT VARCHAR2,
                                     pv_subproducto   OUT VARCHAR2,
                                     pv_tecno_1       OUT VARCHAR2,
                                     pv_tecno_2       OUT VARCHAR2,
                                     pv_tarifabasica  OUT VARCHAR2,
                                     pv_cargoapaquete OUT VARCHAR2 );
  ----------------------------------------------------------------------
  PROCEDURE Actualiza_DatosPlanes (pv_tabla  VARCHAR2);
  ----------------------------------------------------------------------
  PROCEDURE OBTENER_info_CTA ( pv_fecha   VARCHAR2);
  ----------------------------------------------------------------------
end rp_estadistica_facturacion_XX;
/
CREATE OR REPLACE package body rp_estadistica_facturacion_XX is
---------------------------------------------------------------------------------------------
Procedure GrabaBitacora(pv_tipo	       	VARCHAR2,
                  			pv_fecha       	DATE,
                  			pv_msg_error   	VARCHAR2,
                  			pv_usuario     	VARCHAR2,
                  			pv_prg		      VARCHAR2,
                  			pv_comentario	  VARCHAR2 ) IS
--
BEGIN
  --
  EXECUTE IMMEDIATE 'insert into rp_BITACORA_TMP( tipo, fecha, msg_error, usuario, prg, comentario) values(:1,:2,:3,:4,:5,:6)'
  USING  pv_tipo, pv_fecha, pv_msg_error, pv_usuario, pv_prg, pv_comentario ;
  --
END;
--------------------------------------------------------------------------------------
  PROCEDURE EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2) is
    x_cursor integer;
    z_cursor integer;
    name_already_used EXCEPTION;
    PRAGMA EXCEPTION_INIT(name_already_used, -955);
  begin
    x_cursor := dbms_sql.open_cursor;
    DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
    z_cursor := DBMS_SQL.EXECUTE(x_cursor);
    DBMS_SQL.CLOSE_CURSOR(x_cursor);
  EXCEPTION
    WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN DBMS_SQL.CLOSE_CURSOR(x_cursor); END IF;
      pv_error := null;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN DBMS_SQL.CLOSE_CURSOR(x_cursor); END IF;
      pv_error := sqlerrm;
  END EJECUTA_SENTENCIA;
  --
  -----------------------------------------------------------------------------
PROCEDURE Depuracion_IVA_ICE_y_dupli (pd_lvMensErr OUT VARCHAR2  ) IS
    --
    LvSEntencia          VARCHAR2(10000);
    LvSEntencia2         VARCHAR2(10000);
    lvMensErr            VARCHAR2(1000);
    lv_telefono          VARCHAR2(24);
    lv_plan              VARCHAR2(30);
    lv_cuenta            VARCHAR2(24);
    --
    cuenta_regi          NUMBER;
    ln_vez               NUMBER;
    cont                 NUMBER;
    vn_registros_grabar  NUMBER:=5000;
    ------------------------------------------------------------------
    CURSOR  telefono is --  Para los ICE-IVA
            SELECT DISTINCT cuenta,telefono,minutos plan 
            FROM   rp_fact_detalle_cta_telefono  
            WHERE telefono IS NOT NULL AND minutos IS NOT NULL;
    ------------------------------------------------------------------
    CURSOR  regi_iva is  --  Depuraci�n en los registros de IVA e ICE
        SELECT ROWID, R.* FROM rp_fact_detalle_cta_telefono  r
        where  r.cuenta      = lv_cuenta--'3.332' 
        and    r.telefono    = lv_telefono--'4500692' 
        and    r.minutos     = lv_plan-- 'Pool GSM 30,000 plus'
        and    descripcion LIKE 'I.V.A.%12%'
        ORDER BY descripcion;
    ------------------------------------------------------------------
    cursor  regi_ice is --  Depuraci�n en los registros de IVA e ICE
        select rowid, r.telefono
        from   rp_fact_detalle_cta_telefono  r
        where  r.cuenta      = lv_cuenta--'3.332' 
        and    r.telefono    = lv_telefono--'4500692' 
        and    r.minutos     = lv_plan-- 'Pool GSM 30,000 plus'
        AND    (descripcion = 'I.C.E. (15)' or substr(descripcion,1,13) = 'ICE de Teleco')
        ORDER BY descripcion;
    ------------------------------------------------------------------
    CURSOR dupli IS
        SELECT   R.* , COUNT(*) cuantos
        FROM     rp_fact_detalle_cta_telefono  r 
        WHERE    letra !='M'
        GROUP  BY cuenta, telefono, letra, descripcion, varios, minutos, valor
        HAVING COUNT(*) > 1
        ORDER  BY cuenta, telefono, letra, descripcion, varios, minutos, valor;
    ------------------------------------------------------------------
    CURSOR quita_espacios_plan IS
       SELECT ROWID, R.* FROM rp_fact_detalle_cta_telefono r WHERE minutos IS NOT NULL;
    ------------------------------------------------------------------
    Begin
      --
      cuenta_regi := 0;
      lvSentencia := ' update rp_fact_detalle_cta_telefono r set minutos = ltrim(rtrim(minutos)) '||
                     ' where rowid = :1 '; --  i.rowid
      --
      FOR I IN quita_espacios_plan LOOP  -- -- quita los espacios del nombre del plan (minutos)
          --
          cuenta_regi := cuenta_regi + 1;
          BEGIN EXECUTE IMMEDIATE lvSEntencia USING i.rowid;
          EXCEPTION WHEN OTHERS THEN pd_lvMensErr := sqlerrm;
          END;
          IF MOD(cuenta_regi, vn_registros_grabar)= 0 THEN COMMIT; END IF;
          --
      END LOOP;
      COMMIT;
      ---------------------------------------------------------------------------------------------
      --  Primero elimino los registros duplicados: Borro los registros y luego inserto solo uno
      --
      cuenta_regi := 0;
      /*
      LvSEntencia := '   delete from rp_fact_detalle_cta_telefono '||
                     '   where  cuenta     = :1 '|| --||''''|| i.cuenta ||''''||
                     '   and    telefono   = :2 '|| --||''''||i.telefono ||''''||
                     '   and    letra      = :3 '|| --||''''||i.letra||''''||
                     '   and    descripcion= :4 '; --||''''||i.descripcion||''''
      LvSentencia2 := ' insert into rp_fact_detalle_cta_telefono values (:1, :2, :3, :4, :5, :6. :7)' ;
     */
      --
      FOR I IN dupli LOOP
          --
          BEGIN
            --EXECUTE IMMEDIATE lvSEntencia  USING i.cuenta, i.telefono, i.letra, i.descripcion;
            delete from rp_fact_detalle_cta_telefono 
            where  cuenta     = i.cuenta             
            and    decode(telefono,NULL,0,telefono) = decode(i.telefono,NULL,0,i.telefono)
            and    letra      = i.letra 
            and    descripcion= i.descripcion;
          EXCEPTION 
              WHEN OTHERS THEN 
                   pd_lvMensErr := sqlerrm;
                   GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - DELETE '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));    
          END;
          --
          BEGIN
            insert into rp_fact_detalle_cta_telefono values (i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor);
             --EXECUTE IMMEDIATE lvSEntencia2 USING i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor;
          EXCEPTION 
             WHEN OTHERS THEN 
                  pd_lvMensErr := sqlerrm;
                  GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - INSERT '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
          END;
          --
          IF MOD(cuenta_regi, vn_registros_grabar ) = 0 THEN COMMIT; END IF;
          cuenta_regi := cuenta_regi + 1;
          --
      END LOOP;
      COMMIT;
      ---------------------------------------------------------------------------------------------
      -- Luego cuando tengo doble registro por IVA e ICE
      --
      LvSentencia := ' delete from rp_fact_detalle_cta_telefono where rowid = :1 ';
      cuenta_regi := 0;
      --
      FOR K IN telefono LOOP
           --
           lv_telefono := k.telefono;    lv_cuenta   := k.cuenta;      lv_plan     := k.plan;
           ln_vez := 0;
           --  ICE
           FOR I IN regi_ice LOOP 
              ln_vez := ln_vez + 1;
              IF ln_vez = 2 THEN EXECUTE IMMEDIATE lvSEntencia USING i.ROWID; END IF;
           END LOOP;
           -- IVA
           ln_vez := 0;
           FOR I IN regi_iva LOOP 
              ln_vez := ln_vez + 1;
              IF ln_vez = 2 THEN  EXECUTE IMMEDIATE lvSEntencia USING i.ROWID; END IF;
           END LOOP;
           --
           IF MOD(cuenta_regi, vn_registros_grabar)= 0 THEN COMMIT; END IF;
           cuenta_regi := cuenta_regi + 1;
     END LOOP;
     COMMIT;       
    --
    --  Me da problemas en el rato de creacion porque tiene el signo "/" se los quito.
    BEGIN
        update rp_fact_detalle_cta_telefono set descripcion = 'Cuota_Inicial_del_Equipo'
        where letra = 'm'
        and descripcion like 'Cuota_Inicial_del_Equipo%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := sqlerrm;
             GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' Actualiza Cuota_Inicial_del_Equipo '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
    BEGIN
        update rp_fact_detalle_cta_telefono set descripcion = 'Penalizacion'
        where letra = 'm'
        and descripcion like 'Penalizacion%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := sqlerrm;
             GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' Actualiza Penalizacion '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
    BEGIN
        update rp_fact_detalle_cta_telefono set descripcion = 'Pooling Corporativo Adicional'
        where letra = 'm'
        and descripcion like 'Pooling Corporativo Adicional%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := sqlerrm;
             GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' Actualiza Pooling Corporativo Adicional '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
 END Depuracion_IVA_ICE_y_dupli; 
---------------------------------------------------------------------------------
PROCEDURE Duplicado (pd_lvMensErr OUT VARCHAR2  ) IS
    --
    LvSEntencia          VARCHAR2(10000);
    LvSEntencia2          VARCHAR2(10000);
    lvMensErr            VARCHAR2(1000);
    lv_telefono          VARCHAR2(24);
    lv_plan              VARCHAR2(30);
    lv_cuenta            VARCHAR2(24);
    --
    cuenta_regi          NUMBER;
    ln_vez               NUMBER;
    cont                 NUMBER;
    vn_registros_grabar  NUMBER:=5000;
    ------------------------------------------------------------------
    CURSOR dupli IS
        SELECT   R.* , COUNT(*) cuantos
        FROM     rp_fact_detalle_cta_telefono  r 
        WHERE    letra !='M'
        GROUP  BY cuenta, telefono, letra, descripcion, varios, minutos, valor
        HAVING COUNT(*) > 1
        ORDER  BY cuenta, telefono, letra, descripcion, varios, minutos, valor;
    ------------------------------------------------------------------
    Begin
      ---------------------------------------------------------------------------------------------
      --  Primero elimino los registros duplicados: Borro los registros y luego inserto solo uno
      --
      cuenta_regi := 0;
      LvSEntencia := '   delete from rp_fact_detalle_cta_telefono '||
                     '   where  cuenta     = :1 '|| --||''''|| i.cuenta ||''''||
                     '   and    decode(telefono,NULL,0,telefono)   = decode(:2,NULL,0,:2)  '||--telefono   =  '|| --||''''||i.telefono ||''''||
                     '   and    letra      = :3 '|| --||''''||i.letra||''''||
                     '   and    descripcion= :4 '; --||''''||i.descripcion||''''
      LvSentencia2 := ' insert into rp_fact_detalle_cta_telefono values (:1, :2, :3, :4, :5, :6. :7)' ;
      --
      FOR I IN dupli LOOP
          --
          BEGIN
            --EXECUTE IMMEDIATE lvSEntencia  USING i.cuenta, i.telefono, i.letra, i.descripcion;
            delete from rp_fact_detalle_cta_telefono 
            where  cuenta     = i.cuenta             
            and    decode(telefono,NULL,0,telefono) = decode(i.telefono,NULL,0,i.telefono)
            and    letra      = i.letra 
            and    descripcion= i.descripcion;
          EXCEPTION 
              WHEN OTHERS THEN 
                   pd_lvMensErr := sqlerrm;
                   GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - DELETE '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));    
          END;
          --
          BEGIN
            INSERT INTO rp_fact_detalle_cta_telefono VALUES (i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor);
             --EXECUTE IMMEDIATE lvSEntencia2 USING i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor;
          EXCEPTION 
             WHEN OTHERS THEN 
                  pd_lvMensErr := sqlerrm;
                  GrabaBitacora( 'FACT', sysdate, pd_lvMensErr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - INSERT '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
          END;
          --
          IF MOD(cuenta_regi, vn_registros_grabar ) = 0 THEN COMMIT; END IF;
          cuenta_regi := cuenta_regi + 1;
          --
      END LOOP;
      COMMIT;
    --
 END;
-----------------------------------------------------------------------------------------
--
    Procedure  Obtiene_fechas   ( pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 ) is
    --
    -- variables
    ld_fecha_inicial        date;
    ld_fecha_final		      date;
    lv_fecha_I              varchar2(10);
    lv_fecha_F              varchar2(10);
    lv_fecha                varchar2(10);
    mes_rango_v             varchar2(3);
    mes_rango               number;
    mes_actual              number;
    anio_rango              number;
    anio_final              number;
    dia_fin                 number;
    Begin
       --
       mes_actual := to_number(substr(pdFechaBase,5,2));
       anio_final := to_number(substr(pdFechaBase,1,4));
       anio_rango := anio_final;
       --
       dia_fin := 31;
       mes_rango     := mes_actual - 1;
       if    mes_rango  = 0 then 
             mes_rango  := 12;
             anio_rango := anio_rango - 1;
       elsif mes_rango in ( 4, 6, 7, 11 ) then dia_fin := 30;
       elsif mes_rango = 2 then
             if (mod(anio_final,4)=0) then dia_fin := 29;
                                      else dia_fin := 28;
             end if;
          end if;
       -- convierto las fechas
       mes_rango_v := to_char(mes_rango);
       if length(rtrim(to_char(mes_rango))) = 1 then mes_rango_v := '0'||mes_rango; end if;
       --
       -- FINAL            
       lv_fecha_f := dia_fin||'/'||mes_rango_v||'/'||anio_rango;
       --ld_fecha_final	  := to_date(lv_fecha_f, 'dd/mm/yyyy');       
       -- INICIAL
       mes_rango     := mes_rango - 1;
       if    mes_rango  = 0 then 
             mes_rango  := 12;
             anio_rango := anio_rango - 1;
       end if;
       -- convierto las fechas
       mes_rango_v := to_char(mes_rango);
       if length(rtrim(to_char(mes_rango))) = 1 then mes_rango_v := '0'||mes_rango; end if;
       --
       lv_fecha_i := '24/'||mes_rango_v||'/'||anio_rango;
       --ld_fecha_inicial	:= to_date(lv_fecha_i, 'dd/mm/yyyy');       
       -- retorno
       pv_fechaInicial := lv_fecha_i;
       pv_fechaFinal := lv_fecha_f;
    
    End;
    ---------------------------------------------------------------------------------------------
    --
    Procedure Obtiene_fechas_periodo  (   pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 ) is
    --
    -- variables
    lv_fecha_I              varchar2(10);
    lv_fecha_F              varchar2(10);
    lv_fecha                varchar2(10);
    mes_rango_v             varchar2(3);
    mes_rango               number;
    mes_actual              number;
    anio_rango              number;
    anio_final              number;
    Begin
       --
       mes_actual := to_number(substr(pdFechaBase,5,2));
       anio_final := to_number(substr(pdFechaBase,1,4));
       anio_rango := anio_final;
       --
       mes_rango     := mes_actual - 1;
       --
       if mes_rango  = 0 then 
          mes_rango  := 12;
          anio_rango := anio_rango - 1;
       end if;
       --
       -- convierto las fechas
       mes_rango_v := to_char(mes_rango);
       if length(rtrim(to_char(mes_rango))) = 1 then
          mes_rango_v := '0'||mes_rango;
       end if;
       lv_fecha_f := '23/'||mes_rango_v||'/'||anio_rango;
            
       -- INICIAL
       mes_rango     := mes_rango - 1;
       if    mes_rango  = 0 then 
             mes_rango  := 12;
             anio_rango := anio_rango - 1;
       end if;
       -- convierto las fechas
       mes_rango_v := to_char(mes_rango);
       if length(rtrim(to_char(mes_rango))) = 1 then
          mes_rango_v := '0'||mes_rango;
       end if;

       lv_fecha_i := '24'||'/'||mes_rango_v||'/'||anio_rango;
       --ld_fecha_inicial	:= to_date(lv_fecha_i, 'dd/mm/yyyy');
       --ld_fecha_final	  := to_date(lv_fecha_f, 'dd/mm/yyyy');

       -- retorno
       pv_fechaInicial := lv_fecha_i;
       pv_fechaFinal := lv_fecha_f;
    
    End;
  ------------------------------------------------------------------------------
  -- CREA_TABLA
  --
  FUNCTION CREA_TABLA (pv_tabla    VARCHAR2,
                       pvMensErr   OUT VARCHAR2) RETURN NUMBER IS
    --
    lvSentencia       VARCHAR2(32700):=null;
    lvMensErr         VARCHAR2(1000);
    --
    lv_cadena         varchar2(30000):=null;
    lvSentenciaAux    varchar2(30000):=null;
    lvSentencia2      varchar2(1000):=null;
    --
    CURSOR nombre_columna IS
           SELECT /*+ RULE */ DISTINCT 
                      substr(REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(  REPLACE( REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(REPLACE(REPLACE(letra||'_'||descripcion, '	', ''), '�', 'o'), '.', '_')  , ':', '') , ' ', '_')   , '%', '') , ')', ''),    '(', '') ,'-', '') ,'+', '')      , '�', 'o')     , '�', 'a') , 'xisn', 'xion')  , 1,30) ||
                  '                         VARCHAR2(60),' campo
           FROM rp_fact_detalle_cta_telefono WHERE letra IN('m','q','c','q1');
    --
  BEGIN
    --
    lvSentencia := ' CREATE TABLE ' ||pv_tabla ||
                   '( CUENTA               VARCHAR2(24), '||
                   '  TELEFONO             VARCHAR2(24), '||
                   '  TECNOLOGIA           VARCHAR2(4),  '||
                   '  REGION               VARCHAR2(4), '||
                   '  PRODUCTO             VARCHAR2(4), '||
                   '  CENTRO_COSTOS        VARCHAR2(60), '||
                   '  NOMBRES              VARCHAR2(60),' ||
                   '  tipo                 VARCHAR2(4),' ||
                   '  EQUIPO_ACTUAL        VARCHAR2(30),' ;
    --
    -- continuo con la sentencia
    lvSentencia2 := 
                   ' )' ||
                   ' tablespace DATA ' || '  pctfree 10' || '  pctused 40' ||
                   '  initrans 1 ' || '  maxtrans 255' || '  storage ' ||
                   '  (initial 10M ' || '    next 1M ' ||
                   '    minextents 1' || '    maxextents unlimited ' ||
                   '    pctincrease 0) ';
    -- 
    -- Barro los campos 
    --
    FOR I IN nombre_columna LOOP lv_cadena := lv_cadena ||' '||i.campo; END LOOP;
    --
    -- Quito la ultima coma de la cadena formada
    --
    lvSentenciaAux := substr( lv_cadena , 1, length(lv_cadena)-1);
    --
    LvSentencia := LvSentencia||' '||Lvsentenciaaux||' '||lvSentencia2;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --
    -- crea indice
    --
    lvSentenciaAux := 
      '     create index IDX_RP_FACTURACION on  RP_FACTURACION (CUENTA,TELEFONO,C_PLAN,TECNOLOGIA,REGION,PRODUCTO,TIPO) '||
      '   tablespace IND '||
      '   pctfree 10 '||
      '   initrans 2 '||
      '   maxtrans 255 '||
      '   storage '||
      '   ( '||
      '     initial 2M '||
      '     next 1M '||
      '     minextents 1 '||
      '     maxextents unlimited '||
      '     pctincrease 0 '||
      '   ) '
      ;
      EXECUTE IMMEDIATE Lvsentenciaaux;
      --
      -- creando los privilegios
      EXECUTE IMMEDIATE 'grant select, insert, update, delete, references, alter, index on RP_FACTURACION to PUBLIC ';
      --
    pvMensErr:= lvMensErr;
    RETURN 1;
  
  EXCEPTION
    WHEN OTHERS THEN
         pvMensErr := SQLERRM;
         RETURN 0;
  END CREA_TABLA;
  --
  ------------------------------------------------------------------------------
  -- Objetivo: Crear registros por  cuenta, telefono y plan.  Para los casos donde
  --           se tiene informaci�n de la cuenta y no sus telefonos deben ser registrados 
  --           para efecto de cuadre de la cuenta. as� que se modifica para a�adir la cuenta
  --           pero el telefono es ficticio:1000000
  --
  FUNCTION CREA_REGISTROS (pv_tabla       VARCHAR2,
                           pvMensErr  OUT VARCHAR2) RETURN NUMBER IS
    --
    lvSentencia                      VARCHAR2(5000):=NULL;
    lvMensErr                        VARCHAR2(1000);
    --
    cont                             NUMBER:= 0;
    vn_registros_grabar              NUMBER:= 5000;
    --
    CURSOR regi IS -- solo de cuentas y luego insertare todos los registros de telefono e inclusive el ficticio x la cuenta
       SELECT /*+ RULE */ DISTINCT cuenta from rp_fact_detalle_cta_telefono;
    --
  BEGIN
     --
     LvSentencia :=  ' insert /*+ append */ into rp_facturacion NOLOGGING (cuenta,telefono,c_plan)                                                         '||
                     ' ( SELECT /*+ RULE */ DISTINCT RTRIM(LTRIM(cuenta)), RTRIM(LTRIM(telefono)), RTRIM(LTRIM(minutos)) from rp_fact_detalle_cta_telefono '||
                     '   WHERE  telefono IS NOT NULL AND minutos IS NOT NULL and cuenta = :1 and letra not in ('||''''||'A'||''''||' , '||''''||'m'||''''||' , '||''''||'M'||''''|| ' )'||
                     '   UNION                                                                                                                            '||
                     '   SELECT /*+ RULE */ DISTINCT RTRIM(LTRIM(cuenta)), '||''''||'1000000'||''''||' , null from rp_fact_detalle_cta_telefono            '||
                     '   WHERE  telefono IS NULL and cuenta = :2                                                                                           '||
                     '   UNION                                                                                                                             '||
                     '   SELECT /*+ RULE */ DISTINCT RTRIM(LTRIM(cuenta)), RTRIM(LTRIM(telefono)), RTRIM(LTRIM(minutos)) from rp_fact_detalle_cta_telefono '||
                     '   WHERE  telefono IS NOT NULL AND minutos IS NULL and cuenta = :3 and letra not in ('||''''||'A'||''''||' , '||''''||'m'||''''||' , '||''''||'M'||''''|| ' )'||
                     ' ) ';
     FOR I IN regi LOOP
         --
         BEGIN
            EXECUTE IMMEDIATE lvSentencia USING i.cuenta, i.cuenta,i.cuenta;
         EXCEPTION
            WHEN OTHERS THEN
                 lvMensErr := SQLERRM;
                 GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion_XX.CREA_REGISTROS','Procesa x telefono: Cuenta: '||i.cuenta );
         END;
         IF MOD(cont, vn_registros_grabar)= 0 THEN COMMIT; END IF;
         cont := cont + 1;
         --
     END LOOP;
     COMMIT; -- para los faltantes
     RETURN 1;
     --
  EXCEPTION
      WHEN OTHERS THEN
           pvmenserr := SQLERRM;
           RETURN 0;
  END;
  --
  ------------------------------------------------------------------------------
  --
  PROCEDURE Procesa_x_Telefono (pvTabla         VARCHAR2,
                                pvMensErr   OUT VARCHAR2) IS
    --
    lvsentencia                      VARCHAR2(10000):=NULL;
    lvsentencia1                     VARCHAR2(100)  :=NULL;
    lvsentencia2                     VARCHAR2(20000):=NULL;
    lvsentencia3                     VARCHAR2(100)  :=NULL;
    lv_cadena                        VARCHAR2(20000):=NULL;    
    lvmenserr                        VARCHAR2(1000) :=NULL;
    LvSelect                         VARCHAR2(500);
    --
    cont_regi                        NUMBER:= 0;
    vn_registros_grabar              NUMBER:= 50;
    source_cursor                    NUMBER;
    --
    rows_processed                   INTEGER;
    --
    TYPE     RegiCurTyp IS REF CURSOR;
    regi_cv  RegiCurTyp;
    --  variables del cursor 1
    lv_cuenta                        VARCHAR2(24);
    lv_telefono                      VARCHAR2(24);
    lv_plan                          VARCHAR2(60);
    lv_rowid                         VARCHAR2(100);
    --
    CURSOR letra_q_valor IS
          SELECT '  '||
                      substr(REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(  REPLACE( REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(REPLACE(REPLACE(letra||'_'||descripcion, '	', ''), '�', 'o'), '.', '_')  , ':', '') , ' ', '_')   , '%', '') , ')', ''),    '(', '') ,'-', '') ,'+', '')      , '�', 'o')     , '�', 'a') , 'xisn', 'xion')  , 1,30) ||
             ' = '||''''||valor||''''||',' campo_guardar
          FROM (   
                  SELECT r.cuenta, r.telefono, r.minutos, letra, descripcion, --to_char(sum(to_number(replace(valor,',','')))) valor
                         to_char(sum(to_number(   REPLACE( REPLACE(valor, ',', '')  , '$', '')   ))) valor
                  FROM   rp_fact_detalle_cta_telefono r 
                  WHERE  letra         IN ('q','q1')
                  AND    r.cuenta      = lv_cuenta   --'1.10000488' 
                  and    decode(r.telefono,NULL,0,r.telefono)  = decode(lv_telefono,NULL,0,lv_telefono)
                  and    decode(r.minutos,NULL,' ',r.minutos) = decode(lv_plan,NULL,' ',lv_plan)
                  GROUP BY  r.cuenta, r.telefono, r.minutos, letra, descripcion
                  UNION
                  SELECT r.cuenta, r.telefono, r.minutos, letra, descripcion, 
                         to_char(SUM(to_number(   REPLACE( REPLACE(rtrim(valor), ',', '')  , '$', '')   ))) valor
                  FROM   rp_fact_detalle_cta_telefono r
                  WHERE  letra         in ('c','m')
                  AND    r.cuenta      = lv_cuenta   --'1.10000488' 
                  and    decode(r.telefono,NULL,0,r.telefono)  = decode(lv_telefono,NULL,0,lv_telefono)
                  and    decode(r.minutos,NULL,' ',r.minutos) = decode(lv_plan,NULL,' ',lv_plan)
                  and    descripcion != 'Plan:'                  
                  GROUP BY  r.cuenta, r.telefono, r.minutos, letra, descripcion
                );
    --
  BEGIN
      --
      LvSentencia1 := 'Update '|| pvtabla ||' set ';
      LvSentencia3 := ' Where rowid = :1 ';
      --  Cursor principal.
      --
      OPEN regi_cv FOR 'select cuenta, telefono, c_plan , rowid from '||pvtabla; -- open cursor variable
--        OPEN regi_cv FOR 'select cuenta, telefono, c_plan , rowid from '||pvtabla ||' where cuenta =   '||''''||'1.10000006'||'''';
      LOOP
        cont_regi   := 0;
        -- valido que si NO tiene datos se registre en la tabla de bitacora
        FETCH  regi_cv INTO lv_cuenta, lv_telefono, lv_plan , lv_rowid;
        EXIT WHEN regi_cv%NOTFOUND;
               --
               -- Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
               --
               lvSentencia2 := NULL;
               lv_cadena    := NULL;
               IF lv_telefono = '1000000' THEN lv_telefono := NULL; END IF;
               FOR d IN letra_q_valor LOOP
                   lv_cadena := lv_cadena || d.campo_guardar;
               END LOOP;
               lvSentencia2 := substr( lv_cadena , 1, length(lv_cadena)-1); --  Quitar la ultima coma
               --
               --  Actualizando los campos en la RP_FACTURACION
               BEGIN 
                   EXECUTE IMMEDIATE LvSentencia1||' '||Lvsentencia2||' '||Lvsentencia3 USING lv_rowid;
               EXCEPTION
               WHEN OTHERS THEN
                    lvmenserr := SQLERRM; 
                    GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion_XX', 'No pudo. Procesa x telefono: '|| lv_telefono || ' Plan: '||lv_plan|| 'Cuenta: '||lv_cuenta ||'Rowid: '||lv_rowid);
               END;
               -- fin: Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
               cont_regi := cont_regi + 1;
               IF MOD(cont_regi,1000) = 0 THEN COMMIT;END IF;
               --
      END LOOP; -- FOR U IN regi_cv
      --
      commit; -- para los faltantes
      CLOSE regi_cv;
      --
      pvMensErr:= lvMensErr;
      --  
  EXCEPTION
    WHEN OTHERS THEN
         lvmenserr := SQLERRM;
                    INSERT INTO rp_BITACORA_TMP( tipo, fecha, msg_error, usuario, prg, comentario)
                                 VALUES ( 'EXCE', SYSDATE, lvMensErr, USER, 'rp_estadistica_facturacion_XX', 'Procesa x telefono: '|| lv_telefono || ' Plan: '||lv_plan|| 'Cuenta: '||lv_cuenta );
                    COMMIT;
  END;
  --
  ------------------------------------------------------------------------------------------------------
Procedure Guarda_resumen_MINUTOS ( pv_fechaFinal      VARCHAR2,
                                   pvMensErr   OUT VARCHAR2) IS
  --
  LvSentencia                      VARCHAR2(5000);
  LvSentencia2                     VARCHAR2(5000);
  LvSentenciaT                     VARCHAR2(10000);
  LvSelect                         VARCHAR2(5000);
  lvSent_Upd_MinuFactTari_1        VARCHAR2(5000);
  lvSent_Upd_MinuFactTari_2        VARCHAR2(5000);
  --
  source_cursor                    NUMBER;
  rows_processed                   NUMBER;
  cont                             NUMBER;
  --
  lv_plan                          VARCHAR2(60);
  lv_region                        VARCHAR2(4);
  lv_tecnologia                    VARCHAR2(4);
  lv_producto                      VARCHAR2(4);
  lv_tipo                          VARCHAR2(4);
  --
  lv_ap_totalmin		               NUMBER:= 0;
  lv_ap_free_min		               NUMBER:= 0;
  lv_ap_usd		                     NUMBER:= 0;
  lv_anp_totalmin		               NUMBER:= 0;
  lv_anp_free_min		               NUMBER:= 0;
  lv_anp_usd		                   NUMBER:= 0;
  lv_tll_usd		                   NUMBER:= 0;
  lv_tll_bill_min		               NUMBER:= 0;
  lv_tll_iar_usd		               NUMBER:= 0;
  lv_tll_iar_bill_min	             NUMBER:= 0;
  lv_tll_ier_usd		               NUMBER:= 0;
  lv_tll_ier_bill_min	             NUMBER:= 0;
  lv_tlli_usd		                   NUMBER:= 0;
  lv_tlli_bill_min	               NUMBER:= 0;
  lv_tri_usd                       NUMBER:= 0;
  lv_tri_bill_min                  NUMBER:= 0;
  lv_cv_usd		                     NUMBER:= 0;
  lv_cv_bill_min		               NUMBER:= 0;
  lv_llg_usd                       NUMBER:= 0;
--lv_llg_free_min                  NUMBER:= 0;
  lv_llg_bill_min                  NUMBER:= 0;
  --
  t_ap_phones	                     NUMBER:=0;
  t_ap_usd	                       NUMBER:=0;
  t_ap_bill_min	                   NUMBER:=0;
  t_ap_free_min	                   NUMBER:=0;
  t_ap_tota_minu	                 NUMBER:=0;
  --
  t_anp_phones	                   NUMBER:=0;
  t_anp_usd	                       NUMBER:=0;
  t_anp_bill_min	                 NUMBER:=0;
  t_anp_free_min	                 NUMBER:=0;
  t_anp_tota_minu	                 NUMBER:=0;
  --
  t_tll_usd                        NUMBER:= 0;
  t_tll_phones                     NUMBER:= 0;
  t_tll_bill_min                   NUMBER:= 0;
  t_tll_free_min                   NUMBER:= 0;
  t_tll_total_min                  NUMBER:= 0;
  --
  t_tll_iar_usd                    NUMBER:= 0;
  t_tll_iar_phones                 NUMBER:= 0;
  t_tll_iar_bill_min               NUMBER:= 0;
  t_tll_iar_free_min               NUMBER:= 0;
  t_tll_iar_total_min              NUMBER:= 0;
  --
  t_tll_ier_usd                    NUMBER:= 0;
  t_tll_ier_phones                 NUMBER:= 0;
  t_tll_ier_bill_min               NUMBER:= 0;
  t_tll_ier_free_min               NUMBER:= 0;
  t_tll_ier_total_min              NUMBER:= 0;
  --
  t_tlli_usd                       NUMBER:= 0;
  t_tlli_phones                    NUMBER:= 0;
  t_tlli_bill_min                  NUMBER:= 0;
  t_tlli_free_min                  NUMBER:= 0;
  t_tlli_total_min                 NUMBER:= 0;
  --
  t_tri_usd                        NUMBER:= 0;
  t_tri_phones                     NUMBER:= 0;
  t_tri_bill_min                   NUMBER:= 0;
  t_tri_free_min                   NUMBER:= 0;
  t_tri_total_min                  NUMBER:= 0;
  --
  t_cv_usd                         NUMBER:= 0;
  t_cv_phones                      NUMBER:= 0;
  t_cv_bill_min                    NUMBER:= 0;
  t_cv_free_min                    NUMBER:= 0;
  t_cv_total_min                   NUMBER:= 0;
  --
  t_llg_usd                        NUMBER:= 0;
  t_llg_phones                     NUMBER:= 0;
  t_llg_bill_min                   NUMBER:= 0;
  t_llg_free_min                   NUMBER:= 0;
  t_llg_total_min                  NUMBER:= 0;
  --
  t_eg_usd                         NUMBER:= 0;
  t_eg_phones                      NUMBER:= 0;
  t_eg_bill_min                    NUMBER:= 0;
  t_eg_free_min                    NUMBER:= 0;
  t_eg_totalmin                    NUMBER:= 0;
  --
  lv_tarifa_basica                 NUMBER:= 0;
  lv_cargo_a_paquete               NUMBER:= 0;
  t_cargo_a_paquete                NUMBER:= 0;
  t_tarifa_basica                  NUMBER:= 0;
  --
  lv_cargo_a_paquete_plan          NUMBER:= 0;
  lv_tarifa_basica_plan            NUMBER:= 0;
  t_cargo_a_paquete_plan           NUMBER:= 0;
  t_tarifa_basica_plan             NUMBER:= 0;
  --
  lv_anio                          VARCHAR2(4);
  lv_mes                           VARCHAR2(2);
  ln_tarifa                        NUMBER;
  --
  TYPE RegiCurTyp                  IS REF CURSOR;
  regi_cv                          REGICURTYP;
  --
  CURSOR regi_actualiza IS   SELECT F.ROWID, F.* FROM rp_MINUTOS_FACTURADOS_TARIFA  f ;
  --
BEGIN
BEGIN
    -- Actualiza la fecha del proceso
    lv_anio := substr(pv_fechaFinal,7,4);    lv_mes  := substr(pv_fechaFinal,4,2);
    -- Solo se volar�n los datos del mes si existiesen porque puede ser reprocesamiento lo que se haga.
    DELETE FROM rp_MINUTOS_FACTURADOS_TARIFA WHERE anio = lv_anio AND mes = lv_mes; COMMIT;
    -----------------------------------------------------------------------------------------------------------------
    --  CREO REGISTROS EN RP_MINUTOS_FACTURADOS_TARIFA
    LvSelect := 'insert /*+ append */ into RP_MINUTOS_FACTURADOS_TARIFA nologging (c_plan, producto, tipo, tecnologia, anio, mes) '||
                ' ( select distinct rtrim(c_plan) c_plan, producto, tipo, tecnologia, :1, :2 from rp_facturacion where telefono != '||''''||'1000000'||''''||' )';
    BEGIN
       EXECUTE IMMEDIATE LvSelect USING lv_anio, lv_mes; COMMIT;
    EXCEPTION
       WHEN OTHERS THEN
            pvMensErr := substr(pvMensErr||' '||SQLERRM,1,1000);
            GrabaBitacora( 'MINU', SYSDATE, pvMensErr, USER, 'RP_ESTADISTICA_FATURACION.GUARDA_RESUMEN_MINUTOS', ' INSERTANDO EN RP_MINUTOS_FACTURADOS_TARIFA ' );
    END;    
    -----------------------------------------------------------------------------------------------------------------
    -- Sentencia para actualizar registro en Minutos Facturados Tarifa
    lvSent_Upd_MinuFactTari_1:=' update rp_MINUTOS_FACTURADOS_TARIFA set       '||
                             '     ap_usd             =:1,  ap_phones          =:2,  ap_bill_min =    :3,  ap_free_min      = :4 ,'||
                             '     ap_totalmin        =:5,  anp_usd            =:6,  anp_phones  =    :7,  anp_bill_min     = :8 ,'||
                             '     anp_free_min       =:9,  anp_totalmin       =:10, eg_usd      =    :11, eg_phones        = :12,'||
                             '     eg_bill_min        =:13, eg_free_min        =:14, eg_totalmin =    :15, llg_usd          = :16,'||
                             '     llg_phones         =:17, llg_bill_min       =:18, llg_free_min=    :19, llg_totalmin     = :20,'||
                             '     tll_usd            =:21, tll_phones         =:22, tll_bill_min =   :23, tll_free_min     = :24,'||
                             '     tll_totalmin       =:25, tll_iar_usd        =:26, tll_iar_phones=  :27, tll_iar_bill_min = :28,'||
                             '     tll_iar_free_min   =:29, tll_iar_totalmin   =:30, tll_ier_usd   =  :31, tll_ier_phones   = :32,';
    lvSent_Upd_MinuFactTari_2:=                             
                             '     tll_ier_bill_min   =:33, tll_ier_free_min   =:34, tll_ier_totalmin=:35, tlli_usd         = :36,'||
                             '     tlli_phones        =:37, tlli_bill_min      =:38, tlli_free_min   =:39, tlli_totalmin    = :40,'||
                             '     tri_usd            =:41, tri_phones         =:42, tri_bill_min    =:43, tri_free_min     = :44,'||
                             '     tri_totalmin       =:45, cv_usd             =:46, cv_phones       =:47, cv_bill_min      = :48,'||
                             '     cv_free_min        =:49, cv_totalmin        =:50, cargo_a_paquete =:51, tarifa_basica    = :52,'||
                             '     acum_cargoa_paquete=:53, acum_tarifa_basica =:54'||
                             '   where  rowid         =:55';    
    -----------------------------------------------------------------------------------------------------------------
    LvSentencia := 'select  '||
                       '  to_number(decode(q1_consumo_aire_pico,null,0,q1_consumo_aire_pico))		    AP_totalMin, '||
                       '  to_number(decode(q1_consumo_aire_pico_g,null,0,q1_consumo_aire_pico_g))		AP_free_min, '||
                       '  to_number(decode(q_consumo_aire_pico,null,0,q_consumo_aire_pico))		      AP_USD, '||
                       '  to_number(decode(q1_consumo_aire_no_pico,null,0,q1_consumo_aire_no_pico))		    ANP_totalMin, '||
                       '  to_number(decode(q1_consumo_aire_no_pico_g,null,0,q1_consumo_aire_no_pico_g))		ANP_free_min, '||
                       '  to_number(decode(q_consumo_aire_no_pico,null,0,q_consumo_aire_no_pico))		      ANP_USD, '||
                       '  to_number(decode(q_interconexion_local,null,0,q_interconexion_local))		        TLL_USD,'||
                       '  to_number(decode(q1_interconexion_local,null,0,q1_interconexion_local))		      TLL_bill_min,'||
                       '  to_number(decode(q_inter__intra_regional,null,0,q_inter__intra_regional))		    TLL_IAR_USD,'||
                       '  to_number(decode(q1_inter__intra_regional,null,0,q1_inter__intra_regional))   	TLL_IAR_bill_min,';
    LvSelect :=                        
                       '  to_number(decode(q_inter__inter_regional,null,0,q_inter__inter_regional))		    TLL_IER_USD,'||
                       '  to_number(decode(q1_inter__inter_regional,null,0,q1_inter__inter_regional))	    TLL_IER_bill_min,'||
                       '  to_number(decode(q_inter__internacionales,null,0,q_inter__internacionales))	    TLLI_USD,'||
                       '  to_number(decode(q1_inter__internacionales,null,0,q1_inter__internacionales))	  TLLI_bill_min,'||
                       '  to_number(decode(q_roaming_internacional,null,0,q_roaming_internacional))		    TRI_USD,'||
                       '  to_number(decode(q1_roaming_internacional,null,0,q1_roaming_internacional))	    TRI_bill_min,'||
                       '  to_number(decode(q_casillero_de_voz,null,0,q_casillero_de_voz))		       CV_USD,'||
                       '  to_number(decode(q1_casillero_de_voz,null,0,q1_casillero_de_voz))		     CV_bill_min,';
    LvSentencia2 :=
                       '  to_number(decode(q_llamada_gratis,null,0,q_llamada_gratis))              LLG_USD, '||
                       '  to_number(decode(q1_llamada_gratis,null,0,q1_llamada_gratis))           LLG_bill_min, '||
                       '  to_number(decode(q_cargo_a_paquete,null,0,q_cargo_a_paquete))           acum_cargo_a_paquete, '||
                       '  to_number(decode(q_tarifa_basica_,null,0,q_tarifa_basica_))             acum_tarifa_basica, '||
                       '  to_number(decode(c_cargo_a_paquete,null,0,c_cargo_a_paquete))           acum_cargo_a_paquete_plan, '||
                       '  to_number(decode(c_tarifa_basica,null,0,c_tarifa_basica))             acum_tarifa_basica_plan '||
                       '  from    rp_facturacion '||
                       '  where  c_plan          = :1 '||
                       '  and    tecnologia      = :2 '||
                       '  and    producto        = :3 '||
                       '  and    tipo            = :4 ';
    LvSentenciaT := LvSentencia||' '||LvSelect || ' '||LvSentencia2;
    -----------------------------------------------------------------------------------------------------------------
    FOR I IN regi_actualiza LOOP
        -----------------------------------------------------------------------------------------------------------------
        BEGIN
             OPEN regi_cv FOR LvsentenciaT USING i.c_plan, i.tecnologia, i.producto, i.tipo;  -- open cursor variable
             LOOP
                 FETCH regi_cv INTO lv_ap_totalmin ,lv_ap_free_min ,lv_ap_usd ,lv_anp_totalmin  ,lv_anp_free_min ,lv_anp_usd,
                                    lv_tll_usd ,lv_tll_bill_min ,lv_tll_iar_usd ,lv_tll_iar_bill_min ,lv_tll_ier_usd , 
                                    lv_tll_ier_bill_min,lv_tlli_usd,lv_tlli_bill_min,lv_tri_usd ,lv_tri_bill_min ,lv_cv_usd ,
                                    lv_cv_bill_min ,lv_llg_usd,lv_llg_bill_min,lv_cargo_a_paquete ,lv_tarifa_basica,
                                    lv_cargo_a_paquete_plan,lv_tarifa_basica_plan;
                 EXIT WHEN regi_cv%NOTFOUND;
                 --
                 --  Para cargo_a_paquete y tarifa_basica ( OJO esto es por TELEFONO)-- lo que facturo realmente
                 t_cargo_a_paquete := t_cargo_a_paquete   + lv_cargo_a_paquete;
                 t_tarifa_basica   := t_tarifa_basica     + lv_tarifa_basica;
                 --
                 --  Para cargo_a_paquete y tarifa_basica ( OJO esto es por PLAN) -- valor del plan
                 t_cargo_a_paquete_plan := lv_cargo_a_paquete_plan;
                 t_tarifa_basica_plan   := lv_tarifa_basica_plan;
                 --
                 --  Para los Minutos Pico
                 --
                 --'  to_number(decode(q1_consumo_aire_pico,null,0,q1_consumo_aire_pico))		    AP_totalMin, '||
                 --'  to_number(decode(q1_consumo_aire_pico_g,null,0,q1_consumo_aire_pico_g))		AP_free_min, '||
                 --'  to_number(decode(q_consumo_aire_pico,null,0,q_consumo_aire_pico))		      AP_USD, '||                                    
                 if  lv_ap_totalmin > 0 or lv_ap_free_min > 0 then
                     --
                     t_ap_phones := t_ap_phones + 1;     t_ap_usd := t_ap_usd + lv_ap_usd;
                     --
                     if lv_ap_free_min > 0 then
                        -- restar para obtener lo cobrado adicionalmente
                        t_ap_bill_min  := t_ap_bill_min  + (lv_ap_totalmin -  lv_ap_free_min );
                        t_ap_free_min  := t_ap_free_min  + lv_ap_free_min;
                        t_ap_tota_minu := t_ap_tota_minu + lv_ap_totalmin;
                     else
                        t_ap_bill_min := 0; -- para AutoContro siempre vienen los minutos en lv_ap_totalmin y cero en los free_min
                        t_ap_free_min  := t_ap_free_min  + lv_ap_totalmin;
                        t_ap_tota_minu := t_ap_tota_minu + lv_ap_totalmin;
                     end if;
                     --
                 end if;
                 --
                 --  Para los Minutos NO Pico
                 --
                 --'  to_number(decode(q1_consumo_aire_no_pico,null,0,q1_consumo_aire_no_pico))		    ANP_totalMin, '||
                 --'  to_number(decode(q1_consumo_aire_no_pico_g,null,0,q1_consumo_aire_no_pico_g))		ANP_free_min, '||
                 --'  to_number(decode(q_consumo_aire_no_pico,null,0,q_consumo_aire_no_pico))		      ANP_USD, '||
                 if  lv_anp_totalmin > 0 or lv_anp_free_min > 0 then
                     --
                     t_anp_phones := t_anp_phones + 1;
                     t_anp_usd    := t_anp_usd    + lv_anp_usd;
                     --
                     if lv_anp_free_min > 0 then
                        -- restar para obtener lo cobrado adicionalmente
                        t_anp_bill_min  := t_anp_bill_min  + (lv_anp_totalmin -  lv_anp_free_min );
                        t_anp_free_min  := t_anp_free_min  + lv_anp_free_min;
                        t_anp_tota_minu := t_anp_tota_minu + lv_anp_totalmin;
                        --
                     else
                        t_anp_bill_min := 0; -- para AutoContro siempre vienen los minutos en lv_anp_totalmin y cero en los free_min
                        t_anp_free_min  := t_anp_free_min  + lv_anp_totalmin;
                        t_anp_tota_minu := t_anp_tota_minu + lv_anp_totalmin;
                     end if;
                     --
                 end if;                                            
                 
                 --
                 --  Para los Interconexion local
                 --
                 --'  to_number(decode(q_interconexion_local,null,0,q_interconexion_local))		        TLL_USD,'||
                 --'  to_number(decode(q1_interconexion_local,null,0,q1_interconexion_local))		      TLL_bill_min,'||
      
                 if  lv_tll_bill_min > 0 then
                     --
                     t_tll_usd       :=  t_tll_usd + lv_tll_usd;
                     t_tll_phones    :=  t_tll_phones + 1;
                     t_tll_bill_min  :=  t_tll_bill_min + lv_tll_bill_min;
                     t_tll_free_min  :=  0;
                     t_tll_total_min :=  t_tll_total_min + lv_tll_bill_min;
                     --
                 end if;                         
                 --
                 --  Para los Interconexion Intraregional 
                 --
                 --'  to_number(decode(q_inter__intra_regional,null,0,q_inter__intra_regional))		    TLL_IAR_USD,--'||
                 --'  to_number(decode(q1_inter__intra_regional,null,0,q1_inter__intra_regional))   	TLL_IAR_bill_min,--'||
                 --
                 if  lv_tll_iar_bill_min > 0 then
                     t_tll_iar_usd       :=  t_tll_iar_usd + lv_tll_iar_usd ;
                     t_tll_iar_phones    :=  t_tll_iar_phones + 1;
                     t_tll_iar_bill_min  :=  t_tll_iar_bill_min + lv_tll_iar_bill_min;
                     t_tll_iar_free_min  :=  0;
                     t_tll_iar_total_min :=  t_tll_iar_total_min + lv_tll_iar_bill_min;
                 end if;
                 --
                 --  Para los Interconexion Inter - regional 
                 --
                 --'  to_number(decode(q_inter__inter_regional,null,0,q_inter__inter_regional))		    TLL_IER_USD,--'||
                 --'  to_number(decode(q1_inter__inter_regional,null,0,q1_inter__inter_regional))	    TLL_IER_bill_min,--'||
                 --
                 if  lv_tll_ier_bill_min > 0 then
                     t_tll_ier_usd       :=  t_tll_ier_usd + lv_tll_ier_usd;
                     t_tll_ier_phones    :=  t_tll_ier_phones + 1;
                     t_tll_ier_bill_min  :=  t_tll_ier_bill_min + lv_tll_ier_bill_min;
                     t_tll_ier_free_min  :=  0;
                     t_tll_ier_total_min :=  t_tll_ier_total_min + lv_tll_ier_bill_min;
                 end if;
                 --
                 --  Para los internacionales
                 --
                 --'  to_number(decode(q_inter__internacionales,null,0,q_inter__internacionales))	    TLLI_USD,--'||
                 --'  to_number(decode(q1_inter__internacionales,null,0,q1_inter__internacionales))	  TLLI_bill_min,--'||
                 --
                 if  lv_tlli_bill_min > 0 then
                     t_tlli_usd       :=  t_tlli_usd + lv_tlli_usd;
                     t_tlli_phones    :=  t_tlli_phones + 1;
                     t_tlli_bill_min  :=  t_tlli_bill_min + lv_tlli_bill_min;
                     t_tlli_free_min  :=  0;
                     t_tlli_total_min :=  t_tlli_total_min + lv_tlli_bill_min;
                 end if;
                 --
                 --  Para los roaming internacionales
                 --
                 --'  to_number(decode(q_roaming_internacional,null,0,q_roaming_internacional))		    TRI_USD,--'||
                 --'  to_number(decode(q1_roaming_internacional,null,0,q1_roaming_internacional))	    TRI_bill_min,--'||
                 --
                 if  lv_tri_bill_min > 0 then
                     t_tri_usd       :=  t_tri_usd + lv_tri_usd;
                     t_tri_phones    :=  t_tri_phones + 1;
                     t_tri_bill_min  :=  t_tri_bill_min + lv_tri_bill_min;
                     t_tri_free_min  :=  0;
                     t_tri_total_min :=  t_tri_total_min + lv_tri_bill_min;
                 end if;
                 --
                 --  Para los casilleros de voz
                 --
                 --'  to_number(decode(q_casillero_de_voz,null,0,q_casillero_de_voz))		       CV_USD,--'||
                 --'  to_number(decode(q1_casillero_de_voz,null,0,q1_casillero_de_voz))		     CV_bill_min--'||
                 --
                 if  lv_cv_bill_min > 0 then
                     t_cv_usd       :=  t_cv_usd + lv_cv_usd;
                     t_cv_phones    :=  t_cv_phones + 1;
                     t_cv_bill_min  :=  t_cv_bill_min + lv_cv_bill_min;
                     t_cv_free_min  :=  0;
                     t_cv_total_min :=  t_cv_total_min + lv_cv_bill_min;
                 end if;
                 --
                 --  Para llamada_gratis
                 --
                 --'  to_number(decode(q_llamada_gratis,null,0,q_llamada_gratis))              LLG_USD --'||
                 --'  to_number(decode(q1_llamada_gratis,null,0,q1_llamada_gratis))           LLG_free_min --'||
                 --
                 if  lv_llg_bill_min > 0 then
                     t_llg_usd       :=  t_llg_usd + lv_llg_usd;
                     t_llg_phones    :=  t_llg_phones + 1;
                     t_llg_bill_min  :=  t_llg_bill_min + lv_llg_bill_min;
                     t_llg_free_min  :=  0;
                     t_llg_total_min :=  t_llg_total_min + lv_llg_bill_min;
                 end if;
                 --
           END LOOP; -- FOR U IN regi_cv
           COMMIT;
           CLOSE regi_cv;
        END;
        -----------------------------------------------------------------------------------------------------------------
        --
        -- Actualizo el registro
        BEGIN
            EXECUTE IMMEDIATE lvSent_Upd_MinuFactTari_1||' '||lvSent_Upd_MinuFactTari_2 USING 
                                    t_ap_usd,           t_ap_phones,         t_ap_bill_min,          t_ap_free_min, 
                                    t_ap_tota_minu,     t_anp_usd,           t_anp_phones,           t_anp_bill_min, 
                                    t_anp_free_min,     t_anp_tota_minu,     0,                      0,
                                    0,                  0,                   0,                      t_llg_usd, 
                                    t_llg_phones,       t_llg_bill_min,      t_llg_free_min,         t_llg_total_min, 
                                    t_tll_usd,          t_tll_phones,        t_tll_bill_min,         t_tll_free_min, 
                                    t_tll_total_min,    t_tll_iar_usd,       t_tll_iar_phones,       t_tll_iar_bill_min,
                                    t_tll_iar_free_min, t_tll_iar_total_min, t_tll_ier_usd,          t_tll_ier_phones, 
                                    t_tll_ier_bill_min, t_tll_ier_free_min,  t_tll_ier_total_min,    t_tlli_usd, 
                                    t_tlli_phones,      t_tlli_bill_min,     t_tlli_free_min,        t_tlli_total_min,
                                    t_tri_usd,          t_tri_phones,        t_tri_bill_min,         t_tri_free_min,
                                    t_tri_total_min,    t_cv_usd,            t_cv_phones ,           t_cv_bill_min, 
                                    t_cv_free_min,      t_cv_total_min,      t_cargo_a_paquete_plan, t_tarifa_basica_plan, 
                                    t_cargo_a_paquete,  t_tarifa_basica,     i.ROWID;
        EXCEPTION
            WHEN OTHERS THEN
                 pvMensErr := substr(pvMensErr||' '||SQLERRM,1,1000);
                 GrabaBitacora( 'MINU', SYSDATE, pvMensErr, USER, 'RP_ESTADISTICA_FATURACION.GUARDA_RESUMEN_MINUTOS', ' ERROR... ' ||' rowid: ' || i.rowid);
        END;
        COMMIT;
        --
        -- Encero variables
        t_ap_phones	    :=0;    t_ap_usd	      :=0;        t_ap_bill_min	  :=0;        t_ap_free_min	  :=0;        t_ap_tota_minu	:=0;
        t_anp_phones	  :=0;    t_anp_usd	      :=0;        t_anp_bill_min	  :=0;        t_anp_free_min	  :=0;        t_anp_tota_minu	  :=0;
        --
        t_tll_usd          :=0; t_tll_phones    :=0; t_tll_bill_min     :=0; t_tll_free_min     :=0; t_tll_total_min     :=0;
        t_tll_iar_usd      :=0; t_tll_iar_phones:=0; t_tll_iar_bill_min :=0; t_tll_iar_free_min :=0; t_tll_iar_total_min :=0;
        t_tll_ier_usd      :=0; t_tll_ier_phones:=0; t_tll_ier_bill_min :=0; t_tll_ier_free_min :=0; t_tll_ier_total_min :=0;
        t_tlli_usd         :=0; t_tlli_phones   :=0; t_tlli_bill_min    :=0; t_tlli_free_min    :=0; t_tlli_total_min    :=0;
        t_tri_usd          :=0; t_tri_phones    :=0; t_tri_bill_min     :=0; t_tri_free_min     :=0; t_tri_total_min     :=0;
        t_cv_usd           :=0; t_cv_phones     :=0; t_cv_bill_min      :=0; t_cv_free_min      :=0; t_cv_total_min      :=0;
        t_llg_usd          :=0; t_llg_phones    :=0; t_llg_bill_min     :=0; t_llg_free_min     :=0; t_llg_total_min     :=0;
        t_eg_usd           :=0; t_eg_phones     :=0; t_eg_bill_min      :=0; t_eg_free_min      :=0; t_eg_totalmin       :=0;
        --
        t_tarifa_basica    :=0; t_cargo_a_paquete :=0; t_tarifa_basica_plan :=0; t_cargo_a_paquete_plan :=0;
        --
    END LOOP;  
    --

EXCEPTION
    WHEN OTHERS THEN
         pvMensErr := substr(pvMensErr||' '||SQLERRM,1,1000);
         GrabaBitacora( 'MINU', SYSDATE, pvMensErr, USER, 'RP_ESTADISTICA_FATURACION.GUARDA_RESUMEN_MINUTOS', ' ERROR... ' );
         COMMIT;
END;
END;  -- FIN procedimiento
  ------------------------------------------------------------------------------
  -- Antes se ten�a incluida la REGION ahora ya no es necesario JUNIO_2005
  ------------------------------------------------------------------------------
  --  ESto es para el resumen total por  anio, mes, tecnologia, producto
  --
  PROCEDURE Guarda_total_minuto_factura ( pv_fechaFinal      varchar2,
                                        pvMensErr      out varchar2) IS
    ----
    --
    lvsentencia                      varchar2(10000):=null;
    lvsentencia1                     varchar2(10000):=null;
    lvmenserr                        varchar2(1000) :=null;
    LvSelect                         varchar2(500);
    lv_telefono                      varchar2(24);
    lv_anio                          varchar2(4);
    lv_mes                           varchar2(2);
    --
    cont                             number:= 0;
    vn_registros_grabar              number:=1000;
    source_cursor                    number;
    --
    rows_processed                   integer;
    --
    Cursor regi is
          select --c_plan,
          anio, mes, tecnologia, producto,
          sum(nvl(ap_usd,0))        ap_usd,
          sum(nvl(ap_phones,0))     ap_phones,
          sum(nvl(ap_bill_min,0))   ap_bill_min,
          sum(nvl(ap_free_min,0))   ap_free_min,
          sum(nvl(ap_totalmin,0))   ap_totalmin,
          --
          sum(nvl(anp_usd,0))       anp_usd,
          sum(nvl(anp_phones,0))    anp_phones,
          sum(nvl(anp_bill_min,0))  anp_bill_min,
          sum(nvl(anp_free_min,0))  anp_free_min,
          sum(nvl(anp_totalmin,0))  anp_totalmin,
          --
          sum(nvl(eg_usd,0))        eg_usd,
          sum(nvl(eg_phones,0))     eg_phones,
          sum(nvl(eg_bill_min,0))   eg_bill_min,
          sum(nvl(eg_free_min,0))   eg_free_min,
          sum(nvl(eg_totalmin,0))   eg_totalmin,
          --
          sum(nvl(llg_usd,0))        llg_usd,
          sum(nvl(llg_phones,0))     llg_phones,
          sum(nvl(llg_bill_min,0))   llg_bill_min,
          sum(nvl(llg_free_min,0))   llg_free_min,
          sum(nvl(llg_totalmin,0))   llg_totalmin,
          --
          sum(nvl(tll_usd,0))        tll_usd,
          sum(nvl(tll_phones,0))     tll_phones,
          sum(nvl(tll_bill_min,0))   tll_bill_min,
          sum(nvl(tll_free_min,0))   tll_free_min,
          sum(nvl(tll_totalmin,0))   tll_totalmin,
          --
          sum(nvl(tll_iar_usd,0))    tll_iar_usd,
          sum(nvl(tll_iar_phones,0))   tll_iar_phones,
          sum(nvl(tll_iar_bill_min,0))   tll_iar_bill_min,
          sum(nvl(tll_iar_free_min,0))   tll_iar_free_min,
          sum(nvl(tll_iar_totalmin,0))   tll_iar_totalmin,
          --
          sum(nvl(tll_ier_usd,0))   tll_ier_usd,
          sum(nvl(tll_ier_phones,0))   tll_ier_phones,
          sum(nvl(tll_ier_bill_min,0))   tll_ier_bill_min,
          sum(nvl(tll_ier_free_min,0))   tll_ier_free_min,
          sum(nvl(tll_ier_totalmin,0))   tll_ier_totalmin,
          --
          sum(nvl(tlli_usd,0))   tlli_usd,
          sum(nvl(tlli_phones,0))   tlli_phones,
          sum(nvl(tlli_bill_min,0))   tlli_bill_min,
          sum(nvl(tlli_free_min,0))   tlli_free_min,
          sum(nvl(tlli_totalmin,0))   tlli_totalmin,
          --
          sum(nvl(tri_usd,0))   tri_usd,
          sum(nvl(tri_phones,0))   tri_phones,
          sum(nvl(tri_bill_min,0))   tri_bill_min,
          sum(nvl(tri_free_min,0))   tri_free_min,
          sum(nvl(tri_totalmin,0))   tri_totalmin,
          --
          sum(nvl(cv_usd,0))   cv_usd,
          sum(nvl(cv_phones,0))   cv_phones,
          sum(nvl(cv_bill_min,0))   cv_bill_min,
          sum(nvl(cv_free_min,0))   cv_free_min,
          sum(nvl(cv_totalmin,0))   cv_totalmin,
          sum(nvl(tarifa_basica,0)) cv_tarifa_basica,
          sum(nvl(cargo_a_paquete,0)) cv_cargo_a_paquete
          from rp_minutos_facturados_tarifa t
          where t.anio   =  lv_anio
          and   t.mes    =  lv_mes
          group by anio, mes, tecnologia, producto
          ;
    --
  BEGIN
     -- Actualiza la fecha del proceso
     lv_anio := substr(pv_fechaFinal,7,4);
     lv_mes  := substr(pv_fechaFinal,4,2);
     --
     delete rp_minu_factura_tarifa_total where anio = lv_anio and mes = lv_mes;
     commit;
     --  
     For i in regi
     Loop
         --
         lvSentencia := ' insert /*+ append */ into rp_minu_factura_tarifa_total ('||
                      ' anio,mes, tecnologia, ap_usd, ap_phones, ap_bill_min, ap_free_min, ap_totalmin, '||
                      ' anp_usd, anp_phones, anp_bill_min, anp_free_min, anp_totalmin, '||
                      ' eg_usd, eg_phones, eg_bill_min, eg_free_min, eg_totalmin, '||
                      ' llg_usd, llg_phones, llg_bill_min, llg_free_min, llg_totalmin, '||
                      ' tll_usd, tll_phones, tll_bill_min, tll_free_min, tll_totalmin, '||
                      ' tll_iar_usd, tll_iar_phones, tll_iar_bill_min, tll_iar_free_min, tll_iar_totalmin, '||
                      ' tll_ier_usd, tll_ier_phones, tll_ier_bill_min, tll_ier_free_min, tll_ier_totalmin, '||
                      ' tlli_usd, tlli_phones, tlli_bill_min, tlli_free_min, tlli_totalmin, '||
                      ' tri_usd, tri_phones, tri_bill_min, tri_free_min, tri_totalmin, '||
                      ' cv_usd, cv_phones, cv_bill_min, cv_free_min, cv_totalmin, producto '||
                        ' ) '||
                        '                 values('
                        ;                       
        LvSentencia1:=
                        ''''||i.anio||''''||','||
                        ''''||i.mes||''''||','|| 
                        ''''||i.tecnologia||''''||','|| 
                        i.ap_usd||','|| 
                        i.ap_phones||','|| 
                        i.ap_bill_min||','|| 
                        i.ap_free_min||','|| 
                        i.ap_totalmin||','|| 
                        i.anp_usd||','|| 
                        i.anp_phones||','|| 
                        i.anp_bill_min||','|| 
                        i.anp_free_min||','|| 
                        i.anp_totalmin||','|| 
                        i.eg_usd||','|| 
                        i.eg_phones||','|| 
                        i.eg_bill_min||','|| 
                        i.eg_free_min||','|| 
                        i.eg_totalmin||','|| 
                        i.llg_usd||','|| 
                        i.llg_phones||','|| 
                        i.llg_bill_min||','|| 
                        i.llg_free_min||','|| 
                        i.llg_totalmin||','|| 
                        i.tll_usd||','|| 
                        i.tll_phones||','|| 
                        i.tll_bill_min||','|| 
                        i.tll_free_min||','|| 
                        i.tll_totalmin||','|| 
                        i.tll_iar_usd||','|| 
                        i.tll_iar_phones||','|| 
                        i.tll_iar_bill_min||','|| 
                        i.tll_iar_free_min||','|| 
                        i.tll_iar_totalmin||','|| 
                        i.tll_ier_usd||','|| 
                        i.tll_ier_phones||','|| 
                        i.tll_ier_bill_min||','|| 
                        i.tll_ier_free_min||','|| 
                        i.tll_ier_totalmin||','|| 
                        i.tlli_usd||','|| 
                        i.tlli_phones||','|| 
                        i.tlli_bill_min||','|| 
                        i.tlli_free_min||','|| 
                        i.tlli_totalmin||','|| 
                        i.tri_usd||','|| 
                        i.tri_phones||','|| 
                        i.tri_bill_min||','|| 
                        i.tri_free_min||','|| 
                        i.tri_totalmin||','|| 
                        i.cv_usd||','|| 
                        i.cv_phones||','|| 
                        i.cv_bill_min||','|| 
                        i.cv_free_min||','|| 
                        i.cv_totalmin||','||
                        ''''||i.producto||''''||
                        ') ';
         EJECUTA_SENTENCIA(lvSentencia||' '||LvSentencia1, lvMensErr);
         --
         IF MOD(cont, vn_registros_grabar)= 0 THEN COMMIT; END IF;
         --
     END LOOP;
     COMMIT;
     --
     pvMensErr:= lvMensErr;
     --  
  EXCEPTION
    WHEN OTHERS THEN
         NULL;
  END;
  --
  ------------------------------------------------------------------------------
  --
    Procedure  FacturacionSaliente (pv_Fechafinal     varchar2,
                                    pvMens_Error      out varchar2 ) is
    --
    lvSentencia                  varchar2(10000);
    lvSentencia_Insert           varchar2(5000);
    lvSentencia2                 varchar2(5000);
    vl_cont                      number;
    --
    lv_plan                      varchar2(60);
    lv_tipo                      varchar2(10);
    lv_tecnologia                varchar2(4);
    lv_producto                  varchar2(4);
    --
    lv_anio                      varchar2(4);
    lv_mes                       varchar2(2);
    --
    ln_gye_clientes	             number:=0;
    ln_gye_minu_pico             number:=0;
    ln_gye_minu_pico_grat        number:=0;
    ln_gye_dola_pico             number:=0;
    --
    ln_gye_minu_no_pico          number:=0;
    ln_gye_minunopico_grat       number:=0;
    ln_gye_dolar_no_pico         number:=0;
    --
    ln_uio_clientes	             number:=0;
    ln_uio_minu_pico             number:=0;
    ln_uio_minu_pico_grat        number:=0;
    ln_uio_dola_pico             number:=0;
    --
    ln_uio_minu_no_pico          number:=0;
    ln_uio_minunopico_grat       number:=0;
    ln_uio_dolar_no_pico         number:=0;
    --
    ln_tarifa_basica             number:=0;
    ln_acum_tarifa_Basica        number:=0;
    ln_cargo_a_paquete           number:=0;
    ln_Acum_cargo_a_paquete      number:=0;
    --
    Cursor crea_regi is
           select distinct c_plan, tipo, tecnologia, producto from rp_minutos_facturados_tarifa ;
    --
    cursor  regi_leo is
            select *
            from  rp_minutos_facturados_tarifa r 
            where c_plan     = lv_plan--'AC GSM  20 - A'
            and   tecnologia = lv_tecnologia --'GSM'
            and   producto   = lv_producto--'AUT'
            and   tipo       = lv_tipo --'A'
            ;
    --
    CURSOR factu_sali is
           SELECT X.ROWID, X.* FROM rp_facturacion_saliente x 
           WHERE   x.anio =  lv_anio 
           AND     x.mes  =  lv_mes;
    --
    BEGIN
      --
      -- Actualiza la fecha del proceso
      lv_anio := substr(pv_fechaFinal,7,4);
      lv_mes  := substr(pv_fechaFinal,4,2);
      --
      DELETE rp_facturacion_saliente where anio = lv_anio and mes =lv_mes;
      COMMIT;
      --
      FOR I IN crea_regi LOOP
          --
          lvSentencia_Insert:=  
                   ' insert /*+ append */ into rp_facturacion_saliente (anio, mes, producto, plan, tipo, tecnologia) '||
                   '        values('||''''||Lv_anio||''''||','||''''||lv_mes||''''||','||''''||i.producto||''''||','||''''||i.c_plan||''''||','||
                   '                '''||i.tipo||''''||','||''''||i.tecnologia||''''||')'
                   ;
          EJECUTA_SENTENCIA(lvSentencia_Insert, pvMens_Error);
          --
      END LOOP;
      COMMIT;
      --
      --  Actualizo rp_facturacion_saliente
      FOR i IN factu_sali LOOP
      --
            lv_plan     := i.plan;
            lv_producto := i.producto;
            lv_plan     := i.plan;
            lv_tecnologia:=i.tecnologia;
            lv_tipo      := i.tipo;
            --
            ln_gye_clientes 	    := 0;
            ln_gye_minu_pico 	    := 0;
            ln_gye_minu_pico_grat := 0;
            ln_gye_dola_pico      := 0;
            --        
            ln_gye_minu_no_pico   := 0;
            ln_gye_minunopico_grat:= 0;
            ln_gye_dolar_no_pico  := 0;
            --
            ln_uio_clientes       := 0;
            ln_uio_minu_pico      := 0;
            ln_uio_minu_pico_grat := 0;
            ln_uio_dola_pico 	    := 0;
            --        
            ln_uio_minu_no_pico   := 0;
            ln_uio_minunopico_grat:= 0;
            ln_uio_dolar_no_pico  := 0;
            --
            FOR K IN regi_leo LOOP
                IF    k.region = 'COS' THEN
                      --
                      ln_gye_clientes :=              k.ap_phones; -- porque este contiene a los no pico
                      ln_gye_minu_pico :=             k.ap_bill_min;  
                      ln_gye_minu_pico_grat   :=      k.ap_free_min;
                      ln_gye_dola_pico :=             k.ap_usd;
                      --        
                      ln_gye_minu_no_pico :=           k.anp_bill_min;
                      ln_gye_minunopico_grat   :=      k.anp_free_min;
                      ln_gye_dolar_no_pico :=          k.anp_usd;
                ELSE
                      --
                      ln_uio_clientes :=              k.ap_phones; -- porque este contiene a los no pico
                      ln_uio_minu_pico :=             k.ap_bill_min;  
                      ln_uio_minu_pico_grat   :=      k.ap_free_min;
                      ln_uio_dola_pico :=             k.ap_usd;
                      --        
                      ln_uio_minu_no_pico :=           k.anp_bill_min;
                      ln_uio_minunopico_grat   :=      k.anp_free_min;
                      ln_uio_dolar_no_pico :=          k.anp_usd;
                      --
                END IF;
                ln_tarifa_basica        :=       k.tarifa_basica;
                ln_acum_tarifa_Basica   :=       k.acum_tarifa_basica;
                ln_cargo_a_paquete      :=       k.cargo_a_paquete;
                ln_Acum_cargo_a_paquete :=       k.acum_cargoa_paquete;
             --
             END LOOP;
             --
             --  acumulo para actualizar una sola el registro.
             --
             update rp_facturacion_saliente set
             --
                  gye_clientes                   = ln_gye_clientes,
                  gye_minu_pico                  = ln_gye_minu_pico,
                  gye_minu_pico_gratis           = ln_gye_minu_pico_grat,
                  gye_dola_pico                  = ln_gye_dola_pico,
                  gye_minu_no_pico               = ln_gye_minu_no_pico,
                  gye_minu_no_pico_gratis        = ln_gye_minunopico_grat,
                  gye_dolar_no_pico              = ln_gye_dolar_no_pico,
                  --
                  uio_clientes                   = ln_uio_clientes,
                  uio_minu_pico                  = ln_uio_minu_pico,
                  uio_minu_pico_gratis           = ln_uio_minu_pico_grat,
                  uio_dola_pico                  = ln_uio_dola_pico,
                  uio_minu_no_pico               = ln_uio_minu_no_pico,
                  uio_minu_no_pico_gratis        = ln_uio_minunopico_grat,
                  uio_dolar_no_pico              = ln_uio_dolar_no_pico,
                  --
                  nac_cargo_paquete              = ln_cargo_a_paquete,
                  fac_cargo_paquete              = ln_Acum_cargo_a_paquete,
                  nac_tari_basi                  = ln_tarifa_basica,
                  fac_tari_basi                  = ln_acum_tarifa_Basica
             --
             where rowid = i.rowid;
             commit;
       --
       End Loop;    
       --
       -- actualiza los totales de NAC
       --
       update rp_facturacion_saliente c
       set    c.nac_clientes            =   nvl(c.gye_clientes,0)  + nvl(c.uio_clientes,0),
               c.nac_minu_pico           =   nvl(c.gye_minu_pico,0) + nvl(c.uio_minu_pico,0),
               c.nac_minu_pico_gratis    =   nvl(c.gye_minu_pico_gratis,0) + nvl(c.uio_minu_pico_gratis,0),
               c.nac_dola_pico           =   nvl(c.gye_dola_pico,0)        + nvl(c.uio_dola_pico,0),
               c.nac_minu_no_pico        =   nvl(c.gye_minu_no_pico,0)     + nvl(c.uio_minu_no_pico,0),
               c.nac_minu_no_pico_gratis =   nvl(c.gye_minu_no_pico_gratis,0)+nvl(c.uio_minu_no_pico_gratis,0),
               c.nac_dolar_no_pico       =   nvl(c.gye_dolar_no_pico,0)    + nvl(c.uio_dolar_no_pico,0)
       ;
       commit;
       --
       -- actualiza los Porcentajes
       --
       update rp_facturacion_saliente c
       set    c.nac_mou               =   (nac_minu_pico + nac_minu_pico_gratis + nac_minu_no_pico + nac_minu_no_pico_gratis)/nac_clientes,
               c.nac_arpu              =   (nac_dola_pico + nac_dolar_no_pico)/nac_clientes,
               c.nac_ipm               =   (nac_dola_pico + nac_dolar_no_pico)/ (nac_minu_pico + nac_minu_pico_gratis + nac_minu_no_pico + nac_minu_no_pico_gratis)
       where  anio                    =   lv_anio
       and    mes                     =   lv_mes
       and    nac_dola_pico > 0 
       ;
       commit;
    End;
--------------------------------------------------------------------------------------------------
  --
  PROCEDURE MAIN  IS
    --
    -- variables
    --
    source_cursor                 integer;
    rows_processed                integer;
    rows_fetched                  integer;
    --
    lnexistetabla                 number;
    lnnumerr                      number;
    lnexito                       number;
    --
    lvmenserr                     varchar2(3000);
    lv_sentencia                  varchar2(32000);
    lv_fechaBase                  varchar2(10);
    lv_fechaInicial               varchar2(10);
    lv_fechaFinal                 varchar2(10);
    lv_fechaInicial_2             varchar2(10);
    lv_fechaFinal_2               varchar2(10);
    --
    lv_tabla                      varchar2(30):='RP_FACTURACION'; -- siempre en mayuscula
  
  BEGIN
     --
     -- OBTIENE FECHAS DEL PERIODO Y DEL MES
     --
     lv_fechaBase := to_char(sysdate,'yyyymmdd');  
     Obtiene_fechas_periodo(lv_fechaBase, lv_fechaInicial, lv_fechaFinal, lvmenserr);
     Obtiene_fechas(lv_fechaBase, lv_fechaInicial_2, lv_fechaFinal_2, lvmenserr);
     --
     --
     --  Depura Registros IVA e ICE, quita espacios
     
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Depuracion_IVA_ICE_y_dupli(lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Depuracion_IVA_ICE_y_dupli', ' FINO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));    
     commit;
     
     --
     -- Crea Tabla y Registros
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Crea Tabla y Registros', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     -- Busca si existe .
     lv_Sentencia   := 'select count(*) from user_all_tables where table_name = '||''''||lv_tabla||'''';
     source_cursor  := dbms_sql.open_cursor;                  -- ABRIR CURSOR DE SQL DINAMICO
     dbms_sql.parse(source_cursor, lv_Sentencia, 2);          -- EVALUAR CURSOR (obligatorio) (2 es constante)
     dbms_sql.define_column(source_cursor, 1, lnExisteTabla); -- DEFINIR COLUMNA
     rows_processed := dbms_sql.execute(source_cursor);       -- EJECUTAR COMANDO DINAMICO
     rows_fetched   := dbms_sql.fetch_rows(source_cursor);    -- EXTRAIGO LAS FILAS
     dbms_sql.column_value(source_cursor, 1, lnExisteTabla);  -- RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
     dbms_sql.close_cursor(source_cursor);                    -- CIERRAS CURSOR
     --
     -- Si existe la tabla Se dropea
     IF lnExisteTabla = 1 THEN EXECUTE IMMEDIATE 'drop table  '||lv_tabla; END IF;
     --
     -- creamos la tabla de acuerdo a los servicios del periodo
     lnExito := crea_tabla(lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Crea Tabla', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     commit;
     --
     -- Se crean registros segun telefonos
     --
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Crea Tabla y Registros', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     lnExito := Crea_Registros(lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Crea Tabla y Registros', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     commit;
     --
     -- Actualiza todos los valores segun el telefono
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Procesa_x_Telefono', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Procesa_x_Telefono (lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Procesa_x_Telefono', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;

     --
     -- Actualizo la tabla
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Datos_AXIS', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Datos_AXIS;
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Datos_AXIS', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
    
     --
     -- Llena Datos 
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Llena_DAtos', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Actualiza_DatosPlanes(lv_tabla);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Llena_DAtos', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;

/* 
     --
     -- Para MINUTOS FACTURADOS
     --
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Guarda_Resumen_Minutos y Total', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Guarda_Resumen_Minutos (lv_fechaFinal,lvMensErr);
  
     Guarda_total_minuto_factura(lv_fechaFinal, lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Guarda_Resumen_Minutos y Total', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;

     --
     --  Guarda para reporte de FACTURACION SALIENTE POR producto y tecnologia
     --
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Facturacionsaliente', ' INICIO '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     Facturacionsaliente(lv_fechaFinal, lvMensErr);
     GrabaBitacora( 'FACT', sysdate, lvmenserr, user, 'rp_estadistica_facturacion_XX.Facturacionsaliente', ' FIN '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
     
     COMMIT;
*/
     --
   END MAIN;
------------------------------------------------------------------------------------
--  Se cambio, este OK.  JGO ->  Junio 21 del 2005
PROCEDURE DATOS_AXIS IS
lvmenserr                     varchar2(3000);
--
BEGIN
    --
    --  Encera para llenar con datos desde AXIS
    EXECUTE IMMEDIATE 'Truncate table rp_planes_axis_bscs';
    BEGIN
        INSERT /* APPEND */ INTO rp_planes_axis_bscs NOLOGGING
        (   SELECT bp.cod_bscs, bp.id_clase tecno_1, bp.id_detalle_plan,
                   gd.id_plan plan ,  gd.id_subproducto subproducto, gd.id_clase tecno_2
                   ,gp.descripcion,gp.tarifabasica, gp.cargoapaquete,gp.tipo, gp.costo_min_pico, gp.costo_min_no_pico,gp.min_incluidos
            FROM   bs_planes@axis bp , ge_detalles_planes@axis  gd, cp_planes@axis gp
            WHERE  bp.id_detalle_plan   = gd.id_detalle_plan 
            AND    gd.id_plan           = gp.id_plan );
        COMMIT;
    EXCEPTION
       WHEN OTHERS THEN
            lvmenserr := SQLERRM;
            GrabaBitacora( 'AXIS', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion_XX.DATOS_AXIS', ' ERROR '|| to_char(sysdate,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
END;
------------------------------------------------------------------------------------
PROCEDURE Retorna_Informacion_PLAN ( pv_plan          VARCHAR2,
                                     pv_tipo          OUT VARCHAR2,
                                     pv_subproducto   OUT VARCHAR2,
                                     pv_tecno_1       OUT VARCHAR2,
                                     pv_tecno_2       OUT VARCHAR2,
                                     pv_tarifabasica  OUT VARCHAR2,
                                     pv_cargoapaquete OUT VARCHAR2 ) IS
BEGIN
   SELECT tipo, t.subproducto, tecno_1, tecno_2, tarifabasica, cargoapaquete
   INTO   pv_tipo, pv_subproducto, pv_tecno_1, pv_tecno_2, pv_tarifabasica, pv_cargoapaquete
   FROM   rp_planes_axis_bscs t, rateplan r
   WHERE  t.cod_bscs    = r.tmcode
   AND    rtrim(r.des)  = pv_plan 
   and rownum = 1; --AC GSM 105 AAA'
EXCEPTION
   WHEN NO_DATA_FOUND THEN
        pv_tipo         := '';
        pv_subproducto  := '';
        pv_tecno_1      := '';
        pv_tecno_2      := '';
        pv_tarifabasica := '';
        pv_cargoapaquete:= '';
        GrabaBitacora( 'FACT', SYSDATE, '', USER, 'rp_estadistica_facturacion_XX.Retorna_Informacion_PLAN', 'No encuentra en tabla rp_planes_axis_bscs o rateplan el plan: '||pv_plan);
END;
------------------------------------------------------------------------------------
PROCEDURE Actualiza_DatosPlanes (pv_tabla  VARCHAR2) IS
    --
    lvsentencia                      VARCHAR2(10000):=NULL;
    lvsentencia1                     VARCHAR2(100)  :=NULL;
    lvsentencia2                     VARCHAR2(20000):=NULL;
    lvsentencia3                     VARCHAR2(100)  :=NULL;
    lv_cadena                        VARCHAR2(20000):=NULL;    
    lvmenserr                        VARCHAR2(1000) :=NULL;
    LvSelect                         VARCHAR2(500);
    --
    cont_regi                        NUMBER:= 0;
    vn_registros_grabar              NUMBER:= 50;
    source_cursor                    NUMBER;
    --
    rows_processed                   INTEGER;
    --
    TYPE     RegiCurTyp IS REF CURSOR;
    regi_cv  RegiCurTyp;
    --  variables del cursor 1
    lv_tecno_1                       VARCHAR2(4);
    lv_tecno_2                       VARCHAR2(4);
    lv_telefono                      VARCHAR2(24);
    lv_plan                          VARCHAR2(60);
    lv_tipo                          VARCHAR2(4);
    lv_subproducto                   VARCHAR2(4);
    lv_tarifabasica                  NUMBER(13,2);
    lv_cargoapaquete                 NUMBER(13,2);
    --
BEGIN
      --
      LvSentencia1 := ' Update '|| pv_tabla ||' set tecnologia = :1, producto = :2, tipo = :3  Where c_plan = :4 ';
      --
      --  Por cada registro del plan, busco su info y actualizo en RP_FACTURACION
      --
      OPEN regi_cv FOR 'select distinct c_plan from '||pv_tabla|| ' where  telefono != '||''''||'1000000'||''''||' and tecnologia is null ' ; -- open cursor variable
      LOOP
        cont_regi   := 0;
        FETCH  regi_cv INTO lv_plan;
        EXIT WHEN regi_cv%NOTFOUND;  -- si no datos, sale
               Retorna_Informacion_PLAN (lv_plan, lv_tipo, lv_subproducto, lv_tecno_1, lv_tecno_2, lv_tarifabasica, lv_cargoapaquete );               
               BEGIN 
                   EXECUTE IMMEDIATE LvSentencia1 USING lv_tecno_1, lv_subproducto, lv_tipo, lv_plan;
               EXCEPTION
               WHEN OTHERS THEN
                    lvmenserr := SQLERRM; 
                    GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion_XX.Actualiza_DatosPlanes', 'No pudo actualizar el Plan: '||lv_plan);
               END;
               -- fin: Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
               cont_regi := cont_regi + 1;
               IF MOD(cont_regi,100) = 0 THEN COMMIT;END IF;
      END LOOP; -- FOR U IN regi_cv
      COMMIT; -- para los faltantes
      CLOSE regi_cv;
      --
EXCEPTION
    WHEN OTHERS THEN
         lvmenserr := SQLERRM;
                    INSERT INTO rp_BITACORA_TMP( tipo, fecha, msg_error, usuario, prg, comentario)
                                 VALUES ( 'EXCE', SYSDATE, lvMensErr, USER, 'rp_estadistica_facturacion_XX.Actualiza_DatosPlanes', ' Plan: '||lv_plan );
                    COMMIT;
END;
------------------------------------------------------------------------------------
PROCEDURE OBTENER_info_CTA ( pv_fecha   VARCHAR2) IS
--
lvSentencia   VARCHAR2(1000);
lv_mes        VARCHAR2(2);
lv_anio       VARCHAR2(4);
--
BEGIN
   lv_mes := substr(pv_fecha,4,2);
   lv_anio:= substr(pv_fecha,7);
   --
   lvSentencia := 
      ' select /*+ rule */ producto,nombre, count(*) nro_ctas, sum(valor) '||
      ' from co_fact_24'||lv_mes||lv_anio||
      ' where nombre in ( '||
      ''''||'Cargo a Paquete Cto AMPS'||''''||','||''''||'Cargo a Paquete Cto GSM'||''''||','||''''||'Cargo a Paquete Cuenta'||''''||','||
      ''''||'Tarifa Basica'||''''||','||''''||'Tarifa B�sica  AMPS'||''''||','||''''||'Tarifa B�sica  GSM'||''''||','||
      ''''||'Cobro adicional linea pool'||''''||','||''''||'Tarifa Basica Linea Adicional'||''''||','||
      ''''||'Pooling Corporativo Adicional1'||''''||','||''''||'Pooling Corporativo Adicional2'||''''||
      ' ) '||
      ' group by producto,nombre ';
END;
------------------------------------------------------------------------------------
END rp_estadistica_facturacion_XX;
/

