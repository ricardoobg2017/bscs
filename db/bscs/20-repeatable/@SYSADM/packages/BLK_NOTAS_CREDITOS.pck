CREATE OR REPLACE PACKAGE BLK_NOTAS_CREDITOS IS

--==========================================================================================================--
  -- CREADO           : CLS CHRISTIAN YUPA
  -- PROYECTO         : [11554]  Mejoras en el proceso de emision de notas de cr?dito
  -- FECHA            : 21/09/2017 - 08/03/2018
  -- LIDER PROYECTO   : SIS LUIS FLORES
  -- COORDINADOR CLS  : CLS GEOVANNY BARRERA
  -- MOTIVO           : PROCESO PRINCIPAL DE NOTAS DE CREDITO - CARGA A LA FEES
--==========================================================================================================--
--==========================================================================================================--
  -- CREADO           : CLS CINDY CALDER?N 
  -- PROYECTO         : [11554] Ajuste en el proceso de emision de notas de creditos
  -- FECHA            : 23/02/2018
  -- LIDER PROYECTO   : SIS LUIS FLORES
  -- LIDER CLS        : CLS GEOVANNY BARRERA
--==========================================================================================================--


  TYPE TVARCHAR IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR200 IS TABLE OF VARCHAR(200) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

  --
  FUNCTION BLF_PARAMETROS_CRED(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2;
  --
  PROCEDURE BLP_INSERTA_BITACORA_CRED (PN_ID_PROCESO        IN  NUMBER,
                                       PN_ID_REQUERIMIENTO  IN  NUMBER,
                                       PV_USUARIO           IN  VARCHAR2,
                                       PV_TRANSACCION       IN  VARCHAR2,
                                       PV_ESTADO            IN  VARCHAR2,
                                       PV_OBSERVACION       IN  VARCHAR2,
                                       PV_ERROR             OUT VARCHAR2);
  --
  PROCEDURE BLP_ACTUALIZA_BITACORA_CRED (PV_USUARIO          IN  VARCHAR2,
                                         PN_ID_PROCESO       IN  NUMBER,
                                         PN_ID_REQUERIMIENTO IN NUMBER,
                                         PV_TRANSACCION      IN  VARCHAR2,
                                         PV_ESTADO           IN  VARCHAR2,
                                         PV_OBSERVACION      IN  VARCHAR2,
                                         PV_ERROR            OUT VARCHAR2);
  --
  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2;

  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean;

  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number;

  procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number);

  procedure BLP_balancea_carga(pn_id_req number);

  procedure BLP_carga_sndata(PN_ID_REQ IN NUMBER);

    procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion     number,
                                  pv_usuario          varchar2,
                                  pn_hilo             number,
                                  pn_id_requerimiento number,
                                  pn_id_proceso       number,
                                  pv_error            out varchar2);

  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2);

  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null);

  procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2,
                                   pv_error           out varchar2);

  procedure BLP_detiene(pn_id_ejecucion number);

  procedure BLP_ejecuta(pv_usuario          varchar2, -- usuario del axis
                        pn_id_notificacion  number, -- 
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar2,
                        pv_entdate          varchar2, 
                        pv_respaldar        varchar DEFAULT 'N',
                        pv_tabla_respaldo   varchar DEFAULT NULL,
                        pv_band_ejecucion   varchar2 DEFAULT 'A', 
                        pn_id_requerimiento number,
                        pn_id_proceso       number,
                        pv_error            out varchar2);

  --
  procedure blp_occ_mail(pv_mensaje_remark   varchar2, 
                         pv_usuario          varchar2,
                         pn_id_proceso       number,
                         pn_id_requerimiento number,
                         pv_fecha            varchar2,
                         pv_registros        varchar2,
                         pv_archivo          varchar2,
                         pv_error            out varchar2);

  PROCEDURE BLP_VALORES_PARAMETRIZADOS(PV_CODIGO      IN VARCHAR2,
                                       PV_DESCRIPCION IN VARCHAR2,
                                       PV_VALOR       OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2);
  
  PROCEDURE PR_NC_DEVUELVE_FACTURA(PV_CUENTA       VARCHAR2,
                                   PV_VALOR        VARCHAR2,
                                   PV_FACTURA  OUT VARCHAR2,
                                   PD_FECHA_FA OUT DATE,
                                   PD_FECHA_CORTE OUT DATE,
                                   PV_CIUDAD OUT VARCHAR2,
                                   PV_REGION OUT VARCHAR2,
                                   PV_ERROR    OUT VARCHAR2);
  
  --SUD GGO VERIFICACION DE SI TIENE FACTURA CON ICE
  PROCEDURE PR_NC_DEVUELVE_FACT_IECE(PV_CUENTA        IN VARCHAR2,
                                     PV_VALOR         IN VARCHAR2,
                                     PV_FACTURA      OUT VARCHAR2,
                                     PD_FECHA_FA     OUT DATE,
                                     PD_FECHA_CORTE  OUT DATE,
                                     PV_CIUDAD       OUT VARCHAR2,
                                     PV_REGION       OUT VARCHAR2,
                                     PV_CODIGO       OUT NUMBER,
                                     PV_ERROR        OUT VARCHAR2);
  --SUD GGO VERIFICACION DE SI TIENE FACTURA CON ICE


  FUNCTION BLP_VERIFICA_DECIMAL(PV_NUMERO VARCHAR2 ) return number;
  --
  /*PROCEDURE PR_NC_DATOS_CAJA(PV_USUARIO          varchar2,
                           PN_REQUERIMIENTO       NUMBER,
                           PV_CIA                 VARCHAR2,
                           PV_BODEGA              VARCHAR2,
                           PV_PRODUCTO_CAJA       VARCHAR2,
                           PV_ERROR               OUT VARCHAR2);*/
			   
   PROCEDURE PR_NC_PREVIUS_DATOS_CAJA(PV_USUARIO          varchar2,
                           PN_REQUERIMIENTO       NUMBER,
                           PV_CIA                 VARCHAR2,
                           PV_BODEGA              VARCHAR2,
                           PV_PRODUCTO_CAJA       VARCHAR2,
                           PV_ERROR               OUT VARCHAR2);
                           
  PROCEDURE PR_NC_DATOS_CAJA(PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_NC_DATOS_CAJA_STANDBY(PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_NC_DATOS_CAJA_SIN_DOC(PV_ERROR OUT VARCHAR2);
                              
  FUNCTION FNC_DATO_TRAMA_CRED(PV_TRAMA     IN VARCHAR2,
                        PV_DATO      IN VARCHAR2,
                        PV_SEPARATOR IN VARCHAR2 DEFAULT ';') RETURN VARCHAR2; 
  --
  PROCEDURE PNC_SRE_GENERA_NOTAS_CRED(pv_compania  IN VARCHAR2,
                                      pv_usuario   IN VARCHAR2,
                                      pn_bodega    IN NUMBER ,
                                      pn_registros OUT NUMBER,
                                      pv_error     OUT VARCHAR2);  
                                      
  PROCEDURE PNC_SRE_INSERTA_CAJA( PV_CIA               IN  VARCHAR2,
                                 PV_CUENTA            IN  VARCHAR2,
                                 PV_FECHA_INGRESO     IN  VARCHAR2,
                                 PV_DESCRIPCION       IN  VARCHAR2,
                                 PV_DIRECCION         IN  VARCHAR2,
                                 PV_OBSERVACION       IN  VARCHAR2,
                                 PV_NO_FACTURA        IN  VARCHAR2,
                                 PV_NO_CLIENTE        IN  VARCHAR2,
                                 PV_FECHAFAC          IN  VARCHAR2,
                                 PV_STATUSBSCS        IN  VARCHAR2,
                                 PV_LOCALIDAD         IN  VARCHAR2,
                                 PV_PRODUCTO          IN  VARCHAR2,
                                 PN_TOTAL             IN  NUMBER,
                                 PN_TOTALIMPONIBLE    IN  NUMBER,
                                 PN_TOTALIVA          IN  NUMBER,
                                 PN_TOTALICE          IN  NUMBER,
                                 PV_USUARIO           IN  VARCHAR2,
                                 PV_CICLO             IN  VARCHAR2,
                                 PV_RUC               IN  VARCHAR2,
                                 pn_error              OUT NUMBER,
                                 pv_error     OUT VARCHAR2);
 Procedure PR_ENVIA_MAIL_CC_FILES_HTML(Pv_Ip_Servidor  In Varchar2,
                                        Pv_Sender       In Varchar2,
                                        Pv_Recipient    In Varchar2,
                                        Pv_Ccrecipient  In Varchar2,
                                        Pv_Subject      In Varchar2,
                                        Pv_Message      In clob,
                                        Pv_Max_Size     In Varchar2 Default 9999999999,
                                        Pv_Archivo1     In Varchar2 Default Null,
                                        Pv_Archivo2     In Varchar2 Default Null,
                                        Pv_Archivo3     In Varchar2 Default Null,
                                        Pv_Archivo4     In Varchar2 Default Null,
                                        Pv_Archivo5     In Varchar2 Default Null,
                                        Pv_Archivo6     In Varchar2 Default Null,
                                        Pv_Archivo7     In Varchar2 Default Null,
                                        Pv_Archivo8     In Varchar2 Default Null,
                                        Pv_Archivo9     In Varchar2 Default Null,
                                        Pv_Archivo10    In Varchar2 Default Null,
                                        Pv_Archivo11    In Varchar2 Default Null,
                                        Pv_Archivo12     In Varchar2 Default Null,
                                        Pv_Archivo13     In Varchar2 Default Null,
                                        Pv_Archivo14     In Varchar2 Default Null,
                                        Pv_Archivo15     In Varchar2 Default Null,
                                        Pv_Archivo16     In Varchar2 Default Null,
                                        Pv_Archivo17     In Varchar2 Default Null,
                                        Pv_Archivo18     In Varchar2 Default Null,
                                        Pv_Archivo19     In Varchar2 Default Null,
                                        Pv_Archivo20     In Varchar2 Default Null,
                                        Pv_Archivo21     In Varchar2 Default Null,
                                        Pv_Archivo22     In Varchar2 Default Null,
                                        Pv_Archivo23     In Varchar2 Default Null,
                                        Pv_Archivo24     In Varchar2 Default Null,
                                        Pv_Archivo25     In Varchar2 Default Null,
                                        Pv_Archivo26     In Varchar2 Default Null,
                                        Pv_Archivo27     In Varchar2 Default Null,
                                        Pv_Archivo28     In Varchar2 Default Null,
                                        Pv_Archivo29     In Varchar2 Default Null,
                                        Pv_Archivo30     In Varchar2 Default Null,
                                        Pb_Blnresultado In Out Boolean,
                                        Pv_Error        Out Varchar2);
 PROCEDURE PR_REPORTE(PN_ID_REQ   IN  NUMBER,
                       PV_USUARIO  IN  VARCHAR2,
                       PV_PROD     IN  VARCHAR2,
                       PN_NC_GEN   IN  VARCHAR2,
                       PV_ERROR    OUT VARCHAR2);
                       
PROCEDURE PR_REPORTE_MASIVO(PV_ERROR OUT VARCHAR2);

 PROCEDURE PR_NOTIFICACION_INSERCION(PN_ID_REQ   IN  NUMBER,
                                       PV_USUARIO  IN  VARCHAR2,
                                       PV_PROD     IN  VARCHAR2,
                                       PV_ERROR    OUT VARCHAR2);
                                       
PROCEDURE PR_REPORTE_MASIVO_S(PV_ERROR OUT VARCHAR2);

PROCEDURE PR_REPORTE_SIN_DOC(PV_ERROR OUT VARCHAR2);

 PROCEDURE FC_ACT_MESES (PV_VALOR IN VARCHAR2,
                           PN_ERROR OUT NUMBER,
                           PV_ERROR OUT VARCHAR2);
 ---INI [11554] CLS CCALDERON - 23/02/2018
 PROCEDURE PR_INSERTA_DATA_GYE(PV_ERROR  OUT VARCHAR2);
 
 PROCEDURE PR_INSERTA_DATA_GYE_NEW(PV_ERROR  OUT VARCHAR2);
 
 PROCEDURE PR_INSERTAR_GYE_SIN_DOC(PV_ERROR  OUT VARCHAR2);
 
 PROCEDURE PR_REPORTE_ERROR_GYE(PV_ENTRADA  IN  VARCHAR2,
                                PV_ERROR    OUT VARCHAR2);                                
 ---FIN [11554] CLS CCALDERON - 23/02/2018
 
end BLK_NOTAS_CREDITOS;
/
create or replace package body BLK_NOTAS_CREDITOS is
--==========================================================================================================--
  -- CREADO           : CLS CHRISTIAN YUPA
  -- PROYECTO         : [11554]  Mejoras en el proceso de emision de notas de cr?dito
  -- FECHA            : 21/09/2017 - 08/03/2018
  -- LIDER PROYECTO   : SIS LUIS FLORES
  -- COORDINADOR CLS  : CLS GEOVANNY BARRERA
  -- MOTIVO           : PROCESO PRINCIPAL DE NOTAS DE CREDITO - CARGA A LA FEES
--==========================================================================================================--
--==========================================================================================================--
  -- CREADO           : CLS CINDY CALDER?N 
  -- PROYECTO         : [11554] Ajuste en el proceso de emision de notas de creditos
  -- FECHA            : 23/02/2018
  -- LIDER PROYECTO   : SIS LUIS FLORES
  -- LIDER CLS        : CLS GEOVANNY BARRERA
--==========================================================================================================--
--Reenvio objeto 16/03/2018
  tn_sncode          tnumber;
  tv_ACCGLCODE       tvarchar;
  tv_ACCSERV_CATCODE tvarchar;
  tv_ACCSERV_CODE    tvarchar;
  tv_ACCSERV_TYPE    tvarchar;
  tn_vscode          tnumber;
  tn_spcode          tnumber;
  tn_evcode          tnumber;

  --
  FUNCTION BLF_PARAMETROS_CRED(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS
  --
  CURSOR OBTIENE_PARAMETRO (CV_TIPO_PARAMETRO IN VARCHAR2)IS
  SELECT T.VALOR1
    FROM NC_PARAMETROS_GRALES T
   WHERE T.CODIGO = CV_TIPO_PARAMETRO;
  -- 
     LV_VALOR_PAR   VARCHAR2(1000);
   
   BEGIN
    
      OPEN OBTIENE_PARAMETRO (PV_COD_PARAMETRO);
      FETCH OBTIENE_PARAMETRO INTO LV_VALOR_PAR;
      CLOSE OBTIENE_PARAMETRO;
     
       RETURN LV_VALOR_PAR;
      
    
        
    EXCEPTION
     WHEN OTHERS THEN
     RETURN NULL;
      
 END BLF_PARAMETROS_CRED;
  --
  PROCEDURE BLP_INSERTA_BITACORA_CRED (PN_ID_PROCESO        IN  NUMBER,
                                       PN_ID_REQUERIMIENTO  IN  NUMBER,
                                       PV_USUARIO           IN  VARCHAR2,
                                       PV_TRANSACCION       IN  VARCHAR2,
                                       PV_ESTADO            IN  VARCHAR2,
                                       PV_OBSERVACION       IN  VARCHAR2,
                                       PV_ERROR             OUT VARCHAR2) IS

      /* Bitacorizacion */
      
    BEGIN

      -- Se bitacoriza la ejecucion 
      INSERT INTO NC_BITACORA_EMISION_NOTA_CRED
        (ID_PROCESO,ID_REQUERIMIENTO, TRANSACCION, FECHA_INICIO, ESTADO, OBSERVACION, USUARIO)
      VALUES
        (PN_ID_PROCESO,
         PN_ID_REQUERIMIENTO,
         PV_TRANSACCION,
         SYSDATE,
         PV_ESTADO,
         PV_OBSERVACION,
         PV_USUARIO);
         --
         COMMIT;
         --
        
    EXCEPTION
      WHEN OTHERS THEN
       PV_ERROR := 'BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED - ' || SUBSTR(SQLERRM, 1, 500);
  END BLP_INSERTA_BITACORA_CRED;
  --
  PROCEDURE BLP_ACTUALIZA_BITACORA_CRED (PV_USUARIO          IN  VARCHAR2,
                                         PN_ID_PROCESO       IN  NUMBER,
                                         PN_ID_REQUERIMIENTO IN NUMBER,
                                         PV_TRANSACCION      IN  VARCHAR2,
                                         PV_ESTADO           IN  VARCHAR2,
                                         PV_OBSERVACION      IN  VARCHAR2,
                                         PV_ERROR            OUT VARCHAR2) IS

     /* Bitacorizacion */

  BEGIN
  
    UPDATE NC_BITACORA_EMISION_NOTA_CRED G
       SET G.FECHA_FIN   = SYSDATE,
           G.ESTADO      = PV_ESTADO,
           G.OBSERVACION = PV_OBSERVACION
     WHERE G.TRANSACCION LIKE '%'||PV_TRANSACCION||'%'
       AND G.ID_PROCESO  = PN_ID_PROCESO
       AND G.USUARIO = PV_USUARIO
       AND G.ID_REQUERIMIENTO=PN_ID_REQUERIMIENTO
       AND G.ESTADO = 'P';
      --
      COMMIT;
      --
   EXCEPTION
    WHEN OTHERS THEN
     PV_ERROR := 'BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED -'|| SUBSTR(SQLERRM, 1, 500);   
  END  BLP_ACTUALIZA_BITACORA_CRED; 
  
  

  function BLF_obtiene_estado(pn_id_ejecucion number) return varchar2 is
  
    cursor c_estado_ejecucion(cn_id_ejecucion number) is
      select estado
        from nc_carga_nota_credito x
       where x.id_ejecucion = cn_id_ejecucion;
  
    lv_estado nc_carga_nota_credito.estado%type;
  
  begin
  
    open c_estado_ejecucion(pn_id_ejecucion);
    fetch c_estado_ejecucion
      into lv_estado;
    close c_estado_ejecucion;
  
    return lv_estado;
  
  exception
    when others then
      return 'X';
    
  end BLF_obtiene_estado;

  function BLF_verifica_finalizacion(pn_id_ejecucion number) return boolean is
  
    cursor c_verifica_ejecucion(cn_id_ejecucion number) is
      select nvl(count(*), 0)
        from nc_carga_not_cre_hilo x
       where x.id_ejecucion = cn_id_ejecucion
         and estado = 'F';
  
    cursor c_verifica_ejecucion1(cn_id_ejecucion number) is
      select cant_hilos
        from nc_carga_nota_credito
       where id_ejecucion = cn_id_ejecucion;
  
    ln_verifica number;
    ln_hilos    number;
  
  begin
  
    open c_verifica_ejecucion(pn_id_ejecucion);
    fetch c_verifica_ejecucion
      into ln_verifica;
    close c_verifica_ejecucion;
  
    open c_verifica_ejecucion1(pn_id_ejecucion);
    fetch c_verifica_ejecucion1
      into ln_hilos;
    close c_verifica_ejecucion1;
  
    if ln_verifica = ln_hilos then
      return true;
    else
      return false;
    end if;
  
  exception
    when others then
      return false;
    
  end BLF_verifica_finalizacion;
 --
  function BLF_obtiene_ultima_notif(pn_id_ejecucion number) return number is
  
    cursor c_obtiene is
      select substr(observacion, instr(observacion, ':') + 2)
        from nc_carga_nota_credito
       where id_ejecucion = pn_id_ejecucion;
  
    lv_tiempo varchar2(10);
  
  begin
  
    open c_obtiene;
    fetch c_obtiene
      into lv_tiempo;
    if c_obtiene%notfound then
      return - 1;
    end if;
    close c_obtiene;
  
    return to_number(lv_tiempo);
  
  exception
    when others then
      return 0;
    
  end BLF_obtiene_ultima_notif;
 --
 procedure BLP_setea_ultima_notif(pn_id_ejecucion number, pn_valor number) is
  
  begin
  
    update nc_carga_nota_credito
       set observacion = substr(observacion, 1, instr(observacion, ':') + 1) ||
                         to_char(pn_valor)
     where id_ejecucion = pn_id_ejecucion;
  
    commit;
  
  end BLP_setea_ultima_notif;
  --
  procedure BLP_balancea_carga(pn_id_req number) is
  
  begin
  
   /* update nc_CARGA_NOT_CRE_TMP g
       set customer_id =
           (select customer_id from contract_all where co_id = g.co_id)
     where co_id is not null
       and id_requerimiento = pn_id_req;*/
  
    update nc_CARGA_NOT_CRE_TMP j
       set customer_id =
           (select customer_id from customer_all where custcode = j.custcode)
     where custcode is not null
       and id_requerimiento = pn_id_req;
  
    update nc_CARGA_NOT_CRE_TMP j
       set customer_id =
           (select customer_id
              from customer_all
             where customer_id = j.customer_id)
     where customer_id is not null
       and custcode is null
       and id_requerimiento = pn_id_req;
  
    update nc_CARGA_NOT_CRE_TMP
       set status = 'E',
           error  = 'Problemas al obtener el customer_id de la tabla contract_all'
     where customer_id is null
       and id_requerimiento = pn_id_req;
  
    update nc_CARGA_NOT_CRE_TMP
       set status = 'E', error = 'SNCode no existente'
     where (sncode is null OR sncode not in (select sncode from mpusntab))
       and id_requerimiento = pn_id_req;
  
    commit;
  
  end BLP_balancea_carga;

  --
  procedure BLP_carga_sndata(PN_ID_REQ IN NUMBER) is
  
    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;
  
  begin
  
    open lcv_cur for
    
      select x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        from mpulktmb x, mpulkexn y
       where x.sncode = y.sncode
         AND EXISTS (select 'X'
                from nc_CARGA_NOT_CRE_TMP a
               WHERE sncode = x.sncode
                 AND a.id_requerimiento = PN_ID_REQ)
         and X.vscode = (select max(a.vscode)
                           from mpulktmb a
                          where tmcode = 35
                            and sncode = 399)
         and X.tmcode = 35;
  
    FETCH LCV_CUR BULK COLLECT
      INTO tn_sncode,
           tv_ACCGLCODE,
           tv_ACCSERV_CATCODE,
           tv_ACCSERV_CODE,
           tv_ACCSERV_TYPE,
           tn_vscode,
           tn_spcode,
           tn_evcode;
    CLOSE LCV_CUR;
  
    FOR I in tn_sncode.first .. tn_sncode.last LOOP
      tv_ACCGLCODE(tn_sncode(i)) := tv_ACCGLCODE(i);
      tv_ACCSERV_CATCODE(tn_sncode(i)) := tv_ACCSERV_CATCODE(i);
      tv_ACCSERV_CODE(tn_sncode(i)) := tv_ACCSERV_CODE(i);
      tv_ACCSERV_TYPE(tn_sncode(i)) := tv_ACCSERV_TYPE(i);
      tn_vscode(tn_sncode(i)) := tn_vscode(i);
      tn_spcode(tn_sncode(i)) := tn_spcode(i);
      tn_evcode(tn_sncode(i)) := tn_evcode(i);
    
    END LOOP;
  
  end blp_carga_sndata;
  --
  procedure BLP_Carga_Occ_Co_id(pn_id_ejecucion     number,
                                pv_usuario          varchar2,
                                pn_hilo             number,
                                pn_id_requerimiento number,
                                pn_id_proceso       number,
                                pv_error            out varchar2) is
  
    cursor c_config is
      select cant_reg_mem,
             entdate,
             fecha_inicio,
             id_notificacion,
             nombre_respaldo,
             recurrencia,
             remark,
             respaldar,
             tiempo_notif,
             usuario
        from nc_carga_nota_credito
       where id_ejecucion = pn_id_ejecucion;
  
    cursor c_carga(PN_ID_REQUERIMIENTO NUMBER) is
      select h.rowid,
             to_number(replace(replace(h.AMOUNT, ',', '.'), chr(13), '')) amount,
             h.sncode,
           --  h.co_id,
             h.customer_id ---/*-to_number(h.amount)*/
        from sysadm.nc_CARGA_NOT_CRE_TMP H
       where status is null
         and hilo = pn_hilo
         and customer_id is not null
         and h.id_requerimiento = PN_ID_REQUERIMIENTO; 
  
    cursor c_seq(cn_customer_id number) is
      SELECT nvl(max(seqno), 0) seqno
        FROM FEES
       WHERE customer_id = cn_customer_id;
  
    cursor c_hilo_seq is
      SELECT nvl(max(seq), 0) seq
        FROM nc_carga_not_cre_hilo_det bc
       WHERE bc.id_ejecucion = pn_id_ejecucion
         and bc.hilo = pn_hilo;
  
    cursor c_cust_id_status(cn_customer_id number) is
      SELECT cstype, csdeactivated
        from customer
       where customer_id = cn_customer_id;
  
   /* cursor c_co_id_status(cn_co_id number) is
      SELECT entdate
        from contract_history
       where ch_status = 'd'
         and co_id = cn_co_id;*/
  
    cursor c_minutos(cn_id_ejecucion number) is
      select trunc((sysdate - fecha_inicio) * 24 * 60, 2) minutos,
             reg_procesados
        from nc_carga_nota_credito
       where id_ejecucion = cn_id_ejecucion;
  
    tn_sncode2            tnumber;
    tv_ACCGLCODE2         tvarchar;
    tv_ACCSERV_CATCODE2   tvarchar;
    tv_ACCSERV_CODE2      tvarchar;
    tv_ACCSERV_TYPE2      tvarchar;
    tn_vscode2            tnumber;
    tn_spcode2            tnumber;
    tn_evcode2            tnumber;
    tn_ln_seq             tnumber;
    td_entdate            tdate;
    td_valid_from         tdate;
    td_entdate_by_cust    tdate;
    td_valid_from_by_cust tdate;
    tv_error              tvarchar;
    ln_minutos            number;
    ln_registros_total    number;
    ln_counter            number := 0;
    lv_error              varchar2(200);
    lb_nohaydatos         boolean;
    lc_config             c_config%rowtype;
    --lc_co_id_status       c_co_id_status%rowtype;
    ln_hilo_seq           number;
    le_error exception;
    ln_period         number;
    lc_cust_id_status c_cust_id_status%rowtype;
    lc_seq            c_seq%rowtype;
    lv_error_mail     varchar2(1000);
    TV_ROWID          TVARCHAR;
    TN_AMOUNT         TNUMBER;
    TN_SNCODE         TNUMBER;
    --TN_COID           TNUMBER;
    TN_CUSTOMER_ID    TNUMBER;
    TN_SEQ_BY_CUSTID  TNUMBER;
    LV_TRANSACCION    VARCHAR2(4000);
    LV_ERROR_BITA     VARCHAR2(4000);  
    LE_ERROR_BITA     EXCEPTION;
    LV_REFERENCIA     VARCHAR2(4000):='BLK_NOTAS_CREDITOS.BLP_CARGA_OCC_CO_ID';
  BEGIN
   --
     --
    LV_TRANSACCION:='Se realiza la carga a la FEES - Hilo:'||PN_HILO;
    -- Se bitacoriza la ejecucion del proceso
    BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => PN_ID_PROCESO,
                                                 PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                 PV_USUARIO          => pv_usuario,
                                                 PV_TRANSACCION      => LV_TRANSACCION,
                                                 PV_ESTADO           => 'P',
                                                 PV_OBSERVACION      =>  '',
                                                 PV_ERROR            => LV_ERROR_BITA);
    IF LV_ERROR_BITA IS NOT NULL THEN
           RAISE LE_ERROR_BITA;
          END IF;  
    --
   --
    open c_config;
    fetch c_config
      into lc_config;
    close c_config;
  
    open c_hilo_seq;
    fetch c_hilo_seq
      into ln_hilo_seq;
    close c_hilo_seq;
  
    ln_hilo_seq := ln_hilo_seq + 1;
  
    insert into nc_carga_not_cre_hilo_det
    values
      (pn_id_ejecucion, pn_hilo, 'P', 0, ln_hilo_seq, sysdate, null, null);
  
    update nc_carga_not_cre_hilo
       set estado = 'P'
     where id_ejecucion = pn_id_ejecucion
       and hilo = pn_hilo;
  
    commit;
  
    BLP_carga_sndata(pn_id_requerimiento);
  
    open c_carga(pn_id_requerimiento);
    loop
    
      TV_ROWID.delete;
      tv_error.delete;
      TN_AMOUNT.delete;
      TN_SNCODE.delete;
     -- TN_COID.delete;
      tn_sncode2.delete;
      tv_ACCGLCODE2.delete;
      tv_ACCSERV_CATCODE2.delete;
      tv_ACCSERV_CODE2.delete;
      tv_ACCSERV_TYPE2.delete;
      tn_vscode2.delete;
      tn_spcode2.delete;
      tn_evcode2.delete;
      tn_customer_id.delete;
      tn_ln_seq.delete;
      ln_counter := 0;
    
      lb_nohaydatos := true;
    
      fetch c_carga bulk collect
        into TV_ROWID,
             TN_AMOUNT,
             TN_SNCODE,
            -- TN_COID,
             TN_CUSTOMER_ID limit lc_config.cant_reg_mem;
    
      if TV_ROWID.count > 0 THEN
      
        lb_nohaydatos := false;
      
        FOR I IN TV_ROWID.FIRST .. TV_ROWID.LAST
        
         LOOP
        
          tv_ACCGLCODE2(i) := tv_ACCGLCODE(tn_sncode(i));
          tv_ACCSERV_CATCODE2(i) := tv_ACCSERV_CATCODE(tn_sncode(i));
          tv_ACCSERV_CODE2(i) := tv_ACCSERV_CODE(tn_sncode(i));
          tv_ACCSERV_TYPE2(i) := tv_ACCSERV_TYPE(tn_sncode(i));
          tn_vscode2(i) := tn_vscode(tn_sncode(i));
          tn_spcode2(i) := tn_spcode(tn_sncode(i));
          tn_evcode2(i) := tn_evcode(tn_sncode(i));
          tv_error(i) := null;
        
          if not TN_SEQ_BY_CUSTID.exists(TN_CUSTOMER_ID(i)) then
          
            --Obtener el secuencial
            open c_seq(TN_CUSTOMER_ID(i));
            fetch c_seq
              into lc_seq;
            if c_seq%notfound then
              tn_ln_seq(i) := 1;
            else
              tn_ln_seq(i) := lc_seq.seqno + 1;
            end if;
            close c_seq;
          
            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := tn_ln_seq(i);
          
            --Determino si la cuenta est? desactivada
            open c_cust_id_status(TN_CUSTOMER_ID(i));
            fetch c_cust_id_status
              into lc_cust_id_status;
          
            if c_cust_id_status%notfound then
              tv_error(i) := 'Problemas al obtener el estado del customer_id';
            end if;
          
            if lc_cust_id_status.cstype = 'd' then
              --S? est? desactivada, entonces lo ingreso un d?a antes de fecha desact
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_cust_id_status.csdeactivated) - 1;
            else
              --Si no, tomo la fecha del param de entrada
              td_entdate_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);
            end if;
          
            td_valid_from_by_cust(TN_CUSTOMER_ID(i)) := trunc(lc_config.entdate);
          
            close c_cust_id_status;
          
            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));
          
          else
          
            TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i)) + 1;
          
            tn_ln_seq(i) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(i));
          
            td_entdate(i) := td_entdate_by_cust(TN_CUSTOMER_ID(i));
            td_valid_from(i) := td_valid_from_by_cust(TN_CUSTOMER_ID(i));
          
          end if;
        
         /* if TN_COID(i) is not null then
            --Cargos a nivel de CO_ID
            --Determino si el co_id esta desactivado
            open c_co_id_status(TN_COID(i));
            fetch c_co_id_status
              into lc_co_id_status;
          
            if c_co_id_status%notfound then
              close c_co_id_status;
            else
              td_entdate(i) := trunc(lc_co_id_status.entdate) - 1;
              close c_co_id_status;
            end if;
          
          end if;*/
        
          if lc_config.recurrencia = 'N' then
            ln_period := 1;
          else
            ln_period := 99;
          end if;
        
          ln_counter := ln_counter + 1;
          -- end if;
        
        end loop;
      BEGIN
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
         
         INSERT /*+ APPEND */
          INTO FEES
            (customer_id,
             seqno,
             fee_type,
             glcode,
             entdate,
             period,
             username,
             valid_from,
             servcat_code,
             serv_code,
             serv_type,
            -- co_id,
             amount,
             remark,
             currency,
             glcode_disc,
             glcode_mincom,
             tmcode,
             vscode,
             spcode,
             sncode,
             evcode,
             fee_class)
          values
            (tn_customer_id(m),
             tn_ln_seq(m),
             lc_config.recurrencia,
             tv_ACCGLCODE2(M),
             lc_config.entdate - 1,
             ln_period,
             lc_config.usuario,
             td_valid_from(m) - 1,
             tv_ACCSERV_CATCODE2(M),
             tv_ACCSERV_CODE2(M),
             tv_ACCSERV_TYPE2(M),
            -- tn_coid(M),
             tn_amount(M),
             lc_config.remark,
             19,
             tv_ACCGLCODE2(M),
             tv_ACCGLCODE2(M),
             35,
             tn_VSCODE2(M),
             tn_SPCODE2(M),
             tn_sncode(M),
             tn_EVCODE2(M),
             3);
      EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.put_line(SUBSTR(SQLERRM,1,500));
      END;
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          update nc_CARGA_NOT_CRE_TMP
             set status = 'P', error = null
           where rowid = TV_ROWID(M)
             AND id_requerimiento = pn_id_requerimiento
             and hilo = pn_hilo;
        COMMIT;
        update nc_carga_nota_credito
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion;
      
        update nc_carga_not_cre_hilo
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;
      
        update nc_carga_not_cre_hilo_det
           set reg_procesados = reg_procesados + ln_counter
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;
      
        --
        If BLF_obtiene_estado(pn_id_ejecucion => pn_id_ejecucion) = 'U' then
        
          update nc_carga_not_cre_hilo
             set estado = 'U'
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo;
        
          update nc_carga_not_cre_hilo_det
             set estado = 'U', fecha_fin = sysdate
           where id_ejecucion = pn_id_ejecucion
             and hilo = pn_hilo
             and seq = ln_hilo_seq;
        
          exit;
        
        end if;
      
      END IF;
    
      IF c_carga%NOTFOUND OR lb_nohaydatos then
        update nc_carga_not_cre_hilo
           set estado = 'F'
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo;
      
        update nc_carga_not_cre_hilo_det
           set estado = 'F', fecha_fin = sysdate
         where id_ejecucion = pn_id_ejecucion
           and hilo = pn_hilo
           and seq = ln_hilo_seq;
      
        commit;
      
        exit;
      
      end if;
    
      begin
        open c_minutos(pn_id_ejecucion);
        fetch c_minutos
          into ln_minutos, ln_registros_total;
        close c_minutos;
      
      exception
        when others then
          null;
        
      end;
    
      --Notificaciones peri?dicas
      if trunc((sysdate - lc_config.fecha_inicio) * 24 * 60) >=
         BLF_obtiene_ultima_notif(pn_id_ejecucion => pn_id_ejecucion) +
         lc_config.tiempo_notif then
      
        Blp_Setea_Ultima_Notif(pn_id_ejecucion => pn_id_ejecucion,
                               pn_valor        => trunc((sysdate -
                                                        lc_config.fecha_inicio) * 24 * 60) +
                                                  lc_config.tiempo_notif);
      
      end if;
    
      commit;
    
    end loop;
  
    close c_carga;
  
    --Ya finaliz? el proceso?
    if BLF_verifica_finalizacion(pn_id_ejecucion => pn_id_ejecucion) then
      BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                    pd_fecha_fin    => sysdate,
                    pv_observacion  => 'Finalizado',
                    pv_estado       => 'F');
    
      if lc_config.respaldar in ('Y', 'S') then
        BLP_Respalda_tabla(pv_tabla_respaldar => lc_config.nombre_respaldo);
      end if;
    
      begin
      
        open c_minutos(pn_id_ejecucion);
        fetch c_minutos
          into ln_minutos, ln_registros_total;
        close c_minutos;
      
      exception
        when others then
          null;
        
      end;
    
      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion,
                             pv_mensaje         => 'Fin proceso Carga NC. Usuario: ' ||
                                                   lc_config.usuario ||
                                                   ' Remark: ' ||
                                                   lc_config.remark ||
                                                   ' Minutos: ' ||
                                                   to_char(ln_minutos) ||
                                                   ' Total reg: ' ||
                                                   ln_registros_total,
                             pv_error           => lv_error_mail);
    
    end if;
    commit;
    --
      -- se actualiza la bitacora del proceso 
     BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                    PN_ID_PROCESO       => PN_ID_PROCESO,
                                                    PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                    PV_TRANSACCION      => LV_TRANSACCION,
                                                    PV_ESTADO           => 'F',
                                                    PV_OBSERVACION      => '', 
                                                    PV_ERROR            => LV_ERROR_BITA); 
        
    --
  exception
      WHEN LE_ERROR_BITA THEN
        PV_ERROR:='NO SE REALIZO EL PROCESO DE BITACORIZACI?N : '||LV_ERROR_BITA || ' - ' ||LV_REFERENCIA ;
         
     
    when others then
    
      --
      PV_ERROR:='ERROR - '|| LV_REFERENCIA || ' - ' ||SUBSTR(SQLERRM,1,500);
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
        BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                       PN_ID_PROCESO       => PN_ID_PROCESO,
                                                       PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                       PV_TRANSACCION      => LV_TRANSACCION,
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => PV_ERROR,
                                                       PV_ERROR            => LV_ERROR_BITA);
   
 
      --
      if (sqlerrm = 'ORA-00001: unique constraint (SYSADM.PKFEES) violated') then
        BLK_NOTAS_CREDITOS.blp_carga_occ_co_id(pn_id_ejecucion     => pn_id_ejecucion,
                                               pv_usuario          => pv_usuario,
                                               pn_hilo             => pn_hilo,
                                               pn_id_requerimiento => pn_id_requerimiento,
                                               pn_id_proceso       => pn_id_proceso,
                                               pv_error            => pv_error);
      
      end if;
    
      update nc_carga_not_cre_hilo
         set estado = 'E'
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo;
    
      update nc_carga_not_cre_hilo_det
         set estado = 'E', fecha_fin = sysdate, observacion = lv_error
       where id_ejecucion = pn_id_ejecucion
         and hilo = pn_hilo
         and seq = ln_hilo_seq;
    
      commit;
    
  end BLP_Carga_Occ_Co_id;

  --
  procedure BLP_Respalda_tabla(pv_tabla_respaldar varchar2) is
    lv_sql varchar2(500);
  begin
    lv_sql := 'CREATE TABLE ' || pv_tabla_respaldar || ' AS ' ||
              ' select * from nc_CARGA_NOT_CRE_TMP ';
    execute immediate lv_sql;
  exception
    when others then
      null;
  end BLP_Respalda_tabla;
  --
  procedure BLP_actualiza(pn_id_ejecucion   number,
                          pd_fecha_inicio   date default null,
                          pd_fecha_fin      date default null,
                          pv_estado         varchar2 default null,
                          pn_reg_procesados number default null,
                          pv_observacion    varchar2 default null) is
  
  begin
    update nc_carga_nota_credito
       set fecha_inicio   = nvl(pd_fecha_inicio, fecha_inicio),
           fecha_fin      = nvl(pd_fecha_fin, fecha_fin),
           estado         = nvl(pv_estado, estado),
           reg_procesados = nvl(pn_reg_procesados, reg_procesados),
           observacion    = nvl(pv_observacion, observacion)
     where id_ejecucion = pn_id_ejecucion;
  
    commit;
  
  end BLP_actualiza;
  --
  procedure BLP_envia_correo_grupo(pn_id_notificacion number,
                                   pv_mensaje         varchar2,
                                   pv_error           out varchar2) is
  
    cursor c_grupo_noti is
      select b.destinatario
        from nc_carga_grup_not_cred a, bl_carga_notifica b
       where a.id_notifica = b.id_notifica
         and a.estado = 'A'
         and b.estado = 'A'
         and b.tipo = 'MAIL'
         and a.id_notificacion = pn_id_notificacion;
  
    lv_remitentes varchar2(4000) := null;
  
  begin
  
    for i in c_grupo_noti loop
      lv_remitentes := lv_remitentes || i.destinatario || ';';
    end loop;
  
    lv_remitentes := substr(lv_remitentes, 1, length(lv_remitentes) - 1);
  
    send_mail.mail@axis(from_name => 'blk_carga_occ@conecel.com',
                        to_name   => lv_remitentes,
                        cc        => lv_remitentes,
                        cco       => null,
                        subject   => 'Carga de valores a la factura mediante OCC',
                        message   => pv_mensaje);
    -- OJO QUITAR EL COMENTARIO
    commit;
  exception
    when others then
      pv_error := sqlerrm;
  end BLP_envia_correo_grupo;
  --
  procedure BLP_detiene(pn_id_ejecucion number) is
  begin
    BLP_actualiza(pn_id_ejecucion => pn_id_ejecucion,
                  pv_estado       => 'U',
                  pv_observacion  => 'En pausa. Detenido manualmente');
  
  end;

  --
  procedure BLP_ejecuta(pv_usuario          varchar2, -- usuario del axis
                        pn_id_notificacion  number, -- 
                        pn_tiempo_notif     number,
                        pn_cantidad_hilos   number,
                        pn_cantidad_reg_mem number,
                        pv_recurrencia      varchar,
                        pv_Remark           varchar2,
                        pv_entdate          varchar2, 
                        pv_respaldar        varchar DEFAULT 'N',
                        pv_tabla_respaldo   varchar DEFAULT NULL,
                        pv_band_ejecucion   varchar2 DEFAULT 'A', 
                        pn_id_requerimiento number,
                        pn_id_proceso       number,
                        pv_error            out varchar2) is
  
    cursor c_id_ejec is
      SELECT nvl(max(id_ejecucion), 0) id_ejecucion
        FROM nc_carga_nota_credito;
  
    LN_ID_EJECUCION NUMBER;
    LV_REMARK       VARCHAR2(1000);
    LV_FECHA        VARCHAR2(100);
    LV_REFERENCIA   VARCHAR2(100) := 'BLK_NOTAS_CREDITOS.BLP_EJECUTA';
    LV_ERROR        VARCHAR2(5000);
    LD_FECHA        DATE;
    LE_ERROR        EXCEPTION;
    LV_TRANSACCION          VARCHAR2(4000);
    LV_ERROR_BITA           VARCHAR2(4000);
    LE_ERROR_BITA           EXCEPTION;
  
  begin
    --
    LV_TRANSACCION:='Se obtiene la Id ejecucion de la tabla nc_carga_nota_credito';
    -- Se bitacoriza la ejecucion del proceso
    BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => PN_ID_PROCESO,
                                                 PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                 PV_USUARIO          => pv_usuario,
                                                 PV_TRANSACCION      => LV_TRANSACCION,
                                                 PV_ESTADO           => 'P',
                                                 PV_OBSERVACION      =>  '',
                                                 PV_ERROR            => LV_ERROR_BITA);
    IF LV_ERROR_BITA IS NOT NULL THEN
           RAISE LE_ERROR_BITA;
          END IF;  
    --
  -----
 
  -----
  
    --
    open c_id_ejec;
    fetch c_id_ejec
      into ln_id_ejecucion;
    close c_id_ejec;
  
    --ln_id_ejecucion := ln_id_ejecucion + 1;
    ln_id_ejecucion := pn_id_requerimiento;
    
    
    lv_remark := replace(pv_remark, '*', ' ');
    lv_fecha  := replace(pv_entdate, '-', '/');    
    lv_fecha  := replace(lv_fecha,'_-_','');--
    lv_fecha  := replace(lv_fecha,'_','');----
    lv_fecha  := trim(lv_fecha);
    lv_fecha  := lv_fecha || ' 00:00:01';
  
    ld_fecha := to_date(lv_fecha, 'dd/mm/rrrr HH24:mi:ss');
  
    insert into nc_carga_nota_credito
    values
      (ln_id_ejecucion,
       pv_usuario,
       sysdate,
       null,
       'L',
       0,
       pn_id_notificacion,
       pn_tiempo_notif,
       pn_cantidad_hilos,
       pn_cantidad_reg_mem,
       pv_recurrencia,
       lv_remark,
       ld_fecha-1,
       pv_respaldar,
       pv_tabla_respaldo,
       null);
  
    IF pv_band_ejecucion = 'A' THEN
    
      BLP_actualiza(pn_id_ejecucion => ln_id_ejecucion,
                    pv_estado       => 'P',
                    pv_observacion  => 'Procesando: 0');
    
      BLP_balancea_carga(pn_id_req => pn_id_requerimiento);
    
    END IF;
    --
     -- se actualiza la bitacora del proceso 
     BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                    PN_ID_PROCESO       => PN_ID_PROCESO,
                                                    PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                    PV_TRANSACCION      => LV_TRANSACCION,
                                                    PV_ESTADO           => 'F',
                                                    PV_OBSERVACION      =>  'Id ejecucion :' ||ln_id_ejecucion, 
                                                    PV_ERROR            => LV_ERROR_BITA); 
        
  
   
   EXCEPTION
     WHEN LE_ERROR_BITA THEN
        PV_ERROR:='NO SE REALIZO EL PROCESO DE BITACORIZACI?N : '||LV_ERROR_BITA || '- ' || LV_REFERENCIA ;
         
     
    

     WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR - '|| LV_REFERENCIA || ' - ' || LV_ERROR;
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
       BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                      PN_ID_PROCESO       => PN_ID_PROCESO,
                                                      PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                      PV_TRANSACCION      => LV_TRANSACCION,
                                                      PV_ESTADO           => 'E',
                                                      PV_OBSERVACION      => 'ERROR - '|| LV_ERROR,
                                                      PV_ERROR            => LV_ERROR_BITA);
   

    WHEN OTHERS THEN 
      PV_ERROR:='ERROR - '|| LV_REFERENCIA || ' - ' ||SUBSTR(SQLERRM,1,500);
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
        BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                       PN_ID_PROCESO       => PN_ID_PROCESO,
                                                       PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                       PV_TRANSACCION      => LV_TRANSACCION,
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => PV_ERROR,
                                                       PV_ERROR            => LV_ERROR_BITA);
   
 
    
    
    
    
   
    
  end BLP_ejecuta;
  --

  procedure blp_occ_mail(pv_mensaje_remark   varchar2, 
                         pv_usuario          varchar2,
                         pn_id_proceso       number,
                         pn_id_requerimiento number,
                         pv_fecha            varchar2,
                         pv_registros        varchar2,
                         pv_archivo          varchar2,
                         pv_error            out varchar2) is
  
    CURSOR C_SUM_FEES(CN_ID_REQUERIMIENTO NUMBER, cv_remark VARCHAR2) IS
      select sum(amount)
        from sysadm.FEES D
       WHERE customer_id in (select customer_id
                               from sysadm.nc_CARGA_NOT_CRE_TMP
                              where id_requerimiento = CN_ID_REQUERIMIENTO
                                and customer_id is not null)
         and username = pv_usuario
         AND remark = cv_remark;
    
    cursor C_OBTIENE_SNCODE(CN_ID_REQUERIMIENTO NUMBER) IS
      select distinct (sncode)
        from sysadm.nc_CARGA_NOT_CRE_TMP
       where id_requerimiento = CN_ID_REQUERIMIENTO;
  
    CURSOR C_COUNT_FEES(CN_ID_REQUERIMIENTO NUMBER, cv_remark VARCHAR2) IS
      select count(*)
        from sysadm.FEES D
       WHERE customer_id in (select customer_id
                               from sysadm.nc_CARGA_NOT_CRE_TMP
                              where id_requerimiento = CN_ID_REQUERIMIENTO
                                and customer_id is not null)
         and username = pv_usuario
         AND remark = cv_remark;
 
  
    lv_salto             VARCHAR2(10);
    ln_sum_amount        number := 0;
    ln_count_FEES        number := 0;
    LN_SNCODE            NUMBER;
    ln_valor_descartados number := 0;
    lv_mensaje_remark    varchar2(1000) := null;
    lv_fecha             varchar(100);
    le_error exception;
    ln_result               number;
    lv_mensaje_retorno      varchar2(1000);
    ln_id_secuencia         number;
    lv_servidor             varchar2(100);
    lv_puerto               varchar2(100) := '';
    lv_archivo_adjunto      varchar2(1000);
    lv_asunto               varchar2(1000);
    lv_remitente            varchar2(1000);
    lv_tipo_envio           varchar2(1000);
    lv_clase                varchar2(1000);
    lv_reintentos           varchar2(1000);
    lv_encabezado           varchar2(1000);
    lv_mail_cc              varchar2(1000);
    lv_mail_cco             varchar2(1000);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_DIRECTORIO_FINAN     VARCHAR2(500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    LV_REFERENCIA           VARCHAR2(500) := 'BLK_NOTAS_CREDITOS.BLP_OCC_MAIL';
    --LN_BITACORA             NUMBER := 0;
    --LN_ERROR                NUMBER := 0;
    --LN_DETALLE              NUMBER := 0;
    LV_TRANSACCION          VARCHAR2(4000);
    LV_ERROR_BITA           VARCHAR2(4000);
    LE_ERROR_BITA           EXCEPTION;
    LV_ERROR                VARCHAR2(4000);
    LV_TIPO_ENVIO_M         VARCHAR2(5);
    LV_TIPO_ENVIO_A         VARCHAR2(5);
    le_error_scp            EXCEPTION;
    LV_MENSAJE_MAIL         VARCHAR2(4000);
  begin 
    
    --
    LV_TRANSACCION:='Proceso de datos - Modulo Notas Creditoc - Envio Mail final';
    -- Se bitacoriza la ejecucion del proceso
    BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => PN_ID_PROCESO,
                                                 PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                 PV_USUARIO          => pv_usuario,
                                                 PV_TRANSACCION      => LV_TRANSACCION,
                                                 PV_ESTADO           => 'P',
                                                 PV_OBSERVACION      =>  '',
                                                 PV_ERROR            => LV_ERROR_BITA);
    IF LV_ERROR_BITA IS NOT NULL THEN
           RAISE LE_ERROR_BITA;
          END IF;  
 
    --
    lv_mensaje_remark := replace(pv_mensaje_remark, '*', ' ');
    lv_salto          := CHR(13);
    lv_fecha          := replace(pv_fecha, '_', ' ');
    lv_fecha          := replace(lv_fecha, '-', '/');
  
    OPEN C_OBTIENE_SNCODE(pn_id_requerimiento);
    FETCH C_OBTIENE_SNCODE
      INTO LN_SNCODE;
    CLOSE C_OBTIENE_SNCODE;
  
    open C_COUNT_FEES(pn_id_requerimiento, lv_mensaje_remark);
    fetch C_COUNT_FEES
      into ln_count_FEES;
    close C_COUNT_FEES;
  
    open C_SUM_FEES(pn_id_requerimiento, lv_mensaje_remark);
    fetch C_SUM_FEES
      into ln_sum_amount;
    close C_SUM_FEES;
  
    
    --
    LV_ASUNTO:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO');
    --
    IF LV_ASUNTO IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL ASUNTO';
      RAISE LE_ERROR;
    END IF;
    --
    LV_REMITENTE:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_REMITENTE');
    --
    IF LV_REMITENTE IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL REMITENTE';
      RAISE LE_ERROR;
    END IF;
    --
    LV_DIRECTORIO_FINAN:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_RUTA_FINAN_FTP');
    --
    IF LV_DIRECTORIO_FINAN IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL DIRECTORIO DE FINAN';
      RAISE LE_ERROR;
    END IF;
    --
    LV_MAIL_CC:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
    --
    IF LV_MAIL_CC IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL MAIL CON COPIA';
      RAISE LE_ERROR;
    END IF;
    --
    LV_MAIL_CCO:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CO');
    --
    IF LV_MAIL_CCO IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL MAIL CON COPIA OCULTA';
      RAISE LE_ERROR;
    END IF;
    --
    LV_SERVIDOR:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IP_FINAN');
    --
    IF LV_SERVIDOR IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA IP DEL SERVIDOR DE FINAN';
      RAISE LE_ERROR;
    END IF;
    --
    LV_TIPO_ENVIO_M:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_TIPO_REGISTRO_M');
    --
    IF LV_TIPO_ENVIO_M IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL TIPO DE ENVIO DE MAIL';
      RAISE LE_ERROR;
    END IF;
    --
    LV_TIPO_ENVIO_A:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_TIPO_REGISTRO_A');
    --
    IF LV_TIPO_ENVIO_A IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL TIPO DE ENVIO DE MAIL ADJUNTO';
      RAISE LE_ERROR;
    END IF;
    --
    LV_CLASE:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_CLASE');
    --
    IF LV_CLASE IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA CLASE PARA EL ENVIO DEL CORREO';
      RAISE LE_ERROR;
    END IF;
    LV_REINTENTOS:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_REINTENTOS');
    --
     IF LV_REINTENTOS IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DE DEL NUMERO DE REINTENTOS PARA EL ENVIO DEL CORREO';
      RAISE LE_ERROR;
    END IF;
    --
    
    LV_ENCABEZADO:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ENCABEZADO');
    --
     IF LV_ENCABEZADO IS NULL THEN
      LV_ERROR:='ERROR NO SE PUDO OBTENER EL PARAMETRO DEL ENCABEZADO PARA EL ENVIO DEL CORREO';
      RAISE LE_ERROR;
    END IF;
    --
    if pv_registros > ln_count_FEES then
      ln_valor_descartados := pv_registros - ln_count_FEES;
      lv_archivo_adjunto   := '</ARCHIVO1=' || pv_archivo ||
                              '_rechazados_' || pn_id_requerimiento ||
                              '.csv' ||
                              '|DIRECTORIO1='|| lv_directorio_finan ||'|/>';
      lv_tipo_envio:=LV_TIPO_ENVIO_A;
    else
       lv_tipo_envio:=LV_TIPO_ENVIO_M;
    end if;
    if ln_sum_amount is null then
      ln_sum_amount := 0;
    end if;
   --

    ---------------------------------------------------------------------------------
    -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
    ---------------------------------------------------------------------------------   
    LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
    LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                               BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                               ';pnIdServicioInformacion=' ||
                               TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                               ';pvParametroBind1=' || pv_usuario || ';';
  
    LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                             PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                             PV_FAULT_CODE               => LV_FAULT_CODE,
                                                             PV_FAULT_STRING             => LV_FAULT_STRING);
  
    --
    LV_FAULT_STRING:='';
    --
    IF LV_FAULT_STRING IS NOT NULL THEN
      lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                            LV_FAULT_STRING;
      RAISE LE_ERROR_SCP;
    END IF;
  
    LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                              PR_RESPUESTA        => LX_XML_EIS1,
                                                              PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                              PV_NAMESPACE        => NULL);
  
    --
    --
    IF LV_DESTINATARIO IS NULL THEN
      lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
      RAISE LE_ERROR_SCP;
    END IF;
    LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
    LV_DESTINATARIO := TRIM(';' FROM LV_DESTINATARIO);
    lv_encabezado   := replace(lv_encabezado, '?', pv_archivo);
    LV_MENSAJE_MAIL :=lv_encabezado ||lv_salto ||lv_salto ||'Resumen de transacci?n:' ||lv_salto||
                      'Registros exitosos: ' ||ln_count_FEES||lv_salto ||
                      'Registros descartados: ' ||ln_valor_descartados ||lv_salto ||
                      'Fecha hora de procesamiento: ' ||to_char(sysdate,'dd/mm/rrrr hh24:mi') ||lv_salto||
                      'Fecha de facturaci?n: ' ||lv_fecha ||lv_salto||
                      'Cantidad d?lares procesados: $' || ln_sum_amount ||lv_salto||
                      'Sncode procesado: ' ||LN_SNCODE ||lv_salto ||
                      'Usuario: ' ||pv_usuario||lv_archivo_adjunto;
    --
    --
    ln_result       := scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => 'BLK_NOTAS_CREDITOS.blp_occ_mail',
                                                                 pd_fechaenvio      => sysdate,
                                                                 pd_fechaexpiracion => sysdate + 1,
                                                                 pv_asunto          => lv_asunto,
                                                                 pv_mensaje         => LV_MENSAJE_MAIL,
                                                                 pv_destinatario    => lv_destinatario||'//'|| lv_mail_cc ||'//' ||lv_mail_cco,
                                                                 pv_remitente       => lv_remitente,
                                                                 pv_tiporegistro    => lv_tipo_envio,
                                                                 pv_clase           => lv_clase,
                                                                 pv_puerto          => lv_puerto,
                                                                 pv_servidor        => lv_servidor,
                                                                 pv_maxintentos     => lv_reintentos,
                                                                 pv_idusuario       => pv_usuario,
                                                                 pv_mensajeretorno  => lv_mensaje_retorno,
                                                                 pn_idsecuencia     => ln_id_secuencia);
  
    --
    commit;
    --
    if lv_mensaje_retorno is not null or ln_result !=0 then
      --
      raise le_error_scp;
     
    end if;
    --
     -- se actualiza la bitacora del proceso 
     BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                    PN_ID_PROCESO       => PN_ID_PROCESO,
                                                    PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                    PV_TRANSACCION      => LV_TRANSACCION,
                                                    PV_ESTADO           => 'F',
                                                    PV_OBSERVACION      =>  'Id Secuencia notificaciones ' ||
                                                                            ln_id_secuencia ||
                                                                            '. Req. Axis ' ||
                                                                            pn_id_requerimiento,
                                                    PV_ERROR            => LV_ERROR_BITA); 
        
  
   
  
  EXCEPTION
     WHEN LE_ERROR_BITA THEN
        PV_ERROR:='NO SE REALIZO EL PROCESO DE BITACORIZACI?N : '||LV_ERROR_BITA || '- ' || LV_REFERENCIA ;
         
     
     WHEN LE_ERROR_SCP THEN
      ROLLBACK;
      PV_ERROR := 'ERROR - ' || LV_REFERENCIA || ' - ' || LV_MENSAJE_RETORNO;
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
      BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                     PN_ID_PROCESO       => PN_ID_PROCESO,
                                                     PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                     PV_TRANSACCION      => LV_TRANSACCION,
                                                     PV_ESTADO           => 'E',
                                                     PV_OBSERVACION      => 'ERROR - '|| LV_MENSAJE_RETORNO,
                                                     PV_ERROR            => LV_ERROR_BITA);
   

     WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR - '|| LV_REFERENCIA || ' - ' || LV_ERROR;
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
       BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                      PN_ID_PROCESO       => PN_ID_PROCESO,
                                                      PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                      PV_TRANSACCION      => LV_TRANSACCION,
                                                      PV_ESTADO           => 'E',
                                                      PV_OBSERVACION      => 'ERROR - '|| LV_ERROR,
                                                      PV_ERROR            => LV_ERROR_BITA);
   

    WHEN OTHERS THEN 
      PV_ERROR:='ERROR - '|| LV_REFERENCIA || ' - ' ||SUBSTR(SQLERRM,1,500);
      --
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
        BLK_NOTAS_CREDITOS.BLP_ACTUALIZA_BITACORA_CRED(PV_USUARIO          => PV_USUARIO,
                                                       PN_ID_PROCESO       => PN_ID_PROCESO,
                                                       PN_ID_REQUERIMIENTO => PN_ID_REQUERIMIENTO,
                                                       PV_TRANSACCION      => LV_TRANSACCION,
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => PV_ERROR,
                                                       PV_ERROR            => LV_ERROR_BITA);
   
 
    
    
    
    
   
    
  end blp_occ_mail;
  ---
  PROCEDURE BLP_VALORES_PARAMETRIZADOS(PV_CODIGO      IN VARCHAR2, --CODIGO DEL ERROR
                                       PV_DESCRIPCION IN VARCHAR2, --PROCESO QUE ORIGINA EL ERROR
                                       PV_VALOR       OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2) IS
  
    CURSOR C_PARAMETRO(CV_DESCRIPCION IN VARCHAR2) IS
      SELECT P.VALOR1, P.VALOR2
        FROM nc_parametros_grales P
       WHERE UPPER(TRIM(P.DESCRIPCION)) = UPPER(TRIM(CV_DESCRIPCION))
         AND P.ESTADO = 'A';
  
    LB_FOUND     BOOLEAN;
    LC_PARAMETRO C_PARAMETRO%ROWTYPE;
    LV_ERROR     VARCHAR2(2000);
    LE_ERROR EXCEPTION;
    LV_PROGRAM VARCHAR2(2000) := 'BLK_NOTAS_CREDITOS.GCP_VALORES_PARAMETRIZADOS';
  BEGIN
    OPEN C_PARAMETRO(PV_CODIGO);
    FETCH C_PARAMETRO
      INTO LC_PARAMETRO;
    LB_FOUND := C_PARAMETRO%FOUND;
    CLOSE C_PARAMETRO;
    PV_VALOR := LC_PARAMETRO.VALOR1 || ' ' || LC_PARAMETRO.VALOR2 || ' -- ' ||
                PV_DESCRIPCION; --DEVUELVE EL MENSAJE TRADUCIDO DEL ERROR JUNTO CON LA DESCRIPCION DEL PROCESO QUE SE CAY?
    IF NOT LB_FOUND THEN
      LV_ERROR := 'NO SE ENCONTRO EL ERROR PARAMETRIZADO EN LA TABLA nc_parametros_grales';
      RAISE LE_ERROR;
    END IF;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAM || ' ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR: ' || LV_PROGRAM || ' ' || SQLERRM;
    
  END BLP_VALORES_PARAMETRIZADOS;
  
  PROCEDURE PR_NC_DEVUELVE_FACTURA(PV_CUENTA       VARCHAR2,
                                   PV_VALOR        VARCHAR2,
                                   PV_FACTURA  OUT VARCHAR2,
                                   PD_FECHA_FA OUT DATE,
                                   PD_FECHA_CORTE OUT DATE,
                                   PV_CIUDAD OUT VARCHAR2,
                                   PV_REGION OUT VARCHAR2,
                                   PV_ERROR    OUT VARCHAR2) is
  
  
  cursor c_fechas_corte(PN_MESES NUMBER) is
   select to_char(add_months(to_date(a.fecha_fin, 'dd/mm/yyyy') + 1,PN_MESES),'ddmmyyyy') fecha_corte
    from (select s.dia_fin_ciclo || to_char(sysdate, '/mm/rrrr') fecha_fin
            from fa_ciclos_bscs s
           where id_ciclo <> '05') a
   /*UNION
    select to_char(to_date(a.fecha_fin, 'dd/mm/yyyy') + 1,'ddmmyyyy') fecha_corte
        from (select s.dia_fin_ciclo || to_char(sysdate, '/mm/rrrr') fecha_fin
                from fa_ciclos_bscs s
               where id_ciclo <> '05') a*/;
  
  CURSOR C_PARAMETROS(PV_CODIGO VARCHAR2) IS 
  SELECT P.VALOR1
        FROM nc_parametros_grales P
       WHERE P.CODIGO = PV_CODIGO
         AND P.ESTADO = 'A';
         
  CURSOR C_DEVUELVE_FECH_EM (PD_FECHA_CORTE date,--FECHA DE CORTE -1
                             PV_FACTURA     VARCHAR2) IS--NUMERO DE LA FACTURA
   select fecha_emision_2 fecha_emision
     from gsi_customer_notifies a
    where fecha_emision = to_char(PD_FECHA_CORTE,'dd/mm/yyyy')--'14/09/2017'
      and ohrefnum=PV_FACTURA;--'001-065-062818405';

  LC_DEVUELVE_FECH_EM   C_DEVUELVE_FECH_EM%rowtype;
  LC_PARAMETROS C_PARAMETROS%ROWTYPE;
  lv_sql  varchar2(4000);
  lv_sql_plant varchar2(4000);
  lv_sql_fact varchar2(4000);
  lv_sql_fact_plant varchar2(4000);
  lv_region varchar2(1000);
  ln_meses  number;
  ln_valor  number;
  ln_valor_nc number;
  lv_factura varchar2(100);
  ld_fecha_fact date;
  le_error exception;
  lv_error varchar2(1000);
  lv_ciudad varchar2(100);

 begin
  
  IF PV_CUENTA IS NULL THEN
    lv_error:='Ingrese la cuenta';
    raise le_error;
  END IF;
  
  IF PV_VALOR IS NULL THEN
    lv_error:='Ingrese el valor';
    raise le_error;
  END IF;

  OPEN C_PARAMETROS('NOT_CRED_MESES_FACT');
  FETCH C_PARAMETROS INTO LC_PARAMETROS;
  CLOSE C_PARAMETROS;

  ln_meses:=TO_NUMBER(LC_PARAMETROS.VALOR1);
  
  if ln_meses is null then
    ln_meses := 2;
  end if;
  
  lv_sql_fact_plant:='select a.ohrefnum factura, a.cccity, a.cost_desc '||
                 ' from co_fact_<MESES> a '||
                ' WHERE CUSTCODE = :cuenta '||
                ' and rownum < 2';
  lv_sql_fact := lv_sql_fact_plant;
  
  lv_sql_plant:='select sum(a.valor - a.descuento) valor '||
  	         ' from co_fact_<MESES> a '||
          ' WHERE CUSTCODE = :cuenta '||
            ' and tipo not like ''%CARG%'' '||
            ' and tipo not like ''%CRED%'' '/*||
            ' and tipo not like ''%IMP%'''*/; --[11554] CCALDERON - COMENTADO POR CAMBIO - 23/02/2018 
  lv_sql:=lv_sql_plant;
  
  for i in 0..ln_meses loop
     for j in c_fechas_corte(i*-1) loop
       ln_valor:= null;
       lv_factura:=null;
       ld_fecha_fact:=null;
       lv_ciudad:=null;
       lv_region:=null;
       
       lv_sql:=lv_sql_plant;
       lv_sql_fact := lv_sql_fact_plant;
       ld_fecha_fact:=to_date(j.fecha_corte,'dd/mm/yyyy');
       
       lv_sql:=replace(lv_sql,'<MESES>',j.fecha_corte);
       lv_sql_fact:=replace(lv_sql_fact,'<MESES>',j.fecha_corte);
       BEGIN
        execute immediate lv_sql into ln_valor using PV_CUENTA;
       EXCEPTION
         WHEN OTHERS THEN
           ln_valor:=0;
       END;
       --ln_valor:=BLK_NOTAS_CREDITOS.blp_verifica_decimal(pv_numero => ln_valor);
       
       ln_valor_nc:=BLK_NOTAS_CREDITOS.blp_verifica_decimal(pv_numero => PV_VALOR);
       
       if ln_valor >= ln_valor_nc then
         execute immediate lv_sql_fact into lv_factura, lv_ciudad, lv_region using PV_CUENTA;--obtengo el numero de factura a aplicar la nc
         exit;
       end if;
       
       
       
     end loop;
     if lv_factura is not null then
       exit;
     end if;
  
  end loop;

  PV_FACTURA := lv_factura;
  
  open C_DEVUELVE_FECH_EM(ld_fecha_fact-1,lv_factura);
   fetch C_DEVUELVE_FECH_EM into lC_DEVUELVE_FECH_EM;
  close C_DEVUELVE_FECH_EM;
  
  --PD_FECHA_FA := nvl(lC_DEVUELVE_FECH_EM.Fecha_Emision,ld_fecha_fact);
  PD_FECHA_FA := lC_DEVUELVE_FECH_EM.Fecha_Emision;
  PD_FECHA_CORTE := ld_fecha_fact;
  PV_CIUDAD := lv_ciudad;
  PV_REGION := lv_region;
  
  commit;
  
  EXCEPTION
    WHEN le_error THEN
      PV_ERROR:= lv_error;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;
    
  end PR_NC_DEVUELVE_FACTURA;
  
  PROCEDURE PR_NC_DEVUELVE_FACT_IECE(PV_CUENTA        IN VARCHAR2,
                                     PV_VALOR         IN VARCHAR2,
                                     PV_FACTURA      OUT VARCHAR2,
                                     PD_FECHA_FA     OUT DATE,
                                     PD_FECHA_CORTE  OUT DATE,
                                     PV_CIUDAD       OUT VARCHAR2,
                                     PV_REGION       OUT VARCHAR2,
                                     PV_CODIGO       OUT NUMBER,
                                     PV_ERROR        OUT VARCHAR2) is
    
    CURSOR C_FECHAS_CORTE(PN_MESES NUMBER) IS
    SELECT TO_CHAR(ADD_MONTHS(TO_DATE(A.FECHA_FIN, 'dd/mm/yyyy') + 1, PN_MESES),
                              'ddmmyyyy') FECHA_CORTE
      FROM (SELECT S.DIA_FIN_CICLO || TO_CHAR(SYSDATE, '/mm/rrrr') FECHA_FIN
              FROM FA_CICLOS_BSCS S
             WHERE ID_CICLO <> '05') A;
    
    CURSOR C_PARAMETROS(PV_CODIGO VARCHAR2) IS 
    SELECT P.VALOR1
      FROM nc_parametros_grales P
     WHERE P.CODIGO = PV_CODIGO
       AND P.ESTADO = 'A';
           
    CURSOR C_DEVUELVE_FECH_EM (PD_FECHA_CORTE DATE,
                               PV_FACTURA     VARCHAR2) IS
    SELECT FECHA_EMISION_2 FECHA_EMISION
      FROM GSI_CUSTOMER_NOTIFIES A
     WHERE FECHA_EMISION = TO_CHAR(PD_FECHA_CORTE,'dd/mm/yyyy')
       AND OHREFNUM=PV_FACTURA;

    LC_DEVUELVE_FECH_EM   C_DEVUELVE_FECH_EM%rowtype;
    LC_PARAMETROS         C_PARAMETROS%ROWTYPE;
    lv_sql_fact           varchar2(4000);
    lv_sql_fact_iece      varchar2(4000);
    lv_region             varchar2(1000);
    lv_error              varchar2(1000);
    lv_ciudad             varchar2(100);
    lv_factura            varchar2(100);
    ln_meses              number;
    ln_valor              number;
    ln_valor_nc           number;
    ld_fecha_fact         date;
    le_error              exception;

  begin
  
    IF PV_CUENTA IS NULL THEN
      lv_error:='Ingrese la cuenta';
      raise le_error;
    END IF;
    
    IF PV_VALOR IS NULL THEN
      lv_error:='Ingrese el valor';
      raise le_error;
    END IF;

     OPEN C_PARAMETROS('NOT_CRED_MESES_FACT');
    FETCH C_PARAMETROS INTO LC_PARAMETROS;
    CLOSE C_PARAMETROS;

    ln_meses:=TO_NUMBER(LC_PARAMETROS.VALOR1);
    
    if ln_meses is null then
      ln_meses := 2;
    end if;
    
    lv_sql_fact_iece:= 'select count(*), b.factura, b.CCCITY, b.COST_DESC '||
                       '  from (select A.OHREFNUM FACTURA, A.CCCITY, A.COST_DESC'||
                       '          from co_fact_<MESES> a '||
                       '         WHERE CUSTCODE = :cuenta '||
                       '           and upper(tipo) like ''%IMPUESTOS%'' '||
                       '           and upper(nombre2) = ''IVA'' '||
                       '        union all '||
                       '        select A.OHREFNUM FACTURA, A.CCCITY, A.COST_DESC'||
                       '          from co_fact_<MESES> a '||
                       '         WHERE CUSTCODE = :cuenta '||
                       '           and upper(tipo) like ''%IMPUESTOS%'''||
                       '           and upper(nombre2) = ''ICE'') b '||
                       ' group by b.factura, b.CCCITY, b.COST_DESC';
                        
    lv_sql_fact := lv_sql_fact_iece;
  
    for i in 0..ln_meses loop
      for j in c_fechas_corte(i*-1) loop
        ln_valor:= null;
        lv_factura:=null;
        ld_fecha_fact:=null;
        lv_ciudad:=null;
        lv_region:=null;
        
        lv_sql_fact := lv_sql_fact_iece;
        ld_fecha_fact:=to_date(j.fecha_corte,'dd/mm/yyyy');
        lv_sql_fact:=replace(lv_sql_fact,'<MESES>',j.fecha_corte);
        
        ln_valor_nc:=BLK_NOTAS_CREDITOS.blp_verifica_decimal(pv_numero => PV_VALOR);
        
        BEGIN
         execute immediate lv_sql_fact into ln_valor, lv_factura, lv_ciudad, lv_region using PV_CUENTA, PV_CUENTA;
         exit;
        EXCEPTION
          WHEN OTHERS THEN
            ln_valor:=0;
        END;
      end loop;
      if lv_factura is not null then
        exit;
      end if;  
    end loop;

    PV_FACTURA := lv_factura;
    
     open C_DEVUELVE_FECH_EM(ld_fecha_fact-1,lv_factura);
    fetch C_DEVUELVE_FECH_EM into lC_DEVUELVE_FECH_EM;
    close C_DEVUELVE_FECH_EM;
    
    PD_FECHA_FA := lC_DEVUELVE_FECH_EM.Fecha_Emision;
    PD_FECHA_CORTE := ld_fecha_fact;
    PV_CIUDAD := lv_ciudad;
    PV_REGION := lv_region;
    PV_CODIGO := ln_valor;
    
    commit;
  
  EXCEPTION
    WHEN le_error THEN
      PV_ERROR:= lv_error;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;    
  end PR_NC_DEVUELVE_FACT_IECE;
  
  FUNCTION BLP_VERIFICA_DECIMAL(PV_NUMERO VARCHAR2 ) return number IS
    lv_numero varchar2(1000);
                           
  BEGIN
    lv_numero:=PV_NUMERO;
    lv_numero:=REPLACE(lv_numero,'.',',');
    
    RETURN TO_NUMBER(lv_numero);
  
  EXCEPTION
    WHEN OTHERS THEN
      lv_numero:=REPLACE(lv_numero,',','.');
      RETURN TO_NUMBER(lv_numero);
  END BLP_VERIFICA_DECIMAL;

/*PROCEDURE PR_NC_DATOS_CAJA(PV_USUARIO          varchar2,
                           PN_REQUERIMIENTO       NUMBER,
                           PV_CIA                 VARCHAR2,
                           PV_BODEGA              VARCHAR2,
                           PV_PRODUCTO_CAJA       VARCHAR2,
                           PV_ERROR               OUT VARCHAR2) is

  cursor c_datos is
    select a.remark, trunc(a.entdate) fecha_bscs
     from NC_CARGA_NOTA_CREDITO a
    where a.id_ejecucion=PN_REQUERIMIENTO ;
  
  cursor c_ciclo(PV_DIA_CORTE VARCHAR2) is 
   select t.id_ciclo
    from FA_CICLOS_BSCS t 
   where t.dia_ini_ciclo=PV_DIA_CORTE;
  
  TYPE LR_NC_DATOS_CAJA IS RECORD(
      ID_REQUERIMIENTO NUMBER,
      CUENTA VARCHAR2(30),
      NOMBRE_CLIENTE VARCHAR2(200),
      DIRECCION_CLIENTE VARCHAR2(200),
      VALOR_SIN_IMPUESTO NUMBER,
      ID_CLIENTE VARCHAR2(50)
      ); 

   TYPE LT_NC_DATOS_CAJA IS TABLE OF LR_NC_DATOS_CAJA INDEX BY BINARY_INTEGER;

    TYPE CURSOR_TYPE IS REF CURSOR;
    LC_NC_DATOS_CAJA CURSOR_TYPE;
    lc_datos c_datos%rowtype;
    Lc_ciclo c_ciclo%rowtype;
    
    LT_NC_DATOS_CAJA_TMP LT_NC_DATOS_CAJA;
    
    LV_QUERY VARCHAR2(4000);
    LV_INSERT VARCHAR2(4000);
    VALOR_IMPUESTO NUMBER:=0;
    ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
    LV_VALOR_SIN_IMP VARCHAR2(100);
    ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos
    IVA NUMBER:=0;
    LN_CANT_REG NUMBER:=0;
    le_error EXCEPTION;
    LV_ERROR VARCHAR2(4000);
    LV_IVA VARCHAR2(100);
    LN_IVA NUMBER;
    LV_FACTURA         VARCHAR2(1000);
    Ld_FECHA_FACT      date;
    Ld_FECHA_fact_cort date;
    LV_REGION          VARCHAR2(1000);
    LV_CIA             VARCHAR2(100);
    lv_ciudad          varchar2(200);
    ln_registros_nc    number;
    lv_error_caja      varchar2(4000);
    ln_error_caja      number;
    ln_contador        number:=0;
    
  BEGIN  
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
   

     --CCSTREETNO
     LV_QUERY:= 'select TMP.ID_REQUERIMIENTO,
           TMP.CUSTCODE,
           CONALL.CCLINE2,
           CONALL.CCSTREET,
           sum(TMP.Amount) amount,
           CONALL.Cssocialsecno
      FROM NC_CARGA_NOT_CRE_TMP TMP, ccontact_all CONALL
     WHERE CONALL.CUSTOMER_ID = TMP.CUSTOMER_ID
       and CONALL.ccbill = ''X''
       AND TMP.ID_REQUERIMIENTO = '||PN_REQUERIMIENTO||
      ' group by TMP.ID_REQUERIMIENTO,
           TMP.CUSTCODE,
           CONALL.CCLINE2,
           CONALL.CCSTREET,
           CONALL.Cssocialsecno';  
       
      LV_INSERT:= 'INSERT INTO NC_DATOS_CAJA
                   (ID_REQUERIMIENTO,
                    CUENTA,
                    FECHA_CREDITO,
                    NOMBRE_CLIENTE,
                    DIRECCION_CLIENTE,
                    DESCRIPCION_CREDITO,
                    FACTURA,
                    ID_CLIENTE,
                    FECHA_FACTURA,
                    CODIGO_CARGA,
                    CIUDAD,
                    PRODUCTO_CAJA,
                    VALOR_CON_IMPUESTO,
                    VALOR_SIN_IMPUESTO,
                    IVA,
                    ICE,
                    USUARIO,
                    CICLO,
                    CIA) '||
                ' VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19)';
    LV_IVA:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IVA');
    LN_IVA:=TO_NUMBER(LV_IVA);
    
    open c_datos;
     fetch c_datos into lc_datos;
    close c_datos;
    
    OPEN c_ciclo(to_char(lc_datos.fecha_bscs+1,'dd'));
     FETCH c_ciclo INTO Lc_ciclo;
    CLOSE c_ciclo;
    
		OPEN  LC_NC_DATOS_CAJA FOR LV_QUERY;
    LOOP 
                
       FETCH LC_NC_DATOS_CAJA BULK COLLECT INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
       EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
                 
       IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN 
            ---
           FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST..LT_NC_DATOS_CAJA_TMP.LAST  LOOP
               LV_FACTURA:= null;
               LD_FECHA_FACT:=null;
               lv_ciudad:=null;
               VALOR_IMPUESTO:=null;
               IVA:=null;
               lv_error_caja:=null;
               lv_region:=null;
               LV_CIA:=NULL;
               Ld_FECHA_fact_cort:=null;
               LV_VALOR_SIN_IMP:=NULL;--[11554] CCALDERON
               
               
                  
               BLK_NOTAS_CREDITOS.pr_nc_devuelve_factura(pv_cuenta => LT_NC_DATOS_CAJA_TMP(IDX).CUENTA,
                                                         pv_valor => abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO),
                                                         pv_factura => LV_FACTURA,
                                                         PD_FECHA_FA => LD_FECHA_FACT,
                                                         pd_fecha_corte => Ld_FECHA_fact_cort,
                                                         pv_ciudad => lv_ciudad,
                                                         pv_region => lv_region,
                                                         pv_error => LV_ERROR);
               
               IF lv_region = 'Guayaquil' then
                 lv_cia:='2';
               elsif lv_region = 'Quito' then
                 lv_cia:='1';
               end if;
               
               if Ld_FECHA_fact_cort >= to_date('01/06/2016','dd/mm/yyyy') and 
                  Ld_FECHA_fact_cort <= to_date('30/06/2017','dd/mm/yyyy') then
                  LN_IVA:=0.14;
               else
                 if Ld_FECHA_fact_cort >= to_date('01/07/2017','dd/mm/yyyy') AND
                   Ld_FECHA_fact_cort <= to_date('31/07/2017','dd/mm/yyyy') THEN
                   
                   if Ld_FECHA_fact_cort = to_date('02/07/2017','dd/mm/yyyy') then
                     LN_IVA := 0.1393548;
                   elsif Ld_FECHA_fact_cort = to_date('08/07/2017','dd/mm/yyyy') then
                     LN_IVA := 0.1354839;
                   elsif Ld_FECHA_fact_cort = to_date('15/07/2017','dd/mm/yyyy') then
                     LN_IVA := 0.1309677;
                   elsif Ld_FECHA_fact_cort = to_date('24/07/2017','dd/mm/yyyy') then
                     LN_IVA := 0.1251613;
                   end if;
                 ELSE
                   LN_IVA:=TO_NUMBER(LV_IVA);    
                 end if;

               end if;
                                
               \*
               ---INI [11554] CLS CCALDERON - Ajuste notas de creditos - COMENTADO  
               VALOR_IMPUESTO:=LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO*LN_IVA+LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO; 
               IVA:=LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO*LN_IVA; 
               ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos - COMENTADO    
               *\
               
               ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
               VALOR_IMPUESTO:=LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
               LN_IVA:=LN_IVA+1;
               
               LV_VALOR_SIN_IMP:= abs(ROUND(VALOR_IMPUESTO / LN_IVA,2));
               
               IVA:=abs(VALOR_IMPUESTO)-LV_VALOR_SIN_IMP;               
               ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos               
               
               EXECUTE IMMEDIATE LV_INSERT
                        USING LT_NC_DATOS_CAJA_TMP(IDX).ID_REQUERIMIENTO,
                              LT_NC_DATOS_CAJA_TMP(IDX).CUENTA, 
                              lc_datos.fecha_bscs - 1,  
                              LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE,      
                              LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE, 
                              lc_datos.remark,
                              LV_FACTURA,
                              LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                              LD_FECHA_FACT,
                              'IN',
                              lv_ciudad,
                              PV_PRODUCTO_CAJA,
                              abs(VALOR_IMPUESTO),
                              abs(LV_VALOR_SIN_IMP),--abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO) --[11554] CLS CCALDERON COMENTADO
                              abs(IVA),
                              0,
                              PV_USUARIO,        ---[11554] CLS CCALDERON - 23/02/2018
                              Lc_ciclo.Id_Ciclo, ---[11554] CLS CCALDERON - 23/02/2018
                              lv_cia;            ---[11554] CLS CCALDERON - 23/02/2018
     
    --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018                         
             --if LV_FACTURA is not null then
    --[11554] FIN CCALDERON - COMENTADO POR CAMBIO - 23/02/2018           
               ln_contador:=ln_contador+1;
    --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018           
            \* ---Para produccion  
               BLK_NOTAS_CREDITOS.pnc_sre_inserta_caja(pv_cia => lv_cia,
                                            pv_cuenta => LT_NC_DATOS_CAJA_TMP(IDX).CUENTA,
                                            pv_fecha_ingreso => lc_datos.fecha_bscs -1,
                                            pv_descripcion => LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE,
                                            pv_direccion => LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE,
                                            pv_observacion => PV_PRODUCTO_CAJA||' '||lc_datos.remark, --[11554] CLS CCALDERON 
                                            pv_no_factura => LV_FACTURA,
                                            pv_no_cliente => LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                                            pv_fechafac => LD_FECHA_FACT,
                                            pv_statusbscs => 'IN',
                                            pv_localidad => lv_ciudad,
                                            pv_producto => PV_PRODUCTO_CAJA,
                                            pn_total => VALOR_IMPUESTO,
                                            pn_totalimponible => LV_VALOR_SIN_IMP,--LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO, --[11554] CLS CCALDERON 
                                            pn_totaliva => IVA,
                                            pn_totalice => 0,
                                            pv_usuario => PV_USUARIO,
                                            pv_ciclo => Lc_ciclo.Id_Ciclo,
                                            pv_ruc => LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                                            pn_error => ln_error_caja,
                                            pv_error => lv_error_caja);*\
               
                 -------------por pruebas
                \* INSERT INTO ops$inventar.IN_CARGA_NOTAS_BSCS_TRX@unigye (
                                  ID_COMPANIA,
                                  CUENTA,
                                  FECHA_INGRESO,
                                  DESCRIPCION,
                                  DIRECCION,
                                  OBSERVACION,
                                  NO_FACTURA,
                                  NO_CLIENTE,
                                  FECHA_FAC,
                                  STATUS_BSCS,
                                  LOCALIDAD,
                                  PRODUCTO,
                                  TOTAL,
                                  TOTAL_IMPONIBLE,
                                  TOTAL_IVA,
                                  TOTAL_ICE,
                                  ESTADO,
                                  USUARIO_CARGA,
                                  FECHA_CARGA,
                                  NOMBRE_ARCHIVO,
                                  CICLO,  --5359
                                  COD_CIA_REL)   --5779
                    VALUES (
                                  lv_cia,
                                  LT_NC_DATOS_CAJA_TMP(IDX).CUENTA,
                                  lc_datos.fecha_bscs -1,--TO_DATE(LV_FECHAINGRESO,'DD/MM/RRRR'),
                                  LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE,
                                  lc_datos.remark,
                                  LV_FACTURA,
                                  LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                                  LD_FECHA_FACT,
                                  'IN',
                                  lv_ciudad,
                                  PV_PRODUCTO_CAJA,
                                  abs(VALOR_IMPUESTO),
                                  abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO),
                                  abs(IVA),
                                  0,
                                  'X',
                                  PV_USUARIO,
                                  SYSDATE,
                                  null,
                                  LC_CICLO.ID_CICLO, --5359
                                  null\*LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE*\);*\
                 
                 \*OPS$INVENTAR.pck_trx_notas_creditos.pnc_inserta_carga_nota_bscs@unigye(pv_compania => lv_cia,
                                                     pv_cuenta => LT_NC_DATOS_CAJA_TMP(IDX).CUENTA,
                                                     pv_fecha_ingreso => lc_datos.fecha_bscs -1,
                                                     pv_descripcion => LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE,
                                                     pv_direccion => LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE,
                                                     pv_observacion => lc_datos.remark,
                                                     pv_no_factura => LV_FACTURA,
                                                     pv_no_cliente => LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                                                     pv_fechafac => LD_FECHA_FACT,
                                                     pv_statusbscs => 'IN',
                                                     pv_localidad => lv_ciudad,
                                                     pv_producto => PV_PRODUCTO_CAJA,
                                                     pn_total => abs(VALOR_IMPUESTO),
                                                     pn_totalimponible => abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO),
                                                     pn_totaliva => abs(IVA),
                                                     pn_totalice => 0,
                                                     pv_usuario => PV_USUARIO,
                                                     pv_ciclo =>  Lc_ciclo.Id_Ciclo,
                                                     pv_ruc => LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                                                     pn_error => ln_error_caja,
                                                     pv_error => lv_error_caja);*\
    --[11554] FIN CCALDERON - COMENTADO POR CAMBIO - 23/02/2018 
    
    --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018            
                 -------------
                \* if lv_error_caja is not null then
                   BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => PN_REQUERIMIENTO,
                                               pn_id_requerimiento => PN_REQUERIMIENTO,
                                               pv_usuario => pv_usuario,
                                               pv_transaccion => 'Error al insertar a Caja SRE cta.'||LT_NC_DATOS_CAJA_TMP(IDX).CUENTA,
                                               pv_estado => 'E',
                                               pv_observacion => lv_error_caja,
                                               pv_error => lv_error);
                 end if;
                end if;
                LN_CANT_REG:= LN_CANT_REG + 1;

                IF (MOD(LN_CANT_REG,500)=0) THEN
                   LN_CANT_REG:=0;
                   COMMIT;
                END IF;           *\    
    --[11554] FIN CCALDERON - COMENTADO POR CAMBIO - 23/02/2018              
            END LOOP; 
                                                  
       END IF;
               
    END LOOP;

    COMMIT;
    CLOSE LC_NC_DATOS_CAJA;
     --lv_error_caja:= null;  --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018

     --A traves de un SRE Se procesan las NC insertadas en GYE
    --[11554] CCALDERON comentado
     \*BLK_NOTAS_CREDITOS.pnc_sre_genera_notas_cred(pv_compania => pv_cia,
                                               pv_usuario => pv_usuario,
                                               pn_bodega => to_number(pv_bodega),
                                               pn_registros => ln_registros_nc,
                                               pv_error => lv_error_caja);*\
    --[11554] FIN CCALDERON
    
    --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018
    \*BLK_NOTAS_CREDITOS.pnc_sre_genera_notas_cred(pv_compania => '1',
                                               pv_usuario => pv_usuario,
                                               pn_bodega => 210,
                                               pn_registros => ln_registros_nc,
                                               pv_error => lv_error_caja);
    
    BLK_NOTAS_CREDITOS.pnc_sre_genera_notas_cred(pv_compania => '2',
                                               pv_usuario => pv_usuario,
                                               pn_bodega => 227,--to_number(pv_bodega),
                                               pn_registros => ln_registros_nc,
                                               pv_error => lv_error_caja);*\
   --[11554] FIN CCALDERON - COMENTADO POR CAMBIO - 23/02/2018                                            
    
     --POR PRUEBAS YA QUE EL SRE NO FUNCIONA
     \*OPS$INVENTAR.pck_trx_notas_creditos.genera_notascredito@unigye(pv_compania => pv_cia,
                                           pv_usuario => to_number(pv_bodega),
                                           pn_bodega => to_number(pv_bodega));*\
   \*  OPS$INVENTAR.in_nota_credito_bscs_prueba.genera_notascredito_prue@unigye(pv_compania => '1',
                                                       pv_usuario => pv_usuario,
                                                       pn_bodega => 210,
                                                       pn_registros => ln_registros_nc);
    OPS$INVENTAR.in_nota_credito_bscs_prueba.genera_notascredito_prue@unigye(pv_compania => '2',
                                                       pv_usuario => pv_usuario,
                                                       pn_bodega => 227,
                                                       pn_registros => ln_registros_nc);*\
    ln_registros_nc:=ln_contador;
                                           
   --[11554] INI CCALDERON - COMENTADO POR CAMBIO - 23/02/2018  
   \* if lv_error_caja is not null then
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => PN_REQUERIMIENTO,
                                                   pn_id_requerimiento => PN_REQUERIMIENTO,
                                                   pv_usuario => pv_usuario,
                                                   pv_transaccion => 'Error al procesar las NC bod:'||pv_bodega||'-cia:'||pv_cia,
                                                   pv_estado => 'E',
                                                   pv_observacion => lv_error_caja,
                                                   pv_error => lv_error);
    else
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => PN_REQUERIMIENTO,
                                                   pn_id_requerimiento => PN_REQUERIMIENTO,
                                                   pv_usuario => pv_usuario,
                                                   pv_transaccion => 'Exito al procesar las NC bod:'||pv_bodega||'-cia:'||pv_cia,
                                                   pv_estado => 'F',
                                                   pv_observacion => 'Se procesaron '||ln_registros_nc||' notas de credito',
                                                   pv_error => lv_error);
    end if;
    commit;*\
    --[11554] FIN CCALDERON - COMENTADO POR CAMBIO - 23/02/2018  

    lv_error_caja:=null;
    
    BLK_NOTAS_CREDITOS.pr_reporte(pn_id_req => PN_REQUERIMIENTO,
                                pv_usuario => pv_usuario,
                                pv_prod => PV_PRODUCTO_CAJA,
                                pn_nc_gen => ln_registros_nc,
                                pv_error => lv_error_caja);
                                
   if lv_error_caja is not null then
     BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => PN_REQUERIMIENTO,
                                                   pn_id_requerimiento => PN_REQUERIMIENTO,
                                                   pv_usuario => pv_usuario,
                                                   pv_transaccion => 'Error al enviar el mail BSCS',
                                                   pv_estado => 'E',
                                                   pv_observacion => lv_error_caja,
                                                   pv_error => lv_error);
   end if;                                
  -------------finaliza
  EXCEPTION
    WHEN le_error THEN
      PV_ERROR:= lv_error;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;
    
  end PR_NC_DATOS_CAJA;*/
  
  PROCEDURE PR_NC_PREVIUS_DATOS_CAJA(PV_USUARIO       VARCHAR2,
                                     PN_REQUERIMIENTO NUMBER,
                                     PV_CIA           VARCHAR2,
                                     PV_BODEGA        VARCHAR2,
                                     PV_PRODUCTO_CAJA VARCHAR2,
                                     PV_ERROR         OUT VARCHAR2) is
  
    CURSOR C_DATOS(CN_ID_EJECUCION NUMBER) IS
      SELECT A.REMARK, TRUNC(A.ENTDATE) FECHA_BSCS
        FROM NC_CARGA_NOTA_CREDITO A
       WHERE A.ID_EJECUCION = CN_ID_EJECUCION;
  
    CURSOR C_CICLO(PV_DIA_CORTE VARCHAR2) IS
      SELECT T.ID_CICLO
        FROM FA_CICLOS_BSCS T
       WHERE T.DIA_INI_CICLO = PV_DIA_CORTE;
  
    --SUD GGO INI 
    CURSOR C_DATOS_IECE(CN_ID_EJECUCION NUMBER) IS
    SELECT A.REMARK, TRUNC(A.ENTDATE) FECHA_BSCS
      FROM BL_CARGA_EJECUCION_V2 A
     WHERE A.ID_EJECUCION = CN_ID_EJECUCION;

    LV_QUERY_IECE      VARCHAR2(4000);
    LV_INSERT_IECE     VARCHAR2(4000);
    LV_BAND            VARCHAR2(20) := 'N';
    -- SUD FIN GGO
    
    TYPE LR_NC_DATOS_CAJA IS RECORD(
      ID_REQUERIMIENTO   NUMBER,
      CUENTA             VARCHAR2(30),
      NOMBRE_CLIENTE     VARCHAR2(200),
      DIRECCION_CLIENTE  VARCHAR2(200),
      VALOR_SIN_IMPUESTO NUMBER,
      ID_CLIENTE         VARCHAR2(50));
  
    TYPE LT_NC_DATOS_CAJA IS TABLE OF LR_NC_DATOS_CAJA INDEX BY BINARY_INTEGER;
  
    TYPE CURSOR_TYPE IS REF CURSOR;
    LC_NC_DATOS_CAJA CURSOR_TYPE;
    lc_datos         c_datos%rowtype;
    Lc_ciclo         c_ciclo%rowtype;
  
    LT_NC_DATOS_CAJA_TMP LT_NC_DATOS_CAJA;
  
    LV_QUERY       VARCHAR2(4000);
    LV_INSERT      VARCHAR2(4000);
    VALOR_IMPUESTO NUMBER := 0;
    ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
    LV_VALOR_SIN_IMP VARCHAR2(100);
    ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos
    IVA NUMBER := 0;
    le_error EXCEPTION;
    LV_ERROR           VARCHAR2(4000);
    LV_FACTURA         VARCHAR2(1000);
    Ld_FECHA_FACT      date;
    Ld_FECHA_fact_cort date;
    LV_REGION          VARCHAR2(1000);
    LV_CIA             VARCHAR2(100);
    lv_ciudad          varchar2(200);
    LV_ERROR_CAJA      varchar2(4000);
    ln_contador        number := 0;
    LV_VARIABLE_NULA   VARCHAR2(20) := NULL;
    LN_ID_SECUENCIA    NUMBER;
  
  BEGIN
  
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  
    --CCSTREETNO
    LV_QUERY := 'select TMP.ID_REQUERIMIENTO,
           TMP.CUSTCODE,
           CONALL.CCLINE2,
           CONALL.ccline3||CONALL.ccline4 CCSTREET,
           sum(TMP.Amount) amount,
           CONALL.Cssocialsecno
      FROM NC_CARGA_NOT_CRE_TMP TMP, ccontact_all CONALL
     WHERE CONALL.CUSTOMER_ID = TMP.CUSTOMER_ID
       and CONALL.ccbill = ''X''
       AND TMP.ID_REQUERIMIENTO = ' || PN_REQUERIMIENTO ||
                ' group by TMP.ID_REQUERIMIENTO,
           TMP.CUSTCODE,
           CONALL.CCLINE2,
           CONALL.ccline3||CONALL.ccline4,
           CONALL.Cssocialsecno';
  
    LV_INSERT := 'INSERT INTO NC_DATOS_CAJA
                   (ID_REQUERIMIENTO,
                    CUENTA,
                    FECHA_CREDITO,
                    NOMBRE_CLIENTE,
                    DIRECCION_CLIENTE,
                    DESCRIPCION_CREDITO,
                    FACTURA,
                    ID_CLIENTE,
                    FECHA_FACTURA,
                    CODIGO_CARGA,
                    CIUDAD,
                    PRODUCTO_CAJA,
                    VALOR_CON_IMPUESTO,
                    VALOR_SIN_IMPUESTO,
                    IVA,
                    ICE,
                    USUARIO,
                    CICLO,
                    CIA,
                    ID_SECUENCIAL) ' ||
                 ' VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20)';
  
    open c_datos(PN_REQUERIMIENTO);
    fetch c_datos
      into lc_datos;
    close c_datos;
  
    OPEN c_ciclo(to_char(lc_datos.fecha_bscs + 1, 'dd'));
    FETCH c_ciclo
      INTO Lc_ciclo;
    CLOSE c_ciclo;
  
    OPEN LC_NC_DATOS_CAJA FOR LV_QUERY; --select req. cuenta nombrecli direccion monto id_cli 
    LOOP
    
      FETCH LC_NC_DATOS_CAJA BULK COLLECT
        INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
      EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
    
      IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN
        ---
        FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST .. LT_NC_DATOS_CAJA_TMP.LAST LOOP
          LV_FACTURA         := null;
          LD_FECHA_FACT      := null;
          lv_ciudad          := null;
          VALOR_IMPUESTO     := null;
          IVA                := null;
          LV_ERROR_CAJA      := null;
          lv_region          := null;
          LV_CIA             := NULL;
          Ld_FECHA_fact_cort := null;
          LV_VALOR_SIN_IMP   := NULL; --[11554] CCALDERON
        
          SELECT S_NC_DATOS_CAJA.NEXTVAL INTO LN_ID_SECUENCIA FROM DUAL;
        
          EXECUTE IMMEDIATE LV_INSERT
            USING LT_NC_DATOS_CAJA_TMP(IDX).ID_REQUERIMIENTO, 
	    LT_NC_DATOS_CAJA_TMP(IDX).CUENTA, lc_datos.fecha_bscs,
	     LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE, 
	     LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE, 
	     lc_datos.remark, LV_FACTURA, LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
	      LD_FECHA_FACT, 'IN', lv_ciudad, PV_PRODUCTO_CAJA, 
	      lv_variable_nula, --abs(VALOR_IMPUESTO),
         abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO), --[11554] CLS CCALDERON COMENTADO
          lv_variable_nula, --abs(IVA),
          0, PV_USUARIO, ---[11554] CLS CCALDERON - 23/02/2018
          Lc_ciclo.Id_Ciclo, ---[11554] CLS CCALDERON - 23/02/2018
          lv_variable_nula, --lv_cia;   ---[11554] CLS CCALDERON - 23/02/2018
          LN_ID_SECUENCIA;
          ln_contador := ln_contador + 1;
        
        END LOOP;
      
      END IF;
    
    END LOOP;
    COMMIT;
    --[12633] SUD GGO INI 
    --Se agrega nuevo bloque para obtener las notas de creditos para IECE
    LV_BAND := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_BAND');
  
    IF NVL(LV_BAND, 'N') = 'S' THEN
      LV_QUERY_IECE := 'select TMP.ID_REQUERIMIENTO,
                               TMP.CUSTCODE,
                               CONALL.CCLINE2,
                               CONALL.ccline3||CONALL.ccline4 CCSTREET,
                               sum(TMP.Amount) amount,
                               CONALL.Cssocialsecno
                          FROM BL_CARGA_OCC_TMP_V2 TMP, ccontact_all CONALL
                         WHERE CONALL.CUSTOMER_ID = TMP.CUSTOMER_ID
                           and CONALL.ccbill = ''X''
                           AND TMP.ID_REQUERIMIENTO = ' || PN_REQUERIMIENTO || '
                           AND TMP.FILTRO_ICE = ''I''
                         group by TMP.ID_REQUERIMIENTO,
                                  TMP.CUSTCODE,
                                  CONALL.CCLINE2,
                                  CONALL.ccline3||CONALL.ccline4,
                                  CONALL.Cssocialsecno';
    
      LV_INSERT_IECE := 'INSERT INTO NC_DATOS_CAJA
                         (ID_REQUERIMIENTO,
                          CUENTA,
                          FECHA_CREDITO,
                          NOMBRE_CLIENTE,
                          DIRECCION_CLIENTE,
                          DESCRIPCION_CREDITO,
                          FACTURA,
                          ID_CLIENTE,
                          FECHA_FACTURA,
                          CODIGO_CARGA,
                          CIUDAD,
                          PRODUCTO_CAJA,
                          VALOR_CON_IMPUESTO,
                          VALOR_SIN_IMPUESTO,
                          IVA,
                          ICE,
                          USUARIO,
                          CICLO,
                          CIA,
                          ID_SECUENCIAL) ' ||
                       ' VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20)';
    
       OPEN C_DATOS_IECE(PN_REQUERIMIENTO);
      FETCH C_DATOS_IECE INTO LC_DATOS;
      CLOSE C_DATOS_IECE;
      
       OPEN C_CICLO(TO_CHAR(LC_DATOS.FECHA_BSCS + 1, 'DD'));
      FETCH C_CICLO INTO LC_CICLO;
      CLOSE C_CICLO;
    
       OPEN LC_NC_DATOS_CAJA FOR LV_QUERY_IECE;
      LOOP    
       FETCH LC_NC_DATOS_CAJA BULK COLLECT
        INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
        EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
      
        IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN
          ---
          FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST .. LT_NC_DATOS_CAJA_TMP.LAST LOOP
            LV_FACTURA         := null;
            LD_FECHA_FACT      := null;
            lv_ciudad          := null;
            VALOR_IMPUESTO     := null;
            IVA                := null;
            LV_ERROR_CAJA      := null;
            lv_region          := null;
            LV_CIA             := NULL;
            Ld_FECHA_fact_cort := null;
            LV_VALOR_SIN_IMP   := NULL; 
          
            SELECT S_NC_DATOS_CAJA.NEXTVAL INTO LN_ID_SECUENCIA FROM DUAL;
          
            EXECUTE IMMEDIATE LV_INSERT_IECE
              USING LT_NC_DATOS_CAJA_TMP(IDX).ID_REQUERIMIENTO, 
                    LT_NC_DATOS_CAJA_TMP(IDX).CUENTA, 
                    lc_datos.fecha_bscs,
                    LT_NC_DATOS_CAJA_TMP(IDX).NOMBRE_CLIENTE, 
                    LT_NC_DATOS_CAJA_TMP(IDX).DIRECCION_CLIENTE, 
                    lc_datos.remark, 
                    LV_FACTURA, 
                    LT_NC_DATOS_CAJA_TMP(IDX).ID_CLIENTE,
                    LD_FECHA_FACT, 
                    'IN', 
                    lv_ciudad, 
                    PV_PRODUCTO_CAJA, 
                    lv_variable_nula,
                    abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO),
                    lv_variable_nula,
                    0, 
                    PV_USUARIO,
                    Lc_ciclo.Id_Ciclo,
                    lv_variable_nula,
                    LN_ID_SECUENCIA;
                    ln_contador := ln_contador + 1;        
          END LOOP;      
        END IF;    
      END LOOP;
      COMMIT;
    END IF;
    -- SUD GGO FIN 
    LV_ERROR_CAJA := null;
  
    BLK_NOTAS_CREDITOS.PR_NOTIFICACION_INSERCION(PN_ID_REQ  => PN_REQUERIMIENTO,
                                                     PV_USUARIO => PV_USUARIO,
                                                     PV_PROD    => PV_PRODUCTO_CAJA,
                                                     PV_ERROR   => LV_ERROR_CAJA);
    IF LV_ERROR_CAJA IS NOT NULL THEN
      BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => PN_REQUERIMIENTO,
                                                   PN_ID_REQUERIMIENTO => PN_REQUERIMIENTO,
                                                   PV_USUARIO          => PV_USUARIO,
                                                   PV_TRANSACCION      => 'Error al enviar el mail BSCS',
                                                   PV_ESTADO           => 'E',
                                                   PV_OBSERVACION      => LV_ERROR_CAJA,
                                                   PV_ERROR            => LV_ERROR);
    END IF;
    CLOSE LC_NC_DATOS_CAJA;
  
  EXCEPTION
    WHEN le_error THEN
      PV_ERROR := lv_error;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
  END PR_NC_PREVIUS_DATOS_CAJA;
  
  PROCEDURE PR_NC_DATOS_CAJA(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_OBTIENE_CICLOS IS
      SELECT DIA_INI_CICLO FROM FA_CICLOS_BSCS WHERE ID_CICLO <> '05';
  
    CURSOR C_DATOS IS
      SELECT A.ID_REQUERIMIENTO    AS ID_REQUERIMIENTO, --ID_EJECUCION
             A.CUENTA              AS CUENTA,
             A.NOMBRE_CLIENTE      AS NOMBRE_CLIENTE,
             A.DIRECCION_CLIENTE   AS DIRECCION_CLIENTE,
             A.VALOR_SIN_IMPUESTO  AS VALOR_SIN_IMPUESTO,
             A.ID_CLIENTE          AS ID_CLIENTE,
             A.FECHA_CREDITO       AS FECHA_CREDITO, --ENTDATE FECHA_BSCS
             A.DESCRIPCION_CREDITO AS DESCRIPCION_CREDITO, --REMARK
             A.PRODUCTO_CAJA       AS PRODUCTO_CAJA,
             A.USUARIO             AS USUARIO,
             A.CICLO               AS CICLO,
             A.ID_SECUENCIAL       AS ID_SECUENCIAL
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
--             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 3;
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4;--HTS Geovanny Barrera 11/09/2018
  
    TYPE LR_NC_DATOS_CAJA IS RECORD(
      ID_REQUERIMIENTO    NUMBER,
      CUENTA              VARCHAR2(30),
      NOMBRE_CLIENTE      VARCHAR2(200),
      DIRECCION_CLIENTE   VARCHAR2(200),
      VALOR_SIN_IMPUESTO  NUMBER,
      ID_CLIENTE          VARCHAR2(50),
      FECHA_CREDITO       DATE, --ENTDATE FECHA_BSCS
      DESCRIPCION_CREDITO VARCHAR2(200), --REMARK
      PRODUCTO_CAJA       VARCHAR2(200),
      USUARIO             VARCHAR2(50),
      CICLO               VARCHAR2(20),
      ID_SECUENCIAL       NUMBER);
  
    TYPE LT_NC_DATOS_CAJA IS TABLE OF LR_NC_DATOS_CAJA INDEX BY BINARY_INTEGER;
  
    LT_NC_DATOS_CAJA_TMP LT_NC_DATOS_CAJA;
    VALOR_IMPUESTO       NUMBER := 0;
    ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
    LV_VALOR_SIN_IMP VARCHAR2(100);
    ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos
    IVA NUMBER := 0;
    IVA1 NUMBER := 0;
    le_error EXCEPTION;
    LV_ERROR           VARCHAR2(4000);
    LV_IVA             VARCHAR2(100);
    LN_IVA             NUMBER;
    LV_FACTURA         VARCHAR2(1000);
    Ld_FECHA_FACT      date;
    Ld_FECHA_fact_cort date;
    LV_REGION          VARCHAR2(1000);
    LV_CIA             VARCHAR2(100);
    lv_ciudad          varchar2(200);
    ln_contador        number := 0;
    LN_DIA             NUMBER := 0;
    LB_BANDERA         BOOLEAN := FALSE;
    LB_TABLA_COFACT    BOOLEAN := FALSE;
    LN_CANT_TABLAS     NUMBER := 0;
    LV_TABLA_COFACT    VARCHAR2(50);
    LN_VERIFICA_ESTADO NUMBER:=0;--HTS GBARRERA
    LE_ERROR_ESTADO    EXCEPTION;--HTS GBARRERA
 
    -- [12633] SUD INI GGO
    -- Cursor para extraer el tipo descuento D devolucion R
    CURSOR TIPO_IECE (CV_CUENTA VARCHAR2) IS
    SELECT F.STATUS_ICE
      FROM SYSADM.BL_CARGA_OCC_TMP_V2 F, NC_DATOS_CAJA P
     WHERE F.ID_REQUERIMIENTO = P.ID_REQUERIMIENTO
       AND P.CUENTA = CV_CUENTA
       AND F.CUSTCODE = P.CUENTA;

    LV_FACTURA_N          VARCHAR2(1000);
    LV_VALOR_ICE          VARCHAR2(100);
    LV_BANDERA_IMP        VARCHAR2(1);
    LV_TIPO               VARCHAR2(1);
    LV_VERIFICADOR        VARCHAR2(5); 
    LV_TIPO_PERSONA       VARCHAR2(5);
    LV_ID_IDENTIFICACION  VARCHAR2(5);
    LV_VALOR_SUB_ICE      NUMBER;
    LV_VALOR_IVA          NUMBER;
    LV_CODIGO             NUMBER;
    LN_ICE                NUMBER;
    LD_FECHA_FACT_N       DATE;
    -- [12633] SUD FIN GGO
  BEGIN
  
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';
    --INI HTS GBARRERA
    SELECT COUNT(1) INTO LN_VERIFICA_ESTADO FROM NC_DATOS_CAJA WHERE ESTADO='P';
    IF LN_VERIFICA_ESTADO = 0 THEN
      RAISE LE_ERROR_ESTADO;
    END IF;
    --FIN HTS GBARRERA
    
    
    SELECT TO_CHAR(SYSDATE-4, 'DD') INTO LN_DIA FROM DUAL;--HTS Geovanny Barrera 11/09/2018
    --SELECT TO_CHAR(SYSDATE-3, 'DD') INTO LN_DIA FROM DUAL;
    FOR I IN C_OBTIENE_CICLOS LOOP
      IF LN_DIA = I.DIA_INI_CICLO THEN
        LB_BANDERA := TRUE;
      END IF;
    END LOOP;
  
    IF LB_BANDERA = TRUE THEN
      SELECT 'CO_FACT_' || LPAD(LN_DIA,2,'0') || TO_CHAR(SYSDATE, 'MMRRRR')
        INTO LV_TABLA_COFACT
        FROM DUAL;
      SELECT COUNT(*)
        INTO LN_CANT_TABLAS
        FROM ALL_OBJECTS A
       WHERE A.OBJECT_TYPE = 'TABLE'
         AND A.OBJECT_NAME = LV_TABLA_COFACT;
      IF LN_CANT_TABLAS > 0 THEN
        LB_TABLA_COFACT := TRUE;
      END IF;
    END IF;
    --Si existe tabla de corte cofact se realiza la inserccion 
    IF LB_TABLA_COFACT = TRUE THEN
    
      LV_IVA := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IVA');
      LN_IVA := TO_NUMBER(LV_IVA);
    
      OPEN C_DATOS;
      LOOP
        FETCH C_DATOS BULK COLLECT
          INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
        EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
      
        IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN
        
          FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST .. LT_NC_DATOS_CAJA_TMP.LAST LOOP
            LV_FACTURA         := null;
            LD_FECHA_FACT      := null;
            lv_ciudad          := null;
            lv_region          := null;
            LV_CIA             := NULL;
            Ld_FECHA_fact_cort := null;
            IVA                := NULL;
            BLK_NOTAS_CREDITOS.pr_nc_devuelve_factura(pv_cuenta      => LT_NC_DATOS_CAJA_TMP(IDX)
                                                                        .CUENTA,
                                                      pv_valor       => abs(LT_NC_DATOS_CAJA_TMP(IDX)
                                                                            .VALOR_SIN_IMPUESTO),
                                                      pv_factura     => LV_FACTURA,
                                                      PD_FECHA_FA    => LD_FECHA_FACT,
                                                      pd_fecha_corte => Ld_FECHA_fact_cort,
                                                      pv_ciudad      => lv_ciudad,
                                                      pv_region      => lv_region,
                                                      pv_error       => LV_ERROR);
          
            IF lv_region = 'Guayaquil' THEN
              lv_cia := '2';
            ELSIF lv_region = 'Quito' THEN
              lv_cia := '1';
            END IF;
          
            IF Ld_FECHA_fact_cort >= to_date('01/06/2016', 'dd/mm/yyyy') AND
               Ld_FECHA_fact_cort <= to_date('30/06/2017', 'dd/mm/yyyy') THEN
              LN_IVA := 0.14;
            ELSE
              IF Ld_FECHA_fact_cort >= to_date('01/07/2017', 'dd/mm/yyyy') AND
                 Ld_FECHA_fact_cort <= to_date('31/07/2017', 'dd/mm/yyyy') THEN
              
                IF Ld_FECHA_fact_cort = to_date('02/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1393548;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('08/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1354839;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('15/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1309677;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('24/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1251613;
                END IF;
              ELSE
                LN_IVA := TO_NUMBER(LV_IVA);
              end if;
            
            end if;
            --[12633] SUD INI GGO Bandera para activar los calculos del ice
            LV_BANDERA_IMP := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_BAND');
            --[12633] SUD FIN GGO
            IF NVL(LV_BANDERA_IMP, 'N') = 'N' THEN 
            ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
            VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
            LN_IVA         := LN_IVA + 1;
          
            LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
          
            IVA1 := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
            ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos               
         
          IF  LD_FECHA_FACT IS NULL OR LD_FECHA_FACT = '' THEN
            
            UPDATE NC_DATOS_CAJA
               SET FACTURA            = LV_FACTURA,
                   FECHA_FACTURA      = LD_FECHA_FACT,
                   CIUDAD             = lv_ciudad,
                   VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                   VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                   IVA                = abs(IVA1),
                   CIA                = lv_cia,
                   ESTADO             = 'D'
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
          
          ELSE
             
            UPDATE NC_DATOS_CAJA
               SET FACTURA            = LV_FACTURA,
                   FECHA_FACTURA      = LD_FECHA_FACT,
                   CIUDAD             = lv_ciudad,
                   VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                   VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                   IVA                = abs(IVA1),
                   CIA                = lv_cia,
                   ESTADO             = 'R'
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
             
          END IF;
            ELSE
              -- [12633] SUD INI GGO
              -- QC para obtener el tipo de cliente           
              SELECT TO_NUMBER(SUBSTR(F.IDENTIFICACION, 3, 1)) VERIFICADOR,
                     F.TIPO_PERSONA,
                     F.ID_IDENTIFICACION INTO LV_VERIFICADOR, LV_TIPO_PERSONA, LV_ID_IDENTIFICACION
                FROM PORTA.CL_PERSONAS@AXIS  F
               WHERE F.ID_PERSONA IN
                     (SELECT R.ID_PERSONA
                        FROM PORTA.CL_CONTRATOS@AXIS  R
                       WHERE R.CODIGO_DOC = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                         AND R.ESTADO = 'A')
                 AND F.ESTADO = 'A';
                 
              IF LV_ID_IDENTIFICACION = 'RUC' AND LV_VERIFICADOR = 9 AND LV_TIPO_PERSONA = 'J' THEN
                LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_15');   
              ELSE
                IF (LV_ID_IDENTIFICACION = 'RUC' OR LV_ID_IDENTIFICACION = 'CED' OR LV_ID_IDENTIFICACION = 'PAS') AND LV_TIPO_PERSONA = 'N' THEN
                  LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_10');
                ELSE
                  LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_00');
                END IF;
              END IF;   
                 
               --QC para saber si es descuento D o devolucion R
               OPEN TIPO_IECE (LT_NC_DATOS_CAJA_TMP(IDX).CUENTA);
              FETCH TIPO_IECE INTO LV_TIPO;
              CLOSE TIPO_IECE;

              BLK_NOTAS_CREDITOS.PR_NC_DEVUELVE_FACT_IECE(PV_CUENTA      => LT_NC_DATOS_CAJA_TMP(IDX).CUENTA, 
                                                          PV_VALOR       => abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO), 
                                                          PV_FACTURA     => LV_FACTURA_N, 
                                                          PD_FECHA_FA    => LD_FECHA_FACT_N, 
                                                          PD_FECHA_CORTE => Ld_FECHA_fact_cort, 
                                                          PV_CIUDAD      => lv_ciudad, 
                                                          PV_REGION      => lv_region,
                                                          PV_CODIGO      => lv_codigo, 
                                                          PV_ERROR       => LV_ERROR);
              IF lv_region = 'Guayaquil' THEN
                lv_cia := '2';
              ELSIF lv_region = 'Quito' THEN
                lv_cia := '1';
              END IF;
              
              IF LV_TIPO = 'R' AND LV_CODIGO = 2 THEN
                VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                LN_IVA         := LN_IVA + 1;
                
                LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                --Calcula el ice
                LV_VALOR_ICE := LV_VALOR_SIN_IMP * (LN_ICE/100);
                --Suma el valor antes de impuesto + ice
                LV_VALOR_SUB_ICE := LV_VALOR_SIN_IMP + LV_VALOR_ICE;
                --Porcentaje de iva
                LN_IVA         := LN_IVA - 1;
                --Calclula el iva
                LV_VALOR_IVA := abs(ROUND(LV_VALOR_SUB_ICE * LN_IVA, 2));
                --Calcula el valor total
                VALOR_IMPUESTO := abs(LV_VALOR_SUB_ICE) + LV_VALOR_IVA;           
           
                IF  LD_FECHA_FACT_N IS NULL OR LD_FECHA_FACT_N = '' THEN                
                  UPDATE NC_DATOS_CAJA
                     SET FACTURA            = LV_FACTURA_N,
                         FECHA_FACTURA      = LD_FECHA_FACT_N,
                         CIUDAD             = lv_ciudad,
                         VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                         VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                         IVA                = abs(LV_VALOR_IVA),
                         ICE                = LV_VALOR_ICE,
                         CIA                = lv_cia,
                         ESTADO             = 'D'                   
                   WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
                   
                  UPDATE BL_CARGA_OCC_TMP_V2 S
                     SET S.FILTRO_ICE = 'F'
                   WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                     AND S.FILTRO_ICE = 'I';              
                ELSE             
                  UPDATE NC_DATOS_CAJA
                     SET FACTURA            = LV_FACTURA_N,
                         FECHA_FACTURA      = LD_FECHA_FACT_N,
                         CIUDAD             = lv_ciudad,
                         VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                         VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                         IVA                = abs(LV_VALOR_IVA),
                         ICE                = LV_VALOR_ICE,
                         CIA                = lv_cia,
                         ESTADO             = 'R'
                   WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
                     
                  UPDATE BL_CARGA_OCC_TMP_V2 S
                     SET S.FILTRO_ICE = 'F'
                   WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                     AND S.FILTRO_ICE = 'I';               
                END IF;
              ELSE 
                IF (LV_TIPO = 'D' OR LV_TIPO = 'R') AND LV_CODIGO = 1 THEN
                  -- Si el producto no esta configurado no se le calcula el ice
                  VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                  LN_IVA         := LN_IVA + 1;
                
                  LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                
                  IVA1 := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
             
                  IF  LD_FECHA_FACT_N IS NULL OR LD_FECHA_FACT_N = '' THEN                  
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA_N,
                           FECHA_FACTURA      = LD_FECHA_FACT_N,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'D'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;

                    UPDATE BL_CARGA_OCC_TMP_V2 S
                       SET S.FILTRO_ICE = 'F'
                     WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                       AND S.FILTRO_ICE = 'I';                
                  ELSE                   
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA_N,
                           FECHA_FACTURA      = LD_FECHA_FACT_N,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'R'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;

                    UPDATE BL_CARGA_OCC_TMP_V2 S
                       SET S.FILTRO_ICE = 'F'
                     WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                       AND S.FILTRO_ICE = 'I';                   
                  END IF;
                ELSE
                  -- SI ES NINGUNO DE LOS ANTERIORES SE REALIZA EL PROCESO NORMAL
                  VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                  LN_IVA         := LN_IVA + 1;
                
                  LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                
                  IVA1 := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
             
                  IF  LD_FECHA_FACT IS NULL OR LD_FECHA_FACT = '' THEN                  
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA,
                           FECHA_FACTURA      = LD_FECHA_FACT,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'D'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;                
                  ELSE                   
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA,
                           FECHA_FACTURA      = LD_FECHA_FACT,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'R'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;                   
                  END IF;
                END IF;
              END IF;
            END IF;
            ln_contador := ln_contador + 1;
          END LOOP;
        
        END IF;
      
      END LOOP;
    
      COMMIT;
      CLOSE C_DATOS;
    /*ELSIF (LN_DIA = 29 OR LN_DIA = 30 OR LN_DIA = 31 ) THEN 
      RETURN;*/

    ELSE
      BEGIN
        /*SELECT TO_CHAR(LN_DIA || '/' ||
                       (SELECT TO_CHAR(SYSDATE, 'MM/RRRR') FROM DUAL))
          INTO LV_FECHA
          FROM DUAL;*/
      
        UPDATE NC_DATOS_CAJA
           SET ESTADO = 'S'
         WHERE --TO_DATE(FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
               --TO_DATE(LV_FECHA, 'DD/MM/RRRR');
               TO_DATE(FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
               TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      COMMIT;
    END IF;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN LE_ERROR_ESTADO THEN--HTS GBARRERA
       NULL;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
    
  end PR_NC_DATOS_CAJA;
  
  PROCEDURE PR_NC_DATOS_CAJA_STANDBY(PV_ERROR OUT VARCHAR2) IS
    CURSOR C_DATOS IS
      SELECT A.ID_REQUERIMIENTO    AS ID_REQUERIMIENTO, --ID_EJECUCION
             A.CUENTA              AS CUENTA,
             A.NOMBRE_CLIENTE      AS NOMBRE_CLIENTE,
             A.DIRECCION_CLIENTE   AS DIRECCION_CLIENTE,
             A.VALOR_SIN_IMPUESTO  AS VALOR_SIN_IMPUESTO,
             A.ID_CLIENTE          AS ID_CLIENTE,
             A.FECHA_CREDITO       AS FECHA_CREDITO, --ENTDATE FECHA_BSCS
             A.DESCRIPCION_CREDITO AS DESCRIPCION_CREDITO, --REMARK
             A.PRODUCTO_CAJA       AS PRODUCTO_CAJA,
             A.USUARIO             AS USUARIO,
             A.CICLO               AS CICLO,
             A.ID_SECUENCIAL       AS ID_SECUENCIAL
        FROM NC_DATOS_CAJA A
       WHERE A.ESTADO = 'S';
  
    CURSOR C_COUNT_DATOS_S IS
      SELECT COUNT(*) FROM NC_DATOS_CAJA A WHERE A.ESTADO = 'S';
    LN_REGISTROS_STANDBY NUMBER;
  
    TYPE LR_NC_DATOS_CAJA IS RECORD(
      ID_REQUERIMIENTO    NUMBER,
      CUENTA              VARCHAR2(30),
      NOMBRE_CLIENTE      VARCHAR2(200),
      DIRECCION_CLIENTE   VARCHAR2(200),
      VALOR_SIN_IMPUESTO  NUMBER,
      ID_CLIENTE          VARCHAR2(50),
      FECHA_CREDITO       DATE, --ENTDATE FECHA_BSCS
      DESCRIPCION_CREDITO VARCHAR2(200), --REMARK
      PRODUCTO_CAJA       VARCHAR2(200),
      USUARIO             VARCHAR2(50),
      CICLO               VARCHAR2(20),
      ID_SECUENCIAL       NUMBER);
  
    TYPE LT_NC_DATOS_CAJA IS TABLE OF LR_NC_DATOS_CAJA INDEX BY BINARY_INTEGER;
  
    LT_NC_DATOS_CAJA_TMP LT_NC_DATOS_CAJA;
    VALOR_IMPUESTO       NUMBER := 0;
    ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
    LV_VALOR_SIN_IMP VARCHAR2(100);
    ---FIN [11554] CLS CCALDERON - Ajuste notas de creditos
    IVA NUMBER := 0;
    IVA1 NUMBER := 0;
    le_error EXCEPTION;
    LV_ERROR           VARCHAR2(4000);
    LV_IVA             VARCHAR2(100);
    LN_IVA             NUMBER;
    LV_FACTURA         VARCHAR2(1000);
    Ld_FECHA_FACT      date;
    Ld_FECHA_fact_cort date;
    LV_REGION          VARCHAR2(1000);
    LV_CIA             VARCHAR2(100);
    lv_ciudad          varchar2(200);
    ln_contador        number := 0;
  
    -- [12633] SUD INI GGO
    -- Cursor para extraer el tipo descuento D devolucion R
    CURSOR TIPO_IECE (CV_CUENTA VARCHAR2) IS
    SELECT F.STATUS_ICE
      FROM SYSADM.BL_CARGA_OCC_TMP_V2 F, NC_DATOS_CAJA P
     WHERE F.ID_REQUERIMIENTO = P.ID_REQUERIMIENTO
       AND P.CUENTA = CV_CUENTA
       AND F.CUSTCODE = P.CUENTA;

    LV_FACTURA_N            VARCHAR2(1000);
    LV_VALOR_ICE            VARCHAR2(100);
    LV_BANDERA_IMP          VARCHAR2(1);
    LV_TIPO                 VARCHAR2(1);
    LV_VERIFICADOR          VARCHAR2(5); 
    LV_TIPO_PERSONA         VARCHAR2(5);
    LV_ID_IDENTIFICACION    VARCHAR2(5);
    LV_VALOR_SUB_ICE        NUMBER;
    LV_VALOR_IVA            NUMBER;
    LV_CODIGO               NUMBER;
    LN_ICE                  NUMBER;
    LD_FECHA_FACT_N         DATE;
    -- [12633] SUD FIN GGO

  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';
    
    OPEN C_COUNT_DATOS_S;
    FETCH C_COUNT_DATOS_S
      INTO LN_REGISTROS_STANDBY;
    CLOSE C_COUNT_DATOS_S;
  
    IF LN_REGISTROS_STANDBY > 0 THEN
      LV_IVA := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IVA');
      LN_IVA := TO_NUMBER(LV_IVA);
    
      OPEN C_DATOS;
      LOOP
        FETCH C_DATOS BULK COLLECT
          INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
        EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
      
        IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN
        
          FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST .. LT_NC_DATOS_CAJA_TMP.LAST LOOP
            LV_FACTURA         := null;
            LD_FECHA_FACT      := null;
            lv_ciudad          := null;
            lv_region          := null;
            LV_CIA             := NULL;
            Ld_FECHA_fact_cort := null;
          
            BLK_NOTAS_CREDITOS.pr_nc_devuelve_factura(pv_cuenta      => LT_NC_DATOS_CAJA_TMP(IDX)
                                                                        .CUENTA,
                                                      pv_valor       => abs(LT_NC_DATOS_CAJA_TMP(IDX)
                                                                            .VALOR_SIN_IMPUESTO),
                                                      pv_factura     => LV_FACTURA,
                                                      PD_FECHA_FA    => LD_FECHA_FACT,
                                                      pd_fecha_corte => Ld_FECHA_fact_cort,
                                                      pv_ciudad      => lv_ciudad,
                                                      pv_region      => lv_region,
                                                      pv_error       => LV_ERROR);
          
            IF lv_region = 'Guayaquil' THEN
              lv_cia := '2';
            ELSIF lv_region = 'Quito' THEN
              lv_cia := '1';
            END IF;
          
            IF Ld_FECHA_fact_cort >= to_date('01/06/2016', 'dd/mm/yyyy') AND
               Ld_FECHA_fact_cort <= to_date('30/06/2017', 'dd/mm/yyyy') THEN
              LN_IVA := 0.14;
            ELSE
              IF Ld_FECHA_fact_cort >= to_date('01/07/2017', 'dd/mm/yyyy') AND
                 Ld_FECHA_fact_cort <= to_date('31/07/2017', 'dd/mm/yyyy') THEN
              
                IF Ld_FECHA_fact_cort = to_date('02/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1393548;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('08/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1354839;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('15/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1309677;
                ELSIF Ld_FECHA_fact_cort =
                      to_date('24/07/2017', 'dd/mm/yyyy') THEN
                  LN_IVA := 0.1251613;
                END IF;
              ELSE
                LN_IVA := TO_NUMBER(LV_IVA);
              end if;
            end if;
            --[12633] SUD INI GGO Bandera para activar los calculos del ice
            LV_BANDERA_IMP := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_BAND');
            --[12633] SUD FIN GGO
            IF NVL(LV_BANDERA_IMP, 'N') = 'N' THEN 
            ---INI [11554] CLS CCALDERON - Ajuste notas de creditos
            VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
            LN_IVA         := LN_IVA + 1;
          
            LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
            IVA1              := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
          
          
            IF LD_FECHA_FACT IS NULL OR LD_FECHA_FACT = '' THEN
            UPDATE NC_DATOS_CAJA
               SET FACTURA            = LV_FACTURA,
                   FECHA_FACTURA      = LD_FECHA_FACT,
                   CIUDAD             = lv_ciudad,
                   VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                   VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                   IVA                = abs(IVA1),
                   CIA                = lv_cia,
                   ESTADO             = 'D'
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
            
            ELSE 
            UPDATE NC_DATOS_CAJA
               SET FACTURA            = LV_FACTURA,
                   FECHA_FACTURA      = LD_FECHA_FACT,
                   CIUDAD             = lv_ciudad,
                   VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                   VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                   IVA                = abs(IVA1),
                   CIA                = lv_cia,
                   ESTADO             = 'R'
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
          END IF;
            ELSE
              -- [12633] SUD INI GGO
              -- QC para obtener el tipo de cliente           
              SELECT TO_NUMBER(SUBSTR(F.IDENTIFICACION, 3, 1)) VERIFICADOR,
                     F.TIPO_PERSONA,
                     F.ID_IDENTIFICACION INTO LV_VERIFICADOR, LV_TIPO_PERSONA, LV_ID_IDENTIFICACION
                FROM PORTA.CL_PERSONAS@AXIS  F
               WHERE F.ID_PERSONA IN
                     (SELECT R.ID_PERSONA
                        FROM PORTA.CL_CONTRATOS@AXIS  R
                       WHERE R.CODIGO_DOC = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                         AND R.ESTADO = 'A')
                 AND F.ESTADO = 'A';
                 
              IF LV_ID_IDENTIFICACION = 'RUC' AND LV_VERIFICADOR = 9 AND LV_TIPO_PERSONA = 'J' THEN
                LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_15');   
              ELSE
                IF (LV_ID_IDENTIFICACION = 'RUC' OR LV_ID_IDENTIFICACION = 'CED' OR LV_ID_IDENTIFICACION = 'PAS') AND LV_TIPO_PERSONA = 'N' THEN
                  LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_10');
                ELSE
                  LN_ICE := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_IECE_00');
                END IF;
              END IF;   
                 
               --QC para saber si es descuento D o devolucion R
               OPEN TIPO_IECE (LT_NC_DATOS_CAJA_TMP(IDX).CUENTA);
              FETCH TIPO_IECE INTO LV_TIPO;
              CLOSE TIPO_IECE;

              BLK_NOTAS_CREDITOS.PR_NC_DEVUELVE_FACT_IECE(PV_CUENTA      => LT_NC_DATOS_CAJA_TMP(IDX).CUENTA, 
                                                          PV_VALOR       => abs(LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO), 
                                                          PV_FACTURA     => LV_FACTURA_N, 
                                                          PD_FECHA_FA    => LD_FECHA_FACT_N, 
                                                          PD_FECHA_CORTE => Ld_FECHA_fact_cort, 
                                                          PV_CIUDAD      => lv_ciudad, 
                                                          PV_REGION      => lv_region,
                                                          PV_CODIGO      => lv_codigo, 
                                                          PV_ERROR       => LV_ERROR);
              IF lv_region = 'Guayaquil' THEN
                lv_cia := '2';
              ELSIF lv_region = 'Quito' THEN
                lv_cia := '1';
              END IF;
              
              IF LV_TIPO = 'R' AND lv_codigo = 2 THEN
                VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                LN_IVA         := LN_IVA + 1;
                
                LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                --Calcula el ice
                LV_VALOR_ICE := LV_VALOR_SIN_IMP * (LN_ICE/100);
                --Suma el valor antes de impuesto + ice
                LV_VALOR_SUB_ICE := LV_VALOR_SIN_IMP + LV_VALOR_ICE;
                --Porcentaje de iva
                LN_IVA         := LN_IVA - 1;
                --Calclula el iva
                LV_VALOR_IVA := abs(ROUND(LV_VALOR_SUB_ICE * LN_IVA, 2));
                --Calcula el valor total
                VALOR_IMPUESTO := abs(LV_VALOR_SUB_ICE) + LV_VALOR_IVA;           
           
                IF  LD_FECHA_FACT_N IS NULL OR LD_FECHA_FACT_N = '' THEN                
                  UPDATE NC_DATOS_CAJA
                     SET FACTURA            = LV_FACTURA_N,
                         FECHA_FACTURA      = LD_FECHA_FACT_N,
                         CIUDAD             = lv_ciudad,
                         VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                         VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                         IVA                = abs(LV_VALOR_IVA),
                         ICE                = LV_VALOR_ICE,
                         CIA                = lv_cia,
                         ESTADO             = 'D'                   
                   WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;              
                   
                  UPDATE BL_CARGA_OCC_TMP_V2 S
                     SET S.FILTRO_ICE = 'F'
                   WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                     AND S.FILTRO_ICE = 'I';              
                ELSE             
                  UPDATE NC_DATOS_CAJA
                     SET FACTURA            = LV_FACTURA_N,
                         FECHA_FACTURA      = LD_FECHA_FACT_N,
                         CIUDAD             = lv_ciudad,
                         VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                         VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                         IVA                = abs(LV_VALOR_IVA),
                         ICE                = LV_VALOR_ICE,
                         CIA                = lv_cia,
                         ESTADO             = 'R'
                   WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
                   
                  UPDATE BL_CARGA_OCC_TMP_V2 S
                     SET S.FILTRO_ICE = 'F'
                   WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                     AND S.FILTRO_ICE = 'I';
                END IF;
              ELSE 
                IF (LV_TIPO = 'D' OR LV_TIPO = 'R') AND LV_CODIGO = 1 THEN
                  -- Si el producto no esta configurado no se le calcula el ice
                  VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                  LN_IVA         := LN_IVA + 1;
                
                  LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                
                  IVA1 := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
             
                  IF  LD_FECHA_FACT_N IS NULL OR LD_FECHA_FACT_N = '' THEN                  
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA_N,
                           FECHA_FACTURA      = LD_FECHA_FACT_N,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'D'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;                
                   
                  UPDATE BL_CARGA_OCC_TMP_V2 S
                     SET S.FILTRO_ICE = 'F'
                   WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                     AND S.FILTRO_ICE = 'I';               
                  ELSE                   
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA_N,
                           FECHA_FACTURA      = LD_FECHA_FACT_N,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'R'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
                     
                     UPDATE BL_CARGA_OCC_TMP_V2 S
                        SET S.FILTRO_ICE = 'F'
                      WHERE S.CUSTCODE = LT_NC_DATOS_CAJA_TMP(IDX).CUENTA
                        AND S.FILTRO_ICE = 'I';
                                        
                  END IF;
                ELSE
                    -- Si el producto no esta configurado no se le calcula el ice
                  VALOR_IMPUESTO := LT_NC_DATOS_CAJA_TMP(IDX).VALOR_SIN_IMPUESTO;
                  LN_IVA         := LN_IVA + 1;
                
                  LV_VALOR_SIN_IMP := abs(ROUND(VALOR_IMPUESTO / LN_IVA, 2));
                
                  IVA1 := abs(VALOR_IMPUESTO) - LV_VALOR_SIN_IMP;
             
                  IF  LD_FECHA_FACT IS NULL OR LD_FECHA_FACT = '' THEN                  
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA,
                           FECHA_FACTURA      = LD_FECHA_FACT,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'D'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;                
                  ELSE                   
                    UPDATE NC_DATOS_CAJA
                       SET FACTURA            = LV_FACTURA,
                           FECHA_FACTURA      = LD_FECHA_FACT,
                           CIUDAD             = lv_ciudad,
                           VALOR_CON_IMPUESTO = abs(VALOR_IMPUESTO),
                           VALOR_SIN_IMPUESTO = abs(LV_VALOR_SIN_IMP),
                           IVA                = abs(IVA1),
                           CIA                = lv_cia,
                           ESTADO             = 'R'
                     WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;                   
                  END IF;
                END IF;
              END IF;
            END IF;
            LN_CONTADOR := LN_CONTADOR + 1;
          END LOOP;
        END IF;
      END LOOP;
      COMMIT;
      CLOSE C_DATOS;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
  end PR_NC_DATOS_CAJA_STANDBY;
  
  PROCEDURE PR_NC_DATOS_CAJA_SIN_DOC(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_DATOS IS
      SELECT A.ID_REQUERIMIENTO    AS ID_REQUERIMIENTO, --ID_EJECUCION
             A.CUENTA              AS CUENTA,
             A.NOMBRE_CLIENTE      AS NOMBRE_CLIENTE,
             A.DIRECCION_CLIENTE   AS DIRECCION_CLIENTE,
             A.FACTURA             AS FACTURA,
             A.VALOR_SIN_IMPUESTO  AS VALOR_SIN_IMPUESTO,
             A.ID_CLIENTE          AS ID_CLIENTE,
             A.FECHA_CREDITO       AS FECHA_CREDITO, --ENTDATE FECHA_BSCS
             A.DESCRIPCION_CREDITO AS DESCRIPCION_CREDITO, --REMARK
             A.PRODUCTO_CAJA       AS PRODUCTO_CAJA,
             A.USUARIO             AS USUARIO,
             A.CICLO               AS CICLO,
             A.ID_SECUENCIAL       AS ID_SECUENCIAL
        FROM NC_DATOS_CAJA A
       WHERE A.ESTADO = 'D';--HTS Geovanny Barrera 11/09/2018
  
    TYPE LR_NC_DATOS_CAJA IS RECORD(
      ID_REQUERIMIENTO    NUMBER,
      CUENTA              VARCHAR2(30),
      NOMBRE_CLIENTE      VARCHAR2(200),
      DIRECCION_CLIENTE   VARCHAR2(200),
      FACTURA           VARCHAR2(200),
      VALOR_SIN_IMPUESTO    NUMBER,
      ID_CLIENTE          VARCHAR2(50),
      FECHA_CREDITO       DATE, --ENTDATE FECHA_BSCS
      DESCRIPCION_CREDITO VARCHAR2(200), --REMARK
      PRODUCTO_CAJA       VARCHAR2(200),
      USUARIO             VARCHAR2(50),
      CICLO               VARCHAR2(20),
      ID_SECUENCIAL       NUMBER);
  
    TYPE LT_NC_DATOS_CAJA IS TABLE OF LR_NC_DATOS_CAJA INDEX BY BINARY_INTEGER;
  
    LT_NC_DATOS_CAJA_TMP LT_NC_DATOS_CAJA;
    le_error EXCEPTION;
    LV_ERROR           VARCHAR2(4000);
    ln_contador        number := 0;
    LN_VERIFICA_ESTADO NUMBER:=0;--HTS GBARRERA
    LE_ERROR_ESTADO    EXCEPTION;--HTS GBARRERA
    
    CURSOR C_DEVUELVE_FECH_EM (PD_FECHA_CORTE date,--FECHA DE CORTE -1
                             PV_FACTURA     VARCHAR2) IS--NUMERO DE LA FACTURA
    select fecha_emision_2 fecha_emision
     from gsi_customer_notifies a
     where fecha_emision = to_char(PD_FECHA_CORTE,'dd/mm/yyyy')--'14/09/2017'
     and ohrefnum=PV_FACTURA;--'001-065-062818405'; 
     
     LC_DEVUELVE_FECH_EM   C_DEVUELVE_FECH_EM%rowtype;

  BEGIN
  
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';
    --INI HTS GBARRERA
    SELECT COUNT(1) INTO LN_VERIFICA_ESTADO FROM NC_DATOS_CAJA WHERE ESTADO = 'D';
    IF LN_VERIFICA_ESTADO = 0 THEN
      RAISE LE_ERROR_ESTADO;
    END IF;
    --FIN HTS GBARRERA
  
      OPEN C_DATOS;
      LOOP
        FETCH C_DATOS BULK COLLECT
          INTO LT_NC_DATOS_CAJA_TMP LIMIT 1000;
        EXIT WHEN LT_NC_DATOS_CAJA_TMP.COUNT = 0;
      
        IF LT_NC_DATOS_CAJA_TMP.COUNT > 0 THEN
        
          FOR IDX IN LT_NC_DATOS_CAJA_TMP.FIRST .. LT_NC_DATOS_CAJA_TMP.LAST LOOP
            
            OPEN C_DEVUELVE_FECH_EM(TO_DATE(LT_NC_DATOS_CAJA_TMP(IDX).FECHA_CREDITO,'DD/MM/RRRR') -1,
                                   LT_NC_DATOS_CAJA_TMP(IDX).FACTURA);
            FETCH C_DEVUELVE_FECH_EM 
               INTO LC_DEVUELVE_FECH_EM;
            CLOSE C_DEVUELVE_FECH_EM;
                
          ---CONDICION EN DONDE SE DETECTA SI YA EXISTE O NO DOCUMENTO DE LA FACTURA 
          IF LC_DEVUELVE_FECH_EM.FECHA_EMISION IS NULL OR LC_DEVUELVE_FECH_EM.FECHA_EMISION = '' THEN 
           UPDATE NC_DATOS_CAJA
               SET FECHA_FACTURA      = LC_DEVUELVE_FECH_EM.FECHA_EMISION,
                   ESTADO             = 'D'  ----DOCUMENTO AUN NO ENCONTRADO
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
          ELSE
            UPDATE NC_DATOS_CAJA
               SET FECHA_FACTURA      = LC_DEVUELVE_FECH_EM.FECHA_EMISION,
                   ESTADO             = 'C'  ----DOCUMENTO CREADO
             WHERE ID_SECUENCIAL = LT_NC_DATOS_CAJA_TMP(IDX).ID_SECUENCIAL;
          END IF;
            ln_contador := ln_contador + 1;
          END LOOP;
        END IF;
      END LOOP;
      COMMIT;
      CLOSE C_DATOS;
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN LE_ERROR_ESTADO THEN--HTS GBARRERA
       NULL;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
  END PR_NC_DATOS_CAJA_SIN_DOC;
  
  FUNCTION FNC_DATO_TRAMA_CRED(PV_TRAMA     IN VARCHAR2,
                        PV_DATO      IN VARCHAR2,
                        PV_SEPARATOR IN VARCHAR2 DEFAULT ';') RETURN VARCHAR2 IS
  
    LN_POSINI PLS_INTEGER;
    LN_POSFIN PLS_INTEGER;
    LV_DUMMY  VARCHAR2(4000);
    NTH       NUMBER := 1;
  
  BEGIN
  
    IF NTH < 0 THEN
      LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', NTH);
    ELSE
      LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', 1, NTH);
    END IF;
  
    IF LN_POSINI > 0 THEN
      LV_DUMMY  := SUBSTR(PV_TRAMA, LN_POSINI);
      LN_POSFIN := INSTR(LV_DUMMY, PV_SEPARATOR);
      IF LN_POSFIN > 0 THEN
        LV_DUMMY := SUBSTR(LV_DUMMY, 1, LN_POSFIN - 1);
      END IF;
      LV_DUMMY := SUBSTR(LV_DUMMY, INSTR(LV_DUMMY, '=') + 1);
    END IF;
  
    RETURN LV_DUMMY;
  
  END FNC_DATO_TRAMA_CRED;
  --
  PROCEDURE PNC_SRE_GENERA_NOTAS_CRED(pv_compania  IN VARCHAR2,
                                      pv_usuario   IN VARCHAR2,
                                      pn_bodega    IN NUMBER ,
                                      pn_registros OUT NUMBER,
                                      pv_error     OUT VARCHAR2) IS
  
  LV_ERROR_PARAM VARCHAR2(4000);
  LE_ERROR_PARA  EXCEPTION; 
  lv_id_req      VARCHAR2(4000);
  lv_data_source VARCHAR2(4000);
  lv_instancia   VARCHAR2(4000);
  lv_id_tipo_trans VARCHAR2(4000);
  lv_id_integrador VARCHAR2(4000);
  lv_cod_alterno_dist VARCHAR2(4000);
  LE_ERROR            EXCEPTION;
  LV_ERROR             VARCHAR2(4000);
  --LV_ERROR_SRE        VARCHAR2(4000);
  LE_ERROR_SRE        EXCEPTION;
  LX_RESPUESTA                XMLTYPE;
  LV_FAULT_CODE               VARCHAR2(1000);
  LV_FAULT_STRING             VARCHAR2(1000);
  LV_REGISTROS      VARCHAR2(1000);
  lv_trama       VARCHAR2(4000);
  LV_SEPARADOR_TRAMA VARCHAR2(1) :=';';
  
  BEGIN
   ------------------------
      -- LEO LOS PARAMETROS
   ------------------------
     --
      lv_id_req:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_001_REG');

      IF lv_id_req IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID_REQUERIMIENTO - ';
        RAISE LE_ERROR_PARA;
      END IF;
     --
     lv_data_source:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_002_REG');

      IF lv_data_source IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DATA SOURCE - ';
        RAISE LE_ERROR_PARA;
      END IF;  
     --
     lv_instancia:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_003_REG');

      IF lv_instancia IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID_INSTANCIA';
        RAISE LE_ERROR_PARA;
      END IF;  
     --
     lv_id_tipo_trans:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_004_REG');

      IF lv_id_tipo_trans IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID TIPO TRANSACCION';
        RAISE LE_ERROR_PARA;
      END IF; 
     --
    --
     lv_id_integrador:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_005_REG');

      IF lv_id_integrador IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID INTEGRADOR';
        RAISE LE_ERROR_PARA;
      END IF; 
     --
     lv_cod_alterno_dist:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_006_REG');

      IF lv_cod_alterno_dist IS  NULL THEN
        LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO CODIGO ALTERNO';
        RAISE LE_ERROR_PARA;
      END IF; 
     --
     ------------------------------------
  -- Armo la Trama que consume el SRE
  ------------------------------------
      lv_trama := 'Id_Instancia=' || lv_instancia || LV_SEPARADOR_TRAMA ||
                  'Id_Tipo_Transaccion=' || lv_id_tipo_trans || LV_SEPARADOR_TRAMA ||
                  'Id_Integrador=' || lv_id_integrador || LV_SEPARADOR_TRAMA ||
                  'Cod_Alterno_Distribuidor=' || lv_cod_alterno_dist || LV_SEPARADOR_TRAMA ||
                  'Data_Source=' || lv_data_source || LV_SEPARADOR_TRAMA ||
                  'PV_COMPANIA_NC=' ||  pv_compania|| LV_SEPARADOR_TRAMA ||
                  'PV_USUARIO_NC=' ||  pv_usuario || LV_SEPARADOR_TRAMA||
                  'PN_BODEGA_NC=' ||  pn_bodega|| LV_SEPARADOR_TRAMA ;

     --
       lx_respuesta := scp_dat.sck_soap_gtw.scp_consume_servicio(pn_id_req                   => to_number(lv_id_req),
                                                               pv_parametros_requerimiento => lv_trama,
                                                               pv_fault_code               => lv_fault_code,
                                                               pv_fault_string             => lv_fault_string);
     
   --  
     lv_error := scp_dat.sck_soap_gtw.scp_obtener_valor(pn_id_req           => to_number(lv_id_req),
                                                        pr_respuesta        => lx_respuesta,
                                                        pv_nombre_parametro => 'MENSAJE');
     
   --
     LV_REGISTROS := scp_dat.sck_soap_gtw.scp_obtener_valor(pn_id_req           => to_number(lv_id_req),
                                                               pr_respuesta        => lx_respuesta,
                                                               pv_nombre_parametro => 'PN_REGISTROS_NC');
     

   pn_registros:=TO_NUMBER(LV_REGISTROS);
   
   EXCEPTION
     WHEN LE_ERROR_PARA THEN
       PV_ERROR := 'ERROR - '||LV_ERROR_PARAM;
     WHEN LE_ERROR THEN
       PV_ERROR := 'ERROR - '||LV_ERROR;
      
     WHEN OTHERS THEN
       PV_ERROR := 'ERROR - '||SUBSTR(SQLERRM,1,500); 
  END PNC_SRE_GENERA_NOTAS_CRED;  

 PROCEDURE PNC_SRE_INSERTA_CAJA( PV_CIA               IN  VARCHAR2,
                                 PV_CUENTA            IN  VARCHAR2,
                                 PV_FECHA_INGRESO     IN  VARCHAR2,
                                 PV_DESCRIPCION       IN  VARCHAR2,
                                 PV_DIRECCION         IN  VARCHAR2,
                                 PV_OBSERVACION       IN  VARCHAR2,
                                 PV_NO_FACTURA        IN  VARCHAR2,
                                 PV_NO_CLIENTE        IN  VARCHAR2,
                                 PV_FECHAFAC          IN  VARCHAR2,
                                 PV_STATUSBSCS        IN  VARCHAR2,
                                 PV_LOCALIDAD         IN  VARCHAR2,
                                 PV_PRODUCTO          IN  VARCHAR2,
                                 PN_TOTAL             IN  NUMBER,
                                 PN_TOTALIMPONIBLE    IN  NUMBER,
                                 PN_TOTALIVA          IN  NUMBER,
                                 PN_TOTALICE          IN  NUMBER,
                                 PV_USUARIO           IN  VARCHAR2,
                                 PV_CICLO             IN  VARCHAR2,
                                 PV_RUC               IN  VARCHAR2,
                                 pn_error              OUT NUMBER,
                                 pv_error     OUT VARCHAR2) IS                               
    
    LV_ERROR_PARAM VARCHAR2(4000);
    LE_ERROR_PARA  EXCEPTION; 
    lv_id_req      VARCHAR2(4000);
    lv_data_source VARCHAR2(4000);
    lv_instancia   VARCHAR2(4000);
    lv_id_tipo_trans VARCHAR2(4000);
    lv_id_integrador VARCHAR2(4000);
    lv_cod_alterno_dist VARCHAR2(4000);
    LE_ERROR            EXCEPTION;
    LV_ERROR             VARCHAR2(4000);
  --  LV_ERROR_SRE        VARCHAR2(4000);
    LE_ERROR_SRE        EXCEPTION;
    LX_RESPUESTA                XMLTYPE;
    LV_FAULT_CODE               VARCHAR2(1000);
    LV_FAULT_STRING             VARCHAR2(1000);
    --LV_REGISTROS      VARCHAR2(1000);
    lv_trama       VARCHAR2(4000);
    LV_SEPARADOR_TRAMA VARCHAR2(1) :=';';
    LN_ERROR           VARCHAR2(4000);
    BEGIN
     ------------------------
        -- LEO LOS PARAMETROS
     ------------------------
       --
        lv_id_req:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_001_ING');

        IF lv_id_req IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID_REQUERIMIENTO - ';
          RAISE LE_ERROR_PARA;
        END IF;
       --
       lv_data_source:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_002_ING');

        IF lv_data_source IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DATA SOURCE - ';
          RAISE LE_ERROR_PARA;
        END IF;  
       --
       lv_instancia:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_003_ING');

        IF lv_instancia IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID_INSTANCIA';
          RAISE LE_ERROR_PARA;
        END IF;  
       --
       lv_id_tipo_trans:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_004_ING');

        IF lv_id_tipo_trans IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID TIPO TRANSACCION';
          RAISE LE_ERROR_PARA;
        END IF; 
       --
      --
       lv_id_integrador:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_005_ING');

        IF lv_id_integrador IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO ID INTEGRADOR';
          RAISE LE_ERROR_PARA;
        END IF; 
       --
       lv_cod_alterno_dist:=BLF_PARAMETROS_CRED('NOT_CRED_SRE_GYE_006_ING');

        IF lv_cod_alterno_dist IS  NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO CODIGO ALTERNO';
          RAISE LE_ERROR_PARA;
        END IF; 
       --
       ------------------------------------
    -- Armo la Trama que consume el SRE
    ------------------------------------
     
        lv_trama := 'Id_Instancia=' || lv_instancia || LV_SEPARADOR_TRAMA ||
                    'Id_Tipo_Transaccion=' || lv_id_tipo_trans || LV_SEPARADOR_TRAMA ||
                    'Id_Integrador=' || lv_id_integrador || LV_SEPARADOR_TRAMA ||
                    'Cod_Alterno_Distribuidor=' || lv_cod_alterno_dist || LV_SEPARADOR_TRAMA ||
                    'Data_Source=' || lv_data_source || LV_SEPARADOR_TRAMA ||
                    'PV_COMPANIA_NCI='||PV_CIA|| LV_SEPARADOR_TRAMA ||
                    'PV_CUENTA_NCI='|| PV_CUENTA  || LV_SEPARADOR_TRAMA ||
                    'PV_FECHA_INGRESO_NCI=' ||  PV_FECHA_INGRESO|| LV_SEPARADOR_TRAMA ||
                    'PV_DESCRIPCION_NCI=' ||  PV_DESCRIPCION|| LV_SEPARADOR_TRAMA ||
                    'PV_DIRECCION_NCI=' ||  PV_DIRECCION|| LV_SEPARADOR_TRAMA ||
                    'PV_OBSERVACION_NCI=' ||  PV_OBSERVACION|| LV_SEPARADOR_TRAMA ||
                    'PV_NO_FACTURA_NCI=' ||  PV_NO_FACTURA|| LV_SEPARADOR_TRAMA ||
                    'PV_NO_CLIENTE_NCI=' ||  PV_NO_CLIENTE|| LV_SEPARADOR_TRAMA ||
                    'PV_FECHAFAC_NCI=' ||  PV_FECHAFAC|| LV_SEPARADOR_TRAMA ||
                    'PV_STATUSBSCS_NCI=' ||  PV_STATUSBSCS|| LV_SEPARADOR_TRAMA ||
                    'PV_LOCALIDAD_NCI=' ||  PV_LOCALIDAD|| LV_SEPARADOR_TRAMA ||
                    'PV_PRODUCTO_NCI=' ||  PV_PRODUCTO|| LV_SEPARADOR_TRAMA ||
                    'PN_TOTAL_NCI=' ||  PN_TOTAL|| LV_SEPARADOR_TRAMA ||
                    'PN_TOTALIMPONIBLE_NCI=' ||  PN_TOTALIMPONIBLE|| LV_SEPARADOR_TRAMA ||
                    'PN_TOTALIVA_NCI=' ||  PN_TOTALIVA|| LV_SEPARADOR_TRAMA ||
                    'PN_TOTALICE_NCI=' ||  PN_TOTALICE|| LV_SEPARADOR_TRAMA ||
                    'PV_USUARIO_NCI=' ||  PV_USUARIO|| LV_SEPARADOR_TRAMA ||
                    'PV_CICLO_NCI=' ||  PV_CICLO|| LV_SEPARADOR_TRAMA ||
                    'PV_RUC_NCI=' ||  PV_RUC|| LV_SEPARADOR_TRAMA;


       --
         lx_respuesta := scp_dat.sck_soap_gtw.scp_consume_servicio(pn_id_req                   => to_number(lv_id_req),
                                                                 pv_parametros_requerimiento => lv_trama,
                                                                 pv_fault_code               => lv_fault_code,
                                                                 pv_fault_string             => lv_fault_string);
         if lv_fault_string is not null then
           
          BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => to_number(lv_id_req),
                                                   pn_id_requerimiento => to_number(lv_id_req),
                                                   pv_usuario => pv_usuario,
                                                   pv_transaccion => 'Error al insertar en GYE lv_trama : '||lv_trama,
                                                   pv_estado => 'E',
                                                   pv_observacion => lv_fault_string,
                                                   pv_error => lv_error);
         
         end if;
       
     --  
       lv_error := scp_dat.sck_soap_gtw.scp_obtener_valor(pn_id_req           => to_number(lv_id_req),
                                                          pr_respuesta        => lx_respuesta,
                                                          pv_nombre_parametro => 'MENSAJE');
       
     --
       LN_ERROR := scp_dat.sck_soap_gtw.scp_obtener_valor(pn_id_req           => to_number(lv_id_req),
                                                                 pr_respuesta        => lx_respuesta,
                                                                 pv_nombre_parametro => 'COD_RESPUESTA');
       

     pn_error:=TO_NUMBER(LN_ERROR);
     pv_error := lv_error;
   EXCEPTION
     WHEN LE_ERROR_PARA THEN
       PV_ERROR := 'ERROR - '||LV_ERROR_PARAM;
     WHEN LE_ERROR THEN
       PV_ERROR := 'ERROR - '||LV_ERROR;
      
     WHEN OTHERS THEN
       PV_ERROR := 'ERROR - '||SUBSTR(SQLERRM,1,500); 
       
  END PNC_SRE_INSERTA_CAJA;
  
  Procedure PR_Valida_Email(Pv_Email In Varchar2, Pv_Error Out Varchar2) Is

    Lv_Email Varchar2(50);
    Lv_Error Varchar2(2000);
    Le_Error Exception;

  Begin

    Lv_Email := Pv_Email;

    --VALIDO QUE NO SEA NULO
    If Lv_Email Is Null Then
      Lv_Error := 'EL EMAIL NO DEBE SER NULO';
      Raise Le_Error;
    End If;

    --VALIDO QUE POR LO MENOS TENGA 7 CARACTERES: X@Z.COM
    If Length(Lv_Email) < 7 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL NO DEBE SER MENOR A 7 CARACTERES';
      Raise Le_Error;
    End If;

    --VALIDO QUE NO TENGA MAS DE 50 CARACTERES
    If Length(Lv_Email) > 50 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL: ' || Lv_Email ||
                  ' NO DEBE SER MAS DE 50 CARACTERES';
      Raise Le_Error;
    End If;

    --VERIFICO QUE TENGA EL SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, NO TIENE EL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE NO TENGAS MAS DE DOS SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 2) > 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, TIENE MAS DE DOS @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN CARACTER ANTES DEL @
    If Instr(Lv_Email, '@', 1, 1) <= 1 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER ANTES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

  Exception
    When Le_Error Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Lv_Error;
    When Others Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Sqlerrm;
  End PR_Valida_Email;
  
  Procedure PR_ENVIA_MAIL_CC_FILES_HTML(Pv_Ip_Servidor  In Varchar2,
                                        Pv_Sender       In Varchar2,
                                        Pv_Recipient    In Varchar2,
                                        Pv_Ccrecipient  In Varchar2,
                                        Pv_Subject      In Varchar2,
                                        Pv_Message      In clob,
                                        Pv_Max_Size     In Varchar2 Default 9999999999,
                                        Pv_Archivo1     In Varchar2 Default Null,
                                        Pv_Archivo2     In Varchar2 Default Null,
                                        Pv_Archivo3     In Varchar2 Default Null,
                                        Pv_Archivo4     In Varchar2 Default Null,
                                        Pv_Archivo5     In Varchar2 Default Null,
                                        Pv_Archivo6     In Varchar2 Default Null,
                                        Pv_Archivo7     In Varchar2 Default Null,
                                        Pv_Archivo8     In Varchar2 Default Null,
                                        Pv_Archivo9     In Varchar2 Default Null,
                                        Pv_Archivo10    In Varchar2 Default Null,
                                        Pv_Archivo11    In Varchar2 Default Null,
                                        Pv_Archivo12     In Varchar2 Default Null,
                                        Pv_Archivo13     In Varchar2 Default Null,
                                        Pv_Archivo14     In Varchar2 Default Null,
                                        Pv_Archivo15     In Varchar2 Default Null,
                                        Pv_Archivo16     In Varchar2 Default Null,
                                        Pv_Archivo17     In Varchar2 Default Null,
                                        Pv_Archivo18     In Varchar2 Default Null,
                                        Pv_Archivo19     In Varchar2 Default Null,
                                        Pv_Archivo20     In Varchar2 Default Null,
                                        Pv_Archivo21     In Varchar2 Default Null,
                                        Pv_Archivo22     In Varchar2 Default Null,
                                        Pv_Archivo23     In Varchar2 Default Null,
                                        Pv_Archivo24     In Varchar2 Default Null,
                                        Pv_Archivo25     In Varchar2 Default Null,
                                        Pv_Archivo26     In Varchar2 Default Null,
                                        Pv_Archivo27     In Varchar2 Default Null,
                                        Pv_Archivo28     In Varchar2 Default Null,
                                        Pv_Archivo29     In Varchar2 Default Null,
                                        Pv_Archivo30     In Varchar2 Default Null,
                                        Pb_Blnresultado In Out Boolean,
                                        Pv_Error        Out Varchar2) Is

    Crlf        Varchar2(2) := Utl_Tcp.Crlf;
    Lv_Conexion Utl_Smtp.Connection;
    Mailhost    Varchar2(30) := Pv_Ip_Servidor;
    Header      Clob := Empty_Clob;
    Lv_Programa Varchar2(30) := 'WFK_CORREO';
    Lv_Error    Varchar2(1000);
    Le_Error Exception;

    --descomposicion de trama
    Ln_Tamano              Number;
    Lv_Cadena_Cc           Varchar2(2000) := '';
    Lv_Cadena_To           Varchar2(2000) := '';
    Ln_Index_Tabla_Detalle Number;
    Lv_Valor               Varchar2(50);

    Type Varchar2_Table Is Table Of Varchar2(200) Index By Binary_Integer;
    Arreglo_Archivos     Varchar2_Table;
    Mesg_Len             Number;
    Mesg_Length_Exceeded Boolean := False;
    v_Directory_Name     Varchar2(100);
    v_File_Name          Varchar2(100);
    v_Line               Varchar2(1000);
    Mesg                 Varchar2(32767);
    i                    Binary_Integer;
    v_File_Handle        Sys.Utl_File.File_Type;
    v_Slash_Pos          Number;
    Mesg_Too_Long Exception;

  Begin
    If Pv_Ip_Servidor Is Null Then
      Lv_Error        := 'No se ha ingresado la ip del servidor mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Sender Is Null Then
      Lv_Error        := 'No se ha ingresado el correo de la persona que envia el mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Recipient Is Null Then
      Lv_Error        := 'No se ha ingresado los destinatarios.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Subject Is Null Then
      Lv_Error        := 'No se ha ingresado el asunto del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Message Is Null Then
      Lv_Error        := 'No se ha ingresado contenido del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Else
      -- Obtengo la conexion
      Lv_Conexion := Utl_Smtp.Open_Connection(Mailhost, 25);

      -- first load the three filenames into an array for easier handling later ...
      Arreglo_Archivos(1) := Pv_Archivo1;
      Arreglo_Archivos(2) := Pv_Archivo2;
      Arreglo_Archivos(3) := Pv_Archivo3;
      Arreglo_Archivos(4) := Pv_Archivo4;
      Arreglo_Archivos(5) := Pv_Archivo5;
      Arreglo_Archivos(6) := Pv_Archivo6;
      Arreglo_Archivos(7) := Pv_Archivo7;
      Arreglo_Archivos(8) := Pv_Archivo8;
      Arreglo_Archivos(9) := Pv_Archivo9;
      Arreglo_Archivos(10) := Pv_Archivo10;
      Arreglo_Archivos(11) := Pv_Archivo11;
      Arreglo_Archivos(12) := Pv_Archivo12;
      Arreglo_Archivos(13) := Pv_Archivo13;
      Arreglo_Archivos(14) := Pv_Archivo14;
      Arreglo_Archivos(15) := Pv_Archivo15;
      Arreglo_Archivos(16) := Pv_Archivo16;
      Arreglo_Archivos(17) := Pv_Archivo17;
      Arreglo_Archivos(18) := Pv_Archivo18;
      Arreglo_Archivos(19) := Pv_Archivo19;
      Arreglo_Archivos(20) := Pv_Archivo20;
      Arreglo_Archivos(21) := Pv_Archivo21;
      Arreglo_Archivos(22) := Pv_Archivo22;
      Arreglo_Archivos(23) := Pv_Archivo23;
      Arreglo_Archivos(24) := Pv_Archivo24;
      Arreglo_Archivos(25) := Pv_Archivo25;
      Arreglo_Archivos(26) := Pv_Archivo26;
      Arreglo_Archivos(27) := Pv_Archivo27;
      Arreglo_Archivos(28) := Pv_Archivo28;
      Arreglo_Archivos(29) := Pv_Archivo29;
      Arreglo_Archivos(30) := Pv_Archivo30;
      -- envio los parametros de Host y Sender
      Utl_Smtp.Helo(Lv_Conexion, Mailhost);
      Utl_Smtp.Mail(Lv_Conexion, Pv_Sender);

      --descompongo la trama de destinatarios
      --==============================================================================
      Lv_Cadena_To           := Pv_Recipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_To Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_To,
                                           1,
                                           Instr(Lv_Cadena_To, ',') - 1);
          Lv_Cadena_To           := Substr(Lv_Cadena_To,
                                           Instr(Lv_Cadena_To, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            PR_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_To);
          If Lv_Cadena_To Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --descompongo la trama de destinatarios con copia
      --==============================================================================
      Lv_Cadena_Cc           := Pv_Ccrecipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_Cc Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_Cc,
                                           1,
                                           Instr(Lv_Cadena_Cc, ',') - 1);
          Lv_Cadena_Cc           := Substr(Lv_Cadena_Cc,
                                           Instr(Lv_Cadena_Cc, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            PR_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_Cc);
          If Lv_Cadena_Cc Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --abro la conexion para comenzar a adjuntar data
      Utl_Smtp.Open_Data(Lv_Conexion);

      --Defino la cabecera del mail
      Header := 'Date: ' || To_Char(Sysdate, 'dd Mon yy hh24:mi:ss') || Crlf ||
                'From: ' || Pv_Sender || '' || Crlf || 'Subject: ' ||
                Pv_Subject || Crlf || 'To: ' ||
                Substr(Pv_Recipient, 1, Length(Pv_Recipient) - 1) || Crlf;
      If Pv_Ccrecipient Is Not Null Then
        Header := Header || 'CC: ' ||
                  Substr(Pv_Ccrecipient, 1, Length(Pv_Ccrecipient) - 1) || Crlf;
      End If;
      Header := Header || 'Mime-Version: 1.0' || Crlf ||
                'Content-Type: multipart/mixed; boundary="DMW.Boundary.605592468"' || Crlf || '' || Crlf ||
                'This is a Mime message, which your current mail reader may not' || Crlf ||
                'understand. Parts of the message will appear as text. If the remainder' || Crlf ||
                'appears as random characters in the message body, instead of as' || Crlf ||
                'attachments, then you''ll have to extract these parts and decode them' || Crlf ||
                'manually.' || Crlf || '' || Crlf ||
                '--DMW.Boundary.605592468' || Crlf;

      --adjunto la mensaje
      Header := Header || 'MIME-Version: 1.0' || crlf ||
                'Content-type:text/html;charset=iso-8859-1' || crlf || '' || crlf ||
                Pv_Message;
      Utl_Smtp.Write_Data(Lv_Conexion, Header);

      Mesg     := Header;
      Mesg_Len := Length(Mesg);

      If Mesg_Len > Pv_Max_Size Then
        Mesg_Length_Exceeded := True;
      End If;

      -- Append the files ...
      For i In 1 .. 30 Loop

        -- Sale si la longitud del mensaje se excede
        Exit When Mesg_Length_Exceeded;

        -- Si el nombre de archivo ha sido dado y no es nulo
        If Arreglo_Archivos(i) Is Not Null Then
          Begin
            -- Ubica el ultimo '/' o '\' en la ruta dada ...
            v_Slash_Pos := Instr(Arreglo_Archivos(i), '/', -1);
            If v_Slash_Pos = 0 Then
              v_Slash_Pos := Instr(Arreglo_Archivos(i), '\', -1);
            End If;

            -- Separa el nombre del archivo con el nombre del directorio
            v_Directory_Name := Substr(Arreglo_Archivos(i),
                                       1,
                                       v_Slash_Pos - 1);
            v_File_Name      := Substr(Arreglo_Archivos(i), v_Slash_Pos + 1);

            -- Abro el Archivo ...
            v_File_Handle := Sys.Utl_File.Fopen(v_Directory_Name,
                                                v_File_Name,
                                                'r');

            -- Generlo la linea de MIME ...
            Mesg     := Crlf || '--DMW.Boundary.605592468' || Crlf ||
                        'Content-Type: application/octet-stream; name="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Disposition: attachment; filename="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Transfer-Encoding: 7bit' || Crlf || Crlf;
            Mesg_Len := Mesg_Len + Length(Mesg);
            Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

            -- Adjunto el contenido del mensaje al final del Archivo
            Loop
              Sys.Utl_File.Get_Line(v_File_Handle, v_Line);
              If Mesg_Len + Length(v_Line) > Pv_Max_Size Then
                Mesg := '*** Truncado ***' || Crlf;
                Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
                Mesg_Length_Exceeded := True;
                Raise Mesg_Too_Long;
              End If;
              Mesg := v_Line || Crlf;
              Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
              Mesg_Len := Mesg_Len + Length(Mesg);
            End Loop;
          Exception
            When Sys.Utl_File.Invalid_Path Then
              Lv_Error := 'Error al abrir el adjunto: ' ||
                          Arreglo_Archivos(i);
              Raise Le_Error;
            When Others Then
              Null;
          End;
          Mesg := Crlf;
          Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
          --utl_smtp.data(Lv_Conexion, mesg);

          -- close the file ...
          Sys.Utl_File.Fclose(v_File_Handle);
        End If;
      End Loop;
      --===========================================================================================

      -- Adjunto la linea Final
      Mesg := Crlf || '--DMW.Boundary.605592468--' || Crlf;
      Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

      Utl_Smtp.Close_Data(Lv_Conexion);
      Pb_Blnresultado := True;
      Utl_Smtp.Quit(Lv_Conexion);

    End If;

  Exception
    When Utl_Smtp.Invalid_Operation Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Operacion Invalida en Transaccion SMTP ';
      Pb_Blnresultado := False;
    When Utl_Smtp.Transient_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Problemas Temporales enviando el mail - Intente despues. ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
    When Utl_Smtp.Permanent_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Errores en Codigo para Transaccion SMTP.';
      Pb_Blnresultado := False;
    When Le_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Lv_Error;
      Pb_Blnresultado := False;
    When Others Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
  End PR_ENVIA_MAIL_CC_FILES_HTML;
  
  PROCEDURE PR_REPORTE(PN_ID_REQ   IN  NUMBER,
                       PV_USUARIO  IN  VARCHAR2,
                       PV_PROD     IN  VARCHAR2,
                       PN_NC_GEN   IN  VARCHAR2,
                       PV_ERROR    OUT VARCHAR2) IS
     
    
     cursor c_datos is
      select T.CUENTA,t.valor_sin_impuesto valor from NC_DATOS_CAJA t WHERE T.ID_REQUERIMIENTO=PN_ID_REQ AND T.FACTURA IS NULL;
     
     cursor c_datos_count is
      select count(1) total
        from NC_DATOS_CAJA t
       WHERE T.ID_REQUERIMIENTO=PN_ID_REQ 
         AND T.FACTURA IS NULL;
     
     lc_datos_count      c_datos_count%rowtype;     
     LV_MENSAJE          clob;
     Lv_Linea_e          clob;
     --PROD
     lv_remitente         varchar2(500):= 'genera_nc_masiva@claro.com.ec';
     lv_asunto            varchar2(500);
     lb_blnresultado      boolean;
     LV_ERROR             varchar2(1500);
   --  ln_cont              number:=0;
     lv_copia             varchar2(4000);
     lv_para              varchar2(4000);
     Lv_Nomb_Archivo      varchar2(1000);
     Lv_Nombre_Archivo    Varchar2(500) := 'Cuentas_no_generadas_NC';
     Lv_Archivo           Varchar2(100);
     LX_XML_EIS1             SYS.XMLTYPE;
     LN_ID_REQ_EIS1          NUMBER;
     LV_FAULT_CODE           VARCHAR2(4000);
     LV_DESTINATARIO         VARCHAR2(4000);
     LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
     LV_FAULT_STRING         VARCHAR2(4000) := NULL;
     lv_mensaje_retorno      varchar2(1000);
     Lv_Ruta              Varchar2(200) := 'REPORTES_QC_DIR';
     
     Wfout1 Sys.Utl_File.File_Type;
   
   BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nomb_Archivo   := Lv_Ruta||'/'||Lv_Archivo;
    
     -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;

    Wfout1 := Sys.Utl_File.Fopen(Lv_Ruta, Lv_Archivo, 'W');
    
    
    lv_para:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_PARA');
    lv_copia:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
    lv_asunto:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO_MAIL');
    
    open c_datos_count;
     fetch c_datos_count into lc_datos_count;
    close c_datos_count;
    
    LV_MENSAJE := '<b>' || 'GENERACION DE NOTAS DE CREDITO MASIVAS' || '</B><br> <br>' || CHR(10) || CHR(13) || 
                '<b>' || ' Usuario ejecuci?n: </b>' || PV_USUARIO || '<br>' || CHR(10) || CHR(13) ||
                '<b>' || ' Id Requerimiento: </b>' || PN_ID_REQ || '<br>' || CHR(10) || CHR(13) ||
                '<b>' || ' Producto Caja: </b>' || PV_PROD || '<br>' || CHR(10) || CHR(13)||
                '<b>' || ' Total de cuentas generadas las NC : </b>' || PN_NC_GEN || '<br>' || CHR(10) || CHR(13)||
                '<b>' || ' Total de cuentas no generadas NC  : </b>' || nvl(lc_datos_count.total,0) || '<br><br><br>' || CHR(10) || CHR(13);
    --PN_NC_GEN
    Lv_Linea_e   := '<html><head><title>Cuentas no generadas NC '||' </title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<h3>Detalle de las cuentas a las que no se les encontro una factura </h3><br>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<table border="1"><tr>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CUENTA </B></td>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> VALOR </B></td>
     </tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    
     ---------------------------------------------------------------------------------
    -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
    ---------------------------------------------------------------------------------   
    LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
    LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                               BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                               ';pnIdServicioInformacion=' ||
                               TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                               ';pvParametroBind1=' || pv_usuario || ';';
  
    LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                             PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                             PV_FAULT_CODE               => LV_FAULT_CODE,
                                                             PV_FAULT_STRING             => LV_FAULT_STRING);
  
    --
    LV_FAULT_STRING:='';
    --
    IF LV_FAULT_STRING IS NOT NULL THEN
      lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                            LV_FAULT_STRING;
    END IF;
  
    LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                              PR_RESPUESTA        => LX_XML_EIS1,
                                                              PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                              PV_NAMESPACE        => NULL);
  
    IF LV_DESTINATARIO IS NULL THEN
      lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
    END IF;
    LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
    
    if nvl(lc_datos_count.total,0) = 0 then
      LV_MENSAJE := LV_MENSAJE || CHR(10) || CHR(13) || '<br> <b> Se generaron exitosamente todas las notas de cr?ditos </b>';
      Lv_Nomb_Archivo:=null;
    else
      LV_MENSAJE := LV_MENSAJE || ' Se adjunta archivo con las cuentas a las que no se les gener? notas de cr?dito debido a que no se les encontr? una factura. <br>'||CHR(10) || CHR(13);
      
       for i in c_datos loop
         --LV_MENSAJE := LV_MENSAJE || '<tr><td>'||i.cuenta||'&nbsp; </td></tr>';
         Lv_Linea_e := '<tr>' ||
                       '<td>' || i.cuenta          || '&nbsp;</td>'||
                       '<td>' || i.valor || '</td>'||
                       '</tr>';
         Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
         Lv_Linea_e := Null;
       end loop;
       Lv_Linea_e:= '</table></body></html>';
       Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
       -- CIERRA EL ARCHIVO
       -------------------------------------------------------------
       Sys.Utl_File.Fclose(Wfout1);
    end if;
    
    LV_MENSAJE := LV_MENSAJE ||  '<br><br>Saludos cordiales.'|| CHR(10) || CHR(13)|| CHR(13);
    LV_MENSAJE := LV_MENSAJE ||  '<br><br>------------------------'|| CHR(10) || CHR(13);
    LV_MENSAJE := LV_MENSAJE ||  '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::'|| CHR(10) || CHR(13);

    BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor => '130.2.18.61',
                                                           pv_sender => lv_remitente,
                                                           pv_recipient => nvl(LV_DESTINATARIO,lv_para)||','||lv_para||',',
                                                           pv_ccrecipient => lv_copia||',',
                                                           pv_subject  => lv_asunto,
                                                           pv_message  => LV_MENSAJE,
                                                           pv_max_size => '',
                                                           pv_archivo1 => Lv_Nomb_Archivo,
                                                           pv_archivo2 => '',
                                                           pv_archivo3 => '',
                                                           pv_archivo4 => '',
                                                           pv_archivo5 => '',
                                                           pv_archivo6 => '',
                                                           pv_archivo7 => '',
                                                           pv_archivo8 => '',
                                                           pv_archivo9 => '',
                                                           pv_archivo10 => '',
                                                           pv_archivo11 => '',
                                                           pv_archivo12 => '',
                                                           pv_archivo13 => '',
                                                           pv_archivo14 => '',
                                                           pv_archivo15 => '',
                                                           pv_archivo16 => '',
                                                           pv_archivo17 => '',
                                                           pv_archivo18 => '',
                                                           pv_archivo19 => '',
                                                           pv_archivo20 => '',
                                                           pv_archivo21 => '',
                                                           pv_archivo22 => '',
                                                           pv_archivo23 => '',
                                                           pv_archivo24 => '',
                                                           pv_archivo25 => '',
                                                           pv_archivo26 => '',
                                                           pv_archivo27 => '',
                                                           pv_archivo28 => '',
                                                           pv_archivo29 => '',
                                                           pv_archivo30 => '',
                                                           pb_blnresultado => lb_blnresultado,
                                                           pv_error => LV_ERROR);
    sys.utl_file.fremove(Lv_Ruta, Lv_Archivo);--ELIMINO EL ARCHIVO
    commit;
  exception
    when others then
      Sys.Utl_File.Fclose(Wfout1);
      PV_ERROR:=SUBSTR(SQLERRM, 1, 250);
      rollback;
  END PR_REPORTE;
  
  PROCEDURE PR_REPORTE_MASIVO(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_OBTENER_DATOS_CAJA IS
      SELECT DISTINCT (A.ID_REQUERIMIENTO), A.USUARIO, A.PRODUCTO_CAJA
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 3--HTS Geovanny Barrera 11/09/2018
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4
         AND A.ESTADO = 'F';
  
    CURSOR C_CONTAR_DATOS_CAJA IS
      SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 3--HTS Geovanny Barrera 11/09/2018
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4
         AND A.ESTADO = 'F';
  
    CURSOR C_DATOS(LN_ID_REQ NUMBER) IS
      SELECT T.CUENTA, T.VALOR_SIN_IMPUESTO VALOR
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    CURSOR C_DATOS_COUNT(LN_ID_REQ NUMBER) IS
      SELECT COUNT(1) TOTAL
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    lc_datos_count c_datos_count%rowtype;
    LV_MENSAJE     clob;
    Lv_Linea_e     clob;
    --PROD
    lv_remitente            varchar2(500) := 'genera_nc_masiva@claro.com.ec';
    lv_asunto               varchar2(500);
    lb_blnresultado         boolean;
    LV_ERROR                varchar2(1500);
    lv_copia                varchar2(4000);
    lv_para                 varchar2(4000);
    Lv_Nomb_Archivo         varchar2(1000);
    Lv_Nombre_Archivo       Varchar2(500) := 'Cuentas_no_generadas_NC';
    Lv_Archivo              Varchar2(100);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    lv_mensaje_retorno      varchar2(1000);
    Lv_Ruta                 Varchar2(200) := 'REPORTES_QC_DIR';
    LN_CONTAR_DATOS_CAJA    NUMBER;
    Wfout1                  Sys.Utl_File.File_Type;
    LV_ERROR_CAJA           VARCHAR2(800);
  
  BEGIN
  
    FOR J IN C_OBTENER_DATOS_CAJA LOOP
      BEGIN
        EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
      
        Lv_Nombre_Archivo := Lv_Nombre_Archivo || '_' ||
                             to_char(sysdate, 'dd_mm_yyyy');
        Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
        Lv_Nomb_Archivo   := Lv_Ruta || '/' || Lv_Archivo;
      
        -- Abre el archivo
        If Utl_File.Is_Open(Wfout1) Then
          Sys.Utl_File.Fclose(Wfout1);
        End If;
      
        Wfout1 := Sys.Utl_File.Fopen(Lv_Ruta, Lv_Archivo, 'W');
      
        lv_para   := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_PARA');
        lv_copia  := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
        lv_asunto := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO_MAIL');
      
        OPEN C_CONTAR_DATOS_CAJA;
        FETCH C_CONTAR_DATOS_CAJA
          INTO LN_CONTAR_DATOS_CAJA;
        CLOSE C_CONTAR_DATOS_CAJA;
      
        OPEN C_DATOS_COUNT(J.ID_REQUERIMIENTO);
        FETCH C_DATOS_COUNT
          INTO LC_DATOS_COUNT;
        CLOSE C_DATOS_COUNT;
      
        LV_MENSAJE := '<b>' || 'GENERACI&Oacute;N DE NOTAS DE CREDITO MASIVAS' ||
                      '</B><br> <br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Usuario ejecuci&oacute;n: </b>' || J.USUARIO /*PV_USUARIO*/
                      || '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Id Requerimiento: </b>' || J.ID_REQUERIMIENTO /*PN_ID_REQ*/
                      || '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Producto Caja: </b>' || J.PRODUCTO_CAJA /*PV_PROD*/
                      || '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Total de cuentas generadas las NC : </b>' ||
                      LN_CONTAR_DATOS_CAJA /*PN_NC_GEN*/
                      || '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Total de cuentas no generadas NC  : </b>' ||
                      nvl(lc_datos_count.total, 0) || '<br><br><br>' ||
                      CHR(10) || CHR(13);
        --PN_NC_GEN
        Lv_Linea_e := '<html><head><title>Cuentas no generadas NC ' ||
                      ' </title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<BODY>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<h3>Detalle de las cuentas a las que no se les encontro una factura </h3><br>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<table border="1"><tr>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CUENTA </B></td>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> VALOR </B></td>
     </tr>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        ---------------------------------------------------------------------------------
        -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
        ---------------------------------------------------------------------------------   
        LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
        LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                                   BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                                   ';pnIdServicioInformacion=' ||
                                   TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                                   ';pvParametroBind1=' || J.USUARIO /*pv_usuario*/
                                   || ';';
      
        LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                                 PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                                 PV_FAULT_CODE               => LV_FAULT_CODE,
                                                                 PV_FAULT_STRING             => LV_FAULT_STRING);
      
        --
        LV_FAULT_STRING := '';
        --
        IF LV_FAULT_STRING IS NOT NULL THEN
          lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                                LV_FAULT_STRING;
        END IF;
      
        LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                                  PR_RESPUESTA        => LX_XML_EIS1,
                                                                  PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                                  PV_NAMESPACE        => NULL);
      
        --
        --
        IF LV_DESTINATARIO IS NULL THEN
          lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
        END IF;
        LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
      
        if nvl(lc_datos_count.total, 0) = 0 then
          LV_MENSAJE      := LV_MENSAJE || CHR(10) || CHR(13) ||
                             '<br> <b> Se generaron exitosamente todas las notas de cr&eacute;ditos </b>';
         Lv_Nomb_Archivo := null;
        else
          LV_MENSAJE := LV_MENSAJE ||
                        ' Se adjunta archivo con las cuentas a las que no se les gener&oacute; notas de cr&eacute;dito debido a que no se les encontr&oacute; una factura. <br>' ||
                        CHR(10) || CHR(13);
        
          for i in c_datos(J.ID_REQUERIMIENTO) loop
            --LV_MENSAJE := LV_MENSAJE || '<tr><td>'||i.cuenta||'&nbsp; </td></tr>';
            Lv_Linea_e := '<tr>' || '<td>' || i.cuenta || '&nbsp;</td>' ||
                          '<td>' || i.valor || '</td>' || '</tr>';
            Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
            Lv_Linea_e := Null;
          end loop;
          Lv_Linea_e := '</table></body></html>';
          Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
          -- CIERRA EL ARCHIVO
          -------------------------------------------------------------
          Sys.Utl_File.Fclose(Wfout1);
        end if;
      
        LV_MENSAJE := LV_MENSAJE || '<br><br>Saludos cordiales.' || CHR(10) ||
                      CHR(13) || CHR(13);
        LV_MENSAJE := LV_MENSAJE || '<br><br>------------------------' ||
                      CHR(10) || CHR(13);
        LV_MENSAJE := LV_MENSAJE ||
                      '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::' ||
                      CHR(10) || CHR(13);
      
        BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor  => '130.2.18.61',
                                                       pv_sender       => lv_remitente,
                                                       pv_recipient    => nvl(LV_DESTINATARIO,
                                                                              lv_para) || ',' ||
                                                                          lv_para || ',',
                                                       pv_ccrecipient  => lv_copia || ',',
                                                       pv_subject      => lv_asunto,
                                                       pv_message      => LV_MENSAJE,
                                                       pv_max_size     => '',
                                                       pv_archivo1     => Lv_Nomb_Archivo,
                                                       pv_archivo2     => '',
                                                       pv_archivo3     => '',
                                                       pv_archivo4     => '',
                                                       pv_archivo5     => '',
                                                       pv_archivo6     => '',
                                                       pv_archivo7     => '',
                                                       pv_archivo8     => '',
                                                       pv_archivo9     => '',
                                                       pv_archivo10    => '',
                                                       pv_archivo11    => '',
                                                       pv_archivo12    => '',
                                                       pv_archivo13    => '',
                                                       pv_archivo14    => '',
                                                       pv_archivo15    => '',
                                                       pv_archivo16    => '',
                                                       pv_archivo17    => '',
                                                       pv_archivo18    => '',
                                                       pv_archivo19    => '',
                                                       pv_archivo20    => '',
                                                       pv_archivo21    => '',
                                                       pv_archivo22    => '',
                                                       pv_archivo23    => '',
                                                       pv_archivo24    => '',
                                                       pv_archivo25    => '',
                                                       pv_archivo26    => '',
                                                       pv_archivo27    => '',
                                                       pv_archivo28    => '',
                                                       pv_archivo29    => '',
                                                       pv_archivo30    => '',
                                                       pb_blnresultado => lb_blnresultado,
                                                       pv_error        => LV_ERROR);
        SYS.UTL_FILE.FREMOVE(Lv_Ruta, Lv_Archivo); --ELIMINO EL ARCHIVO
        COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => J.ID_REQUERIMIENTO,
                                                       PN_ID_REQUERIMIENTO => J.ID_REQUERIMIENTO,
                                                       PV_USUARIO          => J.USUARIO, --PV_USUARIO,
                                                       PV_TRANSACCION      => 'ERROR AL ENVIAR EL MAIL BSCS',
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => LV_ERROR_CAJA,
                                                       PV_ERROR            => LV_ERROR);
      END;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      Sys.Utl_File.Fclose(Wfout1);
      PV_ERROR := SUBSTR(SQLERRM, 1, 250);
      ROLLBACK;
  END PR_REPORTE_MASIVO;
  
  PROCEDURE PR_NOTIFICACION_INSERCION(PN_ID_REQ  IN NUMBER,
                                      PV_USUARIO IN VARCHAR2,
                                      PV_PROD    IN VARCHAR2,
                                      PV_ERROR   OUT VARCHAR2) IS
  
    LV_MENSAJE clob;
    --PROD
    lv_remitente            varchar2(500) := 'genera_nc_masiva@claro.com.ec';
    lv_asunto               varchar2(500);
    lb_blnresultado         boolean;
    LV_ERROR                varchar2(1500);
    lv_copia                varchar2(4000);
    lv_para                 varchar2(4000);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    lv_mensaje_retorno      varchar2(1000);
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  
    lv_para   := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_PARA');
    lv_copia  := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
    lv_asunto := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO_MAIL');
  
    LV_MENSAJE := '<b>' || 'GENERACI&Oacute;N DE NOTAS DE CREDITO MASIVAS' ||
                  '</B><br> <br>' || CHR(10) || CHR(13) || '<b>' ||
                  ' Usuario ejecuci&oacute;n: </b>' || PV_USUARIO || '<br>' ||
                  CHR(10) || CHR(13) || '<b>' || ' Id Requerimiento: </b>' ||
                  PN_ID_REQ || '<br>' || CHR(10) || CHR(13) || '<b>' ||
                  ' Producto Caja: </b>' || PV_PROD || '<br>' || CHR(10) ||
                  CHR(13); ---||
    ---------------------------------------------------------------------------------
    -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
    ---------------------------------------------------------------------------------   
    LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
    LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                               BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                               ';pnIdServicioInformacion=' ||
                               TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                               ';pvParametroBind1=' || PV_USUARIO || ';';
  
    LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                             PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                             PV_FAULT_CODE               => LV_FAULT_CODE,
                                                             PV_FAULT_STRING             => LV_FAULT_STRING);
  
    --
    LV_FAULT_STRING := '';
    --
    IF LV_FAULT_STRING IS NOT NULL THEN
      lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                            LV_FAULT_STRING;
    END IF;
  
    LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                              PR_RESPUESTA        => LX_XML_EIS1,
                                                              PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                              PV_NAMESPACE        => NULL);
  
    IF LV_DESTINATARIO IS NULL THEN
      lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
    END IF;
    LV_DESTINATARIO := TO_CHAR(replace(LV_DESTINATARIO, 'CORREO=', ''));
    LV_MENSAJE      := LV_MENSAJE ||
                       ' El requerimiento fue encolado y se ejecutar&aacute; en el pr&oacute;ximo corte <br>' ||
                       CHR(10) || CHR(13);
  
    LV_MENSAJE := LV_MENSAJE || '<br><br>Saludos cordiales.' || CHR(10) ||
                  CHR(13) || CHR(13);
    LV_MENSAJE := LV_MENSAJE || '<br><br>------------------------' ||
                  CHR(10) || CHR(13);
    LV_MENSAJE := LV_MENSAJE ||
                  '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::' ||
                  CHR(10) || CHR(13);
  
    BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor  => '130.2.18.61',
                                                   pv_sender       => lv_remitente,
                                                   pv_recipient    => nvl(LV_DESTINATARIO,
                                                                          lv_para) || ',' ||
                                                                      lv_para || ',',
                                                   pv_ccrecipient  => lv_copia || ',',
                                                   pv_subject      => lv_asunto,
                                                   pv_message      => LV_MENSAJE,
                                                   pv_max_size     => '',
                                                   pv_archivo1     => '', --Lv_Nomb_Archivo,
                                                   pv_archivo2     => '',
                                                   pv_archivo3     => '',
                                                   pv_archivo4     => '',
                                                   pv_archivo5     => '',
                                                   pv_archivo6     => '',
                                                   pv_archivo7     => '',
                                                   pv_archivo8     => '',
                                                   pv_archivo9     => '',
                                                   pv_archivo10    => '',
                                                   pv_archivo11    => '',
                                                   pv_archivo12    => '',
                                                   pv_archivo13    => '',
                                                   pv_archivo14    => '',
                                                   pv_archivo15    => '',
                                                   pv_archivo16    => '',
                                                   pv_archivo17    => '',
                                                   pv_archivo18    => '',
                                                   pv_archivo19    => '',
                                                   pv_archivo20    => '',
                                                   pv_archivo21    => '',
                                                   pv_archivo22    => '',
                                                   pv_archivo23    => '',
                                                   pv_archivo24    => '',
                                                   pv_archivo25    => '',
                                                   pv_archivo26    => '',
                                                   pv_archivo27    => '',
                                                   pv_archivo28    => '',
                                                   pv_archivo29    => '',
                                                   pv_archivo30    => '',
                                                   pb_blnresultado => lb_blnresultado,
                                                   pv_error        => LV_ERROR);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
    
      PV_ERROR := SUBSTR(SQLERRM, 1, 250);
      ROLLBACK;
  END PR_NOTIFICACION_INSERCION;
  
  PROCEDURE PR_REPORTE_MASIVO_S(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_OBTENER_DATOS_CAJA IS
      SELECT DISTINCT (A.ID_REQUERIMIENTO), A.USUARIO, A.PRODUCTO_CAJA
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 5
         AND A.ESTADO = 'F';
  
    CURSOR C_CONTAR_DATOS_CAJA IS
      SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4--HTS Geovanny Barrera
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 5
         AND A.ESTADO = 'F';
  
    CURSOR C_DATOS(LN_ID_REQ NUMBER) IS
      SELECT T.CUENTA, T.VALOR_SIN_IMPUESTO VALOR
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    CURSOR C_DATOS_COUNT(LN_ID_REQ NUMBER) IS
      SELECT COUNT(1) TOTAL
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    lc_datos_count c_datos_count%rowtype;
    LV_MENSAJE     clob;
    Lv_Linea_e     clob;
    --PROD
    lv_remitente            varchar2(500) := 'genera_nc_masiva@claro.com.ec';
    lv_asunto               varchar2(500);
    lb_blnresultado         boolean;
    LV_ERROR                varchar2(1500);
    lv_copia                varchar2(4000);
    lv_para                 varchar2(4000);
    Lv_Nomb_Archivo         varchar2(1000);
    Lv_Nombre_Archivo       Varchar2(500) := 'Cuentas_no_generadas_NC';
    Lv_Archivo              Varchar2(100);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    lv_mensaje_retorno      varchar2(1000);
    Lv_Ruta                 Varchar2(200) := 'REPORTES_QC_DIR';
    LN_CONTAR_DATOS_CAJA    NUMBER;
    Wfout1                  Sys.Utl_File.File_Type;
    LV_ERROR_CAJA           VARCHAR2(800);
  
  BEGIN
  
    FOR J IN C_OBTENER_DATOS_CAJA LOOP
      BEGIN
        EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
      
        Lv_Nombre_Archivo := Lv_Nombre_Archivo || '_' ||
                             to_char(sysdate, 'dd_mm_yyyy');
        Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
        Lv_Nomb_Archivo   := Lv_Ruta || '/' || Lv_Archivo;
      
        -- Abre el archivo
        If Utl_File.Is_Open(Wfout1) Then
          Sys.Utl_File.Fclose(Wfout1);
        End If;
      
        Wfout1 := Sys.Utl_File.Fopen(Lv_Ruta, Lv_Archivo, 'W');
      
        lv_para   := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_PARA');
        lv_copia  := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
        lv_asunto := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO_MAIL');
      
        OPEN C_CONTAR_DATOS_CAJA;
        FETCH C_CONTAR_DATOS_CAJA
          INTO LN_CONTAR_DATOS_CAJA;
        CLOSE C_CONTAR_DATOS_CAJA;
      
        OPEN C_DATOS_COUNT(J.ID_REQUERIMIENTO);
        FETCH C_DATOS_COUNT
          INTO LC_DATOS_COUNT;
        CLOSE C_DATOS_COUNT;
      
        LV_MENSAJE := '<b>' || 'GENERACI&Oacute;N DE NOTAS DE CREDITO MASIVAS' ||
                      '</B><br> <br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Usuario ejecuci&oacute;n: </b>' || J.USUARIO || '<br>' ||
                      CHR(10) || CHR(13) || '<b>' ||
                      ' Id Requerimiento: </b>' || J.ID_REQUERIMIENTO ||
                      '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Producto Caja: </b>' || J.PRODUCTO_CAJA || '<br>' ||
                      CHR(10) || CHR(13) || '<b>' ||
                      ' Total de cuentas generadas las NC : </b>' ||
                      LN_CONTAR_DATOS_CAJA || '<br>' || CHR(10) || CHR(13) ||
                      '<b>' || ' Total de cuentas no generadas NC  : </b>' ||
                      nvl(lc_datos_count.total, 0) || '<br><br><br>' ||
                      CHR(10) || CHR(13);
        --PN_NC_GEN
        Lv_Linea_e := '<html><head><title>Cuentas no generadas NC ' ||
                      ' </title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<BODY>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<h3>Detalle de las cuentas a las que no se les encontro una factura </h3><br>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<table border="1"><tr>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CUENTA </B></td>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> VALOR </B></td>
     </tr>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        ---------------------------------------------------------------------------------
        -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
        ---------------------------------------------------------------------------------   
        LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
        LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                                   BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                                   ';pnIdServicioInformacion=' ||
                                   TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                                   ';pvParametroBind1=' || J.USUARIO || ';';
      
        LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                                 PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                                 PV_FAULT_CODE               => LV_FAULT_CODE,
                                                                 PV_FAULT_STRING             => LV_FAULT_STRING);
      
        --
        LV_FAULT_STRING := '';
        --
        IF LV_FAULT_STRING IS NOT NULL THEN
          lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                                LV_FAULT_STRING;
        END IF;
      
        LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                                  PR_RESPUESTA        => LX_XML_EIS1,
                                                                  PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                                  PV_NAMESPACE        => NULL);
      
        IF LV_DESTINATARIO IS NULL THEN
          lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
        END IF;
        LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
      
        if nvl(lc_datos_count.total, 0) = 0 then
          LV_MENSAJE      := LV_MENSAJE || CHR(10) || CHR(13) ||
                             '<br> <b> Se generaron exitosamente todas las notas de cr?ditos </b>';
          Lv_Nomb_Archivo := null;
        else
          LV_MENSAJE := LV_MENSAJE ||
                        ' Se adjunta archivo con las cuentas a las que no se les gener? notas de cr?dito debido a que no se les encontr? una factura. <br>' ||
                        CHR(10) || CHR(13);
        
          for i in c_datos(J.ID_REQUERIMIENTO) loop
            --LV_MENSAJE := LV_MENSAJE || '<tr><td>'||i.cuenta||'&nbsp; </td></tr>';
            Lv_Linea_e := '<tr>' || '<td>' || i.cuenta || '&nbsp;</td>' ||
                          '<td>' || i.valor || '</td>' || '</tr>';
            Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
            Lv_Linea_e := Null;
          end loop;
          Lv_Linea_e := '</table></body></html>';
          Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
          -- CIERRA EL ARCHIVO
          -------------------------------------------------------------
          Sys.Utl_File.Fclose(Wfout1);
        end if;
      
        LV_MENSAJE := LV_MENSAJE || '<br><br>Saludos cordiales.' || CHR(10) ||
                      CHR(13) || CHR(13);
        LV_MENSAJE := LV_MENSAJE || '<br><br>------------------------' ||
                      CHR(10) || CHR(13);
        LV_MENSAJE := LV_MENSAJE ||
                      '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::' ||
                      CHR(10) || CHR(13);
      
        BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor  => '130.2.18.61',
                                                       pv_sender       => lv_remitente,
                                                       pv_recipient    => nvl(LV_DESTINATARIO,
                                                                              lv_para) || ',' ||
                                                                          lv_para || ',',
                                                       pv_ccrecipient  => lv_copia || ',',
                                                       pv_subject      => lv_asunto,
                                                       pv_message      => LV_MENSAJE,
                                                       pv_max_size     => '',
                                                       pv_archivo1     => Lv_Nomb_Archivo,
                                                       pv_archivo2     => '',
                                                       pv_archivo3     => '',
                                                       pv_archivo4     => '',
                                                       pv_archivo5     => '',
                                                       pv_archivo6     => '',
                                                       pv_archivo7     => '',
                                                       pv_archivo8     => '',
                                                       pv_archivo9     => '',
                                                       pv_archivo10    => '',
                                                       pv_archivo11    => '',
                                                       pv_archivo12    => '',
                                                       pv_archivo13    => '',
                                                       pv_archivo14    => '',
                                                       pv_archivo15    => '',
                                                       pv_archivo16    => '',
                                                       pv_archivo17    => '',
                                                       pv_archivo18    => '',
                                                       pv_archivo19    => '',
                                                       pv_archivo20    => '',
                                                       pv_archivo21    => '',
                                                       pv_archivo22    => '',
                                                       pv_archivo23    => '',
                                                       pv_archivo24    => '',
                                                       pv_archivo25    => '',
                                                       pv_archivo26    => '',
                                                       pv_archivo27    => '',
                                                       pv_archivo28    => '',
                                                       pv_archivo29    => '',
                                                       pv_archivo30    => '',
                                                       pb_blnresultado => lb_blnresultado,
                                                       pv_error        => LV_ERROR);
        SYS.UTL_FILE.FREMOVE(Lv_Ruta, Lv_Archivo); --ELIMINO EL ARCHIVO
        COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => J.ID_REQUERIMIENTO,
                                                       PN_ID_REQUERIMIENTO => J.ID_REQUERIMIENTO,
                                                       PV_USUARIO          => J.USUARIO, --PV_USUARIO,
                                                       PV_TRANSACCION      => 'ERROR AL ENVIAR EL MAIL BSCS',
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => LV_ERROR_CAJA,
                                                       PV_ERROR            => LV_ERROR);
      END;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      Sys.Utl_File.Fclose(Wfout1);
      PV_ERROR := SUBSTR(SQLERRM, 1, 250);
      ROLLBACK;
  END PR_REPORTE_MASIVO_S;
  
  PROCEDURE PR_REPORTE_SIN_DOC(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_OBTENER_DATOS_CAJA IS
      SELECT DISTINCT (A.ID_REQUERIMIENTO), A.USUARIO, A.PRODUCTO_CAJA
        FROM NC_DATOS_CAJA A
         WHERE A.ESTADO = 'M';
  
    CURSOR C_CONTAR_DATOS_CAJA IS
      SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
          WHERE A.ESTADO = 'M';
  
    CURSOR C_DATOS(LN_ID_REQ NUMBER) IS
      SELECT T.CUENTA, T.VALOR_SIN_IMPUESTO VALOR
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    CURSOR C_DATOS_COUNT(LN_ID_REQ NUMBER) IS
      SELECT COUNT(1) TOTAL
        FROM NC_DATOS_CAJA T
       WHERE T.ID_REQUERIMIENTO = LN_ID_REQ
         AND T.FACTURA IS NULL;
  
    lc_datos_count c_datos_count%rowtype;
    LV_MENSAJE     clob;
    Lv_Linea_e     clob;
    --PROD
    lv_remitente            varchar2(500) := 'genera_nc_masiva@claro.com.ec';
    lv_asunto               varchar2(500);
    lb_blnresultado         boolean;
    LV_ERROR                varchar2(1500);
    lv_copia                varchar2(4000);
    lv_para                 varchar2(4000);
    Lv_Nomb_Archivo         varchar2(1000);
    Lv_Nombre_Archivo       Varchar2(500) := 'Cuentas_no_generadas_NC';
    Lv_Archivo              Varchar2(100);
    LX_XML_EIS1             SYS.XMLTYPE;
    LN_ID_REQ_EIS1          NUMBER;
    LV_FAULT_CODE           VARCHAR2(4000);
    LV_DESTINATARIO         VARCHAR2(4000);
    LV_TRAMA_PARAMETRO_EIS1 VARCHAR2(3500);
    LV_FAULT_STRING         VARCHAR2(4000) := NULL;
    lv_mensaje_retorno      varchar2(1000);
    Lv_Ruta                 Varchar2(200) := 'REPORTES_QC_DIR';
    LN_CONTAR_DATOS_CAJA    NUMBER;
    Wfout1                  Sys.Utl_File.File_Type;
    LV_ERROR_CAJA           VARCHAR2(800);
  
  BEGIN
  
    FOR J IN C_OBTENER_DATOS_CAJA LOOP
      BEGIN
        EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';
      
        Lv_Nombre_Archivo := Lv_Nombre_Archivo || '_' ||
                             to_char(sysdate, 'dd_mm_yyyy');
        Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
        Lv_Nomb_Archivo   := Lv_Ruta || '/' || Lv_Archivo;
      
        -- Abre el archivo
        If Utl_File.Is_Open(Wfout1) Then
          Sys.Utl_File.Fclose(Wfout1);
        End If;
      
        Wfout1 := Sys.Utl_File.Fopen(Lv_Ruta, Lv_Archivo, 'W');
      
        lv_para   := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_PARA');
        lv_copia  := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_MAIL_CC');
        lv_asunto := BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_ASUNTO_MAIL');
      
        OPEN C_CONTAR_DATOS_CAJA;
        FETCH C_CONTAR_DATOS_CAJA
          INTO LN_CONTAR_DATOS_CAJA;
        CLOSE C_CONTAR_DATOS_CAJA;
      
        OPEN C_DATOS_COUNT(J.ID_REQUERIMIENTO);
        FETCH C_DATOS_COUNT
          INTO LC_DATOS_COUNT;
        CLOSE C_DATOS_COUNT;
      
        LV_MENSAJE := '<b>' || 'GENERACI&Oacute;N DE NOTAS DE CREDITO MASIVAS' ||
                      '</B><br> <br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Usuario ejecuci&oacute;n: </b>' || J.USUARIO || '<br>' ||
                      CHR(10) || CHR(13) || '<b>' ||
                      ' Id Requerimiento: </b>' || J.ID_REQUERIMIENTO ||
                      '<br>' || CHR(10) || CHR(13) || '<b>' ||
                      ' Producto Caja: </b>' || J.PRODUCTO_CAJA || '<br>' ||
                      CHR(10) || CHR(13) || '<b>' ||
                      ' Total de cuentas generadas las NC : </b>' ||
                      LN_CONTAR_DATOS_CAJA || '<br>' || CHR(10) || CHR(13) ||
                      '<b>' || ' Total de cuentas no generadas NC  : </b>' ||
                      nvl(lc_datos_count.total, 0) || '<br><br><br>' ||
                      CHR(10) || CHR(13);
        --PN_NC_GEN
        Lv_Linea_e := '<html><head><title>Cuentas no generadas NC ' ||
                      ' </title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<BODY>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<h3>Detalle de las cuentas a las que no se les encontro una factura </h3><br>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        Lv_Linea_e := '<table border="1"><tr>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CUENTA </B></td>
     <TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> VALOR </B></td>
     </tr>';
        Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
        ---------------------------------------------------------------------------------
        -- EIS A AXIS: PARA OBTENER EL DESTINARIO DEL ENVIO DEL EMAIL DE AXIS
        ---------------------------------------------------------------------------------   
        LN_ID_REQ_EIS1          := TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_001'));
        LV_TRAMA_PARAMETRO_EIS1 := 'dsId=' ||
                                   BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_002') ||
                                   ';pnIdServicioInformacion=' ||
                                   TO_NUMBER(BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_OBT_CORREO_003')) ||
                                   ';pvParametroBind1=' || J.USUARIO || ';';
      
        LX_XML_EIS1 := SCP_DAT.SCK_SOAP_GTW.SCP_CONSUME_SERVICIO(PN_ID_REQ                   => LN_ID_REQ_EIS1,
                                                                 PV_PARAMETROS_REQUERIMIENTO => LV_TRAMA_PARAMETRO_EIS1,
                                                                 PV_FAULT_CODE               => LV_FAULT_CODE,
                                                                 PV_FAULT_STRING             => LV_FAULT_STRING);
      
        --
        LV_FAULT_STRING := '';
        --
        IF LV_FAULT_STRING IS NOT NULL THEN
          lv_mensaje_retorno := 'Error en el consumo del WebService EIS AXIS: ' ||
                                LV_FAULT_STRING;
        END IF;
      
        LV_DESTINATARIO := SCP_DAT.SCK_SOAP_GTW.SCP_OBTENER_VALOR(PN_ID_REQ           => LN_ID_REQ_EIS1,
                                                                  PR_RESPUESTA        => LX_XML_EIS1,
                                                                  PV_NOMBRE_PARAMETRO => 'pvresultadoOut',
                                                                  PV_NAMESPACE        => NULL);
      
        IF LV_DESTINATARIO IS NULL THEN
          lv_mensaje_retorno := ' no se obtuvo el destinatario de AXIS';
        END IF;
        LV_DESTINATARIO := to_char(replace(LV_DESTINATARIO, 'CORREO=', ''));
      
        if nvl(lc_datos_count.total, 0) = 0 then
          LV_MENSAJE      := LV_MENSAJE || CHR(10) || CHR(13) ||
                             '<br> <b> Se generaron exitosamente todas las notas de cr?ditos </b>';
          Lv_Nomb_Archivo := null;
        else
          LV_MENSAJE := LV_MENSAJE ||
                        ' Se adjunta archivo con las cuentas a las que no se les gener? notas de cr?dito debido a que no se les encontr? una factura. <br>' ||
                        CHR(10) || CHR(13);
        
          for i in c_datos(J.ID_REQUERIMIENTO) loop
            --LV_MENSAJE := LV_MENSAJE || '<tr><td>'||i.cuenta||'&nbsp; </td></tr>';
            Lv_Linea_e := '<tr>' || '<td>' || i.cuenta || '&nbsp;</td>' ||
                          '<td>' || i.valor || '</td>' || '</tr>';
            Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
            Lv_Linea_e := Null;
          end loop;
          Lv_Linea_e := '</table></body></html>';
          Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
          -- CIERRA EL ARCHIVO
          -------------------------------------------------------------
          Sys.Utl_File.Fclose(Wfout1);
        end if;
      
        LV_MENSAJE := LV_MENSAJE || '<br><br>Saludos cordiales.' || CHR(10) ||
                      CHR(13) || CHR(13);
        LV_MENSAJE := LV_MENSAJE || '<br><br>------------------------' ||
                      CHR(10) || CHR(13);
        LV_MENSAJE := LV_MENSAJE ||
                      '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::' ||
                      CHR(10) || CHR(13);
      
        BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor  => '130.2.18.61',
                                                       pv_sender       => lv_remitente,
                                                       pv_recipient    => nvl(LV_DESTINATARIO,
                                                                              lv_para) || ',' ||
                                                                          lv_para || ',',
                                                       pv_ccrecipient  => lv_copia || ',',
                                                       pv_subject      => lv_asunto,
                                                       pv_message      => LV_MENSAJE,
                                                       pv_max_size     => '',
                                                       pv_archivo1     => Lv_Nomb_Archivo,
                                                       pv_archivo2     => '',
                                                       pv_archivo3     => '',
                                                       pv_archivo4     => '',
                                                       pv_archivo5     => '',
                                                       pv_archivo6     => '',
                                                       pv_archivo7     => '',
                                                       pv_archivo8     => '',
                                                       pv_archivo9     => '',
                                                       pv_archivo10    => '',
                                                       pv_archivo11    => '',
                                                       pv_archivo12    => '',
                                                       pv_archivo13    => '',
                                                       pv_archivo14    => '',
                                                       pv_archivo15    => '',
                                                       pv_archivo16    => '',
                                                       pv_archivo17    => '',
                                                       pv_archivo18    => '',
                                                       pv_archivo19    => '',
                                                       pv_archivo20    => '',
                                                       pv_archivo21    => '',
                                                       pv_archivo22    => '',
                                                       pv_archivo23    => '',
                                                       pv_archivo24    => '',
                                                       pv_archivo25    => '',
                                                       pv_archivo26    => '',
                                                       pv_archivo27    => '',
                                                       pv_archivo28    => '',
                                                       pv_archivo29    => '',
                                                       pv_archivo30    => '',
                                                       pb_blnresultado => lb_blnresultado,
                                                       pv_error        => LV_ERROR);
        SYS.UTL_FILE.FREMOVE(Lv_Ruta, Lv_Archivo); --ELIMINO EL ARCHIVO
        COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          BLK_NOTAS_CREDITOS.BLP_INSERTA_BITACORA_CRED(PN_ID_PROCESO       => J.ID_REQUERIMIENTO,
                                                       PN_ID_REQUERIMIENTO => J.ID_REQUERIMIENTO,
                                                       PV_USUARIO          => J.USUARIO, --PV_USUARIO,
                                                       PV_TRANSACCION      => 'ERROR AL ENVIAR EL MAIL BSCS',
                                                       PV_ESTADO           => 'E',
                                                       PV_OBSERVACION      => LV_ERROR_CAJA,
                                                       PV_ERROR            => LV_ERROR);
      END;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      Sys.Utl_File.Fclose(Wfout1);
      PV_ERROR := SUBSTR(SQLERRM, 1, 250);
      ROLLBACK;
  END PR_REPORTE_SIN_DOC;
  
   PROCEDURE FC_ACT_MESES (PV_VALOR IN VARCHAR2,
                           PN_ERROR OUT NUMBER,
                           PV_ERROR OUT VARCHAR2)IS
   --
  BEGIN
    
    update nc_parametros_grales p
       set p.valor1=PV_VALOR
     WHERE P.CODIGO = 'NOT_CRED_MESES_FACT'
       AND P.ESTADO = 'A';
  
   COMMIT;
  PN_ERROR:=0;

  EXCEPTION
    WHEN OTHERS THEN
     PN_ERROR:=-1;
     PV_ERROR:= SUBSTR(SQLERRM,250);

  END FC_ACT_MESES;
 
---INI [11554] CLS CCALDERON - 23/02/2018
 PROCEDURE PR_INSERTA_DATA_GYE(PV_ERROR  OUT VARCHAR2) is
  
  TYPE LR_NC_DATOS_CAJA_ACT IS RECORD(     
        ID_REQUERIMIENTO    NUMBER,
        CUENTA              VARCHAR2(30),
        FECHA_CREDITO       DATE,
        NOMBRE_CLIENTE      VARCHAR2(200),
        DIRECCION_CLIENTE   VARCHAR2(200),
        DESCRIPCION_CREDITO VARCHAR2(300),
        FACTURA             VARCHAR2(100),
        ID_CLIENTE          VARCHAR2(20),
        FECHA_FACTURA       DATE,
        CODIGO_CARGA        VARCHAR2(20),
        CIUDAD              VARCHAR2(100),
        PRODUCTO_CAJA       VARCHAR2(100),
        VALOR_CON_IMPUESTO  NUMBER,
        VALOR_SIN_IMPUESTO  NUMBER,
        IVA                 NUMBER,
        ICE                 NUMBER,
        USUARIO             VARCHAR2(100),
        CICLO               VARCHAR2(5),
        CIA                 VARCHAR2(5),
        ROWID_REG           VARCHAR2(4000)
      ); 

   TYPE LT_NC_DATOS_CAJA_ACT IS TABLE OF LR_NC_DATOS_CAJA_ACT INDEX BY BINARY_INTEGER;

    TYPE CURSOR_TYPE_ACT IS REF CURSOR;
    LC_NC_DATOS_CAJA_ACT CURSOR_TYPE_ACT;

    
    LT_NC_DATOS_CAJA_TMP_ACT LT_NC_DATOS_CAJA_ACT;
    
    LV_QUERY            VARCHAR2(4000);
    le_error            EXCEPTION;
    LV_ERROR            VARCHAR2(4000);   
    LV_ERROR_CAJA       VARCHAR2(4000);
    LV_USUARIO          VARCHAR2(1000);
    LN_ERROR_CAJA       NUMBER;
    LN_ID_REQUERIMIENTO NUMBER;
    LN_REGISTROS_NC     NUMBER;
    LN_CONTADOR         NUMBER:=0;
    LN_CANT_REG         NUMBER:=0;
    
  BEGIN  
      
      LV_QUERY:=  'SELECT ID_REQUERIMIENTO,   
                   CUENTA,             
                   FECHA_CREDITO,     
                   NOMBRE_CLIENTE,     
                   DIRECCION_CLIENTE,  
                   DESCRIPCION_CREDITO,
                   FACTURA,            
                   ID_CLIENTE,         
                   FECHA_FACTURA,      
                   CODIGO_CARGA,       
                   CIUDAD,             
                   PRODUCTO_CAJA,      
                   VALOR_CON_IMPUESTO, 
                   VALOR_SIN_IMPUESTO, 
                   IVA,                
                   ICE,                
                   USUARIO,            
                   CICLO,              
                   CIA,
                   ROWID
              FROM NC_DATOS_CAJA
             WHERE ESTADO = ''P''
             AND FECHA_CREDITO = TO_DATE(SYSDATE, ''DD/MM/RRRR'')
             AND FACTURA IS NOT NULL';      
         
		OPEN  LC_NC_DATOS_CAJA_ACT FOR LV_QUERY;
    LOOP 
                
       FETCH LC_NC_DATOS_CAJA_ACT BULK COLLECT INTO LT_NC_DATOS_CAJA_TMP_ACT LIMIT 1000;
       EXIT WHEN LT_NC_DATOS_CAJA_TMP_ACT.COUNT = 0;
                 
       IF LT_NC_DATOS_CAJA_TMP_ACT.COUNT > 0 THEN 
            ---
           FOR IDX_NC IN LT_NC_DATOS_CAJA_TMP_ACT.FIRST..LT_NC_DATOS_CAJA_TMP_ACT.LAST  LOOP
               lv_error_caja:=null;
               ln_contador:=ln_contador+1;
               
               ---Para produccion  
               BLK_NOTAS_CREDITOS.pnc_sre_inserta_caja(pv_cia => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                            pv_cuenta => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                            pv_fecha_ingreso => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                            pv_descripcion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                            pv_direccion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                            pv_observacion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                                              LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                            pv_no_factura => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                            pv_no_cliente => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pv_fechafac => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                            pv_statusbscs => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                            pv_localidad => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD,
                                            pv_producto => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                            pn_total => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO), 
                                            pn_totalimponible => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO),
                                            pn_totaliva => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                            pn_totalice => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                            pv_usuario => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                            pv_ciclo => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO,
                                            pv_ruc => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pn_error => ln_error_caja,
                                            pv_error => lv_error_caja);
               
                 -------------por pruebas
                   /*INSERT INTO ops$inventar.IN_CARGA_NOTAS_BSCS_TRX@unigye (
                                  ID_COMPANIA,
                                  CUENTA,
                                  FECHA_INGRESO,
                                  DESCRIPCION,
                                  DIRECCION,
                                  OBSERVACION,
                                  NO_FACTURA,
                                  NO_CLIENTE,
                                  FECHA_FAC,
                                  STATUS_BSCS,
                                  LOCALIDAD,
                                  PRODUCTO,
                                  TOTAL,
                                  TOTAL_IMPONIBLE,
                                  TOTAL_IVA,
                                  TOTAL_ICE,
                                  ESTADO,
                                  USUARIO_CARGA,
                                  FECHA_CARGA,
                                  NOMBRE_ARCHIVO,
                                  CICLO,  
                                  COD_CIA_REL)  
                    VALUES (
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD, 
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                  'X',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                  SYSDATE,
                                  null,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO, 
                                  null);*/
               
                LV_USUARIO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO;   
                LN_ID_REQUERIMIENTO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_REQUERIMIENTO; 
                           
               if lv_error_caja is not null then
                   BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => LN_ID_REQUERIMIENTO,
                                               pn_id_requerimiento => LN_ID_REQUERIMIENTO,
                                               pv_usuario => LV_USUARIO,
                                               pv_transaccion => 'Error al insertar a Caja SRE cta.'||LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                               pv_estado => 'E',
                                               pv_observacion => lv_error_caja,
                                               pv_error => lv_error);
               end if; 
               
               LN_CANT_REG:= LN_CANT_REG + 1;

               IF (MOD(LN_CANT_REG,500)=0) THEN
                 LN_CANT_REG:=0;
                 COMMIT;
               END IF;           
               
              ---Actualiza el Estado a "F" de finalizado. 
              UPDATE NC_DATOS_CAJA F
                 SET ESTADO = 'F'
              WHERE F.ROWID = LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ROWID_REG;
              COMMIT;
                             
            END LOOP;                                                   
       END IF;
    END LOOP;

    COMMIT;
    CLOSE LC_NC_DATOS_CAJA_ACT;
     lv_error_caja:= null;

    --A traves de un SRE Se procesan las NC insertadas en GYE
    BLK_NOTAS_CREDITOS.pnc_sre_genera_notas_cred(pv_compania => '1',
                                               pv_usuario => LV_USUARIO, 
                                               pn_bodega => 210,
                                               pn_registros => ln_registros_nc,
                                               pv_error => lv_error_caja);
    
    BLK_NOTAS_CREDITOS.pnc_sre_genera_notas_cred(pv_compania => '2',
                                               pv_usuario => LV_USUARIO, 
                                               pn_bodega => 227,
                                               pn_registros => ln_registros_nc,
                                               pv_error => lv_error_caja);                                               
   
   ln_registros_nc:=ln_contador;
   
   ---Se registra en la tabla de bitacora
   if lv_error_caja is not null then
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => LN_ID_REQUERIMIENTO, 
                                                   pn_id_requerimiento => LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LV_USUARIO,
                                                   pv_transaccion => 'Error al procesar las NC batch',
                                                   pv_estado => 'E',
                                                   pv_observacion => lv_error_caja,
                                                   pv_error => lv_error);
    else
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => LN_ID_REQUERIMIENTO,
                                                   pn_id_requerimiento => LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LV_USUARIO,
                                                   pv_transaccion => 'Exito al procesar las NC batch', 
                                                   pv_estado => 'F',
                                                   pv_observacion => 'Se procesaron '||ln_registros_nc||' notas de credito',
                                                   pv_error => lv_error);
    end if;
    commit;
         
    --Se realiza una depuracion cada 2 meses.
    EXECUTE IMMEDIATE 'DELETE FROM NC_DATOS_CAJA C WHERE C.FECHA_CREDITO<=(SYSDATE-60)';
    COMMIT;
     
  EXCEPTION
    WHEN le_error THEN
      PV_ERROR:= lv_error;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;
    
  END PR_INSERTA_DATA_GYE;
  
   PROCEDURE PR_INSERTA_DATA_GYE_NEW(PV_ERROR  OUT VARCHAR2) is
  
     TYPE LR_NC_DATOS_CAJA_ACT IS RECORD(     
        ID_REQUERIMIENTO    NUMBER,
        CUENTA              VARCHAR2(30),
        FECHA_CREDITO       DATE,
        NOMBRE_CLIENTE      VARCHAR2(200),
        DIRECCION_CLIENTE   VARCHAR2(200),
        DESCRIPCION_CREDITO VARCHAR2(300),
        FACTURA             VARCHAR2(100),
        ID_CLIENTE          VARCHAR2(20),
        FECHA_FACTURA       DATE,
        CODIGO_CARGA        VARCHAR2(20),
        CIUDAD              VARCHAR2(100),
        PRODUCTO_CAJA       VARCHAR2(100),
        VALOR_CON_IMPUESTO  NUMBER,
        VALOR_SIN_IMPUESTO  NUMBER,
        IVA                 NUMBER,
        ICE                 NUMBER,
        USUARIO             VARCHAR2(100),
        CICLO               VARCHAR2(5),
        CIA                 VARCHAR2(5),
        ROWID_REG           VARCHAR2(4000)
      ); 

   TYPE LT_NC_DATOS_CAJA_ACT IS TABLE OF LR_NC_DATOS_CAJA_ACT INDEX BY BINARY_INTEGER;

    TYPE CURSOR_TYPE_ACT IS REF CURSOR;
    LC_NC_DATOS_CAJA_ACT CURSOR_TYPE_ACT;

    
    LT_NC_DATOS_CAJA_TMP_ACT LT_NC_DATOS_CAJA_ACT;
    
    LV_QUERY            VARCHAR2(4000);
    le_error            EXCEPTION;
    LV_ERROR            VARCHAR2(4000);   
    LV_ERROR_CAJA       VARCHAR2(4000);
    LV_USUARIO          VARCHAR2(1000);
    LN_ERROR_CAJA       NUMBER;
    LN_ID_REQUERIMIENTO NUMBER;
    LN_REGISTROS_NC     NUMBER;
    LN_REGISTROS_NC2    NUMBER;
    LN_CONTADOR         NUMBER:=0;
    LN_CANT_REG         NUMBER:=0;
    LN_REGISTROS_USUARIO NUMBER:=0;
  
  CURSOR C_VERIFICA_NC_R IS
    SELECT COUNT(*) FROM NC_DATOS_CAJA WHERE ESTADO = 'R';
    
  CURSOR C_COUNT_REG_DIA_CORTE IS
       SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 3;--HTS Geovanny Barrera 11/09/2018
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4;
             
   CURSOR C_COUNT_REG_MAS_DIA_CORTE IS
       SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
       WHERE TO_DATE(A.FECHA_CREDITO, 'DD/MM/RRRR') + 1 =
             --TO_DATE(SYSDATE, 'DD/MM/RRRR') - 4;--HTS Geovanny Barrera 11/09/2018
             TO_DATE(SYSDATE, 'DD/MM/RRRR') - 5;
             
    LN_COUNT_REG_DIA_CORTE                  NUMBER;
    LN_COUNT_REG_MAS_DIA_CORTE              NUMBER;
    LV_VERIFICA_NC_R NUMBER;
    
    CURSOR C_GENERA_NC_SRE IS
      SELECT DISTINCT(USUARIO) FROM NC_DATOS_CAJA A 
      WHERE ESTADO = 'R';
    
    CURSOR C_CTA_USUARIOS_NC_CAJA(CV_USUARIO VARCHAR2) IS
      SELECT COUNT(*) NUMERO FROM NC_DATOS_CAJA 
      WHERE USUARIO = CV_USUARIO AND ESTADO = 'R';
             
    --GUARDAR LOS USUARIOS 
    TYPE LR_NC_USUARIOS IS RECORD(     
        USUARIO             VARCHAR2(100),
        FECHA_CREDITO       DATE,
        ESTADO              VARCHAR2(1)); 
    TYPE LT_NC_USUARIOS IS TABLE OF LR_NC_USUARIOS INDEX BY BINARY_INTEGER;
    TYPE CURSOR_NC_USUARIO IS REF CURSOR;
    LC_NC_USUARIOS CURSOR_NC_USUARIO;
    LT_NC_TMP_USUARIOS LT_NC_USUARIOS;
    LV_QUERY_USUARIOS VARCHAR2(2000);
    
  BEGIN   
  --VERIFICA SI EXISTEN TRANSACCIONES DEL CORTE POR EL ESTADO R DE REGISTRADO  
  OPEN C_VERIFICA_NC_R;
  FETCH C_VERIFICA_NC_R
   INTO LV_VERIFICA_NC_R;
  CLOSE C_VERIFICA_NC_R;
  
  --GUARDAMOS TODOS LOS USUARIOS Y SECUENCIALES CON ESTADO R PARA LUEGO DE PROCESARLOS(ESTADO F) UTILIZARLOS
  IF LV_VERIFICA_NC_R > 0 THEN   
     LV_QUERY_USUARIOS:=  ' SELECT DISTINCT(USUARIO), FECHA_CREDITO, ESTADO 
                           FROM NC_DATOS_CAJA WHERE ESTADO = ''R''';    
     
    OPEN  LC_NC_USUARIOS FOR LV_QUERY_USUARIOS;
      FETCH LC_NC_USUARIOS BULK COLLECT INTO LT_NC_TMP_USUARIOS;
          CLOSE LC_NC_USUARIOS;                                                    

      LV_QUERY:=  'SELECT ID_REQUERIMIENTO,   
                   CUENTA,             
                   FECHA_CREDITO,     
                   NOMBRE_CLIENTE,     
                   DIRECCION_CLIENTE,  
                   DESCRIPCION_CREDITO,
                   FACTURA,            
                   ID_CLIENTE,         
                   FECHA_FACTURA,      
                   CODIGO_CARGA,       
                   CIUDAD,             
                   PRODUCTO_CAJA,      
                   VALOR_CON_IMPUESTO, 
                   VALOR_SIN_IMPUESTO, 
                   IVA,                
                   ICE,                
                   USUARIO,            
                   CICLO,              
                   CIA,
                   ROWID
              FROM NC_DATOS_CAJA
             WHERE ESTADO = ''R''
             AND FACTURA IS NOT NULL';      
         
		OPEN  LC_NC_DATOS_CAJA_ACT FOR LV_QUERY;
    LOOP 
                
       FETCH LC_NC_DATOS_CAJA_ACT BULK COLLECT INTO LT_NC_DATOS_CAJA_TMP_ACT LIMIT 1000;
       EXIT WHEN LT_NC_DATOS_CAJA_TMP_ACT.COUNT = 0;
                 
       IF LT_NC_DATOS_CAJA_TMP_ACT.COUNT > 0 THEN 
            ---
           FOR IDX_NC IN LT_NC_DATOS_CAJA_TMP_ACT.FIRST..LT_NC_DATOS_CAJA_TMP_ACT.LAST  LOOP
               lv_error_caja:=null;
               ln_contador:=ln_contador+1;
               
               ---Para produccion  
               BLK_NOTAS_CREDITOS.pnc_sre_inserta_caja(pv_cia => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                            pv_cuenta => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                            pv_fecha_ingreso => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                            pv_descripcion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                            pv_direccion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                            pv_observacion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                                              LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                            pv_no_factura => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                            pv_no_cliente => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pv_fechafac => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                            pv_statusbscs => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                            pv_localidad => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD,
                                            pv_producto => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                            pn_total => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO), 
                                            pn_totalimponible => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO),
                                            pn_totaliva => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                            pn_totalice => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                            pv_usuario => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                            pv_ciclo => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO,
                                            pv_ruc => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pn_error => ln_error_caja,
                                            pv_error => lv_error_caja);
               
                 -------------por pruebas
                   /*INSERT INTO ops$inventar.IN_CARGA_NOTAS_BSCS_TRX@unigye (
                                  ID_COMPANIA,
                                  CUENTA,
                                  FECHA_INGRESO,
                                  DESCRIPCION,
                                  DIRECCION,
                                  OBSERVACION,
                                  NO_FACTURA,
                                  NO_CLIENTE,
                                  FECHA_FAC,
                                  STATUS_BSCS,
                                  LOCALIDAD,
                                  PRODUCTO,
                                  TOTAL,
                                  TOTAL_IMPONIBLE,
                                  TOTAL_IVA,
                                  TOTAL_ICE,
                                  ESTADO,
                                  USUARIO_CARGA,
                                  FECHA_CARGA,
                                  NOMBRE_ARCHIVO,
                                  CICLO,  
                                  COD_CIA_REL)  
                    VALUES (
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD, 
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                  'X',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                  SYSDATE,
                                  null,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO, 
                                  null);*/
               
                LV_USUARIO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO;   
                LN_ID_REQUERIMIENTO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_REQUERIMIENTO; 
                           
               if lv_error_caja is not null then
                   BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => LN_ID_REQUERIMIENTO,
                                               pn_id_requerimiento => LN_ID_REQUERIMIENTO,
                                               pv_usuario => LV_USUARIO,
                                               pv_transaccion => 'Error al insertar a Caja SRE cta.'||LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                               pv_estado => 'E',
                                               pv_observacion => lv_error_caja,
                                               pv_error => lv_error);
               end if; 
               
               LN_CANT_REG:= LN_CANT_REG + 1;

               IF (MOD(LN_CANT_REG,500)=0) THEN
                 LN_CANT_REG:=0;
                 COMMIT;
               END IF;           
               
              ---Actualiza el Estado a "F" de finalizado. 
              UPDATE NC_DATOS_CAJA F
                 SET ESTADO = 'F'
              WHERE F.ROWID = LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ROWID_REG;
              COMMIT;
                             
            END LOOP;                                                   
       END IF;
    END LOOP;

    COMMIT;
    CLOSE LC_NC_DATOS_CAJA_ACT;
    
    --PROCESO ENVIA A INVENTARIO FECHA DE FACTURA Y FECHA DE CARGA SYSDATE
     FOR IDX_USUARIOS IN LT_NC_TMP_USUARIOS.FIRST..LT_NC_TMP_USUARIOS.LAST  LOOP             
        --A traves de un SRE Se procesan las NC insertadas en GYE
        LV_ERROR_CAJA:=NULL;
    BLK_NOTAS_CREDITOS.PNC_SRE_GENERA_NOTAS_CRED(PV_COMPANIA => '1',
                                               PV_USUARIO => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,
                                               PN_BODEGA => 210,
                                               PN_REGISTROS => LN_REGISTROS_NC,
                                               PV_ERROR => LV_ERROR_CAJA);
    
    BLK_NOTAS_CREDITOS.PNC_SRE_GENERA_NOTAS_CRED(PV_COMPANIA => '2',
                                               PV_USUARIO => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO, 
                                               PN_BODEGA => 227,
                                               PN_REGISTROS => LN_REGISTROS_NC2,
                                               PV_ERROR => LV_ERROR_CAJA);                                               
   --ln_registros_nc:=ln_contador;
   ---Se registra en la tabla de bitacora
   if LV_ERROR_CAJA is not null then
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => 0,--LN_ID_REQUERIMIENTO, 
                                                   pn_id_requerimiento => 0,--LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,--LV_USUARIO,
                                                   pv_transaccion => 'Error al procesar las NC batch',
                                                   pv_estado => 'E',
                                                   pv_observacion => LV_ERROR_CAJA,
                                                   pv_error => lv_error);
    ELSE
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => 1,--LN_ID_REQUERIMIENTO,
                                                   pn_id_requerimiento => 1,--LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,
                                                   pv_transaccion => 'Exito al procesar las NC batch', 
                                                   pv_estado => 'F',
                                                   pv_observacion => 'Se procesaron '||LN_REGISTROS_USUARIO||' notas de credito',
                                                   pv_error => lv_error);
    END IF;
    COMMIT;
   END LOOP;
    
    --SE GENERA REPORTE
    OPEN C_COUNT_REG_DIA_CORTE;
    FETCH C_COUNT_REG_DIA_CORTE
    INTO LN_COUNT_REG_DIA_CORTE;
    CLOSE C_COUNT_REG_DIA_CORTE;
    
    OPEN C_COUNT_REG_MAS_DIA_CORTE;
    FETCH C_COUNT_REG_MAS_DIA_CORTE
    INTO LN_COUNT_REG_MAS_DIA_CORTE;
    CLOSE C_COUNT_REG_MAS_DIA_CORTE;
    
    LV_ERROR_CAJA:= NULL;
    IF LN_COUNT_REG_DIA_CORTE > 0 THEN 
     BLK_NOTAS_CREDITOS.PR_REPORTE_MASIVO(PV_ERROR => LV_ERROR_CAJA);      
      IF LV_ERROR_CAJA IS NOT NULL THEN
        LV_ERROR := LV_ERROR_CAJA;
        RAISE LE_ERROR;
      END IF;
    ELSIF  LN_COUNT_REG_MAS_DIA_CORTE > 0 THEN        
      BLK_NOTAS_CREDITOS.PR_REPORTE_MASIVO_S(PV_ERROR => LV_ERROR_CAJA);         
     IF LV_ERROR_CAJA IS NOT NULL THEN
        LV_ERROR := LV_ERROR_CAJA;
        RAISE LE_ERROR;
      END IF; 
    END IF;
    
    --Se realiza una depuracion cada 2 meses.
    EXECUTE IMMEDIATE 'DELETE FROM NC_DATOS_CAJA C WHERE C.FECHA_CREDITO<=(SYSDATE-60)';
    COMMIT;    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR:= LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;   
  END PR_INSERTA_DATA_GYE_NEW;

 PROCEDURE PR_INSERTAR_GYE_SIN_DOC(PV_ERROR  OUT VARCHAR2) is
  
     TYPE LR_NC_DATOS_CAJA_ACT IS RECORD(     
        ID_REQUERIMIENTO    NUMBER,
        CUENTA              VARCHAR2(30),
        FECHA_CREDITO       DATE,
        NOMBRE_CLIENTE      VARCHAR2(200),
        DIRECCION_CLIENTE   VARCHAR2(200),
        DESCRIPCION_CREDITO VARCHAR2(300),
        FACTURA             VARCHAR2(100),
        ID_CLIENTE          VARCHAR2(20),
        FECHA_FACTURA       DATE,
        CODIGO_CARGA        VARCHAR2(20),
        CIUDAD              VARCHAR2(100),
        PRODUCTO_CAJA       VARCHAR2(100),
        VALOR_CON_IMPUESTO  NUMBER,
        VALOR_SIN_IMPUESTO  NUMBER,
        IVA                 NUMBER,
        ICE                 NUMBER,
        USUARIO             VARCHAR2(100),
        CICLO               VARCHAR2(5),
        CIA                 VARCHAR2(5),
        ROWID_REG           VARCHAR2(4000)
      ); 

   TYPE LT_NC_DATOS_CAJA_ACT IS TABLE OF LR_NC_DATOS_CAJA_ACT INDEX BY BINARY_INTEGER;

    TYPE CURSOR_TYPE_ACT IS REF CURSOR;
    LC_NC_DATOS_CAJA_ACT CURSOR_TYPE_ACT;

    
    LT_NC_DATOS_CAJA_TMP_ACT LT_NC_DATOS_CAJA_ACT;
    
    LV_QUERY            VARCHAR2(4000);
    le_error            EXCEPTION;
    LV_ERROR            VARCHAR2(4000);   
    LV_ERROR_CAJA       VARCHAR2(4000);
    LV_USUARIO          VARCHAR2(1000);
    LN_ERROR_CAJA       NUMBER;
    LN_ID_REQUERIMIENTO NUMBER;
    LN_REGISTROS_NC     NUMBER;
    LN_REGISTROS_NC2    NUMBER;
    LN_CONTADOR         NUMBER:=0;
    LN_CANT_REG         NUMBER:=0;
    LN_REGISTROS_USUARIO NUMBER:=0;
  
  CURSOR C_VERIFICA_NC_C IS
    SELECT COUNT(*) FROM NC_DATOS_CAJA WHERE ESTADO = 'C';
    
  CURSOR C_COUNT_REG_DIA_CORTE IS
       SELECT COUNT(*)
        FROM NC_DATOS_CAJA A
        WHERE ESTADO = 'M';
             
    LN_COUNT_REG_DIA_CORTE                  NUMBER;
    LV_VERIFICA_NC_C NUMBER;
    
    CURSOR C_GENERA_NC_SRE IS
      SELECT DISTINCT(USUARIO) FROM NC_DATOS_CAJA A 
      WHERE ESTADO = 'C';
    
    CURSOR C_CTA_USUARIOS_NC_CAJA(CV_USUARIO VARCHAR2) IS
      SELECT COUNT(*) NUMERO FROM NC_DATOS_CAJA 
      WHERE USUARIO = CV_USUARIO AND ESTADO = 'D';
             
    --GUARDAR LOS USUARIOS 
    TYPE LR_NC_USUARIOS IS RECORD(     
        USUARIO             VARCHAR2(100),
        FECHA_CREDITO       DATE,
        ESTADO              VARCHAR2(1)); 
    TYPE LT_NC_USUARIOS IS TABLE OF LR_NC_USUARIOS INDEX BY BINARY_INTEGER;
    TYPE CURSOR_NC_USUARIO IS REF CURSOR;
    LC_NC_USUARIOS CURSOR_NC_USUARIO;
    LT_NC_TMP_USUARIOS LT_NC_USUARIOS;
    LV_QUERY_USUARIOS VARCHAR2(2000);
    
  BEGIN   
  --VERIFICA SI EXISTEN TRANSACCIONES DEL CORTE POR EL ESTADO R DE REGISTRADO  
  OPEN C_VERIFICA_NC_C;
  FETCH C_VERIFICA_NC_C
   INTO LV_VERIFICA_NC_C;
  CLOSE C_VERIFICA_NC_C;
  
  --GUARDAMOS TODOS LOS USUARIOS Y SECUENCIALES CON ESTADO D PARA LUEGO DE PROCESARLOS(ESTADO F) UTILIZARLOS
  IF LV_VERIFICA_NC_C > 0 THEN   
     LV_QUERY_USUARIOS:=  ' SELECT DISTINCT(USUARIO), FECHA_CREDITO, ESTADO 
                           FROM NC_DATOS_CAJA WHERE ESTADO = ''C''';    
     
    OPEN  LC_NC_USUARIOS FOR LV_QUERY_USUARIOS;
      FETCH LC_NC_USUARIOS BULK COLLECT INTO LT_NC_TMP_USUARIOS;
          CLOSE LC_NC_USUARIOS;                                                    

      LV_QUERY:=  'SELECT ID_REQUERIMIENTO,   
                   CUENTA,             
                   FECHA_CREDITO,     
                   NOMBRE_CLIENTE,     
                   DIRECCION_CLIENTE,  
                   DESCRIPCION_CREDITO,
                   FACTURA,            
                   ID_CLIENTE,         
                   FECHA_FACTURA,      
                   CODIGO_CARGA,       
                   CIUDAD,             
                   PRODUCTO_CAJA,      
                   VALOR_CON_IMPUESTO, 
                   VALOR_SIN_IMPUESTO, 
                   IVA,                
                   ICE,                
                   USUARIO,            
                   CICLO,              
                   CIA,
                   ROWID
              FROM NC_DATOS_CAJA
             WHERE ESTADO = ''C''
             AND FACTURA IS NOT NULL';      
         
		OPEN  LC_NC_DATOS_CAJA_ACT FOR LV_QUERY;
    LOOP 
                
       FETCH LC_NC_DATOS_CAJA_ACT BULK COLLECT INTO LT_NC_DATOS_CAJA_TMP_ACT LIMIT 1000;
       EXIT WHEN LT_NC_DATOS_CAJA_TMP_ACT.COUNT = 0;
                 
       IF LT_NC_DATOS_CAJA_TMP_ACT.COUNT > 0 THEN 
            ---
           FOR IDX_NC IN LT_NC_DATOS_CAJA_TMP_ACT.FIRST..LT_NC_DATOS_CAJA_TMP_ACT.LAST  LOOP
               lv_error_caja:=null;
               ln_contador:=ln_contador+1;
               
               ---Para produccion  
               BLK_NOTAS_CREDITOS.pnc_sre_inserta_caja(pv_cia => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                            pv_cuenta => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                            pv_fecha_ingreso => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                            pv_descripcion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                            pv_direccion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                            pv_observacion => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                                              LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                            pv_no_factura => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                            pv_no_cliente => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pv_fechafac => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                            pv_statusbscs => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                            pv_localidad => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD,
                                            pv_producto => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                            pn_total => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO), 
                                            pn_totalimponible => abs(LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO),
                                            pn_totaliva => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                            pn_totalice => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                            pv_usuario => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                            pv_ciclo => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO,
                                            pv_ruc => LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                            pn_error => ln_error_caja,
                                            pv_error => lv_error_caja);
               
                 -------------por pruebas
                 /*  INSERT INTO ops$inventar.IN_CARGA_NOTAS_BSCS_TRX@unigye (
                                  ID_COMPANIA,
                                  CUENTA,
                                  FECHA_INGRESO,
                                  DESCRIPCION,
                                  DIRECCION,
                                  OBSERVACION,
                                  NO_FACTURA,
                                  NO_CLIENTE,
                                  FECHA_FAC,
                                  STATUS_BSCS,
                                  LOCALIDAD,
                                  PRODUCTO,
                                  TOTAL,
                                  TOTAL_IMPONIBLE,
                                  TOTAL_IVA,
                                  TOTAL_ICE,
                                  ESTADO,
                                  USUARIO_CARGA,
                                  FECHA_CARGA,
                                  NOMBRE_ARCHIVO,
                                  CICLO,  
                                  COD_CIA_REL)  
                    VALUES (
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).NOMBRE_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DIRECCION_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA||' '||
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).DESCRIPCION_CREDITO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_CLIENTE,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).FECHA_FACTURA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CODIGO_CARGA, --'IN',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CIUDAD, 
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).PRODUCTO_CAJA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_CON_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).VALOR_SIN_IMPUESTO,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).IVA,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ICE, --0,
                                  'X',
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO,
                                  SYSDATE,
                                  null,
                                  LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CICLO, 
                                  null);*/
               
                LV_USUARIO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).USUARIO;   
                LN_ID_REQUERIMIENTO:=LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ID_REQUERIMIENTO; 
                           
               if lv_error_caja is not null then
                   BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => LN_ID_REQUERIMIENTO,
                                               pn_id_requerimiento => LN_ID_REQUERIMIENTO,
                                               pv_usuario => LV_USUARIO,
                                               pv_transaccion => 'Error al insertar a Caja SRE cta.'||LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).CUENTA,
                                               pv_estado => 'E',
                                               pv_observacion => lv_error_caja,
                                               pv_error => lv_error);
               end if; 
               
               LN_CANT_REG:= LN_CANT_REG + 1;

               IF (MOD(LN_CANT_REG,500)=0) THEN
                 LN_CANT_REG:=0;
                 COMMIT;
               END IF;           
               
              ---Actualiza el Estado a "M" de MANDAR A REPORTE. 
              UPDATE NC_DATOS_CAJA F
                 SET ESTADO = 'M'
              WHERE F.ROWID = LT_NC_DATOS_CAJA_TMP_ACT(IDX_NC).ROWID_REG;
              COMMIT;
                             
            END LOOP;                                                   
       END IF;
    END LOOP;

    COMMIT;
    CLOSE LC_NC_DATOS_CAJA_ACT;
    
    --PROCESO ENVIA A INVENTARIO FECHA DE FACTURA Y FECHA DE CARGA SYSDATE
     FOR IDX_USUARIOS IN LT_NC_TMP_USUARIOS.FIRST..LT_NC_TMP_USUARIOS.LAST  LOOP             
        --A traves de un SRE Se procesan las NC insertadas en GYE
        LV_ERROR_CAJA:=NULL;
    BLK_NOTAS_CREDITOS.PNC_SRE_GENERA_NOTAS_CRED(PV_COMPANIA => '1',
                                               PV_USUARIO => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,
                                               PN_BODEGA => 210,
                                               PN_REGISTROS => LN_REGISTROS_NC,
                                               PV_ERROR => LV_ERROR_CAJA);
    
    BLK_NOTAS_CREDITOS.PNC_SRE_GENERA_NOTAS_CRED(PV_COMPANIA => '2',
                                               PV_USUARIO => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO, 
                                               PN_BODEGA => 227,
                                               PN_REGISTROS => LN_REGISTROS_NC2,
                                               PV_ERROR => LV_ERROR_CAJA);                                               
   --ln_registros_nc:=ln_contador;
   ---Se registra en la tabla de bitacora
   if LV_ERROR_CAJA is not null then
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => 0,--LN_ID_REQUERIMIENTO, 
                                                   pn_id_requerimiento => 0,--LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,--LV_USUARIO,
                                                   pv_transaccion => 'Error al procesar las NC batch',
                                                   pv_estado => 'E',
                                                   pv_observacion => LV_ERROR_CAJA,
                                                   pv_error => lv_error);
    ELSE
      BLK_NOTAS_CREDITOS.blp_inserta_bitacora_cred(pn_id_proceso => 1,--LN_ID_REQUERIMIENTO,
                                                   pn_id_requerimiento => 1,--LN_ID_REQUERIMIENTO, 
                                                   pv_usuario => LT_NC_TMP_USUARIOS(IDX_USUARIOS).USUARIO,
                                                   pv_transaccion => 'Exito al procesar las NC batch', 
                                                   pv_estado => 'F',
                                                   pv_observacion => 'Se procesaron '||LN_REGISTROS_USUARIO||' notas de credito',
                                                   pv_error => lv_error);
    END IF;
    COMMIT;
   END LOOP;
    
    --SE GENERA REPORTE
    OPEN C_COUNT_REG_DIA_CORTE;
    FETCH C_COUNT_REG_DIA_CORTE
    INTO LN_COUNT_REG_DIA_CORTE;
    CLOSE C_COUNT_REG_DIA_CORTE;
   
    LV_ERROR_CAJA:= NULL;
    IF LN_COUNT_REG_DIA_CORTE > 0 THEN
     BLK_NOTAS_CREDITOS.PR_REPORTE_SIN_DOC(PV_ERROR => LV_ERROR_CAJA);
      IF LV_ERROR_CAJA IS NOT NULL THEN
        LV_ERROR := LV_ERROR_CAJA;
        RAISE LE_ERROR;
        ELSE 
            UPDATE NC_DATOS_CAJA F
                 SET F.ESTADO = 'F'
              WHERE F.ESTADO = 'M';
              COMMIT;
      END IF;
    END IF;
    
    --Se realiza una depuracion cada 2 meses.
    EXECUTE IMMEDIATE 'DELETE FROM NC_DATOS_CAJA C WHERE C.FECHA_CREDITO<=(SYSDATE-60)';
    COMMIT;    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR:= LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;   
  END PR_INSERTAR_GYE_SIN_DOC;

 -----# PARA EL ENVIO DE CORREO #-----
 PROCEDURE PR_REPORTE_ERROR_GYE(PV_ENTRADA  IN  VARCHAR2,
                                PV_ERROR    OUT VARCHAR2) IS
     
     LV_MENSAJE          clob;
     --PROD
     lv_remitente         varchar2(500):= 'error_nc_masiva@claro.com.ec';
     lv_asunto            varchar2(500);
     lb_blnresultado      boolean;
     LV_ERROR             varchar2(1500);
     lv_copia             varchar2(4000);
     lv_para              varchar2(4000);

   BEGIN
   
    lv_para:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_GYE_MAIL_PARA');
    lv_copia:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_GYE_MAIL_CC');
    lv_asunto:=BLK_NOTAS_CREDITOS.BLF_PARAMETROS_CRED('NOT_CRED_GYE_MAIL_ASUNTO');
    
    LV_MENSAJE := '<b>' || 'VERIFICACION DE NOTAS DE CREDITO MASIVAS PARA GYE' || '</B><br><br>' || CHR(10) || CHR(13) || 
                           ' Se ha presentado un inconveniente al momento de realizar la inserci?n a INVENTARIO.' || '<br><br><br>' || CHR(10) || CHR(13) ||
                  '<b>' || ' Se detalla el Error: </b>' || PV_ENTRADA || '<br><br><br>' || CHR(10) || CHR(13);
       
    LV_MENSAJE := LV_MENSAJE ||  '<br><br>Saludos cordiales.'|| CHR(10) || CHR(13)|| CHR(13);
    LV_MENSAJE := LV_MENSAJE ||  '<br><br>------------------------'|| CHR(10) || CHR(13);
    LV_MENSAJE := LV_MENSAJE ||  '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::'|| CHR(10) || CHR(13);

    BLK_NOTAS_CREDITOS.pr_envia_mail_cc_files_html(pv_ip_servidor => '130.2.18.61',
                                                           pv_sender => lv_remitente,
                                                           pv_recipient => lv_para||',',
                                                           pv_ccrecipient => lv_copia||',',
                                                           pv_subject  => lv_asunto,
                                                           pv_message  => LV_MENSAJE,
                                                           pv_max_size => '',
                                                           pv_archivo1 => '',
                                                           pv_archivo2 => '',
                                                           pv_archivo3 => '',
                                                           pv_archivo4 => '',
                                                           pv_archivo5 => '',
                                                           pv_archivo6 => '',
                                                           pv_archivo7 => '',
                                                           pv_archivo8 => '',
                                                           pv_archivo9 => '',
                                                           pv_archivo10 => '',
                                                           pv_archivo11 => '',
                                                           pv_archivo12 => '',
                                                           pv_archivo13 => '',
                                                           pv_archivo14 => '',
                                                           pv_archivo15 => '',
                                                           pv_archivo16 => '',
                                                           pv_archivo17 => '',
                                                           pv_archivo18 => '',
                                                           pv_archivo19 => '',
                                                           pv_archivo20 => '',
                                                           pv_archivo21 => '',
                                                           pv_archivo22 => '',
                                                           pv_archivo23 => '',
                                                           pv_archivo24 => '',
                                                           pv_archivo25 => '',
                                                           pv_archivo26 => '',
                                                           pv_archivo27 => '',
                                                           pv_archivo28 => '',
                                                           pv_archivo29 => '',
                                                           pv_archivo30 => '',
                                                           pb_blnresultado => lb_blnresultado,
                                                           pv_error => LV_ERROR);
    commit;
  exception
    when others then
      PV_ERROR:=SUBSTR(SQLERRM, 1, 250);
      rollback;
 END PR_REPORTE_ERROR_GYE;  
 ---FIN [11554] CLS CCALDERON - 23/02/2018
 
 END BLK_NOTAS_CREDITOS;
/
