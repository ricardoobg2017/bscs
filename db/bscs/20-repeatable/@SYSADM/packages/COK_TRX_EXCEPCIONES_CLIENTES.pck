create or replace package COK_TRX_EXCEPCIONES_CLIENTES is

  -- Author  : GLENN
  -- Created : 09/11/2010 9:55:51
  -- Purpose : [5488]
  
  TYPE c_consulta_excepciones IS REF CURSOR;
  
  TYPE gr_reg_excepciones IS RECORD(LV_ID_EXCEPCIONES_CLIENTES co_excepciones_clientes.id_excepciones_clientes%TYPE,
                                    LV_ID_IDENTIFICACION       co_excepciones_clientes.id_identificacion%TYPE,
                                    LV_IDENTIFICACION          co_excepciones_clientes.identificacion%TYPE,
                                    LV_NOMBRE_COMPLETO         co_excepciones_clientes.nombre_completo%TYPE,
                                    LV_USUARIO_CARGA           co_excepciones_clientes.usuario_carga%TYPE,
                                    LD_FECHA_CARGA             co_excepciones_clientes.fecha_carga%TYPE,
                                    LV_MOTIVO_CARGA            co_excepciones_clientes.motivo_carga%TYPE,
                                    LV_USUARIO_INACTIVACION    co_excepciones_clientes.usuario_inactivacion%TYPE,
                                    LV_FECHA_INACTIVACION      co_excepciones_clientes.fecha_inactivacion%TYPE,
                                    LV_MOTIVO_INACTIVACION     co_excepciones_clientes.motivo_inactivacion%TYPE,
                                    LV_ESTADO                  co_excepciones_clientes.estado%TYPE
                                   );
  
  TYPE Gr_RegExcepciones IS REF CURSOR RETURN gr_reg_excepciones; 
                     
                       
  PROCEDURE cop_consultar(Pv_Identificacion         IN VARCHAR2,
                          Pd_Fecha_desde            IN DATE,
                          Pd_Fecha_hasta            IN DATE,
                          Pv_Usuario                IN VARCHAR2,
                          Pr_RegExcepciones         OUT Gr_RegExcepciones,
                          Pn_Codi_Error             OUT NUMBER,
                          Pv_Mens_Error             OUT VARCHAR2
                         );          
   
                                       
  PROCEDURE cop_registrar_excepciones (Pv_id_identificacion IN VARCHAR2,
                                       Pv_identificacion    IN VARCHAR2,
                                       Pv_nombre_completo   IN VARCHAR2,
                                       Pv_motivo_carga      IN VARCHAR2,
                                       Pv_usuario_carga     IN VARCHAR2,
                                       Pn_Codi_Error        OUT NUMBER,
                                       Pv_Mens_Error        OUT VARCHAR2
                                      );        
                                                                                                                                            
                            
END COK_TRX_EXCEPCIONES_CLIENTES;
/
create or replace package body COK_TRX_EXCEPCIONES_CLIENTES is

  -- Author  : GLENN
  -- Created : 09/11/2010 9:55:51
  -- Purpose : [5488] 

  /*Realiza la consulta de las excepciones por identificacion*/
   PROCEDURE cop_consultar(Pv_Identificacion         IN VARCHAR2,
                           Pd_Fecha_desde            IN DATE,
                           Pd_Fecha_hasta            IN DATE,
                           Pv_Usuario                IN VARCHAR2,
                           Pr_RegExcepciones         OUT Gr_RegExcepciones,
                           Pn_Codi_Error             OUT NUMBER,
                           Pv_Mens_Error             OUT VARCHAR2
                          ) IS
      
      /*c�DIGOS DE ERROR 0 = EXITOSO 1 = ERROR DE DATOS 2 = NO EXISTE REGISTRO
       -1 = ERROR DE BASE DE DATOS O ERROR EXTERNO*/
      --CURSOR c_consulta_excepciones(lc_id_identificacion VARCHAR2,lc_identificacion VARCHAR2, lc_estado VARCHAR2) IS 
      Ln_criterio1               NUMBER;
      Ln_criterio2               NUMBER;
      Ln_criterio3               NUMBER;
      le_error                   EXCEPTION;
      lv_fecha_desde             Varchar2(50);
      lv_fecha_hasta             Varchar2(50);
      ld_fecha_desde             date;
      ld_fecha_hasta             date;
      
        
   BEGIN
     Pn_Codi_Error := 0;

     IF (Pv_Identificacion IS NOT NULL) THEN
         Ln_criterio1 := 1;
     END IF;
      
     IF (Pd_Fecha_Desde IS NOT NULL AND Pd_Fecha_Hasta IS NOT NULL) THEN 
         Ln_criterio2 := 1;
         lv_fecha_desde:= to_char(Pd_Fecha_Desde, 'dd/mm/yyyy')|| '00:00:00';
         lv_fecha_hasta:= to_char(Pd_Fecha_Hasta, 'dd/mm/yyyy')|| '23:59:59';
         ld_fecha_desde:= to_date(lv_fecha_desde, 'dd/mm/yyyy hh24:mi:ss');
         ld_fecha_hasta:= to_date(lv_fecha_hasta, 'dd/mm/yyyy hh24:mi:ss');
         
     END IF;
      
     IF (Pv_Usuario IS NOT NULL) THEN 
         Ln_criterio3 := 1;
     END IF;          

     IF (Ln_criterio1 IS NULL AND Ln_criterio2 IS NULL AND Ln_criterio3 IS NULL) THEN
          Pn_Codi_Error := 1;
          Pv_Mens_Error := '[Ingrese un criterio de b�squeda]';
          RAISE le_error;
     END IF;                
     
     /*Ejecuto el cursor dinamicamente con criterio de b�squeda identificaci�n*/
     IF Ln_criterio1 = 1 THEN              
              OPEN Pr_RegExcepciones FOR SELECT ID_EXCEPCIONES_CLIENTES,
                                                ID_IDENTIFICACION,
                                                IDENTIFICACION,
                                                NOMBRE_COMPLETO,
                                                USUARIO_CARGA,
                                                FECHA_CARGA,
                                                MOTIVO_CARGA,
                                                USUARIO_INACTIVACION,
                                                FECHA_INACTIVACION,
                                                MOTIVO_INACTIVACION,
                                                ESTADO
                                           FROM co_excepciones_clientes e
                                           WHERE e.IDENTIFICACION = Pv_Identificacion;
      END IF;         
      
      /*Ejecuto el cursor dinamicamente con criterio de b�squeda rango de fechas de carga*/
      IF Ln_criterio2 = 1 THEN
              OPEN Pr_RegExcepciones FOR SELECT ID_EXCEPCIONES_CLIENTES,
                                                ID_IDENTIFICACION,
                                                IDENTIFICACION,
                                                NOMBRE_COMPLETO,
                                                USUARIO_CARGA,
                                                FECHA_CARGA,
                                                MOTIVO_CARGA,
                                                USUARIO_INACTIVACION,
                                                FECHA_INACTIVACION,
                                                MOTIVO_INACTIVACION,
                                                ESTADO
                                           FROM co_excepciones_clientes e
                                           WHERE e.Fecha_Carga
                                                 BETWEEN ld_fecha_desde 
                                                     AND ld_fecha_hasta;
      END IF;
               
      /*Ejecuto el cursor dinamicamente con criterio de b�squeda usuario de carga*/  
      IF Ln_criterio3 = 1 THEN
              OPEN Pr_RegExcepciones FOR SELECT ID_EXCEPCIONES_CLIENTES,
                                                ID_IDENTIFICACION,
                                                IDENTIFICACION,
                                                NOMBRE_COMPLETO,
                                                USUARIO_CARGA,
                                                FECHA_CARGA,
                                                MOTIVO_CARGA,
                                                USUARIO_INACTIVACION,
                                                FECHA_INACTIVACION,
                                                MOTIVO_INACTIVACION,
                                                ESTADO
                                           FROM co_excepciones_clientes e
                                           WHERE e.Usuario_Carga = upper(Pv_Usuario);        
      END IF;         
   
   EXCEPTION
     WHEN le_error THEN
        Pv_Mens_Error := 'Error en el m�dulo :: COK_OBJ_EXCEPCIONES_CLIENTES.cop_CONSULTAR, '||Pv_Mens_Error;
   
     WHEN OTHERS THEN
        Pn_Codi_Error := -1;
        Pv_Mens_Error := 'Error en el m�dulo :: COK_OBJ_EXCEPCIONES_CLIENTES.cop_CONSULTAR, '||SQLERRM;
   
   END cop_consultar; 
  
    /*Registra excepciones por clientes*/
    PROCEDURE cop_registrar_excepciones (Pv_id_identificacion IN VARCHAR2,
                                         Pv_identificacion    IN VARCHAR2,
                                         Pv_nombre_completo   IN VARCHAR2,
                                         Pv_motivo_carga      IN VARCHAR2,
                                         Pv_usuario_carga     IN VARCHAR2,
                                         Pn_Codi_Error        OUT NUMBER,
                                         Pv_Mens_Error        OUT VARCHAR2
                                        ) IS 
                                      
      Le_Error         EXCEPTION;
      
    BEGIN
      Pn_Codi_Error := 0;
        
    IF (Pv_id_identificacion IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[Ingrese id de identificacion del cliente]';
        RAISE Le_error;
    END IF;
    
    IF (Pv_identificacion IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[ingrese identificacion del cliente]';
        RAISE Le_error;        
    END IF;
    
    IF (Pv_nombre_completo IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[Ingrese nombre completo del cliente]';        
        RAISE Le_error;        
    END IF;
  
    IF (Pv_motivo_carga IS NULL) THEN
        Pn_Codi_Error := 1; 
        Pv_Mens_Error := '[Ingrese el motivo de carga]';             
        RAISE Le_error;        
    END IF;
    
    IF (Pv_usuario_carga IS NULL) THEN
        Pn_Codi_Error := 1; 
        Pv_Mens_Error := '[Ingrese el usuario de carga]';             
        RAISE Le_error;        
    END IF;
    
    IF (Pv_id_identificacion NOT IN ('CED','PAS','RUC')) THEN
        Pn_Codi_Error := 1; 
        Pv_Mens_Error := '[Dato incorrecto en tipo de identificaci�n]';             
        RAISE Le_error;
    END IF;
    
    COK_OBJ_EXCEPCIONES_CLIENTES.cop_registrar_excepciones(Pv_id_identificacion => Pv_id_identificacion,
                                                           Pv_identificacion => Pv_identificacion,
                                                           Pv_nombre_completo => Pv_nombre_completo,
                                                           Pv_motivo_carga => Pv_motivo_carga,
                                                           Pv_usuario_carga => Pv_usuario_carga,                                                           
                                                           Pn_Codi_Error => Pn_Codi_Error,
                                                           Pv_Mens_Error => Pv_Mens_Error);
      
    EXCEPTION
      WHEN Le_error THEN
           Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_VALIDAR, PRODUCIDO POR :: '||Pv_Mens_Error;
        
      WHEN OTHERS THEN
           Pn_Codi_Error := -1;
           Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_VALIDAR, PRODUCIDO POR :: '||SQLERRM;
    END;      
                                              

END COK_TRX_EXCEPCIONES_CLIENTES;
/
