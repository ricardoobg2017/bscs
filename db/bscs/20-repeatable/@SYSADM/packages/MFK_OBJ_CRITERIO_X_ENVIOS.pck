CREATE OR REPLACE PACKAGE MFK_OBJ_CRITERIO_X_ENVIOS
 /*
 ||   Name       : MFK_OBJ_CRITERIO_X_ENVIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */
 IS
 /*HELP
   Overview of MFK_OBJ_CRITERIO_X_ENVIOS
   HELP*/

 /*EXAMPLES
   Examples of test
   EXAMPLES*/

 /* Constants */

 /* Exceptions */

 /* Variables */
-- MFK_OBJ_CRITERIO_X_ENVIOS
 /* Toggles */

 /* Windows */

 /* Programs */
 --modificado: Cima  Mariuxi Buenaventura
--fecha: 05/11/2011
--motivo: [6092]Mejoras al M�dulo de Env�o de SMS Masivos

PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL);

PROCEDURE MFP_INSERTAR(PN_IDENVIO    IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                       PN_IDCRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PV_VALOR      IN  MF_CRITERIOS_X_ENVIOS.VALOR%TYPE,
                       PV_EFECTO     IN  MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                       PV_SUB_CATEG  IN  VARCHAR2 DEFAULT NULL,
                       PV_MSG_ERROR  IN  OUT VARCHAR2);
--
--
PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO    IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                         PN_IDCRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                         PN_SECUENCIA  IN  MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                         PV_VALOR      IN  MF_CRITERIOS_X_ENVIOS.VALOR%TYPE,
                         PV_EFECTO     IN  MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                         PV_ESTADO     IN  MF_CRITERIOS_X_ENVIOS.ESTADO%TYPE,
                         PV_SUB_CATEG  IN  VARCHAR2 DEFAULT NULL,
                         PV_MSG_ERROR  IN  OUT VARCHAR2);



PROCEDURE MFP_ELIMINAR(PN_IDENVIO      IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                       PN_IDCRITERIO   IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PN_SECUENCIA    IN  MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                       PV_MSG_ERROR    IN  OUT VARCHAR2);



PROCEDURE MFP_DETALLE_INSERTA(PN_ID_ENVIO    IN  MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                              PN_ID_CRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                              PV_TRAMA       IN  VARCHAR2,
                              PV_TIPO        IN  VARCHAR2,
                              PV_ERROR       OUT VARCHAR2);



PROCEDURE MFP_DETALLE_ACTUALIZA(PN_ID_ENVIO    IN  MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                PN_ID_CRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                PV_TRAMA       IN  VARCHAR2,
                                PV_TIPO        IN  VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);



PROCEDURE MFP_DETALLE_ELIMINAR(PN_IDENVIO IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                                PV_ERROR  IN  OUT VARCHAR2);



PROCEDURE MFP_CLONAR(PN_IDENVIO      IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                     PN_OUT_IDENVIO  IN  OUT  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                     PV_MSG_ERROR    IN  OUT VARCHAR2) ;




END MFK_OBJ_CRITERIO_X_ENVIOS;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_CRITERIO_X_ENVIOS
 /*
 ||   Name       : MFK_OBJ_CRITERIO_X_ENVIOS
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by System Department CONECEL S.A. 2003
 */
 IS
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL) is
BEGIN
 NULL;
END MFP_HELP;

PROCEDURE MFP_INSERTAR(PN_IDENVIO     IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                       PN_IDCRITERIO  IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PV_VALOR       IN  MF_CRITERIOS_X_ENVIOS.VALOR%TYPE,
                       PV_EFECTO      IN  MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                       PV_SUB_CATEG   IN  VARCHAR2 DEFAULT NULL,
                       PV_MSG_ERROR   IN  OUT VARCHAR2)
IS
 /*
 ||   Name       : MFP_INSERTAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure
 ||   automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by System Department CONECEL S.A. 2003
 */
   --Cima MAB
 --[6092] Envio Masivo
 --30/06/11
  cursor c_exist(cn_id_envio number, cn_idcriterio number) is
    select count(*) as cantidad
      from MF_CRITERIOS_X_ENVIOS
     where idenvio = cn_id_envio
       and idcriterio = cn_idcriterio;

  LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_INSERTAR';
  LV_ESTADO               MF_CRITERIOS_X_ENVIOS.ESTADO%TYPE := 'A';
  LN_SECUENCIA           MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE;
  LE_ERROR              EXCEPTION;
  LV_ERROR         VARCHAR2(1000);
  lc_exist           c_exist%rowtype;

BEGIN

 PV_MSG_ERROR := NULL;
   open c_exist(PN_IDENVIO, PN_IDCRITERIO);
  fetch c_exist into lc_exist;
  close c_exist;

  if lc_exist.cantidad=0 then
       SELECT mfs_criteriosxenvio_sec.NEXTVAL INTO ln_secuencia  FROM dual;

       INSERT INTO MF_CRITERIOS_X_ENVIOS(IDENVIO,
                                         IDCRITERIO,
                                         SECUENCIA,
                                         VALOR,
                                         EFECTO,
                                         ESTADO)

                               VALUES  (PN_IDENVIO,
                                        PN_IDCRITERIO,
                                        ln_secuencia,
                                        PV_VALOR,
                                        PV_EFECTO,
                                        LV_ESTADO);



       IF PV_SUB_CATEG IS NOT NULL THEN
         MFP_DETALLE_INSERTA(PN_IDENVIO,
                             PN_IDCRITERIO,
                             PV_SUB_CATEG,
                             'C',
                             LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
             RAISE LE_ERROR;
          END IF;
       END IF;
  end if;
  EXCEPTION -- Exception
  WHEN LE_ERROR THEN
      PV_MSG_ERROR  := LV_ERROR;
  WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION;
END MFP_INSERTAR;


PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO      IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                         PN_IDCRITERIO   IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                         PN_SECUENCIA    IN  MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                         PV_VALOR        IN  MF_CRITERIOS_X_ENVIOS.VALOR%TYPE,
                         PV_EFECTO       IN  MF_CRITERIOS_X_ENVIOS.EFECTO%TYPE,
                         PV_ESTADO       IN  MF_CRITERIOS_X_ENVIOS.ESTADO%TYPE,
                         PV_SUB_CATEG    IN  VARCHAR2 DEFAULT NULL,
                         PV_MSG_ERROR    IN  OUT VARCHAR2)
AS
 /*
 ||   Name       : MFP_ACTUALIZAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure
 ||   automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by System Department CONECEL S.A. 2003
 */
  --Cima MAB
 --[6092] Envio Masivo
 --30/06/11
 CURSOR C_table_cur(Cn_idEnvio     number,
                    Cn_idCriterio  number,
                    cn_secuencia   number) IS

  SELECT * FROM MF_CRITERIOS_X_ENVIOS CE
  WHERE ce.idenvio         = cn_idEnvio
  and ce.idcriterio        = cn_idCriterio
  and ce.secuencia         = cn_secuencia
  and ce.estado            = 'A';

  Lc_CurrentRow    MF_CRITERIOS_X_ENVIOS%ROWTYPE ;
  Le_NoDataUpdated EXCEPTION;
  LE_ERROR         EXCEPTION;

  LV_ERROR      VARCHAR2(1000);
  LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_ACTUALIZAR';
BEGIN

  PV_MSG_ERROR := NULL;
   OPEN C_table_cur(PN_IDENVIO,PN_IDCRITERIO,PN_SECUENCIA) ;
  FETCH C_table_cur INTO Lc_CurrentRow ;
     IF C_table_cur%NOTFOUND THEN
        RAISE Le_NoDataUpdated ;
     END IF;
  CLOSE C_table_cur ;

  UPDATE MF_CRITERIOS_X_ENVIOS
     SET VALOR = NVL(PV_VALOR, VALOR)
        ,EFECTO = NVL(PV_EFECTO, EFECTO)
        ,ESTADO = NVL(PV_ESTADO, ESTADO)
   WHERE idEnvio = PN_IDENVIO
     AND idcriterio = PN_IDCRITERIO
     AND secuencia =  PN_SECUENCIA
     AND estado = 'A';

   IF PV_SUB_CATEG IS NOT NULL THEN
      MFP_DETALLE_INSERTA(PN_IDENVIO,
                          PN_IDCRITERIO,
                          PV_SUB_CATEG,
                          'C',
                          LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
   END IF;

  EXCEPTION
  WHEN LE_ERROR THEN
      PV_MSG_ERROR  := LV_ERROR;
  WHEN Le_NoDataUpdated THEN
      PV_MSG_ERROR := 'No se encontro el registro que desea actualizar. '||PV_SUB_CATEG|| ' .'||SQLERRM||'. '||LV_APLICACION;
  WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION;
  CLOSE C_table_cur ;
END MFP_ACTUALIZAR;
--_______________________________________________________________________

PROCEDURE MFP_ELIMINAR(PN_IDENVIO      IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                       PN_IDCRITERIO   IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                       PN_SECUENCIA    IN  MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                       PV_MSG_ERROR    IN  OUT VARCHAR2)
AS
 /*
 ||   Name       : MFP_ELIMINAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure
 ||   automatically generated using the PL/Vision building blocks
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
 ||   Adjusted by System Department CONECEL S.A. 2003
 */
  --Cima MAB
 --[6092] Envio Masivo
 --30/06/11
 CURSOR C_table_cur(Cn_idEnvio    number,
                    Cn_idCriterio number,
                    Cn_secuencia  number) IS

     SELECT COUNT(*) AS cantidad FROM MF_CRITERIOS_X_ENVIOS CE
      WHERE CE.Idenvio = Cn_idEnvio
        AND CE.IDCRITERIO = cn_idCriterio
        AND CE.Secuencia = cn_secuencia
        AND CE.ESTADO = 'A';

   Lc_CurrentRow     MF_CRITERIOS_X_ENVIOS%ROWTYPE ;
   Le_NoDataDeleted  EXCEPTION;
   LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_ELIMINAR';
BEGIN
  PV_MSG_ERROR := NULL;
  DELETE FROM MF_CRITERIOS_X_ENVIOS CE
   WHERE  CE.IDENVIO = PN_IDENVIO
     AND ce.idcriterio = PN_IDCRITERIO
     AND ce.secuencia  = PN_SECUENCIA;

  DELETE FROM mf_detalles_criterios_envios de
   WHERE de.id_envio = PN_IDENVIO
     AND de.idcriterio = PN_IDCRITERIO;

  EXCEPTION
  WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION;
END MFP_ELIMINAR;
--


PROCEDURE MFP_DETALLE_INSERTA(PN_ID_ENVIO    IN  MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                              PN_ID_CRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                              PV_TRAMA       IN  VARCHAR2,
                              PV_TIPO        IN  VARCHAR2,
                              PV_ERROR       OUT VARCHAR2)
AS
  cursor c_exist(cn_id_envio number, cn_idcriterio number,cv_subcategoria varchar2/*, pv_estado varchar2*/) is
  SELECT count(*) as cantidad
    FROM MF_DETALLES_CRITERIOS_ENVIOS
   WHERE id_envio = cn_id_envio
     AND idcriterio = cn_idcriterio
     AND subcategoria = cv_subcategoria;

  LV_APLICACION VARCHAR2(100):='MFK_OBJ_CRITERIO_X_ENVIOS.MFP_DETALLE_INSERTA';
  LV_ERROR      VARCHAR2(1000);
  LE_ERROR      EXCEPTION;

  --10798 INI SUD KBA
  /*LV_DATA         VARCHAR2(2000):=PV_TRAMA;
  LV_DATA_TMP     VARCHAR2(100):=LV_DATA;
  LV_DATA_TEMP    VARCHAR2(100):=LV_DATA;*/
  LV_DATA         VARCHAR2(32000):=PV_TRAMA;
  LV_DATA_TMP     VARCHAR2(32000):=LV_DATA;
  LV_DATA_TEMP    VARCHAR2(32000):=LV_DATA;
  --10798 FIN SUD KBA
  LN_POSICION_ACT NUMBER(10):=1;
  LV_ID           VARCHAR2(1000);
  LN_AVANZA       NUMBER(10):= LENGTH(LV_DATA_TMP);
  lc_exist        c_exist%rowtype;
  lb_found        boolean:= false;
BEGIN
  WHILE ( LN_AVANZA >1)
  LOOP
      LN_POSICION_ACT := INSTR(LV_DATA_TMP,'|',1);
      LV_DATA_TEMP := SUBSTR(LV_DATA_TMP,LN_POSICION_ACT+1,LENGTH(LV_DATA_TMP));
      LV_ID := SUBSTR(LV_DATA_TMP,1,LN_POSICION_ACT-1);
      LV_DATA_TMP := LV_DATA_TEMP;

      IF LV_ID IS NOT NULL THEN
        open  c_exist(PN_ID_ENVIO, PN_ID_CRITERIO,LV_ID);
        fetch c_exist into lc_exist;
        close c_exist;
          if lc_exist.cantidad=0 then
            BEGIN
                INSERT INTO MF_DETALLES_CRITERIOS_ENVIOS(ID_ENVIO,
                                                         IDCRITERIO,
                                                         SUBCATEGORIA,
                                                         ESTADO)
                                                  VALUES(PN_ID_ENVIO,
                                                         PN_ID_CRITERIO,
                                                         LV_ID,
                                                         'A');
                EXCEPTION
                 WHEN OTHERS THEN
                   LV_ERROR := 'ERROR EN LA APLICACION '|| LV_APLICACION ||'|EN LA INSERCION DEL DETALLE INTERNO '|| SUBSTR(SQLERRM,1,250);
                   RAISE LE_ERROR;
            END;
          end if;
      END IF;
      LN_AVANZA := LENGTH(LV_DATA_TMP);
  END LOOP;

  EXCEPTION
   WHEN LE_ERROR THEN
        PV_ERROR := LV_ERROR;
   WHEN OTHERS THEN
        PV_ERROR := 'ERROR EN LA APLICACION '|| LV_APLICACION ||'|'|| SUBSTR(SQLERRM,1,250);
END;
--


PROCEDURE MFP_DETALLE_ACTUALIZA(PN_ID_ENVIO    IN  MF_DETALLES_ENVIOS.IDENVIO%TYPE,
                                PN_ID_CRITERIO IN  MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                PV_TRAMA       IN  VARCHAR2,
                                PV_TIPO        IN  VARCHAR2,
                                PV_ERROR       OUT VARCHAR2)AS
  LV_APLICACION VARCHAR2(100):='MFK_OBJ_CRITERIO_X_ENVIOS.MFP_DETALLE_ACTUALIZA';
  LV_ERROR      VARCHAR2(1000);
  LE_ERROR      EXCEPTION;

BEGIN
    BEGIN
       DELETE FROM MF_DETALLES_CRITERIOS_ENVIOS
       WHERE ID_ENVIO = PN_ID_ENVIO
       AND IDCRITERIO = PN_ID_CRITERIO;
    EXCEPTION
       WHEN OTHERS THEN
         LV_ERROR := 'ERROR EN LA APLICACION '|| LV_APLICACION ||'|EN LA ELIMINACION DEL DETALLE INTERNO '|| SUBSTR(SQLERRM,1,250);
       RAISE LE_ERROR;
    END;

    MFP_DETALLE_INSERTA(PN_ID_ENVIO,
                        PN_ID_CRITERIO,
                        PV_TRAMA,
                        PV_TIPO,
                        LV_ERROR);

 EXCEPTION
   WHEN LE_ERROR THEN
    PV_ERROR := LV_ERROR;
   WHEN OTHERS THEN
           PV_ERROR := 'ERROR EN LA APLICACION '|| LV_APLICACION ||'|'|| SUBSTR(SQLERRM,1,250);
END;
---------------------


PROCEDURE MFP_DETALLE_ELIMINAR(PN_IDENVIO IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                               PV_ERROR   IN  OUT VARCHAR2)
AS
 /*
 ||   Name       : MFP_ELIMINAR
 ||   Created on : 07/2011 --Cima MAB
 ||   Comments   : [6092] Envios Masivos SMS
 */


 CURSOR C_table_cur(Cn_idEnvio number) IS
  SELECT *
  FROM mf_detalles_criterios_envios
  WHERE id_envio=Cn_idEnvio
  AND ESTADO='A';

  Lc_CurrentRow     mf_detalles_criterios_envios%ROWTYPE ;
  Le_NoDataDeleted  EXCEPTION;
  LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_DETALLE_ELIMINAR(PN_IDENVIO)';

BEGIN
   PV_ERROR := NULL;
   OPEN  C_table_cur(PN_IDENVIO) ;
   FETCH C_table_cur INTO Lc_CurrentRow ;
   IF C_table_cur%NOTFOUND THEN
      RAISE Le_NoDataDeleted ;
   END IF;

   DELETE mf_detalles_criterios_envios
    WHERE id_envio=PN_IDENVIO
      AND ESTADO='A';

   CLOSE C_table_cur ;

 EXCEPTION
  WHEN Le_NoDataDeleted  THEN
    PV_ERROR := 'El registro que desea borrar no existe. '||LV_APLICACION;
    CLOSE C_table_cur ;
  WHEN OTHERS THEN
    PV_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION;
    CLOSE C_table_cur ;

END MFP_DETALLE_ELIMINAR;
--


PROCEDURE MFP_CLONAR(PN_IDENVIO      IN  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                     PN_OUT_IDENVIO  IN  OUT  MF_CRITERIOS_X_ENVIOS.IDENVIO%TYPE,
                     PV_MSG_ERROR    IN  OUT VARCHAR2)
AS

 --Cima MAB
 --[6092] Envio Masivo
 --07/07/11
 CURSOR C_table_cur(Cn_idEnvio NUMBER) IS
     SELECT *
       FROM MF_ENVIOS E
      WHERE E.Idenvio = Cn_idEnvio;

 CURSOR C_criterios_envios(Cn_idEnvio NUMBER)IS
      SELECT *
        FROM mf_criterios_x_envios c
       WHERE c.idenvio = Cn_idEnvio;

   Lc_CurrentRow   C_table_cur%ROWTYPE ;
   Le_NoDataClonar EXCEPTION;
   LV_APLICACION   VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_ELIMINAR';
   LB_FOUND        BOOLEAN;
   ln_secuencia    NUMBER;
   ln_secuencia_criterio NUMBER;
   LV_ERROR              VARCHAR2(2000);

BEGIN
  OPEN C_table_cur(PN_IDENVIO);
    FETCH C_table_cur INTO Lc_CurrentRow;
    LB_FOUND := C_table_cur%FOUND;
  CLOSE C_table_cur;
  IF NOT LB_FOUND THEN
    RAISE Le_NoDataClonar;
  END IF;
  IF LB_FOUND THEN
     SELECT mfs_envios_sec.NEXTVAL into ln_secuencia from dual;
      BEGIN
       INSERT INTO mf_envios(IDENVIO,DESCRIPCION,ESTADO,CANTIDAD_MENSAJES,HORAS_SEPARACION_MSN,
                             FORMA_ENVIO,MENSAJE,MONTO_MINIMO,MONTO_MAXIMO,REGION,TIPO_CLIENTE,
                             REQUIERE_EDAD_MORA,USUARIO_INGRESO,FECHA_INGRESO,USUARIO_MODIFICACION,
                             FECHA_MODIFICACION,ID_CICLO,SCORE,TIPO_ENVIO,FINANCIAMIENTO)
                            (SELECT ln_secuencia,DESCRIPCION,'I',CANTIDAD_MENSAJES,HORAS_SEPARACION_MSN,
                             FORMA_ENVIO,MENSAJE,MONTO_MINIMO,MONTO_MAXIMO,REGION,TIPO_CLIENTE,
                             REQUIERE_EDAD_MORA,USUARIO_INGRESO,FECHA_INGRESO,USUARIO_MODIFICACION,
                             FECHA_MODIFICACION,ID_CICLO,SCORE,TIPO_ENVIO,FINANCIAMIENTO FROM mf_envios WHERE estado = 'A' AND Idenvio = PN_IDENVIO);
       EXCEPTION
        WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_ENVIO '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_ENVIO '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
      END;

      BEGIN
         INSERT INTO MF_DETALLES_ENVIOS(IDENVIO,
                                        IDCLIENTE,
                                        TIPOENVIO)
                                       (SELECT ln_secuencia,IDCLIENTE,TIPOENVIO
                                          FROM mf_detalles_envios
                                         WHERE idenvio = PN_IDENVIO);
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DETALLES_ENVIOS '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DETALLES_ENVIOS '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
      END;
  END IF;

 BEGIN
  INSERT INTO MF_CRITERIOS_X_ENVIOS(IDENVIO,
                                    IDCRITERIO,
                                    SECUENCIA,
                                    VALOR,
                                    EFECTO,
                                    ESTADO)
                                   (SELECT ln_secuencia,IDCRITERIO,mfs_criteriosxenvio_sec.NEXTVAL,VALOR,EFECTO,'A'
                                      FROM mf_criterios_x_envios
                                     WHERE idenvio = PN_IDENVIO
                                       AND estado = 'A');
  EXCEPTION
         WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_CRITERIOS_X_ENVIOS '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_CRITERIOS_X_ENVIOS '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
 END;

 BEGIN
   insert into MF_DETALLES_CRITERIOS_ENVIOS(ID_ENVIO,
                                            IDCRITERIO,
                                            SUBCATEGORIA,
                                            ESTADO)
                                           (SELECT ln_secuencia,IDCRITERIO,SUBCATEGORIA,'A'
                                              FROM mf_detalles_criterios_envios
                                             WHERE id_envio = PN_IDENVIO
                                               AND estado = 'A');

 EXCEPTION
         WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DETALLES_CRITERIOS_ENVIOS '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DETALLES_CRITERIOS_ENVIOS '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
 END;
  BEGIN
    INSERT INTO MF_TIME_EJECUCION(IDENVIO,
                                  DIAEJECUCION,
                                  HORAMAXEJECUCION,
                                  PERIODOEJECUCION,
                                  MINUTOSMAXEJECUCION,
                                  ESTADO,
                                  NUMEROSMSMAXENVIO)
                                 (SELECT ln_secuencia,DIAEJECUCION,HORAMAXEJECUCION,PERIODOEJECUCION,MINUTOSMAXEJECUCION,ESTADO,NUMEROSMSMAXENVIO
                                    FROM mf_time_ejecucion
                                   WHERE idenvio = PN_IDENVIO
                                     AND estado = 'A');
   EXCEPTION
         WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_TIME_EJECUCION '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_TIME_EJECUCION '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
  END;
  
    BEGIN
    INSERT INTO MF_DIAS_SEMANA(IDENVIO,
                                  DIA)
                                 (Select ln_secuencia,DIA From MF_DIAS_SEMANA Where IDENVIO =  PN_IDENVIO);
   EXCEPTION
         WHEN NO_DATA_FOUND THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DIAS_SEMANA '||'NO_DATA_FOUND';
           RAISE Le_NoDataClonar;
        WHEN OTHERS THEN
           LV_ERROR := 'ERROR EN CLONAR MF_DIAS_SEMANA '||SUBSTR(SQLERRM,1,255);
           RAISE Le_NoDataClonar;
  END;
  
  COMMIT;
  PN_OUT_IDENVIO := ln_secuencia;
 EXCEPTION -- Exception
   WHEN Le_NoDataClonar  THEN
      PV_MSG_ERROR := ' ERROR:'||LV_ERROR||' En '||LV_APLICACION;
   WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SUBSTR(SQLERRM,1,250)||'. '||LV_APLICACION;
END  MFP_CLONAR;
------------------------
END MFK_OBJ_CRITERIO_X_ENVIOS ;
/
