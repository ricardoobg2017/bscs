CREATE OR REPLACE PACKAGE COMI_REPORTE_BULK IS
    -----------------------------------------------------------------------
    -- JES - 01-SEP-2005
    -- DESARROLLO DEL PROCEDIMIENTO PARA EL PROYECTO DE REPORTES PARA BULK.
    -- GENERACION DEL REPORTE BULK PARA EL PAGO A DA
    -----------------------------------------------------------------------
    PROCEDURE VERIFICA_TABLA( PD_FECHA        IN DATE
                             ,PV_NOMBRE_TABLA IN OUT VARCHAR2
                             ,PV_MSG_ERROR    IN OUT VARCHAR2);

    PROCEDURE ELIMINA_DATOS(PV_MSG_ERROR IN OUT VARCHAR2);

    PROCEDURE SALDOS_MENSUALES( PD_FECHA_FACTURACION IN DATE
                               ,PN_MES IN NUMBER
                               ,PV_MSG_ERROR IN OUT VARCHAR2);

    PROCEDURE LLENA_DATOS( PN_ANIO              IN NUMBER
                          ,PN_PERI              IN NUMBER
                          ,PD_FECHA_INICIO      IN DATE
                          ,PD_FECHA_FIN         IN DATE
                          ,PD_FECHA_FACTURACION IN DATE
                          ,PV_CUENTA            IN VARCHAR2
                          ,PV_MSG_ERROR         IN OUT VARCHAR2);

    PROCEDURE COMSUMO_TOTAL( PV_CUENTA            IN VARCHAR2
                            ,PD_FECHA_ACTIVACION  IN DATE
                            ,PD_FECHA_FACTURACION IN DATE
                            ,PN_COMPANIA          IN OUT NUMBER
                            ,PV_IDENTIFICACION    IN OUT VARCHAR2
                            ,PV_NOMBRES           IN OUT VARCHAR2
                            ,PV_APELLIDOS         IN OUT VARCHAR2
                            ,PN_CONSUMO           IN OUT NUMBER
                            ,PD_FECHA_FINAL       IN OUT DATE
                            ,PV_MSG_ERROR         IN OUT VARCHAR2 );

    PROCEDURE LLENA_CONSUMO_TOTAL( PN_ANIO      IN NUMBER
                                  ,PN_PERI      IN NUMBER
                                  ,PV_CUENTA    IN VARCHAR2
                                  ,PV_MSG_ERROR IN OUT VARCHAR2);

    PROCEDURE GENERA_REPORTE( PN_ANIO       IN NUMBER
                                ,PN_PERI       IN NUMBER
                                ,PV_CUENTA     IN VARCHAR2
                                ,PV_MSG_ERROR  IN OUT VARCHAR2);

    PROCEDURE GENERA_REPORTE_6MESES( PN_ANIO       IN NUMBER
                                    ,PN_PERI       IN NUMBER
                                    ,PV_CUENTA     IN VARCHAR2
                                    ,PV_MSG_ERROR  IN OUT VARCHAR2);

    PROCEDURE EXPORTA_REPORTE( PV_MSG_ERROR  IN OUT VARCHAR2);
    PROCEDURE EXPORTA_REPORTE_6MESES( PV_MSG_ERROR  IN OUT VARCHAR2);
END COMI_REPORTE_BULK;
/
CREATE OR REPLACE PACKAGE BODY COMI_REPORTE_BULK IS
    -----------------------------------------------------------------------
    -- JES - 01-SEP-2005
    -- DESARROLLO DEL PROCEDIMIENTO PARA EL PROYECTO DE REPORTES PARA BULK.
    -- GENERACION DEL REPORTE BULK PARA EL PAGO A DA
    -----------------------------------------------------------------------

    PROCEDURE VERIFICA_TABLA( PD_FECHA        IN DATE
                             ,PV_NOMBRE_TABLA IN OUT VARCHAR2
                             ,PV_MSG_ERROR    IN OUT VARCHAR2) AS

        CURSOR LC_TABLA( CV_TABLA VARCHAR2) IS
            SELECT COUNT(OBJECT_NAME) CONT
            FROM ALL_OBJECTS
            WHERE OBJECT_NAME = CV_TABLA
                  AND OWNER = 'SYSADM'
                  AND OBJECT_TYPE = 'TABLE';
        LN_CONT NUMBER;
    BEGIN
        PV_NOMBRE_TABLA := 'CO_REPCARCLI_' || TO_CHAR(PD_FECHA,'DDMMYYYY');

        OPEN LC_TABLA(PV_NOMBRE_TABLA);
        FETCH LC_TABLA INTO LN_CONT;
        CLOSE LC_TABLA;

        IF LN_CONT = 0 THEN
            PV_MSG_ERROR := 'NO EXISTE FACTURACION GENERADA PARA ' || PD_FECHA;
        ELSIF LN_CONT > 1 THEN
            PV_MSG_ERROR := 'EXISTEN MUCHAS FACTURACIONES GENERADAS PARA ' || PD_FECHA;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'ERROR GENERAL AL VERIFICAR TABLA DE FACTURACION '|| PD_FECHA||': '||SQLERRM;
    END;

    PROCEDURE ELIMINA_DATOS(PV_MSG_ERROR IN OUT VARCHAR2) AS
    BEGIN
        DELETE FROM COMI_REPORTE_BULK_DAT;
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR AL ELIMINAR LOS DATOS DE LA TABLA: '||SQLERRM;
    END;

    ------------------------------------------------------------------------
    -- BUSCA LOS SALDOS MENSUALES DE LAS CUENTAS REGISTRADAS EN COMI_REPORTE_BULK_DAT
    ------------------------------------------------------------------------
    PROCEDURE SALDOS_MENSUALES( PD_FECHA_FACTURACION IN DATE
                               ,PN_MES IN NUMBER
                               ,PV_MSG_ERROR IN OUT VARCHAR2) AS

        LV_CUENTA            VARCHAR2(24);
        LN_CONSUMO           NUMBER;
        LD_FECHA_ACTIVACION  DATE;
        LD_FECHA_INICIO      DATE;
        LD_FECHA_FIN         DATE;

        LV_TABLA1            VARCHAR2(100);
        LV_TABLA2            VARCHAR2(100);
        LV_SENTENCIA         VARCHAR2(1000);

        I_CURSOR        INTEGER;
        I_ROWS          INTEGER;
        I_VOID          INTEGER;

        LE_ERROR             EXCEPTION;
    BEGIN
        LV_SENTENCIA := NULL;
        VERIFICA_TABLA(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES    ), LV_TABLA1, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            NULL;
        ELSE
            LV_SENTENCIA := 'SELECT C.CODIGO_DOC, (BALANCE_12 - INTER_INTERNACIONAL - INTER_INTRA_REG_INTER_REG - INTER_LOCAL - IVA - ICE) CONSUMO, FECHA_ACTIVACION, ' ||
                            'TO_DATE('''||TO_CHAR(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES - 1),'DD-MM-YYYY')||''',''DD-MM-YYYY'') FECHA_INICIO_1, '||
                            'TO_DATE('''||TO_CHAR(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES    ) -1,'DD-MM-YYYY')||''',''DD-MM-YYYY'') FECHA_FIN_1 '||
                            'FROM   ' || LV_TABLA1 || ' S, COMI_REPORTE_BULK_DAT C ' ||
                            'WHERE C.CODIGO_DOC = S.CUENTA '||
                            'AND FECHA_ACTIVACION < TO_DATE('''||TO_CHAR(PD_FECHA_FACTURACION,'DD-MM-YYYY')||''',''DD-MM-YYYY'') ';
        END IF;
        VERIFICA_TABLA(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES + 1), LV_TABLA2, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            NULL;
        ELSE
            IF LV_SENTENCIA IS NOT NULL THEN
                LV_SENTENCIA :=  LV_SENTENCIA || 'UNION ';
            END IF;
            LV_SENTENCIA :=  LV_SENTENCIA ||
                        'SELECT C.CODIGO_DOC, (BALANCE_12 - INTER_INTERNACIONAL - INTER_INTRA_REG_INTER_REG - INTER_LOCAL - IVA - ICE) CONSUMO, FECHA_ACTIVACION, ' ||
                        'TO_DATE('''||TO_CHAR(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES    ),'DD-MM-YYYY')||''',''DD-MM-YYYY'') FECHA_INICIO_1, '||
                        'TO_DATE('''||TO_CHAR(ADD_MONTHS(PD_FECHA_FACTURACION,PN_MES + 1)-1,'DD-MM-YYYY')||''',''DD-MM-YYYY'') FECHA_FIN_1 '||
                        'FROM   ' || LV_TABLA2 || ' S, COMI_REPORTE_BULK_DAT C ' ||
                        'WHERE C.CODIGO_DOC = S.CUENTA '||
                        'AND FECHA_ACTIVACION >= TO_DATE('''||TO_CHAR(PD_FECHA_FACTURACION,'DD-MM-YYYY')||''',''DD-MM-YYYY'') ';
        END IF;
        PV_MSG_ERROR := NULL;
        IF LV_SENTENCIA IS NULL THEN
            --PV_MSG_ERROR := 'NO SE ENCONTRO FACTURACION DEL '|| PN_MES||' MES POSTERIOR A LA FECHA: '||PD_FECHA_FACTURACION;
            RAISE LE_ERROR;
        END IF;

        I_CURSOR := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE( I_CURSOR, LV_SENTENCIA, DBMS_SQL.V7);
        DBMS_SQL.DEFINE_COLUMN( I_CURSOR, 1, LV_CUENTA, 24);
        DBMS_SQL.DEFINE_COLUMN( I_CURSOR, 2, LN_CONSUMO);
        DBMS_SQL.DEFINE_COLUMN( I_CURSOR, 3, LD_FECHA_ACTIVACION);
        DBMS_SQL.DEFINE_COLUMN( I_CURSOR, 4, LD_FECHA_INICIO);
        DBMS_SQL.DEFINE_COLUMN( I_CURSOR, 5, LD_FECHA_FIN);

        I_VOID := DBMS_SQL.EXECUTE(I_CURSOR);
        LOOP
            I_ROWS := DBMS_SQL.FETCH_ROWS(I_CURSOR);
            IF I_ROWS > 0 THEN
                BEGIN
                    DBMS_SQL.COLUMN_VALUE(I_CURSOR, 1, LV_CUENTA);
                    DBMS_SQL.COLUMN_VALUE(I_CURSOR, 2, LN_CONSUMO);
                    DBMS_SQL.COLUMN_VALUE(I_CURSOR, 3, LD_FECHA_ACTIVACION);
                    DBMS_SQL.COLUMN_VALUE(I_CURSOR, 4, LD_FECHA_INICIO);
                    DBMS_SQL.COLUMN_VALUE(I_CURSOR, 5, LD_FECHA_FIN);

                    IF PN_MES = 1 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_1 = LN_CONSUMO, D.FECHA_INICIO_1 = LD_FECHA_INICIO, D.FECHA_FIN_1 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    ELSIF PN_MES = 2 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_2 = LN_CONSUMO, D.FECHA_INICIO_2 = LD_FECHA_INICIO, D.FECHA_FIN_2 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    ELSIF PN_MES = 3 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_3 = LN_CONSUMO, D.FECHA_INICIO_3 = LD_FECHA_INICIO, D.FECHA_FIN_3 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    ELSIF PN_MES = 4 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_4 = LN_CONSUMO, D.FECHA_INICIO_4 = LD_FECHA_INICIO, D.FECHA_FIN_4 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    ELSIF PN_MES = 5 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_5 = LN_CONSUMO, D.FECHA_INICIO_5 = LD_FECHA_INICIO, D.FECHA_FIN_5 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    ELSIF PN_MES = 6 THEN
                        UPDATE COMI_REPORTE_BULK_DAT D
                        SET D.CONSUMO_MES_6 = LN_CONSUMO, D.FECHA_INICIO_6 = LD_FECHA_INICIO, D.FECHA_FIN_6 = LD_FECHA_FIN
                        WHERE D.CODIGO_DOC = LV_CUENTA;
                    END IF;
                    COMMIT;
                EXCEPTION
                    WHEN OTHERS THEN
                        PV_MSG_ERROR := 'ERROR AL ACTUALIZAR EL CONSUMO DEL '||PN_MES||' MES - CUENTA: '||LV_CUENTA||': '||SQLERRM;
                        RAISE LE_ERROR;
                END;
            ELSE
                DBMS_SQL.CLOSE_CURSOR(I_CURSOR);
                EXIT;
            END IF;
        END LOOP;
    EXCEPTION
        WHEN LE_ERROR THEN
            ROLLBACK;
            NULL;
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR GENERAL AL PROCESAR DATOS PARA '|| PN_MES ||' MES: '||SQLERRM;
    END;

    PROCEDURE LLENA_DATOS( PN_ANIO              IN NUMBER
                          ,PN_PERI              IN NUMBER
                          ,PD_FECHA_INICIO      IN DATE
                          ,PD_FECHA_FIN         IN DATE
                          ,PD_FECHA_FACTURACION IN DATE
                          ,PV_CUENTA            IN VARCHAR2
                          ,PV_MSG_ERROR         IN OUT VARCHAR2) AS

        --CURSOR QUE RECUPERA LAS CUENTAS PARA EL REPORTE DE COMISIONES.
        CURSOR LC_CUENTAS(CD_FECHA_INICIO DATE, CD_FECHA_FIN DATE, CD_FECHA_FACTURACION DATE, CV_CUENTA VARCHAR2) IS
            SELECT *
            FROM CUSTOMER_ALL C
            WHERE C.CUSTCODE = NVL(CV_CUENTA, C.CUSTCODE)
                  AND C.CSACTIVATED BETWEEN CD_FECHA_INICIO AND CD_FECHA_FIN
                  AND C.PRGCODE IN ( 5, 6) -- 5 Y 6 SON PARA BULK.
                  AND C.CUSTOMER_ID_HIGH IS NULL
                  AND ( C.CSTYPE = 'a' OR ( C.CSTYPE = 'd' AND C.CSDEACTIVATED > CD_FECHA_FACTURACION ));

        -- CURSOR QUE RECUPERA EL NUMERO DE SERVICIOS BULK POR CUENTA.
        CURSOR LC_CONT_SERVICIOS( CN_CUSTOMER_ID VARCHAR2, CD_FECHA_FACTURACION DATE) IS
            SELECT --+first_rows
                 COUNT(m.CO_ID) CONT
            FROM CUSTOMER_ALL       C,
                 CONTRACT_ALL       E,
                 (SELECT CO_ID, CH_SEQNO, CH_STATUS, CH_REASON, CH_VALIDFROM, ENTDATE, USERLASTMOD, REC_VERSION, REQUEST
                  FROM CONTRACT_HISTORY y
                  WHERE y.CH_SEQNO = ( SELECT MAX(x.CH_SEQNO)
                                 FROM CONTRACT_HISTORY x
                                 WHERE x.CO_ID = y.CO_ID
                                 AND x.CH_VALIDFROM < CD_FECHA_FACTURACION ))   M,
                 DIRECTORY_NUMBER   F,
                 CONTR_SERVICES_CAP L
            WHERE c.CUSTOMER_ID_HIGH = CN_CUSTOMER_ID AND
                  e.CUSTOMER_ID = c.CUSTOMER_ID AND
                  M.CO_ID = E.CO_ID AND
                  L.DN_ID = F.DN_ID AND
                  L.CO_ID = E.CO_ID AND
                  L.MAIN_DIRNUM = 'X'
                  AND ((M.CH_STATUS = 'a' AND CD_FECHA_FACTURACION BETWEEN L.CS_ACTIV_DATE AND NVL(L.CS_DEACTIV_DATE,CD_FECHA_FACTURACION))
                      OR
                      (M.CH_STATUS <> 'a' AND CD_FECHA_FACTURACION BETWEEN L.CS_ACTIV_DATE AND L.CS_DEACTIV_DATE))
                  AND e.TMCODE IN (
                      584,585,586,608,609,610,611,612,613,614,615,616,
                      629,630,631,632,633,634,635,636,637,638,639,640,
                      641,642,643,662,663,664,666,667,668,669,670,671,
                      -- jes 16-04-2007 se a�aden los codigos de 2 planes bulk nuevos
                      779,780,781,781,
					  -- jes 11-03-2008
					  779,780,781,781,926,927,928,928,1004,1005,1007,1008,
                      1010,1011,1013,1014,1006,1006,1009,1009,1012,1012,1015,1015
                      );

        LE_ERROR       EXCEPTION;
        LN_CONT_LINEAS NUMBER;
        LD_FECHA_FACTURACION DATE;
    BEGIN
        FOR I IN  LC_CUENTAS( PD_FECHA_INICIO, PD_FECHA_FIN, PD_FECHA_FACTURACION, PV_CUENTA) LOOP
            BEGIN
                IF I.CSACTIVATED > PD_FECHA_FACTURACION THEN
                    LD_FECHA_FACTURACION := ADD_MONTHS(PD_FECHA_FACTURACION, 1);
                ELSE
                    LD_FECHA_FACTURACION := PD_FECHA_FACTURACION;
                END IF;
                OPEN LC_CONT_SERVICIOS(I.CUSTOMER_ID, LD_FECHA_FACTURACION);
                FETCH LC_CONT_SERVICIOS INTO LN_CONT_LINEAS;
                CLOSE LC_CONT_SERVICIOS;

                INSERT INTO COMI_REPORTE_BULK_DAT(  CODI_ANIO, CODI_PERI, FECHA_FACTURACION, COMPANIA,
                                                    CODIGO_DOC, IDENTIFICACION, NOMBRES, APELLIDOS, CONT_LINEAS,
                                                    CONSUMO_TOTAL, FECHA_ACTIVACION, FECHA_FINAL,
                                                    CONSUMO_MES_1, FECHA_INICIO_1, FECHA_FIN_1,
                                                    CONSUMO_MES_2, FECHA_INICIO_2, FECHA_FIN_2, ESTADO, FECHA_REGISTRO)
                VALUES( PN_ANIO, PN_PERI, LD_FECHA_FACTURACION, NULL,
                        I.CUSTCODE, NULL, NULL, NULL, LN_CONT_LINEAS,
                        NULL, I.CSACTIVATED, I.CSDEACTIVATED,
                        NULL, NULL, NULL,
                        NULL, NULL, NULL, I.CSTYPE, SYSDATE);
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    PV_MSG_ERROR := 'ERROR AL INSERTAR LOS DATOS - CUENTA: '||I.CUSTCODE||': '||SQLERRM;
                    RAISE LE_ERROR;
            END;
        END LOOP;
    EXCEPTION
        WHEN LE_ERROR THEN
            ROLLBACK;
            NULL;
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR GENERAL AL INSERTAR LOS DATOS: '||SQLERRM;
            RAISE LE_ERROR;
    END;

    PROCEDURE COMSUMO_TOTAL( PV_CUENTA            IN VARCHAR2
                            ,PD_FECHA_ACTIVACION  IN DATE
                            ,PD_FECHA_FACTURACION IN DATE
                            ,PN_COMPANIA          IN OUT NUMBER
                            ,PV_IDENTIFICACION    IN OUT VARCHAR2
                            ,PV_NOMBRES           IN OUT VARCHAR2
                            ,PV_APELLIDOS         IN OUT VARCHAR2
                            ,PN_CONSUMO           IN OUT NUMBER
                            ,PD_FECHA_FINAL       IN OUT DATE
                            ,PV_MSG_ERROR         IN OUT VARCHAR2 ) AS
        LN_CURSOR            INTEGER;
        LN_VOID              INTEGER;
        LN_ROWS              INTEGER;
        LV_SENTENCIA         VARCHAR2(4000);
        LE_ERROR             EXCEPTION;
        LD_FECHA_FACTURACION DATE;
        LV_TABLA             VARCHAR2(100);

        LB_PRIMERO           BOOLEAN;

        LV_CUENTA            VARCHAR2(24);
        LN_COMPANIA          NUMBER;
        LN_CONSUMO           NUMBER;
        LV_IDENTIFICACION    VARCHAR2(20);
        LV_NOMBRES           VARCHAR2(40);
        LV_APELLIDOS         VARCHAR2(40);
    BEGIN
        LV_SENTENCIA := NULL;
        LD_FECHA_FACTURACION := PD_FECHA_FACTURACION;
        PN_CONSUMO := 0;
        LB_PRIMERO := TRUE;
        LOOP
            comi_reporte_bulk.VERIFICA_TABLA(LD_FECHA_FACTURACION, LV_TABLA,PV_MSG_ERROR);
            EXIT WHEN PV_MSG_ERROR IS NOT NULL;
            IF LB_PRIMERO THEN
                LB_PRIMERO := FALSE;
            ELSE
                LV_SENTENCIA := LV_SENTENCIA || ' UNION ';
            END IF;
            LV_SENTENCIA := LV_SENTENCIA ||
                            'SELECT S.CUENTA, (BALANCE_12 - INTER_INTERNACIONAL - INTER_INTRA_REG_INTER_REG - INTER_LOCAL - IVA - ICE) CONSUMO, ' ||
                            'S.COMPANIA, ' ||
                            'S.RUC, ' ||
                            'S.NOMBRES, ' ||
                            'S.APELLIDOS ' ||
                            'FROM '||LV_TABLA||' S '||
                            'WHERE S.CUENTA = :PV_CUENTA';
            LD_FECHA_FACTURACION := ADD_MONTHS( LD_FECHA_FACTURACION, 1);
        END LOOP;
        PD_FECHA_FINAL := ADD_MONTHS( LD_FECHA_FACTURACION, -1);
        PV_MSG_ERROR := NULL;

        LN_CURSOR := dbms_sql.open_cursor;
        DBMS_SQL.PARSE( LN_CURSOR, LV_SENTENCIA, dbms_sql.v7);
        DBMS_SQL.BIND_VARIABLE( LN_CURSOR,':PV_CUENTA', PV_CUENTA);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 1, LV_CUENTA, 24);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 2, LN_CONSUMO);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 3, LN_COMPANIA);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 4, LV_IDENTIFICACION, 20);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 5, LV_NOMBRES, 40);
        DBMS_SQL.DEFINE_COLUMN( LN_CURSOR, 6, LV_APELLIDOS, 40);

        LN_VOID := DBMS_SQL.EXECUTE( LN_CURSOR);
        LB_PRIMERO := TRUE;
        LOOP
            LN_ROWS := DBMS_SQL.FETCH_ROWS(LN_CURSOR);
            IF LN_ROWS > 0 THEN
                BEGIN
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 1, LV_CUENTA);
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 2, LN_CONSUMO);
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 3, LN_COMPANIA);
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 4, LV_IDENTIFICACION);
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 5, LV_NOMBRES);
                    DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 6, LV_APELLIDOS);

                    PN_CONSUMO        := PN_CONSUMO + LN_CONSUMO;
                    IF LB_PRIMERO THEN
                        LB_PRIMERO := FALSE;
                        PV_IDENTIFICACION := LV_IDENTIFICACION;
                        PV_NOMBRES        := LV_NOMBRES;
                        PV_APELLIDOS      := LV_APELLIDOS;
                        PN_COMPANIA       := LN_COMPANIA;
                    END IF;
                EXCEPTION
                    WHEN OTHERS THEN
                        PV_MSG_ERROR := 'ERROR CONTAR  EL CONSUMO TOTAL DE LA CUENTA '||PV_CUENTA||' : '||SQLERRM;
                        RAISE LE_ERROR;
                END;
            ELSE
                DBMS_SQL.CLOSE_CURSOR(LN_CURSOR);
                EXIT;
            END IF;
        END LOOP;
    EXCEPTION
        WHEN LE_ERROR THEN
            NULL;
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'ERROR AL BUSCAR EL COMSUMO TOTAL DE LAS CUENTA: '||PV_CUENTA||' : '||SQLERRM;
    END;

    PROCEDURE LLENA_CONSUMO_TOTAL( PN_ANIO      IN NUMBER
                                  ,PN_PERI      IN NUMBER
                                  ,PV_CUENTA    IN VARCHAR2
                                  ,PV_MSG_ERROR IN OUT VARCHAR2) AS
        CURSOR LC_DATOS IS
            SELECT *
            FROM COMI_REPORTE_BULK_DAT C
            WHERE C.CODI_ANIO = PN_ANIO
                  AND C.CODI_PERI = PN_PERI
                  AND C.CODIGO_DOC = NVL(PV_CUENTA, C.CODIGO_DOC);
        LE_ERROR EXCEPTION;
    BEGIN
        FOR C IN LC_DATOS LOOP
            BEGIN
                COMI_REPORTE_BULK.COMSUMO_TOTAL(C.CODIGO_DOC, C.FECHA_ACTIVACION, C.FECHA_FACTURACION, C.COMPANIA, C.IDENTIFICACION, C.NOMBRES, C.APELLIDOS, C.CONSUMO_TOTAL, C.FECHA_FINAL, PV_MSG_ERROR );
                IF PV_MSG_ERROR IS NOT NULL THEN
                    RAISE LE_ERROR;
                END IF;
                UPDATE COMI_REPORTE_BULK_DAT D
                SET D.COMPANIA = C.COMPANIA,
                    D.IDENTIFICACION = C.IDENTIFICACION,
                    D.NOMBRES = C.NOMBRES,
                    D.APELLIDOS = C.APELLIDOS,
                    D.CONSUMO_TOTAL = C.CONSUMO_TOTAL,
                    D.FECHA_FINAL = C.FECHA_FINAL
                WHERE D.CODIGO_DOC = C.CODIGO_DOC;
                COMMIT;
            EXCEPTION
                WHEN LE_ERROR THEN
                    RAISE LE_ERROR;
                WHEN OTHERS THEN
                    PV_MSG_ERROR := 'ERROR AL BUSCAR EL CONSUMO TOTAL DE LA CUENTA '||C.CODIGO_DOC||': '||SQLERRM;
                    RAISE LE_ERROR;
            END;
        END LOOP;
    EXCEPTION
        WHEN LE_ERROR THEN
            ROLLBACK;
            NULL;
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR AL BUSCAR EL CONSUMO TOTAL PARA LAS CUENTAS: '||SQLERRM;
    END;

    PROCEDURE GENERA_REPORTE( PN_ANIO       IN NUMBER
                             ,PN_PERI       IN NUMBER
                             ,PV_CUENTA     IN VARCHAR2
                             ,PV_MSG_ERROR  IN OUT VARCHAR2) IS
        -----------------------------------------------------------------------
        -- JES - 01-SEP-2005
        -- DESARROLLO DEL PROCEDIMIENTO PARA EL PROYECTO DE REPORTES PARA BULK.
        -----------------------------------------------------------------------

        LD_FECHA_FACTURACION DATE;
        LD_FECHA_INICIO      DATE;
        LD_FECHA_FIN         DATE;



        LE_ERROR             EXCEPTION;
    BEGIN
        PV_MSG_ERROR := NULL;
        -- DETERMINA LAS FECHAS A UTILIZAR EN EL PROCESO.
        LD_FECHA_FACTURACION := TO_DATE('24-'||PN_PERI||'-'||PN_ANIO||' 00:00:00','DD-MM-YYYY HH24:MI:SS');
        LD_FECHA_INICIO      := TO_DATE('01-'||PN_PERI||'-'||PN_ANIO||' 00:00:00','DD-MM-YYYY HH24:MI:SS');
        LD_FECHA_FIN         := LAST_DAY(LD_FECHA_INICIO) + 1 - (1/(24*60*60));

        -- ELIMINO LOS DATOS DE PROCESO ANTERIORES.
        COMI_REPORTE_BULK.ELIMINA_DATOS(PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --OBTENGO LOS DATOS
        COMI_REPORTE_BULK.LLENA_DATOS( PN_ANIO
                                      ,PN_PERI
                                      ,LD_FECHA_INICIO
                                      ,LD_FECHA_FIN
                                      ,LD_FECHA_FACTURACION
                                      ,PV_CUENTA
                                      ,PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

        -- BUSCO LOS SALDOS DEL PRIMER MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 1, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

        --BUSCO LOS SALDOS DEL SEGUNDO MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 2, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        -- BUSCO EL CONSUMO TOTAL...
        COMI_REPORTE_BULK.LLENA_CONSUMO_TOTAL(PN_ANIO,PN_PERI, PV_CUENTA,PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

    EXCEPTION
        WHEN LE_ERROR THEN
            NULL;
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'Error general: '||SQLERRM;
    END;

    PROCEDURE GENERA_REPORTE_6MESES( PN_ANIO       IN NUMBER
                                    ,PN_PERI       IN NUMBER
                                    ,PV_CUENTA     IN VARCHAR2
                                    ,PV_MSG_ERROR  IN OUT VARCHAR2) IS
        -----------------------------------------------------------------------
        -- JES - 25-NOV-2005
        -- DESARROLLO DEL PROCEDIMIENTO PARA EL PROYECTO DE REPORTES PARA BULK.
        -----------------------------------------------------------------------

        LD_FECHA_FACTURACION DATE;
        LD_FECHA_INICIO      DATE;
        LD_FECHA_FIN         DATE;

        LE_ERROR             EXCEPTION;
    BEGIN
        PV_MSG_ERROR := NULL;
        -- DETERMINA LAS FECHAS A UTILIZAR EN EL PROCESO.
        LD_FECHA_FACTURACION := TO_DATE('24-'||PN_PERI||'-'||PN_ANIO||' 00:00:00','DD-MM-YYYY HH24:MI:SS');
        LD_FECHA_INICIO      := TO_DATE('01-'||PN_PERI||'-'||PN_ANIO||' 00:00:00','DD-MM-YYYY HH24:MI:SS');
        LD_FECHA_FIN         := LAST_DAY(LD_FECHA_INICIO) + 1 - (1/(24*60*60));

        -- ELIMINO LOS DATOS DE PROCESO ANTERIORES.
        COMI_REPORTE_BULK.ELIMINA_DATOS(PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --OBTENGO LOS DATOS
        COMI_REPORTE_BULK.LLENA_DATOS( PN_ANIO
                                      ,PN_PERI
                                      ,LD_FECHA_INICIO
                                      ,LD_FECHA_FIN
                                      ,LD_FECHA_FACTURACION
                                      ,PV_CUENTA
                                      ,PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

        -- BUSCO LOS SALDOS DEL PRIMER MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 1, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

        --BUSCO LOS SALDOS DEL SEGUNDO MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 2, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --BUSCO LOS SALDOS DEL TERCER MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 3, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --BUSCO LOS SALDOS DEL CUARTO MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 4, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --BUSCO LOS SALDOS DEL QUINTO MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 5, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        --BUSCO LOS SALDOS DEL SEXTO MES DE FACTURACION COMPLETO.
        COMI_REPORTE_BULK.SALDOS_MENSUALES(LD_FECHA_FACTURACION, 6, PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;
        -- BUSCO EL CONSUMO TOTAL...
        COMI_REPORTE_BULK.LLENA_CONSUMO_TOTAL(PN_ANIO,PN_PERI, PV_CUENTA,PV_MSG_ERROR);
        IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;

    EXCEPTION
        WHEN LE_ERROR THEN
            NULL;
        WHEN OTHERS THEN
            PV_MSG_ERROR := 'Error general: '||SQLERRM;
    END;


    PROCEDURE EXPORTA_REPORTE( PV_MSG_ERROR  IN OUT VARCHAR2) IS
    BEGIN
        INSERT INTO COMI_REPORTE_BULK_DAT@AXIS(CODI_ANIO,
                                            CODI_PERI,
                                            FECHA_FACTURACION,
                                            COMPANIA,
                                            CODIGO_DOC,
                                            IDENTIFICACION,
                                            NOMBRES,
                                            APELLIDOS,
                                            CONT_LINEAS,
                                            CONSUMO_TOTAL,
                                            FECHA_ACTIVACION,
                                            FECHA_FINAL,
                                            CONSUMO_MES_1,
                                            FECHA_INICIO_1,
                                            FECHA_FIN_1,
                                            CONSUMO_MES_2,
                                            FECHA_INICIO_2,
                                            FECHA_FIN_2,
                                            ESTADO_BSCS,
                                            FECHA_REGISTRO_BSCS)
        --INSERT INTO COMI_REPORTE_BULK_DAT@AXIS
                    SELECT CODI_ANIO,
                   CODI_PERI,
                   FECHA_FACTURACION,
                   COMPANIA,
                   CODIGO_DOC,
                   IDENTIFICACION,
                   NOMBRES,
                   APELLIDOS,
                   CONT_LINEAS,
                   CONSUMO_TOTAL,
                   FECHA_ACTIVACION,
                   FECHA_FINAL,
                   CONSUMO_MES_1,
                   FECHA_INICIO_1,
                   FECHA_FIN_1,
                   CONSUMO_MES_2,
                   FECHA_INICIO_2,
                   FECHA_FIN_2,
                   ESTADO,
                   FECHA_REGISTRO
            FROM COMI_REPORTE_BULK_DAT;
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR AL EXPORTAR DATOS: '||SQLERRM;
    END;

    PROCEDURE EXPORTA_REPORTE_6MESES( PV_MSG_ERROR  IN OUT VARCHAR2) IS
    BEGIN
        INSERT INTO COMI_REPORTE_BULK_DAT@AXIS(CODI_ANIO,
                                            CODI_PERI,
                                            FECHA_FACTURACION,
                                            COMPANIA,
                                            CODIGO_DOC,
                                            IDENTIFICACION,
                                            NOMBRES,
                                            APELLIDOS,
                                            CONT_LINEAS,
                                            CONSUMO_TOTAL,
                                            FECHA_ACTIVACION,
                                            FECHA_FINAL,
                                            CONSUMO_MES_1,
                                            FECHA_INICIO_1,
                                            FECHA_FIN_1,
                                            CONSUMO_MES_2,
                                            FECHA_INICIO_2,
                                            FECHA_FIN_2,
                                            ESTADO_BSCS,
                                            FECHA_REGISTRO_BSCS,
                                            CONSUMO_MES_3,
                                            FECHA_INICIO_3,
                                            FECHA_FIN_3,
                                            CONSUMO_MES_4,
                                            FECHA_INICIO_4,
                                            FECHA_FIN_4,
                                            CONSUMO_MES_5,
                                            FECHA_INICIO_5,
                                            FECHA_FIN_5,
                                            CONSUMO_MES_6,
                                            FECHA_INICIO_6,
                                            FECHA_FIN_6)
        --INSERT INTO COMI_REPORTE_BULK_DAT@AXIS
                    SELECT CODI_ANIO,
                   CODI_PERI,
                   FECHA_FACTURACION,
                   COMPANIA,
                   CODIGO_DOC,
                   IDENTIFICACION,
                   NOMBRES,
                   APELLIDOS,
                   CONT_LINEAS,
                   CONSUMO_TOTAL,
                   FECHA_ACTIVACION,
                   FECHA_FINAL,
                   CONSUMO_MES_1,
                   FECHA_INICIO_1,
                   FECHA_FIN_1,
                   CONSUMO_MES_2,
                   FECHA_INICIO_2,
                   FECHA_FIN_2,
                   ESTADO,
                   FECHA_REGISTRO,
                   CONSUMO_MES_3,
                   FECHA_INICIO_3,
                   FECHA_FIN_3,
                   CONSUMO_MES_4,
                   FECHA_INICIO_4,
                   FECHA_FIN_4,
                   CONSUMO_MES_5,
                   FECHA_INICIO_5,
                   FECHA_FIN_5,
                   CONSUMO_MES_6,
                   FECHA_INICIO_6,
                   FECHA_FIN_6
            FROM COMI_REPORTE_BULK_DAT;
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            PV_MSG_ERROR := 'ERROR AL EXPORTAR DATOS: '||SQLERRM;
    END;

END;
/
