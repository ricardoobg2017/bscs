CREATE OR REPLACE PACKAGE REPORTE_FACTURA_FINAN IS

  PROCEDURE PRC_REPORTE_FINAN(PN_HILO      NUMBER,
                              PV_FECHA_INI VARCHAR2,
                              PV_FECHA_FIN VARCHAR2,
                              PV_ERROR     OUT VARCHAR2);

  PROCEDURE PRC_CONSULTA_FACTURA(PN_HILO      NUMBER,
                                 PV_FECHA_INI VARCHAR2,
                                 PV_FECHA_FIN VARCHAR2,
                                 PV_ERROR     OUT VARCHAR2);
  PROCEDURE PRC_GENERA_REPORTE(PN_HILO      NUMBER,
                               PV_FECHA_INI VARCHAR2,
                               PV_FECHA_FIN VARCHAR2,
                               PV_ERROR     OUT VARCHAR2);

  FUNCTION FNC_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;

END REPORTE_FACTURA_FINAN;
/
CREATE OR REPLACE PACKAGE BODY REPORTE_FACTURA_FINAN IS
  --=====================================================================================================--
  -- Creado por:        SUD Maricela LLiguicota
  -- Lider SIS:         SIS Paola Carvajal.
  -- Lider PDS:         SUD Gina Cabezas.
  -- Fecha de creacion: 06 de Noviembre del 2017
  -- Proyecto:          [11565] Garantia Extendida
  -- Descripcion:       Proceso  que obtiene la base de datos para el reporte.
  --=====================================================================================================--
  PROCEDURE PRC_REPORTE_FINAN(PN_HILO      NUMBER,
                              PV_FECHA_INI VARCHAR2,
                              PV_FECHA_FIN VARCHAR2,
                              PV_ERROR     OUT VARCHAR2) IS
  
    CURSOR C_FEES(CN_HILO NUMBER, CD_FECHA_INI VARCHAR2, CD_FECHA_FIN VARCHAR2) IS
      SELECT *
        FROM FEES C
       WHERE C.INSERTIONDATE >=
             TO_DATE(CD_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
         AND C.INSERTIONDATE <=
             TO_DATE(CD_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
         AND C.USERNAME = 'SISFINAN'
         AND C.REMARK LIKE '%SISFINAN:%'
         AND MOD(C.CUSTOMER_ID,
                 NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                       'TOTAL_HILOS_EJE'),
                     1)) = NVL(CN_HILO, 0);
  
    CURSOR C_OBTIENE_DATOS(CI_CUSTOMERID INTEGER, CI_SEQNO INTEGER, CI_SNCODE INTEGER) IS
      SELECT *
        FROM FEES_REPORTE_FACTURA_BASE A
       WHERE A.CUSTOMER_ID = CI_CUSTOMERID
         AND A.SEQNO = CI_SEQNO
         AND A.SNCODE = CI_SNCODE;
  
    LD_CORTE         DATE;
    
    LV_CADENA        VARCHAR2(100);
    LC_OBTIENE_DATOS C_OBTIENE_DATOS%ROWTYPE;
    LN_CUOTA         NUMBER := 0;
    LN_FINAN         NUMBER := 0;
    LN_CON1          NUMBER := 0;
    LN_CON2          NUMBER := 0;
    LN_CANT          NUMBER := 0;
    LV_ERROR         VARCHAR2(1000) := NULL;
    LV_TABLA         VARCHAR2(1000) := NULL;
    LV_CICLO         VARCHAR2(2) := NULL;
    LV_MES_LAT       VARCHAR2(50) := NULL;
    LV_DIA           VARCHAR2(50) := NULL;
    LV_ANIO          VARCHAR2(50) := NULL;
  
    TYPE T_ELEMENTOSREPORTE IS TABLE OF C_FEES%ROWTYPE;
    ELEMENTOSREPORTE T_ELEMENTOSREPORTE;
  
  BEGIN
  
    OPEN C_FEES(PN_HILO, PV_FECHA_INI, PV_FECHA_FIN);
    LOOP
      FETCH C_FEES BULK COLLECT
        INTO ELEMENTOSREPORTE LIMIT 1000;
      EXIT WHEN ELEMENTOSREPORTE.COUNT = 0;
      FOR I IN 1 .. ELEMENTOSREPORTE.COUNT LOOP
      
        LN_CUOTA := NULL;
      
        INSERT INTO FEES_REPORTE_FACTURA_BASE
          (CUSTOMER_ID,
           SEQNO,
           AMOUNT,
           REMARK,
           ENTDATE,
           PERIOD,
           VALID_FROM,
           SNCODE,
           LASTMODDATE)
        VALUES
          (ELEMENTOSREPORTE(I).CUSTOMER_ID,
           ELEMENTOSREPORTE(I).SEQNO,
           ELEMENTOSREPORTE(I).AMOUNT,
           ELEMENTOSREPORTE(I).REMARK,
           ELEMENTOSREPORTE(I).ENTDATE,
           ELEMENTOSREPORTE(I).PERIOD,
           ELEMENTOSREPORTE(I).VALID_FROM,
           ELEMENTOSREPORTE(I).SNCODE,
           ELEMENTOSREPORTE(I).LASTMODDATE);
      
        LV_CADENA := SUBSTR(ELEMENTOSREPORTE(I).REMARK,
                            INSTR(ELEMENTOSREPORTE(I).REMARK, '_', -1) + 1);
        LN_FINAN  := TO_NUMBER(LV_CADENA);
      
        LN_CON1 := INSTR(ELEMENTOSREPORTE(I).REMARK, '_', -1, 4); --70   
        LN_CON2 := INSTR(ELEMENTOSREPORTE(I).REMARK, '_', -1, 3); --73
      
        IF (LN_CON2 - LN_CON1) = 2 THEN
        
          LN_CUOTA := TO_NUMBER(SUBSTR(ELEMENTOSREPORTE(I).REMARK,
                                       INSTR(ELEMENTOSREPORTE(I).REMARK,
                                             '_',
                                             -1,
                                             4) + 1,
                                       1));
        
        ELSE
        
          LN_CUOTA := TO_NUMBER(SUBSTR(ELEMENTOSREPORTE(I).REMARK,
                                       INSTR(ELEMENTOSREPORTE(I).REMARK,
                                             '_',
                                             -1,
                                             4) + 1,
                                       2));
        
        END IF;
      
        UPDATE FEES_REPORTE_FACTURA_BASE C
           SET C.FINAN = LN_FINAN, C.quota_finan = LN_CUOTA
         WHERE C.CUSTOMER_ID = ELEMENTOSREPORTE(I)
        .CUSTOMER_ID
           AND C.SEQNO = ELEMENTOSREPORTE(I)
        .SEQNO
           AND C.SNCODE = ELEMENTOSREPORTE(I).SNCODE;
      
        LC_OBTIENE_DATOS := NULL;
        LV_DIA:=NULL;
        LV_ANIO:=NULL;
        LV_MES_LAT:=NULL;
        
        
        OPEN C_OBTIENE_DATOS(ELEMENTOSREPORTE(I).CUSTOMER_ID,
                             ELEMENTOSREPORTE(I).SEQNO,
                             ELEMENTOSREPORTE(I).SNCODE);
        FETCH C_OBTIENE_DATOS
          INTO LC_OBTIENE_DATOS; --ENDATE
        CLOSE C_OBTIENE_DATOS;
      
        LV_MES_LAT := TO_CHAR(LC_OBTIENE_DATOS.LASTMODDATE, 'MM');
        IF LV_MES_LAT IS NULL THEN
          LV_MES_LAT := TO_CHAR(LC_OBTIENE_DATOS.ENTDATE, 'MM');
        END IF;
      
        LV_DIA  := TO_CHAR(LC_OBTIENE_DATOS.ENTDATE, 'DD');
        LV_ANIO := TO_CHAR(LC_OBTIENE_DATOS.ENTDATE, 'YYYY');
      
        IF LV_DIA BETWEEN 24 AND 31 THEN
          LV_TABLA     := 'CO_FACT_02' || LV_MES_LAT || LV_ANIO;
          LV_CICLO     := '04';
          LD_CORTE     := TO_DATE('02/' || LV_MES_LAT || LV_ANIO,
                                  'DD/MM/YYYY');
        END IF;
      
        IF LV_DIA = 01 THEN
          LV_TABLA := 'CO_FACT_02' || LV_MES_LAT || LV_ANIO;
          LV_CICLO := '04';
          LD_CORTE := TO_DATE('02/' || LV_MES_LAT || LV_ANIO, 'DD/MM/YYYY');
        END IF;
      
        IF LV_DIA BETWEEN 4 AND 7 THEN
          LV_TABLA := 'CO_FACT_08' || LV_MES_LAT || LV_ANIO;
          LV_CICLO := '02';
          LD_CORTE := TO_DATE('08/' || LV_MES_LAT || LV_ANIO, 'DD/MM/YYYY');
        
        END IF;
        IF LV_DIA BETWEEN 11 AND 14 THEN
          LV_TABLA := 'CO_FACT_15' || LV_MES_LAT || LV_ANIO;
          LV_CICLO := '03';
          LD_CORTE := TO_DATE('15/' || LV_MES_LAT || LV_ANIO, 'DD/MM/YYYY');
        
        END IF;
        IF LV_DIA BETWEEN 20 AND 23 THEN
          LV_TABLA := 'CO_FACT_24' || LV_MES_LAT || LV_ANIO;
          LV_CICLO := '01';
          LD_CORTE := TO_DATE('24/' || LV_MES_LAT || LV_ANIO, 'DD/MM/YYYY');
        
        END IF;
      
        UPDATE FEES_REPORTE_FACTURA_BASE C
           SET C.TABLE_COFACT = LV_TABLA,
               C.BILL_CYCLE   = LV_CICLO,
               C.BILL_CUT     = LD_CORTE
         WHERE C.CUSTOMER_ID = ELEMENTOSREPORTE(I)
        .CUSTOMER_ID
           AND C.SEQNO = ELEMENTOSREPORTE(I)
        .SEQNO
           AND C.SNCODE = ELEMENTOSREPORTE(I).SNCODE;
      
        COMMIT;
      END LOOP;
      IF LN_CANT = NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                         'NUM_COMMIT'),
                       1) THEN
        COMMIT;
        LN_CANT := 0;
      END IF;
      LN_CANT := LN_CANT + 1;
    END LOOP;
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR := 'ERROR GENERAL' || SQLERRM;
      PV_ERROR := LV_ERROR;
    
  END PRC_REPORTE_FINAN;
  --=====================================================================================================--
  -- Creado por:        SUD Maricela LLiguicota
  -- Lider SIS:         SIS Paola Carvajal.
  -- Lider PDS:         SUD Gina Cabezas.
  -- Fecha de creacion: 06 de Noviembre del 2017
  -- Proyecto:          [11565] Garantia Extendida
  -- Descripcion:       Proceso que obtiene los datos de la facturacion correspondiente al ciclo.
  --=====================================================================================================--
  PROCEDURE PRC_CONSULTA_FACTURA(PN_HILO      NUMBER,
                                 PV_FECHA_INI VARCHAR2,
                                 PV_FECHA_FIN VARCHAR2,
                                 PV_ERROR     OUT VARCHAR2) IS
  
    CURSOR C_REPORTE(CV_FECHA_INI VARCHAR2, CV_FECHA_FIN VARCHAR2, CN_HILO NUMBER) IS
      SELECT C.TABLE_COFACT, C.CUSTOMER_ID, C.SEQNO, C.SNCODE, C.REMARK
        FROM FEES_REPORTE_FACTURA_BASE C
       WHERE C.ENTDATE >=
             TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
         AND C.ENTDATE <=
             TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
         AND C.INVOICE_NUMBER IS NULL
         AND C.INVOICE_VALUE IS NULL
         AND C.DEBT IS NULL
         AND C.MARK IS NULL
         AND C.CUSTCODE IS NULL
         AND MOD(C.FINAN,
                 NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                       'TOTAL_HILOS_EJE'),
                     1)) = NVL(CN_HILO, 0);
  
    CURSOR C_MARK(CV_REMARK VARCHAR2) IS
      SELECT REGEXP_SUBSTR(CV_REMARK, '(\s)(\w*)(-*)(\w*)(:)') FROM DUAL;
  
    TYPE T_ELEMENTOSREPORTE IS TABLE OF C_REPORTE%ROWTYPE;
    ELEMENTOSREPORTE T_ELEMENTOSREPORTE;
  
    LN_DEUDA      NUMBER := 0;
    LN_CANT       NUMBER := 0;
    LV_SENTENCIA  VARCHAR2(1000) := NULL;
    LV_NUME_FACT  VARCHAR2(200) := '9999';
    LN_VALOR_FACT NUMBER := 0;
    LV_TABLA      VARCHAR2(200);
    LV_ERROR      VARCHAR2(1000) := NULL;
    LV_MARK       VARCHAR2(50);
    LV_CUSTCODE   VARCHAR(24) := '9999';
    LE_ERROR EXCEPTION;
    LE_ERROR_SCP EXCEPTION;
  
  
    BEGIN
      OPEN C_REPORTE(PV_FECHA_INI, PV_FECHA_FIN, PN_HILO);
      LOOP
        FETCH C_REPORTE BULK COLLECT
          INTO ELEMENTOSREPORTE LIMIT 1000;
        EXIT WHEN ELEMENTOSREPORTE.COUNT = 0;
        FOR I IN 1 .. ELEMENTOSREPORTE.COUNT LOOP
        
          BEGIN
            LV_NUME_FACT  := NULL;
            LN_VALOR_FACT := NULL;
            LN_DEUDA      := NULL;
            LV_MARK       := NULL;
            LV_CUSTCODE   := NULL;
          
            LV_TABLA := ELEMENTOSREPORTE(I).table_cofact;
          
            OPEN C_MARK(ELEMENTOSREPORTE(I).REMARK);
            FETCH C_MARK
              INTO LV_MARK;
            CLOSE C_MARK;
          
            LV_SENTENCIA := 'SELECT S.OHREFNUM,S.VALOR,NVL(P.OHOPNAMT_GL,0) DEUDA,S.CUSTCODE 
            FROM ' || LV_TABLA ||
                            ' S, ORDERHDR_ALL P 
            WHERE S.CUSTOMER_ID=' ||
                            ELEMENTOSREPORTE(I).CUSTOMER_ID || ' 
                           AND S.SERVICIO=' ||
                            ELEMENTOSREPORTE(I)
                           .SNCODE || ' 
                           AND S.OHREFNUM=P.OHREFNUM
                           AND P.CUSTOMER_ID=S.CUSTOMER_ID
                           AND P.OHREFNUM=S.OHREFNUM';
          
            EXECUTE IMMEDIATE LV_SENTENCIA
              INTO LV_NUME_FACT, LN_VALOR_FACT, LN_DEUDA, LV_CUSTCODE;
          
          EXCEPTION
            WHEN OTHERS THEN
              LN_DEUDA      := 0;
              LV_NUME_FACT  := '9999';
              LN_VALOR_FACT := 0;
              LV_CUSTCODE   := '9999';
            
          END;
          BEGIN
          
            UPDATE FEES_REPORTE_FACTURA_BASE C
               SET C.INVOICE_NUMBER = LV_NUME_FACT,
                   C.INVOICE_VALUE  = LN_VALOR_FACT,
                   C.DEBT           = LN_DEUDA,
                   C.MARK           = LV_MARK,
                   C.CUSTCODE       = LV_CUSTCODE
            
             WHERE C.CUSTOMER_ID = ELEMENTOSREPORTE(I)
            .CUSTOMER_ID
               AND C.SEQNO = ELEMENTOSREPORTE(I)
            .SEQNO
               AND C.SNCODE = ELEMENTOSREPORTE(I).SNCODE;
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := 'ERROR AL ACTUALIZAR LA TABLA :' || SQLERRM;
            
          END;
        
        END LOOP;
      
        IF LN_CANT = NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                           'NUM_COMMIT'),
                         1) THEN
          COMMIT;
          LN_CANT := 0;
        END IF;
        LN_CANT := LN_CANT + 1;
      END LOOP;
    
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR := 'ERROR GENERAL' || SQLERRM;
        PV_ERROR := LV_ERROR;
      
    END PRC_CONSULTA_FACTURA;
  --=====================================================================================================--
  -- Creado por:        SUD Maricela LLiguicota
  -- Lider SIS:         SIS Paola Carvajal.
  -- Lider PDS:         SUD Gina Cabezas.
  -- Fecha de creacion: 06 de Noviembre del 2017
  -- Proyecto:          [11565] Garantia Extendida
  -- Descripcion:       Proceso que genera reporte en la tabla final(co_fees_reporte_f) unificacion de datos
  --=====================================================================================================--
  PROCEDURE PRC_GENERA_REPORTE(PN_HILO      NUMBER,
                               PV_FECHA_INI VARCHAR2,
                               PV_FECHA_FIN VARCHAR2,
                               PV_ERROR     OUT VARCHAR2) IS
  
    CURSOR C_OBTIENE_DATO(CN_HILO NUMBER, CV_FECHA_INI VARCHAR2, CV_FECHA_FIN VARCHAR2) IS
      SELECT FINAN, COUNT(*)
        FROM FEES_REPORTE_FACTURA_BASE C
       WHERE C.ENTDATE >=
             TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
         AND C.ENTDATE <=
             TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
         AND MOD(C.FINAN,
                 NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                       'TOTAL_HILOS_EJE'),
                     1)) = NVL(CN_HILO, 0)
       GROUP BY FINAN;
  
    CURSOR C_VERIFICA_REG_FINAN(CN_FINAN NUMBER) IS
      SELECT DISTINCT (QUOTA_FINAN), FINAN
        FROM FEES_REPORTE_FACTURA_BASE
       WHERE FINAN = CN_FINAN
         AND MARK LIKE '%SISFINAN:%'
       ORDER BY QUOTA_FINAN ASC;
  
    CURSOR C_VERIFICA_REG_CUOTA(CN_FINAN NUMBER, CN_CUOTA NUMBER) IS
      SELECT T.*, T.ROWID
        FROM FEES_REPORTE_FACTURA_BASE T
       WHERE T.FINAN = CN_FINAN
         AND T.QUOTA_FINAN = CN_CUOTA;
  
    CURSOR C_VERIFICA_EXISTENTE(CN_FINAN NUMBER, CN_CUOTA NUMBER) IS
      SELECT T.*, T.ROWID
        FROM FEES_REPORTE_FACTURA_F T
       WHERE T.FINAN = CN_FINAN
         AND T.QUOTA_FINAN = CN_CUOTA;
  
    LC_VERIF_EXIS C_VERIFICA_EXISTENTE%ROWTYPE;
    LE_ERROR EXCEPTION;
    LB_FOUND BOOLEAN := FALSE;
    LE_ERROR_SCP EXCEPTION;
    LN_VALOR NUMBER := 0;
    LN_CUOTA NUMBER := 0;
    LN_FINAN NUMBER := 0;
    LN_CANT  NUMBER := 0;
    LV_MARK  VARCHAR2(30);
    LV_ERROR VARCHAR2(1000) := NULL;
  
  BEGIN
    --Obtengo la base de datos 
    FOR A IN C_OBTIENE_DATO(PN_HILO, PV_FECHA_INI, PV_FECHA_FIN) LOOP
      --Verifico el numero de registros que tiene el financiamiento  
      FOR B IN C_VERIFICA_REG_FINAN(A.FINAN) LOOP
        --Consulto por cuota y financiamiento  
        FOR C IN C_VERIFICA_REG_CUOTA(B.FINAN, B.QUOTA_FINAN) LOOP
        
          IF C.MARK = ' SISFINAN:' OR C.MARK = ' PLAZOVENCIDO-SISFINAN:' THEN
          
            OPEN C_VERIFICA_EXISTENTE(B.FINAN, B.QUOTA_FINAN);
            FETCH C_VERIFICA_EXISTENTE
              INTO LC_VERIF_EXIS;
            LB_FOUND := C_VERIFICA_EXISTENTE%NOTFOUND;
            CLOSE C_VERIFICA_EXISTENTE;
          
            IF LB_FOUND THEN
            
              INSERT INTO FEES_REPORTE_FACTURA_F
                (CUSTOMER_ID,
                 SEQNO,
                 AMOUNT,
                 REMARK,
                 ENTDATE,
                 PERIOD,
                 VALID_FROM,
                 FINAN,
                 QUOTA_FINAN,
                 INVOICE_NUMBER,
                 INVOICE_VALUE,
                 TABLE_COFACT,
                 CUSTCODE,
                 SNCODE,
                 BILL_CYCLE,
                 BILL_CUT,
                 MARK,
                 DEBT,
                 MANAGEMEN,
                 WARRANTY,
                 LASTMODDATE)
              VALUES
                (C.CUSTOMER_ID,
                 C.SEQNO,
                 C.AMOUNT,
                 C.REMARK,
                 C.ENTDATE,
                 C.PERIOD,
                 C.VALID_FROM,
                 C.FINAN,
                 C.QUOTA_FINAN,
                 C.INVOICE_NUMBER,
                 C.INVOICE_VALUE,
                 C.TABLE_COFACT,
                 C.CUSTCODE,
                 C.SNCODE,
                 C.BILL_CYCLE,
                 C.BILL_CUT,
                 C.MARK,
                 C.DEBT,
                 NULL,
                 NULL,
                 C.LASTMODDATE);
            
              IF LN_CANT = NVL(REPORTE_FACTURA_FINAN.FNC_OBTENER_VALOR_PARAMETRO(11565,
                                                                                 'NUM_COMMIT'),
                               1) THEN
                COMMIT;
                LN_CANT := 0;
              END IF;
            
              LN_CANT := LN_CANT + 1;
            
            END IF;
            LB_FOUND := FALSE;
            COMMIT;
          END IF;
        
          IF C.MARK = ' GESTIONCOBRO-SISFINAN:' THEN
            LN_VALOR := C.AMOUNT;
            LN_CUOTA := C.QUOTA_FINAN;
            LN_FINAN := C.FINAN;
            LV_MARK  := C.MARK;
          
          END IF;
        
          IF C.MARK = ' GARANTIAEXTENDIDA-SISFINAN:' THEN
          
            LN_VALOR := C.AMOUNT;
            LN_CUOTA := C.QUOTA_FINAN;
            LN_FINAN := C.FINAN;
            LV_MARK  := C.MARK;
          
          END IF;
        
        END LOOP;
      
        IF LV_MARK = ' GESTIONCOBRO-SISFINAN:' THEN
        
          UPDATE FEES_REPORTE_FACTURA_F T
             SET T.MANAGEMEN = LN_VALOR
           WHERE T.FINAN = LN_FINAN
             AND T.QUOTA_FINAN = LN_CUOTA;
        
        END IF;
      
        IF LV_MARK = ' GARANTIAEXTENDIDA-SISFINAN:' THEN
        
          UPDATE FEES_REPORTE_FACTURA_F T
             SET T.WARRANTY = LN_VALOR
           WHERE T.FINAN = LN_FINAN
             AND T.QUOTA_FINAN = LN_CUOTA;
        
        END IF;
      
        LN_VALOR := 0;
        LN_CUOTA := 0;
        LN_FINAN := 0;
      
        COMMIT;
      
      END LOOP;
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR := 'ERROR GENERAL' || SQLERRM;
      PV_ERROR := LV_ERROR;
    
  END PRC_GENERA_REPORTE;
  --=====================================================================================================--
  -- Creado por:        SUD Maricela LLiguicota
  -- Lider SIS:         SIS Paola Carvajal.
  -- Lider PDS:         SUD Gina Cabezas.
  -- Fecha de creacion: 01 de Noviembre del 2017
  -- Proyecto:          [11565] Garantia Extendida
  -- Descripcion:       Funcion que obtiene datos parametrizados.
  --=====================================================================================================--
  FUNCTION FNC_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END FNC_OBTENER_VALOR_PARAMETRO;

END REPORTE_FACTURA_FINAN;
/
