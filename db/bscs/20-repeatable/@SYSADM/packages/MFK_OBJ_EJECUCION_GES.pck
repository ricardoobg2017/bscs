CREATE OR REPLACE PACKAGE MFK_OBJ_EJECUCION_GES IS

  PROCEDURE mfp_insertar_ejecucion(pn_idenvio             IN mf_time_ejecucion_ges.idenvio%TYPE,
                                   pd_diaejecucion        IN mf_time_ejecucion_ges.diaejecucion%TYPE,
                                   pn_horamaxejecucion    IN mf_time_ejecucion_ges.horamaxejecucion%TYPE,
                                   pn_minutosmaxejecucion IN mf_time_ejecucion_ges.minutosmaxejecucion%TYPE,
                                   pv_periodoejecucion    IN mf_time_ejecucion_ges.periodoejecucion%TYPE,
                                   pn_numerosmsmaxenvio   IN mf_time_ejecucion_ges.numerosmsmaxenvio%TYPE,
                                   pv_error               OUT VARCHAR2);

  PROCEDURE mfp_insertar_dias(pn_idenvio   IN mf_dias_semana_ges.idenvio%TYPE,
                              pv_dia       IN mf_dias_semana_ges.dia%TYPE,
                              pv_msg_error IN OUT VARCHAR2);

END MFK_OBJ_EJECUCION_GES;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_EJECUCION_GES IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar las ejecuciones en la tabla
  **               MF_TIME_EJECUCION_GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar_ejecucion(pn_idenvio             IN mf_time_ejecucion_ges.idenvio%TYPE,
                                   pd_diaejecucion        IN mf_time_ejecucion_ges.diaejecucion%TYPE,
                                   pn_horamaxejecucion    IN mf_time_ejecucion_ges.horamaxejecucion%TYPE,
                                   pn_minutosmaxejecucion IN mf_time_ejecucion_ges.minutosmaxejecucion%TYPE,
                                   pv_periodoejecucion    IN mf_time_ejecucion_ges.periodoejecucion%TYPE,
                                   pn_numerosmsmaxenvio   IN mf_time_ejecucion_ges.numerosmsmaxenvio%TYPE,
                                   pv_error               OUT VARCHAR2) IS
                                   
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_EJECUCION_GES.MFP_INSERTAR_EJECUCION';
    lv_estado     mf_time_ejecucion_ges.estado%TYPE := 'A';
    le_error      EXCEPTION;
    lv_error      VARCHAR2(1000);
    
  BEGIN
  
    pv_error := NULL;
    INSERT INTO mf_time_ejecucion_ges
      (idenvio,
       diaejecucion,
       horamaxejecucion,
       minutosmaxejecucion,
       periodoejecucion,
       numerosmsmaxenvio,
       estado)
    VALUES
      (pn_idenvio,
       pd_diaejecucion,
       pn_horamaxejecucion,
       pn_minutosmaxejecucion,
       pv_periodoejecucion,
       pn_numerosmsmaxenvio,
       lv_estado);
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error;
    WHEN OTHERS THEN
      pv_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar las ejecuciones en la tabla
  **               MF_DIAS_SEMANA_GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar_dias(pn_idenvio   IN mf_dias_semana_ges.idenvio%TYPE,
                              pv_dia       IN mf_dias_semana_ges.dia%TYPE,
                              pv_msg_error IN OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_EJECUCION_GES.MFP_INSERTAR_DIAS';
    le_error      EXCEPTION;
    lv_error      VARCHAR2(1000);
  
  BEGIN
    
    pv_msg_error := NULL;
    INSERT INTO mf_dias_semana_ges
      (idenvio, dia)
    VALUES
      (pn_idenvio, pv_dia);
  
  EXCEPTION
    WHEN le_error THEN
      pv_msg_error := lv_error;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

END MFK_OBJ_EJECUCION_GES;
/
