create or replace package GCK_OBJ_SUS_REAC_INAC_MASIVA_L is

 --======================================================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Inserta a la tabla de BL_GSI_RELOJ
--======================================================================================--
  PROCEDURE BLP_INSERT_BL_GSI_RELOJ(PV_CUENTA      IN  VARCHAR2,
                                    PN_DESPACHADOR IN  NUMBER,
                                    PV_ERROR       OUT VARCHAR2);

end GCK_OBJ_SUS_REAC_INAC_MASIVA_L;
/
CREATE OR REPLACE PACKAGE BODY GCK_OBJ_SUS_REAC_INAC_MASIVA_L IS
 --======================================================================================--
  -- Creado: CLS Jesus Banchen
  -- Proyecto      : [9824] - Suspension/Reactivacion/Inactivacion de Lineas Masivas
  -- Fecha         : 18/11/2014
  -- Lider Proyecto: SIS Hugo Melendrez
  -- Lider CLS     : CLS Maria de los Angeles Recillo
  -- Motivo        : Inserta a la tabla de BL_GSI_RELOJ
--======================================================================================--

PROCEDURE BLP_INSERT_BL_GSI_RELOJ(PV_CUENTA      IN  VARCHAR2,
                                  PN_DESPACHADOR IN  NUMBER,
                                  PV_ERROR       OUT VARCHAR2) IS

 --Declaracion de variables
  LE_MIERROR           EXCEPTION;
  LV_ERROR             VARCHAR2(500);
  LV_APLICACION        VARCHAR2(60) := 'GCK_OBJ_SUS_REAC_INAC_MASIVA_L.BLP_INSERT_BL_GSI_RELOJ - ';

  BEGIN
    --Validacion de Variables
    IF PV_CUENTA is null THEN
      Lv_Error := 'Falta la cuenta';
      raise LE_MIERROR;
    END IF;

    IF PN_DESPACHADOR is null THEN
      Lv_Error := 'Falta el disparador';
      raise LE_MIERROR;
    END IF;

    DELETE BL_GSI_RELOJ;
    commit;
    
    --Registro en la tabla de BL_GSI_RELOJ
    INSERT INTO BL_GSI_RELOJ
      (CUENTA, DEUDA, SALDO, CREDITOS, MENSAJE, CONTRATO, DESPACHADOR)
    VALUES
      (PV_CUENTA, NULL, NULL, NULL, NULL, NULL, PN_DESPACHADOR);

COMMIT;
  EXCEPTION
      WHEN LE_MIERROR THEN
           LV_APLICACION := LV_APLICACION || Lv_Error;
           Pv_Error := SUBSTR(LV_APLICACION, 1, 500);
      WHEN OTHERS THEN
          Pv_Error := SUBSTR(LV_APLICACION || SQLERRM, 1, 500);
   END BLP_INSERT_BL_GSI_RELOJ;

end GCK_OBJ_SUS_REAC_INAC_MASIVA_L;
/
