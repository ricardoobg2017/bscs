create or replace package PCK_OCC is
/* Source of PACKAGE PCK_OCC is not available */


 procedure ocf_consulta_ciclo (Ln_CicloAdmin IN NUMBER,
                               Lv_CicloAxis  IN OUT VARCHAR2 );

 
 procedure ocf_consulta_fechas (Lv_CicloAxis IN VARCHAR2,
                                Lv_DiaMax    IN OUT VARCHAR2,
                                Lv_DiaMin    IN OUT VARCHAR2);
 
end PCK_OCC; 
/
create or replace package body PCK_OCC is

  -- Private type declarations
 
 procedure ocf_consulta_ciclo (Ln_CicloAdmin IN NUMBER,
                               Lv_CicloAxis  IN OUT VARCHAR2 ) IS

 CURSOR C_CicloAxis (Cn_CicloAdmin NUMBER)  IS
 SELECT ID_CICLO
 FROM --fa_ciclos_axis_bscs
 porta.fa_ciclos_axis_bscs@axis
 WHERE ID_CICLO_ADMIN = Cn_CicloAdmin;
 
/* [4813] JPE Cursor que tomará la información de la tabla local de BSCS
 Cambio se realiza por inconsistencias en informes ACO AAI-VFS-272-09*/
 CURSOR C_CicloAxisLocal (Cn_CicloAdmin NUMBER)  IS
 SELECT ID_CICLO
 FROM fa_ciclos_axis_bscs
 WHERE ID_CICLO_ADMIN = Cn_CicloAdmin;
/* [4813] FIN JPE */

 LC_CicloAxis  C_CicloAxis%ROWTYPE;
 lv_gv_par_flag  varchar2(2);
 
 begin
 /* [4813] JPE Se consutla bandera para cambio de la consulta a tabla local fa_ciclos_axis_bscs*/
    
    select valor  into lv_gv_par_flag
    from gv_parametros where id_tipo_parametro=753
    and id_parametro='GV_FA_CICLOS'; 

    IF lv_gv_par_flag = 'S'THEN     
      OPEN C_CicloAxisLocal(Ln_CicloAdmin);
      FETCH  C_CicloAxisLocal INTO Lv_CicloAxis;
      CLOSE C_CicloAxisLocal;
    ELSE
      OPEN C_CicloAxis(Ln_CicloAdmin);
      FETCH  C_CicloAxis INTO Lv_CicloAxis;
      CLOSE C_CicloAxis;
    END IF;
 /* [4813] FIN JPE*/   
 end  ocf_consulta_ciclo;

 procedure ocf_consulta_fechas (Lv_CicloAxis IN VARCHAR2,
                                Lv_DiaMax    IN OUT VARCHAR2,
                                Lv_DiaMin    IN OUT VARCHAR2) IS

 CURSOR C_Dias (Cn_CicloAxis VARCHAR2)  IS
 SELECT DIA_INI_CICLO, dia_fin_ciclo
 FROM fa_ciclos_bscs  s
 WHERE id_ciclo = Cn_CicloAxis; 

 
 LC_Dias      C_Dias%ROWTYPE;
 
 begin
    OPEN C_Dias(Lv_CicloAxis);
    FETCH  C_Dias INTO Lv_DiaMin,Lv_DiaMax ;
    CLOSE C_Dias;
   
 end  ocf_consulta_fechas; 
 
end PCK_OCC;
/
