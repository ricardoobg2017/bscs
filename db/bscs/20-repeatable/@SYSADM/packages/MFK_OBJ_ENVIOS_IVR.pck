CREATE OR REPLACE PACKAGE MFK_OBJ_ENVIOS_IVR IS

  PROCEDURE mfp_insertar(pv_descripcion          IN mf_envios_ivr.descripcion%TYPE,
                         pv_estado               IN mf_envios_ivr.estado%TYPE,
                         pn_cantidad_mensajes    IN mf_envios_ivr.cantidad_mensajes%TYPE,
                         pn_horas_separacion_msn IN mf_envios_ivr.horas_separacion_msn%TYPE,
                         pv_forma_envio          IN mf_envios_ivr.forma_envio%TYPE,
                         pv_mensaje              IN mf_envios_ivr.mensaje%TYPE,
                         pn_monto_minimo         IN mf_envios_ivr.monto_minimo%TYPE,
                         pn_monto_maximo         IN mf_envios_ivr.monto_maximo%TYPE,
                         pv_region               IN mf_envios_ivr.region%TYPE,
                         pv_requiere_edad_mora   IN mf_envios_ivr.requiere_edad_mora%TYPE,
                         pn_tipo_cliente         IN mf_envios_ivr.tipo_cliente%TYPE,
                         pv_usuario_ingreso      IN mf_envios_ivr.usuario_ingreso%TYPE,
                         pd_fecha_ingreso        IN mf_envios_ivr.fecha_ingreso%TYPE,
                         pv_usuario_modificacion IN mf_envios_ivr.usuario_modificacion%TYPE,
                         pd_fecha_modificacion   IN mf_envios_ivr.fecha_modificacion%TYPE,
                         pn_idenvio              OUT mf_envios_ivr.idenvio%TYPE,
                         pv_msg_error            IN OUT VARCHAR2,
                         pv_ciclo                IN mf_envios_ivr.id_ciclo%TYPE,
                         pv_tipo_cliente         IN VARCHAR2 DEFAULT NULL,
                         pv_categ_cliente        IN VARCHAR2 DEFAULT NULL,
                         pv_score                IN VARCHAR2 DEFAULT 'N',
                         pv_tipo_envio           IN mf_envios_ivr.tipo_envio%TYPE DEFAULT NULL,
                         PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL);

  PROCEDURE mfp_actualizar(pn_idenvio              IN mf_envios_ivr.idenvio%TYPE,
                           pv_descripcion          IN mf_envios_ivr.descripcion%TYPE,
                           pv_estado               IN mf_envios_ivr.estado%TYPE,
                           pn_cantidad_mensajes    IN mf_envios_ivr.cantidad_mensajes%TYPE,
                           pn_horas_separacion_msn IN mf_envios_ivr.horas_separacion_msn%TYPE,
                           pv_forma_envio          IN mf_envios_ivr.forma_envio%TYPE,
                           pv_mensaje              IN mf_envios_ivr.mensaje%TYPE,
                           pn_monto_minimo         IN mf_envios_ivr.monto_minimo%TYPE,
                           pn_monto_maximo         IN mf_envios_ivr.monto_maximo%TYPE,
                           pv_region               IN mf_envios_ivr.region%TYPE,
                           pv_requiere_edad_mora   IN mf_envios_ivr.requiere_edad_mora%TYPE,
                           pn_tipo_cliente         IN mf_envios_ivr.tipo_cliente%TYPE,
                           pv_usuario_ingreso      IN mf_envios_ivr.usuario_ingreso%TYPE,
                           pd_fecha_ingreso        IN mf_envios_ivr.fecha_ingreso%TYPE,
                           pv_usuario_modificacion IN mf_envios_ivr.usuario_modificacion%TYPE,
                           pd_fecha_modificacion   IN mf_envios_ivr.fecha_modificacion%TYPE,                      
                           pv_msg_error            IN OUT VARCHAR2,
                           pv_ciclo                IN mf_envios_ivr.id_ciclo%TYPE,
                           pv_tipo_cliente         IN VARCHAR2 DEFAULT NULL,
                           pv_categ_cliente        IN VARCHAR2 DEFAULT NULL,
                           pv_score                IN VARCHAR2 DEFAULT 'N',
                           pv_tipo_envio           IN mf_envios_ivr.tipo_envio%TYPE DEFAULT NULL,
                           PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL); 

  PROCEDURE mfp_eliminar(pn_idenvio   IN mf_envios_ivr.idenvio%TYPE,
                         pv_msg_error IN OUT VARCHAR2);

  PROCEDURE mfp_eliminar_logico(pn_idenvio   IN mf_envios_ivr.idenvio%TYPE,
                                pv_msg_error IN OUT VARCHAR2);

  PROCEDURE mfp_detalle_inserta(pn_id_envio IN mf_detalles_envios_ivr.idenvio%TYPE,
                                pv_trama    IN VARCHAR2,
                                pv_tipo     IN VARCHAR2,
                                pv_error    OUT VARCHAR2);

  PROCEDURE mfp_insertar_bitacora(pn_cuenta_bscs    IN mf_bitacora_envio_ivr.num_cuenta_bscs%TYPE,
                                  pv_num_celular    IN mf_bitacora_envio_ivr.num_celular%TYPE,
                                  pv_nombre_cliente IN mf_bitacora_envio_ivr.nombre_cliente%TYPE,
                                  pv_cedula         IN mf_bitacora_envio_ivr.cedula%TYPE,
                                  pn_monto_deuda    IN mf_bitacora_envio_ivr.monto_deuda%TYPE,
                                  pd_fecha_envio    IN mf_bitacora_envio_ivr.fecha_envio%TYPE,
                                  pd_fecha_registro IN mf_bitacora_envio_ivr.fecha_registro%TYPE,
                                  pv_mensaje        IN mf_bitacora_envio_ivr.mensaje%TYPE,
                                  pn_id_envio       IN mf_bitacora_envio_ivr.id_envio%TYPE,
                                  pn_secuencia      IN mf_bitacora_envio_ivr.secuencia%TYPE,
                                  pn_calificacion   IN mf_bitacora_envio_ivr.calificacion%TYPE DEFAULT NULL,
                                  pv_categoria      IN mf_bitacora_envio_ivr.categoria_cli%TYPE DEFAULT NULL,
                                  pn_hilo           IN NUMBER,
                                  pv_error          OUT VARCHAR2);

END MFK_OBJ_ENVIOS_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_ENVIOS_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar los envios en la tabla
  **               MF_ENVIOS_IVR
  /*----------------------------------------------------------------------------------------*/
 /*====================================================================================
 LIDER SIS :	     ANTONIO MAYORGA
 Proyecto  :	     [11334] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
 Modificado Por:	 IRO Andres Balladares.
 Fecha     :	     03/04/2017
 LIDER IRO :	     Juan Romero Aguilar 
 PROPOSITO :	     Considerar financiamiento para el modulo SMS, IVR y Gestores
#=====================================================================================*/
  
  PROCEDURE mfp_insertar(pv_descripcion          IN mf_envios_ivr.descripcion%TYPE,
                         pv_estado               IN mf_envios_ivr.estado%TYPE,
                         pn_cantidad_mensajes    IN mf_envios_ivr.cantidad_mensajes%TYPE,
                         pn_horas_separacion_msn IN mf_envios_ivr.horas_separacion_msn%TYPE,
                         pv_forma_envio          IN mf_envios_ivr.forma_envio%TYPE,
                         pv_mensaje              IN mf_envios_ivr.mensaje%TYPE,
                         pn_monto_minimo         IN mf_envios_ivr.monto_minimo%TYPE,
                         pn_monto_maximo         IN mf_envios_ivr.monto_maximo%TYPE,
                         pv_region               IN mf_envios_ivr.region%TYPE,
                         pv_requiere_edad_mora   IN mf_envios_ivr.requiere_edad_mora%TYPE,
                         pn_tipo_cliente         IN mf_envios_ivr.tipo_cliente%TYPE,
                         pv_usuario_ingreso      IN mf_envios_ivr.usuario_ingreso%TYPE,
                         pd_fecha_ingreso        IN mf_envios_ivr.fecha_ingreso%TYPE,
                         pv_usuario_modificacion IN mf_envios_ivr.usuario_modificacion%TYPE,
                         pd_fecha_modificacion   IN mf_envios_ivr.fecha_modificacion%TYPE,
                         pn_idenvio              OUT mf_envios_ivr.idenvio%TYPE,
                         pv_msg_error            IN OUT VARCHAR2,
                         pv_ciclo                IN mf_envios_ivr.id_ciclo%TYPE,
                         pv_tipo_cliente         IN VARCHAR2 DEFAULT NULL,
                         pv_categ_cliente        IN VARCHAR2 DEFAULT NULL,
                         pv_score                IN VARCHAR2 DEFAULT 'N',
                         pv_tipo_envio           IN mf_envios_ivr.tipo_envio%TYPE DEFAULT NULL,
                         PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL) IS
  
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_INSERTAR';
    ln_idenvio    mf_envios_ivr.idenvio%TYPE;
    lv_error      VARCHAR2(1000);
    le_error      EXCEPTION;
    
  BEGIN
  
    pv_msg_error := NULL;
    SELECT mfs_envios_sec_ivr.nextval INTO ln_idenvio FROM dual;
    INSERT INTO mf_envios_ivr
      (idenvio,
       descripcion,
       estado,
       cantidad_mensajes,
       horas_separacion_msn,
       forma_envio,
       mensaje,
       monto_minimo,
       monto_maximo,
       region,
       requiere_edad_mora,
       tipo_cliente,
       usuario_ingreso,
       fecha_ingreso,
       usuario_modificacion,
       fecha_modificacion,
       tipo_envio,
       score,
       id_ciclo,
       FINANCIAMIENTO)
    VALUES
      (ln_idenvio,
       pv_descripcion,
       pv_estado,
       pn_cantidad_mensajes,
       pn_horas_separacion_msn,
       pv_forma_envio,
       pv_mensaje,
       pn_monto_minimo,
       pn_monto_maximo,
       pv_region,
       pv_requiere_edad_mora,
       pn_tipo_cliente,
       pv_usuario_ingreso,
       pd_fecha_ingreso,
       pv_usuario_modificacion,
       pd_fecha_modificacion,
       pv_tipo_envio,
       pv_score,
       pv_ciclo,
       PV_FINANCIAMIENTO);
    pn_idenvio := ln_idenvio;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msg_error := lv_error;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite actualizar los envios en la tabla
  **               MF_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_actualizar(pn_idenvio              IN mf_envios_ivr.idenvio%TYPE,
                           pv_descripcion          IN mf_envios_ivr.descripcion%TYPE,
                           pv_estado               IN mf_envios_ivr.estado%TYPE,
                           pn_cantidad_mensajes    IN mf_envios_ivr.cantidad_mensajes%TYPE,
                           pn_horas_separacion_msn IN mf_envios_ivr.horas_separacion_msn%TYPE,
                           pv_forma_envio          IN mf_envios_ivr.forma_envio%TYPE,
                           pv_mensaje              IN mf_envios_ivr.mensaje%TYPE,
                           pn_monto_minimo         IN mf_envios_ivr.monto_minimo%TYPE,
                           pn_monto_maximo         IN mf_envios_ivr.monto_maximo%TYPE,
                           pv_region               IN mf_envios_ivr.region%TYPE,
                           pv_requiere_edad_mora   IN mf_envios_ivr.requiere_edad_mora%TYPE,
                           pn_tipo_cliente         IN mf_envios_ivr.tipo_cliente%TYPE,
                           pv_usuario_ingreso      IN mf_envios_ivr.usuario_ingreso%TYPE,
                           pd_fecha_ingreso        IN mf_envios_ivr.fecha_ingreso%TYPE,
                           pv_usuario_modificacion IN mf_envios_ivr.usuario_modificacion%TYPE,
                           pd_fecha_modificacion   IN mf_envios_ivr.fecha_modificacion%TYPE,
                           pv_msg_error            IN OUT VARCHAR2,
                           pv_ciclo                IN mf_envios_ivr.id_ciclo%TYPE,
                           pv_tipo_cliente         IN VARCHAR2 DEFAULT NULL,
                           pv_categ_cliente        IN VARCHAR2 DEFAULT NULL,
                           pv_score                IN VARCHAR2 DEFAULT 'N',
                           pv_tipo_envio           IN mf_envios_ivr.tipo_envio%TYPE DEFAULT NULL,
                           PV_FINANCIAMIENTO       IN VARCHAR2 DEFAULT NULL) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER) IS
      SELECT * FROM mf_envios_ivr e WHERE e.idenvio = cn_idenvio;
  
    lc_currentrow mf_envios_ivr%ROWTYPE;
    le_nodataupdated EXCEPTION;
    le_error         EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_ACTUALIZAR';
    lv_error         VARCHAR2(1000);
    
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_table_cur(pn_idenvio);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodataupdated;
    END IF;
  
    UPDATE mf_envios_ivr
       SET descripcion          = nvl(pv_descripcion, descripcion),
           estado               = nvl(pv_estado, estado),
           cantidad_mensajes    = nvl(pn_cantidad_mensajes, cantidad_mensajes),
           horas_separacion_msn = nvl(pn_horas_separacion_msn, horas_separacion_msn),
           forma_envio          = nvl(pv_forma_envio, forma_envio),
           mensaje              = nvl(pv_mensaje, mensaje),
           monto_minimo         = nvl(pn_monto_minimo, monto_minimo),
           monto_maximo         = nvl(pn_monto_maximo, monto_maximo),
           region               = nvl(pv_region, region),
           requiere_edad_mora   = nvl(pv_requiere_edad_mora, requiere_edad_mora),
           tipo_cliente         = nvl(pn_tipo_cliente, tipo_cliente),
           usuario_ingreso      = nvl(pv_usuario_ingreso, usuario_ingreso),
           fecha_ingreso        = nvl(pd_fecha_ingreso, fecha_ingreso),
           usuario_modificacion = nvl(pv_usuario_modificacion, usuario_modificacion),
           fecha_modificacion   = nvl(pd_fecha_modificacion, fecha_modificacion),
           tipo_envio           = nvl(pv_tipo_envio, tipo_envio),
           id_ciclo             = nvl(pv_ciclo, id_ciclo),
           score                = nvl(pv_score, score),
           FINANCIAMIENTO       = NVL(PV_FINANCIAMIENTO,FINANCIAMIENTO)
     WHERE idenvio = pn_idenvio;
    CLOSE c_table_cur;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msg_error := lv_error;
    WHEN le_nodataupdated THEN
      pv_msg_error := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite eliminar los envios en la tabla
  **               MF_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_eliminar(pn_idenvio   IN mf_envios_ivr.idenvio%TYPE,
                         pv_msg_error IN OUT VARCHAR2) AS
                         
    CURSOR c_table_cur(cn_idenvio NUMBER) IS
      SELECT * FROM mf_envios_ivr e WHERE e.idenvio = cn_idenvio;
  
    lc_currentrow    mf_envios_ivr%ROWTYPE;
    le_nodatadeleted EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_ELIMINAR';
    
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_table_cur(pn_idenvio);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodatadeleted;
    END IF;
  
    DELETE FROM mf_envios_ivr e WHERE e.idenvio = pn_idenvio;
    DELETE FROM mf_detalles_envios_ivr d WHERE d.idenvio = pn_idenvio;
    CLOSE c_table_cur;
  
  EXCEPTION
    WHEN le_nodatadeleted THEN
      pv_msg_error := 'El registro que desea borrar no existe. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite eliminar los envios en la tabla
  **               MF_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_eliminar_logico(pn_idenvio   IN mf_envios_ivr.idenvio%TYPE,
                                pv_msg_error IN OUT VARCHAR2) AS
  
    CURSOR c_table_cur(cn_idenvio NUMBER) IS
      SELECT * FROM mf_envios_ivr e WHERE e.idenvio = cn_idenvio;
  
    lc_currentrow    mf_envios_ivr%ROWTYPE;
    le_nodatadeleted EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_ELIMINAR_LOGICO';
    
  BEGIN
  
    pv_msg_error := NULL;
    OPEN c_table_cur(pn_idenvio);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodatadeleted;
    END IF;
  
    UPDATE mf_envios_ivr e SET e.estado = 'I' WHERE e.idenvio = pn_idenvio;
    CLOSE c_table_cur;
  
  EXCEPTION 
    WHEN le_nodatadeleted THEN
      pv_msg_error := 'El registro que desea borrar no existe. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msg_error := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar el detalle de envios en la tabla
  **               MF_DETALLES_ENVIOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_detalle_inserta(pn_id_envio IN mf_detalles_envios_ivr.idenvio%TYPE,
                                pv_trama    IN VARCHAR2,
                                pv_tipo     IN VARCHAR2,
                                pv_error    OUT VARCHAR2) AS
                                
    lv_aplicacion   VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_DETALLE_INSERTA';
    lv_error        VARCHAR2(1000);
    le_error        EXCEPTION;
    lv_data         VARCHAR2(100) := pv_trama;
    lv_data_tmp     VARCHAR2(100) := lv_data;
    lv_data_temp    VARCHAR2(100) := lv_data;
    ln_posicion_act NUMBER(10) := 1;
    lv_id           VARCHAR2(100);
    ln_avanza       NUMBER(10) := length(lv_data_tmp);
    
  BEGIN
    
    DELETE FROM mf_detalles_envios_ivr
     WHERE idenvio = pn_id_envio
       AND tipoenvio = pv_tipo;
       
    WHILE (ln_avanza > 1) LOOP
      ln_posicion_act := instr(lv_data_tmp, '|', 1);
      lv_data_temp    := substr(lv_data_tmp,
                                ln_posicion_act + 1,
                                length(lv_data_tmp));
      lv_id           := substr(lv_data_tmp, 1, ln_posicion_act - 1);
    
      IF lv_id IS NOT NULL THEN
        BEGIN
          INSERT INTO mf_detalles_envios_ivr
            (idenvio, idcliente, tipoenvio)
          VALUES
            (pn_id_envio, lv_id, pv_tipo);
        EXCEPTION
          WHEN OTHERS THEN
            lv_error := 'ERROR EN LA APLICACION ' || lv_aplicacion ||
                        '|EN LA INSERCION DEL DETALLE INTERNO ' ||
                        substr(SQLERRM, 1, 250);
            RAISE le_error;
        END;
      END IF;
      lv_data_tmp := lv_data_temp;
      ln_avanza   := length(lv_data_tmp);
    END LOOP;
    
    IF length(lv_data_tmp) > 0 THEN
      INSERT INTO mf_detalles_envios_ivr
        (idenvio, idcliente, tipoenvio)
      VALUES
        (pn_id_envio, lv_data_tmp, pv_tipo);
    END IF;
    
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error;
    WHEN OTHERS THEN
      pv_error := 'ERROR EN LA APLICACION ' || lv_aplicacion || '|' || substr(SQLERRM, 1, 250);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar la bitacora de envios en la tabla
  **               MF_BITACORA_ENVIO_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar_bitacora(pn_cuenta_bscs    IN mf_bitacora_envio_ivr.num_cuenta_bscs%TYPE,
                                  pv_num_celular    IN mf_bitacora_envio_ivr.num_celular%TYPE,
                                  pv_nombre_cliente IN mf_bitacora_envio_ivr.nombre_cliente%TYPE,
                                  pv_cedula         IN mf_bitacora_envio_ivr.cedula%TYPE,
                                  pn_monto_deuda    IN mf_bitacora_envio_ivr.monto_deuda%TYPE,
                                  pd_fecha_envio    IN mf_bitacora_envio_ivr.fecha_envio%TYPE,
                                  pd_fecha_registro IN mf_bitacora_envio_ivr.fecha_registro%TYPE,
                                  pv_mensaje        IN mf_bitacora_envio_ivr.mensaje%TYPE,
                                  pn_id_envio       IN mf_bitacora_envio_ivr.id_envio%TYPE,
                                  pn_secuencia      IN mf_bitacora_envio_ivr.secuencia%TYPE,
                                  pn_calificacion   IN mf_bitacora_envio_ivr.calificacion%TYPE DEFAULT NULL,
                                  pv_categoria      IN mf_bitacora_envio_ivr.categoria_cli%TYPE DEFAULT NULL,
                                  pn_hilo           IN NUMBER,
                                  pv_error          OUT VARCHAR2) AS
  
    lv_aplicacion VARCHAR2(100) := 'MFK_OBJ_ENVIOS_IVR.MFP_INSERTAR_BITACORA';
    
  BEGIN
    
    BEGIN
      EXECUTE IMMEDIATE 'INSERT INTO MF_BITACORA_ENVIO_IVR ' ||
                        ' (NUM_CUENTA_BSCS, ' || ' NUM_CELULAR, ' ||
                        ' NOMBRE_CLIENTE, ' || ' CEDULA, ' ||
                        ' MONTO_DEUDA, ' || ' FECHA_ENVIO, ' ||
                        ' FECHA_REGISTRO, ' || ' ID_ENVIO, ' ||
                        ' SECUENCIA, ' || ' MENSAJE, ' || ' HILO, ' ||
                        ' CALIFICACION, ' || ' ESTADO_ARCHIVO, ' ||
                        ' CATEGORIA_CLI) ' || ' VALUES ' || '( :1, ' ||
                        ' :2, ' || ' :3, ' || ' :4, ' || ' :5, ' || ' :6, ' ||
                        ' :7, ' || ' :8, ' || ' :9, ' || ' :10, ' ||
                        ' :11, ' || ' :12, ' || ' :13, ' || ' :14) '
        USING pn_cuenta_bscs, pv_num_celular, pv_nombre_cliente, pv_cedula, pn_monto_deuda, pd_fecha_envio, pd_fecha_registro, pn_id_envio, pn_secuencia, pv_mensaje, pn_hilo, pn_calificacion, 'I', pv_categoria;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR EN EL APLICATIVO:' || lv_aplicacion || ' ERROR:' || substr(SQLERRM, 1, 250);
  END;

END MFK_OBJ_ENVIOS_IVR;
/
