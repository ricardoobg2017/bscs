CREATE OR REPLACE PACKAGE MIG_PARAMETERS AS

PROCEDURE MAIN;

END MIG_PARAMETERS;
/
CREATE OR REPLACE PACKAGE BODY MIG_PARAMETERS AS



PROCEDURE MAIN IS


-- Global Variables

v_par_id             NUMBER ;
v_parid               NUMBER ;
v_errcode           VARCHAR2(100) ;
v_errmsg           VARCHAR2(100) ;
v_seq               NUMBER := 0 ;
v_p_val_string       PARAMETER_VALUE.prm_value_string%TYPE ;
v_p_val_date       PARAMETER_VALUE.prm_value_date%TYPE ;
v_p_val_num        PARAMETER_VALUE.prm_value_number%TYPE ;
v_des               VARCHAR2(30) ;
v_cont             NUMBER := 0 ;


CURSOR c1 IS



select u.customer_id, c.co_id, c.tmcode, s.entry_date,
       u.target_reached, c.co_crd_days, c.co_crd_clicks, s.sncode
from contract_all c, customer_all u, profile_service s, mig_postmigration@cbill m
where u.customer_id = c.customer_id
  and m.account = u.target_reached
  and m.activnum = c.co_crd_days
  and s.co_id = c.co_id
  and s.sncode in (1,6,49,55,57,61,62,63,64,78,79,80,81,85,86,87,89,96,97) ;


vcom               c1%ROWTYPE;



BEGIN

   select first_prm_value_id-1  into  v_seq  from mig_postmigration_seq ;

   OPEN c1;

   LOOP
      FETCH c1 INTO vcom;
      EXIT WHEN c1%NOTFOUND;

      v_p_val_string := NULL ;
      v_p_val_date := NULL ;
      v_p_val_num := NULL ;
      v_des := NULL ;
      IF vcom.sncode in (49, 57, 89) THEN
        v_par_id := 40;
        select fu_pack_id, fup_des into v_p_val_num, v_des
          from mig_lkfup@cbill
         where tmcode = vcom.tmcode
           and fup_type != 'I';
     ELSIF vcom.sncode in (1, 6) THEN
        v_par_id := 47 ;
        v_p_val_num := 82 ;
        v_des := 'XFree Units Package DUMMY' ;
     ELSIF vcom.sncode = 55 THEN
        v_par_id := 44;
        select decode(tmclass, 'T', 1, 4)
          into v_p_val_num
          from mig_lkplan@cbill
         where tmcode = vcom.tmcode ;
        IF v_p_val_num = 1 THEN
          v_des := 'Tarifario       Normal';
        ELSE
          v_des := 'Autocontrol  Normal';
        END IF;
      ELSIF vcom.sncode in (61, 62, 63, 64, 78, 79, 80, 81, 85, 86, 87) THEN
        IF vcom.sncode in (78, 79, 80, 81) THEN
          v_par_id := 42;
        ELSE
          v_par_id := 7;
        END IF;
        BEGIN
          select '5939'|| featurenxx || rtrim(LTRIM(to_char(featurestation,'000009')))
            into v_p_val_string
            from avfeat@cbill f
           where f.avf_avaccnum = vcom.target_reached
             and f.avf_avnum = vcom.co_crd_days
             and f.feature = 2 ;
        EXCEPTION
          WHEN  NO_DATA_FOUND  THEN
            v_p_val_string := NULL;
        END ;
        IF v_p_val_string = '59390000000' THEN
          v_p_val_string := NULL;
        END IF;
        v_des := NULL ;
      ELSIF  vcom.sncode in (96,97)  THEN
        IF  vcom.sncode = 96  THEN
          v_par_id := 45;
        ELSE
          v_par_id := 43;
        END IF ;
        select status_bloq, des
          into v_p_val_num, v_des
          from mig_lkstatus@cbill
         where  status_cbill = vcom.co_crd_clicks ;
      END IF;
      v_seq := v_seq + 1 ;

      insert into parameter_value_base (prm_value_id,entry_date )
      values (v_seq,vcom.entry_date );

      update profile_service
         set prm_value_id = v_seq
       where co_id = vcom.co_id
         and sncode = vcom.sncode
         and profile_id = 0 ;

      insert into parameter_value (prm_value_id, prm_no, prm_seqno,
                                   parent_seqno,
                                   sibling_seqno, complex_seqno, value_seqno,
                                   complex_level, parameter_id, prm_value_date,
                                   prm_value_string, prm_value_number,
                                   prm_description,
                                   prm_valid_from, rec_version )
      values
             (v_seq,1,1,1,1,1,1,1,v_par_id,NULL,v_p_val_string,v_p_val_num,
              v_des,vcom.entry_date,1 ) ;

      IF  v_cont = 5000  THEN
	    v_cont := 0 ;
        commit;
      END IF ;
	  v_cont := v_cont + 1 ;

   END LOOP;

END;


END MIG_PARAMETERS;
/

