                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   CREATE OR REPLACE PACKAGE BLK_CARGA_OCC_NC IS

  /*--=====================================================================================--
   Creado por     : CIMA. 
   L�der proyecto : CIMA. Hugo fuentes
   CRM            : SIS Juan Carlos Romero
   Fecha          : 29/01/2014
   Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS 
   Motivo         : Paquete para despachar las cargas de creditos que se le van a realizar
                    notas de credito replica de BLK_CARGA_OCC.   
  --=====================================================================================--*/

  TYPE TVARCHAR IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR200 IS TABLE OF VARCHAR(200) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

  FUNCTION BLF_OBTIENE_ESTADO(PN_ID_EJECUCION NUMBER) RETURN VARCHAR2;

  FUNCTION BLF_VERIFICA_FINALIZACION(PN_ID_EJECUCION NUMBER) RETURN BOOLEAN;

  FUNCTION BLF_OBTIENE_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER) RETURN NUMBER;

  FUNCTION BLF_OBTIENE_REG_PROCESADOS(PN_ID_EJECUCION NUMBER) RETURN NUMBER;

  PROCEDURE BLP_SETEA_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                   PN_VALOR        NUMBER);

  PROCEDURE BLP_BALANCEA_CARGA(PN_HILOS INTEGER);

  PROCEDURE BLP_CARGA_SNDATA;

  PROCEDURE BLP_CARGA_OCC_CO_ID(PN_ID_EJECUCION NUMBER,
                                PN_HILO         NUMBER,
                                PV_PRODUCTO     VARCHAR2,
                                PV_COMPANIA     VARCHAR2,
                                PV_GENERA       VARCHAR2);

  PROCEDURE BLP_RESPALDA_TABLA(PV_TABLA_RESPALDAR VARCHAR2);

  PROCEDURE BLP_ACTUALIZA(PN_ID_EJECUCION   NUMBER,
                          PD_FECHA_INICIO   DATE DEFAULT NULL,
                          PD_FECHA_FIN      DATE DEFAULT NULL,
                          Pv_Estado         VARCHAR2 DEFAULT NULL,
                          PN_REG_PROCESADOS NUMBER DEFAULT NULL,
                          PV_OBSERVACION    VARCHAR2 DEFAULT NULL);

  PROCEDURE BLP_ENVIA_SMS(PV_DESTINATARIO VARCHAR2,
                          PV_MENSAJE      VARCHAR2);

  PROCEDURE BLP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                PV_MENSAJE         VARCHAR2);

  PROCEDURE BLP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                   PV_MENSAJE         VARCHAR2);

  PROCEDURE BLP_EJECUTA(PV_USUARIO          VARCHAR2,
                        PN_ID_NOTIFICACION  NUMBER,
                        PN_TIEMPO_NOTIF     NUMBER,
                        PN_CANTIDAD_HILOS   NUMBER,
                        PN_CANTIDAD_REG_MEM NUMBER,
                        PV_RECURRENCIA      VARCHAR,
                        PV_REMARK           VARCHAR,
                        PD_ENTDATE          DATE,
                        PV_RESPALDAR        VARCHAR,
                        PV_TABLA_RESPALDO   VARCHAR,
                        PN_ID_EJECUCION     OUT NUMBER,
                        PV_PRODUCTO         VARCHAR2,
                        PV_COMPANIA         VARCHAR2,
                        PV_GENERA           VARCHAR2,
                        Pv_Error            OUT VARCHAR2);

END BLK_CARGA_OCC_NC;
/
CREATE OR REPLACE PACKAGE BODY BLK_CARGA_OCC_NC IS

 /*--=====================================================================================--
    Creado por     : CIMA. 
    L�der proyecto : CIMA. Hugo fuentes
    CRM            : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS 
    Motivo         : Paquete para despachar las cargas de creditos que se le van a realizar
                     notas de credito replica de BLK_CARGA_OCC.   
   --=====================================================================================--*/
  Tn_Sncode          TNUMBER;
  Tv_Accglcode       TVARCHAR;
  Tv_Accserv_Catcode TVARCHAR;
  Tv_Accserv_Code    TVARCHAR;
  Tv_Accserv_Type    TVARCHAR;
  Tn_Vscode          TNUMBER;
  Tn_Spcode          TNUMBER;
  Tn_Evcode          TNUMBER;



 /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Funcion que obtiene estado de ejecucion.  
   --=====================================================================================--*/

 
  FUNCTION BLF_OBTIENE_ESTADO(PN_ID_EJECUCION NUMBER) RETURN VARCHAR2 IS
    Lv_Estado BL_CARGA_EJECUCION.estado%TYPE;
  
  BEGIN
  
    SELECT estado
      INTO Lv_Estado
      FROM BL_CARGA_EJECUCION x
     WHERE x.id_ejecucion = pn_id_ejecucion;
    RETURN Lv_Estado;
  
  EXCEPTION
    WHEN OTHERS THEN
    
      RETURN 'X';
    
  END;


   /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Funcion  para verificar el estado de la ejecucion.  
   --=====================================================================================--*/
  
  FUNCTION BLF_VERIFICA_FINALIZACION(PN_ID_EJECUCION NUMBER) RETURN BOOLEAN IS
    ln_verifica NUMBER;
    ln_hilos    NUMBER;
  
  BEGIN
  
    SELECT NVL(COUNT(*), 0)
      INTO ln_verifica
      FROM BL_CARGA_EJEC_HILO_STAT x
     WHERE x.id_ejecucion = PN_ID_EJECUCION
       AND estado = 'F';
  
    SELECT cant_hilos
      INTO ln_hilos
      FROM BL_CARGA_EJECUCION
     WHERE id_ejecucion = PN_ID_EJECUCION;
  
    IF ln_verifica = ln_hilos THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
    
      RETURN FALSE;
    
  END;
  
  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Funcion para obrener ultima notificacion.  
   --=====================================================================================--*/
  
  FUNCTION BLF_OBTIENE_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER) RETURN NUMBER IS
  
    lv_tiempo VARCHAR2(10);
  
    CURSOR c_obtiene IS
      SELECT SUBSTR(observacion, instr(observacion, ':') + 2)
        FROM BL_CARGA_EJECUCION
       WHERE id_ejecucion = PN_ID_EJECUCION;
  
  BEGIN
  
    OPEN c_obtiene;
    FETCH c_obtiene
      INTO lv_tiempo;
    IF c_obtiene%NOTFOUND THEN
      RETURN - 1;
    END IF;
    CLOSE c_obtiene;
  
    RETURN TO_NUMBER(lv_tiempo);
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
    
  END;

  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Funcion para obtener cantidad de registros procesados en la ejecucion.  
   --=====================================================================================--*/
  
  FUNCTION BLF_OBTIENE_REG_PROCESADOS(PN_ID_EJECUCION NUMBER) RETURN NUMBER IS
  
    ln_reg NUMBER;
  
    CURSOR c_obtiene IS
      SELECT reg_procesados
        FROM BL_CARGA_EJECUCION
       WHERE id_ejecucion = PN_ID_EJECUCION;
  
  BEGIN
  
    OPEN c_obtiene;
    FETCH c_obtiene
      INTO ln_reg;
    IF c_obtiene%NOTFOUND THEN
      RETURN - 1;
    END IF;
    CLOSE c_obtiene;
  
    RETURN ln_reg;
  
  END;

  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para actualizar la observacion de la ejecucion.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_SETEA_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                   PN_VALOR        NUMBER) IS
  
  BEGIN
  
    UPDATE BL_CARGA_EJECUCION
       SET observacion = SUBSTR(observacion, 1, instr(observacion, ':') + 1) || TO_CHAR(pn_valor)
     WHERE id_ejecucion = PN_ID_EJECUCION;
  
    COMMIT;
  
  END;


  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para balancear la ejecucion.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_BALANCEA_CARGA(PN_HILOS INTEGER) IS
  
    ln_hilo INTEGER := 0;
  
  BEGIN
  
    UPDATE BL_CARGA_OCC_TMP_NC g
       SET customer_id = (SELECT customer_id
                            FROM contract_all
                           WHERE co_id = g.co_id)
     WHERE co_id IS NOT NULL;
  
    UPDATE BL_CARGA_OCC_TMP_NC j
       SET customer_id = (SELECT customer_id
                            FROM customer
                           WHERE custcode = j.custcode)
     WHERE custcode IS NOT NULL;
  
   UPDATE BL_CARGA_OCC_TMP_NC
       SET status = 'E',
           error  = 'Problemas al obtener el customer_id de la tabla contract_all'
     WHERE customer_id IS NULL;
  
    UPDATE BL_CARGA_OCC_TMP_NC
       SET status = 'E',
           error  = 'SNCode no existente'
     WHERE sncode IS NULL
        OR sncode NOT IN (SELECT sncode
                            FROM mpusntab);
  
    FOR j IN (SELECT customer_id,
                     COUNT(*)
                FROM BL_CARGA_OCC_TMP_NC
               WHERE customer_id IS NOT NULL
               GROUP BY customer_id
               ORDER BY 2 DESC) LOOP
      UPDATE BL_CARGA_OCC_TMP_NC
         SET hilo = ln_hilo
       WHERE customer_id = j.customer_id;
      ln_hilo := ln_hilo + 1;
      IF ln_hilo = pn_hilos THEN
        ln_hilo := 0;
      END IF;
    
    END LOOP;
  
    COMMIT;
  
  END;

   /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para obtener informacion adicional a registrar.  
   --=====================================================================================--*/

  PROCEDURE BLP_CARGA_SNDATA IS
  
    CURSOR x IS
      SELECT x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        FROM mpulktmb x,
             mpulkexn y;
  
    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;
  
  BEGIN
  
    OPEN lcv_cur FOR
      SELECT x.sncode,
             x.ACCGLCODE,
             x.ACCSERV_CATCODE,
             x.ACCSERV_CODE,
             x.ACCSERV_TYPE,
             x.VSCODE,
             x.SPCODE,
             y.EVCODE
        FROM mpulktmb x,
             mpulkexn y
       WHERE tmcode = 35
         AND x.sncode IN (SELECT DISTINCT sncode
                            FROM BL_CARGA_OCC_TMP_NC)
         AND vscode = (SELECT MAX(a.vscode)
                         FROM mpulktmb a
                        WHERE tmcode = 35
                          AND sncode = 399)
         AND x.sncode = y.sncode;
  
    FETCH LCV_CUR BULK COLLECT
      INTO Tn_Sncode, Tv_Accglcode, Tv_Accserv_Catcode, Tv_Accserv_Code, Tv_Accserv_Type, Tn_Vscode, Tn_Spcode, Tn_Evcode;
  
    CLOSE LCV_CUR;
  
    FOR I IN Tn_Sncode.FIRST .. Tn_Sncode.LAST LOOP
      Tv_Accglcode(Tn_Sncode(i)) := Tv_Accglcode(i);
      Tv_Accserv_Catcode(Tn_Sncode(i)) := Tv_Accserv_Catcode(i);
      Tv_Accserv_Code(Tn_Sncode(i)) := Tv_Accserv_Code(i);
      Tv_Accserv_Type(Tn_Sncode(i)) := Tv_Accserv_Type(i);
      Tn_Vscode(Tn_Sncode(i)) := Tn_Vscode(i);
      Tn_Spcode(Tn_Sncode(i)) := Tn_Spcode(i);
      Tn_Evcode(Tn_Sncode(i)) := Tn_Evcode(i);
    
    END LOOP;
  
  END;

   /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento que registra los creditos en la tabla FEES.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_CARGA_OCC_CO_ID(PN_ID_EJECUCION NUMBER,
                                PN_HILO         NUMBER,
                                PV_PRODUCTO     VARCHAR2,
                                PV_COMPANIA     VARCHAR2,
                                PV_GENERA       VARCHAR2) IS
  
    CURSOR C_CONFIG IS
      SELECT *
        FROM BL_CARGA_EJECUCION
       WHERE ID_EJECUCION = PN_ID_EJECUCION;
  
    CURSOR C_CARGA IS
      SELECT h.rowid,
             h.amount,
             h.sncode,
             h.co_id,
             h.customer_id
        FROM BL_CARGA_OCC_TMP_NC H
       WHERE STATUS IS NULL
         AND HILO = PN_HILO
         AND CUSTOMER_ID IS NOT NULL;
  
    CURSOR C_SEQ(CN_CUSTOMER_ID NUMBER) IS
      SELECT NVL(MAX(seqno), 0) SEQNO
        FROM FEES
       WHERE CUSTOMER_ID = CN_CUSTOMER_ID;
  
    CURSOR C_HILO_SEQ IS
      SELECT NVL(MAX(SEQ), 0) SEQ
        FROM BL_CARGA_EJEC_HILO_DETAIL BC
       WHERE BC.ID_EJECUCION = PN_ID_EJECUCION
         AND BC.HILO = PN_HILO;
  
    CURSOR C_CUST_ID_STATUS(CN_CUSTOMER_ID NUMBER) IS
      SELECT CSTYPE,
             CSDEACTIVATED
        FROM CUSTOMER
       WHERE CUSTOMER_ID = CN_CUSTOMER_ID;
  
    lc_cust_id_status c_cust_id_status%ROWTYPE;
  
    CURSOR C_CO_ID_STATUS(CN_CO_ID NUMBER) IS
      SELECT ENTDATE
        FROM CONTRACT_HISTORY
       WHERE CH_STATUS = 'D'
         AND CO_ID = CN_CO_ID;
  
    lc_co_id_status       c_co_id_status%ROWTYPE;
    lc_config             c_config%ROWTYPE;
    lc_seq                c_seq%ROWTYPE;
    Tn_Sncode2            tnumber;
    Tv_Accglcode2         tvarchar;
    Tv_Accserv_Catcode2   tvarchar;
    Tv_Accserv_Code2      tvarchar;
    Tv_Accserv_Type2      tvarchar;
    Tn_Vscode2            tnumber;
    Tn_Spcode2            tnumber;
    Tn_Evcode2            tnumber;
    tn_ln_seq             tnumber;
    td_entdate            tdate;
    td_valid_from         tdate;
    td_entdate_by_cust    tdate;
    td_valid_from_by_cust tdate;
    tv_rowid              TVARCHAR;
    tn_amount             TNUMBER;
    tn_sncode             TNUMBER;
    tn_coid               TNUMBER;
    tn_customer_id        TNUMBER;
    tn_seq_by_custid      TNUMBER;
    tv_error              tvarchar;
    Lv_Error              VARCHAR2(200);
    ln_minutos            NUMBER;
    ln_registros_total    NUMBER;
    ln_counter            NUMBER := 0;
    ln_period             NUMBER;
    ln_hilo_seq           NUMBER;
    lb_nohaydatos         BOOLEAN;
    le_error EXCEPTION;
  
  BEGIN
  
    OPEN C_CONFIG;
    FETCH C_CONFIG
      INTO lc_config;
    CLOSE C_CONFIG;
  
    OPEN C_HILO_SEQ;
    FETCH C_HILO_SEQ
      INTO ln_hilo_seq;
    CLOSE C_HILO_SEQ;
  
    ln_hilo_seq := ln_hilo_seq + 1;
  
    INSERT INTO bl_carga_ejec_hilo_detail
    VALUES
      (PN_ID_EJECUCION,
       PN_HILO,
       'P',
       0,
       ln_hilo_seq,
       SYSDATE,
       NULL,
       NULL);
  
    UPDATE bl_carga_ejec_hilo_stat
       SET estado = 'P'
     WHERE id_ejecucion = PN_ID_EJECUCION
       AND hilo = pn_hilo;
  
    COMMIT;
  
    BLP_CARGA_SNDATA;
  
    OPEN c_carga;
  
    LOOP
    
      tv_rowid.DELETE;
      tv_error.DELETE;
      tn_amount.DELETE;
      Tn_Sncode.DELETE;
      tn_coid.DELETE;
      Tn_Sncode2.DELETE;
      Tv_Accglcode2.DELETE;
      Tv_Accserv_Catcode2.DELETE;
      Tv_Accserv_Code2.DELETE;
      Tv_Accserv_Type2.DELETE;
      Tn_Vscode2.DELETE;
      Tn_Spcode2.DELETE;
      Tn_Evcode2.DELETE;
      tn_customer_id.DELETE;
      tn_ln_seq.DELETE;
      ln_counter := 0;
    
      lb_nohaydatos := TRUE;
    
      FETCH C_CARGA BULK COLLECT
        INTO TV_ROWID, TN_AMOUNT, Tn_Sncode, TN_COID, tn_customer_id LIMIT lc_config.cant_reg_mem;
    
      IF TV_ROWID.COUNT > 0 THEN
      
        lb_nohaydatos := FALSE;
      
        FOR I IN TV_ROWID.FIRST .. TV_ROWID.LAST
        
         LOOP
        
          Tv_Accglcode2(i) := Tv_Accglcode(Tn_Sncode(i));
          Tv_Accserv_Catcode2(i) := Tv_Accserv_Catcode(Tn_Sncode(i));
          Tv_Accserv_Code2(i) := Tv_Accserv_Code(Tn_Sncode(i));
          Tv_Accserv_Type2(i) := Tv_Accserv_Type(Tn_Sncode(i));
          Tn_Vscode2(i) := Tn_Vscode(Tn_Sncode(i));
          Tn_Spcode2(i) := Tn_Spcode(Tn_Sncode(i));
          Tn_Evcode2(i) := Tn_Evcode(Tn_Sncode(i));
          tv_error(i) := NULL;
        
          IF NOT TN_SEQ_BY_CUSTID.EXISTS(tn_customer_id(i)) THEN
          
            --Obtener el secuencial
            OPEN C_SEQ(tn_customer_id(i));
            FETCH C_SEQ
              INTO lc_seq;
            IF C_SEQ%NOTFOUND THEN
              tn_ln_seq(i) := 1;
            ELSE
              tn_ln_seq(i) := lc_seq.seqno + 1;
            END IF;
            CLOSE C_SEQ;
          
            TN_SEQ_BY_CUSTID(tn_customer_id(i)) := tn_ln_seq(i);
          
            --Determino si la cuenta est� desactivada
            OPEN C_CUST_ID_STATUS(tn_customer_id(i));
            FETCH C_CUST_ID_STATUS
              INTO lc_cust_id_status;
          
            IF C_CUST_ID_STATUS%NOTFOUND THEN
              tv_error(i) := 'Problemas al obtener el estado del customer_id';
            
            END IF;
          
            IF lc_cust_id_status.cstype = 'd' THEN
              --S� est� desactivada, entonces lo ingreso un d�a antes de fecha desact
              td_entdate_by_cust(tn_customer_id(i)) := TRUNC(lc_cust_id_status.csdeactivated) - 1;
            ELSE
              --Si no, tomo la fecha del param de entrada
              td_entdate_by_cust(tn_customer_id(i)) := TRUNC(lc_config.entdate);
            END IF;
          
            td_valid_from_by_cust(tn_customer_id(i)) := TRUNC(lc_config.entdate);
          
            CLOSE c_cust_id_status;
          
            td_entdate(i) := td_entdate_by_cust(tn_customer_id(i));
            td_valid_from(i) := td_valid_from_by_cust(tn_customer_id(i));
          
          ELSE
          
            TN_SEQ_BY_CUSTID(tn_customer_id(i)) := TN_SEQ_BY_CUSTID(tn_customer_id(i)) + 1;
          
            tn_ln_seq(i) := TN_SEQ_BY_CUSTID(tn_customer_id(i));
          
            td_entdate(i) := td_entdate_by_cust(tn_customer_id(i));
            td_valid_from(i) := td_valid_from_by_cust(tn_customer_id(i));
          
          END IF;
        
          IF TN_COID(i) IS NOT NULL THEN
            --Cargos a nivel de CO_ID
            --Determino si el co_id esta desactivado
            OPEN c_co_id_status(TN_COID(i));
            FETCH c_co_id_status
              INTO lc_co_id_status;
          
            IF c_co_id_status%NOTFOUND THEN
              CLOSE c_co_id_status;
            ELSE
              td_entdate(i) := TRUNC(lc_co_id_status.entdate) - 1;
              CLOSE c_co_id_status;
            END IF;
          
          END IF;
        
          IF lc_config.recurrencia = 'N' THEN
            ln_period := 1;
          ELSE
            ln_period := 99;
          END IF;
        
          ln_counter := ln_counter + 1;
        
        END LOOP;
      
        --inserto en fees 
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          INSERT /*+ APPEND */
          INTO fees
            (customer_id,
             seqno,
             fee_type,
             glcode,
             entdate,
             period,
             username,
             valid_from,
             servcat_code,
             serv_code,
             serv_type,
             co_id,
             amount,
             remark,
             currency,
             glcode_disc,
             glcode_mincom,
             tmcode,
             vscode,
             spcode,
             sncode,
             evcode,
             fee_class)
          VALUES
            (tn_customer_id(m),
             tn_ln_seq(m),
             lc_config.recurrencia,
             Tv_Accglcode2(M),
             td_entdate(m),
             ln_period,
             lc_config.usuario,
             td_valid_from(m),
             Tv_Accserv_Catcode2(M),
             Tv_Accserv_Code2(M),
             Tv_Accserv_Type2(M),
             tn_coid(M),
             tn_amount(M),
             lc_config.remark,
             19,
             Tv_Accglcode2(M),
             Tv_Accglcode2(M),
             35,
             Tn_Vscode2(M),
             Tn_Spcode2(M),
             Tn_Sncode(M),
             Tn_Evcode2(M),
             3);
      
        --inseerto en fees_cargas_nc
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
        
          INSERT /*+ APPEND */
          INTO fees_cargas_nc
            (customer_id,
             seqno,
             fee_type,
             glcode,
             entdate,
             period,
             username,
             valid_from,
             servcat_code,
             serv_code,
             serv_type,
             co_id,
             amount,
             remark,
             currency,
             glcode_disc,
             glcode_mincom,
             tmcode,
             vscode,
             spcode,
             sncode,
             evcode,
             fee_class,
             producto,
             compania,
             genera_nc,
             estado             
             )
          VALUES
            (tn_customer_id(m),
             tn_ln_seq(m),
             lc_config.recurrencia,
             Tv_Accglcode2(M),
             td_entdate(m),
             ln_period,
             lc_config.usuario,
             td_valid_from(m),
             Tv_Accserv_Catcode2(M),
             Tv_Accserv_Code2(M),
             Tv_Accserv_Type2(M),
             tn_coid(M),
             tn_amount(M),
             lc_config.remark,
             19,
             Tv_Accglcode2(M),
             Tv_Accglcode2(M),
             35,
             Tn_Vscode2(M),
             Tn_Spcode2(M),
             Tn_Sncode(M),
             Tn_Evcode2(M),
             3,
             pv_producto,
             pv_compania,
             pv_genera,
             'X' );
      
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          UPDATE bl_carga_occ_tmp_nc
             SET status = 'P',
                 error  = NULL
           WHERE ROWID = TV_ROWID(m);
      
        UPDATE BL_CARGA_EJECUCION
           SET reg_procesados = reg_procesados + ln_counter
         WHERE id_ejecucion = PN_ID_EJECUCION;
      
        UPDATE bl_carga_ejec_hilo_stat
           SET reg_procesados = reg_procesados + ln_counter
         WHERE id_ejecucion = PN_ID_EJECUCION
           AND hilo = pn_hilo;
      
        UPDATE bl_carga_ejec_hilo_detail
           SET reg_procesados = reg_procesados + ln_counter
         WHERE id_ejecucion = PN_ID_EJECUCION
           AND hilo = pn_hilo
           AND seq = ln_hilo_seq;
      
        IF BLF_obtiene_estado(PN_ID_EJECUCION => PN_ID_EJECUCION) = 'U' THEN
        
          UPDATE bl_carga_ejec_hilo_stat
             SET estado = 'U'
           WHERE id_ejecucion = PN_ID_EJECUCION
             AND hilo = pn_hilo;
        
          UPDATE bl_carga_ejec_hilo_detail
             SET estado    = 'U',
                 fecha_fin = SYSDATE
           WHERE id_ejecucion = PN_ID_EJECUCION
             AND hilo = pn_hilo
             AND seq = ln_hilo_seq;
        
          EXIT;
        
        END IF;
      
      END IF;
    
      IF c_carga%NOTFOUND OR lb_nohaydatos THEN
        UPDATE bl_carga_ejec_hilo_stat
           SET estado = 'F'
         WHERE id_ejecucion = PN_ID_EJECUCION
           AND hilo = pn_hilo;
      
        UPDATE bl_carga_ejec_hilo_detail
           SET estado    = 'F',
               fecha_fin = SYSDATE
         WHERE id_ejecucion = PN_ID_EJECUCION
           AND hilo = pn_hilo
           AND seq = ln_hilo_seq;
      
        COMMIT;
      
        EXIT;
      
      END IF;
    
      BEGIN
        SELECT TRUNC((SYSDATE - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          INTO ln_minutos,
               ln_registros_total
          FROM BL_CARGA_EJECUCION
         WHERE id_ejecucion = PN_ID_EJECUCION;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      BEGIN
        SELECT TRUNC((SYSDATE - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          INTO ln_minutos,
               ln_registros_total
          FROM BL_CARGA_EJECUCION
         WHERE id_ejecucion = PN_ID_EJECUCION;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      --Notificaciones peri�dicas
      IF TRUNC((SYSDATE - lc_config.fecha_inicio) * 24 * 60) >= BLF_obtiene_ultima_notif(PN_ID_EJECUCION => PN_ID_EJECUCION) + lc_config.tiempo_notif THEN
      
        BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion, pv_mensaje => 'Avance proceso Carga OCC. Usuario: ' || lc_config.usuario || ' Minutos: ' || TO_CHAR(ln_minutos) || ' Total reg: ' || ln_registros_total);
      
        Blp_Setea_Ultima_Notif(PN_ID_EJECUCION => PN_ID_EJECUCION, pn_valor => TRUNC((SYSDATE - lc_config.fecha_inicio) * 24 * 60) + lc_config.tiempo_notif);
      
      END IF;
    
      COMMIT;
    
    END LOOP;
  
    CLOSE c_carga;
  
    --Ya finaliz� el proceso?
    IF BLF_verifica_finalizacion(PN_ID_EJECUCION => PN_ID_EJECUCION) THEN
      BLP_actualiza(PN_ID_EJECUCION => PN_ID_EJECUCION, pd_fecha_fin => SYSDATE, pv_observacion => 'Finalizado', Pv_Estado => 'F');
    
      IF lc_config.respaldar IN ('Y', 'S') THEN
        BLP_Respalda_tabla(pv_tabla_respaldar => lc_config.nombre_respaldo);
      END IF;
    
      BEGIN
        SELECT TRUNC((SYSDATE - fecha_inicio) * 24 * 60, 2) minutos,
               reg_procesados
          INTO ln_minutos,
               ln_registros_total
          FROM BL_CARGA_EJECUCION
         WHERE id_ejecucion = PN_ID_EJECUCION;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion, pv_mensaje => 'Fin proceso Carga OCC. Usuario: ' || lc_config.usuario || ' Remark: ' || lc_config.remark || ' Minutos: ' || TO_CHAR(ln_minutos) || ' Total reg: ' || ln_registros_total);
    
      BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion, pv_mensaje => 'Fin proceso Carga OCC. Usuario: ' || lc_config.usuario || ' Minutos: ' || TO_CHAR(ln_minutos) || ' Total reg: ' || ln_registros_total);
    
    END IF;
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      Lv_Error := SUBSTR(SQLERRM, 1, 199);
      BLP_actualiza(PN_ID_EJECUCION => PN_ID_EJECUCION, pd_fecha_fin => SYSDATE, pv_observacion => Lv_Error, Pv_Estado => 'E');
    
      BLP_envia_sms_grupo(pn_id_notificacion => lc_config.id_notificacion, pv_mensaje => 'Error proceso Carga OCC: ' || SUBSTR(Lv_Error, 1, 100));
    
      BLP_envia_correo_grupo(pn_id_notificacion => lc_config.id_notificacion, pv_mensaje => 'Error proceso Carga OCC: ' || SQLERRM);
    
      UPDATE bl_carga_ejec_hilo_stat
         SET estado = 'E'
       WHERE id_ejecucion = PN_ID_EJECUCION
         AND hilo = pn_hilo;
    
      UPDATE bl_carga_ejec_hilo_detail
         SET estado      = 'E',
             fecha_fin   = SYSDATE,
             observacion = Lv_Error
       WHERE id_ejecucion = PN_ID_EJECUCION
         AND hilo = pn_hilo
         AND seq = ln_hilo_seq;
    
      COMMIT;
    
  END BLP_Carga_Occ_Co_id;


  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para respaldar informacion de la tabla de cargas.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_RESPALDA_TABLA(PV_TABLA_RESPALDAR VARCHAR2) IS
  
    lv_sql VARCHAR2(500);
  
  BEGIN
  
    lv_sql := 'CREATE TABLE ' || pv_tabla_respaldar || ' AS ' || ' select * from BL_CARGA_OCC_TMP_NC ';
  
    EXECUTE IMMEDIATE lv_sql;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
    
  END;

   /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para actualizar la tabla de BL_CARGA_EJECUCION.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_ACTUALIZA(PN_ID_EJECUCION   NUMBER,
                          PD_FECHA_INICIO   DATE DEFAULT NULL,
                          PD_FECHA_FIN      DATE DEFAULT NULL,
                          PV_ESTADO         VARCHAR2 DEFAULT NULL,
                          PN_REG_PROCESADOS NUMBER DEFAULT NULL,
                          PV_OBSERVACION    VARCHAR2 DEFAULT NULL) IS
  
  BEGIN
    UPDATE BL_CARGA_EJECUCION
       SET fecha_inicio   = NVL(PD_FECHA_INICIO, fecha_inicio),
           fecha_fin      = NVL(PD_FECHA_FIN, fecha_fin),
           estado         = NVL(PV_ESTADO, estado),
           reg_procesados = NVL(PN_REG_PROCESADOS, reg_procesados),
           observacion    = NVL(PV_OBSERVACION, observacion)
     WHERE id_ejecucion = PN_ID_EJECUCION;
  
    COMMIT;
  
  END;

  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento que envia SMS de ejecucion.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_ENVIA_SMS(PV_DESTINATARIO VARCHAR2,
                          PV_MENSAJE      VARCHAR2) IS
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
    lv_msg_error VARCHAR2(200);
  
  BEGIN
  
   
     swk_sms.send@axis(nombre_servidor => 'sms',
    id_servciio => pv_destinatario,
    pv_mensaje => pv_mensaje,
    pv_msg_err => lv_msg_error);
  
   
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;


  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para enviar sms al grupo que esta realcionado 
                     con las cargas de credito BSCS.  
   --=====================================================================================--*/
  
  PROCEDURE BLP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                PV_MENSAJE         VARCHAR2) IS
  
    CURSOR c_grupo_noti IS
      SELECT b.destinatario
        FROM BL_CARGA_GRUPO_NOTIFICACION a,
             BL_CARGA_NOTIFICA           b
       WHERE a.id_notifica = b.id_notifica
         AND a.estado = 'A'
         AND b.estado = 'A'
         AND b.tipo = 'SMS'
         AND a.id_notificacion = PN_ID_NOTIFICACION;
  
  BEGIN
  
    FOR j IN c_grupo_noti LOOP
      BLP_ENVIA_SMS(j.destinatario, PV_MENSAJE);
    END LOOP;
  
  END;
  
  
  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento para enviar correo al grupo que esta realcionado 
                     con las cargas de credito BSCS.  
   --=====================================================================================--*/
  
  
  PROCEDURE BLP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                   PV_MENSAJE         VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    CURSOR c_grupo_noti IS
      SELECT b.destinatario
        FROM bl_carga_grupo_notificacion a,
             bl_carga_notifica           b
       WHERE a.id_notifica = b.id_notifica
         AND a.estado = 'A'
         AND b.estado = 'A'
         AND b.tipo = 'MAIL'
         AND a.id_notificacion = PN_ID_NOTIFICACION;
  
    lv_remitentes VARCHAR2(4000) := NULL;
  BEGIN
  
    FOR i IN c_grupo_noti LOOP
      lv_remitentes := lv_remitentes || i.destinatario || ';';
    END LOOP;
    lv_remitentes := SUBSTR(lv_remitentes, 1, LENGTH(lv_remitentes) - 1);
  
     send_mail.mail@axis(from_name => 'blk_carga_occ_nc@conecel.com',
    to_name   => lv_remitentes,
    cc        => null,
    cco       => null,
    subject   => 'Proceso BLK_CARGA_OCC_NC',
    message   => pv_mensaje);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
 
  
  /*--=====================================================================================--
    Modificado por : CIMA. Byron Anton
    L�der proyecto : CIMA. Hugo fuentes
    L�der Sis      : SIS Juan Carlos Romero
    Fecha          : 29/01/2014
    Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS  
    Motivo         : Procedimiento que despacha los creditos de la tabla de bitacora BL_CARGA_OCC_TMP_NC.  
   --=====================================================================================--*/
  
  
  PROCEDURE BLP_EJECUTA(PV_USUARIO          VARCHAR2,
                        PN_ID_NOTIFICACION  NUMBER,
                        PN_TIEMPO_NOTIF     NUMBER,
                        PN_CANTIDAD_HILOS   NUMBER,
                        PN_CANTIDAD_REG_MEM NUMBER,
                        PV_RECURRENCIA      VARCHAR,
                        PV_REMARK           VARCHAR,
                        PD_ENTDATE          DATE,
                        PV_RESPALDAR        VARCHAR,
                        PV_TABLA_RESPALDO   VARCHAR,
                        PN_ID_EJECUCION     OUT NUMBER,
                        PV_PRODUCTO         VARCHAR2,
                        PV_COMPANIA         VARCHAR2,
                        PV_GENERA           VARCHAR2,
                        PV_ERROR            OUT VARCHAR2) IS
  
    ln_id_ejecucion NUMBER;
    
    CURSOR c_id_ejec IS
      SELECT NVL(MAX(id_ejecucion), 0) id_ejecucion
        FROM BL_CARGA_EJECUCION;
  
  BEGIN
  
    OPEN c_id_ejec;
    FETCH c_id_ejec
      INTO ln_id_ejecucion;
    CLOSE c_id_ejec;
  
    ln_id_ejecucion := ln_id_ejecucion + 1;
  
    INSERT INTO BL_CARGA_EJECUCION
    VALUES
      (ln_id_ejecucion,
       PV_USUARIO,
       SYSDATE,
       NULL,
       'L',
       0,
       PN_ID_NOTIFICACION,
       PN_TIEMPO_NOTIF,
       PN_CANTIDAD_HILOS,
       PN_CANTIDAD_REG_MEM,
       PV_RECURRENCIA,
       PV_REMARK,
       PD_ENTDATE,
       PV_RESPALDAR,
       PV_TABLA_RESPALDO,
       NULL);
  
    /* comentado para que no envie correos
    BLP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION => PN_ID_NOTIFICACION, PV_MENSAJE => 'INICIO DEL PROCESO CARGA OCC. USUARIO: ' || PV_USUARIO || ' REMARK: ' || PV_REMARK || ' FECHA: ' || TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI'));
  
    BLP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION => PN_ID_NOTIFICACION, PV_MENSAJE => 'INICIO DEL PROCESO CARGA OCC. USUARIO: ' || PV_USUARIO || ' FECHA: ' || TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI'));
  */
    BLP_ACTUALIZA(PN_ID_EJECUCION => LN_ID_EJECUCION, PV_ESTADO => 'P', PV_OBSERVACION => 'PROCESANDO: 0');
  
    BLP_BALANCEA_CARGA(PN_HILOS => PN_CANTIDAD_HILOS);
  
    FOR i IN 0 .. PN_CANTIDAD_HILOS - 1 LOOP
      INSERT INTO bl_carga_ejec_hilo_stat
      VALUES
        (ln_id_ejecucion,
         TO_CHAR(i),
         'P',
         0);
      COMMIT;
    
    END LOOP;
  
    FOR i IN 0 .. PN_CANTIDAD_HILOS - 1 LOOP
          
    BLP_CARGA_OCC_CO_ID(TO_CHAR(ln_id_ejecucion), i, pv_producto, pv_compania, pv_genera);
    
    END LOOP;
    COMMIT;
    PN_ID_EJECUCION := ln_id_ejecucion;
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Error := 'Error en ejecutar la carga: '||sqlerrm;
  END;


END BLK_CARGA_OCC_NC;
/
