CREATE OR REPLACE Package GCK_API_PUNTOS Is

  -- Author  : Cristian Medina Aguirre  CME
  -- Created : 24/10/2003 16:10:02
  -- Purpose : Programa de Puntos
  -- Versi�n 1.0




  Function canje (pv_telefono In Varchar2,
                  pn_id_contrato In Number,
                  pn_puntos_canjear In Number,
                  pv_origen   IN Varchar2,                    
                  pv_error    Out Varchar2) Return Number;
                  
  procedure CLP_CONSULTA_CUENTA(PV_SERVICIO         VARCHAR2,
                                PV_CONTRATO         IN OUT VARCHAR2,
                                PV_CUENTA           IN OUT VARCHAR2,
                                PV_CLIENTE          IN OUT VARCHAR2,
                                LN_ID_DETALLE_PLAN  IN OUT NUMBER,                                
                                PV_CODIGOERROR      IN OUT VARCHAR2);
                                
  procedure VALIDA_SERVICIO  (Pv_Servicio   VARCHAR2,                                        
                              Pv_contrato   NUMBER,
                              Pv_Estado           IN OUT VARCHAR2,
                              Pv_Subproducto      IN OUT VARCHAR2,
                              Pv_id_persona       IN OUT NUMBER,
   				  						      Pv_Mensaje					IN OUT VARCHAR2);
                                


/*  \* PROCEDIMIENTO DE CONSULTA INICIAL *\
  Function Estado_Actual (pv_telefono    In Varchar2,
           pv_ingresados Out Varchar2,
           pv_mensaje    Out Varchar2
           ) Return Number;

  \* MODIFICA LOS NUMEROS SELECCIONADOS POR EL CLIENTE *\
  Function Cambia_Numeros (pv_telefono    In Varchar2,
           pv_numero_ant  In Varchar2,
           pv_numero_act  In Varchar2,
           pv_origen      In Varchar2,
           pv_error      Out Varchar2,
           pn_secuencia   In Number   Default Null
           ) Return Number;

  \* REGISTRA NUMERO INGRESADO *\
  Function Configura_Numeros (pv_telefono   In Varchar2,
           pv_numero      In Varchar2,
           pv_origen      In Varchar2,
           pv_error      Out Varchar2,
           pn_contrato    In Number   Default Null,
           pv_producto    In Varchar2 Default Null,
           pv_cuenta      In Varchar2 Default Null,
           pn_secuencia   In Number   Default Null,
           pv_accion      In Varchar2 default Null
           ) Return Number;

  \* ELIMINA/INACTIVA LOS NUMEROS SELECCIONADOS POR EL CLIENTE *\
  Function Elimina_Numeros (pv_telefono    In Varchar2,
           pv_numero      In Varchar2,
           pv_origen      In Varchar2,
           pv_error      Out Varchar2,
           pn_contrato    In Number   Default Null,
           pv_producto    In Varchar2 Default Null,
           pv_cuenta      In Varchar2 Default Null,
           pn_secuencia   In Number   Default Null
           ) Return Number;

  \* VERIFICA EL NUMERO A SER CONFIGURADO POR EL CLIENTE *\
  Function Verifica_Numero (pv_telefono_cliente In Varchar2,
           pv_telefono_config  In Varchar2,
           pv_error           Out Varchar2
           ) Return Number;

  \* VERIFICA EL PERIODO PARA MODIFICAR UN NUMERO *\
  Function Verifica_Periodo_Cambiar (pv_telefono  In Varchar2,
           pn_dias     Out Number,
           pn_costo    Out Number,
           pv_mensaje  Out Varchar2
           ) Return Number;

  \* CONSULTA FEATURE MAS FAMILIA Y MAS AMIGOS ACTIVO *\
  Function Consulta_Feature(pv_telefono     In Varchar2,
           pv_producto    Out Varchar2,
           pd_fecha_desde Out Date,
           pv_detalle     Out Varchar2,
           pv_error       Out Varchar2
           ) Return Number;

  \* CONSULTA TELEFONOS CONFIGURADOS *\
  Function Consulta_Telefonos_Ingresados (pv_telefono     In Varchar2,
           pn_cantidad    Out Number,
           pv_telefonos   Out Varchar2,
           pv_error       Out Varchar2
           ) Return Number;

  \* OBTENGO LA FECHA PIVOTE *\
  Procedure Obtiene_Fecha_Pivote (pv_telefono  In Varchar2,
            pd_pivote   Out Date,
            pv_error    Out Varchar2);

  \* OBTENGO EL COSTO CON IVA Y SIN IVA SEGUN LA ACCION SEA ACTIVAR ACTUALIZAR AGREGAR *\
  Procedure Obtiene_costo (pv_accion         In Varchar2,
            pn_costo_con_iva Out Number,
            pn_costo_sin_iva Out Number,
            pv_error         Out Varchar2);

  \* SIMPLIFICA LA CONSULTA DE SALDO EN TECNOMEN *\ 
  Procedure Consulta_Saldo (pv_telefono  In Varchar2,
            pn_saldo    Out Number,
            pv_error    Out Varchar2);

  \* OBTIENE INFORMACION BASICA DE UN SERVICIO ACTIVO EN AXIS *\
  Procedure Obtiene_Info (pv_telefono      In Varchar2,
            pv_producto     Out Varchar2,
            pn_contrato     Out Number,
            pv_red          Out Varchar2,
            pv_error        Out Varchar2);*/
            
   procedure PROCESA_EDAD_SERVICIO;
   procedure PROCESA_EDAD_CONTRATO;

End GCK_API_PUNTOS;
/

