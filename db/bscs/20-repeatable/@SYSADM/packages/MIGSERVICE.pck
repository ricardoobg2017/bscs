CREATE OR REPLACE PACKAGE MigService
/*
**
**
** References:
**    HLD plsql/73998.rtf: Database Migration Concepts II
**
** Author: Peter Sorg, SEMA Telecoms
**
** Filenames: migutil.sph - Package header
**            migutil.spb - Package body
**
** Overview:
**
** This package contains utility procedures the migration package .
** It facilitates an user exit for integrations.
**
**
** Major Modifications (when, who, what)
**
** 02/2001 - PSO - initial version of MigService package
**
*/
IS
  /*------------------------- Version Control------------------------*/
   cosHeadScriptVersion CONSTANT DabUtil.tosScriptVersion  := '/main/1';
   cosHeadScriptWhat    CONSTANT DabUtil.tosScriptWhat     := 'but_bscs/bscs/database/scripts/alter/fix/migservice.sph, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

   /*
   ** =====================================================================
   **
   ** Procedure: GetParaValue
   **
   ** Purpose: Load a configurable migration value
   **          Support character values
   **
   ** =====================================================================
   */
   PROCEDURE GetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,poosValue OUT DabUtil.tosParaValue

       -- Default Value String
      ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   );

   /*
   ** Overload: Number type Values
   */
   PROCEDURE GetParaValue
   (
       -- Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value Number
      ,poonValue OUT DabUtil.tonParaValue

       -- Default Value Number
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   );

   /*
   ** =====================================================================
   **
   ** Procedure: SetParaValue
   **
   ** Purpose: Set a configurable migration value
   **
   ** Overload: Character type values
   ** =====================================================================
   */
   PROCEDURE SetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,piosParaValue IN DabUtil.tosParaValue
   );

   /*
   ** Overload: Number type Values
   */
   PROCEDURE SetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,pionParaValue IN DabUtil.tonParaValue
   );

   /*
   ** =====================================================================
   **
   ** Procedure: ObjectStorage
   **
   ** Purpose: Provide storage parameters for database objects
   **
   ** =====================================================================
   */
   PROCEDURE  ObjectStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Object Type
      ,piosObjectType IN DabUtil.tosObjectType

       -- Storage Clause
      ,poosStorageClause OUT DabUtil.tosStorageClause

       --  Object Number (enumerates indexes or unique keys per table
      ,pionObjectNumber  IN INTEGER DEFAULT ( 1 )

       -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   );

   /*
   ** =====================================================================
   **
   ** Procedure: GetKeyRange
   **
   ** Purpose: Determine start and stop key of a migration step
   **
   ** =====================================================================
   */
   PROCEDURE GetKeyRange
   (
       -- Table name
       piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

       -- Infimum Key Value of the migration step
       -- This is the largest value not to be migrated between
      ,piosKeyInfValue IN DabUtil.tosKeyValue

       -- Maximal Key Value of the migration step
      ,piosKeyMaxValue IN DabUtil.tosKeyValue

       -- Maximal number of rows between Startkey and Stopkey
       -- Commit size.
      ,pmonNumberOfRows IN OUT INTEGER

       -- Start key for the migration
      ,poosStartKeyValue OUT DabUtil.tosKeyValue

       -- Stop key for the migration
      ,poosStopKeyValue OUT DabUtil.tosKeyValue

       -- Number of key values between start key and stop key
      ,poonNumberOfKeys OUT INTEGER

   );

   /*
   ** =====================================================================
   **
   ** Procedure: GetKeyValues
   **
   ** Purpose: Determine the key ranges of a parallel migration.
   **
   ** =====================================================================
   */
   PROCEDURE GetKeyValues
   (
       -- Table name
       piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

       -- Number of parallel steps
      ,pionMigParallelNum IN INTEGER

       -- Total number of rows
      ,poonTotalRowNum  OUT INTEGER

       -- Array of key values
      ,poarKeyValues OUT DabUtil.tarKeyValues

   );

   /*
   ** =====================================================================
   **
   ** Procedure: GetTranferStatement
   **
   ** Purpose: Build the SQL statement for the data transfer method.
   **          The SQL statement is of the form:
   **          INSERT INTO TargetTable  ()
   **          SELECT .. FROM SourceTable
   **
   ** =====================================================================
   */
   PROCEDURE GetTransferStatement
   (
       -- Target table name
       piosTargetTableName IN DabUtil.tosObjectName

       -- Schema of the target table
      ,piosTargetSchema IN DabUtil.tosSchema

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- Schema of the source table
      ,piosSourceSchema IN DabUtil.tosSchema

       -- Name of the database link from the target to the source database
      ,piosSourceDBLink IN DabUtil.tosDBLink

       -- Where clause added to the SQL statement
      ,piasWhereClause  IN DabUtil.tasText

       -- Transfer statement: INSERT INTO ... SELECT FROM ...
      ,poasTransferStatement  OUT DabUtil.tasText

       -- Informational message
      ,poasMessageText  OUT DabUtil.tasText
   );

END MigService;
/
CREATE OR REPLACE PACKAGE BODY MigService
/*
**
**
** References:
**
** Author: Peter Sorg, SEMA Telecoms
**
** Filenames: migutil.sph - Package header
**            migutil.spb - Package body
**
** Overview:
**
** This package provides service utility procedures the migration package.
** It facilitates an user exit for integrations.
**
**
** Major Modifications (when, who, what)
**
** 02/2001 - PSO - initial version of MigService package
**
*/
IS

   /*------------------------- CMVC version control------------------------*/
   cosBodyScriptVersion CONSTANT DabUtil.tosScriptVersion := '/main/2';
   cosBodyScriptWhat    CONSTANT DabUtil.tosScriptWhat    := 'but_bscs/bscs/database/scripts/alter/fix/migservice.spb, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';

   /* ---------------------------------------------------------------------
   **  Constants
   */
   cosNewline VARCHAR2(1) := CHR( 10 );

   /* ---------------------------------------------------------------------
   **  Static variables and exceptions
   */
   seoApplicationError EXCEPTION;
   PRAGMA EXCEPTION_INIT( seoApplicationError , -20002 );
   sosErrorText VARCHAR2(2000) := NULL;

   /*------------------------- Privat Types      --------------------------*/

   TYPE tasColumnName IS TABLE OF DabUtil.tosAttributeName
   INDEX BY BINARY_INTEGER;

   TYPE tasDataType IS TABLE OF VARCHAR2(9)
   INDEX BY BINARY_INTEGER;

   TYPE tanDataLength IS TABLE OF INTEGER
   INDEX BY BINARY_INTEGER;

   TYPE tasNullable IS TABLE OF  VARCHAR2(1)
   INDEX BY BINARY_INTEGER;

   TYPE tanArrayIndex IS TABLE OF INTEGER
   INDEX BY BINARY_INTEGER;


   /*------------------------- Privat Procedures --------------------------*/

   /*
   **=====================================================
   **
   ** Procedure: CallError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE CallError( piosErrorText IN VARCHAR2 )
   IS
   BEGIN
      sosErrorText := piosErrorText;
      RAISE seoApplicationError;
   END CallError;

   /*
   **=====================================================
   **
   ** Procedure: RaiseError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE RaiseError
   (
       piosProcName IN VARCHAR2
      ,piosErrorText IN VARCHAR2 DEFAULT NULL
   )
   IS
      losErrorText VARCHAR2(2000) ;
   BEGIN
      losErrorText := 'MigService.' || piosProcName || ' : ' || piosErrorText || cosNewLine;
         losErrorText := losErrorText || sosErrorText
                         || cosNewLine ||  SQLERRM ;
      sosErrorText := NULL;
      RAISE_APPLICATION_ERROR( -20001 , losErrorText , TRUE );
   END RaiseError;

   /*
   **=====================================================
   **
   ** Procedure: CallTrace
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */
   PROCEDURE CallTrace( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( 'REM TRACE: ' || piosInfoText );
   END CallTrace;

   /*
   **=====================================================
   **
   ** Procedure: CallInfo
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */
   PROCEDURE CallInfo( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( 'REM INFO: ' || piosInfoText );
   END CallInfo;

   /*
   **=====================================================
   **
   ** Procedure: CallWarning
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */
   PROCEDURE CallWarning( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( 'REM WARNING: ' || piosInfoText || SQLERRM );
   END CallWarning;

   /*------------------------- Public Modules -----------------------------*/
   /*
   ** Version control
   */
   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN cosBodyScriptVersion || '|' || cosBodyScriptWhat;
   END PrintBodyVersion;

   /*
   ** =====================================================================
   **
   ** Procedure: GetParaValue
   **
   ** Purpose: Load a configurable migration value
   **          Support character values
   **
   ** =====================================================================
   */
   PROCEDURE GetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,poosValue OUT DabUtil.tosParaValue

       -- Default Value String
      ,piosDefaultValue IN DabUtil.tosParaValue DEFAULT NULL
   )
   IS
      losSelectedValue MIG_PARAMETER.PARAMETER_VALUE_CHAR%TYPE;

      CURSOR locParaValue ( xcrParameterKey VARCHAR2 )
      IS
         SELECT PARAMETER_VALUE_CHAR
         FROM   MIG_PARAMETER
         WHERE  PARAMETER_KEY = xcrParameterKey;

   BEGIN
      OPEN locParaValue ( piosParaKey );

      FETCH locParaValue
      INTO  losSelectedValue;

      IF locParaValue%FOUND
      THEN
         poosValue := losSelectedValue;
      ELSIF piosDefaultValue IS NOT NULL
      THEN
         poosValue := piosDefaultValue;
      ELSE
         CallError( 'No parameter value defined for key ' || piosParaKey );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetParaValue' );
   END GetParaValue;

   /*
   ** Overload: Number type Values
   */
   PROCEDURE GetParaValue
   (
       -- Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value Number
      ,poonValue OUT DabUtil.tonParaValue

       -- Default Value Number
      ,pionDefaultValue IN DabUtil.tonParaValue DEFAULT NULL
   )
   IS
      lonSelectedValue MIG_PARAMETER.PARAMETER_VALUE_NUM%TYPE;

      CURSOR locParaValue ( xcrParameterKey VARCHAR2 )
      IS
         SELECT PARAMETER_VALUE_NUM
         FROM   MIG_PARAMETER
         WHERE  PARAMETER_KEY = xcrParameterKey;

   BEGIN
      OPEN locParaValue ( piosParaKey );

      FETCH locParaValue
      INTO  lonSelectedValue;

      IF locParaValue%FOUND
      THEN
         poonValue := lonSelectedValue;
      ELSIF pionDefaultValue IS NOT NULL
      THEN
         poonValue := pionDefaultValue;
      ELSE
         CallError( 'No parameter value defined for key ' || piosParaKey );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetParaValue' );
   END GetParaValue;

   /*
   ** =====================================================================
   **
   ** Procedure: SetParaValue
   **
   ** Purpose: Set a configurable migration value
   **
   ** Overload: Character type values
   ** =====================================================================
   */
   PROCEDURE SetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,piosParaValue IN DabUtil.tosParaValue
   )
   IS
     lobUpdateRow BOOLEAN := FALSE;
   BEGIN

      BEGIN
         INSERT INTO MIG_PARAMETER
         (
             PARAMETER_KEY
            ,PARAMETER_VALUE_CHAR
         )
         VALUES
         (
             piosParaKey
            ,piosParaValue
         );
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            lobUpdateRow := TRUE;
         WHEN OTHERS
         THEN
            CallError( 'Insert of parameter key ' || piosParaKey
                        || ' into MIG_PARAMETER failed. ' || SQLERRM ) ;
      END;

      ----------------------------------------------------------
      -- Update value if row exists
      IF lobUpdateRow
      THEN
         BEGIN
            UPDATE MIG_PARAMETER
            SET  PARAMETER_VALUE_CHAR = piosParaValue
                ,PARAMETER_VALUE_NUM  = NULL
            WHERE PARAMETER_KEY = piosParaKey;
         EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Update of MIG_PARAMETER for key '
                        || piosParaKey || ' failed. ' || SQLERRM );
         END;
      END IF;

   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'SetParaValue' );
   END SetParaValue;

   /*
   ** Overload: Number type Values
   */
   PROCEDURE SetParaValue
   (
       -- Parameter Key
       piosParaKey IN DabUtil.tosParaKey

       -- Value String
      ,pionParaValue IN DabUtil.tonParaValue
   )
   IS
     lobUpdateRow BOOLEAN := FALSE;
   BEGIN

      BEGIN
         INSERT INTO MIG_PARAMETER
         (
             PARAMETER_KEY
            ,PARAMETER_VALUE_NUM
         )
         VALUES
         (
             piosParaKey
            ,pionParaValue
         );
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            lobUpdateRow := TRUE;
         WHEN OTHERS
         THEN
            CallError( 'Insert of parameter key ' || piosParaKey
                        || ' into MIG_PARAMETER failed. ' || SQLERRM ) ;
      END;

      ----------------------------------------------------------
      -- Update value if row exists
      IF lobUpdateRow
      THEN
         BEGIN
            UPDATE MIG_PARAMETER
            SET  PARAMETER_VALUE_NUM = pionParaValue
                ,PARAMETER_VALUE_CHAR = NULL
            WHERE PARAMETER_KEY = piosParaKey;
         EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Update of MIG_PARAMETER for key '
                        || piosParaKey || ' failed. ' || SQLERRM );
         END;
      END IF;

   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'SetParaValue' );
   END SetParaValue;

   /*
   ** =====================================================================
   **
   ** Procedure: ObjectStorage
   **
   ** Purpose: Provide storage parameters for database objects
   **
   ** =====================================================================
   */
   PROCEDURE  ObjectStorage
   (
       -- Table Name
       piosTableName IN  DabUtil.tosObjectName

       -- Object Type
      ,piosObjectType IN DabUtil.tosObjectType

       -- Storage Clause
      ,poosStorageClause OUT DabUtil.tosStorageClause

       --  Object Number (enumerates indexes or unique keys per table
      ,pionObjectNumber  IN INTEGER DEFAULT ( 1 )

       -- Default storage clause
      ,piosDefaultStorage IN DabUtil.tosStorageClause DEFAULT NULL
   )
   IS
      losParaKey DabUtil.tosParaKey ;
      losDefaultStorage DabUtil.tosStorageClause;
      def_available INTEGER := 0;
   BEGIN
      IF piosObjectType = DabUtil.cosTable
      THEN
         losParaKey := 'PARA_TABLE_STORAGE';
      ELSIF piosObjectType = DabUtil.cosPkey
      THEN
         losParaKey := 'PARA_PKEY_STORAGE';
      ELSIF piosObjectType = DabUtil.cosUkey
      THEN
         losParaKey := 'PARA_UKEY_STORAGE';
      ELSIF piosObjectType = DabUtil.cosIndex
      THEN
         losParaKey := 'PARA_INDEX_STORAGE';
      ELSIF piosObjectType = DabUtil.cosPartition
      THEN
         losParaKey := 'PARA_PARTITION_STORAGE';
      ELSE
         CallError( 'Object type ' || piosObjectType || ' is not supported in this context.' );
      END IF;

      IF piosDefaultStorage IS NULL
      THEN
         -- PN 221698
         -- Get default storage parameter from table if not set outside
         SELECT count(*)
         INTO   def_available
         FROM   mig_parameter
         WHERE  PARAMETER_KEY = losParaKey;
         IF def_available >= 1
         THEN
            GetParaValue( losParaKey , losDefaultStorage );
         END IF;

         IF losDefaultStorage IS NULL
         THEN
            losDefaultStorage := ' ';
         END IF;
      ELSE
         losDefaultStorage := piosDefaultStorage;
      END IF;

      -- PN 221698
      -- Get storage parameter for table or object
      -- GetParaValue( losParaKey    , poosStorageClause ,  losDefaultStorage );

      -- PN 223621
      -- Get default storage parameter per table if not set outside
      SELECT count(*)
      INTO   def_available
      FROM   mig_parameter
      WHERE  PARAMETER_KEY = losParaKey || '_' || piosTableName;
      IF def_available >= 1
      THEN
         GetParaValue( losParaKey || '_' || piosTableName , poosStorageClause
                      , losDefaultStorage );
      ELSE
         GetParaValue( piosTableName , poosStorageClause ,  losDefaultStorage );
      END IF;

   EXCEPTION
   WHEN OTHERS
   THEN
      RaiseError( 'ObjectStorage' );
   END ObjectStorage;

   /*
   ** =====================================================================
   **
   ** Procedure: GetKeyRange
   **
   ** Purpose: Determine start and stop key of a migration step
   **
   ** =====================================================================
   */
   PROCEDURE GetKeyRange
   (
       -- Table name
       piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

       -- Infimum Key Value of the migration step
       -- This is the largest value not to be migrated between
      ,piosKeyInfValue IN DabUtil.tosKeyValue

       -- Maximal Key Value of the migration step
      ,piosKeyMaxValue IN DabUtil.tosKeyValue

       -- Maximal number of rows between Startkey and Stopkey
       -- Commit size.
      ,pmonNumberOfRows IN OUT INTEGER

       -- Start key for the migration
      ,poosStartKeyValue OUT DabUtil.tosKeyValue

       -- Stop key for the migration
      ,poosStopKeyValue OUT DabUtil.tosKeyValue

       -- Number of key values between start key and stop key
      ,poonNumberOfKeys OUT INTEGER
   )
   IS
      losSQLText       DabUtil.tosSQLText;
      losStartKeyValue DabUtil.tosKeyValue;
      losStopKeyValue  DabUtil.tosKeyValue;
	    losStopKeyValue1 DabUtil.tosKeyValue;
      lonNumberOfKeys  INTEGER := 0;
      lonNumberOfRows  INTEGER := 0;
   BEGIN

      ------------------------------------------------------------------
      -- Validate input parameters
      IF pmonNumberOfRows IS NULL
      OR pmonNumberOfRows <= 0
      THEN
         CallError( 'Number of rows to be migrated has to be > 0' );
      END IF;

      ------------------------------------------------------------------
      -- Fetch start and stop key from the table to be migrated.
      -- There are five different select statements depending on
      -- the NULLITY of InfValue and MaxValue.

      IF piosKeyInfValue = piosKeyMaxValue
      THEN
         lonNumberOfRows := 0;
      ELSIF piosKeyInfValue IS NOT NULL
      AND   piosKeyMaxValue IS NOT NULL
      THEN
         ---------------------------------------------------------------
         -- 1) The InfimumValue to MaxValue case (Range BETWEEN two values)

         losSQLText :=
          'SELECT  MIN(' || piosKeyColumn || ' ) FROM '
        || piosTableName || ' WHERE ' || piosKeyColumn || ' > :1   AND '
        || piosKeyColumn || ' <= :2';


         BEGIN
            EXECUTE IMMEDIATE losSQLText
            INTO    losStartKeyValue
            USING   piosKeyInfValue
                   ,piosKeyMaxValue;

            IF losStartKeyValue IS NULL THEN
              lonNumberOfKeys := 0;
              lonNumberOfRows := 0;
            ELSE
               losStopKeyValue := least(losStartKeyValue + pmonNumberOfRows, piosKeyMaxValue);
               lonNumberOfKeys := losStopKeyValue - losStartKeyValue + 1;
           END IF;

         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Can not execute: "' || losSQLText || '".' );
         END;

      ELSIF   piosKeyInfValue IS NOT NULL
      AND     piosKeyMaxValue IS NULL
      THEN
         ---------------------------------------------------------------
         -- 2) The InfValue to +Infinity case ( ex: last portion of a table)

          losSQLText := 'SELECT  MIN(' || piosKeyColumn || ' ) , MAX(' || piosKeyColumn || ' ) FROM '
        			          || piosTableName || ' WHERE ' || piosKeyColumn || ' > :1 ' ;

         BEGIN
            EXECUTE IMMEDIATE losSQLText
            INTO    losStartKeyValue
                    ,losStopKeyValue1
            USING   piosKeyInfValue;

            IF losStartKeyValue IS NULL THEN
               lonNumberOfKeys := 0;
               lonNumberOfRows := 0;
            ELSE
               losStopKeyValue := least(losStartKeyValue + pmonNumberOfRows, losStopKeyValue1);
               lonNumberOfKeys := losStopKeyValue - losStartKeyValue + 1;
           END IF;

         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Can not execute: "' || losSQLText || '".' );
         END;

      ELSIF   piosKeyInfValue IS NULL
      AND  piosKeyMaxValue IS NOT NULL
      THEN
         ---------------------------------------------------------------
         -- 3) The -Infinity to MaxValue case ( ex: first portion of a table)

        losSQLText := 'SELECT  MIN(' || piosKeyColumn || ' ) FROM '
                      || piosTableName || ' WHERE ' || piosKeyColumn || ' <= :1 ';

         BEGIN
            EXECUTE IMMEDIATE losSQLText
            INTO    losStartKeyValue
            USING   piosKeyMaxValue;

            IF losStartKeyValue IS NULL THEN
               lonNumberOfKeys := 0;
               lonNumberOfRows := 0;
           ELSE
               losStopKeyValue := least(losStartKeyValue + pmonNumberOfRows, piosKeyMaxValue) ;
               lonNumberOfKeys := losStopKeyValue - losStartKeyValue + 1;
           END IF;

         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Can not execute: "' || losSQLText || '".' );
         END;

      ELSE
         ---------------------------------------------------------------
         -- 4) The -infinity to +infinity case ( ex: complete table)

         losSQLText :=
'SELECT  MIN( KEY_COLUMN )
        ,MAX( KEY_COLUMN )
        ,COUNT(DISTINCT KEY_COLUMN)
        ,MAX(ROW_COUNTER)
FROM
(
   SELECT  ' || piosKeyColumn || ' KEY_COLUMN
          ,ROW_NUMBER() OVER ( ORDER BY ' || piosKeyColumn || ' ) ROW_NUMBER
          ,COUNT(*) OVER ( ORDER BY ' || piosKeyColumn || ' ) ROW_COUNTER
   FROM ' || piosTableName || '
)
WHERE ROW_NUMBER <= :1 ' ;

         BEGIN
            EXECUTE IMMEDIATE losSQLText
            INTO    losStartKeyValue
                   ,losStopKeyValue
                   ,lonNumberOfKeys
                   ,lonNumberOfRows
            USING   pmonNumberOfRows;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Can not execute: "' || losSQLText || '".' );
         END;
      END IF;

--
--    determine number of rows
--
	    IF lonNumberOfKeys != 0
      THEN
        losSQLText := 'SELECT  COUNT(*) FROM '
                    || piosTableName || ' WHERE ' || piosKeyColumn || ' BETWEEN :1 AND :2 ';

        BEGIN
          EXECUTE IMMEDIATE losSQLText
          INTO    lonNumberOfRows
          USING   losStartKeyValue
                 ,losStopKeyValue;

        EXCEPTION
          WHEN OTHERS
          THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
          END;
      ELSE
        lonNumberOfRows := 0;
      END IF;

      -- Return output values
      poosStartKeyValue := losStartKeyValue;
      poosStopKeyValue  := losStopKeyValue;
      poonNumberOfKeys  := lonNumberOfKeys;
      pmonNumberOfRows  := lonNumberOfRows;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetKeyRange' );
   END GetKeyRange;

   /*
   ** =====================================================================
   **
   ** Procedure: GetKeyValues
   **
   ** Purpose: Determine the key ranges of a parallel migration.
   **
   ** =====================================================================
   */
   PROCEDURE GetKeyValues
   (
       -- Table name
       piosTableName IN DabUtil.tosObjectName

       -- Key column
      ,piosKeyColumn IN DabUtil.tosAttributeName

       -- Number of parallel steps
      ,pionMigParallelNum IN INTEGER

       -- Total number of rows
      ,poonTotalRowNum  OUT INTEGER

       -- Array of key values
      ,poarKeyValues OUT DabUtil.tarKeyValues

   )
   IS
      TYPE tocRangeQuery    IS REF CURSOR;
      locRangeQuery    tocRangeQuery;
      losSQLText       DabUtil.tosSQLText;
      lonProcessNumber INTEGER;
      losKeyMaxValue   DabUtil.tosKeyValue;
      lonNumberOfKeys  INTEGER;
      lonNumberOfRows  INTEGER;
      lonArrayIndex    BINARY_INTEGER;
      lonTotalRowNum   INTEGER;
      larKeyValues     DabUtil.tarKeyValues;
   BEGIN

      ------------------------------------------------------------------
      -- Validate input parameters
      IF piosTableName IS NULL
      OR piosKeyColumn IS NULL
      OR pionMigParallelNum IS NULL
      OR pionMigParallelNum <= 0
      THEN
         CallError( 'Invalid parameters' );
      END IF;

      ------------------------------------------------------------------
      -- Get total number of rows
      losSQLText := 'SELECT COUNT(*) FROM ' || piosTableName;
      BEGIN
         EXECUTE IMMEDIATE losSQLText
         INTO    lonTotalRowNum;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      ------------------------------------------------------------------
      -- Check number of rows
      IF lonTotalRowNum < pionMigParallelNum
      THEN
         poonTotalRowNum := lonTotalRowNum;
         RETURN;
      END IF;

      ------------------------------------------------------------------
      -- Define range query
      losSQLText :=
'SELECT  PROCESS_NUMBER
        ,MAX( KEY_VALUE)
        ,COUNT(DISTINCT KEY_VALUE)
        ,COUNT (KEY_VALUE)
FROM
(
   SELECT  CEIL( ( :1 * ROW_NUMBER() OVER ( ORDER BY ' || piosKeyColumn || ' ) ) / :2 ) PROCESS_NUMBER
          ,' || piosKeyColumn || ' KEY_VALUE
   FROM    ' || piosTableName || '
)
GROUP BY PROCESS_NUMBER ORDER BY PROCESS_NUMBER';

      ------------------------------------------------------------------
      -- Open cursor
      BEGIN
         OPEN locRangeQuery
         FOR losSQLText
         USING pionMigParallelNum, lonTotalRowNum;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Can not open cursor for: "' || losSQLText || '".' );
      END;

      ------------------------------------------------------------------
      -- Fetch start and stop key from the table to be migrated.

      lonArrayIndex := 0;

      LOOP
         BEGIN
            FETCH locRangeQuery
            INTO  lonProcessNumber
                 ,losKeyMaxValue
                 ,lonNumberOfKeys
                 ,lonNumberOfRows;
            EXCEPTION
               WHEN OTHERS
               THEN
                    CallError( 'Fetch from query: ' || losSQLText );
            END;

         EXIT WHEN locRangeQuery%NOTFOUND;

         lonArrayIndex := lonArrayIndex + 1;

         IF lonProcessNumber != lonArrayIndex
         THEN
            CallError( 'Internal: Range query is not well defined.' );
         END IF;

         larKeyValues( lonArrayIndex ).sKeyMaxValue := losKeyMaxValue;
         larKeyValues( lonArrayIndex ).nNumberOfKeys := lonNumberOfKeys;
         larKeyValues( lonArrayIndex ).nNumberOfRows := lonNumberOfRows;
      END LOOP;

      CLOSE locRangeQuery;
      IF   lonArrayIndex  != pionMigParallelNum
      THEN
            CallError( 'Internal: Range query is not well defined.' );
      END IF;

      -- Return output values
      poonTotalRowNum := lonTotalRowNum;
      poarKeyValues := larKeyValues;

   EXCEPTION
      WHEN OTHERS
      THEN
         IF locRangeQuery%ISOPEN
         THEN
            CLOSE locRangeQuery;
         END IF;
         RaiseError( 'GetKeyValues' );
   END GetKeyValues;

   /*
   ** =====================================================================
   **
   ** Procedure: GetTranferStatement
   **
   ** Purpose: Build the SQL statement for the data transfer method.
   **          The SQL statement is of the form:
   **          INSERT INTO TargetTable  ()
   **          SELECT .. FROM SourceTable
   **
   ** =====================================================================
   */
   PROCEDURE GetTransferStatement
   (
       -- Target table name
       piosTargetTableName IN DabUtil.tosObjectName

       -- Schema of the target table
      ,piosTargetSchema IN DabUtil.tosSchema

       -- Source table name
      ,piosSourceTableName IN DabUtil.tosObjectName

       -- Schema of the source table
      ,piosSourceSchema IN DabUtil.tosSchema

       -- Name of the database link from the target to the source database
      ,piosSourceDBLink IN DabUtil.tosDBLink

       -- Where clause added to the SQL statement
      ,piasWhereClause  IN DabUtil.tasText

       -- Transfer statement: INSERT INTO ... SELECT FROM ...
      ,poasTransferStatement  OUT DabUtil.tasText

       -- Informational message
      ,poasMessageText  OUT DabUtil.tasText
   )
   IS
      TYPE tocTableQuery    IS REF CURSOR;
      locTableQuery    tocTableQuery;

      lasSQLText       DabUtil.tasText;
      lasMessage       DabUtil.tasText;

      losSQLText         DabUtil.tosSQLText;
      losIndentation   VARCHAR2(8);
      losSourceTable   DabUtil.tosDBLink;

      lasTargetColumn tasColumnName;
      lasTargetDataType   tasDataType;
      lanTargetDataLength tanDataLength;
      lasTargetNullable   tasNullable;

      lasSourceColumn tasColumnName;
      lasSourceDataType   tasDataType;
      lanSourceDataLength tanDataLength;
      lasSourceNullable   tasNullable;

      lanTargetMap        tanArrayIndex;
      lanTargetGap        tanArrayIndex;
      lanSourceGap        tanArrayIndex;

      lonTargetColumnCount INTEGER := 0;
      lonSourceColumnCount INTEGER := 0;

      lonTargetIndex     BINARY_INTEGER;
      lonTargetMapIndex  BINARY_INTEGER;
      lonTargetGapIndex  BINARY_INTEGER;
      lonSourceIndex     BINARY_INTEGER;
      lonSourceGapIndex  BINARY_INTEGER;

      lobContinue BOOLEAN;
   BEGIN

      ------------------------------------------------------------------
      -- Initialize text arrays for SQL statement and message text
      lasSQLText :=  DabUtil.tasText();
      lasMessage :=  DabUtil.tasText();

      ------------------------------------------------------------------
      -- Set source table name (local or remote)
      IF piosSourceDBLink IS NULL
      THEN
         losSourceTable := piosSourceTableName;
      ELSE
         losSourceTable := piosSourceTableName || '@' || piosSourceDBLink;
      END IF;

      CallInfo( 'Insert into ' || piosTargetTableName || ' select from ' || losSourceTable || cosNewline);

      ------------------------------------------------------------------
      -- Get column list of the target table
      BEGIN
         SELECT  COLUMN_NAME
                ,DATA_TYPE
                ,DATA_LENGTH
                ,NULLABLE
         BULK COLLECT INTO  lasTargetColumn
                           ,lasTargetDataType
                           ,lanTargetDataLength
                           ,lasTargetNullable
         FROM    DBA_TAB_COLUMNS
         WHERE OWNER = piosTargetSchema
         AND   TABLE_NAME = piosTargetTableName
         ORDER BY COLUMN_NAME;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Can select from DBA_TAB_COLUMNS for target.' );
      END;
      lonTargetColumnCount := SQL%ROWCOUNT;
      CallInfo( 'Number of target columns = ' || lonTargetColumnCount );

      ------------------------------------------------------------------
      -- Raise an error if the query returned now rows.
      -- We assume that the table or view exists exactly with the name
      -- contained in piosTargetTableName. Synonyms have to be resolved by
      -- the calling module.
      IF lonTargetColumnCount = 0
      THEN
         CallError( 'Target table does not exists' );
      END IF;

      ------------------------------------------------------------------
      -- Get column list of the source table (local or remote).
      IF piosSourceDBLink IS NULL
      THEN
         ---------------------------------------------------------------
         -- Use local data dictonary.
         BEGIN
            SELECT  COLUMN_NAME
                   ,DATA_TYPE
                   ,DATA_LENGTH
                   ,NULLABLE
            BULK COLLECT INTO  lasSourceColumn
                              ,lasSourceDataType
                              ,lanSourceDataLength
                              ,lasSourceNullable
            FROM    DBA_TAB_COLUMNS
            WHERE OWNER = piosSourceSchema
            AND   TABLE_NAME = piosSourceTableName
            ORDER BY COLUMN_NAME;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Can select from DBA_TAB_COLUMNS for source.' );
         END;
         lonSourceColumnCount := SQL%ROWCOUNT;
      ELSE
         ---------------------------------------------------------------
         -- Get the column specification from the data dictonary of the
         -- remote database.
         losSQLText :='
SELECT  COLUMN_NAME
       ,DATA_TYPE
       ,DATA_LENGTH
       ,NULLABLE
FROM    DBA_TAB_COLUMNS@'  || piosSourceDBLink || '
WHERE OWNER = :1
AND   TABLE_NAME = :2
ORDER BY COLUMN_NAME';

         ---------------------------------------------------------------
         -- We use a dynamic cursor and loop explicitely through the
         -- result set. Oracle 9 supports bulk selects in dynamic SQL but
         -- 8.i does not. (There is a trick for 8.i described in ch. 8 of
         -- the Applications Developer Guide, but I found this method instable.)
         BEGIN
            OPEN   locTableQuery
            FOR    losSQLText
            USING  piosSourceSchema
                  ,piosSourceTableName;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Open cursor for "' || losSQLText || '".' );
         END;

         lonSourceIndex := 1;
         LOOP
            BEGIN
               FETCH  locTableQuery
               INTO   lasSourceColumn(lonSourceIndex)
                     ,lasSourceDataType(lonSourceIndex)
                     ,lanSourceDataLength(lonSourceIndex)
                     ,lasSourceNullable(lonSourceIndex);
            EXCEPTION
               WHEN OTHERS
               THEN
                  CallError( 'Fetch from cursor: "' || losSQLText || '".' );
            END;
            EXIT WHEN locTableQuery%NOTFOUND;
            lonSourceIndex := lonSourceIndex + 1;
         END LOOP;
         lonSourceColumnCount := locTableQuery%ROWCOUNT;

         BEGIN
            CLOSE locTableQuery;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Close cursor: "' || losSQLText || '".' );
         END;

      END IF;

      CallInfo( 'Number of source columns = ' || lonSourceColumnCount );

      ------------------------------------------------------------------
      -- Raise an error, if the query abouve returned no rows.
      IF lonSourceColumnCount = 0
      THEN
         CallError( 'Source table does not exists' );
      END IF;

      ------------------------------------------------------------------
      -- Compare the two arrays of column specifications.
      -- We scan the columns of the target table. For every column of
      -- the target table we look in a loop, whether the column also exists
      -- in the source table. In the latter case the column will be transferred.
      -- We can assume that both column list are sorted in the same order.
      -- (which in fact depends only on the sort order defined for this session
      -- in the local database, NLS_SORT)
      -- The indexes of all target columns that are to be selected from
      -- the source table are stored in the array lanTargetMap.
      BEGIN
         lonTargetIndex := lasTargetColumn.FIRST;
         lonSourceIndex := lasSourceColumn.FIRST;
         lonTargetMapIndex := 0;
         lonTargetGapIndex := 0;
         lonSourceGapIndex := 0;

         WHILE lonTargetIndex <= lasTargetColumn.LAST
         AND   lonSourceIndex <= lasSourceColumn.LAST
         LOOP
            lobContinue := TRUE;

            -----------------------------------------------------------
            -- It is not necessary to scan all columns of the source table
            -- again and again. A triangular approach is used.
            WHILE lonSourceIndex <= lasSourceColumn.LAST
            AND lobContinue
            LOOP
               IF lasTargetColumn(lonTargetIndex) < lasSourceColumn( lonSourceIndex )
               THEN
                  lonTargetGapIndex := lonTargetGapIndex + 1;
                  lanTargetGap( lonTargetGapIndex ) := lonTargetIndex;
                  lobContinue := FALSE;
               ELSIF lasTargetColumn(lonTargetIndex) = lasSourceColumn( lonSourceIndex )
               THEN
                  lonSourceIndex := lasSourceColumn.NEXT( lonSourceIndex );
                  lonTargetMapIndex := lonTargetMapIndex + 1;
                  lanTargetMap( lonTargetMapIndex ) := lonTargetIndex;
                  lobContinue := FALSE;
               ELSE
                  lonSourceGapIndex := lonSourceGapIndex + 1;
                  lanSourceGap( lonSourceGapIndex ) := lonSourceIndex;
                  lonSourceIndex := lasSourceColumn.NEXT( lonSourceIndex );
               END IF;
            END LOOP;

            lonTargetIndex := lasTargetColumn.NEXT( lonTargetIndex );
         END LOOP;

      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Compare column specifications of source and target table.' );
      END;

      ------------------------------------------------------------------
      -- First add all remaining source columns to the source gap list.
      WHILE lonSourceIndex <= lasSourceColumn.LAST
      LOOP
         lonSourceGapIndex := lonSourceGapIndex + 1;
         lanSourceGap( lonSourceGapIndex ) := lonSourceIndex;
         lonSourceIndex := lasSourceColumn.NEXT( lonSourceIndex );
      END LOOP;

      ------------------------------------------------------------------
      -- Now add all remaining target columns to the target gap list.
      WHILE lonTargetIndex <= lasTargetColumn.LAST
      LOOP
         lonTargetGapIndex := lonTargetGapIndex + 1;
         lanTargetGap( lonTargetGapIndex ) := lonTargetIndex;
         lonTargetIndex := lasTargetColumn.NEXT( lonTargetIndex );
      END LOOP;

      ------------------------------------------------------------------
      -- Check, whether all columns of the source have found a target.
      IF lanTargetGap.COUNT > 0
      THEN
         DabUtil.AddText
         (
             'The following columns of the target table are not found in the source.'
            ,lasMessage
         );

         lonTargetGapIndex := lanTargetGap.FIRST;
         WHILE lonTargetGapIndex <= lanTargetGap.LAST
         LOOP
            DabUtil.AddText
            (
                lasTargetColumn( lanTargetGap(lonTargetGapIndex ) )
               ,lasMessage
            );
            lonTargetGapIndex := lanTargetGap.NEXT( lonTargetGapIndex );
         END LOOP;
      END IF;

      ------------------------------------------------------------------
      -- Check whether all columns of the source have found a target.
      IF lanSourceGap.COUNT > 0
      THEN
         DabUtil.AddText
         (
             'The following columns of the source table are not found in the target.'
            ,lasMessage
         );

         lonSourceGapIndex := lanSourceGap.FIRST;
         WHILE lonSourceGapIndex <= lanSourceGap.LAST
         LOOP
            DabUtil.AddText
            (
                lasSourceColumn( lanSourceGap( lonSourceGapIndex ) )
               ,lasMessage
            );
            lonSourceGapIndex := lanSourceGap.NEXT( lonSourceGapIndex );
         END LOOP;
      END IF;

      ------------------------------------------------------------------
      -- Build SQL Statement
      DabUtil.AddText
      (
          'INSERT INTO ' || piosTargetTableName  || ' ('
         ,lasSQLText
      );

      losIndentation := '	 ';
      lonTargetMapIndex := lanTargetMap.FIRST;

      WHILE lonTargetMapIndex <= lanTargetMap.LAST
      LOOP
         DabUtil.AddText
         (
             losIndentation || lasTargetColumn( lanTargetMap ( lonTargetMapIndex ) )
            ,lasSQLText
         );
         losIndentation := '	,';
         lonTargetMapIndex := lonTargetMapIndex + 1;
      END LOOP;
      DabUtil.AddText( ') SELECT ' , lasSQLText );

      losIndentation := '	 ';
      lonTargetMapIndex := lanTargetMap.FIRST;

      WHILE lonTargetMapIndex <= lanTargetMap.LAST
      LOOP
         DabUtil.AddText
         (
             losIndentation || lasTargetColumn( lanTargetMap ( lonTargetMapIndex ) )
            ,lasSQLText
         );
         losIndentation := '	,';
         lonTargetMapIndex := lonTargetMapIndex + 1;
      END LOOP;
      DabUtil.AddText( 'FROM ' || losSourceTable , lasSQLText );

      IF piasWhereClause IS NOT NULL
      THEN
         DabUtil.AddText
         (
             piasWhereClause
            ,lasSQLText
         );
      END IF;

      -- Return output values
      poasTransferStatement := lasSQLText;
      poasMessageText := lasMessage;

   EXCEPTION
      WHEN OTHERS
      THEN
         RaiseError( 'GetTransferStatement' );
   END GetTransferStatement;
END MigService;
/

