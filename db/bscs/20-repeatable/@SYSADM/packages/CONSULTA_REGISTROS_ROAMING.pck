create or replace package CONSULTA_REGISTROS_ROAMING is

  -- Author  : JROMERO
  -- Created : 04/10/2018 16:18:56
  -- Purpose : 
  
 PROCEDURE CP_GENERA_ARCHIVO_ROAMING(PV_NUMEROS     VARCHAR2,
                                     PV_FECHA_DESDE VARCHAR2,
                                     PV_FECHA_HASTA VARCHAR2,
                                     PN_RANGO_FECHA NUMBER,
                                     PN_ERROR       OUT NUMBER,
                                     PV_ERROR       OUT VARCHAR2);


end CONSULTA_REGISTROS_ROAMING;
/
create or replace package body CONSULTA_REGISTROS_ROAMING is


 PROCEDURE CP_GENERA_ARCHIVO_ROAMING(PV_NUMEROS     VARCHAR2,
                                     PV_FECHA_DESDE VARCHAR2,
                                     PV_FECHA_HASTA VARCHAR2,
                                     PN_RANGO_FECHA NUMBER,
                                     PN_ERROR       OUT NUMBER,
                                     PV_ERROR       OUT VARCHAR2) IS
                                     
                                     
                                     
  CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2, CV_NOMBRE VARCHAR2) IS
    SELECT VALOR 
     FROM GV_PARAMETROS A
    WHERE A.ID_PARAMETRO = CV_PARAMETRO
     AND  A.NOMBRE = CV_NOMBRE;--RUTA_ARCHIVOS_ROAMING

  LV_QUERY VARCHAR2(4000);
  
  LV_PROGRAMA VARCHAR2(1000):='CONSULTA_REGISTROS_ROAMING.CP_GENERA_ARCHIVO_ROAMING';

  TYPE REFCURSORN_NUMERO IS REF CURSOR;
  LF_REFCURSORN_NUMERO REFCURSORN_NUMERO;
  TYPE REPORTEN_NUMERO IS RECORD(
    LV_DATO VARCHAR2(4000));
  LR_REPORTEN_NUMERO REPORTEN_NUMERO;

  LV_FILE_LOG   SYS.UTL_FILE.FILE_TYPE;
  LV_FILE_DATOS SYS.UTL_FILE.FILE_TYPE;

  LN_FECHA_DESDE VARCHAR2(15);
  LN_FECHA_HASTA VARCHAR2(15);

  VALOR_FECHA_DESDE DATE;
  VALOR_FECHA_HASTA DATE;

  DIFERENCIA_FECHAS NUMBER;
  
  LD_FECHA_INGRESADA DATE;

  lv_campo_cadena   varchar2(4000);
  lv_tabla_roaming  varchar2(1000);
  ln_pos_pipe_desde number;
  ln_pos_pipe_hasta number;

  LV_SECUENCIA      VARCHAR2(250);
  LN_NOMBRE_ARCHIVO VARCHAR(1000);
  
   
  LV_TRAMA_TMP     VARCHAR2(30000);
  LP_FOLIO         VARCHAR2(2000);
  LV_TRAMA         CLOB;
  LB_TRAMA_LLENA   BOOLEAN := FALSE;
  LN_DELIMITADOR   NUMBER;       
  LE_ERROR EXCEPTION;
  
  LV_PARAMETRO VARCHAR2(250);
  LV_DIRECCION      VARCHAR2(50);
  
BEGIN
  

  IF PV_NUMEROS IS NULL THEN
  LV_PROGRAMA:='LOS REGISTROS CONFIGURADOS DE LOS NUMEROS SE ENCUENTRAN NULOS.';
  RAISE LE_ERROR;
  END IF;
  
  OPEN C_PARAMETROS ('DBLINK_RTXPROD','CONSULTA_VOZ_ROAMING');--@BSCS_TO_RTX_LINK'
  FETCH C_PARAMETROS
  INTO LV_PARAMETRO;
  CLOSE C_PARAMETROS;
  
  OPEN C_PARAMETROS ('RUTA_ARCHIVOS_ROAMING','CONSULTA_VOZ_ROAMING');--direccion de ruta de archivos
  FETCH C_PARAMETROS
  INTO LV_DIRECCION;
  CLOSE C_PARAMETROS;
  

  LN_FECHA_DESDE := PV_FECHA_DESDE;
  LN_FECHA_HASTA := PV_FECHA_HASTA;

  IF LN_FECHA_DESDE IS NULL THEN
    LN_FECHA_HASTA := TO_CHAR(SYSDATE, 'YYYYMMDD');
  END IF;

  IF LN_FECHA_DESDE IS NOT NULL AND LN_FECHA_HASTA IS NULL THEN
    LN_FECHA_HASTA := LN_FECHA_DESDE;
    LN_FECHA_DESDE := '';
  END IF;

  IF LN_FECHA_DESDE IS NULL THEN
    SELECT TO_DATE(LN_FECHA_HASTA, 'YYYYMMDD') - PN_RANGO_FECHA
      INTO VALOR_FECHA_DESDE
      FROM DUAL;
    SELECT TO_DATE(LN_FECHA_HASTA, 'YYYYMMDD')
      INTO VALOR_FECHA_HASTA
      FROM DUAL;
  ELSE
    SELECT TO_DATE(PV_FECHA_DESDE, 'YYYYMMDD')
      INTO VALOR_FECHA_DESDE
      FROM DUAL;
    SELECT TO_DATE(PV_FECHA_HASTA, 'YYYYMMDD')
      INTO VALOR_FECHA_HASTA
      FROM DUAL;
  END IF;

  SELECT VALOR_FECHA_HASTA - VALOR_FECHA_DESDE
    INTO DIFERENCIA_FECHAS
    FROM DUAL;

  --LV_TABLA_ROAMING := 'UDR_LT_' || PV_FECHA || '_ROAMING A';
  LV_TABLA_ROAMING := 'UDR_LT_' || TO_CHAR(VALOR_FECHA_DESDE,'YYYYMM')|| '_ROAMING A'||LV_PARAMETRO;

  LV_TRAMA:=PV_NUMEROS;


  FOR I IN 0 .. DIFERENCIA_FECHAS LOOP
    LB_TRAMA_LLENA := FALSE;
    LV_TRAMA:=PV_NUMEROS;
    
    SELECT to_char(SEQ_ARCHIVO_JASPER.nextval, '00000')
      INTO LV_SECUENCIA
      FROM DUAL;
  
  
    LN_NOMBRE_ARCHIVO := 'JASPER_VOICE_T' || TO_CHAR((VALOR_FECHA_DESDE + I), 'YYYYMMDD')||to_char(sysdate,'HH24MISS') || '_EC_' || LV_SECUENCIA || '.dat';
    --JASPER_SMS_T20181003120558_EC_8979.dat
    
    LD_FECHA_INGRESADA:=VALOR_FECHA_DESDE+I;
    
    WHILE NOT LB_TRAMA_LLENA  LOOP
      --SE DESCOMPONE LA TRAMA 
      LN_DELIMITADOR := INSTR(LV_TRAMA, '|', 1);
      LV_TRAMA_TMP   := SUBSTR(LV_TRAMA, 1, LN_DELIMITADOR - 1);
      --------.1..-------
    
      LN_DELIMITADOR := INSTR(LV_TRAMA_TMP, '|', 1);
      LP_FOLIO       := SUBSTR(LV_TRAMA_TMP, 1, LN_DELIMITADOR - 1);
      LV_TRAMA_TMP   := SUBSTR(LV_TRAMA_TMP,
                               LN_DELIMITADOR + 1,
                               LENGTH(LV_TRAMA_TMP) - 1);
    
      LN_DELIMITADOR := INSTR(LV_TRAMA, '|', 1);
      LV_TRAMA_TMP   := SUBSTR(LV_TRAMA, 1, LN_DELIMITADOR - 1);
    
      LV_TRAMA := SUBSTR(LV_TRAMA,
                         LENGTH(LV_TRAMA_TMP) + 2,
                         LENGTH(LV_TRAMA));
      IF LENGTH(LV_TRAMA) > 1 THEN
        
      
        LV_QUERY := 'SELECT ''VOICE'' || ''|'' || ' ||
                    '         DECODE(A.FOLLOW_UP_CALL_TYPE, 1, ''MO'', 2, ''MT'', ''NULL'') || ''|'' || ' ||
                    '         TO_CHAR(A.INITIAL_START_TIME_TIMESTAMP, ''YYYYMMDDHH24MISS'') || ''|'' || ' ||
                    '         A.S_P_PORT_ADDRESS || ''|'' || ''NULL'' || ''|'' || ' ||
                    '         DECODE(A.FOLLOW_UP_CALL_TYPE,  ' ||
                    '                1, ' ||
                    '                A.O_P_NUMBER_ADDRESS, ' ||
                    '                2, ' ||
                    '                A.S_P_PORT_ADDRESS, ' ||
                    '                ''NULL'') || ''|'' || SUBSTR(A.EXPORT_FILE, 3, 7) || ''|'' || ' ||
                    '         A.DURATION_VOLUME DATO ' ||    
                    '   FROM '||lv_tabla_roaming||
                    '   WHERE TRUNC(A.INITIAL_START_TIME_TIMESTAMP) = :1 '||
                    '     AND A.S_P_PORT_ADDRESS = :2 '||
                    '     AND SERVICE_LOGIC_CODE LIKE ''%GSMT11%'' ' ||
                    '      OR SERVICE_LOGIC_CODE LIKE ''%GSMT10%'' ' ||
                    '     AND A.FOLLOW_UP_CALL_TYPE IN (1, 2)';
        
        OPEN LF_REFCURSORN_NUMERO FOR LV_QUERY
          USING LD_FECHA_INGRESADA, LV_TRAMA_TMP;
        LOOP
          FETCH LF_REFCURSORN_NUMERO
            INTO LR_REPORTEN_NUMERO;
          EXIT WHEN LF_REFCURSORN_NUMERO%NOTFOUND;
        
          BEGIN
            IF NOT UTL_FILE.is_open(LV_FILE_DATOS) THEN
               lv_file_datos := sys.utl_file.fopen(LV_DIRECCION,
                                                   LN_NOMBRE_ARCHIVO,
                                                   'w');
            END IF;
            sys.utl_file.put_line(lv_file_datos,
                                  LR_REPORTEN_NUMERO.LV_DATO,TRUE);
                   
          EXCEPTION
            WHEN OTHERS THEN
                 PN_ERROR:=1;
                 LV_PROGRAMA:='Error en la consulta de registros en la tabla roaming.';  
          END;
        END LOOP;
      ELSE
        LB_TRAMA_LLENA := TRUE;
      END IF;
    END LOOP;
    IF UTL_FILE.is_open(LV_FILE_DATOS) THEN
        SYS.UTL_FILE.FCLOSE(LV_FILE_DATOS);
    END IF;   
  END LOOP;
  -- sys.utl_file.fclose(lv_file_log);
  EXCEPTION
    WHEN LE_ERROR THEN
      PN_ERROR := 1;
      PV_ERROR := LV_PROGRAMA;
    WHEN OTHERS THEN
      PN_ERROR := 1;
      PV_ERROR := 'Error: ' || SUBSTR(SQLERRM, 1, 200) || ', ' || LV_PROGRAMA;
end;


end CONSULTA_REGISTROS_ROAMING;
/
