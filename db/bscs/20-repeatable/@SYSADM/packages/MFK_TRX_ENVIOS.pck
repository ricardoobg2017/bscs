create or replace package MFK_TRX_ENVIOS is

  TYPE Gtr_Categ_Cliente IS RECORD(CODE     varchar2(10),
                                   NAMES    varchar2(30),
                                   SEGMENTO varchar2(10));
                                   
  TYPE Gr_Categ_Cliente IS REF CURSOR RETURN Gtr_Categ_Cliente;
  
	TYPE Gt_Categ_Cliente IS TABLE OF Gtr_Categ_Cliente INDEX BY BINARY_INTEGER;
  
  TYPE Gtr_Forma_Pago IS RECORD(CODE varchar2(10),
                                NAMES varchar2(70),
                                grupo varchar2(60));
  TYPE Gr_Forma_Pago IS REF CURSOR RETURN Gtr_Forma_Pago;
	TYPE Gt_Forma_Pago IS TABLE OF Gtr_Forma_Pago INDEX BY BINARY_INTEGER;
  
  TYPE Gtr_Planes IS RECORD(CODE varchar2(10),
                            NAMES varchar2(70),
                            grupo varchar2(60));
  TYPE Gr_Planes IS REF CURSOR RETURN Gtr_Planes;
	TYPE Gt_Planes IS TABLE OF Gtr_Planes INDEX BY BINARY_INTEGER;
  
    
 PROCEDURE MFP_CONFIGURA_ENVIO(pn_idEnvio          mf_envios.idenvio%TYPE,
                               pd_fechaEjecucion   mf_envio_ejecuciones.fecha_ejecucion%TYPE,
                               pn_secuencia        OUT mf_envio_ejecuciones.secuencia%TYPE,
                               pv_msgError         OUT VARCHAR2);
                                 
 
 
 PROCEDURE MFP_CATEGORIZA_CLIENTE(Pt_Categ_Cliente  IN OUT Gt_Categ_Cliente);     
     
 
 PROCEDURE MFP_ELIMINAR_LOGICO(PN_IDENVIO    IN  MF_ENVIOS.IDENVIO%TYPE, 
                              PV_MSG_ERROR  IN  OUT VARCHAR2);                           


END MFK_TRX_ENVIOS;
/
create or replace package body MFK_TRX_ENVIOS is
   
   PROCEDURE MFP_CONFIGURA_ENVIO(pn_idEnvio          mf_envios.idenvio%TYPE,
                                 pd_fechaEjecucion   mf_envio_ejecuciones.fecha_ejecucion%TYPE,
                                 pn_secuencia        OUT mf_envio_ejecuciones.secuencia%TYPE,
                                 pv_msgError         OUT VARCHAR2) IS

    CURSOR c_envios(cn_idEnvio NUMBER) IS
    SELECT e.estado,e.cantidad_mensajes,e.horas_separacion_msn
      FROM mf_envios e
     WHERE e.idenvio = cn_idEnvio;
      
    Cursor C_criterios_x_envio(cn_idEnvio NUMBER) IS
    Select 'X' 
      From mf_criterios_x_envios 
     Where idenvio=cn_idEnvio;  
    
    lc_envios c_envios%ROWTYPE;  
    lb_found       BOOLEAN;
    le_error       EXCEPTION;
    ln_indice      PLS_INTEGER := 1;
    ld_fechaEjecucion   DATE;
    lc_criterios_x_envio C_criterios_x_envio%Rowtype;
    lb_notfound   Boolean;
   
    BEGIN
          OPEN c_envios(pn_idEnvio);
          FETCH c_envios INTO lc_envios;
          lb_found := c_envios%FOUND;
          CLOSE c_envios;
          
          IF NOT lb_found THEN
             pv_msgError := 'El Envio Nro:'||pn_idEnvio||' No Existe o no Fue Encontrado';
             RAISE le_error;
          END IF;
          
          IF lc_envios.estado != 'A' THEN
             pv_msgError := 'El Envio Nro:'||pn_idEnvio||' se encuentra Inactivo';
             RAISE le_error;
          END IF;
          
          Open C_criterios_x_envio(pn_idEnvio);
          Fetch C_criterios_x_envio Into lC_criterios_x_envio;
          lb_notfound:=C_criterios_x_envio%Notfound;
          Close C_criterios_x_envio;
          
          If lb_notfound Then
             pv_msgError := 'Por favor verifique el Envio Nro:'||pn_idEnvio||', no tiene criterios asociados';
             RAISE le_error;
          End If;
          
          ld_fechaEjecucion := pd_fechaEjecucion;
          
          WHILE(lc_envios.cantidad_mensajes  >= ln_indice) LOOP
                mfk_obj_envio_ejecuciones.MFP_INSERTAR(PN_IDENVIO               => pn_idEnvio,
                                                       PD_FECHA_EJECUCION       => ld_fechaEjecucion,
                                                       PD_FECHA_INICIO          => NULL,
                                                       PD_FECHA_FIN             => NULL,
                                                       PV_MENSAJE_PROCESO       => NULL,
                                                       PN_TOT_MENSAJES_ENVIADOS => NULL,
                                                       PN_SECUENCIA             => pn_secuencia,
                                                       PV_MSGERROR              => pv_msgError);
                ln_indice := ln_indice +1;
                
                ld_fechaEjecucion :=  ld_fechaEjecucion  + lc_envios.horas_separacion_msn / 24;                                       
   
          END LOOP;
          COMMIT;
       
   EXCEPTION
     WHEN le_error THEN
        RETURN;
    END MFP_CONFIGURA_ENVIO;

PROCEDURE MFP_CATEGORIZA_CLIENTE(Pt_Categ_Cliente  IN OUT Gt_Categ_Cliente) IS

CURSOR C_CATEG_CLIENTE IS 
 Select Codigo As CODE, Descripcion As NAMES, Segmento From Mf_Categorias_Cliente;
 
Ln_Contador NUMBER;

BEGIN
       Ln_Contador := 1;
       FOR i IN C_CATEG_CLIENTE LOOP
      
        Pt_Categ_Cliente(Ln_Contador).CODE := i.CODE;
        Pt_Categ_Cliente(Ln_Contador).NAMES := i.NAMES;
        Pt_Categ_Cliente(Ln_Contador).SEGMENTO := i.SEGMENTO;
        
        Ln_Contador := Ln_Contador + 1;      
      END LOOP;      
      
    BEGIN
      DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
 EXCEPTION
 WHEN OTHERS THEN
    BEGIN
      DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    Pt_Categ_Cliente(1).CODE := '0';
    Pt_Categ_Cliente(1).NAMES := 'SIN DATOS';
    Pt_Categ_Cliente(1).SEGMENTO := 'SIN DATOS';
END;    

----------
PROCEDURE MFP_ELIMINAR_LOGICO(PN_IDENVIO    IN  MF_ENVIOS.IDENVIO%TYPE, 
                              PV_MSG_ERROR  IN  OUT VARCHAR2) 
AS  
 
 CURSOR C_table_cur(cn_idEnvio number) IS 
     SELECT * FROM MF_ENVIOS E
     WHERE E.IDENVIO = cn_idEnvio; 
     
   Lc_CurrentRow     MF_ENVIOS%ROWTYPE ;
   Le_NoDataDeleted  EXCEPTION;
   LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIOS.MFP_ELIMINAR';
   LV_ERROR VARCHAR2(1000);
   LB_FOUND      BOOLEAN := FALSE;
BEGIN

   PV_MSG_ERROR := NULL;
   OPEN C_table_cur(PN_IDENVIO) ;
     FETCH C_table_cur INTO Lc_CurrentRow ;
     LB_FOUND := C_table_cur%FOUND;
    CLOSE C_table_cur ;
    
    IF NOT LB_FOUND THEN
        LV_ERROR := 'El registro que desea borrar no existe. '||LV_APLICACION;
       raise Le_NoDataDeleted;
    END IF;
    
    MFK_OBJ_ENVIOS.MFP_ELIMINAR_LOGICO(pn_idEnvio,
                                    LV_ERROR);
      
    IF LV_ERROR IS NOT NULL THEN
       raise Le_NoDataDeleted;
    END IF;
  
    COMMIT;
    
  EXCEPTION -- Exception 
   WHEN Le_NoDataDeleted  THEN
      ROLLBACK;
      PV_MSG_ERROR := LV_ERROR; 
   WHEN OTHERS THEN
      ROLLBACK;
      PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
END MFP_ELIMINAR_LOGICO;    
--                                                         

end MFK_TRX_ENVIOS;
/
