CREATE OR REPLACE package cok_api_comisiones is
--=====================================================--
   --    CREADO POR :                        Evelyn A.  Mendoza Burgos
   --    FECHA DE CREACION:           24/10/2007
   --    PROYECTO:                            [1404] COMISION PLANES CORPORATIVOS
   --    MOTIVO:                                 REGISTRO DE CONSUMOS  Y SALDOS PARA EL SEGUNDO Y TERCER PAGO DE COMISI�N.
--=====================================================--

    procedure cop_elimina_pricegroup_consumo (pv_msg_error in out varchar2);
 
    procedure cop_config_pricegroup_consumo (pv_code  in varchar2, pv_msg_error in out varchar2);
       
    procedure cop_eliminar_consumo(pv_msg_error in out varchar2);  
    
    procedure cop_generar_consumo  (pd_fecha_inicio in date,
                                                                     pd_fecha_final   in date,
                                                                     pv_msg_error  in out varchar2) ;
                                                      
end cok_api_comisiones;
/
CREATE OR REPLACE package body cok_api_comisiones is
--=====================================================--
   --    CREADO POR :                        Evelyn A.  Mendoza Burgos
   --    FECHA DE CREACION:           24/10/2007
   --    PROYECTO:                            [1404] COMISION PLANES CORPORATIVOS
   --    MOTIVO:                                 REGISTRO DE CONSUMOS  Y SALDOS PARA EL SEGUNDO Y TERCER PAGO DE COMISI�N.
--=====================================================--


      -- Procedimiento que elimina los tipos de planes de la tabla co_prgcode_consumos
 procedure cop_elimina_pricegroup_consumo (pv_msg_error in out varchar2) is
      begin
          delete from co_prgcode_consumo_tmp;
      exception
          when others then
               pv_msg_error := 'cok_api_comisiones. Error al eliminar los tipos de planes de la tabla co_prgcode_consumos : '||sqlerrm;
      end;
   

      -- Procedimiento que obtiene las configuraciones de los tipos de planes de la tabla co_prgcode_consumos
 procedure cop_config_pricegroup_consumo (pv_code in varchar2,pv_msg_error in out varchar2 ) is
      begin
           insert into co_prgcode_consumo_tmp values (pv_code);
      exception
          when others then
               pv_msg_error := 'cok_api_comisiones. Error al insertar c�digo de tipos de planes en la tabla co_prgcode_consumos : '||sqlerrm;
      end;

    
      -- Procedimiento que elimina los consumos y saldos registrados  en la tabla temporal
  procedure cop_eliminar_consumo(pv_msg_error in out varchar2) is
      begin
          delete from  co_consumo_cuentas_tmp;
      exception
          when others then
               pv_msg_error := 'cok_api_comisiones. Error al eliminar los datos de la tabla temporal co_consumo_cuentas_tmp: '||sqlerrm;
      end;
   
--========================================================================================--
      -- Procedimiento que verifica la tabla de los consumos creados mensualmente, permite obtener 
      -- los saldos y consumos de las cuentas 
--========================================================================================--      
  procedure cop_generar_consumo (pd_fecha_inicio in date,
                                                                 pd_fecha_final   in date,
                                                                 pv_msg_error  in out varchar2) is
                                                                              
   --========= cursor que obtiene el nombre de la tabla y el ciclo a la que pertenece  =========--    
   cursor c_ciclos (pc_fecha_inicial date, pc_fecha_final date) is
          select  *from  co_billcycles
            where  fecha_emision  between pc_fecha_inicial  and pc_fecha_final;
            

    --======= declaraci�n de variables =========--  
    lv_sentencia1              varchar2(5000);
    lv_sentencia                varchar2(4000);
    lv_erro_mess               varchar2(200);
    lb_found                       boolean := false;
    le_mi_error                  exception;         
    ln_codi_error              number:=0;           
    lv_tipo_erro                 varchar2(1);        
       
      
    begin 
           lv_erro_mess := null;                   
                    -- Llamada al procedimiento que elimina los datos de la tabla temporal
            cok_api_comisiones.cop_eliminar_consumo(lv_erro_mess);
            if lv_erro_mess is not null then 
                raise le_mi_error; 
            end if;

          for i in c_ciclos (pd_fecha_inicio, pd_fecha_final) loop
                lb_found:=c_ciclos%found;                                                                                      
                      begin              
                              ln_codi_error := 0;  
                              lv_erro_mess := null;                    
                              lv_tipo_erro  := null;

                                --==========   Obtenci�n de los consumos y saldos de la cuenta seg�n su corte de facturaci�n   ===============--
                                -- EME [1404] 13/02/2008 
                                -- Se modific� el query q extrae los consumos para que contemple activaciones desde hace 1 a�o atras 
                                -- y no solo de hace 7 meses
                                -- Se a�adi� nvl a los campos para que en caso de null no afecte el query.
                              lv_sentencia1:= 'insert into co_consumo_cuentas_tmp ';
                              lv_sentencia  := 'select  c.custcode cuenta ,'||
                                                              'to_date('''||to_char(i.fecha_emision,'dd-mm-yyyy')||''',''dd-mm-yyyy'')   fecha_corte '||
                                                              ',nvl(balance_12 - inter_internacional - inter_intra_reg_inter_reg - inter_local - iva - ice,0)  consumo '||
                                                              ',nvl(r.inter_internacional ,0)  internacional '||
                                                              ',nvl(r.inter_intra_reg_inter_reg ,0)  interegional '||
                                                              ',nvl(r.inter_local,0)   interlocal '||
                                                              ',nvl(r.iva,0)   iva  '||
                                                              ',nvl(r.ice,0)  ice  '||
                                                              ',nvl(r.totalvencida,0)  valor_vencido '||
                                                              ',nvl(r.balance_12,0)   valor_actual '||
                                                              ',r.fech_max_pago  fecha_pago ' ||
                                                              ',null completo'||
                                                              ',sysdate   fecha_registro ' ||                            
                                                          'from customer_all c ,  fa_ciclos_axis_bscs b , '||i.tabla_rep_detcli ||'  r 
                                                          where c.prgcode in ( select prgcode from co_prgcode_consumo_tmp) '||
                                                          '/*   EME [1404] 13/02/2008 and c.csactivated >=   add_months(to_date( '''|| pd_fecha_inicio ||''', ''dd-mm-yyyy''), -12)*/ '||                                                          
                                                          'and c.csactivated >=   add_months(to_date( '''|| pd_fecha_inicio ||''', ''dd-mm-yyyy''), -12) '||
                                                          'and c.customer_id_high is null '||
                                                          'and c.paymntresp = ''X'' '||
                                                          'and b.id_ciclo_admin = c.billcycle '||
                                                          'and r.cuenta = c.custcode';        
        
                              lv_sentencia1 := lv_sentencia1 || lv_sentencia;    

                              execute immediate  lv_sentencia1;   
                              
                      exception
                                        when others then 
                                               ln_codi_error := 1;   
                                               lv_erro_mess :=  ' Error al ejecutar la sentencia:  '|| sqlerrm;                                            
                                               lv_tipo_erro  := 'W';   -- WARNING
                                                  begin   
                                                          insert into co_inconsistencias(
                                                                             nombre_tabla
                                                                            ,nomb_proc
                                                                            ,fech_inic
                                                                            ,fech_fina
                                                                            ,codi_erro
                                                                            ,mesg_erro
                                                                            ,tipo_error
                                                                            ,fech_carga                                                
                                                                            )   
                                                                            values   
                                                                            (
                                                                             i.tabla_rep_detcli,   
                                                                            'COK_API_COMISIONES.COP_GENERAR_CONSUMO',
                                                                             pd_fecha_inicio,   
                                                                             pd_fecha_final,   
                                                                             ln_codi_error,   
                                                                             lv_erro_mess,   
                                                                             lv_tipo_erro,   
                                                                             sysdate   
                                                                            );   
                                                  exception   
                                                         when others then   
                                                                 null;   
                                                  end;                                          
                        end;                            
          end loop; 

         if  not lb_found  then
                      lv_erro_mess := 'Cok_api_comisiones. No existen configuraciones en co_billcycles para generar consumo en la fecha_emision: '||pd_fecha_inicio;           
                      raise le_mi_error;
         end if;
                           
    exception
            when le_mi_error then
                   ln_codi_error := 2;    -- NO ENCONTRO DATOS EN  CO_BILLCYCLES
                   pv_msg_error := lv_erro_mess;
                   lv_tipo_erro:= 'E';                                            
                   begin   
                              insert into co_inconsistencias(
                                                 nombre_tabla
                                                ,nomb_proc
                                                ,fech_inic
                                                ,fech_fina
                                                ,codi_erro
                                                ,mesg_erro
                                                ,tipo_error
                                                ,fech_carga                                                
                                                )   
                                                values   
                                                ( 
                                                'GENERAL '||ln_codi_error,   
                                                'COK_API_COMISIONES.COP_GENERAR_CONSUMO',
                                                 pd_fecha_inicio,   
                                                 pd_fecha_final,   
                                                 ln_codi_error,   
                                                 pv_msg_error,   
                                                 lv_tipo_erro,   
                                                 sysdate   
                                                );   
                      exception   
                             when others then   
                                     null;   
                      end; 
                   
            when others then
                    ln_codi_error := 3;      
                    pv_msg_error := 'cok_api_comisiones. Error general: '|| sqlerrm;                                                  
                    lv_tipo_erro:= 'E';
                   begin   
                              insert into co_inconsistencias(
                                                 nombre_tabla
                                                ,nomb_proc
                                                ,fech_inic
                                                ,fech_fina
                                                ,codi_erro
                                                ,mesg_erro
                                                ,tipo_error
                                                ,fech_carga                                                
                                                )   
                                                values   
                                                (
                                                'GENERAL '||ln_codi_error,      
                                                'COK_API_COMISIONES.COP_GENERAR_CONSUMO',
                                                 pd_fecha_inicio,   
                                                 pd_fecha_final,   
                                                 ln_codi_error,   
                                                 pv_msg_error,   
                                                 lv_tipo_erro,   
                                                 sysdate   
                                                );   
                      exception   
                             when others then   
                                     null;   
                      end;                        
    end;                                                                     
end cok_api_comisiones;
/
