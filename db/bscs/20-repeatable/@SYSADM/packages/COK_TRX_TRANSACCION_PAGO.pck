create or replace package COK_TRX_TRANSACCION_PAGO is

FUNCTION COF_JOURNAL_TICKET(PV_FECHA     IN date,
                          PV_CUSTCODE    IN VARCHAR2,
                          PV_CAUSERNAME  IN VARCHAR2,
                          PV_CAREM       IN VARCHAR2,
                          PV_CAREM1       IN VARCHAR2) RETURN VARCHAR2;
                          
FUNCTION CLF_OBTIENE_PARAMETROS(PV_ID_PARAMETRO VARCHAR2) RETURN VARCHAR2;

FUNCTION DEVUELVE_VALOR(PV_RESPUESTA IN CLOB,
                          PV_TAG1      IN VARCHAR2,
                          PV_TAG2      IN VARCHAR2) RETURN VARCHAR2;
FUNCTION DEVUELVE_VALOR_PADRE(TRAMA     IN CLOB,
                                VALOR     IN VARCHAR2,
                                APARICION IN NUMBER) RETURN clob;
end COK_TRX_TRANSACCION_PAGO;
/
create or replace package body COK_TRX_TRANSACCION_PAGO is

  FUNCTION COF_JOURNAL_TICKET(PV_FECHA   IN date,
                          PV_CUSTCODE    IN VARCHAR2,
                          PV_CAUSERNAME  IN VARCHAR2,
                          PV_CAREM       IN VARCHAR2,
                          PV_CAREM1       IN VARCHAR2) RETURN VARCHAR2 IS

  LV_TICKET            VARCHAR2(150);
  LV_DATA_SOURCE       VARCHAR2(100);
  lv_ServicioInforma   VARCHAR2(100);
  LV_ID_REQ            VARCHAR2(100);
  LV_CADENA_PARAMETROS VARCHAR2(4000);
  LV_FAULT_STRING      VARCHAR2(500);
  LV_FAULT_CODE        VARCHAR2(100);
  Lx_Response          SYS.xmltype;
  Ln_Trama_Respuesta   NUMBER;
  Lv_Trama_Respuesta   CLOB;
  Lv_Error             varchar2(2500);
  Le_Error             exception;
  Lv_AUX_TRAM          varchar2(150);
  Ln_contador          number:='1';
  formatos             varchar2(1500);
  ln_proceso           number;
  LV_PROCESO           VARCHAR2(120);
  lv_canal             varchar2(20);
  --0 cuando error en el jeis consumo
  -- null cuando no existe el proceso: "PV_CAREM"
  BEGIN
  IF PV_CAUSERNAME='PRCKSKVE'THEN
  formatos := 'alter session set nls_date_format =''MM/DD/YYYY''';
  EXECUTE IMMEDIATE formatos;
  
  SELECT INSTR(PV_CAREM,'PROCESO:') INTO ln_proceso FROM DUAL; 

  IF ln_proceso <> 0 THEN
    SELECT TRIM(SUBSTR(PV_CAREM, 
     INSTR(PV_CAREM,'PROCESO:')+8))id_proceso INTO LV_PROCESO FROM dual;
     
   lv_canal := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(11526,'CANAL');
  
   
  LV_DATA_SOURCE     := CLF_OBTIENE_PARAMETROS('JEIS_DAT_JOUR');
  lv_ServicioInforma := CLF_OBTIENE_PARAMETROS('JEIS_IDINFORM__JOUR');
  LV_ID_REQ          := CLF_OBTIENE_PARAMETROS('JEIS_IDREQUER__JOUR');
  
  LV_CADENA_PARAMETROS := 'dsId='||LV_DATA_SOURCE||
                          ';pnIdServicioInformacion='||LV_SERVICIOINFORMA ||
                          ';pvParametroBind1='||PV_FECHA||
                          ';pvParametroBind2='||PV_CUSTCODE||
                          ';pvParametroBind3='||PV_CAUSERNAME||
                          ';pvParametroBind4='||LV_PROCESO||
                          ';pvParametroBind5='||lv_canal;
                          
  Lx_Response := scp_dat.sck_soap_gtw.scp_consume_servicio(PN_ID_REQ                   => LV_ID_REQ,
                                                             PV_PARAMETROS_REQUERIMIENTO => LV_CADENA_PARAMETROS,
                                                             PV_FAULT_CODE               => LV_FAULT_CODE,
                                                             PV_FAULT_STRING             => LV_FAULT_STRING);
    IF LV_FAULT_STRING IS NOT NULL THEN
      --Valida si hubo error en consumo de webservices
      Lv_Error := 'Error en el consumo de WebServices GYE' ||
                  Lv_Fault_String || ' - ' || Lv_Fault_Code;
      RAISE Le_Error;
    end if; 
                                                    
        Ln_Trama_Respuesta := scp_dat.sck_soap_gtw.SCP_OBTENER_VALOR(PN_ID_REQ           => LV_ID_REQ,
                                                                 PR_RESPUESTA        => Lx_Response,
                                                                 PV_NOMBRE_PARAMETRO => 'pnerrorOut',
                                                                 PV_NAMESPACE        => NULL);
                                                                 
    IF LN_TRAMA_RESPUESTA = 0 THEN
      Lv_Trama_Respuesta := LX_RESPONSE.GETCLOBVAL();
      Lv_Trama_Respuesta := LX_RESPONSE.GETCLOBVAL(); --se descompone la trama
      Lv_Trama_Respuesta := LOWER(Lv_Trama_Respuesta);
      Lv_Trama_Respuesta := REPLACE(Lv_Trama_Respuesta, 'ns0:', '');
      Lv_Trama_Respuesta := REPLACE(Lv_Trama_Respuesta, 'env:', '');
      LX_RESPONSE        := XMLTYPE(Lv_Trama_Respuesta);
    
      Lv_Trama_Respuesta := scp_dat.sck_soap_gtw.SCP_OBTENER_VALOR(PN_ID_REQ           => LV_ID_REQ,
                                                                   PR_RESPUESTA        => Lx_Response,
                                                                   PV_NOMBRE_PARAMETRO => 'result',
                                                                   PV_NAMESPACE        => NULL);
                                                                   
      Lv_Trama_Respuesta := LX_RESPONSE.GETCLOBVAL(); --se descompone la trama    
    ELSE
      LV_ERROR := 'Error al consumir el JEIS de la plataforma GYE - Financiero';
      RAISE LE_ERROR;
    END IF;

     Lv_AUX_TRAM        := devuelve_valor_padre(Lv_Trama_Respuesta,
                                               'result',
                                               Ln_contador);
    
   LV_TICKET := DEVUELVE_VALOR(Lv_AUX_TRAM, '<pvresultadoout>', '</pvresultadoout>');
   
   LV_TICKET := REPLACE(LV_TICKET, '&quot;', '"');
   
   END IF;     
   END IF;
      RETURN LV_TICKET;
EXCEPTION
    WHEN lE_Error then
      return 0;
    WHEN OTHERS THEN
      lv_error:=sqlerrm;
      RETURN 0;
  END COF_JOURNAL_TICKET;
  
   FUNCTION CLF_OBTIENE_PARAMETROS(PV_ID_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS
  
    LV_PROCESO_PAR_SCP_P     VARCHAR2(30);
    LV_VALOR_PAR_SCP_P       VARCHAR2(160);
    LV_DESCRIPCION_PAR_SCP_P VARCHAR2(500);
    LN_ERROR_SCP_P           NUMBER := 0;
    LV_ERROR_SCP_P           VARCHAR2(500);
  
  BEGIN
    --para gestion de procesos
    SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(PV_ID_PARAMETRO,
                                                LV_PROCESO_PAR_SCP_P,
                                                LV_VALOR_PAR_SCP_P,
                                                LV_DESCRIPCION_PAR_SCP_P,
                                                LN_ERROR_SCP_P,
                                                LV_ERROR_SCP_P);
    IF LN_ERROR_SCP_P = 0 THEN
      RETURN LV_VALOR_PAR_SCP_P;
    END IF;
    RETURN NULL;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END CLF_OBTIENE_PARAMETROS;
  
  FUNCTION DEVUELVE_VALOR(PV_RESPUESTA IN CLOB,
                          PV_TAG1      IN VARCHAR2,
                          PV_TAG2      IN VARCHAR2) RETURN VARCHAR2 IS
    LN_TOPE      NUMBER;
    LV_RESULTADO clob;
  BEGIN
  
    SELECT LENGTH(PV_TAG1) INTO LN_TOPE FROM DUAL;
    SELECT SUBSTR(PV_RESPUESTA,
                  (INSTR(PV_RESPUESTA, PV_TAG1) + LN_TOPE),
                  (INSTR(PV_RESPUESTA, PV_TAG2) -
                  (INSTR(PV_RESPUESTA, PV_TAG1) + LN_TOPE)))
      INTO LV_RESULTADO
      FROM DUAL;
  
    RETURN LV_RESULTADO;
  END;
  FUNCTION DEVUELVE_VALOR_PADRE(TRAMA     IN CLOB,
                                VALOR     IN VARCHAR2,
                                APARICION IN NUMBER) RETURN clob IS
    RESULTS     clob;
    LN_POSICION NUMBER := 0;
    LN_LONGITUD NUMBER := 0;
  BEGIN
    LN_POSICION := INSTR((TRAMA), '<' || (VALOR) || '>', 1, APARICION) +
                   LENGTH('<' || (VALOR) || '>');
  
    LN_LONGITUD := INSTR((TRAMA), '</' || (VALOR) || '>', 1, APARICION) -
                   LN_POSICION;
  
    RESULTS := SUBSTR(TRAMA, LN_POSICION, LN_LONGITUD);
  
    RETURN(RESULTS);
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
    
  END DEVUELVE_VALOR_PADRE;
end COK_TRX_TRANSACCION_PAGO;
/
