CREATE OR REPLACE PACKAGE GSI_PLANESQC IS

 --======================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 14793 Automatizacion de Planes QC. 
  -- Fecha         : 02/10/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizar el proceso de planes QC. para uso de Billing
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 14793 Automatizacion de Planes QC. 
  -- Fecha         : 16/12/2015 -18/12/2015 - 23/12/2015 - 06/01/2016
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Se agregan nuevos QC al proceso QC. Planes
--======================================================================================--

  FUNCTION PLF_PARAMETROS_QC(PV_COD_PARAMETRO VARCHAR2)RETURN VARCHAR2 ;
  
  PROCEDURE PLP_BITACORIZA_LOGPLANES_QC (PV_COD_PROCESO IN VARCHAR2,
                                         PV_TRANSACCION IN VARCHAR2, 
                                         PV_ESTADO      IN VARCHAR2,
                                         PV_OBSERVACION IN VARCHAR2,
                                         PV_USUARIO     IN VARCHAR2,
                                         PV_ERROR       OUT VARCHAR2);
  
  --Proceso que obtiene la tabla de tarifas anterior
  PROCEDURE PLP_TABLAS_TARIFAS_ANT_QC(PD_CIERRE_ACTUAL   IN DATE,
                                      PD_CIERRE_ANTERIOR OUT DATE,
                                      PV_ERROR           OUT VARCHAR2);
  --                                    
  PROCEDURE PLP_CONSULTA_CICLO_QC (PD_CIERRE_ACTUAL  OUT DATE,
                                   PV_ERROR          OUT VARCHAR2);
                                   
  -- Paso 1: Insert a la tabla GSI_PLANES
   PROCEDURE PLP_OBTIENE_PLANES_QC (PV_ERROR         OUT VARCHAR2); 
  
  -- Paso 2: Cuadra_TB_Planes_com_vs_bscs
   PROCEDURE PLP_CUADRA_TB_PLANES_COM_BSCS (PV_ERROR OUT VARCHAR2); 
   
  -- Paso 3: Query_costos_OCC
  PROCEDURE PLP_QUERY_COSTOS_OCC_QC (PV_ERROR        OUT VARCHAR2);  
   
  -- Paso 4: Revisar_Features_Consistentes_con_el_producto
  PROCEDURE PLP_TOLL_INTERCONEXION_QC (PV_ERROR      OUT VARCHAR2);
  
  -- Paso 5:  Revisar_Features_Consistentes_con_el_producto
  PROCEDURE PLP_FEATURES_CONSISTENTES_QC (PV_ERROR   OUT VARCHAR2); 

  -- Paso 6:  Diferencia de Tarifas en TN3
  PROCEDURE PLP_TARIFAS_TN3_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                                PV_ERROR        OUT VARCHAR2);
 
  -- Paso 7:  Diferencia de Tarifas en BSCS
  PROCEDURE PLP_TARIFAS_BSCS_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                                 PV_ERROR       OUT VARCHAR2); 
  
  -- Paso 8: Costos_Features_BSCS_vs_CuotaMensual
  PROCEDURE PLP_COSTOS_FEATURES_QC (PV_ERROR       OUT VARCHAR2);
 
  -- Paso 9: Revisa_Occ_No_Asociados_BSCS
  PROCEDURE PLP_OCC_NOASOCIADOS_QC (PV_ERROR OUT VARCHAR2);
 
  -- Paso 10: Diferentes_profiles_oper_axis
  PROCEDURE PLP_PROFILES_OPER_QC (PV_ERROR OUT VARCHAR2);
 
  -- Paso 11: Valida_Nombres_Incorrectos_en_Plan
  PROCEDURE PLP_PLAN_INCORRECTOS_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 12: Verificar_Servicios_entre_tablas_BSCS
  PROCEDURE PLP_VERIFICA_SERVICIOS (PV_ERROR OUT VARCHAR2);
  
  -- Paso 13: Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF
  PROCEDURE PLP_APN_GENERICAS_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 14: Concilia_mk_mapea_plan_ext
  PROCEDURE PLP_CONCILIA_PLAN_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 15: Revisa features m2m creados como MMS
  PROCEDURE PLP_FEATURE_M2M2_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 16: Revisa Profiles negativos_planes
  PROCEDURE PLP_PROFILES_NEGATIVOS_PLAN_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 17: Revisa Profiles negativos_features
  PROCEDURE PLP_PROFILES_NEGATIVOS_FEAT_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 18: Cuadra_TB_Planes_com_vs_bscs_Maxima_version_bscs
  PROCEDURE PLP_TB_PLANES_COM_BSCS_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 19: Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF_TAR
  PROCEDURE PLP_APN_GENERICAS_TAR_QC (PV_ERROR OUT VARCHAR2);
  
  -- Paso 20 VerificaFeatures_PCRF en cl_internet_wap
 PROCEDURE PLP_FEATURES_PCRF_QC (PV_ERROR OUT VARCHAR2);
  --Proceso principal
  PROCEDURE PLP_PRINCIPAL_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                              PV_ERROR OUT VARCHAR2);
  
  --Ejecuta los todos los pasos planes QC.
  PROCEDURE PLP_ESCENARIOS_PLANES_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2);
  
  --Genera el archivo excel
  PROCEDURE PLP_GENERERA_ARCHIVO_QC (PV_ARCHIVO      OUT VARCHAR2,
                                     PV_ERROR        OUT VARCHAR2);
                
  --Transfiere el archivo al servidor FINAN_27                                 
  PROCEDURE PLP_TRANSFIERE_ARCHIVO_QC (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                       PN_ERROR           OUT NUMBER,
                                       PV_ERROR           OUT VARCHAR2);
   
  -- Envio de mail con correo adjunto 
  PROCEDURE PLP_ENVIA_REPORTE_MAIL_QC (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2);                            
  --Limpio la tabla de bitacora y la respaldo en una TMP
  PROCEDURE PLP_LIMPIA_TABLA_QC (PV_ERROR OUT VARCHAR2);
  
  
  --
  TYPE GR_DIST_CUADRA_TBPLANES IS RECORD (ID_PLAN              VARCHAR2(10),
                                          OBSERVACION          VARCHAR2(350),
                                          TARIFA_BASICA_COM    VARCHAR2(300),
                                          TARIFA_BSCS          FLOAT,
                                          TARIFA_BASICA_AXIS  NUMBER(13,2));
                                          
  TYPE GT_DIST_CUADRA_TBPLANES IS TABLE OF GR_DIST_CUADRA_TBPLANES INDEX BY BINARY_INTEGER;
  --
  TYPE GR_DIST_CUADRA_TBDIF IS RECORD (ID_PLAN               VARCHAR2(10),    
                                       OBSERVACION           VARCHAR2(350),
                                       TARIFA_BASICA_COM     FLOAT,
                                       TARIFA_BSCS           FLOAT,
                                       DIFERENCIA_TB_BSCS    FLOAT,
                                       TARIFA_BASICA_AXIS    FLOAT,
                                       DIFERENCIA_TB_AXIS    FLOAT);
  TYPE GT_DIST_CUADRA_TBDIF IS TABLE OF GR_DIST_CUADRA_TBDIF INDEX BY BINARY_INTEGER;
  --
  TYPE GR_DIST_TABLA_TARIFAS IS RECORD (CODIGO                  VARCHAR2(30),
                                        DESCRIPCION             VARCHAR2(350),
                                        CLARO_INPOOL            VARCHAR2(300),
                                        PRECIO_INPOOL           VARCHAR2(300),
                                        DIFERENCIA_INPOOL       VARCHAR2(300),
                                        COSTO_MIN_CLAROACLARO   VARCHAR2(300),
                                        CLARO_CON_VALOR         VARCHAR2(300),
                                        DIFERENCIA_CLARO        VARCHAR2(300),
                                        COSTO_MIN_CLAROAMOVI    VARCHAR2(300),
                                        MOVI_CON_VALOR          VARCHAR2(300),  
                                        DIFERENCIA_MOVISTAR     VARCHAR2(300) , 
                                        COSTO_MIN_CLAROAALEGRO  VARCHAR2(300),
                                        ALEGRO_CON_VALOR        VARCHAR2(300),
                                        DIFERENCIA_ALEGRO       VARCHAR2(300),
                                        COSTO_MIN_CLAROAFIJO    VARCHAR2(300),
                                        LOCAL_CON_VALOR         VARCHAR2(300),
                                        DIFERENCIA_FIJAS        VARCHAR2(300));
  ---
                                                                       
                                     
  TYPE GT_DIST_TABLA_TARIFAS IS TABLE OF GR_DIST_TABLA_TARIFAS INDEX BY BINARY_INTEGER;
  --
  TYPE TVARCHAR     IS TABLE OF VARCHAR2 (10)  INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR_50  IS TABLE OF VARCHAR2 (50)  INDEX BY BINARY_INTEGER;
  TYPE TNUMBER_10   IS TABLE OF NUMBER   (10)  INDEX BY BINARY_INTEGER;
  TYPE TNUMBER      IS TABLE OF NUMBER   (10)  INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR_1   IS TABLE OF VARCHAR2 (1)   INDEX BY BINARY_INTEGER;
  -- Paso 21 : Valida mk_mapea_plan_ext con ID_PLAN_EXT 1 y 8 a la vez
  PROCEDURE PLP_VALIDA_MK_MAPEA_PLAN_EXT(PV_ERROR OUT VARCHAR2);
  
  -- PMERA 18/12/2015
  -- Paso 22 : Verifica planes con promocion de Megas que no se les haya configurado los features en la tabla mk_param_gen_aut
  PROCEDURE PLP_VALIDA_PROMO_MEGAS(PV_ERROR OUT VARCHAR2);
  
  -- Paso 23 : Verifica que el feature que se haya asociado a la promocion de sms sea promocional  o stand comercial
  PROCEDURE PLP_FEATURE_PROMOSMS(PV_ERROR OUT VARCHAR2);
   -- Paso 24 : Verificar Costos Features Cuota Mensual BSCS, Axis vs COM

  PROCEDURE PLP_CUOTA_BSCS_AXIS_COM (PV_ERROR OUT VARCHAR2) ;
  --
  TYPE GR_DIST_PROFILES_NEG_FEAT IS RECORD (PLAN_AXIS     VARCHAR2(300),
                                            DESCRIPCION   VARCHAR2(300),
                                            TIPO          VARCHAR2(10) ,
                                            TIPO_SVA      VARCHAR2(10),
                                            PLAN_TECNOMEN VARCHAR2(100));
  TYPE GT_DIST_PROFILES_NEG_FEAT IS TABLE OF GR_DIST_PROFILES_NEG_FEAT INDEX BY BINARY_INTEGER;                
  --INI PMERA 06/01/2016  --Se agregan nuevos QC 
  TYPE GR_FEATURE_ADICIONAL IS RECORD (ID_TIPO_DETALLE_SERV  VARCHAR2(10),
                                       DESCRIPCION           VARCHAR2(300),
                                       ID_DETALLE_PLAN       VARCHAR2(100),
                                       TIPO_FEATURE          VARCHAR2(100),
                                       OCULTO_ROL            VARCHAR2(10)); 
                                                 
  TYPE GT_FEATURE_ADICIONAL IS TABLE OF GR_FEATURE_ADICIONAL INDEX BY BINARY_INTEGER;                                               
  -- Paso 25 : Verificar que un feature Incluido No se encuentre configurado como Adicional                                               
  PROCEDURE PLP_FEATURE_CONF_ADICIONAL (PV_ERROR OUT VARCHAR2);
  -- Paso 26 : Validar el campo ID_TIPO_PLAN_SUPTEL contra la Base de COM
  PROCEDURE PLP_TIPO_PLAN_SUPTEL (PV_ERROR OUT VARCHAR2);     
  
  --FIN PMERA 06/01/2016  --Se agregan nuevos QC  
  --INI PMERA 25/01/2016  --Se agregan nuevos QC 
  -- Paso 27 : Valida el campo TIPO_PLAN contra la base de COM
   PROCEDURE PLP_VALIDA_TIPO_PLAN (PV_ERROR OUT VARCHAR2);
  --FIN PMERA 25/01/2016  --Se agregan nuevos QC                                                                
END GSI_PLANESQC;
/
CREATE OR REPLACE PACKAGE BODY GSI_PLANESQC IS

 --======================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 14793 Automatizacion de Planes QC. 
  -- Fecha         : 02/10/2015
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Automatizar el proceso de planes QC. para uso de Billing
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [A8] Mejoras Billing - TR 14793 Automatizacion de Planes QC. 
  -- Fecha         : 16/12/2015 - 18/12/2015 - 23/12/2015 - 06/01/2016
  -- Lider Proyecto: SIS Natividad Mantilla
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Se agregan nuevos QC al proceso QC. Planes
--======================================================================================--
 --
 LV_USUARIO              VARCHAR2(50);
 --
 CURSOR TRANSACCION (CN_TIPO_PARAMETRO IN NUMBER, CV_PARAMETRO VARCHAR2) IS
 SELECT T.DESCRIPCION FROM GV_PARAMETROS T
 WHERE T.ID_TIPO_PARAMETRO=CN_TIPO_PARAMETRO
 AND T.ID_PARAMETRO=CV_PARAMETRO;
 
 FUNCTION PLF_PARAMETROS_QC(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS

  --
  CURSOR OBTIENE_PARAMETRO (CN_TIPO_PARAMETRO IN NUMBER,CV_ID_PARAMETRO IN VARCHAR2)IS
  SELECT T.VALOR
    FROM GV_PARAMETROS T
   WHERE T.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
     AND T.ID_PARAMETRO = CV_ID_PARAMETRO;
  -- 
     LV_VALOR_PAR   VARCHAR2(1000);
   
   BEGIN
    
      OPEN OBTIENE_PARAMETRO (14793,PV_COD_PARAMETRO);
      FETCH OBTIENE_PARAMETRO INTO LV_VALOR_PAR;
      CLOSE OBTIENE_PARAMETRO;
     
       RETURN LV_VALOR_PAR;
      
    
        
    EXCEPTION
     WHEN OTHERS THEN
     RETURN NULL;
      
 END PLF_PARAMETROS_QC;
 --Se bitacoriza la ejecucion de los procesos en la tabla de log 
 PROCEDURE PLP_BITACORIZA_LOGPLANES_QC (PV_COD_PROCESO IN VARCHAR2,
                                        PV_TRANSACCION IN VARCHAR2, 
                                        PV_ESTADO      IN VARCHAR2,
                                        PV_OBSERVACION IN VARCHAR2,
                                        PV_USUARIO     IN VARCHAR2,
                                        PV_ERROR       OUT VARCHAR2) IS
 
 
  
 BEGIN
   --
   INSERT INTO GSI_LOG_PLANESQC(COD_SEQ,
                                TRANSACCION,
                                FECHA_EVENTO,
                                ESTADO,
                                OBSERVACION,
                                USUARIO)
                        VALUES (PV_COD_PROCESO,
                                PV_TRANSACCION,
                                SYSDATE,
                                PV_ESTADO,
                                PV_OBSERVACION,
                                PV_USUARIO);
   --
   COMMIT;
   --                                           
   EXCEPTION
   WHEN OTHERS THEN
   PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);   
 END  PLP_BITACORIZA_LOGPLANES_QC;                                   
 
 
 --Proceso que obtiene la tabla de tarifas anterior
 PROCEDURE PLP_TABLAS_TARIFAS_ANT_QC(PD_CIERRE_ACTUAL   IN DATE,
                                     PD_CIERRE_ANTERIOR OUT DATE,
                                     PV_ERROR           OUT VARCHAR2) IS
 
  --
  CURSOR CAMBIO_FECHA (CV_DIA VARCHAR2)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE FA.DIA_INI_CICLO=CV_DIA; 
  --
  LV_DIA              VARCHAR2(2);
  LV_FECHA_ANT        VARCHAR2(2); 
  LV_ERROR            VARCHAR2(2000);         
  LE_ERROR            EXCEPTION;
  LC_FECHA_ANT        CAMBIO_FECHA%ROWTYPE;  
  LB_FOUND            BOOLEAN;
  --
  BEGIN
    --
     -- EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';--POR PRUEBAS
    --

   LV_DIA:= EXTRACT (DAY FROM  PD_CIERRE_ACTUAL);
   
   IF LV_DIA =  2  THEN
     LV_FECHA_ANT:='24';
     --
   ELSIF LV_DIA = 8  THEN
     LV_FECHA_ANT:='02';
   ELSIF LV_DIA = 15 THEN
     LV_FECHA_ANT:='08';
   ELSIF LV_DIA = 24 THEN
     LV_FECHA_ANT:= '15';
   END IF;
   --
    OPEN CAMBIO_FECHA (LV_FECHA_ANT);
         FETCH CAMBIO_FECHA INTO LC_FECHA_ANT;
         LB_FOUND:=CAMBIO_FECHA%FOUND;
         CLOSE CAMBIO_FECHA;
        --
         IF LB_FOUND THEN
           IF LV_FECHA_ANT = '24' THEN
           PD_CIERRE_ANTERIOR:=ADD_MONTHS(TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR'),-1);
           ELSE
           PD_CIERRE_ANTERIOR:=TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR');  
           END IF;
         ELSE
           LV_ERROR:='No se obtuvo la fecha del ciclo anterior para la tablas tarifas';
           RAISE LE_ERROR;
         END IF;
   --                                         
   EXCEPTION
   WHEN LE_ERROR THEN
     PV_ERROR := LV_ERROR;
   WHEN OTHERS THEN
     PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);         

 
 END PLP_TABLAS_TARIFAS_ANT_QC;
 -- Proceso para obtener la fecha de corte del ciclo administrativo
 PROCEDURE PLP_CONSULTA_CICLO_QC (PD_CIERRE_ACTUAL         OUT DATE,
                                  PV_ERROR                 OUT VARCHAR2)IS

 -- 
  CURSOR FECHA (CV_CICLO VARCHAR)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE ID_CICLO =CV_CICLO;   
 
   LV_ERROR            VARCHAR2(2000);
   LV_CICLO            VARCHAR2(2);
   LE_ERROR_CICLO      EXCEPTION;
   LC_FECHA            FECHA%ROWTYPE;  
   LB_FOUND            BOOLEAN;
   LN_DIA              VARCHAR2(2);
 --
 BEGIN
    
    --
     -- EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';--POR PRUEBAS
    --

     LN_DIA:= EXTRACT (DAY FROM  SYSDATE);
     --
     IF LN_DIA >=2  AND LN_DIA< 8 THEN 
      LV_CICLO:='04';
     ELSIF LN_DIA >= 8 AND LN_DIA <= 14 THEN
     
     LV_CICLO:='02';
    
     ELSIF LN_DIA >= 15 AND LN_DIA < 24 THEN
     
      LV_CICLO:='03';
     
     ELSIF (LN_DIA >= 24 AND LN_DIA <= 31) OR LN_DIA =1   THEN
     
      LV_CICLO:='01';
     
     
     END IF; 
         OPEN FECHA (LV_CICLO);
         FETCH FECHA INTO LC_FECHA;
         LB_FOUND:=FECHA%FOUND;
         CLOSE FECHA;
        --
         IF LB_FOUND THEN
           PD_CIERRE_ACTUAL:=TO_DATE(LC_FECHA.FECHA_INICIO,'DD/MM/RRRR');
         ELSE
           LV_ERROR:='No se obtuvo el ciclo admistrativo de la fecha ' || TO_CHAR(SYSDATE,'DD/MM/RRRR');
           RAISE LE_ERROR_CICLO;
         END IF;
       
 EXCEPTION
  WHEN LE_ERROR_CICLO THEN
  PV_ERROR := LV_ERROR;
 WHEN OTHERS THEN
  PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);         

END   PLP_CONSULTA_CICLO_QC;           
 -- Paso 1: Insert a la tabla GSI_PLANES
 PROCEDURE PLP_OBTIENE_PLANES_QC (PV_ERROR       OUT VARCHAR2)IS
                                
    CURSOR OBTENGO_PLANES (CV_FECHA VARCHAR2) IS
    SELECT ID_PLAN FROM AXISFAC.GE_DETALLES_PLANES@AXIS P
    WHERE P.FECHA_DESDE >= TO_DATE(CV_FECHA,'DD/MM/YYYY') 
    AND ESTADO = 'A'
    AND ID_SUBPRODUCTO IN ('TAR','AUT','PPA')
    MINUS
    SELECT ID_PLAN FROM PORTA.GE_DET_PLANES_NO_COMERCIAL@AXIS 
    WHERE ESTADO = 'A';
    --
    LV_PARAMETRO_FECHA VARCHAR2(50);                              
    LV_ERROR           VARCHAR2(1000);
    LC_OBTENGO_PLANES  OBTENGO_PLANES%ROWTYPE;
    LE_ERROR           EXCEPTION;
    LN_DATA            NUMBER;
    LB_PLANES          BOOLEAN;
    LV_TRANSACCION     VARCHAR2(1000);
    

   BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    LV_TRANSACCION:='Obtiene Planes 3 meses atras';
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_001', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
    DELETE  FROM AXISFAC.GSI_PLANES@AXIS;
    --
    COMMIT;
    --
     LV_PARAMETRO_FECHA:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_FECHA_DESDE_QC');
    
    IF LV_PARAMETRO_FECHA IS NULL THEN
      LV_ERROR:= 'No se obtuvo la fecha para cargar la tabla GSI_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF; 
   SELECT COUNT(*) INTO LN_DATA FROM AXISFAC.GSI_PLANES@AXIS WHERE ROWNUM <=1; 
   
   IF LN_DATA > 0 THEN 
   LV_ERROR:= 'La tabla GSI_PLANES en AXIS debe estar sin registros'; 
   RAISE LE_ERROR;
   END IF;  
   --
   OPEN OBTENGO_PLANES (LV_PARAMETRO_FECHA);
   FETCH OBTENGO_PLANES INTO LC_OBTENGO_PLANES;
   LB_PLANES:=OBTENGO_PLANES%FOUND;
   CLOSE OBTENGO_PLANES;
   --
   IF LB_PLANES THEN 
    -- Se inserta los ID_PLAN a la tablas GSI_PLANES de AXIS
    FOR CARGO_PLANES IN OBTENGO_PLANES(LV_PARAMETRO_FECHA)
    LOOP 
   
    INSERT INTO AXISFAC.GSI_PLANES@AXIS(ID_PLAN)
    VALUES (CARGO_PLANES.ID_PLAN);
    END LOOP;
    --
    COMMIT;
    --                           
       
   ELSE
    LV_ERROR:='NO se obtuvo ID_PLAN de las tablas GE_DETALLES_PLANES, GE_DET_PLANES_NO_COMERCIAL de AXIS';
    RAISE LE_ERROR;
   END IF;                            
  
   -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_001', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_001', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_001', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
                               
                                
 END PLP_OBTIENE_PLANES_QC; 
  
  
 -- Paso 2: Cuadra_TB_Planes_com_vs_bscs
 -- Este QC lo que hace es:
 -- Validar la TB configurado en COMERCIAL Versus lo configurado en BSCS y Axis
 -- Lo correcto es que la TB de Axis y BSCS sea igual a lo solicitado por COMERCIAL 
 PROCEDURE PLP_CUADRA_TB_PLANES_COM_BSCS (PV_ERROR OUT VARCHAR2) IS
    
    --
    LV_PARAMETRO_TIPO_PLAN  VARCHAR2(32000);  
    LV_ERROR                VARCHAR2(1000);
    LE_ERROR                EXCEPTION;
    LV_MENSAJE_ERROR        VARCHAR2(1000);
    LV_TRANSACCION          VARCHAR2(1000);
    LV_PLANES_EXCLUIR       VARCHAR2(32000);
    TYPE CURSOR_TYPE        IS REF CURSOR;
    LV_QUERY                LONG;
    LC_CUADRA_PLANES        CURSOR_TYPE;
    LT_TABLA_PLANES         GT_DIST_CUADRA_TBPLANES;
    
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_CUADRA_PLANES_COM_VS_BSCS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_002', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
    
    LV_PARAMETRO_TIPO_PLAN:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PLANES_CORPORATIVOS_QC');
    --
     IF LV_PARAMETRO_TIPO_PLAN IS NULL THEN
      LV_ERROR:= 'No se obtuvo los ID_TIPO_PLAN para consultar la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    
    
    LV_PLANES_EXCLUIR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PLANES_A_EXCLUIR');
    
    IF LV_PLANES_EXCLUIR IS NULL THEN
      LV_ERROR:= 'No se obtuvo los PLANES_A_EXCLUIR para la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CUADRA_PLANES_COM_VS_BSCS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error TB COM no concide en Axis/BSCS';
    END IF;
    
    LV_QUERY:='SELECT B.ID_PLAN,
                      B.OBSERVACION, 
                       REPLACE(A.TARIFA_BASICA, '','', ''.'') AS TARIFA_BASICA_COM,
                       D.ACCESSFEE AS TARIFA_BSCS,
                       E.TARIFABASICA AS TARIFA_BASICA_AXIS
                FROM AXISFAC.DATOS_PLANES@AXIS       A,
                     AXISFAC.GE_DETALLES_PLANES@AXIS B,
                     AXISFAC.BS_PLANES@AXIS          C,
                     MPULKTM1                        D,
                     AXISFAC.CP_PLANES@AXIS          E
                WHERE A.CODIGO = B.ID_PLAN
                AND  B.id_subproducto in (''TAR'',''AUT'')
                AND  B.ESTADO = ''A''
                AND  B.id_clase=''GSM''
                AND B.ID_DETALLE_PLAN = C.ID_DETALLE_PLAN
                AND C.COD_BSCS = D.TMCODE
                AND B.ID_PLAN IN (select ID_PLAN from PORTA.ge_planes@AXIS a
                                  where ESTADO = ''A'' )
                AND D.SNCODE = 2
                AND A.CODIGO = E.ID_PLAN
                AND A.TARIFA_BASICA IS NOT NULL
                AND (REPLACE(A.TARIFA_BASICA, '','', ''.'') - D.ACCESSFEE <> 0 OR
                     REPLACE(A.TARIFA_BASICA, '','', ''.'') - D.ACCESSFEE IS NULL OR
                     REPLACE(A.TARIFA_BASICA, '','', ''.'') - E.TARIFABASICA <> 0 OR
                     REPLACE(A.TARIFA_BASICA, '','', ''.'') - E.TARIFABASICA IS NULL)
               AND   B.ID_TIPO_PLAN NOT IN ( <TIPO_PLAN> )
               AND   B.ID_PLAN NOT IN ( <ID_PLANES> )';
               
    LV_QUERY:=REPLACE(LV_QUERY,'<TIPO_PLAN>',LV_PARAMETRO_TIPO_PLAN);
    LV_QUERY:=REPLACE(LV_QUERY,'<ID_PLANES>',LV_PLANES_EXCLUIR);           
    
    --
     OPEN  LC_CUADRA_PLANES FOR LV_QUERY;
     LOOP
     LT_TABLA_PLANES.delete;
     FETCH LC_CUADRA_PLANES BULK COLLECT INTO LT_TABLA_PLANES limit 600;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_CUADRA_PLANES%ROWCOUNT < 1);
      IF LT_TABLA_PLANES.COUNT > 0 THEN
        FOR M IN LT_TABLA_PLANES.FIRST .. LT_TABLA_PLANES.LAST
        LOOP
           INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.TARIFA_BASICA_COM,
                                            T.TARIFA_BSCS,
                                            T.TARIFA_BASICA_AXIS,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_002',
                                              LT_TABLA_PLANES(M).ID_PLAN,
                                              LT_TABLA_PLANES(M).OBSERVACION,
                                              LT_TABLA_PLANES(M).TARIFA_BASICA_COM,
                                              LT_TABLA_PLANES(M).TARIFA_BASICA_AXIS,
                                              LT_TABLA_PLANES(M).TARIFA_BSCS,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
      
         --
         COMMIT;
         --                                      

       
       END LOOP;
                            
      END IF;
     
     EXIT WHEN (LC_CUADRA_PLANES%NOTFOUND);
   
    END LOOP;
     --
     COMMIT;
     --
     CLOSE     LC_CUADRA_PLANES; 
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_002', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_002', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_002', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
                                       
 END   PLP_CUADRA_TB_PLANES_COM_BSCS;
       
 -- Paso 3: Query_costos_OCC
 --- QC para determinar OCC con features diferentes entre Productos.
 --Determino los features que son OCC y verifico que las tarifas sean iguales para el feature AUT Y TAR
 PROCEDURE PLP_QUERY_COSTOS_OCC_QC (PV_ERROR       OUT VARCHAR2)IS  
    --
    CURSOR OBTENGO_COSTOS_OCC IS
    SELECT A.VALOR_OMISION, A.ID_TIPO_DETALLE_SERV, A.DESCRIPCION, A.ID_CLASIFICACION_03 AS ID_CLASIFICACION_DIF1, B.ID_CLASIFICACION_03 AS ID_CLASIFICACION_DIF2
    FROM (SELECT B.ID_TIPO_DETALLE_SERV, B.VALOR_OMISION, B.ID_CLASIFICACION_03, B.DESCRIPCION
    FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXIS B
    WHERE B.ID_CLASIFICACION_02 IS NOT NULL
    AND B.ID_CLASIFICACION_02 NOT IN ('99')
    AND B.ID_SUBPRODUCTO IN ('AUT')
    AND B.ESTADO='A') A, (SELECT B.ID_TIPO_DETALLE_SERV, B.VALOR_OMISION, B.ID_CLASIFICACION_03, B.DESCRIPCION
            FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXIS B
            WHERE B.ID_CLASIFICACION_02 IS NOT NULL
            AND B.ID_CLASIFICACION_02 NOT IN ('99')
            AND B.ID_SUBPRODUCTO IN ('TAR')
            AND B.ESTADO='A') B
    WHERE A.VALOR_OMISION =  B.VALOR_OMISION     
   AND A.ID_CLASIFICACION_03 <> B.ID_CLASIFICACION_03; 

   --
    LV_ERROR                VARCHAR2(1000);
    LE_ERROR                EXCEPTION;
    LV_MENSAJE_ERROR        VARCHAR2(1000);
    LV_TRANSACCION          VARCHAR2(1000);
    
   -- 
  BEGIN
   -- USUARIO
   SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
   FROM DUAL ;
   -- SE OBTIENE EL VALOR DE LA TRACCION 
   OPEN TRANSACCION (14793,'GSI_QUERY_COSTOS_OCC');
   FETCH TRANSACCION INTO LV_TRANSACCION;
   CLOSE TRANSACCION;
   -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_003', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
   --
   LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_QUERY_COSTOS_OCC');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Costo OCC diferentes entre Productos';
    END IF;
   -- 
   FOR COSTOS_OCC IN OBTENGO_COSTOS_OCC
   LOOP
    INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                          T.VALOR_OMISION,
                                          T.ID_TIPO_DETALLE_SERVICIOS,
                                          T.DESCRIPCION,
                                          T.ID_CLASIFICACION_DIF1,
                                          T.ID_CLASIFICACION_DIF2,
                                          T.MENSAJE_TECNICO,
                                          T.USUARIO,
                                          T.ESTADO,
                                          T.FECHA_EVENTO)
                                      VALUES ('QC_003',
                                              COSTOS_OCC.VALOR_OMISION,
                                              COSTOS_OCC.ID_TIPO_DETALLE_SERV,
                                              COSTOS_OCC.DESCRIPCION,
                                              COSTOS_OCC.ID_CLASIFICACION_DIF1,
                                              COSTOS_OCC.ID_CLASIFICACION_DIF2,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);

    
   
   END LOOP; 
     --
     COMMIT;
     --
      -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_003', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
 EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_003', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_003', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
 
 END  PLP_QUERY_COSTOS_OCC_QC;                   

 -- Paso 4: Revisa_Toll_Interconexion
 --Para verificar si tienen correctamente configurado el toll 
 --Para que puedan hacer realizar compra tiempo aire

 PROCEDURE PLP_TOLL_INTERCONEXION_QC (PV_ERROR       OUT VARCHAR2)IS         
    --
    CURSOR OBTENGO_TOLL_INTERCONEXION IS 
    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'PERSONAL 1'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'A'
    UNION ALL
    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'PERSONAL 2'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'AA'
    UNION ALL
    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'PERSONAL 3'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'AAA'
    UNION ALL

    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'CORPORATIVO 2'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'AAA'
    UNION ALL
    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'CORPORATIVO 1'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'AAA'
    UNION ALL
    SELECT B.ID_PLAN, B.DESCRIPCION, B.TIPO, A.TIPO_INTERCONEXION 
    FROM  AXISFAC.DATOS_PLANES@AXIS A, AXISFAC.CP_PLANES@AXIS B
    WHERE A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)
    AND A.TIPO_INTERCONEXION  = 'CORPORATIVO 3'
    AND  A.CODIGO = B.ID_PLAN
    AND TIPO <> 'AAA';
    --
    LV_ERROR                VARCHAR2(1000);
    LE_ERROR                EXCEPTION;
    LV_MENSAJE_ERROR        VARCHAR2(1000);
    LV_TRANSACCION          VARCHAR2(1000);
  BEGIN
   -- USUARIO
   SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
   FROM DUAL ;
   -- SE OBTIENE EL VALOR DE LA TRACCION 
   OPEN TRANSACCION (14793,'GSI_REVISA_TOLL_INTERCONEXION');
   FETCH TRANSACCION INTO LV_TRANSACCION;
   CLOSE TRANSACCION;
   -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_004', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
   --
   LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISA_TOLL_INTERCONEXION');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Toll incorrecto vs la Interconexion solicitada por COM';
    END IF;
   -- 
   FOR TOLL_INTERCONEXION IN OBTENGO_TOLL_INTERCONEXION
   LOOP
    INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                          T.ID_PLAN,
                                          T.DESCRIPCION,
                                          T.TIPO,
                                          T.TIPO_INTERCONEXION,
                                          T.MENSAJE_TECNICO,
                                          T.USUARIO,
                                          T.ESTADO,
                                          T.FECHA_EVENTO)
                                      VALUES ('QC_004',
                                              TOLL_INTERCONEXION.ID_PLAN,
                                              TOLL_INTERCONEXION.DESCRIPCION,
                                              TOLL_INTERCONEXION.TIPO,
                                              TOLL_INTERCONEXION.TIPO_INTERCONEXION,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);

 
    
   END LOOP; 
     --
     COMMIT;
     --
      -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_004', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_004', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_004', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
  
 END PLP_TOLL_INTERCONEXION_QC;    
 -- Paso 5:  Revisar_Features_Consistentes_con_el_producto
 --Qc para verificar si un plan tiene asociado un feature 
 --con un producto diferente al de mi plan 
 
 PROCEDURE PLP_FEATURES_CONSISTENTES_QC (PV_ERROR       OUT VARCHAR2)IS    
   --
   CURSOR OBTENGO_FEATURES_CONSIST IS
   SELECT DISTINCT B.ID_DETALLE_PLAN, B.ID_PLAN, B.OBSERVACION AS NOMBRE_PLAN, B.ID_SUBPRODUCTO AS SUBPRODUCTO_PLAN,
   SUBSTR(ID_TIPO_DETALLE_SERV,1,3) AS SUBPRODUCTO_FEATURE
   FROM AXISFAC.CL_SERVICIOS_PLANES@AXIS A, AXISFAC.GE_DETALLES_PLANES@AXIS B
   WHERE A.ID_DETALLE_PLAN = B.ID_DETALLE_PLAN 
   AND B.ID_SUBPRODUCTO <> SUBSTR(ID_TIPO_DETALLE_SERV,1,3) 
   AND ID_SUBPRODUCTO IN ('AUT', 'TAR');
   
   -- 
   LV_ERROR                VARCHAR2(1000);
   LE_ERROR                EXCEPTION;
   LV_MENSAJE_ERROR        VARCHAR2(1000);
   LV_TRANSACCION          VARCHAR2(1000);
  BEGIN
   -- USUARIO
   SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
   FROM DUAL ;
   -- SE OBTIENE EL VALOR DE LA TRACCION 
   OPEN TRANSACCION (14793,'GSI_REVISAR_FEATURES_CONSIST');
   FETCH TRANSACCION INTO LV_TRANSACCION;
   CLOSE TRANSACCION;
   -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_005', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
   --
   LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISAR_FEATURES_CONSIST');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Feature asociado diferente al Producto de plan';
    END IF;
    --
    FOR FEATURES_CONSISTENTE IN OBTENGO_FEATURES_CONSIST
    LOOP
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_DETALLE_PLAN,
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.SUBPRODUCTO_PLAN,
                                            T.SUBPRODUCTO_FEATURE,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                        VALUES ('QC_005',
                                                FEATURES_CONSISTENTE.ID_DETALLE_PLAN,
                                                FEATURES_CONSISTENTE.ID_PLAN,
                                                FEATURES_CONSISTENTE.NOMBRE_PLAN,
                                                FEATURES_CONSISTENTE.SUBPRODUCTO_PLAN,
                                                FEATURES_CONSISTENTE.SUBPRODUCTO_FEATURE,
                                                LV_MENSAJE_ERROR,
                                                LV_USUARIO,
                                                'A',
                                                SYSDATE);

 
     
   END LOOP; 
     --
     COMMIT;
     --
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_005', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_005', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_005', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
  
 END PLP_FEATURES_CONSISTENTES_QC;
 -- Paso 6:  Diferencia de Tarifas en TN3
 --Qc para sacar las diferencias de la tarifas en TN3
 PROCEDURE PLP_TARIFAS_TN3_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                               PV_ERROR        OUT VARCHAR2)IS    
   --
   TYPE CURSOR_TYPE        IS REF CURSOR;
   LV_QUERY                LONG;
   LC_TARIFAS_TN3          CURSOR_TYPE;
   LT_TABLA_TARIFAS        GT_DIST_TABLA_TARIFAS;
   LV_ERROR                VARCHAR2(1000);
   LE_ERROR                EXCEPTION;
   LV_MENSAJE_ERROR        VARCHAR2(1000);
   LV_NOM_TABLA            VARCHAR2(50);
   LV_COSTO                VARCHAR2(10);
   LV_TABLA                VARCHAR2(100);
   LD_FECHA_ACTUAL         DATE;
   LD_FECHA_ANTERIOR       DATE;
   LN_EXISTE               NUMBER;
   LV_TRANSACCION          VARCHAR2(1000);
   LV_OBSERVACION          VARCHAR2(1000);
  
   --
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_REVISI�N_TARIFAS_VS_TN3');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_006', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
    -- 
    LV_COSTO:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_COSTO_MINIMO_QC');
    
    IF LV_COSTO IS NULL THEN
      LV_ERROR:= 'No se obtuvo el valor del costo minimo para obtener las diferencias';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISI�N_TARIFAS_VS_TN3');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Diferencia de Tarifas en TN3';
    END IF;
    --
    LV_NOM_TABLA:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_TARIFAS_TN3_QC');
    
    --
    IF LV_NOM_TABLA IS NULL THEN
      LV_NOM_TABLA:='GSI_TARIFAS_TN3_';
    END IF;
    
    IF PV_FECHA_ACTUAL IS NOT NULL THEN
    LV_TABLA:='SMS.'||LV_NOM_TABLA||TO_CHAR(TRUNC(TO_DATE(PV_FECHA_ACTUAL,'DD/MM/RRRR')),'DDMMRRRR')||'@COLECTOR';-- POR PRUEBAS
    --LV_TABLA:=LV_NOM_TABLA||TO_CHAR(TRUNC(TO_DATE(PV_FECHA_ACTUAL,'DD/MM/RRRR')),'DDMMRRRR')||'@COLECTOR';

    ELSE
      
      -- Se invoca el proceso para obtener la fecha de corte actual
      GSI_PLANESQC.PLP_CONSULTA_CICLO_QC(PD_CIERRE_ACTUAL => LD_FECHA_ACTUAL,
                                         PV_ERROR         => LV_ERROR);
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;                                     
      --
      LV_TABLA:=LV_NOM_TABLA||TO_CHAR(TRUNC(LD_FECHA_ACTUAL),'DDMMRRRR');-- POR PRUEBAS
      SELECT COUNT(*) INTO LN_EXISTE FROM ALL_TABLES@COLECTOR
      WHERE TABLE_NAME=UPPER(LV_TABLA);     
            LV_TABLA:='SMS.'||LV_NOM_TABLA||TO_CHAR(TRUNC(LD_FECHA_ACTUAL),'DDMMRRRR')||'@COLECTOR';-- POR PRUEBAS

      IF (LN_EXISTE = 0) THEN    
       -- Se invoca al proceso para obtener la tabla del cierre_anterior
       GSI_PLANESQC.PLP_TABLAS_TARIFAS_ANT_QC(PD_CIERRE_ACTUAL   => LD_FECHA_ACTUAL , 
                                              PD_CIERRE_ANTERIOR => LD_FECHA_ANTERIOR , 
                                              PV_ERROR           => LV_ERROR );
         IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
         END IF;
       --
       LV_TABLA:='SMS.'||LV_NOM_TABLA||TO_CHAR(TRUNC(LD_FECHA_ANTERIOR),'DDMMRRRR')||'@COLECTOR';
       --
      END IF;             

      --
    END IF;
    --
    LV_OBSERVACION:='Se utilizo la tabla '||LV_TABLA;

    
    LV_QUERY :=  'SELECT A.CODIGO, A.NOMBRE_PLAN,
                 DECODE(A.COSTO_MIN_INPOLL_INC,''N/A'',''0'',''NA'',''0'',A.COSTO_MIN_INPOLL_INC), B.CLARO_ONNET_VPN_773, (DECODE(A.COSTO_MIN_INPOLL_INC,''N/A'',''0'',''NA'',''0'',A.COSTO_MIN_INPOLL_INC) - B.CLARO_ONNET_VPN_773)  AS DIFERENCIA_INPOOL,
                 A.COSTO_MIN_CLAROACLARO, 
                 DECODE(B.CLARO_OFFNET_VPN_772,0,B.CLARO,B.CLARO_OFFNET_VPN_772) CLARO_CON_VALOR_TN3,
                 A.COSTO_MIN_CLAROACLARO - DECODE(B.CLARO_OFFNET_VPN_772,0,B.CLARO,B.CLARO_OFFNET_VPN_772) AS DIFERENCIA_CLARO,
                 A.COSTO_MIN_CLAROAMOVI,
                 DECODE(B.MOVISTAR_VPN_772,0,B.MOVISTAR,B.MOVISTAR_VPN_772) MOVI_CON_VALOR_TN3,
                 A.COSTO_MIN_CLAROAMOVI - DECODE (B.MOVISTAR_VPN_772,0,B.MOVISTAR,B.MOVISTAR_VPN_772) AS DIFERENCIA_MOVISTAR, 
                 A.COSTO_MIN_CLAROAALEGRO, 
                 DECODE(B.ALEGRO_VPN_772,0,B.ALEGRO,B.ALEGRO_VPN_772) ALEGRO_CON_VALOR, 
                 A.COSTO_MIN_CLAROAALEGRO - DECODE(B.ALEGRO_VPN_772,0,B.ALEGRO,B.ALEGRO_VPN_772) AS DIFERENCIA_ALEGRO,        
                 A.COSTO_MIN_CLAROAFIJO, 
                 DECODE(B.LOCAL_VPN_772,0,B.LOCAL,B.LOCAL_VPN_772 ) LOCAL_CON_VALOR_TN3,
                 A.COSTO_MIN_CLAROAFIJO - DECODE(B.LOCAL_VPN_772,0,B.LOCAL,B.LOCAL_VPN_772 ) AS DIFERENCIA_FIJAS
                 FROM  AXISFAC.DATOS_PLANES@AXIS A, '||LV_TABLA||' B' 
                 ||' WHERE A.CODIGO = B.ID_PLAN
                    AND   A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)';
                    
           
                 
    --
     OPEN  LC_TARIFAS_TN3 FOR LV_QUERY;
     LOOP
     LT_TABLA_TARIFAS.delete;
     FETCH LC_TARIFAS_TN3 BULK COLLECT INTO LT_TABLA_TARIFAS limit 600;--LIMITE PARAMETRIZADO

        EXIT WHEN (LC_TARIFAS_TN3%ROWCOUNT < 1);

        IF LT_TABLA_TARIFAS.COUNT > 0 THEN
        
        FOR M IN LT_TABLA_TARIFAS.FIRST .. LT_TABLA_TARIFAS.LAST 
          LOOP
       IF TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_INPOOL) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_CLARO) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_MOVISTAR) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_ALEGRO) > TO_NUMBER(LV_COSTO) OR  TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_FIJAS) > TO_NUMBER(LV_COSTO) THEN
            INSERT
             INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.CODIGO,
                                            T.DESCRIPCION,
                                            T.PRECIO_INPOOL,
                                            T.CLARO_INPOOL,
                                            T.DIFERENCIA_INPOOL,
                                            T.COSTO_MIN_CLAROACLARO,
                                            T.CLARO_CON_VALOR,
                                            T.DIFERENCIA_CLARO,
                                            T.COSTO_MIN_CLAROAMOVI,
                                            T.MOVI_CON_VALOR,
                                            T.DIFERENCIA_MOVISTAR,
                                            T.COSTO_MIN_CLAROAALEGRO,
                                            T.ALEGRO_CON_VALOR,
                                            T.DIFERENCIA_ALEGRO,
                                            T.COSTO_MIN_CLAROAFIJO,
                                            T.LOCAL_CON_VALOR,
                                            T.DIFERENCIA_FIJAS,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                            VALUES ('QC_006',
                                                    LT_TABLA_TARIFAS(M).CODIGO,
                                                    LT_TABLA_TARIFAS(M).DESCRIPCION             ,
                                                    LT_TABLA_TARIFAS(M).PRECIO_INPOOL           ,
                                                    LT_TABLA_TARIFAS(M).CLARO_INPOOL           ,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_INPOOL      ,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROACLARO ,
                                                    LT_TABLA_TARIFAS(M).CLARO_CON_VALOR         ,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_CLARO        ,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAMOVI    ,
                                                    LT_TABLA_TARIFAS(M).MOVI_CON_VALOR         ,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_MOVISTAR     ,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAALEGRO,
                                                    LT_TABLA_TARIFAS(M).ALEGRO_CON_VALOR,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_ALEGRO,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAFIJO,
                                                    LT_TABLA_TARIFAS(M).LOCAL_CON_VALOR,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_FIJAS,
                                                    LV_MENSAJE_ERROR,
                                                    LV_USUARIO,
                                                    'A',
                                                    SYSDATE);


         --
         COMMIT;
         --
        END IF;
       END LOOP;
      END IF;
     EXIT WHEN (LC_TARIFAS_TN3%NOTFOUND);

    END LOOP;
     CLOSE     LC_TARIFAS_TN3;
     --
      COMMIT;
     --
     
      -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_006', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => LV_OBSERVACION,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_006', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_006', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);     
  END PLP_TARIFAS_TN3_QC;
  
 -- Paso 7:  Diferencia de Tarifas en BSCS
 --Qc para sacar las diferencias de la tarifas en BSCS
 PROCEDURE PLP_TARIFAS_BSCS_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2)IS    
   --
   TYPE CURSOR_TYPE        IS REF CURSOR;
   LV_QUERY                LONG;
   LC_TARIFAS_BSCS          CURSOR_TYPE;
   LT_TABLA_TARIFAS        GT_DIST_TABLA_TARIFAS;
   LV_ERROR                VARCHAR2(1000);
   LE_ERROR                EXCEPTION;
   LV_MENSAJE_ERROR        VARCHAR2(1000);
   LV_NOM_TABLA            VARCHAR2(50);
   LV_COSTO                VARCHAR2(10);
   LV_TABLA                VARCHAR2(100);
   LD_FECHA_ACTUAL         DATE;
   LD_FECHA_ANTERIOR       DATE;
   LN_EXISTE               NUMBER;
   LV_TRANSACCION          VARCHAR2(1000);
   LV_OBSERVACION          VARCHAR2(1000);
   
   

   --
   
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_REVISI�N_TARIFAS_VS_BSCS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_007', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
     
    -- 
    LV_COSTO:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_COSTO_MINIMO_QC');
    
    IF LV_COSTO IS NULL THEN
      LV_ERROR:= 'No se obtuvo el valor del costo minimo para obtener las diferencias';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISI�N_TARIFAS_VS_BSCS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Diferencia de Tarifas en BSCS';
    END IF;
    --
    LV_NOM_TABLA:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_TARIFAS_BSCS_QC');
    
    --
    IF LV_NOM_TABLA IS NULL THEN
      LV_NOM_TABLA:='GSI_TARIFAS_BSCS_';
    END IF;
    
    IF PV_FECHA_ACTUAL IS NOT NULL THEN
     LV_TABLA:=LV_NOM_TABLA||TO_CHAR(TRUNC(TO_DATE(PV_FECHA_ACTUAL,'DD/MM/RRRR')),'DDMMRRRR');
    ELSE
      -- Se invoca el proceso para obtener la fecha de corte actual
      GSI_PLANESQC.PLP_CONSULTA_CICLO_QC(PD_CIERRE_ACTUAL => LD_FECHA_ACTUAL,
                                         PV_ERROR         => LV_ERROR);
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;                                     
      --
      LV_TABLA:=LV_NOM_TABLA||TO_CHAR(TRUNC(LD_FECHA_ACTUAL),'DDMMRRRR');
      --
      SELECT COUNT(*) INTO LN_EXISTE FROM ALL_TABLES
      WHERE TABLE_NAME=UPPER(LV_TABLA);     
      IF (LN_EXISTE = 0) THEN    
       -- Se invoca al proceso para obtener la tabla del cierre_anterior
       GSI_PLANESQC.PLP_TABLAS_TARIFAS_ANT_QC(PD_CIERRE_ACTUAL   => LD_FECHA_ACTUAL , 
                                              PD_CIERRE_ANTERIOR => LD_FECHA_ANTERIOR , 
                                              PV_ERROR           => LV_ERROR );
         IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
         END IF;
       --
       LV_TABLA:=LV_NOM_TABLA||TO_CHAR(TRUNC(LD_FECHA_ANTERIOR),'DDMMRRRR');
       --
      END IF;
    
    END IF;
     --
     LV_OBSERVACION:='Se utilizo la tabla '||LV_TABLA;
     --
    LV_QUERY := 'SELECT A.CODIGO, A.NOMBRE_PLAN,
                        DECODE(A.COSTO_MIN_INPOLL_INC,''N/A'',''0'',''NA'',''0'',A.COSTO_MIN_INPOLL_INC), B.CLARO_INPOOL, (DECODE(A.COSTO_MIN_INPOLL_INC,''N/A'',''0'',''NA'',''0'',A.COSTO_MIN_INPOLL_INC)- B.CLARO_INPOOL ) AS DIFERENCIA_INPOOL,        
                        A.COSTO_MIN_CLAROACLARO, B.CLARO, ( A.COSTO_MIN_CLAROACLARO -B.CLARO) AS DIFERENCIA_CLARO,
                        A.COSTO_MIN_CLAROAMOVI, B.MOVISTAR, (A.COSTO_MIN_CLAROAMOVI -  B.MOVISTAR) AS DIFERENCIA_MOVI,
                        A.COSTO_MIN_CLAROAALEGRO, B.ALEGRO, (A.COSTO_MIN_CLAROAALEGRO - B.ALEGRO) AS DIFERENCIA_ALEGRO,
                        A.COSTO_MIN_CLAROAFIJO, B.FIJA, (A.COSTO_MIN_CLAROAFIJO - B.FIJA ) AS DIFERENCIA_FIJA
                 FROM AXISFAC.DATOS_PLANES@AXIS A, '||LV_TABLA||' B'
                 ||' WHERE A.CODIGO = B.ID_PLAN
                    AND A.CODIGO IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)'; 
                    
           
     OPEN  LC_TARIFAS_BSCS FOR LV_QUERY;
     LOOP
     LT_TABLA_TARIFAS.delete;
     FETCH LC_TARIFAS_BSCS BULK COLLECT INTO LT_TABLA_TARIFAS limit 600;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_TARIFAS_BSCS%ROWCOUNT < 1);

        IF LT_TABLA_TARIFAS.COUNT > 0 THEN
        FOR M IN LT_TABLA_TARIFAS.FIRST .. LT_TABLA_TARIFAS.LAST
        LOOP 
        IF TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_INPOOL) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_CLARO) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_MOVISTAR) > TO_NUMBER(LV_COSTO) OR TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_ALEGRO) > TO_NUMBER(LV_COSTO) OR  TO_NUMBER(LT_TABLA_TARIFAS(M).DIFERENCIA_FIJAS) > TO_NUMBER(LV_COSTO) THEN  


        
            INSERT 
             INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.CODIGO,
                                            T.DESCRIPCION,
                                            T.PRECIO_INPOOL,
                                            T.CLARO_INPOOL,
                                            T.DIFERENCIA_INPOOL,
                                            T.COSTO_MIN_CLAROACLARO,
                                            T.CLARO_CON_VALOR,
                                            T.DIFERENCIA_CLARO,
                                            T.COSTO_MIN_CLAROAMOVI,
                                            T.MOVI_CON_VALOR,
                                            T.DIFERENCIA_MOVISTAR,
                                            T.COSTO_MIN_CLAROAALEGRO,
                                            T.ALEGRO_CON_VALOR,
                                            T.DIFERENCIA_ALEGRO,
                                            T.COSTO_MIN_CLAROAFIJO,
                                            T.LOCAL_CON_VALOR,
                                            T.DIFERENCIA_FIJAS,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                            VALUES ('QC_007',
                                                    LT_TABLA_TARIFAS(M).CODIGO,
                                                    LT_TABLA_TARIFAS(M).DESCRIPCION             ,
                                                    LT_TABLA_TARIFAS(M).PRECIO_INPOOL           ,
                                                    LT_TABLA_TARIFAS(M).CLARO_INPOOL           ,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_INPOOL      ,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROACLARO ,
                                                    LT_TABLA_TARIFAS(M).CLARO_CON_VALOR         ,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_CLARO        ,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAMOVI    ,
                                                    LT_TABLA_TARIFAS(M).MOVI_CON_VALOR         ,  
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_MOVISTAR     , 
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAALEGRO,
                                                    LT_TABLA_TARIFAS(M).ALEGRO_CON_VALOR,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_ALEGRO,
                                                    LT_TABLA_TARIFAS(M).COSTO_MIN_CLAROAFIJO,
                                                    LT_TABLA_TARIFAS(M).LOCAL_CON_VALOR,
                                                    LT_TABLA_TARIFAS(M).DIFERENCIA_FIJAS,
                                                    LV_MENSAJE_ERROR,
                                                    LV_USUARIO,
                                                    'A',
                                                    SYSDATE);

     
         --
         COMMIT;
         --                                      

       END IF; 
       END LOOP;
                            
      END IF;
     
     EXIT WHEN (LC_TARIFAS_BSCS%NOTFOUND);
   
    END LOOP;
     
     CLOSE     LC_TARIFAS_BSCS; 
      --
      COMMIT;
      --
      -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_007', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => LV_OBSERVACION,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                             
     
 EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_007', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_007', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);    
  END PLP_TARIFAS_BSCS_QC;
 -- Paso 8: Costos_Features_BSCS_vs_CuotaMensual
 --Qc para los cotos de los features de BSCS y la cuota mensual
 PROCEDURE PLP_COSTOS_FEATURES_QC (PV_ERROR       OUT VARCHAR2)IS 
    --Obtengo los features fijos del plan en Axis
    CURSOR SERVICIOS_PLANES IS 
    SELECT H.ID_PLAN, G.ID_TIPO_DETALLE_SERV
      FROM CL_SERVICIOS_PLANES@AXIS G,
           (SELECT ID_DETALLE_PLAN, ID_PLAN
              FROM GE_DETALLES_PLANES@AXIS
             WHERE ID_PLAN IN (SELECT ID_PLAN FROM AXISFAC.GSI_PLANES@AXIS)) H
     WHERE G.ID_DETALLE_PLAN = H.ID_DETALLE_PLAN
       AND G.OBLIGATORIO = 'S'
       AND G.ACTUALIZABLE = 'N'
       AND OCULTO_ROL IS NULL;
    --Obtengo el tmcode asociado al plan
    CURSOR CODIGO_BSCS IS 
    SELECT /*+RULE+*/
     A.ROWID, C.COD_BSCS
      FROM GSI_PLANES_FEATURES     A,
           GE_DETALLES_PLANES@AXIS B,
           BS_PLANES@AXIS          C
     WHERE A.ID_PLAN = B.ID_PLAN
       AND B.ID_DETALLE_PLAN = C.ID_DETALLE_PLAN
       AND C.ID_CLASE = 'GSM';
    --Obtengo el sncode asociado al feature
    CURSOR SNCODE_ASOCIADO IS  
    SELECT A.ROWID, B.SN_CODE
      FROM GSI_PLANES_FEATURES A, BS_SERVICIOS_PAQUETE@AXIS B
      WHERE SUBSTR(A.ID_TIPO_DETALLE_SERV, 5, 4) = B.COD_AXIS;
    --Para sacar el valor de UN OCC
    CURSOR VALOR_OCC IS 
    SELECT /*+RULE +*/
     A.ROWID,
     A.ID_TIPO_DETALLE_SERV,
     TO_NUMBER(Q.ID_CLASIFICACION_03) AS COSTO,
     ID_CLASIFICACION_02 AS SNCODE
      FROM GSI_PLANES_FEATURES A, CL_TIPOS_DETALLES_SERVICIOS@AXIS Q
     WHERE A.ID_TIPO_DETALLE_SERV = Q.ID_TIPO_DETALLE_SERV
       AND Q.ID_CLASIFICACION_03 IS NOT NULL
       AND ID_CLASIFICACION_03 NOT IN ('10-D', '5-D');
    --Para sacar el valor del feature
    CURSOR VALOR_FEATURE IS
    SELECT ROWID, TMCODE, SNCODE FROM GSI_PLANES_FEATURES
    WHERE VALOR IS NULL;
    
    --Para saber si se asociaron los OCC al plan
    CURSOR ASOCIARON_OCC IS
    SELECT ROWID, TMCODE, SNCODE FROM GSI_PLANES_FEATURES
    WHERE OBSERVACION2='FEATURE ES UN OCC';
    --Verifico que todos los features que mapean a BSCS, est�n asociados a los planes en BSCS.
    CURSOR VERIFICO_FEATURES_BSCS IS
    SELECT OBSERVACION, COUNT(*) AS VALOR
    FROM GSI_PLANES_FEATURES 
    WHERE VALOR IS NOT NULL
    GROUP BY OBSERVACION;
    --Para sacar el valor de la TB
    CURSOR VALOR_TB IS
    SELECT ROWID, TMCODE, SNCODE
      FROM GSI_PLANES_FEATURES
     WHERE VALOR IS NULL;
    --Qc para ver si la cuota del plan es igual a la suma de todos los features fijos del plan, m�s la TB
    CURSOR CUOTA_TB(CV_ID_CLASE_AMX IN VARCHAR2)IS
    SELECT B.*, A.CUOTA_MENSUAL AS CUOTA_MENSUAL_AXIS
    FROM  CP_PLANES@AXIS A, 
      (SELECT A.ID_PLAN,B.DESCRIPCION, SUM(A.VALOR) AS CUOTA_MENSUAL_BSCS
      FROM  GSI_PLANES_FEATURES A, PORTA.GE_PLANES@AXIS B
      WHERE A.OBSERVACION2 IS NULL
      AND   A.ID_PLAN = B.ID_PLAN
      GROUP BY A.ID_PLAN,B.DESCRIPCION) B,
      GE_DETALLES_PLANES@AXIS C
      WHERE A.ID_PLAN = B.ID_PLAN
      AND   TO_NUMBER(A.CUOTA_MENSUAL) <> TO_NUMBER(B.CUOTA_MENSUAL_BSCS)
      AND   B.ID_PLAN = C.ID_PLAN
      AND   C.ID_CLASE_AMX <> CV_ID_CLASE_AMX;
    --
    LN_DATOS               NUMBER;
    LV_ERROR               VARCHAR2(1000);
    LE_ERROR               EXCEPTION;
    LC_SERV_PLANES         SERVICIOS_PLANES%ROWTYPE;
    LB_SERV_PLANES         BOOLEAN;
    LN_NUMERO              DECIMAL(10):=0;
    LN_EXISTE              NUMBER;
    LN_PT_COSTO            NUMBER;
    LC_FEATURES            VERIFICO_FEATURES_BSCS%ROWTYPE;
    LV_PARAMETRO_CLASE_AMX VARCHAR2(5);
    LV_MENSAJE_ERROR       VARCHAR2(1000);
    LV_TRANSACCION         VARCHAR2(1000);
    
    
  BEGIN
   
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_COSTOS_FEATURES_BSCS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_008', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
      DELETE  FROM gsi_planes_features;
      
      COMMIT;
    --
    SELECT COUNT(*) INTO LN_DATOS FROM gsi_planes_features WHERE ROWNUM <=1;
    
    IF LN_DATOS > 0 THEN 
     LV_ERROR:= 'La tabla gsi_planes_features debe estar sin registros'; 
    RAISE LE_ERROR;
    END IF;  
    --
    OPEN SERVICIOS_PLANES;
    FETCH SERVICIOS_PLANES INTO LC_SERV_PLANES;
    LB_SERV_PLANES:=SERVICIOS_PLANES%FOUND;
    CLOSE SERVICIOS_PLANES;
     --
     IF LB_SERV_PLANES THEN 
      -- Se inserta los features fijos del plan en Axis
      FOR PLANES IN SERVICIOS_PLANES
      LOOP 
     
      INSERT INTO GSI_PLANES_FEATURES(ID_PLAN, ID_TIPO_DETALLE_SERV)
      VALUES (PLANES.ID_PLAN,PLANES.ID_TIPO_DETALLE_SERV);
      END LOOP;
      --
      COMMIT;
      --
     ELSE
      LV_ERROR:='NO se obtuvo el ID_PLAN,ID_TIPO_DETALLE_SERV de las tablas CL_SERVICIOS_PLANES de AXIS';
      RAISE LE_ERROR;
     END IF;  
    --inserto el tmcode asociado al plan 
    FOR I IN CODIGO_BSCS LOOP
   
      UPDATE GSI_PLANES_FEATURES
      SET    TMCODE = I.COD_BSCS
      WHERE  ROWID = I.ROWID;
   
    END LOOP;
    
    --inserto el sncode asociado al feature
    FOR J IN  SNCODE_ASOCIADO LOOP
  
     UPDATE  GSI_PLANES_FEATURES
     SET     SNCODE = J.SN_CODE
     WHERE   ROWID = J.ROWID;
  
    END LOOP;
    --Para sacar el valor de UN OCC
    FOR M IN VALOR_OCC LOOP

        LN_NUMERO:= LN_NUMERO + 1;
       
        UPDATE GSI_PLANES_FEATURES
        SET    VALOR = M.COSTO,
               SNCODE = M.SNCODE,  
               OBSERVACION2= 'FEATURE ES UN OCC'             
        WHERE  ROWID = M.ROWID;   

         IF MOD(LN_NUMERO,800)=0 THEN
             COMMIT;
         END IF;                
        
     END LOOP;   
    --Para sacar el valor del feature
    FOR D IN VALOR_FEATURE LOOP 
       SELECT COUNT(*) INTO LN_EXISTE 
       FROM   MPULKTM1 G
       WHERE  TMCODE = D.TMCODE
       AND    SNCODE = D.SNCODE;
       
       IF LN_EXISTE = 0 THEN
         
           UPDATE GSI_PLANES_FEATURES
           SET    OBSERVACION = 'FEATURE NO EXISTE EN BSCS'
           WHERE  ROWID = D.ROWID; 
           COMMIT;    
       
       ELSE
         
           SELECT G.ACCESSFEE INTO LN_PT_COSTO 
           FROM   MPULKTM1 G
           WHERE  TMCODE = D.TMCODE
           AND    SNCODE = D.SNCODE;
       
           UPDATE GSI_PLANES_FEATURES
           SET    VALOR = LN_PT_COSTO,
                  OBSERVACION = 'FEATURE EXISTE EN BSCS'
           WHERE  ROWID = D.ROWID;
           COMMIT;    
       
       END IF;
       COMMIT;
     
    END LOOP;
     -- Seteo las variables
     LN_EXISTE:=0;
     LN_PT_COSTO:=0;
    --Para saber si se asociaron los OCC al plan
    
    FOR F IN ASOCIARON_OCC LOOP 
   
       SELECT COUNT(*) INTO LN_EXISTE 
       FROM   MPULKTM1 G
       WHERE  TMCODE = F.TMCODE
       AND    SNCODE = F.SNCODE;
       
       IF LN_EXISTE = 0 THEN
         
           UPDATE GSI_PLANES_FEATURES
           SET    OBSERVACION = 'FEATURE NO EXISTE EN BSCS'
           WHERE  ROWID = F.ROWID; 
           COMMIT;    
       
       ELSE
         
           SELECT G.ACCESSFEE INTO LN_PT_COSTO 
           FROM   MPULKTM1 G
           WHERE  TMCODE = F.TMCODE
           AND    SNCODE = F.SNCODE;
       
           UPDATE GSI_PLANES_FEATURES
           SET    OBSERVACION = 'FEATURE EXISTE EN BSCS'
           WHERE  ROWID = F.ROWID;
           COMMIT;    
       
       END IF;
       --
       COMMIT;
       --
    END LOOP;
     --
      COMMIT;  
     --
     OPEN VERIFICO_FEATURES_BSCS;
     FETCH VERIFICO_FEATURES_BSCS INTO LC_FEATURES;
     CLOSE VERIFICO_FEATURES_BSCS;
     
     IF LC_FEATURES.VALOR <=0 THEN 
       LV_ERROR:='Los features no estan asociados a los planes en BSCS'; 
       RAISE LE_ERROR;
     END IF;
       
     --Para Obtener los costos de la TB ----
     INSERT INTO gsi_planes_features(ID_PLAN, TMCODE, ID_TIPO_DETALLE_SERV, SNCODE)
     SELECT DISTINCT ID_PLAN, TMCODE,'TB',2 FROM GSI_PLANES_FEATURES;
     -- Seteo las variables
     LN_EXISTE:=0;
     LN_PT_COSTO:=0;
     --Para sacar el valor de la TB
     FOR A IN VALOR_TB LOOP 
       SELECT COUNT(*) INTO LN_EXISTE 
       FROM   MPULKTM1 G
       WHERE  TMCODE = A.TMCODE
       AND    SNCODE = A.SNCODE;
       
       IF LN_EXISTE = 0 THEN
         
           UPDATE GSI_PLANES_FEATURES
           SET    OBSERVACION = 'FEATURE NO EXISTE EN BSCS'
           WHERE  ROWID = A.ROWID; 
           COMMIT;    
       
       ELSE
         
           SELECT G.ACCESSFEE INTO LN_PT_COSTO 
           FROM   MPULKTM1 G
           WHERE  TMCODE = A.TMCODE
           AND    SNCODE = A.SNCODE;
       
           UPDATE GSI_PLANES_FEATURES
           SET    VALOR = LN_PT_COSTO,
                  OBSERVACION = 'FEATURE EXISTE EN BSCS'
           WHERE  ROWID = A.ROWID;
           COMMIT;    
       
       END IF;
       COMMIT;
     
     END LOOP;
     --
     COMMIT;
     --
     
    LV_PARAMETRO_CLASE_AMX:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CLASES_AMX');
    
    IF LV_PARAMETRO_CLASE_AMX IS NULL THEN
      LV_ERROR:= 'No se obtuvo el id_calse_amx para consultar la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_COSTOS_FEATURES_BSCS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Cuota Mensual no cuadra con los features fijos + TB del plan';
    END IF;
      
    FOR CUOTA_TB_BSCS IN CUOTA_TB(LV_PARAMETRO_CLASE_AMX)
    LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.CUOTA_MENSUAL_BSCS,
                                            T.CUOTA_MENSUAL_AXIS,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_008',
                                              CUOTA_TB_BSCS.ID_PLAN,
                                              CUOTA_TB_BSCS.DESCRIPCION,
                                              CUOTA_TB_BSCS.CUOTA_MENSUAL_BSCS,
                                              CUOTA_TB_BSCS.CUOTA_MENSUAL_AXIS,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
    
    

    END LOOP;
     --
     COMMIT;
     --
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_008', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);     
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_008', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_008', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);  

  END PLP_COSTOS_FEATURES_QC;
  
 -- Paso 9: Revisa_Occ_No_Asociados_BSCS
 --Qc para revisar occs no asociados en bscs. el proceso se ejecuta a diario en bscs.
 PROCEDURE PLP_OCC_NOASOCIADOS_QC (PV_ERROR OUT VARCHAR2)IS
 
    --
   CURSOR OCC_NOASOCIADOS IS 
   SELECT T.ROWID,
          T.ID_DETALLE_PLAN,
          T.ID_TIPO_DETALLE_SERV,
          T.SNCODE,
          T.TMCODE,
          T.VERSION,
          T.ASOCIADO
      FROM GSI_FEATURE_PLAN T
      WHERE ASOCIADO IS NULL
      AND TMCODE IS NOT NULL;
     
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   TV_ROWID               TVARCHAR_50;
   TN_ID_DETALLE_PLAN     TNUMBER_10;
   TV_ID_TIPO_DETALLE     TVARCHAR;
   TN_SNCODE              TNUMBER;
   TN_TMOCODE             TNUMBER;
   TN_VERSION             TNUMBER;
   TV_ASOCIADO            TVARCHAR_1;
   LN_LIMITE              NUMBER:=600;
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_REVISA_OCC_NO_ASOCIADOS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    
    --   
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISA_OCC_NO_ASOCIADOS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Feature no esta asociado a plan en BSCS';
    END IF;
    --
    
    OPEN OCC_NOASOCIADOS;
    LOOP
      TV_ROWID.DELETE;
      TN_ID_DETALLE_PLAN.delete;
      TV_ID_TIPO_DETALLE.delete;
      TN_SNCODE.DELETE;
      TN_TMOCODE.DELETE;
      TN_VERSION.DELETE;
      TV_ASOCIADO.DELETE;
      
      FETCH OCC_NOASOCIADOS BULK COLLECT
        INTO TV_ROWID,
             TN_ID_DETALLE_PLAN,
             TV_ID_TIPO_DETALLE,
             TN_SNCODE,
             TN_TMOCODE,
             TN_VERSION,
             TV_ASOCIADO LIMIT LN_LIMITE;
    
      IF TV_ROWID.COUNT > 0 THEN
       FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
       INSERT 
             INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_DETALLE_PLAN,
                                            T.ID_TIPO_DETALLE_SERVICIOS,
                                            T.SNCODE,
                                            T.TMCODE ,
                                            T.QC_VERSION,
                                            T.ASOCIADO,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                            VALUES ('QC_009',
                                                    TN_ID_DETALLE_PLAN(M),
                                                    TV_ID_TIPO_DETALLE(M),
                                                    TN_SNCODE(M),
                                                    TN_TMOCODE(M),
                                                    TN_VERSION(M),
                                                    TV_ASOCIADO(M),
                                                    LV_MENSAJE_ERROR,
                                                    LV_USUARIO,
                                                    'A',
                                                    SYSDATE);

      --
      COMMIT;
      --
      END IF;
      EXIT WHEN (OCC_NOASOCIADOS%NOTFOUND);

      END LOOP;
     
      CLOSE OCC_NOASOCIADOS;
     
      -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
 END PLP_OCC_NOASOCIADOS_QC; 
 -- Paso 10: Diferentes_profiles_oper_axis
 --Qc para obtener los diferentes profiles de OPER a AXIS.
 PROCEDURE PLP_PROFILES_OPER_QC (PV_ERROR OUT VARCHAR2) IS
   --
   CURSOR PROFILES_OPER IS 
   SELECT A.CODI_PLAN,
       A.NOMB_PLAN      AS NOMBRE_PLAN,
       A.CODI_PLAN_TECN AS CODIGO_TN3_OPER,
       B.CODI_PLAN_TECN AS CODIGO_TN3_AXIS
   FROM CONTROL.CONT_PLUS_PLANES_DAT@prueba A,
       PORTA.CONT_PLUS_PLANES_DAT@AXIS B
   WHERE A.CODI_PLAN = B.CODI_PLAN
   AND A.CODI_RED = 'GSM'
   AND B.CODI_RED = 'GSM'
   AND A.CODI_PLAN_TECN <> B.CODI_PLAN_TECN;
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_DIF_PROFILES_OPER_AXIS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_010', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    
    --   
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_DIF_PROFILES_OPER_AXIS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error en profiles Oper vs Axis';
    END IF;
    --
    FOR PROFILES IN PROFILES_OPER
    LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.CODI_PLAN,
                                            T.DESCRIPCION,
                                            T.CODIGO_TN3_OPER,
                                            T.CODIGO_TN3_AXIS,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_010',
                                              PROFILES.CODI_PLAN,
                                              PROFILES.NOMBRE_PLAN,
                                              PROFILES.CODIGO_TN3_OPER,
                                              PROFILES.CODIGO_TN3_AXIS,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
    
    
   
    END LOOP;
      --
     COMMIT;
     --
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_010', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:=LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_010', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_010', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);  
 END PLP_PROFILES_OPER_QC;    
 -- Paso 11: Valida_Nombres_Incorrectos_en_Plan
 --Qc para obtener los nombre incorrecto en el plan de la tabla rateplan.
 PROCEDURE PLP_PLAN_INCORRECTOS_QC (PV_ERROR OUT VARCHAR2)IS
   --
   CURSOR NUMEROS_INCORRECTOS IS
   SELECT A.TMCODE,A.DES,A.SHDES FROM RATEPLAN A
   WHERE (DES LIKE '1%' OR
          DES LIKE '2%' OR
          DES LIKE '3%' OR
          DES LIKE '4%' OR
          DES LIKE '5%' OR
          DES LIKE '6%' OR
          DES LIKE '7%' OR
          DES LIKE '8%' OR
          DES LIKE '9%' OR 
          DES LIKE '0%')
   AND   SHDES NOT LIKE 'DT%';

    --
    CURSOR CARACTERES_ESP IS
    SELECT A.TMCODE,A.DES,A.SHDES FROM RATEPLAN A
    WHERE (DES LIKE '%?%' OR
           DES LIKE '%+%' ) 
    AND   SHDES NOT LIKE 'DT%';
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_VALIDA_NOM_INCORRECTOS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_011', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --   
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VALIDA_NOM_INCORRECTOS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error en nombre del Plan, caracteres raros';
    END IF;
    --
    FOR CARACTERES IN CARACTERES_ESP
    LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.TMCODE,
                                            T.DESCRIPCION,
                                            T.SHDES,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_011',
                                              CARACTERES.TMCODE,
                                              CARACTERES.DES,
                                              CARACTERES.SHDES,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
    
    
   
    END LOOP;
    --
     COMMIT;
    --
    
    FOR NUMEROS IN NUMEROS_INCORRECTOS
    LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.TMCODE,
                                            T.DESCRIPCION,
                                            T.SHDES,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_011',
                                              NUMEROS.TMCODE,
                                              NUMEROS.DES,
                                              NUMEROS.SHDES,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
    
    
   
    END LOOP;
      --
     COMMIT;
     --
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_011', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_011', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_011', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
 END  PLP_PLAN_INCORRECTOS_QC;
 -- Paso 12: Verificar_Servicios_entre_tablas_BSCS
 --Qc para Verificas los servicios de las tablas mpulktmm y mpulktmb.
 PROCEDURE PLP_VERIFICA_SERVICIOS (PV_ERROR OUT VARCHAR2) IS
   
   --
   CURSOR VERIFICA_SERVICIOS IS 
   SELECT DISTINCT TMCODE,VSCODE,SNCODE FROM MPULKTMM
   MINUS
   SELECT DISTINCT TMCODE, VSCODE,SNCODE FROM MPULKTMB; 
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

   BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_VERIFICAR_SERVICIOS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_012', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --   
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VERIFICAR_SERVICIOS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Verificar servicios en BSCS';
    END IF;
    --
    FOR VERIFICA IN VERIFICA_SERVICIOS
    LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.TMCODE,
                                            T.VSCODE,
                                            T.SNCODE,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_012',
                                              VERIFICA.TMCODE,
                                              VERIFICA.VSCODE,
                                              VERIFICA.SNCODE,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
    
    
   
    END LOOP;
    --
     COMMIT;
    --
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_012', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_012', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_012', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);    
 END   PLP_VERIFICA_SERVICIOS;
 -- Paso 13: Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF
 --Qc para Verificas las apn genericas no configuradas en IP_DIRECCIONES_REF de Axis.
 PROCEDURE PLP_APN_GENERICAS_QC (PV_ERROR OUT VARCHAR2) IS
   
   CURSOR VALOR_OMISION (CV_CATEGORY VARCHAR2) IS
   SELECT NAME AS VALOR FROM CL_TECNOMEN.PACKAGEDEFINITION@COLECTOR A
   WHERE A.CATEGORY_QUALIFIER = CV_CATEGORY
   AND SUBSTR(NAME,5,4) NOT IN ('1016','1995','2325','2509','2895','1828','2075','2846','APN')
   AND NAME LIKE 'AUT%'
   MINUS
   SELECT ID_TIPO_DETALLE_SERV FROM PORTA.IP_DIRECCIONES_TIPOS@AXIS
   WHERE ID_TIPO_DETALLE_SERV LIKE 'AUT%';
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_CATEGORY            VARCHAR2(3);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_VERIFICA_APN');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_013', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --   
    LV_CATEGORY:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CATEGORY_QUALIFIER_QC');
    
    IF LV_CATEGORY IS NULL THEN
      LV_CATEGORY:='12';
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VERIFICA_APN');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Verificar APN generico no configurado en IP_Direccciones_REF';
    END IF;
    --
    FOR C IN VALOR_OMISION (LV_CATEGORY)
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.VALOR,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_013',
                                              C.VALOR,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_013', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_013', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_013', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
 END  PLP_APN_GENERICAS_QC;
 -- Paso 14: Concilia_mk_mapea_plan_ext
 --Qc para detectar features mal configurados en PCRF.
 PROCEDURE PLP_CONCILIA_PLAN_QC (PV_ERROR OUT VARCHAR2) IS
   --
   CURSOR MAPEA_PLAN IS 
   SELECT ID_DETALLE_PLAN, ID_FEATURE
    FROM PORTA.MK_MAPEA_PLAN_EXT@AXIS A
    WHERE A.ID_EQUIPO IS NULL
    AND ESTADO = 'A'
    GROUP BY ID_DETALLE_PLAN, ID_FEATURE
    HAVING COUNT(*) > 1;
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);
  BEGIN
      -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_CONCILIA_PLAN_EXT');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_014', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CONCILIA_PLAN_EXT');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error detectar features mal configurados en PCRF';
    END IF;
    --
    FOR I IN MAPEA_PLAN
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_DETALLE_PLAN,
                                            T.ID_FEATURE,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_014',
                                              I.ID_DETALLE_PLAN,
                                              I.ID_FEATURE,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --  
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_014', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);         
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_014', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_014', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);  
  END  PLP_CONCILIA_PLAN_QC; 
 -- Paso 15: Revisa features m2m creados como MMS
 --Qc para verificar feature M2M2 creados como MMS
 PROCEDURE PLP_FEATURE_M2M2_QC (PV_ERROR OUT VARCHAR2) IS
  
   --
   CURSOR MAPEO_PLAN_CRE IS 
   SELECT A.PLAN_AXIS AS PLAN_AXIS,A.PLAN_CRE AS PLAN_CRE ,A.DESCRIPCION AS DESCRIPCION ,A.TIPO_SVA  AS TIPO_SVA 
   FROM ES_MAPEO_PLAN_CRE@espejo_rcc  A
   --FROM ES_MAPEO_PLAN_CRE@ESPEJO.CONECEL.COM  A --por pruebas
   WHERE A.DESCRIPCION LIKE '%M2M%'
   AND TIPO_SVA = 'MMS'; 
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_REVISA_FEATURES_MMS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_015', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_REVISA_FEATURES_MMS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Verifica feature M2M2 creados como MMS';
    END IF;
    --
    FOR G IN MAPEO_PLAN_CRE
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.PLAN_AXIS,
                                            T.PLAN_CRE,
                                            T.DESCRIPCION,
                                            T.TIPO_SVA,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_015',
                                              G.PLAN_AXIS,
                                              G.PLAN_CRE,
                                              G.DESCRIPCION,
                                              G.TIPO_SVA,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --    
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_015', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_015', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_015', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);    
 END  PLP_FEATURE_M2M2_QC;
 -- Paso 16: Revisa Profiles negativos_planes
 PROCEDURE PLP_PROFILES_NEGATIVOS_PLAN_QC (PV_ERROR OUT VARCHAR2) IS
   
   --
   CURSOR PLUS_PLANES_DAT IS 
   SELECT H.CODI_PLAN, H.NOMB_PLAN, H.CODI_PLAN_TECN
   FROM AXISFAC.CONT_PLUS_PLANES_DAT@AXIS H, GE_DETALLES_PLANES@AXIS K
   WHERE (CODI_PLAN_TECN LIKE '-%' OR CODI_PLAN_TECN IS NULL)
   AND   'BP'||H.CODI_PLAN = K.ID_PLAN
   AND    K.ID_CLASE <> 'DTH';
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_PROFILES_NEGATPLAN_QC');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_016', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PROFILES_NEGATPLAN_QC');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Revisar Profiles Negativos de Planes';
    END IF;
    --
    FOR P IN PLUS_PLANES_DAT
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.CODI_PLAN,
                                            T.DESCRIPCION,
                                            T.CODIGO,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_016',
                                              P.CODI_PLAN,
                                              P.NOMB_PLAN,
                                              P.CODI_PLAN_TECN,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_016', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);         
  EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_016', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_016', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
 END  PLP_PROFILES_NEGATIVOS_PLAN_QC;
 -- Paso 17: Revisa Profiles negativos_planes
 PROCEDURE PLP_PROFILES_NEGATIVOS_FEAT_QC (PV_ERROR OUT VARCHAR2) IS
     --
     LV_ERROR                VARCHAR2(1000);
     LE_ERROR                EXCEPTION;
     LV_MENSAJE_ERROR        VARCHAR2(1000);
     LV_TRANSACCION          VARCHAR2(1000);
     TYPE CURSOR_TYPE        IS REF CURSOR;
     LV_QUERY                LONG;
     LC_NEGT_FEAT            CURSOR_TYPE;
     LT_TABLA_NEGT_FEAT      GT_DIST_PROFILES_NEG_FEAT; 
     LV_PLAN_AXIS            LONG;
  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_PROFILES_NEGATFEAT_QC');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_017', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PROFILES_NEGATFEAT_QC');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Revisar Profiles Negativos de Features';
    END IF;
    --
    LV_QUERY:='SELECT A.PLAN_AXIS, DESCRIPCION, TIPO, TIPO_SVA, PLAN_TECNOMEN
               FROM ES_MAPEO_PLAN_CRE@espejo_rcc  A
               WHERE (A.PLAN_TECNOMEN LIKE ''-%'' OR A.PLAN_TECNOMEN IS NULL)
               AND A.PLAN_AXIS NOT IN (<PLAN_AXIS>)';
               
    
      LV_PLAN_AXIS:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PLAN_AXIS');
    
    IF LV_PLAN_AXIS IS NULL THEN
      LV_ERROR:= 'No se obtuvo los Planes de Axis de la tabla ES_MAPEO_PLAN_CRE de ESPEJO';
      RAISE LE_ERROR;
      
    END IF;
     LV_QUERY:=REPLACE(LV_QUERY,'<PLAN_AXIS>',LV_PLAN_AXIS);
    --
     OPEN  LC_NEGT_FEAT FOR LV_QUERY;
     LOOP
     LT_TABLA_NEGT_FEAT.DELETE;
     FETCH LC_NEGT_FEAT BULK COLLECT INTO LT_TABLA_NEGT_FEAT limit 600;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_NEGT_FEAT%ROWCOUNT < 1);
      IF LT_TABLA_NEGT_FEAT.COUNT > 0 THEN
        FOR M IN LT_TABLA_NEGT_FEAT.FIRST .. LT_TABLA_NEGT_FEAT.LAST
        LOOP
           INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                                 T.PLAN_AXIS,
                                                 T.DESCRIPCION,
                                                 T.TIPO,
                                                 T.TIPO_SVA,
                                                 T.PLAN_TECNOMEN,
                                                 T.MENSAJE_TECNICO,
                                                 T.USUARIO,
                                                 T.ESTADO,
                                                 T.FECHA_EVENTO)
                                      VALUES ('QC_017',
                                              LT_TABLA_NEGT_FEAT(M).PLAN_AXIS,
                                              LT_TABLA_NEGT_FEAT(M).DESCRIPCION,
                                              LT_TABLA_NEGT_FEAT(M).TIPO,
                                              LT_TABLA_NEGT_FEAT(M).TIPO_SVA,
                                              LT_TABLA_NEGT_FEAT(M).PLAN_TECNOMEN,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
      
         --
         COMMIT;
         --                                      
       END LOOP;
      END IF;
     EXIT WHEN (LC_NEGT_FEAT%NOTFOUND);
    END LOOP;
     --
     COMMIT;
     --
     CLOSE     LC_NEGT_FEAT; 

     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_017', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);           
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_017', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_017', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);   
 END PLP_PROFILES_NEGATIVOS_FEAT_QC; 
 -- Paso 18  Cuadra_TB_Planes_com_vs_bscs_Maxima_version_bscs
 -- Validar la TB configurado en COMERCIAL Versus lo configurado en BSCS y Axis
 -- Lo correcto es que la TB de Axis y BSCS sea igual a lo solicitado por COMERCIAL
 PROCEDURE PLP_TB_PLANES_COM_BSCS_QC (PV_ERROR OUT VARCHAR2) IS  

 
       --
       LV_ERROR                VARCHAR2(1000);
       LE_ERROR                EXCEPTION;
       LV_MENSAJE_ERROR        VARCHAR2(1000);
       LV_TRANSACCION          VARCHAR2(1000);
       LV_PARAMETRO_TIPO_PLAN  VARCHAR2(32000); 
       LV_PLANES_EXCLUIR       VARCHAR2(32000);
       TYPE CURSOR_TYPE        IS REF CURSOR;
       LV_QUERY                LONG;
       LC_CUADRA_PLANES        CURSOR_TYPE;
       LT_TABLA_PLANES         GT_DIST_CUADRA_TBDIF; 
    
   BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_TB_PLANES_COM_QC');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_018', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    -- 
    LV_PARAMETRO_TIPO_PLAN:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PLANES_CORPORATIVOS_QC');
    
    IF LV_PARAMETRO_TIPO_PLAN IS NULL THEN
      LV_ERROR:= 'No se obtuvo los ID_TIPO_PLAN para consultar la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    
    
    LV_PLANES_EXCLUIR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PLANES_A_EXCLUIR');
    
    IF LV_PLANES_EXCLUIR IS NULL THEN
      LV_ERROR:= 'No se obtuvo los PLANES_A_EXCLUIR para la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_TB_PLANES_COM_QC');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Cuadra TB_Planes_com_vs_bscs Maxima_version_bscs';
    END IF;
    --
              LV_QUERY:='SELECT B.ID_PLAN,
                        B.OBSERVACION,
                        REPLACE(A.TARIFA_BASICA, '','', ''.'') AS TARIFA_BASICA_COM,
                        D.ACCESSFEE AS TARIFA_BSCS,
                        A.TARIFA_BASICA - D.ACCESSFEE DIFERENCIA_TB_BSCS,
                        E.TARIFABASICA AS TARIFA_BASICA_AXIS,
                        A.TARIFA_BASICA - E.TARIFABASICA AS DIFERENCIA_TB_AXIS
                 FROM AXISFAC.DATOS_PLANES@AXIS A,
                      AXISFAC.GE_DETALLES_PLANES@AXIS B,
                      AXISFAC.BS_PLANES@AXIS C,
                      MPULKTMB D,
                      AXISFAC.CP_PLANES@AXIS E,
                      (SELECT TMCODE, MAX(VSCODE) AS VSCODE
                      FROM MPULKTMB
                      GROUP BY TMCODE) F
                 WHERE A.CODIGO = B.ID_PLAN
                 AND   b.id_subproducto in (''TAR'',''AUT'')
                 AND   b.ESTADO = ''A'' 
                 AND   b.id_clase=''GSM''       
                 AND B.ID_DETALLE_PLAN = C.ID_DETALLE_PLAN
                 AND C.COD_BSCS = D.TMCODE
                 AND B.ID_PLAN IN (select ID_PLAN from porta.ge_planes@AXIS a where ESTADO = ''A'' )
                 AND D.SNCODE = 2
                 AND A.CODIGO = E.ID_PLAN
                 AND A.TARIFA_BASICA IS NOT NULL
                 AND (REPLACE(A.TARIFA_BASICA, '','', ''.'') - D.ACCESSFEE <> 0 OR
                 REPLACE(A.TARIFA_BASICA, '','', ''.'') - D.ACCESSFEE IS NULL OR
                 REPLACE(A.TARIFA_BASICA, '','', ''.'') - E.TARIFABASICA <> 0 OR
                  REPLACE(A.TARIFA_BASICA, '','', ''.'') - E.TARIFABASICA IS NULL) 
                 AND D.TMCODE = F.TMCODE
                 AND D.VSCODE = F.VSCODE
                 AND   B.ID_TIPO_PLAN NOT IN ( <TIPO_PLAN> )
                 AND   B.ID_PLAN NOT IN ( <ID_PLANES>  )';
               
    LV_QUERY:=REPLACE(LV_QUERY,'<TIPO_PLAN>',LV_PARAMETRO_TIPO_PLAN);
    LV_QUERY:=REPLACE(LV_QUERY,'<ID_PLANES>',LV_PLANES_EXCLUIR);           
    
    --
     OPEN  LC_CUADRA_PLANES FOR LV_QUERY;
     LOOP
     LT_TABLA_PLANES.delete;
     FETCH LC_CUADRA_PLANES BULK COLLECT INTO LT_TABLA_PLANES limit 600;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_CUADRA_PLANES%ROWCOUNT < 1);
      IF LT_TABLA_PLANES.COUNT > 0 THEN
        FOR M IN LT_TABLA_PLANES.FIRST .. LT_TABLA_PLANES.LAST
        LOOP
           INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                                T.ID_PLAN,
                                                T.DESCRIPCION,
                                                T.TARIFA_BASICA_COM,
                                                T.TARIFA_BSCS,
                                                T.DIFERENCIA_TB_BSCS,
                                                T.TARIFA_BASICA_AXIS,
                                                T.DIFERENCIA_TB_AXIS,
                                                T.MENSAJE_TECNICO,
                                                T.USUARIO,
                                                T.ESTADO,
                                                T.FECHA_EVENTO)
                                      VALUES ('QC_018',
                                              LT_TABLA_PLANES(M).ID_PLAN,
                                              LT_TABLA_PLANES(M).OBSERVACION,
                                              LT_TABLA_PLANES(M).TARIFA_BASICA_COM,
                                              LT_TABLA_PLANES(M).TARIFA_BSCS,
                                              LT_TABLA_PLANES(M).DIFERENCIA_TB_BSCS,
                                              LT_TABLA_PLANES(M).TARIFA_BASICA_AXIS,
                                              LT_TABLA_PLANES(M).DIFERENCIA_TB_AXIS,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
      
         --
         COMMIT;
         --                                      

       
       END LOOP;
                            
      END IF;
     
     EXIT WHEN (LC_CUADRA_PLANES%NOTFOUND);
   
    END LOOP;
     --
     COMMIT;
     --
     CLOSE     LC_CUADRA_PLANES; 
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_018', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);       
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_018', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_018', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);    
 END PLP_TB_PLANES_COM_BSCS_QC; 
 
 -- Paso 19 Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF_TAR
 PROCEDURE PLP_APN_GENERICAS_TAR_QC (PV_ERROR OUT VARCHAR2) IS  
 
 CURSOR VALOR_OMISION (CV_CATEGORY VARCHAR2) IS
   SELECT NAME AS VALOR FROM CL_TECNOMEN.PACKAGEDEFINITION@COLECTOR A
   WHERE A.CATEGORY_QUALIFIER = CV_CATEGORY
   AND SUBSTR(NAME,5,4) NOT IN ('1016','1995','2325','2509','2895','1828','2075','2846','APN')
   AND NAME LIKE 'TAR%'
   MINUS
   SELECT ID_TIPO_DETALLE_SERV FROM PORTA.IP_DIRECCIONES_TIPOS@AXIS
   WHERE ID_TIPO_DETALLE_SERV LIKE 'TAR%';
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_CATEGORY            VARCHAR2(3);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_APN_GENERICAS_TAR');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_019', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
    --   
    LV_CATEGORY:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CATEGORY_QUALIFIER_QC');
    
    IF LV_CATEGORY IS NULL THEN
      LV_CATEGORY:='12';
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_APN_GENERICAS_TAR');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error APN_generico_no_configurado_en_IP_DIRECCIONES_REF_TAR';
    END IF;
    --
    FOR C IN VALOR_OMISION (LV_CATEGORY)
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.VALOR,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_019',
                                              C.VALOR,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_019', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_019', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_019', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
 END PLP_APN_GENERICAS_TAR_QC;
 -- Paso 20 VerificaFeatures_PCRF en cl_internet_wap
 PROCEDURE PLP_FEATURES_PCRF_QC (PV_ERROR OUT VARCHAR2) IS  
 
   CURSOR FEATURE_PCRF_QC IS
   SELECT A.ID_PLAN,
       A.DESCRIPCION,
       A.CANT_KB_PLAN,
       A.TIPO_PLAN,
       A.COSTO_PLAN,
       A.PRODUCTO
   FROM AXISFAC.CL_PLANES_INTERNET_WAP @AXIS A
   WHERE ID_PLAN IN (SELECT ID_TIPO_DETALLE_SERV
                     FROM AXISFAC.CL_TIPOS_DETALLES_SERVICIOS@AXIS A
                    WHERE UPPER(A.DESCRIPCION) LIKE '%WHA%'
                   UNION ALL
                   SELECT ID_TIPO_DETALLE_SERV
                     FROM AXISFAC.CL_TIPOS_DETALLES_SERVICIOS@AXIS A
                    WHERE UPPER(A.DESCRIPCION) LIKE '%FACEB%'
                   UNION ALL
                   SELECT ID_TIPO_DETALLE_SERV
                     FROM AXISFAC.CL_TIPOS_DETALLES_SERVICIOS@AXIS A
                    WHERE UPPER(A.DESCRIPCION) LIKE '%REDES%')
    AND ID_PLAN NOT IN ('PPA-1244',
                       'PPA-2955',
                       'PPA-2955A',
                       'PPA-2955R',
                       'PPA-9077',
                       'PPA-9077R');
   --                    
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);                   
   BEGIN
      -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_FEATURES_PCRF_QC');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_020', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);
      --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_FEATURES_PCRF_QC');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error Verifica_Features_PCRF en cl_internet_wap';
    END IF;
    --
    FOR C IN FEATURE_PCRF_QC 
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.VALOR,
                                            T.TIPO,
                                            T.CUOTA_MENSUAL_AXIS,
                                            T.PLAN_AXIS, 
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_020',
                                              C.ID_PLAN,
                                              C.DESCRIPCION,
                                              C.CANT_KB_PLAN,
                                              C.TIPO_PLAN,
                                              C.COSTO_PLAN,
                                              C.PRODUCTO,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_020', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);   
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_020', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_020', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
 END PLP_FEATURES_PCRF_QC;
 --Proceso principal
 PROCEDURE PLP_PRINCIPAL_QC (PV_FECHA_ACTUAL  IN VARCHAR2,
                             PV_ERROR         OUT VARCHAR2) IS
   
   --
    LV_ERROR               VARCHAR2(1000);
    LE_ERROR               EXCEPTION; 
    LV_ARCHIVO             VARCHAR2(1000);
    LN_ERROR               NUMBER;
   --
 BEGIN
    

    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_PRIN', 
                                PV_TRANSACCION => 'Inicio Proceso de Ejecucion de los planes QC', 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);   
    --Limpio la tabla de bitacora y la respaldo en una TMP
    GSI_PLANESQC.PLP_LIMPIA_TABLA_QC(PV_ERROR => LV_ERROR);
    
   
    --Ejecuta los todos los pasos planes QC.
    GSI_PLANESQC.PLP_ESCENARIOS_PLANES_QC(PV_FECHA_ACTUAL => PV_FECHA_ACTUAL,
                                          PV_ERROR        => LV_ERROR);
     
   
    --Genera el archivo excel
    GSI_PLANESQC.PLP_GENERERA_ARCHIVO_QC(PV_ARCHIVO      => LV_ARCHIVO,
                                         PV_ERROR        => LV_ERROR); 
     
     --Transfiere el archivo al servidor FINAN_27
    GSI_PLANESQC.PLP_TRANSFIERE_ARCHIVO_QC(PV_NOMBRE_ARCHIVO => LV_ARCHIVO, 
                                           PN_ERROR          => LN_ERROR, 
                                           PV_ERROR          => LV_ERROR);
    
       
     --
     GSI_PLANESQC.PLP_ENVIA_REPORTE_MAIL_QC(PV_NOMBRE_ARCHIVO => LV_ARCHIVO, 
                                            PV_ERROR          => LV_ERROR);
    
    
       
    
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_PRIN', 
                                       PV_TRANSACCION => 'Fin Proceso de Ejecucion de los planes QC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
    --
    EXCEPTION
      
          WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_PRIN', 
                                       PV_TRANSACCION => 'Proceso de Ejecucion de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
 END PLP_PRINCIPAL_QC;
 --Ejecuta los todos los pasos planes QC.
 PROCEDURE PLP_ESCENARIOS_PLANES_QC (PV_FECHA_ACTUAL IN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
   
    --
    LV_ERROR               VARCHAR2(1000);
    LE_ERROR               EXCEPTION; 
  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ESCN', 
                                       PV_TRANSACCION => 'Inicio Ejecucion de los escenarios de los planes QC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);   
    -- Paso 1: Insert a la tabla GSI_PLANES
     GSI_PLANESQC.PLP_OBTIENE_PLANES_QC (PV_ERROR =>LV_ERROR); 
    
    -- Paso 2: Cuadra_TB_Planes_com_vs_bscs
     GSI_PLANESQC.PLP_CUADRA_TB_PLANES_COM_BSCS (PV_ERROR =>LV_ERROR); 
     
    -- Paso 3: Query_costos_OCC
     GSI_PLANESQC.PLP_QUERY_COSTOS_OCC_QC (PV_ERROR =>LV_ERROR);  
      
    -- Paso 4: Revisar_Features_Consistentes_con_el_producto
     GSI_PLANESQC.PLP_TOLL_INTERCONEXION_QC (PV_ERROR =>LV_ERROR);
    
    -- Paso 5:  Revisar_Features_Consistentes_con_el_producto
     GSI_PLANESQC.PLP_FEATURES_CONSISTENTES_QC (PV_ERROR =>LV_ERROR); 
     
    -- Paso 6:  Diferencia de Tarifas en TN3
     GSI_PLANESQC.PLP_TARIFAS_TN3_QC (PV_FECHA_ACTUAL => PV_FECHA_ACTUAL,
                                      PV_ERROR        =>LV_ERROR);
      
    -- Paso 7:  Diferencia de Tarifas en BSCS
     GSI_PLANESQC.PLP_TARIFAS_BSCS_QC (PV_FECHA_ACTUAL => PV_FECHA_ACTUAL,
                                       PV_ERROR =>LV_ERROR); 
    
    -- Paso 8: Costos_Features_BSCS_vs_CuotaMensual
     GSI_PLANESQC.PLP_COSTOS_FEATURES_QC (PV_ERROR =>LV_ERROR);
      
    -- Paso 9: Revisa_Occ_No_Asociados_BSCS
     GSI_PLANESQC.PLP_OCC_NOASOCIADOS_QC (PV_ERROR =>LV_ERROR);
     
    -- Paso 10: Diferentes_profiles_oper_axis
     GSI_PLANESQC.PLP_PROFILES_OPER_QC (PV_ERROR =>LV_ERROR);
     
    -- Paso 11: Valida_Nombres_Incorrectos_en_Plan
     GSI_PLANESQC.PLP_PLAN_INCORRECTOS_QC (PV_ERROR =>LV_ERROR);
     
    -- Paso 12: Verificar_Servicios_entre_tablas_BSCS
     GSI_PLANESQC.PLP_VERIFICA_SERVICIOS (PV_ERROR =>LV_ERROR);
   
    -- Paso 13: Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF
     GSI_PLANESQC.PLP_APN_GENERICAS_QC (PV_ERROR =>LV_ERROR);
    
    -- Paso 14: Concilia_mk_mapea_plan_ext
     GSI_PLANESQC.PLP_CONCILIA_PLAN_QC (PV_ERROR =>LV_ERROR);
             
    -- Paso 15: Revisa features m2m creados como MMS
     GSI_PLANESQC.PLP_FEATURE_M2M2_QC (PV_ERROR =>LV_ERROR);   
    
    -- Paso 16: Revisa Profiles negativos_planes
     GSI_PLANESQC.PLP_PROFILES_NEGATIVOS_PLAN_QC (PV_ERROR =>LV_ERROR);   
      
    -- Paso 17: Revisa Profiles negativos_features
    GSI_PLANESQC.PLP_PROFILES_NEGATIVOS_FEAT_QC (PV_ERROR =>LV_ERROR);   
     
    -- Paso 18: Cuadra_TB_Planes_com_vs_bscs_Maxima_version_bscs        
    GSI_PLANESQC.PLP_TB_PLANES_COM_BSCS_QC (PV_ERROR =>LV_ERROR); 
      
    -- Paso 19: Verifica_APN_generico_no_configurado_en_IP_DIRECCIONES_REF_TAR
    GSI_PLANESQC.PLP_APN_GENERICAS_TAR_QC (PV_ERROR =>LV_ERROR);  
    
    -- Paso 20 VerificaFeatures_PCRF en cl_internet_wap
    GSI_PLANESQC.PLP_FEATURES_PCRF_QC(PV_ERROR => LV_ERROR);    
    
    --Paso 21. Verificacion de la tabla mk_mapea_plan_ext que no tenga configurado el campo ID_PLAN_EXT 1 Y 8 a la vez
    GSI_PLANESQC.PLP_VALIDA_MK_MAPEA_PLAN_EXT(PV_ERROR => LV_ERROR);          
    
    -- Paso 22 : Verifica planes con promocion de Megas que no se les haya configurado los features en la tabla mk_param_gen_aut
    
    GSI_PLANESQC.PLP_VALIDA_PROMO_MEGAS(PV_ERROR => LV_ERROR);
    -- Paso 23 : Verifica que el feature que se haya asociado a la promocion de sms sea promocional  o stand comercial
    
    GSI_PLANESQC.PLP_FEATURE_PROMOSMS (PV_ERROR => LV_ERROR);
    -- Paso 24 : Verificar Costos Features Cuota Mensual BSCS, Axis vs COM
    
    GSI_PLANESQC.PLP_CUOTA_BSCS_AXIS_COM(PV_ERROR => LV_ERROR);
    --INI PMERA 06/01/2016  --Se agregan nuevos QC 
    -- Paso 25 : Verificar que un feature Incluido No se encuentre configurado como Adicional
    
    GSI_PLANESQC.PLP_FEATURE_CONF_ADICIONAL (PV_ERROR => LV_ERROR);
    -- Paso 26 : Validar el campo ID_TIPO_PLAN_SUPTEL contra la Base de COM
    
    GSI_PLANESQC.PLP_TIPO_PLAN_SUPTEL (PV_ERROR => LV_ERROR);
    --FIN PMERA 06/01/2016  --Se agregan nuevos QC 
    
    --INI PMERA 25/01/2016  --Se agregan nuevos QC 
    
    -- Paso 27 : Valida el campo TIPO_PLAN contra la base de COM
    GSI_PLANESQC.PLP_VALIDA_TIPO_PLAN (PV_ERROR => LV_ERROR);
    --FIN PMERA 06/01/2016  --Se agregan nuevos QC 
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ESCN', 
                                       PV_TRANSACCION => 'Fin Ejecucion de los escenarios de los planes QC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    EXCEPTION
    
          WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
           -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ESCN', 
                                       PV_TRANSACCION => 'Ejecucion de los escenarios de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);   
  END PLP_ESCENARIOS_PLANES_QC;
  
  --Genera el archivo excel
  PROCEDURE PLP_GENERERA_ARCHIVO_QC (PV_ARCHIVO      OUT VARCHAR2,
                                     PV_ERROR        OUT VARCHAR2) IS
   
   
   --
   CURSOR EJECUTO_SENTENCIAS IS
   SELECT T.COD_SEQ,T.DESCRIPCION,T.EXPRESION_SQL,T.ETIQUETAS
   FROM GSI_SENTENCIAS_PLANESQC T
   WHERE T.ESTADO = 'A' 
   ORDER BY COD_SEQ  ASC;
   
   --
   CURSOR OBSERVACION (LV_COD VARCHAR2) IS
   SELECT T.OBSERVACION
   FROM GSI_LOG_PLANESQC T
   WHERE T.COD_SEQ = LV_COD AND t.TRANSACCION LIKE '%Fin%' AND T.FECHA_EVENTO =(SELECT MAX(S.FECHA_EVENTO)
   FROM GSI_LOG_PLANESQC S
   WHERE S.COD_SEQ = LV_COD);
  
  
    TYPE CURSOR_TYPE_T   IS REF CURSOR;
    LC_REGISTROS         CURSOR_TYPE_t;
    LF_FILE              SYS.UTL_FILE.FILE_TYPE;
    LV_DIRECTORIO        VARCHAR2(50); 
    LV_NOMBRE_ARCHIVO    VARCHAR2(50);  
    LV_LINEA             CLOB;
    LV_ERROR             VARCHAR2(1000);
    LE_ERROR             EXCEPTION;
    LD_FECHA             DATE;
    LL_VAR_AUX_1         LONG;
    LN_POSPIPE           PLS_INTEGER := 1;
    LN_POSNEXT           PLS_INTEGER := 1;
    LV_CADENA            LONG;
    LV_CONT              VARCHAR(20);
    LN_CONT_TMP          NUMBER:=2;
    LV_QUERY             LONG; 
    LL_VAR_AUX           LONG;
    LL_VALOR             LONG;
    LN_POSPIPE_REG       PLS_INTEGER := 1;
    LN_POSNEXT_REG       PLS_INTEGER := 1;
    LV_NOMBRE_PROGRAMA   VARCHAR2(100):='GSI_PLANESQC.PLP_GENERERA_ARCHIVO_QC';
    LN_CONTADOR          NUMBER:=0;
    LV_QUERY_QC          VARCHAR2(4000);
    LV_OBSE_TARIFAS      VARCHAR2(4000);
    LV_OBSERVACION       VARCHAR2(4000);
    
  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Inicio Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    --
    LV_DIRECTORIO:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_DIRECTORIO_QC');
    
    IF LV_DIRECTORIO IS NULL THEN
      LV_DIRECTORIO:='Error no se obtuvo el directorio';
      RAISE LE_ERROR;
    END IF;
    --
     LV_NOMBRE_ARCHIVO:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_NOMBRE_ARCH');
    
    IF LV_NOMBRE_ARCHIVO IS NULL THEN
      LV_NOMBRE_ARCHIVO:='Error no se obtuvo el nombre del archivo';
      RAISE LE_ERROR;
    END IF;
    --
     LD_FECHA:=TO_DATE(SYSDATE,'DD/MM/RRRR');    
    --
     LV_NOMBRE_ARCHIVO:= REPLACE(LV_NOMBRE_ARCHIVO,'<fecha>',TO_CHAR(LD_FECHA,'DDMMYYYY'));
    --
    
     LF_FILE := SYS.UTL_FILE.FOPEN(LV_DIRECTORIO, LV_NOMBRE_ARCHIVO, 'W');

     LV_LINEA := '<html><head><title>Automatizacion_de_planes_Qc</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
     
     LV_LINEA := '<style>
                .sombra      {background-color:#CCCCCC;}
                .cabecera    {font-weight:bold}
                .observacion2 {background-color:#004080; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .observacion {background-color:#FFFF33; font-weight:bold; text-align:center; vertical-align:center;}
                .celdavacia  {background-color:#EAEAEA;}
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .dettitulo2  {background-color:#000000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .col1_defi   {color:black; font-family:"Tahoma"; font-size:8.0pt; mso-font-charset:0; text-align:center; vertical-align:middle; white-space:normal;}
               ';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

     LV_LINEA := '<BODY>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

  --DETALLES DEL REPORTE GENERADO (FECHA)
     LV_LINEA := '<TABLE>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

          LV_LINEA := '<TR></TR>
           <TR>
           <TD></TD>
           <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7> Automatizaci&oacute;n de QC de planes </td>
           </TR>
           <TR>
           <TD></TD><TD ALIGN="RIGTH" COLSPAN=7><B> Fecha de Generaci&oacute;n:  '||to_char(sysdate, 'dd/mm/yyyy')||'</td>
           </TR>
           <TR></TR>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

     --CABECERA DEL REPORTE (TITULOS DE LOS CAMPOS DEL REPORTE)
     FOR G IN EJECUTO_SENTENCIAS
     LOOP
     
       LN_CONTADOR:=0;
       LV_CONT:=TO_CHAR(LN_CONT_TMP);
       
       
       IF G.COD_SEQ='QC_008_1' THEN 
         LN_CONT_TMP:=8;
         LV_CONT:=TO_CHAR(LN_CONT_TMP) || '.1';
         
       END IF; 
       
       IF G.COD_SEQ='QC_009_1' THEN 
         LN_CONT_TMP:=9;
         LV_CONT:=TO_CHAR(LN_CONT_TMP) || '.1';
       END IF;     
     --
     --TITULO DEL PASO DEL REPORTE
          LV_LINEA := '<TABLE><TR></TR><TR></TR>
           <TR>
           <TD></TD>
             <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7>' ||LV_CONT||'.- '||REPLACE(REPLACE(REPLACE(REPLACE(G.DESCRIPCION, '�', '&Ntilde;'), '�', '&ntilde;'), '�', '&oacute;'), '�', '&Oacute;')|| '</td>  
           </TR><TR></TR></TABLE>'; -- 16/11/2015 MMI (reemplaza tildes)
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
       LN_CONT_TMP:=LN_CONT_TMP+1;
         
       
         --
         OPEN OBSERVACION (G.COD_SEQ);
         FETCH OBSERVACION INTO LV_OBSE_TARIFAS;
         CLOSE OBSERVACION;
       
         IF G.COD_SEQ='QC_006' OR G.COD_SEQ='QC_007' THEN  
           LV_LINEA := '<TABLE>
              <TR>
              <TD></TD>
              <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7>' ||LV_OBSE_TARIFAS|| '</td>
              </TR><TR></TR></TABLE>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
         
           
        END IF;
         --
        LV_QUERY:=G.EXPRESION_SQL;
       OPEN LC_REGISTROS FOR LV_QUERY;
                    

       --
       LOOP
          FETCH LC_REGISTROS
            INTO LL_VALOR;
          IF   LC_REGISTROS%ROWCOUNT < 1 THEN
           BEGIN 
           LV_OBSERVACION:='';
           
           LV_QUERY_QC:='SELECT T.OBSERVACION
                         FROM GSI_LOG_PLANESQC T
                         WHERE  T.COD_SEQ=''' ||G.COD_SEQ ||''' AND T.ESTADO = ''E''
                         AND T.FECHA_EVENTO = (SELECT MAX(S.FECHA_EVENTO) 
                                               FROM GSI_LOG_PLANESQC S WHERE S.COD_SEQ = '''||G.COD_SEQ||''')'; 
           
           EXECUTE IMMEDIATE (LV_QUERY_QC) INTO LV_OBSERVACION;
           EXCEPTION 
                WHEN OTHERS THEN
                  NULL;
              END;
           
           --
           IF LV_OBSERVACION IS NULL THEN
              
              LV_OBSERVACION:=' No hay Casos ';
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA := '<TR><td></td>
                          <td  class=''observacion2'' style="border-style: solid;" > OBSERVACI&Oacute;N </td>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA); 
                LV_LINEA := '</TR></TABLE>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                --
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA :='<TR><td></td>
                <td class=''celdavacia2'' align=''center'' style="border-style: solid;"> '||LV_OBSERVACION||' </td>
                </TR><TR></TR>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
               LV_LINEA := '</TABLE>';
               SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
              
            ELSE
             
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA := '<TR><td></td>
                          <td  class=''dettitulo'' style="border-style: solid;" > OBSERVACI&Oacute;N </td>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA); 
                LV_LINEA := '</TR></TABLE>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                --
                LV_LINEA := '<TABLE >';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
                LV_LINEA :='<TR><td></td>
                <td class=''observacion'' align=''center'' style="border-style: solid;"> '||LV_OBSERVACION||' </td>
                </TR><TR></TR>';
                SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
               LV_LINEA := '</TABLE>';
               SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
           END IF;
               EXIT;
         ELSE
           LN_CONTADOR:=LN_CONTADOR+1;
           IF LN_CONTADOR = 1 THEN
            LV_CADENA:=G.ETIQUETAS;
            LN_POSPIPE := INSTR(LV_CADENA, '|', 1, 1);
            LV_LINEA := '<TABLE ><TD></TD>';
            SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
            LL_VAR_AUX_1:='';
            IF LN_POSPIPE > 0 THEN
             WHILE LN_POSPIPE > 0 LOOP
             
              LL_VAR_AUX_1 := TRIM(SUBSTR(LV_CADENA, LN_POSNEXT, LN_POSPIPE - LN_POSNEXT));
              IF LL_VAR_AUX_1 IS NOT NULL THEN
               LL_VAR_AUX_1 := SUBSTR(LV_CADENA, LN_POSNEXT, LN_POSPIPE - LN_POSNEXT);
              ELSE
               LL_VAR_AUX_1 := 'NULL';
              END IF;
                  --  
                  LV_LINEA := '<td  class=''dettitulo'' style="border-style: solid;" >'||LL_VAR_AUX_1||'</td>';
                  SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);                           
                LN_POSNEXT := LN_POSPIPE + 1;
                LN_POSPIPE := INSTR(LV_CADENA, '|', LN_POSNEXT, 1);
               END LOOP;
              END IF;    
              
              LV_LINEA := '</TABLE>';
              SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
              LV_CADENA:='';
              LN_POSNEXT:=1;
              LN_POSPIPE:=1;
              LV_LINEA := '<TABLE >';
              SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
            END IF;
            --
            
            LL_VAR_AUX:='';
            LV_LINEA :='<TR><TD></TD>';
            SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
            
           LN_POSPIPE_REG := INSTR(LL_VALOR, '|', 1, 1);
              
         
             IF LN_POSPIPE_REG > 0 THEN
             WHILE LN_POSPIPE_REG > 0 LOOP
           
               LL_VAR_AUX := TRIM(SUBSTR(LL_VALOR, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG));
              IF LL_VAR_AUX IS NOT NULL THEN
                 LL_VAR_AUX := SUBSTR(LL_VALOR, LN_POSNEXT_REG, LN_POSPIPE_REG - LN_POSNEXT_REG);
              ELSE
                 LL_VAR_AUX := 'NULL';
              END IF;
                 --
                  
              
                   LV_LINEA := '<td class=''celdavacia2'' align=''center'' style="border-style: solid;">  '||LL_VAR_AUX||' </td>';
                   SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

                 --      
                 LN_POSNEXT_REG := LN_POSPIPE_REG + 1;
                 LN_POSPIPE_REG := INSTR(LL_VALOR, '|', LN_POSNEXT_REG, 1);
                 
              
              END LOOP;
             
              END IF;  
              
               LV_LINEA :='</TR>';
               SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
               LL_VALOR:='';
               LN_POSNEXT_REG:=1;
              LN_POSPIPE_REG:=1;
          EXIT WHEN LC_REGISTROS%NOTFOUND;
         END IF;
        
        END LOOP;
        
           
           IF  LC_REGISTROS%ISOPEN THEN
          CLOSE LC_REGISTROS;
          END IF;
          --
         
     END LOOP;
   
   LV_LINEA := '</TABLE>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   LV_LINEA:='</BODY></HTML>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   
   UTL_FILE.FCLOSE(LF_FILE);
   PV_ARCHIVO := LV_NOMBRE_ARCHIVO; 
   -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
 
  EXCEPTION
    	WHEN LE_ERROR THEN
			PV_ERROR := 'ERROR - '||LV_ERROR;
			
      
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
		
     WHEN UTL_FILE.INVALID_PATH THEN 
            PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'RUTA NO VALIDA: '||'PLANES_QC_DIR'; 
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'RUTA NO VALIDA: '||'PLANES_QC_DIR',
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);

       WHEN UTL_FILE.INVALID_FILEHANDLE THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'NOMBRE ARCHIVO NO VALIDO: '||LV_NOMBRE_ARCHIVO; 

         -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'NOMBRE ARCHIVO NO VALIDO: '||LV_NOMBRE_ARCHIVO,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
       
       WHEN UTL_FILE.INVALID_OPERATION THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'OPERACION NO VALIDA: '||LV_NOMBRE_ARCHIVO; 
        -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'OPERACION NO VALIDA: '||LV_NOMBRE_ARCHIVO,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);

       WHEN UTL_FILE.WRITE_ERROR THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE ESCRITURA: '||LV_NOMBRE_ARCHIVO||' '||SQLERRM; 
  
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE ESCRITURA: '||LV_NOMBRE_ARCHIVO||' '||SQLERRM,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
      WHEN UTL_FILE.READ_ERROR THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE LECTURA DEL ARCHIVO: '||LV_NOMBRE_ARCHIVO; 
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE LECTURA DEL ARCHIVO: '||LV_NOMBRE_ARCHIVO,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);

    WHEN OTHERS THEN 
          PV_ERROR:=LV_NOMBRE_PROGRAMA||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500);                                
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_ARCH', 
                                       PV_TRANSACCION => 'Fin Generacion del Archivo de los planes QC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
  
  END  PLP_GENERERA_ARCHIVO_QC; 
  --Transfiere el archivo al servidor FINAN_27
  PROCEDURE PLP_TRANSFIERE_ARCHIVO_QC (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                       PN_ERROR           OUT NUMBER,
                                       PV_ERROR           OUT VARCHAR2)IS
  
    LV_IP                 VARCHAR2(100);
    LV_USUARIO_FTP        VARCHAR2(100);
    LV_RUTA_REMOTA        VARCHAR2(1000);
    LV_BORRAR_ARCH_LOCAL  VARCHAR2(3);
    LV_ERROR              VARCHAR2(1000);
    LN_ERROR              EXCEPTION;
    LV_DIRECTORIO         VARCHAR2(1000);
    LE_ERROR              EXCEPTION;
   -- LV_COMANDO            VARCHAR2(500);
   -- LV_SALIDA             VARCHAR2(500);
    LV_RUTA_LOCAL         VARCHAR2(100);
   
   BEGIN
     
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_TRAN', 
                                       PV_TRANSACCION => 'Inicio Transfiere Archivo al servidor de FINAN', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);    
     ------------------------
        -- LEO LOS PARAMETROS
     ------------------------
       --
         LV_IP:=PLF_PARAMETROS_QC('GSI_IP_SERVIDOR');

        IF LV_IP IS NULL THEN 
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO IP_SERVIDOR';
          RAISE LE_ERROR;
        END IF;
           
       --
        LV_USUARIO_FTP:=PLF_PARAMETROS_QC('GSI_USUARIO');

        IF LV_USUARIO_FTP IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO USUARIO DEL SERVIDOR';
          RAISE LE_ERROR;
        END IF; 
       --
        LV_RUTA_REMOTA:=PLF_PARAMETROS_QC('GSI_RUTA_REMOTA');
          
        IF LV_RUTA_REMOTA IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE RUTA DE LOCAL';
          RAISE LE_ERROR;
        END IF;
          
       --
        LV_DIRECTORIO:=PLF_PARAMETROS_QC('GSI_DIRECTORIO_QC');

        IF LV_DIRECTORIO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL DIRECTORIO';
          RAISE LE_ERROR;
        END IF;
       --
        LV_BORRAR_ARCH_LOCAL:=PLF_PARAMETROS_QC('GSI_BORRA_ARCHIVO');
          
        IF LV_BORRAR_ARCH_LOCAL IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE BORRAR O NO BORRAR EL ARCHIVO';
          RAISE LE_ERROR;
        END IF;
      --
       LV_RUTA_LOCAL:=PLF_PARAMETROS_QC('GSI_RUTA_LOCAL_QC');
          
        IF LV_RUTA_LOCAL IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA RUTA LOCAL';
          RAISE LE_ERROR;
        END IF;  
      --
      --LV_COMANDO := 'chmod 777 '||LV_RUTA_LOCAL||PV_NOMBRE_ARCHIVO;
    
      --Osutil.Runoscmd(Lv_Comando, Lv_Salida);
      --
      COMMIT;--POR PRUEBAS
      --  
      scp_dat.sck_ftp_gtw.scp_transfiere_archivo(PV_IP =>                 LV_IP,
                                                 PV_USUARIO =>            LV_USUARIO_FTP,
                                                 PV_RUTA_REMOTA =>        LV_RUTA_REMOTA,
                                                 PV_NOMBRE_ARCH =>        PV_NOMBRE_ARCHIVO,
                                                 PV_RUTA_LOCAL =>         LV_DIRECTORIO,
                                                 PV_BORRAR_ARCH_LOCAL =>  LV_BORRAR_ARCH_LOCAL,
                                                 PN_ERROR =>              PN_ERROR,
                                                 PV_ERROR =>              LV_ERROR);
    
    
     IF LV_ERROR IS NOT NULL OR PN_ERROR != 0 THEN
        RAISE LE_ERROR;
     END IF;
   
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_TRAN', 
                                       PV_TRANSACCION => 'Fin Transfiere Archivo al servidor de FINAN', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
   
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_TRANS', 
                                       PV_TRANSACCION => 'Transfiere Archivo al servidor de FINAN', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
          WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
           -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_TRANS', 
                                       PV_TRANSACCION => 'Transfiere Archivo al servidor de FINAN', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);  
  END PLP_TRANSFIERE_ARCHIVO_QC;     
  -- Envio de mail con correo adjunto                                      
  PROCEDURE PLP_ENVIA_REPORTE_MAIL_QC (PV_NOMBRE_ARCHIVO  IN VARCHAR2,
                                       PV_ERROR           OUT VARCHAR2) IS
    --
    LV_RUTA              VARCHAR2(1000);
    LV_ASUNTO            VARCHAR2(100);
    LV_MENSAJE           VARCHAR2(1000);
    LV_DESTINATARIO      VARCHAR2(1000);
    LV_REMITENTE         VARCHAR2(1000);
    LV_TIPO_REGISTRO     VARCHAR2(1000);
    LV_CLASE             VARCHAR2(1000);
    LV_PUERTO            VARCHAR2(1000);
    LV_SERVIDOR          VARCHAR2(1000);
    LV_MAXINTENTOS       VARCHAR2(1000);
    LV_PROGRAM           VARCHAR2(100):='GSI_PLANESQC.PLP_ENVIA_REPORTE_MAIL_QC';
    LV_ERROR             VARCHAR2(1000);
    LE_ERROR             EXCEPTION;
    LN_ID_SECUENCIA      NUMBER;
    LV_SALTO             VARCHAR2(5);
    LN_RESULT            NUMBER;
   
    
 
   BEGIN 
     --
     LV_SALTO :=CHR(13);                                   
     -- USUARIO
     SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
     FROM DUAL ;
     
      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_MAIL', 
                                       PV_TRANSACCION => 'Inicio Envio de Mail Adjunto', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
                                       
     ------------------------
        -- LEO LOS PARAMETROS
     ------------------------
       --
        LV_RUTA:=PLF_PARAMETROS_QC('GSI_RUTA_REMOTA');

        IF LV_RUTA IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER LA RUTA DEL DIRECTORIO DE FINAN';
          RAISE LE_ERROR;
        END IF;
       --
         LV_ASUNTO:=PLF_PARAMETROS_QC('GSI_ASUNTO_CORREO_QC');

        IF LV_ASUNTO IS NULL THEN 
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE ASUNTO';
          RAISE LE_ERROR;
        END IF;
           
       --
        LV_MENSAJE:=PLF_PARAMETROS_QC('GSI_MENSAJE_QC');

        IF LV_MENSAJE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE MENSAJE';
          RAISE LE_ERROR;
        END IF; 
       --
        LV_DESTINATARIO:=PLF_PARAMETROS_QC('GSI_DEST_CORREO_QC');
          
        IF LV_DESTINATARIO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE DESTINATARIO';
          RAISE LE_ERROR;
        END IF;
          
       --
        LV_REMITENTE:=PLF_PARAMETROS_QC('GSI_REMT_CORREO_QC');

        IF LV_REMITENTE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE REMITENTE';
          RAISE LE_ERROR;
        END IF;
       --
        LV_TIPO_REGISTRO:=PLF_PARAMETROS_QC('GSI_TIPO_ADJUNTO_QC');
          
        IF LV_TIPO_REGISTRO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE TIPO DE REGISTRO DE MAIL ADJUNTO';
          RAISE LE_ERROR;
        END IF;
      --
        LV_CLASE:=PLF_PARAMETROS_QC('GSI_CLASE_MAIL_QC');
          
        IF LV_CLASE IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA CLASE';
          RAISE LE_ERROR;
        END IF;
      --
        LV_MAXINTENTOS:=PLF_PARAMETROS_QC('GSI_MAXIMO_INT_QC');
          
        IF LV_MAXINTENTOS IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE MAXIMO DE INTENTOS';
          RAISE LE_ERROR;
        END IF;
      --
        LV_PUERTO:=PLF_PARAMETROS_QC('GSI_PUERTO_QC');
          
        IF LV_PUERTO IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DEL PUERTO DEL SERVIDOR DE FINAN';
          RAISE LE_ERROR;
        END IF;
       --
        LV_SERVIDOR:=PLF_PARAMETROS_QC('GSI_IP_SERVIDOR');
          
        IF LV_SERVIDOR IS NULL THEN
          LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA IP DEL SERVIDOR DE FINAN';
          RAISE LE_ERROR;
        END IF;     
       --
       LV_MENSAJE:=REPLACE(LV_MENSAJE,'<lv_usuario>',LV_USUARIO);
       LV_MENSAJE:=REPLACE(LV_MENSAJE,'<fecha>',TO_CHAR(SYSDATE,'DD/MM/YYYY'));
       LV_MENSAJE:=LV_MENSAJE||LV_SALTO||LV_SALTO||'Saludos cordiales.'||LV_SALTO||LV_SALTO||
                   '------------------------'||LV_SALTO||
                   ':: Este mensaje e-mail fue enviado automaticamente ::';
       
     
      --POR PRUEBA EN DESARROLLO
      LV_SERVIDOR:=NULL;
      LV_PUERTO:=NULL;
      --          
        LN_RESULT:= scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite  => LV_PROGRAM,
                                                                        pd_fechaenvio      => sysdate,
                                                                        pd_fechaexpiracion => sysdate+1,
                                                                        pv_asunto          => LV_ASUNTO,
                                                                        pv_mensaje         => LV_MENSAJE||'</ARCHIVO1='||PV_NOMBRE_ARCHIVO||'|DIRECTORIO1='||LV_RUTA||'|/>',
                                                                        pv_destinatario    => LV_DESTINATARIO,
                                                                        pv_remitente       => LV_REMITENTE,
                                                                        pv_tiporegistro    => LV_TIPO_REGISTRO,
                                                                        pv_clase           => LV_CLASE,
                                                                        pv_puerto          => LV_PUERTO,   
                                                                        pv_servidor        => LV_SERVIDOR, 
                                                                        pv_maxintentos     => TO_NUMBER(LV_MAXINTENTOS),
                                                                        pv_idusuario       => USER,
                                                                        pv_mensajeretorno  => LV_ERROR,
                                                                        pn_idsecuencia     => LN_ID_SECUENCIA);                            
       
      
      
           -- se maneja el error del envio del correo
                IF LV_ERROR IS NOT NULL OR LN_RESULT != 0 THEN
                 RAISE LE_ERROR;
                END IF;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_MAIL', 
                                       PV_TRANSACCION => 'Fin Envio de Mail Adjunto', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
   
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          --
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_MAIL', 
                                       PV_TRANSACCION => 'Envio de Mail Adjunto', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
          WHEN OTHERS THEN 
          PV_ERROR:=LV_PROGRAM||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500);                                
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_MAIL', 
                                       PV_TRANSACCION => 'Envio de Mail Adjunto', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_PROGRAM||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
  END PLP_ENVIA_REPORTE_MAIL_QC;
  --Limpio la tabla de bitacora y la respaldo en una TMP
  PROCEDURE PLP_LIMPIA_TABLA_QC (PV_ERROR OUT VARCHAR2) IS
    
    --
    LV_DELETE           VARCHAR2(1000);
    LV_NOMBRE_PROGRAMA  VARCHAR2(100):='GSI_PLANESQC.PLP_LIMPIA_TABLA_QC';
    LV_ERROR            VARCHAR2(500);
    --
   BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_LIMP', 
                                       PV_TRANSACCION => 'Inicio Se limpia la tabla GSI_BITACORA_PLANESQC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
   
         --
          INSERT INTO GSI_BITACORA_PLANESQC_HIST 
          SELECT * FROM GSI_BITACORA_PLANESQC;
          --
          LV_DELETE := 'TRUNCATE TABLE GSI_BITACORA_PLANESQC';

          EXECUTE IMMEDIATE (LV_DELETE);
  
           COMMIT;

      -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_LIMP', 
                                       PV_TRANSACCION => 'Fin Se limpia la tabla GSI_BITACORA_PLANESQC', 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
     
     EXCEPTION
      WHEN OTHERS THEN
       PV_ERROR := LV_NOMBRE_PROGRAMA||'-->ERROR - '||SQLERRM;
       ROLLBACK;
       -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_LIMP', 
                                       PV_TRANSACCION => 'Se limpia la tabla GSI_BITACORA_PLANESQC', 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => LV_NOMBRE_PROGRAMA||'-->ERROR - '||SQLERRM,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
  END PLP_LIMPIA_TABLA_QC;  
  
  -- Paso 21 : Valida mk_mapea_plan_ext con ID_PLAN_EXT 1 y 8 a la vez
  PROCEDURE PLP_VALIDA_MK_MAPEA_PLAN_EXT(PV_ERROR OUT VARCHAR2) IS
        
  CURSOR C1_VALIDA_MK_MAPEA IS  
      SELECT K.ID_FEATURE, K.ID_DETALLE_PLAN, K.ID_PLAN_EXT FROM PORTA.MK_MAPEA_PLAN_EXT@AXIS K
      WHERE ID_DETALLE_PLAN IN (SELECT ID_DETALLE_PLAN FROM PORTA.MK_MAPEA_PLAN_EXT@AXIS
                          WHERE  ID_PLAN_EXT IN (1,2,3,4,5,6) 
                          AND    ESTADO = 'A'
                          INTERSECT
                          SELECT ID_DETALLE_PLAN FROM PORTA.MK_MAPEA_PLAN_EXT@AXIS
                          WHERE  ID_PLAN_EXT IN (8,9) 
                          AND    ESTADO = 'A'
                         ) 
       AND ID_PLAN_EXT IN (1,2,3,4,5,6,8,9) 
       ORDER BY ID_DETALLE_PLAN;

   
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_VALIDA_MK_MAPEA_EXT');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_021', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VALIDA_MK_MAPEA_EXT');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error existen planes en la mk_mapea_plan_ext  con ID_PLAN_EXT 1 y 8 a la vez';
    END IF;
    --
    FOR C IN C1_VALIDA_MK_MAPEA
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ, --constante
                                            T.id_feature,
                                            T.id_detalle_plan,
                                            T.VALOR,  
                                            T.MENSAJE_TECNICO,--constante
                                            T.USUARIO,--constante
                                            T.ESTADO,--constante
                                            T.FECHA_EVENTO--constante
                                            )
                                      VALUES ('QC_021',--constante
                                              C.ID_FEATURE,
                                              C.ID_DETALLE_PLAN,
                                              C.ID_PLAN_EXT,                                              
                                              LV_MENSAJE_ERROR,--constante
                                              LV_USUARIO,--constante
                                              'A',--constante
                                              SYSDATE--constante
                                              );
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_021', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_021', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_021', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    
  END PLP_VALIDA_MK_MAPEA_PLAN_EXT;
  ---PMERA 18/12/2015
  -- Paso 22 : Verifica planes con promocion de Megas que no se les haya configurado los features en la tabla mk_param_gen_aut

  PROCEDURE PLP_VALIDA_PROMO_MEGAS(PV_ERROR OUT VARCHAR2) IS
        
    CURSOR PROMO_MEGAS IS  
     --
     SELECT distinct k.id_promocion, k.valor FROM PORTA.MK_PARAM_PROMO_AUT@AXIS k 
     WHERE ID_PROMOCION IN (SELECT ID_PROMOCION FROM PORTA.MK_PROMOCIONES_MAESTRA_AUT@AXIS B
                       WHERE  B.TIPO_PROMO = 'PROMO_BANDA_ANCHA'
                       AND SYSDATE BETWEEN B.FECHA_DESDE AND B.FECHA_HASTA)
     AND ID_PARAMETRO = 'PLANES_VALIDOS'
     MINUS 
     SELECT distinct d.id_promocion, d.id_parametro FROM PORTA.MK_PARAM_GEN_AUT@AXIS d;
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_PROMO_MEGAS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_022', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_PROMO_MEGAS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error al verificar planes con promocion de Megas que no se les haya configurado los features en la tabla mk_param_gen_aut';
    END IF;
    --
    FOR C IN PROMO_MEGAS
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ, --constante
                                            T.ID_PROMO,
                                            T.VALOR_OMISION,
                                            T.MENSAJE_TECNICO,--constante
                                            T.USUARIO,--constante
                                            T.ESTADO,--constante
                                            T.FECHA_EVENTO--constante
                                            )
                                      VALUES ('QC_022',--constante
                                              C.ID_PROMOCION,
                                              C.VALOR,                                             
                                              LV_MENSAJE_ERROR,--constante
                                              LV_USUARIO,--constante
                                              'A',--constante
                                              SYSDATE--constante
                                              );
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_022', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_022', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_022', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    
  END PLP_VALIDA_PROMO_MEGAS; 
  -- Paso 23 : Verifica que el feature que se haya asociado a la promocion de sms sea promocional  o stand comercial

  PROCEDURE PLP_FEATURE_PROMOSMS(PV_ERROR OUT VARCHAR2) IS
        
    CURSOR FEATURE_SMS IS
    SELECT DECODE(C.PK_TYPE_ID,0,'STANDARD COMERCIAL',1,'COMERCIAL',2,'PROMOCIONAL',5,'INCLUIDO',3,'REFUND',C.PK_TYPE_ID) TIPO_PAQUETE,
     C.NAME
     FROM CL_TECNOMEN.TEC_PACKAGEDEFINITION@COLECTOR C                                                                     
     WHERE NAME IN (SELECT DISTINCT VALOR FROM PORTA.MK_PARAM_PROMO_AUT@AXIS
     WHERE ID_PROMOCION IN (SELECT ID_PROMOCION FROM PORTA.MK_PROMOCIONES_MAESTRA_AUT@AXIS WHERE TIPO_PROMO='PROMO_SMS' 
                       AND    ESTADO = 'A'
                       AND    FECHA_HASTA >= SYSDATE)
                       AND    (VALOR LIKE  'TAR%' OR VALOR LIKE  'AUT%'))
     AND  C.PK_TYPE_ID NOT IN (2,0);
    
   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_FEATURE_SMS');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_023', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_FEATURE_SMS');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error al verificar que el feature que se haya asociado a la promocion de sms sea promocional  o stand comercial';
    END IF;
    --
    FOR C IN FEATURE_SMS
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ, --constante
                                            T.DESCRIPCION,
                                            T.VALOR_OMISION,
                                            T.MENSAJE_TECNICO,--constante
                                            T.USUARIO,--constante
                                            T.ESTADO,--constante
                                            T.FECHA_EVENTO--constante
                                            )
                                      VALUES ('QC_023',--constante
                                              C.TIPO_PAQUETE,
                                              C.NAME,                                             
                                              LV_MENSAJE_ERROR,--constante
                                              LV_USUARIO,--constante
                                              'A',--constante
                                              SYSDATE--constante
                                              );
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_023', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_023', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_023', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    
  END PLP_FEATURE_PROMOSMS; 
  
   -- Paso 24 : Verificar Costos Features Cuota Mensual BSCS, Axis vs COM

  PROCEDURE PLP_CUOTA_BSCS_AXIS_COM (PV_ERROR OUT VARCHAR2) IS
        
    CURSOR CUOTA_BSCS_AXIS_COM IS  
      SELECT B.*, A.CUOTA_MENSUAL AS CUOTA_MENSUAL_AXIS, D.CUPO_MENSUAL AS CUPO_MENSUAL_COM
      FROM  CP_PLANES@AXIS A, 
            (SELECT A.ID_PLAN,B.DESCRIPCION, SUM(A.VALOR) AS CUOTA_MENSUAL_BSCS
            FROM  GSI_PLANES_FEATURES A, GE_PLANES@AXIS B
            WHERE A.OBSERVACION2 IS NULL
            AND   A.ID_PLAN = B.ID_PLAN
            GROUP BY A.ID_PLAN,B.DESCRIPCION) B,
            GE_DETALLES_PLANES@AXIS C,
            AXISFAC.DATOS_PLANES@AXIS   D
      WHERE A.ID_PLAN = B.ID_PLAN
      AND   B.ID_PLAN = C.ID_PLAN
      AND   C.ID_CLASE_AMX <> '003' 
      AND   A.ID_PLAN = D.CODIGO
      AND   (TO_NUMBER(B.CUOTA_MENSUAL_BSCS) <> TO_NUMBER(D.CUPO_MENSUAL)
            OR TO_NUMBER(A.CUOTA_MENSUAL)  <> TO_NUMBER(D.CUPO_MENSUAL)
            )
      AND   D.CUPO_MENSUAL <> '0';


   --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);
   LV_PARAMETRO_CLASE_AMX VARCHAR2(5);


  BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_MENSUAL_AXIS_BSCS_COM');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
     
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_024', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    
    LV_PARAMETRO_CLASE_AMX:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_CLASES_AMX');
    
    IF LV_PARAMETRO_CLASE_AMX IS NULL THEN
      LV_ERROR:= 'No se obtuvo el id_calse_amx para consultar la tabla GE_DETALLES_PLANES de AXIS';
      RAISE LE_ERROR;
      
    END IF;
    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_MENSUAL_AXIS_BSCS_COM');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error al verificar Costos Features Cuota Mensual BSCS, Axis vs COM';
    END IF;
    --
    FOR C IN CUOTA_BSCS_AXIS_COM
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.CUOTA_MENSUAL_BSCS,
                                            T.CUOTA_MENSUAL_AXIS,
                                            T.CUPO_MENSUAL_COM,
                                            T.MENSAJE_TECNICO,
                                            T.USUARIO,
                                            T.ESTADO,
                                            T.FECHA_EVENTO)
                                      VALUES ('QC_024',
                                              C.ID_PLAN,
                                              C.DESCRIPCION,
                                              C.CUOTA_MENSUAL_BSCS,
                                              C.CUOTA_MENSUAL_AXIS,
                                              C.CUPO_MENSUAL_COM,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_024', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);        
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_024', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_024', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR); 
    
  END PLP_CUOTA_BSCS_AXIS_COM;
  --INI PMERA 06/01/2016  --Se agregan nuevos QC 
  -- Paso 25 : Verificar que un feature Incluido No se encuentre configurado como Adicional
  PROCEDURE PLP_FEATURE_CONF_ADICIONAL (PV_ERROR OUT VARCHAR2) IS
    
  
     LV_ERROR                VARCHAR2(1000);
     LE_ERROR                EXCEPTION;
     LV_MENSAJE_ERROR        VARCHAR2(1000);
     LV_TRANSACCION          VARCHAR2(1000);
     TYPE CURSOR_TYPE        IS REF CURSOR;
     LV_QUERY                LONG;
     LC_FEATURE_ADICONAL     CURSOR_TYPE;
     LT_TABLA_FEATURE        GT_FEATURE_ADICIONAL; 
     LV_FECHA_DESDE          VARCHAR2(50);
     LV_VALOR_OMISION        VARCHAR2(4000);
  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_FEATURE_ADICIONAL');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_025', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_FEATURE_ADICIONAL');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error en verificar que un feature Incluido No se encuentre configurado como Adicional';
    END IF;
    --
    LV_FECHA_DESDE:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_FECHA_CONF_ADICIONAL');
    
    IF LV_FECHA_DESDE IS NULL THEN
       LV_ERROR:='No se obtuvo la fecha desde para los features Incluidos que No se encuentre configurado como Adicional';
       RAISE LE_ERROR;
    END IF;
    --
    LV_VALOR_OMISION:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VALOR_OMISION_ADICIONAL');
    
    IF LV_VALOR_OMISION IS NULL THEN
       LV_ERROR:='No se obtuvo la cadena de valores de omision para los features incluidos no se encuentran configurados como adicional';
    END IF;
    
    LV_QUERY:='SELECT B.ID_TIPO_DETALLE_SERV, B.DESCRIPCION, A.ID_DETALLE_PLAN,
               DECODE(A.OBLIGATORIO || A.ACTUALIZABLE,
                             ''NS'', ''ADICIONAL'',
                             ''SN'', ''FIJO'',
                             ''SS'', ''FIJO VARIABLE'',
                             ''MAL CONFIGURADO'') TIPO_FEATURE ,NVL(A.OCULTO_ROL,''N'')
               FROM PORTA.CL_SERVICIOS_PLANES@AXIS A, PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXIS B
               WHERE A.ID_TIPO_DETALLE_SERV IN (SELECT PLAN_AXIS FROM PORTA.AXIS_MAPEO_PLAN_CRE@AXIS K
                                                WHERE K.PACKAGE_TYPE = 5)
               AND  A.ID_TIPO_DETALLE_SERV = B.ID_TIPO_DETALLE_SERV
               AND  A.VALOR_OMISION NOT IN ('||LV_VALOR_OMISION||')'||
              'AND  A.OBLIGATORIO = ''N''
               AND  A.ACTUALIZABLE = ''S''
               AND  A.FECHA_DESDE >= TO_DATE('||LV_FECHA_DESDE||')'; 
         
   OPEN  LC_FEATURE_ADICONAL FOR LV_QUERY;
     LOOP      
      LT_TABLA_FEATURE.DELETE;
     FETCH LC_FEATURE_ADICONAL BULK COLLECT INTO LT_TABLA_FEATURE limit 600;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_FEATURE_ADICONAL%ROWCOUNT < 1);
      IF LT_TABLA_FEATURE.COUNT > 0 THEN
        FORALL M IN LT_TABLA_FEATURE.FIRST .. LT_TABLA_FEATURE.LAST
        
           INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ,
                                                 T.ID_TIPO_DETALLE_SERVICIOS,
                                                 T.DESCRIPCION,
                                                 T.ID_DETALLE_PLAN,
                                                 T.TIPO_FEATURE,
                                                 T.OCULTO_ROL,
                                                 T.MENSAJE_TECNICO,
                                                 T.USUARIO,
                                                 T.ESTADO,
                                                 T.FECHA_EVENTO)
                                      VALUES ('QC_025',
                                              LT_TABLA_FEATURE(M).ID_TIPO_DETALLE_SERV ,
                                              LT_TABLA_FEATURE(M).DESCRIPCION,
                                              LT_TABLA_FEATURE(M).ID_DETALLE_PLAN,
                                              LT_TABLA_FEATURE(M).TIPO_FEATURE,
                                              LT_TABLA_FEATURE(M).OCULTO_ROL,
                                              LV_MENSAJE_ERROR,
                                              LV_USUARIO,
                                              'A',
                                              SYSDATE);
      
         --
         COMMIT;
         --                                      
      
      END IF;
     EXIT WHEN (LC_FEATURE_ADICONAL%NOTFOUND);
    END LOOP;
     --
     COMMIT;
     --
     CLOSE     LC_FEATURE_ADICONAL; 

     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_025', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);           
   EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_025', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_025', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);                  
  END PLP_FEATURE_CONF_ADICIONAL;
  
  -- Paso 26 : Validar el campo ID_TIPO_PLAN_SUPTEL contra la Base de COM
  PROCEDURE PLP_TIPO_PLAN_SUPTEL (PV_ERROR OUT VARCHAR2) IS
    
   CURSOR PLAN_SUPTEL IS
   SELECT A.ID_PLAN,
          B.DESCRIPCION,
          C.SUPTEL              AS ID_TIPO_PLAN_SUPTEL_COM,
          A.ID_CLASIFICACION_02 AS ID_TIPO_PLAN_SUPTEL_SIS
     FROM PORTA.GE_DETALLES_PLANES@AXIS A,
          PORTA.GE_PLANES@AXIS          B,
          AXISFAC.DATOS_PLANES@AXIS     C
    WHERE A.ID_PLAN = B.ID_PLAN
      AND A.ID_SUBPRODUCTO IN ('TAR', 'AUT')
      AND A.ESTADO = 'A'
      AND B.ESTADO = 'A'
      AND A.ID_PLAN = C.CODIGO
      AND C.SUPTEL <> A.ID_CLASIFICACION_02
      AND C.SUPTEL <> -1;
  --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_FEATURE_ADICIONAL');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_026', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_ID_TIPO_PLAN_SUPTEL');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error en validar el campo ID_TIPO_PLAN_SUPTEL contra la Base de COM';
    END IF;
    --
    FOR C IN PLAN_SUPTEL
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ, --constante
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.ID_TIPO_PLAN_SUPTEL_COM,
                                            T.ID_TIPO_PLAN_SUPTEL_SIS,
                                            T.MENSAJE_TECNICO,--constante
                                            T.USUARIO,--constante
                                            T.ESTADO,--constante
                                            T.FECHA_EVENTO--constante
                                            )
                                      VALUES ('QC_026',--constante
                                              C.ID_PLAN,
                                              C.DESCRIPCION,
                                              C.ID_TIPO_PLAN_SUPTEL_COM,
                                              C.ID_TIPO_PLAN_SUPTEL_SIS,                                             
                                              LV_MENSAJE_ERROR,--constante
                                              LV_USUARIO,--constante
                                              'A',--constante
                                              SYSDATE--constante
                                              );
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_026', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);       
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_026', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_026', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
                                       
  END PLP_TIPO_PLAN_SUPTEL;
  --FIN PMERA 06/01/2016  --Se agregan nuevos QC 
  
  --INI PMERA 25/01/2015 -- Se agregan nuevos QC
  -- Paso 27 : Valida el campo TIPO_PLAN contra la base de COM
   
  PROCEDURE PLP_VALIDA_TIPO_PLAN (PV_ERROR OUT VARCHAR2) IS
    
   CURSOR ID_TIPO_PLAN IS
    SELECT A.ID_PLAN,
          B.DESCRIPCION,
          C.TIPO_PLAN              AS ID_TIPO_PLAN_COM,
          A.ID_TIPO_PLAN           AS ID_TIPO_PLAN_SIS
     FROM PORTA.GE_DETALLES_PLANES@AXIS A,
          PORTA.GE_PLANES@AXIS          B,
          AXISFAC.DATOS_PLANES@AXIS     C
    WHERE A.ID_PLAN = B.ID_PLAN
      AND A.ID_SUBPRODUCTO IN ('TAR', 'AUT')
      AND A.ESTADO = 'A'
      AND B.ESTADO = 'A'
      AND A.ID_PLAN = C.CODIGO
      AND C.TIPO_PLAN <> A.ID_TIPO_PLAN
      AND C.TIPO_PLAN <> -1;
  --
   LV_ERROR               VARCHAR2(1000);
   LE_ERROR               EXCEPTION;
   LV_MENSAJE_ERROR       VARCHAR2(1000);
   LV_TRANSACCION         VARCHAR2(1000);

  BEGIN
    
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ;
    -- SE OBTIENE EL VALOR DE LA TRACCION 
    OPEN TRANSACCION (14793,'GSI_VALIDA_TIPO_PLAN');
    FETCH TRANSACCION INTO LV_TRANSACCION;
    CLOSE TRANSACCION;
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_027', 
                                PV_TRANSACCION => 'Inicio '||LV_TRANSACCION, 
                                PV_ESTADO      => 'F',
                                PV_OBSERVACION => NULL,
                                PV_USUARIO     => LV_USUARIO,
                                PV_ERROR       => LV_ERROR);

    --
    LV_MENSAJE_ERROR:=GSI_PLANESQC.PLF_PARAMETROS_QC('GSI_VALIDA_TIPO_PLAN');
    
    IF LV_MENSAJE_ERROR IS NULL THEN
      LV_MENSAJE_ERROR:='Error en valida el campo TIPO_PLAN contra la base de COM';
    END IF;
    --
    FOR C IN ID_TIPO_PLAN
     LOOP 
      INSERT INTO GSI_BITACORA_PLANESQC T  (T.COD_SEQ, --constante
                                            T.ID_PLAN,
                                            T.DESCRIPCION,
                                            T.TIPO_PLAN_COM,
                                            T.TIPO_PLAN_SIS,
                                            T.MENSAJE_TECNICO,--constante
                                            T.USUARIO,--constante
                                            T.ESTADO,--constante
                                            T.FECHA_EVENTO--constante
                                            )
                                      VALUES ('QC_027',--constante
                                              C.ID_PLAN,
                                              C.DESCRIPCION,
                                              C.ID_TIPO_PLAN_COM,
                                              C.ID_TIPO_PLAN_SIS,                                       
                                              LV_MENSAJE_ERROR,--constante
                                              LV_USUARIO,--constante
                                              'A',--constante
                                              SYSDATE--constante
                                              );
     END LOOP;
     --
     COMMIT;
     --   
     -- Se invoca el proceso de bitacorizacion para finalizar el proceso en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_027', 
                                       PV_TRANSACCION => 'Fin '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'F',
                                       PV_OBSERVACION => NULL,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);       
    EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:='ERROR - '||LV_ERROR;
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion en caso de error y se registra en la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_027', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
    
    
          
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);                               
          --
          ROLLBACK;
          -- Se invoca el proceso de bitacorizacion para la tabla de log
           PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_027', 
                                       PV_TRANSACCION => 'Error '||LV_TRANSACCION, 
                                       PV_ESTADO      => 'E',
                                       PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                       PV_USUARIO     => LV_USUARIO,
                                       PV_ERROR       => LV_ERROR);
                                       
  END PLP_VALIDA_TIPO_PLAN;
  --FIN PMERA 25/01/2015 -- Se agregan nuevos QC
  
END GSI_PLANESQC;
/
