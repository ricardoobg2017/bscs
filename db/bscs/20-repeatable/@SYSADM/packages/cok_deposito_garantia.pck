create or replace package cok_deposito_garantia is

  -- Author  : KSANCHEZ
  -- Created : 04/02/2013 11:46:29
  -- Purpose : 
  
  -- Public type declarations


PROCEDURE deposito_garantia(pv_region varchar2,
                            pdfechcierreperiodo date,
                            pn_coderror  OUT NUMBER,
                            pv_error     OUT varchar2);

PROCEDURE inserta_registro(pv_region                IN VARCHAR2,
                           pv_ciclo                 IN VARCHAR2,
                           pv_cuenta                IN VARCHAR2,
                           pv_nombres               IN VARCHAR2,
                           pv_apellidos             IN VARCHAR2,
                           pv_ruc                   IN VARCHAR2,
                           pv_forma_pago            IN VARCHAR2,
                           pd_fecha_apertura_plan   IN VARCHAR2,
                           pv_nombre_vendedor       IN VARCHAR2,
                           pv_usuario_ingreso_gar   IN VARCHAR2,
                           pn_solicitud             IN NUMBER,
                           pv_tipo_garantia         IN VARCHAR2,
                           pd_fec_ing_garantia      IN VARCHAR2,
                           pd_fec_cad_garantia      IN VARCHAR2,
                           pn_valor_deposito        IN NUMBER,
                           pd_fecha_aplicacion      IN VARCHAR2,
                           pd_fecha_renuncia        IN VARCHAR2,
                           pv_mayor_vencido         IN VARCHAR2,
                           pn_total_cargos          IN NUMBER,
                           pn_total_consumos        IN NUMBER,
                           pn_total_deuda           IN NUMBER,                          
                           pv_status_cuenta         IN VARCHAR2,
                           pv_bp_plan	              IN VARCHAR2,
                           pv_detalle_plan	        IN varchar2,
                           pv_error                 OUT VARCHAR2);


end cok_deposito_garantia;
/
create or replace package body cok_deposito_garantia is
  -------------------------------------------------------------------------------------------
  -- Modificado por      : CLS Kerlin Sanchez.
  -- Fecha               : 13/04/2016
  -- Lider Claro         : Jackelinne G�mez
  -- Lider PDS           : Mariuxi Jofat
  -- PROYECTO            : [10717]ACTIVACI�N AUTOM�TICA SOL. CRED. CON DEPOSITO EN GARANT�A
  -- Objetivo            : Generacion de Reportes de Movimientos y Aplicacion de Dep. en Gar.
  -------------------------------------------------------------------------------------------

PROCEDURE deposito_garantia(pv_region varchar2,
                            pdfechcierreperiodo date,
                            pn_coderror  OUT NUMBER,
                            pv_error     OUT varchar2) IS



CURSOR c_existe(pv_tabla VARCHAR2) IS
SELECT 'X' 
FROM all_tables a
WHERE upper(a.TABLE_NAME) = upper(pv_tabla);

lv_fecha        varchar2(8) := to_char(pdfechcierreperiodo, 'ddMMyyyy');
lv_fecha2        varchar2(8) := to_char(pdfechcierreperiodo,'MON','NLS_DATE_LANGUAGE=spanish');
lv_tabla_origen varchar2(60) := 'CO_REPCARCLI_'||lv_fecha;
lv_tabla_origen2 varchar2(60) := 'BS_FACT_' ||lv_fecha2||'@COLECTOR' ;
lv_existe       varchar2(1);
lv_error        VARCHAR2(1000);
le_error        EXCEPTION;
lv_sentencia    VARCHAR2(5000);
ln_cod_err      NUMBER:=0;
src_cur         INTEGER;
ignore          INTEGER;
--ln_commit         number := 0;
lv_region             varchar2(10);
region                varchar2(10); 
ciclo                 varchar2(10); 
cuenta                varchar2(24); 
nombres               varchar2(40); 
apellidos             varchar2(40); 
ruc                   varchar2(20); 
forma_pago            varchar2(60); 
fecha_apertura_plan   varchar2(10); 
vendedor              varchar2(80); 
usuario_ingreso_gar   varchar2(40); 
solicitud             varchar2(10); 
tipo_garantia         varchar2(30); 
fec_ing_garantia      varchar2(10); 
fec_cad_garantia      varchar2(10); 
valor_deposito        varchar2(10); 
fecha_aplicacion      varchar2(10); 
fecha_renuncia        varchar2(10); 
mayor_vencido         varchar2(10); 
total_cargos          varchar2(10); 
total_consumos        varchar2(10); 
total_deuda           varchar2(10); 
status_cuenta         varchar2(20); 
bp_plan               varchar2(20); 
detalle_plan          varchar2(20);




BEGIN 


IF pdfechcierreperiodo IS  NULL THEN
   ln_cod_err := -5;
   lv_error :='fecha no puede ser nula';
   RAISE le_error;
 END IF;
 
 OPEN c_existe(lv_tabla_origen);
 FETCH c_existe INTO lv_existe;
 CLOSE c_existe;
 
 IF nvl(lv_existe,'N') <> 'X' THEN
   ln_cod_err := -1;
   lv_error :='Tabla '|| lv_tabla_origen||' no esta creada';
   RAISE le_error;
 END IF; 
 
  OPEN c_existe(lv_tabla_origen2);
 FETCH c_existe INTO lv_existe;
 CLOSE c_existe;
 
 IF nvl(lv_existe,'N') <> 'X' THEN
   ln_cod_err := -1;
   lv_error :='Tabla '|| lv_tabla_origen2||' no esta creada';
   RAISE le_error;
 END IF; 
 IF (pv_region='3') THEN
   lv_region:='(1,2)';
 ELSE
   lv_region:=pv_region;
 END IF;
 
   Execute Immediate 'TRUNCATE TABLE DEP_GARANTIA_TMP';  
 
   lv_sentencia := ' select /*+ RULE */ '||
  ' a.costcenter_id, cc.id_ciclo, c.cuenta, c.nombres, c.apellidos, c.ruc, c.forma_pago, c.fech_aper_cuenta fecha_apertura_plan,'|| 
  ' dv.nombre vendedor, dg.id_usuario_ingreso, dg.id_solicitud, dg.id_tipo_garantia, dg.fecha_ingreso,'||
  ' dg.fecha_vencimiento, dg.valor_deposito, dg.fecha_aplicacion, dg.fecha_renuncia, c.mayorvencido,'||
  ' nvl((select to_number(replace(fa.valor, '','', ''.'')) from ' || lv_tabla_origen2 || ' fa where fa.telefono is null and fa.cuenta = dg.codigo_doc and fa.descripcion1 = ''Cargo a Facturaci�n''),0) total_cargos,'|| 
  ' nvl((select to_number(replace(fa.valor, '','', ''.'')) from ' || lv_tabla_origen2 || ' fa where fa.telefono is null and fa.cuenta = dg.codigo_doc and fa.descripcion1 = ''Consumos del Mes''),0) total_consumos,'|| 
  ' (nvl((select to_number(replace(fa.valor, '','', ''.''))from ' || lv_tabla_origen2 || ' fa where fa.telefono is null and fa.cuenta = dg.codigo_doc'|| 
  ' and fa.descripcion1 = ''Cargo a Facturaci�n''),0)+nvl((select to_number(replace(fa.valor, '','', ''.''))from ' || lv_tabla_origen2 || ' fa where fa.telefono is null'|| 
  ' and fa.cuenta = dg.codigo_doc and fa.descripcion1 = ''Consumos del Mes''),0)) total_deuda,  r.rs_desc, y.id_plan, y.id_detalle_plan'||
  ' from customer_all a,' || lv_tabla_origen || ' c,'||
  ' fa_ciclos_axis_bscs cc,'||
  ' porta.cr_deposito_garantia_slctd@axis dg,'||
  ' (select distinct (x.id_detalle_plan),x.id_plan, x.id_deposito_garantia '||
                     'from porta.cr_detalle_deposito_slctd@axis x where x.id_deposito_garantia in'|| 
                '(select a.id_deposito_garantia from porta.cr_deposito_garantia_slctd@axis a)) y, '||
  ' reasonstatus_all r,'|| 
  ' (select distinct(vendedor) ven, nombre '||
    '  from porta.in_vendedor_axis_cobra@axis)  dv'||
  ' where  a.costcenter_id in '||lv_region||''||
  ' and c.cuenta = dg.codigo_doc'||
  ' and c.id_cliente = a.customer_id '||
  ' and dg.id_deposito_garantia = y.id_deposito_garantia(+)'||
  ' and a.csreason = r.rs_id '||
  ' and a.billcycle = cc.id_ciclo_admin '||
  ' and dg.id_estado_deposito not in(1,3) '||
  ' and dg.id_vendedor_persona = dv.ven'; 
  
  src_cur := dbms_sql.open_cursor;
  -- parse the SELECT statement
  dbms_sql.parse(src_cur, lv_sentencia, dbms_sql.NATIVE);
  dbms_sql.define_column(src_cur,1,region,10);                 
  dbms_sql.define_column(src_cur,2,ciclo,10);                   
  dbms_sql.define_column(src_cur,3,cuenta,24);                 
  dbms_sql.define_column(src_cur,4,nombres,40);                
  dbms_sql.define_column(src_cur,5,apellidos,40);              
  dbms_sql.define_column(src_cur,6,ruc,20);                    
  dbms_sql.define_column(src_cur,7,forma_pago,60);             
  dbms_sql.define_column(src_cur,8,fecha_apertura_plan,10);    
  dbms_sql.define_column(src_cur,9,vendedor,80);               
  dbms_sql.define_column(src_cur,10,usuario_ingreso_gar,40);   
  dbms_sql.define_column(src_cur,11,solicitud,10);             
  dbms_sql.define_column(src_cur,12,tipo_garantia,30);         
  dbms_sql.define_column(src_cur,13,fec_ing_garantia,10);      
  dbms_sql.define_column(src_cur,14,fec_cad_garantia,10);      
  dbms_sql.define_column(src_cur,15,valor_deposito,10);        
  dbms_sql.define_column(src_cur,16,fecha_aplicacion,10);      
  dbms_sql.define_column(src_cur,17,fecha_renuncia,10);        
  dbms_sql.define_column(src_cur,18,mayor_vencido,10);         
  dbms_sql.define_column(src_cur,19,total_cargos,10);          
  dbms_sql.define_column(src_cur,20,total_consumos,10);        
  dbms_sql.define_column(src_cur,21,total_deuda,10);           
  dbms_sql.define_column(src_cur,22,status_cuenta,20);         
  dbms_sql.define_column(src_cur,23,bp_plan,20);               
  dbms_sql.define_column(src_cur,24,detalle_plan,20);          
  ignore := dbms_sql.execute(src_cur);                     
                                                                        
    LOOP                                                               
    -- Fetch a row from the source table
       IF dbms_sql.fetch_rows(src_cur) > 0 THEN
          dbms_sql.column_value(src_cur,1,region);                                                    
          dbms_sql.column_value(src_cur,2,ciclo);                                 
          dbms_sql.column_value(src_cur,3,cuenta);                      
          dbms_sql.column_value(src_cur,4,nombres);                     
          dbms_sql.column_value(src_cur,5,apellidos);                  
          dbms_sql.column_value(src_cur,6,ruc);                              
          dbms_sql.column_value(src_cur,7,forma_pago);                
          dbms_sql.column_value(src_cur,8,fecha_apertura_plan);    
          dbms_sql.column_value(src_cur,9,vendedor);             
          dbms_sql.column_value(src_cur,10,usuario_ingreso_gar);           
          dbms_sql.column_value(src_cur,11,solicitud);                  
          dbms_sql.column_value(src_cur,12,tipo_garantia);           
          dbms_sql.column_value(src_cur,13,fec_ing_garantia);              
          dbms_sql.column_value(src_cur,14,fec_cad_garantia);                
          dbms_sql.column_value(src_cur,15,valor_deposito);                  
          dbms_sql.column_value(src_cur,16,fecha_aplicacion);              
          dbms_sql.column_value(src_cur,17,fecha_renuncia);                 
          dbms_sql.column_value(src_cur,18,mayor_vencido);            
          dbms_sql.column_value(src_cur,19,total_cargos);             
          dbms_sql.column_value(src_cur,20,total_consumos);         
          dbms_sql.column_value(src_cur,21,total_deuda);            
          dbms_sql.column_value(src_cur,22,status_cuenta );         
          dbms_sql.column_value(src_cur,23,bp_plan);                
          dbms_sql.column_value(src_cur,24,detalle_plan);           
                                                                       
          inserta_registro(pv_region                 => region,
                           pv_ciclo                  => ciclo,
                           pv_cuenta                 => cuenta,
                           pv_nombres                => nombres,
                           pv_apellidos              => apellidos,
                           pv_ruc                    => ruc,
                           pv_forma_pago             => forma_pago,
                           pd_fecha_apertura_plan    => to_date(fecha_apertura_plan,'dd/mm/yyyy'),
                           pv_nombre_vendedor        => vendedor,
                           pv_usuario_ingreso_gar    => usuario_ingreso_gar,
                           pn_solicitud              => to_number(solicitud),
                           pv_tipo_garantia          => tipo_garantia,
                           pd_fec_ing_garantia       => to_date(fec_ing_garantia,'dd/mm/yyyy'),
                           pd_fec_cad_garantia       => to_date(fec_cad_garantia,'dd/mm/yyyy'),
                           pn_valor_deposito         => valor_deposito,
                           pd_fecha_aplicacion       => to_date(fecha_aplicacion,'dd/mm/yyyy'),
                           pd_fecha_renuncia         => to_date(fecha_renuncia,'dd/mm/yyyy'),
                           pv_mayor_vencido          => mayor_vencido,
                           pn_total_cargos           => to_number(total_cargos),
                           pn_total_consumos         => to_number(total_consumos),
                           pn_total_deuda            => to_number(total_deuda),
                           pv_status_cuenta          => status_cuenta,
                           pv_bp_plan	               => bp_plan,
                           pv_detalle_plan           => detalle_plan,
                           pv_error                  => lv_error); 
        ELSE                  
           EXIT; -- No more rows                   
        END IF;
                
        commit;
                
    END LOOP;
    -- close all cursors
dbms_sql.close_cursor(src_cur); --Cierro el cursor
       
--///////////////////////////////////////////////////////////////////////////

pn_coderror :=0;

EXCEPTION
  WHEN le_error THEN
    pn_coderror := ln_cod_err;
    pv_error := lv_error;
  WHEN OTHERS THEN
    pn_coderror :=-2;
    pv_error :=SQLERRM;
  
END deposito_garantia;

PROCEDURE inserta_registro(pv_region                IN VARCHAR2,
                           pv_ciclo                 IN VARCHAR2,
                           pv_cuenta                IN VARCHAR2,
                           pv_nombres               IN VARCHAR2,
                           pv_apellidos             IN VARCHAR2,
                           pv_ruc                   IN VARCHAR2,
                           pv_forma_pago            IN VARCHAR2,
                           pd_fecha_apertura_plan   IN VARCHAR2,
                           pv_nombre_vendedor       IN VARCHAR2,
                           pv_usuario_ingreso_gar   IN VARCHAR2,
                           pn_solicitud             IN NUMBER,
                           pv_tipo_garantia         IN VARCHAR2,
                           pd_fec_ing_garantia      IN VARCHAR2,
                           pd_fec_cad_garantia      IN VARCHAR2,
                           pn_valor_deposito        IN NUMBER,
                           pd_fecha_aplicacion      IN VARCHAR2,
                           pd_fecha_renuncia        IN VARCHAR2,
                           pv_mayor_vencido         IN VARCHAR2,
                           pn_total_cargos          IN NUMBER,
                           pn_total_consumos        IN NUMBER,
                           pn_total_deuda           IN NUMBER,
                           pv_status_cuenta         IN VARCHAR2,                           
                           pv_bp_plan	              IN VARCHAR2,
                           pv_detalle_plan	        IN varchar2,
                           pv_error                 OUT VARCHAR2)IS
  

                      
BEGIN

  INSERT INTO dep_garantia_tmp(region,
                               ciclo,
                               cuenta,
                               nombres,
                               apellidos,
                               ruc,
                               forma_pago,
                               fecha_apertura_plan,
                               nombre_vendedor,
                               usuario_ingreso_gar,
                               solicitud,
                               tipo_garantia,
                               fec_ing_garantia,
                               fec_cad_garantia,
                               valor_deposito,
                               fecha_aplicacion,
                               fecha_renuncia,
                               mayor_vencido,
                               total_cargos,  
                               total_consumos,
                               total_deuda,
                               status_cuenta,
                               bp_plan,
                               detalle_plan)
  VALUES(pv_region, 
         pv_ciclo,
         pv_cuenta,
         pv_nombres,
         pv_apellidos,
         pv_ruc,
         pv_forma_pago,
         TO_DATE (pd_fecha_apertura_plan,'DD/MM/RRRR'),
         pv_nombre_vendedor,
         pv_usuario_ingreso_gar,
         pn_solicitud,
         pv_tipo_garantia,
         TO_DATE (pd_fec_ing_garantia,'DD/MM/RRRR'),
         TO_DATE (pd_fec_cad_garantia,'DD/MM/RRRR'),
         pn_valor_deposito,
         TO_DATE (pd_fecha_aplicacion,'DD/MM/RRRR'),
         TO_DATE (pd_fecha_renuncia,'DD/MM/RRRR'),
         pv_mayor_vencido,
         pn_total_cargos,  
         pn_total_consumos,
         pn_total_deuda,
         pv_status_cuenta,
         pv_bp_plan,
         pv_detalle_plan);  
         
         
EXCEPTION
  WHEN OTHERS THEN
    pv_error := 'Error al inserta registro-'||sqlerrm;

END inserta_registro;                        

end cok_deposito_garantia;
/
