CREATE OR REPLACE PACKAGE GSI_RETARIFICADOS_CARGAS IS

  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n:  Realiza la carga a la fees por dividido por remark en GPRS y SMS de los registros del corte que se ha ejecutado
  --=====================================================================================--
  -- Desarrollado por:  SUD. Diana Mero Morales
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 29/06/2012
  -- Proyecto: [6071] Mejoras al Servicios Internacional Roaming
  -- PV_CODIGOERROR 0 => Exito
  -- PV_CODIGOERROR 1 => Error de ingreso de datos
  -- PV_CODIGOERROR 2 => Error inesperado
  --=====================================================================================--

  TYPE RECORD_CARGAS IS RECORD(
    ID_SERVICIO  GSI_CARGAS_X_REMARK.ID_SERVICIO%TYPE,
    SESIONES     GSI_CARGAS_X_REMARK.SESIONES%TYPE,
    BYTES        GSI_CARGAS_X_REMARK.BYTES%TYPE,
    MONTO        GSI_CARGAS_X_REMARK.MONTO%TYPE,
    SNCODE       GSI_CARGAS_X_REMARK.SNCODE%TYPE,
    REMARK       GSI_CARGAS_X_REMARK.REMARK%TYPE,
    TIPO_CONSUMO GSI_CARGAS_X_REMARK.TIPO_CONSUMO%TYPE,
    PROCESO      GSI_CARGAS_X_REMARK.PROCESO%TYPE,
    FECHA_CORTE  GSI_CARGAS_X_REMARK.FECHA_CORTE%TYPE);

  TYPE TAB_CARGAS IS TABLE OF RECORD_CARGAS;

  TYPE VAR_CURSOR IS REF CURSOR;

  CURSOR C_GET_COID_CUSTOMER(CD_FECHA_CORTE DATE, CV_ID_SERVICIO VARCHAR2) IS
    SELECT CO_ID, CUSTOMER_ID
      FROM GSI_PAQROAMING_ACTIVADOS_HIST@BSCS_TO_RTX_LINK A
     WHERE A.FECHA_CORTE = CD_FECHA_CORTE
       AND A.ID_SERVICIO = CV_ID_SERVICIO
       AND ROWNUM = 1;
  --

  CURSOR C_GET_COID(CV_ID_SERVICIO VARCHAR2) IS
    SELECT C.CO_ID
      FROM CL_SERVICIOS_CONTRATADOS@AXIS C
     WHERE C.ID_SERVICIO = CV_ID_SERVICIO
       AND C.ESTADO = 'A';

  CURSOR C_GET_COID_MAX_FE(CV_ID_SERVICIO VARCHAR2) IS
    SELECT C.CO_ID
      FROM CL_SERVICIOS_CONTRATADOS@AXIS C
     WHERE C.ID_SERVICIO = CV_ID_SERVICIO
       AND C.FECHA_INICIO =
           (SELECT MAX(FECHA_INICIO)
              FROM CL_SERVICIOS_CONTRATADOS@AXIS C
             WHERE C.ID_SERVICIO = CV_ID_SERVICIO);

  /*  CURSOR C_GET_COID(CV_ID_SERVICIO VARCHAR2)IS    
  SELECT CO_ID 
    FROM (SELECT A.DN_NUM, SC.CO_ID, MAX(SC.CS_ACTIV_DATE) INI 
            FROM DIRECTORY_NUMBER A, CONTR_SERVICES_CAP SC
           WHERE A.DN_ID=SC.DN_ID 
             AND A.DN_NUM = CV_ID_SERVICIO
           GROUP BY A.DN_NUM, SC.CO_ID 
           ORDER BY INI DESC) 
   WHERE ROWNUM = 1;*/

  CURSOR C_GET_CUSTOMER_ID(CN_COID NUMBER) IS
    SELECT CUSTOMER_ID FROM CONTRACT_ALL WHERE CO_ID = CN_COID;

  CURSOR C_PARAMETROS(CV_TIPO_PARAM VARCHAR2, CV_PARAM VARCHAR2) IS
    SELECT P.VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = CV_TIPO_PARAM
       AND P.ID_PARAMETRO = CV_PARAM;

  PROCEDURE GSI_LLENA_CARGAS_X_REMARK(PD_FECHA_CORTE DATE,
                                      PV_TIPO_SVA    VARCHAR2,
                                      PV_REMARK      IN OUT VARCHAR2,
                                      PV_TIPO        IN VARCHAR2,
                                      PV_COD_ERROR   OUT VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2);

  PROCEDURE GSI_CARGA_FEES(PD_FECHA_CORTE DATE,
                           PV_REMARK      VARCHAR2,
                           PD_FECHA_FIN   DATE,
                           PV_ERROR       OUT VARCHAR2);

  PROCEDURE GSI_REMARK_GPRS(PD_FECHA_CORTE    IN DATE,
                            PD_FECHA_INI      IN DATE,
                            PD_FECHA_FIN      IN DATE,
                            PV_FORMULA_FECHA  IN VARCHAR2,
                            PV_TIPO_SVA       IN VARCHAR2,
                            PV_REMARK         IN VARCHAR2,
                            PV_REMARK_STANDAR IN VARCHAR2,
                            PV_COD_ERROR      OUT VARCHAR2,
                            PV_ERROR          OUT VARCHAR2);

  PROCEDURE GSI_REMARK_SMS(PD_FECHA_CORTE     IN DATE,
                           PD_FECHA_INI       IN DATE,
                           PD_FECHA_FIN       IN DATE,
                           PD_FECHA_IF        IN DATE,
                           PD_FECHA_FF        IN DATE,
                           PV_FORMULA_FECHA   IN VARCHAR2,
                           PV_TIPO_SVA        IN VARCHAR2,
                           PV_REMARK          IN VARCHAR2,
                           PV_TIPO            IN VARCHAR2,
                           PV_REMARK_STANDAR  IN VARCHAR2,
                           PB_CARGA_SMS_PUERT IN BOOLEAN,
                           PV_COD_ERROR       OUT VARCHAR2,
                           PV_ERROR           OUT VARCHAR2);

  PROCEDURE GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          IN VARCHAR2,
                                        PV_ESTADO              IN VARCHAR2,
                                        PD_FECHA_CORTE         IN DATE,
                                        PD_FECHA_PROCESAMIENTO IN DATE,
                                        PV_REPROCESO           IN VARCHAR2,
                                        PV_NOMBRE_PROCESO      IN VARCHAR2,
                                        PV_ERROR               IN OUT VARCHAR2);

  PROCEDURE GSI_ARMA_MENSAJE(PV_MENSAJE IN OUT VARCHAR2,
                             PV_ERROR   OUT VARCHAR2);

end GSI_RETARIFICADOS_CARGAS;
/
CREATE OR REPLACE PACKAGE BODY GSI_RETARIFICADOS_CARGAS IS

  --*====================================================================**--
  --* CREADO POR : SUD. Diana Mero Morales
  --* FECHA DE CRACION: 29/06/2012
  --* OBJETIVO: Realiza la carga a la fees por dividido por remark en GPRS y SMS de los registros del corte que se ha ejecutado
  --*  PV_CODIGOERROR 0 => Exito
  --*  PV_CODIGOERROR 1 => Error de ingreso de datos
  --*  PV_CODIGOERROR 2 => Error inesperado
  --*====================================================================**--

  PROCEDURE GSI_LLENA_CARGAS_X_REMARK(PD_FECHA_CORTE DATE,
                                      PV_TIPO_SVA    VARCHAR2,
                                      PV_REMARK      IN OUT VARCHAR2,
                                      PV_TIPO        IN VARCHAR2 DEFAULT NULL, --"Sms de Entrada (IN) o Salida (OUT)"
                                      PV_COD_ERROR   OUT VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2) IS
  
    LV_PROGRAMA       VARCHAR2(100) := 'GSI_CARGA_RETARIFICADOS.GSI_LLENA_CARGAS_X_REMARK';
    LD_FECHA_INI      DATE;
    LD_FECHA_FIN      DATE;
    LV_FORMULA_FECHA  VARCHAR2(25) := NULL;
    LV_ID_PROCESO     VARCHAR2(3);
    LV_REMARK_STANDAR VARCHAR2(2000) := NULL;
    LE_ERROR_ESPERADO EXCEPTION;
    LV_ERROR_ARMA_MSJ VARCHAR2(4000) := NULL;
    LE_ERROR EXCEPTION;
    LB_CARGA_SMS_PUERTO BOOLEAN := FALSE;
    LV_ALTER_S          VARCHAR2(100) := NULL;
    LD_FECHA_FIN_FEES   DATE;
    LD_FECHA_INI_FEES   DATE;
  
  BEGIN
  
    LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    EXECUTE IMMEDIATE LV_ALTER_S;
  
    IF PV_REMARK = 'REMARK_GPRS_ADICIONALES' THEN
      LV_ID_PROCESO := '5';
    ELSIF PV_REMARK = 'REMARK_GPRS_EVENTOS' THEN
      LV_ID_PROCESO := '6';
    ELSIF PV_REMARK = 'REMARK_GPRS_EVENTOSNOPAQ' THEN
      LV_ID_PROCESO := '7';
    ELSIF PV_REMARK = 'REMARK_SMS_OUT_AMERICA' THEN
      LV_ID_PROCESO := '8';
    ELSIF PV_REMARK = 'REMARK_SMS_IN_AMERICA' THEN
      LV_ID_PROCESO := '9';
    ELSIF PV_REMARK = 'REMARK_SMS_OUT_RMUNDO' THEN
      LV_ID_PROCESO := '10';
    ELSIF PV_REMARK = 'REMARK_SMS_IN_RMUNDO' THEN
      LV_ID_PROCESO := '11';
    ELSIF PV_REMARK = 'REMARK_SMS_OUT_EVENTO' THEN
      LV_ID_PROCESO := '12';
    ELSIF PV_REMARK = 'REMARK_SMS_IN_EVENTO' THEN
      LV_ID_PROCESO := '13';
    END IF;
  
    GSI_RETARIFICADOS_CARGAS.GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          => LV_ID_PROCESO,
                                                         PV_ESTADO              => 'INICIADO',
                                                         PD_FECHA_CORTE         => PD_FECHA_CORTE,
                                                         PD_FECHA_PROCESAMIENTO => SYSDATE,
                                                         PV_REPROCESO           => 'N',
                                                         PV_NOMBRE_PROCESO      => LV_PROGRAMA,
                                                         PV_ERROR               => PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RETURN;
    END IF;
  
    GSI_RETARIFICACIONES_ROAMING.GSI_CONTROL_PROCESOS@BSCS_TO_RTX_LINK(PV_ID_PROCESO  => LV_ID_PROCESO,
                                                                       PD_FECHA_CORTE => PD_FECHA_CORTE,
                                                                       PV_REPROCESO   => 'N',
                                                                       PV_COD_ERROR   => PV_COD_ERROR,
                                                                       PV_ERROR       => PV_ERROR);
  
    IF PV_COD_ERROR = '1' THEN
      RAISE LE_ERROR_ESPERADO;
    ELSIF PV_COD_ERROR = '2' THEN
      RAISE LE_ERROR;
    END IF;
  
    GSI_RETARIFICACIONES_ROAMING.GSI_GET_RANGO_DIAS@BSCS_TO_RTX_LINK(PD_FECHA_CORTE   => PD_FECHA_CORTE,
                                                                     PD_FECHA_INI     => LD_FECHA_INI,
                                                                     PD_FECHA_FIN     => LD_FECHA_FIN,
                                                                     PB_INTERNACIONAL => TRUE,
                                                                     PV_ERROR         => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RAISE LE_ERROR;
    END IF;
  
    OPEN C_PARAMETROS(6071, PV_REMARK);
    FETCH C_PARAMETROS
      INTO PV_REMARK;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'FORMULA_FECHA');
    FETCH C_PARAMETROS
      INTO LV_FORMULA_FECHA;
    CLOSE C_PARAMETROS;
  
    IF PV_REMARK = 'Adicionales' THEN
      LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK ||
                           ' del ';
    ELSIF PV_REMARK = 'Eventos' THEN
      LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK ||
                           ' luego de paquete del ';
    ELSIF PV_REMARK = 'Eventos sin paquete' THEN
      /*LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK ||
                           ' eventos sin paquete del ';*/
      LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK ||
                           ' del ';                           
    ELSIF PV_REMARK = 'SMSRO_PAQUETE_ZONA_AMERICA' THEN
      LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK || ' ';
    ELSIF PV_REMARK = 'SMSRO_PAQUETE_ZONA_RMUNDO' THEN
      LV_REMARK_STANDAR := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK || ' ';
    ELSIF PV_REMARK = 'SMS_EVENTOS' THEN
      LV_REMARK_STANDAR   := 'Roaming ' || PV_TIPO_SVA || ' ' || PV_REMARK || ' ';
      LB_CARGA_SMS_PUERTO := TRUE;
    END IF;
  
    DELETE FROM BL_CARGA_OCC_TMP T;
  
    --*Obtiene la fecha fin que se dirige a la fees que se dirige a la fees 
    GSI_RETARIFICACIONES_ROAMING.GSI_GET_RANGO_DIAS@BSCS_TO_RTX_LINK(PD_FECHA_CORTE   => PD_FECHA_CORTE,
                                                                     PD_FECHA_INI     => LD_FECHA_INI_FEES,
                                                                     PD_FECHA_FIN     => LD_FECHA_FIN_FEES,
                                                                     PB_INTERNACIONAL => FALSE,
                                                                     PV_ERROR         => PV_ERROR);
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RAISE LE_ERROR;
    END IF;
  
    IF PV_TIPO_SVA = 'GPRS' THEN
      GSI_RETARIFICADOS_CARGAS.GSI_REMARK_GPRS(PD_FECHA_CORTE    => PD_FECHA_CORTE,
                                               PD_FECHA_INI      => LD_FECHA_INI_FEES, --LD_FECHA_INI
                                               PD_FECHA_FIN      => LD_FECHA_FIN_FEES, --LD_FECHA_FIN
                                               PV_FORMULA_FECHA  => LV_FORMULA_FECHA,
                                               PV_TIPO_SVA       => PV_TIPO_SVA,
                                               PV_REMARK         => PV_REMARK,
                                               PV_REMARK_STANDAR => LV_REMARK_STANDAR,
                                               PV_COD_ERROR      => PV_COD_ERROR,
                                               PV_ERROR          => PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
        PV_COD_ERROR := '2';
        RAISE LE_ERROR;
      END IF;
    ELSE
      GSI_RETARIFICADOS_CARGAS.GSI_REMARK_SMS(PD_FECHA_CORTE     => PD_FECHA_CORTE,
                                              PD_FECHA_INI       => LD_FECHA_INI,
                                              PD_FECHA_FIN       => LD_FECHA_FIN,
                                              PD_FECHA_IF        => LD_FECHA_INI_FEES, --**16/11/2012                                             
                                              PD_FECHA_FF        => LD_FECHA_FIN_FEES, --**16/11/2012
                                              PV_FORMULA_FECHA   => LV_FORMULA_FECHA,
                                              PV_TIPO_SVA        => PV_TIPO_SVA,
                                              PV_REMARK          => PV_REMARK,
                                              PV_TIPO            => PV_TIPO,
                                              PV_REMARK_STANDAR  => LV_REMARK_STANDAR,
                                              PB_CARGA_SMS_PUERT => LB_CARGA_SMS_PUERTO,
                                              PV_COD_ERROR       => PV_COD_ERROR,
                                              PV_ERROR           => PV_ERROR);
      IF PV_ERROR IS NOT NULL THEN
        PV_COD_ERROR := '2';
        RAISE LE_ERROR;
      END IF;
    END IF;
  
    COMMIT;
  
    GSI_RETARIFICADOS_CARGAS.GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          => LV_ID_PROCESO,
                                                         PV_ESTADO              => 'PROCESADO',
                                                         PD_FECHA_CORTE         => PD_FECHA_CORTE,
                                                         PD_FECHA_PROCESAMIENTO => SYSDATE,
                                                         PV_REPROCESO           => 'N',
                                                         PV_NOMBRE_PROCESO      => LV_PROGRAMA,
                                                         PV_ERROR               => PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RAISE LE_ERROR;
    END IF;
  
    PV_COD_ERROR := '0';
    PV_ERROR     := '**** ' || PV_TIPO_SVA || ' - Carga de ' || PV_REMARK ||
                    ' con �xito *******';
  
  EXCEPTION
    WHEN LE_ERROR_ESPERADO THEN
      ROLLBACK;
      GSI_RETARIFICADOS_CARGAS.GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          => LV_ID_PROCESO,
                                                           PV_ESTADO              => 'CANCELADO',
                                                           PD_FECHA_CORTE         => PD_FECHA_CORTE,
                                                           PD_FECHA_PROCESAMIENTO => SYSDATE,
                                                           PV_REPROCESO           => 'N',
                                                           PV_NOMBRE_PROCESO      => LV_PROGRAMA,
                                                           PV_ERROR               => PV_ERROR);
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || PV_ERROR;
      GSI_RETARIFICADOS_CARGAS.GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          => LV_ID_PROCESO,
                                                           PV_ESTADO              => 'ERROR',
                                                           PD_FECHA_CORTE         => PD_FECHA_CORTE,
                                                           PD_FECHA_PROCESAMIENTO => SYSDATE,
                                                           PV_REPROCESO           => 'N',
                                                           PV_NOMBRE_PROCESO      => LV_PROGRAMA,
                                                           PV_ERROR               => PV_ERROR);
    
      GSI_RETARIFICADOS_CARGAS.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                PV_ERROR   => LV_ERROR_ARMA_MSJ);
    
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR     := 'Error en programa:' || LV_PROGRAMA || '; ' ||
                      SQLERRM;
      PV_COD_ERROR := '2';
      GSI_RETARIFICADOS_CARGAS.GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          => LV_ID_PROCESO,
                                                           PV_ESTADO              => 'ERROR',
                                                           PD_FECHA_CORTE         => PD_FECHA_CORTE,
                                                           PD_FECHA_PROCESAMIENTO => SYSDATE,
                                                           PV_REPROCESO           => 'N',
                                                           PV_NOMBRE_PROCESO      => LV_PROGRAMA,
                                                           PV_ERROR               => PV_ERROR);
    
      GSI_RETARIFICADOS_CARGAS.GSI_ARMA_MENSAJE(PV_MENSAJE => PV_ERROR,
                                                PV_ERROR   => LV_ERROR_ARMA_MSJ);
  END GSI_LLENA_CARGAS_X_REMARK;

  PROCEDURE GSI_CARGA_FEES(PD_FECHA_CORTE DATE,
                           PV_REMARK      VARCHAR2,
                           PD_FECHA_FIN   DATE,
                           PV_ERROR       OUT VARCHAR2) IS
  
    LV_PROGRAMA VARCHAR2(100) := 'GSI_CARGA_RETARIFICADOS.GSI_LLENA_BL_OCC_TMP';
  
    CURSOR C_NUM_REG_CARGA IS
      SELECT COUNT(*) FROM BL_CARGA_OCC_TMP;
  
    LV_USER_CARGA    VARCHAR2(25) := NULL;
    LN_NUM_REG_CARGA NUMBER := 0;
    LN_ID_EJECUCION  NUMBER := 0;
    --LD_FECHA_INI     DATE;
    LD_FECHA_FIN      DATE := pd_fecha_fin;
    LV_CANTIDAD_HILOS VARCHAR2(3) := NULL;
    LV_COMMIT         VARCHAR2(5) := NULL;
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    OPEN C_PARAMETROS(6071, 'USER_CARGA');
    FETCH C_PARAMETROS
      INTO LV_USER_CARGA;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'CANT_HILOS_FEES');
    FETCH C_PARAMETROS
      INTO LV_CANTIDAD_HILOS; --3
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'COMMIT_CARGA');
    FETCH C_PARAMETROS
      INTO LV_COMMIT;
    CLOSE C_PARAMETROS;
  
    OPEN C_NUM_REG_CARGA;
    FETCH C_NUM_REG_CARGA
      INTO LN_NUM_REG_CARGA;
    CLOSE C_NUM_REG_CARGA;
  
    /* GSI_RETARIFICACIONES_ROAMING.GSI_GET_RANGO_DIAS@BSCS_TO_RTX_LINK( PD_FECHA_CORTE   => PD_FECHA_CORTE,
                                                                      PD_FECHA_INI     => LD_FECHA_INI,
                                                                      PD_FECHA_FIN     => LD_FECHA_FIN,
                                                                      PB_INTERNACIONAL => FALSE,
                                                                      PV_ERROR         => PV_ERROR);
    
    IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
    END IF;*/
  
    --Llamo al proceso que se encargar� de realizar la carga de tabla bl_occ_tmp a la tabla fees
    BLK_CARGA_OCC.BLP_EJECUTA(PV_USUARIO          => LV_USER_CARGA,
                              PN_ID_NOTIFICACION  => 1,
                              PN_TIEMPO_NOTIF     => 1,
                              PN_CANTIDAD_HILOS   => NVL(LV_CANTIDAD_HILOS,
                                                         0),
                              PN_CANTIDAD_REG_MEM => NVL(LV_COMMIT, 0),
                              PV_RECURRENCIA      => 'N',
                              PV_REMARK           => PV_REMARK,
                              PD_ENTDATE          => LD_FECHA_FIN,
                              PV_RESPALDAR        => 'N',
                              PV_TABLA_RESPALDO   => NULL,
                              PN_ID_EJECUCION     => LN_ID_EJECUCION);
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || PV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; Error: ' ||
                  SQLERRM;
  END GSI_CARGA_FEES;

  PROCEDURE GSI_REMARK_GPRS(PD_FECHA_CORTE    IN DATE,
                            PD_FECHA_INI      IN DATE,
                            PD_FECHA_FIN      IN DATE,
                            PV_FORMULA_FECHA  IN VARCHAR2,
                            PV_TIPO_SVA       IN VARCHAR2,
                            PV_REMARK         IN VARCHAR2,
                            PV_REMARK_STANDAR IN VARCHAR2,
                            PV_COD_ERROR      OUT VARCHAR2,
                            PV_ERROR          OUT VARCHAR2) IS
  
    LV_PROGRAMA       VARCHAR2(100) := 'GSI_CARGA_RETARIFICADOS.GSI_REMARK_GPRS';
    LV_TABLA          VARCHAR2(35) := NULL;
    LV_DB_LINK        VARCHAR2(50) := '@BSCS_TO_RTX_LINK';
    LV_SENTENCIA      VARCHAR2(5000) := NULL;
    LV_SELECT         VARCHAR2(4000) := NULL;
    LV_FROM           VARCHAR2(100) := NULL;
    LV_WHERE          VARCHAR2(2000) := NULL;
    LV_ADD_1          VARCHAR2(100) := NULL;
    LV_ADD_2          VARCHAR2(1000) := NULL;
    LV_SN_CODE_GPRS   VARCHAR2(25) := NULL;
    C_CURSOR          VAR_CURSOR;
    LR_REG_CARGA      RECORD_CARGAS;
    LN_CO_ID          NUMBER := 0;
    LN_CUSTOMER_ID    NUMBER := 0;
    ln_COMMIT         NUMBER := 0; --Contador para dar commit
    LV_COMMIT         VARCHAR2(10); -- indica cada cuantos registros se puede dar commit
    LV_CARGAR_AUT     VARCHAR2(1) := NULL;
    LV_CONDIC_AUT     VARCHAR2(500) := NULL;
    LB_ENCONTRADO     BOOLEAN;
    LV_REMARK_STANDAR VARCHAR2(2000) := NULL;
    LE_ERROR_ESPERADO EXCEPTION;
    LE_ERROR          EXCEPTION;
    LV_ALTER_S      VARCHAR2(100) := NULL;
    LV_FECHA_EVENTO VARCHAR2(30) := NULL;
    LV_FECHA_INI    VARCHAR2(100) := NULL;
    LV_FECHA_FIN    VARCHAR2(100) := NULL;
    LV_FONO         VARCHAR2(20) := NULL;
    LD_FECHA_FIN    DATE := PD_FECHA_FIN;
  
  BEGIN
  
    LV_FECHA_INI := TO_CHAR(TRUNC(PD_FECHA_INI), 'DD/MM/YYYY');
    LV_FECHA_FIN := TO_CHAR(TRUNC(PD_FECHA_FIN), 'DD/MM/YYYY');
  
    LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
    EXECUTE IMMEDIATE LV_ALTER_S;
  
    OPEN C_PARAMETROS(6071, 'SNCODE_GPRS');
    FETCH C_PARAMETROS
      INTO LV_SN_CODE_GPRS;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'CARGAR_FEES_AUT');
    FETCH C_PARAMETROS
      INTO LV_CARGAR_AUT;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'FECHA_EVENTO');
    FETCH C_PARAMETROS
      INTO LV_FECHA_EVENTO;
    CLOSE C_PARAMETROS;
  
    IF NVL(LV_CARGAR_AUT, 'N') = 'N' THEN
      --Aqui esta controlando que los Autocontroles no Los a cargar en la Fees
      LV_CONDIC_AUT := 'and nvl(producto,' || '''TAR''' || ')' || ' = ' ||
                       '''TAR''';
    ELSE
      LV_CONDIC_AUT := NULL;
    END IF;
  
    LV_SELECT := 'select s_p_number_address,' || ' count(*) sesiones,' ||
                 ' sum(data_volume_new) Bytes_Adicionales,' ||
                 ' sum(rated_flat_amount_new) MONTO,' || '''' ||
                 LV_SN_CODE_GPRS || '''' || ', ' || '''' || PV_REMARK || '''' || ',' || '''' ||
                 PV_TIPO_SVA || '''' || ', ' || ' ''RETARIFICACION'' ' || ', ' ||
                 'to_date(''' ||
                 to_char(TRUNC(PD_FECHA_CORTE), 'dd/mm/yyyy') ||
                 ''',''dd/mm/yyyy'')';
  
    LV_TABLA := 'SYSADM.GSI_TRAFICO_GRX_' ||
                TO_CHAR(PD_FECHA_CORTE, 'YYYYMMDD');
  
    LV_FROM := ' from ' || LV_TABLA || LV_DB_LINK || ' h ';
  
    IF PV_REMARK = 'Eventos sin paquete' THEN
      LV_ADD_1 := ' and id_paquete is null ';
      LV_ADD_2 := ' and s_p_number_address not in (select s_p_number_address from GSI_ROAM_LINEAS_DIRECTORES@BSCS_TO_RTX_LINK) ';
    ELSE
      LV_ADD_1 := ' and id_paquete is not null ';
    END IF;
  
    LV_WHERE := ' where remark_new=' || '''' || PV_REMARK || '''' ||
                LV_ADD_1 || ' and rated_flat_amount_new!=0' ||
                ' and initial_start_time_timestamp + NVL ((initial_start_time_time_offset ' ||
                PV_FORMULA_FECHA || '), 0)>=to_date(''' || LV_FECHA_EVENTO ||
                ''',''dd/mm/yyyy hh24:mi:ss'')' || LV_CONDIC_AUT ||
                LV_ADD_2 || ' group by s_p_number_address';
  
    LV_SENTENCIA := LV_SELECT || LV_FROM || LV_WHERE;
  
    LV_REMARK_STANDAR := PV_REMARK_STANDAR || LV_FECHA_INI || ' al ' ||
                         LV_FECHA_FIN;
  
    OPEN C_CURSOR FOR LV_SENTENCIA;
    COMMIT;
    LOOP
    
      LR_REG_CARGA := NULL;
    
      FETCH C_CURSOR
        INTO LR_REG_CARGA;
    
      EXIT WHEN C_CURSOR%NOTFOUND;
    
      LN_CO_ID       := 0;
      LN_CUSTOMER_ID := 0;
    
      OPEN C_GET_COID_CUSTOMER(PD_FECHA_CORTE, LR_REG_CARGA.ID_SERVICIO);
      FETCH C_GET_COID_CUSTOMER
        INTO LN_CO_ID, LN_CUSTOMER_ID;
      LB_ENCONTRADO := C_GET_COID_CUSTOMER%FOUND;
      CLOSE C_GET_COID_CUSTOMER;
    
      --IF NOT LB_ENCONTRADO THEN
      IF NVL(LN_CO_ID, 0) = 0 THEN
      
        LV_FONO := OBTIENE_TELEFONO_DNC(LR_REG_CARGA.ID_SERVICIO, 'V');
      
        OPEN C_GET_COID(LV_FONO);
        FETCH C_GET_COID
          INTO LN_CO_ID;
        CLOSE C_GET_COID;
      
        IF NVL(LN_CO_ID, 0) = 0 THEN
          OPEN C_GET_COID_MAX_FE(LV_FONO);
          FETCH C_GET_COID_MAX_FE
            INTO LN_CO_ID;
          CLOSE C_GET_COID_MAX_FE;
        END IF;
      
        OPEN C_GET_CUSTOMER_ID(LN_CO_ID);
        FETCH C_GET_CUSTOMER_ID
          INTO LN_CUSTOMER_ID;
        CLOSE C_GET_CUSTOMER_ID;
      
      END IF;
    
      INSERT INTO BL_CARGA_OCC_TMP
        (CO_ID, AMOUNT, SNCODE, STATUS, ERROR, HILO, CUSTOMER_ID, CUSTCODE)
      VALUES
        (LN_CO_ID,
         LR_REG_CARGA.MONTO,
         LR_REG_CARGA.SNCODE,
         NULL,
         NULL,
         NULL,
         LN_CUSTOMER_ID,
         NULL);
    
      LN_COMMIT := LN_COMMIT + 1;
    
      IF LN_COMMIT >= TO_NUMBER(LV_COMMIT) THEN
        COMMIT;
        LN_COMMIT := 0;
      END IF;
    
    END LOOP;
  
    COMMIT;
  
    CLOSE C_CURSOR;
  
    GSI_CARGA_FEES(PD_FECHA_CORTE,
                   LV_REMARK_STANDAR,
                   LD_FECHA_FIN,
                   PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || PV_ERROR;
    WHEN OTHERS THEN
      PV_COD_ERROR := '2';
      PV_ERROR     := 'Error en programa: ' || LV_PROGRAMA || '; ' ||
                      SQLERRM;
  END GSI_REMARK_GPRS;

  PROCEDURE GSI_REMARK_SMS(PD_FECHA_CORTE     IN DATE,
                           PD_FECHA_INI       IN DATE,
                           PD_FECHA_FIN       IN DATE,
                           PD_FECHA_IF        IN DATE,
                           PD_FECHA_FF        IN DATE,
                           PV_FORMULA_FECHA   IN VARCHAR2,
                           PV_TIPO_SVA        IN VARCHAR2,
                           PV_REMARK          IN VARCHAR2,
                           PV_TIPO            IN VARCHAR2,
                           PV_REMARK_STANDAR  IN VARCHAR2,
                           PB_CARGA_SMS_PUERT IN BOOLEAN,
                           PV_COD_ERROR       OUT VARCHAR2,
                           PV_ERROR           OUT VARCHAR2) IS
  
    LV_PROGRAMA       VARCHAR2(100) := 'GSI_CARGA_RETARIFICADOS.GSI_REMARK_SMS';
    LV_TABLA          VARCHAR2(35) := NULL;
    LV_DB_LINK        VARCHAR2(50) := '@BSCS_TO_RTX_LINK';
    LV_SENTENCIA      VARCHAR2(5000) := NULL;
    LV_RANGE_FECHA    VARCHAR2(2000) := NULL;
    LV_SELECT         VARCHAR2(4000) := NULL;
    LV_FROM           VARCHAR2(100) := NULL;
    LV_WHERE          VARCHAR2(2000) := NULL;
    C_CURSOR          VAR_CURSOR;
    LR_REG_CARGA      RECORD_CARGAS;
    LN_CO_ID          NUMBER := 0;
    LN_CUSTOMER_ID    NUMBER := 0;
    LN_COMMIT         NUMBER := 0; --Contador para dar commit
    LV_COMMIT         VARCHAR2(10); -- indica cada cuantos registros se puede dar commit
    LV_SMS            VARCHAR(1);
    LV_SN_CODE        VARCHAR2(15);
    LB_ENCONTRADO     BOOLEAN;
    LV_REMARK_STANDAR VARCHAR2(2000) := NULL;
    LE_ERROR_ESPERADO EXCEPTION;
    LE_ERROR          EXCEPTION;
    LN_FOLLOW_UP_CALL_TYPE NUMBER := 1;
    LV_ALTER_S             VARCHAR2(100) := NULL;
    LV_REMARK_SMS_CC       VARCHAR2(100) := NULL;
    LV_FECHA_NEW_ESQUEMA   VARCHAR2(25) := NULL;
    LV_FECHA_INI           VARCHAR2(100) := NULL;
    LV_FECHA_FIN           VARCHAR2(100) := NULL;
    LV_FONO                VARCHAR2(20) := NULL;
    LD_FECHA_FIN           DATE := PD_FECHA_FF; --PD_FECHA_FIN;
  BEGIN
  
    LV_FECHA_INI := TO_CHAR(TRUNC(PD_FECHA_IF), 'DD/MM/YYYY'); --TO_CHAR(TRUNC(PD_FECHA_INI),'DD/MM/YYYY');
    LV_FECHA_FIN := TO_CHAR(TRUNC(PD_FECHA_FF), 'DD/MM/YYYY'); --TO_CHAR(TRUNC(PD_FECHA_FIN),'DD/MM/YYYY');
  
    LV_ALTER_S := 'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS''';
    EXECUTE IMMEDIATE LV_ALTER_S;
  
    OPEN C_PARAMETROS(6071, 'SMS_SNCODE_' || PV_TIPO);
    FETCH C_PARAMETROS
      INTO LV_SN_CODE;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'SMS_' || PV_TIPO);
    FETCH C_PARAMETROS
      INTO LV_SMS;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'REMARK_SMS_CONCOSTO');
    FETCH C_PARAMETROS
      INTO LV_REMARK_SMS_CC;
    CLOSE C_PARAMETROS;
  
    OPEN C_PARAMETROS(6071, 'FECHA_NEW_ESQUEMA');
    FETCH C_PARAMETROS
      INTO LV_FECHA_NEW_ESQUEMA;
    CLOSE C_PARAMETROS;
  
    --LOOP
    --DELETE FROM BL_CARGA_OCC_TMP T;
    --EXIT WHEN LN_FOLLOW_UP_CALL_TYPE >= 3;
  
    LV_SELECT := 'select s_p_number_address, ' || 'count(*) mensajes, ' || '0,' || --Pongo cero xq sms no aplica bytes
                 'sum(rated_flat_amount) monto, ' || ' :SN_CODE,' || '''' ||
                 PV_REMARK || ''',' || '''' || PV_TIPO_SVA || '''' || ', ' ||
                 ' ''RETARIFICACION'' ' || ', ' || 'TO_DATE(''' ||
                 TO_CHAR(TRUNC(PD_FECHA_CORTE), 'DD/MM/YYYY') ||
                 ''',''DD/MM/YYYY'') ';
  
    LV_TABLA := 'sysadm.UDR_LT_' || to_char(TRUNC(PD_FECHA_INI), 'yyyymm') ||
                '_SMS';
  
    LV_FROM := ' FROM ' || LV_TABLA || LV_DB_LINK;
  
    LV_RANGE_FECHA := ' and entry_date_timestamp>=to_date(''' ||
                      TO_CHAR(PD_FECHA_INI, 'DD/MM/YYYY HH24:MI:SS') ||
                      ''',''DD/MM/YYYY HH24:MI:SS'' )' ||
                      ' and entry_date_timestamp<=to_date(''' ||
                      TO_CHAR(PD_FECHA_FIN, 'DD/MM/YYYY HH24:MI:SS') ||
                      ''',''DD/MM/YYYY HH24:MI:SS'' )';
  
    IF NOT PB_CARGA_SMS_PUERT THEN
      LV_WHERE := ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset' ||
                  PV_FORMULA_FECHA || '), 0)>=to_date(''' ||
                  LV_FECHA_NEW_ESQUEMA || ''',''dd/mm/yyyy hh24:mi:ss'') ' ||
                  LV_RANGE_FECHA || ' and rated_flat_amount!=0' ||
                  ' and follow_up_call_type=:TIPO_SMS' || ' and remark=' || '''' ||
                  PV_REMARK || '''' || ' group by s_p_number_address';
    ELSE
      LV_WHERE := ' where initial_start_time_timestamp + NVL ((initial_start_time_time_offset' ||
                  PV_FORMULA_FECHA || '), 0)>=to_date(''' ||
                  LV_FECHA_NEW_ESQUEMA || ''',''dd/mm/yyyy hh24:mi:ss'') ' ||
                  LV_RANGE_FECHA || ' and rated_flat_amount!=0' ||
                  ' and follow_up_call_type=:TIPO_SMS' ||
                  ' and remark in (' || '''' || PV_REMARK || '''' || ',' || '''' ||
                  LV_REMARK_SMS_CC || '''' || ')' ||
                  ' group by s_p_number_address';
    END IF;
  
    LV_SENTENCIA := LV_SELECT || LV_FROM || LV_WHERE;
  
    /*************************************************************
       **SI UTILIZA 2 UDR
    **************************************************************/
  
    IF TO_CHAR(PD_FECHA_INI, 'YYYYMM') <> TO_CHAR(PD_FECHA_FIN, 'YYYYMM') THEN
      --Si es en meses diferentes se toma de tablas diferentes
      LV_WHERE := replace(LV_WHERE,
                          LV_RANGE_FECHA,
                          ' and entry_date_timestamp>=to_date(''' ||
                          TO_CHAR(PD_FECHA_INI, 'DD/MM/YYYY HH24:MI:SS') ||
                          ''',''DD/MM/YYYY HH24:MI:SS'' )');
    
      LV_RANGE_FECHA := ' and entry_date_timestamp>=to_date(''' ||
                        TO_CHAR(PD_FECHA_INI, 'DD/MM/YYYY HH24:MI:SS') ||
                        ''',''DD/MM/YYYY HH24:MI:SS'' )';
    
      LV_SENTENCIA := LV_SELECT || LV_FROM || LV_WHERE;
    
      LV_FROM := REPLACE(LV_FROM,
                         LV_TABLA,
                         'SYSADM.UDR_LT_' || TO_CHAR(PD_FECHA_FIN, 'YYYYMM') ||
                         '_SMS');
    
      LV_WHERE := REPLACE(LV_WHERE,
                          LV_RANGE_FECHA,
                          ' and entry_date_timestamp<=to_date(''' ||
                          TO_CHAR(PD_FECHA_FIN, 'DD/MM/YYYY HH24:MI:SS') ||
                          ''',''DD/MM/YYYY HH24:MI:SS'' )');
    
      LV_SENTENCIA := LV_SENTENCIA || ' UNION ALL ' || LV_SELECT || LV_FROM ||
                      LV_WHERE;
    
      /** Para 2 Udr **/
      LV_REMARK_STANDAR := PV_REMARK_STANDAR || PV_TIPO || ' del ' ||
                           LV_FECHA_INI || ' al ' || LV_FECHA_FIN;
      /** Para 2 Udr **/
      OPEN C_CURSOR FOR LV_SENTENCIA
        USING LV_SN_CODE, LV_SMS, LV_SN_CODE, LV_SMS;
    ELSE
      OPEN C_CURSOR FOR LV_SENTENCIA
        USING LV_SN_CODE, LV_SMS;
      LV_REMARK_STANDAR := PV_REMARK_STANDAR || PV_TIPO || ' del ' ||
                           LV_FECHA_INI || ' al ' || LV_FECHA_FIN;
    END IF;
    COMMIT;
    LOOP
    
      LR_REG_CARGA := NULL;
    
      FETCH C_CURSOR
        INTO LR_REG_CARGA;
    
      EXIT WHEN C_CURSOR%NOTFOUND;
    
      LN_CO_ID       := 0;
      LN_CUSTOMER_ID := 0;
    
      OPEN C_GET_COID_CUSTOMER(PD_FECHA_CORTE, LR_REG_CARGA.ID_SERVICIO);
      FETCH C_GET_COID_CUSTOMER
        INTO LN_CO_ID, LN_CUSTOMER_ID;
      LB_ENCONTRADO := C_GET_COID_CUSTOMER%FOUND;
      CLOSE C_GET_COID_CUSTOMER;
    
      --IF NOT LB_ENCONTRADO THEN
      IF NVL(LN_CO_ID, 0) = 0 THEN
        LV_FONO := OBTIENE_TELEFONO_DNC(LR_REG_CARGA.ID_SERVICIO, 'V');

        OPEN C_GET_COID(LV_FONO);
        FETCH C_GET_COID
          INTO LN_CO_ID;
        CLOSE C_GET_COID;
      
        IF nvl(LN_CO_ID, 0) = 0 THEN
          OPEN C_GET_COID_MAX_FE(LV_FONO);
          FETCH C_GET_COID_MAX_FE
            INTO LN_CO_ID;
          CLOSE C_GET_COID_MAX_FE;
        END IF;
      
        OPEN C_GET_CUSTOMER_ID(LN_CO_ID);
        FETCH C_GET_CUSTOMER_ID
          INTO LN_CUSTOMER_ID;
        CLOSE C_GET_CUSTOMER_ID;
      END IF;
    
      INSERT INTO BL_CARGA_OCC_TMP
        (CO_ID, AMOUNT, SNCODE, STATUS, ERROR, HILO, CUSTOMER_ID, CUSTCODE)
      VALUES
        (LN_CO_ID,
         LR_REG_CARGA.MONTO,
         LR_REG_CARGA.SNCODE,
         NULL,
         NULL,
         NULL,
         LN_CUSTOMER_ID,
         NULL);
    
      LN_COMMIT := LN_COMMIT + 1;
    
      IF LN_COMMIT >= TO_NUMBER(LV_COMMIT) THEN
        COMMIT;
        LN_COMMIT := 0;
      END IF;
    
    END LOOP;
    COMMIT;
    CLOSE C_CURSOR;
  
    GSI_CARGA_FEES(PD_FECHA_CORTE,
                   LV_REMARK_STANDAR,
                   LD_FECHA_FIN,
                   PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      PV_COD_ERROR := '2';
      RAISE LE_ERROR;
    END IF;
  
   -- LN_FOLLOW_UP_CALL_TYPE := LN_FOLLOW_UP_CALL_TYPE + 1;
  
    --END LOOP;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || PV_ERROR;
    WHEN OTHERS THEN
      PV_COD_ERROR := '2';
      PV_ERROR     := 'Error en programa: ' || LV_PROGRAMA || '; ' ||
                      SQLERRM;
  END GSI_REMARK_SMS;

  PROCEDURE GSI_BITACORIZA_PROCESOS_ROA(PV_ID_PROCESO          IN VARCHAR2,
                                        PV_ESTADO              IN VARCHAR2,
                                        PD_FECHA_CORTE         IN DATE,
                                        PD_FECHA_PROCESAMIENTO IN DATE,
                                        PV_REPROCESO           IN VARCHAR2,
                                        PV_NOMBRE_PROCESO      IN VARCHAR2,
                                        PV_ERROR               IN OUT VARCHAR2) IS
  
    --PRAGMA AUTONOMOUS_TRANSACTION;
  
    CURSOR C_DESCRIPCION_PROCESO(CV_ID_PROCESO VARCHAR2) IS
      SELECT P.DESCRIPCION
        FROM GSI_PROCESOS_RETARIFICA_ROA@BSCS_TO_RTX_LINK P
       WHERE P.ID_PROCESO = CV_ID_PROCESO
         AND P.ESTADO = 'A';
  
    LV_PROGRAMA    VARCHAR2(100) := 'GSI_RETARIFICACIONES_ROAMING.GSI_BITACORIZA_PROCESOS_ROA';
    LV_PROCESO     VARCHAR2(100) := NULL;
    LV_DESCRIPCION VARCHAR2(500) := NULL;
  
  BEGIN
  
    IF PV_ID_PROCESO IS NULL OR PV_ESTADO IS NULL OR PD_FECHA_CORTE IS NULL OR
       PD_FECHA_PROCESAMIENTO IS NULL OR PV_REPROCESO IS NULL THEN
    
      SELECT DESCRIPCION
        INTO LV_PROCESO
        FROM GSI_PROCESOS_RETARIFICA_ROA@BSCS_TO_RTX_LINK
       WHERE ID_PROCESO = PV_ID_PROCESO;
    
      PV_ERROR := 'Error en programa:' || LV_PROGRAMA ||
                  '; Error: Ning�n par�metro puede ser nulo para bitacorizar proceso.' ||
                  'Se intent� bitacorizar proceso: ' || LV_PROCESO;
      RETURN;
    
    END IF;
  
    OPEN C_DESCRIPCION_PROCESO(PV_ID_PROCESO);
    FETCH C_DESCRIPCION_PROCESO
      INTO LV_DESCRIPCION;
    CLOSE C_DESCRIPCION_PROCESO;
  
    IF PV_ESTADO = 'INICIADO' THEN
      INSERT INTO GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK
        (ID_PROCESO,
         DESCRIPCION_PROCESO,
         NOMBRE_PROCESO_EJECUTADO,
         FECHA_CORTE,
         FECHA_PROCESAMIENTO,
         ESTADO,
         REPROCESO,
         ERROR)
      VALUES
        (PV_ID_PROCESO,
         LV_DESCRIPCION,
         PV_NOMBRE_PROCESO,
         PD_FECHA_CORTE,
         PD_FECHA_PROCESAMIENTO,
         PV_ESTADO,
         PV_REPROCESO,
         NULL);
    
    ELSIF PV_ESTADO = 'PROCESADO' THEN
      UPDATE GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK R
         SET R.ESTADO                   = PV_ESTADO,
             R.REPROCESO                = PV_REPROCESO,
             R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
       WHERE R.FECHA_CORTE = PD_FECHA_CORTE
         AND R.ID_PROCESO = PV_ID_PROCESO
         AND R.ESTADO = 'INICIADO'
         AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
         AND R.FECHA_PROCESAMIENTO =
             (SELECT MAX(S.FECHA_PROCESAMIENTO)
                FROM GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK S
               WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                 AND S.ID_PROCESO = PV_ID_PROCESO
                 AND S.ESTADO = 'INICIADO'
                 AND S.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO);
    
      IF SQL%NOTFOUND THEN
        PV_ERROR := 'Error en programa:' || LV_PROGRAMA ||
                    '; Error al actualizar tabla de bitacora';
        RETURN;
      END IF;
    
    ELSIF PV_ESTADO = 'ERROR' THEN
      UPDATE GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK R
         SET R.REPROCESO = PV_REPROCESO,
             R.ERROR     = PV_ERROR,
             R.ESTADO    = PV_ESTADO
       WHERE R.FECHA_CORTE = PD_FECHA_CORTE
         AND R.ID_PROCESO = PV_ID_PROCESO
         AND R.ESTADO = 'INICIADO'
         AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
         AND R.FECHA_PROCESAMIENTO =
             (SELECT MAX(S.FECHA_PROCESAMIENTO)
                FROM GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK S
               WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                 AND S.ID_PROCESO = PV_ID_PROCESO
                 AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
                 AND S.ESTADO = 'INICIADO');
      IF SQL%NOTFOUND THEN
        PV_ERROR := 'Error en programa:' || LV_PROGRAMA ||
                    '; Error al actualizar tabla de bitacora';
        RETURN;
      END IF;
    
    ELSIF PV_ESTADO = 'WARNING' THEN
      UPDATE GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK R
         SET R.REPROCESO = PV_REPROCESO,
             R.ERROR     = PV_ERROR,
             R.ESTADO    = PV_ESTADO
       WHERE R.FECHA_CORTE = PD_FECHA_CORTE
         AND R.ID_PROCESO = PV_ID_PROCESO
         AND R.ESTADO = 'INICIADO'
         AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
         AND R.FECHA_PROCESAMIENTO =
             (SELECT MAX(S.FECHA_PROCESAMIENTO)
                FROM GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK S
               WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                 AND S.ID_PROCESO = PV_ID_PROCESO
                 AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
                 AND S.ESTADO = 'INICIADO');
      IF SQL%NOTFOUND THEN
        PV_ERROR := 'Error en programa:' || LV_PROGRAMA ||
                    '; Error al actualizar tabla de bitacora';
        RETURN;
      END IF;
    ELSIF PV_ESTADO = 'CANCELADO' THEN
      UPDATE GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK R
         SET R.REPROCESO = PV_REPROCESO,
             R.ERROR     = PV_ERROR,
             R.ESTADO    = PV_ESTADO
       WHERE R.FECHA_CORTE = PD_FECHA_CORTE
         AND R.ID_PROCESO = PV_ID_PROCESO
         AND R.ESTADO = 'INICIADO'
         AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
         AND R.FECHA_PROCESAMIENTO =
             (SELECT MAX(S.FECHA_PROCESAMIENTO)
                FROM GSI_PROCESOS_BITACORA_ROA@BSCS_TO_RTX_LINK S
               WHERE S.FECHA_CORTE = PD_FECHA_CORTE
                 AND S.ID_PROCESO = PV_ID_PROCESO
                 AND R.NOMBRE_PROCESO_EJECUTADO = PV_NOMBRE_PROCESO
                 AND S.ESTADO = 'INICIADO');
      IF SQL%NOTFOUND THEN
        PV_ERROR := 'Error en programa:' || LV_PROGRAMA ||
                    '; Error al actualizar tabla de bitacora';
        RETURN;
      END IF;
    
    END IF;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || SQLERRM;
  END GSI_BITACORIZA_PROCESOS_ROA;

  PROCEDURE GSI_ARMA_MENSAJE(PV_MENSAJE IN OUT VARCHAR2,
                             PV_ERROR   OUT VARCHAR2) IS
  
    LV_CAD1      VARCHAR2(4000);
    LV_CAD2      VARCHAR2(4000);
    LV_MSJ       VARCHAR2(4000);
    LN_POS_PUNTO NUMBER;
    LN_POS_RAYA  NUMBER;
    LN_TAM       NUMBER;
    LN_ENCUENTRA NUMBER;
  
  BEGIN
    LV_CAD1      := PV_MENSAJE;
    LN_POS_PUNTO := LENGTH(SUBSTR(LV_CAD1,
                                  (INSTR(LV_CAD1, 'Error en programa:')),
                                  (INSTR(LV_CAD1, 'a: ') + 1)));
  
    WHILE (LN_POS_PUNTO <> 0) LOOP
    
      LN_TAM  := LENGTH(LV_CAD1);
      LN_TAM  := LN_TAM - LN_POS_PUNTO;
      LV_CAD1 := SUBSTR(LV_CAD1, -LN_TAM);
      LV_CAD1 := TRIM(LV_CAD1);
    
      LN_POS_RAYA := INSTR(LV_CAD1, ';');
      LN_POS_RAYA := LN_POS_RAYA - 1;
      LV_CAD2     := SUBSTR(LV_CAD1, 1, LN_POS_RAYA);
    
      IF LV_MSJ IS NOT NULL THEN
        LV_MSJ := LV_MSJ || '. ' || LV_CAD2;
      ELSE
        LV_MSJ := LV_CAD2;
      END IF;
    
      LN_ENCUENTRA := INSTR(LV_CAD1, 'Error en programa:');
    
      IF LN_ENCUENTRA > 0 THEN
        LN_POS_PUNTO := LENGTH(SUBSTR(LV_CAD1,
                                      (INSTR(LV_CAD1, 'Error en programa:')),
                                      (INSTR(LV_CAD1, 'a: ') + 1)));
      ELSE
        LN_POS_RAYA  := INSTR(LV_CAD1, ';');
        LV_CAD1      := SUBSTR(LV_CAD1, INSTR(LV_CAD1, ';'));
        LV_MSJ       := LV_MSJ || '. ' || LV_CAD1;
        LN_POS_PUNTO := 0;
      END IF;
    
    END LOOP;
  
    IF LV_MSJ IS NOT NULL THEN
      PV_MENSAJE := TRIM(LV_MSJ);
    ELSE
      PV_MENSAJE := TRIM(LV_CAD1);
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error ' || SQLCODE || ' ' || SQLERRM;
  END GSI_ARMA_MENSAJE;

END GSI_RETARIFICADOS_CARGAS;
/
