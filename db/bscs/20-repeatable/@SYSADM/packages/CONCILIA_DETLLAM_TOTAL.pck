CREATE OR REPLACE package CONCILIA_DETLLAM_TOTAL is

  -- Author  : DCHONILLO
  -- Created : 21/04/2004 08:53:56
  -- Purpose : Identificar telefonos cuya factura  no presenta correctamente 
             --el TOTAL en las llamadas entrantes. La tabla a verificar sera
             --udr_lt_anio|periodo_ante|periodo_act       

  PROCEDURE DETALLE_SIN_TOTAL (periodo_ante in varchar2,
                               periodo_act in varchar2,
                               anio in varchar2,
                               nom_tabla In Varchar2,
                               resultado out varchar2);
  PROCEDURE TOTAL_LLAMADAS(contrato in number, 
                           custom   in number,
                           tabla in varchar2,
                           mensaje out varchar2 );
end CONCILIA_DETLLAM_TOTAL;
/
CREATE OR REPLACE package body CONCILIA_DETLLAM_TOTAL is

PROCEDURE DETALLE_SIN_TOTAL (periodo_ante In varchar2, 
                             periodo_act In varchar2,
                             anio In varchar2, 
                             nom_tabla In Varchar2, 
                             resultado out varchar2) IS
---periodo_ante se debe ingresar en el formato mm
---periodo_act se debe ingrear en el formato mm
---anio se debe ingresar en el fromat mm

cursor detalle is 
select /*+ rule +*/ a.co_id,c.customer_id,b.valid_from_date
from profile_service a, 
pr_serv_status_hist b, contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status='A' 
union all
select /*+ rule +*/ a.co_id,c.customer_id, b.valid_from_date
from profile_service a,
pr_serv_status_hist b, contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status ='S' and b.valid_from_date between to_date ('25/'||periodo_ante||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss') 
and to_date ('24/'||periodo_act||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss')union all
select /*+ rule +*/ a.co_id, c.customer_id, b.valid_from_date
from profile_service a,
pr_serv_status_hist b,  contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status ='D' and b.valid_from_date between to_date ('25/'||periodo_ante||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss') 
and to_date ('24/'||periodo_act||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss');


cursor temp is 
select * from BS_TMP_COID where estado is null;

periodo varchar2(10);
result varchar2(500);
le_error exception;
error varchar2(90);

begin    
 if periodo_ante > periodo_act then
   error:='Error al ingresar datos, revisar';
   raise le_error;
 end if;
 Begin
   insert into tmp_monitor_diana VALUES ('INICIO CARGA TABLA',SYSDATE);
   COMMIT;
   for j in detalle loop 
   insert into BS_TMP_COID values (J.CO_ID,J.CUSTOMER_ID,NULL,j.valid_from_date,NULL);
   end loop;
   commit;
   insert into tmp_monitor_diana VALUES ('FIN CARGA TABLA',SYSDATE);
   COMMIT;
 end;
 
 begin
 insert into tmp_monitor_diana VALUES ('INICIA CONSULTA LLAMADAS',SYSDATE);
 commit;
-- periodo:=anio||periodo_ante||periodo_act; 
--for i in temp loop
 for i  IN temp LOOP
   
     total_llamadas (i.co_id, i.customer_id,nom_tabla,result);
     update bs_tmp_coid set estado ='PROCESADO'---, fecha=sysdate
     where co_id=i.co_id and customer_id=i.customer_id;
     commit;
 end loop;
 if result is not null then
    resultado:=result;
 else
 resultado:='Finalizado. Revisar tabla es_registros con codigo_error 5';
 end if;
 
 insert into tmp_monitor_diana VALUES ('FIN CONSULTA LLAMADAS',SYSDATE);
 commit;
 end;
   --end;
exception
when le_error then 
resultado:=error;
rollback;

END DETALLE_SIN_TOTAL;  



procedure total_llamadas(contrato in number, custom in number, tabla in varchar2, mensaje out varchar2) is


cursor numero (coid number) is
select f.dn_num from 
contract_all d,
directory_number f, 
contr_services_cap g
where d.co_id=coid
and g.co_id=d.co_id 
and f.dn_id=g.dn_id;

--mensaje varchar2(400);
le_error exception;
le_error_cur exception;
entrantes number;
salientes number;

telefono varchar2(15);
dat_num numero%rowtype;
lv_sentencia varchar2(200);
lv_sentencia2 varchar2(200);
--tabla varchar2(40);
cursor_1 integer;
cursor_2 integer;
cursor_exe integer;
---existe boolean;
--numero varchar2(15);
--

begin

---tabla:='udr_lt_'||periodo;
lv_sentencia:='select count(*) salientes from '||tabla||'@BSCS_TO_RTX_LINK ';
lv_sentencia:=lv_sentencia||'where cust_info_customer_id='||custom||' and ';
lv_sentencia:=lv_sentencia||'cust_info_contract_id='||contrato||' and follow_up_call_type=1 and rownum=1';
cursor_1:=dbms_sql.open_cursor;
dbms_sql.parse(cursor_1,lv_sentencia,dbms_sql.v7);
dbms_sql.define_column(cursor_1,1,salientes);
cursor_exe:=dbms_sql.execute(cursor_1);

loop
  exit when DBMS_SQL.FETCH_ROWS(cursor_1) = 0;
  dbms_sql.column_value(cursor_1,1,salientes);
end loop;

if salientes=0 then
  cursor_exe:=null;
  lv_sentencia2:='select count(*) entrantes from '||tabla||'@BSCS_TO_RTX_LINK ';
  lv_sentencia2:=lv_sentencia2||'where cust_info_customer_id='||custom||' and ';
  lv_sentencia2:=lv_sentencia2||'cust_info_contract_id='||contrato||' and follow_up_call_type=2 and rownum=1';
  cursor_2:=dbms_sql.open_cursor;
  dbms_sql.parse(cursor_2,lv_sentencia2,dbms_sql.v7);
  dbms_sql.define_column(cursor_2,1,entrantes);
  cursor_exe:=dbms_sql.execute(cursor_2);
  loop
    exit when DBMS_SQL.FETCH_ROWS(cursor_2) = 0;
    dbms_sql.column_value(cursor_2,1,entrantes);
  end loop;

  if entrantes>0 then

      open numero(contrato);
      fetch numero into dat_num;
      close numero;
      
      telefono:=to_number(dat_num.dn_num);      

      begin 
      insert into es_registros 
      values (sysdate,5,custom,contrato,'A','REVISAR_DETALLE',sysdate,telefono,null,null,null,null,'DCH');
      
      EXCEPTION 
      WHEN OTHERS THEN
      raise le_error;
      end;

   end if;
   dbms_sql.close_cursor(cursor_2); 
end if;
dbms_sql.close_cursor(cursor_1); 

commit;      
Exception 
When  LE_ERROR Then
MENSAJE:='ERROR EN TOTAL LLAMADAS ' ||SQLERRM;
DBMS_OUTPUT.PUT_LINE(MENSAJE);
when others then
MENSAJE:='ERROR EN TOTAL_LLAMADAS '||SQLERRM;
Rollback;

end total_llamadas;

end CONCILIA_DETLLAM_TOTAL;
/

