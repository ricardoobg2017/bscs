CREATE OR REPLACE PACKAGE MFK_TRX_CRITERIOS_IVR IS

  TYPE gtr_forma_pago IS RECORD(
    code  VARCHAR2(10),
    names VARCHAR2(70),
    grupo VARCHAR2(60));
  TYPE gr_forma_pago IS REF CURSOR RETURN gtr_forma_pago;
  TYPE gt_forma_pago IS TABLE OF gtr_forma_pago INDEX BY BINARY_INTEGER;

  TYPE gtr_planes IS RECORD(
    code  VARCHAR2(10),
    names VARCHAR2(70),
    grupo VARCHAR2(60));
  TYPE gr_planes IS REF CURSOR RETURN gtr_planes;
  TYPE gt_planes IS TABLE OF gtr_planes INDEX BY BINARY_INTEGER;

  TYPE gtr_calificacion IS RECORD(
    code  VARCHAR2(10),
    names VARCHAR2(70),
    grupo VARCHAR2(60));
  TYPE gr_calificacion IS REF CURSOR RETURN gtr_calificacion;
  TYPE gt_calificacion IS TABLE OF gtr_calificacion INDEX BY BINARY_INTEGER;

  PROCEDURE mfp_forma_pago(pt_forma_pago IN OUT gt_forma_pago);

  PROCEDURE mfp_planes(pt_planes IN OUT gt_planes);

  PROCEDURE mfp_calificacion(pt_calificacion IN OUT gt_calificacion);

END MFK_TRX_CRITERIOS_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_TRX_CRITERIOS_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la carga de subcriterios de forma de pagos desde
  **               la tabla MF_FORMAS_PAGOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_forma_pago(pt_forma_pago IN OUT gt_forma_pago) AS
    
    CURSOR c_forma_pago IS
      SELECT idbanco code, nombre_banco names, nombre_grupo grupo
        FROM mf_formas_pagos_ivr
       ORDER BY 3, 1;
  
    ln_contador NUMBER;
  
  BEGIN
    
    ln_contador := 1;
    FOR i IN c_forma_pago LOOP
      pt_forma_pago(ln_contador).code := i.code;
      pt_forma_pago(ln_contador).names := i.names;
      pt_forma_pago(ln_contador).grupo := i.grupo;
      ln_contador := ln_contador + 1;
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      pt_forma_pago(1).code := '0';
      pt_forma_pago(1).names := 'SIN DATOS';
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la carga de subcriterios de planes desde  
  **               la tabla MF_PLANES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_planes(pt_planes IN OUT gt_planes) AS
    
    CURSOR c_planes IS
      SELECT p.tmcode code, p.descripcion names, p.id_plan grupo
        FROM mf_planes_ivr p;
  
    ln_contador NUMBER;
  
  BEGIN
    
    ln_contador := 1;
    FOR i IN c_planes LOOP
      pt_planes(ln_contador).code := i.code;
      pt_planes(ln_contador).names := i.names;
      pt_planes(ln_contador).grupo := i.grupo;
      ln_contador := ln_contador + 1;
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      pt_planes(1).code := '0';
      pt_planes(1).names := 'SIN DATOS';
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite la carga de subcriterios de categorias desde  
  **               la tabla PR_ASIGNACION_CUPO
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_calificacion(pt_calificacion IN OUT gt_calificacion) AS
    
    CURSOR c_calificacion IS
      SELECT DISTINCT p.tipo_rango code,
                      (SELECT MAX(t.valor_minimo) valor_minimo
                         FROM pr_asignacion_cupo t
                        WHERE t.tipo_rango = p.tipo_rango
                          AND t.id_gestion = p.id_gestion) names,
                      (SELECT MIN(t.valor_maximo) valor_minimo
                         FROM pr_asignacion_cupo t
                        WHERE t.tipo_rango = p.tipo_rango
                          AND t.id_gestion = p.id_gestion) grupo
        FROM pr_asignacion_cupo p
       WHERE p.id_gestion = 'IVR'
         AND p.estado = 'A'
       ORDER BY p.tipo_rango DESC;
  
    ln_contador NUMBER;
  
  BEGIN
    
    ln_contador := 1;
    FOR i IN c_calificacion LOOP
      pt_calificacion(ln_contador).code := i.code;
      pt_calificacion(ln_contador).names := i.names;
      pt_calificacion(ln_contador).grupo := i.grupo;
      ln_contador := ln_contador + 1;
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      pt_calificacion(1).code := '0';
      pt_calificacion(1).names := 'SIN DATOS';
  END;

END MFK_TRX_CRITERIOS_IVR;
/
