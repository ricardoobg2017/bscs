CREATE OR REPLACE PACKAGE Gck_Dc_Emergente_Bscs IS

   PROCEDURE Prc_Consulta_Currentdebt(Pv_Cuenta         IN VARCHAR2,
                                      Pv_Servicio       IN VARCHAR2,
                                      Pv_Saldo          OUT VARCHAR2,
                                      Pd_Fecha_Max_Pago OUT DATE,
                                      Pn_Error          OUT NUMBER,
                                      Pv_Error          OUT VARCHAR2);

   FUNCTION Getparametros(Pv_Cod_Parametro VARCHAR2) RETURN VARCHAR2;

END Gck_Dc_Emergente_Bscs;
/
CREATE OR REPLACE PACKAGE BODY Gck_Dc_Emergente_Bscs IS

   PROCEDURE Prc_Consulta_Currentdebt(Pv_Cuenta         IN VARCHAR2,
                                      Pv_Servicio       IN VARCHAR2,
                                      Pv_Saldo          OUT VARCHAR2,
                                      Pd_Fecha_Max_Pago OUT DATE,
                                      Pn_Error          OUT NUMBER,
                                      Pv_Error          OUT VARCHAR2) IS
   
      CURSOR Cur_Deudasaldo(Cv_Cuenta VARCHAR2) IS
         SELECT /*+ rule +*/
          a.Customer_Id, Nvl(SUM(b.Ohopnamt_Doc), 0) Saldo, MAX(b.Ohduedate) Fecha_Max_Pago
           FROM Customer_All a, Orderhdr_All b
          WHERE a.Custcode = Cv_Cuenta
            AND a.Customer_Dealer = 'C'
            AND a.Customer_Id = b.Customer_Id
            AND b.Ohstatus IN ('IN', 'CM', 'CO')
          GROUP BY a.Custcode, a.Customer_Id;
   
      CURSOR Cur_Cargosfechainiperiodo(Cn_Customerid           NUMBER,
                                       Cd_Fecha_Inicio_Periodo DATE,
                                       Cd_Fecha_Fin_Periodo    DATE) IS
         SELECT Nvl(SUM(Amount), 0) Cargosvarios
           FROM Sysadm.Fees b
          WHERE b.Customer_Id = Cn_Customerid
            AND b.Period = 1
            AND Trunc(b.Entdate) >= Cd_Fecha_Inicio_Periodo
            AND Trunc(b.Entdate) <= Cd_Fecha_Fin_Periodo;
   
      CURSOR Cur_Cargos(Cn_Customerid NUMBER) IS
         SELECT Nvl(SUM(Amount), 0) Cargosvarios
           FROM Sysadm.Fees b
          WHERE b.Customer_Id = Cn_Customerid
            AND b.Period = 1;
   
      CURSOR Cur_Customer_Service(Cv_Service VARCHAR2) IS
         SELECT c.Customer_Id
           FROM Directory_Number a, Contr_Services_Cap b, Contract_All c
          WHERE a.Dn_Id = b.Dn_Id
            AND b.Co_Id = c.Co_Id
            AND a.Dn_Num = Cv_Service
            AND b.Cs_Deactiv_Date IS NULL;
   
      CURSOR Cur_Deudasaldoservice(Cv_Customer_Id VARCHAR2) IS
         SELECT /*+ rule +*/
          a.Customer_Id,
          Nvl(SUM(b.Ohopnamt_Doc), 0) Saldo,
          MAX(b.Ohduedate) Fecha_Max_Pago,
          a.Custcode
           FROM Customer_All a, Orderhdr_All b
          WHERE a.Customer_Dealer = 'C'
            AND a.Customer_Id = b.Customer_Id
            AND a.Customer_Id = Cv_Customer_Id
            AND b.Ohstatus IN ('IN', 'CM', 'CO')
          GROUP BY a.Custcode, a.Customer_Id;
   
      Lc_Deudasaldo         Cur_Deudasaldo%ROWTYPE;
      Lc_Deudasaldoservicio Cur_Deudasaldoservice%ROWTYPE;
      Lc_Cargos             Cur_Cargos%ROWTYPE;
      Lc_Cargosperiodo      Cur_Cargosfechainiperiodo%ROWTYPE;
      Lb_Found              BOOLEAN;
      Lv_Valor_Bandera      VARCHAR2(1000) := NULL;
      Ln_Customer_Id        NUMBER := 0;
      Ln_Cargos             NUMBER := 0;
      Ld_Fechaini_Periodo   DATE;
      Ld_Fechafin_Periodo   DATE;
   
      ---Parametros SRE
      Lc_Trama          CLOB;
      Ln_Requerimiento  NUMBER;
      Ln_Instancia      NUMBER;
      Ln_Tipo_Trans     NUMBER;
      Ln_Id_Integrador  NUMBER;
      Ln_Cod_Alterno    NUMBER;
      Lv_Datasource     VARCHAR2(50) := NULL;
      Lv_Fault_Code     VARCHAR2(4000) := NULL;
      Lv_Fault_String   VARCHAR2(4000) := NULL;
      Ln_Error          NUMBER := 0;
      Lv_Error          VARCHAR2(4000) := NULL;
      Lx_Respuesta      Xmltype;
      Lv_Bandera_Cargos VARCHAR(50) := NULL;
   
      --
      Lc_Customer_Id     Cur_Customer_Service%ROWTYPE;
      Lv_Id_Servicio_Int VARCHAR2(50) := NULL;
      Ln_Saldo_Actual    Orderhdr_All.Ohopnamt_Doc%TYPE;
      Lv_Custcode        Customer_All.Custcode%TYPE;
      Le_Error_Sre       EXCEPTION;
   
   BEGIN
   
      IF (Pv_Cuenta IS NOT NULL) THEN
      
         OPEN Cur_Deudasaldo(Pv_Cuenta);
         FETCH Cur_Deudasaldo
            INTO Lc_Deudasaldo;
         Lb_Found := Cur_Deudasaldo%FOUND;
         CLOSE Cur_Deudasaldo;
      
         IF Lb_Found THEN
            Pd_Fecha_Max_Pago := Lc_Deudasaldo.Fecha_Max_Pago;
            Ln_Customer_Id    := Lc_Deudasaldo.Customer_Id;
            Ln_Saldo_Actual   := Lc_Deudasaldo.Saldo;
            Lv_Custcode       := Pv_Cuenta;
         END IF;
      
      ELSIF Pv_Servicio IS NOT NULL THEN
      
         Lv_Id_Servicio_Int := Obtiene_Telefono_Dnc_Int(Pv_Telefono         => Pv_Servicio,
                                                        Pn_Red              => NULL,
                                                        Pv_Version_Telefono => NULL);
      
         OPEN Cur_Customer_Service(Lv_Id_Servicio_Int);
         FETCH Cur_Customer_Service
            INTO Lc_Customer_Id;
         CLOSE Cur_Customer_Service;
      
         OPEN Cur_Deudasaldoservice(Lc_Customer_Id.Customer_Id);
         FETCH Cur_Deudasaldoservice
            INTO Lc_Deudasaldoservicio;
         Lb_Found := Cur_Deudasaldoservice%FOUND;
         CLOSE Cur_Deudasaldoservice;
      
         IF Lb_Found THEN
            Pd_Fecha_Max_Pago := Lc_Deudasaldoservicio.Fecha_Max_Pago;
            Ln_Customer_Id    := Lc_Deudasaldoservicio.Customer_Id;
            Ln_Saldo_Actual   := Lc_Deudasaldoservicio.Saldo;
            Lv_Custcode       := Lc_Deudasaldoservicio.Custcode;
         END IF;
      
      ELSE
         Pn_Error := 1;
         Pv_Error := 'DEBE INGRESAR LA CUENTA O EL NUMERO DE SERVICIO A CONSULTAR';
         RETURN;
      END IF;
   
      Lv_Valor_Bandera  := Getparametros('BANDERA_SRE_DC_EMERGENTE');
      Lv_Bandera_Cargos := Getparametros('BANDERA_HABILITA_CARGOS');
   
      IF Nvl(Lv_Bandera_Cargos, 'N') = 'S' THEN
      
         IF Nvl(Lv_Valor_Bandera, 'N') = 'S' THEN
         
            Ln_Requerimiento := To_Number(Getparametros('SB_REQUERIMIENTO'));
            Ln_Instancia     := To_Number(Getparametros('SB_ID_INSTANCIA'));
            Ln_Tipo_Trans    := To_Number(Getparametros('SB_TIPO_TRANSACCION'));
            Ln_Id_Integrador := To_Number(Getparametros('SB_ID_INTEGRADOR'));
            Ln_Cod_Alterno   := To_Number(Getparametros('SB_COD_ALTERNO'));
            Lv_Datasource    := Getparametros('SB_DATA_SOURCE');
            Lc_Trama         := 'Id_Instancia=' || Ln_Instancia || ';Id_Tipo_Transaccion=' ||
                                Ln_Tipo_Trans || ';Id_Integrador=' || Ln_Id_Integrador ||
                                ';Cod_Alterno_Distribuidor=' || Ln_Cod_Alterno || ';Data_Source=' ||
                                Lv_Datasource || ';CUENTA=' || Lv_Custcode || ';';
         
            Lx_Respuesta := Scp_Dat.Sck_Soap_Gtw.Scp_Consume_Servicio(Pn_Id_Req                   => Ln_Requerimiento,
                                                                      Pv_Parametros_Requerimiento => Lc_Trama,
                                                                      Pv_Fault_Code               => Lv_Fault_Code,
                                                                      Pv_Fault_String             => Lv_Fault_String);
         
            IF Lv_Fault_String IS NOT NULL THEN
               Ln_Error := 401;
               Lv_Error := 'ERROR AL CONSUMIR WEBSERVICES DEL SRE-SALDO ACTUAL - EXTRAE FECHA INICIO DE PERIODO AXIS: ' ||
                           Lv_Fault_String;
               RAISE Le_Error_Sre;
            ELSE
            
               FOR r IN (SELECT Extractvalue(VALUE(Xml_List), '/PCK_RESPUESTA/PER_INI/text()') AS Propertyvalue
                           FROM TABLE(Xmlsequence(Extract(Lx_Respuesta, '//SRE/PCK_RESPUESTA'))) Xml_List) LOOP
               
                  Ld_Fechaini_Periodo := To_Date(r.Propertyvalue, 'dd/mm/yyyy');
               
               END LOOP;
            
               FOR t IN (SELECT Extractvalue(VALUE(Xml_List), '/PCK_RESPUESTA/PER_FIN/text()') AS Propertyvalue
                           FROM TABLE(Xmlsequence(Extract(Lx_Respuesta, '//SRE/PCK_RESPUESTA'))) Xml_List) LOOP
               
                  Ld_Fechafin_Periodo := To_Date(t.Propertyvalue, 'dd/mm/yyyy');
               
               END LOOP;
            
            END IF;
         
            -- Invocacion al SRE
         
            OPEN Cur_Cargosfechainiperiodo(Ln_Customer_Id,
                                           Ld_Fechaini_Periodo,
                                           Ld_Fechafin_Periodo);
            FETCH Cur_Cargosfechainiperiodo
               INTO Lc_Cargosperiodo;
            CLOSE Cur_Cargosfechainiperiodo;
         
            Ln_Cargos := Lc_Cargosperiodo.Cargosvarios;
         
         ELSE
         
            OPEN Cur_Cargos(Ln_Customer_Id);
            FETCH Cur_Cargos
               INTO Lc_Cargos;
            CLOSE Cur_Cargos;
         
            Ln_Cargos := Lc_Cargos.Cargosvarios;
         
         END IF;
      
      END IF;
   
      Pv_Saldo := Nvl(Ln_Saldo_Actual, 0) + Nvl(Ln_Cargos, 0);
      Pn_Error := 0;
      Pv_Error := 'Success!';
   
   EXCEPTION
      WHEN Le_Error_Sre THEN
         Pn_Error := Ln_Error;
         Pv_Error := Lv_Error;
      WHEN OTHERS THEN
         Pn_Error := 1;
         Pv_Error := SQLERRM;
   END Prc_Consulta_Currentdebt;

   FUNCTION Getparametros(Pv_Cod_Parametro VARCHAR2) RETURN VARCHAR2 IS
   
      Lv_Valor_Par   VARCHAR2(1000);
      Lv_Proceso     VARCHAR2(1000);
      Lv_Descripcion VARCHAR2(4000);
      Ln_Error       NUMBER;
      Lv_Error       VARCHAR2(4000);
   
   BEGIN
      Scp_Dat.Sck_Api.Scp_Parametros_Procesos_Lee(Pv_Cod_Parametro,
                                                  Lv_Proceso,
                                                  Lv_Valor_Par,
                                                  Lv_Descripcion,
                                                  Ln_Error,
                                                  Lv_Error);
      RETURN Lv_Valor_Par;
   EXCEPTION
      WHEN OTHERS THEN
         RETURN NULL;
   END Getparametros;
END Gck_Dc_Emergente_Bscs;
/
