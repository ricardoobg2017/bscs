CREATE OR REPLACE PACKAGE GSIB_SECURITY IS
  -- 10189
  -- SIS Luis Flores
  -- PDS Fernando Ortega
  -- RGT Ney Miranda
  -- Ultimo Cambio: 30/07/2015 
  -- Paquete que genera la logica para usuarios, contraseņas, logs
  -- para sap
  --
  
  FUNCTION add_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2;
  -- 
  FUNCTION delete_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2;
  -- 
  FUNCTION valid_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2;
  --
  FUNCTION change_password(pv_id_usuario VARCHAR2, pv_old_password VARCHAR2, pv_new_password VARCHAR2) RETURN VARCHAR2;
  --
  FUNCTION reset_password(pv_id_usuario VARCHAR2) RETURN VARCHAR2;
  --
  PROCEDURE dotracel(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL);
  -- 
  PROCEDURE dotracem(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL);
  -- 
  PROCEDURE dotraceh(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL);

END GSIB_SECURITY;
/
CREATE OR REPLACE PACKAGE BODY GSIB_SECURITY IS

  -- Metodo para encriptar la contraseņa
  FUNCTION get_hash(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN dbms_obfuscation_toolkit.md5(input_string => upper(pv_id_usuario) || upper(pv_password));
  END;

  -- Agregar un nuevo usuario
  FUNCTION add_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2 IS
    lv_password   VARCHAR2(1000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_SECURITY.ADD_USER';
  BEGIN
    IF pv_password IS NULL THEN
      RETURN 'La contraseņa no puede ir vacia.';
    END IF;
  
    lv_password := get_hash(pv_id_usuario, pv_password);
    INSERT INTO gsib_usuarios (id_usuario, password, fec_creacion, estado) VALUES (pv_id_usuario, lv_password, SYSDATE, 'A');
    COMMIT;
  
    dotraceh(lv_aplicacion, 'Se registro nuevo usuario: ' || pv_id_usuario, sc_id_transaccion_sap.nextval);
    RETURN '';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Error. ' || SQLERRM;
  END;

  -- Eliminar un usuario logicamente
  FUNCTION delete_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2 IS
    lv_password   VARCHAR2(1000);
    lv_error      VARCHAR2(1000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_SECURITY.DELETE_USER';
  BEGIN
    lv_error := valid_user(pv_id_usuario, pv_password);
    IF lv_error IS NOT NULL THEN
      RETURN lv_error;
    END IF;
  
    lv_password := get_hash(pv_id_usuario, pv_password);
    UPDATE GSIB_USUARIOS a
       SET a.estado = 'I'
     WHERE a.id_usuario = pv_id_usuario
       AND a.password = lv_password
       AND a.estado = 'A';
    COMMIT;
  
    dotraceh(lv_aplicacion, 'Se elimino el usuario: ' || pv_id_usuario, sc_id_transaccion_sap.nextval);
    RETURN '';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Error. ' || SQLERRM;
  END;

  -- Validar el usuario
  FUNCTION valid_user(pv_id_usuario VARCHAR2, pv_password VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR c_existe IS
      SELECT 'X', estado
        FROM GSIB_USUARIOS x
       WHERE x.id_usuario = pv_id_usuario
         AND x.estado = 'A';
    lv_existe VARCHAR2(1);
    lv_estado VARCHAR2(1);
  
    -- validar la contraseņa
    CURSOR c_acceso(cv_password VARCHAR2) IS
      SELECT 'X'
        FROM GSIB_USUARIOS x
       WHERE x.id_usuario = pv_id_usuario
         AND x.password = cv_password
         AND x.estado = 'A';
    lv_acceso VARCHAR2(1);
  
    lv_password VARCHAR2(1000);
    --lv_aplicacion varchar2(100) := 'GSIB_SECURITY.VALID_USER';  
  
  BEGIN
    OPEN c_existe;
    FETCH c_existe
      INTO lv_existe, lv_estado;
    CLOSE c_existe;
  
    -- no existe
    IF lv_existe IS NULL THEN
      RETURN 'El usuario no existe.';
    END IF;
  
    -- no esta activo
    IF lv_estado = 'I' THEN
      RETURN 'El usuario esta inactivo';
    END IF;
  
    lv_password := get_hash(pv_id_usuario, pv_password);
  
    OPEN c_acceso(lv_password);
    FETCH c_acceso
      INTO lv_acceso;
    CLOSE c_acceso;
  
    -- la contraseņa no coinciden
    IF lv_acceso IS NULL THEN
      RETURN 'La contraseņa no coincide.';
    END IF;
  
    RETURN '';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Error. ' || SQLERRM;
  END;

  -- Cambiar la contraseņa del usuario
  FUNCTION change_password(pv_id_usuario VARCHAR2, pv_old_password VARCHAR2, pv_new_password VARCHAR2) RETURN VARCHAR2 IS
    lv_error      VARCHAR2(1000);
    lv_password   VARCHAR2(1000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_SECURITY.CHANGE_PASSWORD';
  BEGIN
    lv_error := valid_user(pv_id_usuario, pv_old_password);
    IF lv_error IS NOT NULL THEN
      RETURN lv_error;
    END IF;
  
    lv_password := get_hash(pv_id_usuario, pv_new_password);
    UPDATE GSIB_USUARIOS a
       SET a.password = lv_password
     WHERE a.id_usuario = pv_id_usuario
       AND a.estado = 'A';
  
    COMMIT;
    dotracem(lv_aplicacion, 'El usuario ' || pv_id_usuario || ', renovo su contraseņa', sc_id_transaccion_sap.nextval);
    RETURN '';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Error. ' || SQLERRM;
  END;

  -- Resetear la contraseņa con una de pruebas
  FUNCTION reset_password(pv_id_usuario VARCHAR2) RETURN VARCHAR2 IS
    lv_password   VARCHAR2(1000);
    lv_aplicacion VARCHAR2(100) := 'GSIB_SECURITY.RESET_PASSWORD';
  BEGIN
    lv_password := get_hash(pv_id_usuario, '12345');
    UPDATE GSIB_USUARIOS a
       SET a.password = lv_password
     WHERE a.id_usuario = pv_id_usuario
       AND a.estado = 'A';
    COMMIT;
    dotraceh(lv_aplicacion, 'Se reseteo la contraseņa al usuario. ' || pv_id_usuario, sc_id_transaccion_sap.nextval);
    RETURN '';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Error. ' || SQLERRM;
  END;

  -- Regitrar novedades
  PROCEDURE dotrace(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pv_prioridad VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    ln_sequencia NUMBER;
    lv_user      VARCHAR2(100);
    lv_ip        VARCHAR2(100);
    lv_host      VARCHAR2(100);
  BEGIN
    SELECT nvl(MAX(id_sequencia), 0) + 1 INTO ln_sequencia FROM gsib_novedades;
    SELECT USER, sys_context('userenv', 'ip_address'), sys_context('userenv', 'host') INTO lv_user, lv_ip, lv_host FROM dual;
    INSERT INTO GSIB_NOVEDADES
      (id_sequencia, id_usuario, proceso, observacion, fec_registro, prioridad, ipaddress, hostname, id_transaccion)
    VALUES
      (ln_sequencia, lv_user, substr(pv_proceso, 1, 250), substr(pv_observacion, 1, 500), SYSDATE, pv_prioridad, 
      substr(lv_ip, 1, 100), substr(lv_host, 1, 100), pn_id_transaccion);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      NULL;
  END;

  -- Novedades de bajo riesgo
  PROCEDURE dotracel(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL) IS
  BEGIN
    dotrace(pv_proceso, pv_observacion, 'BAJA', pn_id_transaccion);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  -- Novedades de medio riesgo
  PROCEDURE dotracem(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL) IS
  BEGIN
    dotrace(pv_proceso, pv_observacion, 'MEDIA', pn_id_transaccion);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  -- Novedades de alto riesgo
  PROCEDURE dotraceh(pv_proceso VARCHAR2, pv_observacion VARCHAR2, pn_id_transaccion NUMBER DEFAULT NULL) IS
  BEGIN
    dotrace(pv_proceso, pv_observacion, 'ALTA', pn_id_transaccion);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

END GSIB_SECURITY;
/
