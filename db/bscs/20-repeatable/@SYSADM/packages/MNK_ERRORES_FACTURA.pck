CREATE OR REPLACE package MNK_ERRORES_FACTURA is
  --===================================================
  -- Proyecto  : Buscador de Errores en Facturas de Clientes Celulares
  -- Autores   : Alan Pab� R.
  -- Fecha     : 15/Abril/2004
  -- Versi�n   : 1.0
  -- Descripci�n: Este procedimiento busca identificar los siguientes errores en la facturaci�n
  -- del "Cobro por Emisi�n de Factura(103)".
  -- .- Si tiene Detalle de Llamadas, no se cobra(103).--COBRO EMISION CON DETALLE DE LLAMADAS (6)
  -- .- Grupo de Planes exento de cobro, no se cobra(103) -- COBRO EMISION PLANES EXENTOS (7)
  -- .- Cobro de m�s de .50 por concepto de 103 -- MAL COBRO EN DOLARES POR EMISION (8)
  -- .- solo se puede cobrar uno de los tres (13 , 31 , 103) -- COBRO DE MAS DE UN RUBRO 13 31 103 (9)
  -- .- Si debe cobrar y no se cobra -- DEBE COBRAR Y NO SE COBRA EMISION (10)
  --  Tabla de Errores (ES_CONFIGURA)
  --
  --================= M E N U =============
--  
-- 1    Errores en Servicios (Features Cobro de Emisi�n de Factura (103))
-- 1.1	COBRO EMISION CON DETALLE DE LLAMADAS (6)
-- 1.2  COBRO EMISION PLANES EXENTOS (7)
--
  --=======================================
/*--
----------- 1 --------------------------
 procedure MNP_LLENA_ERRORES(p_fecha in varchar2,
                             p_error out varchar2);*/
--
----------- 1.1 --------------------------
 procedure MNP_103_DET_LLAMADAS(--p_telefono in varchar2,
                                p_fecha in varchar2,
                                p_error out varchar2);
--
----------- 1.2 --------------------------
 procedure MNP_103_PLAN_EXENTO(--p_telefono in varchar2,
                                p_fecha in varchar2,
                                p_error out varchar2);
--                                
end MNK_ERRORES_FACTURA;
/
CREATE OR REPLACE package body MNK_ERRORES_FACTURA is
lv_fecha          varchar2(20);
ln_contador       number;
ld_fecha          date;
-- 
/* procedure MNP_LLENA_ERRORES(p_fecha in varchar2,
                             p_error out varchar2) is 
cnt     number;                             
cursor err_emision is                             
  Select \*+ RULE +*\ cl.custcode cuenta, cl.customer_id cliente, co.co_id contrato, co.tmcode plan, cf.ohrefnum factura, 
                      df.otshipdate fecha, df.otamt_revenue_doc cobro,
        substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) feature
  from orderhdr_all cf, ordertrailer df, customer_all cl, contract_all co
  where cf.ohxact = df.otxact
    and cl.customer_id = co.customer_id
    and cl.customer_id = cf.customer_id
--    and cl.customer_id = 100203
--    and cl.custcode = '1.10080764'
    and substr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) in (13,31,103)
    and df.otshipdate = p_fecha;
--
BEGIN
--
  delete es_emision;
  commit;
	cnt := 0;
	for t in err_emision loop
  	begin
     insert into es_emision
       (cuenta, cliente, contrato, plan, factura, fecha, cobro, feature)
     values
       (t.cuenta, t.cliente, t.contrato, t.plan, t.factura, t.fecha, t.cobro, t.feature);
  --
    cnt := cnt + 1;
  		if mod(cnt, 100) = 0 then
  			commit;
  		end if;
  	end;
	end loop;
	exception
	when others then
		null;  
commit;
--
END MNP_LLENA_ERRORES;*/
--
-- 1.1	COBRO EMISION CON DETALLE DE LLAMADAS (6)
 procedure MNP_103_DET_LLAMADAS(p_fecha in varchar2,
                                p_error out varchar2) is 
--
ln_cont_det      number;
ln_cont_feature  number;
ln_facturacion   varchar2(10);
cursor clientes is
select /*+ use index(oh_cust_id_1) +*/ distinct cf.customer_id cliente from orderhdr_all cf
where cf.ohentdate = p_fecha
order by cf.customer_id;
--
 cursor mal_cobro(c_cliente varchar2) is
  Select /*+ RULE +*/ cl.custcode cuenta, cl.customer_id cliente, co.tmcode plan, cf.ohrefnum factura, 
                      df.otshipdate fecha, df.otamt_revenue_doc cobro,
        substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) feature
  from orderhdr_all cf, ordertrailer df, customer_all cl, contract_all co
  where cf.ohxact = df.otxact
    and cl.customer_id = co.customer_id
    and cl.customer_id = cf.customer_id
    and cl.customer_id = c_cliente
--    and cl.custcode = '1.10080764'
    and substr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) in (13,31,103)
    and df.otshipdate = p_fecha
 order by cl.customer_id;
--
 begin
   For i in clientes Loop
   ln_cont_det       := 0;
   ln_cont_feature   := 0;
       For j in mal_cobro(i.cliente) Loop
       ln_facturacion := j.fecha;
           If j.feature in (13,31) Then
              ln_cont_det := ln_cont_det + 1;
           End If;
           If j.feature = 103 Then
              ln_cont_feature := ln_cont_feature + 1; 
           End If;           
       End Loop;
   If ln_cont_det > 0 and ln_cont_feature > 0 Then
      insert into es_registros2
        (fecha_ejec, codigo_error, customer_id, co_id, status, observacion, fecha, telefono, estado_axis, fecha_axis_ini)
      values
        (sysdate, 6, i.cliente, null, 'A', 'Al menos uno de los telefonos tiene detalle de llamadas', ln_facturacion, null, null, null);
--
      commit;
   End If;
   End Loop;        
--     
 end MNP_103_DET_LLAMADAS;
-- 
-- 1.2 COBRO EMISION PLANES EXENTOS (7)
 procedure MNP_103_PLAN_EXENTO(p_fecha in varchar2,
                               p_error out varchar2) is 
--
ln_cont_plan     number;
ln_cont_feature  number;
ln_cont_contra   number;
ln_facturacion   varchar2(10);
cursor clientes is
select /*+ use index(oh_cust_id_1) +*/ distinct cf.customer_id cliente from orderhdr_all cf
where cf.ohentdate = p_fecha
order by cf.customer_id;
--
 cursor mal_cobro(c_cliente varchar2) is
  Select /*+ RULE +*/ cl.custcode cuenta, cl.customer_id cliente, co.tmcode plan, cf.ohrefnum factura, 
                      df.otshipdate fecha, df.otamt_revenue_doc cobro,
        substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) feature
  from orderhdr_all cf, ordertrailer df, customer_all cl, contract_all co
  where cf.ohxact = df.otxact
    and cl.customer_id = co.customer_id
    and cl.customer_id = cf.customer_id
    and cl.customer_id = c_cliente
--    and cl.custcode = '1.10080764'
    and substr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
        instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
        instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
        instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) = 103
    and df.otshipdate = p_fecha
 order by cl.customer_id;
--
 begin
   For i in clientes Loop
   ln_cont_plan      := 0;
   ln_cont_feature   := 0;
   ln_cont_contra    := 0;
       For j in mal_cobro(i.cliente) Loop
       ln_facturacion := j.fecha;
       ln_cont_contra := ln_cont_contra + 1;
           If j.plan not in (9,72,73,76,77,80,81,82,83,91,92,93,94,95,96,101,109,118,
                             134,135,153,156,157,160,164,165,168,186,187,188,189,190,
                             191,192,202,203,204,205,284,212,215,275,281,272) Then
              ln_cont_plan := ln_cont_plan + 1;
           End If;
           If j.feature = 103 Then
              ln_cont_feature := ln_cont_feature + 1;
           End If;
       End Loop;
   If ln_cont_contra = ln_cont_plan  and ln_cont_feature > 0 Then
      insert into es_registros2
        (fecha_ejec, codigo_error, customer_id, co_id, status, observacion, fecha, telefono, estado_axis, fecha_axis_ini)
      values
        (sysdate, 7, i.cliente, null, 'A', 'Se cobra emision a los planes exentos', ln_facturacion, null, null, null);
--
      commit;
   End If;
   End Loop;
 end MNP_103_PLAN_EXENTO;
--
end MNK_ERRORES_FACTURA;
/

