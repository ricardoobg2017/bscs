CREATE OR REPLACE PACKAGE CLK_RECLASIFICACION IS

  PROCEDURE gcp_genera_principal(pv_fecha IN VARCHAR2,
                                 pv_cia   IN VARCHAR2,
                                 pv_ciclo IN VARCHAR2,
                                 pn_hilo  IN NUMBER,
                                 pv_error OUT VARCHAR2);

  PROCEDURE gcp_cartera_mensual(pv_fecha IN VARCHAR2,
                                pv_ciclo IN VARCHAR2,
                                pn_hilo  IN NUMBER,
                                pv_error OUT VARCHAR2);

  PROCEDURE gcp_cartera_reclasificar(pv_fecha IN VARCHAR2,
                                     pv_ciclo IN VARCHAR2,
                                     pn_hilo  IN NUMBER,
                                     pv_error OUT VARCHAR2);

  PROCEDURE gcp_cartera_castigar(pn_hilo IN NUMBER, pv_error OUT VARCHAR2);

  FUNCTION gcf_obtiene_clientes(pv_dia IN VARCHAR2, pv_fecha IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE clp_obtiene_info_adic(pv_cia   VARCHAR2,
                                  pv_hilo  VARCHAR2,
                                  pv_error OUT VARCHAR2);

  FUNCTION gcf_obtiene_ciclo(pv_cuenta VARCHAR2) RETURN VARCHAR2;

  PROCEDURE clp_cuenta_cartera(pv_cuenta IN VARCHAR2,
                               pn_cia    IN NUMBER,
                               pn_cart   OUT NUMBER,
                               pn_saldo  OUT NUMBER,
                               pn_mora   OUT NUMBER,
                               pv_error  OUT VARCHAR2);

  PROCEDURE clp_consulta_deuda(pv_cuenta IN VARCHAR2,
                               pn_deuda  OUT NUMBER,
                               pv_error  OUT VARCHAR2);

  PROCEDURE gcp_genera_journal(pv_fecha   IN VARCHAR2,
                               pv_ciclo   IN VARCHAR2,
                               pn_hilo    IN VARCHAR2,
                               pv_usuario IN VARCHAR2,
                               pv_credit  IN VARCHAR2,
                               pv_formapa IN VARCHAR2,
                               pv_error   OUT VARCHAR2);

  PROCEDURE cop_generar_pagos(pv_cuenta   IN VARCHAR2,
                              pv_fechaini IN VARCHAR2,
                              pv_fechafin IN VARCHAR2 DEFAULT NULL,
                              pv_ciclo    IN VARCHAR2,
                              pv_clase    IN VARCHAR2 DEFAULT NULL,
                              pv_usuario  IN VARCHAR2 DEFAULT NULL,
                              pv_credit   IN VARCHAR2 DEFAULT NULL,
                              pv_formapa  IN VARCHAR2 DEFAULT NULL,
                              pv_error    OUT VARCHAR2);

  PROCEDURE cop_generar_credito(pv_cuenta   IN VARCHAR2,
                                pv_fechaini IN VARCHAR2,
                                pv_fechafin IN VARCHAR2,
                                pv_ciclo    IN VARCHAR2,
                                pv_clase    IN VARCHAR2,
                                pv_usuario  IN VARCHAR2,
                                pv_credit   IN VARCHAR2,
                                pv_error    OUT VARCHAR2);

  PROCEDURE cop_generar_cargos(pv_cuenta   IN VARCHAR2,
                               pv_fechaini IN VARCHAR2,
                               pv_fechafin IN VARCHAR2,
                               pv_ciclo    IN VARCHAR2,
                               pv_clase    IN VARCHAR2,
                               pv_usuario  IN VARCHAR2,
                               pv_credit   IN VARCHAR2,
                               pv_error    OUT VARCHAR2);

  PROCEDURE gcp_respaldo_tabla(pv_fecha IN VARCHAR2, pv_error OUT VARCHAR2);

  PROCEDURE genera_preasiento_rec(PV_FECHACTUAL   IN VARCHAR2,
                                  PV_FECHANTERIOR IN VARCHAR2,
                                  PD_FECHA        IN DATE,
                                  PV_ERROR        OUT VARCHAR2);
  /*
  procedure pr_obtiene_cadena(PV_FEATURES          OUT VARCHAR2,
                              PV_FEATURES_COMILLAS OUT VARCHAR2,
                              PN_VIGENCIA          OUT NUMBER,
                              PN_NOTIFICACION      OUT NUMBER,
                              PN_ERROR             OUT NUMBER,
                              PV_ERROR             OUT VARCHAR2);*/

  FUNCTION gvf_obtener_valor_parametro(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;

  PROCEDURE gvp_actualiza_parametro(pn_id_tipo_parametro IN NUMBER,
                                    pv_id_parametro      IN VARCHAR2,
                                    pv_valor             IN VARCHAR2,
                                    pv_error             OUT VARCHAR2);

  PROCEDURE gcp_gen_cuentas_vencer(pn_region     IN NUMBER,
                                   pv_fecha      IN VARCHAR2,
                                   pv_name_file1 OUT VARCHAR2,
                                   pv_name_file2 OUT VARCHAR2,
                                   pv_error      OUT VARCHAR2);

  PROCEDURE gcp_gen_cuentas_pagos(pn_region     IN NUMBER,
                                  pv_fecha      IN VARCHAR2,
                                  pv_name_file1 OUT VARCHAR2,
                                  pv_error      OUT VARCHAR2);
                                  
  PROCEDURE gcp_gen_reporte_final(pn_region     IN NUMBER,
                                   pv_fecha      IN VARCHAR2,
                                   pv_name_file1 OUT VARCHAR2,
                                   pv_error      OUT VARCHAR2) ;   
                                   
  PROCEDURE clp_mail_notificacion_final(PV_FECHA IN VARCHAR2, 
                                      PV_CIA IN VARCHAR2, 
                                      PV_ARCHIVO IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2) ;                                                                  

  PROCEDURE clp_mail_notificacion(PV_FECHA   IN VARCHAR2,
                                  PV_CIA     IN VARCHAR2,
                                  PV_ARCHIVO IN VARCHAR2,
                                  PV_TRAMA_ARCHIVOS IN VARCHAR2, -- SUD CAC :: 062015
                                  PV_ERROR   OUT VARCHAR2);

  PROCEDURE clp_mail_notificacion2(PV_FECHA IN VARCHAR2,
                                   PV_CIA   IN VARCHAR2, --PV_ARCHIVO IN VARCHAR2,
                                   PV_ERROR OUT VARCHAR2);

  FUNCTION gcf_obtiene_mes RETURN VARCHAR2;
  
  PROCEDURE genera_preasiento_rec_sap(PV_FECHACTUAL   IN VARCHAR2,
                                      PV_FECHANTERIOR IN VARCHAR2,
                                      PD_FECHA        IN DATE,
                                      PV_ERROR        OUT VARCHAR2);

  PROCEDURE GENERA_SECUENCIA_POLIZA(PV_DOCUMENTO OUT VARCHAR2,
                                    PV_ERROR     OUT VARCHAR2);

  procedure reiniciar_secuencia(p_nombre varchar2);

  procedure reiniciar_secuencia_cas(p_nombre varchar2);

  PROCEDURE GENERA_SECUENCIA_POLIZA_CAS(PV_DOCUMENTO OUT VARCHAR2,
                                        PV_ERROR     OUT VARCHAR2);

  PROCEDURE GCP_DESPACHA_TRAMA_SAP(PV_TRAMA IN CLOB, PV_ERROR OUT VARCHAR2);

    PROCEDURE GCP_INSERTAR_TRAMA(PV_PROCESO          IN VARCHAR2,
                                 PC_TRAMA            IN CLOB,
                                 PV_ESTADO_DESPACHO  IN VARCHAR2 DEFAULT NULL,
                                 PV_ESTADO_REPROCESO IN VARCHAR2 DEFAULT NULL,
                                 PD_FECHA_INGRESO    IN DATE DEFAULT NULL,
                                 PD_FECHA_DESPACHO   IN DATE DEFAULT NULL,
                                 PD_FECHA_REPROCESO  IN DATE DEFAULT NULL,
                                 PV_COMPANIA         IN VARCHAR2,
                                 PN_MONTO            IN NUMBER,
                                 PV_MSG_ERROR        OUT VARCHAR2) ;

  PROCEDURE GCP_ACTUALIZAR_TRAMA(PN_ID_TRAMA         IN NUMBER,
                                 PV_PROCESO          IN VARCHAR2,
                                 PC_TRAMA            IN CLOB DEFAULT NULL,
                                 PV_ESTADO_DESPACHO  IN VARCHAR2 DEFAULT NULL,
                                 PV_ESTADO_REPROCESO IN VARCHAR2 DEFAULT NULL,
                                 PD_FECHA_DESPACHO   IN DATE DEFAULT NULL,
                                 PD_FECHA_REPROCESO  IN DATE DEFAULT NULL,
                                 PV_COMENTARIO       IN VARCHAR2 DEFAULT NULL,
                                 PV_COMPANIA         IN VARCHAR2,
                                 PV_MSG_ERROR        OUT VARCHAR2);

  PROCEDURE GCP_ENVIA_TRAMAS(PV_ERROR OUT VARCHAR2);
  
  PROCEDURE genera_reporte_excel(pv_directorio in varchar2,
                                 pv_file       in varchar2,
                                 pv_query      in varchar2);
  PROCEDURE clp_mail_notifica_pagos(PV_FECHA IN VARCHAR2, 
                                        PV_CIA IN VARCHAR2, 
                                        PV_ARCHIVO IN VARCHAR2,
                                        PV_ERROR OUT VARCHAR2);
  
  --10855 INI SUD KAE
  PROCEDURE genera_reporte_excel_spool(pv_arch_ref   IN VARCHAR2,
                                       pv_file       IN VARCHAR2,
                                       pv_query      IN VARCHAR2,
                                       pv_directorio IN VARCHAR2,
                                       pv_path_tipo  IN VARCHAR2 DEFAULT NULL);
  --10855 FIN SUD KAE--

--INICIO [10995] IRO_HRO 02/03/2017

function GCP_OBTIENE_CTAS_INACT(PV_CUENTA   IN VARCHAR2) return boolean;

PROCEDURE GENERA_PREASIENTO_SAP_DTH(PV_FECHACTUAL   IN VARCHAR2,
                                    PV_FECHANTERIOR IN VARCHAR2,
                                    PD_FECHA        IN DATE,
                                    PV_ERROR        OUT VARCHAR2);

--FIN [10995] IRO_HRO 02/03/2017

END CLK_RECLASIFICACION;
/
CREATE OR REPLACE PACKAGE BODY CLK_RECLASIFICACION IS
  v_fh        UTL_FILE.FILE_TYPE;
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Procedimiento Principal, que se encarga de invocar al resto de procesos
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--
  /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      JUAN ROMERO
    CREADO POR:     IRO HUGO ROMERO  IRO_HRO 
    PROYECTO:       [10995] Reloj de Cobranzas - Reclasificacion de Cartera
    FECHA:          02/03/2017
    PROP�SITO:      Se modifica el procedimiento GCP_CARTERA_RECLASIFICAR para que envie a reclasificar las 
                    cuentas, unicamente si su estado es inactivo.
    SIGLAS DE BUSQ: IRO_HRO                    
  **********************************************************************************************************/
  /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      IRO JUAN ROMERO
    CREADO POR:     IRO HUGO ROMERO 
    PROYECTO:       [11188] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
    FECHA:          13/04/2017
    PROP�SITO:      Se modifica el procedimiento GCP_CARTERA_CASTIGAR para que envie el producto (MOVIL o DTH)
                    de las cuentas que deben ser castigadas en la tabla CO_CARTERA_CLIENTES.
    SIGLAS DE BUSQ: IRO_HRO                    
  **********************************************************************************************************/
    /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      IRO JUAN ROMERO
    CREADO POR:     IRO HUGO ROMERO 
    PROYECTO:       [11334] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
    FECHA:          18/05/2017
    PROP�SITO:      Se adiciona la elaboraci�n de un reporte con el detalle de las cuentas no procesadas por 
                    los modulos de reclasificaci�n y castigo.
    SIGLAS DE BUSQ: IRO_HRO                    
  ***********************************************************************************************************/
  /**********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      IRO JUAN ROMERO
    CREADO POR:     IRO GENESIS LINDAO 
    PROYECTO:       [11603] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
    FECHA:          21/11/2017
    PROP�SITO:      Ajustes en la l�gica de obtenci�n de cuentas mediante hilos en los procedimientos 
    		    gcp_genera_journal y el clp_obtiene_info_adic
    SIGLAS DE BUSQ: IRO_GL                    
  ***********************************************************************************************************/
 
  PROCEDURE gcp_genera_principal(pv_fecha IN VARCHAR2,
                                 pv_cia   IN VARCHAR2,
                                 pv_ciclo IN VARCHAR2,
                                 pn_hilo  IN NUMBER,
                                 pv_error OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(150) := 'CLK_RECLASIFICACION.GCP_GENERA_PRINCIPAL - ';
    lv_error      VARCHAR2(4000);
    le_error EXCEPTION;
  
  BEGIN
  
    -- Llama a proceso que realiza la carga de cuentas: Reclasificadas o Castigadas
    gcp_cartera_mensual(pv_fecha => pv_fecha,
                        pv_ciclo => pv_ciclo,
                        pn_hilo  => pn_hilo,
                        pv_error => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
    -- Llama a proceso que adiciona datos a las cuentas cargadas
    clp_obtiene_info_adic(pv_cia   => pv_cia,
                          pv_hilo  => to_char(pn_hilo),
                          pv_error => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
    -- Llama a proceso que ingresa los pagos, creditos y cargos a las cuentas
    gcp_genera_journal(pv_fecha   => pv_fecha,
                       pv_ciclo   => pv_ciclo,
                       pn_hilo    => pn_hilo,
                       pv_usuario => 'TODOS',
                       pv_credit  => 'TODOS',
                       pv_formapa => 'TODOS',
                       pv_error   => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que realiza la carga de cuentas: Reclasificadas o Castigadas
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE gcp_cartera_mensual(pv_fecha IN VARCHAR2,
                                pv_ciclo IN VARCHAR2,
                                pn_hilo  IN NUMBER,
                                pv_error OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(150) := 'GCP_CARTERA_MENSUAL - ';
    lv_error      VARCHAR2(2000);
    le_error EXCEPTION;
  
  BEGIN
  
    -- Llama al proceso que carga las cuentas reclasificadas (a 330 dias)
    gcp_cartera_reclasificar(pv_fecha => pv_fecha,
                             pv_ciclo => pv_ciclo,
                             pn_hilo  => pn_hilo,
                             pv_error => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
    -- Llama al proceso que carga las cuentas castigadas (a 60 meses)
    gcp_cartera_castigar(pn_hilo => pn_hilo, pv_error => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE le_error;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END gcp_cartera_mensual;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que carga las Cuentas Reclasificadas (a 330 dias) y las inserta en la 
  --           tabla CO_CARTERA_CLIENTES con tipo_proceso: 'P'
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 06/06/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Liberar proceso para ejecucion en R1.
  --===============================================================================================--

  PROCEDURE gcp_cartera_reclasificar(pv_fecha IN VARCHAR2,
                                     pv_ciclo IN VARCHAR2,
                                     pn_hilo  IN NUMBER,
                                     pv_error OUT VARCHAR2) IS
  
    -- Cursor que obtiene los ciclos de facturacion   
    CURSOR c_obtiene_ciclo IS
      SELECT id_ciclo, dia_ini_ciclo
        FROM fa_ciclos_bscs
       WHERE id_ciclo != 05
         AND id_ciclo = nvl(pv_ciclo, id_ciclo);
  
    -- Cursor que verifica valores parametrizados
    CURSOR c_parametros(cv_parametro VARCHAR2) IS
      SELECT o.valor
        FROM gv_parametros o
       WHERE o.id_tipo_parametro = 7399
         AND o.id_parametro = cv_parametro;
  
    TYPE tc_cartmens IS REF CURSOR;
    rc_repcartcli tc_cartmens;
  
    ln_resultado NUMBER;
    le_exception EXCEPTION;
    lv_error         VARCHAR2(2000);
    lv_nombre_tabla  VARCHAR2(32650);
    lv_nombre_tabla2 VARCHAR2(32650);
    lb_found         BOOLEAN := FALSE;
    lv_linea_cam2    VARCHAR2(32650);
    lv_nombre_cam2   VARCHAR2(32650);
    lv_commit        VARCHAR2(100);
    lv_sentencia1    VARCHAR2(32650);
    lv_sentencia     VARCHAR2(32650);
    lv_tabla_temp    VARCHAR2(200) := 'CO_CARTERA_CLIENTES';
    lv_dias          VARCHAR2(40) := '''240'',''270'',''300'',''330'''; --number := 240;
    --
    --[11188] IRO_HRO 15/03/2017
    --lv_gv_producto   gv_parametros.valor%type := gvf_obtener_valor_parametro(7399,'PRODUCTO');
    --
    --lv_dias240       VARCHAR2(20) := '240'; --number := 240;
    --lv_dias270       VARCHAR2(20) := '270';
    --lv_dias300       VARCHAR2(20) := '300';
    --lv_dias330       VARCHAR2(20) := '330'; --number := 330;
    lv_fecha         VARCHAR2(20);
    lv_index1        VARCHAR2(200);
    lv_index2        VARCHAR2(200);
    ln_contador      NUMBER := 0;
    ln_hilo          NUMBER;
    lv_proceso       VARCHAR2(2) := 'P';
    lv_dia_ini_ciclo VARCHAR2(2);
    lv_aplicacion    VARCHAR2(150) := 'GCP_CARTERA_RECLASIFICAR - ';
    ln_mod           VARCHAR2(2);
    --ln_exe           VARCHAR2(2); -- 7399 20140606 SUD MTR
  
    lv_cia    co_cartera_clientes.cia%TYPE;
    lv_cuenta co_cartera_clientes.cuenta%TYPE;
    --
    lv_ciclo co_cartera_clientes.ciclo%type;
    --
    lv_producto       co_cartera_clientes.producto%TYPE;
    lv_canton         co_cartera_clientes.canton%TYPE;
    lv_provincia      co_cartera_clientes.provincia%TYPE;
    lv_apellidos      co_cartera_clientes.apellidos%TYPE;
    lv_nombres        co_cartera_clientes.nombres%TYPE;
    lv_identificacion co_cartera_clientes.identificacion%TYPE;
    ln_totaladeuda    co_cartera_clientes.totaladeuda%TYPE;
    lv_mora           co_cartera_clientes.mora%TYPE;
    lv_direcccion     co_cartera_clientes.direcccion%TYPE;
    lv_telf1          VARCHAR2(200);--co_cartera_clientes.telf1%type;
    lv_telf2          VARCHAR2(200);--co_cartera_clientes.telf2%type;
    lv_estado       VARCHAR2(2);--[11334] IRO_HRO 18/05/2017
  
  BEGIN
  
    -- Verifica parametro de entrada
    IF pn_hilo IS NULL THEN
      lv_error := 'Ingrese el numero de hilo a procesar';
      RAISE le_exception;
    END IF;
  
    ln_hilo := pn_hilo;
  
    -- Toma la fecha ingresada con 30 dias atras
    lv_fecha := to_char(add_months(to_date(pv_fecha, 'DD/MM/RRRR'), -1),
                        'DD/MM/RRRR');
  
    -- Recorre cada uno de los ciclos de facturacion
    FOR i IN c_obtiene_ciclo LOOP
      lv_dia_ini_ciclo := i.dia_ini_ciclo;
      lv_ciclo         := i.id_ciclo;
    
      -- Llama a la funcion que verifica si existen las tablas de donde se extraen las cuentas
      -- dependiendo del ciclo de facturacion y de la fecha ingresada
      ln_resultado := gcf_obtiene_clientes(pv_dia   => lv_dia_ini_ciclo,
                                           pv_fecha => lv_fecha);
    
      IF ln_resultado IS NULL OR ln_resultado = 0 THEN
        lv_error := 'NO EXISTE LA TABLA ' || lv_dia_ini_ciclo ||
                    substr(lv_fecha, 4, 2) || substr(lv_fecha, 7) ||
                    ', FAVOR REVISAR!';
        RAISE le_exception;
      END IF;
    
      -- Obtiene los campos de la tabla de trabajo co_cartera_clientes
      OPEN c_parametros('CAMPOS_TABLA_CARTERA');
      FETCH c_parametros
        INTO lv_linea_cam2;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurado los campos de la tabla co_cartera_clientes';
        RAISE le_exception;
      END IF;
    
      -- Obtiene los campos de las tablas de donde se extraeran las cuentas reclasificadas
      OPEN c_parametros('CAMPOS_TABLA_DETALLE');
      FETCH c_parametros
        INTO lv_nombre_cam2;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurado los campos de la tabla de cuentas reclasificadas';
        RAISE le_exception;
      END IF;
    
      -- Obtiene el n�mero en el cual se hara commit
      OPEN c_parametros('NUM_COMMIT_G');
      FETCH c_parametros
        INTO lv_commit;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurado el n�mero de commit para las cuentas reclasificadas';
        RAISE le_exception;
      END IF;
    
      -- Obtiene el valor de MOD para ejecutar por hilos el proceso
      OPEN c_parametros('RAC_MOD');
      FETCH c_parametros
        INTO ln_mod;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurado el valor MOD para la ejecucion por hilos del proceso';
        RAISE le_exception;
      END IF;
    
      -- Obtiene la compania para ejecutar el proceso --> DEFAULT = GUAYAQUIL
      /*OPEN c_parametros('COMPANIA_EXE');
      FETCH c_parametros
        INTO ln_exe;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;*/ --7399 20140606 SUD MTR
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurada la compania para la ejecucion del proceso';
        RAISE le_exception;
      END IF;
    
      -- Se arman los nombres de las tablas de donde se extraeran las cuentas reclasificadas
      lv_nombre_tabla  := 'CO_REPCARCLI_' || lv_dia_ini_ciclo ||
                          substr(lv_fecha, 4, 2) || substr(lv_fecha, 7) || ' R';
      lv_nombre_tabla2 := 'CO_REPORTE_ADIC_' || lv_dia_ini_ciclo ||
                          substr(lv_fecha, 4, 2) || substr(lv_fecha, 7) || ' C';
    
      -- Se arman los nombres de los indices
      lv_index1 := ' /*+ INDEX (R, CO_REPCARCLI_' || lv_dia_ini_ciclo ||
                   substr(lv_fecha, 4, 2) || substr(lv_fecha, 7) || '_PK) ' ||
                   '+*/';
    
      lv_index2 := ' /*+ INDEX (C, CO_REPORTE_ADIC_' || lv_dia_ini_ciclo ||
                   substr(lv_fecha, 4, 2) || substr(lv_fecha, 7) || '_PK) ' ||
                   '+*/';
    
      -- Se arma la sentencia de insercion
      lv_sentencia1 := 'insert /*+ APPEND +*/  into ' || lv_tabla_temp || ' (' ||
                       lv_linea_cam2 || ')';
      -- Filtro
    
      -- Se arma sentencia para obtener cuentas que pasaran a reclasificacion
      /*      lv_sentencia := ' select ' || lv_index1 || '  ' || lv_index2 || '  ' ||
       lv_nombre_cam2 || ' from ' || lv_nombre_tabla || ',' ||
       lv_nombre_tabla2 || ' where R.cuenta = C.cuenta ' ||
       ' and R.totaladeuda <> 0 
           and (R.mayorvencido = :1 
           or R.mayorvencido = :2 
           or R.mayorvencido = :3 
           or R.mayorvencido = :4) ' ||
       ' AND  R.forma_pago !=  ' ||
       '''CARTERA RECLASIFICADA''' ||
      --' And SUBSTR(R.CUENTA,-1)= :2 ';
       ' And MOD(SUBSTR(R.CUENTA,3),:5)= :6 ';*/
    
      lv_sentencia := ' select ' || lv_index1 || '  ' || lv_index2 || '  ' ||
                      lv_nombre_cam2 || ' from ' || lv_nombre_tabla || ',' ||
                      lv_nombre_tabla2 || ' where R.cuenta = C.cuenta ' ||
                      ' and R.totaladeuda <> 0 '|| 
                      --' and R.producto not in('||lv_gv_producto||')' ||--[11188] IRO_HRO 15/03/2017
                      ' and (R.mayorvencido in (' ||
                      lv_dias || ') ) ' || ' AND  R.forma_pago !=  ' ||
                      '''CARTERA RECLASIFICADA''' ||
                     --' And SUBSTR(R.CUENTA,-1)= :2 ';
                      ' And MOD(SUBSTR(R.CUENTA,3),:1)= :2 '|| 
		      -- INICIO [11603] IRO_GL 21/11/2017
                      ' AND NOT EXISTS(SELECT CL.CUENTA FROM '||
                      lv_tabla_temp||' CL WHERE CL.CUENTA=C.cuenta)';
		      -- FIN [10603] IRO_GL 21/11/2017
    
      -- Obtiene las cuentas reclasificadas                
      OPEN rc_repcartcli FOR lv_sentencia
      --USING lv_totaldias, ln_hilo;
      --USING lv_dias240, lv_dias270, lv_dias300, lv_dias330, to_number(ln_mod), ln_hilo;
        USING /*lv_dias,*/
      to_number(ln_mod), ln_hilo;
    
      -- Recorre cada cuenta y la inserta en la tabla de trabajo co_cartera_clientes
      LOOP
        FETCH rc_repcartcli
          INTO lv_cia,
               lv_cuenta,
               lv_producto,
               lv_provincia,
               lv_canton,
               lv_apellidos,
               lv_nombres,
               lv_identificacion,
               ln_totaladeuda,
               lv_mora,
               lv_direcccion,
               lv_telf1,
               lv_telf2;
        EXIT WHEN rc_repcartcli%NOTFOUND;
           
       --INICIO [10995] IRO_HRO 02/03/2017
	     IF GCP_OBTIENE_CTAS_INACT (PV_CUENTA=> LV_CUENTA) THEN
         --[11334] IRO_HRO 18/05/2017
         lv_estado:='C';
         lv_error:='';
       ELSE
         lv_estado:='A';
         lv_error:='La cuenta est� activa.';
       END IF;
         --[11334] IRO_HRO 18/05/2017                            
       lv_sentencia := lv_sentencia1 ||
                        ' VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)';--[11334] IRO_HRO 18/05/2017
        --IF ln_exe = lv_cia THEN -- 7399 20140606 SUD MTR
          BEGIN
            EXECUTE IMMEDIATE lv_sentencia
              USING lv_cia, lv_cuenta, lv_ciclo, lv_producto, lv_provincia, lv_canton, lv_apellidos, lv_nombres, lv_identificacion, ln_totaladeuda, lv_mora, SUBSTR(lv_telf1,1,12), SUBSTR(lv_telf2,1,12), lv_direcccion, sysdate, lv_proceso, lv_estado,lv_error;--[11334] IRO_HRO 18/05/2017
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
              -- Si alguna cuenta da error, inserta en la misma tabla co_cartera_clientes con
              -- la observacion respectiva
              INSERT /*+ APPEND +*/
              INTO co_cartera_clientes
                (cia, cuenta, totaladeuda, proceso, comentario, estado)
              VALUES
                (lv_cia,
                 lv_cuenta,
                 ln_totaladeuda,
                 lv_proceso,
                 lv_error,--[11334] IRO_HRO 18/05/2017
                 'E');
             COMMIT;
             ln_contador := 0;                
          END;
          IF ln_contador > to_number(lv_commit) THEN
            COMMIT;
            ln_contador := 0;
          END IF;
          ln_contador := ln_contador + 1;
         --FIN [10995] IRO_HRO 02/03/2017
        --END IF; -- LN_EXE -- 7399 20140606 SUD MTR
      END LOOP;
      CLOSE rc_repcartcli;
      --   
      COMMIT;
    END LOOP;
  
  EXCEPTION
    WHEN le_exception THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END gcp_cartera_reclasificar;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que carga las Cuentas Castigadas (a 60 meses) y las inserta en la 
  --           tabla CO_CARTERA_CLIENTES con tipo_proceso: 'R'
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE gcp_cartera_castigar(pn_hilo IN NUMBER, pv_error OUT VARCHAR2) IS
  
    -- Cursor que verifica valores parametrizados
    CURSOR c_parametros(cv_parametro VARCHAR2) IS
      SELECT o.valor
        FROM gv_parametros o
       WHERE o.id_tipo_parametro = 7399
         AND o.id_parametro = cv_parametro;
  
    TYPE tc_cartmens IS REF CURSOR;
    rc_repcartcli tc_cartmens;
  
    le_exception EXCEPTION;
    lv_error      VARCHAR2(2000);
    lb_found      BOOLEAN := FALSE;
    lv_linea_cam2 VARCHAR2(32650);
    lv_commit     VARCHAR2(100);
    lv_sentencia1 VARCHAR2(32650);
    lv_sentencia  VARCHAR2(32650);
    lv_tabla_temp VARCHAR2(200) := 'CO_CARTERA_CLIENTES';
    ln_contador   NUMBER := 0;
    ln_hilo       NUMBER;
    lv_proceso    VARCHAR2(2) := 'R';
    lv_aplicacion VARCHAR2(150) := 'GCP_CARTERA_CASTIGAR - ';
    ln_cartera    number;
    ln_mod        VARCHAR2(2);
  
    lv_cia         co_cartera_clientes.cia%TYPE;
    lv_cuenta      co_cartera_clientes.cuenta%TYPE;
    ln_totaladeuda co_cartera_clientes.totaladeuda%TYPE;
    lv_mora        co_cartera_clientes.mora%TYPE;
    LV_PRODUCTO    co_cartera_clientes.PRODUCTO%TYPE;  -- [11603] 21/11/2017 IRO_GL
    --
    lv_mora_pendiente co_cartera_clientes.mora%TYPE;
  
    --INICIO [11188] IRO_HRO 13/04/2017
    --CURSOR PARA OBTENER EL PRGCODE DE LA CUENTA
    CURSOR C_OBTENER_PRGCODE(CV_CUENTA VARCHAR2) IS
      SELECT A.PRGCODE
        FROM CUSTOMER_ALL A
       WHERE A.CUSTCODE = decode(substr(CV_CUENTA, 1, 1),
                                 '1',
                                 CV_CUENTA,
                                 CV_CUENTA || '.00.00.100000');
  
    LV_PRGCODE  VARCHAR2(10) := NULL;
    --LV_PRODUCTO VARCHAR2(10) := NULL;   -- [11603] 21/11/2017 IRO_GL
    --FIN [11188] IRO_HRO 13/04/2017
  
  BEGIN
  
    -- Verifica parametro de entrada
    IF pn_hilo IS NULL THEN
      lv_error := 'Ingrese el numero de hilo a procesar';
      RAISE le_exception;
    END IF;
  
    ln_hilo := pn_hilo;
  
    -- Obtiene los campos de la tabla de trabajo co_cartera_clientes
    OPEN c_parametros('CAMPO_MORA60');
    FETCH c_parametros
      INTO lv_linea_cam2;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado los campos de la tabla co_cartera_clientes';
      RAISE le_exception;
    END IF;
  
    -- Obtiene el n�mero en el cual se hara commit
    OPEN c_parametros('NUM_COMMIT_G');
    FETCH c_parametros
      INTO lv_commit;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado el n�mero de commit para las cuentas castigadas';
      RAISE le_exception;
    END IF;
  
    -- Obtiene el valor de MOD para ejecutar por hilos el proceso
    OPEN c_parametros('RAC_MOD');
    FETCH c_parametros
      INTO ln_mod;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado el valor MOD para la ejecucion por hilos del proceso';
      RAISE le_exception;
    END IF;
    -- Se obtiene el tiempo de mora no reclasificado a castigo
    OPEN c_parametros('MORA_PENDIENTE');
    FETCH c_parametros
      INTO lv_mora_pendiente;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado el valor de mora pendiente del proceso';
      RAISE le_exception;
    END IF;
    -- Se arma la sentencia de insercion
    lv_sentencia1 := 'insert /*+ APPEND +*/  into ' || lv_tabla_temp || ' (' ||
                     lv_linea_cam2 || ')';
    -- Se arma sentencia para obtener cuentas que pasaran a castigo
    OPEN c_parametros('C_MORA60');
    FETCH c_parametros
      INTO lv_sentencia;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado los campos de la tabla co_cartera_clientes';
      RAISE le_exception;
    END IF;
    -- Obtiene las cuentas castigadas                
    OPEN rc_repcartcli FOR lv_sentencia
      USING TO_NUMBER(lv_mora_pendiente), TO_NUMBER(ln_mod), ln_hilo;
      --USING TO_NUMBER(ln_mod), ln_hilo;
  
    -- Recorre cada cuenta y la inserta en la tabla de trabajo co_cartera_clientes
    LOOP
      FETCH rc_repcartcli
        INTO lv_cia, lv_cuenta, ln_totaladeuda, lv_mora, ln_cartera, LV_PRODUCTO; -- [11603] 21/11/2017 IRO_GL
      EXIT WHEN rc_repcartcli%NOTFOUND;
      IF (LV_PRODUCTO IS NULL) THEN     -- [11603] 21/11/2017 IRO_GL
      --INICIO [11188] IRO_HRO 13/04/2017
      LV_PRGCODE:=null;
      OPEN C_OBTENER_PRGCODE(LV_CUENTA);
      FETCH C_OBTENER_PRGCODE
        INTO LV_PRGCODE;
      CLOSE C_OBTENER_PRGCODE;
    
      IF LV_PRGCODE = 7 THEN
        LV_PRODUCTO := 'DTH';
      ELSE
        LV_PRODUCTO := 'SMA';
      END IF;
      --FIN [11188] IRO_HRO 13/04/2017
      END IF;     -- [11603] 21/11/2017 IRO_GL
    
      lv_sentencia := lv_sentencia1 || ' VALUES (:1,:2,:3,:4,:5,:6,:7,:8)';
      BEGIN
        EXECUTE IMMEDIATE lv_sentencia
          USING lv_cia, lv_cuenta, ln_totaladeuda, lv_mora, sysdate, lv_proceso, 'C', LV_PRODUCTO;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
          -- Si alguna cuenta da error, inserta en la misma tabla co_cartera_clientes con
          -- la observacion respectiva
          INSERT /*+ APPEND +*/
          INTO co_cartera_clientes
            (cia, cuenta, totaladeuda, proceso, comentario, estado)
          VALUES
            (lv_cia,
             lv_cuenta,
             ln_totaladeuda,
             lv_proceso,
             substr(lv_error, 1, 2000),
             'E');
          COMMIT;
          ln_contador := 0;            
      END;
    
      IF ln_contador > to_number(lv_commit) THEN
        COMMIT;
        ln_contador := 0;
      END IF;
      ln_contador := ln_contador + 1;
    END LOOP;
    CLOSE rc_repcartcli;
    COMMIT;
  
  EXCEPTION
    WHEN le_exception THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END gcp_cartera_castigar;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Funcion que verifica si existen las tablas de donde se extraen las cuentas
  --           dependiendo del ciclo de facturacion y de la fecha ingresada
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  FUNCTION gcf_obtiene_clientes(pv_dia IN VARCHAR2, pv_fecha IN VARCHAR2)
    RETURN NUMBER IS
  
    lv_nombre_tabla2 VARCHAR2(2000);
    lv_nombre_tabla  VARCHAR2(2000);
    lv_sentencia     VARCHAR2(32650);
    lv_error         VARCHAR2(1000);
    ln_existe_tabla  NUMBER;
    le_exception EXCEPTION;
  
  BEGIN
  
    -- Se arman los nombres de las tablas de donde se extraeran las cuentas
    lv_nombre_tabla := 'CO_REPCARCLI_' || pv_dia || substr(pv_fecha, 4, 2) ||
                       substr(pv_fecha, 7);
  
    lv_nombre_tabla2 := 'CO_REPORTE_ADIC_' || pv_dia ||
                        substr(pv_fecha, 4, 2) || substr(pv_fecha, 7);
  
    -- Arma la sentencia dinamica para la primera tabla
    lv_sentencia := 'select count(*) from all_tables where table_name = :1';
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        INTO ln_existe_tabla
        USING lv_nombre_tabla;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_exception;
        END IF;
    END;
  
    -- Valida el resultado de la sentencia
    IF ln_existe_tabla IS NULL OR ln_existe_tabla = 0 THEN
      lv_error := 'NO EXISTE LA TABLA ' || lv_nombre_tabla ||
                  ', FAVOR REVISAR!';
      RAISE le_exception;
    END IF;
  
    -- Arma la sentencia dinamica para la segunda tabla
    lv_sentencia := 'select count(*) from all_tables where table_name = :2';
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        INTO ln_existe_tabla
        USING lv_nombre_tabla2;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_exception;
        END IF;
    END;
  
    -- Valida el resultado de la sentencia
    IF ln_existe_tabla IS NULL OR ln_existe_tabla = 0 THEN
      RAISE le_exception;
    END IF;
    RETURN 1;
  
  EXCEPTION
    WHEN le_exception THEN
      RETURN 0;
  END gcf_obtiene_clientes;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que adiciona datos a las cuentas cargadas en la tabla CO_CARTERA_CLIENTES
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE clp_obtiene_info_adic(pv_cia   VARCHAR2,
                                  pv_hilo  VARCHAR2,
                                  pv_error OUT VARCHAR2) IS
  
    -- Cursor que verifica valores parametrizados
    CURSOR c_parametros(cv_parametro VARCHAR2) IS
      SELECT o.valor
        FROM gv_parametros o
       WHERE o.id_tipo_parametro = 7399
         AND o.id_parametro = cv_parametro;
  
    -- Cursor para obtener las cuentas a las que se adicionaran datos
    CURSOR c_obtiene_cuentas(cv_cia  VARCHAR2,
                             cv_hilo VARCHAR2,
                             n_hilo  number) IS
      SELECT /*+ INDEX (R, IDX_CARTERA_CUENTA_CICLO) +*/
       r.cuenta, r.ciclo, r.proceso    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
        FROM co_cartera_clientes r
       WHERE r.cia = nvl(cv_cia, r.cia)
            --AND substr(r.cuenta, -1) = cv_hilo
         AND MOD(substr(r.cuenta, 3), n_hilo) = cv_hilo
         AND r.ciclo IS NULL
         AND r.estado ='C';    -- [11603] 21/11/2017 IRO_GL
  
    lv_cia  VARCHAR2(10);
    lv_hilo VARCHAR2(10);
    le_exception EXCEPTION;
    lv_error        VARCHAR2(4000);
    lb_found        BOOLEAN := FALSE;
    lv_commit       VARCHAR2(100);
    ln_contador     NUMBER := 0;
    lv_cuenta_ciclo VARCHAR2(200);
    lv_sentencia    VARCHAR2(32650);
    lv_forma_pago   VARCHAR2(32650);
    lv_tipo_cliente VARCHAR2(32650);
    lv_descripcion  VARCHAR2(60);
    lv_aplicacion   VARCHAR2(150) := 'CLP_OBTIENE_INFO_ADIC - ';
    ln_mod          VARCHAR2(2);
    le_exception1 EXCEPTION;
  
    -- Para obtener la forma de pago de las cuentas
    TYPE tc_forma_pago IS REF CURSOR;
    rc_forma_pago    tc_forma_pago;
    lv_id_forma_pago VARCHAR2(3);
    ln_id_contrato   NUMBER(10);
  
    -- Para obtener tipo de cliente de la cuenta
    TYPE tc_obtener_tipo_cliente IS REF CURSOR;
    rc_obtener_tipo_cliente tc_obtener_tipo_cliente;
  
  BEGIN
  
    -- Verifica parametro de entrada
    IF pv_hilo IS NULL THEN
      lv_error := 'Ingrese el numero de hilo a procesar';
      RAISE le_exception;
    END IF;
  
    lv_cia  := pv_cia;
    lv_hilo := pv_hilo;
  
    -- Obtiene el n�mero en el cual se hara commit
    OPEN c_parametros('NUM_COMMIT_G');
    FETCH c_parametros
      INTO lv_commit;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    IF NOT lb_found THEN
      lv_error := 'No se encuentra configurado el n�mero de commit para las cuentas castigadas';
      RAISE le_exception;
    END IF;
  
    -- Obtiene el valor de MOD para ejecutar por hilos el proceso
    OPEN c_parametros('RAC_MOD');
    FETCH c_parametros
      INTO ln_mod;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    -- Obtiene las cuentas a las que se adicionaran datos
    FOR j IN c_obtiene_cuentas(lv_cia, lv_hilo, to_number(ln_mod)) LOOP
    
      -- Encera las variables
      --lv_cuenta_ciclo  := NULL;
      lv_id_forma_pago := NULL;
      ln_id_contrato   := NULL;
      lv_descripcion   := NULL;
      lv_error         := NULL;
    
      -- Llama a funcion que obtiene el ciclo de facturacion de las cuentas
      lv_cuenta_ciclo := gcf_obtiene_ciclo(pv_cuenta => j.cuenta);
    
      -- Obtiene el query para obtener la forma de pago de las cuentas
      OPEN c_parametros('C_FORMA_PAGO');
      FETCH c_parametros
        INTO lv_forma_pago;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurada la busqueda de forma de pago para las cuentas';
        RAISE le_exception;
      END IF;
    
      -- Obtiene el query para obtener el tipo de cliente de las cuentas
      OPEN c_parametros('C_TIPO_CLIENTE');
      FETCH c_parametros
        INTO lv_tipo_cliente;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurada la busqueda de tipo de cliente para las cuentas';
        RAISE le_exception;
      END IF;
    
      -- Obtiene la forma de pago
      OPEN rc_forma_pago FOR lv_forma_pago USING j.cuenta, j.cuenta;
      LOOP
        FETCH rc_forma_pago INTO lv_id_forma_pago, ln_id_contrato;
        EXIT WHEN rc_forma_pago%ROWCOUNT = 1 OR rc_forma_pago%NOTFOUND;
      END LOOP;
      CLOSE rc_forma_pago;
    
      -- Obtiene el tipo de cliente
      OPEN rc_obtener_tipo_cliente FOR lv_tipo_cliente
        USING ln_id_contrato;
      LOOP
        FETCH rc_obtener_tipo_cliente
          INTO lv_descripcion;
        EXIT WHEN rc_obtener_tipo_cliente%ROWCOUNT = 1 OR rc_obtener_tipo_cliente%NOTFOUND;
      END LOOP;
      CLOSE rc_obtener_tipo_cliente;
    
      -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
      lv_sentencia := 'UPDATE /*+INDEX (g, IDX_CARTERA_CUENTA_CICLO)+*/ 
                           CO_CARTERA_CLIENTES G
                       SET CICLO =  :1 ' ||
                      ' , FORMA_PAGO = :2 ' || ' , TIPO_CLIENTE =  :3 ' ||
                      ' , ESTADO = :4 ' || ' WHERE CUENTA = :5  AND PROCESO = :6 ';   -- 11603 15/11/2017 Iro Jromero, se adiciona campo proceso 
      BEGIN
      
        IF lv_cuenta_ciclo IS NULL AND j.ciclo IS NULL THEN
          -- INICIO [11603] 21/11/2017 IRO_GL 
          -- Para cuentas a castigar no es necesario validar el ciclo, se le asigna de forma gen�rica el ciclo 01
          IF j.PROCESO = 'P' THEN 
          lv_error := 'No se encuentra ciclo para la cuenta' || j.cuenta;
          RAISE le_exception1;
	ELSE
             lv_cuenta_ciclo:='01';
          END IF; 
          -- FIN [11603] 21/11/2017 IRO_GL  
        ELSIF lv_cuenta_ciclo IS NULL AND j.ciclo IS NOT NULL THEN
          lv_cuenta_ciclo := j.ciclo;
        END IF;
      
        EXECUTE IMMEDIATE lv_sentencia
          USING lv_cuenta_ciclo, lv_id_forma_pago, lv_descripcion, 'F', j.cuenta, j.proceso;  -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
      
      EXCEPTION
        WHEN le_exception1 THEN
          lv_error := lv_aplicacion || substr(lv_error, 1, 200);
          -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
          -- la observacion respectiva       
          UPDATE co_cartera_clientes
             SET comentario = lv_error, estado = 'E'
           WHERE cuenta = j.cuenta;
           COMMIT;
           ln_contador := 0;
        WHEN OTHERS THEN
          lv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
          -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
          -- la observacion respectiva       
          UPDATE co_cartera_clientes
             SET comentario = lv_error, estado = 'E'
           WHERE cuenta = j.cuenta;
           COMMIT;
           ln_contador := 0;
      END;
    
      IF ln_contador > to_number(lv_commit) THEN
        COMMIT;
        ln_contador := 0;
      END IF;
      ln_contador := ln_contador + 1;
    END LOOP;
    COMMIT;
  
  EXCEPTION
    WHEN le_exception THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END clp_obtiene_info_adic;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Funcion que obtiene el ciclo de facturacion de la cuenta
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  FUNCTION gcf_obtiene_ciclo(pv_cuenta VARCHAR2) RETURN VARCHAR2 IS
  
    -- Cursor para obtener el ciclo de facturacion
    CURSOR c_ciclo(cv_billcycle VARCHAR2) IS
      SELECT /*+ RULE +*/
       ci.id_ciclo
        FROM fa_ciclos_axis_bscs ci
       WHERE ci.id_ciclo_admin = cv_billcycle;
  
    -- Cursor que obtiene el billcycle a la que pertenece la cuenta
    CURSOR c_cliente(cv_cuenta VARCHAR2) IS
      SELECT c.billcycle, c.customer_id
        FROM customer_all c
       WHERE c.custcode = cv_cuenta;
  
    lv_ciclo   VARCHAR2(2);
    lb_found   BOOLEAN;
    lc_cliente c_cliente%ROWTYPE;
  
  BEGIN
  
    -- Obtiene el billcycle
    OPEN c_cliente(pv_cuenta);
    FETCH c_cliente
      INTO lc_cliente;
    CLOSE c_cliente;
  
    -- Obtiene el ciclo de facturacion
    OPEN c_ciclo(lc_cliente.billcycle);
    FETCH c_ciclo
      INTO lv_ciclo;
    lb_found := c_ciclo%FOUND;
    CLOSE c_ciclo;
  
    -- Si no encuentra el ciclo
    IF NOT lb_found THEN
      lv_ciclo := NULL;
    END IF;
    RETURN lv_ciclo;
  END;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que busca cuenta reclasificada en Cartera (GYE)
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE clp_cuenta_cartera(pv_cuenta IN VARCHAR2,
                               pn_cia    IN NUMBER,
                               pn_cart   OUT NUMBER,
                               pn_saldo  OUT NUMBER,
                               pn_mora   OUT NUMBER,
                               pv_error  OUT VARCHAR2) IS
    -- Cursor que verifica valores parametrizados
    CURSOR c_parametros(cv_parametro VARCHAR2) IS
      SELECT o.valor
        FROM gv_parametros o
       WHERE o.id_tipo_parametro = 7399
         AND o.id_parametro = cv_parametro;
  
    le_exception EXCEPTION;
    lv_aplicacion VARCHAR2(150) := 'CLP_CUENTA_CARTERA - ';
    lv_error      VARCHAR2(2000);
    ln_cia        NUMBER;
    lv_cuenta     VARCHAR2(24);
    lv_sentencia  VARCHAR2(32650);
  
    TYPE tc_cartera IS REF CURSOR;
    rc_cartera   tc_cartera;
    ln_cmpn      NUMBER;
    lv_nume_cuen VARCHAR2(24);
    ln_tipo_cart NUMBER := NULL;
    ln_edad_mora NUMBER;
    ln_saldo     NUMBER;
  
  BEGIN
    IF NVL(pv_cuenta, '0') = '0' THEN
      lv_error := 'Ingrese la cuenta a procesar';
      RAISE le_exception;
    END IF;
  
    IF NVL(pn_cia, 0) = 0 THEN
      lv_error := 'Ingrese la region de la cuenta a procesar';
      RAISE le_exception;
    END IF;
  
    lv_cuenta := pv_cuenta;
    ln_cia    := pn_cia;
  
    OPEN c_parametros('C_CARTERA');
    FETCH c_parametros
      INTO lv_sentencia;
    CLOSE c_parametros;
  
    -- Busca cuenta en cartera
    OPEN rc_cartera FOR lv_sentencia
      USING lv_cuenta, ln_cia;
    LOOP
      FETCH rc_cartera
        INTO ln_cmpn, lv_nume_cuen, ln_tipo_cart, ln_edad_mora;
      EXIT WHEN rc_cartera%NOTFOUND;
    END LOOP;
    CLOSE rc_cartera;
  
    IF NVL(ln_tipo_cart, -1) = -1 THEN
      lv_error := 'INCONSISTENCIA ENTRE BSCS & GYE. REPORTAR. ';
      RAISE le_exception;
    ELSIF NVL(ln_tipo_cart, -1) <> -1 THEN
      IF ln_saldo <= 0 THEN
        lv_error := 'NO HAY DEUDA VENCIDA. ';
      END IF;
      pn_cart  := ln_tipo_cart;
      pn_saldo := ln_saldo;
      pn_mora  := ln_edad_mora;
    END IF;
  
  EXCEPTION
    WHEN le_exception THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,1000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que obtiene deuda vencida o deuda actual dependiento el periodo en el 
  --           que me encuentre realizando la consulta
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 09/01/2015
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Ajuste a proceso - Creditos diferentes de cero
  --===============================================================================================--

  PROCEDURE clp_consulta_deuda(pv_cuenta IN VARCHAR2,
                               pn_deuda  OUT NUMBER,
                               pv_error  OUT VARCHAR2) IS
  
    CURSOR c_deuda_pendiente_actual(cv_cuenta VARCHAR2) IS
      SELECT /*+ rule +*/
       nvl(SUM(ohinvamt_doc), 0) deuda, nvl(SUM(o.ohopnamt_doc), 0) saldo
        FROM customer_all c, orderhdr_all o
       WHERE c.custcode = cv_cuenta
         AND c.customer_dealer = 'C'
         AND c.customer_id = o.customer_id
         AND o.ohstatus IN ('IN', 'CM', 'CO')
       GROUP BY c.custcode;
  
    CURSOR c_creditos(cv_cuenta VARCHAR2, cv_inicio_periodo VARCHAR2) IS  
      SELECT /*+ rule +*/
       nvl(SUM(amount), 0) creditos
        FROM customer_all a, fees b
       WHERE a.custcode = cv_cuenta
         AND a.customer_id = b.customer_id
         AND b.period <> 0 -- SUD MTR 18122014
         AND b.sncode = 46
         /*AND trunc(b.entdate) >= to_date(cv_inicio_periodo, 'dd/mm/rrrr')*/; --SUD CAC 18122014
  
    lc_deuda_pendiente_actual c_deuda_pendiente_actual%ROWTYPE;
    lc_creditos               c_creditos%ROWTYPE;
    lb_notfound               BOOLEAN;
    lb_found                  BOOLEAN;
    ln_deuda                  NUMBER(10, 2) := 0;
    ln_creditos               NUMBER := 0;
    ln_saldo_real             NUMBER := 0;
    lv_inicio_periodo         VARCHAR2(10);
    lv_cuenta                 customer_all.custcode%TYPE;
    lv_aplicacion             VARCHAR2(150) := 'CLP_CONSULTA_DEUDA - ';
  
  BEGIN
  
    lv_cuenta := pv_cuenta;
  
    OPEN c_deuda_pendiente_actual(lv_cuenta);
    FETCH c_deuda_pendiente_actual
      INTO lc_deuda_pendiente_actual;
    lb_notfound := c_deuda_pendiente_actual%NOTFOUND;
    CLOSE c_deuda_pendiente_actual;
  
    IF lb_notfound THEN
      pn_deuda := 0;
    
    ELSE
      ln_deuda := lc_deuda_pendiente_actual.saldo;
    END IF;
    -- Obtener creditos realizados durante el mes y no aplicados
    OPEN c_creditos(lv_cuenta, lv_inicio_periodo);
    FETCH c_creditos
      INTO lc_creditos;
    lb_found := c_creditos%FOUND;
    CLOSE c_creditos;
    --
    IF lb_found THEN
      ln_creditos := lc_creditos.creditos;
    ELSE
      ln_creditos := 0;
    END IF;
    --
    ln_saldo_real := ln_deuda + ln_creditos;
  
    --No tiene deuda pendiente y puede continuar con la transaccion
    IF ln_saldo_real > 0 THEN
      pn_deuda := ln_saldo_real;
    ELSE
      --Tiene deuda
      pn_deuda := 0;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que consulta e ingresa los pagos, creditos y cargos a las cuentas
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE gcp_genera_journal(pv_fecha   IN VARCHAR2,
                               pv_ciclo   IN VARCHAR2,
                               pn_hilo    IN VARCHAR2,
                               pv_usuario IN VARCHAR2,
                               pv_credit  IN VARCHAR2,
                               pv_formapa IN VARCHAR2,
                               pv_error   OUT VARCHAR2) IS
  
    -- Cursor que obtiene las cuentas procesadas                       
    CURSOR c_obtiene_cuentas(cv_ciclo VARCHAR2,
                             hilo     VARCHAR2,
                             n_hilo   number) IS
      SELECT /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/
       g.cia, g.cuenta, g.ciclo, g.identificacion, g.producto, g.proceso, g.estado  -- [11603] 21/11/2017 IRO_GL 
        FROM co_cartera_clientes g
       WHERE g.ciclo = nvl(cv_ciclo, g.ciclo)
            --AND substr(g.cuenta, -1) = hilo
         AND MOD(substr(g.cuenta, 3), n_hilo) = hilo
         AND g.total IS NULL
         AND g.ESTADO IN ('F','C');    -- [11603] 21/11/2017 IRO_GL 
  
    -- Cursor que verifica valores parametrizados
    CURSOR c_parametros(cv_parametro VARCHAR2) IS
      SELECT o.valor
        FROM gv_parametros o
       WHERE o.id_tipo_parametro = 7399
         AND o.id_parametro = cv_parametro;
  
    -- Cursor que obtiene el saldo de la cuenta
    CURSOR c_obtiene_saldo(cv_cuenta VARCHAR2) IS
      SELECT cuenta, (nvl(totaladeuda, 0) - nvl(pagos, 0)) saldo
        FROM co_cartera_clientes t
       WHERE cuenta = cv_cuenta;
  
    -- Cursor que obtiene el valor total de la cuenta
    CURSOR c_obtiene_total(cv_cuenta VARCHAR2) IS
      SELECT cuenta,
             (nvl(saldos, 0) + nvl(credito, 0) + nvl(cargo, 0)) total
        FROM co_cartera_clientes t
       WHERE cuenta = cv_cuenta;
  
    -- Cursor que obtiene los ciclos de facturacion
    CURSOR c_obtiene_ciclo IS
      SELECT dia_ini_ciclo
        FROM fa_ciclos_bscs
       WHERE id_ciclo != 05
         AND id_ciclo = nvl(pv_ciclo, id_ciclo);
  
    lv_error VARCHAR2(4000);
    le_exception EXCEPTION;
    le_error     EXCEPTION;
    ln_contador     NUMBER := 0;
    lv_commit       NUMBER;
    lb_found        BOOLEAN;
    lv_ciclo        VARCHAR2(2);
    ln_hilo         VARCHAR2(2);
    lv_sentencia    VARCHAR2(3650);
    lc_total_cuenta c_obtiene_total%ROWTYPE;
	  /*ECARFO-12519 EPMV INI*/
    lv_parametro_12519 varchar2(1);
    /*ECARFO-12519 EPMV FIN*/						 
    lc_saldo        c_obtiene_saldo%ROWTYPE;
    lv_fecha_ini    VARCHAR2(50);
    lv_fecha_fin    VARCHAR2(50);
    lv_aplicacion   VARCHAR2(150) := 'GCP_GENERA_JOURNAL - ';
    ld_fecha        DATE;
    ln_deuda        NUMBER;
    lv_estado       VARCHAR2(2);
    lv_comentario   VARCHAR2(1000);
    ln_mod          VARCHAR2(2);
  
  BEGIN
  
    -- Verifica parametro de entrada
    IF pn_hilo IS NULL THEN
      lv_error := 'Ingrese el numero de hilo a procesar';
      RAISE le_exception;
    END IF;
  
    lv_ciclo := pv_ciclo;
    ln_hilo  := pn_hilo;
  
    ld_fecha := add_months(to_date(pv_fecha, 'DD/MM/RRRR'), -1);
  
    -- Obtiene el valor de MOD para ejecutar por hilos el proceso
    OPEN c_parametros('RAC_MOD');
    FETCH c_parametros
      INTO ln_mod;
    lb_found := c_parametros%FOUND;
    CLOSE c_parametros;
  
    -- Recorre cada uno de los ciclos de facturacion
    FOR h IN c_obtiene_ciclo LOOP
    
      -- Arma la fecha de inicio para el procesamiento
      lv_fecha_ini := h.dia_ini_ciclo || '/' || to_char(ld_fecha, 'MM') || '/' ||
                      to_char(ld_fecha, 'YYYY');
    
      -- Arma la fecha fin para el procesamiento
      lv_fecha_fin := substr(pv_fecha, 1, 2) || '/' ||
                      substr(pv_fecha, 4, 2) || '/' || substr(pv_fecha, 7);
    
      IF lv_fecha_ini IS NULL THEN
        lv_error := 'No se ingresado la fecha de inicio';
        RAISE le_exception;
      END IF;
    
      IF lv_fecha_fin IS NULL THEN
        lv_error := 'No se ha ingresado la fecha fin';
        RAISE le_exception;
      END IF;
    
      -- Obtiene el n�mero en el cual se hara commit
      OPEN c_parametros('NUM_COMMIT_G');
      FETCH c_parametros
        INTO lv_commit;
      lb_found := c_parametros%FOUND;
      CLOSE c_parametros;
    
      IF NOT lb_found THEN
        lv_error := 'No se encuentra configurado el n�mero de commit para las cuentas castigadas';
        RAISE le_exception;
      END IF;
    
      -- Obtiene las cuentas para ingresa los pagos, creditos y cargos a las cuentas
      FOR j IN c_obtiene_cuentas(lv_ciclo, ln_hilo, to_number(ln_mod)) LOOP
      
        BEGIN
        
          -- Encera las variables
          ln_deuda      := 0;
          lv_estado     := NULL;
          lv_comentario := NULL;
          IF j.proceso = 'P' THEN
            -- Consulta e Ingresa los pagos por cada cuenta en la tabla CO_CARTERA_CLIENTES
            cop_generar_pagos(pv_cuenta   => j.cuenta,
                              pv_fechaini => lv_fecha_ini,
                              pv_fechafin => lv_fecha_fin,
                              pv_ciclo    => j.ciclo,
                              pv_clase    => 1,
                              pv_usuario  => pv_usuario,
                              pv_credit   => pv_credit,
                              pv_formapa  => pv_formapa,
                              pv_error    => lv_error);
          
            IF lv_error IS NOT NULL THEN
              RAISE le_error;
            END IF;
          
            -- Consulta e Ingresa los creditos por cada cuenta en la tabla CO_CARTERA_CLIENTES
            cop_generar_credito(pv_cuenta   => j.cuenta,
                                pv_fechaini => lv_fecha_ini,
                                pv_fechafin => lv_fecha_fin,
                                pv_ciclo    => j.ciclo,
                                pv_clase    => 2,
                                pv_usuario  => pv_usuario,
                                pv_credit   => pv_credit,
                                pv_error    => lv_error);
          
            IF lv_error IS NOT NULL THEN
              RAISE le_error;
            END IF;
          
            -- Consulta e Ingresa los cargos por cada cuenta en la tabla CO_CARTERA_CLIENTES
            cop_generar_cargos(pv_cuenta   => j.cuenta,
                               pv_fechaini => lv_fecha_ini,
                               pv_fechafin => lv_fecha_fin,
                               pv_ciclo    => j.ciclo, -- Para obtener de todos los ciclos
                               pv_clase    => 3, -- Para obtener los cargos de las cuentas
                               pv_usuario  => pv_usuario, -- Para obtener de todos los usuarios
                               pv_credit   => pv_credit,
                               pv_error    => lv_error);
          
            IF lv_error IS NOT NULL THEN
              RAISE le_error;
            END IF;
          
            -- Calcula el saldo de la cuenta
            OPEN c_obtiene_saldo(j.cuenta);
            FETCH c_obtiene_saldo
              INTO lc_saldo;
            CLOSE c_obtiene_saldo;

            -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
            lv_sentencia := ' UPDATE /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/ 
                        CO_CARTERA_CLIENTES G
                        SET G.SALDOS =  :1 WHERE G.CUENTA = :2 AND G.PROCESO = :3 ';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
          
            BEGIN
              EXECUTE IMMEDIATE lv_sentencia
                USING nvl(lc_saldo.saldo, 0), lc_saldo.cuenta,'P';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
            EXCEPTION
              WHEN OTHERS THEN
                lv_error := substr(SQLERRM, 1, 200);
                RAISE le_error;
            END;
          
            -- Calcula el valor total de la cuenta
            OPEN c_obtiene_total(j.cuenta);
            FETCH c_obtiene_total
              INTO lc_total_cuenta;
            CLOSE c_obtiene_total;
          
            -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
            lv_sentencia := ' UPDATE /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/ 
                        CO_CARTERA_CLIENTES G
                        SET G.TOTAL = :1 WHERE G.CUENTA = :2 AND G.PROCESO = :3 ';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
          
            BEGIN
              EXECUTE IMMEDIATE lv_sentencia
                USING nvl(lc_total_cuenta.total, 0), lc_total_cuenta.cuenta,'P';  -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
            EXCEPTION
              WHEN OTHERS THEN
                lv_error := substr(SQLERRM, 1, 200);
                RAISE le_error;
            END;
          
            -- Realiza el cuadre de saldo
            -- Proceso que consulta la deuda de la cuenta
            clp_consulta_deuda(pv_cuenta => j.cuenta,
                               pn_deuda  => ln_deuda,
                               pv_error  => lv_error);
            IF lv_error IS NOT NULL THEN
              RAISE le_error;
            ELSE
			
			        /*ECARFO-12519 EPMV INI*/
              --si la bandera esta activa se redondea a dos decimales
              --ya que el valor ln_deuda traido de clp_consulta_deuda viene formateada a dos decimales.
              lv_parametro_12519 := gvf_obtener_valor_parametro(12519,'BAND_CLK_RECLASIFICACION');
        
              if nvl(lv_parametro_12519,'N') = 'S' then
                 lc_total_cuenta.total := round(lc_total_cuenta.total,2);
              end if;   
              /*ECARFO-12519 EPMV FIN*/		
			  
              -- En caso de descuadre de saldos
              IF nvl(lc_total_cuenta.total, 0) != nvl(ln_deuda, 0) THEN
                lv_estado     := 'IV';
                lv_comentario := lv_aplicacion ||
                                 'INCONSISTENCIA EN BSCS - VALOR: [' ||
                                 ln_deuda || ']. REPORTAR A BILLING.';
                lv_sentencia  := ' UPDATE /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/ 
                          CO_CARTERA_CLIENTES G
                          SET G.TOTAL = :1, G.ESTADO = :2, G.COMENTARIO = :3 || G.COMENTARIO WHERE G.CUENTA = :4 AND G.PROCESO = :5 ';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
                BEGIN
                  EXECUTE IMMEDIATE lv_sentencia
                    USING ln_deuda, lv_estado, lv_comentario, j.cuenta,'P';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
                EXCEPTION
                  WHEN OTHERS THEN
                    lv_error := substr(SQLERRM, 1, 200);
                    RAISE le_error;
                END;
                -- Cuando los saldos cuadran              
              ELSIF nvl(lc_total_cuenta.total, 0) = nvl(ln_deuda, 0) THEN
                lv_estado     := 'V';
                lv_comentario := lv_aplicacion ||
                                 'VALORES CARGADOS EXITOSAMENTE.';
              
                lv_sentencia := ' UPDATE /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/ 
                          CO_CARTERA_CLIENTES G
                          SET G.ESTADO = :1, G.COMENTARIO = :2 || G.COMENTARIO WHERE G.CUENTA = :3 AND G.PROCESO = :4 ';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
                BEGIN
                  EXECUTE IMMEDIATE lv_sentencia
                    USING lv_estado, lv_comentario, j.cuenta,'P';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
                EXCEPTION
                  WHEN OTHERS THEN
                    lv_error := substr(SQLERRM, 1, 200);
                    RAISE le_error;
                END;
              END IF;
            END IF;
          ELSIF j.proceso = 'R' THEN
            lv_comentario := lv_aplicacion ||
                             'VALORES CARGADOS EXITOSAMENTE.';
            UPDATE co_cartera_clientes f
               SET f.total      = f.totaladeuda,
                   f.estado     = 'V',
                   f.comentario = lv_comentario
             WHERE f.cuenta = j.cuenta
             AND   f.proceso= 'R';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
          END IF;
        
          IF ln_contador > to_number(lv_commit) THEN
            COMMIT;
            ln_contador := 0;
          END IF;
          ln_contador := ln_contador + 1;
        
        EXCEPTION
          WHEN le_error THEN
            COMMIT;    -- [11603] 21/11/2017 IRO_GL
            lv_error := lv_aplicacion || lv_error;
            -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
            -- la observacion respectiva       
            UPDATE co_cartera_clientes
               SET comentario = lv_error, estado = 'E'
             WHERE cuenta = j.cuenta;
              COMMIT;--[11334] IRO_HRO 18/05/2017
              ln_contador := 0;--[11334] IRO_HRO 18/05/2017             
          WHEN OTHERS THEN
            COMMIT;    -- [11603] 21/11/2017 IRO_GL
            lv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
            -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
            -- la observacion respectiva       
            UPDATE co_cartera_clientes
               SET comentario = lv_error, estado = 'E'
             WHERE cuenta = j.cuenta;
              COMMIT;--[11334] IRO_HRO 18/05/2017
              ln_contador := 0;--[11334] IRO_HRO 18/05/2017 
        END;
      END LOOP;
      COMMIT;
    END LOOP;
  
  EXCEPTION
    WHEN le_exception THEN
      ROLLBACK;
      pv_error := substr(lv_aplicacion || lv_error,1,2000);
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END gcp_genera_journal;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez 
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que consulta e ingresa los pagos por cada cuenta en la tabla 
  --           CO_CARTERA_CLIENTES
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE cop_generar_pagos(pv_cuenta   IN VARCHAR2,
                              pv_fechaini IN VARCHAR2,
                              pv_fechafin IN VARCHAR2 DEFAULT NULL,
                              pv_ciclo    IN VARCHAR2,
                              pv_clase    IN VARCHAR2 DEFAULT NULL,
                              pv_usuario  IN VARCHAR2 DEFAULT NULL,
                              pv_credit   IN VARCHAR2 DEFAULT NULL,
                              pv_formapa  IN VARCHAR2 DEFAULT NULL,
                              pv_error    OUT VARCHAR2) IS
  
    lv_error VARCHAR2(1000);
    le_exception EXCEPTION;
    ln_pago       NUMBER;
    lv_sentencia  VARCHAR2(3650);
    lv_aplicacion VARCHAR2(150) := 'COP_GENERAR_PAGOS - ';
  
    -- Cursor que suma los cargos de las cuentas
    CURSOR cr_suma_pagos(cv_cuenta   VARCHAR2,
                         cv_ciclo    VARCHAR2,
                         cv_fechaini VARCHAR2,
                         cv_fechafin VARCHAR2,
                         cv_clase    VARCHAR2,
                         cv_usuario  VARCHAR2,
                         cv_formapa  VARCHAR2,
                         cv_credit   VARCHAR2) IS
      SELECT /*+RULE+*/
            SUM(cashreceipts_all.cacuramt_pay) valor_pago
        FROM customer_all,
             cashreceipts_all,
             ccontact_all,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       WHERE customer_all.custcode = cv_cuenta
         AND cashreceipts_all.customer_id = customer_all.customer_id
         AND customer_all.customer_id = ccontact_all.customer_id(+)
         AND customer_all.billcycle = ma.id_ciclo_admin
         AND ci.id_ciclo = ma.id_ciclo
         AND (ci.id_ciclo = cv_ciclo OR
             to_number(ci.id_ciclo) > decode(cv_ciclo, 'TODOS', 0))
         AND ccontact_all.ccbill = 'X'
         AND ccontact_all.ccseq <> 0
         AND cashreceipts_all.catype IN (1, 3, 9)
         AND caentdate >= to_date(cv_fechaini, 'DD/MM/YYYY HH24:MI:SS')
         AND cachkdate BETWEEN
             to_date(ci.dia_ini_ciclo || '/' ||
                     to_char(to_date(cv_fechaini, 'DD/MM/YYYY HH24:MI:SS'),
                             'MM/YYYY'),
                     'DD/MM/YYYY HH24:MI:SS') AND
             to_date(cv_fechafin, 'DD/MM/YYYY HH24:MI:SS')
         AND paymntresp = 'X'
         AND to_number(substr(cv_clase, 1, 1)) IN (0, 1)
         AND (ltrim(rtrim(cv_usuario)) = causername OR cv_usuario = 'TODOS')
         AND (ltrim(rtrim(cv_formapa)) = cabanksubacc OR
             pv_formapa = 'TODOS')
         AND (ltrim(rtrim(cv_credit)) = customer_all.cstradecode OR
             pv_credit = 'TODOS');
  
  BEGIN
  
    -- Obtiene la suma de los cargos de  las cuentas
    OPEN cr_suma_pagos(pv_cuenta,
                       pv_ciclo,
                       pv_fechaini,
                       pv_fechafin,
                       pv_clase,
                       pv_usuario,
                       pv_formapa,
                       pv_credit);
    FETCH cr_suma_pagos
      INTO ln_pago;
    CLOSE cr_suma_pagos;
  
    -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
    lv_sentencia := ' UPDATE CO_CARTERA_CLIENTES
                      SET PAGOS = :1 ' ||
                    ' WHERE CUENTA = :2 AND PROCESO = :3 ';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        USING nvl(ln_pago, 0), pv_cuenta,'P';    -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        RAISE le_exception;
    END;
  
  EXCEPTION
    WHEN le_exception THEN
      pv_error := substr(lv_aplicacion || lv_error,1,1000);
    WHEN OTHERS THEN
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END cop_generar_pagos;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que consulta e ingresa los creditos por cada cuenta en la tabla 
  --           CO_CARTERA_CLIENTES
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE cop_generar_credito(pv_cuenta   IN VARCHAR2,
                                pv_fechaini IN VARCHAR2,
                                pv_fechafin IN VARCHAR2,
                                pv_ciclo    IN VARCHAR2,
                                pv_clase    IN VARCHAR2,
                                pv_usuario  IN VARCHAR2,
                                pv_credit   IN VARCHAR2,
                                pv_error    OUT VARCHAR2) IS
  
    lv_error VARCHAR2(1000);
    le_exception EXCEPTION;
    ln_pago       NUMBER;
    lv_sentencia  VARCHAR2(3650);
    lv_aplicacion VARCHAR2(150) := 'COP_GENERAR_CREDITO - ';
  
    -- Cursor que suma los creditos de las cuentas
    CURSOR cr_suma_credito(cv_cuenta   VARCHAR2,
                           cv_ciclo    VARCHAR2,
                           cv_fechaini VARCHAR2,
                           cv_fechafin VARCHAR2,
                           cv_clase    VARCHAR2,
                           cv_usuario  VARCHAR2,
                           cv_credit   VARCHAR2) IS
      SELECT SUM(valor_bd) valor_bd
        FROM (SELECT /*+RULE+*/ 
               SUM(orderhdr_all.ohinvamt_doc) valor_bd
                FROM customer_all        cu,
                     orderhdr_all,
                     ccontact_all        c,
                     fa_ciclos_bscs      ci,
                     fa_ciclos_axis_bscs ma
               WHERE cu.custcode = cv_cuenta
                 AND cu.customer_id = orderhdr_all.customer_id(+)
                 AND c.ccbill = 'X'
                 AND orderhdr_all.ohxact > 0
                 AND ohentdate BETWEEN
                     to_date(ci.dia_ini_ciclo || '/' ||
                             to_char(to_date(cv_fechaini,
                                             'DD/MM/YYYY HH24:MI:SS'),
                                     'MM/YYYY'),
                             'dd/mm/yyyy hh24:mi:ss') AND
                     to_date(cv_fechafin, 'dd/mm/yyyy hh24:mi:ss')
                 AND ohstatus = 'CM'
                 AND ohinvtype <> 5
                 AND to_char(ohentdate, 'dd') <> ci.dia_ini_ciclo
                 AND c.customer_id = cu.customer_id
                 AND c.ccseq <> 0
                 AND cu.billcycle = ma.id_ciclo_admin
                 AND ci.id_ciclo = ma.id_ciclo
                 AND (ci.id_ciclo = cv_ciclo OR
                     to_number(ci.id_ciclo) > decode(cv_ciclo, 'TODOS', 0))
                 AND paymntresp = 'X'
                 AND to_number(substr(cv_clase, 1, 1)) IN (0, 2)
                 AND (cv_usuario = ohuserid OR cv_usuario = 'TODOS')
                 AND (cv_credit = cu.cstradecode OR cv_credit = 'TODOS')
              UNION ALL
              SELECT /*+RULE+*/ 
                SUM(f.amount) valor_bd
                FROM customer_all        cu,
                     fees                f,
                     mpusntab            m,
                     ccontact_all        c,
                     fa_ciclos_bscs      ci,
                     fa_ciclos_axis_bscs ma
               WHERE cu.custcode = cv_cuenta
                 AND cu.customer_id = f.customer_id(+)
                 AND cu.customer_id > 0
                 AND f.entdate BETWEEN
                     to_date(ci.dia_ini_ciclo || '/' ||
                             to_char(to_date(cv_fechaini,
                                             'DD/MM/YYYY HH24:MI:SS'),
                                     'MM/YYYY'),
                             'dd/mm/yyyy hh24:mi:ss') AND
                     to_date(cv_fechafin, 'dd/mm/yyyy hh24:mi:ss')
                 AND c.customer_id = cu.customer_id
                 AND c.ccseq <> 0
                 AND cu.billcycle = ma.id_ciclo_admin
                 AND ci.id_ciclo = ma.id_ciclo
                 AND (ci.id_ciclo = pv_ciclo OR
                     to_number(ci.id_ciclo) > decode(cv_ciclo, 'TODOS', 0))
                 AND c.ccbill = 'X'
                 AND f.sncode = m.sncode
                 AND m.sncode > 0
                 AND paymntresp = 'X'
                 AND to_number(substr(cv_clase, 1, 1)) IN (0, 2)
                 AND (cv_usuario = f.username OR cv_usuario = 'TODOS')
                 AND (cv_credit = cu.cstradecode OR cv_credit = 'TODOS')
                 AND nvl(f.amount, 0) < 0
              UNION ALL
              SELECT /*+RULE+*/ 
                SUM(f.amount) valor_bd
                FROM customer_all        cu,
                     fees                f,
                     mpusntab            m,
                     ccontact_all        c,
                     fa_ciclos_bscs      ci,
                     fa_ciclos_axis_bscs ma
               WHERE cu.custcode = cv_cuenta
                 AND cu.customer_id = f.customer_id(+)
                 AND cu.customer_id > 0
                 AND f.entdate BETWEEN
                     to_date(ci.dia_ini_ciclo || '/' ||
                             to_char(to_date(pv_fechaini,
                                             'DD/MM/YYYY HH24:MI:SS'),
                                     'MM/YYYY'),
                             'dd/mm/yyyy hh24:mi:ss') AND
                     to_date(pv_fechafin, 'dd/mm/yyyy hh24:mi:ss')
                 AND c.customer_id = cu.customer_id
                 AND c.ccseq <> 0
                 AND cu.billcycle = ma.id_ciclo_admin
                 AND ci.id_ciclo = ma.id_ciclo
                 AND (ci.id_ciclo = pv_ciclo OR
                     to_number(ci.id_ciclo) > decode(pv_ciclo, 'TODOS', 0))
                 AND c.ccbill = 'X'
                 AND f.sncode = m.sncode
                 AND m.sncode > 0
                 AND paymntresp IS NULL
                 AND to_number(substr(pv_clase, 1, 1)) IN (0, 2)
                 AND (pv_usuario = f.username OR pv_usuario = 'TODOS')
                 AND (pv_credit = cu.cstradecode OR pv_credit = 'TODOS')
                 AND nvl(f.amount, 0) < 0) f;
  
  BEGIN
  
    -- Obtiene la suma de los creditos de  las cuentas
    OPEN cr_suma_credito(pv_cuenta,
                         pv_ciclo,
                         pv_fechaini,
                         pv_fechafin,
                         pv_clase,
                         pv_usuario,
                         pv_credit);
    FETCH cr_suma_credito
      INTO ln_pago;
    CLOSE cr_suma_credito;
  
    -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
    lv_sentencia := ' UPDATE CO_CARTERA_CLIENTES
                    SET CREDITO =  :1' ||
                    ' WHERE CUENTA = :2 AND PROCESO = :3 ';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        USING nvl(ln_pago, 0), pv_cuenta,'P';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        RAISE le_exception;
    END;
  
  EXCEPTION
    WHEN le_exception THEN
      pv_error := substr(lv_aplicacion || lv_error,1,1000);
    WHEN OTHERS THEN
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END cop_generar_credito;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que consulta e ingresa los cargos por cada cuenta en la tabla 
  --           CO_CARTERA_CLIENTES
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE cop_generar_cargos(pv_cuenta   IN VARCHAR2,
                               pv_fechaini IN VARCHAR2,
                               pv_fechafin IN VARCHAR2,
                               pv_ciclo    IN VARCHAR2,
                               pv_clase    IN VARCHAR2,
                               pv_usuario  IN VARCHAR2,
                               pv_credit   IN VARCHAR2,
                               pv_error    OUT VARCHAR2) IS
  
    lv_error VARCHAR2(1000);
    le_exception EXCEPTION;
    ln_pago       NUMBER;
    lv_sentencia  VARCHAR2(3650);
    lv_aplicacion VARCHAR2(150) := 'COP_GENERAR_CREDITO - ';
  
    -- Cursor que suma los cargos de las cuentas
    CURSOR cr_suma_cargos(cv_cuenta   VARCHAR2,
                          cv_ciclo    VARCHAR2,
                          cv_fechaini VARCHAR2,
                          cv_fechafin VARCHAR2,
                          cv_clase    VARCHAR2,
                          cv_usuario  VARCHAR2,
                          cv_credit   VARCHAR2) IS
      SELECT SUM(monto) monto
        FROM (SELECT /*+RULE+*/ 
                SUM(f.amount) monto
                FROM customer_all        cu,
                     fees                f,
                     mpusntab            m,
                     ccontact_all        c,
                     fa_ciclos_bscs      ci,
                     fa_ciclos_axis_bscs ma
               WHERE cu.custcode = cv_cuenta
                 AND cu.customer_id = f.customer_id
                 AND f.entdate BETWEEN
                     to_date(ci.dia_ini_ciclo || '/' ||
                             to_char(to_date(cv_fechaini,
                                             'DD/MM/YYYY HH24:MI:SS'),
                                     'MM/YYYY'),
                             'dd/mm/yyyy hh24:mi:ss') AND
                     to_date(cv_fechafin, 'dd/mm/yyyy hh24:mi:ss')
                 AND c.customer_id = cu.customer_id
                 AND c.ccseq <> 0
                 AND f.sncode = m.sncode
                 AND m.sncode > 0
                 AND cu.billcycle = ma.id_ciclo_admin
                 AND ci.id_ciclo = ma.id_ciclo
                 AND (ci.id_ciclo = cv_ciclo OR
                     to_number(ci.id_ciclo) > decode(cv_ciclo, 'TODOS', 0))
                 AND c.ccbill = 'X'
                 AND paymntresp = 'X'
                 AND to_number(substr(cv_clase, 1, 1)) IN (0, 3)
                 AND (ltrim(rtrim(cv_usuario)) = f.username OR
                     cv_usuario = 'TODOS')
                 AND (ltrim(rtrim(cv_credit)) = cu.cstradecode OR
                     cv_credit = 'TODOS')
                 AND nvl(f.amount, 0) > 0
                 AND f.sncode <> 103
              UNION ALL
              SELECT /*+RULE+*/
                SUM(f.amount) monto
                FROM customer_all        cu,
                     fees                f,
                     mpusntab            m,
                     ccontact_all        c,
                     fa_ciclos_bscs      ci,
                     fa_ciclos_axis_bscs ma
               WHERE cu.custcode = cv_cuenta
                 AND cu.customer_id = f.customer_id
                 AND cu.customer_id > 0
                 AND f.entdate BETWEEN
                     to_date(ci.dia_ini_ciclo || '/' ||
                             to_char(to_date(cv_fechaini,
                                             'DD/MM/YYYY HH24:MI:SS'),
                                     'MM/YYYY'),
                             'dd/mm/yyyy hh24:mi:ss') AND
                     to_date(cv_fechafin, 'dd/mm/yyyy hh24:mi:ss')
                 AND c.customer_id = cu.customer_id
                 AND c.ccseq <> 0
                 AND f.sncode = m.sncode
                 AND m.sncode > 0
                 AND cu.billcycle = ma.id_ciclo_admin
                 AND ci.id_ciclo = ma.id_ciclo
                 AND (ci.id_ciclo = cv_ciclo OR
                     to_number(ci.id_ciclo) > decode(cv_ciclo, 'TODOS', 0))
                 AND c.ccbill = 'X'
                 AND paymntresp IS NULL
                 AND nvl(customer_id_high, '-1') <> '-1'
                 AND to_number(substr(cv_clase, 1, 1)) IN (0, 3)
                 AND (ltrim(rtrim(cv_usuario)) = f.username OR
                     cv_usuario = 'TODOS')
                 AND (ltrim(rtrim(cv_credit)) = cu.cstradecode OR
                     cv_credit = 'TODOS')
                 AND nvl(f.amount, 0) > 0
                 AND f.sncode <> 103) f;
  
  BEGIN
  
    -- Obtiene la suma de los creditos de  las cuentas
    OPEN cr_suma_cargos(pv_cuenta,
                        pv_ciclo,
                        pv_fechaini,
                        pv_fechafin,
                        pv_clase,
                        pv_usuario,
                        pv_credit);
    FETCH cr_suma_cargos
      INTO ln_pago;
    CLOSE cr_suma_cargos;
  
    -- Actualiza la tabla de trabajo CO_CARTERA_CLIENTES con la informacion obtenida
    lv_sentencia := ' UPDATE CO_CARTERA_CLIENTES
                    SET CARGO =  :1' ||
                    ' WHERE CUENTA = :2 AND PROCESO = :3 ';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        USING nvl(ln_pago, 0), pv_cuenta,'P';   -- [11603] 21/11/2017 IRO_GL, se adiciona campo proceso
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        RAISE le_exception;
    END;
  
  EXCEPTION
    WHEN le_exception THEN
      pv_error := substr(lv_aplicacion || lv_error,1,1000);
    WHEN OTHERS THEN
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END cop_generar_cargos;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo.
  -- Created : 03/07/2013 
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Proceso que realiza el respaldo de la tabla de trabajo CO_CARTERA_CLIENTES
  --           con su respectiva fecha de ejecucion
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modified: 22/05/2014
  -- Author	 : SUD Margarita Trujillo.
  -- Purpose : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
  --===============================================================================================--

  PROCEDURE gcp_respaldo_tabla(pv_fecha IN VARCHAR2, pv_error OUT VARCHAR2) IS
  
    lvsentencia VARCHAR2(3600);
    ln_count    NUMBER := 0;
    lv_mes      VARCHAR2(10);
    lv_anio      VARCHAR2(10);
    ln_control  NUMBER := 0;
    le_error EXCEPTION;
    lv_error      VARCHAR2(4000);
    lv_aplicacion VARCHAR2(150) := 'GCP_RESPALDO_TABLA - ';
  
  BEGIN
  
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  
    -- Extrae el mes de la fecha ingresada
    SELECT to_char(add_months(to_date(pv_fecha, 'DD/MM/YYYY'), -1), 'MM')
      INTO lv_mes
      FROM dual;
  
    -- Extrae el anio de la fecha ingresada
    SELECT to_char(add_months(to_date(pv_fecha, 'DD/MM/YYYY'), -1), 'RRRR')
      INTO lv_anio
      FROM dual;
  
    -- Cuenta los registros que se encuentran en la tabla CO_CARTERA_CLIENTES
    SELECT COUNT(*) INTO ln_count FROM co_cartera_clientes;
  
    -- Verifica si la tabla que se va a crear como respaldo no existe
    SELECT COUNT(*)
      INTO ln_control
      FROM all_tables
     WHERE table_name LIKE 'CO_CART_CLIENTES_' || lv_mes || lv_anio;
  
    -- Si hay registros y la tabla de respaldo no existe
    IF ln_count > 0 AND ln_control = 0 THEN
      lvsentencia := 'CREATE TABLE CO_CART_CLIENTES_' || lv_mes || lv_anio ||
                     ' AS SELECT * FROM CO_CARTERA_CLIENTES';
      BEGIN
        EXECUTE IMMEDIATE lvsentencia;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := substr(SQLERRM, 1, 200);
          IF instr(lv_error, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE le_error;
          END IF;
      END;
      -- Trunca la tabla de trabajo para una nueva ejecucion
      lvsentencia := 'TRUNCATE TABLE CO_CARTERA_CLIENTES';
      BEGIN
        EXECUTE IMMEDIATE lvsentencia;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := substr(SQLERRM, 1, 200);
          IF instr(lv_error, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE le_error;
          END IF;
      END;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_aplicacion || lv_error;
    WHEN OTHERS THEN
      NULL;
      pv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
  END gcp_respaldo_tabla;

  /******************************************************************************
   Proyecto   : [7399] Reclasificacion Cartera                       
   Lider Claro: SIS Maria Elizabeth Estevez
   Creado Por : SUD 
   Fecha      : 31/05/2013 
   Descripcion: Procedimiento que genera el preasiento en peoplesoft
   Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
   Modified   : 22/05/2014
   Author	  : SUD Margarita Trujillo.
   Purpose    : Optimizacion general del porceso de Extraccion de cuentas por Vencer.
   Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
   Modified   : 22/07/2014
   Author	  : SUD Margarita Trujillo.
   Purpose    : Modificacion en unidad operativa.
  *******************************************************************************/
  
  /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      JUAN ROMERO
    CREADO POR:     IRO HUGO ROMERO  IRO_HRO 
    PROYECTO:       [11188] Equipo Cambio Continuo Reloj de Cobranzas y Reactivaciones - Reclasificacion de Cartera
    FECHA:          17/03/2017
    PROP�SITO:      Se invoca al procedimiento GENERA_PREASIENTO_SAP_DTH el cual ser� el encargado de 
                    crear los asientos contables de reclasificacion - castigo para enviar a SAP de las
                    cuentas DTH.
    SIGLAS DE BUSQ: IRO_HRO                    
  **********************************************************************************************************/
  PROCEDURE genera_preasiento_rec(PV_FECHACTUAL   IN VARCHAR2,
                                  PV_FECHANTERIOR IN VARCHAR2,
                                  PD_FECHA        IN DATE,
                                  PV_ERROR        OUT VARCHAR2) IS
  
    CURSOR C_COMPANIAS IS
    /*SELECT DISTINCT DECODE(N.CIA, 1, 2, 2, 1) COMPANIA
                                        FROM CO_CARTERA_CLIENTES N
                                       WHERE N.ESTADO_CARTERA IS NOT NULL --= 'R'
                                         AND N.ESTADO IN ('D', 'V') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
                                         AND N.PROCESO IN ('P', 'R')
                                         AND N.NO_FACTURA IS NOT NULL;*/ --= 'P';
      SELECT DISTINCT B.COMPANIA, B.AJUSTE
        FROM (SELECT DISTINCT DECODE(N.CIA, 1, 2, 2, 1) COMPANIA,
                              N.NO_FACTURA AJUSTE
                FROM CO_CARTERA_CLIENTES N
               WHERE N.ESTADO_CARTERA IS NOT NULL --= 'R'
                 AND N.ESTADO IN ('D') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
                 AND N.PROCESO IN ('P')
                 AND N.NO_FACTURA IS NOT NULL
              UNION
              SELECT DISTINCT TO_NUMBER(N.CIA) COMPANIA, N.NO_FACTURA AJUSTE
                FROM CO_CARTERA_CLIENTES N
               WHERE N.ESTADO_CARTERA IS NOT NULL --= 'C'
                 AND N.ESTADO IN ('V') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
                 AND N.PROCESO IN ('R')
                 AND N.NO_FACTURA IS NOT NULL) B;
  
    CURSOR C_OBTIENE_TOTAL(CN_COMPANIA NUMBER) IS
      SELECT SUM(TOTAL) VALOR
        FROM CO_CARTERA_CLIENTES
       WHERE CIA = CN_COMPANIA
         AND ESTADO_CARTERA = 'R'
         AND ESTADO IN ('D') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
         AND PROCESO = 'P';
  
    CURSOR C_OBTIENE_TOTAL2(CN_COMPANIA NUMBER, CN_AJUSTE NUMBER) IS
      SELECT SUM(TOTAL) VALOR
        FROM CO_CARTERA_CLIENTES
       WHERE CIA = CN_COMPANIA
         AND ESTADO_CARTERA = 'C'
         AND ESTADO = 'V' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
         AND PROCESO = 'R'
         AND NO_FACTURA = CN_AJUSTE;
  
    LV_GLOSA    VARCHAR2(200);
    LV_DOC_TYPE VARCHAR2(200);
    LE_CONTROL_ERROR EXCEPTION;
    LE_ERROR         EXCEPTION;
    LV_TRANSID   VARCHAR2(10) := NULL;
    LN_SEC       NUMBER;
    LN_TRANSLINE NUMBER := 0;
    --
    LV_REFERENCIA VARCHAR2(200);
    LN_VALOR      NUMBER;
    --
    LV_PROCESO VARCHAR2(15) := '[%PROC%]';
    LV_REPLACE VARCHAR2(15);
    LV_JRNL    VARCHAR2(20);
    LV_SEC     VARCHAR2(10);
    -- RECLASIFICACION
    LV_CTA_DEBE  VARCHAR2(200);
    LV_CTA_HABER VARCHAR2(200); --
    -- CASTIGO
    LV_CTA_DEBE1  VARCHAR2(200);
    LV_CTA_HABER1 VARCHAR2(200);
    LV_CTA_DEBE2  VARCHAR2(200);
    LV_CTA_HABER2 VARCHAR2(200); --
    LV_UNIDAD     VARCHAR2(8);
    LN_TOTAL      NUMBER;
    LN_CIA        NUMBER;
    LV_BAN_PS     VARCHAR2(10):='N';
    LV_BAN_SAP    VARCHAR2(10):='N';  
    LV_BAN_UP_PS     VARCHAR2(10):='N';     
    LV_BAN_UP_SAP_PS    VARCHAR2(10):='N';
    
    --[11188] IRO_HRO 09/03/2017
    LV_BAN_SAP_DTH VARCHAR2(10):='N';
  
    LV_ERROR VARCHAR2(4000);
  BEGIN
    
    --10172 GCA para activar env�o de asientos a PS o SAP
    
    LV_BAN_PS:=NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'BAND_PS'),'N');
                                                                   
    LV_BAN_SAP:= NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'BAND_SAP'),'N');
                                                                   
    LV_BAN_UP_PS:=NVL(GVF_OBTENER_VALOR_PARAMETRO(10172, 'BAND_UPD_PS'),'N');        
    
    LV_BAN_UP_SAP_PS:=NVL(GVF_OBTENER_VALOR_PARAMETRO(10172, 'BAND_UPD_SAP_PS'),'N');  
    
    --[11188] IRO_HRO 09/03/2017
    LV_BAN_SAP_DTH:= NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(11188,'BAND_SAP_DTH'),'N'); 
    
   --10172 GCA para activar env�o de asientos a PS                                                           
   IF LV_BAN_PS = 'S' THEN
    -- GLOSE DEL ASIENTO CONTABLE 
    LV_GLOSA := GVF_OBTENER_VALOR_PARAMETRO(7399, 'GLOSA');
    IF LV_GLOSA IS NULL THEN
      PV_ERROR := 'GLOSA NO CONFIGURADA.. ';
      RAISE LE_ERROR;
    END IF;
  
    --CUENTA DEUDORA -- Reclasificacion
    LV_CTA_DEBE := GVF_OBTENER_VALOR_PARAMETRO(7399, 'CONSUMO_V_COBRAR');
    IF LV_CTA_DEBE IS NULL THEN
      PV_ERROR := 'CUENTA DEUDORA NO CONFIGURADA ';
      RAISE LE_ERROR;
    END IF;
  
    --CUENTA DEUDORA -- Castigo
    LV_CTA_DEBE2 := GVF_OBTENER_VALOR_PARAMETRO(7399, 'REC_GSTN_COBRO');
    IF LV_CTA_DEBE2 IS NULL THEN
      PV_ERROR := 'CUENTA DE RECUPERACION DE COBRO NO CONFIGURADA ';
      RAISE LE_ERROR;
    END IF;
  
    --CUENTA ACREEDORA -- Castigo
    LV_CTA_HABER1 := GVF_OBTENER_VALOR_PARAMETRO(7399, 'CONSUMO_V_COBRAR');
    IF LV_CTA_HABER1 IS NULL THEN
      PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
      RAISE LE_ERROR;
    END IF;
  
    --CUENTA ACREEDORA -- Castigo
    LV_CTA_HABER2 := GVF_OBTENER_VALOR_PARAMETRO(7399, 'ACRD_CONTRA');
    IF LV_CTA_HABER2 IS NULL THEN
      PV_ERROR := 'CUENTA ACREEDORA POR CONTRA NO CONFIGURADA ';
      RAISE LE_ERROR;
    END IF;
  
    --ASIENTO POR COMPANIAS
    FOR I IN C_COMPANIAS LOOP
      BEGIN
        LV_PROCESO := '[%PROC%]';
        SELECT NVL(MAX((TO_NUMBER(SUBSTR(TRANSACTION_ID,-- 7399 - AJUSTE || INI SUD MTR
                                         2,
                                         LENGTH(TRANSACTION_ID) - 1)))),-- 7399 FIN SUD MTR
                   0) + 1
          INTO LN_SEC
          FROM PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
        --ps_jgen_acct_entry@finsys -- DESCOMENTAR PARA PRODUCCION
         WHERE BUSINESS_UNIT = 'CONEC'
           AND LEDGER_GROUP = 'REAL'
           AND LEDGER = 'LOCAL'
           AND TRANSACTION_ID LIKE 'C%' --'GL%'
           AND DOC_TYPE LIKE 'CMCRV%'
           AND APPL_JRNL_ID LIKE 'MOD_CART%';
      
        LN_TRANSLINE := 0; -- inicializo lineas de transaccion
      
        IF I.COMPANIA IN (2) THEN
          -- En BSCS, GYE
          --CUENTA ACREEDORA -- Reclasificacion
          LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(7399,
                                                      'GYE_CONSUMO_CEL');
          IF LV_CTA_HABER IS NULL THEN
            PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
          --CUENTA DEUDORA -- Castigo
          LV_CTA_DEBE1 := GVF_OBTENER_VALOR_PARAMETRO(7399, 'GYE_PROVISION');
          IF LV_CTA_DEBE1 IS NULL THEN
            PV_ERROR := 'CUENTA DE PROVISION NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
          -- DEFINO UNIDAD DE EXPLOTACION
          LV_UNIDAD := 'GYE_999'; -- [7399] SUD MTR 21072014
          -- CODIGO DE COMPANIA EN BSCS
          LN_CIA := 1;
        ELSIF I.COMPANIA IN (1) THEN
          -- En BSCS, UIO
          --CUENTA ACREEDORA -- Reclasificacion
          LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(7399,
                                                      'UIO_CONSUMO_CEL');
          IF LV_CTA_HABER IS NULL THEN
            PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
          --CUENTA DEUDORA -- Castigo
          LV_CTA_DEBE1 := GVF_OBTENER_VALOR_PARAMETRO(7399, 'UIO_PROVISION');
          IF LV_CTA_DEBE1 IS NULL THEN
            PV_ERROR := 'CUENTA DE PROVISION NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
          -- DEFINO UNIDAD DE EXPLOTACION
          LV_UNIDAD := 'UIO_999'; -- [7399] SUD MTR 21072014
          -- CODIGO DE COMPANIA EN BSCS
          LN_CIA := 2;
        END IF;
        -- DEFINO MODULO Y PLANTILLA PARA PRIMER AJUSTE
        SELECT DECODE(I.COMPANIA, 2, 'CMCRV2', 1, 'CMCRV1')
          INTO LV_DOC_TYPE
          FROM DUAL;
      
        SELECT DECODE(TO_NUMBER(I.COMPANIA), 1, 'MOD_CART1', 2, 'MOD_CART2')
          INTO LV_JRNL
          FROM DUAL;
        -- DEFINO LA LONGITUD DE LA SECUENCIA
        SELECT TO_CHAR(LN_SEC, '000000009') INTO LV_SEC FROM DUAL;
        -- SE MARCA EL PERIODO DE AJUSTE
        LV_GLOSA := REPLACE(REPLACE(REPLACE(LV_GLOSA,
                                            '[%%FECHANTERIOR%%]',
                                            PV_FECHANTERIOR),
                                    '[%%FECHACTUAL%%]',
                                    PV_FECHACTUAL),
                            '[%FACT%]',
                            I.AJUSTE);
        -- DEFINE CODIGO DE TRANSACCION
        LV_TRANSID := 'C' || TRIM(LV_SEC);
        -- SUMARIZA LA CANTIDAD A RECLASIFICAR
        OPEN C_OBTIENE_TOTAL(LN_CIA);
        FETCH C_OBTIENE_TOTAL
          INTO LN_TOTAL;
        IF C_OBTIENE_TOTAL%NOTFOUND THEN
          CLOSE C_OBTIENE_TOTAL;
        ELSIF C_OBTIENE_TOTAL%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
          LN_TRANSLINE := LN_TRANSLINE + 1;
          -- Defino el proceso por el que se hara el primer ajuste
          LV_REPLACE    := 'RECLASIFICACION';
          LV_REFERENCIA := REPLACE(LV_GLOSA, LV_PROCESO, LV_REPLACE);
          -- PRIMER AJUSTE -- DEBE
          begin
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               LN_TRANSLINE,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               LN_TRANSLINE,
               LV_CTA_DEBE,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               LN_TOTAL,
               LN_TOTAL,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE),
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 1, AL INSERTAR EL PREASIENTO EN LA TABLA PS_JGEN_ACCT_ENTRY ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
        
          --FOR J IN C_OBTIENE_CUENTAS(LN_CIA) LOOP
          LN_TRANSLINE := LN_TRANSLINE + 1; -- LA LINEA DEL HABER.
          LN_VALOR     := LN_TOTAL * -1;
          -- PRIMER AJUSTE -- HABER
          Begin
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_transline,
               LV_CTA_HABER,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               ln_valor,
               ln_valor,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE), --J.NO_FACTURA, -- AJUSTE
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 2, AL INSERTAR EL PREASIENTO ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
          LV_PROCESO := LV_REPLACE;
        END IF; -- C_OBTIENE_TOTAL%FOUND
        CLOSE C_OBTIENE_TOTAL;
        -- Defino mOdulo y plantilla para segundo ajuste
        SELECT DECODE(I.COMPANIA, 1, 'CCASC1', 2, 'CCASC2')
          INTO LV_DOC_TYPE
          FROM DUAL;
      
        SELECT DECODE(TO_NUMBER(I.COMPANIA), 2, 'MOD_CART2', 1, 'MOD_CART1')
          INTO LV_JRNL
          FROM DUAL;
        -- SUMARIZA LA CANTIDAD A CASTIGAR
        OPEN C_OBTIENE_TOTAL2(I.COMPANIA, I.AJUSTE);
        FETCH C_OBTIENE_TOTAL2
          INTO LN_TOTAL;
        IF C_OBTIENE_TOTAL2%NOTFOUND THEN
          CLOSE C_OBTIENE_TOTAL2;
        ELSIF C_OBTIENE_TOTAL2%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
          -- Defino el proceso por el que se hara el segundo ajuste
          LV_REPLACE    := 'CASTIGO';
          LV_REFERENCIA := REPLACE(NVL(LV_REFERENCIA, LV_GLOSA),
                                   LV_PROCESO,
                                   LV_REPLACE);
          --
          LN_TRANSLINE := LN_TRANSLINE + 1;
          -- Convierte a negativo el valor obtenido
          LN_VALOR := LN_TOTAL * -1;
          -- SEGUNDO AJUSTE -- DEBE
          BEGIN
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               LN_TRANSLINE,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               LN_TRANSLINE,
               LV_CTA_DEBE1,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               LN_TOTAL,
               LN_TOTAL,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE),
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 3, AL INSERTAR EL PREASIENTO EN LA TABLA PS_JGEN_ACCT_ENTRY ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
          --
          LN_TRANSLINE := LN_TRANSLINE + 1;
          -- SEGUNDO AJUSTE -- HABER
          BEGIN
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_transline,
               LV_CTA_HABER1,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               LN_VALOR,
               LN_VALOR,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE), --J.NO_FACTURA,
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 4, AL INSERTAR EL PREASIENTO ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
          LN_TRANSLINE := LN_TRANSLINE + 1;
          -- TERCER AJUSTE -- DEBE
          BEGIN
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               LN_TRANSLINE,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               LN_TRANSLINE,
               LV_CTA_DEBE2,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               LN_TOTAL, --J.TOTAL,
               LN_TOTAL, --J.TOTAL,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE),
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 5, AL INSERTAR EL PREASIENTO EN LA TABLA PS_JGEN_ACCT_ENTRY ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
          --
          LN_TRANSLINE := LN_TRANSLINE + 1;
          -- TERCER AJUSTE -- HABER
          BEGIN
            insert into --ps_jgen_acct_entry@finsys
            PS_JGEN_ACCT_ENTRY_CART -- COMENTAR PARA PRODUCCION
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            values
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               LV_JRNL,
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_transline,
               LV_CTA_HABER2,
               LV_UNIDAD,
               ' ',
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               ln_valor,
               ln_valor,
               LV_DOC_TYPE,
               pd_fecha,
               lv_referencia,
               'N',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               TO_CHAR(I.AJUSTE), --J.NO_FACTURA,
               ' ',
               ' ',
               ' ',
               0);
          
          EXCEPTION
            WHEN OTHERS THEN
              LV_ERROR := SUBSTR('ERROR 6, AL INSERTAR EL PREASIENTO ' ||
                                 SQLERRM,
                                 1,
                                 1500);
              RAISE LE_CONTROL_ERROR;
          END;
        END IF; -- C_OBTIENE_TOTAL2%FOUND
        CLOSE C_OBTIENE_TOTAL2;
        --END LOOP; --C_OBTIENE_CUENTAS
        
     --10172 ACTUALIZO COMENTARIO PONIENDO MARCA DE PS
        UPDATE CO_CARTERA_CLIENTES T
              SET  COMENTARIO= SUBSTR(T.COMENTARIO,1,1800)||'- PS'
            WHERE NO_FACTURA = I.AJUSTE
                 --AND ESTADO IN ('V', 'D');
              AND ESTADO IN ('D')
              AND CIA = LN_CIA;
              
             UPDATE CO_CARTERA_CLIENTES T
             SET  COMENTARIO= SUBSTR(T.COMENTARIO,1,1800)||'- PS'
           WHERE NO_FACTURA = I.AJUSTE
             AND ESTADO IN ('V')
             AND CIA = I.COMPANIA;         
       
      --10172 SE ACTIVA SOLO SI EL PROCESO CREA ASIENTO EN PS
      IF LV_BAN_UP_PS ='S' THEN
       UPDATE CO_CARTERA_CLIENTES
           SET ESTADO = 'T'
         WHERE NO_FACTURA = I.AJUSTE
           --AND ESTADO IN ('V', 'D');
           AND ESTADO IN ('D')
           AND CIA = LN_CIA;
           
        UPDATE CO_CARTERA_CLIENTES
           SET ESTADO = 'T'
         WHERE NO_FACTURA = I.AJUSTE
           AND ESTADO IN ('V')
           AND CIA = I.COMPANIA;
           
       END IF;  
      
       
        COMMIT;
      EXCEPTION
        WHEN LE_CONTROL_ERROR THEN
          ROLLBACK;
          UPDATE CO_CARTERA_CLIENTES
             SET ESTADO = 'E', COMENTARIO = LV_ERROR || COMENTARIO
           WHERE /*CIA = LN_CIA AND*/
           NO_FACTURA = I.AJUSTE;
          COMMIT;
          RAISE LE_ERROR;
      END;
    END LOOP; --C_COMPANIAS
    
  END IF; 
   --10172 si la bandera para enviar los asientos a sap esta en S realiza la replica
   IF LV_BAN_SAP = 'S' THEN
     CLK_RECLASIFICACION.GENERA_PREASIENTO_REC_SAP(PV_FECHACTUAL => PV_FECHACTUAL,
                                                PV_FECHANTERIOR => PV_FECHANTERIOR,
                                                PD_FECHA => PD_FECHA,
                                                PV_ERROR => PV_ERROR);
     IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF;  
         
   END IF;
   
   --INICIO [11188] IRO_HRO 09/03/2017
   --BANDERA QUE INDICA EL ENVIO DE ASIENTOS A SAP DE LAS CUENTAS DTH.
   IF LV_BAN_SAP_DTH = 'S' THEN
     CLK_RECLASIFICACION.GENERA_PREASIENTO_SAP_DTH(PV_FECHACTUAL => PV_FECHACTUAL,
                                                PV_FECHANTERIOR => PV_FECHANTERIOR,
                                                PD_FECHA => PD_FECHA,
                                                PV_ERROR => PV_ERROR);
     IF PV_ERROR IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF;  
         
   END IF;
   --FIN [11188] IRO_HRO 09/03/2017
   
   --10172 SE ACTIVA SOLO SI EL PROCESO CREA ASIENTO EN PS Y SAP
   IF LV_BAN_UP_SAP_PS = 'S' THEN
       FOR I IN C_COMPANIAS LOOP
         IF I.COMPANIA=2 THEN
           LN_CIA := 1;
         ELSE
           IF I.COMPANIA=1 THEN
            LN_CIA := 2; 
           END IF; 
         END IF;   
          UPDATE CO_CARTERA_CLIENTES
               SET ESTADO = 'T'
             WHERE NO_FACTURA = I.AJUSTE
               --AND ESTADO IN ('V', 'D');
               AND ESTADO IN ('D')
               AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
               AND CIA = LN_CIA;
               
            UPDATE CO_CARTERA_CLIENTES
               SET ESTADO = 'T'
             WHERE NO_FACTURA = I.AJUSTE
               AND ESTADO IN ('V')
               AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
               AND CIA = I.COMPANIA;
               
            COMMIT;
       END LOOP;
   END IF;    
       
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := PV_ERROR || SQLERRM;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
    
  END genera_preasiento_rec;
  --
  FUNCTION gvf_obtener_valor_parametro(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
    --
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER,
                       CV_ID_PARAMETRO      VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END gvf_obtener_valor_parametro;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 09/01/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Actualiza parametros del proyecto
  --===============================================================================================--  
  PROCEDURE gvp_actualiza_parametro(pn_id_tipo_parametro IN NUMBER,
                                    pv_id_parametro      IN VARCHAR2,
                                    pv_valor             IN VARCHAR2,
                                    pv_error             OUT VARCHAR2) IS
    
    lv_programa VARCHAR2(60) := 'GVP_ACTUALIZA_PARAMETRO';
    lv_sql LONG;
    lv_sentencia LONG;
    lv_error VARCHAR2(4000); 
    le_salida EXCEPTION;                             
  BEGIN
    -- BSCS
    lv_sentencia := ' update gv_parametros '||
                       ' set valor = NVL( :1 ,VALOR) '||
                     ' where id_tipo_parametro = :2 '||
                       ' and id_parametro = :3';
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia
        USING pv_valor, pn_id_tipo_parametro, pv_id_parametro;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_salida;
        END IF;
    END;
    -- FINANCIERO
    lv_sql := gvf_obtener_valor_parametro(7399,'C_PARAM_REFR');
    BEGIN
      EXECUTE IMMEDIATE lv_sql
        USING pv_valor, pn_id_tipo_parametro, pv_id_parametro;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_salida;
        END IF;
    END;
  EXCEPTION
    WHEN le_salida THEN
      pv_error := lv_programa || ' - ' || substr(lv_error, 1, 150);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' - ' || substr(SQLERRM, 1, 150);
  END gvp_actualiza_parametro;
  
 --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 19/08/2014
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento que genera el reporte de cuentas castigadas / reclasificadas
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 09/01/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Transferencia FTP archivo adjunto Notificaciones
  --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--  
  --===============================================================================================--
  -- Project : [10855] RECLASIFICACION Y CASTIGO
  -- Author  : SUD Kevin Avalos
  -- Created : 06/06/2016  
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Modificacion en la cantidad de parametros enviados a la funcion que genera el reporte
  -- Purpose : Generacion de un reporte a partir de una sentencia SQL
  --===============================================================================================-- 
  PROCEDURE gcp_gen_cuentas_vencer(pn_region     IN NUMBER,
                                   pv_fecha      IN VARCHAR2,
                                   pv_name_file1 OUT VARCHAR2,
                                   pv_name_file2 OUT VARCHAR2,
                                   pv_error      OUT VARCHAR2) IS
  
    lv_programa   VARCHAR2(60) := 'GCP_GEN_CUENTAS_VENCER';
    lv_regionrec  VARCHAR2(5);
    lv_regioncas  VARCHAR2(5);
    lv_archivo_cv VARCHAR2(100);
    lv_archivo_cc VARCHAR2(100); 
    lv_extension  VARCHAR2(5) := '.xls';
    lv_extension1  VARCHAR2(5):= '.txt';
    lv_directorio VARCHAR2(500) := NULL;
    lv_sql        VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    le_salida     EXCEPTION;
  
    lv_error       VARCHAR2(4000);
    lv_sentencia_1 VARCHAR2(32650);
    lv_sentencia_2 VARCHAR2(32650);
    lv_sentencia_3 VARCHAR2(32650);
    lv_sentencia_4 VARCHAR2(32650);
    lv_sentencia_5 VARCHAR2(32650);
    lv_hoja_1      VARCHAR2(500);
    lv_hoja_2      VARCHAR2(500);
    lv_hoja_3      VARCHAR2(500);
    lv_hoja_4      VARCHAR2(500);
    lv_hoja_5      VARCHAR2(500);
    lv_bypass      VARCHAR2(20);
    lv_mora        VARCHAR2(4);
    lv_sql2        LONG;
    lv_sql3        LONG;
    le_salida2     EXCEPTION;
    --
    ln_error NUMBER;
  
  BEGIN
    
    IF NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'BAND_CORREO_7399'),'N') ='N' THEN
      RETURN;
    END IF;
    
    BEGIN
      EXECUTE IMMEDIATE lv_sql;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    IF pn_region = 1 THEN
      lv_regioncas := '1';
      lv_regionrec := '2';
    ELSE
      lv_regioncas := '2';
      lv_regionrec := '1';
    END IF;
    -- Obtengo parametro de bypass de deuda
    lv_bypass := gvf_obtener_valor_parametro(7399,'BYPASS_BSCS');
    
    -- Obtengo parametro de mora pendiente
    lv_mora := gvf_obtener_valor_parametro(7399,'MORA_PENDIENTE');    
    
    -- Armo nombres de los archivos que guardaran los nombres de las hojas
    lv_archivo_cv := 'CUENTAS_VENCER_R' || lv_regioncas || '_' || to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyymmdd'); --pv_fecha;
    lv_archivo_cc := 'CUENTAS_CASTIGAR_R' || lv_regioncas || '_' || to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyymmdd'); --pv_fecha;
    -- Arma el nombre de los archivos que guardaran los nombres de las hojas con la extension
    lv_archivo_cv := lv_archivo_cv || lv_extension1;
    lv_archivo_cc := lv_archivo_cc || lv_extension1;
     
    -- Obtiene la ruta donde se guardara el archivo
    lv_directorio := gvf_obtener_valor_parametro(7399,'DIRECTORIO_REPORTE');
  
    -- Archivo 1: Reporte de cuentas con DEUDAS POR VENCER(MAS DE 11 MESES EN MORA), MAYORES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_1 := gvf_obtener_valor_parametro(7399,'QUERY_RPGE11_GTBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<REGION>', lv_regionrec);
    
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_1 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_1 := REPLACE(lv_hoja_1, '<CONDICION>', substr('11M_mayor'||lv_bypass,1,13));
  
    --lv_hoja_1 := REPLACE(lv_hoja_1, '<REGION>', lv_regioncas);--sud cac 
    lv_hoja_1 := REPLACE(lv_hoja_1, '<REGIONCART>', lv_regioncas);
       
    -- Arma el nombre del archivo con la extension
    lv_hoja_1 := lv_hoja_1 || lv_extension;
    -- Generamos Archivo 1
    /*genera_reporte_excel(lv_directorio, lv_hoja_1, lv_sentencia_1);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB    
    genera_reporte_excel_spool(lv_archivo_cv,
                               lv_hoja_1,
                               lv_sentencia_1,
                               lv_directorio);
    --10855 FIN SUD KAE    
    -- Archivo 2: Reporte de cuentas con DEUDAS DE 8 A 11 MESES EN MORA, CON SALDOS MENORES O IGUALES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_2 := gvf_obtener_valor_parametro(7399,'QUERY_RPLE11_LEBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_2 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_2 := REPLACE(lv_hoja_2, '<CONDICION>', substr('8M_menor'||lv_bypass,1,13));
  
    --lv_hoja_2 := REPLACE(lv_hoja_2, '<REGION>', lv_regioncas);--sud cac 
    lv_hoja_2 := REPLACE(lv_hoja_2, '<REGIONCART>', lv_regioncas);   
    -- Arma el nombre del archivo con la extension
    lv_hoja_2 := lv_hoja_2 || lv_extension;
    -- Generamos Archivo 1
	/*genera_reporte_excel(lv_directorio, lv_hoja_2, lv_sentencia_2);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(lv_archivo_cv,
                               lv_hoja_2,
                               lv_sentencia_2,
                               lv_directorio);
    --10855 FIN SUD KAE        
    -- Archivo 3: Reporte de cuentas con DEUDAS POR VENCER(MAS DE 11 MESES EN MORA), CON SALDOS MENORES O IGUALES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_3 := gvf_obtener_valor_parametro(7399,'QUERY_RPGE11_LEBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_3 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_3 := REPLACE(lv_hoja_3, '<CONDICION>', substr('11M_menor'||lv_bypass,1,13));  
    lv_hoja_3 := REPLACE(lv_hoja_3, '<REGIONCART>', lv_regioncas);
    
    -- Arma el nombre del archivo con la extension
    lv_hoja_3 := lv_hoja_3 || lv_extension;
	    /*genera_reporte_excel(lv_directorio, lv_hoja_3, lv_sentencia_3);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(lv_archivo_cv,
                               lv_hoja_3,
                               lv_sentencia_3,
                               lv_directorio);
    --10855 FIN SUD KAE   
    -- Archivo 4: Reporte de cuentas con DEUDAS POR VENCER(MAS DE 11 MESES EN MORA), CON INCONSISTENCIA A REPORTAR EN BILLING, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_4 := gvf_obtener_valor_parametro(7399,'QUERY_RPGE8_INCONS');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_4 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_4 := REPLACE(lv_hoja_4, '<CONDICION>', substr('8M_11M_INCONS',1,13));
    lv_hoja_4 := REPLACE(lv_hoja_4, '<REGIONCART>', lv_regioncas);    
    --                                      
    -- Arma el nombre del archivo con la extension
    lv_hoja_4 := lv_hoja_4 || lv_extension;
    -- Generamos Archivo 1
    /*genera_reporte_excel(lv_directorio, lv_hoja_4, lv_sentencia_4);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(lv_archivo_cv,
                               lv_hoja_4,
                               lv_sentencia_4,
                               lv_directorio);
    --10855 FIN SUD KAE
    
    --Generamos el Archivo.txt donde se guardaran los nombres de las hojas de cada archivo :: 062015
    /*DECLARE 
     ARCHIVOS        UTL_FILE.FILE_TYPE;
    BEGIN
     ARCHIVOS := UTL_FILE.FOPEN(UPPER(lv_directorio),lv_archivo_cv,'W',32767);
        UTL_FILE.PUT_LINE(ARCHIVOS,lv_hoja_1);
           UTL_FILE.PUT_LINE(ARCHIVOS,lv_hoja_2);
              UTL_FILE.PUT_LINE(ARCHIVOS,lv_hoja_3);
                 UTL_FILE.PUT_LINE(ARCHIVOS,lv_hoja_4);
                    UTL_FILE.PUT_LINE(archivos,' ');
        UTL_FILE.FCLOSE(ARCHIVOS);
    END;
    ---    
    BEGIN
          --PERMISOS A LOS ARCHIVOS QUE SE CREAN
          EXECUTE IMMEDIATE CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'PERMISO_ARCHIVO')
             USING IN lv_directorio, IN lv_archivo_cv;
             
          EXCEPTION
            WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE(SUBSTR('PROBLEMA PERMISOS: '||sqlerrm,1,120));          
    END;*/        
    ---
    -- Archivo5 - Hoja 1: Reporte de cuentas con DEUDAS RECLASIFICADAS POR VENCER (60 MESES DE MORA), POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_5 := gvf_obtener_valor_parametro(7399,'QUERY_RPCR_GE60');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_5 := REPLACE(lv_sentencia_5, '<MORA>', to_number(lv_mora));
  
    lv_sentencia_5 := REPLACE(lv_sentencia_5, '<REGIONCART>', lv_regioncas);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_5 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    --lv_hoja_5 := REPLACE(lv_hoja_5, '<CONDICION>', substr('8M_11M_INCONS',1,13));
    lv_hoja_5 := REPLACE(lv_hoja_5, '<CONDICION>', substr('60M_RD_X_VENC',1,13));
  
    lv_hoja_5 := REPLACE(lv_hoja_5, '<REGIONCART>', lv_regioncas);   
    -- 
    lv_hoja_5 := lv_hoja_5 || lv_extension;
    /*genera_reporte_excel(lv_directorio, lv_hoja_5, lv_sentencia_5);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(lv_archivo_cc,
                               lv_hoja_5,
                               lv_sentencia_5,
                               lv_directorio);
    --10855 FIN SUD KAE
    
    /*DECLARE 
      ARCHIVOS        UTL_FILE.FILE_TYPE;
    BEGIN
      ARCHIVOS := UTL_FILE.FOPEN(UPPER(lv_directorio),lv_archivo_cc,'W',32767);
      UTL_FILE.PUT_LINE(ARCHIVOS,lv_hoja_5);
          UTL_FILE.PUT_LINE(archivos,' ');
      UTL_FILE.FCLOSE(ARCHIVOS);
    END;
    
   BEGIN
          --PERMISOS A LOS ARCHIVOS QUE SE CREAN
          EXECUTE IMMEDIATE CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'PERMISO_ARCHIVO')
             USING IN lv_directorio, IN lv_archivo_cc;
             
          EXCEPTION
            WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE(SUBSTR('PROBLEMA PERMISOS: '||sqlerrm,1,120));          
    END;*/ 
    
    pv_name_file1 := substr(lv_archivo_cv, 1,instr(lv_archivo_cv,'.',1)- 1);
    pv_name_file2 := substr(lv_archivo_cc, 1,instr(lv_archivo_cc,'.',1)- 1);                                  

  EXCEPTION
    WHEN le_salida THEN
      NULL;
    WHEN le_salida2 THEN
      pv_error := lv_programa || ' - ' || substr(lv_error, 1, 150);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' - ' || substr(SQLERRM, 1, 150);
  END gcp_gen_cuentas_vencer;
 --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 12/03/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento que genera el reporte final de cuentas castigadas / reclasificadas
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 12/03/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Transferencia FTP archivo adjunto Notificaciones
  --===============================================================================================-- 
   --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--  
  --===============================================================================================--
  -- Project : [10855] RECLASIFICACION Y CASTIGO
  -- Author  : SUD Kevin Avalos
  -- Created : 06/06/2016  
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Modificacion en la cantidad de parametros enviados a la funcion que genera el reporte
  -- Purpose : Generacion de un reporte a partir de una sentencia SQL
  --===============================================================================================--
  PROCEDURE gcp_gen_reporte_final(pn_region     IN NUMBER,
                                   pv_fecha      IN VARCHAR2,
                                   pv_name_file1 OUT VARCHAR2,
                                   pv_error      OUT VARCHAR2) IS
  
    lv_programa   VARCHAR2(60) := 'GCP_GEN_REPORTE_FINAL';
    lv_regionrec  VARCHAR2(5);
    lv_regioncas  VARCHAR2(5);
    lv_archivo    VARCHAR2(100);
    lv_archivo2   VARCHAR2(100);
    lv_extension  VARCHAR2(5) := '.xls';
    lv_directorio VARCHAR2(500) := NULL;
    lv_sql        VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    le_salida     EXCEPTION;
  
    lv_error       VARCHAR2(4000);
    lv_sentencia_1 VARCHAR2(32650);
    lv_sentencia_2 VARCHAR2(32650);
    lv_sentencia_3 VARCHAR2(32650);
    lv_sentencia_4 VARCHAR2(32650);
    lv_sentencia_5 VARCHAR2(32650);
    lv_sentencia_6 VARCHAR2(32650);
    lv_sentencia_7 VARCHAR2(32650);
    lv_hoja_1      VARCHAR2(500);
    lv_hoja_2      VARCHAR2(500);
    lv_hoja_3      VARCHAR2(500);
    lv_hoja_4      VARCHAR2(500);
    lv_hoja_5      VARCHAR2(500);
    lv_hoja_6      VARCHAR2(500);
    lv_hoja_7      VARCHAR2(500);
    lv_bypass      VARCHAR2(20);
    lv_mora        VARCHAR2(4);
    LV_JRNL        VARCHAR2(500);
    LV_DOC_TYPE    VARCHAR2(500);
    lv_fecha_fin   VARCHAR2(500);
    lv_fecha_ini   VARCHAR2(500);    
    lv_sql2        LONG;
    lv_sql3        LONG;
    Lv_ARCHIVO1    VARCHAR2(1000);
    le_salida2     EXCEPTION;
    ln_error NUMBER; 
    lv_extension2  VARCHAR2(5) := '.txt';
    LV_DOC_TYPE2   VARCHAR2(500);
    --[11334] IRO_HRO 18/05/2017  
    lv_proceso1    VARCHAR2(5);
    lv_Cia1        VARCHAR2(5);
    lv_proceso2    VARCHAR2(5);
    lv_Cia2        VARCHAR2(5);
    --[11334] IRO_HRO 18/05/2017
	  archivos       UTL_FILE.FILE_TYPE;
  BEGIN
    
    IF NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'BAND_CORREO_7399'),'N') ='N' THEN
      RETURN;
    END IF;
    
    BEGIN
      EXECUTE IMMEDIATE lv_sql;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;     
   
    select to_char(last_day(pv_fecha),'dd/mm/yyyy') into lv_fecha_fin from dual;    

    IF pn_region = 1 THEN
      lv_regioncas := '1';
      lv_regionrec := '2';
      LV_JRNL:='MOD_CART1';
      LV_DOC_TYPE:='CMCRV1';
      LV_DOC_TYPE2:='CCASC1';
    --[11334] IRO_HRO 18/05/2017
      lv_proceso1:= 'P';
      lv_Cia1:= '2';
      lv_proceso2:= 'R';
      lv_Cia2:= '1';
    --[11334] IRO_HRO 18/05/2017
      
    ELSE
      lv_regioncas := '2';
      lv_regionrec := '1';
      LV_JRNL:='MOD_CART2';  
      LV_DOC_TYPE:='CMCRV2';          
      LV_DOC_TYPE2:='CCASC2';
     --[11334] IRO_HRO 18/05/2017
      lv_proceso1:= 'P';
      lv_Cia1:= '1';
      lv_proceso2:= 'R';
      lv_Cia2:= '2';
    --[11334] IRO_HRO 18/05/2017
    END IF;

    -- Obtengo parametro de bypass de deuda
    lv_bypass := gvf_obtener_valor_parametro(7399,'BYPASS_BSCS');
    
    -- Obtengo parametro de mora pendiente
    lv_mora := gvf_obtener_valor_parametro(7399,'MORA_PENDIENTE');
  
    -- Armo nombre del archivo
    lv_archivo := 'CUENTAS_RECLASIFICADAS_R' || lv_regioncas || '_' || to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyymmdd'); --pv_fecha;  
    -- Arma el nombre del archivo con la extension
    lv_archivo2 := lv_archivo || lv_extension2;
    -- Obtiene la ruta donde se guardara el archivo
    lv_directorio := gvf_obtener_valor_parametro(7399,'DIRECTORIO_REPORTE');
  
    -- Hoja 1: Reporte de cuentas con CUENTAS RECLASIFICADAS, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_1 := gvf_obtener_valor_parametro(7399,'QUERY_CTS_RECLAS');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_1 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CTS_RECLA');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_1 := REPLACE(lv_hoja_1, '<CONDICION>', substr('11M_mayor'||lv_bypass,1,13));
  
    --lv_hoja_1 := REPLACE(lv_hoja_1, '<REGION>', lv_regioncas);--sud cac 
    lv_hoja_1 := REPLACE(lv_hoja_1, '<REGIONCART>', lv_regioncas);
    
    lv_hoja_1 := lv_hoja_1 || lv_extension;    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_1,
                         pv_query      => lv_sentencia_1);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB  
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_1,
                               pv_query      => lv_sentencia_1,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
    -- Hoja 2: Reporte de cuentas con CUENTAS CASTIGADAS, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_2 := gvf_obtener_valor_parametro(7399,'QUERY_CTS_CAST');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<BYPASS>', to_number(lv_bypass));   
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<REGION>', lv_regioncas);
    -- Fin SUD CAC :: 062015

    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_2 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CTS_CAS');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_2 := REPLACE(lv_hoja_2, '<CONDICION>', substr('8M_menor'||lv_bypass,1,13));
  
    --lv_hoja_2 := REPLACE(lv_hoja_2, '<REGION>', lv_regioncas);--sud cac 
    lv_hoja_2 := REPLACE(lv_hoja_2, '<REGIONCART>', lv_regioncas);

    lv_hoja_2 := lv_hoja_2 || lv_extension;
     
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_2,
                         pv_query      => lv_sentencia_2);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_2,
                               pv_query      => lv_sentencia_2,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
    -- Hoja 3: Reporte de cuentas con DEUDAS CANCELADAS, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_3 := gvf_obtener_valor_parametro(7399,'QUERY_CTS_DEUDAS_CANC');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_3 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_DEUD_CANC');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_3 := REPLACE(lv_hoja_3, '<CONDICION>', substr('11M_menor'||lv_bypass,1,13));
  
    --lv_hoja_3 := REPLACE(lv_hoja_3, '<REGION>', lv_regioncas);--sud cac
    lv_hoja_3 := REPLACE(lv_hoja_3, '<REGIONCART>', lv_regioncas);
    
    lv_hoja_3 := lv_hoja_3 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_3,
                         pv_query      => lv_sentencia_3);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_3,
                               pv_query      => lv_sentencia_3,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
    -- Hoja 4: Reporte de cuentas con CONDONACION 11 MESES, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_4 := gvf_obtener_valor_parametro(7399,'QUERY_CND_11M');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_4 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CND_11_M');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_4 := REPLACE(lv_hoja_4, '<CONDICION>', substr('8M_11M_INCONS',1,13));
  
    --lv_hoja_4 := REPLACE(lv_hoja_4, '<REGION>', lv_regioncas);--sud cac
    lv_hoja_4 := REPLACE(lv_hoja_4, '<REGIONCART>', lv_regioncas);    
    ------                        
    lv_hoja_4 := lv_hoja_4 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_4,
                         pv_query      => lv_sentencia_4);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB  
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_4,
                               pv_query      => lv_sentencia_4,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
    -- Hoja 5: Reporte de cuentas con CONDONACION 8 A 10 MESES, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_5 := gvf_obtener_valor_parametro(7399,'QUERY_CND_8_10M');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_5 := REPLACE(lv_sentencia_5, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_5 := REPLACE(lv_sentencia_5, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_5 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CND_8_10_M');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_5 := REPLACE(lv_hoja_5, '<CONDICION>', substr('8M_11M_INCONS',1,13));
  
    lv_hoja_5 := REPLACE(lv_hoja_5, '<REGIONCART>', lv_regioncas);    
    lv_hoja_5 := lv_hoja_5 || lv_extension;
     
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_5,
                         pv_query      => lv_sentencia_5);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB  
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_5,
                               pv_query      => lv_sentencia_5,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE    
    lv_fecha_ini:=to_date(pv_fecha,'dd/mm/yyyy');
     -- Hoja 6: Reporte de cuentas Autom�tico
    -- Obtiene el query dinamico
    lv_sentencia_6 := gvf_obtener_valor_parametro(7399,'QUERY_AUTOM_PEOPLE');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<FECHA_INI>', lv_fecha_ini);
  
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<FECHA_FIN>', lv_fecha_fin);       
    
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<CIA>',pn_region);
          
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<MOD_CART>',LV_JRNL);
  
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<CMCRV>', LV_DOC_TYPE);
    
    lv_sentencia_6 := REPLACE(lv_sentencia_6, '<CCASC>', LV_DOC_TYPE2);    

    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_6 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_AUTOM_CAS');
  
    lv_hoja_6 := REPLACE(lv_hoja_6, '<REGIONCART>', lv_regioncas);    
    ------                                         
    lv_hoja_6 := lv_hoja_6 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_6,
                         pv_query      => lv_sentencia_6);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_6,
                               pv_query      => lv_sentencia_6,
                               pv_arch_ref   => lv_archivo2);
    
    --INICIO [11334] IRO_HRO 12/05/2017
    
    -- Obtiene el query dinamico
    lv_sentencia_7 := gvf_obtener_valor_parametro(11334,'QUERY_CTS_NO_PROCES');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte    
    --[11334] IRO_HRO 18/05/2017
    lv_sentencia_7 := REPLACE(lv_sentencia_7, '<PROCESO1>', lv_proceso1);
    lv_sentencia_7 := REPLACE(lv_sentencia_7, '<CIA1>', lv_Cia1);
    lv_sentencia_7 := REPLACE(lv_sentencia_7, '<PROCESO2>', lv_proceso2);
    lv_sentencia_7 := REPLACE(lv_sentencia_7, '<CIA2>', lv_Cia2);
    --[11334] IRO_HRO 18/05/2017   
    
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_7 := gvf_obtener_valor_parametro(11334,'HOJA_REPOR_NO_PROCES');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
   
    lv_hoja_7 := REPLACE(lv_hoja_7, '<REGIONCART>', lv_regioncas);
    
    lv_hoja_7 := lv_hoja_7 || lv_extension;
    
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_7,
                               pv_query      => lv_sentencia_7,
                               pv_arch_ref   => lv_archivo2);
    
    --FIN [11334] IRO_HRO 12/05/2017
     
    
    --10855 INI SUD KAE
    --Parametro de salida con el nombre del archivo
    pv_name_file1 := lv_archivo;
    
   /*-- Ini SUD CAC :: 062015
   archivos := UTL_FILE.FOPEN(upper(lv_directorio),lv_archivo2,'w',32767);
     UTL_FILE.PUT_LINE(archivos,lv_hoja_1);
        UTL_FILE.PUT_LINE(archivos,lv_hoja_2);
           UTL_FILE.PUT_LINE(archivos,lv_hoja_3);
              UTL_FILE.PUT_LINE(archivos,lv_hoja_4);
                 UTL_FILE.PUT_LINE(archivos,lv_hoja_5);
                    UTL_FILE.PUT_LINE(archivos,lv_hoja_6);
                     UTL_FILE.PUT_LINE(archivos,' ');
    UTL_FILE.FCLOSE(archivos); 
      ---lv_archivo2   
   BEGIN
      --PERMISOS A LOS ARCHIVOS QUE SE CREADOS
      EXECUTE IMMEDIATE CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'PERMISO_ARCHIVO')
         USING IN lv_directorio, IN lv_archivo2;
             
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE(SUBSTR('PROBLEMA PERMISOS: '||sqlerrm,1,120));          
   END;*/
   
   
  EXCEPTION
    WHEN le_salida THEN
      NULL;
    WHEN le_salida2 THEN
      pv_error := lv_programa || ' - ' || substr(lv_error, 1, 150);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' - ' || substr(SQLERRM, 1, 150);
  END gcp_gen_reporte_final;
  
 --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 19/08/2014
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento que genera el reporte de cuentas reclasificadas con pagos y/o abonos
  --           generados.
  --===============================================================================================--
 -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 09/01/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Transferencia FTP archivo adjunto Notificaciones
  --===============================================================================================--
  --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--  
  --===============================================================================================--
  -- Project : [10855] RECLASIFICACION Y CASTIGO
  -- Author  : SUD Kevin Avalos
  -- Created : 06/06/2016  
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Modificacion en la cantidad de parametros enviados a la funcion que genera el reporte
  -- Purpose : Generacion de un reporte a partir de una sentencia SQL
  --===============================================================================================-- 
  PROCEDURE gcp_gen_cuentas_pagos(pn_region     IN NUMBER,
                                  pv_fecha      IN VARCHAR2,
                                  pv_name_file1 OUT VARCHAR2,
                                  pv_error      OUT VARCHAR2) IS
  
    lv_programa   VARCHAR2(60) := 'GCP_GEN_CUENTAS_PAGOS';
    lv_regionrec  VARCHAR2(5);
    lv_regioncas  VARCHAR2(5);
    lv_archivo    VARCHAR2(100);
    lv_extension  VARCHAR2(5) := '.xls';
    lv_directorio VARCHAR2(500) := NULL;
    lv_sql        VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    le_salida     EXCEPTION;
  
    lv_error       VARCHAR2(4000);
    lv_sentencia_1 VARCHAR2(32650);
    lv_sentencia_2 VARCHAR2(32650);
    lv_sentencia_3 VARCHAR2(32650);
    lv_sentencia_4 VARCHAR2(32650);
    lv_hoja_1      VARCHAR2(500);
    lv_hoja_2      VARCHAR2(500);
    lv_hoja_3      VARCHAR2(500);
    lv_hoja_4      VARCHAR2(500);
    lv_bypass      VARCHAR2(20);
    le_salida2     EXCEPTION;   
    lv_archivo2   VARCHAR2(100);    
    lv_extension2  VARCHAR2(5) := '.txt';
    archivos        UTL_FILE.FILE_TYPE;
  BEGIN
    
    IF NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'BAND_CORREO_7399'),'N') ='N' THEN
      RETURN;
    END IF;
    
    BEGIN
      EXECUTE IMMEDIATE lv_sql;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    IF pn_region = 1 THEN
      lv_regioncas := '1';
      lv_regionrec := '2';
    ELSE
      lv_regioncas := '2';
      lv_regionrec := '1';
    END IF;
    -- Obtengo parametro de bypass de deuda
    lv_bypass := gvf_obtener_valor_parametro(7399,'BYPASS_BSCS');
    
    -- Obtengo parametro de mora pendiente
  
    -- Armo nombre del archivo
    lv_archivo := 'CUENTAS_CON_PAGOS_R' || lv_regioncas || '_' || to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyymmdd'); --pv_fecha;
  
    -- Arma el nombre del archivo con la extension
    lv_archivo2 := lv_archivo || lv_extension2;

    -- Obtiene la ruta donde se guardara el archivo
    lv_directorio := gvf_obtener_valor_parametro(7399,'DIRECTORIO_REPORTE');
  
    -- Hoja 1: Reporte de cuentas con DEUDAS MARCADAS (MAS DE 11 MESES, A RECLASIFICACION), MAYORES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_1 := gvf_obtener_valor_parametro(7399,'QUERY_MARCA_GTBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_1 := REPLACE(lv_sentencia_1, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_1 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_1 := REPLACE(lv_hoja_1, '<CONDICION>', substr('MARCADAS_REC',1,13));
    
    lv_hoja_1 := REPLACE(lv_hoja_1, '<REGIONCART>', lv_regioncas);
    
    lv_hoja_1 := lv_hoja_1 || lv_extension;    
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_1,
                         pv_query      => lv_sentencia_1);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB  
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_1,
                               pv_query      => lv_sentencia_1,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE 
    -- Hoja 2: Reporte de cuentas con DEUDAS DE 8 A 11 MESES EN MORA, CON SALDOS MENORES O IGUALES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_2 := gvf_obtener_valor_parametro(7399,'QUERY_RPLE11_LEBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_2 := REPLACE(lv_sentencia_2, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_2 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_2 := REPLACE(lv_hoja_2, '<CONDICION>', substr('8M_menor'||lv_bypass,1,13));
   
    lv_hoja_2 := REPLACE(lv_hoja_2, '<REGIONCART>', lv_regioncas);
    
    lv_hoja_2 := lv_hoja_2 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_2,
                         pv_query      => lv_sentencia_2);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB  
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_2,
                               pv_query      => lv_sentencia_2,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
  
    -- Hoja 3: Reporte de cuentas con DEUDAS POR VENCER(MAS DE 11 MESES EN MORA), CON SALDOS MENORES O IGUALES A $15, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_3 := gvf_obtener_valor_parametro(7399,'QUERY_RPGE11_LEBP');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_3 := REPLACE(lv_sentencia_3, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_3 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_3 := REPLACE(lv_hoja_3, '<CONDICION>', substr('11M_menor'||lv_bypass,1,13));
    --
    lv_hoja_3 := REPLACE(lv_hoja_3, '<REGIONCART>', lv_regioncas);
    lv_hoja_3 := lv_hoja_3 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_3,
                         pv_query      => lv_sentencia_3);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB    
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_3,
                               pv_query      => lv_sentencia_3,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE
    
    -- Hoja 4: Reporte de cuentas con DEUDAS MARCADAS (MAS DE 11 MESES, A RECLASIFICACION), CANCELADAS EN BSCS, POR REGION
    -- Obtiene el query dinamico
    lv_sentencia_4 := gvf_obtener_valor_parametro(7399,'QUERY_MARCA_DELETE');
  
    -- Reemplaza el valor de la region para ejecutar el query del reporte
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<BYPASS>', to_number(lv_bypass));
  
    lv_sentencia_4 := REPLACE(lv_sentencia_4, '<REGION>', lv_regionrec);
  
    -- Obtiene el nombre de la hoja del reporte
    lv_hoja_4 := gvf_obtener_valor_parametro(7399,'HOJA_REPOR_CR');
  
    -- Reemplaza el valor del tipo y region para el nombre de la hoja
    lv_hoja_4 := REPLACE(lv_hoja_4, '<CONDICION>', substr('CANCELADAS',1,13));
    
    lv_hoja_4 := REPLACE(lv_hoja_4, '<REGIONCART>', lv_regioncas);
    lv_hoja_4 := lv_hoja_4 || lv_extension;
    
    /*genera_reporte_excel(pv_directorio => lv_directorio,
                         pv_file       => lv_hoja_4,
                         pv_query      => lv_sentencia_4);*/
    --10855 INI SUD KAE-- SE AGREGAN DOS PARAMETROS MAS AL PROCESO PARA INDICAR SU ARCHIVO DE REFERENCIA Y SI LA RUTA ES RFS O RDB    
    genera_reporte_excel_spool(pv_directorio => lv_directorio,
                               pv_file       => lv_hoja_4,
                               pv_query      => lv_sentencia_4,
                               pv_arch_ref   => lv_archivo2);
    --10855 FIN SUD KAE

    --Parametro de salida con el nombre del archivo
    pv_name_file1 := lv_archivo;
    /*--Ini SUD CAC :: 062015
    archivos := UTL_FILE.FOPEN(upper(lv_directorio),lv_archivo2,'w',32767);
     UTL_FILE.PUT_LINE(archivos,lv_hoja_1);
        UTL_FILE.PUT_LINE(archivos,lv_hoja_2);
           UTL_FILE.PUT_LINE(archivos,lv_hoja_3);
              UTL_FILE.PUT_LINE(archivos,lv_hoja_4);
                UTL_FILE.PUT_LINE(archivos,' ');

    UTL_FILE.FCLOSE(archivos); 
    
    BEGIN
      --PERMISOS A LOS ARCHIVOS QUE SE CREADOS
      EXECUTE IMMEDIATE CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'PERMISO_ARCHIVO')
         USING IN lv_directorio, IN lv_archivo2;
             
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE(SUBSTR('PROBLEMA PERMISOS: '||sqlerrm,1,120));          
   END;*/
   
    --                                 
  EXCEPTION
    WHEN le_salida THEN
      NULL;
    WHEN le_salida2 THEN
      pv_error := lv_programa || ' - ' || substr(lv_error, 1, 150);
    WHEN OTHERS THEN
      pv_error := lv_programa || ' - ' || substr(SQLERRM, 1, 150);
  END gcp_gen_cuentas_pagos;

  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 19/08/2014
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento para obtener los parametros del envio de mails del reporte
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 01/09/2014
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Ajsutes a proceso de env�o de Notificaciones
  --===============================================================================================--
  --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--  
  PROCEDURE clp_mail_notificacion(PV_FECHA IN VARCHAR2, 
                                  PV_CIA IN VARCHAR2, 
                                  PV_ARCHIVO IN VARCHAR2,
                                  PV_TRAMA_ARCHIVOS IN VARCHAR2, --SUD CAC :: 062015
                                  PV_ERROR OUT VARCHAR2) IS

    LV_APLICACION    VARCHAR2(150) := 'CLP_MAIL_NOTIFICACION - ';
    LV_MAILSUBJECT   VARCHAR2(32650);
    LV_CUERPOCORREO  VARCHAR2(6000);
    LV_FROM          VARCHAR2(50);
    LV_TO            VARCHAR2(1000);
    LV_CC            VARCHAR2(1000);
    lv_parametroTO   VARCHAR2(500) := NULL;
    lv_parametroCC   VARCHAR2(500) := NULL;
    LV_USUARIO       VARCHAR2(30):=NULL;
    LV_PUERTO        VARCHAR2(20);  
    LV_SERVIDOR      VARCHAR2(50);  
    LN_VALORRETORNO  NUMBER;            
    LE_EXCEPTION     EXCEPTION;     
    LV_PROGRAMA      VARCHAR2(500):= 'CLK_RECLASIFICACION'; --SUD MTR
    Lv_Error         VARCHAR2(1000);
    lv_mes           VARCHAR2(1000);
    lv_anio           varchar2(100);    
    Le_Error         EXCEPTION;
    ---
    lv_notifica VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'NOTIFICA_TIPO');
    LV_DIRECTORIO VARCHAR2(250) := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'DIRECTORIO_REPORTE');
    LV_ARCHIVO VARCHAR2(1000) :=NULL;
    LN_ERROR   NUMBER;
    LV_MAIL_HOST VARCHAR2(50):= clk_reclasificacion.gvf_obtener_valor_parametro(7399,'MAIL_HOST'); 
    LV_MENSAGE  VARCHAR2(3000):=null;
    
     FUNCTION GETDATOTRAMA(PV_TRAMA     IN VARCHAR2,
                        PV_DATO      IN VARCHAR2,
                        PV_SEPARATOR IN VARCHAR2 DEFAULT ';') RETURN VARCHAR2 IS
  
          LN_POSINI PLS_INTEGER;
          LN_POSFIN PLS_INTEGER;
          LV_DUMMY  VARCHAR2(4000);
          NTH       NUMBER := 1;
        
        BEGIN
        
          IF NTH < 0 THEN
            LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', NTH);
          ELSE
            LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', 1, NTH);
          END IF;
        
          IF LN_POSINI > 0 THEN
            LV_DUMMY  := SUBSTR(PV_TRAMA, LN_POSINI);
            LN_POSFIN := INSTR(LV_DUMMY, PV_SEPARATOR);
            IF LN_POSFIN > 0 THEN
              LV_DUMMY := SUBSTR(LV_DUMMY, 1, LN_POSFIN - 1);
            END IF;
            LV_DUMMY := SUBSTR(LV_DUMMY, INSTR(LV_DUMMY, '=') + 1);
          END IF;
      
        RETURN LV_DUMMY;      
    END GETDATOTRAMA;

  BEGIN
    -- Validacion de la region
    IF pv_cia IS NULL THEN
        lv_error := 'DEBE INGRESAR LA REGION PARA CONTINUAR';
        RAISE le_exception;
    END IF;
    -- Validacion de la fecha
    IF pv_fecha IS NULL THEN
      lv_error := 'DEBE INGRESAR LA FECHA';
      RAISE le_exception;
    END IF;
    -- Obtiene FROM para envio de mail
    lv_from := gvf_obtener_valor_parametro(7399,'FROM_NAME_SIS');
    
    -- Obtengo los destinatarios de correos dependiendo de la region
      IF pv_cia = '1' THEN
        lv_parametroTO := 'MAIL_TO_R1';
        lv_parametroCC := 'MAIL_CC_R1';
      ELSE
        lv_parametroTO := 'MAIL_TO_R2';
        lv_parametroCC := 'MAIL_CC_R2';
      END IF;
      
    lv_mes:=clk_reclasificacion.gcf_obtiene_mes;  
    lv_anio:=to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyy');
    -- Obtiene TO para envio de mail
    lv_to := gvf_obtener_valor_parametro(7399,lv_parametroTO);
    
    -- Obtiene CC para envio de mail
    lv_cc := gvf_obtener_valor_parametro(7399,lv_parametroCC);  
    
    -- Obtiene Asunto para envio de mail
    LV_MAILSUBJECT := gvf_obtener_valor_parametro(7399,'ASUNTO_MAIL');  
      --
    -- Arma la plantilla para el nuevo componente de notificaciones                  
    LV_CUERPOCORREO := '[[8586]]<<' || lv_mes ||'- '||lv_anio|| ',>>';                                     
              
    Lv_MailSubject := REPLACE(Lv_MailSubject,'&','y');    
  
	 IF NVL(gvf_obtener_valor_parametro(7399,'ENVIA_CORREO_FTP'),'N') = 'N'  THEN
      --Indica que se va enviar utilizando .JAR de envio de correos
      DBMS_OUTPUT.PUT_LINE('MAIL_HOST_R'||pv_cia||':'||LV_MAIL_HOST);
      DBMS_OUTPUT.PUT_LINE('REMITENTE_R'||pv_cia||':'||Lv_From);
      DBMS_OUTPUT.PUT_LINE('MAIL_TO_R'||pv_cia||':'||Lv_To);
      DBMS_OUTPUT.PUT_LINE('MAIL_CC_R'||pv_cia||':'||Lv_CC);
      DBMS_OUTPUT.PUT_LINE('MAIL_SUBJECT_R'||pv_cia||':'||LV_MAILSUBJECT);
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_1:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_1'));
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_2');
      LV_MENSAGE:=REPLACE(LV_MENSAGE,'[[VALOR1]]',lv_mes ||'- '||lv_anio);      
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_2:'||LV_MENSAGE);   
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_3');     
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_3:'||LV_MENSAGE);  
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_4');
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_4:'||LV_MENSAGE);    
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_5:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_5'));
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_6:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEI_6'));     
      RETURN;      
      
    END IF;
    
    Lv_To   := REPLACE(Lv_To,';',',');
    Lv_CC   := REPLACE(Lv_CC,';',','); 
    Lv_From := REPLACE(Lv_From,';',NULL);
    
    --INICIO FTP Archivos 
    LV_ARCHIVO:=NULL; 
    LV_ARCHIVO:= GETDATOTRAMA(PV_TRAMA => PV_TRAMA_ARCHIVOS,
                               PV_DATO => 'ARCHIVO1',
                               PV_SEPARATOR => ';');   
                               
    --
    IF LV_ARCHIVO IS NOT NULL THEN 
      --
      BEGIN
        EXECUTE IMMEDIATE GVF_OBTENER_VALOR_PARAMETRO(7399,'SQL_PCK_FTP')      
          USING GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_IP_REMOTO'), 
                GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_USER_REMOTO'),
                GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_DIR_REMOTO'),
                LV_ARCHIVO,
                LV_DIRECTORIO,
                'N',
            OUT LN_ERROR,
            OUT LV_ERROR;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := substr(SQLERRM, 1, 2000);
          IF instr(lv_error, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE LE_EXCEPTION;
          END IF;
      END;
      --                                                                                              
      if ln_error <> 0 then
        raise LE_EXCEPTION;
      end if;                                 
    END IF;
    --
    LV_ARCHIVO:=NULL; 
    LV_ARCHIVO:= GETDATOTRAMA(PV_TRAMA => PV_TRAMA_ARCHIVOS,
                               PV_DATO => 'ARCHIVO2',
                               PV_SEPARATOR => ';');      
                               
    --
    IF LV_ARCHIVO IS NOT NULL THEN 
      --
      BEGIN
        EXECUTE IMMEDIATE GVF_OBTENER_VALOR_PARAMETRO(7399,'SQL_PCK_FTP')      
          USING GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_IP_REMOTO'), 
                GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_USER_REMOTO'),
                GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_DIR_REMOTO'),
                LV_ARCHIVO,
                LV_DIRECTORIO,
                'N',
            OUT LN_ERROR,
            OUT LV_ERROR;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := substr(SQLERRM, 1, 2000);
          IF instr(lv_error, 'ORA-01403') > 0 THEN
            NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
          ELSE
            RAISE LE_EXCEPTION;
          END IF;
      END;
      --                                                                                              
      if ln_error <> 0 then
        raise LE_EXCEPTION;
      end if;    
    END IF;
    --FIN FTP Archivos

  -- nueva funcionalidad con plantilla 
  -- Proceso para Envio de Notificaciones     
  -- scp_dat.sck_notificar_gtw.scp_f_notificar                       
    LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  => LV_PROGRAMA,
                                                   PD_FECHAENVIO      => SYSDATE, 
                                                   PD_FECHAEXPIRACION => SYSDATE + 1 / 24,
                                                   PV_ASUNTO          => LV_MAILSUBJECT,
                                                   PV_MENSAJE         => LV_CUERPOCORREO||'//'||PV_ARCHIVO,
                                                   PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                   PV_REMITENTE       => LV_FROM,
                                                   PV_TIPOREGISTRO    => lv_notifica,
                                                   PV_CLASE           => 'RAC',
                                                   PV_PUERTO          => LV_PUERTO,
                                                   PV_SERVIDOR        => LV_SERVIDOR,
                                                   PV_MAXINTENTOS     => 2,
                                                   PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                   PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                           
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;

  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := 'Error en Envio: ' || LN_VALORRETORNO || '-' || LV_APLICACION || LV_ERROR;
            
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 100);
          
  END clp_mail_notificacion;
 --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Cristhian Acosta
  -- Created : 12/03/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento para obtener los parametros del envio de mails del reporte
  --===============================================================================================--
  --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--     
PROCEDURE clp_mail_notificacion_final(PV_FECHA IN VARCHAR2, 
                                      PV_CIA IN VARCHAR2, 
                                      PV_ARCHIVO IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2) IS

    LV_APLICACION    VARCHAR2(150) := 'CLP_MAIL_NOTIFICACION - ';
    LV_MAILSUBJECT   VARCHAR2(32650);
    LV_CUERPOCORREO  VARCHAR2(6000);
    LV_FROM          VARCHAR2(50);
    LV_TO            VARCHAR2(1000);
    LV_CC            VARCHAR2(1000);
    lv_parametroTO   VARCHAR2(500) := NULL;
    lv_parametroCC   VARCHAR2(500) := NULL;
    LV_USUARIO       VARCHAR2(30):=NULL;
    LV_PUERTO        VARCHAR2(20);  
    LV_SERVIDOR      VARCHAR2(50);  
    LN_VALORRETORNO  NUMBER;            
    LE_EXCEPTION     EXCEPTION;     
    LV_PROGRAMA      VARCHAR2(500):= 'CLK_RECLASIFICACION'; --SUD MTR
    Lv_Error         VARCHAR2(1000);
    Le_Error         EXCEPTION;
    lv_mes           VARCHAR2(1000);
    lv_anio           varchar2(100);
    lv_plantilla       varchar2(200);
    ---
    lv_notifica VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'NOTIFICA_TIPO');
    ln_error         number:=null;
    lv_archivo       VARCHAR2(1000);
    lv_directorio    VARCHAR2(500) := NULL;
    le_salida2       EXCEPTION;
    LV_MAIL_HOST     VARCHAR2(50):= clk_reclasificacion.gvf_obtener_valor_parametro(7399,'MAIL_HOST'); 
    LV_MENSAGE       VARCHAR2(3000):=null;

  BEGIN
    LV_ARCHIVO:= PV_ARCHIVO; 
    -- Validacion de la region
    IF pv_cia IS NULL THEN
        lv_error := 'DEBE INGRESAR LA REGION PARA CONTINUAR';
        RAISE le_exception;
    END IF;
    -- Validacion de la fecha
    IF pv_fecha IS NULL THEN
      lv_error := 'DEBE INGRESAR LA FECHA';
      RAISE le_exception;
    END IF;
    -- Obtiene FROM para envio de mail
    lv_from := gvf_obtener_valor_parametro(7399,'FROM_NAME_SIS');
    lv_plantilla:= gvf_obtener_valor_parametro(7399,'PLANTILLA_REP_FINAL');
    
    -- Obtengo los destinatarios de correos dependiendo de la region
      IF pv_cia = '1' THEN
        lv_parametroTO := 'MAIL_TO_R1';
        lv_parametroCC := 'MAIL_CC_R1';
      ELSE
        lv_parametroTO := 'MAIL_TO_R2';
        lv_parametroCC := 'MAIL_CC_R2';
      END IF;
    -- Obtiene TO para envio de mail
    lv_to := gvf_obtener_valor_parametro(7399,lv_parametroTO);
    
    -- Obtiene CC para envio de mail
    lv_cc := gvf_obtener_valor_parametro(7399,lv_parametroCC);  
    
    -- Obtiene Asunto para envio de mail
    LV_MAILSUBJECT := gvf_obtener_valor_parametro(7399,'ASUNTO_MAIL_REP_FINAL');
    Lv_MailSubject := REPLACE(Lv_MailSubject,'&','y');  
    lv_mes:=clk_reclasificacion.gcf_obtiene_mes;   
    --
    lv_anio:=to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyy');
      --
 	
    IF NVL(gvf_obtener_valor_parametro(7399,'ENVIA_CORREO_FTP'),'N') = 'N'  THEN
      --Indica que se va enviar utilizando .JAR de envio de correos
      DBMS_OUTPUT.PUT_LINE('MAIL_HOST_R'||pv_cia||':'||LV_MAIL_HOST);
      DBMS_OUTPUT.PUT_LINE('REMITENTE_R'||pv_cia||':'||Lv_From);
      DBMS_OUTPUT.PUT_LINE('MAIL_TO_R'||pv_cia||':'||Lv_To);
      DBMS_OUTPUT.PUT_LINE('MAIL_CC_R'||pv_cia||':'||Lv_CC);
      DBMS_OUTPUT.PUT_LINE('MAIL_SUBJECT_R'||pv_cia||':'||LV_MAILSUBJECT);
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_1:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_1'));
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_2');
      LV_MENSAGE:=REPLACE(LV_MENSAGE,'[[VALOR1]]',lv_mes ||'- '||lv_anio);      
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_2:'||LV_MENSAGE);    
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_3');     
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_3:'||LV_MENSAGE);  
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_4');
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_4:'||LV_MENSAGE);    
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_5:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_5'));
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_6:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEF_6')); 
    
      RETURN;      
      
    END IF;       

    -- Arma la plantilla para el nuevo componente de notificaciones                  
    --LV_CUERPOCORREO := '[[80015]]<<' || lv_mes ||'- '||lv_anio|| ',>>';   
    LV_CUERPOCORREO := '[['||lv_plantilla||']]<<' || lv_mes ||'- '||lv_anio|| ',>>';   
    
                                     
    Lv_To   := REPLACE(Lv_To,';',',');
    Lv_CC   := REPLACE(Lv_CC,';',','); 
    Lv_From := REPLACE(Lv_From,';',NULL);
              
   lv_directorio := gvf_obtener_valor_parametro(7399,'DIRECTORIO_REPORTE');
    --
    BEGIN
      EXECUTE IMMEDIATE GVF_OBTENER_VALOR_PARAMETRO(7399,'SQL_PCK_FTP')      
        USING GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_IP_REMOTO'), 
              GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_USER_REMOTO'),
              GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_DIR_REMOTO'),
              LV_ARCHIVO,
              LV_DIRECTORIO,
              'N',
          OUT LN_ERROR,
          OUT LV_ERROR;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 2000);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_salida2;
        END IF;
    END;
    --                                                                                              
    if ln_error <> 0 then
      raise le_salida2;
    end if;                     
    
    LV_ARCHIVO:='</ARCHIVO1='||PV_ARCHIVO||'|DIRECTORIO1='||gvf_obtener_valor_parametro(7399,'FTP_RUTA_REMOTA')||'|/>';     
    -- nueva funcionalidad con plantilla 
    -- Proceso para Envio de Notificaciones     
    -- scp_dat.sck_notificar_gtw.scp_f_notificar                       
    LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  => LV_PROGRAMA,
                                                   PD_FECHAENVIO      => SYSDATE, 
                                                   PD_FECHAEXPIRACION => SYSDATE + 1 / 24,
                                                   PV_ASUNTO          => LV_MAILSUBJECT,
                                                   PV_MENSAJE         => LV_CUERPOCORREO||'//'||LV_ARCHIVO,
												                           PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                   PV_REMITENTE       => LV_FROM,
                                                   PV_TIPOREGISTRO    => lv_notifica,
                                                   PV_CLASE           => 'RAC',
                                                   PV_PUERTO          => LV_PUERTO,
                                                   PV_SERVIDOR        => LV_SERVIDOR,
                                                   PV_MAXINTENTOS     => 2,
                                                   PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                   PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                           
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;

  EXCEPTION
	WHEN le_salida2 THEN
      PV_ERROR := lv_programa || ' - ' || substr(lv_error, 1, 150);
      
    WHEN LE_EXCEPTION THEN
      PV_ERROR := 'Error en Envio: ' || LN_VALORRETORNO || '-' || LV_APLICACION || LV_ERROR;
            
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 100);
          
  END clp_mail_notificacion_final;
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 19/08/2014
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento para obtener los parametros del envio de mails del reporte
  --===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Margarita Trujillo
  -- Created : 09/01/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Ajustes al proceso de notificacion
  --===============================================================================================--  
  PROCEDURE clp_mail_notificacion2(PV_FECHA IN VARCHAR2, 
                                   PV_CIA IN VARCHAR2, --PV_ARCHIVO IN VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS

    LV_APLICACION    VARCHAR2(150) := 'CLP_MAIL_NOTIFICACION2 - ';
    LV_MAILSUBJECT   VARCHAR2(32650);
    LV_CUERPOCORREO  VARCHAR2(6000);
    LV_FROM          VARCHAR2(50);
    LV_TO            VARCHAR2(1000);
    LV_CC            VARCHAR2(1000);
    lv_parametroTO   VARCHAR2(500) := NULL;
    lv_parametroCC   VARCHAR2(500) := NULL;
    LV_USUARIO       VARCHAR2(30):=NULL;
    LV_PUERTO        VARCHAR2(20);  
    LV_SERVIDOR      VARCHAR2(50);  
    LN_VALORRETORNO  NUMBER;            
    LE_EXCEPTION     EXCEPTION;     
    LV_PROGRAMA      VARCHAR2(500):= 'CLK_RECLASIFICACION';
    Lv_Error         VARCHAR2(1000);
    Le_Error         EXCEPTION;

  BEGIN
    -- Validacion de la region
    IF pv_cia IS NULL THEN
        lv_error := 'DEBE INGRESAR LA REGION PARA CONTINUAR';
        RAISE le_exception;
    END IF;
    -- Validacion de la fecha
    IF pv_fecha IS NULL THEN
      lv_error := 'DEBE INGRESAR LA FECHA';
      RAISE le_exception;
    END IF;
    -- Obtiene FROM para envio de mail
    lv_from := gvf_obtener_valor_parametro(7399,'FROM_NAME_SIS');
    
    -- Obtengo los destinatarios de correos dependiendo de la region
      IF pv_cia = '1' THEN
        lv_parametroTO := 'MAIL_TO_R1';
        lv_parametroCC := 'MAIL_CC_R1';
      ELSE
        lv_parametroTO := 'MAIL_TO_R2';
        lv_parametroCC := 'MAIL_CC_R2';
      END IF;
    -- Obtiene TO para envio de mail
    lv_to := gvf_obtener_valor_parametro(7399,lv_parametroTO);
    
    -- Obtiene CC para envio de mail
    lv_cc := gvf_obtener_valor_parametro(7399,lv_parametroCC);  
    
    -- Obtiene Asunto para envio de mail
    LV_MAILSUBJECT := gvf_obtener_valor_parametro(7399,'ASUNTO_MAIL');  
      --
      -- Arma la plantilla para el nuevo componente de notificaciones                  
      LV_CUERPOCORREO := '[[8590]]<<' || PV_FECHA || ',>>';     
                                   
      Lv_To   := REPLACE(Lv_To,';',',');
      Lv_CC   := REPLACE(Lv_CC,';',','); 
      Lv_From := REPLACE(Lv_From,';',NULL);
            
      Lv_MailSubject := REPLACE(Lv_MailSubject,'&','y');    
  
  -- nueva funcionalidad con plantilla 
  -- Proceso para Envio de Notificaciones     
  -- scp_dat.sck_notificar_gtw.scp_f_notificar                       
    LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  => LV_PROGRAMA,
                                                   PD_FECHAENVIO      => SYSDATE, 
                                                   PD_FECHAEXPIRACION => SYSDATE + 1,
                                                   PV_ASUNTO          => LV_MAILSUBJECT,
                                                   PV_MENSAJE         => LV_CUERPOCORREO,--||'//'||PV_ARCHIVO,
                                                   PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                   PV_REMITENTE       => LV_FROM,
                                                   PV_TIPOREGISTRO    => 'M',
                                                   PV_CLASE           => 'RAC',
                                                   PV_PUERTO          => LV_PUERTO,
                                                   PV_SERVIDOR        => LV_SERVIDOR,
                                                   PV_MAXINTENTOS     => 2,
                                                   PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                   PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                           
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;

  EXCEPTION
    WHEN LE_EXCEPTION THEN
      PV_ERROR := 'Error en Envio: ' || LN_VALORRETORNO || '-' || LV_APLICACION || LV_ERROR;
            
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 100);
          
  END clp_mail_notificacion2;
--===============================================================================================--
  -- Project : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Author  : SUD Cristhian Acosta
  -- Created : 12/03/2015
  -- CRM     : SIS Ma. Elizabeth Estevez
  -- Purpose : Procedimiento para obtener el mes de ejecucion de reporte
  --===============================================================================================--
  
FUNCTION gcf_obtiene_mes RETURN VARCHAR2 IS            
  lv_mes varchar2(500);
  BEGIN
  
    
   SELECT DECODE(to_char(sysdate, 'MM'),
                 '01',
                 'Enero',
                 '02',
                 'Febrero',
                 '03',
                 'Marzo',
                 '04',
                 'Abril',
                 '05',
                 'Mayo',
                 '06',
                 'Junio',
                 '07',
                 'Julio',
                 '08',
                 'Agosto',
                 '09',
                 'Septiembre',
                 '10',
                 'Octubre',
                 '11',
                 'Noviembre',
                 '12',
                 'Diciembre',
                 NULL)
     INTO lv_mes
     FROM DUAL;
     
     return lv_mes;
  END;

 -- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento crea asientos de reclasificaci�n -castigo para enviar a sap
  -- ======================================================================================================

  PROCEDURE genera_preasiento_rec_sap(PV_FECHACTUAL   IN VARCHAR2,
                                      PV_FECHANTERIOR IN VARCHAR2,
                                      PD_FECHA        IN DATE,
                                      PV_ERROR        OUT VARCHAR2) IS
  
    CURSOR C_COMPANIAS IS
      SELECT DISTINCT B.COMPANIA, B.AJUSTE
        FROM (SELECT DISTINCT DECODE(N.CIA, 1, 2, 2, 1) COMPANIA, --comento MES para proyecto SAP        
                              N.NO_FACTURA AJUSTE
                FROM CO_CARTERA_CLIENTES N
               WHERE N.ESTADO_CARTERA IS NOT NULL --= 'R'
                 AND N.ESTADO IN ('D') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
                 AND N.PROCESO IN ('P')
                 AND N.PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
                 AND N.NO_FACTURA IS NOT NULL
              UNION
              SELECT DISTINCT TO_NUMBER(N.CIA) COMPANIA, N.NO_FACTURA AJUSTE
                FROM CO_CARTERA_CLIENTES N
               WHERE N.ESTADO_CARTERA IS NOT NULL --= 'C'
                 AND N.ESTADO IN ('V') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
                 AND N.PROCESO IN ('R')
                 AND N.PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
                 AND N.NO_FACTURA IS NOT NULL) B;
  
    CURSOR C_OBTIENE_TOTAL(CN_COMPANIA NUMBER) IS
      SELECT SUM(TOTAL) VALOR
        FROM CO_CARTERA_CLIENTES
       WHERE CIA = CN_COMPANIA
         AND ESTADO_CARTERA = 'R'
         AND ESTADO IN ('D') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
         AND PROCESO = 'P'
         AND PRODUCTO NOT IN ('DTH');--[11188] IRO_HRO 09/03/2017
  
    CURSOR C_OBTIENE_TOTAL2(CN_COMPANIA NUMBER, CN_AJUSTE NUMBER) IS
      SELECT SUM(TOTAL) VALOR
        FROM CO_CARTERA_CLIENTES
       WHERE CIA = CN_COMPANIA
         AND ESTADO_CARTERA = 'C'
         AND ESTADO = 'V' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
         AND PROCESO = 'R'
         AND NO_FACTURA = CN_AJUSTE
         AND PRODUCTO NOT IN ('DTH');--[11188] IRO_HRO 09/03/2017
  
    LV_GLOSA VARCHAR2(200);
    LE_CONTROL_ERROR EXCEPTION;
    LE_ERROR         EXCEPTION;
  
    --
    LV_REFERENCIA VARCHAR2(200);
    LN_VALOR      NUMBER;
    --
    LV_PROCESO VARCHAR2(15) := '[%PROC%]';
    LV_REPLACE VARCHAR2(15);
  
    -- RECLASIFICACION
    LV_CTA_DEBE  VARCHAR2(200);
    LV_CTA_HABER VARCHAR2(200); --
    -- CASTIGO
  
    LN_TOTAL NUMBER;
    LN_CIA   NUMBER;
  
    LV_ERROR VARCHAR2(4000);
  
    LV_CABECERA         VARCHAR2(4000) := NULL;
    LV_CABECERA_CASTIGO VARCHAR2(4000) := NULL;
    LN_LINEA_ASIENTO    NUMBER := 0;
    LV_ASIENTO_1        VARCHAR2(4000) := NULL;
    LV_ASIENTO_2        VARCHAR2(4000) := NULL;
    LV_ASIENTO_CAS_1    VARCHAR2(4000) := NULL;
    LV_ASIENTO_CAS_2    VARCHAR2(4000) := NULL;
    LV_ASIENTO_CAS_3    VARCHAR2(4000) := NULL;
    LV_ASIENTO_CAS_4    VARCHAR2(4000) := NULL;
    LV_TRAMA_ASIENTOS   CLOB := NULL;
    LV_ANIO             VARCHAR2(10) := NULL;
    LV_MES              VARCHAR2(10) := NULL;
    LV_FECHA_ASIENTO    VARCHAR2(50) := NULL;
    LV_ID_POLIZA        VARCHAR2(50) := NULL;
    LV_ID_POLIZA_CAS    VARCHAR2(50) := NULL;
    LV_DIVISION         VARCHAR2(50) := NULL;
    LV_TRAMA            CLOB;
    LV_CLAVE_COSTA      VARCHAR2(50) := NULL;
    LV_CLAVE_SIERRA     VARCHAR2(50) := NULL;
    LV_DESC_POLIZA_COS  VARCHAR2(2000) := NULL;
    LV_DESC_POLIZA_SIE  VARCHAR2(2000) := NULL;
    LD_FECHACTUAL       DATE;
    LD_FECHANTERIOR     DATE;
    LV_FECHACTUAL       VARCHAR2(2000) := NULL;
    LV_FECHANTERIOR     VARCHAR2(2000) := NULL;
    LV_BAN_UP_SAP       VARCHAR2(10) := 'N';
    LV_PROCESO_SAP      VARCHAR2(1000) := NULL;
    LV_DESPACHA_TRAMA   VARCHAR2(10):='N';
  
  BEGIN
  
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''YYYYMMDD''';
    
    LV_DESPACHA_TRAMA:=NVL(GVF_OBTENER_VALOR_PARAMETRO(10172, 'BAND_DESPACHA_TRAMAS'),'N');  
  
    LD_FECHACTUAL   := TO_DATE(PV_FECHACTUAL, 'DD/MM/YYYY');
    LD_FECHANTERIOR := TO_DATE(PV_FECHANTERIOR, 'DD/MM/YYYY');
  
    LV_FECHACTUAL   := TO_CHAR(LD_FECHACTUAL, 'DD/MM/YYYY');
    LV_FECHANTERIOR := TO_CHAR(LD_FECHANTERIOR, 'DD/MM/YYYY');
  
    LV_BAN_UP_SAP := NVL(GVF_OBTENER_VALOR_PARAMETRO(10172, 'BAND_UPD_SAP'),
                         'N');
  
    -- GLOSE DEL ASIENTO CONTABLE 
    LV_GLOSA := GVF_OBTENER_VALOR_PARAMETRO(10172, 'GLOSA');
    IF LV_GLOSA IS NULL THEN
      PV_ERROR := 'GLOSA NO CONFIGURADA.. ';
      RAISE LE_ERROR;
    END IF;
  
    --CUENTA DEUDORA -- Reclasificacion
    LV_CTA_DEBE := GVF_OBTENER_VALOR_PARAMETRO(10172, 'CUENTA_RECLA_DEBE');
    IF LV_CTA_DEBE IS NULL THEN
      PV_ERROR := 'CUENTA DEUDORA NO CONFIGURADA ';
      RAISE LE_ERROR;
    END IF;
  
    LV_ANIO          := TO_CHAR(SYSDATE, 'YYYY');
    LV_MES           := TO_CHAR(SYSDATE, 'MM');
    LV_FECHA_ASIENTO := TO_CHAR(PD_FECHA, 'YYYYMMDD');
  
    LV_DESC_POLIZA_COS := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                          PV_ID_PARAMETRO      => 'DESC_POLIZA_COSTA');
  
    LV_DESC_POLIZA_SIE := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                          PV_ID_PARAMETRO      => 'DESC_POLIZA_SIERRA');
  
    LV_CLAVE_COSTA := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                      PV_ID_PARAMETRO      => 'CLAVE_COSTA');
  
    LV_CLAVE_SIERRA := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                       PV_ID_PARAMETRO      => 'CLAVE_SIERRA');
  
    --ASIENTO POR COMPANIAS
    FOR I IN C_COMPANIAS LOOP
      BEGIN
      
        LV_PROCESO_SAP    := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                         'PROCESO_RECLA');
        LN_LINEA_ASIENTO  := 0;
        LV_TRAMA_ASIENTOS := NULL;
        LV_TRAMA          := NULL;
      
        --crea secuencia de poliza x reclasificaci�n
      
        CLK_RECLASIFICACION.GENERA_SECUENCIA_POLIZA(PV_DOCUMENTO => LV_ID_POLIZA,
                                                    PV_ERROR     => PV_ERROR);
      
        IF PV_ERROR IS NOT NULL THEN
        
          PV_ERROR := SUBSTR(PV_ERROR ||
                             '- ERROR , AL GENERAR SECUENCIA DE POLIZA ' ||
                             SQLERRM,
                             1,
                             1500);
          RAISE LE_CONTROL_ERROR;
        END IF;
      
        --extraemos cabecera de asiento de reclasificacion y reemplazamos valores
      
        LV_CABECERA := GVF_OBTENER_VALOR_PARAMETRO(10172,'PARAM_CABECERA_RECL');
      
        LV_CABECERA := REPLACE(LV_CABECERA, '<%ID_POLIZA%>', LV_ID_POLIZA);
      
        LV_CABECERA := REPLACE(LV_CABECERA, '<%FECHA%>', LV_FECHA_ASIENTO);
      
        LV_CABECERA := REPLACE(LV_CABECERA, '<%MES%>', LV_MES);
      
        LV_CABECERA := REPLACE(LV_CABECERA, '<%ANIO%>', LV_ANIO);
      
        LV_PROCESO := '[%PROC%]';
      
        IF I.COMPANIA IN (2) THEN
        
          LV_CABECERA := REPLACE(LV_CABECERA,
                                 '<%ID_REGION%>',
                                 LV_CLAVE_COSTA);
        
          LV_CABECERA := REPLACE(LV_CABECERA,
                                 '<%DESCR_REGION%>',
                                 LV_DESC_POLIZA_COS);
        
          LV_DIVISION := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                         PV_ID_PARAMETRO      => 'DIVISION_COSTA');
          -- En BSCS, GYE
          --CUENTA ACREEDORA -- Reclasificacion
          LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                      'CUENTA_RECLA_HABER_COS');
          IF LV_CTA_HABER IS NULL THEN
            PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
        
          -- CODIGO DE COMPANIA EN BSCS
          LN_CIA := 1;
        ELSIF I.COMPANIA IN (1) THEN
        
          LV_CABECERA := REPLACE(LV_CABECERA,
                                 '<%ID_REGION%>',
                                 LV_CLAVE_SIERRA);
        
          LV_CABECERA := REPLACE(LV_CABECERA,
                                 '<%DESCR_REGION%>',
                                 LV_DESC_POLIZA_SIE);
        
          LV_DIVISION := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10172,
                                                                         PV_ID_PARAMETRO      => 'DIVISION_SIERRA');
          -- En BSCS, UIO
          --CUENTA ACREEDORA -- Reclasificacion
          LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                      'CUENTA_RECLA_HABER_SIE');
          IF LV_CTA_HABER IS NULL THEN
            PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
            RAISE LE_ERROR;
          END IF;
        
          -- CODIGO DE COMPANIA EN BSCS
          LN_CIA := 2;
        END IF;
      
        -- SE MARCA EL PERIODO DE AJUSTE
        LV_GLOSA := REPLACE(REPLACE(REPLACE(LV_GLOSA,
                                            '[%%FECHANTERIOR%%]',
                                            LV_FECHANTERIOR),
                                    '[%%FECHACTUAL%%]',
                                    LV_FECHACTUAL),
                            '[%FACT%]',
                            I.AJUSTE);
      
        -- SUMARIZA LA CANTIDAD A RECLASIFICAR
        OPEN C_OBTIENE_TOTAL(LN_CIA);
        FETCH C_OBTIENE_TOTAL
          INTO LN_TOTAL;
        IF C_OBTIENE_TOTAL%NOTFOUND THEN
          CLOSE C_OBTIENE_TOTAL;
        ELSIF C_OBTIENE_TOTAL%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
          -- Defino el proceso por el que se hara el primer ajuste
          --  LV_REPLACE    := 'RECLA';
          LV_REPLACE    := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                       'PROC_GLOSA_REC');
          LV_REFERENCIA := REPLACE(LV_GLOSA, LV_PROCESO, LV_REPLACE);
          -- PRIMER AJUSTE -- DEBE
          begin
          
            --crea asiento reclasificacion linea 1
          
            LN_LINEA_ASIENTO := LN_LINEA_ASIENTO + 1;
          
            LV_ASIENTO_1 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                        'PARAM_RECLA_LINEA_1');
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                    '<%NO_LINEA%>',
                                    LN_LINEA_ASIENTO);
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1, '<%MONTO%>', LN_TOTAL);
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                    '<%NO_DOCUMENTO_INTERNO%>',
                                    TO_CHAR(I.AJUSTE));
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                    '<%GLOSA_RECLA%>',
                                    LV_REFERENCIA);
          
            LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                    '<%CUENTA_RECLA_HABER%>',
                                    LV_CTA_HABER);
          
          END;
        
          --FOR J IN C_OBTIENE_CUENTAS(LN_CIA) LOOP
          LN_VALOR := LN_TOTAL;
          -- PRIMER AJUSTE -- HABER
        
          Begin
          
            --crea asiento reclasificacion linea 2
          
            LN_LINEA_ASIENTO := LN_LINEA_ASIENTO + 1;
          
            LV_ASIENTO_2 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                        'PARAM_RECLA_LINEA_2');
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%NO_LINEA%>',
                                    LN_LINEA_ASIENTO);
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2, '<%MONTO%>', LN_VALOR);
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%NO_DOCUMENTO_INTERNO%>',
                                    TO_CHAR(I.AJUSTE));
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%GLOSA_RECLA%>',
                                    LV_REFERENCIA);
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%CUENTA_RECLA_DEBE%>',
                                    LV_CTA_DEBE);
          
            LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                    '<%DIVISION%>',
                                    LV_DIVISION);
          
          END;
          LV_TRAMA_ASIENTOS := LV_TRAMA_ASIENTOS || LV_ASIENTO_1 ||LV_ASIENTO_2;
          LV_TRAMA          := LV_CABECERA || LV_TRAMA_ASIENTOS || ']';
        
          CLK_RECLASIFICACION.GCP_INSERTAR_TRAMA(PV_PROCESO          => LV_ID_POLIZA,--LV_PROCESO_SAP, 
                                                 PC_TRAMA            => LV_TRAMA,
                                                 PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'INGRESO'),--'I',
                                                 PV_ESTADO_REPROCESO => NULL,
                                                 PD_FECHA_INGRESO    => SYSDATE,
                                                 PD_FECHA_DESPACHO   => NULL,
                                                 PD_FECHA_REPROCESO  => NULL,
                                                 PV_COMPANIA         => LN_CIA,
                                                 PN_MONTO            => LN_VALOR,
                                                 PV_MSG_ERROR        => PV_ERROR);
        
          IF PV_ERROR IS NOT NULL THEN
          
            PV_ERROR := SUBSTR(PV_ERROR ||
                               '- ERROR , AL INSERTAR TRAMA DE POLIZA ' ||
                               SQLERRM,
                               1,
                               1500);
            RAISE LE_ERROR;
          END IF;
        
          commit;
        
          LV_PROCESO := LV_REPLACE;
        END IF; -- C_OBTIENE_TOTAL%FOUND
        CLOSE C_OBTIENE_TOTAL;
      
        ---reclasificacion de cartera gca
      
        -- Defino mOdulo y plantilla para segundo ajuste
      
        -- SUMARIZA LA CANTIDAD A CASTIGAR
        OPEN C_OBTIENE_TOTAL2(I.COMPANIA, I.AJUSTE);
        FETCH C_OBTIENE_TOTAL2
          INTO LN_TOTAL;
        IF C_OBTIENE_TOTAL2%NOTFOUND THEN
          CLOSE C_OBTIENE_TOTAL2;
        ELSIF C_OBTIENE_TOTAL2%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
        
          -- Defino el proceso por el que se hara el segundo ajuste
          --LV_REPLACE    := 'CASTIGO';
          LV_REPLACE    := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                       'PROC_GLOSA_CAS');
          LV_REFERENCIA := REPLACE(NVL(LV_REFERENCIA, LV_GLOSA),
                                   LV_PROCESO,
                                   LV_REPLACE);
          --
        
          -- Convierte a negativo el valor obtenido
          LN_VALOR := LN_TOTAL;
        
          --genero secuencia de asiento castigo
          CLK_RECLASIFICACION.GENERA_SECUENCIA_POLIZA_CAS(PV_DOCUMENTO => LV_ID_POLIZA_CAS,
                                                          PV_ERROR     => PV_ERROR);
        
          IF PV_ERROR IS NOT NULL THEN
          
            PV_ERROR := SUBSTR(PV_ERROR ||
                               '- ERROR , AL GENERAR SECUENCIA DE POLIZA ' ||
                               SQLERRM,
                               1,
                               1500);
            RAISE LE_CONTROL_ERROR;
          END IF;
        
          --extraigo cabecera de asiento castigo
        
          LV_CABECERA_CASTIGO := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                             'PARAM_CABECERA_CASTIGO');
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%ID_POLIZA%>',
                                         LV_ID_POLIZA_CAS);
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%FECHA%>',
                                         LV_FECHA_ASIENTO);
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%MES%>',
                                         LV_MES);
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%ANIO%>',
                                         LV_ANIO);
        
          --extraigo asiento 1 castigo  
          LV_ASIENTO_CAS_1 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                          'TRAMA_CASTIGO_LINEA_1');
        
          LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                      '<%FECHA%>',
                                      LV_FECHA_ASIENTO);
        
          LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                      '<%MONTO%>',
                                      LN_VALOR);
        
          LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                      '<%NO_NOTA_INTERNA%>',
                                      TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                      '<%GLOSA%>',
                                      LV_REFERENCIA);
        
          LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                      '<%DIVISION%>',
                                      LV_DIVISION);
        
          --extraigo asiento 2 castigo                                 
          LV_ASIENTO_CAS_2 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                          'TRAMA_CASTIGO_LINEA_2');
        
          LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                      '<%FECHA%>',
                                      LV_FECHA_ASIENTO);
        
          LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                      '<%MONTO%>',
                                      LN_TOTAL);
        
          LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                      '<%NO_NOTA_INTERNA%>',
                                      TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                      '<%GLOSA%>',
                                      LV_REFERENCIA);
        
          --extraigo asiento 3 castigo                                  
          LV_ASIENTO_CAS_3 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                          'TRAMA_CASTIGO_LINEA_3');
        
          LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                      '<%FECHA%>',
                                      LV_FECHA_ASIENTO);
        
          LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                      '<%MONTO%>',
                                      LN_TOTAL);
        
          LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                      '<%NO_NOTA_INTERNA%>',
                                      TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                      '<%GLOSA%>',
                                      LV_REFERENCIA);
          --extraigo asiento 4 castigo                                  
          LV_ASIENTO_CAS_4 := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                          'TRAMA_CASTIGO_LINEA_4');
        
          LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                      '<%FECHA%>',
                                      LV_FECHA_ASIENTO);
        
          LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                      '<%MONTO%>',
                                      ln_valor);
        
          LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                      '<%NO_NOTA_INTERNA%>',
                                      TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                      '<%GLOSA%>',
                                      LV_REFERENCIA);
        
          IF I.COMPANIA IN (2) THEN
          
            LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                           '<%ID_REGION%>',
                                           LV_CLAVE_COSTA);
          
            LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                           '<%REGION%>',
                                           LV_DESC_POLIZA_COS);
          
          ELSE
            IF I.COMPANIA IN (1) THEN
            
              LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                             '<%ID_REGION%>',
                                             LV_CLAVE_SIERRA);
            
              LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                             '<%REGION%>',
                                             LV_DESC_POLIZA_SIE);
            END IF;
          
          END IF;
        
          LV_TRAMA_ASIENTOS := NULL;
          LV_TRAMA          := NULL; 
          LV_TRAMA_ASIENTOS := LV_TRAMA_ASIENTOS || LV_ASIENTO_CAS_1 ||LV_ASIENTO_CAS_2 || LV_ASIENTO_CAS_3 ||LV_ASIENTO_CAS_4;
          LV_TRAMA          := LV_CABECERA_CASTIGO || LV_TRAMA_ASIENTOS || ']';
        
          LV_PROCESO_SAP := GVF_OBTENER_VALOR_PARAMETRO(10172,
                                                        'PROCESO_CAS');
        
          CLK_RECLASIFICACION.GCP_INSERTAR_TRAMA(PV_PROCESO          => LV_ID_POLIZA_CAS,--LV_PROCESO_SAP,
                                                 PC_TRAMA            => LV_TRAMA,
                                                 PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'INGRESO'),--'I',
                                                 PV_ESTADO_REPROCESO => NULL,
                                                 PD_FECHA_INGRESO    => SYSDATE,
                                                 PD_FECHA_DESPACHO   => NULL,
                                                 PD_FECHA_REPROCESO  => NULL,
                                                 PV_COMPANIA         => LN_CIA,--I.COMPANIA,   
                                                 PN_MONTO            => LN_VALOR,                                              
                                                 PV_MSG_ERROR        => PV_ERROR);
        
          IF PV_ERROR IS NOT NULL THEN
          
            PV_ERROR := SUBSTR(PV_ERROR ||
                               '- ERROR , AL INSERTAR TRAMA DE POLIZA ' ||
                               SQLERRM,
                               1,
                               1500);
            RAISE LE_ERROR;
          END IF;
        
          commit;
        
        END IF; -- C_OBTIENE_TOTAL2%FOUND
        CLOSE C_OBTIENE_TOTAL2;
      
        --END LOOP; --C_OBTIENE_CUENTAS
      
        --10172 ACTUALIZACION DE COMENTARIO Q CREO ASIENTO EN SAP
        UPDATE CO_CARTERA_CLIENTES T
           SET COMENTARIO = SUBSTR(T.COMENTARIO, 1, 1800) || '- SAP'
         WHERE NO_FACTURA = I.AJUSTE
              --AND ESTADO IN ('V', 'D');
           AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
           AND ESTADO IN ('D')
           AND CIA = LN_CIA;
      
        UPDATE CO_CARTERA_CLIENTES T
           SET COMENTARIO = SUBSTR(T.COMENTARIO, 1, 1800) || '- SAP'
         WHERE NO_FACTURA = I.AJUSTE
           AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
           AND ESTADO IN ('V')
           AND CIA = I.COMPANIA;
      
        --10172 SE ACTIVA SOLO SI EL PROCESO CREA ASIENTO EN SAP 
        IF LV_BAN_UP_SAP = 'S' THEN
          UPDATE CO_CARTERA_CLIENTES
             SET ESTADO = 'T'
           WHERE NO_FACTURA = I.AJUSTE
                --AND ESTADO IN ('V', 'D');
             AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
             AND ESTADO IN ('D')
             AND CIA = LN_CIA;
        
          UPDATE CO_CARTERA_CLIENTES
             SET ESTADO = 'T'
           WHERE NO_FACTURA = I.AJUSTE
             AND PRODUCTO NOT IN ('DTH')--[11188] IRO_HRO 09/03/2017
             AND ESTADO IN ('V')
             AND CIA = I.COMPANIA;
        END IF;
      
        COMMIT;
      EXCEPTION
        WHEN LE_CONTROL_ERROR THEN
          ROLLBACK;
          UPDATE CO_CARTERA_CLIENTES
             SET ESTADO = 'E', COMENTARIO = LV_ERROR || COMENTARIO
           WHERE /*CIA = LN_CIA AND*/
           NO_FACTURA = I.AJUSTE;
          COMMIT;
          RAISE LE_ERROR;
      END;
    END LOOP; --C_COMPANIAS
    
    IF LV_DESPACHA_TRAMA = 'S' THEN
  
      CLK_RECLASIFICACION.GCP_ENVIA_TRAMAS(PV_ERROR => PV_ERROR);
      
       IF PV_ERROR IS NOT NULL THEN
              
        PV_ERROR := SUBSTR(PV_ERROR ||
                           '- ERROR , AL GENERAR DESPACHAR TRAMAS SAP ' ||
                           SQLERRM,
                           1,
                           1500);
        RAISE LE_ERROR;
      END IF;  
    END IF;  
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR :=  SUBSTR(PV_ERROR || ' - ' || SQLERRM, 1, 1000);
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
    
  END genera_preasiento_rec_sap;
-- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento genera la secuencia de polizas reclasificacion
  -- ======================================================================================================
  
PROCEDURE GENERA_SECUENCIA_POLIZA(PV_DOCUMENTO OUT VARCHAR2,
                               PV_ERROR     OUT VARCHAR2) IS
  
    CURSOR C_ULTIMO_DIA IS
      SELECT VALOR
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = 10172
         AND ID_PARAMETRO = 'ULTIMO_DD';
  
    LV_SECUENCIA_POLIZA  VARCHAR2(200);
    LV_DIA_ACTUAL VARCHAR2(2);
    LV_DIA_PASADO VARCHAR2(2);
  
  BEGIN
    SELECT TO_CHAR(SYSDATE, 'DD') INTO LV_DIA_ACTUAL FROM DUAL;
  
    OPEN C_ULTIMO_DIA;
    FETCH C_ULTIMO_DIA
      INTO LV_DIA_PASADO;
    CLOSE C_ULTIMO_DIA;
  
    IF LV_DIA_ACTUAL != LV_DIA_PASADO THEN
      UPDATE GV_PARAMETROS
         SET VALOR = LV_DIA_ACTUAL
       WHERE ID_TIPO_PARAMETRO = 10172
         AND ID_PARAMETRO = 'ULTIMO_DD';
    
      CLK_RECLASIFICACION.reiniciar_secuencia('SEQ_ID_POLIZA_RECLA_SAP'); 
    END IF;
  
          --aqui modifique, no es BSCS, es solo BS
    SELECT 'BS'||TRIM(TO_CHAR(SYSDATE, 'YYMMDD')) ||
           TRIM(TO_CHAR(SEQ_ID_POLIZA_RECLA_SAP.NEXTVAL, '09'))
          INTO LV_SECUENCIA_POLIZA
      FROM DUAL;  
 
    PV_DOCUMENTO := LV_SECUENCIA_POLIZA;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR('GENERA_SECUENCIA_POLIZA' || SQLERRM, 1, 1500);
  END GENERA_SECUENCIA_POLIZA; 
-- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento reinicia la secuencia de polizas reclasificacion
  -- ====================================================================================================== 
  
procedure reiniciar_secuencia (p_nombre varchar2  )is
    res number;
begin
    execute immediate
    'select ' || p_nombre || '.nextval from dual' INTO res;

    execute immediate
    'alter sequence ' || p_nombre || ' increment by -' || res ||
                                                          ' minvalue 0';

    execute immediate
    'select ' || p_nombre || '.nextval from dual' INTO res;

    execute immediate
    'alter sequence ' || p_nombre || ' increment by 1 minvalue 0';
end;
-- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento reinicia la secuencia de polizas castigo
  -- ====================================================================================================== 

procedure reiniciar_secuencia_cas (p_nombre varchar2  )is
    res number;
    res1 number;
begin
    execute immediate
    'select ' || p_nombre || '.nextval from dual' INTO res;    
    
    res1:=99-res;

    execute immediate
    'alter sequence ' || p_nombre || ' increment by ' || res1 ||
                                                          ' minvalue 0';                                                                                                            

    execute immediate
    'select ' || p_nombre || '.nextval from dual' INTO res;

    execute immediate
    'alter sequence ' || p_nombre || ' increment by -1 minvalue 0';
end;
-- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento genera la secuencia de polizas castigo
  -- ====================================================================================================== 
PROCEDURE GENERA_SECUENCIA_POLIZA_CAS(PV_DOCUMENTO OUT VARCHAR2,
                               PV_ERROR     OUT VARCHAR2) IS
  
    CURSOR C_ULTIMO_DIA IS
      SELECT VALOR
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = 10172
         AND ID_PARAMETRO = 'ULTIMO_DD_CAS';
  
    LV_SECUENCIA_POLIZA  VARCHAR2(200);
    LV_DIA_ACTUAL VARCHAR2(2);
    LV_DIA_PASADO VARCHAR2(2);
  
  BEGIN
    SELECT TO_CHAR(SYSDATE, 'DD') INTO LV_DIA_ACTUAL FROM DUAL;
  
    OPEN C_ULTIMO_DIA;
    FETCH C_ULTIMO_DIA
      INTO LV_DIA_PASADO;
    CLOSE C_ULTIMO_DIA;
  
    IF LV_DIA_ACTUAL != LV_DIA_PASADO THEN
      UPDATE GV_PARAMETROS
         SET VALOR = LV_DIA_ACTUAL
       WHERE ID_TIPO_PARAMETRO = 10172
         AND ID_PARAMETRO = 'ULTIMO_DD_CAS';
    
      CLK_RECLASIFICACION.REINICIAR_SECUENCIA_CAS('SEQ_ID_POLIZA_CASTIGO_SAP'); 
    END IF;
  
          
    SELECT 'CA'||TRIM(TO_CHAR(SYSDATE, 'YYMMDD')) ||
           TRIM(TO_CHAR(SEQ_ID_POLIZA_CASTIGO_SAP.NEXTVAL, '09'))
          INTO LV_SECUENCIA_POLIZA
      FROM DUAL;  
 
    PV_DOCUMENTO := LV_SECUENCIA_POLIZA;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR('GENERA_SECUENCIA_POLIZA_CAS' || SQLERRM, 1, 1500);
  END GENERA_SECUENCIA_POLIZA_CAS;
  
  -- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento despacha trama de asientos a sap
  -- ======================================================================================================
  
  PROCEDURE GCP_DESPACHA_TRAMA_SAP(PV_TRAMA IN CLOB,
                                   PV_ERROR OUT VARCHAR2) IS
                                     
    lv_XML_trama           clob;
    lx_response            Sys.Xmltype;
    ln_id_req              number;
    le_error               EXCEPTION;
    lv_fault_code          varchar2(2000);
    lv_fault_string        varchar2(2000);
    LV_RESP_EXITO          varchar2(2000);
    lv_id_tipo_transaccion varchar2(100);
  
  BEGIN
    
    LN_ID_REQ := TO_NUMBER(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'ID_REQUERIMIENTO')); 
    lv_id_tipo_transaccion:=CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'ID_TRANSACCION');
    
    lv_XML_trama :='Transaccion='||lv_id_tipo_transaccion||'|CadenaParametros='||PV_TRAMA||'|';
                                       
    --invocamos el consumo de webServices
    lx_response := Scp_Dat.Sck_Soap_Gtw.Scp_Consume_Servicio(ln_id_req,
                                                             lv_XML_trama,
                                                             lv_fault_code,
                                                             lv_fault_string);
                                                             
    if lv_fault_string is not null then
      PV_ERROR := 'Error en consumo de webServices ' || lv_fault_string ||
                  ' - ' || lv_fault_code;
      raise le_error;
    end if;
     
    PV_ERROR := scp_dat.sck_soap_gtw.scp_obtener_valor(ln_id_req,
                                                       lx_response,
                                                       'MENSAJE');  
                                                          
                                                                                                                                                                                                                                                                                                                     
    IF PV_ERROR IS NOT NULL THEN 
      RAISE LE_ERROR;
    END IF;   
    
    LV_RESP_EXITO := scp_dat.sck_soap_gtw.scp_obtener_valor(ln_id_req,
                                                           lx_response,
                                                           'Resultado');   
                                                           
    IF LV_RESP_EXITO <>'EXITO' THEN 
      PV_ERROR:=LV_RESP_EXITO;
      RAISE LE_ERROR;
    END IF;                                                                                                             
    --
    -- Compuebo que no haya un Fault en el servicio
    If Lv_Fault_Code Is Not Null Or Lv_Fault_String Is Not Null Then
      -- Lanzo la excepcion asociada al Fault en el servicio
      PV_ERROR := Substr(': Fault en servicio web. ID_REQUERIMIENTO=' || Ln_Id_Req || 'PARAMETROS=' || lv_XML_trama, 1, 1000);
      PV_ERROR := PV_ERROR||' - '||Substr(Lv_Fault_Code  || ' - ' || Lv_Fault_String, 1, 1000);
      Raise Le_Error;
         
    Else
      -- Comprobamos que la respuesta no sea nula, de lo contrario lanza exception
      If lx_response Is Null Then
        PV_ERROR := 'Error al llamar paquete CLK_RECLASIFICACION: ' || PV_ERROR;
        Raise Le_Error;
      End If;
    End If;
               
  EXCEPTION
    WHEN le_error THEN
    ROLLBACK;
      PV_ERROR := SUBSTR(PV_ERROR, 1, 1000);
      
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'Error al despachar hacia SAP - ' ||  SUBSTR(SQLERRM, 1, 200);
  END GCP_DESPACHA_TRAMA_SAP;
  -- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento crea inserta trama asientos para enviar a sap
  -- ======================================================================================================
  
  PROCEDURE GCP_INSERTAR_TRAMA(PV_PROCESO          IN VARCHAR2,
                               PC_TRAMA            IN CLOB,
                               PV_ESTADO_DESPACHO  IN VARCHAR2 DEFAULT NULL,
                               PV_ESTADO_REPROCESO IN VARCHAR2 DEFAULT NULL,
                               PD_FECHA_INGRESO    IN DATE DEFAULT NULL,
                               PD_FECHA_DESPACHO   IN DATE DEFAULT NULL,
                               PD_FECHA_REPROCESO  IN DATE DEFAULT NULL,
                               PV_COMPANIA         IN VARCHAR2,
                               PN_MONTO            IN NUMBER,
                               PV_MSG_ERROR        OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(100) := 'CLK_RECLASIFICACION.GCP_INSERTAR_TRAMA';
    LN_ID_TRAMA   NUMBER := 0;
  BEGIN
  
    PV_MSG_ERROR := NULL;
    SELECT NVL(MAX(ID_TRAMA), 0) + 1
      INTO LN_ID_TRAMA
      FROM TB_SAP_TRAMAS_REC_CAS;
  
    INSERT INTO TB_SAP_TRAMAS_REC_CAS T
      (ID_TRAMA,
       PROCESO,
       TRAMA,
       ESTADO_DESPACHO,
       ESTADO_REPROCESO,
       FECHA_INGRESO,
       FECHA_DESPACHO,
       FECHA_REPROCESO,
       COMPANIA,
       MONTO_TOTAL)
    VALUES
      (LN_ID_TRAMA,
       PV_PROCESO,
       PC_TRAMA,
       PV_ESTADO_DESPACHO,
       PV_ESTADO_REPROCESO,
       PD_FECHA_INGRESO,
       PD_FECHA_DESPACHO,
       PD_FECHA_REPROCESO,
       PV_COMPANIA,
       PN_MONTO);
  
  EXCEPTION
    -- Exception
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error al insertar ' ||
                      SUBSTR(SQLERRM, 1, 200) || '. ' || LV_APLICACION;
  END GCP_INSERTAR_TRAMA;
  
  -- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento actualiza informaci�n de la trama que se envia a sap
  -- ======================================================================================================  
  
  PROCEDURE GCP_ACTUALIZAR_TRAMA(PN_ID_TRAMA         IN NUMBER,
                                 PV_PROCESO          IN VARCHAR2,
                                 PC_TRAMA            IN CLOB DEFAULT NULL,
                                 PV_ESTADO_DESPACHO  IN VARCHAR2 DEFAULT NULL,
                                 PV_ESTADO_REPROCESO IN VARCHAR2 DEFAULT NULL,
                                 PD_FECHA_DESPACHO   IN DATE DEFAULT NULL,
                                 PD_FECHA_REPROCESO  IN DATE DEFAULT NULL,
                                 PV_COMENTARIO       IN VARCHAR2 DEFAULT NULL,
                                 PV_COMPANIA         IN VARCHAR2,
                                 PV_MSG_ERROR        OUT VARCHAR2) AS
 
    Le_NoDataUpdated EXCEPTION;
  
    LV_APLICACION VARCHAR2(100) := 'CLK_RECLASIFICACION.GCP_ACTUALIZAR_TRAMA';
  BEGIN
  
    PV_MSG_ERROR := NULL;
   
  
    UPDATE TB_SAP_TRAMAS_REC_CAS T
       SET
           T.TRAMA            = NVL(PC_TRAMA, T.TRAMA),
           T.ESTADO_DESPACHO  = NVL(PV_ESTADO_DESPACHO, T.ESTADO_DESPACHO),
           T.ESTADO_REPROCESO = NVL(PV_ESTADO_REPROCESO, T.ESTADO_REPROCESO),
           T.FECHA_DESPACHO   = NVL(PD_FECHA_DESPACHO, T.FECHA_DESPACHO),
           T.FECHA_REPROCESO  = NVL(PD_FECHA_REPROCESO, T.FECHA_REPROCESO),
           T.COMENTARIO       = SUBSTR(T.COMENTARIO || ' ' || PV_COMENTARIO,
                                       1,
                                       2900)
     WHERE T.ID_TRAMA = PN_ID_TRAMA
       AND T.COMPANIA = PV_COMPANIA
       AND T.PROCESO  = PV_PROCESO;
  
  
  EXCEPTION
    -- Exception
    WHEN Le_NoDataUpdated THEN
      PV_MSG_ERROR := 'No se encontro el registro que desea actualizar. ' ||
                      SUBSTR(SQLERRM, 1, 200) || '. ' || LV_APLICACION;
    
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error al actualizar la trama ' ||
                      SUBSTR(SQLERRM, 1, 200) || '. ' || LV_APLICACION;
    
  END GCP_ACTUALIZAR_TRAMA;
-- ======================================================================================================
  -- Creado  por:     SUD Gina Cabezas Andrade
  -- Lider Claro:     SIS Ma.Elizabeth Estevez
  -- Lider PDS:       SUD Cristhian Acosta Ch.
  -- Fecha de Creacion: 19/02/2015
  -- Proyecto: [10172]  Interfase SAP PeopleSoft ReclasificacionCastigoCondonacion
  -- Descripcion :Este procedimiento envia a despachar las tramas con estado I o R a sap
  -- ======================================================================================================  
    
  PROCEDURE GCP_ENVIA_TRAMAS(PV_ERROR OUT VARCHAR2) IS
    CURSOR C_DESPACHA_TRAMA IS
      SELECT *
        FROM TB_SAP_TRAMAS_REC_CAS T
       WHERE T.ESTADO_DESPACHO = NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'INGRESO'),'I')
          OR T.ESTADO_REPROCESO = NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'REPROCESO'),'R');
  
    LE_ERROR EXCEPTION;
    LV_ERROR      VARCHAR2(6000);
    LV_COMENTARIO VARCHAR2(50) := 'TRAMA DESPACHADA CON EXITO. ';
  
    LV_APLICACION VARCHAR2(100) := 'CLK_RECLASIFICACION.GCP_ENVIA_TRAMAS';
  BEGIN
  
    FOR I IN C_DESPACHA_TRAMA LOOP
    
      CLK_RECLASIFICACION.GCP_DESPACHA_TRAMA_SAP(PV_TRAMA => I.TRAMA,
                                                 PV_ERROR => LV_ERROR);
    
      --Si el proceso de despacho devuelve un error actualiza la trama con error,estado E y el comentario de error                                                
      IF LV_ERROR IS NOT NULL THEN
      
        CLK_RECLASIFICACION.GCP_ACTUALIZAR_TRAMA(PN_ID_TRAMA         => I.ID_TRAMA,
                                                 PV_PROCESO          => I.PROCESO,
                                                 PC_TRAMA            => I.TRAMA,
                                                 PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'ERROR'),--'R',
                                                 PV_ESTADO_REPROCESO => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'ERROR'),--'R',
                                                 PD_FECHA_DESPACHO   => SYSDATE,
                                                 PD_FECHA_REPROCESO  => NULL,
                                                 PV_COMENTARIO       => LV_ERROR,
                                                 PV_COMPANIA         => I.COMPANIA,
                                                 PV_MSG_ERROR        => PV_ERROR);
        IF PV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
        END IF;
      
      ELSE
        --si el proceso de despacho esta correcto actualizamos los estados a D despachado y fecha de despacho
        IF I.ESTADO_DESPACHO = NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'INGRESO'),'I') AND I.ESTADO_REPROCESO IS NULL THEN
          CLK_RECLASIFICACION.GCP_ACTUALIZAR_TRAMA(PN_ID_TRAMA         => I.ID_TRAMA,
                                                   PV_PROCESO          => I.PROCESO,
                                                   PC_TRAMA            => I.TRAMA,
                                                   PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'DESPACHO'),--'D',
                                                   PV_ESTADO_REPROCESO => NULL,
                                                   PD_FECHA_DESPACHO   => SYSDATE,
                                                   PD_FECHA_REPROCESO  => NULL,
                                                   PV_COMENTARIO       => LV_COMENTARIO,
                                                   PV_COMPANIA         => I.COMPANIA,                                                   
                                                   PV_MSG_ERROR        => PV_ERROR);
          IF PV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        
        ELSE
          IF I.ESTADO_REPROCESO = NVL(CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'REPROCESO'),'R') THEN
            CLK_RECLASIFICACION.GCP_ACTUALIZAR_TRAMA(PN_ID_TRAMA         => I.ID_TRAMA,
                                                     PV_PROCESO          => I.PROCESO,
                                                     PC_TRAMA            => I.TRAMA,
                                                     PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'DESPACHO'),--'D',
                                                     PV_ESTADO_REPROCESO => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(10172,'DESPACHO'),--'D',
                                                     PD_FECHA_DESPACHO   => SYSDATE,
                                                     PD_FECHA_REPROCESO  => SYSDATE,
                                                     PV_COMENTARIO       => LV_COMENTARIO,
                                                     PV_COMPANIA         => I.COMPANIA,                                                     
                                                     PV_MSG_ERROR        => PV_ERROR);
            IF PV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
            END IF;
          END IF;
        END IF;
      END IF;
    
      COMMIT;
    
    END LOOP;
  
  EXCEPTION
    WHEN LE_ERROR THEN
    
      PV_ERROR := SUBSTR(PV_ERROR || ' - ' || SQLERRM, 1, 1000);
    
    WHEN OTHERS THEN
      PV_ERROR := 'Ocurrio el siguiente error ' || SUBSTR(SQLERRM, 1, 200) || '. ' ||
                  LV_APLICACION;
    
  END GCP_ENVIA_TRAMAS;
  --===============================================================================================--  
  --  PROYECTO     : [7399] Re-clasificacion Automatica de Cartera
  -- LIDER SUD		 : SUD Cristhian Acosta
  -- LIDER CLARO   : SIS Ma. Elizabeth Estevez
  -- DESARROLLADOR : SUD Cristhian Acosta
  -- FECHA         : 25/06/2015
  -- COMENTARIO    : Ajustes a envios de Notificaciones frameworks de arquitectura
  --===============================================================================================--   
 PROCEDURE genera_reporte_excel(pv_directorio in varchar2,
                                 pv_file       in varchar2,
                                 pv_query      in varchar2) is
                            
     --Inicio - Generaci�n del reporte
      PROCEDURE run_query(p_sql IN VARCHAR2) IS
        v_v_val     VARCHAR2(4000);
        v_n_val     NUMBER;
        v_d_val     DATE;
        v_ret       NUMBER;
        c           NUMBER;
        d           NUMBER;
        col_cnt     INTEGER;
        f           BOOLEAN;
        rec_tab     DBMS_SQL.DESC_TAB;
        col_num     NUMBER;
        
      BEGIN
        c := DBMS_SQL.OPEN_CURSOR;
        -- parse the SQL statement
        DBMS_SQL.PARSE(c, p_sql, DBMS_SQL.NATIVE);
        -- start execution of the SQL statement
        d := DBMS_SQL.EXECUTE(c);
        -- get a description of the returned columns
        DBMS_SQL.DESCRIBE_COLUMNS(c, col_cnt, rec_tab);
        -- bind variables to columns
        UTL_FILE.PUT_LINE(v_fh,'<tr>');
        FOR j in 1..col_cnt
        LOOP
          CASE rec_tab(j).col_type
            WHEN 1 THEN DBMS_SQL.DEFINE_COLUMN(c,j,v_v_val,4000);
            WHEN 2 THEN DBMS_SQL.DEFINE_COLUMN(c,j,v_n_val);
            WHEN 12 THEN DBMS_SQL.DEFINE_COLUMN(c,j,v_d_val);
          ELSE
            DBMS_SQL.DEFINE_COLUMN(c,j,v_v_val,4000);
          END CASE;
        END LOOP;
        -- Output the column headers
        FOR j in 1..col_cnt
         LOOP 
          UTL_FILE.PUT_LINE(v_fh,'<td width="120" align="center" bgcolor="#A4A4A4">');
          UTL_FILE.PUT_LINE(v_fh,'<B>');    
          UTL_FILE.PUT_LINE(v_fh,'<Type="String" font size="2" color="black" face="Book Antiqua" >'||rec_tab(j).col_name||'</font>');
          UTL_FILE.PUT_LINE(v_fh,'</Type>'); 
          UTL_FILE.PUT_LINE(v_fh,'</B>');       
          UTL_FILE.PUT_LINE(v_fh,'</td>');      
        END LOOP;
         UTL_FILE.PUT_LINE(v_fh,'</tr>');
        -- Output the data
       LOOP
          v_ret := DBMS_SQL.FETCH_ROWS(c);
          EXIT WHEN v_ret = 0;
         UTL_FILE.PUT_LINE(v_fh,'<tr>');
          FOR j in 1..col_cnt
          LOOP
            CASE rec_tab(j).col_type
              WHEN 1 THEN DBMS_SQL.COLUMN_VALUE(c,j,v_v_val);
                          UTL_FILE.PUT_LINE(v_fh,'<td width="120" align="center">');  
                          UTL_FILE.PUT_LINE(v_fh,'<Type="String" font   size="0"   color="black"   face="Book Antiqua" >'||v_v_val||'</font>');
                          UTL_FILE.PUT_LINE(v_fh,'</td>');
              WHEN 2 THEN DBMS_SQL.COLUMN_VALUE(c,j,v_n_val);
                          UTL_FILE.PUT_LINE(v_fh,'<td width="90" align="center" >');  
                          UTL_FILE.PUT_LINE(v_fh,'<Type="Number"   font   size="2"   color="black"   face="Book Antiqua" >'||to_char(v_n_val)||'</:font>');
                          UTL_FILE.PUT_LINE(v_fh,'</td>');
              WHEN 12 THEN DBMS_SQL.COLUMN_VALUE(c,j,v_d_val);
                          UTL_FILE.PUT_LINE(v_fh,'<td width="90" align="center">');    
                          UTL_FILE.PUT_LINE(v_fh,'<Type="DateTime"   font   size="2"   color="black"   face="Book Antiqua" >'||to_char(v_d_val,'YYYY-MM-DD"T"HH24:MI:SS')||'</font>');
                          UTL_FILE.PUT_LINE(v_fh,'</td>');
            ELSE
              DBMS_SQL.COLUMN_VALUE(c,j,v_v_val);
                          UTL_FILE.PUT_LINE(v_fh,'<td width="120" align="center">');  
                          UTL_FILE.PUT_LINE(v_fh,'<Type="String"   font   size="0"   color="black"   face="Book Antiqua" >'||v_v_val||'</font>');
                          UTL_FILE.PUT_LINE(v_fh,'</td>');
            END CASE;
          END LOOP;
            UTL_FILE.PUT_LINE(v_fh,'</tr>');      
        END LOOP;
        DBMS_SQL.CLOSE_CURSOR(c);
      END;
      --
      PROCEDURE start_workbook IS
      --  v_fh        UTL_FILE.FILE_TYPE;
      BEGIN
        UTL_FILE.PUT_LINE(v_fh,'<Html>');
        UTL_FILE.PUT_LINE(v_fh,'<Head>');  
        UTL_FILE.PUT_LINE(v_fh,'</head> <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="APPLICATION/VND.MS-EXCEL">');
        
        END;
      PROCEDURE end_workbook IS
        BEGIN
            UTL_FILE.PUT_LINE(v_fh,'</Html>');
        END;
      --
      PROCEDURE start_worksheet IS
        BEGIN
            UTL_FILE.PUT_LINE(v_fh,'<body>');
            UTL_FILE.PUT_LINE(v_fh,'<Table border="1">');
        END;
      PROCEDURE end_worksheet IS
        BEGIN        
            UTL_FILE.PUT_LINE(v_fh,'</Table>');
            UTL_FILE.PUT_LINE(v_fh,'</body>');
        END;
      --
      PROCEDURE set_date_style IS
        BEGIN 

            UTL_FILE.PUT_LINE(v_fh,'<tr><font NumberFormat   Format="dd/mm/yyyy\ hh:mm:ss"/</font></tr>');
        
        END;                                   
     --Fin - Generaci�n del reporte                       
  BEGIN
  --
        V_FH := UTL_FILE.FOPEN(UPPER(PV_DIRECTORIO),PV_FILE,'w',32767);
        START_WORKBOOK;
        SET_DATE_STYLE;
        START_WORKSHEET;
        RUN_QUERY(PV_QUERY);        
        END_WORKSHEET; 
        END_WORKBOOK;
        UTL_FILE.FCLOSE(V_FH);
                
        BEGIN
          --PERMISOS A LOS ARCHIVOS QUE SE CREAN
          EXECUTE IMMEDIATE CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(7399,'PERMISO_ARCHIVO')
             USING IN PV_DIRECTORIO, IN PV_FILE;
             
          EXCEPTION
            WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE(SUBSTR('PROBLEMA PERMISOS: '||sqlerrm,1,120));          
        END;
  --        
   END;
 --===============================================================================================--
  -- Project   : [7399] AUTOMATIZACION DE RECLASIFICACION DE CARTERA
  -- Modificado: Silvana Pl�as
  -- Fecha     : 19/06/2015
  -- CRM       : SIS Ma. Elizabeth Estevez
  -- Purpose   : Procedimiento para obtener los parametros del envio de mails del reporte
  --             Ajustes
  --===============================================================================================--   
  PROCEDURE clp_mail_notifica_pagos(PV_FECHA IN VARCHAR2, 
                                      PV_CIA IN VARCHAR2, 
                                      PV_ARCHIVO IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2) IS

    LV_APLICACION    VARCHAR2(150) := 'CLP_MAIL_NOTIFICA_PAGOS - ';
    LV_MAILSUBJECT   VARCHAR2(32650);
    LV_CUERPOCORREO  VARCHAR2(6000);
    LV_FROM          VARCHAR2(50);
    LV_TO            VARCHAR2(1000);
    LV_CC            VARCHAR2(1000);
    lv_parametroTO   VARCHAR2(500) := NULL;
    lv_parametroCC   VARCHAR2(500) := NULL;
    LV_USUARIO       VARCHAR2(30):=NULL;
    LV_PUERTO        VARCHAR2(20);  
    LV_SERVIDOR      VARCHAR2(50);  
    LN_VALORRETORNO  NUMBER;            
    LE_EXCEPTION     EXCEPTION;     
    LV_PROGRAMA      VARCHAR2(500):= 'CLK_RECLASIFICACION'; --SUD MTR
    Lv_Error         VARCHAR2(1000);
    ln_error         number:=null;
    Le_Error         EXCEPTION;
    lv_mes           VARCHAR2(1000);
    lv_anio           varchar2(100);   
    lv_plantilla       varchar2(200);
    Pv_ARCHIVO1       VARCHAR2(1000);
    --
    lv_archivo       VARCHAR2(100);
    lv_directorio    VARCHAR2(500) := NULL;
    le_salida2       EXCEPTION;
    ---
    lv_notifica VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'NOTIFICA_TIPO');
    LV_MAIL_HOST VARCHAR2(50):= clk_reclasificacion.gvf_obtener_valor_parametro(7399,'MAIL_HOST'); 
    LV_MENSAGE  VARCHAR2(3000):=null;

  BEGIN  
    -- Validacion de la region
    IF pv_cia IS NULL THEN
        lv_error := 'DEBE INGRESAR LA REGION PARA CONTINUAR';
        RAISE le_exception;
    END IF;
    -- Validacion de la fecha
    IF pv_fecha IS NULL THEN
      lv_error := 'DEBE INGRESAR LA FECHA';
      RAISE le_exception;
    END IF;
    -- Obtiene FROM para envio de mail
    lv_from := gvf_obtener_valor_parametro(7399,'FROM_NAME_SIS');
    lv_plantilla:= gvf_obtener_valor_parametro(7399,'PLANTILLA_REP_CTS_PAGO');
    
    -- Obtengo los destinatarios de correos dependiendo de la region
      IF pv_cia = '1' THEN
        lv_parametroTO := 'MAIL_TO_R1';
        lv_parametroCC := 'MAIL_CC_R1';
      ELSE
        lv_parametroTO := 'MAIL_TO_R2';
        lv_parametroCC := 'MAIL_CC_R2';
      END IF;
    -- Obtiene TO para envio de mail
    lv_to := gvf_obtener_valor_parametro(7399,lv_parametroTO);
    
    -- Obtiene CC para envio de mail
    lv_cc := gvf_obtener_valor_parametro(7399,lv_parametroCC);  
    
    -- Obtiene Asunto para envio de mail
    LV_MAILSUBJECT := gvf_obtener_valor_parametro(7399,'ASUNTO_MAIL_PAGOS');   
      --
    lv_mes:=clk_reclasificacion.gcf_obtiene_mes;    
    --lv_anio:=to_char(to_date(pv_fecha,'yyyy/mm/dd'),'yyyy');  
    lv_anio:=to_char(to_date(pv_fecha,'dd/mm/yyyy'),'yyyy');
    -- Arma la plantilla para el nuevo componente de notificaciones                  
    --LV_CUERPOCORREO := '[[80016]]<<' || lv_mes ||'- '||lv_anio|| ',>>';   
    LV_CUERPOCORREO := '[['||lv_plantilla||']]<<' || lv_mes ||'- '||lv_anio|| ',>>';                                           
              
    Lv_MailSubject := REPLACE(Lv_MailSubject,'&','y');  
    
    IF NVL(gvf_obtener_valor_parametro(7399,'ENVIA_CORREO_FTP'),'N') = 'N'  THEN
      --Indica que se va enviar utilizando .JAR de envio de correos
      DBMS_OUTPUT.PUT_LINE('MAIL_HOST_R'||pv_cia||':'||LV_MAIL_HOST);
      DBMS_OUTPUT.PUT_LINE('REMITENTE_R'||pv_cia||':'||Lv_From);
      DBMS_OUTPUT.PUT_LINE('MAIL_TO_R'||pv_cia||':'||Lv_To);
      DBMS_OUTPUT.PUT_LINE('MAIL_CC_R'||pv_cia||':'||Lv_CC);
      DBMS_OUTPUT.PUT_LINE('MAIL_SUBJECT_R'||pv_cia||':'||LV_MAILSUBJECT);
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_1:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_1'));
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_2');
      LV_MENSAGE:=REPLACE(LV_MENSAGE,'[[VALOR1]]',lv_mes ||'- '||lv_anio);      
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_2:'||LV_MENSAGE);    
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_3');     
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_3:'||LV_MENSAGE);  
      --
      LV_MENSAGE:=gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_4');
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_4:'||LV_MENSAGE);    
      --
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_5:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_5'));
      DBMS_OUTPUT.PUT_LINE('MAIL_MESSAGE_R'||pv_cia||'_6:'||gvf_obtener_valor_parametro(7399,'MAIL_MESSAGEP_6')); 
    
      RETURN;      
      
    END IF;   
    
    Lv_To   := REPLACE(Lv_To,';',',');
    Lv_CC   := REPLACE(Lv_CC,';',','); 
    Lv_From := REPLACE(Lv_From,';',NULL);
    
    LV_ARCHIVO:= PV_ARCHIVO;
    lv_directorio := gvf_obtener_valor_parametro(7399,'DIRECTORIO_REPORTE');  
   
    BEGIN
      EXECUTE IMMEDIATE GVF_OBTENER_VALOR_PARAMETRO(7399,'SQL_PCK_FTP')      
        USING GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_IP_REMOTO'), 
              GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_USER_REMOTO'),
              GVF_OBTENER_VALOR_PARAMETRO(7399,'FTP_DIR_REMOTO'),
              LV_ARCHIVO,
              LV_DIRECTORIO,
              'N',
          OUT LN_ERROR,
          OUT LV_ERROR;
          
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 2000);
        IF instr(lv_error, 'ORA-01403') > 0 THEN
          NULL; --CUANDO EL ERROR ES NOT_DATA_FOUND
        ELSE
          RAISE le_salida2;
        END IF;
    END;
    --                                           
    if ln_error <> 0 then
      raise le_salida2;
    end if;
    --      
    Pv_ARCHIVO1:='</ARCHIVO1='||PV_ARCHIVO||'|DIRECTORIO1='||gvf_obtener_valor_parametro(7399,'FTP_RUTA_REMOTA')||'|/>';  
    -- nueva funcionalidad con plantilla 
    -- Proceso para Envio de Notificaciones     
    -- scp_dat.sck_notificar_gtw.scp_f_notificar                       
    LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  => LV_PROGRAMA,
                                                   PD_FECHAENVIO      => SYSDATE, 
                                                   PD_FECHAEXPIRACION => SYSDATE + 1 / 24,
                                                   PV_ASUNTO          => LV_MAILSUBJECT,
                                                   PV_MENSAJE         => LV_CUERPOCORREO||'//'||Pv_ARCHIVO1,
                                                   PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                   PV_REMITENTE       => LV_FROM,
                                                   PV_TIPOREGISTRO    => lv_notifica,
                                                   PV_CLASE           => 'RAC',
                                                   PV_PUERTO          => LV_PUERTO,
                                                   PV_SERVIDOR        => LV_SERVIDOR,
                                                   PV_MAXINTENTOS     => 2,
                                                   PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                   PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                           
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;

  EXCEPTION
    WHEN le_salida2 THEN
      PV_ERROR := lv_programa || ' - ' || substr(lv_error, 1, 150);
    WHEN LE_EXCEPTION THEN
      PV_ERROR := 'Error en Envio: ' || LN_VALORRETORNO || '-' || LV_APLICACION || LV_ERROR;
            
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || SUBSTR(SQLERRM, 1, 100);
          
  END clp_mail_notifica_pagos;
    
  --===============================================================================================--
  -- Project : [10855] RECLASIFICACION Y CASTIGO
  -- Author  : SUD Kevin Avalos
  -- Created : 03/06/2016  
  -- CRM     : SIS Maria Estevez
  -- PDS     : SUD Cristhian Acosta
  -- Purpose : Guarda los QC's para que sean utilizados por spool para generar Archivos .xls
  --===============================================================================================--
  PROCEDURE genera_reporte_excel_spool(pv_arch_ref   IN VARCHAR2,
                                       pv_file       IN VARCHAR2,
                                       pv_query      IN VARCHAR2,
                                       pv_directorio IN VARCHAR2,
                                       pv_path_tipo  IN VARCHAR2 DEFAULT NULL) IS
  
    --Inicio - Generacion del reporte
    LV_PATH               VARCHAR2(500);
    LB_FOUND_FILE         BOOLEAN;
    LB_FOUND_PATH         BOOLEAN;  
    LD_FECHA_EJECUCION    DATE:=TRUNC(SYSDATE);
    LV_LINEA              LONG;
    LV_ESTADO             VARCHAR2(2);
    LN_SECUENCIA          NUMBER;
    LV_FILE               VARCHAR2(500);
    LV_ARCH_REF           VARCHAR2(500);
    LV_VALORES_COL        LONG:=NULL;
    --
    CURSOR C_NAME_FILE IS
      SELECT * FROM CO_REP_SPOOL_EXCEL WHERE NOMBRE_ARCHIVO = pv_file;
  
    CURSOR C_GET_PATH IS
      SELECT D.DIRECTORY_PATH
        FROM ALL_DIRECTORIES D
       WHERE D.DIRECTORY_NAME = PV_DIRECTORIO;
  
    LC_FILE_EXISTS C_NAME_FILE%ROWTYPE;
    LC_PATH_FILE   C_GET_PATH%ROWTYPE;
    --
  
    PROCEDURE run_query(p_sql IN VARCHAR2) IS
      v_v_val VARCHAR2(4000);
      v_n_val NUMBER;
      v_d_val DATE;
      v_ret   NUMBER;
      c       NUMBER;
      d       NUMBER;
      col_cnt INTEGER;
      f       BOOLEAN;
      rec_tab DBMS_SQL.DESC_TAB;
      col_num NUMBER;    
    BEGIN
      c := DBMS_SQL.OPEN_CURSOR;
      -- parse the SQL statement
      DBMS_SQL.PARSE(c, p_sql, DBMS_SQL.NATIVE);
      -- get a description of the returned columns
      DBMS_SQL.DESCRIBE_COLUMNS(c, col_cnt, rec_tab);
      -- bind variables to columns
      --10855 SUD INI KAE
      ln_secuencia       := 0;
      lv_linea           := '';
      -- Output the column headers
      lv_valores_col:=null;
      FOR j IN 1 .. col_cnt LOOP
        --
        lv_valores_col :=  lv_valores_col||''''||rec_tab(j).col_name||''',';
        --
      END LOOP;
      --
        lv_valores_col := substr(lv_valores_col,1, length(lv_valores_col)-1 );
        IF lv_valores_col IS NOT NULL THEN
          lv_valores_col := 'SELECT '||lv_valores_col||'  FROM DUAL ';
        END IF;
      --
      DBMS_SQL.CLOSE_CURSOR(c);
      --
    END;   
    --Fin - Generacion del reporte                       
  BEGIN
    --
    IF PV_QUERY IS NULL THEN
       RETURN;  
    END IF;
    --
    OPEN C_NAME_FILE;
    FETCH C_NAME_FILE
      INTO LC_FILE_EXISTS;
    LB_FOUND_FILE := C_NAME_FILE%FOUND;
    CLOSE C_NAME_FILE;
    --   
    IF (LB_FOUND_FILE) THEN
      DELETE FROM CO_REP_SPOOL_EXCEL C
       WHERE C.NOMBRE_ARCHIVO = LC_FILE_EXISTS.NOMBRE_ARCHIVO;
      COMMIT;
    END IF;
    --
    IF (pv_path_tipo = 'RFS') THEN
      --es la ruta del directorio f�sico
      LV_PATH := PV_DIRECTORIO;
      --
    ELSIF (pv_path_tipo = 'RDB') OR pv_path_tipo IS NULL THEN
      --es un directorio de base de datos
      --se extrae la ruta f�sica
      OPEN C_GET_PATH;
      FETCH C_GET_PATH
        INTO LC_PATH_FILE;
      LB_FOUND_PATH := C_GET_PATH%FOUND;
      CLOSE C_GET_PATH;
      --
      LV_PATH := LC_PATH_FILE.DIRECTORY_PATH;
      --
    END IF;
    --
    LV_FILE := PV_FILE;
    LV_ARCH_REF := PV_ARCH_REF;
    --    
    RUN_QUERY(PV_QUERY);
    --    
    IF LV_VALORES_COL IS NOT NULL THEN
      --
      INSERT INTO CO_REP_SPOOL_EXCEL
      VALUES
        (CO_REP_SPOOL_EXCEL_SECUENCIA.NEXTVAL,
         LV_FILE,
         LD_FECHA_EJECUCION,
         LV_VALORES_COL,
         'C',
         LV_ARCH_REF,
         LV_PATH);  
         --    
    END IF;    
    --
    IF PV_QUERY IS NOT NULL THEN
      --
      INSERT INTO CO_REP_SPOOL_EXCEL
      VALUES
        (CO_REP_SPOOL_EXCEL_SECUENCIA.NEXTVAL,
         LV_FILE,
         LD_FECHA_EJECUCION,
         PV_QUERY,
         'Q',
         LV_ARCH_REF,
         LV_PATH);  
      --
    END IF;
    --
    COMMIT;
    --
  END;

  --INICIO [10995] IRO_HRO 02/03/2017
  function GCP_OBTIENE_CTAS_INACT(PV_CUENTA IN VARCHAR2) return boolean is
 
   --CURSOR PARA OBTENER EL CUSTOMER_ID DE LA CUENTA
   CURSOR C_OBTENER_CUSTOMER(CV_CUENTA VARCHAR2) IS
     SELECT A.CUSTOMER_ID
       FROM CUSTOMER_ALL A
      WHERE A.CUSTCODE = decode(substr(CV_CUENTA, 1, 1),
                                '1',
                                CV_CUENTA,
                                CV_CUENTA || '.00.00.100000');
                                              
   --CURSOR PARA OBTENER EL CO_ID INACTIVO DE LA CUENTA
   CURSOR C_CTAS_INACT(CV_CUSTOMER VARCHAR2) IS
     SELECT A.CO_ID, A.CUSTOMER_ID, B.CH_STATUS, B.CH_VALIDFROM
       FROM CONTRACT_ALL A, CONTRACT_HISTORY B
      WHERE A.CUSTOMER_ID = CV_CUSTOMER
        AND A.CO_ID = B.CO_ID
        AND B.LASTMODDATE <= SYSDATE
      ORDER BY 1, 4 DESC;
 
   LN_COID     NUMBER := 1;
   LV_CUSTOMER VARCHAR2(20) := NULL;
 
   TYPE CONTRACT_HISTORY IS RECORD(
     CO_ID        INTEGER,
     CH_SEQNO     INTEGER,
     CH_STATUS    VARCHAR2(1),
     CH_VALIDFROM DATE);
 
   TYPE TB_CONTR_HIS IS TABLE OF CONTRACT_HISTORY INDEX BY BINARY_INTEGER;
   T_STAT TB_CONTR_HIS;
 
 BEGIN
 
   OPEN C_OBTENER_CUSTOMER(PV_CUENTA);
   FETCH C_OBTENER_CUSTOMER
     INTO LV_CUSTOMER;
   CLOSE C_OBTENER_CUSTOMER;
 
   OPEN C_CTAS_INACT(LV_CUSTOMER);
   FETCH C_CTAS_INACT BULK COLLECT
     INTO T_STAT;
   CLOSE C_CTAS_INACT;
 
   IF T_STAT.COUNT > 0 THEN
     FOR C IN T_STAT.FIRST .. T_STAT.LAST LOOP
       IF LN_COID <> T_STAT(C).CO_ID THEN
         IF T_STAT(C).CH_STATUS = 'd' THEN
           null;
         ELSE
           return false;
         END IF;
       END IF;
       LN_COID := T_STAT(C).CO_ID;
     END LOOP;
   else
     return true;
   END IF;
   return true;
 EXCEPTION
   WHEN OTHERS THEN
     return true;  
 END;
--FIN [10995] IRO_HRO 02/03/2017

  /*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      JUAN ROMERO
    CREADO POR:     IRO HUGO ROMERO  IRO_HRO 
    PROYECTO:       [11188] Equipo Cambio Continuo Reloj de Cobranzas y Reactivaciones - Reclasificacion de Cartera
    FECHA:          15/03/2017
    PROP�SITO:      Se crea el procedimiento GENERA_PREASIENTO_SAP_DTH el cual es el encargado de 
                    crear los asientos contables de reclasificacion - castigo para enviar a SAP de las
                    cuentas DTH.
    SIGLAS DE BUSQ: IRO_HRO                    
  **********************************************************************************************************/

PROCEDURE GENERA_PREASIENTO_SAP_DTH(PV_FECHACTUAL   IN VARCHAR2,
                                    PV_FECHANTERIOR IN VARCHAR2,
                                    PD_FECHA        IN DATE,
                                    PV_ERROR        OUT VARCHAR2) IS

  CURSOR C_COMPANIAS IS
    SELECT DISTINCT B.COMPANIA, B.AJUSTE
      FROM (SELECT DISTINCT DECODE(N.CIA, 1, 2, 2, 1) COMPANIA,
                            N.NO_FACTURA AJUSTE
              FROM CO_CARTERA_CLIENTES N
             WHERE N.ESTADO_CARTERA IS NOT NULL --= 'R'
               AND N.ESTADO IN ('D') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
               AND N.PROCESO IN ('P')
               AND N.PRODUCTO = 'DTH'
               AND N.NO_FACTURA IS NOT NULL
            UNION
            SELECT DISTINCT TO_NUMBER(N.CIA) COMPANIA, N.NO_FACTURA AJUSTE
              FROM CO_CARTERA_CLIENTES N
             WHERE N.ESTADO_CARTERA IS NOT NULL --= 'C'
               AND N.ESTADO IN ('V') --= 'D' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
               AND N.PROCESO IN ('R')
               AND N.PRODUCTO = 'DTH'
               AND N.NO_FACTURA IS NOT NULL) B;

  CURSOR C_OBTIENE_TOTAL(CN_COMPANIA NUMBER) IS
    SELECT SUM(TOTAL) VALOR
      FROM CO_CARTERA_CLIENTES
     WHERE CIA = CN_COMPANIA
       AND ESTADO_CARTERA = 'R'
       AND ESTADO IN ('D') -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
       AND PROCESO = 'P'
       AND PRODUCTO = 'DTH';

  CURSOR C_OBTIENE_TOTAL2(CN_COMPANIA NUMBER, CN_AJUSTE NUMBER) IS
    SELECT SUM(TOTAL) VALOR
      FROM CO_CARTERA_CLIENTES
     WHERE CIA = CN_COMPANIA
       AND ESTADO_CARTERA = 'C'
       AND ESTADO = 'V' -- DESPUES DE GENERAR CARGO POR CREDITO EN BSCS
       AND PROCESO = 'R'
       AND NO_FACTURA = CN_AJUSTE
       AND PRODUCTO = 'DTH';

  LV_GLOSA VARCHAR2(200);
  LE_CONTROL_ERROR EXCEPTION;
  LE_ERROR EXCEPTION;
  LV_REFERENCIA VARCHAR2(200);
  LN_VALOR      NUMBER;
  LV_PROCESO    VARCHAR2(15) := '[%PROC%]';
  LV_REPLACE    VARCHAR2(15);

  -- RECLASIFICACION
  LV_CTA_DEBE         VARCHAR2(200);
  LV_CTA_HABER        VARCHAR2(200);
  LN_TOTAL            NUMBER;
  LN_CIA              NUMBER;
  LV_ERROR            VARCHAR2(4000);
  LV_CABECERA         VARCHAR2(4000) := NULL;
  LV_CABECERA_CASTIGO VARCHAR2(4000) := NULL;
  LN_LINEA_ASIENTO    NUMBER := 0;
  LV_ASIENTO_1        VARCHAR2(4000) := NULL;
  LV_ASIENTO_2        VARCHAR2(4000) := NULL;
  LV_ASIENTO_CAS_1    VARCHAR2(4000) := NULL;
  LV_ASIENTO_CAS_2    VARCHAR2(4000) := NULL;
  LV_ASIENTO_CAS_3    VARCHAR2(4000) := NULL;
  LV_ASIENTO_CAS_4    VARCHAR2(4000) := NULL;
  LV_TRAMA_ASIENTOS   CLOB := NULL;
  LV_ANIO             VARCHAR2(10) := NULL;
  LV_MES              VARCHAR2(10) := NULL;
  LV_FECHA_ASIENTO    VARCHAR2(50) := NULL;
  LV_ID_POLIZA        VARCHAR2(50) := NULL;
  LV_ID_POLIZA_CAS    VARCHAR2(50) := NULL;
  LV_DIVISION         VARCHAR2(50) := NULL;
  LV_TRAMA            CLOB;
  LV_CLAVE_COSTA      VARCHAR2(50) := NULL;
  LV_CLAVE_SIERRA     VARCHAR2(50) := NULL;
  LV_DESC_POLIZA_COS  VARCHAR2(2000) := NULL;
  LV_DESC_POLIZA_SIE  VARCHAR2(2000) := NULL;
  LD_FECHACTUAL       DATE;
  LD_FECHANTERIOR     DATE;
  LV_FECHACTUAL       VARCHAR2(2000) := NULL;
  LV_FECHANTERIOR     VARCHAR2(2000) := NULL;
  LV_BAN_UP_SAP_DTH   VARCHAR2(10) := 'N';
  LV_BAND_TRAMA_DTH   VARCHAR2(10) := 'N';

BEGIN

  EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''YYYYMMDD''';

  LV_BAND_TRAMA_DTH := NVL(GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                       'BAND_DESPACHA_TRAMAS_DTH'),
                           'N');

  LD_FECHACTUAL   := TO_DATE(PV_FECHACTUAL, 'DD/MM/YYYY');
  LD_FECHANTERIOR := TO_DATE(PV_FECHANTERIOR, 'DD/MM/YYYY');

  LV_FECHACTUAL     := TO_CHAR(LD_FECHACTUAL, 'DD/MM/YYYY');
  LV_FECHANTERIOR   := TO_CHAR(LD_FECHANTERIOR, 'DD/MM/YYYY');
  LV_BAN_UP_SAP_DTH := NVL(GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                       'BAND_UPD_SAP_DTH'),
                           'N');

  -- GLOSE DEL ASIENTO CONTABLE 
  LV_GLOSA := GVF_OBTENER_VALOR_PARAMETRO(11188, 'GLOSA');
  IF LV_GLOSA IS NULL THEN
    PV_ERROR := 'GLOSA NO CONFIGURADA.. ';
    RAISE LE_ERROR;
  END IF;

  --CUENTA DEUDORA -- RECLASIFICACION
  LV_CTA_DEBE := GVF_OBTENER_VALOR_PARAMETRO(11188, 'CUENTA_RECLA_DEBE_DTH');
  IF LV_CTA_DEBE IS NULL THEN
    PV_ERROR := 'CUENTA DEUDORA NO CONFIGURADA ';
    RAISE LE_ERROR;
  END IF;

  LV_ANIO          := TO_CHAR(SYSDATE, 'YYYY');
  LV_MES           := TO_CHAR(SYSDATE, 'MM');
  LV_FECHA_ASIENTO := TO_CHAR(PD_FECHA, 'YYYYMMDD');

  LV_DESC_POLIZA_COS := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                        PV_ID_PARAMETRO      => 'DESC_POLIZA_COSTA');

  LV_DESC_POLIZA_SIE := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                        PV_ID_PARAMETRO      => 'DESC_POLIZA_SIERRA');

  LV_CLAVE_COSTA := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                    PV_ID_PARAMETRO      => 'CLAVE_COSTA');

  LV_CLAVE_SIERRA := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                     PV_ID_PARAMETRO      => 'CLAVE_SIERRA');

  --ASIENTO POR COMPANIAS
  FOR I IN C_COMPANIAS LOOP
    BEGIN
    
      LN_LINEA_ASIENTO  := 0;
      LV_TRAMA_ASIENTOS := NULL;
      LV_TRAMA          := NULL;
    
      --CREA SECUENCIA DE POLIZA X RECLASIFICACI�N
    
      CLK_RECLASIFICACION.GENERA_SECUENCIA_POLIZA(PV_DOCUMENTO => LV_ID_POLIZA,
                                                  PV_ERROR     => PV_ERROR);
    
      IF PV_ERROR IS NOT NULL THEN
      
        PV_ERROR := SUBSTR(PV_ERROR ||
                           '- ERROR , AL GENERAR SECUENCIA DE POLIZA ' ||
                           SQLERRM,
                           1,
                           1500);
        RAISE LE_CONTROL_ERROR;
      END IF;
    
      --EXTRAEMOS CABECERA DE ASIENTO DE RECLASIFICACION Y REEMPLAZAMOS VALORES
    
      LV_CABECERA := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                 'PARAM_CABECERA_RECL');
    
      LV_CABECERA := REPLACE(LV_CABECERA, '<%ID_POLIZA%>', LV_ID_POLIZA);
    
      LV_CABECERA := REPLACE(LV_CABECERA, '<%FECHA%>', LV_FECHA_ASIENTO);
    
      LV_CABECERA := REPLACE(LV_CABECERA, '<%MES%>', LV_MES);
    
      LV_CABECERA := REPLACE(LV_CABECERA, '<%ANIO%>', LV_ANIO);
    
      LV_PROCESO := '[%PROC%]';
    
      IF I.COMPANIA IN (2) THEN
      
        LV_CABECERA := REPLACE(LV_CABECERA, '<%ID_REGION%>', LV_CLAVE_COSTA);
      
        LV_CABECERA := REPLACE(LV_CABECERA,
                               '<%DESCR_REGION%>',
                               LV_DESC_POLIZA_COS);
      
        LV_DIVISION := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                       PV_ID_PARAMETRO      => 'DIVISION_COSTA');
      
        -- En BSCS, GYE
        --CUENTA ACREEDORA -- Reclasificacion
        LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                    'CUENTA_RECLA_HABER_COS_DTH');
        IF LV_CTA_HABER IS NULL THEN
          PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
          RAISE LE_ERROR;
        END IF;
      
        -- CODIGO DE COMPANIA EN BSCS
        LN_CIA := 1;
      ELSIF I.COMPANIA IN (1) THEN
      
        LV_CABECERA := REPLACE(LV_CABECERA,
                               '<%ID_REGION%>',
                               LV_CLAVE_SIERRA);
      
        LV_CABECERA := REPLACE(LV_CABECERA,
                               '<%DESCR_REGION%>',
                               LV_DESC_POLIZA_SIE);
      
        LV_DIVISION := CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11188,
                                                                       PV_ID_PARAMETRO      => 'DIVISION_SIERRA');
      
        -- En BSCS, UIO
        --CUENTA ACREEDORA -- Reclasificacion
        LV_CTA_HABER := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                    'CUENTA_RECLA_HABER_SIE_DTH');
        IF LV_CTA_HABER IS NULL THEN
          PV_ERROR := 'CUENTA ACREEDORA NO CONFIGURADA ';
          RAISE LE_ERROR;
        END IF;
      
        -- CODIGO DE COMPANIA EN BSCS
        LN_CIA := 2;
      END IF;
    
      -- SE MARCA EL PERIODO DE AJUSTE
      LV_GLOSA := REPLACE(REPLACE(REPLACE(LV_GLOSA,
                                          '[%%FECHANTERIOR%%]',
                                          LV_FECHANTERIOR),
                                  '[%%FECHACTUAL%%]',
                                  LV_FECHACTUAL),
                          '[%FACT%]',
                          I.AJUSTE);
    
      -- SUMARIZA LA CANTIDAD A RECLASIFICAR
      OPEN C_OBTIENE_TOTAL(LN_CIA);
      FETCH C_OBTIENE_TOTAL
        INTO LN_TOTAL;
      IF C_OBTIENE_TOTAL%NOTFOUND THEN
        CLOSE C_OBTIENE_TOTAL;
      ELSIF C_OBTIENE_TOTAL%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
      
        -- Defino el proceso por el que se hara el primer ajuste
        LV_REPLACE    := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                     'PROC_GLOSA_REC');
        LV_REFERENCIA := REPLACE(LV_GLOSA, LV_PROCESO, LV_REPLACE);
        -- PRIMER AJUSTE -- DEBE
        begin
        
          --CREA ASIENTO RECLASIFICACION LINEA 1
        
          LN_LINEA_ASIENTO := LN_LINEA_ASIENTO + 1;
        
          LV_ASIENTO_1 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                      'PARAM_RECLA_LINEA_1');
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                  '<%NO_LINEA%>',
                                  LN_LINEA_ASIENTO);
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                  '<%FECHA%>',
                                  LV_FECHA_ASIENTO);
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1, '<%MONTO%>', LN_TOTAL);
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                  '<%NO_DOCUMENTO_INTERNO%>',
                                  TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                  '<%GLOSA_RECLA%>',
                                  LV_REFERENCIA);
        
          LV_ASIENTO_1 := REPLACE(LV_ASIENTO_1,
                                  '<%CUENTA_RECLA_HABER%>',
                                  LV_CTA_HABER);
        END;
      
        LN_VALOR := LN_TOTAL;
        -- PRIMER AJUSTE -- HABER
      
        Begin
        
          --CREA ASIENTO RECLASIFICACION LINEA 2
        
          LN_LINEA_ASIENTO := LN_LINEA_ASIENTO + 1;
        
          LV_ASIENTO_2 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                      'PARAM_RECLA_LINEA_2');
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                  '<%NO_LINEA%>',
                                  LN_LINEA_ASIENTO);
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                  '<%FECHA%>',
                                  LV_FECHA_ASIENTO);
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2, '<%MONTO%>', LN_VALOR);
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                  '<%NO_DOCUMENTO_INTERNO%>',
                                  TO_CHAR(I.AJUSTE));
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                  '<%GLOSA_RECLA%>',
                                  LV_REFERENCIA);
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2,
                                  '<%CUENTA_RECLA_DEBE%>',
                                  LV_CTA_DEBE);
        
          LV_ASIENTO_2 := REPLACE(LV_ASIENTO_2, '<%DIVISION%>', LV_DIVISION);
        
        END;
        LV_TRAMA_ASIENTOS := LV_TRAMA_ASIENTOS || LV_ASIENTO_1 ||
                             LV_ASIENTO_2;
        LV_TRAMA          := LV_CABECERA || LV_TRAMA_ASIENTOS || ']';
      
        CLK_RECLASIFICACION.GCP_INSERTAR_TRAMA(PV_PROCESO          => LV_ID_POLIZA,
                                               PC_TRAMA            => LV_TRAMA,
                                               PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                                                                                      'INGRESO'), --'I',
                                               PV_ESTADO_REPROCESO => NULL,
                                               PD_FECHA_INGRESO    => SYSDATE,
                                               PD_FECHA_DESPACHO   => NULL,
                                               PD_FECHA_REPROCESO  => NULL,
                                               PV_COMPANIA         => LN_CIA,
                                               PN_MONTO            => LN_VALOR,
                                               PV_MSG_ERROR        => PV_ERROR);
      
        IF PV_ERROR IS NOT NULL THEN
        
          PV_ERROR := SUBSTR(PV_ERROR ||
                             '- ERROR , AL INSERTAR TRAMA DE POLIZA ' ||
                             SQLERRM,
                             1,
                             1500);
          RAISE LE_ERROR;
        END IF;
      
        commit;
      
        LV_PROCESO := LV_REPLACE;
      END IF;
      CLOSE C_OBTIENE_TOTAL;
    
      ---RECLASIFICACION DE CARTERA GCA
    
      -- DEFINO MODULO Y PLANTILLA PARA SEGUNDO AJUSTE
    
      -- SUMARIZA LA CANTIDAD A CASTIGAR
      OPEN C_OBTIENE_TOTAL2(I.COMPANIA, I.AJUSTE);
      FETCH C_OBTIENE_TOTAL2
        INTO LN_TOTAL;
      IF C_OBTIENE_TOTAL2%NOTFOUND THEN
        CLOSE C_OBTIENE_TOTAL2;
      ELSIF C_OBTIENE_TOTAL2%FOUND AND NVL(LN_TOTAL, 0) > 0 THEN
      
        -- DEFINO EL PROCESO POR EL QUE SE HARA EL SEGUNDO AJUSTE
        LV_REPLACE    := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                     'PROC_GLOSA_CAS');
        LV_REFERENCIA := REPLACE(NVL(LV_REFERENCIA, LV_GLOSA),
                                 LV_PROCESO,
                                 LV_REPLACE);
      
        LN_VALOR := LN_TOTAL;
      
        --GENERO SECUENCIA DE ASIENTO CASTIGO
        CLK_RECLASIFICACION.GENERA_SECUENCIA_POLIZA_CAS(PV_DOCUMENTO => LV_ID_POLIZA_CAS,
                                                        PV_ERROR     => PV_ERROR);
      
        IF PV_ERROR IS NOT NULL THEN
        
          PV_ERROR := SUBSTR(PV_ERROR ||
                             '- ERROR , AL GENERAR SECUENCIA DE POLIZA ' ||
                             SQLERRM,
                             1,
                             1500);
          RAISE LE_CONTROL_ERROR;
        END IF;
      
        --EXTRAIGO CABECERA DE ASIENTO CASTIGO
      
        LV_CABECERA_CASTIGO := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                           'PARAM_CABECERA_CASTIGO');
      
        LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                       '<%ID_POLIZA%>',
                                       LV_ID_POLIZA_CAS);
      
        LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                       '<%FECHA%>',
                                       LV_FECHA_ASIENTO);
      
        LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                       '<%MES%>',
                                       LV_MES);
      
        LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                       '<%ANIO%>',
                                       LV_ANIO);
      
        --EXTRAIGO ASIENTO 1 CASTIGO  
        LV_ASIENTO_CAS_1 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                        'TRAMA_CASTIGO_LINEA_1_DTH');
      
        LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
      
        LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1, '<%MONTO%>', LN_VALOR);
      
        LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                    '<%NO_NOTA_INTERNA%>',
                                    TO_CHAR(I.AJUSTE));
      
        LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                    '<%GLOSA%>',
                                    LV_REFERENCIA);
      
        LV_ASIENTO_CAS_1 := REPLACE(LV_ASIENTO_CAS_1,
                                    '<%DIVISION%>',
                                    LV_DIVISION);
      
        --EXTRAIGO ASIENTO 2 CASTIGO                                 
        LV_ASIENTO_CAS_2 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                        'TRAMA_CASTIGO_LINEA_2_DTH');
      
        LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
      
        LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2, '<%MONTO%>', LN_TOTAL);
      
        LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                    '<%NO_NOTA_INTERNA%>',
                                    TO_CHAR(I.AJUSTE));
      
        LV_ASIENTO_CAS_2 := REPLACE(LV_ASIENTO_CAS_2,
                                    '<%GLOSA%>',
                                    LV_REFERENCIA);
      
        --EXTRAIGO ASIENTO 3 CASTIGO                                  
        LV_ASIENTO_CAS_3 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                        'TRAMA_CASTIGO_LINEA_3_DTH');
      
        LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
      
        LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3, '<%MONTO%>', LN_TOTAL);
      
        LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                    '<%NO_NOTA_INTERNA%>',
                                    TO_CHAR(I.AJUSTE));
      
        LV_ASIENTO_CAS_3 := REPLACE(LV_ASIENTO_CAS_3,
                                    '<%GLOSA%>',
                                    LV_REFERENCIA);
      
        --EXTRAIGO ASIENTO 4 CASTIGO                                  
        LV_ASIENTO_CAS_4 := GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                        'TRAMA_CASTIGO_LINEA_4_DTH');
      
        LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                    '<%FECHA%>',
                                    LV_FECHA_ASIENTO);
      
        LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4, '<%MONTO%>', ln_valor);
      
        LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                    '<%NO_NOTA_INTERNA%>',
                                    TO_CHAR(I.AJUSTE));
      
        LV_ASIENTO_CAS_4 := REPLACE(LV_ASIENTO_CAS_4,
                                    '<%GLOSA%>',
                                    LV_REFERENCIA);
      
        IF I.COMPANIA IN (2) THEN
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%ID_REGION%>',
                                         LV_CLAVE_COSTA);
        
          LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                         '<%REGION%>',
                                         LV_DESC_POLIZA_COS);
        
        ELSE
          IF I.COMPANIA IN (1) THEN
          
            LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                           '<%ID_REGION%>',
                                           LV_CLAVE_SIERRA);
          
            LV_CABECERA_CASTIGO := REPLACE(LV_CABECERA_CASTIGO,
                                           '<%REGION%>',
                                           LV_DESC_POLIZA_SIE);
          END IF;
        
        END IF;
      
        LV_TRAMA_ASIENTOS := NULL;
        LV_TRAMA          := NULL;
        LV_TRAMA_ASIENTOS := LV_TRAMA_ASIENTOS || LV_ASIENTO_CAS_1 ||
                             LV_ASIENTO_CAS_2 || LV_ASIENTO_CAS_3 ||
                             LV_ASIENTO_CAS_4;
        LV_TRAMA          := LV_CABECERA_CASTIGO || LV_TRAMA_ASIENTOS || ']';
      
        CLK_RECLASIFICACION.GCP_INSERTAR_TRAMA(PV_PROCESO          => LV_ID_POLIZA_CAS,
                                               PC_TRAMA            => LV_TRAMA,
                                               PV_ESTADO_DESPACHO  => CLK_RECLASIFICACION.GVF_OBTENER_VALOR_PARAMETRO(11188,
                                                                                                                      'INGRESO'), --'I',
                                               PV_ESTADO_REPROCESO => NULL,
                                               PD_FECHA_INGRESO    => SYSDATE,
                                               PD_FECHA_DESPACHO   => NULL,
                                               PD_FECHA_REPROCESO  => NULL,
                                               PV_COMPANIA         => LN_CIA, --I.COMPANIA,   
                                               PN_MONTO            => LN_VALOR,
                                               PV_MSG_ERROR        => PV_ERROR);
      
        IF PV_ERROR IS NOT NULL THEN
        
          PV_ERROR := SUBSTR(PV_ERROR ||
                             '- ERROR , AL INSERTAR TRAMA DE POLIZA ' ||
                             SQLERRM,
                             1,
                             1500);
          RAISE LE_ERROR;
        END IF;
      
        commit;
      
      END IF;
      CLOSE C_OBTIENE_TOTAL2;
    
      --11188 ACTUALIZACION DE COMENTARIO Q CREO ASIENTO EN SAP
      UPDATE CO_CARTERA_CLIENTES T
         SET COMENTARIO = SUBSTR(T.COMENTARIO, 1, 1800) || '- SAP'
       WHERE NO_FACTURA = I.AJUSTE
         AND PRODUCTO = 'DTH'
         AND ESTADO IN ('D')
         AND CIA = LN_CIA;
    
      UPDATE CO_CARTERA_CLIENTES T
         SET COMENTARIO = SUBSTR(T.COMENTARIO, 1, 1800) || '- SAP'
       WHERE NO_FACTURA = I.AJUSTE
         AND PRODUCTO = 'DTH'
         AND ESTADO IN ('V')
         AND CIA = I.COMPANIA;
    
      --11188 SE ACTIVA SOLO SI EL PROCESO CREA ASIENTO EN SAP 
      IF LV_BAN_UP_SAP_DTH = 'S' THEN
        UPDATE CO_CARTERA_CLIENTES
           SET ESTADO = 'T'
         WHERE NO_FACTURA = I.AJUSTE
           AND ESTADO IN ('D')
           AND PRODUCTO = 'DTH'
           AND CIA = LN_CIA;
      
        UPDATE CO_CARTERA_CLIENTES
           SET ESTADO = 'T'
         WHERE NO_FACTURA = I.AJUSTE
           AND ESTADO IN ('V')
           AND PRODUCTO = 'DTH'
           AND CIA = I.COMPANIA;
      END IF;
    
      COMMIT;
    EXCEPTION
      WHEN LE_CONTROL_ERROR THEN
        ROLLBACK;
        UPDATE CO_CARTERA_CLIENTES
           SET ESTADO = 'E', COMENTARIO = LV_ERROR || COMENTARIO
         WHERE NO_FACTURA = I.AJUSTE;
        COMMIT;
        RAISE LE_ERROR;
    END;
  END LOOP; --C_COMPANIAS

  IF LV_BAND_TRAMA_DTH = 'S' THEN
  
    CLK_RECLASIFICACION.GCP_ENVIA_TRAMAS(PV_ERROR => PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
    
      PV_ERROR := SUBSTR(PV_ERROR ||
                         '- ERROR , AL GENERAR DESPACHAR TRAMAS SAP ' ||
                         SQLERRM,
                         1,
                         1500);
      RAISE LE_ERROR;
    END IF;
  END IF;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := SUBSTR(PV_ERROR || ' - ' || SQLERRM, 1, 1000);
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM,1,1000);
  
END GENERA_PREASIENTO_SAP_DTH;

END CLK_RECLASIFICACION;
/
