CREATE OR REPLACE PACKAGE PCK_REP_FINANCIAMIENTOS_MES_HW IS

PROCEDURE P_DETALLE_REPORTE_LEGADO(PV_FECHACORTE VARCHAR2,
                                   PV_PRODUCTO   VARCHAR2,
                                   PV_ERROR      OUT VARCHAR2);

PROCEDURE P_DETALLE_REPORTE_HW(PV_FECHACORTE VARCHAR2,
                               PV_PRODUCTO   VARCHAR2,
                               PV_ERROR      OUT VARCHAR2);

END PCK_REP_FINANCIAMIENTOS_MES_HW;
/
CREATE OR REPLACE PACKAGE BODY PCK_REP_FINANCIAMIENTOS_MES_HW IS
--=====================================================================================--
-- CREADO POR:     Hitss Mariuxi S�nchez Tumbaco.
-- FECHA CREA:     20/01/2021
-- PROYECTO:       12904 Financiamiento de equipos y reporteria por v
-- LIDER PDS:      Hitss Andr�s Balladares
-- LIDER :         TIC Adrian Roelas
-- MOTIVO:         Reporte detallado de los financiamientos vendidos por mes.
--=====================================================================================--


PROCEDURE P_DETALLE_REPORTE_LEGADO(PV_FECHACORTE VARCHAR2,
                                   PV_PRODUCTO   VARCHAR2,
                                   PV_ERROR      OUT VARCHAR2)IS
  
  CURSOR C_MES_CARGA (Cv_FechaCarga VARCHAR2,
                      Cv_Producto   VARCHAR2) IS
  SELECT TO_CHAR(TO_DATE(X.MES_CARGADO,'MM/YYYY'),'MMYYYY') MES_CARGADO
    FROM FN_VIEW_CAB_SEG_REPORTE X
   WHERE TO_DATE(X.MES_CARGADO,'MM/YYYY') >= TO_DATE(Cv_FechaCarga,'MM/YYYY')
     AND TO_DATE(X.MES_CARGADO,'MM/YYYY') < ADD_MONTHS(TO_DATE(Cv_FechaCarga, 'MM/YYYY'),12)
     AND X.PRODUCTO = Cv_Producto
   ORDER BY TO_DATE(X.MES_CARGADO,'MM/YYYY') ASC;
  
  TYPE T_OBTIENE_DATOS_TABLA IS REF CURSOR;
  C_OBTIENE_DATOS_TABLA T_OBTIENE_DATOS_TABLA;
  
  TYPE TNIDFINAN IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TNIDCONTRATO IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TVCUENTA IS TABLE OF VARCHAR2(24) INDEX BY BINARY_INTEGER;
  TYPE TVPRODUCTO IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
  TYPE TNORDEN IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TVTIPOFINAN IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
  TYPE TNTOTAL IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDFECHATRAN IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  TYPE TVIDENTIFICACION IS TABLE OF VARCHAR2(15) INDEX BY BINARY_INTEGER;
  TYPE TVNOMBRE IS TABLE OF VARCHAR2(300) INDEX BY BINARY_INTEGER;
  
  Tn_IdFinan          TNIDFINAN;
  Tn_IdContrato       TNIDCONTRATO;
  Tv_Cuenta           TVCUENTA;
  Tv_Producto         TVPRODUCTO;
  Tn_Orden            TNORDEN;
  Tv_TipoFinan        TVTIPOFINAN;
  Tn_Total            TNTOTAL;
  Td_FechaTran        TDFECHATRAN;
  Tv_Identificacion   TVIDENTIFICACION;
  Tv_Nombre           TVNOMBRE;
  Lv_Sql              VARCHAR2(10000);
  Lv_SqlScript        VARCHAR2(10000);
  Lv_Depuracion       VARCHAR2(600);
  
BEGIN
  
  Lv_Depuracion:= 'TRUNCATE TABLE FN_DETALLE_REPORTE_FINAN_VENTA';
  EXECUTE IMMEDIATE Lv_Depuracion;
  
  Lv_Sql:='SELECT X.ID_FINANCIAMIENTO, X.ID_CONTRATO, X.CUENTA, X.PRODUCTO, X.NO_ORDEN, X.TIPO_FINAN, X.MONTO_TOTAL,
  X.FECHA_TRANSACCION, X.IDENTIFICACION, X.NOMBRE_COMPLETO
  FROM PORTA.FN_FINANCIAMIENTOS_MES_<MES>@AXIS X
  WHERE X.PRODUCTO = ''<PRODUCTO>''';
  
  FOR I IN C_MES_CARGA(PV_FECHACORTE, PV_PRODUCTO) LOOP
      Lv_SqlScript := Lv_Sql;
      Lv_SqlScript := REPLACE(Lv_SqlScript, '<MES>', I.MES_CARGADO);
      Lv_SqlScript := REPLACE(Lv_SqlScript, '<PRODUCTO>', PV_PRODUCTO);
      
      OPEN C_OBTIENE_DATOS_TABLA FOR Lv_SqlScript;
      LOOP
        FETCH C_OBTIENE_DATOS_TABLA BULK COLLECT
         INTO Tn_IdFinan, Tn_IdContrato, Tv_Cuenta,Tv_Producto, Tn_Orden, Tv_TipoFinan, Tn_Total,
              Td_FechaTran, Tv_Identificacion, Tv_Nombre LIMIT 1000;
         EXIT WHEN Tv_Cuenta.COUNT=0;
         
         FORALL J IN TV_CUENTA.FIRST .. TV_CUENTA.LAST
         INSERT INTO FN_DETALLE_REPORTE_FINAN_VENTA(FECHA_MES, ID_FINANCIAMIENTO, ID_CONTRATO, CUENTA,
                                                    PRODUCTO, NO_ORDEN, TIPO_FINAN, MONTO_TOTAL, FECHA_TRANSACCION,
                                                    IDENTIFICACION, NOMBRE_COMPLETO, USUARIO_INGRESO, FECHA_INGRESO)
                                             VALUES(I.MES_CARGADO, Tn_IdFinan(J), Tn_IdContrato(J), Tv_Cuenta(J),
                                                    Tv_Producto(J), Tn_Orden(J), Tv_TipoFinan(J), Tn_Total(J),
                                                    Td_FechaTran(J), Tv_Identificacion(J), Tv_Nombre(J), USER, SYSDATE);
         
         COMMIT;
         
          Tn_IdFinan.DELETE;
          Tn_IdContrato.DELETE; 
          Tv_Cuenta.DELETE;
          Tv_Producto.DELETE;
          Tn_Orden.DELETE;
          Tv_TipoFinan.DELETE;
          Tn_Total.DELETE;
          Td_FechaTran.DELETE;
          Tv_Identificacion.DELETE;
          Tv_Nombre.DELETE;
         
         EXIT WHEN C_OBTIENE_DATOS_TABLA%NOTFOUND;
      END LOOP;
      CLOSE C_OBTIENE_DATOS_TABLA;
  END LOOP;
  
EXCEPTION
  WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'PCK_REP_FINANCIAMIENTOS_MES_HW.P_DETALLE_REPORTE_LEGADO: ' || SUBSTR(SQLERRM, 1, 120);
END P_DETALLE_REPORTE_LEGADO;

PROCEDURE P_DETALLE_REPORTE_HW(PV_FECHACORTE VARCHAR2,
                               PV_PRODUCTO   VARCHAR2,
                               PV_ERROR      OUT VARCHAR2)IS
  
  CURSOR C_MES_CARGA (Cv_FechaCarga VARCHAR2) IS
  SELECT TO_CHAR(TO_DATE(X.MES_CARGADO,'MM/YYYY'),'MMYYYY') MES_CARGADO
    FROM FN_VIEW_CAB_SEG_REPORTE X
   WHERE TO_DATE(X.MES_CARGADO,'MM/YYYY') >= TO_DATE(Cv_FechaCarga,'MM/YYYY')
     AND TO_DATE(X.MES_CARGADO,'MM/YYYY') < ADD_MONTHS(TO_DATE(Cv_FechaCarga, 'MM/YYYY'),12)
   ORDER BY TO_DATE(X.MES_CARGADO,'MM/YYYY') ASC;
  
  TYPE T_OBTIENE_DATOS_TABLA IS REF CURSOR;
  C_OBTIENE_DATOS_TABLA T_OBTIENE_DATOS_TABLA;
  
  TYPE TVIDFINANCRM IS TABLE OF VARCHAR2(20) INDEX BY BINARY_INTEGER;
  TYPE TVIDFINANCBS IS TABLE OF VARCHAR2(20) INDEX BY BINARY_INTEGER;
  TYPE TVPRODUCTO IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
  TYPE TNORDEN IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TNTOTAL IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDFECHATRAN IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  
  Tn_IdFinanCRM       TVIDFINANCRM;
  Tn_IdFinanCBS       TVIDFINANCBS;
  Tv_Producto         TVPRODUCTO;
  Tn_Orden            TNORDEN;
  Tn_Total            TNTOTAL;
  Td_FechaTran        TDFECHATRAN;
  Lv_Sql              VARCHAR2(10000);
  Lv_SqlScript        VARCHAR2(10000);
  Lv_Depuracion       VARCHAR2(600);
  
BEGIN
  
  Lv_Depuracion:= 'TRUNCATE TABLE FN_DETALLE_REPORTE_FINAN_VENTA';
  EXECUTE IMMEDIATE Lv_Depuracion;
  
  Lv_Sql:='SELECT X.ID_FINAN_CRM, X.ID_FINAN_CBS, X.NO_ORDEN, X.TOTAL_FINANCIAMIENTO, X.FECHA_CREACION, X.PRODUCTO
  FROM PORTA.FN_FINANCIAMIENTOS@AXIS X
  WHERE TO_CHAR(X.TOTAL_FINANCIAMIENTO, ''MM/YYYY'') = ''<FECHA>''';
  
  FOR I IN C_MES_CARGA(PV_FECHACORTE) LOOP
      Lv_SqlScript := Lv_Sql;
      Lv_SqlScript := REPLACE(Lv_SqlScript, '<FECHA>', I.MES_CARGADO);
      
      OPEN C_OBTIENE_DATOS_TABLA FOR Lv_SqlScript;
      LOOP
        FETCH C_OBTIENE_DATOS_TABLA BULK COLLECT
         INTO Tn_IdFinanCRM, Tn_IdFinanCBS, Tn_Orden, Tn_Total,
              Td_FechaTran, Tv_Producto LIMIT 1000;
         EXIT WHEN Tn_IdFinanCRM.COUNT=0;
         
         FORALL J IN Tn_IdFinanCRM.FIRST .. Tn_IdFinanCRM.LAST
         INSERT INTO FN_DETALLE_REP_FINAN_VENTA_HW(FECHA_MES, ID_FINANCIAMIENTOCRM, ID_FINANCIAMIENTOCBS,
                                                   PRODUCTO, NO_ORDEN, MONTO_TOTAL,
                                                   FECHA_TRANSACCION, USUARIO_INGRESO, FECHA_INGRESO)
                                            VALUES(I.MES_CARGADO, Tn_IdFinanCRM(J), Tn_IdFinanCBS(J),
                                                   Tv_Producto(J), Tn_Orden(J), Tn_Total(J),
                                                   Td_FechaTran(J), USER, SYSDATE);
         
         COMMIT;
         
         Tn_IdFinanCRM.DELETE;
         Tn_IdFinanCBS.DELETE;
         Tv_Producto.DELETE;
         Tn_Orden.DELETE;
         Tn_Total.DELETE;
         Td_FechaTran.DELETE;
         
         EXIT WHEN C_OBTIENE_DATOS_TABLA%NOTFOUND;
      END LOOP;
      CLOSE C_OBTIENE_DATOS_TABLA;
  END LOOP;
  
EXCEPTION
  WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'PCK_REP_FINANCIAMIENTOS_MES_HW.P_DETALLE_REPORTE_HW: ' || SUBSTR(SQLERRM, 1, 120);
END P_DETALLE_REPORTE_HW;

END PCK_REP_FINANCIAMIENTOS_MES_HW;
/
