CREATE OR REPLACE package MKK_RPT_CARTERA is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  PROCEDURE INSERTA_OPE_CUADRE;
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date);
  --FUNCTION SET_MORA(pdFecha in date) RETURN number;
  --FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
  --  RETURN VARCHAR2;
  PROCEDURE DATOS_CLIENTE(pdFechaPeriodo date, pvError out varchar2);
  PROCEDURE DATOS_CLIENTE2(pdFechaPeriodo date, pvError out varchar2);
  PROCEDURE NUM_FACTURA(pdFechCierrePeriodo date, pvError out varchar2);
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2;
  --FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)RETURN VARCHAR2;
  PROCEDURE CREA_INDICE(pdFechaPeriodo in date, pv_error out varchar2);
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pv_error out varchar2);
end;
/
CREATE OR REPLACE package body MKK_RPT_CARTERA is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  
  gvServicios    varchar2(32000);
  gvServicios2   varchar2(32000);

  PROCEDURE INSERTA_OPE_CUADRE is
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
  
  BEGIN
  
    -- se llena trunca la tabla de reportes 
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'truncate table ope_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade from co_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
  END;  
  
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(10000);
    lvServ2            varchar2(5000);
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
  
    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from cob_servicios
       where cargo2 = 0;
  
    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from cob_servicios
       where cargo2 = 1;
  
  BEGIN
    lvServ1 := '';
    for s in c_servicios1 loop
      lvServ1 := lvServ1 || s.nombre2 || '+';
    end loop;
    lvServ1 := substr(lvServ1, 1, length(lvServ1) - 1);
  
    lvServ2 := '';
    for t in c_servicios2 loop
      lvServ2 := lvServ2 || t.nombre2 || '+';
    end loop;
    lvServ2 := substr(lvServ2, 1, length(lvServ2) - 1);
  
    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 || ',' || ' TOTAL2 = ' ||
                   lvServ2 || ',' || ' TOTAL3 = credito_promocion,' ||
                   ' TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END;

  -------------------------------------------
  --             SET_SERVICIOS
  -------------------------------------------
  PROCEDURE SET_SERVICIOS(pdFecha in date) IS
  
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
  
    cursor cur_servicios is
      select /*+ rule */
       d.custcode,
       sum(otamt_revenue_gl) as Valor,
       replace(h.nombre2, '.', '_') nombre2
        from ordertrailer  A, --items de la factura
             mpusntab      b, --maestro de servicios
             orderhdr_all  c, --cabecera de facturas
             customer_all  d, -- maestro de cliente
             ccontact_all  f, -- información demografica de la cuente
             payment_all   g, --Forna de pago
             COB_SERVICIOS h, --formato de reporte
             COB_GRUPOS    I, --NOMBRE GRUPO
             COSTCENTER    j
       where a.otxact = c.ohxact and c.ohentdate = pdFecha and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and
             substr(otname,
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                    instr(otname,
                          '.',
                          instr(otname,
                                '.',
                                instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
             b.sncode and d.customer_id = f.customer_id and f.ccbill = 'X' and
             g.customer_id = d.customer_id and act_used = 'X' and
             h.servicio = b.sncode and h.ctactble = a.otglsale AND
             D.PRGCODE = I.PRGCODE and j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                a.otglsale,
                a.otxact,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE
      UNION
      SELECT /*+ rule */
       d.custcode, sum(TAXAMT_TAX_CURR) AS VALOR, h.nombre2
        FROM ORDERTAX_ITEMS A, --items de impuestos relacionados a la factura
             TAX_CATEGORY   B, --maestro de tipos de impuestos
             orderhdr_all   C, --cabecera de facturas
             customer_all   d,
             ccontact_all   f, --datos demograficos
             payment_all    g, -- forma de pago
             COB_SERVICIOS  h,
             COB_GRUPOS     I,
             COSTCENTER     J
       where c.ohxact = a.otxact and c.ohentdate = pdFecha and
            --c.ohentdate=to_date('24/11/2003','DD/MM/YYYY') and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and taxamt_tax_curr > 0 and
             A.TAXCAT_ID = B.TAXCAT_ID and d.customer_id = f.customer_id and
             f.ccbill = 'X' and g.customer_id = d.customer_id and
             act_used = 'X' and h.servicio = A.TAXCAT_ID and
             h.ctactble = A.glacode AND D.PRGCODE = I.PRGCODE and
             j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                A.glacode,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE;
  
  BEGIN
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT custcode, sum(valor), sum(nvl(descuento,0)), replace(nombre2, ''.'', ''_'') nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY custcode, nombre2';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lvCuenta, 24);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
   dbms_sql.define_column(source_cursor, 4,  lvNombre2, 40);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lvCuenta);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);
      dbms_sql.column_value(source_cursor, 4, lvNombre2);
      
      lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                     ' set '||lvNombre2||' = '||lvNombre2||'+'||lnValor||'-'||lnDescuento||
                     ' where cuenta = ''' ||lvCuenta|| '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII:=lII+1;
      if lII = 2000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);
    
/*    for i in cur_servicios loop
      lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                     ' set ' || i.nombre2 || ' = ' || i.nombre2 || '+' ||
                     to_char(i.valor) || ' where cuenta = ''' || i.custcode || '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII := lII + 1;
      if lII = 5000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
*/
  END;

/*  -------------------------------------------------------
  --      SET_MORA
  --      Función que coloca la letra -V en caso
  --      de que el saldo del ultimo periodo sea negativo
  -------------------------------------------------------
  FUNCTION SET_MORA(pdFecha in date) RETURN number is
    lnMes       number;
    lvSentencia varchar2(1000);
    lvMensErr   varchar2(1000);
  BEGIN
    lnMes       := to_number(to_char(pdFecha, 'MM'));
    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set mayorvencido = ''''''-V''' || ' where balance_' ||
                   lnMes || ' < 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
    return 1;
  END;

  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN
    --lnMesFinal:= to_number(to_char(pdFecha,'MM'));
    lvSentencia := '';
    --for lII in 1..lnMesFinal-1 loop
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || 'balance_' || lII || ',';
    end loop;
    lvSentencia := lvSentencia || 'balance_12';
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_BALANCES;*/

  PROCEDURE DATOS_CLIENTE(pdFechaPeriodo date, pvError out varchar2) IS
  lII       integer;
  
  cursor c_datos_cliente is
    select /*+ rule */
    d.customer_id,f.cclname,f.cssocialsecno,f.cccity,f.ccstate,
    f.ccname,f.ccline3||f.ccline4 dir2, f.cctn, f.cctn2, d.custcode
    from 
    customer_all d, -- maestro de cliente
    ccontact_all f, -- información demografica de la cuente
    payment_all g,  --Forna de pago
    costcenter j,
    trade_all h
    where
    d.customer_id  = f.customer_id and
    f.ccbill       = 'X' and
    d.customer_id  = g.customer_id and
    d.costcenter_id= j.cost_id and
    d.cstradecode  = h.tradecode(+);
  
  BEGIN

    lII := 0;
    FOR b IN c_datos_cliente LOOP
      execute immediate 'update MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||' set'||
                          ' id_cliente = :1,'||
                          ' apellidos = :2,'||
                          ' ruc = :3,'||
                          ' canton = :4,'||
                          ' provincia = :5,'||
                          ' direccion = :6,'||
                          ' direccion2 = :7,'||
                          ' telefono1 = :8,'||
                          ' telefono2 = :9'||
                          ' where cuenta = :10'
      using b.customer_id, b.cclname, b.cssocialsecno, b.cccity, b.ccstate, b.ccname, b.dir2, b.cctn, b.cctn2, b.custcode;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    END LOOP;
    commit;

  EXCEPTION
  WHEN OTHERS THEN
      pvError := 'datos_cliente: '||sqlerrm;
  END;
  
  
  PROCEDURE DATOS_CLIENTE2(pdFechaPeriodo date, pvError out varchar2) is
  
    lvSentencia      varchar2(2000);
    lvMensErr        varchar2(2000);
    lII              number;
  
    CURSOR c_registros IS
      select /*+ rule */
       d.customer_id,
       k.bank_id,
       k.bankname,
       nvl(i.producto, 'x') producto,
       nvl(j.cost_desc, 'x') cost_desc,
       g.bankaccno,
       g.valid_thru_date,
       g.accountowner,
       d.csactivated,
       d.cstradecode
      from customer_all    d,
             payment_all     g,
             read.COB_GRUPOS I,
             COSTCENTER      j,
             bank_all        k
      where d.customer_id = g.customer_id and act_used = 'X' and
            D.PRGCODE = I.PRGCODE and j.cost_id = d.costcenter_id and
            g.bank_id = k.bank_id;
  
  BEGIN
    
    lII := 0;
    -- se actualizan la forma de pago, el producto y centro de costo
    FOR b IN c_registros LOOP
      execute immediate 'update MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||' set'||
                     ' forma_pago = :1,'||
                     ' tarjeta_cuenta = :2,'||
                     ' fech_expir_tarjeta = :3,'||
                     ' tipo_cuenta = :4,'||
                     ' fech_aper_cuenta = :5,'||
                     ' grupo = :6'||
                     ' where id_cliente = :7'
      using b.bank_id, b.bankaccno, b.valid_thru_date, b.accountowner, b.csactivated, b.cost_desc, b.customer_id;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;      
    END LOOP;
    commit;

  EXCEPTION
  WHEN OTHERS THEN
      pvError := 'datos_cliente2: '||sqlerrm;
  END;
  
  --------------------------------------------
  -- NUM_FACTURA
  --------------------------------------------
  PROCEDURE NUM_FACTURA(pdFechCierrePeriodo date, pvError out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
  
    CURSOR FACTURA IS
      select customer_id, OHREFNUM
        from orderhdr_all
       where ohstatus = 'IN' and ohentdate = pdFechCierrePeriodo;
  
  Begin
  
    lII := 0;
    FOR b IN FACTURA LOOP
      execute immediate 'update MK_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy')||' set'||
                        ' num_factura = :1'||
                        ' where id_cliente = :2'
      using b.ohrefnum, b.customer_id;
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    END LOOP;
    commit;

  EXCEPTION
  WHEN OTHERS THEN
      pvError := 'num_factura: '||sqlerrm;
  END;
  
  --------------------------------------------
  -- SET_CAMPOS_SERVICIOS
  --------------------------------------------
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia      varchar2(32000);
    lvNombreServicio varchar2(100);

    cursor c_servicios is
    select column_name from user_tab_columns where table_name = 'RP_FACTURACION' 
    and column_name like 'Q_%'
    and column_name not like 'Q1_%';      
  
  BEGIN
    lvSentencia := '';
    gvServicios := '';
    gvServicios2:= '';
    for s in c_servicios loop
        lvSentencia := lvSentencia || substr(s.column_name,3) || ' NUMBER default 0, ';
        gvServicios := gvServicios||substr(s.column_name,3)||', ';
        gvServicios2:= gvServicios2||s.column_name||', ';
    end loop;
    
    /*for s in c_servicios1 loop
      lvSentencia := lvSentencia || s.nombre2 || ' NUMBER default 0, ';
    end loop;
    -- este campo divide los servicios de tipo no cargo
    -- de los tipo occ o cargos segun el reporte de detalle de clientes.
    lvSentencia := lvSentencia || ' TOTAL1 NUMBER default 0, ';
    for t in c_servicios2 loop
      lvSentencia := lvSentencia || t.nombre2 || ' NUMBER default 0, ';
    end loop;*/
    
    return(lvSentencia);
    
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_SERVICIOS;

/*  --------------------------------------------
  -- SET_CAMPOS_MORA
  --------------------------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN
    --lnMesFinal:= to_number(to_char(pdFecha,'MM'));
    lvSentencia := '';
    --for lII in 1..lnMesFinal loop
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_MORA;
*/
  PROCEDURE CREA_INDICE(pdFechaPeriodo in date, pv_error out varchar2) IS
  lvMensErr   varchar2(2000);
  lvSentencia varchar2(2000);
  leError     exception;
  BEGIN
     lvSentencia := 'create index IDX_MKCARTERA1_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (CUENTA)' ||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    
     lvSentencia := 'create index IDX_MKCARTERA2_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (ID_CLIENTE)' ||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    
  EXCEPTION
  WHEN leError THEN
       pv_error := lvMensErr;
  WHEN OTHERS THEN
       pv_error := 'crea_indice :'||sqlerrm;
  
  END;
  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    
    lvSentencia VARCHAR2(32000);
    lvMensErr   VARCHAR2(1000);
    leException exception;
    
  BEGIN
    lvSentencia := 'CREATE TABLE MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||
                   '( CUENTA                VARCHAR2(24),'||
                   '  TELEFONO              VARCHAR2(24),'||
                   '  ID_CLIENTE            NUMBER,' ||
                   '  PRODUCTO              VARCHAR2(30),' ||
                   '  CANTON                VARCHAR2(40),' ||
                   '  PROVINCIA             VARCHAR2(25),' ||
                   '  APELLIDOS             VARCHAR2(40),' ||
                   '  NOMBRES               VARCHAR2(60),' ||
                   '  RUC                   VARCHAR2(20),' ||
                   '  FORMA_PAGO            VARCHAR2(58),' ||
                   '  TARJETA_CUENTA        VARCHAR2(25),' ||
                   '  FECH_EXPIR_TARJETA    VARCHAR2(20),' ||
                   '  TIPO_CUENTA           VARCHAR2(40),' ||
                   '  FECH_APER_CUENTA      DATE,' ||
                   '  TELEFONO1             VARCHAR2(25),' ||
                   '  TELEFONO2             VARCHAR2(25),' ||
                   '  DIRECCION             VARCHAR2(70),' ||
                   '  DIRECCION2            VARCHAR2(200),' ||
                   '  GRUPO                 VARCHAR2(10),' ||
                   '  NUM_FACTURA           VARCHAR2(30),' ||
                   '  COMPANIA              NUMBER default 0,' ||                   
                   --MKK_RPT_CARTERA.set_campos_mora(pdFechaPeriodo, lvMensErr) ||
                   --'  TOTALVENCIDA          NUMBER default 0,' ||
                   --'  TOTALADEUDA           NUMBER default 0,' ||
                   --'  MAYORVENCIDO          VARCHAR2(10),' ||
                   MKK_RPT_CARTERA.set_campos_servicios(lvMensErr) ||
                   '  SALDOANTER            NUMBER default 0,' ||
                   '  TOTAL2                NUMBER default 0,' ||
                   --'  TOTAL3                NUMBER default 0,' ||
                   '  TOTAL_FACT            NUMBER default 0,' ||
                   '  SALDOANT              NUMBER default 0,' ||
                   '  PAGOSPER              NUMBER default 0,' ||
                   '  CREDTPER              NUMBER default 0,' ||
                   '  CMPER                 NUMBER default 0,' ||
                   '  CONSMPER              NUMBER default 0,' ||
                   '  DESCUENTO             NUMBER default 0)'||
                   '  tablespace DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '  storage  (initial 256K    next 256K' ||
                   '    minextents 1    maxextents 505' ||
                   '    pctincrease 0)';
                   
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;

    lvSentencia := 'create public synonym MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||' for sysadm.CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;
    
    lvSentencia := 'grant all on MK_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;
  
    return 1;
  
  EXCEPTION
    when leException then
         pv_error := lvMensErr;
         return 0;
    when others then
      pv_error := 'crea_tabla: '||sqlerrm;
      return 0;
  END CREA_TABLA;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pv_error out varchar2) IS
  
    -- variables
    lvSentencia    VARCHAR2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(2000);
    lnExito        NUMBER;
    leException    EXCEPTION;    
    lII            NUMBER; --contador para los commits
  
  BEGIN
  
    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''RP_FACTURACION''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
  
    -- si la tabla del cual consultamos existe entonces
    -- se procede a calcular los valores
    if lnExisteTabla = 1 then
    
      --busco la tabla de detalle de clientes
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''MK_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla = 1 then
      
         lvSentencia := 'drop table MK_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         if lvMensErr is not null then
            raise leException;
         end if;
      end if;
      
      lnExito := MKK_RPT_CARTERA.crea_tabla(pdFechCierrePeriodo,lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;
    
    
      --se insertan los datos generales y los saldos de la tabla
      --de proceso inicial de reportes
      --lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      lvSentencia := 'insert  into MK_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy')||' NOLOGGING ' ||
                     '(cuenta, telefono, producto, nombres, compania,'||substr(gvServicios,1,length(gvServicios)-2)||') '||
                     'select cuenta, telefono, producto, nombres, decode(region,''GYE'',1,2),'||substr(gvServicios2,1,length(gvServicios2)-2)||' from RP_FACTURACION';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      commit;
      
      -- se crea el indice en la tabla a este nivel debido a que se ingresa informacion
      -- con la opcion NOLOGGING y necesita no tener indices por rapidez
      MKK_RPT_CARTERA.crea_indice(pdFechCierrePeriodo,lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;      
      
      MKK_RPT_CARTERA.datos_cliente(pdFechCierrePeriodo,lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;
      
      MKK_RPT_CARTERA.datos_cliente2(pdFechCierrePeriodo,lvMensErr);      
      if lvMensErr is not null then
         raise leException;
      end if;
      
      MKK_RPT_CARTERA.num_factura(pdFechCierrePeriodo,lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;
    
    end if;

  EXCEPTION
    WHEN leException then
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pv_error := lvMensErr;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pv_error := sqlerrm;
  
  END MAIN;

end;
/

