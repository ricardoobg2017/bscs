CREATE OR REPLACE PACKAGE RC_TRX_SUSPENSION_BSCS_DTH IS

/*******************************************************************************************************
  * [8693]       RELOJ DE COBRANZAS DTH
  * Autor :     IRO JUAN ROMERO
  * Creacion:   13/08/2013
  * OBJETIVO:   Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                de la tabla PROFILE_SERVICE_ADV_CHARGE para cada co_id, 
                evitando errores en la facturacion.
 ******************************************************************************************************/
 
 /*******************************************************************************************************
  * [8693]            RELOJ DE COBRANZAS DTH
  * Autor :           IRO JUAN ROMERO
  * Modificacion:     26/08/2013
  * OBJETIVO:         Adicion de un parametro para verificar que el estado actual en Bscs de la cuenta,
                      sea igual al estado enviado, ya sea este suspension o inactivacion.(s,d)
 ******************************************************************************************************/  
 
   /*******************************************************************************************************
  * [8693]            RELOJ DE COBRANZAS DTH
  * Autor :           IRO JUAN ROMERO
  * Modificacion:     10/10/2013
  * OBJETIVO:         Realizar la validacion correspondiente para que, si no se encuentran registros
                      en la tabla profile_service_adv_charge para algun co_id,esto no sea devuelto 
                      como error y por lo tanto no se reintente la transacci�n.
 ******************************************************************************************************/    
 /*******************************************************************************************************
  * [8694]            TRANSACCIONES POSTVENTA DTH - FASE IV
  * Autor :           CLS CHRISTIAN MARQUEZ (CLS CMA)
  * Modificacion:     31/01/2014
  * OBJETIVO:         Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                      de la tabla PROFILE_SERVICE_ADV_CHARGE por un SNCODE ESPECIFICO DEL co_id, 
                      evitando errores en la facturacion en las TRX Programadas.
 ******************************************************************************************************/
 /*******************************************************************************************************
  * [10547]           JGO_10547_CIMA_MOR_atencion_informes
  * Lider PDS         CIM MALULY ORBES
  * Autor :           CIM A.GONZALEZ
  * Modificacion:     23/10/2015
  * OBJETIVO:         Realizar la validacion para que no se actualize los servicios no prorrateables en 
                      la tabla profile_service_adv_charge
 ******************************************************************************************************/                            
                               
PROCEDURE RC_ACTUALIZA_VALOR_SNCODE (Pv_co_id            IN NUMBER,
                                     Pv_per_ini          IN VARCHAR2,
                                     Pv_per_fin          IN VARCHAR2,
                                     Pv_estatus_bscs     IN VARCHAR2,
                                     Pv_response         out NUMBER,
                                     Pv_etapa            out varchar2,
                                     Pv_detalle          out varchar2);                              
                                                               
                               
PROCEDURE RC_BITACORIZA_SUSP_BSCS_DTH(Pv_id_proceso_scp   varchar2,
                                      Pv_tipo_error       number,
                                      Pv_notifica         varchar2,
                                      Pv_mensaje_app      varchar2,
                                      Pv_mensaje_tec      varchar2);

PROCEDURE RC_ACTUALIZA_VALOR_X_SNCODE(pv_co_id        in number,
                                      pv_per_ini      in varchar2,
                                      pv_per_fin      in varchar2,
                                      pv_estatus_bscs in varchar2,
                                      pv_trama_sncode in varchar2,
                                      pv_response     out number,
                                      pv_etapa        out varchar2,
                                      pv_detalle      out varchar2);
                                      

END RC_TRX_SUSPENSION_BSCS_DTH;
/
CREATE OR REPLACE PACKAGE BODY RC_TRX_SUSPENSION_BSCS_DTH IS

  /*******************************************************************************************************
  * [8693] RELOJ DE COBRANZAS DTH
  * Autor :     IRO JUAN ROMERO
  * Creacion:   13/08/2013
  * OBJETIVO:   Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                de la tabla PROFILE_SERVICE_ADV_CHARGE para cada co_id, 
                evitando errores en la facturacion.
                
  ******************************************************************************************************/
  
  /*******************************************************************************************************
  * [8693]            RELOJ DE COBRANZAS DTH
  * Autor :           IRO JUAN ROMERO
  * Modificacion:     26/08/2013
  * OBJETIVO:         Adicion de un parametro para verificar que el estado actual en Bscs de la cuenta,
                      sea igual al estado enviado, ya sea este suspension o inactivacion.(s,d)
 ******************************************************************************************************/ 
 
  /*******************************************************************************************************
  * [8693]            RELOJ DE COBRANZAS DTH
  * Autor :           IRO JUAN ROMERO
  * Modificacion:     10/10/2013
  * OBJETIVO:         Realizar la validacion correspondiente para que, si no se encuentran registros
                      en la tabla profile_service_adv_charge para algun co_id,esto no sea devuelto 
                      como error y por lo tanto no se reintente la transacci�n.
 ******************************************************************************************************/ 
  /*******************************************************************************************************
  * [10547]           JGO_10547_CIMA_MOR_atencion_informes  - Prorrateo DTH
  * Lider PDS         CIM MALULY ORBES
  * Autor :           CIM A.GONZALEZ
  * Modificacion:     23/10/2015
  * OBJETIVO:         Realizar la validacion para que no se actualize los servicios no prorrateables en 
                      la tabla profile_service_adv_charge
 ******************************************************************************************************/ 
   /*******************************************************************************************************
  * [11417]           CONTROL DE ACTUALIZACION EN LA TABLA profile_service_adv_charge
  * Autor :           CIM MALULY ORBES / CIM A.GONZALEZ
  * Modificacion:     04/08/2017
  * OBJETIVO:         Realizar la validacion para que no se actualize varias veces los valores de
                      la tabla profile_service_adv_charge
 ******************************************************************************************************/ 
  /*******************************************************************************************************
  * [12324]           Ajuste Prorrateo Cambio de Plan DTH           
  * LIDER:            SIS XAVIER TRIVI�O/SUD RICHARD RIVERA
  * MODIFICADO POR :  SUD KATIUSKA BARRETO
  * OBJETIVO:         SE MODIFICA VARIABLES LV_PRORRATEO, LN_TMCODE_ULT PARA QUE NO TOME VALORES
  *                   ANTERIORES Y SE ACTUALICEN EN CADA CONSULTA QUE REALICE.
 ******************************************************************************************************/ 
/*=====PROCEDIMIENTO PARA ACTUALIZAR VALORES EN LA TABLA PROFILE_SERVICE_ADV_CHARGE EN BSCS=====*/  
  
PROCEDURE RC_ACTUALIZA_VALOR_SNCODE (Pv_co_id            IN NUMBER,
                                     Pv_per_ini          IN VARCHAR2,
                                     Pv_per_fin          IN VARCHAR2,
                                     Pv_estatus_bscs     IN VARCHAR2,-- 26/08/2013 Iro Juan Romero
                                     Pv_response         out NUMBER,
                                     Pv_etapa            out varchar2,
                                     Pv_detalle          out varchar2)IS
                                     
                                                     
-- CURSOR PARA OBTENER EL ESTADO DEL CO_ID EN BSCS                                                     
CURSOR c_status_bscs (Cv_co_id number,Cv_status_bscs varchar2) IS-- 26/08/2013 Iro Juan Romero
   SELECT A.ch_status FROM CONTRACT_HISTORY A
   WHERE A.CO_ID=Cv_co_id
   AND A.CH_SEQNO=(SELECT MAX(B.CH_SEQNO) FROM CONTRACT_HISTORY  B
                   WHERE B.CO_ID=Cv_co_id)
   AND A.CH_STATUS=Cv_status_bscs; -- 26/08/2013 Iro Juan Romero
---------------------------------------------------------
--INI [10547]     CIM A.GONZALEZ
--CURSOR PARA validar los cambios que el proceso solo realize el calculo a los features prorrateables
CURSOR C_PARAMETRO (LN_PARAMETRO NUMBER)IS
select VALOR from gv_parametros WHERE ID_TIPO_PARAMETRO = LN_PARAMETRO;-- 22/10/2015 Cim A.Gonzalez 
--CURSOR PARA OBTENER�el TMcode del co_id
cursor C_TMCODE(cv_code number)is
select tmcode from contract_all where co_id = cv_code;-- 22/10/2015 Cim A.Gonzalez 
-- cursor para identificar por cada sn_code si es prorrateable o no prorrateable
CURSOR c_prorrateo(cv_code number, CV_TMCODE NUMBER,LV_CV_STATUS varchar2)is
select proind from mpulktmb where
 sncode = cv_code and 
 tmcode = CV_TMCODE and
 vscode = (select max(vscode) from mpulktmb
           where sncode = cv_code and tmcode = CV_TMCODE)
and status=LV_CV_STATUS;-- 22/10/2015 Cim A.Gonzalez 
--FIN [10547]     CIM A.GONZALEZ
--INI [11417]     CIM A.GONZALEZ 04/08/2017
--CURSOR PARA CONTROLAR EL PROCESO DE prorrateO --  Cim A.Go
CURSOR C_CONT_PRO (LV_CONTROR_PRORRATEO VARCHAR2)IS
select VALOR from gv_parametros WHERE ID_PARAMETRO = LV_CONTROR_PRORRATEO ;

CURSOR C_CONT_PROC_PRO (cv_code NUMBER,CV_SNCODE NUMBER ) IS
SELECT COUNT(*)  FROM  PROFILE_SERVICE_ADV_CHARGE_h 
WHERE CO_ID = cv_code
AND SNCODE = CV_SNCODE
AND ADV_CHARGE_END_DATE = TO_DATE(Pv_per_fin,'DD/MM/RRRR') ;
--FIN [11417]     CIM A.GONZALEZ 04/08/2017

---------------------------------------------------------
/*==============Variables locales==============*/

Ln_dias_ciclo                          number;
Ln_dias_cobrar                         number; 
Lb_Found                               boolean;
Le_error_parametro                     exception;-- 10/10/2013 Iro Juan Romero
Ln_valor_ADV_CHARGE                    number(12,5);
Ln_valor_ADV_CHARGE_ITEM_PRICE         number(12,5);
Lv_etapa                               varchar2(100);
Lv_status_bscs                         varchar2(100);
Lv_mensaje_error                       varchar2(4000):=NULL;
Ln_ADV_CHARGE                          number;
Ln_ADV_CHARGE_ITEM_PRICE               number;
Ln_num_reg_act                         number;
LE_ERROR_STATUS                        EXCEPTION;-- 26/08/2013 Iro Juan Romero
LV_CV_STATUS                           varchar2(2):='P';-- PARA VER EL ESTADO DE PRODUCION
LN_TMCODE_ULT                          NUMBER;--22/10/2015 Cim A.Gonzalez
LV_PRORRATEO                           VARCHAR2(2);-- 22/10/2015 Cim A.Gonzalez
LN_PARAMETRO                           NUMBER :=10547;-- 22/10/2015 Cim A.Gonzalez
L_BANDERA                              varchar2(1);-- 22/10/2015 Cim A.Gonzalez
LV_CONTROL_PRORRATEO                   Varchar2(100):='BAN_CONTROL_PROCESO_PRO';--  Cim A.Go
LV_BAN_CONT_PRO                        varchar2(1);
LN_CON_PRO                             number;

--Variables de scp_dat
lv_id_proceso_scp          varchar2(100):='SUSPENSION_DTH';
Ln_parametro_dias          varchar2(4000);
lv_descripcion_scp         varchar2(500);
ln_error_scp               number:=0;
lv_error_scp               varchar2(500);

-------------------------------------------------

BEGIN
  
                           
    -------------------------Recuperar el estatus en bscs del co_id
    Lv_etapa:='Recuperaci�n del estatus actual en bscs del contrato';
    open c_status_bscs(Pv_co_id,(lower(Pv_estatus_bscs)));-- 26/08/2013 Iro Juan Romero
    fetch c_status_bscs
    into Lv_status_bscs;
    Lb_Found := c_status_bscs%FOUND;
    close c_status_bscs;
    
    IF NOT Lb_Found THEN
      Lv_mensaje_error:= 'El co_id< '||Pv_co_id||' > no tiene en la tabla contract_history, un estatus final que coincida con el recibido< '||(lower(Pv_estatus_bscs))||' >';-- 25/08/2013 Iro Juan Romero
      RAISE LE_ERROR_STATUS;-- 26/08/2013 Iro Juan Romero
    END IF;

    Lv_etapa:='Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
    Ln_num_reg_act:=0;
    FOR I IN (select a.ADV_CHARGE, 
                     a.ADV_CHARGE_ITEM_PRICE,
                     a.SNCODE
             from SYSADM.PROFILE_SERVICE_ADV_CHARGE a      
             where a.co_id=Pv_co_id) LOOP
             --INI [10547]     CIM A.GONZALEZ
             OPEN C_PARAMETRO (LN_PARAMETRO);
             FETCH C_PARAMETRO
             INTO L_BANDERA;
             CLOSE C_PARAMETRO;
             
             -- INI [11417]     CIM A.GONZALEZ 04/08/2017
             OPEN C_CONT_PRO (LV_CONTROL_PRORRATEO);
             FETCH C_CONT_PRO
             INTO LV_BAN_CONT_PRO;
             CLOSE C_CONT_PRO;
             -- FIN [11417]
             
             
             IF NVL(L_BANDERA,'N')='S' THEN -- Bandera q da paso al nuevo esquema solo realiza el calculo a pos prorrateables
             
             LN_TMCODE_ULT:=null;--12324
             open C_TMCODE (Pv_co_id);
             fetch C_TMCODE
             into LN_TMCODE_ULT;-- obtenemos el tmcode de ese co_id
             CLOSE C_TMCODE;
             
             LV_PRORRATEO:=null; --12324           
             open c_prorrateo(I.SNCODE,LN_TMCODE_ULT,LV_CV_STATUS);
             FETCH c_prorrateo
             INTO LV_PRORRATEO;-- obtenemos si es prorratable o no prorrateable
             CLOSE c_prorrateo;
             
             --consulto si es prorrateable ingresa a modificar los campos en la tabla PROFILE_SERVICE_ADV_CHARGE
             IF NVL(LV_PRORRATEO,'N') = 'Y' THEN --  Y : prorrateable N: no prorrateable
             
          Lv_etapa:='Recuperacion del valor del parametro <DIAS_ANT_FIN_CICLO>';
          scp_dat.sck_api.scp_parametros_procesos_lee('DIAS_ANT_FIN_CICLO',
                                                lv_id_proceso_scp,
                                                Ln_parametro_dias,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
         
         If ln_error_scp <> 0 Then
           Lv_mensaje_error:= lv_error_scp;
           raise Le_error_parametro;-- 10/10/2013 Iro Juan Romero
         End If;
      
         Ln_ADV_CHARGE:=I.ADV_CHARGE;
         Ln_ADV_CHARGE_ITEM_PRICE:=I.ADV_CHARGE_ITEM_PRICE;
            
         Lv_etapa:='Calculo de la variable <Ln_dias_ciclo>';
         Ln_dias_ciclo:= TO_DATE(Pv_per_fin,'DD/MM/RRRR') - TO_DATE(Pv_per_ini,'DD/MM/RRRR')+1;
                       
         Lv_etapa:='Calculo de valor para las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         Ln_dias_cobrar:= Ln_dias_ciclo + to_number(Ln_parametro_dias);
         Ln_valor_ADV_CHARGE:=round((Ln_ADV_CHARGE/Ln_dias_ciclo)*Ln_dias_cobrar,5);
         Ln_valor_ADV_CHARGE_ITEM_PRICE:=round((Ln_ADV_CHARGE_ITEM_PRICE/Ln_dias_ciclo)*Ln_dias_cobrar,5);           
         Lv_etapa:='Actualizacion de las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         
         -- INI [11417]     CIM A.GONZALEZ 04/08/2017
          IF NVL(LV_BAN_CONT_PRO,'N')='S' THEN
               open C_CONT_PROC_PRO (Pv_co_id,I.SNCODE);
               FETCH C_CONT_PROC_PRO
               INTO LN_CON_PRO;--- SI EXTISTE YA UN REGISTRO EN LA MISMA FECHA 
               CLOSE C_CONT_PROC_PRO;
               
                if LN_CON_PRO >= 1 then    
               -- YA PASO EL PROCESO      
               Lv_etapa:='YA SE EJECUTO EL PROCESO DEL ADV_CHARGE AL MENOS 1 VEZ' ||I.SNCODE ;
               ELSE
                 update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
                 set A.ADV_CHARGE=Ln_valor_ADV_CHARGE,A.ADV_CHARGE_ITEM_PRICE=Ln_valor_ADV_CHARGE_ITEM_PRICE
                 where A.co_id=Pv_co_id
                 and A.SNCODE=I.SNCODE;               
              END IF; 

         ELSE
         update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
         set A.ADV_CHARGE=Ln_valor_ADV_CHARGE,A.ADV_CHARGE_ITEM_PRICE=Ln_valor_ADV_CHARGE_ITEM_PRICE
         where A.co_id=Pv_co_id
         and A.SNCODE=I.SNCODE;
         END IF; 

         ---
         END IF;-- termina consulta de pprorrateo
           ELSE-- si la bandera no es S se mantiene el proceso anterior
                  Lv_etapa:='Recuperacion del valor del parametro <DIAS_ANT_FIN_CICLO>';
          scp_dat.sck_api.scp_parametros_procesos_lee('DIAS_ANT_FIN_CICLO',
                                                lv_id_proceso_scp,
                                                Ln_parametro_dias,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
         
         If ln_error_scp <> 0 Then
           Lv_mensaje_error:= lv_error_scp;
           raise Le_error_parametro;-- 10/10/2013 Iro Juan Romero
         End If;
      
         Ln_ADV_CHARGE:=I.ADV_CHARGE;
         Ln_ADV_CHARGE_ITEM_PRICE:=I.ADV_CHARGE_ITEM_PRICE;
            
         Lv_etapa:='Calculo de la variable <Ln_dias_ciclo>';
         Ln_dias_ciclo:= TO_DATE(Pv_per_fin,'DD/MM/RRRR') - TO_DATE(Pv_per_ini,'DD/MM/RRRR')+1;
                       
         Lv_etapa:='Calculo de valor para las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         Ln_dias_cobrar:= Ln_dias_ciclo + to_number(Ln_parametro_dias);
         Ln_valor_ADV_CHARGE:=round((Ln_ADV_CHARGE/Ln_dias_ciclo)*Ln_dias_cobrar,5);
         Ln_valor_ADV_CHARGE_ITEM_PRICE:=round((Ln_ADV_CHARGE_ITEM_PRICE/Ln_dias_ciclo)*Ln_dias_cobrar,5);           
         Lv_etapa:='Actualizacion de las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
         update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
         set A.ADV_CHARGE=Ln_valor_ADV_CHARGE,A.ADV_CHARGE_ITEM_PRICE=Ln_valor_ADV_CHARGE_ITEM_PRICE
         where A.co_id=Pv_co_id
         and A.SNCODE=I.SNCODE;
         
          END IF;
         Ln_num_reg_act:=Ln_num_reg_act+1;      
    END LOOP;
    if Ln_num_reg_act <= 0 then
      Lv_etapa:='Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
      Lv_mensaje_error:= 'No se encontraron registros para el co_id <'||Pv_co_id||'>';
      raise LE_ERROR_STATUS;-- 10/10/2013 Iro Juan Romero
    end if;       
    commit;
    Pv_response:=0;
    Pv_etapa:='RC_TRX_SUSPENSION_BSCS_DTH.RC_ACTUALIZA_VALOR_SNCODE';
    Pv_detalle:='Procesamiento exitoso';
EXCEPTION
    WHEN  LE_ERROR_STATUS THEN
      Pv_response:=0;
      Pv_etapa:='RC_ACTUALIZA_VALOR_SNCODE: '||Lv_etapa;
      Pv_detalle:=Lv_mensaje_error;
      --Bitacorizando el error suscitado
      RC_BITACORIZA_SUSP_BSCS_DTH(Lv_id_proceso_scp,1,'N',Lv_etapa,Lv_mensaje_error);
    WHEN  Le_error_parametro THEN-- 10/10/2013 Iro Juan Romero
      Pv_response:=1;
      Pv_etapa:='RC_ACTUALIZA_VALOR_SNCODE: '||Lv_etapa;
      Pv_detalle:=Lv_mensaje_error;
      --Bitacorizando el error suscitado
      RC_BITACORIZA_SUSP_BSCS_DTH(Lv_id_proceso_scp,2,'S',Lv_etapa,Lv_mensaje_error);  
    WHEN OTHERS THEN
      Pv_response:=1;
      Pv_etapa:='RC_ACTUALIZA_VALOR_SNCODE: '||Lv_etapa;
      Pv_detalle:=sqlerrm;
      rollback;
      --Bitacorizando el error suscitado
      RC_BITACORIZA_SUSP_BSCS_DTH(Lv_id_proceso_scp,3,'S',Lv_etapa,sqlerrm);
    
END RC_ACTUALIZA_VALOR_SNCODE;  
  
/*=========PROCEDIMIENTO PARA BITACORIZAR ERRORES EN EL PAQUETE RELOJ.RC_TRX_SUSPENSION_BSCS_DTH=====*/

PROCEDURE RC_BITACORIZA_SUSP_BSCS_DTH(Pv_id_proceso_scp   varchar2,
                                      Pv_tipo_error       number,
                                      Pv_notifica         varchar2,
                                      Pv_mensaje_app      varchar2,
                                      Pv_mensaje_tec      varchar2) IS

-------Variables

  Lv_tipo_error                       number:=Pv_tipo_error;
  ln_total_registros_scp              NUMBER:=1;
  ln_id_bitacora_scp                  number:=0;
  ln_id_detalle_bitacora_scp          number:=0;
  ln_error_scp                        number:=0;
  lv_error_scp                        varchar2(4000);
  lv_mensaje_apl_scp                  varchar2(4000):=Pv_mensaje_app; 
  LV_MENSAJE                          varchar2(4000):=Pv_mensaje_tec;                                   
  Lv_id_proceso_scp                   varchar2(30):=Pv_id_proceso_scp; 
  Lv_notifica                         varchar2(1):=Pv_notifica;
  lv_unidad_registro_scp              varchar2(50):='Decodificador';
  lv_referencia_scp                   varchar2(50):='RELOJ.RC_TRX_SUSPENSION_BSCS_DTH.PCK';  
  Le_excepcion                        exception;                                    
        
  BEGIN
   --Inicia la bitacora
    scp_dat.sck_api.scp_bitacora_procesos_ins(Lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    IF ln_error_scp <> 0 THEN

      raise Le_excepcion;

    END IF;

    --Inserta el mensaje de error en la bitacora
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          LV_MENSAJE,
                                          Null,
                                          Lv_tipo_error,
                                          null,
                                          null,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          Lv_notifica,
                                          ln_id_detalle_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
                                          
     IF ln_error_scp <> 0 THEN
       
      raise Le_excepcion;

    END IF;                                     

    --Finaliza la bitacora
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
                                          
     IF ln_error_scp <> 0 THEN

      raise Le_excepcion;

    END IF;                                     
                                          

  EXCEPTION
    WHEN Le_excepcion then
      null;
    when others then
      null;  

END RC_BITACORIZA_SUSP_BSCS_DTH;
/*******************************************************************************************************
  * [8694]            TRANSACCIONES POSTVENTA DTH - FASE IV
  * Autor :           CLS CHRISTIAN MARQUEZ (CLS CMA)
  * Creacion:         31/01/2014
  * OBJETIVO:         Realizar la actualizacion de los valores en los campos ADV_CHARGE y ADV_CHARGE_ITEM_PRICE
                      de la tabla PROFILE_SERVICE_ADV_CHARGE por un SNCODE ESPECIFICO DEL co_id, 
                      evitando errores en la facturacion en las TRX Programadas.
 ******************************************************************************************************/    
  /*******************************************************************************************************
  * [10547]           JGO_10547_CIMA_MOR_atencion_informes  -  Prorrateo DTH
  * Lider PDS         CIM MALULY ORBES
  * Autor :           CIM A.GONZALEZ
  * Modificacion:     23/10/2015
  * OBJETIVO:         Realizar la validacion para que no se actualize los servicios no prorrateables en 
                      la tabla profile_service_adv_charge
 ******************************************************************************************************/ 
/*******************************************************************************************************
  * [12324]           Ajuste Prorrateo Cambio de Plan DTH           
  * LIDER:            SIS XAVIER TRIVI�O/SUD RICHARD RIVERA
  * MODIFICADO POR :  SUD KATIUSKA BARRETO
  * OBJETIVO:         SE MODIFICA VARIABLES LV_PRORRATEO, LN_TMCODE_ULT PARA QUE NO TOME VALORES
  *                   ANTERIORES Y SE ACTUALICEN EN CADA CONSULTA QUE REALICE.
 ******************************************************************************************************/ 
 --INI CLS CMA
PROCEDURE RC_ACTUALIZA_VALOR_X_SNCODE(pv_co_id        in number,
                                      pv_per_ini      in varchar2,
                                      pv_per_fin      in varchar2,
                                      pv_estatus_bscs in varchar2,
                                      pv_trama_sncode in varchar2,
                                      pv_response     out number,
                                      pv_etapa        out varchar2,
                                      pv_detalle      out varchar2) is

  -- CURSOR PARA OBTENER EL ESTADO DEL CO_ID EN BSCS                                                     
  cursor c_status_bscs(cv_co_id number, cv_status_bscs varchar2) is -- 26/08/2013 iro juan romero
    select a.ch_status
      from contract_history a
     where a.co_id = cv_co_id
       and a.ch_seqno = (select max(b.ch_seqno)
                           from contract_history b
                          where b.co_id = cv_co_id)
       and a.ch_status = cv_status_bscs;

  cursor c_coid_sncode(cn_co_id number, cn_sncode number) is
    select a.adv_charge, a.adv_charge_item_price, a.sncode
      from sysadm.profile_service_adv_charge a
     where a.co_id = cn_co_id
       and a.sncode = cn_sncode;
---------------------------------------------------------
--INI [10547]     CIM A.GONZALEZ
--CURSOR PARA validar los cambios que el proceso solo realize el calculo a los features prorrateables
CURSOR C_PARAMETRO (LN_PARAMETRO NUMBER)IS
select VALOR from gv_parametros WHERE ID_TIPO_PARAMETRO = LN_PARAMETRO;-- 22/10/2015 Cim A.Gonzalez 
--CURSOR PARA OBTENER�el TMcode del co_id
cursor C_TMCODE(cv_code number)is
select tmcode from contract_all where co_id = cv_code;-- 22/10/2015 Cim A.Gonzalez 
-- cursor para identificar por cada sn_code si es prorrateable o no prorrateable
CURSOR c_prorrateo(cv_code number, CV_TMCODE NUMBER,LV_CV_STATUS varchar2)is
select proind from mpulktmb where
 sncode = cv_code and 
 tmcode = CV_TMCODE and
 vscode = (select max(vscode) from mpulktmb
           where sncode = cv_code and tmcode = CV_TMCODE)
and status=LV_CV_STATUS;-- 22/10/2015 Cim A.Gonzalez 
--FIN [10547]     CIM A.GONZALEZ
--INI [11417]     CIM A.GONZALEZ 04/08/2017
--CURSOR PARA CONTROLAR EL PROCESO DE prorrateO --  Cim A.Go
CURSOR C_CONT_PRO (LV_CONTROL_PRORRATEO VARCHAR2)IS
select VALOR from gv_parametros WHERE ID_PARAMETRO = LV_CONTROL_PRORRATEO ;--  Cim A.Go

CURSOR C_CONT_PROC_PRO (cv_code NUMBER,CV_SNCODE NUMBER ) IS
SELECT COUNT(*)  FROM  PROFILE_SERVICE_ADV_CHARGE_h 
WHERE CO_ID = cv_code
AND SNCODE = CV_SNCODE
AND ADV_CHARGE_END_DATE = TO_DATE(Pv_per_fin,'DD/MM/RRRR') ;
--FIN [11417]     CIM A.GONZALEZ 04/08/2017
---------------------------------------------------------        
  Ln_dias_ciclo  number;
  Ln_dias_cobrar number;
  Lb_Found       boolean;
  Le_error_parametro exception;
  Ln_valor_ADV_CHARGE            number(12, 5);
  Ln_valor_ADV_CHARGE_ITEM_PRICE number(12, 5);
  Lv_etapa                       varchar2(100);
  Lv_status_bscs                 varchar2(100);
  lv_trama                       varchar2(4000);
  lv_separador                   varchar2(4);
  Lv_mensaje_error               varchar2(4000) := NULL;
  Ln_ADV_CHARGE                  number;
  Ln_ADV_CHARGE_ITEM_PRICE       number;
  Ln_num_reg_act                 number;
  ln_sncode                      number;
  LE_ERROR_STATUS EXCEPTION;
  Ln_parametro_dias varchar2(4000);
  LV_CV_STATUS                           varchar2(2):='P';-- PARA VER EL ESTADO DE PRODUCION
  LN_TMCODE_ULT                          NUMBER;--22/10/2015 Cim A.Gonzalez
  LV_PRORRATEO                           VARCHAR2(2);-- 22/10/2015 Cim A.Gonzalez
  LN_PARAMETRO                           NUMBER :=10548;-- 22/10/2015 Cim A.Gonzalez
  L_BANDERA                              varchar2(1);-- 22/10/2015 Cim A.Gonzalez
  ----
LV_CONTROL_PRORRATEO                   Varchar2(100):='BAN_CONTROL_PROCESO_PRO';--  Cim A.Go
LV_BAN_CONT_PRO                        varchar2(1);
LN_CON_PRO                             number;
  ----

  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp          number := 0;
  ln_id_detalle_scp           number;
  ln_total_registros_scp      number := 0;
  lv_id_proceso_scp           varchar2(100) := 'SUSPENSION_DTH';
  lv_referencia_scp           varchar2(100) := 'RC_TRX_SUSPENSION_BSCS_DTH.RC_ACTUALIZA_VALOR_X_SNCODE';
  lv_unidad_registro_scp      varchar2(30) := TO_CHAR(null);
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);
  lv_descripcion_scp          varchar2(4000);
  ---------------------------------------------------------------

BEGIN
  ln_total_registros_scp := 1;
  lv_unidad_registro_scp := pv_co_id;

  --SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: CODIGO GENERADO AUTOM�TICAMENTE. REGISTRO DE BITACORA DE EJECUCI�N
  ----------------------------------------------------------------------------
  scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                            lv_referencia_scp,
                                            null,
                                            null,
                                            null,
                                            null,
                                            ln_total_registros_scp,
                                            lv_unidad_registro_scp,
                                            ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
  ----------------------------------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'INICIO PROCESO RC_ACTUALIZA_VALOR_X_SNCODE';
  lv_mensaje_tec_scp := pv_trama_sncode;
  lv_mensaje_acc_scp := pv_co_id;
  scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            pv_co_id,
                                            pv_per_ini,
                                            pv_per_fin,
                                            null,
                                            null,
                                            'N',
                                            ln_id_detalle_scp,
                                            ln_error_scp,
                                            lv_error_scp);

  ----------------------------------------------------------------------------
  -------------------------Recuperar el estatus en bscs del co_id
  Lv_etapa := 'Recuperaci�n del estatus actual en bscs del contrato';
  open c_status_bscs(Pv_co_id, (lower(Pv_estatus_bscs)));
  fetch c_status_bscs
    into Lv_status_bscs;
  Lb_Found := c_status_bscs%FOUND;
  close c_status_bscs;

  IF NOT Lb_Found THEN
    Lv_mensaje_error := 'El co_id< ' || Pv_co_id ||
                        ' > no tiene en la tabla contract_history, un estatus final que coincida con el recibido< ' ||
                        (lower(Pv_estatus_bscs)) || ' >'; -- 25/08/2013 Iro Juan Romero
    ----------------------------------------------------------------------
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := Lv_etapa;
    lv_mensaje_tec_scp := Lv_mensaje_error;
    lv_mensaje_acc_scp := NULL;
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              pv_co_id,
                                              pv_per_ini,
                                              pv_per_fin,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    ----------------------------------------------------------------------------
    RAISE LE_ERROR_STATUS;
  END IF;

  Lv_etapa       := 'Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
  Ln_num_reg_act := 0;
  ----------------------------------------------------------------------
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := Lv_etapa;
  lv_mensaje_tec_scp := pv_trama_sncode;
  lv_mensaje_acc_scp := pv_co_id;
  scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            pv_co_id,
                                            pv_per_ini,
                                            pv_per_fin,
                                            null,
                                            null,
                                            'N',
                                            ln_id_detalle_scp,
                                            ln_error_scp,
                                            lv_error_scp);

  ----------------------------------------------------------------------------

  lv_separador := ',';
  if substr(pv_trama_sncode, -1, 1) <> lv_separador then
    lv_trama := pv_trama_sncode || lv_separador;
  else
    lv_trama := pv_trama_sncode;
  end if;
  loop
    exit when length(lv_trama) <= 0 or lv_trama is null;
    ln_sncode := to_number(substr(lv_trama,
                                  0,
                                  instr(lv_trama, lv_separador) - 1));
    lv_trama  := substr(lv_trama, instr(lv_trama, lv_separador) + 1);
----**************
   --INI [10547]     CIM A.GONZALEZ
             OPEN C_PARAMETRO (LN_PARAMETRO);
             FETCH C_PARAMETRO
             INTO L_BANDERA;
             CLOSE C_PARAMETRO;
             
             --INI [11417]     CIM A.GONZALEZ 04/08/2017
             OPEN C_CONT_PRO (LV_CONTROL_PRORRATEO);
             FETCH C_CONT_PRO
             INTO LV_BAN_CONT_PRO;
             CLOSE C_CONT_PRO;
             --FIN [11417]
             
             IF NVL(L_BANDERA,'N')='S' THEN -- namdera q da paso al nuevo esquema solo realiza el calculo a pos prorrateables
             
             LN_TMCODE_ULT:=NULL;--12324
             open C_TMCODE (Pv_co_id);
             fetch C_TMCODE
             into LN_TMCODE_ULT;-- obtenemos el tmcode de ese co_id
             CLOSE C_TMCODE;
             
             LV_PRORRATEO:=NULL;--12324            
             open c_prorrateo(ln_sncode,LN_TMCODE_ULT,LV_CV_STATUS);
             FETCH c_prorrateo
             INTO LV_PRORRATEO;-- obtenemos si es prorratable o no prorrateable
             CLOSE c_prorrateo;

             --consulto si es prorrateable ingresa a modificar los campos en la tabla PROFILE_SERVICE_ADV_CHARGE
            IF NVL(LV_PRORRATEO,'N') = 'Y' THEN --  Y : prorrateable N: no prorrateable
--****************
    for i in c_coid_sncode(to_number(Pv_co_id), ln_sncode) loop
    
      Lv_etapa := 'Recuperacion del valor del parametro <DIAS_ANT_FIN_CICLO>';
    
      scp_dat.sck_api.scp_parametros_procesos_lee('DIAS_ANT_FIN_CICLO',
                                                  lv_id_proceso_scp,
                                                  Ln_parametro_dias,
                                                  lv_descripcion_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
    
      If ln_error_scp <> 0 Then
        Lv_mensaje_error := lv_error_scp;
        ----------------------------------------------------------------------
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := Lv_etapa;
        lv_mensaje_tec_scp := Lv_mensaje_error;
        lv_mensaje_acc_scp := NULL;
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  pv_co_id,
                                                  ln_sncode,
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
      
        ----------------------------------------------------------------------------
        raise Le_error_parametro;
      
      End If;
    
      Ln_ADV_CHARGE            := I.ADV_CHARGE;
      Ln_ADV_CHARGE_ITEM_PRICE := I.ADV_CHARGE_ITEM_PRICE;
    
      Lv_etapa := 'Calculo de la variable <Ln_dias_ciclo>';
      ----------------------------------------------------------------------
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Lv_etapa;
      lv_mensaje_tec_scp := lv_trama;
      lv_mensaje_acc_scp := pv_co_id;
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                pv_co_id,
                                                ln_sncode,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ----------------------------------------------------------------------------
      Ln_dias_ciclo := TO_DATE(Pv_per_fin, 'DD/MM/RRRR') -
                       TO_DATE(Pv_per_ini, 'DD/MM/RRRR') + 1;
    
      Lv_etapa                       := 'Calculo de valor para las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
      Ln_dias_cobrar                 := Ln_dias_ciclo +
                                        to_number(Ln_parametro_dias);
      Ln_valor_ADV_CHARGE            := round((Ln_ADV_CHARGE /
                                              Ln_dias_ciclo) *
                                              Ln_dias_cobrar,
                                              5);
      Ln_valor_ADV_CHARGE_ITEM_PRICE := round((Ln_ADV_CHARGE_ITEM_PRICE /
                                              Ln_dias_ciclo) *
                                              Ln_dias_cobrar,
                                              5);
      Lv_etapa                       := 'Actualizacion de las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
      ----------------------------------------------------------------------
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Lv_etapa;
      lv_mensaje_tec_scp := lv_trama;
      lv_mensaje_acc_scp := pv_co_id;
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                pv_co_id,
                                                ln_sncode,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ----------------------------------------------------------------------------
      
      --INI [11417]     CIM A.GONZALEZ 04/08/2017
       IF NVL(LV_BAN_CONT_PRO,'N')='S' THEN
               open C_CONT_PROC_PRO (Pv_co_id,I.SNCODE);
               FETCH C_CONT_PROC_PRO
               INTO LN_CON_PRO;--- SI EXTISTE YA UN REGISTRO EN LA MISMA FECHA 
               CLOSE C_CONT_PROC_PRO;
               
                if LN_CON_PRO >= 1 then
                  
               -- YA PASO EL PROCESO    
               Lv_etapa:='YA SE EJECUTO EL PROCESO DEL ADV_CHARGE AL MENOS 1 VEZ  ' ||I.SNCODE ;
                lv_mensaje_apl_scp := Lv_etapa;
                lv_mensaje_tec_scp := lv_trama;
                lv_mensaje_acc_scp := NULL;
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  pv_co_id,
                                                  ln_sncode,
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
               
               
               ELSE
                 update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
                  set A.ADV_CHARGE=Ln_valor_ADV_CHARGE,A.ADV_CHARGE_ITEM_PRICE=Ln_valor_ADV_CHARGE_ITEM_PRICE
                  where A.co_id=Pv_co_id
                  and A.SNCODE=I.SNCODE;
                    
                    Lv_etapa:='SE ACTUALIZA POR PRIMERA VEZ EL A.ADV_CHARGE_ITEM_PRICE PARA EL ' ||I.SNCODE ;
                lv_mensaje_apl_scp := Lv_etapa;
                lv_mensaje_tec_scp := lv_trama;
                lv_mensaje_acc_scp := NULL;
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  pv_co_id,
                                                  ln_sncode,
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
               
                    
       
                    
                    
                 
              END IF; 
              ELSE
                  update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
                   set A.ADV_CHARGE            = Ln_valor_ADV_CHARGE, A.ADV_CHARGE_ITEM_PRICE = Ln_valor_ADV_CHARGE_ITEM_PRICE
                   where A.co_id = Pv_co_id
                   and A.SNCODE = I.SNCODE;      
                END IF;
      -----**************--------
      
      Ln_num_reg_act := Ln_num_reg_act + 1; 
    END LOOP;
    End If;
    Else
      --**
       for i in c_coid_sncode(to_number(Pv_co_id), ln_sncode) loop
    
      Lv_etapa := 'Recuperacion del valor del parametro <DIAS_ANT_FIN_CICLO>';
    
      scp_dat.sck_api.scp_parametros_procesos_lee('DIAS_ANT_FIN_CICLO',
                                                  lv_id_proceso_scp,
                                                  Ln_parametro_dias,
                                                  lv_descripcion_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
    
      If ln_error_scp <> 0 Then
        Lv_mensaje_error := lv_error_scp;
        ----------------------------------------------------------------------
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := Lv_etapa;
        lv_mensaje_tec_scp := Lv_mensaje_error;
        lv_mensaje_acc_scp := NULL;
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  pv_co_id,
                                                  ln_sncode,
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
      
        ----------------------------------------------------------------------------
        raise Le_error_parametro;
      
      End If;
    
      Ln_ADV_CHARGE            := I.ADV_CHARGE;
      Ln_ADV_CHARGE_ITEM_PRICE := I.ADV_CHARGE_ITEM_PRICE;
    
      Lv_etapa := 'Calculo de la variable <Ln_dias_ciclo>';
      ----------------------------------------------------------------------
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Lv_etapa;
      lv_mensaje_tec_scp := lv_trama;
      lv_mensaje_acc_scp := pv_co_id;
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                pv_co_id,
                                                ln_sncode,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ----------------------------------------------------------------------------
      Ln_dias_ciclo := TO_DATE(Pv_per_fin, 'DD/MM/RRRR') -
                       TO_DATE(Pv_per_ini, 'DD/MM/RRRR') + 1;
    
      Lv_etapa                       := 'Calculo de valor para las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
      Ln_dias_cobrar                 := Ln_dias_ciclo +
                                        to_number(Ln_parametro_dias);
      Ln_valor_ADV_CHARGE            := round((Ln_ADV_CHARGE /
                                              Ln_dias_ciclo) *
                                              Ln_dias_cobrar,
                                              5);
      Ln_valor_ADV_CHARGE_ITEM_PRICE := round((Ln_ADV_CHARGE_ITEM_PRICE /
                                              Ln_dias_ciclo) *
                                              Ln_dias_cobrar,
                                              5);
      Lv_etapa                       := 'Actualizacion de las variables <ADV_CHARGE, ADV_CHARGE_ITEM_PRICE>';
      ----------------------------------------------------------------------
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Lv_etapa;
      lv_mensaje_tec_scp := lv_trama;
      lv_mensaje_acc_scp := pv_co_id;
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                pv_co_id,
                                                ln_sncode,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ----------------------------------------------------------------------------
      update SYSADM.PROFILE_SERVICE_ADV_CHARGE A
         set A.ADV_CHARGE            = Ln_valor_ADV_CHARGE,
             A.ADV_CHARGE_ITEM_PRICE = Ln_valor_ADV_CHARGE_ITEM_PRICE
       where A.co_id = Pv_co_id
         and A.SNCODE = I.SNCODE;
      Ln_num_reg_act := Ln_num_reg_act + 1;
    END LOOP;
      --***
      end if;
  end loop;
  if Ln_num_reg_act <= 0 then
    Lv_etapa := 'Recuperacion de datos en la tabla PROFILE_SERVICE_ADV_CHARGE';
    ----------------------------------------------------------------------
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := Lv_etapa;
    lv_mensaje_tec_scp := lv_trama;
    lv_mensaje_acc_scp := pv_co_id;
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              pv_co_id,
                                              ln_sncode,
                                              null,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    ----------------------------------------------------------------------------
    Lv_mensaje_error := 'No se encontraron registros para el co_id <' ||
                        Pv_co_id || '>';
    raise LE_ERROR_STATUS; -- 10/10/2013 Iro Juan Romero
  end if;
  commit;

  Pv_response := 0;
  Pv_etapa    := NULL;
  Pv_detalle  := 'Procesamiento exitoso';

  scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            ln_total_registros_scp,
                                            null,
                                            ln_error_scp,
                                            lv_error_scp);

  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
EXCEPTION
  WHEN LE_ERROR_STATUS THEN
    Pv_response := 0;
    Pv_etapa    := 'RC_ACTUALIZA_VALOR_SNCODE: ' || Lv_etapa;
    Pv_detalle  := Lv_mensaje_error;
    --Bitacorizando el error suscitado
    lv_mensaje_apl_scp := 'Error : ' || pv_etapa;
    lv_mensaje_tec_scp := Pv_etapa;
    lv_mensaje_acc_scp := 'Revisar Datos Enviados';
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              pv_co_id,
                                              null,
                                              null,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ln_registros_procesados_scp := 1;
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              null,
                                              ln_registros_procesados_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  WHEN Le_error_parametro THEN
    -- 10/10/2013 Iro Juan Romero
    Pv_response := -1;
    Pv_etapa    := 'RC_ACTUALIZA_VALOR_SNCODE: ' || Lv_etapa;
    Pv_detalle  := Lv_mensaje_error;
    --Bitacorizando el error suscitado
    lv_mensaje_apl_scp := 'Error : ' || pv_etapa;
    lv_mensaje_tec_scp := Pv_etapa;
    lv_mensaje_acc_scp := 'Revisar Datos Enviados';
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              pv_co_id,
                                              null,
                                              null,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              null,
                                              ln_registros_error_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  WHEN OTHERS THEN
    Pv_response := -1;
    Pv_etapa    := 'RC_ACTUALIZA_VALOR_SNCODE: ' || Lv_etapa;
    Pv_detalle  := sqlerrm;
    rollback;
    --Bitacorizando el error suscitado
    --Bitacorizando el error suscitado
    lv_mensaje_apl_scp := 'Error : ' || pv_etapa;
    lv_mensaje_tec_scp := Pv_etapa;
    lv_mensaje_acc_scp := 'Revisar Datos Enviados';
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              pv_co_id,
                                              null,
                                              null,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              null,
                                              ln_registros_error_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
  
END RC_ACTUALIZA_VALOR_X_SNCODE;
 --FIN CLS CMA

END RC_TRX_SUSPENSION_BSCS_DTH;
/
