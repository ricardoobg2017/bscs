create or replace package BLK_OCC_INTO_FEES is

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Se migra del aplicativo realizado en Visual a Forms y Report
-- permitiendo la carga a la fees de manera manual y autom�tica.
--==================================================================================
-- Modificado por: Wendy Requena Hojas
--Creaci�n de proceso BLP_GENERA_ARCHIVO
--==================================================================================

--==================================================================================
-- Variables Globales
   Gv_CicloAxis varchar2(2);
   Gv_DiaMax    varchar2(2);
   Gv_DiaMin    varchar2(2);
------------------------------------------------------------------------------------

  procedure BLP_PRINCIPAL(Pn_IdTransaccion number,
                        Pv_IdSva         varchar2,
                        Pn_IdBitacora    number,
                        Pn_Aplicativo    number default 0);

  function BLF_VALIDA_STATUS_CLIENTE(Pv_EsCta           varchar2,
                                     Pv_Custcode        varchar2,
                                     Pv_Status      out varchar2,
                                     Pd_Fecha    in out DATE,
                                     Pi_Error       out Integer
                                    ) return VARCHAR2;

  function BLF_OBTIENE_CUSTOMER_ID(Pv_EsCuenta     varchar2,
                                 Pv_dnNum          varchar2,
                                 Pv_Custcode       varchar2,
                                 Pi_Coid           Integer,
                                 Pd_Fecha   in out date,
                                 Pi_Error      out Integer
                                 ) return Integer;

  function BLF_OBTIENE_SECUENCIA(PI_CUSTOMERID     Integer,
                                 PI_ERROR      out Integer
                                ) return integer;

  function  BLF_OBTIENE_MPULKTMB(PI_SNCODE               Integer,
                                 PV_ACCGLCODE       Out Varchar2,
                                 PV_ACCSERV_CATCODE Out Varchar2,
                                 PV_ACCSERV_CODE    Out Varchar2,
                                 PV_ACCSERV_TYPE    Out Varchar2,
                                 PI_VSCODE          Out Integer,
                                 PI_SPCODE          Out Integer
                                ) return number;

  function BLF_EVCODE(PI_SNCODE integer) RETURN INTEGER;

  procedure BLP_GETCYCLEAXIS(Pv_CICLOADMIN VARCHAR2);
  procedure BLP_GETDATECYCLE;
 PROCEDURE BLP_REMOTE_COID(PV_Servicio        VARCHAR2,
                          Pd_FECHSUBMIT      Date,
                          Pn_COId       OUT  number,
                          PV_MSGERR     OUT  VARCHAR2
                         );
 procedure BLP_REVERSO(PN_IDTX            NUMBER,
                      PV_IDSVA           VARCHAR2 DEFAULT NULL,
                      PV_TABLENAME       VARCHAR2,
                      PN_BITACORA        NUMBER,
                      PV_MSGERR      OUT VARCHAR2
                     );
PROCEDURE BLP_DELETE_BITA(PV_MSGERR out varchar2);

PROCEDURE BLP_UPD_FEES_TMP(PN_IDTX            NUMBER,
                           PV_IDSVA           VARCHAR2,
                           Pv_TIPO            varchar2,
                           Pv_valor           varchar2,
                           PI_CUSTOMER        INTEGER,
                           PI_SEQNO           INTEGER,
                           PV_REMARK          VARCHAR2,
                           PV_ROWID           VARCHAR2,
                           PV_MSGERR      OUT VARCHAR2
                     );

PROCEDURE BLP_UPD_DETALLE_PROC(PN_IDTX            NUMBER,
                               PN_IDSVA           VARCHAR2,
                               PN_LOADED          NUMBER,
                               PN_SUCCESS         NUMBER,
                               PN_FAILED          NUMBER,
                               PN_AMOUNT_LOADED   NUMBER,
                               PD_FECHA_FEES      DATE,
                               PV_REMARK          VARCHAR2,
                               PV_MSGERR      OUT VARCHAR2
                              );
/*Proceso Generel que realiza la ceaci�n de un archivo*/
Procedure BLP_GENERA_ARCHIVO(Pv_Directorio    In  Varchar2 ,
                             Pv_ArchivoDatos  In  Varchar2 ,
                             Pv_Line          In Varchar2);
end BLK_OCC_INTO_FEES;
/
create or replace package body BLK_OCC_INTO_FEES is

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Se migra del aplicativo realizado en Visual a Forms, Report y PL/SQL
-- permitiendo la carga a la fees de manera manual y autom�tica.
--==================================================================================
procedure BLP_PRINCIPAL(Pn_IdTransaccion number,
                        Pv_IdSva         varchar2,
                        Pn_IdBitacora    number,
                        Pn_Aplicativo    number default 0) is
  Cursor C_fees_tmp(Cn_IdTx number, Cv_IdSva varchar2) is
    select t.*, t.rowid
      from fees_tmp t
     where t.transaction_id = Cn_IdTx
       and t.id_sva         = Cv_IdSva--;
       and t.customer_id is null
       and t.seqno is null;

   --
   Cursor C_FeesCount (Cn_IdTx number, Cv_IdSva varchar2) is
   select count(*) x
   from fees_tmp t
   where t.transaction_id = Cn_IdTx
   and t.id_sva         = Cv_IdSva;
   --

   Cursor C_Parametros is
   select valor
   from gv_parametros@COLECTOR.WORLD
   where id_tipo_parametro = 2
   and   id_parametro = 'RUTA_FILE';
   --
   --Obtiene el ultimo COID por servicio
   Cursor C_LastCoid(Cv_Num varchar2)is
   SELECT Cc.Co_Id
   FROM Directory_Number   Dn,
        Contr_Services_Cap Cc
   WHERE Dn.Dn_Num = Cv_Num
   AND Dn.Dn_Id = Cc.Dn_Id
   AND Nvl(Cc.Cs_Deactiv_Date, SYSDATE) =
       (SELECT MAX(Nvl(Cc1.Cs_Deactiv_Date, SYSDATE))
          FROM Contr_Services_Cap Cc1
         WHERE Cc1.Dn_Id = Cc.Dn_Id);



  lv_status_cliente     varchar2(1):='a';
  lv_status             varchar2(1);
  li_error              Integer;
  li_CustomerId         Integer;
  li_Secuencia          Integer;
  --
  li_CantError          Integer:=0;
  li_CantSuccess        Integer:=0;
  li_CantLoaded         Integer:=0;
  --
  File_Handle           Utl_File.File_Type;
  lv_directorio         varchar2(200);
  lv_archivo            varchar2(200);
  --
  Ln_Monto              Number :=0;
  lc_Cfeestmp           c_Fees_Tmp%rowtype;
  lc_CfeesCount         C_FeesCount%rowtype;
  --
  lv_remark             varchar2(1000);
  lv_resumen            varchar2(2000);
  lv_SelValidFrom       varchar2(1000);
  ld_Fecha              Date;
  ld_ValidFrom          Date;
  ld_EntDate            Date;
  type Select_Cursor    is ref cursor;
  C_Select              Select_Cursor;
  --
  lv_accglcode          varchar2(30);
  lv_accserv_catcode    varchar2(10);
  lv_accserv_code       varchar2(10);
  lv_accserv_type       varchar2(3);
  li_vscode             integer;
  li_spcode             integer;
  --
  lv_FeeType            varchar2(1);
  lv_valor              varchar2(40);
  li_Period             Integer;
  --
  Ln_Coid               Number;
  --
  lv_insert             varchar2(2000);
  li_evcode             Integer;
   --scp
  Lb_NotFound           Boolean:=False;
  Lv_Error		          Varchar2(4000);
  Lv_Notifica_Scp		    Varchar2(1)	:= 'N';
  Lv_Proceso            varchar2(100):=NULL;
  Lv_Descripcion        varchar2(200):=NULL;
  Lv_Valor_commit              varchar2(50):=null;
  Lv_Mensaje_Aplicacion	Varchar2(4000)	:= null;
  Lv_Mensaje_Accion	    Varchar2(4000)	:= null;
  Lv_Mensaje_Tecnico	  Varchar2(4000)	:= null;
  Ln_NivelError		      Number		:= 0;
  Ln_Error		          Number		:= 0;
  --
  mi_excepcion          Exception;
  lv_msgerr             varchar2(1000);
  Lv_Comando            varchar2(100) := 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS ='||'''. ''';
  Lv_alter_fecha        varchar2(100) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''dd/mm/rrrr hh24:mi:ss''';




begin
  execute immediate Lv_Comando;
  execute immediate Lv_alter_fecha;

  if Pn_Idtransaccion is null then
     Lv_Notifica_Scp       := 'S';
     Lv_Mensaje_Aplicacion := 'El Id_transaccion es requerido para la insercci�n en la fees. Revisar el shell que llama al paquete BLK_OCC_INTO_FEES.BLP_PRINCIPAL ';
     Lv_Mensaje_Tecnico	   := 'El parametro Id_transaccion es requerido para la insercci�n en la fees. Revisar el shell que llama al paquete BLK_OCC_INTO_FEES.BLP_PRINCIPAL ';
     Lv_Mensaje_Accion	   := null;
     Ln_NivelError         := 3;
     if Pn_Aplicativo <> 1 then
       Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                             Lv_Mensaje_Aplicacion,
                                             Lv_Mensaje_Tecnico,
                                             Lv_Mensaje_Accion,
                                             Ln_NivelError,
                                             Pv_IdSva,
                                             NULL,
                                             NULL,
                                             Pn_IdTransaccion,
                                             NULL,
                                             Lv_Notifica_Scp,
                                             Ln_Error,
                                              Lv_Error);
     else
       Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                             'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                             'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                             Lv_Mensaje_Accion,
                                             Ln_NivelError,
                                             Pv_IdSva,
                                             NULL,
                                             NULL,
                                             Pn_IdTransaccion,
                                             NULL,
                                             Lv_Notifica_Scp,
                                             Ln_Error,
                                              Lv_Error);
     end if;
  end if;

  if Pv_Idsva is null then
     Lv_Notifica_Scp       := 'S';
     Lv_Mensaje_Aplicacion := 'El Id_SVA es requerido para la insercci�n en la fees. Revisar el shell que llama al paquete BLK_OCC_INTO_FEES.BLP_PRINCIPAL ';
     Lv_Mensaje_Tecnico	   := 'El parametro Id_SVA es requerido para la insercci�n en la fees. Revisar el shell que llama al paquete BLK_OCC_INTO_FEES.BLP_PRINCIPAL ';
     Lv_Mensaje_Accion	   := null;
     Ln_NivelError         := 3;
    if Pn_Aplicativo <> 1 then
     Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                           Lv_Mensaje_Aplicacion,
                                           Lv_Mensaje_Tecnico,
                                           Lv_Mensaje_Accion,
                                           Ln_NivelError,
                                           Pv_IdSva,
                                           NULL,
                                           NULL,
                                           Pn_IdTransaccion,
                                           NULL,
                                           Lv_Notifica_Scp,
                                           Ln_Error,
                                           Lv_Error);
    else
     Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                           'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                           'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                           Lv_Mensaje_Accion,
                                           Ln_NivelError,
                                           Pv_IdSva,
                                           NULL,
                                           NULL,
                                           Pn_IdTransaccion,
                                           NULL,
                                           Lv_Notifica_Scp,
                                           Ln_Error,
                                           Lv_Error);
    end if;
  end if;

  open C_FeesCount(Pn_IdTransaccion, Pv_IdSva);
  fetch C_FeesCount into lc_CfeesCount;
  close C_FeesCount;

  li_CantLoaded:=lc_CfeesCount.x;
  if li_CantLoaded = 0 then
    if Pn_Aplicativo <> 1 then
      SMK_CARGAS_SVA.SP_UPDATE_ESTADOS_BSCS@COLECTOR.WORLD(Pn_IdTransaccion,Pv_IdSva,'INSERT_FEES_TMP','E');
    end if;
     -- Bitacorizar con SCP
    lv_insert:=sqlerrm;
    Lv_Notifica_Scp       := 'N';
    Lv_Mensaje_Aplicacion	:= 'No existen datos en la fees_tmp para el id_transaction '||Pn_IdTransaccion||' e id_sva '||Pv_IdSva;
    Lv_Mensaje_Tecnico	  := null;
    Lv_Mensaje_Accion	    := 'No existen datos en la fees_tmp para el id_transaction '||Pn_IdTransaccion||' e id_sva '||Pv_IdSva;
    Ln_NivelError         := 3;
    if Pn_Aplicativo <> 1 then
      Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                            Lv_Mensaje_Aplicacion,
                                            Lv_Mensaje_Tecnico,
                                            Lv_Mensaje_Accion,
                                            Ln_NivelError,
                                            Pv_IdSva,
                                            null,
                                            null,
                                            Pn_IdTransaccion,
                                            null,
                                            Lv_Notifica_Scp,
                                            Ln_Error,
                                            Lv_Error);
    else
      Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                            'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                            'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                            Lv_Mensaje_Accion,
                                            Ln_NivelError,
                                            Pv_IdSva,
                                            null,
                                            null,
                                            Pn_IdTransaccion,
                                            null,
                                            Lv_Notifica_Scp,
                                            Ln_Error,
                                            Lv_Error);

    end if;
  end if;
  if Pn_Aplicativo <> 1 then
    Begin
      Select 'Proceso Autom�tico - Transacci�n N�. '||a.id_transaccion||' Fact.'||to_char(sysdate,'dd/mm/rrrr')||' carga '||a.id_sva||' ('||to_char(b.fecha_inicio,'dd/mm/rrrr')||' - '||to_char(b.fecha_fin,'dd/mm/rrrr')||') '||a.usuario
        Into lv_remark
        From sva_detalle_procesos@COLECTOR.WORLD  a,
             sva_procesos@COLECTOR.WORLD          b
       where b.id_transaccion = Pn_IdTransaccion
         and a.id_transaccion = b.id_transaccion
         and a.id_sva         = Pv_IdSva;
    Exception
      When no_data_found then
           lv_remark := 'Proceso Autom�tico - Transacci�n N�. '||Pn_IdTransaccion||' Fecha de Carga: '||to_char(sysdate,'dd/mm/rrrr')||' carga '||Pv_IdSva;
    End;
  end if;
  for i in C_fees_tmp(Pn_IdTransaccion, Pv_IdSva) loop
    Lb_NotFound:=True;
    lv_resumen:=null;
    li_error:=null;

         -- Se valida el status del cliente si es cuenta
         ld_Fecha:=i.fecha;
         lv_status_cliente:= blf_valida_status_cliente(Pv_EsCta    => i.es_cuenta,
                                                       Pv_Custcode => i.id_cuenta,
                                                       Pv_Status   => lv_status,
                                                       Pd_Fecha    => ld_Fecha,
                                                       Pi_Error    => li_error
                                                      );
         if li_error = 1 then
            if lv_status ='d' then
               lv_resumen:='La fecha no se encuentra en el rango del periodo STATUS deactive';
            else
               lv_resumen:='Error al validar status del cliente';
            end if;
         end if;
         --
         if li_error <> 1 then
            li_CustomerId:=BLF_OBTIENE_CUSTOMER_ID(Pv_EsCuenta => i.es_cuenta,
                                                   Pv_Custcode => i.id_cuenta,
                                                   Pv_dnNum    => i.id_servicio,
                                                   Pi_Coid     => i.co_id,
                                                   Pd_Fecha    => i.fecha,
                                                   Pi_Error    => li_error
                                                  );
            if li_error = 1 then
               lv_resumen:='No se encuentra customer_id';
            else
               if li_error = 2 then
                  lv_resumen:='La fecha no se encuentra en el rango del periodo STATUS Inactivo';
                  li_error:=1;
               end if;
            end if;
         end if;
         --
         if li_error <> 1 then
            li_Secuencia:=BLF_OBTIENE_SECUENCIA(PI_CUSTOMERID => li_CustomerId,
                                                PI_ERROR      => li_error);
            if li_error = 1 then
               lv_resumen:='No se obtiene la secuencia';
            end if;
         end if;
         --
         if li_error <> 1 then
            li_error:=   BLF_OBTIENE_MPULKTMB (PI_SNCODE          => i.id_occ,
                                               PV_ACCGLCODE       => lv_accglcode,
                                               PV_ACCSERV_CATCODE => lv_accserv_catcode,
                                               PV_ACCSERV_CODE    => lv_accserv_code,
                                               PV_ACCSERV_TYPE    => lv_accserv_type,
                                               PI_VSCODE          => li_vscode,
                                               PI_SPCODE          => li_spcode
                                              );

            if li_error = 1 then
               lv_resumen:='Problemas al obtener datos de la tabla mpulktmb';
            end if;
         end if;
         --
         if li_error <> 1 then
            li_error:=BLF_EVCODE(PI_SNCODE => i.id_occ);
            li_evcode:=li_error;
            if li_error = 1 then
               lv_resumen:='No se enontro el ev_code en la tabla mpulkexn en base al parametro '||i.id_occ;
            end if;
         end if;
         --
         if li_error = 1 then
            --Cantidad de errores
            li_CantError:= li_CantError + 1;
            -- En caso de haber errores abrimos un archivo para bitacorizar
            if li_CantError = 1 then
               open C_Parametros;
               fetch C_Parametros into lv_directorio;
               close C_Parametros;
               -- En caso de no estar configurado
               if lv_directorio is null then
                  lv_directorio :='/bscs/bscsprod/procesos/OCC/FEES_reject';
               end if;
               lv_archivo    :=to_char(sysdate,'dd-mm-rrrr')||'_'||to_char(Pn_IdTransaccion)||'_'||Pv_IdSva||'.txt';
               File_Handle := Utl_File.fopen(lv_directorio, lv_archivo, 'a');
            end if;
            -- enviar a SCP el siguiente msg
            if i.id_cuenta is not null then
             lv_resumen:= to_char(sysdate,'dd/mm/rrrr hh24:mi:ss')||' '|| i.id_cuenta||' '||lv_resumen;
             Utl_File.Put_Line(File_Handle, lv_resumen);
             insert into sva_fees_rechaza(transaction_id,id_sva,mensaje,fecha_evento,valor) values(Pn_IdTransaccion,Pv_IdSva,lv_resumen,sysdate,i.id_cuenta);
            end if;
            if i.id_servicio is not null then
             lv_resumen:= to_char(sysdate,'dd/mm/rrrr hh24:mi:ss')||' '|| i.id_servicio||' '||lv_resumen;
             Utl_File.Put_Line(File_Handle, lv_resumen);
             insert into sva_fees_rechaza(transaction_id,id_sva,mensaje,fecha_evento,valor) values(Pn_IdTransaccion,Pv_IdSva,lv_resumen,sysdate,i.id_servicio);
            end if;
            if i.co_id is not null then
             lv_resumen:= to_char(sysdate,'dd/mm/rrrr hh24:mi:ss') ||' '|| i.co_id ||' '|| lv_resumen;
             Utl_File.Put_Line(File_Handle, lv_resumen);
             insert into sva_fees_rechaza(transaction_id,id_sva,mensaje,fecha_evento,valor) values(Pn_IdTransaccion,Pv_IdSva,lv_resumen,sysdate,i.co_id);
            end if;

                 Lv_Notifica_Scp       := 'S';
                 Lv_Mensaje_Aplicacion := lv_resumen;
                 Lv_Mensaje_Tecnico	   := lv_resumen||' '||'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd-MON-rr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd-MON-rr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                 Lv_Mensaje_Accion	   := 'Revisar la Fees_tmp para ';
                 Ln_NivelError         := 2;

                if Pn_Aplicativo <> 1 then
                  Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                       Lv_Mensaje_Aplicacion,
                                                       Lv_Mensaje_Tecnico,
                                                       Lv_Mensaje_Accion,
                                                       Ln_NivelError,
                                                       Pv_IdSva,
                                                       NULL,
                                                       NULL,
                                                       li_canterror,--Cantidad de errores
                                                       Pn_IdTransaccion,
                                                       Lv_Notifica_Scp,
                                                       Ln_Error,
                                                       Lv_Error);
                else
                  Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                       'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                       'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                       Lv_Mensaje_Accion,
                                                       Ln_NivelError,
                                                       Pv_IdSva,
                                                       NULL,
                                                       NULL,
                                                       li_canterror,--Cantidad de errores
                                                       Pn_IdTransaccion,
                                                       Lv_Notifica_Scp,
                                                       Ln_Error,
                                                       Lv_Error);
                end if;

         end if;
         --
          if Pn_Aplicativo = 1 then
            lv_remark:= i.remark;
          else
            if i.id_occ = 103 then
                lv_remark:= 'Proceso Autom�tico - Facturaci�n';
            elsif i.id_occ = 33 then
                lv_remark:= 'Proceso Autom�tico - IES Eventos';
            elsif i.id_occ = 35 then
                lv_remark:= 'Proceso Autom�tico - Ring Tones';
            elsif i.id_occ = 106 then
                lv_remark:= 'Proceso Autom�tico - Big Brother';
            elsif i.id_occ = 34 Then
                lv_remark:= 'Proceso Autom�tico -IES Adicionales';
            End If;
          end if;
         --
         if upper(i.es_recurrente) ='N' then
            lv_FeeType:='N';
            li_Period := 1;
         else
            lv_FeeType:='R';
            li_Period := -1;
         end if;
         --
         ld_ValidFrom:=i.fecha;

         if    i.es_cuenta ='I' and li_error <> 1 then
               lv_SelValidFrom:='select cs_activ_date from contr_services_cap where co_id = :valor ';
               Lb_NotFound:=null;
               --
               open C_Select for lv_SelValidFrom using i.co_id;
               fetch C_Select into ld_Fecha;
               Lb_NotFound:= C_Select%notfound;
               close C_Select;

               if Lb_NotFound then
               ld_ValidFrom:=i.fecha;
                  Lv_Notifica_Scp       := 'S';
                  Lv_Mensaje_Aplicacion	:= 'No se encontro la fecha de activacion del cliente para validar valid_from ';
                  Lv_Mensaje_Tecnico	  := 'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd-MON-rr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd-MON-rr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                  Lv_Mensaje_Accion	    := 'Por favor verificar paquete BLK_OCC_INTO_FEES';
                  Ln_NivelError         := 2;

                  if Pn_Aplicativo <> 1 then
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                          Lv_Mensaje_Aplicacion,
                                                          Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  else
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  end if;

               end if;
               --
               Lb_NotFound:=true;
               if ld_Fecha  > i.fecha  then
                  ld_ValidFrom:=ld_Fecha;
               end if;
         elsif i.es_cuenta ='C' and li_error <> 1 then
               lv_SelValidFrom:='select csactivated from customer_all where  custcode = :valor ';
               Lb_NotFound:=null;
               --
               open C_Select for lv_SelValidFrom using i.id_cuenta;
               fetch C_Select into ld_Fecha;
               Lb_NotFound:= C_Select%notfound;
               close C_Select;

               if Lb_NotFound then
               ld_ValidFrom:=i.fecha;
                  Lv_Notifica_Scp       := 'S';
                  Lv_Mensaje_Aplicacion	:= 'No se encontro la fecha de activacion del cliente para validar valid_from ';
                  Lv_Mensaje_Tecnico	  := 'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd-MON-rr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd-MON-rr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                  Lv_Mensaje_Accion	    := 'Por favor verificar paquete BLK_OCC_INTO_FEES';
                  Ln_NivelError         := 2;

                  if Pn_Aplicativo <> 1 then
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                          Lv_Mensaje_Aplicacion,
                                                          Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  else
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  end if;
               end if;
               --
               Lb_NotFound:= true;
               if ld_Fecha  > i.fecha  then
                  ld_ValidFrom:=ld_Fecha;
               end if;
         elsif i.es_cuenta ='S' and li_error <> 1 then
               lv_SelValidFrom:='select cs_activ_date from contr_services_cap where dn_id = ( select dn_id from directory_number where dn_num = :num ) and co_id = :val ';
               Lb_NotFound:=null;
               --
               open C_LastCoid(i.id_servicio);
               fetch C_LastCoid into Ln_Coid;
               Lb_NotFound:=C_LastCoid%notfound;
               close C_LastCoid;
               --
               if Lb_NotFound then
               ld_ValidFrom:=i.fecha;
                  Lv_Notifica_Scp       := 'S';
                  Lv_Mensaje_Aplicacion	:= 'No se encontro co_id para el servicio '||i.id_servicio||' el cual se utiliza en la obtencion de la fecha del valid from se utiliza la fecha de la fees_tmp '||i.fecha;
                  Lv_Mensaje_Tecnico	  := 'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd-MON-rr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd-MON-rr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                  Lv_Mensaje_Accion	    := 'Por favor verificar paquete BLK_OCC_INTO_FEES';
                  Ln_NivelError         := 2;

                  if Pn_Aplicativo <> 1 then
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                          Lv_Mensaje_Aplicacion,
                                                          Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  else
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                 end if;
               else
               --
               open C_Select for lv_SelValidFrom using i.id_servicio, Ln_Coid;
               fetch C_Select into ld_Fecha;
               Lb_NotFound:= C_Select%notfound;
               close C_Select;
               end if;

               --
               Lb_NotFound:=true;
               if ld_Fecha  > i.fecha  then
                  ld_ValidFrom:=ld_Fecha;
               end if;
         end if;

         if li_error <> 1 then
            -- Si todo estuvo ok se realiza el Insert a la fees
            lv_insert:=' INSERT INTO sysadm.fees (customer_id, seqno, fee_type, glcode, entdate, period, username, valid_from, servcat_code, serv_code, serv_type, co_id, amount, remark, currency, glcode_disc, glcode_mincom, tmcode, vscode, spcode, sncode, evcode, fee_class) ';
            lv_insert:=lv_insert||'  VALUES (';
            lv_insert:=lv_insert||to_char(li_CustomerId)||', ';                                                                                         -- CUSTOMER_ID
            lv_insert:=lv_insert||to_char(li_Secuencia)||', ';                                                                                          -- SEQ_NO
            lv_insert:=lv_insert||''''||lv_FeeType||''', ';                                                                                             -- FEE_TYPE N once a time R recurring
            lv_insert:=lv_insert||''''||lv_accglcode||''', ';                                                                                           -- GLCODE
            /*if lv_status ='d' then
               lv_insert:=lv_insert||'to_date('''||ld_Fecha||''',''yyyy/MM/dd''), ';                                                                    -- ENTDATE
            else
               lv_insert:=lv_insert||'to_date('''||i.fecha||''',''yyyy/MM/dd''), ';                                                                     -- ENTDATE
            end if;*/
            ld_EntDate:=sysdate;
            lv_insert:=lv_insert||''''||to_char(ld_EntDate,'dd/mm/rrrr hh24:mi:ss')||''', ';                                                                       -- ENTDATE
            lv_insert:=lv_insert||to_char(li_Period)||', ';                                                                                             -- PERIOD
            lv_insert:=lv_insert||'decode('''||upper(i.usuario)||''','''',''FACTURAC'','''||upper(i.usuario)||''')'||', ';                              -- USERNAME
            lv_insert:=lv_insert||''''||to_char(ld_ValidFrom,'dd/mm/rrrr')||''', ';                                                                     -- VALID_FROM
            lv_insert:=lv_insert||''''||lv_accserv_catcode||''', ';                                                                                     -- SERVCAT_CODE
            lv_insert:=lv_insert||''''||lv_accserv_code||''', ';                                                                                        -- SERV_CODE
            lv_insert:=lv_insert||''''||lv_accserv_type||''', ';                                                                                        -- SERV_TYPE
            if i.es_cuenta ='C' then
               lv_insert:=lv_insert||' null, ';                                                                                                         -- COID
            elsif i.es_cuenta ='S' then
               lv_insert:=lv_insert||Ln_Coid||' ,';                                                                                                     -- COID
            else
               lv_insert:=lv_insert||i.CO_ID||' ,';
            end if;
            lv_insert:=lv_insert||'decode(upper('''||i.occ_positivo||'''),''N'',abs('||i.valor||') * -1 ,''S'','||i.valor||'), ';                       -- AMOUNT
            lv_insert:=lv_insert||''''||lv_remark||''', ';                                                                                              -- REMARK
            lv_insert:=lv_insert||'19, ';                                                                                                               -- CURRENCY 19 para USD
            lv_insert:=lv_insert||''''||lv_accglcode||''', ';                                                                                           -- GLCODE_DISC
            lv_insert:=lv_insert||''''||lv_accglcode||''', ';                                                                                           -- GLCODE_MINCOM
            lv_insert:=lv_insert||'35, ';                                                                                                               -- TMCODE siempre es 35 para OCC en la tabla mputmtab
            lv_insert:=lv_insert||to_char(li_vscode)||', ';                                                                                             -- VSCODE
            lv_insert:=lv_insert||to_char(li_spcode)||', ';                                                                                             -- SPCODE
            lv_insert:=lv_insert||to_char(i.id_occ)||', ';                                                                                              -- SNCODE
            lv_insert:=lv_insert||to_char(li_evcode)||', ';                                                                                             -- EVCODE
            lv_insert:=lv_insert||'3) ';                                                                                                                -- FEE_CLASS is a credit/charge, based on an event service, generated by a user (OCC)
            begin
              execute immediate lv_insert;
              -- Si se realiza correctamente la insercion contabilizo los registros insertados
              if Pn_Aplicativo <> 1 then
                li_CantSuccess:=li_CantSuccess+1;
                scp.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'P_COMMIT',
                                                        pv_id_proceso => Lv_Proceso,
                                                        pv_valor => Lv_Valor_commit,
                                                        pv_descripcion => Lv_Descripcion,
                                                        pn_error => Ln_Error,
                                                        pv_error => Lv_Error);
                if li_CantSuccess mod to_number(Lv_Valor_commit) = 0 then
                  commit;
                end if;
              end if;
              -- Si se realiza correctamente la insercion SUMARIZO
              Ln_Monto := Ln_Monto + i.valor;
              ---
              if i.es_cuenta ='C' then
                 lv_valor:=i.id_cuenta;
              elsif i.es_cuenta ='S'then
                 lv_valor:=i.id_servicio;
              elsif i.es_cuenta ='I' then
                 lv_valor:=to_char(i.co_id);
              end if;
              BLP_UPD_FEES_TMP(PN_IDTX     => Pn_IdTransaccion,
                               PV_IDSVA    => Pv_IdSva,
                               Pv_TIPO     => i.es_cuenta,
                               Pv_valor    => lv_valor,
                               PI_CUSTOMER => li_CustomerId,
                               PI_SEQNO    => li_Secuencia,
                               PV_REMARK   => lv_remark,
                               PV_ROWID    => i.rowid,
                               PV_MSGERR   => lv_msgerr);

              if lv_msgerr is not null then
                  Lv_Notifica_Scp       := 'S';
                  Lv_Mensaje_Aplicacion	:= 'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd/mm/rrrr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd/mm/rrrr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                  Lv_Mensaje_Tecnico    := lv_msgerr;
                  Lv_Mensaje_Accion	    := 'Por favor verificar paquete BLK_OCC_INTO_FEES.BLP_UPD_FEES_TMP';
                  Ln_NivelError         := 2;
                  raise mi_excepcion;
              end if;
              exception
              when mi_excepcion then
                  if Pn_Aplicativo <> 1 then
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                          Lv_Mensaje_Aplicacion,
                                                          Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  else
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  end if;
              when others then
                /*if Pn_Aplicativo <> 1 then
                  SMK_CARGAS_SVA.SP_UPDATE_ESTADOS@COLECTOR.WORLD(Pn_IdTransaccion,Pv_IdSva,'INSERT_FEES_TMP','E',Lv_Error);
                end if;*/
                   -- Bitacorizar con SCP
                  lv_insert             :=sqlerrm;
                  Lv_Notifica_Scp       := 'S';
                  Lv_Mensaje_Aplicacion	:= sqlerrm;
                  Lv_Mensaje_Tecnico	  := 'customer_id = '||li_CustomerId||' seqno = '||li_Secuencia||' fee_type = '||lv_FeeType||' glcode = '||lv_accglcode||' entdate = '|| to_char(sysdate,'dd-MON-rr') ||' period = '||li_Period||' username = '||i.usuario||' valid_from = '||to_char(i.fecha,'dd-MON-rr')||' servcat_code = '||lv_accserv_catcode||' serv_code = '||lv_accserv_code||' serv_type = '||lv_accserv_type||' co_id = '||i.co_id||' amount = '||i.valor||' remark = '||lv_remark||' currency = 19 '||' glcode_disc = '||lv_accglcode||' glcode_mincom = '||lv_accglcode||' tmcode = '||35||' vscode = '||li_vscode||' spcode = '||li_spcode||' sncode = '||i.id_occ||' evcode = '||li_evcode||' fee_class = 3';
                  Lv_Mensaje_Accion	    := 'Por favor verificar paquete BLK_OCC_INTO_FEES.BL_PRINCIPAL';
                  Ln_NivelError         := 3;
                  if Pn_Aplicativo <> 1 then
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_IdBitacora,
                                                          Lv_Mensaje_Aplicacion,
                                                          Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  else
                    Scp.Sck_Api.Scp_Detalles_Bitacora_Ins@COLECTOR.WORLD(Pn_IdBitacora,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Aplicacion,
                                                          'Procesamiento desde aplicativo - '||Lv_Mensaje_Tecnico,
                                                          Lv_Mensaje_Accion,
                                                          Ln_NivelError,
                                                          Pv_IdSva,
                                                          null,
                                                          null,
                                                          Pn_IdTransaccion,
                                                          null,
                                                          Lv_Notifica_Scp,
                                                          Ln_Error,
                                                          Lv_Error);
                  end if;
            end;
         end if;
  end loop;
  ---INI CAMBIO CARGA OCC 27/07/2017 ##### [11414] CLS CCA    
  if li_CantLoaded <> 0 then
     commit;
  end if;
  ---FIN CAMBIO CARGA OCC 27/07/2017 ##### [11414] CLS CCA
  
  -- Una vez finalizado el proceso en caso de haber existido error
  -- se cierra el archivo de bitacorizacion
  if li_CantError >=1 then
     Utl_File.Fclose(File_Handle);
  end if;
  if Pn_Aplicativo <> 1 then
    SMK_CARGAS_SVA.SP_UPDATE_ESTADOS@COLECTOR.WORLD(Pn_IdTransaccion,Pv_IdSva,'INSERT_FEES','F',Lv_Error);
  end if;
  BLP_UPD_DETALLE_PROC(PN_IDTX =>Pn_IdTransaccion,
                       PN_IDSVA => Pv_IdSva,
                       PN_LOADED => li_CantLoaded,
                       PN_SUCCESS => li_CantSuccess,
                       PN_FAILED => li_CantError,
                       PN_AMOUNT_LOADED => Ln_Monto,
                       PD_FECHA_FEES    => ld_EntDate,
                       PV_REMARK        => lv_remark,
                       PV_MSGERR => lv_resumen);

  Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  Pn_IdBitacora,
  					li_CantSuccess,
  					li_CantError,
  					Ln_Error,
  					Lv_Error);
end BLP_PRINCIPAL;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Si es cuenta se valida el estatus del cliente (Ok)
--==================================================================================

function BLF_VALIDA_STATUS_CLIENTE(Pv_EsCta           varchar2,
                                   Pv_Custcode        varchar2,
                                   Pv_Status      out varchar2,
                                   Pd_Fecha    in out DATE,
                                   Pi_Error       out Integer
                                  ) return VARCHAR2 is

  Cursor C_ValidaStatus(Cv_custcode varchar2) is
    SELECT cstype, csdeactivated, billcycle
      FROM customer_all
      WHERE custcode = Cv_custcode;

  lc_validastatus   C_ValidaStatus%rowtype;
  --
  lb_notfound       boolean;
  lv_status_cliente varchar2(1):='a';

  ld_InicPeriod     date;
  ld_EndPeriod      date;

  ln_dia            number;
begin
      --
         Pi_Error:=0;
         if upper(Pv_EsCta) = 'C' then
             open C_ValidaStatus(Pv_Custcode);
             fetch C_ValidaStatus into lc_validastatus;
             lb_notfound:=C_ValidaStatus%notfound;
             close C_ValidaStatus;
             --
             if(lb_notfound) then
             -- bitacorizar con SCP Error
               Pi_Error:=1;
               return lv_status_cliente;
             end if;

             BLP_GETCYCLEAXIS(lc_validastatus.billcycle);
             BLP_GETDATECYCLE;


             ln_dia:=to_number(to_char(Pd_Fecha,'dd'));

             if ln_dia >= to_number(Gv_DiaMin) then
             -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
                ld_InicPeriod := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
                ld_EndPeriod  := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,1),'mm/rrrr'),'dd/mm/rrrr');
             else
             -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
                ld_InicPeriod := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,-1),'mm/rrrr'),'dd/mm/rrrr');
                ld_EndPeriod  := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
             end if;

             if lc_validastatus.cstype = 'd' then
                if lc_validastatus.csdeactivated is not null then
                   if (lc_validastatus.csdeactivated >= ld_InicPeriod) and (lc_validastatus.csdeactivated <= ld_EndPeriod) then
                      Pv_Status:='d';
                      Pd_Fecha := lc_validastatus.csdeactivated - 1;
                      return 'd';
                   else
                      if (lc_validastatus.csdeactivated >= ld_EndPeriod) then
                         Pv_Status:='d';
                         Pd_Fecha := lc_validastatus.csdeactivated - 1;
                         return 'd';
                      else
                         Pv_Status:='d';
                         Pi_Error:=1;
                      end if;
                   end if;
                end if;
             else
                Pv_Status:= lc_validastatus.cstype;
                Pd_Fecha := sysdate;
             end if;
        end if;

       return lv_status_cliente;
end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Si es coid se valida el estatus del cliente
--==================================================================================

function BLF_VALIDA_STATUS_CLIENTE_COID(Pv_EsCta           varchar2,
                                        Pv_Custcode        varchar2,
                                        Pv_Status      out varchar2,
                                        Pd_Fecha    in out DATE,
                                        Pi_Error       out Integer
                                       ) return VARCHAR2 is

  Cursor C_ValidaStatus(Cv_coid varchar2) is
    SELECT cstype, csdeactivated, billcycle
      FROM customer_all cu, contract_all co
      WHERE cu.customer_id = co.customer_id
      and   co.co_id = Cv_coid;



  lc_validastatus   C_ValidaStatus%rowtype;
  --
  lb_notfound       boolean;
  lv_status_cliente varchar2(1):='a';

  ld_InicPeriod     date;
  ld_EndPeriod      date;

  ln_dia            number;
begin
      --
         Pi_Error:=0;
         if upper(Pv_EsCta) = 'I' then
             open C_ValidaStatus(Pv_Custcode);
             fetch C_ValidaStatus into lc_validastatus;
             lb_notfound:=C_ValidaStatus%notfound;
             close C_ValidaStatus;
             --
             if(lb_notfound) then
             -- bitacorizar con SCP Error
               Pi_Error:=1;
               return lv_status_cliente;
             end if;

             BLP_GETCYCLEAXIS(lc_validastatus.billcycle);
             BLP_GETDATECYCLE;


             ln_dia:=to_number(to_char(Pd_Fecha,'dd'));

             if ln_dia >= to_number(Gv_DiaMin) then
             -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
                ld_InicPeriod := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
                ld_EndPeriod  := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,1),'mm/rrrr'),'dd/mm/rrrr');
             else
             -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
                ld_InicPeriod := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,-1),'mm/rrrr'),'dd/mm/rrrr');
                ld_EndPeriod  := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
             end if;

             if lc_validastatus.cstype = 'd' then
                if lc_validastatus.csdeactivated is not null then
                   if (lc_validastatus.csdeactivated >= ld_InicPeriod) and (lc_validastatus.csdeactivated <= ld_EndPeriod) then
                      Pv_Status:='d';
                      Pd_Fecha := lc_validastatus.csdeactivated - 1;
                      return 'd';
                   else
                      if (lc_validastatus.csdeactivated >= ld_EndPeriod) then
                         Pv_Status:='d';
                         Pd_Fecha := lc_validastatus.csdeactivated - 1;
                         return 'd';
                      else
                         Pv_Status:='d';
                         Pi_Error:=1;
                      end if;
                   end if;
                end if;
             else
                Pv_Status:= lc_validastatus.cstype;
                Pd_Fecha := sysdate;
             end if;
        end if;

       return lv_status_cliente;
end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Se valida el estatus del cliente ya sea por
-- cuenta, servicio, coid
--==================================================================================

function BLF_OBTIENE_CUSTOMER_ID(Pv_EsCuenta     varchar2,
                                 Pv_dnNum        varchar2,
                                 Pv_Custcode     varchar2,
                                 Pi_Coid         Integer,
                                 Pd_Fecha in out date,
                                 Pi_Error    out Integer
                                 ) return Integer is

   Cursor C_Cuenta(Cv_Custcode varchar2) is
     SELECT customer_id, csdeactivated, billcycle
       FROM customer_all
       WHERE custcode = Cv_Custcode;
   lc_cuenta C_cuenta%rowtype;
   --

   Cursor C_Servicio(Cv_dnnum varchar2) is
      SELECT co.customer_id, cc.cs_deactiv_date, cu.billcycle
         FROM directory_number dn, contr_services_cap cc, contract_all co, customer_all cu
         WHERE cc.co_id = co.co_id
         AND cc.main_dirnum = 'X'
         -- AND cc.cs_deactiv_date is null
         AND cc.seqno = (select max(cc1.seqno) from contr_services_cap cc1 where cc1.co_id = cc.co_id)
         AND dn.dn_id = cc.dn_id
         AND dn.dn_num = Cv_dnnum
         AND cu.customer_id = co.customer_id
         order by cc.cs_deactiv_date desc;
  lc_servicio C_Servicio%rowtype;
  --

  Cursor C_Coid (Cn_coid number) is
  SELECT cu.customer_id, cu.csdeactivated, cu.billcycle
       FROM customer_all cu, contract_all co
       WHERE cu.customer_id = co.customer_id
       and   co.co_id =Cn_coid;
  lc_Coid    C_Coid%rowtype;
  --

  lb_notfound   boolean;
  --li_customerId integer;
  ln_dia        number;
  ld_InicPeriod date;
  ld_EndPeriod  date;

begin

     if Pv_EsCuenta ='C' then
        open  C_Cuenta(Pv_Custcode);
        fetch C_cuenta into lc_cuenta;
        lb_notfound:=C_Cuenta%notfound;
        close C_Cuenta;
        --
        if lb_notfound then
           --bitacorizar error scp
           --salir
           Pi_Error:=1;
           return 0;
        end if;

        BLP_GETCYCLEAXIS(Pv_CICLOADMIN => lc_cuenta.billcycle);
        BLP_GETDATECYCLE;

         ln_dia:=to_number(to_char(Pd_Fecha,'dd'));

         if ln_dia >= to_number(Gv_DiaMin) then
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,1),'mm/rrrr'),'dd/mm/rrrr');
         else
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,-1),'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
         end if;

         if lc_cuenta.csdeactivated is not null then
            if (lc_cuenta.csdeactivated >= ld_InicPeriod) and (lc_cuenta.csdeactivated <= ld_EndPeriod) then
               Pd_Fecha:=lc_cuenta.csdeactivated - 1;
            else
               if (lc_cuenta.csdeactivated >= ld_EndPeriod) then
                  Pd_Fecha:=lc_cuenta.csdeactivated - 1;
               else
                  -- bitacorizar error scp
                  -- salir
                  Pi_Error:=2;
                  return 0;
               end if;
            end if;
         end if;
        -- Se retorna el customer_id
         Pi_Error:=0;
       return lc_cuenta.customer_id;
     elsif Pv_EsCuenta='S' then
        open C_Servicio(Pv_dnNum);
        fetch C_Servicio into lc_servicio;
        lb_notfound:=C_Servicio%notfound;
        close C_Servicio;
        --
        if lb_notfound then
           --bitacorizar error scp
           --salir
           Pi_Error:=1;
           return 0;
        end if;

        BLP_GETCYCLEAXIS(Pv_CICLOADMIN => lc_servicio.billcycle);
        BLP_GETDATECYCLE;

        ln_dia:=to_number(to_char(Pd_Fecha,'dd'));

         if ln_dia >= to_number(Gv_DiaMin) then
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,1),'mm/rrrr'),'dd/mm/rrrr');
         else
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,-1),'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
         end if;

         if lc_servicio.cs_deactiv_date is not null then
            if (lc_servicio.cs_deactiv_date >= ld_InicPeriod) and (lc_servicio.cs_deactiv_date <= ld_EndPeriod) then
               Pd_Fecha:=lc_servicio.cs_deactiv_date - 1;
            else
               if (lc_servicio.cs_deactiv_date >= ld_EndPeriod) then
                  Pd_Fecha:=lc_servicio.cs_deactiv_date - 1;
               else
                  -- bitacorizar error scp
                  -- salir
                  Pi_Error:=2;
                  return 0;
               end if;
            end if;
         end if;
        -- Se retorna el customer_id
        Pi_Error:=0;
       return lc_servicio.customer_id;
       --Co_Id
       elsif Pv_EsCuenta='I' then
        open C_Coid(Pi_Coid);
        fetch C_Coid into lc_Coid;
        lb_notfound:=C_Coid%notfound;
        close C_Coid;
        --
        if lb_notfound then
           --bitacorizar error scp
           --salir
           Pi_Error:=1;
           return 0;
        end if;

        BLP_GETCYCLEAXIS(Pv_CICLOADMIN => lc_Coid.billcycle);
        BLP_GETDATECYCLE;

        ln_dia:=to_number(to_char(Pd_Fecha,'dd'));

         if ln_dia >= to_number(Gv_DiaMin) then
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,1),'mm/rrrr'),'dd/mm/rrrr');
         else
         -- 24/07/2005 a 23/08/2005   �   08/07/2005 a 07/08/2005
            ld_InicPeriod := to_date (Gv_DiaMax||'/'||to_char(add_months(Pd_Fecha,-1),'mm/rrrr'),'dd/mm/rrrr');
            ld_EndPeriod  := to_date (Gv_DiaMin||'/'||to_char(Pd_Fecha,'mm/rrrr'),'dd/mm/rrrr');
         end if;

         if lc_Coid.csdeactivated is not null then
            if (lc_Coid.csdeactivated >= ld_InicPeriod) and (lc_Coid.csdeactivated <= ld_EndPeriod) then
               Pd_Fecha:=lc_Coid.csdeactivated - 1;
            else
               if (lc_Coid.csdeactivated >= ld_EndPeriod) then
                  Pd_Fecha:=lc_Coid.csdeactivated - 1;
               else
                  -- bitacorizar error scp
                  -- salir
                  Pi_Error:=2;
                  return 0;
               end if;
            end if;
         end if;
        -- Se retorna el customer_id
         Pi_Error:=0;
       return lc_Coid.customer_id;
    end if;
end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Retorna el ciclo de facturaci�n de Axis (OK)
--==================================================================================

procedure BLP_GETCYCLEAXIS(Pv_CICLOADMIN VARCHAR2) is
begin
     PCK_OCC.OCF_CONSULTA_CICLO(Ln_CicloAdmin => to_number(Pv_CICLOADMIN),
                                Lv_CicloAxis  => Gv_CicloAxis
                               );

end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Retorna la fecha m�nimo y m�ximo para el ciclo (OK)
--==================================================================================

procedure BLP_GETDATECYCLE is
begin
     PCK_OCC.OCF_CONSULTA_FECHAS(Lv_CicloAxis => Gv_CicloAxis,
                                 Lv_DiaMax    => Gv_DiaMax   ,
                                 Lv_DiaMin    => Gv_DiaMin
                                 );

end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Obtiene la secuencia max(seqno) de la fees en base al customer_id (OK)
--==================================================================================

function BLF_OBTIENE_SECUENCIA(PI_CUSTOMERID     Integer,
                               PI_ERROR      out Integer
                              ) return integer is
  Cursor C_Sec(Ci_CustomerId INTEGER) is
  SELECT max(seqno) seq
	 FROM sysadm.fees
	WHERE customer_id = Ci_CustomerId;

  lc_Sec      C_Sec%rowtype;
  lb_notfound boolean;
begin
     open C_Sec(PI_CUSTOMERID);
     fetch C_Sec into lc_Sec;
     lb_notfound:=C_Sec%notfound;
     close C_Sec;
     --
     if lb_notfound then
        PI_ERROR:=0;
        return 1;
     else
        PI_ERROR:=0;
        return nvl(lc_Sec.Seq,0) + 1;
     end if;
     Exception
      When others then
      PI_ERROR:=1;
      return 1;
end;
--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Si es cuenta se valida el estatus del cliente (OK)
--==================================================================================

function  BLF_OBTIENE_MPULKTMB(PI_SNCODE               Integer,
                               PV_ACCGLCODE       Out Varchar2,
                               PV_ACCSERV_CATCODE Out Varchar2,
                               PV_ACCSERV_CODE    Out Varchar2,
                               PV_ACCSERV_TYPE    Out Varchar2,
                               PI_VSCODE          Out Integer,
                               PI_SPCODE          Out Integer
                              ) return number is
  Cursor C_Mpulktmb(Ci_sncode INTEGER) is
   SELECT accglcode, accserv_catcode, accserv_code, accserv_type , vscode, spcode
  	 FROM mpulktmb
  	 WHERE tmcode = 35
  	 AND sncode = Ci_sncode
  	 AND vscode = (select max(a.vscode) from mpulktmb a where tmcode = 35 and sncode = Ci_sncode);

  lc_mpulktmb C_Mpulktmb%rowtype;
  lb_notfound boolean;
  --
  lv_accglcode varchar2(30):='';
  lv_accserv_catcode varchar2(10):='';
  lv_accserv_code varchar2(10):='';
  lv_accserv_type varchar2(3):='';
  li_vscode integer:=0;
  li_spcode integer:=0;

begin
     open C_Mpulktmb(PI_SNCODE);
     fetch C_Mpulktmb into lc_mpulktmb;
     lb_notfound:=C_Mpulktmb%notfound;
     close C_Mpulktmb;

     if lb_notfound then
        PV_ACCGLCODE       := lv_accglcode;
        PV_ACCSERV_CATCODE := lv_accserv_catcode;
        PV_ACCSERV_CODE    := lv_accserv_code;
        PV_ACCSERV_TYPE    := lv_accserv_type;
        PI_VSCODE          := li_vscode;
        PI_SPCODE          := li_spcode;
        return 1;
     else
        PV_ACCGLCODE       := lc_mpulktmb.accglcode;
        PV_ACCSERV_CATCODE := lc_mpulktmb.accserv_catcode;
        PV_ACCSERV_CODE    := lc_mpulktmb.accserv_code;
        PV_ACCSERV_TYPE    := lc_mpulktmb.accserv_type;
        PI_VSCODE          := lc_mpulktmb.vscode;
        PI_SPCODE          := lc_mpulktmb.spcode;
        return 0;
     end if;
end;
--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Obtiene el evcode de la tabla mpulkexn en funci�n del sncode (OK)
--==================================================================================
function BLF_EVCODE(PI_SNCODE integer) RETURN INTEGER is
  Cursor C_Evcode(Cn_sncode INTEGER) is
  SELECT evcode
  	FROM mpulkexn
  	WHERE sncode =  Cn_sncode;

  lc_evcode C_Evcode%rowtype;
  lb_notfound boolean;
begin

     open C_Evcode(PI_SNCODE);
     fetch C_Evcode into lc_evcode;
     lb_notfound:=C_Evcode%notfound;
     close C_Evcode;

     if lb_notfound then
        return 1;
     else
         return lc_evcode.evcode;
     end if;
end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Obtenci�n de coid para actualizar en tabla sva_servicios_contratados
-- desde sms_proc@COLECTOR.WORLD
--==================================================================================
PROCEDURE BLP_REMOTE_COID(PV_Servicio        VARCHAR2,
                          Pd_FECHSUBMIT      Date,
                          Pn_COId       OUT  number,
                          PV_MSGERR     OUT  VARCHAR2
                         )is
  
  
  Cursor C_ObtieneCOId (Cv_Servicio varchar2, Cd_FechSubmit Date) is
  SELECT  csc.co_id
  FROM    DIRECTORY_NUMBER DN, CONTR_SERVICES_CAP CSC 
  where   DN.dn_num =  Cv_Servicio 
  and     DN.dn_id = CSC.dn_id
  and     Cd_FechSubmit between  CSC.cs_activ_date and nvl(CSC.cs_deactiv_date,SYSDATE);
  
  Lv_App      varchar2(60):='BLK_OCC_INTO_FEES.BLP_REMOTE_COID';
  Lb_NotFound boolean;
  Ln_CoId     number:=null;
  Le_Exception exception;
  --
  begin
        
     PV_MSGERR:=null;
     
     if PV_Servicio is null then
        PV_MSGERR:=Lv_App||' El servicio no puede ser nulo';
        raise Le_Exception;
     end if;
     
     if Pd_FECHSUBMIT is null then
        PV_MSGERR:=Lv_App||' La fecha en que se genero el SVA no puede ser nulo';
        raise Le_Exception;
     end if;
               
     OPEN C_ObtieneCOId(PV_Servicio, Pd_FECHSUBMIT);
     FETCH C_ObtieneCOId INTO Ln_CoId;
     Lb_NotFound:=C_ObtieneCOId%NOTFOUND;
     CLOSE C_ObtieneCOId;
          
     Pn_COId:=Ln_CoId;
     
  exception
  When Le_Exception then
       null;
  end;
  
  

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Actualizacion del
--==================================================================================
PROCEDURE BLP_UPD_DETALLE_PROC(PN_IDTX            NUMBER,
                               PN_IDSVA           VARCHAR2,
                               PN_LOADED          NUMBER,
                               PN_SUCCESS         NUMBER,
                               PN_FAILED          NUMBER,
                               PN_AMOUNT_LOADED   NUMBER,
                               PD_FECHA_FEES      DATE,
                               PV_REMARK          VARCHAR2,
                               PV_MSGERR      OUT VARCHAR2
                              )is
--  pragma autonomous_transaction;

  --
  Lv_App      varchar2(60):='BLK_OCC_INTO_FEES.BLP_UPD_DETALLE_PROC';
  Le_Exception exception;
  begin

     PV_MSGERR:=null;

     if PN_IDTX is null then
        PV_MSGERR:=Lv_App||' El Id Transaccion no puede ser nulo';
        raise Le_Exception;
     end if;

     BEGIN
          UPDATE SVA_DETALLE_PROCESOS@COLECTOR.WORLD DP
          SET  DP.CANTIDAD_CARGADOS   = PN_SUCCESS,
               DP.CANTIDAD_RECHAZADOS = PN_FAILED,
               DP.CANTIDAD            = PN_LOADED,
               DP.MONTO_CARGADO       = PN_AMOUNT_LOADED,
               DP.FECHA_FEES          = PD_FECHA_FEES,
               DP.COMENTARIO_FEES     = PV_REMARK
          WHERE DP.ID_TRANSACCION = PN_IDTX
          AND   DP.ID_SVA         = PN_IDSVA;
  --        COMMIT;
     END;


  exception
  When Le_Exception then
       null;
  When Others then
       PV_MSGERR:=Lv_App||' Ocurrio el siguiente error '||sqlerrm;
  end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Reverso de la fees o fees_tmp dinamicamente por TRANSACTION_ID
--==================================================================================
PROCEDURE BLP_REVERSO(PN_IDTX            NUMBER,
                      PV_IDSVA           VARCHAR2 DEFAULT NULL,
                      PV_TABLENAME       VARCHAR2,
                      PN_BITACORA        NUMBER,
                      PV_MSGERR      OUT VARCHAR2
                     )is

  Cursor C_Reverso(Cn_Idtx number, Cv_IdSVA varchar2 ) is
  Select f.customer_id, f.seqno
  from fees_tmp f
  where f.transaction_id = Cn_Idtx
  and f.id_sva= Cv_idSVA
  and f.customer_id is not null
  and f.seqno is not null;
  --
  Lc_Reverso  C_Reverso%rowtype;
  Lv_App      varchar2(60):='BLK_OCC_INTO_FEES.BLP_REVERSO';
  Lv_Delete   varchar2(2000);
  Lv_Upd      varchar2(2000);
  Le_Exception exception;
  --
  Lv_Mensaje_Aplicacion	Varchar2(4000)	:= null;
  Lv_Mensaje_Accion	    Varchar2(4000)	:= null;
  Lv_Mensaje_Tecnico	  Varchar2(4000)	:= null;
  Ln_NivelError		      Number		:= 0;
  Ln_Error		          Number		:= 0;
  Lv_Error		          varchar2(4000)  :=null;
  Lv_Notifica_Scp       varchar2(1):=null;

  begin
     PV_MSGERR:=null;

     if PN_IDTX is null then
        PV_MSGERR:=Lv_App||' El Id Transaccion no puede ser nulo';

       Lv_Notifica_Scp       := 'S';
       Lv_Mensaje_Aplicacion := Lv_App||' El Id Transaccion no puede ser nulo';
       Lv_Mensaje_Tecnico	   := 'Verificar estado del shel sva_reverso'||PV_TABLENAME||'.sh';
       Lv_Mensaje_Accion	   := null;
       Ln_NivelError         := 3;
        raise Le_Exception;
     end if;

     if PV_TABLENAME is null then
        PV_MSGERR:=Lv_App||' El nombre de la tabla sobre la cual se va a realizar el reverso no puede ser null';
       Lv_Notifica_Scp       := 'S';
       Lv_Mensaje_Aplicacion := Lv_App||' El nombre de la tabla sobre la cual se va a realizar el reverso no puede ser null';
       Lv_Mensaje_Tecnico	   := null;
       Lv_Mensaje_Accion	   := null;
       Ln_NivelError         := 3;
        raise Le_Exception;
     end if;


     if upper(PV_TABLENAME) = 'FEES' then
          For i in C_Reverso(PN_IDTX, PV_IDSVA) loop
            Begin
              Lv_Delete:='delete '||PV_TABLENAME||' x where x.customer_id = :cust_id and x.seqno = :secuencial ';
              execute immediate Lv_Delete using i.customer_id, i.seqno;
                 -- Registro en la bitacora
                 Lv_Upd:=' Update sva_bita_reverso sva set sva.FECHA_REVERSO = sysdate where sva.CUSTOMER_ID = :cust and sva.SEQNO = :seq and sva.TRANSACTION_ID = :trx and sva.id_sva = :sva ';
                 execute immediate Lv_Upd using i.customer_id, i.seqno, PN_IDTX, PV_IDSVA;

            exception
              when others then
                Lv_Error := sqlerrm;
            end;
          end loop;
          commit;
     elsif upper(PV_TABLENAME) = 'FEES_TMP' then
        Lv_Delete:='delete '||PV_TABLENAME||' x where x.transaction_id = :idtx';
        Lv_Delete:=Lv_Delete||' and x.ID_SVA = :idsva ';
        -- Se reversa por id_transaccion e id_sva sobre la fees_tmp
        execute immediate Lv_Delete using PN_IDTX, PV_IDSVA;
        commit;
         Lv_Notifica_Scp       := 'N';
         Lv_Mensaje_Aplicacion := Lv_App||' se realizo el reverso sobre la tabla '||PV_TABLENAME||' para el SVA: '||PV_IDSVA||' con Transacci�n N� '||PN_IDTX ;
         Lv_Mensaje_Tecnico	   := 'Reverso de todas los SVA con Transaccion N� '||PN_IDTX;
         Lv_Mensaje_Accion	   := null;
         Ln_NivelError         := 0;
         Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(PN_BITACORA,
                                               Lv_Mensaje_Aplicacion,
                                               Lv_Mensaje_Tecnico,
                                               Lv_Mensaje_Accion,
                                               Ln_NivelError,
                                               PV_IDSVA,
                                               NULL,
                                               NULL,
                                               PN_IDTX,
                                               NULL,
                                               Lv_Notifica_Scp,
                                               Ln_Error,
                                               Lv_Error);
     end if;
  exception
  When Le_Exception then
     Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(PN_BITACORA,
                                           Lv_Mensaje_Aplicacion,
                                           Lv_Mensaje_Tecnico,
                                           Lv_Mensaje_Accion,
                                           Ln_NivelError,
                                           PV_IDSVA,
                                           NULL,
                                           NULL,
                                           PN_IDTX,
                                           NULL,
                                           Lv_Notifica_Scp,
                                           Ln_Error,
                                           Lv_Error);
  When Others then
       PV_MSGERR:=Lv_App||' Ocurrio el siguiente error al tratar de reversar '||substr(sqlerrm,1,3500);
       rollback;
     Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(PN_BITACORA,
                                           Lv_App||' Ocurrio el siguiente error al tratar de reversar en el paquete BLP_REVERSO: '||substr(sqlerrm,1,3500),
                                           null,
                                           null,
                                           Ln_NivelError,
                                           PV_IDSVA,
                                           NULL,
                                           NULL,
                                           PN_IDTX,
                                           NULL,
                                           Lv_Notifica_Scp,
                                           Ln_Error,
                                           Lv_Error);
  end;

--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Procedure para borrar los datos de tabla SVA_BITA_REVERSO
--==================================================================================
PROCEDURE BLP_DELETE_BITA(PV_MSGERR out varchar2) is

  Cursor C_Param is
  SELECT VALOR
  FROM GV_PARAMETROS@COLECTOR.WORLD
  WHERE ID_TIPO_PARAMETRO = 7 AND ID_PARAMETRO = 'KEEP_DATA';
  --
  Lc_Param    C_Param%rowtype;
  Lv_App      varchar2(60):='BLK_OCC_INTO_FEES.BLP_DELETE_BITA';
  Lv_Delete   varchar2(2000);
  Le_Exception exception;
  --
  begin
    open C_Param;
    fetch C_Param into Lc_Param;
    close C_Param;
    --
    Lv_Delete:='Delete sva_bita_reverso where FECHA_EVENTO <= trunc(sysdate - to_number(:fecha) ) ';
    execute immediate Lv_Delete using Lc_Param.Valor;
    commit;
  exception
  When Others then
       PV_MSGERR:=Lv_App||' Ocurrio el siguiente error al tratar borrar la bitacora '||substr(sqlerrm,1,500);
  end;



--==================================================================================
-- Autor: MPAZMINO
-- Fecha: 24/01/2007
-- [2042] Mejoras cargas de OCC
-- Descripci�n: Actualizo datos (Cstomer_id, seqno) para reverso en la fees_tmp
--==================================================================================
PROCEDURE BLP_UPD_FEES_TMP(PN_IDTX            NUMBER,
                           PV_IDSVA           VARCHAR2,
                           Pv_TIPO            varchar2,
                           Pv_valor           varchar2,
                           PI_CUSTOMER        INTEGER,
                           PI_SEQNO           INTEGER,
                           PV_REMARK          VARCHAR2,
                           PV_ROWID           VARCHAR2,
                           PV_MSGERR      OUT VARCHAR2
                     )is
  --pragma autonomous_transaction;
  --
  Lv_App      varchar2(60):='BLK_OCC_INTO_FEES.BLP_UPD_FEES_TMP';
  Lv_UPD      varchar2(2000);
  Lv_UPD2     varchar2(2000);
  Lv_INS      varchar2(2000);
  Le_Exception exception;
  --
  begin

     PV_MSGERR:=null;

     if PN_IDTX is null then
        PV_MSGERR:=Lv_App||' El IdTransaccion no puede ser nulo';
        raise Le_Exception;
     end if;

     if PV_IDSVA is null then
        PV_MSGERR:=Lv_App||' El IdSVA no puede ser nulo';
        raise Le_Exception;
     end if;

     if pv_tipo = 'C' then
        Lv_UPD2:=' and x.id_cuenta = :pv_cuenta and x.rowid = :pv_rowid ';
     elsif pv_tipo ='S' then
        Lv_UPD2:=' and x.id_servicio = :pv_servicio and x.rowid = :pv_rowid ';
     elsif pv_tipo ='I' then
        Lv_UPD2:=' and x.co_id = to_number(:pn_coid) and x.rowid = :pv_rowid ';
     else
       PV_MSGERR:=Lv_App||' Los valores permitidos son C para cuenta, I para COID y S para Servicio no se reconoce el valor '||pv_tipo;
       raise Le_Exception;
     end if;
      begin
        Lv_UPD:='Update fees_tmp x set x.customer_id = :custid, x.seqno = :secuencial, x.remark = :remark  where x.transaction_id = :tx_id and x.id_sva = :id_sva '||Lv_UPD2;
        execute immediate Lv_UPD using PI_CUSTOMER, PI_SEQNO, PV_REMARK, PN_IDTX, PV_IDSVA, Pv_valor, PV_ROWID;

      exception
      when others then
        PV_MSGERR:=Lv_App||' Ocurrio el siguiente error al tratar de actualizar la fees_tmp '||sqlerrm;
        raise Le_Exception;
      end;
      begin
        Lv_INS:='Insert into sva_bita_reverso (customer_id, seqno, transaction_id, id_sva, fecha_evento) values(:custid, :seqno, :tx_id, :id_sva, :fecha)';
        execute immediate Lv_INS using PI_CUSTOMER, PI_SEQNO, PN_IDTX,PV_IDSVA, sysdate;

      exception
      when others then
        PV_MSGERR:=Lv_App||' Ocurrio el siguiente error al tratar de insertar en sva_bita_reverso '||sqlerrm;
        raise Le_Exception;
      end;
  exception
  When Le_Exception then
       null;
  When Others then
       PV_MSGERR:=Lv_App||' Ocurrio el siguiente error al tratar de reversar '||sqlerrm;
  end;

--=====================================================================================--
--Creado por: Wendy Requena
--Proceso Generel que realiza la ceaci�n de un archivo
--=====================================================================================--

Procedure BLP_GENERA_ARCHIVO(Pv_Directorio     In  Varchar2 ,
                             Pv_ArchivoDatos  In  Varchar2 ,
                             Pv_Line          In  Varchar2) Is

  File_Handle                 Utl_File.File_Type;
  Lv_Error                    Varchar2(500)      := Null;
Begin
  File_Handle := Utl_File.fopen(Pv_Directorio, Pv_ArchivoDatos, 'a');
  Utl_File.Put_Line(File_Handle, Pv_Line);
  Utl_File.Fclose(File_Handle);
Exception
  When Others Then
    Lv_Error := Sqlerrm;
End;

end BLK_OCC_INTO_FEES;
/
