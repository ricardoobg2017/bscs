CREATE OR REPLACE PACKAGE MMK_CUENTA_CARTERA IS

  PROCEDURE MM_PROCESA_CUSTOMER(PV_ERROR OUT VARCHAR2,
                                PN_HILO  OUT NUMBER);
  PROCEDURE MM_MEDIOS_CUENTA_CARTERA(pi_hilo INTEGER);

  PROCEDURE MM_PROCESA_CUSTOMER_HILOS(PV_CICLO IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2,
                                      PN_HILO  OUT NUMBER);

  PROCEDURE MM_PROCESA_CUSTOMER_HILOS2(pv_ciclo VARCHAR2,
                                       pi_hilo  INTEGER);
END;
/
CREATE OR REPLACE PACKAGE BODY MMK_CUENTA_CARTERA IS
  ------------------------------------------------
  -- Objetivo: Procesar las cuentas
  -- Author:
  -- CIMA
  -- 27/08/2010
  --
  ------------------------------------------------

  PROCEDURE MM_PROCESA_CUSTOMER(PV_ERROR OUT VARCHAR2,
                                PN_HILO  OUT NUMBER) IS
    LV_ID_PROCESO_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA';
    LV_REFERENCIA_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER';
    LV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP                NUMBER := 0;
    LV_ERROR_SCP                VARCHAR2(4000);
    LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
    LV_PROCESO_PAR_SCP          VARCHAR2(30);
    LN_TOTAL_REGISTROS_SCP      NUMBER := 0;
    LN_ID_BITACORA_SCP          NUMBER := 0;
  
    LV_MENSAJE_APL_SCP VARCHAR2(4000);
    LV_MENSAJE_TEC_SCP VARCHAR2(4000);
    LV_MENSAJE_ACC_SCP VARCHAR2(4000);
    QUERY              VARCHAR2(5000);
    SOURCE_CURSOR      INTEGER;
    ROWS_PROCESSED     INTEGER;
    TOTAL              NUMBER;
    LV_ID_PARAMETRO    VARCHAR2(100) := 'HILOS_CUENTA_CARTERA';
  
    LV_VALOR_PAR_SCP       VARCHAR2(4000);
    LV_DESCRIPCION_PAR_SCP VARCHAR2(500);
  
  BEGIN
    PN_HILO := 0;
    -- SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    PN_HILO := TO_NUMBER(LV_VALOR_PAR_SCP) - 1;
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP = 0 THEN
      LV_MENSAJE_APL_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER';
      LV_MENSAJE_TEC_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER';
      LV_MENSAJE_ACC_SCP := '';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            0,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    ELSE
      RETURN;
    END IF;
  
    ----------------------------------------------------------------------
    SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
    -- SCP:MENSAJE
  
    LV_MENSAJE_APL_SCP := 'Inicio eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Inicio eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    BEGIN
      --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
      QUERY := 'TRUNCATE TABLE MMAG_DATOS_MEDIO_BSCS_TMP';
      EXECUTE IMMEDIATE QUERY;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    
    --QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Fin eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    -----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio eliminacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio eliminacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    BEGIN
      --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
      QUERY := 'DROP TABLE SYSADM.CUST_ID_BASECARGA';
      EXECUTE IMMEDIATE QUERY;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    
    --QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin de eliminacion de la tabla CUST_ID_BASECARGA';
    LV_MENSAJE_TEC_SCP := 'Fin de eliminacion de la tabla CUST_ID_BASECARGA';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'create table SYSADM.CUST_ID_BASECARGA ( CUSTOMER_ID INTEGER not null,';
    QUERY := QUERY || ' ESTADO      VARCHAR2(1)) ';
    QUERY := QUERY ||
             ' tablespace DATA pctfree 10 pctused 40 initrans 1 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
  
    --QUERY := 'COMMIT';
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Fin creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'create index CUST_ID_BASECARGA_IDX1 on CUST_ID_BASECARGA (MOD(CUSTOMER_ID,' ||
             LV_VALOR_PAR_SCP || '))';
    QUERY := QUERY || ' tablespace DATA ';
    QUERY := QUERY || ' pctfree 10 initrans 2 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
  
    QUERY := 'create index CUST_ID_BASECARGA_IDX2 on CUST_ID_BASECARGA (estado)';
    QUERY := QUERY || ' tablespace DATA ';
    QUERY := QUERY || ' pctfree 10 initrans 2 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
    --QUERY := 'COMMIT';
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Fin creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Inicio inserción en CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio inserción en CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'INSERT INTO CUST_ID_BASECARGA select /*+ rule */ a.customer_id, ''A'' from customer_all a, ';
    QUERY := QUERY ||
             ' payment_all  b where a.customer_id = b.customer_id and bank_id in ';
    QUERY := QUERY ||
             ' (select bank_id from list_bankcard WHERE estado = ''A'' and tipo=''C'') and act_used = ''X'' ';
    QUERY := QUERY || ' and paymntresp = ''X''';
  
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);
    TOTAL          := ROWS_PROCESSED;
  
    -- SCP:AVANCE
    LN_REGISTROS_PROCESADOS_SCP := ROWS_PROCESSED;
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                          LN_REGISTROS_PROCESADOS_SCP,
                                          NULL,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    --
  
    QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin de inserción en CUST_ID_BASECARGA. Registros procesados: ' ||
                          TO_CHAR(LN_REGISTROS_PROCESADOS_SCP);
    LV_MENSAJE_TEC_SCP := 'Fin de inserción en CUST_ID_BASECARGA. Registros procesados: ' ||
                          TO_CHAR(LN_REGISTROS_PROCESADOS_SCP);
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    --
  
    -- SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------------
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PN_HILO  := 0;
      PV_ERROR := 'Error MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER : ' ||
                  SQLERRM;
    
      -- SCP:MENSAJE
      LV_MENSAJE_APL_SCP := 'Error en MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER';
      LV_MENSAJE_TEC_SCP := PV_ERROR;
      LV_MENSAJE_ACC_SCP := 'Revise el proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER.';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --
    
      COMMIT;
  END;

  PROCEDURE MM_MEDIOS_CUENTA_CARTERA(pi_hilo INTEGER) IS
  
    --ln_commit number := 0;
    ln_commit                   PLS_INTEGER := 0;
    ln_CUENTA                   PLS_INTEGER := 0;
    LV_ID_PROCESO_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA_PASO2';
    LV_REFERENCIA_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA';
    LV_ID_PARAMETRO             VARCHAR2(100) := 'HILOS_CUENTA_CARTERA';
    LV_ID_PARAMETRO3            VARCHAR2(100) := 'REGISTROS_COMMIT';
    LV_ID_PARAMETRO2            VARCHAR2(100) := 'CICLO_POR_DEFECTO';
    LV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP                NUMBER := 0;
    LV_ERROR_SCP                VARCHAR2(4000);
    LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
    LV_PROCESO_PAR_SCP          VARCHAR2(30);
    LN_TOTAL_REGISTROS_SCP      NUMBER := 0;
    LN_ID_BITACORA_SCP          NUMBER := 0;
    LV_VALOR_PAR_SCP            VARCHAR2(4000);
    LV_MENSAJE_APL_SCP          VARCHAR2(4000);
    LV_MENSAJE_TEC_SCP          VARCHAR2(4000);
    LV_MENSAJE_ACC_SCP          VARCHAR2(4000);
    QUERY                       VARCHAR2(5000);
    LV_DESCRIPCION_PAR_SCP      VARCHAR2(500);
    SOURCE_CURSOR               INTEGER;
    ROWS_PROCESSED              INTEGER;
    TOTAL                       NUMBER;
    PV_ERROR                    VARCHAR2(2000);
    LN_HILO                     NUMBER := 0;
    LN_COMMIT2                  NUMBER := 0;
    LV_CICLO                    VARCHAR2(2);
  
    lv_sentencia VARCHAR2(2000);
    li_seq       INTEGER := 0;
  
  BEGIN
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LN_HILO := TO_NUMBER(LV_VALOR_PAR_SCP);
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO3,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LN_COMMIT2 := TO_NUMBER(LV_VALOR_PAR_SCP);
  
    --LN_HILO := 10;
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO2,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LV_CICLO := LV_VALOR_PAR_SCP;
  
    -- SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP = 0 THEN
      LV_MENSAJE_APL_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA';
      LV_MENSAJE_TEC_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA';
      LV_MENSAJE_ACC_SCP := '';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            0,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    ELSE
      RETURN;
    END IF;
    ----------------------------------------------------------------------
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Inicio inserción en MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Inicio inserción en MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    COMMIT;
    for j in (select a.customer_id,
                     a.rowid
                from CUST_ID_BASECARGA a
               where mod(a.customer_id,
                         LN_HILO) = pi_hilo
                 and estado = 'A')
    
     loop
    
      BEGIN
        lv_sentencia := 'select max(seq_id) from payment_all where bank_id in ';
        --lv_sentencia := lv_sentencia || '(1) ';
        lv_sentencia := lv_sentencia ||
                        ' (select bank_id from list_bankcard WHERE estado = ''A'' ';
        lv_sentencia := lv_sentencia || ' and tipo = ''I'') ';
        lv_sentencia := lv_sentencia || ' AND customer_id=:a';
        EXECUTE IMMEDIATE lv_sentencia
          INTO li_seq
          USING j.customer_id;
      EXCEPTION
        WHEN OTHERS THEN
          li_seq := 0;
      END;
    
      insert into MMAG_DATOS_MEDIO_BSCS_TMP
        SELECT customer_all.customer_id,
               orderhdr_all.ohxact,
               ccontact_all.ccfname,
               ccontact_all.ccname,
               ccontact_all.cclname,
               ccontact_all.ccstreet,
               ccontact_all.cccity,
               ccontact_all.cctn,
               customer_all.termcode,
               customer_all.costcenter_id,
               customer_all.custcode,
               customer_all.prgcode,
               customer_all.cssocialsecno,
               payment_all.valid_thru_date,
               payment_all.bankaccno,
               payment_all.accountowner,
               orderhdr_all.ohrefnum,
               orderhdr_all.ohrefdate,
               orderhdr_all.ohinvamt_doc,
               orderhdr_all.ohopnamt_doc,
               payment_all.bank_id,
               payment_all.payment_type,
               customer_all.cscusttype,
               0 status,
               0 tipo_error,
               get_cbill_code(bank_id) bank_cbill,
               ltrim(rtrim(id_producto)) id_producto,
               LV_CICLO
          from payment_all          payment_all,
               customer_all         customer_all,
               orderhdr_all         orderhdr_all,
               ccontact_all         ccontact_all,
               mm_mapeo_prod_formas m
         where customer_all.customer_id = j.customer_id
           and payment_all.customer_id = customer_all.customer_id
           and orderhdr_all.customer_id = customer_all.customer_id
           and ccontact_all.customer_id = customer_all.customer_id
           and ohopnamt_doc > 0
           and ohstatus <> 'RD'
           and bank_id = m.bank_bscs(+)
           and paymntresp = 'X'
           and ccbill = 'X'
           and payment_all.seq_id = li_seq;
    
      update CUST_ID_BASECARGA m
         set m.estado = 'X'
       where m.rowid = j.rowid;
      ln_CUENTA := ln_CUENTA + 1;
      if ln_commit = LN_COMMIT2 then
        commit;
        ln_commit := 0;
      else
        ln_commit := ln_commit + 1;
      end if;
    
    end loop;
    commit;
    LV_MENSAJE_APL_SCP := 'Fin de inserción en MMAG_DATOS_MEDIO_BSCS_TMP. Registros procesados: ' ||
                          ln_CUENTA;
    LV_MENSAJE_TEC_SCP := 'Fin de inserción en MMAG_DATOS_MEDIO_BSCS_TMP. Registros procesados: ' ||
                          ln_CUENTA;
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    -- SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------------
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA : ' ||
                  SQLERRM;
    
      -- SCP:MENSAJE
      LV_MENSAJE_APL_SCP := 'Error en MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA';
      LV_MENSAJE_TEC_SCP := PV_ERROR;
      LV_MENSAJE_ACC_SCP := 'Revise el proceso MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA.';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --
    
      COMMIT;
  end;
  PROCEDURE MM_PROCESA_CUSTOMER_HILOS(PV_CICLO IN VARCHAR2,
                                      PV_ERROR OUT VARCHAR2,
                                      PN_HILO  OUT NUMBER) IS
    LV_ID_PROCESO_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA_HILO';
    LV_REFERENCIA_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS';
    LV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP                NUMBER := 0;
    LV_ERROR_SCP                VARCHAR2(4000);
    LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
    LV_PROCESO_PAR_SCP          VARCHAR2(30);
    LN_TOTAL_REGISTROS_SCP      NUMBER := 0;
    LN_ID_BITACORA_SCP          NUMBER := 0;
  
    LV_MENSAJE_APL_SCP VARCHAR2(4000);
    LV_MENSAJE_TEC_SCP VARCHAR2(4000);
    LV_MENSAJE_ACC_SCP VARCHAR2(4000);
    QUERY              VARCHAR2(5000);
    SOURCE_CURSOR      INTEGER;
    ROWS_PROCESSED     INTEGER;
    TOTAL              NUMBER;
    LV_ID_PARAMETRO    VARCHAR2(100) := 'HILOS_CUENTA_CARTERA';
  
    LV_VALOR_PAR_SCP       VARCHAR2(4000);
    LV_DESCRIPCION_PAR_SCP VARCHAR2(500);
  
  BEGIN
  
    IF PV_CICLO IS NULL THEN
      PN_HILO  := 0;
      PV_ERROR := 'INGRESE CICLO';
      RETURN;
    END IF;
  
    IF PV_CICLO <> '01' AND PV_CICLO <> '02' AND PV_CICLO <> '03' AND
       PV_CICLO <> '04' AND PV_CICLO <> '05' THEN
      PN_HILO  := 0;
      PV_ERROR := 'INGRESE CICLO CORRECTO';
      RETURN;
    END IF;
  
    PN_HILO := 0;
    -- SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    PN_HILO := TO_NUMBER(LV_VALOR_PAR_SCP) - 1;
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP = 0 THEN
      LV_MENSAJE_APL_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS';
      LV_MENSAJE_TEC_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS';
      LV_MENSAJE_ACC_SCP := '';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            0,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    ELSE
      PN_HILO := 0;
      RETURN;
    END IF;
  
    ----------------------------------------------------------------------
    SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
    -- SCP:MENSAJE
  
    LV_MENSAJE_APL_SCP := 'Inicio eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Inicio eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    BEGIN
      --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
      QUERY := 'TRUNCATE TABLE MMAG_DATOS_MEDIO_BSCS_TMP';
      EXECUTE IMMEDIATE QUERY;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    
    --QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Fin eliminacion de datos de la tabla MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    -----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio eliminacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio eliminacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    BEGIN
      --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
      QUERY := 'DROP TABLE SYSADM.CUST_ID_BASECARGA';
      EXECUTE IMMEDIATE QUERY;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    
    --QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin de eliminacion de la tabla CUST_ID_BASECARGA';
    LV_MENSAJE_TEC_SCP := 'Fin de eliminacion de la tabla CUST_ID_BASECARGA';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'create table SYSADM.CUST_ID_BASECARGA ( CUSTOMER_ID INTEGER not null,';
    QUERY := QUERY || ' ESTADO      VARCHAR2(1)) ';
    QUERY := QUERY ||
             ' tablespace DATA pctfree 10 pctused 40 initrans 1 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
  
    --QUERY := 'COMMIT';
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Fin creacion de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    LV_MENSAJE_APL_SCP := 'Inicio creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'create index CUST_ID_BASECARGA_IDX1 on CUST_ID_BASECARGA (MOD(CUSTOMER_ID,' ||
             LV_VALOR_PAR_SCP || '))';
    QUERY := QUERY || ' tablespace DATA ';
    QUERY := QUERY || ' pctfree 10 initrans 2 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
  
    QUERY := 'create index CUST_ID_BASECARGA_IDX2 on CUST_ID_BASECARGA (estado)';
    QUERY := QUERY || ' tablespace DATA ';
    QUERY := QUERY || ' pctfree 10 initrans 2 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
  
    QUERY := 'create index CUST_ID_BASECARGA_IDX3 on CUST_ID_BASECARGA (CUSTOMER_ID)';
    QUERY := QUERY || ' tablespace DATA ';
    QUERY := QUERY || ' pctfree 10 initrans 2 maxtrans 255 ';
    QUERY := QUERY ||
             ' storage( initial 64K minextents 1 maxextents unlimited ) ';
  
    EXECUTE IMMEDIATE QUERY;
    --QUERY := 'COMMIT';
    /*DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);*/
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Fin creacion del indice de la tabla CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------
    --SOURCE_CURSOR := DBMS_SQL.OPEN_CURSOR;
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Inicio inserción en CUST_ID_BASECARGA.';
    LV_MENSAJE_TEC_SCP := 'Inicio inserción en CUST_ID_BASECARGA.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    QUERY := 'INSERT INTO CUST_ID_BASECARGA SELECT a.customer_id ,''A'' ';
    QUERY := QUERY ||
             ' FROM customer_all a, payment_all  b WHERE a.customer_id = b.customer_id AND b.act_used = ''X'' ';
    QUERY := QUERY || ' and a.paymntresp = ''X'' ';
    QUERY := QUERY ||
             ' AND billcycle IN (SELECT ID_CICLO_ADMIN FROM fa_ciclos_axis_bscs WHERE ID_CICLO= ''' ||
             PV_CICLO || ''')';
    --QUERY := QUERY || ' AND ROWNUM <1000';
    --QUERY := QUERY || ' GROUP BY a.customer_id';
  
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);
    TOTAL          := ROWS_PROCESSED;
  
    -- SCP:AVANCE
    LN_REGISTROS_PROCESADOS_SCP := ROWS_PROCESSED;
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                          LN_REGISTROS_PROCESADOS_SCP,
                                          NULL,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    --
  
    QUERY := 'COMMIT';
    DBMS_SQL.PARSE(SOURCE_CURSOR,
                   QUERY,
                   2);
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Fin de inserción en CUST_ID_BASECARGA. Registros procesados: ' ||
                          TO_CHAR(LN_REGISTROS_PROCESADOS_SCP);
    LV_MENSAJE_TEC_SCP := 'Fin de inserción en CUST_ID_BASECARGA. Registros procesados: ' ||
                          TO_CHAR(LN_REGISTROS_PROCESADOS_SCP);
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    --
  
    -- SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------------
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PN_HILO  := 0;
      PV_ERROR := 'Error MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS : ' ||
                  SQLERRM;
    
      -- SCP:MENSAJE
      LV_MENSAJE_APL_SCP := 'Error en MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS';
      LV_MENSAJE_TEC_SCP := PV_ERROR;
      LV_MENSAJE_ACC_SCP := 'Revise el proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS.';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --
    
      COMMIT;
  END;
  PROCEDURE MM_PROCESA_CUSTOMER_HILOS2(pv_ciclo VARCHAR2,
                                       pi_hilo  INTEGER) IS
  
    --ln_commit number := 0;
    ln_commit                   PLS_INTEGER := 0;
    ln_CUENTA                   PLS_INTEGER := 0;
    LV_ID_PROCESO_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA_HILO_PASO2';
    LV_REFERENCIA_SCP           VARCHAR2(100) := 'MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS2';
    LV_ID_PARAMETRO             VARCHAR2(100) := 'HILOS_CUENTA_CARTERA';
    LV_ID_PARAMETRO3            VARCHAR2(100) := 'REGISTROS_COMMIT';
    LV_ID_PARAMETRO2            VARCHAR2(100) := 'CICLO_POR_DEFECTO';
    LV_UNIDAD_REGISTRO_SCP      VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP                NUMBER := 0;
    LV_ERROR_SCP                VARCHAR2(4000);
    LN_REGISTROS_PROCESADOS_SCP NUMBER := 0;
    LV_PROCESO_PAR_SCP          VARCHAR2(30);
    LN_TOTAL_REGISTROS_SCP      NUMBER := 0;
    LN_ID_BITACORA_SCP          NUMBER := 0;
    LV_VALOR_PAR_SCP            VARCHAR2(4000);
    LV_MENSAJE_APL_SCP          VARCHAR2(4000);
    LV_MENSAJE_TEC_SCP          VARCHAR2(4000);
    LV_MENSAJE_ACC_SCP          VARCHAR2(4000);
    QUERY                       VARCHAR2(5000);
    LV_DESCRIPCION_PAR_SCP      VARCHAR2(500);
    SOURCE_CURSOR               INTEGER;
    ROWS_PROCESSED              INTEGER;
    TOTAL                       NUMBER;
    PV_ERROR                    VARCHAR2(2000);
    LN_HILO                     NUMBER := 0;
    LN_COMMIT2                  NUMBER := 0;
    LV_CICLO                    VARCHAR2(2);
  
    lv_sentencia VARCHAR2(2000);
    li_seq       INTEGER := 0;
  
  BEGIN
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LN_HILO := TO_NUMBER(LV_VALOR_PAR_SCP);
  
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO3,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LN_COMMIT2 := TO_NUMBER(LV_VALOR_PAR_SCP);
  
    --LN_HILO := 10;
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(LV_ID_PARAMETRO2,
                                            LV_PROCESO_PAR_SCP,
                                            LV_VALOR_PAR_SCP,
                                            LV_DESCRIPCION_PAR_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
    LV_CICLO := LV_VALOR_PAR_SCP;
  
    -- SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP = 0 THEN
      LV_MENSAJE_APL_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS2';
      LV_MENSAJE_TEC_SCP := 'Inicio del proceso MMK_CUENTA_CARTERA.MM_PROCESA_CUSTOMER_HILOS2';
      LV_MENSAJE_ACC_SCP := '';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            0,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    ELSE
      RETURN;
    END IF;
    ----------------------------------------------------------------------
  
    -- SCP:MENSAJE
    LV_MENSAJE_APL_SCP := 'Inicio inserción en MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_TEC_SCP := 'Inicio inserción en MMAG_DATOS_MEDIO_BSCS_TMP.';
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    COMMIT;
    for j in (select a.customer_id
                from CUST_ID_BASECARGA a
               where mod(a.customer_id,
                         LN_HILO) = pi_hilo
                 and estado = 'A')
    
     loop
    
      insert into MMAG_DATOS_MEDIO_BSCS_TMP
        SELECT customer_all.customer_id,
               orderhdr_all.ohxact,
               ccontact_all.ccfname,
               ccontact_all.ccname,
               ccontact_all.cclname,
               ccontact_all.ccstreet,
               ccontact_all.cccity,
               ccontact_all.cctn,
               customer_all.termcode,
               customer_all.costcenter_id,
               customer_all.custcode,
               customer_all.prgcode,
               customer_all.cssocialsecno,
               payment_all.valid_thru_date,
               payment_all.bankaccno,
               payment_all.accountowner,
               orderhdr_all.ohrefnum,
               orderhdr_all.ohrefdate,
               orderhdr_all.ohinvamt_doc,
               orderhdr_all.ohopnamt_doc,
               payment_all.bank_id,
               payment_all.payment_type,
               customer_all.cscusttype,
               0 status,
               0 tipo_error,
               get_cbill_code(bank_id) bank_cbill,
               ltrim(rtrim(id_producto)) id_producto,
               PV_CICLO
          from payment_all          payment_all,
               customer_all         customer_all,
               orderhdr_all         orderhdr_all,
               ccontact_all         ccontact_all,
               mm_mapeo_prod_formas m
         WHERE customer_all.customer_id = J.CUSTOMER_ID
           AND customer_all.customer_id = payment_all.customer_id
           and orderhdr_all.customer_id = customer_all.customer_id
           and ccontact_all.customer_id = customer_all.customer_id
           and ohopnamt_doc > 0
           and ohstatus <> 'RD'
           and bank_id = m.bank_bscs(+)
           and paymntresp = 'X'
           and ccbill = 'X'
           and payment_all.act_used = 'X'
        /*AND customer_all.billcycle IN
                                                                                                                               (SELECT ID_CICLO_ADMIN
                                                                                                                                  FROM fa_ciclos_axis_bscs
                                                                                                                                 WHERE ID_CICLO = pv_ciclo)*/
        ;
    
      update CUST_ID_BASECARGA m
         set m.estado = 'X'
       WHERE m.customer_id = J.CUSTOMER_ID;
      --where m.rowid = j.rowid;
    
      ln_CUENTA := ln_CUENTA + 1;
      if ln_commit = LN_COMMIT2 then
        commit;
        ln_commit := 0;
      else
        ln_commit := ln_commit + 1;
      end if;
    
    end loop;
    commit;
  
    LV_MENSAJE_APL_SCP := 'Fin de inserción en MMAG_DATOS_MEDIO_BSCS_TMP. Registros procesados: ' ||
                          ln_CUENTA;
    LV_MENSAJE_TEC_SCP := 'Fin de inserción en MMAG_DATOS_MEDIO_BSCS_TMP. Registros procesados: ' ||
                          ln_CUENTA;
    LV_MENSAJE_ACC_SCP := '';
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                          LV_MENSAJE_APL_SCP,
                                          LV_MENSAJE_TEC_SCP,
                                          LV_MENSAJE_ACC_SCP,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'N',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    -- SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    ----------------------------------------------------------------------------
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA : ' ||
                  SQLERRM;
    
      -- SCP:MENSAJE
      LV_MENSAJE_APL_SCP := 'Error en MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA';
      LV_MENSAJE_TEC_SCP := PV_ERROR;
      LV_MENSAJE_ACC_SCP := 'Revise el proceso MMK_CUENTA_CARTERA.MM_MEDIOS_CUENTA_CARTERA.';
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            LV_MENSAJE_ACC_SCP,
                                            3,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
      --
    
      COMMIT;
  end;
END;
/
