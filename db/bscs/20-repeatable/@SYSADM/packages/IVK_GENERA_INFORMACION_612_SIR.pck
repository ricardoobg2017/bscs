CREATE OR REPLACE PACKAGE IVK_GENERA_INFORMACION_612_SIR IS
  -- AUTHOR  : SAREVALO
  -- AUTHOR  : JSANCHEZ
  -- CREATED : 16/05/2008 16:07:05
  -- PURPOSE : MEJORAS EN LA EXTRACCION DE INFORMACIÓN DE CLIENTES TARIFARIOS
-----------------------------------------------------------------------------------------------------
PROCEDURE IVP_OBJ_FREE_UNITS (PV_ERROR OUT VARCHAR2 );
-----------------------------------------------------------------------------------------------------
PROCEDURE IVP_LLENA_HILOS(PV_ERROR OUT VARCHAR2 );

-----------------------------------------------------------------------------------------------------
PROCEDURE IVP_EXTRAE_DATOS_TAR (PN_HILO NUMBER,
                                  PV_ERROR OUT VARCHAR2);
-----------------------------------------------------------------------------------------------------

PROCEDURE IVP_ACTUALIZA_TMP_612 (pn_hilo number , PV_ERROR OUT VARCHAR2 );
-----------------------------------------------------------------------------------------------------

PROCEDURE IVP_GENERA_INFO_TAR_VL(PV_ERROR OUT VARCHAR2) ;

END IVK_GENERA_INFORMACION_612_SIR;
/
CREATE OR REPLACE PACKAGE BODY IVK_GENERA_INFORMACION_612_SIR IS
  -- AUTHOR  : SAREVALO
  -- AUTHOR  : JSANCHEZ
  -- CREATED : 16/05/2008 16:07:05
  -- PURPOSE : MEJORAS EN LA EXTRACCION DE INFORMACIÓN

PROCEDURE IVP_OBJ_FREE_UNITS (PV_ERROR OUT VARCHAR2 ) IS
/*
Este procedimiento cargara la data de la tabla free_units de RTX a una tabla del mismo nombre con
estructura similar pero en BSCS
*/
LN_COUNT NUMBER;


-- SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0;
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='IVP_OBJ_FREE_UNITS';
lv_referencia_scp varchar2(100):='IVK_GENERA_INFORMACION_612_TMP.IVP_OBJ_FREE_UNITS';
lv_unidad_registro_scp varchar2(30):='telefonos';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------



BEGIN

-- SCP:INICIO
----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <>0 then
   return;
end if;
----------------------------------------------------------------------

SELECT COUNT(*) INTO LN_COUNT FROM FREE_UNITS ;

IF LN_COUNT > 0 THEN
DELETE FREE_UNITS;
NULL;

END IF;

-- SCP:MENSAJE
----------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de mensaje de error
----------------------------------------------------------------------
lv_mensaje_apl_scp:='Inicio de copia de tabla FREE_UNITS.';
lv_mensaje_tec_scp:='Inicio de copia de tabla FREE_UNITS.';
lv_mensaje_acc_scp:='';
scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

INSERT INTO FREE_UNITS ( SELECT  FUT.TMCODE,
                                 FUT.DES,
                                 FUT.UNITS,
                                 FUT.POOL,
                                 FUT.DOLARES,
                                 FUT.TIPO ,
                                 NULL
                         FROM FREE_UNITS@bscs_to_rtx_link fut

                       );
COMMIT;
-- SCP:MENSAJE
----------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de mensaje de error
----------------------------------------------------------------------
lv_mensaje_apl_scp:='Fin de copia de tabla FREE_UNITS.';
lv_mensaje_tec_scp:='Fin de copia de tabla FREE_UNITS.';
lv_mensaje_acc_scp:='';
scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

-- SCP:FIN
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------
COMMIT;
EXCEPTION
WHEN OTHERS THEN
ROLLBACK;
PV_ERROR := SUBSTR(SQLERRM,1,150);

-- SCP:MENSAJE
----------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de mensaje de error
----------------------------------------------------------------------
lv_mensaje_apl_scp:='Error en la copia de tabla FREE_UNITS.';
lv_mensaje_tec_scp:= PV_ERROR;
lv_mensaje_acc_scp:='';
scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

END IVP_OBJ_FREE_UNITS;


PROCEDURE IVP_LLENA_HILOS (PV_ERROR OUT VARCHAR2 ) IS
/*
Procedimiento que asigna los hilos a cada uno de los
registros de la tabla free_units de BSCS
*/

 CURSOR C_HILOS IS
   SELECT T.ROWID
   FROM FREE_UNITS T;



LN_NRO_HILOS  NUMBER(2):=3;
LN_HILOS      NUMBER(2):=0;
LE_ERROR      EXCEPTION;

-- SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0;
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='IVP_LLENA_HILOS';
lv_referencia_scp varchar2(100):='IVK_GENERA_INFORMACION_612_TMP.IVP_LLENA_HILOS';
lv_unidad_registro_scp varchar2(30):='telefonos';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------

BEGIN

-- SCP:INICIO
----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <>0 then
   return;
end if;
----------------------------------------------------------------------

-- SCP:PARAMETRO
--------------------------------------------------
-- SCP: Código generado automáticamente. Lectura de prámetros
--------------------------------------------------
scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('IVR_HILOS_EXTRACCION',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <> 0 then
   lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
   lv_mensaje_tec_scp:=lv_error_scp;
   lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   return;
end if;
--------------------------------------------------

LN_NRO_HILOS := TO_NUMBER(lv_valor_par_scp);

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Inicio de actualizacion de FREE_UNITS con el número de hilo.';
  lv_mensaje_tec_scp:='Inicio de actualizacion de FREE_UNITS con el número de hilo.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------


  FOR I IN C_HILOS LOOP

    LN_HILOS := LN_HILOS + 1 ;

    UPDATE FREE_UNITS T
      SET T.ID_HILO = LN_HILOS
    WHERE ROWID = I.ROWID;

    ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
    IF LN_HILOS  =  LN_NRO_HILOS THEN
       LN_HILOS := 0;
       COMMIT;
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
    END IF;

  END LOOP;
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Fin de actualizacion de FREE_UNITS con el número de hilo.';
  lv_mensaje_tec_scp:='Fin de actualizacion de FREE_UNITS con el número de hilo.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Truncando tabla IVR_TARIFARIOS_612.';
  lv_mensaje_tec_scp:='Truncando tabla IVR_TARIFARIOS_612.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

   EXECUTE IMMEDIATE 'truncate table IVR_TARIFARIOS_612';

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Tabla IVR_TARIFARIOS_612 truncada con exito.';
  lv_mensaje_tec_scp:='Tabla IVR_TARIFARIOS_612 truncada con exito.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

-- SCP:FIN
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

   EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'NO SE ENCONTRO CANTIDAD DE HILOS PARA ASIGNAR';

    -- SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp:='Error en la actualización de tabla FREE_UNITS.';
    lv_mensaje_tec_scp:= PV_ERROR;
    lv_mensaje_acc_scp:='';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
COMMIT;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := SUBSTR(SQLERRM,1,150);

    -- SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp:='Error en la actualización de tabla FREE_UNITS.';
    lv_mensaje_tec_scp:= PV_ERROR;
    lv_mensaje_acc_scp:='';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

END IVP_LLENA_HILOS;
-------------------------------------------------------------------------------------------------------------
PROCEDURE IVP_EXTRAE_DATOS_TAR  (  PN_HILO NUMBER,
                                   PV_ERROR OUT VARCHAR2
                                  ) IS

/*
procedimiento que extrae informacion de clientes tarifario
*/
---Definición de tipos de dato para implementar bulk collect
     TYPE  LT_CUSTOMER_ID        IS TABLE OF  CUSTOMER_ALL.CUSTOMER_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CUSTOMER_ID_HIGH   IS TABLE OF  CUSTOMER_ALL.CUSTOMER_ID_HIGH%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CUSTCODE           IS TABLE OF  CUSTOMER_ALL.CUSTCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TMCODE             IS TABLE OF  CUSTOMER_ALL.TMCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_BILLCYCLE          IS TABLE OF  CUSTOMER_ALL.BILLCYCLE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CO_ID              IS TABLE OF  CONTRACT_ALL.CO_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TMCODE1            IS TABLE OF  CONTRACT_ALL.TMCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DN_ID              IS TABLE OF  CONTR_SERVICES_CAP.DN_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CS_ACTIV_DATE      IS TABLE OF  CONTR_SERVICES_CAP.CS_ACTIV_DATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CS_DEACTIV_DATE    IS TABLE OF  CONTR_SERVICES_CAP.CS_DEACTIV_DATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CH_STATUS          IS TABLE OF  CURR_CO_STATUS.CH_STATUS%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CH_VALIDFROM       IS TABLE OF CURR_CO_STATUS.CH_VALIDFROM%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_ENTDATE            IS TABLE OF CURR_CO_STATUS.ENTDATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DES                IS TABLE OF FREE_UNITS.DES%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_UNITS              IS TABLE OF FREE_UNITS.UNITS%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_POOL               IS TABLE OF FREE_UNITS.POOL%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DOLARES            IS TABLE OF FREE_UNITS.DOLARES%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TIPO               IS TABLE OF FREE_UNITS.TIPO%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_PRGCODE            IS TABLE OF CUSTOMER_ALL.PRGCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_PAYMNTRESP         IS TABLE OF CUSTOMER_ALL.PAYMNTRESP%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CSTRADECODE        IS TABLE OF CUSTOMER_ALL.CSTRADECODE%TYPE INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------------------------------------


-----------------cursor------------

cursor c_ivr (cn_hilo number )  is
 select 
        cus.customer_id,
       cus.customer_id_high,
       cus.custcode,
       cus.tmcode,
       cus.billcycle,
       con.co_id,
       con.tmcode,
       cse.dn_id,
       cse.cs_activ_date,
       cse.cs_deactiv_date,
       cst.ch_status,
       cst.CH_VALIDFROM,
       cst.ENTDATE,
       fut.des,
       fut.units,
       fut.pool,
       fut.dolares,
       fut.tipo,
       cus.prgcode,
       cus.paymntresp,
       cus.cstradecode
   from customer_all_ivr       cus,
       contract_all_ivr        con,
       contr_services_cap_ivr  cse,
       curr_co_status      cst,
       free_units fut
   where cus.customer_id = con.customer_id
   and con.co_id = cse.co_id
   and cse.cs_activ_date=(select max(x.cs_activ_date)
                            from contr_services_cap_ivr x
                           where x.co_id=con.co_id)
   and con.co_id=cst.co_id
   and con.tmcode=fut.tmcode
   and cst.ch_status in ('a','s')
   and (
       cse.cs_deactiv_date is null
       or cse.cs_deactiv_date > sysdate-45
       )
--       and fut.id_hilo = cn_hilo
--   AND CUS.CUSTOMER_ID = 200379
--   AND CON.CO_ID = 1185310;
   order by cus.customer_id, con.co_id;
-----------------------------------

------------Declaracion de Variables type para uso del bulk collect------------------------------------------
     LA_CUSTOMER_ID              LT_CUSTOMER_ID     ;
     LA_CUSTOMER_ID_HIGH         LT_CUSTOMER_ID_HIGH;
     LA_CUSTCODE                 LT_CUSTCODE;
     LA_TMCODE                   LT_TMCODE;
     LA_BILLCYCLE                LT_BILLCYCLE;
     LA_CO_ID                    LT_CO_ID;
     LA_TMCODE1                  LT_TMCODE1;
     LA_DN_ID                    LT_DN_ID;
     LA_CS_ACTIV_DATE            LT_CS_ACTIV_DATE;
     LA_CS_DEACTIV_DATE          LT_CS_DEACTIV_DATE;
     LA_CH_STATUS                LT_CH_STATUS;
     LA_CH_VALIDFROM             LT_CH_VALIDFROM;
     LA_ENTDATE                  LT_ENTDATE;
     LA_DES                      LT_DES;
     LA_UNITS                    LT_UNITS;
     LA_POOL                     LT_POOL;
     LA_DOLARES                  LT_DOLARES;
     LA_TIPO                     LT_TIPO;
     LA_PRGCODE                  LT_PRGCODE;
     LA_PAYMNTRESP               LT_PAYMNTRESP;
     LA_CSTRADECODE              LT_CSTRADECODE;
-------------------------------------------------------------------------------------------------------------

--------otras variables--------------------------
     LN_HILO                     NUMBER;
     LN_CON_ROW                  NUMBER;
     LN_COUNT                    NUMBER;
     LV_INSERT                   VARCHAR2(10000);
     LV_INSERT_1                   VARCHAR2(10000);
     LV_INSERT_2                   VARCHAR2(10000);
     LV_MENSAJE                  VARCHAR2(1000);
     LV_ERROR                    VARCHAR2(201);
     LE_ERROR                    EXCEPTION;
     ln_flag                     number;

-------------------------------------------------

-- SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0;
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='IVP_EXTRAE_DATOS_TAR';
lv_referencia_scp varchar2(100):='IVK_GENERA_INFORMACION_612_TMP.IVP_EXTRAE_DATOS_TAR('||to_char(PN_HILO)||')';
lv_unidad_registro_scp varchar2(30):='telefonos';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------

LN_REG_COMMIT NUMBER;
LN_LIM_BULK NUMBER;

BEGIN

-- SCP:INICIO
----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <>0 then
   return;
end if;
----------------------------------------------------------------------

-- SCP:PARAMETRO
--------------------------------------------------
-- SCP: Código generado automáticamente. Lectura de prámetros
--------------------------------------------------
scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('IVR_COMMIT_EXTRACCION',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <> 0 then
   lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
   lv_mensaje_tec_scp:=lv_error_scp;
   lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   return;
end if;
--------------------------------------------------
LN_REG_COMMIT:=TO_NUMBER(lv_valor_par_scp);

-- SCP:PARAMETRO
--------------------------------------------------
-- SCP: Código generado automáticamente. Lectura de prámetros
--------------------------------------------------
scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('IVR_LIMIT_EXTRACCION',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <> 0 then
   lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
   lv_mensaje_tec_scp:=lv_error_scp;
   lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   return;
end if;
--------------------------------------------------
LN_LIM_BULK:=TO_NUMBER(lv_valor_par_scp);

ln_flag := 1;
     LN_CON_ROW := 0 ;
     LN_HILO    := PN_HILO;

      IF LN_FLAG = 1   THEN
            LV_INSERT  := 'INSERT INTO IVR_TARIFARIOS_612 NOLOGGING  (CUSTOMER_ID, CUSTOMER_ID_HIGH, CUSTCODE,TMCODE_CUS,';
            LV_INSERT  := LV_INSERT || ' BILLCYCLE, CO_ID, TMCODE_CON, DN_ID, CS_ACTIV_DATE, CS_DEACTIV_DATE, ';
            LV_INSERT  := LV_INSERT || ' CH_STATUS, CH_VALIDFROM, ENTDATE, DES, UNITS, POOL, DOLARES, TIPO,';
            LV_INSERT  := LV_INSERT || ' PRGCODE, PAYMNTRESP, CSTRADECODE, id_hilo )';
--            LV_INSERT  := LV_INSERT || ' (';
            LV_INSERT  := LV_INSERT || ' SELECT CUS.CUSTOMER_ID,CUS.CUSTOMER_ID_HIGH,CUS.CUSTCODE,CUS.TMCODE,CUS.BILLCYCLE,CON.CO_ID,CON.TMCODE TMCODE_CON,';
            LV_INSERT  := LV_INSERT || ' CSE.DN_ID,CSE.CS_ACTIV_DATE,CSE.CS_DEACTIV_DATE,CST.CH_STATUS,CST.CH_VALIDFROM,CST.ENTDATE, ';
            LV_INSERT  := LV_INSERT || ' FUT.DES,FUT.UNITS,FUT.POOL,FUT.DOLARES,FUT.TIPO,CUS.PRGCODE,CUS.PAYMNTRESP,CUS.CSTRADECODE,0 ';
            LV_INSERT  := LV_INSERT || ' FROM CUSTOMER_ALL_IVR CUS, CONTRACT_ALL_IVR CON, CONTR_SERVICES_CAP_IVR CSE,CURR_CO_STATUS CST,FREE_UNITS FUT ';
            LV_INSERT  := LV_INSERT || ' WHERE CUS.CUSTOMER_ID = CON.CUSTOMER_ID AND CON.CO_ID = CSE.CO_ID AND CSE.CS_ACTIV_DATE = ';
            LV_INSERT  := LV_INSERT || ' (SELECT MAX(X.CS_ACTIV_DATE) FROM CONTR_SERVICES_CAP_IVR X WHERE X.CO_ID = CON.CO_ID) ';
            LV_INSERT  := LV_INSERT || ' AND CON.CO_ID = CST.CO_ID AND CON.TMCODE = FUT.TMCODE AND CST.CH_STATUS IN (''a'', ''s'')';
            LV_INSERT_1  := ' AND (CSE.CS_DEACTIV_DATE IS NULL OR CSE.CS_DEACTIV_DATE > SYSDATE - 45)';
--            LV_INSERT_1  := LV_INSERT_1 || ')';
            LV_INSERT_2  := LV_INSERT || LV_INSERT_1;
            EXECUTE IMMEDIATE LV_INSERT_2;
/*
  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Inicio de Bulk Collect de extraccion.';
  lv_mensaje_tec_scp:='Inicio de Bulk Collect de extraccion.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------


         OPEN C_IVR (LN_HILO);

           LOOP
             FETCH   C_IVR
                   BULK COLLECT INTO
                     LA_CUSTOMER_ID ,
                     LA_CUSTOMER_ID_HIGH,
                     LA_CUSTCODE,
                     LA_TMCODE,
                     LA_BILLCYCLE,
                     LA_CO_ID,
                     LA_TMCODE1,
                     LA_DN_ID,
                     LA_CS_ACTIV_DATE,
                     LA_CS_DEACTIV_DATE,
                     LA_CH_STATUS,
                     LA_CH_VALIDFROM,
                     LA_ENTDATE,
                     LA_DES,
                     LA_UNITS,
                     LA_POOL,
                     LA_DOLARES,
                     LA_TIPO,
                     LA_PRGCODE,
                     LA_PAYMNTRESP,
                     LA_CSTRADECODE LIMIT LN_LIM_BULK;

              EXIT WHEN C_IVR%NOTFOUND;
           END LOOP;

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Fin de Bulk Collect de extraccion.';
  lv_mensaje_tec_scp:='Fin de Bulk Collect de extraccion.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Inicio de Inserción de datos en tabla IVR_TARIFARIOS_612.';
  lv_mensaje_tec_scp:='Inicio de Inserción de datos en tabla IVR_TARIFARIOS_612.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------


            LN_COUNT :=  LA_CUSTOMER_ID.COUNT;

           IF LN_COUNT > 0 THEN

               FOR I IN LA_CUSTOMER_ID.FIRST .. LA_CUSTOMER_ID.LAST LOOP
                  BEGIN
                   LN_CON_ROW := LN_CON_ROW + 1;

                   EXECUTE IMMEDIATE   LV_INSERT   USING
                                                   LA_CUSTOMER_ID(I) ,
                                                   LA_CUSTOMER_ID_HIGH(I),
                                                   LA_CUSTCODE(I),
                                                   LA_TMCODE(I),
                                                   LA_BILLCYCLE(I),
                                                   LA_CO_ID(I),
                                                   LA_TMCODE1(I),
                                                   LA_DN_ID(I),
                                                   LA_CS_ACTIV_DATE(I),
                                                   LA_CS_DEACTIV_DATE(I),
                                                   LA_CH_STATUS(I),
                                                   LA_CH_VALIDFROM(I),
                                                   LA_ENTDATE(I),
                                                   LA_DES(I),
                                                   LA_UNITS(I),
                                                   LA_POOL(I),
                                                   LA_DOLARES(I),
                                                   LA_TIPO(I),
                                                   LA_PRGCODE(I),
                                                   LA_PAYMNTRESP(I),
                                                   LA_CSTRADECODE(I) ,
--                                                   1;
                                                   LN_HILO;

                   ln_registros_procesados_scp:=ln_registros_procesados_scp+1;

                   IF LN_CON_ROW >= LN_REG_COMMIT THEN
                      COMMIT;
                      LN_CON_ROW := 0 ;
                      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
                   END IF;


                   EXCEPTION
                     WHEN OTHERS THEN
                       PV_ERROR := SUBSTR(SQLERRM,1,200);
                   -- SCP:MENSAJE
                    ----------------------------------------------------------------------
                    -- SCP: Código generado automáticamente. Registro de mensaje de error
                    ----------------------------------------------------------------------
                    lv_mensaje_apl_scp:='Error en insercion de datos en  IVR_TARIFARIOS_612 ('||pn_hilo||')';
                    lv_mensaje_tec_scp:=PV_ERROR;
                    lv_mensaje_acc_scp:='';
                    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
                    ----------------------------------------------------------------------------
                    END;
               END LOOP;

                   COMMIT;
                   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);

              -- SCP:MENSAJE
              ----------------------------------------------------------------------
              -- SCP: Código generado automáticamente. Registro de mensaje de error
              ----------------------------------------------------------------------
              lv_mensaje_apl_scp:='Fin de Inserción de datos en tabla IVR_TARIFARIOS_612.';
              lv_mensaje_tec_scp:='Fin de Inserción de datos en tabla IVR_TARIFARIOS_612.';
              lv_mensaje_acc_scp:='';
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
              ----------------------------------------------------------------------------



                    ---llamamos al proceso de actualizacion... que tambien se hara por hilos...
                    BEGIN

                      -- SCP:MENSAJE
                      ----------------------------------------------------------------------
                      -- SCP: Código generado automáticamente. Registro de mensaje de error
                      ----------------------------------------------------------------------
                      lv_mensaje_apl_scp:='Inicio de Actualización de datos adicionales en IVR_TARIFARIOS_612.';
                      lv_mensaje_tec_scp:='Inicio de Actualización de datos adicionales en IVR_TARIFARIOS_612.';
                      lv_mensaje_acc_scp:='';
                      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
                      ----------------------------------------------------------------------------
*/


                     LN_HILO:= PN_HILO ;
                     LN_HILO:= 0 ;
                     IVK_GENERA_INFORMACION_612_SIR.IVP_ACTUALIZA_TMP_612( LN_HILO, LV_ERROR );
                     --LV_ERROR:=null;
                     IF LV_ERROR IS NOT NULL THEN
                     PV_ERROR:= LV_ERROR ;

                     END IF;
/*
                      -- SCP:MENSAJE
                      ----------------------------------------------------------------------
                      -- SCP: Código generado automáticamente. Registro de mensaje de error
                      ----------------------------------------------------------------------
                      lv_mensaje_apl_scp:='Fin de Actualización de datos adicionales en IVR_TARIFARIOS_612.';
                      lv_mensaje_tec_scp:='Fin de Actualización de datos adicionales en IVR_TARIFARIOS_612.';
                      lv_mensaje_acc_scp:='';
                      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
                      ----------------------------------------------------------------------------


                     EXCEPTION
                     WHEN OTHERS THEN
                       PV_ERROR := SUBSTR(SQLERRM,1,200);
                       -- SCP:MENSAJE
                       ----------------------------------------------------------------------
                       -- SCP: Código generado automáticamente. Registro de mensaje de error
                       ----------------------------------------------------------------------
                       lv_mensaje_apl_scp:='Error en la Actualización de datos adicionales en IVR_TARIFARIOS_612. Hilo('||pn_hilo||')';
                       lv_mensaje_tec_scp:= PV_ERROR;
                       lv_mensaje_acc_scp:='';
                       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
                       ----------------------------------------------------------------------------
                     END;

               ELSE
                     LV_MENSAJE := 'NO SE ENCONTRARON DATOS PARA EL HILO NUMERO ' || LN_HILO;
                      ----------------------------------------------------------------------
                      -- SCP: Código generado automáticamente. Registro de mensaje de error
                      ----------------------------------------------------------------------
                      lv_mensaje_apl_scp:='Error en la extracción de los datos a guardar en la tabla IVR_TARIFARIOS_612.';
                      lv_mensaje_tec_scp:='Error en la extracción de los datos: '||LV_MENSAJE;
                      lv_mensaje_acc_scp:='';
                      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
                      ----------------------------------------------------------------------------

                     RAISE LE_ERROR;
               END IF ;
*/
     END IF;  --end flag = 1

-- SCP:FIN
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------
COMMIT;


   EXCEPTION
   WHEN LE_ERROR THEN
   PV_ERROR :=  LV_MENSAJE ;

----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

   WHEN OTHERS THEN
   PV_ERROR := SUBSTR(SQLERRM,1,200);
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

end;

-------------------------------------------------------------------------------------------------------------
   --PROCEDIMIENTO QUE ACTUALIZARA LA TABLA IVR_TMP_612 LA INFORMACION RESTANTE
-------------------------------------------------------------------------------------------------------------
PROCEDURE IVP_ACTUALIZA_TMP_612 (pn_hilo number , PV_ERROR OUT VARCHAR2 ) IS
/*
procedimiento llamado por el IVP_EXTRAE_DATOS_TAR para actualizacion de campos que faltaron en la extracción
de datos de clientes tarifario
*/

-- SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0;
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='IVP_ACTUALIZA_TMP_612';
lv_referencia_scp varchar2(100):='IVK_GENERA_INFORMACION_612_TMP.IVP_ACTUALIZA_TMP_612('||to_char(PN_HILO)||')';
lv_unidad_registro_scp varchar2(30):='telefonos';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------
LN_REG_COMMIT NUMBER;


------------Variables type para el co_id-
TYPE TL_CO_ID       IS TABLE OF IVR_TARIFARIOS_612.CO_ID%TYPE INDEX BY BINARY_INTEGER;
TYPE TL_CUSTOMER_ID IS TABLE OF IVR_TARIFARIOS_612.CUSTOMER_ID%TYPE INDEX BY BINARY_INTEGER;
TYPE TL_CICLO IS TABLE OF IVR_TARIFARIOS_612.BILLCYCLE%TYPE INDEX BY BINARY_INTEGER;
TYPE TL_CS_ACTIV_DATE IS TABLE OF IVR_TARIFARIOS_612.CS_ACTIV_DATE%TYPE INDEX BY BINARY_INTEGER;
LA_CO_ID          TL_CO_ID;
LA_CUSTOMER_ID    TL_CUSTOMER_ID;
LA_CICLO          TL_CICLO;
LA_CS_ACTIV_DATE  TL_CS_ACTIV_DATE;
------------Variables para las DML ------
LV_QUERY              VARCHAR2(2500);
LV_QUERY_1            VARCHAR2(1500);
LV_QUERY_2            VARCHAR2(1500);
LV_QUERY_3            VARCHAR2(1500);
LV_UPDATE             VARCHAR2(1000);
-----------------------------------------

------variables para el update-----------
LV_TMCODE             INTEGER;
LV_TMCODE_DATE        DATE;
LV_TMCODE_DATE_HIST   DATE;
LV_DN_NUM             VARCHAR2(63);
LV_PREV_BALANCE       NUMBER;
LD_DATE_LAST_BILL     DATE;
LD_DATE_NEXT_BILL     DATE;
-----------------------------------------

--------otras ---------------------------
LV_ID_CICLO           VARCHAR2(2);
LV_CICLO_BSCS         VARCHAR2(2);
LV_INICIO_PERIODO     VARCHAR2(10); -- variable para sacar la fecha de inicio del periodo actual
LV_FIN_PERIODO        VARCHAR2(10); -- variable para sacar la fecha en que finaliza el periodo actual
LV_DESC_CICLO         VARCHAR2(60);
LV_CODIGOERROR        VARCHAR2(2000);
LN_CON_ROW            NUMBER;
LN_HILO               NUMBER;
LN_CO_ID              NUMBER;
LN_CUSTOMER_ID        NUMBER;
LN_CICLO              NUMBER;
LN_DIAS_PERIODO       NUMBER;
LN_DIAS_NO_FACTURADOS NUMBER;
LN_CS_ACTIV_DATE      DATE;
-----------------------------------------
BEGIN
LN_HILO  := PN_HILO;
-----------------------------------------

-- SCP:INICIO
----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <>0 then
 return;
end if;
----------------------------------------------------------------------
-- SCP:PARAMETRO
--------------------------------------------------
-- SCP: Código generado automáticamente. Lectura de prámetros
--------------------------------------------------
scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('IVR_COMMIT_ACTUALIZACION',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <> 0 then
   lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
   lv_mensaje_tec_scp:=lv_error_scp;
   lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   return;
end if;
--------------------------------------------------
LN_REG_COMMIT:=TO_NUMBER(lv_valor_par_scp);


-----------------------------------------



---------- estos querys retorna los campos a actualizar -------------
LV_QUERY   :=
               'select '||
               '       rph.tmcode, '||
               '       rph.tmcode_date,         '||
               '       rph1.tmcode_date         '||
               'from   contract_all       con,  '||
               '       contr_services_cap cse,  '||
               '       curr_co_status     cst,  '||
               '       rateplan_hist      rph1, '||
               '       (select co_id,  '||
               '               seqno,  '||
               '               tmcode, '||
               '               tmcode_date,  '||
               '               userlastmod,  '||
               '               rec_version,  '||
               '               request_id,   '||
               '               transactionno '||
               '        from rateplan_hist   '||
               '        where tmcode_date > sysdate-45 '||
               '          and co_id= :1 '||
               '          and tmcode_date =(  '||
               '                           Select Max(tmcode_date) '||
               '                           From rateplan_hist '||
               '                           Where co_id = :2 '||
               '                           and tmcode_date< (Select Max(tmcode_date) '||
               '                                             From rateplan_hist '||
               '                                             Where co_id = :3) '||
               '                          )  '||
               '       ) rph '||
               'where  con.co_id = :4     '||
               'and con.co_id = cse.co_id '||
               'and cse.cs_activ_date=(select max(x.cs_activ_date)  '||
               '                          from contr_services_cap x '||
               '                          where x.co_id=:5) '||
               'and con.co_id=cst.co_id    '||
               'and con.co_id=rph.co_id(+) '||
               'And con.co_id=rph1.co_id   '||
               'And rph1.tmcode_date = ( Select Max(tmcode_date) '||
               '                         From rateplan_hist '||
               '                         Where co_id = :6   '||
               '                          )         '||
               'and cst.ch_status in (''a'',''s'')  '||
               'and ( ' ||
               '     cse.cs_deactiv_date is null '||
               '      or cse.cs_deactiv_date > sysdate-45) ';
----------------------------------------------------------------------------------
LV_QUERY_1 :=
               'select '||
               '     dnu.dn_num, '||
               '     decode(nvl(to_char(cus.customer_id_high),''NULO''),''NULO'',cus.prev_balance, cuh.prev_balance) '||
               'from customer_all       cus, '||
               '     contract_all       con, '||
               '     contr_services_cap cse, '||
               '     directory_number   dnu, '||
               '     curr_co_status     cst, '||
               '     customer_all       cuh  '||
               'where con.co_id = :1         '||
               'and cus.customer_id = con.customer_id  '||
               'and con.co_id = cse.co_id              '||
               'and cse.dn_id = dnu.dn_id              '||
               'and cse.cs_activ_date=(select max(x.cs_activ_date)      '||
               '                              from contr_services_cap x '||
               '                              where x.co_id= :2         '||
               '                       ) '||
               'and con.co_id=cst.co_id  '||
               'and cus.customer_id_high= cuh.customer_id(+)    '||
               'and cst.ch_status in (''a'',''s'')              '||
               'and (                                           '||
               '    cse.cs_deactiv_date is null                 '||
               '    or cse.cs_deactiv_date > sysdate-45 )       ';
---------------------------------------------------------------------------
LV_QUERY_2 :=  'select  max(lbc_date) date_last_bill '||
               'from lbc_date_hist where customer_id = :1' ;
---------------------------------------------------------------------------

---------------------------------------------------------------------------
LV_QUERY_3 := ' SELECT f.id_ciclo' ||
              ' FROM fa_ciclos_axis_bscs f, fa_ciclos_bscs c'||
              ' WHERE f.id_ciclo = c.id_ciclo'||
              ' AND f.id_ciclo_admin = :1 ';
---------------------------------------------------------------------------



LV_UPDATE  :=
                ' update /*+append+*/  IVR_TARIFARIOS_612 nologging '||
                '    set tmcode_hist      =  :1 , '||
                '        tmcode_date      =  :2 , '||
                '        tmcode_date_hist =  :3 , '||
                '        dn_num           =  :4 , '||
                '        prev_balance     =  :5 , '||
                '        date_last_bill   =  :6 , '||
                '        DATE_NEXT_BILL   =  :7   '||
                '    where co_id =  :8            '||
                '    and id_hilo =  :9            ';


  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Inicio de Bulk Collect de modificacion.';
  lv_mensaje_tec_scp:='Inicio de Bulk Collect de modificacion.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

 

-- extraigo todos los co_id de la IVR_TARIFARIOS_612
    SELECT /*+INDEX(T, IVR_TARIFARIOS_612_IND2)+*/ CO_ID, CUSTOMER_ID, BILLCYCLE , CS_ACTIV_DATE
    BULK COLLECT INTO LA_CO_ID, LA_CUSTOMER_ID, LA_CICLO , LA_CS_ACTIV_DATE
    FROM  IVR_TARIFARIOS_612 T
    WHERE ID_HILO  = LN_HILO ;


  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Fin de Bulk Collect de modificacion.';
  lv_mensaje_tec_scp:='Fin de Bulk Collect de modificacion.';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

  LN_CON_ROW:=0;

  -- SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp:='Inicio de Modificacion de datos en tabla IVR_TARIFARIOS_612 ('||pn_hilo||')';
  lv_mensaje_tec_scp:='Inicio de Modificacion de datos en tabla IVR_TARIFARIOS_612 ('||pn_hilo||')';
  lv_mensaje_acc_scp:='';
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------

   FOR I IN LA_CO_ID.FIRST .. LA_CO_ID.LAST LOOP
     BEGIN
     LN_CO_ID         := LA_CO_ID(I);
     LN_CUSTOMER_ID   := LA_CUSTOMER_ID(I);
     LN_CICLO         := LA_CICLO(i);
     LN_CS_ACTIV_DATE := LA_CS_ACTIV_DATE(i);
     BEGIN
           INSERT INTO ivr_control_cli_ivr
             VALUES (pn_hilo,LN_CUSTOMER_ID,LN_CO_ID,SYSDATE,'pruebas');
             COMMIT;

           EXECUTE IMMEDIATE LV_QUERY   INTO  LV_TMCODE, LV_TMCODE_DATE, LV_TMCODE_DATE_HIST  USING LN_CO_ID, LN_CO_ID, LN_CO_ID, LN_CO_ID, LN_CO_ID, LN_CO_ID  ;
           EXECUTE IMMEDIATE LV_QUERY_1 INTO  LV_DN_NUM, LV_PREV_BALANCE                      USING LN_CO_ID, LN_CO_ID;
           EXECUTE IMMEDIATE LV_QUERY_2 INTO  LD_DATE_LAST_BILL                               USING LN_CUSTOMER_ID;
           EXECUTE IMMEDIATE LV_QUERY_3 INTO  LV_ID_CICLO                  USING LN_CICLO;
           
           ivr_obtiene_ciclo(pd_fecha       => NULL ,
                                     pv_ciclo_axis  => LV_ID_CICLO ,
                                     pv_ciclo_bscs  => LV_CICLO_BSCS,
                                     pv_per_ini     => LV_INICIO_PERIODO,
                                     pv_per_fin     => LV_FIN_PERIODO,
                                     pv_desc_ciclo  => LV_DESC_CICLO,
                                     Pv_CodigoError => LV_CODIGOERROR         
                                     );
                                     
           LN_DIAS_PERIODO       := to_number(to_date(LV_FIN_PERIODO,'dd/mm/yyyy')+1 - to_date(LV_INICIO_PERIODO,'dd/mm/yyyy'));
           --Consulto lbc_date_hist mediante el QUERY_2 si ld_date_las_bill es nulo es posible que sea cuenta nueva
           IF LD_DATE_LAST_BILL IS NULL THEN
                  LN_DIAS_NO_FACTURADOS := to_number(to_date(LV_FIN_PERIODO,'dd/mm/yyyy')+1 - trunc(LN_CS_ACTIV_DATE));
                  IF LN_DIAS_NO_FACTURADOS>LN_DIAS_PERIODO THEN
                     LD_DATE_LAST_BILL := to_date(LV_INICIO_PERIODO,'dd/mm/yyyy');
                     LD_DATE_NEXT_BILL := to_date(LV_FIN_PERIODO,'dd/mm/yyyy');
                  ELSE 
                     LD_DATE_LAST_BILL := NULL;
                     LD_DATE_NEXT_BILL := to_date(LV_FIN_PERIODO,'dd/mm/yyyy');
                  END IF;
           
           ELSE
                 --Caso que no sea nulo ( ya no es cuenta nueva pero se debe verificar si facturo o no)       
                  LN_DIAS_NO_FACTURADOS := to_number(to_date(LV_FIN_PERIODO,'dd/mm/yyyy')+1 - trunc(LD_DATE_LAST_BILL));   
                  IF LN_DIAS_NO_FACTURADOS>LN_DIAS_PERIODO THEN
                     LD_DATE_LAST_BILL := to_date(LV_INICIO_PERIODO,'dd/mm/yyyy');
                     LD_DATE_NEXT_BILL := to_date(LV_FIN_PERIODO,'dd/mm/yyyy');
                  ELSE
                     LD_DATE_LAST_BILL := trunc(LD_DATE_LAST_BILL);
                     LD_DATE_NEXT_BILL := to_date(LV_FIN_PERIODO,'dd/mm/yyyy');
                  END IF;
           END IF;        

           
           
           EXECUTE IMMEDIATE LV_UPDATE  USING LV_TMCODE, LV_TMCODE_DATE, LV_TMCODE_DATE_HIST, LV_DN_NUM, LV_PREV_BALANCE, LD_DATE_LAST_BILL,LD_DATE_NEXT_BILL, LN_CO_ID, LN_HILO;

             ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
             LN_CON_ROW:=LN_CON_ROW + 1 ;
             IF LN_CON_ROW >= LN_REG_COMMIT THEN

               COMMIT;
               LN_CON_ROW := 0 ;
               scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
             END IF;
       EXCEPTION
           WHEN OTHERS THEN
             PV_ERROR := SUBSTR(SQLERRM,1,200);
             INSERT INTO ivr_control_cli
             VALUES (pn_hilo,LN_CUSTOMER_ID,LN_CO_ID,SYSDATE,PV_ERROR);
             COMMIT;
             PV_ERROR := NULL;
       END;
       EXCEPTION
           WHEN OTHERS THEN
             PV_ERROR := SUBSTR(SQLERRM,1,200);
         -- SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:='Error en modificacion de datos en  IVR_TARIFARIOS_612 ('||pn_hilo||')';
          lv_mensaje_tec_scp:=PV_ERROR;
          lv_mensaje_acc_scp:='';
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          END;

   END LOOP ;
   COMMIT;

----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

EXCEPTION
WHEN OTHERS THEN
PV_ERROR := SUBSTR(SQLERRM,1,200);
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------

END IVP_ACTUALIZA_TMP_612;
----------------------------------
--VIDEOLLAMADA
-----------------------------------------------------------------------------
PROCEDURE IVP_GENERA_INFO_TAR_VL(PV_ERROR OUT VARCHAR2) IS
  -- Author  : CLS Homero Riera
  -- Created : 27/05/2008 12:31:44 
  -- Purpose : 
CURSOR C_CURSOR IS
  SELECT 
       CUS.CUSTOMER_ID,
       CON.CO_ID,
       DNU.DN_NUM,
       PRS.SNCODE,
       PRS.STATUS,
       PRS.VALID_FROM_DATE VALID_FROM_DATE
  FROM CUSTOMER_ALL_IVR CUS,
       CONTRACT_ALL_IVR CON,
       CONTR_SERVICES_CAP_IVR CSE,
       DIRECTORY_NUMBER DNU,
       CURR_CO_STATUS CST,
       SYSADM.FREE_UNITS FUT,
       RATEPLAN_HIST RPH1,
       CUSTOMER_ALL_IVR CUH,
       (SELECT CO_ID,
               SEQNO,
               TMCODE,
               TMCODE_DATE,
               USERLASTMOD,
               REC_VERSION,
               REQUEST_ID,
               TRANSACTIONNO
          FROM RATEPLAN_HIST
         WHERE TMCODE_DATE > SYSDATE - 45) RPH,
       PR_SERV_STATUS_HIST PRS
 WHERE CUS.CUSTOMER_ID = CON.CUSTOMER_ID
   AND CUS.CUSTOMER_ID_HIGH = CUH.CUSTOMER_ID(+)
   AND CON.CO_ID = CSE.CO_ID
   AND CON.CO_ID = PRS.CO_ID
   AND CON.CO_ID = CST.CO_ID
   AND CON.TMCODE = FUT.TMCODE
   AND CON.CO_ID = RPH.CO_ID(+)
   AND CON.CO_ID = RPH1.CO_ID
   AND CSE.DN_ID = DNU.DN_ID
   AND CSE.CS_ACTIV_DATE = (SELECT MAX(X.CS_ACTIV_DATE)
                              FROM CONTR_SERVICES_CAP_IVR X
                             WHERE X.CO_ID = CON.CO_ID)
   AND (CSE.CS_DEACTIV_DATE IS NULL OR CSE.CS_DEACTIV_DATE > SYSDATE - 45)
   AND PRS.SNCODE IN (SELECT SNCODE
                        FROM IVR612.IVR_PAQUETES_SERVICIOS@BSCS_TO_RTX_LINK
                       WHERE ESTADO = 'A')
   AND PRS.STATUS IN ('A', 'D', 'S')
   AND RPH1.TMCODE_DATE =
       (SELECT MAX(TMCODE_DATE) FROM RATEPLAN_HIST WHERE CO_ID = CON.CO_ID)
   AND CST.CH_STATUS IN ('a', 's');
/*   ORDER BY CUS.CUSTOMER_ID,
            CON.CO_ID,
            DNU.DN_NUM,
            TO_NUMBER((NVL(TO_CHAR(RPH.TMCODE_DATE, 'YYYYMMDDHH24MISS'),
                           '0'))) DESC;*/
CURSOR C_SERVICIOS IS
SELECT DN_NUM, SNCODE,FECHA_EVENTO
FROM SYSADM.IVR_FEATURES_PRO
ORDER BY FECHA_EVENTO ASC ;

/*CURSOR C_COD_AXIS(ID_SERVICIO VARCHAR2,FECHA_EVENTO DATE) IS
SELECT CL.ID_TIPO_DETALLE_SERV
  FROM PORTA.CL_DETALLES_SERVICIOS@AXIS CL, PORTA.VL_FEATURES_VIDEOLLAMADA@AXIS VL
 WHERE CL.ID_SERVICIO = OBTIENE_TELEFONO_DNC(ID_SERVICIO)
   AND FECHA_EVENTO BETWEEN ((CL.FECHA_DESDE)-(2/1440)) AND  ((CL.FECHA_DESDE)+(2/1440))  
   AND CL.ID_TIPO_DETALLE_SERV = VL.ID_TIPO_DETALLE_SERV;
*/

CURSOR C_COD_AXIS(PN_SNCODE NUMBER) IS
SELECT PS.COD_AXIS
  FROM IVR612.IVR_PAQUETES_SERVICIOS@BSCS_TO_RTX_LINK PS
 WHERE PS.SNCODE = PN_SNCODE;

LV_COD_AXIS VARCHAR2(20);                           
BEGIN
      DELETE IVR_FEATURES_PRO;
      COMMIT;
      FOR I IN C_CURSOR LOOP
         INSERT INTO SYSADM.IVR_FEATURES_PRO(CUSTOMER_ID,CO_ID,DN_NUM,SNCODE,ESTADO,FECHA_EVENTO)
         VALUES(I.CUSTOMER_ID,I.CO_ID,I.DN_NUM,I.SNCODE,I.STATUS,I.VALID_FROM_DATE);
      END LOOP;
     COMMIT;
     
     FOR I IN C_SERVICIOS LOOP
        OPEN C_COD_AXIS(I.SNCODE);
        FETCH C_COD_AXIS INTO LV_COD_AXIS;
        CLOSE C_COD_AXIS;
                
        UPDATE SYSADM.IVR_FEATURES_PRO SET COD_AXIS=LTRIM(RTRIM(LV_COD_AXIS))
        WHERE DN_NUM=I.DN_NUM 
        AND SNCODE = I.SNCODE
        AND FECHA_EVENTO=I.FECHA_EVENTO;
     END LOOP;
     COMMIT;
     
EXCEPTION
   WHEN OTHERS THEN
        PV_ERROR:='ERROR EN EL PROCESO DE EXTRACCION DE DATOS----'||SQLERRM;
        ROLLBACK;
END;

END IVK_GENERA_INFORMACION_612_SIR;
/
