CREATE OR REPLACE PACKAGE PCK_GSI_QC_REVISA_TARIFAS_BSCS IS

  -- Author  : CLS GBARRERA
  -- Created : 23/02/2016 14:47:12
  -- Lider SIS : SIS Christopher Crespo
  -- Lides PDS : CLS May Mite
  -- Purpose : Realiza validaciones y versiona las tarifas y almacena la ultima version
  
  --Variables globales
  gv_archivo1 varchar2(100):='';
  gv_archivo2 varchar2(100):='';
  gv_archivo3 varchar2(100):='';
  gv_archivo4 varchar2(100):='';
  gv_archivo5 varchar2(100):='';
  gv_archivo6 varchar2(100):='';
  gv_archivo7 varchar2(100):='';
  gv_archivo8 varchar2(100):='';
  
  PROCEDURE PR_PRINCIPAL(PV_ERROR OUT VARCHAR2);
                       
  PROCEDURE PRC_TN3_BSCS_GSI_OB_INF_AD (PV_TABLA_BSCS_NEW VARCHAR2,
                                        PV_TABLA_BSCS_OLD VARCHAR2,
                                        PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_PLANES_RTXPROD;
  
  PROCEDURE PR_AGREGA_CAMPOS(PV_TABLA_BSCS_NEW VARCHAR2,
                             PV_TABLA_BSCS_OLD VARCHAR2,
                             PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OB_PAREJAS_BULK(PV_TABLA_NEW VARCHAR2,
                               PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBT_TARIFAS_COM (PV_TABLA_NEW VARCHAR2, 
                                PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBT_TARIFAS_OTRAS_TAB (PV_TABLA_NEW VARCHAR2,
                                      PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBT_HOLDING(PV_TABLA_NEW VARCHAR2,
                           PV_ERROR     OUT VARCHAR2);
   
  PROCEDURE PR_OBT_HOLDING2(PV_TABLA_NEW VARCHAR2,
                            PV_ERROR     OUT VARCHAR2);
                          
  PROCEDURE PR_ACTUALIZA_COSTOS(PV_TABLA_NEW VARCHAR2,
                                PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBT_INF_ADICIONAL(PV_TABLA_NEW VARCHAR2,
                                 PV_ERROR     OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_CLIENTES_CTAS(PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_COSTOS_BGH(PV_TABLA_NEW VARCHAR2,
                                  PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_TARIFA_BASICA(PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_FEATURES_VPN (PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_PAQ_FIJOS(PV_TABLA_NEW VARCHAR2,
                                 PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_OBTIENE_FU_PACK_ID(PV_TABLA_NEW VARCHAR2,
                                  PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_REPORTE_FACT(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_REPORTE_LDI(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_REPORTE_LDI_BSCS_COLEC(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_REPORTE_PLAN_PAR_BSCS_COLEC(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_REPORTE_COM(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);

  PROCEDURE PR_REPORTE_AUT_PURO_BULK(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);
    
  PROCEDURE PR_REPORTE_AUT_PURO_VPN(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_REPORTE_BGH(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE PR_GENERA_REPORTES_XLS(PV_ERROR OUT VARCHAR2);
  
  Procedure PR_ENVIA_MAIL_CC_FILES_HTML(Pv_Ip_Servidor  In Varchar2,
                                      Pv_Sender       In Varchar2,
                                      Pv_Recipient    In Varchar2,
                                      Pv_Ccrecipient  In Varchar2,
                                      Pv_Subject      In Varchar2,
                                      Pv_Message      In clob,
                                      Pv_Max_Size     In Varchar2 Default 9999999999,
                                      Pv_Archivo1     In Varchar2 Default Null,
                                      Pv_Archivo2     In Varchar2 Default Null,
                                      Pv_Archivo3     In Varchar2 Default Null,
                                      Pv_Archivo4     In Varchar2 Default Null,
                                      Pv_Archivo5     In Varchar2 Default Null,
                                      Pv_Archivo6     In Varchar2 Default Null,
                                      Pv_Archivo7     In Varchar2 Default Null,
                                      Pv_Archivo8     In Varchar2 Default Null,
                                      Pb_Blnresultado In Out Boolean,
                                      Pv_Error        Out Varchar2);
  
  PROCEDURE PR_ENVIA_MAIL(PV_ERROR OUT VARCHAR2);
  PROCEDURE PR_INSERTA_BITACORA(PN_PROCESO   NUMBER,
                              PV_PROCESO   VARCHAR2,
                              PD_FECHA_INI DATE,
                              PD_FECHA_FIN DATE,
                              PV_OBSERVACION VARCHAR2 DEFAULT NULL);
  PROCEDURE PR_GENERA_HISTORICA(PV_TABLA_NEW VARCHAR2,
                              PV_TABLA_ANT VARCHAR2,
                              PV_ERROR OUT VARCHAR2);
END PCK_GSI_QC_REVISA_TARIFAS_BSCS;
/
CREATE OR REPLACE PACKAGE BODY PCK_GSI_QC_REVISA_TARIFAS_BSCS IS

  -- Author  : CLS GBARRERA
  -- Created : 23/02/2016 14:47:12
  -- Lider SIS : SIS Christopher Crespo
  -- Lides PDS : CLS May Mite
  -- Purpose : Realiza validaciones y versiona las tarifas y almacena la ultima version

PROCEDURE PR_PRINCIPAL(PV_ERROR OUT VARCHAR2) IS
  
  lv_error varchar2(1000);
  le_error exception;
  --v_tabla_bscs varchar2(60):='GSI_TARIFAS_BSCS_';
  --v_tabla_bscs_new varchar2(60):='24112015';
  --v_tabla_bscs_base varchar2(60):='24112015';
  --v_tabla_origen_bscs varchar2(60):=null;
  --v_tabla_salida_bscs varchar2(60):=null;
  v_tabla_tn3 varchar2(60):='GSI_TARIFAS_BSCS_';
  v_tabla_bscs_new varchar2(60):=to_char(trunc(sysdate),'yyyymmdd');--'20160217';--'15022016';---poner la nueva
  v_tabla_bscs_base varchar2(60):=to_char(trunc(sysdate),'ddmmyyyy');--'15022016';-- poner la anterior
  v_tabla_bscs_base2 varchar2(60):=to_char(trunc(sysdate-1),'yyyymmdd');--'20160216';-- poner la anterior
  --v_tabla_origen_tn3 varchar2(60):=null;
  --v_tabla_salida_tn3 varchar2(60):=null;
  v_tabla_origen_bscs varchar2(60):=null;
  v_tabla_origen_bscs2 varchar2(60):=null;
  v_tabla_salida_bscs varchar2(60):=null;
  Ln_Proceso number:=0;
  ld_feha_fin date;
  ld_feha_inicio date;
  --v_db_link_bscs varchar2(60):='@bscs.conecel.com';
  --v_db_link_colector varchar2(60):='@colector';
  
BEGIN

  v_tabla_salida_bscs:= v_tabla_tn3 || v_tabla_bscs_new;
  v_tabla_origen_bscs:= v_tabla_tn3 || v_tabla_bscs_base;
  v_tabla_origen_bscs2:= v_tabla_tn3 || v_tabla_bscs_base2;
  
  select nvl(max(ID_PROCESO),0)+ 1 into Ln_Proceso from GSI_BITACORA_TARIFAS;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_agrega_campos(pv_tabla_bscs_new => v_tabla_salida_bscs,
                                                 pv_tabla_bscs_old => v_tabla_origen_bscs,
                                                 pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_AGREGA_CAMPOS',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_agrega_campos : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.prc_tn3_bscs_gsi_ob_inf_ad(pv_tabla_bscs_new => v_tabla_salida_bscs,
                                                           pv_tabla_bscs_old => v_tabla_origen_bscs,
                                                           pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PRC_TN3_BSCS_GSI_OB_INF_AD',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.prc_tn3_bscs_gsi_ob_inf_ad : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_ob_parejas_bulk(pv_tabla_new => v_tabla_salida_bscs,
                                                   pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OB_PAREJAS_BULK',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.PCK_GSI_QC_REVISA_TARIFAS_BSCS : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_tarifas_com(pv_tabla_new => v_tabla_salida_bscs,
                                                   pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBT_TARIFAS_COM',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_tarifas_com : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_tarifas_otras_tab(pv_tabla_new => v_tabla_salida_bscs,
                                                         pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBT_TARIFAS_OTRAS_TAB',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_tarifas_otras_tab : '||lv_error;
    raise le_error;
  end if;

  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.PR_OBT_HOLDING(pv_tabla_new => v_tabla_salida_bscs,
                                                pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBT_HOLDING',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.PR_OBT_HOLDING : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.PR_OBT_HOLDING2(pv_tabla_new => v_tabla_salida_bscs,
                                               pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBT_HOLDING2',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.PR_OBT_HOLDING2 : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_actualiza_costos(pv_tabla_new =>  v_tabla_salida_bscs,
                                                     pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_ACTUALIZA_COSTOS',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_actualiza_costos : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_inf_adicional(pv_tabla_new => v_tabla_salida_bscs,
                                                     pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBT_INF_ADICIONAL',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_obt_inf_adicional : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_costos_bgh(pv_tabla_new => v_tabla_salida_bscs,
                                                       pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBTIENE_COSTOS_BGH',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_costos_bgh : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_fu_pack_id(pv_tabla_new => v_tabla_salida_bscs,
                                                       pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBTIENE_TARIFA_BASICA',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_fu_pack_id : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_tarifa_basica(pv_tabla_new => v_tabla_salida_bscs,
                                                          pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBTIENE_TARIFA_BASICA',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_tarifa_basica : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.PR_OBTIENE_PAQ_FIJOS(pv_tabla_new => v_tabla_salida_bscs,
                                                      pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_ACTUALIZA_FEATURES_FIJOS',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_actualiza_features_fijos : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_features_vpn(pv_tabla_new => v_tabla_salida_bscs,
                                                         pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBTIENE_FEATURES_VPN',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_features_vpn : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_clientes_ctas(pv_tabla_new => v_tabla_salida_bscs,
                                                          pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_OBTIENE_CLIENTES_CTAS',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_obtiene_clientes_ctas : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_genera_historica(pv_tabla_new => v_tabla_salida_bscs,
                                                     pv_tabla_ant => v_tabla_origen_bscs2,
                                                     pv_error     => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_GENERA_HISTORICA',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR PCK_GSI_QC_REVISA_TARIFAS_BSCS.pr_genera_historica : '||lv_error;
    raise le_error;
  end if;
  
  ld_feha_inicio:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_envia_mail(pv_error => lv_error);
  ld_feha_fin:=sysdate;
  pck_gsi_qc_revisa_tarifas_bscs.pr_inserta_bitacora(pn_proceso     => Ln_Proceso,
                                                     pv_proceso     => 'PR_ENVIA_MAL',
                                                     pd_fecha_ini   => ld_feha_inicio,
                                                     pd_fecha_fin   => ld_feha_fin,
                                                     pv_observacion => lv_error);
  if lv_error is not null then
    lv_error := 'ERROR pck_gsi_qc_revisa_tarifas_bscs.pr_envia_mail : '||lv_error;
    raise le_error;
  end if;
  
  commit;
  
EXCEPTION
  WHEN le_error then
    PV_ERROR := lv_error;
    rollback;
  WHEN OTHERS THEN
    PV_ERROR:=sqlerrm;
    rollback;
END PR_PRINCIPAL;  

PROCEDURE PRC_TN3_BSCS_GSI_OB_INF_AD (PV_TABLA_BSCS_NEW VARCHAR2,
                                      PV_TABLA_BSCS_OLD VARCHAR2,
                                      PV_ERROR OUT VARCHAR2) IS

  cursor c_parametros(PV_ID_PARAM VARCHAR2)IS
   select valor
     from GSI_QC_CONFIG_TARIFAS_BSCS a
    where a.id_parametro = PV_ID_PARAM;

  ll_valor long:=null;
  lv_error varchar2(1000);

  v_tabla_salida_tn3 VARCHAR2(100):= PV_TABLA_BSCS_NEW;
  v_tabla_origen_tn3 varchar2(100):= PV_TABLA_BSCS_OLD;
  le_error exception;
BEGIN
  
  --EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_BITACORA_TARIFAS';
  /*EXECUTE IMMEDIATE 'CREATE TABLE GSI_BITACORA_TARIFAS (
  ID_PROCESO number,
  PASO number,
  query varchar2(300),
  fecha_inicio date,
  fecha_fin date,
  REGISTROS_ACTUALIZADOS number)';*/
  
  ---PASO 1 TOLL
  open c_parametros('PASO_1_TOLL');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);

  Execute Immediate ll_valor;
  --QUEMADO PORQUE NO EXISTE UNA TABLA QUE CONTENGA LOS TOLLS
  Execute Immediate 'UPDATE '||v_tabla_salida_tn3 ||' a set toll = ( '||
                    ' select distinct b.toll from gsi_tarifas_bscs_final b where b.id_plan = a.id_plan ) ';
  commit;

  ---PASO 3 TOLL_COM
  ll_valor:=null;
  open c_parametros('PASO_3_TOLL_COM');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);

  Execute Immediate ll_valor;

  commit;

  ---PASO 5 TIPO
  ll_valor:=null;
  open c_parametros('PASO_5_TIPO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 6 Cant_cuentas
  ll_valor:=null;
  open c_parametros('PASO_6_CANT_CUENTAS');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 7 cant_Clientes
  ll_valor:=null;
  open c_parametros('PASO_7_CANT_CLIENTES');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 9 TABLA QUE TIENE LOS DESCUENTOS GSI_DESCUENTOS_HMO TABLA CON DESCUENTOS
/*  
  ll_valor:=null;
  open c_parametros('PASO_9_APLIC_DSCTO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;*/

  ---PASO 10 TABLA ACTUALIZO EL TOLL DE LOS DE DESCUENTO PARA QUE NO COMPARE
  
  ll_valor:=null;
  open c_parametros('PASO_10_TOLL_DSCTO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;


  ---PASO 11 MARCAR COMO BULK LOS PLANES BULK
  
  ll_valor:=null;
  open c_parametros('PASO_11_PLANES_BULK');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
 
  commit;

  ---PASO 12 MARCAR ACTUALIZAR VPN
  
  ll_valor:=null;
  open c_parametros('PASO_12_ACT_VPN');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 13 MARCAR PLANES PERSONALES VPN
  
  ll_valor:=null;
  open c_parametros('PASO_13_AUT_PURO_VPN');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 14 SCRIPT PARA MARCAR LOS CONTROLADOS PUROS
  
  ll_valor:=null;
  open c_parametros('PASO_14_AUT_PURO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
 
  commit;

  ---PASO 15 SCRIPT PARA actualizar clientes
  
  ll_valor:=null;
  open c_parametros('PASO_15_ACT_CLIENTES');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;


  ---PASO 16 SCRIPT PARA actualizar cuentas
  
  ll_valor:=null;
  open c_parametros('PASO_16_ACT_CUENTAS');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 17 ACTUALIZAR TOLL DE TABLA DE BITACORA COMERCIAL (ESTE PASO DURA 7 MINUTOS SE DESCOMENTARA SOLO LA PRIMERA VEZ)-----

  
  ll_valor:=null;
  open c_parametros('PASO_17_ACT_TOLL_COM');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 18 RECURSO-----
  
  ll_valor:=null;
  open c_parametros('PASO_18_RECURSO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 19 FECHA_CREACION-----
  
  ll_valor:=null;
  open c_parametros('PASO_19_FECHA_CREACION');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
  
  commit;

  ---PASO 20 ESTADO-----
  
  ll_valor:=null;
  open c_parametros('PASO_20_ESTADO');
   fetch c_parametros into ll_valor;
  close c_parametros;
  ll_valor := replace(ll_valor,'<tabla_salida>',v_tabla_salida_tn3);
  ll_valor := replace(ll_valor,'<tabla_origen>',v_tabla_origen_tn3);
  Execute Immediate ll_valor;
 
  commit;

EXCEPTION
  WHEN le_error THEN
    PV_ERROR := lv_error;
  WHEN OTHERS THEN
    PV_ERROR := SQLERRM;
END PRC_TN3_BSCS_GSI_OB_INF_AD;
--ACTUALIZA LA TABLA TEMPORAL EN COLECTOR POR FALTA DE DBLINK A RTXPROD
PROCEDURE PR_PLANES_RTXPROD IS
  
begin
delete /*+APPEND */ FROM bl_planes_vpn_copia;

insert into bl_planes_vpn_copia (
        id_plan, 
        id_detalle_plan, 
        id_subproducto, 
        observacion, 
        estado)
select  id_plan, 
        id_detalle_plan, 
        id_subproducto, 
        observacion, 
        estado
 from bulk.bl_planes_vpn@bscs_to_rtx_link;
commit;
END;

PROCEDURE PR_AGREGA_CAMPOS(PV_TABLA_BSCS_NEW VARCHAR2,
                           PV_TABLA_BSCS_OLD VARCHAR2,
                           PV_ERROR OUT VARCHAR2) IS
  
cursor c_parametros(PV_ID_PARAM VARCHAR2)IS
select valor from GSI_QC_CONFIG_TARIFAS_BSCS a
where a.id_parametro = PV_ID_PARAM;

ll_valor long:=null;
ln_tablaExiste number:=0;
le_error exception;

BEGIN
  
  open c_parametros('COLUM_TARIFAS');
   fetch c_parametros into ll_valor;
  close c_parametros;
  
  --VERIFICO SI LA TABLA EXISTE
  execute immediate 'SELECT COUNT(*) FROM all_tables WHERE table_name= :tabla' into ln_tablaExiste using PV_TABLA_BSCS_NEW;
  
  if ln_tablaExiste = 0 then--Sino existe la tabla la creo
     execute immediate 'CREATE TABLE '||PV_TABLA_BSCS_NEW||' TABLESPACE TBS_TARIFAS_DAT AS '||'SELECT * FROM '||PV_TABLA_BSCS_OLD;
     ll_valor := replace(ll_valor,'<tabla_tarifas>',PV_TABLA_BSCS_NEW);
     execute immediate ll_valor;--Agrego las nuevas columnas
     --Agrego los comentarios de las columnas
     ll_valor:=null;
     open c_parametros('COMEN_TARIFAS');
       fetch c_parametros into ll_valor;
     close c_parametros;
     ll_valor := replace(ll_valor,'<tabla_tarifas>',PV_TABLA_BSCS_NEW);
     execute immediate ll_valor;
  else
     raise le_error;
  end if;
  
EXCEPTION
  WHEN le_error THEN
    PV_ERROR := 'La tabla '||PV_TABLA_BSCS_NEW||' ya existe';
  WHEN OTHERS THEN
    PV_ERROR := SQLERRM;
END PR_AGREGA_CAMPOS;     

PROCEDURE PR_OB_PAREJAS_BULK(PV_TABLA_NEW VARCHAR2,
                              PV_ERROR OUT VARCHAR2) IS
  PV_PLAN varchar2(10):=null;
  PV_CONTEO number:=0;
  lc_principal sys_refcursor;
  
  LV_ID_PLAN varchar2(15);
  LV_TIPO varchar2(15);
  LN_ID_TIPO number;
  lv_query long;
begin
  lv_query :='select a.id_plan, a.tipo, a.id_tipo 
  from ge_planes_bulk@axis a, '||PV_TABLA_NEW||' b
  where a.id_plan = b.id_plan
  and b.id_plan_pareja is null';
  
  open lc_principal for lv_query;
  loop
    LV_ID_PLAN := null;
    LV_TIPO := null;
    LN_ID_TIPO := null;
    PV_PLAN :=null;
    PV_CONTEO :=0;  

  FETCH lc_principal into LV_ID_PLAN, LV_TIPO, LN_ID_TIPO;
  exit when lc_principal%notfound or LV_ID_PLAN is null ;
  
  select COUNT(*) into pv_conteo
  from  ge_planes_bulk@axis b ,ge_detalles_Planes@axis c
  where c.id_plan=b.id_plan 
    and b.TIPO = LV_TIPO 
    and ID_TIPO= LN_ID_TIPO 
    and c.id_CLASE='GSM' 
    and b.id_plan != LV_ID_PLAN;

  if pv_conteo > 0 then
  select b.id_plan into pv_plan
  from  ge_planes_bulk@axis b ,ge_detalles_Planes@axis c
  where c.id_plan=b.id_plan and b.TIPO = LV_TIPO and ID_TIPO= LN_ID_TIPO and c.id_CLASE='GSM' and b.id_plan !=LV_ID_PLAN and rownum=1;
  
  execute immediate
  'update '||PV_TABLA_NEW||'
  set id_plan_pareja = :pv_plan
  where id_plan = :LV_ID_PLAN' using pv_plan, LV_ID_PLAN;
  
  commit;
  end if;
  
  end loop;
  close lc_principal;
EXCEPTION
  when others then
    pv_error := sqlerrm;
END PR_OB_PAREJAS_BULK;

--Obtiene las tarifas comerciales
PROCEDURE PR_OBT_TARIFAS_COM (PV_TABLA_NEW VARCHAR2, 
                              PV_ERROR OUT VARCHAR2) is
  
CURSOR CUR_OBTIENE_TARIFAS_COM (lv_plan varchar2) is
 select a.codigo,nvl(decode(replace(a.costo_min_inpoll_inc,'/',''),'NA','-1',a.costo_min_inpoll_inc),'-2') INPOOL_COM,
    nvl(decode(replace(a.costo_min_claroaclaro,'/',''),'NA','-1',a.costo_min_claroaclaro),'-2') CLARO_COM,
    nvl(decode(replace(a.costo_min_claroamovi,'/',''),'NA','-1',a.costo_min_claroamovi),'-2') MOVISTAR_COM,
    nvl(decode(replace(a.costo_min_claroaalegro,'/',''),'NA','-1',a.costo_min_claroaalegro),'-2') ALEGRO_COM,
    nvl(decode(replace(a.costo_min_claroafijo,'/',''),'NA','-1',a.costo_min_claroafijo),'-2') FIJAS_COM
    from axisfac.datos_planes@axis a where a.codigo in (lv_plan);

ln_limite number:=5;
ln_actualiza number:=0;
ln_rowid rowid;
---lv_sentencia_1 varchar2(200):='alter session set NLS_NUMERIC_CHARACTERS=''.,''';
lv_error Varchar2(150);
ln_INPOOL_COM number:=0;
ln_CLARO_COM number:=0;
ln_MOVISTAR_COM number:=0;
ln_ALEGRO_COM number:=0;
ln_FIJAS_COM number:=0;
ln_PRUEBA number:=0;
CUR_PLANES_SIS sys_refcursor;
ll_query long;
lv_rowid_plan_sis varchar2(1000);
lv_id_plan_sis varchar2(20);

BEGIN 
  ll_query := 'SELECT a.rowid,a.id_plan FROM '||PV_TABLA_NEW||' a order by 2';
  
open CUR_PLANES_SIS for ll_query;
loop
  lv_rowid_plan_sis:= NULL;
  lv_id_plan_sis :=null;
  
  FETCH CUR_PLANES_SIS INTO lv_rowid_plan_sis,lv_id_plan_sis;
  EXIT WHEN CUR_PLANES_SIS%notfound or lv_rowid_plan_sis is null;
   BEGIN 
         ln_actualiza:=0;
         ln_rowid:=lv_rowid_plan_sis;
        
        -- Execute Immediate lv_sentencia_1;         
         FOR j in CUR_OBTIENE_TARIFAS_COM (lv_id_plan_sis) LOOP
         BEGIN  
                  ------ACTUALIZO LA TABlA CON LA CANTIDAD DE CASOS----------
                   BEGIN
                   ln_INPOOL_COM:=to_number(replace(j.INPOOL_COM,'.',','));
                   EXCEPTION
                     WHEN OTHERS THEN
                       ln_INPOOL_COM:=to_number(replace(j.INPOOL_COM,',','.'));
                   END;
                   BEGIN
                   ln_CLARO_COM:=to_number(replace(j.CLARO_COM,'.',','));
                   EXCEPTION
                     WHEN OTHERS THEN
                       ln_CLARO_COM:=to_number(replace(j.CLARO_COM,',','.'));
                   END;
                   BEGIN
                   ln_MOVISTAR_COM:=to_number(replace(j.MOVISTAR_COM,'.',','));
                   EXCEPTION
                     WHEN OTHERS THEN
                       ln_MOVISTAR_COM:=to_number(replace(j.MOVISTAR_COM,',','.'));
                   END;
                   BEGIN
                   ln_ALEGRO_COM:=to_number(replace(j.ALEGRO_COM,'.',','));
                   EXCEPTION
                     WHEN OTHERS THEN
                       ln_ALEGRO_COM:=to_number(replace(j.ALEGRO_COM,',','.'));
                   END;
                   BEGIN
                   ln_FIJAS_COM:=to_number(replace(j.FIJAS_COM,'.',','));
                   EXCEPTION
                     WHEN OTHERS THEN
                       ln_FIJAS_COM:=to_number(replace(j.FIJAS_COM,',','.'));
                   END;
                   ln_PRUEBA:=0;                   
                   --dbms_output.put_line('TARIFA CLARO : ' || ln_CLARO_COM);
                   execute immediate
                   'update '||PV_TABLA_NEW||' a '||
                   'set a.COM_INPOOL=:ln_INPOOL_COM,a.COM_CLARO=:ln_CLARO_COM, '||
                   'COM_MOVISTAR=:ln_MOVISTAR_COM,COM_ALEGRO=:ln_ALEGRO_COM,COM_FIJAS=:ln_FIJAS_COM '||
                   'where a.rowid=:ln_rowid' using ln_INPOOL_COM,ln_CLARO_COM,ln_MOVISTAR_COM,ln_ALEGRO_COM,ln_FIJAS_COM,ln_rowid;
                   
                   exception
                   when others then
                          lv_error:=SQLERRM;  
                          pv_error:='ERROR 1 :: '||lv_error;
                          --dbms_output.put_line('ERROR 1 : ' || lv_error || lv_id_plan_sis);
                          execute immediate
                          'update '||PV_TABLA_NEW||' a 
                          set a.COM_INPOOL=-3,a.COM_CLARO=-3,
                          COM_MOVISTAR=-3,COM_ALEGRO=-3,COM_FIJAS=-3
                          where a.rowid=:ln_rowid' using ln_rowid;
                          commit;
          END;          
         END LOOP;
        if (ln_actualiza=ln_limite) then 
              commit;
             ln_actualiza:=0;        
        end if;
        ln_actualiza:=ln_actualiza+1;  
        exception
        when others then
        lv_error:='ERROR 2 :: '||SQLERRM;
        --pv_error:=lv_error;  
        --dbms_output.put_line('ERROR 2 : ' || lv_error || i.id_plan );  
        execute immediate
        'update '||PV_TABLA_NEW||' a
        set a.COM_INPOOL=-4,a.COM_CLARO=-4,
        COM_MOVISTAR=-4,COM_ALEGRO=-4,COM_FIJAS=-4
        where a.rowid=:ln_rowid' using ln_rowid;
        commit;
 END; 
end loop;
close CUR_PLANES_SIS;
commit;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR :='ERROR 1 :: '||lv_id_plan_sis||' - '||sqlerrm;
END PR_OBT_TARIFAS_COM;

--Obtengo otras tarifas
PROCEDURE PR_OBT_TARIFAS_OTRAS_TAB (PV_TABLA_NEW VARCHAR2,
                                    PV_ERROR OUT VARCHAR2)IS

ln_limite        number:=5;
ln_actualiza     number:=0;
ln_rowid         rowid;
---lv_sentencia_1 varchar2(200):='alter session set NLS_NUMERIC_CHARACTERS=''.,''';
lv_error         Varchar2(150);
ln_INPOOL_COM    number:=0;
ln_CLARO_COM     number:=0;
ln_MOVISTAR_COM  number:=0;
ln_ALEGRO_COM    number:=0;
ln_FIJAS_COM     number:=0;
--
lv_rowid_tar varchar2(1000);
lv_id_plan_tar varchar2(15);
ln_com_inpool_tar number;
ln_com_claro_tar number;
ln_com_movistar_tar number;
ln_com_alegro_tar number;
ln_com_fijas_tar number;
--
ln_INPOOL_COM_1   number:=0;
ln_CLARO_COM_1    number:=0;
ln_MOVISTAR_COM_1 number:=0;
ln_ALEGRO_COM_1   number:=0;
ln_FIJAS_COM_1    number:=0;
LN_EXISTE_PLAN    INTEGER:=0;
--
CUR_PLANES_SIS    sys_refcursor;
CUR_TARIFAS_COM_1 sys_refcursor;
ll_query          long;
ll_query2         long;
lv_rowid_plan_sis varchar2(1000);
lv_id_plan_sis    varchar2(20);

BEGIN 
  ll_query := 'SELECT a.rowid,a.id_plan FROM '||PV_TABLA_NEW||' a ';
  

open CUR_PLANES_SIS for ll_query;
loop
  lv_rowid_plan_sis:= NULL;
  lv_id_plan_sis :=null;
  
  FETCH CUR_PLANES_SIS INTO lv_rowid_plan_sis,lv_id_plan_sis;
  EXIT WHEN CUR_PLANES_SIS%notfound or lv_rowid_plan_sis is null;
  
   BEGIN 
         --ln_actualiza:=0;
         ln_rowid:=lv_rowid_plan_sis; 

         BEGIN 
                  -----ENCERO LAS VARIABLES------
                  ln_INPOOL_COM :=0;
                  ln_CLARO_COM :=0;
                  ln_MOVISTAR_COM :=0;
                  ln_ALEGRO_COM :=0;
                  ln_FIJAS_COM :=0;
                  -----ENCERO LAS VARIABLES------
                  ln_INPOOL_COM_1:=0;
                  ln_CLARO_COM_1 :=0;
                  ln_MOVISTAR_COM_1 :=0;
                  ln_ALEGRO_COM_1 :=0;
                  ln_FIJAS_COM_1 :=0;
          
                  -------OBTENGO LAS TARIFAS DE LOS PLANES-----------
                  LN_EXISTE_PLAN:=0;
                  SELECT COUNT(*) INTO  LN_EXISTE_PLAN FROM axisfac.tarifas_planes@axis x where
                  x.id_plan in (lv_id_plan_sis);
                   
                  IF (LN_EXISTE_PLAN > 0 ) THEN
                        ---INPOOL----
                        BEGIN
                          select nvl(decode(replace(x.tiempo_aire,'/',''),'NA',-1,to_number(trim(replace(x.tiempo_aire,'.',',')))),-2) into ln_INPOOL_COM
                          from axisfac.tarifas_planes@axis x where 
                          x.id_plan in (lv_id_plan_sis) and x.prefijo='773' and rownum=1;
                        EXCEPTION
                          WHEN NO_DATA_FOUND THEN
                            ln_INPOOL_COM:=0;
                          WHEN OTHERS THEN--Si la configuracion de punto y coma falla
                            select nvl(decode(replace(x.tiempo_aire,'/',''),'NA',-1,to_number(trim(replace(x.tiempo_aire,',','.')))),-2) into ln_INPOOL_COM
                            from axisfac.tarifas_planes@axis x where 
                            x.id_plan in (lv_id_plan_sis) and x.prefijo='773' and rownum=1;
                        END;
                        BEGIN
                          ---CLARO----
                          select nvl(decode(replace(x.tiempo_aire,'/',''),'NA',-1,to_number(trim(replace(x.tiempo_aire,'.',',')))),-2) into ln_CLARO_COM 
                          from axisfac.tarifas_planes@axis x where 
                          x.id_plan in (lv_id_plan_sis) and x.prefijo='774' and rownum=1;
                        EXCEPTION
                          WHEN NO_DATA_FOUND THEN
                            ln_CLARO_COM:=0;
                          WHEN OTHERS THEN--Si la configuracion de punto y coma falla
                            ---CLARO----
                            select nvl(decode(replace(x.tiempo_aire,'/',''),'NA',-1,to_number(trim(replace(x.tiempo_aire,',','.')))),-2) into ln_CLARO_COM 
                            from axisfac.tarifas_planes@axis x where 
                            x.id_plan in (lv_id_plan_sis) and x.prefijo='774' and rownum=1;
                        END;
                   END IF;
                  LN_EXISTE_PLAN:=0;
                  SELECT COUNT(*) INTO LN_EXISTE_PLAN FROM axisfac.tarifas_planes@axis x where x.id_plan in (lv_id_plan_sis);
                  IF (LN_EXISTE_PLAN > 0 ) THEN
                          BEGIN
                            ---MOVISTAR---- 
                            select to_number(trim(replace(z.precio_final,'.',','))) into ln_MOVISTAR_COM 
                            from axisfac.tarifas_operadoras@axis z where z.id_plan in (lv_id_plan_sis)
                            and z.operadora_destino='MOVISTAR' and rownum=1;
                          EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                              ln_MOVISTAR_COM:=0;
                            WHEN OTHERS THEN
                              ---MOVISTAR----
                              select to_number(trim(replace(z.precio_final,',','.'))) into ln_MOVISTAR_COM
                              from axisfac.tarifas_operadoras@axis z where z.id_plan in (lv_id_plan_sis)
                              and z.operadora_destino='MOVISTAR' and rownum=1;
                          END;
                          BEGIN
                            ---ALEGRO----
                            select to_number(trim(replace(z.precio_final,'.',','))) into ln_ALEGRO_COM 
                            from axisfac.tarifas_operadoras@axis z where id_plan in (lv_id_plan_sis)
                            and z.operadora_destino='ALEGRO' and rownum=1;
                          EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                              ln_ALEGRO_COM:=0;
                            WHEN OTHERS THEN
                              ---ALEGRO----
                              select to_number(trim(replace(z.precio_final,',','.'))) into ln_ALEGRO_COM 
                              from axisfac.tarifas_operadoras@axis z where id_plan in (lv_id_plan_sis)
                              and z.operadora_destino='ALEGRO' and rownum=1;
                          END;
                          BEGIN
                            ---FIJAS----
                            select to_number(trim(replace(z.precio_final,'.',','))) into ln_FIJAS_COM from axisfac.tarifas_operadoras@axis z
                            where id_plan in (lv_id_plan_sis)
                            and z.operadora_destino='FIJO' and rownum=1;
                          EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                              ln_FIJAS_COM:=0;
                            WHEN OTHERS THEN
                              ---FIJAS----
                              select to_number(trim(replace(z.precio_final,',','.'))) into ln_FIJAS_COM from axisfac.tarifas_operadoras@axis z
                              where id_plan in (lv_id_plan_sis)
                              and z.operadora_destino='FIJO' and rownum=1;
                          END;
                   END IF;
                  
                  ------OBTIENE LAS TARIFAS DE COM ---------
                  ll_query2 := 'SELECT  a.rowid,
                                  a.id_plan, '||
                                 'a.com_inpool, '||
                                 'a.com_claro, '||
                                 'a.com_movistar, '||
                                 'a.com_alegro, '||
                                 'a.com_fijas '||
                            'FROM '||PV_TABLA_NEW||' a where a.id_plan ='''||lv_id_plan_sis||'''';

                  OPEN CUR_TARIFAS_COM_1 FOR ll_query2;
                    LOOP
                       --
                       lv_rowid_tar:=null;
                       lv_id_plan_tar:=null;
                       ln_com_inpool_tar:=0;
                       ln_com_claro_tar:=0;
                       ln_com_movistar_tar:=0;
                       ln_com_alegro_tar:=0;
                       ln_com_fijas_tar:=0;
                       
                       fetch CUR_TARIFAS_COM_1
                       into lv_rowid_tar,
                            lv_id_plan_tar,
                            ln_com_inpool_tar,
                            ln_com_claro_tar,
                            ln_com_movistar_tar,
                            ln_com_alegro_tar,
                            ln_com_fijas_tar;
                           
                        exit when CUR_TARIFAS_COM_1%notfound or lv_rowid_tar is null;
                        
                           ln_INPOOL_COM_1:=ln_com_inpool_tar;
                           ln_CLARO_COM_1:=ln_com_claro_tar;
                           ln_MOVISTAR_COM_1:=ln_com_movistar_tar;
                           ln_ALEGRO_COM_1:=ln_com_alegro_tar;
                           ln_FIJAS_COM_1:=ln_com_fijas_tar;
                           
                           IF  (ln_INPOOL_COM_1 < 0)  THEN  
                            -- 
                            execute immediate ' update ' ||PV_TABLA_NEW
                               ||' a set a.COM_INPOOL=:ln_INPOOL_COM
                               where a.rowid=:ln_rowid' using ln_INPOOL_COM,ln_rowid;
                            --
                            END IF;
                            IF (ln_claro_com_1< 0) THEN
                             --
                             execute immediate ' update '||PV_TABLA_NEW 
                               ||' a set a.COM_CLARO=:ln_CLARO_COM
                                   where a.rowid=:ln_rowid' using ln_CLARO_COM,ln_rowid;
                             --
                            END IF;                    
                            IF (ln_movistar_com_1< 0) THEN
                             --
                               execute immediate ' update '||PV_TABLA_NEW
                                ||' a  set COM_MOVISTAR=:ln_MOVISTAR_COM
                                 where a.rowid=:ln_rowid' using ln_MOVISTAR_COM,ln_rowid;     
                             --
                            END IF;
                             IF (ln_alegro_com_1< 0) THEN
                              --
                              execute immediate ' update '||PV_TABLA_NEW
                               ||' a  set COM_ALEGRO=:ln_ALEGRO_COM
                                 where a.rowid=:ln_rowid' using ln_ALEGRO_COM,ln_rowid;     
                              --
                             END IF;
                             IF (ln_FIJAS_COM_1< 0) THEN
                              --
                              execute immediate ' update '||PV_TABLA_NEW 
                                ||' a set COM_FIJAS=:ln_FIJAS_COM
                                 where a.rowid=:ln_rowid' using ln_FIJAS_COM,ln_rowid;                            
                             END IF; 

                   end loop;
                   close CUR_TARIFAS_COM_1;
                   exception
                   when others then
                          lv_error:=':: ERROR 3 :: '||lv_id_plan_sis||' '||SQLERRM; 
                          pv_error:= lv_error;
                          --dbms_output.put_line('ERROR 1 : ' || lv_error);
                          execute immediate 'update' || PV_TABLA_NEW 
                          ||' a set a.ERROR= -6 where a.rowid=:ln_rowid' using ln_rowid;
                          --
                          commit;
                          --
          END;          

        if (ln_actualiza=ln_limite) then 
              commit;
             ln_actualiza:=0;        
        end if;
        ln_actualiza:=ln_actualiza+1;  
        exception
        when others then
        lv_error:=':: ERROR 2 :: '||lv_id_plan_sis||' '||SQLERRM;  
        pv_error:=lv_error;
        --dbms_output.put_line('ERROR 2 : ' || lv_error);  
        execute immediate 'update' || PV_TABLA_NEW
         ||' a set a.ERROR= -7
        where a.rowid=:ln_rowid' using ln_rowid;
        commit;
 END; 
end loop;
close CUR_PLANES_SIS;
commit;
EXCEPTION
  when others then
    pv_error := ':: ERROR 1 :: '||lv_id_plan_sis||' '||sqlerrm;
END; 
PROCEDURE PR_OBT_HOLDING(PV_TABLA_NEW VARCHAR2,
                         PV_ERROR     OUT VARCHAR2) IS

  CURSOR CUR_PLANES_SIS(lv_plan varchar2) is 
   SELECT W.OPERADORA_DESTINO,W.PRECIO_FINAL
      FROM PORTA.GE_TARIFAS_OPERADORAS@axis W
     WHERE W.ID_REQUERIMIENTO =
             (SELECT A.ID_REQUERIMIENTO
      FROM PORTA.GE_INV_PLANES_FEAT@axis A
      WHERE A.CODIGO = lv_plan);
  
  CURSOR C_OBTIENE_DETALLE_PLAN(lv_plan varchar2) is
    select b.id_detalle_plan 
      from porta.ge_planes@axis a, 
           porta.ge_detalles_planes@axis b 
     where a.id_plan = b.id_plan 
       and a.id_plan=lv_plan;
  
  --- Para saber si 1 BP aplica a promoci�n de Llamadas Ilimilitadas:
  CURSOR C_LLAM_ILIMITADAS(lv_detallePlan varchar2) is
    select valor id_promocion, descripcion descripcion_promocion
       from scp_dat.scp_parametros_procesos@axis p
      where p.id_proceso = 'SOA_GENERAL'
        and p.id_parametro = 'SOA_DETALLE_' || lv_detallePlan;
  
  CURSOR C_DOBL_MINUTOS(lv_id_plan varchar2) is
  ---Para saber si 1 BP aplica a promoci�n de Voz: Doble de Minutos
    select a.id_promocion, b.descripcion descripcion_promocion
      from porta.mk_param_promo_aut@axis a, porta.Mk_Promociones_Maestra_Aut@axis b, porta.ge_detalles_planes@axis c
     where a.id_promocion=b.id_promocion and c.id_detalle_plan=a.valor 
       and c.id_plan = lv_id_plan
       and a.id_parametro='PLANES_VALIDOS' 
       and tipo_promo = 'PROMO_VOZ'
     order by a.id_promocion, a.valor;

  lC_LLAM_ILIMITADAS C_LLAM_ILIMITADAS%rowtype;
  lC_DOBL_MINUTOS C_DOBL_MINUTOS%rowtype;
  lC_OBTIENE_DETALLE_PLAN C_OBTIENE_DETALLE_PLAN%rowtype;
  CUR_OBT_TARIFAS sys_refcursor;
  lb_found boolean;
  lv_rowid varchar2(1000);
  lv_id_plan varchar2(15);
  ll_query long;
  ln_cont number:=0;
  
BEGIN 
  

    ll_query :='SELECT a.rowid,a.id_plan FROM '||PV_TABLA_NEW||' a ';--obtener tarifas
    
    OPEN CUR_OBT_TARIFAS FOR ll_query;
    loop
      lv_id_plan:=null;
      lv_rowid:=null;
      
      fetch CUR_OBT_TARIFAS into lv_rowid,lv_id_plan;
      exit when CUR_OBT_TARIFAS%notfound or lv_rowid is null;
      ln_cont := ln_cont + 1;
      
      for j in CUR_PLANES_SIS(lv_id_plan)  loop
        
        BEGIN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_MOVISTAR = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),''.'','','')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''MOVISTAR''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        EXCEPTION
          WHEN OTHERS THEN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_MOVISTAR = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),'','',''.'')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''MOVISTAR''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        END;
        
        BEGIN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_ALEGRO = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),''.'','','')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''CNT MOVIL''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        EXCEPTION
          WHEN OTHERS THEN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_ALEGRO = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),'','',''.'')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''CNT MOVIL''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        END;
        
        BEGIN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_FIJAS = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),''.'','','')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''FIJOS''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        EXCEPTION
          WHEN OTHERS THEN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_FIJAS = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),'','',''.'')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''FIJOS''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        END;
        
        BEGIN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_INPOOL = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),''.'','','')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''INPOOL''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        EXCEPTION
          WHEN OTHERS THEN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_INPOOL = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),'','',''.'')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''INPOOL''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        END;
        
        BEGIN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_CLARO = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),''.'','','')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''CLARO''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        EXCEPTION
          WHEN OTHERS THEN
            execute immediate
              'update '||PV_TABLA_NEW||' z set '||
                    ' COM_CLARO = to_number(replace(trim(nvl(:PRECIO_FINAL,-1)),'','',''.'')) '||
              'WHERE z.rowid=:lv_rowid '||
               ' AND :OPERADORA_DESTINO=''CLARO''' using j.PRECIO_FINAL,lv_rowid,j.OPERADORA_DESTINO;
        END;
        
      end loop;
      
      --OBTENGO LAS PROMOCIONES
      open C_OBTIENE_DETALLE_PLAN(lv_id_plan);
       fetch C_OBTIENE_DETALLE_PLAN into lC_OBTIENE_DETALLE_PLAN;
      close C_OBTIENE_DETALLE_PLAN;
      
      OPEN C_LLAM_ILIMITADAS(lC_OBTIENE_DETALLE_PLAN.Id_Detalle_Plan);
       FETCH C_LLAM_ILIMITADAS INTO LC_LLAM_ILIMITADAS;
       lb_found := C_LLAM_ILIMITADAS%FOUND;
      CLOSE C_LLAM_ILIMITADAS;
      
      IF lb_found THEN
        execute immediate
        'update '||PV_TABLA_NEW||' z set '||
                    ' id_promocion = :1 ,'||
                    ' desc_promocion = :2 '||
              'WHERE z.rowid=:lv_rowid '
         using LC_LLAM_ILIMITADAS.ID_PROMOCION,LC_LLAM_ILIMITADAS.DESCRIPCION_PROMOCION,lv_rowid;
      END IF;
      
      OPEN C_DOBL_MINUTOS(lv_id_plan);
       FETCH C_DOBL_MINUTOS INTO LC_DOBL_MINUTOS;
       lb_found := C_DOBL_MINUTOS%FOUND;
      CLOSE C_DOBL_MINUTOS;
      
      IF lb_found THEN
        execute immediate
        'update '||PV_TABLA_NEW||' z set '||
                    ' id_promocion = :1 ,'||
                    ' desc_promocion = :2 '||
              'WHERE z.rowid=:lv_rowid '
         using LC_DOBL_MINUTOS.ID_PROMOCION,LC_DOBL_MINUTOS.DESCRIPCION_PROMOCION,lv_rowid;
      END IF;
      
      IF ln_cont > 10 THEN
        ln_cont:=0;
        COMMIT;
      END IF;
      
     end loop;
     close CUR_OBT_TARIFAS;

commit;

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:=':: ERROR :: '||SQLERRM;
END PR_OBT_HOLDING;
--Obtengo la tarifa holding
PROCEDURE PR_OBT_HOLDING2(PV_TABLA_NEW VARCHAR2,
                          PV_ERROR     OUT VARCHAR2) IS

CURSOR CUR_OBT_HOLDING is 
 SELECT *
 FROM (SELECT CODIGO,ID_REQUERIMIENTO,
 max(decode(prefijo,771,tiempo_aire)) TARIFAS_INPOOL_771,
 max(decode(prefijo,772,tiempo_aire)) TARIFAS_VPN_772,
 max(decode(prefijo,773,tiempo_aire)) TARIFAS_INPOOL_773,
 max(decode(prefijo,774,tiempo_aire)) TARIFAS_ONNET_774,
 max(decode(prefijo,776,tiempo_aire)) HOLDING_776,
 max(decode(prefijo,777,tiempo_aire)) HOLDING_777,
 max(decode(prefijo,778,tiempo_aire)) HOLDING_778,
 max(decode(prefijo,779,tiempo_aire)) HOLDING_779,
 max(decode(prefijo,780,tiempo_aire)) HOLDING_780,
 max(decode(prefijo,781,tiempo_aire)) HOLDING_781
 from( 
   SELECT A.CODIGO,A.ID_REQUERIMIENTO,B.PREFIJO,B.TIPO_MKT,B.TIEMPO_AIRE
     FROM porta.GE_INV_PLANES_FEAT@axis A,
          porta.GE_TARIFAS_PLANES@axis B
    WHERE A.ID_REQUERIMIENTO=B.ID_REQUERIMIENTO
      --AND A.CODIGO=lv_plan
      AND B.ESTADO='A')
    GROUP BY CODIGO,ID_REQUERIMIENTO);
  
  CUR_PLANES_SIS sys_refcursor;
  lv_rowid varchar2(1000);
  lv_id_plan varchar2(15);
  ll_query long;
BEGIN 
  
for i in CUR_OBT_HOLDING loop
    ll_query :='SELECT a.rowid FROM '||PV_TABLA_NEW||' a where a.id_plan=:lv_plan';--obtener tarifas
    
    OPEN CUR_PLANES_SIS FOR ll_query using i.CODIGO;
    loop
      lv_id_plan:=null;
      lv_rowid:=null;
      
      fetch CUR_PLANES_SIS into lv_rowid;
      exit when CUR_PLANES_SIS%notfound or lv_rowid is null;
      BEGIN
          execute immediate
          'update '||PV_TABLA_NEW||' z set 
            z.COM_771=to_number(replace(trim(nvl(:TARIFAS_INPOOL_771,-1)),'','',''.'')),
            z.COM_776=to_number(replace(trim(nvl(:HOLDING_776,-1)),'','',''.'')),
            z.COM_777=to_number(replace(trim(nvl(:HOLDING_777,-1)),'','',''.'')),
            z.COM_778=to_number(replace(trim(nvl(:HOLDING_778,-1)),'','',''.'')),
            z.COM_779=to_number(replace(trim(nvl(:HOLDING_779,-1)),'','',''.'')),
            z.COM_780=to_number(replace(trim(nvl(:HOLDING_780,-1)),'','',''.'')),
            z.COM_781=to_number(replace(trim(nvl(:HOLDING_781,-1)),'','',''.''))
          WHERE z.rowid=:row_id' using i.TARIFAS_INPOOL_771,i.HOLDING_776,i.HOLDING_777,i.HOLDING_778,i.HOLDING_779,
                                       i.HOLDING_780,i.HOLDING_781,lv_rowid;
      EXCEPTION
        WHEN OTHERS THEN
          execute immediate
          'update '||PV_TABLA_NEW||' z set 
            z.COM_771=to_number(replace(trim(nvl(:TARIFAS_INPOOL_771,-1)),''.'','','')),
            z.COM_776=to_number(replace(trim(nvl(:HOLDING_776,-1)),''.'','','')),
            z.COM_777=to_number(replace(trim(nvl(:HOLDING_777,-1)),''.'','','')),
            z.COM_778=to_number(replace(trim(nvl(:HOLDING_778,-1)),''.'','','')),
            z.COM_779=to_number(replace(trim(nvl(:HOLDING_779,-1)),''.'','','')),
            z.COM_780=to_number(replace(trim(nvl(:HOLDING_780,-1)),''.'','','')),
            z.COM_781=to_number(replace(trim(nvl(:HOLDING_781,-1)),''.'','',''))
          WHERE z.rowid=:row_id' using i.TARIFAS_INPOOL_771,i.HOLDING_776,i.HOLDING_777,i.HOLDING_778,i.HOLDING_779,
                                       i.HOLDING_780,i.HOLDING_781,lv_rowid;
     END;
     end loop;
     CLOSE CUR_PLANES_SIS;
end loop;

commit;

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:=':: ERROR :: '||SQLERRM;
END PR_OBT_HOLDING2;

--Obtengo los costos
PROCEDURE PR_ACTUALIZA_COSTOS(PV_TABLA_NEW VARCHAR2,
                              PV_ERROR OUT VARCHAR2) IS
/*
VPN ---SI SE TRUNCA
BULK ---SI SE TRUNCA
AUT_PURO_VPN ----NO SE TRUNCAN
AUT-PURO ----NO SE TRUNCAN
*/
BEGIN
  ---ACTUALIZAR INPOLL-----
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_INPOOL=trunc(b.COM_inpool/60,5)*60 
where b.COM_INPOOL >= 0 and b.tipo in (''VPN'',''BULK'')';
---ACTUALIZAR CLARO -----
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_CLARO=trunc(b.COM_CLARO/60,5)*60 
where b.COM_CLARO >= 0 and b.tipo in (''VPN'',''BULK'')'; 
---ACTUALIZAR MOVISTAR ----- INTERCONEXION 0.0639
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_MOVISTAR= trunc(0.0639/60,5)*60 + trunc((b.COM_movistar- 0.0639)/60,5)*60
where b.COM_movistar >= 0 and b.tipo in (''VPN'',''BULK'')';
---ACTUALIZAR ALEGRO ----- INTERCONEXION 0.0915
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_ALEGRO= trunc(0.0915/60,5)*60 + trunc((b.COM_alegro- 0.0915)/60,5)*60
where b.COM_alegro >= 0 and b.tipo in (''VPN'',''BULK'')';

---ACTUALIZAR FIJOS ----- INTERCONEXION 0.0162
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_FIJAS= trunc(0.0162/60,5)*60 + trunc((b.COM_FIJAS- 0.0162)/60,5)*60
where b.COM_FIJAS >= 0 and b.tipo in (''VPN'',''BULK'')';

---ACTUALIZAR VPN Y BULK -----
/*execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_771=trunc(b.CLARO_771/60,5)*60 
where b.CLARO_ONNET_VPN_771 >= 0 and b.tipo in (''VPN'',''BULK'')'; */

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_776=trunc(b.CLARO_776/60,5)*60 
where b.CLARO_776 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_777=trunc(b.CLARO_777/60,5)*60 
where b.CLARO_777 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_778=trunc(b.CLARO_778/60,5)*60 
where b.CLARO_778 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_779=trunc(b.CLARO_779/60,5)*60 
where b.CLARO_779 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_780=trunc(b.CLARO_780/60,5)*60 
where b.CLARO_780 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_781=trunc(b.CLARO_781/60,5)*60 
where b.CLARO_781 >= 0 and b.tipo in (''VPN'',''BULK'')'; 

/*execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_771=CLARO_ONNET_VPN_771
where b.CLARO_ONNET_VPN_771 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';*/ 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_776=CLARO_776
where b.CLARO_776 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_777=CLARO_777
where b.CLARO_777 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_778=CLARO_778
where b.CLARO_778 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_779=CLARO_779
where b.CLARO_779 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_780=CLARO_780
where b.CLARO_780 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

execute immediate
'update '||PV_TABLA_NEW||' b 
set b.FACT_781=CLARO_781
where b.CLARO_781 >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')'; 

---------------------------
---------------------------
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_INPOOL=b.COM_inpool 
where b.COM_INPOOL >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';
---ACTUALIZAR CLARO -----
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_CLARO=b.COM_CLARO 
where b.COM_CLARO >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';
---ACTUALIZAR MOVISTAR ----- INTERCONEXION 0.0639
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_MOVISTAR= b.COM_movistar
where b.COM_movistar >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';
---ACTUALIZAR ALEGRO ----- INTERCONEXION 0.0915
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_ALEGRO= b.COM_alegro
where b.COM_alegro >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';

---ACTUALIZAR FIJOS ----- INTERCONEXION 0.0162
execute immediate
'update '||PV_TABLA_NEW||' b set b.FACT_FIJAS=b.COM_FIJAS
where b.COM_FIJAS >= 0 and b.tipo in (''AUT_PURO_VPN'',''AUT-PURO'')';

commit;

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:=SQLERRM;
    rollback;
END;

  --Obtengo una informacion adicional de las tarifas
  PROCEDURE PR_OBT_INF_ADICIONAL(PV_TABLA_NEW VARCHAR2,
                                 PV_ERROR     OUT VARCHAR2) IS

  CURSOR CUR_CARACTERISTICA_AXIS ( VC_ID_PLAN varchar2) is
  select b.Descripcion cat_amx, c.Descripcion clase_amx, d.Descripcion tipo_cliente, e.Descripcion tipo_plan, f.Descripcion segmento, 
         g.descripcion as Desc_Tipo_plan_Suptel,  a.id_clasificacion_03 as REP_ACTIVACION,
         a.*
  from  ge_detalles_planes@axis a, 
        Ge_Categorias_Amx@axis b, 
        ge_clases_amx@axis c, 
        porta.ge_tipo_clientes@axis d,
        porta.ge_tipos_planes@axis e, 
        porta.ge_segmentos_planes@axis f,
        PORTA.GE_TIPO_PLAN_SUPTEL@axis g
  where a.id_plan in (VC_ID_PLAN)
  and   a.Id_Categoria_Amx=b.Id_Categoria_Amx
  and   a.Id_Clase_Amx=c.Id_Clase_Amx and a.Id_Clasificacion_01=d.id_tipo_cliente
  and   a.Id_Tipo_Plan=e.Id_Tipo_Plan and a.Id_Segmento=f.Id_Segmento
  and   a.id_clasificacion_02 = g.id_tipo_plan_suptel(+);

  LN_CONT_COMMIT number :=0;
  LN_LIMITE_COMMIT number :=50;
  CUR_FUENTES_PLANES sys_refcursor;
  ll_query long;
  lv_rowid_plan varchar2(1000);
  lv_id_plan varchar2(20);
  ln_rowid  rowid;

  BEGIN
  ll_query:='select rowid,a.id_Plan from '||PV_TABLA_NEW||' a';

  OPEN CUR_FUENTES_PLANES FOR ll_query;
  loop
      lv_rowid_plan:= null;
      lv_id_plan :=null;
      
      FETCH CUR_FUENTES_PLANES INTO lv_rowid_plan,lv_id_plan;
      exit when CUR_FUENTES_PLANES%notfound or lv_rowid_plan is null;
      begin 
        ln_rowid:=lv_rowid_plan;
      LN_CONT_COMMIT:=LN_CONT_COMMIT+1;
      FOR J in CUR_CARACTERISTICA_AXIS (lv_id_plan) loop        
        begin  
          execute immediate
             'update '||PV_TABLA_NEW||' Z SET Z.CATEGORIA_AMX=:cat_amx ,
                  Z.CLASE_AMX= :clase_amx '
                  ||',Z.TIPO_CLIENTE= :tipo_cliente '
                  ||',Z.TIPO_PLAN= :tipo_plan '
                  ||',Z.SEGMENTO= :segmento '
                  ||',Z.PLAN_SUPTEL= :Desc_Tipo_plan_Suptel '
                  ||'WHERE Z.rowid= :ln_rowid' using j.cat_amx,j.clase_amx,j.tipo_cliente,j.tipo_plan,j.segmento,j.Desc_Tipo_plan_Suptel,ln_rowid; 
           
          exception
                when others then
                 pv_error:='ERROR :: id_plan '||lv_id_plan ||SQLERRM;         
            END;    
      END LOOP;
      if (LN_CONT_COMMIT=LN_LIMITE_COMMIT) then
         COMMIT;
         LN_CONT_COMMIT:=0; 
      end if;
   end;
  END LOOP;
  CLOSE CUR_FUENTES_PLANES;
  commit;
  EXCEPTION
  when others then
    PV_ERROR:='ERROR :: id_plan '||lv_id_plan ||sqlerrm;
  END PR_OBT_INF_ADICIONAL;

  PROCEDURE PR_OBTIENE_CLIENTES_CTAS(PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR     OUT VARCHAR2) IS
    
    lv_error varchar2(1000);
    
  BEGIN

    --Se actualiza la tabla de axis
    --axisfac.pck_gsi_qc_revisa_tarifas_axis.pr_obtiene_clientes_ctas@AXIS(pv_error => lv_error);
    
    --Actualizo la cantidad de clientes y cuentas de AXIS
    execute immediate
    'UPDATE '||PV_TABLA_NEW||' A '||
     ' SET A.CANT_CLIENTES = (SELECT B.CANT_CLIENTES '||
                             ' FROM axisfac.GSI_clientes_por_plan@AXIS B '||
                            ' WHERE B.ID_PLAN = A.ID_PLAN), '||
        ' A.CANT_CUENTAS = (SELECT B.CANT_CUENTAS '||
                            ' FROM axisfac.GSI_clientes_por_plan@AXIS B '||
                            ' WHERE B.ID_PLAN = A.ID_PLAN), '||
        ' A.FECHA_ACT_CLIENTES= (SELECT B.FECHA_ACT '||
                            ' FROM axisfac.GSI_clientes_por_plan@AXIS B '||
                            ' WHERE B.ID_PLAN = A.ID_PLAN) ';
    commit;
    
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:=SQLERRM;

  END PR_OBTIENE_CLIENTES_CTAS;
  --nuevo
  PROCEDURE PR_OBTIENE_COSTOS_BGH(PV_TABLA_NEW VARCHAR2,
                                  PV_ERROR OUT VARCHAR2) IS
    
    /*CURSOR CUR_COSTO_PLANES is 
      select a.rowid,
             a.id_plan
        from GSI_TARIFAS_BSCS_FINAL a
        where a.id_subproducto is null;*/

    CURSOR CUR_OBTENER_DATOS_PLANES (var_id_plan varchar2) is
      SELECT /*+ rule */ dp.id_plan,
             dp.id_detalle_plan,
             v.tmcode,
             dp.ID_SUBPRODUCTO
       FROM bs_planes@axis u, rateplan v, ge_detalles_planes@axis dp
       where dp.id_plan =var_id_plan
         and dp.id_detalle_plan = u.id_detalle_plan
         and u.cod_bscs = v.tmcode
         and u.tipo <> 'A'
         and u.id_clase <> 'TDM';

    cursor CUR_BGH_TARIF (var_tmcode varchar2) is
      select a.tmcode,a.shdes,a.firstname,a.secondname,a.porder,a.plan_type,
        a.tax_code_iva,a.tax_code_ice
          from DOC1.BGH_TARIFF_PLAN_DOC1 a
          WHERE a.tmcode in (var_tmcode);
        
    LN_porta_porta    varchar2(100);
    LN_porta_fijas    varchar2(100);
    LN_porta_movistar varchar2(100);
    LN_porta_alegro   varchar2(100);
    lv_rowid          varchar2(1000);
    lv_id_plan        varchar2(15);
    LN_CONTADOR       number:=0;
    LN_LIMITE         number:=100;
    CUR_COSTO_PLANES  sys_refcursor;
    ll_query          long;
    ll_query2         long;
    
  BEGIN
   ll_query :='SELECT a.rowid,a.id_plan FROM '||PV_TABLA_NEW||' a ';--obtener tarifas
   OPEN CUR_COSTO_PLANES FOR ll_query;
   LOOP
      lv_id_plan:=null;
      lv_rowid:=null;
      fetch CUR_COSTO_PLANES into lv_rowid,lv_id_plan;
      exit when CUR_COSTO_PLANES%notfound or lv_rowid is null;
  --FOR I in CUR_COSTO_PLANES LOOP
  ---sncode,costo,id_plan,porta_porta,porta_fijas,porta_movistar,porta_otecel

      FOR J in CUR_OBTENER_DATOS_PLANES(lv_id_plan) LOOP
          LN_porta_porta:=null;
          LN_porta_fijas:=null;
          LN_porta_movistar:=null;
          LN_porta_alegro:=null;
          
          FOR Z in CUR_BGH_TARIF (J.tmcode) LOOP
                    IF UPPER (Z.FIRSTNAME) LIKE '%COSTO%CLARO%CLARO%' THEN 
                         LN_porta_porta:=Z.SECONDNAME;
                    ELSIF UPPER (Z.FIRSTNAME) LIKE '%COSTO%CLARO%FIJA%' THEN
                         LN_porta_fijas:=Z.SECONDNAME;                         
                    ELSIF UPPER (Z.FIRSTNAME) LIKE '%COSTO%OTECE%' THEN
                         LN_porta_movistar:=Z.SECONDNAME; 
                    ELSIF UPPER (Z.FIRSTNAME) LIKE '%COSTO%CLARO%TELEC%' THEN
                         LN_porta_alegro:=Z.SECONDNAME; 
                    END IF;
          END LOOP;
          ll_query2:='UPDATE '||PV_TABLA_NEW||' a '||
              'set '||
                 ' a.id_detalle_plan=:id_detalle_plan, '||
                 ' a.id_subproducto=:ID_SUBPRODUCTO, '||
                 ' a.bgh_porta_porta=replace(replace(:LN_porta_porta,''$'',''''),'' '',''''), '||
                 ' a.bgh_porta_fijas=replace(replace(:LN_porta_fijas,''$'',''''),'' '',''''), '||
                 ' a.bgh_porta_movistar=replace(replace(:LN_porta_movistar,''$'',''''),'' '',''''), '||
                 ' a.bgh_porta_alegro=replace(replace(:LN_porta_alegro,''$'',''''),'' '','''') '||
              ' where rowid=:lv_rowid ';
          execute immediate ll_query2
           using J.id_detalle_plan,J.ID_SUBPRODUCTO,LN_porta_porta,
                 LN_porta_fijas,LN_porta_movistar,LN_porta_alegro,lv_rowid;
          ---ACTUALIZAR LA TABLA FINAL DE TARIFAS
     END LOOP;
     IF (LN_CONTADOR=LN_LIMITE) THEN
           COMMIT;
           LN_CONTADOR:=0;
     END IF;
     LN_CONTADOR:=LN_CONTADOR+1;
  END LOOP;
  CLOSE CUR_COSTO_PLANES;
  COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLERRM = 'ORA-01403: No se ha encontrado ning�n dato' THEN
        PV_ERROR:=NULL;
      ELSE
        PV_ERROR:=SQLERRM;
      END IF;
      ROLLBACK;
  END PR_OBTIENE_COSTOS_BGH;

  PROCEDURE PR_OBTIENE_TARIFA_BASICA(PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS

   /*CURSOR CUR_REP_URG is
    select rowid, a.id_plan BP, a.descripcion NOMBRE_PLAN, a.tmcode, a.id_detalle_plan
      from gsi_tarifas_bscs_final a
       where a.tarifa_basica is null;*/
  ---WHERE upper(a.NOMBRE_PLAN) like '%CONTROL EMP%'

  CUR_REP_URG          sys_refcursor;
  LL_QUERY_C           long;
  lv_id_plan_c         varchar2(20);
  lv_nombre_plan_c     varchar2(100);
  lv_rowid_c           varchar2(1000);
  ln_tmcode_c          number;
  ln_id_detalle_plan_c number;
  LN_TARIFA_BASICA     float;
  LN_EXISTE            integer;
  LN_TMCODE_MAESTRO    integer;
  LN_TDMA              integer; 
  ln_cont              number:=0;
  LN_ES_POOL            number:=0;

  BEGIN
 
  LL_QUERY_C:= 'select rowid, id_plan BP, descripcion NOMBRE_PLAN, '|| 
                  ' tmcode, id_detalle_plan from '||PV_TABLA_NEW;
  OPEN CUR_REP_URG FOR LL_QUERY_C;
  LOOP
     lv_rowid_c:=null;
     lv_id_plan_c:=null;
     lv_nombre_plan_c:=null;
     ln_tmcode_c:=null;
     ln_id_detalle_plan_c:=null;
  --FOR I in CUR_REP_URG LOOP
     FETCH CUR_REP_URG INTO lv_rowid_c,
                            lv_id_plan_c,
                            lv_nombre_plan_c,
                            ln_tmcode_c,
                            ln_id_detalle_plan_c;
     exit when CUR_REP_URG%notfound or lv_rowid_c is null;
      ln_cont:= ln_cont + 1;
     BEGIN
  ----SACAR TARIFA BASICA PERSONALES -----
          LN_TARIFA_BASICA:=-2;
          LN_TDMA:=0;
        
          select count(*)
            into LN_TDMA
            from sysadm.mpulktmb X
           where vscode in (select max(vscode)
                              from mpulktmb
                             where tmcode = ln_tmcode_c
                               and sncode = 2)
             and X.tmcode = ln_tmcode_c
             and X.sncode = 2;
             
         IF  (LN_TDMA >0) THEN 
              select ACCESSFEE
                into LN_TARIFA_BASICA
                from sysadm.mpulktmb X
               where vscode in (select max(vscode)
                                  from mpulktmb
                                 where tmcode = ln_tmcode_c
                                   and sncode = 2)
                 and X.tmcode = ln_tmcode_c
                 and X.sncode = 2;
         ELSE
             LN_TARIFA_BASICA:=999999;            
         END IF;    
                       

    IF (LN_TARIFA_BASICA <=0) THEN
      -------  SACO EL TMCODE MAESTRO--------------
                  select u.cod_bscs
                    into LN_TMCODE_MAESTRO
                    from bs_planes@axis u
                   where u.id_detalle_plan in (ln_id_detalle_plan_c)
                     and u.TIPO = 'A';
         --Se busca si el plan maestro tiene tarifa basica
          select count(*)
                          into LN_ES_POOL
                          from sysadm.mpulktmb X
                          where vscode in (select max(vscode)
                                  from sysadm.mpulktmb
                                 where tmcode = LN_TMCODE_MAESTRO
                                           and sncode = 1)
                         and X.tmcode = LN_TMCODE_MAESTRO
                         and X.sncode = 1 and accessfee > 0 ;
                   
          
    -- IF (UPPER(lv_nombre_plan_c) LIKE '%CONTROL EMP%' or UPPER(lv_nombre_plan_c) LIKE '%POOL%') THEN
    ---SI TIENE TB EL MAESTRO SIGNIFICA QUE PUEDE SER POOL O CONTROL EMPRESARIAL
     IF (LN_ES_POOL >0 ) THEN               
              ----SACAR TARIFA BASICAS DE LOS POOL Y CONTROL EMPRESARIAL-----
  --        select b.TARIFABASICA into LN_TARIFA_BASICA from cp_planes@axis b WHERE b.ID_PLAN=I.BP;
                     
                  select ACCESSFEE
                    into LN_TARIFA_BASICA
                    from sysadm.mpulktmb X
                    where vscode in (select max(vscode)
                            from sysadm.mpulktmb
                           where tmcode = LN_TMCODE_MAESTRO
                                     and sncode = 1)
                   and X.tmcode = LN_TMCODE_MAESTRO
                   and X.sncode = 1;
      ELSE   
          
          select count(*) into LN_EXISTE from ge_planes_bulk@axis
          where ID_PLAN=lv_id_plan_c;
          
          ----SACAR TARIFA BASICAS DE LOS BULK -----
          IF  (LN_EXISTE > 0) THEN
             select o.MIN_DOLARES_PLAN into LN_TARIFA_BASICA from cl_parametros_bulk@axis o
             where o.ID_PLAN=lv_id_plan_c and rownum<=1;
          ELSE
                  select count(*) into LN_EXISTE from cp_planes@axis b
                  WHERE b.ID_PLAN=lv_id_plan_c and b.tipo_UNIDAD='D';
       
                  IF (LN_EXISTE > 0)  THEN
                        select nvl(b.MIN_INCLUIDOS,-1) into LN_TARIFA_BASICA from cp_planes@axis b
                        WHERE b.ID_PLAN=lv_id_plan_c and b.tipo_UNIDAD='D';
                  ELSE          
                       
                           LN_TARIFA_BASICA:=-1;
                        
                  END IF;
          END IF;      
          
       END IF;   
    END IF;  
       
       execute immediate
         'UPDATE '||PV_TABLA_NEW||' c '||
          ' set c.tarifa_basica=:LN_TARIFA_BASICA '||
         ' WHERE C.ROWID=:lv_rowid_c ' using LN_TARIFA_BASICA,lv_rowid_c;
       IF ln_cont = 100 THEN
         ln_cont :=0;
         commit;
       END IF;
      EXCEPTION
        WHEN OTHERS THEN
           NULL;
    END;
  END LOOP;
  CLOSE CUR_REP_URG;
  COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLERRM like 'ORA-01403%' THEN
        PV_ERROR:=NULL;
      ELSE
        PV_ERROR:=SQLERRM;
      END IF;
      ROLLBACK;
  END PR_OBTIENE_TARIFA_BASICA;

  PROCEDURE PR_OBTIENE_FEATURES_VPN (PV_TABLA_NEW VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
    
    CURSOR C_TIPOS_VPN IS
     SELECT DISTINCT TIPO
       FROM SYSADM.GSI_SNCODE_BSCS
      WHERE TIPO !='NO APLICA';
    
    /*CURSOR C_TARIFAS_FINAL IS
     SELECT ROWID CODIGO, ID_PLAN
       FROM GSI_TARIFAS_BSCS_FINAL;*/
    
    CURSOR C_FEATURES_VPN(PV_TIPO VARCHAR2,PV_ID_PLAN VARCHAR2) IS
            select --a.id_plan,
                   --b.id_detalle_plan,
                   --a.descripcion,
                   d.id_tipo_detalle_serv,
                   --d.descripcion nombre_feature,
                   --c.valor_omision,
                   decode(c.obligatorio || c.actualizable,
                          'NS',
                          'ADICIONAL',
                          'SN',
                          'FIJO',
                          'SS',
                          'FIJO VARIABLE',
                          'MAL_CONFIGURADO') TIPO
                from ge_planes@axis                   a,
                     ge_detalles_planes@axis          b,
                     cl_servicios_planes@axis         c,
                     cl_tipos_detalles_servicios@axis d
               where a.id_plan = b.id_plan
                 and b.id_detalle_plan = c.id_detalle_plan
                 and c.valor_omision in
                     (select distinct cod_axis from bs_servicios_paquete@axis where sn_code in (SELECT UNIQUE SNCODE
                        FROM SYSADM.GSI_SNCODE_BSCS 
                       where tipo =PV_TIPO and tipo !='NO APLICA' ))--'VIRTUAL PRIVATE NETWORK CONTROL'
                 and c.id_tipo_detalle_serv = d.id_tipo_detalle_serv
                 and a.id_plan =PV_ID_PLAN;
                 
     C_TARIFAS_FINAL sys_refcursor;
     LL_QUERY_C      long;
     lv_rowid_c      varchar2(1000);
     lv_id_plan_c    varchar2(20);
     LV_FEATURES     VARCHAR2(1000);
     LV_TIPO         VARCHAR2(1000);
     
  BEGIN
    
    LL_QUERY_C :='SELECT ROWID CODIGO, ID_PLAN '||
                  ' FROM '||PV_TABLA_NEW;
    
    OPEN C_TARIFAS_FINAL FOR LL_QUERY_C;
    --FOR K IN C_TARIFAS_FINAL LOOP
    LOOP
      lv_rowid_c:=NULL;
      lv_id_plan_c:=NULL;
      
      FETCH C_TARIFAS_FINAL INTO lv_rowid_c, lv_id_plan_c;
      
      exit when C_TARIFAS_FINAL%notfound or lv_rowid_c is null;
     
      FOR I IN C_TIPOS_VPN LOOP
        LV_FEATURES:=NULL;
        LV_TIPO:=NULL;
        
        FOR J IN C_FEATURES_VPN(I.TIPO,lv_id_plan_c) LOOP
          IF LV_FEATURES IS NULL THEN
            LV_FEATURES:= J.ID_TIPO_DETALLE_SERV;
            LV_TIPO:= J.TIPO;
          ELSE
            LV_FEATURES:= LV_FEATURES || ';'||J.ID_TIPO_DETALLE_SERV;
            LV_TIPO:= LV_TIPO || ';'||J.TIPO;
          END IF;
          
        END LOOP;
        
        IF I.TIPO = 'VIRTUAL PRIVATE NETWORK CONTROL' THEN
          EXECUTE IMMEDIATE
          'UPDATE '||PV_TABLA_NEW||' A 
           SET A.VPN_APROVISIONADOR = :LV_FEATURES,
               A.VPN_APROVISIONADOR_TIPO = :LV_TIPO
           WHERE A.ROWID = :lv_rowid_c' USING LV_FEATURES,LV_TIPO,lv_rowid_c;
        ELSIF I.TIPO = 'HOLDING VPN ENTRE CUENTAS' THEN
          EXECUTE IMMEDIATE
          'UPDATE '||PV_TABLA_NEW||' A 
           SET A.VPN_HOLDING = :LV_FEATURES,
               A.VPN_HOLDING_TIPO = :LV_TIPO
           WHERE A.ROWID = :lv_rowid_c' USING LV_FEATURES,LV_TIPO,lv_rowid_c;
        ELSIF I.TIPO = 'Llamadas VPN Holding ILIM' THEN
          EXECUTE IMMEDIATE
          'UPDATE '||PV_TABLA_NEW||' A 
           SET A.VPN_HOLDING_ILIM = :LV_FEATURES,
               A.VPN_HOLDING_ILIM_TIPO = :LV_TIPO
           WHERE A.ROWID = :lv_rowid_c' USING LV_FEATURES,LV_TIPO,lv_rowid_c;
        END IF;
        
        COMMIT;
      END LOOP;
      
    END LOOP;
    CLOSE C_TARIFAS_FINAL;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:=SQLERRM;
  END PR_OBTIENE_FEATURES_VPN;

  PROCEDURE PR_CONCATENA_PAQ_FIJOS(PV_TABLA_NEW VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS

      CURSOR CUR_FEATURE_FIJOS (PARAM_ID_PLAN VARCHAR2) IS 
      select T.BP, T.ID_TIPO_DETALLE_SERV,T.NOMBRE_FEATURE,T.MENSAJES,T.COSTO,TIPO_SVA,T.Adicional
        FROM GSI_COSTOS_FEATURES_FIJOS T
       where T.BP in (PARAM_ID_PLAN)
       and T.id_tipo_detalle_serv not in ('AUT-1304', 'TAR-1304') and tipo_sva<>'N/A'  and T.COSTO >0
      ORDER BY decode(T.TIPO_SVA,'SMS',1,'WAP',2,'MMS',3,10);  
      
      CUR_LISTADO_PLANES sys_refcursor;
      LL_QUERY_C         long;
      lv_rowid_c         varchar2(1000);
      lv_id_plan_c       varchar2(20);
      LC_FEATURES        VARCHAR2(500):='NULL';
      LN_BAND_FIRST      INTEGER:=1;
      ln_cont            number:=0;

    BEGIN
      LL_QUERY_C := 'select rowid,id_plan BP '||
                     ' from '||PV_TABLA_NEW;
      OPEN CUR_LISTADO_PLANES FOR LL_QUERY_C;
      
      LOOP
      --FOR K in CUR_LISTADO_PLANES LOOP
          lv_rowid_c:=NULL;
          lv_id_plan_c:=NULL;
          
          FETCH CUR_LISTADO_PLANES INTO lv_rowid_c,
                                        lv_id_plan_c;
          
          exit when CUR_LISTADO_PLANES%notfound or lv_rowid_c is null;
          
          LC_FEATURES:=NULL;
          LN_BAND_FIRST:=1;
          FOR T in CUR_FEATURE_FIJOS (lv_id_plan_c) LOOP
              IF (LN_BAND_FIRST=1) THEN
                 IF  (UPPER(T.TIPO_SVA) NOT LIKE '%SMS%') THEN
                     LC_FEATURES:= 'N/A' || '|' || 'NO TIENE' || '|' || '0' || '|' || '0' || '|' || '0' || '|';
                     LN_BAND_FIRST:=LN_BAND_FIRST+1;                     
                 ELSE 
                     LC_FEATURES:=T.ID_TIPO_DETALLE_SERV || '|' || T.NOMBRE_FEATURE || '|' || T.MENSAJES || '|' || T.COSTO || '|' || T.ADICIONAL || '|';
                 END IF;                 
              END IF;       
              IF (LN_BAND_FIRST=2) THEN     
                  IF  (UPPER(T.TIPO_SVA) NOT LIKE '%SMS%') THEN                 
                      LC_FEATURES:=LC_FEATURES || 'N/A' || '|' || 'NO TIENE' || '|' || '0' || '|' || '0' || '|' || '0' || '|';                     
                      LN_BAND_FIRST:=LN_BAND_FIRST+1;
                  ELSE  
                    LC_FEATURES:=LC_FEATURES || T.ID_TIPO_DETALLE_SERV || '|' || T.NOMBRE_FEATURE || '|' || T.MENSAJES || '|' || T.COSTO || '|' || T.ADICIONAL || '|';              
                  END IF;       
              END IF;    
              IF (LN_BAND_FIRST>=3 and UPPER(T.TIPO_SVA) NOT LIKE '%SMS%') THEN
                 LC_FEATURES:=LC_FEATURES || T.ID_TIPO_DETALLE_SERV || '|' || T.NOMBRE_FEATURE || '|' || T.MENSAJES || '|' || T.COSTO || '|' || T.ADICIONAL || '|';
              END IF;
              LN_BAND_FIRST:=LN_BAND_FIRST+1;
          END LOOP;      
          ln_cont:= ln_cont + 1;
          
          execute immediate
          'UPDATE '||PV_TABLA_NEW||' y '||
           ' SET y.features_fijos=SUBSTR(:LC_FEATURES,1,length(:LC_FEATURES)-1) '||
          ' WHERE y.rowid=:lv_id_plan_c' using LC_FEATURES, LC_FEATURES, lv_id_plan_c;
           
          IF ln_cont = 100 THEN
            commit;
            ln_cont := 0;
          END IF;
      END LOOP;
      CLOSE CUR_LISTADO_PLANES;
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR:= SQLERRM;
      
  END PR_CONCATENA_PAQ_FIJOS;

  PROCEDURE PR_OBTIENE_PAQ_FIJOS(PV_TABLA_NEW VARCHAR2,
                                 PV_ERROR OUT VARCHAR2) IS

      CURSOR CUR_FEATURES_FIJOS(BP varchar2) is
                 select a.id_plan,
                       b.id_detalle_plan,
                       a.descripcion,
                       d.id_tipo_detalle_serv,
                       d.descripcion nombre_feature,
                       c.valor_omision,
      --               d.id_grupo_tipo_det_servicio,
                       decode(c.obligatorio || c.actualizable,
                              'NS',
                              'ADICIONAL',
                              'SN',
                              'FIJO',
                              'SS',
                              'FIJO VARIABLE',
                              'MAL_CONFIGURADO') TIPO
     --                  c.fecha_desde,
     --                  c.oculto_rol,
     --                  c.obligatorio,
     --                  c.actualizable,
     --                  b.id_subproducto
                  from ge_planes@axis                   a,
                       ge_detalles_planes@axis          b,
                       cl_servicios_planes@axis         c,
                       cl_tipos_detalles_servicios@axis d
                 where a.id_plan = b.id_plan
                   and a.id_plan in (BP)
                   and b.id_detalle_plan = c.id_detalle_plan
                   and (c.obligatorio = 'S' and c.actualizable = 'N')
                   and c.valor_omision not in
                       ('93', '302', '304', '303', '305', '306', '307', '308',
                        '309', '310', '311', '312', '314', '315', '316', '317',
                        '323', '318', '319', '320', '321', '322', '324', '325',
                        '331', '326', '327', '328', '329', '330', '332', '344',
                        '346', '354', '355', '356', '357', '358', '359', '360',
                        '365', '366', '367', '368', '369', '370', '373', '376',
                        '377', '378', '379', '382', '383', '384', '385', '386', '20',
                        '375', '1246', '398', '313', '381', '600', '32', '1', '12', '35',
                        '800', '632', '472', '473', '415', '414', '416', '969',
                        '412', '673', '674', '675', '473', '695', '34', '4',
                        '821', '1218', '1219', '1012', '150', '34', '679',
                        'LLAESP', 'LLAINT', '1088', '1069', '410', '411', '409',
                        '968', '557', '556', '31', '917', '1089', '388','1304','1483','1292','1693')
                   and c.id_tipo_detalle_serv = d.id_tipo_detalle_serv;

      CUR_PLANES_LISTADO SYS_REFCURSOR;
      LL_QUERY_C         LONG;
      lv_rowid_c         varchar2(1000);
      lv_id_plan_c       varchar2(20);
      ln_tmcode_c        number;
      ln_existe_mapeo    integer := -2;
      ln_sncode          integer := 0;
      LN_COSTO           float := 0;
      lc_tipo_sva        varchar2(10); 
      lf_mensajes        float (10);
      ln_cont            number:=0;
      lv_error           varchar2(1000);
      
    BEGIN
      
      LL_QUERY_C := 'select rowid, '||
                        ' id_plan BP, '||
                        ' tmcode '||
                   ' from '||PV_TABLA_NEW;
                   
      execute immediate 'truncate table GSI_COSTOS_FEATURES_FIJOS';
      
      OPEN CUR_PLANES_LISTADO FOR LL_QUERY_C;
      LOOP
      
        lv_rowid_c:=null;
        lv_id_plan_c:=null;
        ln_tmcode_c:=null;
        
        FETCH CUR_PLANES_LISTADO INTO lv_rowid_c,
                                      lv_id_plan_c,
                                      ln_tmcode_c;
        
        exit when CUR_PLANES_LISTADO%notfound or lv_rowid_c is null;
        
      --FOR T in CUR_PLANES_LISTADO loop
        ln_cont := ln_cont +1;
        FOR J IN CUR_FEATURES_FIJOS(lv_id_plan_c) LOOP
        
          select count(*) into ln_existe_mapeo
            from bs_servicios_paquete@axis MM 
           where MM.cod_axis in (J.valor_omision)
             and MM.ID_CLASE <> 'TDM';
        
          LN_COSTO := 0;
          IF (ln_existe_mapeo < 0) THEN
            ln_sncode := -2; --- NO TIENE MAPEO COSTO 0
          ELSE
            select RR.sn_code into ln_sncode
              from bs_servicios_paquete@axis RR
             where RR.cod_axis in (J.valor_omision)
               and RR.ID_CLASE <> 'TDM'
               and rownum < 2;
          END IF;
        
          IF ln_sncode > 0 THEN
            select ACCESSFEE
              into LN_COSTO
              from sysadm.mpulktmb@bscsprod X
             where vscode in (select max(vscode)
                                from sysadm.mpulktmb@bscsprod
                               where tmcode = ln_tmcode_c
                                 and sncode = ln_sncode)
               and X.tmcode = ln_tmcode_c
               and X.sncode = ln_sncode;
          END IF;
          
          select count(*) into ln_existe_mapeo
          from es_mapeo_plan_cre@espejo_rcc a
          WHERE PLAN_AXIS in (J.id_tipo_detalle_serv);     
          
          IF ln_existe_mapeo >0 THEN
              select a.tipo_sva,mensajes 
                 into lc_tipo_sva,lf_mensajes 
                from es_mapeo_plan_cre@espejo_rcc a
              WHERE PLAN_AXIS in (J.id_tipo_detalle_serv);
          ELSE    
              lc_tipo_sva:='N/A';
              lf_mensajes:=-1;
          END IF;
          insert into GSI_COSTOS_FEATURES_FIJOS
            (bp, tmcodes, sncode, id_tipo_detalle_serv, costo,tipo_sva,mensajes,nombre_feature)
          values
            (lv_id_plan_c, ln_tmcode_c, ln_sncode, J.id_tipo_detalle_serv, LN_COSTO,lc_tipo_sva,lf_mensajes,J.nombre_feature);
            
        END LOOP;
        
        if ln_cont = 20 THEN
          ln_cont:=0;
          commit;
        END IF;
      END LOOP;
      CLOSE CUR_PLANES_LISTADO;
      COMMIT;
      PR_CONCATENA_PAQ_FIJOS(pv_tabla_new => PV_TABLA_NEW,
                             pv_error     => lv_error);
      
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR:=lv_id_plan_c||'::'||SQLERRM;
        IF PV_ERROR LIKE '%ORA-01403%' THEN
          PV_ERROR:='';
        END IF;
        rollback;
    END PR_OBTIENE_PAQ_FIJOS;

  PROCEDURE PR_OBTIENE_FU_PACK_ID(PV_TABLA_NEW VARCHAR2,
                                  PV_ERROR OUT VARCHAR2) IS
    
    CURSOR C_FU_PACK_ID(PV_TMCODE VARCHAR2) IS
      Select b.des, x.accessfee, a.fu_pack_id, d.long_name, c.free_units_volume
        From sysadm.fup_tariff a, 
             sysadm.rateplan b, 
             sysadm.fup_element_definition c, 
             sysadm.fu_pack d, 
             sysadm.mpulktm1 x
        Where a.tmcode=b.tmcode 
        And a.fu_pack_id=d.fu_pack_id 
        And a.tmcode=x.tmcode
        And x.sncode=2
        And c.fu_pack_id=a.fu_pack_id 
        And c.fup_version= (Select Max(fup_version) From sysadm.fup_element_definition e 
                            Where e.fu_pack_id=a.fu_pack_id
                            And work_state='P')
        And a.tmcode = PV_TMCODE;--Poner aqui los TMCODES
    
    C_TMCODE    SYS_REFCURSOR;
    LV_CADENA   VARCHAR2(1500);
    LN_CONT     NUMBER:=0;
    LL_QUERY    LONG;
    LN_TMCODE_C NUMBER;
    
  BEGIN
   
  LL_QUERY :='SELECT DISTINCT TMCODE FROM '||PV_TABLA_NEW;
  
  OPEN C_TMCODE FOR LL_QUERY;
  
  LOOP
    LN_TMCODE_C :=NULL;
    
    FETCH C_TMCODE INTO LN_TMCODE_C;
    EXIT WHEN C_TMCODE%NOTFOUND OR LN_TMCODE_C IS NULL;
--   FOR I IN C_TMCODE LOOP
     LV_CADENA := NULL;
     LN_CONT := LN_CONT + 1;
     
     FOR J IN C_FU_PACK_ID(LN_TMCODE_C) LOOP
       IF LV_CADENA IS NULL THEN
         LV_CADENA := '<'||j.des||'|'||j.accessfee||'|'||j.fu_pack_id||'|'||j.long_name||'|'||j.free_units_volume||'>';
       ELSE
         LV_CADENA := LV_CADENA||'<'||j.des||'|'||j.accessfee||'|'||j.fu_pack_id||'|'||j.long_name||'|'||j.free_units_volume||'>';
       END IF;
     END LOOP;
     
     EXECUTE IMMEDIATE
     'UPDATE '||PV_TABLA_NEW||' A '||
      'SET A.FU_PACK_ID = :LV_CADENA '||
      'WHERE A.TMCODE = :TMCODE' USING LV_CADENA,LN_TMCODE_C;
      
     IF LN_CONT > 100 THEN
       COMMIT;
       LN_CONT:=0;
     END IF;
   END LOOP;
   COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:= SQLERRM;
  END PR_OBTIENE_FU_PACK_ID;

  PROCEDURE PR_REPORTE_FACT(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    --Obtiene los valores facturados
    cursor c_tarifas is
      select a.id_plan,
         a.descripcion,
         a.com_inpool,
         a.claro_inpool,
         a.fact_inpool,
         a.com_claro,
         a.fact_claro,
         a.claro,
         a.com_movistar,
         a.fact_movistar,
         a.movistar,
         a.com_alegro,
         a.fact_alegro,
         a.alegro,
         a.com_fijas,
         a.fact_fijas,
         a.fija,
         a.toll toll_sis,
         a.toll_com,
         a.CATEGORIA_AMX,
         a.CLASE_AMX,
         a.TIPO_CLIENTE,
         a.TIPO_PLAN,
         a.SEGMENTO,
         a.PLAN_SUPTEL,
         a.estado,
         a.cant_clientes
    from GSI_TARIFAS_BSCS_TMP a
   where (a.claro_inpool != a.fact_inpool or a.claro != a.fact_claro or
         a.movistar != a.fact_movistar or a.alegro != a.fact_alegro or
         a.fija != a.fact_fijas)
     and a.estado = 'A';
     
     --DESA
     --LV_DIRECTORIO       Varchar2(200) := 'PLANES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_valores_fac';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');

    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_INPOOL </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_INPOOL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_INPOOL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_CLARO </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_MOVISTAR </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_MOVISTAR </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> MOVISTAR </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_ALEGRO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_ALEGRO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ALEGRO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FIJA </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>'||
       '</TR>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    for a in c_tarifas loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e := '<tr>'||
                      '<td>'||a.id_plan||'</td>'||
                      '<td>'||a.descripcion||'</td>'||
                      '<td>'||a.com_inpool||'</td>'||
                      '<td>'||a.claro_inpool||'</td>'||
                      '<td>'||a.fact_inpool||'</td>'||
                      '<td>'||a.com_claro||'</td>'||
                      '<td>'||a.fact_claro||'</td>'||
                      '<td>'||a.claro||'</td>'||
                      '<td>'||a.com_movistar||'</td>'||
                      '<td>'||a.fact_movistar||'</td>'||
                      '<td>'||a.movistar||'</td>'||
                      '<td>'||a.com_alegro||'</td>'||
                      '<td>'||a.fact_alegro||'</td>'||
                      '<td>'||a.alegro||'</td>'||
                      '<td>'||a.com_fijas||'</td>'||
                      '<td>'||a.fact_fijas||'</td>'||
                      '<td>'||a.fija||'</td>'||
                      '<td>'||a.toll_sis||'</td>'||
                      '<td>'||a.toll_com||'</td>'||
                      '<td>'||a.CATEGORIA_AMX||'</td>'||
                      '<td>'||a.CLASE_AMX||'</td>'||
                      '<td>'||a.TIPO_CLIENTE||'</td>'||
                      '<td>'||a.TIPO_PLAN||'</td>'||
                      '<td>'||a.SEGMENTO||'</td>'||
                      '<td>'||a.PLAN_SUPTEL||'</td>'||
                      '<td>'||a.estado||'</td>'||
                      '<td>'||a.cant_clientes||'</td>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo1:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_FACT;

  PROCEDURE PR_REPORTE_LDI(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    ----ESTO ES PARA SACAR LA PARTE DE LDI----
    cursor c_tarifas is
      select a.id_plan,
         a.descripcion,
         a.toll toll_sis,
         a.toll_com,
         a.CATEGORIA_AMX,
         a.CLASE_AMX,
         a.TIPO_CLIENTE,
         a.TIPO_PLAN,
         a.SEGMENTO,
         a.PLAN_SUPTEL,
         a.estado,
         a.cant_clientes
        from GSI_TARIFAS_BSCS_TMP a
       where a.toll != a.toll_com
         and a.estado = 'A';
     
     --DESA
     --LV_DIRECTORIO       Varchar2(200) := 'PLANES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_LDI_Toll_VS_TollCom';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>'||
       '</TR>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    
    for a in c_tarifas loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e := '<tr>'||
                      '<TD>'||a.id_plan||'</TD>'||
                      '<TD>'||a.descripcion||'</TD>'||
                      '<TD>'||a.toll_sis||'</TD>'||
                      '<TD>'||a.toll_com||'</TD>'||
                      '<TD>'||a.CATEGORIA_AMX||'</TD>'||
                      '<TD>'||a.CLASE_AMX||'</TD>'||
                      '<TD>'||a.TIPO_CLIENTE||'</TD>'||
                      '<TD>'||a.TIPO_PLAN||'</TD>'||
                      '<TD>'||a.SEGMENTO||'</TD>'||
                      '<TD>'||a.PLAN_SUPTEL||'</TD>'||
                      '<TD>'||a.estado||'</TD>'||
                      '<TD>'||a.cant_clientes||'</TD>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo2:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_LDI;

  PROCEDURE PR_REPORTE_LDI_BSCS_COLEC(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    ----ESTO ES PARA SACAR LDI DEL PLAN SIMIL DE BSCS Y DE TN3----
    cursor c_tarifas is
      select a.id_plan,
             a.descripcion,
             a.toll           toll_sis_bscs,
             a.toll_com       toll_com_bscs,
             a.id_plan_pareja,
             b.id_Plan        id_Plan_tn3,
             b.toll           toll_sis_tn3,
             b.toll_com       toll_com_tn3,
             a.CATEGORIA_AMX,
             a.CLASE_AMX,
             a.TIPO_CLIENTE,
             a.TIPO_PLAN,
             a.SEGMENTO,
             a.PLAN_SUPTEL,
             a.estado,
             a.cant_clientes
        from GSI_TARIFAS_BSCS_TMP a, 
         sms.gsi_tarifas_tn3_FINAL@colector b
       where a.id_plan_pareja = b.id_Plan 
         and a.toll_com <> b.toll_com;
     
     --DESA
     --LV_DIRECTORIO       Varchar2(200) := 'PLANES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_LDI_BSCS_COLEC';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS_BSCS </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM_BSCS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN_PAREJA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN_TN3 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS_TN3 </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM_TN3 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>'||
       '</TR>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    
    for a in c_tarifas loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e := '<tr>'||
                      '<TD>'||A.ID_PLAN||'</TD>'||
                      '<TD>'||A.DESCRIPCION||'</TD>'||
                      '<TD>'||A.TOLL_SIS_BSCS||'</TD>'||
                      '<TD>'||A.TOLL_COM_BSCS||'</TD>'||
                      '<TD>'||A.ID_PLAN_PAREJA||'</TD>'||
                      '<TD>'||A.ID_PLAN_TN3||'</TD>'||
                      '<TD>'||A.TOLL_SIS_TN3||'</TD>'||
                      '<TD>'||A.TOLL_COM_TN3||'</TD>'||
                      '<TD>'||A.CATEGORIA_AMX||'</TD>'||
                      '<TD>'||A.CLASE_AMX||'</TD>'||
                      '<TD>'||A.TIPO_CLIENTE||'</TD>'||
                      '<TD>'||A.TIPO_PLAN||'</TD>'||
                      '<TD>'||A.SEGMENTO||'</TD>'||
                      '<TD>'||A.PLAN_SUPTEL||'</TD>'||
                      '<TD>'||A.ESTADO||'</TD>'||
                      '<TD>'||A.CANT_CLIENTES||'</TD>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo3:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_LDI_BSCS_COLEC;

  PROCEDURE PR_REPORTE_PLAN_PAR_BSCS_COLEC(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    ----ESTO ES PARA SACAR LDI DEL PLAN PAREJA DE BSCS VS TN3----
    cursor c_tarifas is
      select a.id_plan,
             a.descripcion,
             a.toll           toll_sis_bscs,
             a.toll_com       toll_com_bscs,
             a.id_plan_pareja,
             b.id_Plan        id_Plan_tn3,
             b.toll           toll_sis_tn3,
             b.toll_com       toll_com_tn3,
             a.CATEGORIA_AMX,
             a.CLASE_AMX,
             a.TIPO_CLIENTE,
             a.TIPO_PLAN,
             a.SEGMENTO,
             a.PLAN_SUPTEL,
             a.estado,
             a.cant_clientes
        from GSI_TARIFAS_BSCS_TMP a,
         sms.gsi_tarifas_tn3_FINAL@colector b
       where a.id_plan_pareja = b.id_Plan
       and (a.toll_com <> a.toll or
            b.toll_com <> b.toll);
     
     --DESA
     --LV_DIRECTORIO       Varchar2(200) := 'PLANES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_PLAN_PAREJA_BSCS_COLEC';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS_BSCS </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM_BSCS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN_PAREJA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN_TN3 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_SIS_TN3 </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM_TN3 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>'||
       '</TR>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    
    for a in c_tarifas loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e := '<tr>'||
                      '<TD>'||A.ID_PLAN||'</TD>'||
                      '<TD>'||A.DESCRIPCION||'</TD>'||
                      '<TD>'||A.TOLL_SIS_BSCS||'</TD>'||
                      '<TD>'||A.TOLL_COM_BSCS||'</TD>'||
                      '<TD>'||A.ID_PLAN_PAREJA||'</TD>'||
                      '<TD>'||A.ID_PLAN_TN3||'</TD>'||
                      '<TD>'||A.TOLL_SIS_TN3||'</TD>'||
                      '<TD>'||A.TOLL_COM_TN3||'</TD>'||
                      '<TD>'||A.CATEGORIA_AMX||'</TD>'||
                      '<TD>'||A.CLASE_AMX||'</TD>'||
                      '<TD>'||A.TIPO_CLIENTE||'</TD>'||
                      '<TD>'||A.TIPO_PLAN||'</TD>'||
                      '<TD>'||A.SEGMENTO||'</TD>'||
                      '<TD>'||A.PLAN_SUPTEL||'</TD>'||
                      '<TD>'||A.ESTADO||'</TD>'||
                      '<TD>'||A.CANT_CLIENTES||'</TD>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo4:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_PLAN_PAR_BSCS_COLEC;

  PROCEDURE PR_REPORTE_COM(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    -----ESTO ES PARA REVISAR QUE ESTEN TODOS CUADRADOS SEGUN LO ENVIADO POR COMERCIAL PARA PLANES BULK---------------------------
    cursor c_tarifas is
        select a.id_plan,
               a.descripcion,
               a.toll,
               a.chile - b.chile REV_CHILE,
               a.cuba - b.cuba REV_CUBA,
               a.espana - b.espana REV_ESPANA,
               a.italia - b.italia REV_italia,
               a.canada - b.canada REV_canada,
               a.usa - b.usa REV_usa,
               a.europa - b.europa REV_europa,
               a.japon - b.japon REV_japon,
               a.maritima - b.maritima REV_maritima,
               a.mexico - b.mexico REV_mexico,
               a.mexico_cel - b.mexico_cel REV_mexico_CEL,
               a.pacto_andino - b.pacto_andino REV_pacto_andino,
               a.restoamerica - b.restoamerica REV_restoamerica,
               a.restodelmundo - b.restodelmundo REV_restodelmundo,
               a.zona_especial - b.zona_especial REV_zona_especial,
               a.alemania - b.alemania REV_alemania,
               a.argentina - b.argentina REV_argentina,
               a.bolivia - b.bolivia REV_bolivia,
               a.brasil - b.brasil REV_brasil,
               a.china - b.china REV_china,
               a.colombia - b.colombia REV_colombia,
               a.costa_rica - b.costa_rica REV_costa_rica,
               a.francia - b.francia REV_francia,
               a.cant_Clientes,
               a.Cant_cuentas
          from GSI_TARIFAS_BSCS_TMP a, GSI_PLANTILLAS_TOLL b
         where a.toll = b.nombre
           and (a.chile - b.chile != 0 or a.cuba - b.cuba != 0 or
               a.espana - b.espana != 0 or
               --a.italia-b.italia!=0 or
               a.canada - b.canada != 0 or a.usa - b.usa != 0 or
               a.europa - b.europa != 0 or a.japon - b.japon != 0 or
               a.maritima - b.maritima != 0 or a.mexico - b.mexico != 0 or
               a.mexico_cel - b.mexico_cel != 0 or
               a.pacto_andino - b.pacto_andino != 0 or
               a.restoamerica - b.restoamerica != 0 or
               a.restodelmundo - b.restodelmundo != 0 or
               a.zona_especial - b.zona_especial != 0
               --a.alemania-b.alemania!=0 or
               --a.argentina-b.argentina!=0 or
               --a.bolivia-b.bolivia!=0 or
               --a.brasil-b.brasil!=0 or
               --a.china-b.china!=0 or
               --a.colombia-b.colombia!=0 or
               --a.costa_rica-b.costa_rica!=0 or
               --a.francia-b.francia!=0 
               );
     
     --DESA
     --LV_DIRECTORIO       Varchar2(200) := 'PLANES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_COM';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_CHILE </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_CUBA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_CUBA </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_ITALIA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_CANADA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_USA </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_USA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_JAPON </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_MARITIMA </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_MEXICO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_MEXICO_CEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_PACTO_ANDINO </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_RESTOAMERICA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_RESTODELMUNDO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_ZONA_ESPECIAL </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_ALEMANIA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_ARGENTINA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_BOLIVIA </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_BRASIL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_CHINA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_COLOMBIA </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_COSTA_RICA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> REV_FRANCIA </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CUENTAS </td>'||
       '</TR>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    
    for a in c_tarifas loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e := '<tr>'||
                      '<TD>'||A.ID_PLAN||'</TD>'||
                      '<TD>'||A.DESCRIPCION||'</TD>'||
                      '<TD>'||A.TOLL||'</TD>'||
                      --'<TD>'||A.TOLL_CCR||'</TD>'||
                      '<TD>'||A.REV_CHILE||'</TD>'||
                      '<TD>'||A.REV_CUBA||'</TD>'||
                      '<TD>'||A.REV_ESPANA||'</TD>'||
                      '<TD>'||A.REV_ITALIA||'</TD>'||
                      '<TD>'||A.REV_CANADA||'</TD>'||
                      '<TD>'||A.REV_USA||'</TD>'||
                      '<TD>'||A.REV_EUROPA||'</TD>'||
                      '<TD>'||A.REV_JAPON||'</TD>'||
                      '<TD>'||A.REV_MARITIMA||'</TD>'||
                      '<TD>'||A.REV_MEXICO||'</TD>'||
                      '<TD>'||A.REV_MEXICO_CEL||'</TD>'||
                      '<TD>'||A.REV_PACTO_ANDINO||'</TD>'||
                      '<TD>'||A.REV_RESTOAMERICA||'</TD>'||
                      '<TD>'||A.REV_RESTODELMUNDO||'</TD>'||
                      '<TD>'||A.REV_ZONA_ESPECIAL||'</TD>'||
                      '<TD>'||A.REV_ALEMANIA||'</TD>'||
                      '<TD>'||A.REV_ARGENTINA||'</TD>'||
                      '<TD>'||A.REV_BOLIVIA||'</TD>'||
                      '<TD>'||A.REV_BRASIL||'</TD>'||
                      '<TD>'||A.REV_CHINA||'</TD>'||
                      '<TD>'||A.REV_COLOMBIA||'</TD>'||
                      '<TD>'||A.REV_COSTA_RICA||'</TD>'||
                      '<TD>'||A.REV_FRANCIA||'</TD>'||
                      '<TD>'||A.CANT_CLIENTES||'</TD>'||
                      '<TD>'||A.CANT_CUENTAS||'</TD>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo5:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_COM;
  
  PROCEDURE PR_REPORTE_AUT_PURO_BULK(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    --Obtiene las tarifas bulk
    cursor c_tarifas_bulk_aut_puro is
      select a.id_plan,
             a.tmcode,
             a.descripcion,
             a.fecha_creacion,
             a.cant_cuentas,
             a.cant_clientes,
             a.estado,
             a.toll,
             a.toll_com,
             decode(a.claro - a.fact_claro, 0,'-',a.claro) as claro,
             decode(a.claro - a.fact_claro, 0,'-',a.com_claro) as com_claro,
             decode(a.movistar - a.fact_movistar, 0,'-',a.movistar) as movistar,--a.movistar,
             decode(a.movistar - a.fact_movistar, 0,'-',a.com_movistar) as com_movistar,
             decode(a.FIJA - a.fact_fijas, 0,'-',a.FIJA) as tar_local,
             decode(a.FIJA - a.fact_fijas, 0,'-',a.com_fijas) as com_fijas,
             decode(a.alegro - a.fact_alegro,0,'-',alegro) as alegro,--a.alegro,
             decode(a.alegro - a.fact_alegro,0,'-',com_alegro) as com_alegro,
             a.categoria_amx, 
             a.clase_amx, 
             a.tipo_cliente, 
             a.tipo_plan, 
             a.segmento, 
             a.plan_suptel,
             a.recurso,
             tipo
        from GSI_TARIFAS_BSCS_TMP a
       where estado = 'A'
         and (tipo = 'BULK' or tipo = 'AUT-PURO')
         --and id_plan in (select a.id_plan from Gsi_Tarifas_Tn3_FINAL a where "1700_VPN_772"=0) --BULK
         and (
                (a.claro - a.fact_claro)  <> 0 or
                (a.movistar - a.fact_movistar)  <> 0 or
                (a.alegro - a.fact_alegro)  <> 0 or
                (a.FIJA - a.fact_fijas)  <> 0 
              );
     --DESA
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     --LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_BULK_AUT_PURO';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos TN3</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TMCODE </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FECHA_CREACION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CUENTAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> MOVISTAR </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_MOVISTAR </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TAR_LOCAL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ALEGRO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_ALEGRO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>'||    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> RECURSO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO </td>'||
    '</tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    for a in c_tarifas_bulk_aut_puro loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e :=
       '<tr>'||
       '<TD align="left" > '||a.id_plan|| ' </td>';
       
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tmcode|| ' </td>'||
       '<TD align="left" > '||a.descripcion|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.fecha_creacion|| ' </td>'||
       '<TD align="left" > '||a.cant_cuentas|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.cant_clientes|| ' </td>'||
       '<TD align="left" > '||a.estado|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.toll|| ' </td>'||
       '<TD align="left" > '||a.toll_com|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.claro|| ' </td>'||
       '<TD align="left" > '||a.com_claro|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.movistar|| ' </td>'||
       '<TD align="left" > '||a.com_movistar|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tar_local|| ' </td>'||
       '<TD align="left" > '||a.com_fijas|| ' </td>'||
       '<TD align="left" > '||a.alegro|| ' </td>'||
       '<TD align="left" > '||a.com_alegro|| '</td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.CATEGORIA_AMX|| ' </td>'||
       '<TD align="left" > '||a.CLASE_AMX|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.TIPO_CLIENTE|| ' </td>'||
       '<TD align="left" > '||a.TIPO_PLAN|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.SEGMENTO|| ' </td>'||
       '<TD align="left" > '||a.PLAN_SUPTEL|| ' </td>'||
       '<TD align="left" > '||a.recurso|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        
      Lv_Linea_e := '<TD align="left" > '||a.tipo|| ' </td>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
  --    exit;
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo6:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_AUT_PURO_BULK;
  
PROCEDURE PR_REPORTE_AUT_PURO_VPN(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    --Obtiene las tarifas bulk
    cursor c_tarifas_bulk_aut_puro is
      select a.id_plan,
             a.tmcode,
             a.descripcion,
             a.fecha_creacion,
             a.cant_cuentas,
             a.cant_clientes,
             a.estado,
             a.toll,
             a.toll_com,
             decode(a.claro - a.fact_claro, 0,'-',a.claro) as claro,
             decode(a.claro - a.fact_claro, 0,'-',a.com_claro) as com_claro,
             decode(a.MOVISTAR - a.fact_movistar, 0,'-',a.MOVISTAR) as movistar,--a.movistar,
             decode(a.MOVISTAR - a.fact_movistar, 0,'-',a.com_movistar) as com_movistar,
             decode(a.FIJA - a.fact_fijas, 0,'-',a.FIJA) as tar_local,
             decode(a.FIJA - a.fact_fijas, 0,'-',a.com_fijas) as com_fijas,
             decode(a.CLARO_INPOOL - a.fact_inpool, 0,'-',a.CLARO_INPOOL) as inpool,
             decode(a.CLARO_INPOOL - a.fact_inpool, 0,'-',a.com_inpool) as com_inpool,
             decode(a.fact_776 - a.CLARO_776, 0,'-',a.com_776) as fact_776,
             decode(a.fact_776 - a.CLARO_776, 0,'-',a.CLARO_776) as claro_vpn_776,
             --decode(a.FACT_771 - a.CLARO_ONNET_VPN_771, 0,'-',a.COM_771) as FACT_771,
             --decode(a.FACT_771 - a.CLARO_ONNET_VPN_771, 0,'-',a.CLARO_ONNET_VPN_771) as CLARO_ONNET_VPN_771,
             decode(a.FACT_777 - a.CLARO_777, 0,'-',a.COM_777) as FACT_777,
             decode(a.FACT_777 - a.CLARO_777, 0,'-',a.CLARO_777) as claro_vpn_777,
             decode(a.FACT_778 - a.CLARO_778, 0,'-',a.COM_778) as FACT_778,
             decode(a.FACT_778 - a.CLARO_778, 0,'-',a.CLARO_778) as CLARO_VPN_778,
             decode(a.FACT_779 - a.CLARO_779, 0,'-',a.COM_779)as FACT_779,
             decode(a.FACT_779 - a.CLARO_779, 0,'-',a.CLARO_779) as CLARO_VPN_779,
             decode(a.FACT_780 - a.CLARO_780, 0,'-',a.COM_780) FACT_780,
             decode(a.FACT_780 - a.CLARO_780, 0,'-',a.CLARO_780) CLARO_VPN_780,
             decode(a.FACT_781 - a.CLARO_781, 0,'-',a.COM_781) FACT_781,
             decode(a.FACT_781 - a.CLARO_781, 0,'-',a.CLARO_781) CLARO_VPN_781,
             decode(a.alegro - a.fact_alegro,0,'-',alegro) as alegro,--a.alegro,
             decode(a.alegro - a.fact_alegro,0,'-',com_alegro) as com_alegro,
             a.recurso,
             tipo
        from GSI_TARIFAS_BSCS_TMP a
       where estado = 'A'
         and (tipo ='VPN' or tipo = 'AUT_PURO_VPN')
         --and id_plan in (select a.id_plan from Gsi_Tarifas_Tn3_FINAL a where "1700_VPN_772"!=0) --VPN
         and (
                   a.FACT_776 <> a.CLARO_776 or
                   a.FACT_777 <> a.CLARO_777 or
                   a.FACT_778 <> a.CLARO_778 or
                   a.FACT_779 <> a.CLARO_779 or
                   a.FACT_780 <> a.CLARO_780 or
                   a.FACT_781 <> a.CLARO_781 or
                   a.claro <> a.fact_claro or
                   --a.FACT_771 <> a.CLARO_ONNET_VPN_771 or
                   a.FACT_INPOOL <> a.CLARO_INPOOL or
                   a.FACT_ALEGRO <> a.ALEGRO or
                   a.FACT_MOVISTAR <> a.MOVISTAR or
                   a.FACT_FIJAS <> a.FIJA or
                   (a.alegro - a.fact_alegro)  <> 0 or
                   --a.FACT_FIJAS <> A.INTERREG_VPN_775 or
                   a.FIJA - a.fact_fijas <> 0 
              );
     --DESA
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     --LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_VPN_AUT_PURO_VPN';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos TN3</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TMCODE </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FECHA_CREACION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CUENTAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> MOVISTAR </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_MOVISTAR </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TAR_LOCAL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> INPOOL </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_INPOOL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_776 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_776 </td>'--||
       /*'<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_771 </td>'*/;
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       --'<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_ONNET_VPN_771 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_777 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_777 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_778 </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_778 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_779 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_779 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FACT_780 </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_780 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_781 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLARO_VPN_781 </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ALEGRO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_ALEGRO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> RECURSO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO </td>'||
    '</tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    for a in c_tarifas_bulk_aut_puro loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e :=
       '<tr>'||
       '<TD align="left" > '||a.id_plan|| ' </td>';
       
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tmcode|| ' </td>'||
       '<TD align="left" > '||a.descripcion|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.fecha_creacion|| ' </td>'||
       '<TD align="left" > '||a.cant_cuentas|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.cant_clientes|| ' </td>'||
       '<TD align="left" > '||a.estado|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.toll|| ' </td>'||
       '<TD align="left" > '||a.toll_com|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.claro|| ' </td>'||
       '<TD align="left" > '||a.com_claro|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.movistar|| ' </td>'||
       '<TD align="left" > '||a.com_movistar|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tar_local|| ' </td>'||
       '<TD align="left" > '||a.com_fijas|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.inpool|| ' </td>'||
       '<TD align="left" > '||a.com_inpool|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.fact_776|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_776|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

/*      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_771|| ' </td>'||
       '<TD align="left" > '||a.CLARO_ONNET_VPN_771|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);*/
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_777|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_777|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_778|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_778|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_779|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_779|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_780|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_780|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.FACT_781|| ' </td>'||
       '<TD align="left" > '||a.claro_vpn_781|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        
       Lv_Linea_e := '<TD align="left" > '||a.alegro|| ' </td>'||
                     '<TD align="left" > '||a.com_alegro|| ' </td>'||
                     '<TD align="left" > '||a.recurso|| ' </td>'||
                     '<TD align="left" > '||a.tipo|| ' </td>'||
                    '</tr>';
      /*Lv_Linea_e := '<tr>'||
                      '</td>'||a.id_plan|| '</td>'||
                      '</td>'||a.id_tn3|| '</td>'||
                      '</td>'||a.tariff_plan_id|| '</td>'||
                      '</td>'||a.descripcion|| '</td>'||
                      '</td>'||a.fecha_creacion|| '</td>'||
                      '</td>'||a.cant_cuentas|| '</td>'||
                      '</td>'||a.cant_clientes|| '</td>'||
                      '</td>'||a.estado|| '</td>'||
                      '</td>'||a.toll|| '</td>'||
                      '</td>'||a.toll_com|| '</td>'||
                      '</td>'||a.claro|| '</td>'||
                      '</td>'||a.com_claro|| '</td>'||
                      '</td>'||a.movistar|| '</td>'||
                      '</td>'||a.com_movistar|| '</td>'||
                      '</td>'||a.tar_local|| '</td>'||
                      '</td>'||a.com_fijas|| '</td>'||
                      '</td>'||a.inpool|| '</td>'||
                      '</td>'||a.com_inpool|| '</td>'||
                      '</td>'||a.fact_776|| '</td>'||
                      '</td>'||a.claro_vpn_776|| '</td>'||
                      '</td>'||a.FACT_771|| '</td>'||
                      '</td>'||a.CLARO_ONNET_VPN_771|| '</td>'||
                      '</td>'||a.FACT_777|| '</td>'||
                      '</td>'||a.claro_vpn_777|| '</td>'||
                      '</td>'||a.FACT_778|| '</td>'||
                      '</td>'||a.CLARO_VPN_778|| '</td>'||
                      '</td>'||a.FACT_779|| '</td>'||
                      '</td>'||a.CLARO_VPN_779|| '</td>'||
                      '</td>'||a.FACT_780|| '</td>'||
                      '</td>'||a.CLARO_VPN_780|| '</td>'||
                      '</td>'||a.FACT_781|| '</td>'||
                      '</td>'||a.CLARO_VPN_781|| '</td>'||
                      '</td>'||a.alegro|| '</td>'||
                      '</td>'||a.com_alegro|| '</td>'||
                      '</td>'||a.tipo|| '</td>'||
                    '</tr>';*/
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
  --    exit;
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo7:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_AUT_PURO_VPN;
  
  PROCEDURE PR_REPORTE_BGH(PV_EXISTE_REP OUT VARCHAR2,PV_ERROR OUT VARCHAR2) IS

    --Obtiene las tarifas bulk
    cursor c_tarifas_bulk_aut_puro is
      select a.id_plan,
             a.tmcode,
             a.descripcion,
             a.fecha_creacion,
             a.cant_cuentas,
             a.cant_clientes,
             a.estado,
             a.toll,
             a.toll_com,
             decode(a.com_claro - a.bgh_porta_porta, 0,'-',a.bgh_porta_porta) as claro,
             decode(a.com_claro - a.bgh_porta_porta, 0,'-',a.com_claro) as com_claro,
             decode(a.com_movistar - a.bgh_porta_movistar, 0,'-',a.bgh_porta_movistar) as movistar,--a.movistar,
             decode(a.com_movistar - a.bgh_porta_movistar, 0,'-',a.com_movistar) as com_movistar,
             decode(a.com_fijas - a.bgh_porta_fijas, 0,'-',a.bgh_porta_fijas) as tar_local,
             decode(a.com_fijas - a.bgh_porta_fijas, 0,'-',a.com_fijas) as com_fijas,
             decode(a.com_alegro - a.bgh_porta_alegro,0,'-',bgh_porta_alegro) as alegro,--a.alegro,
             decode(a.com_alegro - a.bgh_porta_alegro,0,'-',com_alegro) as com_alegro,
             a.categoria_amx, 
             a.clase_amx, 
             a.tipo_cliente, 
             a.tipo_plan, 
             a.segmento, 
             a.plan_suptel,
             a.recurso,
             tipo
        from GSI_TARIFAS_BSCS_TMP a
       where estado = 'A'
         --and (tipo = 'BULK' or tipo = 'AUT-PURO')
         --and id_plan in (select a.id_plan from Gsi_Tarifas_Tn3_FINAL a where "1700_VPN_772"=0) --BULK
         and (
                (a.com_claro - a.bgh_porta_porta)  <> 0 or
                (a.com_movistar - a.bgh_porta_movistar )  <> 0 or
                (a.com_alegro - a.bgh_porta_alegro )  <> 0 or
                (a.com_fijas - a.bgh_porta_fijas )  <> 0 
              );
     --DESA
     LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';--DIRECTORIO DONDE SE ENCUENTRA LA RUTA
     --PROD
     --LV_DIRECTORIO       Varchar2(200) := 'REPORTES_QC_DIR';
     Lv_Nombre_Archivo   Varchar2(100) := 'Reporte_Tarifas_BGH';
     Lv_Archivo          Varchar2(100);
     Lv_Nombre_Archivo2  Varchar2(100);
     Lv_Linea_e          clob;
     ln_existe           number:=0;
     Wfout1 Sys.Utl_File.File_Type;
     Wfout2 Sys.Utl_File.File_Type;
     
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
    Lv_Nombre_Archivo := Lv_Nombre_Archivo ||'_'|| to_char(sysdate,'dd_mm_yyyy');
    Lv_Archivo        := Lv_Nombre_Archivo || '.xls';
    Lv_Nombre_Archivo2 := Lv_Nombre_Archivo ||'_'|| to_char(sysdate-1,'dd_mm_yyyy')||'.xls';
    -- Abre el archivo de ayer
    If Utl_File.Is_Open(Wfout2) Then
      Sys.Utl_File.Fclose(Wfout2);
    End If;
    
    Wfout2 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Nombre_Archivo2, 'W');
    Sys.Utl_File.Fclose(Wfout2);
    Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Nombre_Archivo2);
    
    -- Abre el archivo
    If Utl_File.Is_Open(Wfout1) Then
      Sys.Utl_File.Fclose(Wfout1);
    End If;
    
    Wfout1 := Sys.Utl_File.Fopen(LV_DIRECTORIO, Lv_Archivo, 'W');


    Lv_Linea_e   := '<html><head><title>Reporte deteccion casos BSCS</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

    Lv_Linea_e := '<style>
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                </style></head>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e := '<BODY><table border="1"><tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ID_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TMCODE </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=   
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> DESCRIPCION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> FECHA_CREACION </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CUENTAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CANT_CLIENTES </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=     
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> ESTADO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TOLL_COM </td>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> BGH_CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_CLARO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> BGH_MOVISTAR </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_MOVISTAR </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> BGH_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_FIJAS </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> BGH_ALEGRO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> COM_ALEGRO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CATEGORIA_AMX </td>'||    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> CLASE_AMX </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_CLIENTE </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=    
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO_PLAN </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> SEGMENTO </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> PLAN_SUPTEL </td>'||
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> RECURSO </td>';
   Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
   Lv_Linea_e :=
       '<TD align="left" style="font-family:Verdana, Arial; font-size:10pt; background-color:#C81414; color:#FFFFFF" ><B> TIPO </td>'||
    '</tr>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    
    for a in c_tarifas_bulk_aut_puro loop
      ln_existe:=ln_existe+1;
      Lv_Linea_e :=
       '<tr>'||
       '<TD align="left" > '||a.id_plan|| ' </td>';
       
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tmcode|| ' </td>'||
       '<TD align="left" > '||a.descripcion|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.fecha_creacion|| ' </td>'||
       '<TD align="left" > '||a.cant_cuentas|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.cant_clientes|| ' </td>'||
       '<TD align="left" > '||a.estado|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.toll|| ' </td>'||
       '<TD align="left" > '||a.toll_com|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.claro|| ' </td>'||
       '<TD align="left" > '||a.com_claro|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.movistar|| ' </td>'||
       '<TD align="left" > '||a.com_movistar|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.tar_local|| ' </td>'||
       '<TD align="left" > '||a.com_fijas|| ' </td>'||
       '<TD align="left" > '||a.alegro|| ' </td>'||
       '<TD align="left" > '||a.com_alegro|| '</td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.CATEGORIA_AMX|| ' </td>'||
       '<TD align="left" > '||a.CLASE_AMX|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
      
      Lv_Linea_e :=
       '<TD align="left" > '||a.TIPO_CLIENTE|| ' </td>'||
       '<TD align="left" > '||a.TIPO_PLAN|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);

      Lv_Linea_e :=
       '<TD align="left" > '||a.SEGMENTO|| ' </td>'||
       '<TD align="left" > '||a.PLAN_SUPTEL|| ' </td>'||
       '<TD align="left" > '||a.recurso|| ' </td>';
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
        
      Lv_Linea_e := '<TD align="left" > '||a.tipo|| ' </td>'||
                    '</tr>';
      --esta parte revisa las variables que vas a mostrar en el excel
      Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
  --    exit;
      Lv_Linea_e := Null;
      -------------------------------------------------------------
    end loop;
    
    Lv_Linea_e:= '</table></body></html>';
    Sys.Utl_File.Put_Line(Wfout1, Lv_Linea_e);
    -- CIERRA EL ARCHIVO
    -------------------------------------------------------------
    Sys.Utl_File.Fclose(Wfout1);
    -------------------------------------------------------------
    
    if ln_existe > 0 then
      PV_EXISTE_REP:='SI';
      gv_archivo8:=Lv_Archivo;
    else
      PV_EXISTE_REP:='NO';
      Sys.UTL_FILE.FREMOVE(LV_DIRECTORIO,Lv_Archivo);
    end if;
    
  EXCEPTION
    WHEN OTHERS THEN
        Sys.Utl_File.Fclose(Wfout1);
        PV_ERROR:=SQLERRM;
  END PR_REPORTE_BGH;
  
  PROCEDURE PR_GENERA_REPORTES_XLS(PV_ERROR OUT VARCHAR2) IS

    lv_error   varchar2(1000);
    lv_adjunta varchar2(2);
    
  BEGIN
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_fact(pv_existe_rep => lv_adjunta,
                                                  pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_ldi(pv_existe_rep => lv_adjunta,
                                                 pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_ldi_bscs_colec(pv_existe_rep => lv_adjunta,
                                                            pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_plan_par_bscs_colec(pv_existe_rep => lv_adjunta,
                                                                 pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_com(pv_existe_rep => lv_adjunta,
                                                 pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.PR_REPORTE_AUT_PURO_BULK(pv_existe_rep => lv_adjunta,
                                                           pv_error      => lv_error);
                                                           
   pck_gsi_qc_revisa_tarifas_bscs.PR_REPORTE_AUT_PURO_VPN(pv_existe_rep => lv_adjunta,
                                                           pv_error      => lv_error);
   
   pck_gsi_qc_revisa_tarifas_bscs.pr_reporte_bgh(pv_existe_rep => lv_adjunta,
                                                 pv_error      => lv_error);
   
   
  EXCEPTION
    WHEN OTHERS THEN
        PV_ERROR:=SQLERRM;
  END PR_GENERA_REPORTES_XLS;
Procedure PR_Valida_Email(Pv_Email In Varchar2, Pv_Error Out Varchar2) Is

    Lv_Email Varchar2(50);
    Lv_Error Varchar2(2000);
    Le_Error Exception;

  Begin

    Lv_Email := Pv_Email;

    --VALIDO QUE NO SEA NULO
    If Lv_Email Is Null Then
      Lv_Error := 'EL EMAIL NO DEBE SER NULO';
      Raise Le_Error;
    End If;

    --VALIDO QUE POR LO MENOS TENGA 7 CARACTERES: X@Z.COM
    If Length(Lv_Email) < 7 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL NO DEBE SER MENOR A 7 CARACTERES';
      Raise Le_Error;
    End If;

    --VALIDO QUE NO TENGA MAS DE 50 CARACTERES
    If Length(Lv_Email) > 50 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL: ' || Lv_Email ||
                  ' NO DEBE SER MAS DE 50 CARACTERES';
      Raise Le_Error;
    End If;

    --VERIFICO QUE TENGA EL SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, NO TIENE EL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE NO TENGAS MAS DE DOS SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 2) > 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, TIENE MAS DE DOS @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN CARACTER ANTES DEL @
    If Instr(Lv_Email, '@', 1, 1) <= 1 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER ANTES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

  Exception
    When Le_Error Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Lv_Error;
    When Others Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Sqlerrm;
  End PR_Valida_Email;
  
  Procedure PR_ENVIA_MAIL_CC_FILES_HTML(Pv_Ip_Servidor  In Varchar2,
                                        Pv_Sender       In Varchar2,
                                        Pv_Recipient    In Varchar2,
                                        Pv_Ccrecipient  In Varchar2,
                                        Pv_Subject      In Varchar2,
                                        Pv_Message      In clob,
                                        Pv_Max_Size     In Varchar2 Default 9999999999,
                                        Pv_Archivo1     In Varchar2 Default Null,
                                        Pv_Archivo2     In Varchar2 Default Null,
                                        Pv_Archivo3     In Varchar2 Default Null,
                                        Pv_Archivo4     In Varchar2 Default Null,
                                        Pv_Archivo5     In Varchar2 Default Null,
                                        Pv_Archivo6     In Varchar2 Default Null,
                                        Pv_Archivo7     In Varchar2 Default Null,
                                        Pv_Archivo8     In Varchar2 Default Null,
                                        Pb_Blnresultado In Out Boolean,
                                        Pv_Error        Out Varchar2) Is

    Crlf        Varchar2(2) := Utl_Tcp.Crlf;
    Lv_Conexion Utl_Smtp.Connection;
    Mailhost    Varchar2(30) := Pv_Ip_Servidor;
    Header      Clob := Empty_Clob;
    Lv_Programa Varchar2(30) := 'WFK_CORREO';
    Lv_Error    Varchar2(1000);
    Le_Error Exception;

    --descomposicion de trama
    Ln_Tamano              Number;
    Lv_Cadena_Cc           Varchar2(2000) := '';
    Lv_Cadena_To           Varchar2(2000) := '';
    Ln_Index_Tabla_Detalle Number;
    Lv_Valor               Varchar2(50);

    Type Varchar2_Table Is Table Of Varchar2(200) Index By Binary_Integer;
    Arreglo_Archivos     Varchar2_Table;
    Mesg_Len             Number;
    Mesg_Length_Exceeded Boolean := False;
    v_Directory_Name     Varchar2(100);
    v_File_Name          Varchar2(100);
    v_Line               Varchar2(1000);
    Mesg                 Varchar2(32767);
    i                    Binary_Integer;
    v_File_Handle        Sys.Utl_File.File_Type;
    v_Slash_Pos          Number;
    Mesg_Too_Long Exception;

  Begin
    If Pv_Ip_Servidor Is Null Then
      Lv_Error        := 'No se ha ingresado la ip del servidor mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Sender Is Null Then
      Lv_Error        := 'No se ha ingresado el correo de la persona que envia el mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Recipient Is Null Then
      Lv_Error        := 'No se ha ingresado los destinatarios.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Subject Is Null Then
      Lv_Error        := 'No se ha ingresado el asunto del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Message Is Null Then
      Lv_Error        := 'No se ha ingresado contenido del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Else
      -- Obtengo la conexion
      Lv_Conexion := Utl_Smtp.Open_Connection(Mailhost, 25);

      -- first load the three filenames into an array for easier handling later ...
      Arreglo_Archivos(1) := Pv_Archivo1;
      Arreglo_Archivos(2) := Pv_Archivo2;
      Arreglo_Archivos(3) := Pv_Archivo3;
      Arreglo_Archivos(4) := Pv_Archivo4;
      Arreglo_Archivos(5) := Pv_Archivo5;
      Arreglo_Archivos(6) := Pv_Archivo6;
      Arreglo_Archivos(7) := Pv_Archivo7;
      Arreglo_Archivos(8) := Pv_Archivo8;
      -- envio los parametros de Host y Sender
      Utl_Smtp.Helo(Lv_Conexion, Mailhost);
      Utl_Smtp.Mail(Lv_Conexion, Pv_Sender);

      --descompongo la trama de destinatarios
      --==============================================================================
      Lv_Cadena_To           := Pv_Recipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_To Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_To,
                                           1,
                                           Instr(Lv_Cadena_To, ',') - 1);
          Lv_Cadena_To           := Substr(Lv_Cadena_To,
                                           Instr(Lv_Cadena_To, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            PR_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_To);
          If Lv_Cadena_To Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --descompongo la trama de destinatarios con copia
      --==============================================================================
      Lv_Cadena_Cc           := Pv_Ccrecipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_Cc Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_Cc,
                                           1,
                                           Instr(Lv_Cadena_Cc, ',') - 1);
          Lv_Cadena_Cc           := Substr(Lv_Cadena_Cc,
                                           Instr(Lv_Cadena_Cc, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            PR_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_Cc);
          If Lv_Cadena_Cc Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --abro la conexion para comenzar a adjuntar data
      Utl_Smtp.Open_Data(Lv_Conexion);

      --Defino la cabecera del mail
      Header := 'Date: ' || To_Char(Sysdate, 'dd Mon yy hh24:mi:ss') || Crlf ||
                'From: ' || Pv_Sender || '' || Crlf || 'Subject: ' ||
                Pv_Subject || Crlf || 'To: ' ||
                Substr(Pv_Recipient, 1, Length(Pv_Recipient) - 1) || Crlf;
      If Pv_Ccrecipient Is Not Null Then
        Header := Header || 'CC: ' ||
                  Substr(Pv_Ccrecipient, 1, Length(Pv_Ccrecipient) - 1) || Crlf;
      End If;
      Header := Header || 'Mime-Version: 1.0' || Crlf ||
                'Content-Type: multipart/mixed; boundary="DMW.Boundary.605592468"' || Crlf || '' || Crlf ||
                'This is a Mime message, which your current mail reader may not' || Crlf ||
                'understand. Parts of the message will appear as text. If the remainder' || Crlf ||
                'appears as random characters in the message body, instead of as' || Crlf ||
                'attachments, then you''ll have to extract these parts and decode them' || Crlf ||
                'manually.' || Crlf || '' || Crlf ||
                '--DMW.Boundary.605592468' || Crlf;

      --adjunto la mensaje
      Header := Header || 'MIME-Version: 1.0' || crlf ||
                'Content-type:text/html;charset=iso-8859-1' || crlf || '' || crlf ||
                Pv_Message;
      Utl_Smtp.Write_Data(Lv_Conexion, Header);

      Mesg     := Header;
      Mesg_Len := Length(Mesg);

      If Mesg_Len > Pv_Max_Size Then
        Mesg_Length_Exceeded := True;
      End If;

      -- Append the files ...
      For i In 1 .. 8 Loop

        -- Sale si la longitud del mensaje se excede
        Exit When Mesg_Length_Exceeded;

        -- Si el nombre de archivo ha sido dado y no es nulo
        If Arreglo_Archivos(i) Is Not Null Then
          Begin
            -- Ubica el ultimo '/' o '\' en la ruta dada ...
            v_Slash_Pos := Instr(Arreglo_Archivos(i), '/', -1);
            If v_Slash_Pos = 0 Then
              v_Slash_Pos := Instr(Arreglo_Archivos(i), '\', -1);
            End If;

            -- Separa el nombre del archivo con el nombre del directorio
            v_Directory_Name := Substr(Arreglo_Archivos(i),
                                       1,
                                       v_Slash_Pos - 1);
            v_File_Name      := Substr(Arreglo_Archivos(i), v_Slash_Pos + 1);

            -- Abro el Archivo ...
            v_File_Handle := Sys.Utl_File.Fopen(v_Directory_Name,
                                                v_File_Name,
                                                'r');

            -- Generlo la linea de MIME ...
            Mesg     := Crlf || '--DMW.Boundary.605592468' || Crlf ||
                        'Content-Type: application/octet-stream; name="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Disposition: attachment; filename="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Transfer-Encoding: 7bit' || Crlf || Crlf;
            Mesg_Len := Mesg_Len + Length(Mesg);
            Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

            -- Adjunto el contenido del mensaje al final del Archivo
            Loop
              Sys.Utl_File.Get_Line(v_File_Handle, v_Line);
              If Mesg_Len + Length(v_Line) > Pv_Max_Size Then
                Mesg := '*** Truncado ***' || Crlf;
                Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
                Mesg_Length_Exceeded := True;
                Raise Mesg_Too_Long;
              End If;
              Mesg := v_Line || Crlf;
              Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
              Mesg_Len := Mesg_Len + Length(Mesg);
            End Loop;
          Exception
            When Sys.Utl_File.Invalid_Path Then
              Lv_Error := 'Error al abrir el adjunto: ' ||
                          Arreglo_Archivos(i);
              Raise Le_Error;
            When Others Then
              Null;
          End;
          Mesg := Crlf;
          Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
          --utl_smtp.data(Lv_Conexion, mesg);

          -- close the file ...
          Sys.Utl_File.Fclose(v_File_Handle);
        End If;
      End Loop;
      --===========================================================================================

      -- Adjunto la linea Final
      Mesg := Crlf || '--DMW.Boundary.605592468--' || Crlf;
      Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

      Utl_Smtp.Close_Data(Lv_Conexion);
      Pb_Blnresultado := True;
      Utl_Smtp.Quit(Lv_Conexion);

    End If;

  Exception
    When Utl_Smtp.Invalid_Operation Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Operacion Invalida en Transaccion SMTP ';
      Pb_Blnresultado := False;
    When Utl_Smtp.Transient_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Problemas Temporales enviando el mail - Intente despues. ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
    When Utl_Smtp.Permanent_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Errores en Codigo para Transaccion SMTP.';
      Pb_Blnresultado := False;
    When Le_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Lv_Error;
      Pb_Blnresultado := False;
    When Others Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
  End PR_ENVIA_MAIL_CC_FILES_HTML;

PROCEDURE PR_ENVIA_MAIL(PV_ERROR OUT VARCHAR2) IS

  CURSOR C_PARAMETROS(PV_PARAMETRO VARCHAR2)IS
   SELECT A.VALOR 
     FROM GSI_QC_CONFIG_TARIFAS_BSCS A 
    where a.id_parametro=PV_PARAMETRO;

  LC_PARAMETROS        C_PARAMETROS%ROWTYPE;
  lv_remitente         varchar2(500) :='reporte_tarifas@claro.com.ec';
  --DIRECTORIO DONDE SE ENCUENTRA LA RUTA
  --Lv_directorio        Varchar2(200) := 'PLANES_QC_DIR';--DESA
  Lv_directorio    Varchar2(200) := 'REPORTES_QC_DIR';--PROD
  lv_Asunto            varchar2(100);
  lv_para              varchar2(4000);
  lv_copia             varchar2(4000);
  LV_MENSAJE           clob;
  lb_blnresultado      boolean;
  lv_error             varchar2(1000);
  
BEGIN
  --GENERO LOS REPORTES XLS
  pck_gsi_qc_revisa_tarifas_bscs.pr_genera_reportes_xls(pv_error => lv_error);
  
  lv_asunto := ':: Reporte de Tarifas BSCS ::';
  LV_MENSAJE := '<br>Estimad@s,<br>Adjunto encontraran los reportes generados en BSCS.'|| CHR(10) || CHR(13);
  
  OPEN C_PARAMETROS('MAIL_PARA');
   FETCH C_PARAMETROS INTO LC_PARAMETROS;
  CLOSE C_PARAMETROS;
  lv_para:=LC_PARAMETROS.valor;
  
  OPEN C_PARAMETROS('MAIL_COPIA');
   FETCH C_PARAMETROS INTO LC_PARAMETROS;
  CLOSE C_PARAMETROS;
  lv_copia:=LC_PARAMETROS.valor;
  LV_MENSAJE := LV_MENSAJE ||  '<br><br>Saludos cordiales.'|| CHR(10) || CHR(13)|| CHR(13);
  LV_MENSAJE := LV_MENSAJE ||  '<br><br>------------------------'|| CHR(10) || CHR(13);
  LV_MENSAJE := LV_MENSAJE ||  '<br>:: Este mensaje e-mail fue enviado autom&aacute;ticamente ::';
  
  --ENVIO EL CORREO
  pck_gsi_qc_revisa_tarifas_bscs.pr_envia_mail_cc_files_html(
                                         pv_ip_servidor => '130.2.18.61',
                                         pv_sender => lv_remitente,
                                         pv_recipient => lv_para||',',
                                         pv_ccrecipient => lv_copia||',',
                                         pv_subject => lv_asunto,
                                         pv_message => LV_MENSAJE,
                                         pv_max_size => '',
                                         pv_archivo1 => Lv_directorio||'/'||gv_archivo1,
                                         pv_archivo2 => Lv_directorio||'/'||gv_archivo2,
                                         pv_archivo3 => Lv_directorio||'/'||gv_archivo3,
                                         pv_archivo4 => Lv_directorio||'/'||gv_archivo4,
                                         pv_archivo5 => Lv_directorio||'/'||gv_archivo5,
                                         pv_archivo6 => Lv_directorio||'/'||gv_archivo6,
                                         pv_archivo7 => Lv_directorio||'/'||gv_archivo7,
                                         pv_archivo8 => Lv_directorio||'/'||gv_archivo8,
                                         pb_blnresultado => lb_blnresultado,
                                         pv_error => LV_ERROR
                                                            );
  EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_TARIFAS_BSCS_TMP';
  commit;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:=SQLERRM;
    rollback;
END PR_ENVIA_MAIL;

PROCEDURE PR_INSERTA_BITACORA(PN_PROCESO   NUMBER,
                              PV_PROCESO   VARCHAR2,
                              PD_FECHA_INI DATE,
                              PD_FECHA_FIN DATE,
                              PV_OBSERVACION VARCHAR2 DEFAULT NULL) IS
  
BEGIN
  insert into GSI_BITACORA_TARIFAS
  (ID_PROCESO,PROCESO,fecha_inicio,fecha_fin,OBSERVACION)
  values (PN_PROCESO,PV_PROCESO,PD_FECHA_INI,PD_FECHA_FIN,PV_OBSERVACION);
commit;
END PR_INSERTA_BITACORA;

--Genero la tabla historica por versiones y la final
PROCEDURE PR_GENERA_HISTORICA(PV_TABLA_NEW VARCHAR2,
                              PV_TABLA_ANT VARCHAR2,
                              PV_ERROR OUT VARCHAR2) IS
  
  CURSOR C_VERIFICA_VERSION_INI IS
   SELECT count(*) from GSI_TARIFAS_BSCS_HISTORICA;
  
  CURSOR C_OBT_ULT_VERSION(PV_ID_PLAN VARCHAR2)IS
   SELECT nvl(MAX(A.NO_VERSION),0) + 1 VERSION  FROM GSI_TARIFAS_BSCS_HISTORICA A
    WHERE A.ID_PLAN = PV_ID_PLAN;
  
  LC_OBT_ULT_VERSION C_OBT_ULT_VERSION%ROWTYPE;
  ln_tablaExiste number;
  ln_existe_vers_ini number;
  ln_cont number:=0;
  ld_fecha_version_ant date;
  ld_fecha_version_new date;
  lv_fecha_version_ant varchar2(10);
  lv_fecha_version_new varchar2(10);
  lc_cambios_tarifas sys_refcursor;
  ll_cambios_tarifas long;
  ll_datos_tarifas long;
  lc_datos_tarifas sys_refcursor;
  ln_id_promocion number;
  lv_desc_promocion varchar2(300);
  ---VARIABLES DE LA TABLA DE TARIFAS
  lv_tar_id_plan  varchar2(60);
  lv_tar_descripcion  varchar2(60);
  lv_tar_tmcode  integer;
  lv_tar_1700  number;
  lv_tar_1800  number;
  lv_tar_claro  number;
  lv_tar_claro_inpool  number;
  lv_tar_claro_776  number;
  lv_tar_claro_777  number;
  lv_tar_claro_778  number;
  lv_tar_claro_779  number;
  lv_tar_claro_780  number;
  lv_tar_claro_781  number;
  lv_tar_movistar  number;
  lv_tar_alegro  number;
  lv_tar_fija  number;
  lv_tar_chile  number;
  lv_tar_cuba  number;
  lv_tar_espana  number;
  lv_tar_italia  number;
  lv_tar_canada  number;
  lv_tar_usa  number;
  lv_tar_europa  number;
  lv_tar_japon  number;
  lv_tar_maritima  number;
  lv_tar_mexico  number;
  lv_tar_mexico_cel  number;
  lv_tar_pacto_andino  number;
  lv_tar_restoamerica  number;
  lv_tar_restodelmundo  number;
  lv_tar_zona_especial  number;
  lv_tar_alemania  number;
  lv_tar_argentina  number;
  lv_tar_bolivia  number;
  lv_tar_brasil  number;
  lv_tar_china  number;
  lv_tar_colombia  number;
  lv_tar_costa_rica  number;
  lv_tar_francia  number;
  lv_tar_com_771  number;
  lv_tar_com_776  number;
  lv_tar_com_777  number;
  lv_tar_com_778  number;
  lv_tar_com_779  number;
  lv_tar_com_780  number;
  lv_tar_com_781  number;
  lv_tar_toll  varchar2(20);
  lv_tar_toll_com  varchar2(20);
  lv_tar_tipo  varchar2(20);
  lv_tar_cant_cuentas  number;
  lv_tar_cant_clientes  number;
  lv_tar_aplica_descuento  varchar2(60);
  lv_tar_fecha_creacion  date;
  lv_tar_recurso  varchar2(100);
  lv_tar_id_plan_pareja  varchar2(10);
  lv_tar_estado  varchar2(1);
  lv_tar_com_inpool  number;
  lv_tar_com_claro  number;
  lv_tar_com_movistar  number;
  lv_tar_com_alegro  number;
  lv_tar_com_fijas  number;
  lv_tar_categoria_amx  varchar2(20);
  lv_tar_clase_amx  varchar2(20);
  lv_tar_tipo_cliente  varchar2(40);
  lv_tar_tipo_plan  varchar2(40);
  lv_tar_segmento  varchar2(40);
  lv_tar_plan_suptel  varchar2(40);
  lv_bgh_porta_porta VARCHAR2(100);
  lv_bgh_porta_fijas VARCHAR2(100);
  lv_bgh_porta_movistar VARCHAR2(100);
  lv_bgh_porta_alegro VARCHAR2(100);
  lv_TARIFA_BASICA	VARCHAR2(15);
  lv_FEATURES_FIJOS	VARCHAR2(1000);
  lv_VPN_APROVISIONADOR	VARCHAR2(1000);
  lv_VPN_APROVISIONADOR_TIPO	VARCHAR2(1000);
  lv_VPN_HOLDING	VARCHAR2(1000);
  lv_VPN_HOLDING_TIPO	VARCHAR2(1000);
  lv_VPN_HOLDING_ILIM	VARCHAR2(1000);
  lv_VPN_HOLDING_ILIM_TIPO	VARCHAR2(1000);
  ln_ID_DETALLE_PLAN	NUMBER;
  lv_ID_SUBPRODUCTO	VARCHAR2(30);
  lv_FU_PACK_ID	VARCHAR2(1500);

  LL_CAMPOS_COMPARAR LONG:=
                  ' id_plan, '||
                  'descripcion, '||
                  'tmcode, '||
                  '"1700", '||
                  '"1800", '||
                  'claro, '||
                  'claro_inpool, '||
                  'claro_776, '||
                  'claro_777, '||
                  'claro_778, '||
                  'claro_779, '||
                  'claro_780, '||
                  'claro_781, '||
                  'movistar, '||
                  'alegro, '||
                  'fija, '||
                  'chile, '||
                  'cuba, '||
                  'espana, '||
                  'italia, '||
                  'canada, '||
                  'usa, '||
                  'europa, '||
                  'japon, '||
                  'maritima, '||
                  'mexico, '||
                  'mexico_cel, '||
                  'pacto_andino, '||
                  'restoamerica, '||
                  'restodelmundo, '||
                  'zona_especial, '||
                  'alemania, '||
                  'argentina, '||
                  'bolivia, '||
                  'brasil, '||
                  'china, '||
                  'colombia, '||
                  'costa_rica, '||
                  'francia, '||
                  'com_771, '||
                  'com_776, '||
                  'com_777, '||
                  'com_778, '||
                  'com_779, '||
                  'com_780, '||
                  'com_781, '||
                  'toll, '||
                  'toll_com, '||
                  'tipo, '||
                  'fecha_creacion, '||
                  'recurso, '||
                  'id_plan_pareja, '||
                  'estado, '||
                  'com_inpool, '||
                  'com_claro, '||
                  'com_movistar, '||
                  'com_alegro, '||
                  'com_fijas, '||
                  'categoria_amx, '||
                  'clase_amx, '||
                  'tipo_cliente, '||
                  'tipo_plan, '||
                  'segmento, '||
                  'plan_suptel ,'||
                  'bgh_porta_porta,'||
                  'bgh_porta_fijas,'||
                  'bgh_porta_movistar,'||
                  'bgh_porta_alegro,'||
                  'TARIFA_BASICA,'||
                  'FEATURES_FIJOS,'||
                  'VPN_APROVISIONADOR,'||
                  'VPN_APROVISIONADOR_TIPO,'||
                  'VPN_HOLDING,'||
                  'VPN_HOLDING_TIPO,'||
                  'VPN_HOLDING_ILIM,'||
                  'VPN_HOLDING_ILIM_TIPO,'||
                  'ID_DETALLE_PLAN,'||
                  'ID_SUBPRODUCTO,'||
                  'FU_PACK_ID,'||
                  'id_promocion,'||
                  'DESC_PROMOCION';
BEGIN
    lv_fecha_version_ant:=REPLACE(upper(PV_TABLA_ANT),'GSI_TARIFAS_BSCS_','');
    lv_fecha_version_new:=REPLACE(upper(PV_TABLA_NEW),'GSI_TARIFAS_BSCS_','');
    
    ld_fecha_version_ant :=to_date(substr(lv_fecha_version_ant,7,2)||'/'||
                           substr(lv_fecha_version_ant,5,2)||'/'||
                           substr(lv_fecha_version_ant,0,4),'dd/mm/yyyy');
    
    ld_fecha_version_new :=to_date(substr(lv_fecha_version_new,7,2)||'/'||
                           substr(lv_fecha_version_new,5,2)||'/'||
                           substr(lv_fecha_version_new,0,4),'dd/mm/yyyy');
    
    --VERIFICO SI LA TABLA DE AYER EXISTE
    --execute immediate 'SELECT COUNT(*) FROM all_tables WHERE table_name= :tabla' into ln_tablaExiste using replace(PV_TABLA_ANT,'sms.','');

    OPEN C_VERIFICA_VERSION_INI;
     FETCH C_VERIFICA_VERSION_INI INTO ln_existe_vers_ini;
    CLOSE C_VERIFICA_VERSION_INI;
    
    IF ln_existe_vers_ini = 0 THEN--Verifico si hay una version inicial insertada sino es asi inserto todo lo de hoy
      ll_cambios_tarifas:='insert into GSI_TARIFAS_BSCS_HISTORICA (fecha_version,no_version, '||LL_CAMPOS_COMPARAR||
      ' ) select to_date('''||to_char(ld_fecha_version_new,'dd/mm/yyyy')||''',''dd/mm/yyyy''), 1, '||LL_CAMPOS_COMPARAR|| 
      ' from '||PV_TABLA_NEW;

      execute immediate ll_cambios_tarifas;

      ll_cambios_tarifas:='insert into GSI_TARIFAS_BSCS_FINAL (fecha_version, no_version, '||LL_CAMPOS_COMPARAR||
      ' ) select to_date('''||to_char(ld_fecha_version_new,'dd/mm/yyyy')||''',''dd/mm/yyyy''), 1,'||LL_CAMPOS_COMPARAR||
      ' from '||PV_TABLA_NEW;
      execute immediate ll_cambios_tarifas;
    
    ELSE --SI EXISTE LA VERSION INICIAL POR LO QUE SE PROCEDE A REALIZAR UNA COMPARACION DE AYER Y HOY EN LAS TARIFAS

      ll_cambios_tarifas:='select '||LL_CAMPOS_COMPARAR||' from '||PV_TABLA_NEW||
                               ' minus '||
                          'select '||LL_CAMPOS_COMPARAR||' from gsi_tarifas_bscs_final ';--||PV_TABLA_ANT;
                          
      OPEN lc_cambios_tarifas for ll_cambios_tarifas;
      loop
          lv_tar_id_plan:=null;
          lv_tar_descripcion:=null;
          lv_tar_tmcode:=null;
          lv_tar_1700:=null;
          lv_tar_1800:=null;
          lv_tar_claro:=null;
          lv_tar_claro_inpool:=null;
          lv_tar_claro_776:=null;
          lv_tar_claro_777:=null;
          lv_tar_claro_778:=null;
          lv_tar_claro_779:=null;
          lv_tar_claro_780:=null;
          lv_tar_claro_781:=null;
          lv_tar_movistar:=null;
          lv_tar_alegro:=null;
          lv_tar_fija:=null;
          lv_tar_chile:=null;
          lv_tar_cuba:=null;
          lv_tar_espana:=null;
          lv_tar_italia:=null;
          lv_tar_canada:=null;
          lv_tar_usa:=null;
          lv_tar_europa:=null;
          lv_tar_japon:=null;
          lv_tar_maritima:=null;
          lv_tar_mexico:=null;
          lv_tar_mexico_cel:=null;
          lv_tar_pacto_andino:=null;
          lv_tar_restoamerica:=null;
          lv_tar_restodelmundo:=null;
          lv_tar_zona_especial:=null;
          lv_tar_alemania:=null;
          lv_tar_argentina:=null;
          lv_tar_bolivia:=null;
          lv_tar_brasil:=null;
          lv_tar_china:=null;
          lv_tar_colombia:=null;
          lv_tar_costa_rica:=null;
          lv_tar_francia:=null;
          lv_tar_com_771:=null;
          lv_tar_com_776:=null;
          lv_tar_com_777:=null;
          lv_tar_com_778:=null;
          lv_tar_com_779:=null;
          lv_tar_com_780:=null;
          lv_tar_com_781:=null;
          lv_tar_toll:=null;
          lv_tar_toll_com:=null;
          lv_tar_tipo:=null;
          lv_tar_cant_cuentas:=null;
          lv_tar_cant_clientes:=null;
          lv_tar_aplica_descuento:=null;
          lv_tar_fecha_creacion:=null;
          lv_tar_recurso:=null;
          lv_tar_id_plan_pareja:=null;
          lv_tar_estado:=null;
          lv_tar_com_inpool:=null;
          lv_tar_com_claro:=null;
          lv_tar_com_movistar:=null;
          lv_tar_com_alegro:=null;
          lv_tar_com_fijas:=null;
          lv_tar_categoria_amx:=null;
          lv_tar_clase_amx:=null;
          lv_tar_tipo_cliente:=null;
          lv_tar_tipo_plan:=null;
          lv_tar_segmento:=null;
          lv_tar_plan_suptel:=null;
          lv_bgh_porta_porta:=null;
          lv_bgh_porta_fijas:=null;
          lv_bgh_porta_movistar:=null;
          lv_bgh_porta_alegro:=null;
          lv_TARIFA_BASICA:=null;
          lv_FEATURES_FIJOS:=null;
          lv_VPN_APROVISIONADOR:=null;
          lv_VPN_APROVISIONADOR_TIPO:=null;
          lv_VPN_HOLDING:=null;
          lv_VPN_HOLDING_TIPO:=null;
          lv_VPN_HOLDING_ILIM:=null;
          lv_VPN_HOLDING_ILIM_TIPO:=null;
          ln_ID_DETALLE_PLAN:=null;
          lv_ID_SUBPRODUCTO:=null;
          lv_FU_PACK_ID:=null;
          ln_id_promocion:=null;
          lv_desc_promocion:=null;
          
          ln_cont:=ln_cont+1;
          
         FETCH lc_cambios_tarifas into 
          lv_tar_id_plan,
          lv_tar_descripcion,
          lv_tar_tmcode,
          lv_tar_1700,
          lv_tar_1800,
          lv_tar_claro,
          lv_tar_claro_inpool,
          lv_tar_claro_776,
          lv_tar_claro_777,
          lv_tar_claro_778,
          lv_tar_claro_779,
          lv_tar_claro_780,
          lv_tar_claro_781,
          lv_tar_movistar,
          lv_tar_alegro,
          lv_tar_fija,
          lv_tar_chile,
          lv_tar_cuba,
          lv_tar_espana,
          lv_tar_italia,
          lv_tar_canada,
          lv_tar_usa,
          lv_tar_europa,
          lv_tar_japon,
          lv_tar_maritima,
          lv_tar_mexico,
          lv_tar_mexico_cel,
          lv_tar_pacto_andino,
          lv_tar_restoamerica,
          lv_tar_restodelmundo,
          lv_tar_zona_especial,
          lv_tar_alemania,
          lv_tar_argentina,
          lv_tar_bolivia,
          lv_tar_brasil,
          lv_tar_china,
          lv_tar_colombia,
          lv_tar_costa_rica,
          lv_tar_francia,
          lv_tar_com_771, 
          lv_tar_com_776, 
          lv_tar_com_777, 
          lv_tar_com_778, 
          lv_tar_com_779, 
          lv_tar_com_780, 
          lv_tar_com_781, 
          lv_tar_toll, 
          lv_tar_toll_com, 
          lv_tar_tipo, 
          lv_tar_fecha_creacion, 
          lv_tar_recurso, 
          lv_tar_id_plan_pareja, 
          lv_tar_estado, 
          lv_tar_com_inpool, 
          lv_tar_com_claro, 
          lv_tar_com_movistar, 
          lv_tar_com_alegro, 
          lv_tar_com_fijas, 
          lv_tar_categoria_amx, 
          lv_tar_clase_amx, 
          lv_tar_tipo_cliente, 
          lv_tar_tipo_plan, 
          lv_tar_segmento, 
          lv_tar_plan_suptel,
          lv_bgh_porta_porta,
          lv_bgh_porta_fijas,
          lv_bgh_porta_movistar,
          lv_bgh_porta_alegro,
          lv_TARIFA_BASICA,
          lv_FEATURES_FIJOS,
          lv_VPN_APROVISIONADOR,
          lv_VPN_APROVISIONADOR_TIPO,
          lv_VPN_HOLDING,
          lv_VPN_HOLDING_TIPO,
          lv_VPN_HOLDING_ILIM,
          lv_VPN_HOLDING_ILIM_TIPO,
          ln_ID_DETALLE_PLAN,
          lv_ID_SUBPRODUCTO,
          lv_FU_PACK_ID,
          ln_id_promocion,
          lv_desc_promocion;
          
        exit when lv_tar_id_plan is null or lc_cambios_tarifas%notfound;
        
        OPEN C_OBT_ULT_VERSION(lv_tar_id_plan);
          FETCH C_OBT_ULT_VERSION INTO LC_OBT_ULT_VERSION;
        CLOSE C_OBT_ULT_VERSION;
         
        DELETE /*+ APPEND */ GSI_TARIFAS_BSCS_FINAL
         WHERE ID_PLAN = lv_tar_id_plan;
        
        execute immediate
        'insert into GSI_TARIFAS_BSCS_HISTORICA (fecha_version, no_version, '||LL_CAMPOS_COMPARAR||
        ' ) VALUES (:ld_fecha,:version,:lv_tar_id_plan, '||
                            ':lv_tar_descripcion, '||
                            ':lv_tar_tmcode, '||
                            ':lv_tar_1700, '||
                            ':lv_tar_1800, '||
                            ':lv_tar_claro, '||
                            ':lv_tar_claro_inpool, '||
                            ':lv_tar_claro_776, '||
                            ':lv_tar_claro_777, '||
                            ':lv_tar_claro_778, '||
                            ':lv_tar_claro_779, '||
                            ':lv_tar_claro_780, '||
                            ':lv_tar_claro_781, '||
                            ':lv_tar_movistar, '||
                            ':lv_tar_alegro, '||
                            ':lv_tar_fija, '||
                            ':lv_tar_chile, '||
                            ':lv_tar_cuba, '||
                            ':lv_tar_espana, '||
                            ':lv_tar_italia, '||
                            ':lv_tar_canada, '||
                            ':lv_tar_usa, '||
                            ':lv_tar_europa, '||
                            ':lv_tar_japon, '||
                            ':lv_tar_maritima, '||
                            ':lv_tar_mexico, '||
                            ':lv_tar_mexico_cel, '||
                            ':lv_tar_pacto_andino, '||
                            ':lv_tar_restoamerica, '||
                            ':lv_tar_restodelmundo, '||
                            ':lv_tar_zona_especial, '||
                            ':lv_tar_alemania, '||
                            ':lv_tar_argentina, '||
                            ':lv_tar_bolivia, '||
                            ':lv_tar_brasil, '||
                            ':lv_tar_china, '||
                            ':lv_tar_colombia, '||
                            ':lv_tar_costa_rica, '||
                            ':lv_tar_francia, '||
                            ':lv_tar_com_771, '||
                            ':lv_tar_com_776, '||
                            ':lv_tar_com_777, '||
                            ':lv_tar_com_778, '||
                            ':lv_tar_com_779, '||
                            ':lv_tar_com_780, '||
                            ':lv_tar_com_781, '||
                            ':lv_tar_toll, '||
                            ':lv_tar_toll_com, '||
                            ':lv_tar_tipo, '||
                            ':lv_tar_fecha_creacion, '||
                            ':lv_tar_recurso, '||
                            ':lv_tar_id_plan_pareja, '||
                            ':lv_tar_estado, '||
                            ':lv_tar_com_inpool, '||
                            ':lv_tar_com_claro, '||
                            ':lv_tar_com_movistar, '||
                            ':lv_tar_com_alegro, '||
                            ':lv_tar_com_fijas, '||
                            ':lv_tar_categoria_amx, '||
                            ':lv_tar_clase_amx, '||
                            ':lv_tar_tipo_cliente, '||
                            ':lv_tar_tipo_plan, '||
                            ':lv_tar_segmento, '||
                            ':lv_tar_plan_suptel,'||
                            ':lv_bgh_porta_porta,'||
                            ':lv_bgh_porta_fijas,'||
                            ':lv_bgh_porta_movistar,'||
                            ':lv_bgh_porta_alegro, '||
                            ':lv_TARIFA_BASICA, '||
                            ':lv_FEATURES_FIJOS, '||
                            ':lv_VPN_APROVISIONADOR, '||
                            ':lv_VPN_APROVISIONADOR_TIPO, '||
                            ':lv_VPN_HOLDING, '||
                            ':lv_VPN_HOLDING_TIPO, '||
                            ':lv_VPN_HOLDING_ILIM, '||
                            ':lv_VPN_HOLDING_ILIM_TIPO, '||
                            ':ln_ID_DETALLE_PLAN, '||
                            ':lv_ID_SUBPRODUCTO, '||
                            ':lv_FU_PACK_ID, '||
                            ':ln_id_promocion, '||
                            ':lv_desc_promocion '||
                             ' ) '
            using
              ld_fecha_version_new,
              LC_OBT_ULT_VERSION.VERSION,
              lv_tar_id_plan,
              lv_tar_descripcion,
              lv_tar_tmcode,
              lv_tar_1700,
              lv_tar_1800,
              lv_tar_claro,
              lv_tar_claro_inpool,
              lv_tar_claro_776,
              lv_tar_claro_777,
              lv_tar_claro_778,
              lv_tar_claro_779,
              lv_tar_claro_780,
              lv_tar_claro_781,
              lv_tar_movistar,
              lv_tar_alegro,
              lv_tar_fija,
              lv_tar_chile,
              lv_tar_cuba,
              lv_tar_espana,
              lv_tar_italia,
              lv_tar_canada,
              lv_tar_usa,
              lv_tar_europa,
              lv_tar_japon,
              lv_tar_maritima,
              lv_tar_mexico,
              lv_tar_mexico_cel,
              lv_tar_pacto_andino,
              lv_tar_restoamerica,
              lv_tar_restodelmundo,
              lv_tar_zona_especial,
              lv_tar_alemania,
              lv_tar_argentina,
              lv_tar_bolivia,
              lv_tar_brasil,
              lv_tar_china,
              lv_tar_colombia,
              lv_tar_costa_rica,
              lv_tar_francia,
              lv_tar_com_771, 
              lv_tar_com_776, 
              lv_tar_com_777, 
              lv_tar_com_778, 
              lv_tar_com_779, 
              lv_tar_com_780, 
              lv_tar_com_781, 
              lv_tar_toll, 
              lv_tar_toll_com, 
              lv_tar_tipo, 
              lv_tar_fecha_creacion, 
              lv_tar_recurso, 
              lv_tar_id_plan_pareja, 
              lv_tar_estado, 
              lv_tar_com_inpool, 
              lv_tar_com_claro, 
              lv_tar_com_movistar, 
              lv_tar_com_alegro, 
              lv_tar_com_fijas, 
              lv_tar_categoria_amx, 
              lv_tar_clase_amx, 
              lv_tar_tipo_cliente, 
              lv_tar_tipo_plan, 
              lv_tar_segmento, 
              lv_tar_plan_suptel,
              lv_bgh_porta_porta,
              lv_bgh_porta_fijas,
              lv_bgh_porta_movistar,
              lv_bgh_porta_alegro,
              lv_TARIFA_BASICA,
              lv_FEATURES_FIJOS,
              lv_VPN_APROVISIONADOR,
              lv_VPN_APROVISIONADOR_TIPO,
              lv_VPN_HOLDING,
              lv_VPN_HOLDING_TIPO,
              lv_VPN_HOLDING_ILIM,
              lv_VPN_HOLDING_ILIM_TIPO,
              ln_ID_DETALLE_PLAN,
              lv_ID_SUBPRODUCTO,
              lv_FU_PACK_ID,
              ln_id_promocion,
              lv_desc_promocion;
              --INSERTO EN LA TABLA FINAL
              execute immediate
              'insert into GSI_TARIFAS_BSCS_FINAL (fecha_version, no_version,'||LL_CAMPOS_COMPARAR||
              ' ) VALUES (:ld_fecha,:version,:lv_tar_id_plan, '||
                          ':lv_tar_descripcion, '||
                            ':lv_tar_tmcode, '||
                            ':lv_tar_1700, '||
                            ':lv_tar_1800, '||
                            ':lv_tar_claro, '||
                            ':lv_tar_claro_inpool, '||
                            ':lv_tar_claro_776, '||
                            ':lv_tar_claro_777, '||
                            ':lv_tar_claro_778, '||
                            ':lv_tar_claro_779, '||
                            ':lv_tar_claro_780, '||
                            ':lv_tar_claro_781, '||
                            ':lv_tar_movistar, '||
                            ':lv_tar_alegro, '||
                            ':lv_tar_fija, '||
                            ':lv_tar_chile, '||
                            ':lv_tar_cuba, '||
                            ':lv_tar_espana, '||
                            ':lv_tar_italia, '||
                            ':lv_tar_canada, '||
                            ':lv_tar_usa, '||
                            ':lv_tar_europa, '||
                            ':lv_tar_japon, '||
                            ':lv_tar_maritima, '||
                            ':lv_tar_mexico, '||
                            ':lv_tar_mexico_cel, '||
                            ':lv_tar_pacto_andino, '||
                            ':lv_tar_restoamerica, '||
                            ':lv_tar_restodelmundo, '||
                            ':lv_tar_zona_especial, '||
                            ':lv_tar_alemania, '||
                            ':lv_tar_argentina, '||
                            ':lv_tar_bolivia, '||
                            ':lv_tar_brasil, '||
                            ':lv_tar_china, '||
                            ':lv_tar_colombia, '||
                            ':lv_tar_costa_rica, '||
                            ':lv_tar_francia, '||
                            ':lv_tar_com_771, '||
                            ':lv_tar_com_776, '||
                            ':lv_tar_com_777, '||
                            ':lv_tar_com_778, '||
                            ':lv_tar_com_779, '||
                            ':lv_tar_com_780, '||
                            ':lv_tar_com_781, '||
                            ':lv_tar_toll, '||
                            ':lv_tar_toll_com, '||
                            ':lv_tar_tipo, '||
                            ':lv_tar_fecha_creacion, '||
                            ':lv_tar_recurso, '||
                            ':lv_tar_id_plan_pareja, '||
                            ':lv_tar_estado, '||
                            ':lv_tar_com_inpool, '||
                            ':lv_tar_com_claro, '||
                            ':lv_tar_com_movistar, '||
                            ':lv_tar_com_alegro, '||
                            ':lv_tar_com_fijas, '||
                            ':lv_tar_categoria_amx, '||
                            ':lv_tar_clase_amx, '||
                            ':lv_tar_tipo_cliente, '||
                            ':lv_tar_tipo_plan, '||
                            ':lv_tar_segmento, '||
                            ':lv_tar_plan_suptel,'||
                            ':lv_bgh_porta_porta,'||
                            ':lv_bgh_porta_fijas,'||
                            ':lv_bgh_porta_movistar,'||
                            ':lv_bgh_porta_alegro, '||
                            ':lv_TARIFA_BASICA, '||
                            ':lv_FEATURES_FIJOS, '||
                            ':lv_VPN_APROVISIONADOR, '||
                            ':lv_VPN_APROVISIONADOR_TIPO, '||
                            ':lv_VPN_HOLDING, '||
                            ':lv_VPN_HOLDING_TIPO, '||
                            ':lv_VPN_HOLDING_ILIM, '||
                            ':lv_VPN_HOLDING_ILIM_TIPO, '||
                            ':ln_ID_DETALLE_PLAN, '||
                            ':lv_ID_SUBPRODUCTO, '||
                            ':lv_FU_PACK_ID, '||
                            ':ln_id_promocion, '||
                            ':lv_desc_promocion '||
                           ' )'
            using
              ld_fecha_version_new,
              LC_OBT_ULT_VERSION.VERSION,
              lv_tar_id_plan,
              lv_tar_descripcion,
              lv_tar_tmcode,
              lv_tar_1700,
              lv_tar_1800,
              lv_tar_claro,
              lv_tar_claro_inpool,
              lv_tar_claro_776,
              lv_tar_claro_777,
              lv_tar_claro_778,
              lv_tar_claro_779,
              lv_tar_claro_780,
              lv_tar_claro_781,
              lv_tar_movistar,
              lv_tar_alegro,
              lv_tar_fija,
              lv_tar_chile,
              lv_tar_cuba,
              lv_tar_espana,
              lv_tar_italia,
              lv_tar_canada,
              lv_tar_usa,
              lv_tar_europa,
              lv_tar_japon,
              lv_tar_maritima,
              lv_tar_mexico,
              lv_tar_mexico_cel,
              lv_tar_pacto_andino,
              lv_tar_restoamerica,
              lv_tar_restodelmundo,
              lv_tar_zona_especial,
              lv_tar_alemania,
              lv_tar_argentina,
              lv_tar_bolivia,
              lv_tar_brasil,
              lv_tar_china,
              lv_tar_colombia,
              lv_tar_costa_rica,
              lv_tar_francia,
              lv_tar_com_771, 
              lv_tar_com_776, 
              lv_tar_com_777, 
              lv_tar_com_778, 
              lv_tar_com_779, 
              lv_tar_com_780, 
              lv_tar_com_781, 
              lv_tar_toll, 
              lv_tar_toll_com, 
              lv_tar_tipo, 
              lv_tar_fecha_creacion, 
              lv_tar_recurso, 
              lv_tar_id_plan_pareja, 
              lv_tar_estado, 
              lv_tar_com_inpool, 
              lv_tar_com_claro, 
              lv_tar_com_movistar, 
              lv_tar_com_alegro, 
              lv_tar_com_fijas, 
              lv_tar_categoria_amx, 
              lv_tar_clase_amx, 
              lv_tar_tipo_cliente, 
              lv_tar_tipo_plan, 
              lv_tar_segmento, 
              lv_tar_plan_suptel,
              lv_bgh_porta_porta,
              lv_bgh_porta_fijas,
              lv_bgh_porta_movistar,
              lv_bgh_porta_alegro,
              lv_TARIFA_BASICA,
              lv_FEATURES_FIJOS,
              lv_VPN_APROVISIONADOR,
              lv_VPN_APROVISIONADOR_TIPO,
              lv_VPN_HOLDING,
              lv_VPN_HOLDING_TIPO,
              lv_VPN_HOLDING_ILIM,
              lv_VPN_HOLDING_ILIM_TIPO,
              ln_ID_DETALLE_PLAN,
              lv_ID_SUBPRODUCTO,
              lv_FU_PACK_ID,
              ln_id_promocion,
              lv_desc_promocion;
         if ln_cont = 5 then
           commit;
         end if;
         
      end loop;
      CLOSE lc_cambios_tarifas;
    END IF;
    
    EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_TARIFAS_BSCS_TMP';
    EXECUTE IMMEDIATE 'INSERT INTO GSI_TARIFAS_BSCS_TMP SELECT * FROM '||PV_TABLA_NEW;
    commit;
    
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:=SQLERRM;
END PR_GENERA_HISTORICA;

END PCK_GSI_QC_REVISA_TARIFAS_BSCS;
/
