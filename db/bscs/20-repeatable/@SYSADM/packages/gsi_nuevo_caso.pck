create or replace package gsi_nuevo_caso IS

end gsi_nuevo_caso;
/
create or replace package body gsi_nuevo_caso IS

Procedure GSI_BUSCA_INCONSISTENCIAS_F(/*PV_CICLO  Varchar2,*/
                                      --- PV_FECHA_CORTE IN OUT DATE, ---SUD CAC---
                                      PV_FECHA_INICIO DATE,---SUD CAC se agrega variable
                                      PV_FECHA_FIN DATE) is ---CAC se agrega variable

    ----Saca los ciclos respectivos a cada ciclo de cierre
    
   /* cursor CICLOS is
      select id_ciclo_admin
      from fa_ciclos_axis_bscs
      where id_ciclo = nvl(PV_CICLO,id_ciclo); */  ---SUD CAC MODIFICACION---
    
--- SUD CAC                               
---DETERMINAR SERVICIOS QUE TIENEN MOVIMIENTOS EN CL_NOVEDADES EN UN RANGO DE FECHAS
           CURSOR SERVICIO_NOVEDADES( LV_FECHA_INICIO VARCHAR2, 
                                      LV_FECHA_FIN VARCHAR2)IS
                                       
                       select distinct '593'||d.id_servicio AS SERVICIO
                       from   cl_novedades@AXIS d
                       where  d.fecha between TO_DATE(LV_FECHA_INICIO,'DD/MM/YYYY') 
                                          and TO_DATE(LV_FECHA_FIN,'DD/MM/YYYY')
                       and    d.id_subproducto in ('AUT','TAR')
                       And    d.id_tipo_novedad='GESE'
                       and    d.estado     = 'A';
--                       and id_servicio = '90305835';
                                            
       
------------------------
------------------------   
      
      ---Cursor se utiliza para verificar si hubo un cambio de PPT A TAR
      
           cursor c_verifica (v_tel varchar2, fecha_verf varchar2)is
                 
                 SELECT /*+rule+*/ CO_ID
                 FROM CL_servicios_contratados@AXIS 
                 WHERE OBSERVACION LIKE 'CAMBIO DE NUMERO%'
                 AND ID_SERVICIO = v_tel
                 and TRUNC(fecha_inicio) = to_date(fecha_verf,'dd/mm/yyyy')
                 order by fecha_inicio;
           
            LC_C_verifica   c_verifica%ROWTYPE;
            LB_FOUND_ver BOOLEAN;
            
      ---Cursor se utiliza para ver si en el rango de analisis se encuentran Estado 31 y 91      
            cursor s_verifica (s_tel varchar2, fecha_s varchar2) is
                  SELECT /*+rule+*/ VALOR, FECHA_DESDE, FECHA_HASTA
                  FROM CL_DETALLES_SERVICIOS@AXIS
                  WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'                  
                  AND ID_SERVICIO = s_tel
                  AND valor IN ('31','91','25')---valor de cambio o reposicion
                  AND FECHA_DESDE >= to_date(fecha_s,'dd/mm/yyyy hh24:mi:ss')
                  ORDER BY FECHA_DESDE;

            LC_s_verifica   s_verifica%ROWTYPE;
            LB_FOUND_s_ver BOOLEAN;   
         
    
           cursor TELEFONO_SERVICIO(LV_TEL_SERV VARCHAR2, ---UTILIZAR LA FUNCION DE OBTENER NUMERO
                                    LV_FECHA1   VARCHAR2, 
                                    LV_FECHA2   VARCHAR2) IS
                 SELECT /*+rule+*/VALOR,FECHA_DESDE,FECHA_HASTA
                 FROM CL_DETALLES_SERVICIOS@AXIS
                 WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'
                 /*AND ESTADO = 'A'*/---SUB CAC 
                 AND ID_SERVICIO = LV_TEL_SERV
                 AND valor IN ('23', '29', '30', '32', '92','26','27','80','34','36'/*,'33'*/)---SUD CAC
                 AND FECHA_DESDE >= TO_DATE(LV_FECHA1, 'DD/MM/YYYY')                 
                 AND FECHA_DESDE <= TO_DATE(LV_FECHA2, 'DD/MM/YYYY')
                 ORDER BY FECHA_DESDE;
             
         /*LC_SERVICIO   TELEFONO_SERVICIO%ROWTYPE;*/
         /*LB_FOUND_SERV BOOLEAN;*/ 
         
            --23  ACTIVO NUEVO
            --29  REACTIVADO CON CARGO
            --30  REACTIVADO SIN CARGO
            --32  ACTIVO / CAMBIO
            --92  ACTIV.CAMBIO AUTOCONTROL
            
            --26 T/C SUSPENDIDO ROBO
            --27 SUSP.COBR.AUTOCONTROL
            --34 TC SUSPENSION PARCIAL
            --36 T/C SUSPENDIDO USUARIO
            --80 SUSPENSION PRE AVANZADA
            --33 SUSPENDIDO POR COBRANZA AVANZADA ---EN BSCS NO SE REFLEJA
            
           
    
     ---determinar CO_ID Y EL PERIODO DEL CICLO QUE PERTENECE EL CICLO
         cursor TELEFONOS(LV_DN_NUM VARCHAR2)IS  
    
                SELECT /*+rule+*/ A.DN_NUM AS SERVICIO,b.CO_ID,E.BILLCYCLE AS ID_CICLO_ADMIN
                FROM 
                       directory_number a,
                       contr_services_cap b,
                       contract_all d,
                       customer_all e
                where E.CUSTOMER_ID = D.CUSTOMER_ID
                and d.co_id = b.co_id
                AND B.CS_DEACTIV_DATE IS NULL
                AND B.DN_ID = A.DN_ID
                AND A.DN_NUM =LV_DN_NUM;
  
         
         LC_TELEFONOS   TELEFONOS%ROWTYPE;
         LB_FOUND_TEL BOOLEAN;
         
    
    
        ---Se scan los datos del servicio EN BSCS
            CURSOR SERVICIOS(LV_CO_ID_BSCS NUMBER,lv_fecha_1 date,lv_fecha_2 date) IS
                SELECT /*+rule+*/ /*c.entry_date , c.sncode, C.STATUS,H.STATUS_HISTNO*/ DISTINCT C.ENTRY_DATE,C.STATUS
                  FROM pr_serv_status_hist c, profile_service h
                 WHERE h.co_id = LV_CO_ID_BSCS
                   AND h.co_id = c.co_id
                      --AND  c.sncode NOT IN (select cod_sncode from SNCODE_CONCILIA)
                   and c.sncode IN (/*5,*/ 2)---SUD CAC SE TRABAJA SOLO CON SNCODE 2
                   AND C.STATUS NOT IN ('O')
                  /* and h.status_histno = c.histno*/
                   and c.entry_date > lv_fecha_1
                   AND c.entry_date <= lv_fecha_2 
                   ORDER BY C.ENTRY_DATE;
                   
            /*LB_FOUND_SERV BOOLEAN;*/
     

       cursor REVISADOS(CO_ID NUMBER, MES VARCHAR2, CICLO VARCHAR2,LV_FECHA_AXIS DATE,LV_FECHA_BSCS DATE) IS
             select /*+rule+*/ *
             from RESPALDO_CONCILIACION
             WHERE CO_ID_BSCS = CO_ID
             AND CICLO = CICLO
             AND MES_CIERRE = MES
             AND FECHA_AXIS=LV_FECHA_AXIS
             AND FECHA_BSCS=LV_FECHA_BSCS;

      LC_REV       REVISADOS%ROWTYPE;
      LB_FOUND_REV BOOLEAN;

    --- VARIABLES PARA DETERMINAR LOS RANGOS DE FECHA DEL PERIODO
    /*
    --- SUD CAC ----------------
    LV_dia_ini      varchar2(2);
    LV_dia_fin      varchar2(2);
    LV_mes_ini      varchar2(2);
    LV_anio_ini     varchar2(4);
    LV_anio_fin     varchar2(4);
    LV_fecha_ini_ci varchar2(10);
    LV_fecha_fin_ci varchar2(10);
    ----------------------------
    */
    
    LV_mes_fin      varchar2(2);
    
    -- VARIABLES PARA VALIDAR EL COMMIT
    LN_CONTADOR NUMBER := 0;
    LN_VALIDA   NUMBER := 500;
    ---VARIABLES PARA LA TABLA DE RESPALDO
    LV_FECHA_REVISA VARCHAR2(10);

   
    
    
    ---SUD CAC  VARIABLES PARA SCP------------
     lvMensErr VARCHAR2(2000) := null;
     lv_valor           VARCHAR2(1);
     lv_descripcion     VARCHAR2(500);
    ------------------------------------------
    ---SUD CAC--------------------------------
    ESTADO_AXIS VARCHAR2(2):='';
    LN_CONTADOR_SERVICIO23 NUMBER:=0;
    
    TYPE TT_SERVICIOS IS TABLE OF RESPALDO_CONCILIACION.SERVICIO_AXIS%TYPE INDEX BY BINARY_INTEGER;
    VTT_SERVICIOS TT_SERVICIOS;
                 
    ------------------------------------------
    
    /*BANDERA NUMBER:=0;*/
    cont number:=0;
    FECHA_1 DATE;
    FECHA_2 date;
    
    CONT_VER1 number:=0;
    CONT_VER2 number:=0;
    
    TELELEFONO VARCHAR2(10):='';
    
    BANDERA_SALTA BOOLEAN:=FALSE;
  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_BUSCA_INCONSISTENCIAS',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
                                               
     if nvl(lv_valor, 'N') = 'S' then
 
    ---SUD CAC----
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                           'GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS',
                                           null,
                                           null,
                                           null,
                                           null,
                                           ln_total_registros_scp,
                                           lv_unidad_registro_scp,
                                           ln_id_bitacora_scp,
                                           ln_error_scp,
                                           lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
    
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje inicio de Proceso
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          'Inicio de GSI_REVISA_INCONSISTENCIA.GSI_BUSCA_INCONSISTENCIAS',
                                          lvMensErr,
                                          '-',
                                          0,
                                          null,
                                          NULL/*'Ciclo: '|| PV_CICLO*/,
                                          'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
                                          
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
    
    --- ESTABLECER LA FECHA DE REVISION
    LV_FECHA_REVISA := to_date(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    ---1. Buscar la fecha de inicio y fin de ciclo
    
    ---SUD CAC
    ----------
    /* 
    SELECT a.dia_ini_ciclo, a.dia_fin_ciclo
      into LV_dia_ini, LV_dia_fin
      FROM fa_ciclos_bscs a
     where id_ciclo = PV_CICLO;
   
   
    -------------------------------------------------------------------------
    If to_NUMBER(to_char(PV_FECHA_CORTE, 'dd')) < to_NUMBER(lv_dia_ini) Then
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 1 Then
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy') - 1;
      Else
        ---LPad(char, length, pad_string)
        lv_mes_ini  := lpad(to_char(PV_FECHA_CORTE, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_fin  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
    Else
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 12 Then
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy') + 1;
      Else
        lv_mes_fin  := lpad(to_char(PV_FECHA_CORTE, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_ini  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
    End If;
    -------------------------------------------------------------------------
    LV_fecha_ini_ci := LV_dia_ini || '/' || lv_mes_ini || '/' ||
                       lv_anio_ini;
    LV_fecha_fin_ci := LV_dia_fin || '/' || lv_mes_fin || '/' ||
                       lv_anio_fin;
    -----------------------------------------------------------------------------
    */
    --- SUD CAC---
    lv_mes_fin:=to_char(PV_FECHA_FIN,'mm');
    --------------
    VTT_SERVICIOS.DELETE;
    
    
    FECHA_2:=PV_FECHA_INICIO;
        
    OPEN SERVICIO_NOVEDADES(TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),
                            TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')  );
    
               LOOP
                 
                EXIT WHEN SERVICIO_NOVEDADES%NOTFOUND;
               
                FETCH SERVICIO_NOVEDADES BULK COLLECT INTO VTT_SERVICIOS limit 10000;
                
                
                ---En busca de Inconsistencia de Fechas
                  for B in 1 .. VTT_SERVICIOS.COUNT LOOP
                       
                    LN_CONTADOR := 0;
                    LN_VALIDA   := 500;
                    
                     LN_CONTADOR := LN_CONTADOR + 1;
                     IF LN_VALIDA = LN_CONTADOR THEN
                       LN_VALIDA := LN_CONTADOR + 500;
                       COMMIT;
                     END IF;
                     
                    /*BANDERA:=0;*/
                    -------
                    ---PARA VERIFICAR EL SERVIIO TIENE PROBLEMAS
                    -------
                    CONT_VER1:=0;
                    CONT_VER2:=0;
                    ---------------
                    ---------------
                    cont:=0;
                    ---------------
                    ---------------
                    
                    FECHA_2:=PV_FECHA_INICIO;
                    select PV_FECHA_INICIO + 2 INTO FECHA_1 from dual;
                    
                    TELELEFONO:=obtiene_telefono_dnc@axis(VTT_SERVICIOS(B));
                  
                  OPEN s_verifica(TELELEFONO,
                                  to_char(PV_FECHA_INICIO,'dd/mm/yyyy hh24:mi:ss'));
                       FETCH s_verifica INTO lc_s_verifica;
                       LB_FOUND_s_ver:= s_verifica%FOUND;
                       CLOSE s_verifica;                    
                  
                  IF(LB_FOUND_s_ver = FALSE)THEN 
                  
                              OPEN TELEFONOS (VTT_SERVICIOS(B));
                                   FETCH TELEFONOS INTO LC_TELEFONOS;
                                   LB_FOUND_TEL:= TELEFONOS%FOUND;
                              CLOSE TELEFONOS;
                                   
                  IF LB_FOUND_TEL = TRUE THEN 
                  
                  FOR LC_SERVICIO IN TELEFONO_SERVICIO(TELELEFONO,
                                                      TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),  /*LV_fecha_ini_ci SUD CAC*/
                                                      TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')) LOOP
                         
                                  CONT_VER1:=CONT_VER1+1;
                                  IF LC_SERVICIO.VALOR IN ('23','29','30','32','92')THEN
                                    ESTADO_AXIS:='A';
                                  ELSIF LC_SERVICIO.VALOR IN ('26','27','34','36','80')THEN
                                    ESTADO_AXIS:='S';
                                  END IF;
                             
                                   ---------------------
                                   ---------------------
                                                   ---- SI ES UN CAMBIO DE PPT A TAR 
                                                   ---- ESTO NO SE REFLEJA EN BSCS
                                                  IF LC_SERVICIO.VALOR IN ('32')THEN
                                                   OPEN c_verifica(TELELEFONO,to_char(LC_SERVICIO.FECHA_DESDE,'dd/mm/yyyy'));
                                                         FETCH c_verifica INTO LC_C_verifica;
                                                         LB_FOUND_ver:= c_verifica%FOUND;
                                                   CLOSE c_verifica; 
                                                  END IF;
                                 ---------------------
                                 ---------------------
                                 
                            IF LB_FOUND_ver =TRUE THEN
                               -----
                               -----                               
                             
                               LB_FOUND_ver:=FALSE;
                               CONT_VER2:=CONT_VER2+1;
                               FECHA_2:=LC_SERVICIO.FECHA_DESDE;
                               select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                               
                            ELSE 
                            
                             FOR C IN SERVICIOS(LC_TELEFONOS.CO_ID,FECHA_2,FECHA_1) LOOP                 
                               
                                   IF(C.STATUS <> ESTADO_AXIS)THEN
                                         BANDERA_SALTA:=TRUE;
                                         EXIT; 
                                   END IF;
                                  
                                  
                                  CONT_VER2:=CONT_VER2+1;
                                  FECHA_2:=C.entry_date;
                                  select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                                  
                                  IF LC_SERVICIO.VALOR = 23 THEN  --VALIDA SERVICIO 23 --SUD CAC
                                     
                                     IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >    
                                        TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN
                                        
                                        --SUD CAC---
                                        --SCP Procedimiento de busquedas de inconsistencias
                                        --SCP:MENSAJE
                                        ----------------------------------------------------------------------
                                        -- SCP: Registro de mensaje DETALLE
                                        ----------------------------------------------------------------------
                                        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                              'SERVICIOS NUEVOS',
                                                                              lvMensErr,
                                                                              '-',
                                                                              0,
                                                                              null,
                                                                              VTT_SERVICIOS(B)||' CO_ID: '|| TO_CHAR(LC_TELEFONOS.CO_ID),
                                                                              'F. BSCS: '||TO_CHAR(C.entry_date,'dd/MM/yyyy')||' F. AXIS '||TO_CHAR(LC_SERVICIO.FECHA_DESDE,'dd/MM/yyyy'),
                                                                              LC_SERVICIO.VALOR,---VALOR EN AXIS 23
                                                                              null,
                                                                              'S',
                                                                              ln_error_scp,
                                                                              lv_error_scp);
                                                                                            
                                        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
                                        ---fin Registro de mensaje DETALLE------------------------------------------
                                        ----------------------------------------------------------------------------
                                        LN_CONTADOR_SERVICIO23:=LN_CONTADOR_SERVICIO23+1;
                                       
                                        exit;
                                    end if;---FIN IF COMPARA FECHAS SERVICIOS CON VALOR 23
                                       
                                    ELSE -- SUD CAC
                                  
                                        IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >---fecha bscs    
                                           TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN   ---fecha axis

                                            OPEN REVISADOS(LC_TELEFONOS.CO_ID, 
                                                           lv_mes_fin,
                                                           LC_TELEFONOS.ID_CICLO_ADMIN,
                                                           LC_SERVICIO.FECHA_DESDE,
                                                           C.ENTRY_DATE);
                                                           
                                            FETCH REVISADOS
                                              INTO LC_REV;
                                            LB_FOUND_REV := REVISADOS%FOUND;
                                            CLOSE REVISADOS;

                                                IF LB_FOUND_REV THEN
                                                    UPDATE RESPALDO_CONCILIACION
                                                       SET ESTADO = 'A', COMENTARIO = ''
                                                       WHERE ciclo = LC_TELEFONOS.ID_CICLO_ADMIN
                                                       AND mes_cierre = lv_mes_fin
                                                       and co_id_bscs = LC_TELEFONOS.co_id
                                                       and fecha_axis = LC_SERVICIO.FECHA_DESDE
                                                       and fecha_bscs = C.ENTRY_DATE;
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                 ELSE
                                                      cont:=cont+1;
                                                      insert into RESPALDO_CONCILIACION
                                                       values
                                                      (LV_FECHA_REVISA,
                                                       VTT_SERVICIOS(B),
                                                       LC_SERVICIO.FECHA_DESDE,
                                                       LC_TELEFONOS.CO_ID,
                                                       C.ENTRY_DATE,
                                                       'A',
                                                       null,
                                                       LC_TELEFONOS.ID_CICLO_ADMIN,
                                                       lv_mes_fin,
                                                       cont,
                                                       ESTADO_AXIS);
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                END IF;-- FIN IF CURSOR REVISADOS
                                                
                                                commit;
                                               
                                           
                                          end if;-- FIN IF DE COMPARACION DE FECHAS
                                          
                                    
                                       END IF; --VALIDA SERVICIO 23
                                   
                                    Exit;--- SALIR DEL LOOP
                                    
                                END LOOP; ---FIN LOOP CURSOR SERVICIOS
                                
                                IF(BANDERA_SALTA)THEN
                                   BANDERA_SALTA:=TRUE;
                                   EXIT;
                                END IF;
                                
                             END IF;
                  
                       END LOOP; --- FIN LOOP TELEFONO SERVICIO
                       
                       ----GUARDA SERVICIOS Q NO SE REFLEJEN EN BSCS
                       IF (CONT_VER1<>CONT_VER2)THEN 
                           --SUD CAC---
                            --SCP Procedimiento de busquedas de inconsistencias
                            --SCP:MENSAJE
                            ----------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------
                            
                            
                            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                  'Cuenta con Problemas Revisar: '||VTT_SERVICIOS(B),
                                                                  lvMensErr,
                                                                  '-',
                                                                  0,
                                                                  null,
                                                                  NULL,
                                                                  'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                                                  null,
                                                                  null,
                                                                  'S',
                                                                  ln_error_scp,
                                                                  lv_error_scp);
                             
                            --SCP:FIN
                            ----------------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------------                                                                  
                                                                  
                                             
                       END IF;
                       ----------------
                       ----------------
                   END IF; ---CURSOR TELEFONOS
                 END IF;
                
                end loop; -- FIN LOOP VARIABLE TIPO TABLA
               
             END LOOP;--FIN LOOP CURSOR BULK COLLECT
  
CLOSE SERVICIO_NOVEDADES;
   
   
    
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------
      
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se encontraron: '||to_char(LN_CONTADOR_SERVICIO23)||' Servicios Nuevos Activados INCONSISTENTES',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            NULL,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
                                            
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se ejecutó GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            null,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
      ----------------------------------------------------------------------------
      
      commit;
   ----CAC----
    
    EXCEPTION
      
      WHEN NO_DATA_FOUND THEN
      
      lvMensErr:= 'NO SE ENCONTRO DATOS EN LA TABLA fa_ciclos_bscs';
      
      IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_CONCILIA_BSCS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      ----------------------------------------------------------------------------

    WHEN OTHERS THEN      
    
     
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     
     IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      ----------------------------------------------------------------------------
    -------
    -------CAC
    

    ---Llamada al procedimiento de actualización
    --- GSI_REVISA_TABLA;

  end GSI_BUSCA_INCONSISTENCIAS_F;



end gsi_nuevo_caso;
/
