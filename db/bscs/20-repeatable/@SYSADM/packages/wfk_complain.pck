create or replace package WFK_COMPLAIN is

  -- Proyecto   : [8665] Inconsistencia de HC
  -- Creado por : Gloria Salazar
  -- Fecha      : 20/02/2013
  -- Objetivo   : Consultar los rubros de la Hoja de C�lculo de Proceso WF de SDA
  --***************************************************************************************************************************
  -- Proyecto      : [10182] Facturaci�n con 12% de IVA a productos de Roaming y Adendum
  -- Modifcado por : UEES Lucy Caregua
  -- Lider CLARO   : SIS Julia Rodas
  -- Fecha         : 02/06/2015
  -- Objetivo      : Incorporar producto Roaming con el impuesto de IVA desde la fecha en la que se implemento el cambio.
  --***************************************************************************************************************************
  
  -- Proyecto   : 10503 - Nuevo Esquema para Traspasos Masivos
  -- Creado por : Josue Rocafuerte
  -- Fecha      : 06/01/2016
  -- Objetivo   : Ajustes a la wf_complain_temporal
  
  -- Proyecto   : 10503 - Nuevo Esquema para Traspasos Masivos
  -- Creado por : Josue Rocafuerte
  -- Fecha      : 22/01/2016
  -- Objetivo   : Ajustes a la HC para validar los periodos sean por Facturas autorizadas por el SRI 
  --              Se amplio las variables a 4000--
  
  
 
  -- Proyecto    : [10503] Nuevo Esquema Para Proceso Masivo de Traspaso
  -- Lider SIS   : Diana Chonillo
  -- Lider PDS   : RGT Fernando Ortega
  -- Recurso PDS : RGT Josue Rocafuerte
  -- Fecha       : 01/02/2016
  -- Objetivo    : Ajuste para Obtener Producto para Rubros de llamadas para las cuentas a devolver.
    
  -- Proyecto    : [10503] Nuevo Esquema Para Proceso Masivo de Traspaso
  -- Lider SIS   : Diana Chonillo
  -- Lider PDS   : RGT Fernando Ortega
  -- Recurso PDS : RGT Josue Rocafuerte
  -- Fecha       : 05/02/2016
  -- Objetivo    : Ajuste por Obtener sumarizar Rubros.
    
  -- [10808] SDA EN L�NEA SIS DCHONILLO -- RGT RRAMIREZ -- RGT JROCAFUERTE
  -- AJUSTE EN BALANCE PARA RUBROS DE HC
  
  -- [10910] IVA 14 E ICE 15 NOTAS DE CREDITO SIS DCHONILLO -- RGT RRAMIREZ -- RGT JROCAFUERTE
  -- AJUSTE EN BALANCE PARA RUBROS DE HC
  
  TYPE Gr_ValoresRubros IS RECORD(
    valor     NUMBER,
    descuento NUMBER,
    factura   VARCHAR2(100));
-- 10182
-- UEES LCA   
  CURSOR C_PARAMETRO (GN_TIPO_PARAMETRO NUMBER,
                        GV_ID_PARAMETRO   VARCHAR2) IS
    SELECT VALOR FROM GV_PARAMETROS G
    WHERE G.ID_TIPO_PARAMETRO= GN_TIPO_PARAMETRO
    AND G.ID_PARAMETRO= GV_ID_PARAMETRO;    
-- FIN 10182 

  TYPE Gt_ValoresRubros IS TABLE OF Gr_ValoresRubros INDEX BY BINARY_INTEGER;

  procedure wfp_principal(pv_fecha varchar2, pv_cuenta varchar2);

  procedure wfp_obtiene_rubros_llamadas(pv_fecha  varchar2,
                                        pv_cuenta varchar2,
                                        pv_error  out varchar2);

  procedure wfp_obtiene_rubros_servicios(pv_fecha  varchar2,
                                         pv_cuenta varchar2,
                                         pv_error  out varchar2);
                                         
  --10503                                       
  PROCEDURE WFP_PRINCIPAL_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2);  
  PROCEDURE WFP_OBTIENE_RUBROS_SERVICIOS_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2);
  PROCEDURE WFP_VALIDA_PERIODOS(PV_PERIODOS         IN VARCHAR2,
                                PV_CUENTA           IN VARCHAR2,
                                PV_PERIODOS_VALIDOS OUT VARCHAR2,
                                PV_FACTURAS         OUT VARCHAR2,
                                PV_ERROR            OUT VARCHAR2);
                                
  PROCEDURE WFP_OBTIENE_RUBROS_LLAMADAS_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2);
  PROCEDURE WFP_VALIDA_RUBROS(PV_ERROR OUT VARCHAR2);
  --10503 - Fin
  --10910
  FUNCTION WFF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO IN NUMBER,
                                      PV_ID_PARAMETRO IN VARCHAR2) RETURN VARCHAR2;
  --10910
end WFK_COMPLAIN;
/
create or replace package body WFK_COMPLAIN is

  -- Proyecto   : [8665] Inconsistencia de HC
  -- Creado por : Gloria Salazar
  -- Fecha      : 20/02/2013
  -- Objetivo   : Consultar los rubros de la Hoja de C�lculo de Proceso WF de SDA

  --***************************************************************************************************************************
  -- Proyecto      : [10182] Facturaci�n con 12% de IVA a productos de Roaming y Adendum
  -- Modifcado por : UEES Lucy Caregua
  -- Lider CLARO   : SIS Julia Rodas
  -- Fecha         : 02/06/2015
  -- Objetivo      : Incorporar producto Roaming con el impuesto de IVA desde la fecha en la que se implemento el cambio.
  --***************************************************************************************************************************

  -- Proyecto   : 10503 - Nuevo Esquema para Traspasos Masivos
  -- Creado por : Josue Rocafuerte
  -- Fecha      : 16/12/2015
  -- Objetivo   : Ajustes a la wf_complain_temporal
  
 -- 10503 :: Funcion para contar las cadenas
 FUNCTION WFF_CONTARCADENA(PV_CADENA IN VARCHAR2, PV_PBUSQUEDA IN VARCHAR2 DEFAULT '-') RETURN NUMBER IS
   LV_LONGITUD NUMBER(10);
   LV_CONTADOR NUMBER(10) := 0;
   LV_CARACTER VARCHAR2(1); 
 BEGIN
   LV_LONGITUD := LENGTH(PV_CADENA);
   FOR I IN 1 .. LV_LONGITUD LOOP
     LV_CARACTER := SUBSTR(PV_CADENA, I, 1);
     IF LV_CARACTER = PV_PBUSQUEDA THEN
       LV_CONTADOR := LV_CONTADOR + 1;
     END IF;
   END LOOP;
   RETURN LV_CONTADOR;
 EXCEPTION
   WHEN OTHERS THEN
     RETURN LV_CONTADOR;
 END WFF_CONTARCADENA;
  
  procedure wfp_principal(pv_fecha varchar2, pv_cuenta varchar2) is
  
    lv_error varchar2(1000);
  
  begin
  
    delete WF_COMPLAIN_TEMPORAL;
    commit;
  
    wfk_complain.wfp_obtiene_rubros_servicios(pv_fecha,
                                              pv_cuenta,
                                              lv_error);
  
    wfk_complain.wfp_obtiene_rubros_llamadas(pv_fecha, pv_cuenta, lv_error);
  
    COMMIT;
  
  exception
    when others then
      rollback;
    
  end;

  procedure wfp_obtiene_rubros_servicios(pv_fecha  varchar2,
                                         pv_cuenta varchar2,
                                         pv_error  out varchar2) is
  
    -- cursor para traer bandera
    cursor c_bandera(cn_tipo number , cv_parametro varchar2) is 
    select valor 
      from gv_parametros 
     where id_tipo_parametro = cn_tipo
       and id_parametro = cv_parametro;
    
    
    lv_query varchar2(32700);
    
    -- 10182 INI
    Lv_fecha_cobro_iva_roam  gv_parametros.valor%type;
    -- 10182 FIN
     
    lv_valor    varchar2(15);--10503
  
  begin
     
    lv_query := 'INSERT INTO WF_COMPLAIN_TEMPORAL(NOMBRE2,
                                                   VALOR,
                                                   CUSTOMER_ID,
                                                   CUSTCODE,
                                                   OHREFNUM,
                                                   TIPO,
                                                   NOMBRE,
                                                   SERVICIO,
                                                   DESCUENTO,
                                                   COBRADO,
                                                   CTACLBLEP,
                                                   CTA_DEVOL,
                                                   TIPO_IVA)
                 SELECT A.NOMBRE2,
                        A.VALOR,
                        A.CUSTOMER_ID,
                        A.CUSTCODE,
                        A.OHREFNUM,
                        A.TIPO,
                        TRIM(DECODE(A.SERVICIO, 3, ''I.V.A. Por servicios (12%)'',4,''ICE de Telecomunicaci�n (15%)'', B.DES)) NOMBRE,
                        (select to_char(z.SERVICIO)
                           from COB_SERVICIOS z
                          where z.NOMBRE2 = a.NOMBRE2
                            and rownum < 2) SERVICIO,
                        A.DESCUENTO,
                        A.VALOR - A.DESCUENTO COBRADO,
                        (select CTAPSOFT
                           from COB_SERVICIOS z
                          where z.NOMBRE2 = a.NOMBRE2
                            and rownum < 2) CTACLBLEP,
                        (select CTA_DEVOL
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) CTA_DEVOL,
                        (select TIPO_IVA
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) TIPO_IVA
                   FROM SYSADM.co_fact_' || pv_fecha ||
                ' A, MPUSNTAB B
                  WHERE A.SERVICIO = B.SNCODE
                    AND A.CUSTCODE = ''' || pv_cuenta || '''
                    AND A.SERVICIO not in
              (select substr(otname,instr(otname, ''.'', 1, 3) + 1,instr(substr(otname, instr(otname, ''.'', 1, 3) + 1), ''.'') - 1)
                 from ordertrailer a, orderhdr_all b, customer_all c
                where a.otshipdate = to_date(''' || pv_fecha ||
                ''', ''ddmmyyyy'')
                  and b.OHSTATUS IN (''IN'', ''CM'')
                  and b.OHUSERID IS NULL
                  and a.otshipdate = b.ohentdate
                  and a.otxact = b.ohxact
                  and b.customer_id = c.customer_id
                  and c.custcode = ''' || pv_cuenta || '''
                  and (otname like (''%.BASE.%'') or otname like (''%.IC.%''))
                group by substr(otname,instr(otname, ''.'', 1, 3) + 1,instr(substr(otname, instr(otname, ''.'', 1, 3) + 1),''.'') - 1))';
    
    

    EXECUTE IMMEDIATE lv_query;
    
    /*delete temp_edu;
    insert into temp_edu values(lv_query);
    
    commit;*/
    
   --10182 INI       
   OPEN C_PARAMETRO(10182,'FECHA_INICIO_COBRO_IVA_ROAM');
    FETCH C_PARAMETRO
      INTO Lv_fecha_cobro_iva_roam;
    CLOSE C_PARAMETRO;
  
  if to_date(pv_fecha,'dd/mm/yyyy') < to_date(Lv_fecha_cobro_iva_roam,'dd/mm/yyyy') then
      update WF_COMPLAIN_TEMPORAL
       set TIPO_IVA = 'EXP', TIPO='004 - EXENTO'
     where nombre2 LIKE '%ROAM%';
          
  END IF;
  --10182 FIN
  
    update WF_COMPLAIN_TEMPORAL
       set valor =
           (select sum(valor)
              from WF_COMPLAIN_TEMPORAL
             where tipo = '003 - IMPUESTOS'
               and nombre2 like 'IVA%'),
           descuento = (select sum(descuento)
                        from WF_COMPLAIN_TEMPORAL
                       where tipo = '003 - IMPUESTOS'
                         and nombre2 like 'IVA%'),
           cobrado = (select sum(cobrado)
                        from WF_COMPLAIN_TEMPORAL
                       where tipo = '003 - IMPUESTOS'
                         and nombre2 like 'IVA%')
     where tipo = '003 - IMPUESTOS'
       and nombre2 = 'IVA';
       
    delete WF_COMPLAIN_TEMPORAL
     where tipo = '003 - IMPUESTOS'
       and nombre2 like 'IVA_%'; 
  
  exception
    when others then
      pv_error := sqlerrm;
      rollback;
    
  end;

  procedure wfp_obtiene_rubros_llamadas(pv_fecha  varchar2,
                                        pv_cuenta varchar2,
                                        pv_error  out varchar2) is
  
    cursor c_obtiene_valores_llamadas is
      select substr(otname, 1, instr(otname, '.', '1', '1') - 1) tmcode,
             substr(otname,
                    instr(otname, '.', 1, 3) + 1,
                    instr(substr(otname, instr(otname, '.', 1, 3) + 1), '.') - 1) sncode,
             substr(otname,
                    instr(otname, '.', 1, 11) + 1,
                    instr(substr(otname, instr(otname, '.', 1, 11) + 1), '.') - 1) zncode,
             sum(a.otamt_revenue_gl) valor,
             sum(a.otamt_disc_doc) descuento,
             b.ohrefnum
        from ordertrailer a, orderhdr_all b, customer_all c
       where a.otshipdate = to_date(pv_fecha, 'ddmmyyyy')
         and b.OHSTATUS IN ('IN', 'CM')
         and b.OHUSERID IS NULL
         and a.otshipdate = b.ohentdate
         and a.otxact = b.ohxact
         and b.customer_id = c.customer_id
         and c.custcode = pv_cuenta
         and (otname like ('%.BASE.%') or otname like ('%.IC.%'))
       group by substr(otname, 1, instr(otname, '.', '1', '1') - 1),
                substr(otname,
                       instr(otname, '.', 1, 3) + 1,
                       instr(substr(otname, instr(otname, '.', 1, 3) + 1),
                             '.') - 1),
                substr(otname,
                       instr(otname, '.', 1, 11) + 1,
                       instr(substr(otname, instr(otname, '.', 1, 11) + 1),
                             '.') - 1),
                b.ohrefnum;
  
    cursor c_obtiene_mapeo is
      select tmcode,
             sncode,
             zncode,
             condicion_tmcode,
             condicion_sncode,
             condicion_zncode,
             label_id
        from wf_mapeo_rubros_llamadas
       where estado = 'A'
       order by label_id;
  
    cursor c_obtiene_doc1(cn_label_id number) is
      select DES,
             DECODE(UPPER(TAXINF),
                    'EXCENTO',
                    '004 - EXENTO',
                    '002 - ADICIONALES') tipo,
             CTAPS,
             CTA_DEVOL
        from doc1.service_label_doc1
       where label_id = cn_label_id;
  
    cursor c_obtiene_customer_id is
      select customer_id from customer_all where custcode = pv_cuenta;
  
    lv_error         varchar2(1000);
    lv_customer_id   varchar2(100);
    ln_band          number := 0;
    ln_band1         number := 0;
    ln_band2         number := 0;
    ln_band3         number := 0;
    lc_obtiene_doc1  c_obtiene_doc1%rowtype;
    Lt_ValoresRubros Gt_ValoresRubros;
  --10182 INICIO
   Lv_fecha_cobro_iva_roam  gv_parametros.valor%type;
   LV_TIPO_WF        VARCHAR2(25);
   ln_cont_roam      Number;
  --10182 FIN
  
  begin
  
    open c_obtiene_customer_id;
    fetch c_obtiene_customer_id
      into lv_customer_id;
    close c_obtiene_customer_id;
  
    for w in c_obtiene_valores_llamadas loop
      for i in c_obtiene_mapeo loop
        ln_band1 := 0;
        ln_band2 := 0;
        ln_band3 := 0;
        --An�lisis tmcode
        if i.tmcode is not null and i.condicion_tmcode is not null then
          if i.condicion_tmcode = '=' then
            if i.tmcode = w.tmcode then
              ln_band1 := 1;
            else
              ln_band1 := 0;
            end if;
          elsif i.condicion_tmcode = '<>' then
            if i.tmcode <> w.tmcode then
              ln_band1 := 1;
            else
              ln_band1 := 0;
            end if;
          end if;
        else
          ln_band1 := 1;
        end if;
        --An�lisis sncode
        if i.sncode is not null and i.condicion_sncode is not null then
          if i.condicion_sncode = '=' then
            if i.sncode = w.sncode then
              ln_band2 := 1;
            else
              ln_band2 := 0;
            end if;
          elsif i.condicion_sncode = '<>' then
            if i.sncode <> w.sncode then
              ln_band2 := 1;
            else
              ln_band2 := 0;
            end if;
          end if;
        else
          ln_band2 := 1;
        end if;
        --An�lisis zncode
        if i.zncode is not null and i.condicion_zncode is not null then
          if i.condicion_zncode = '=' then
            if i.zncode = w.zncode then
              ln_band3 := 1;
            else
              ln_band3 := 0;
            end if;
          elsif i.condicion_zncode = '<>' then
            if i.zncode <> w.zncode then
              ln_band3 := 1;
            else
              ln_band3 := 0;
            end if;
          end if;
        else
          ln_band3 := 1;
        end if;
      
        if ln_band1 = 1 and ln_band2 = 1 and ln_band3 = 1 then
          begin
            Lt_ValoresRubros(i.label_id).valor := nvl(Lt_ValoresRubros(i.label_id)
                                                      .valor,
                                                      0) + nvl(w.valor,0);
            Lt_ValoresRubros(i.label_id).descuento := nvl(Lt_ValoresRubros(i.label_id)
                                                          .descuento,
                                                          0) + nvl(w.descuento,0);
            Lt_ValoresRubros(i.label_id).factura := w.ohrefnum;
          exception
            when others then
              Lt_ValoresRubros(i.label_id).valor := nvl(w.valor,0);
              Lt_ValoresRubros(i.label_id).descuento := nvl(w.descuento,0);
              Lt_ValoresRubros(i.label_id).factura := w.ohrefnum;
          end;
          ln_band := 1;
          exit;
        end if;
      
      end loop;
    
    end loop;
  
  --10182
   OPEN C_PARAMETRO(10182,'FECHA_INICIO_COBRO_IVA_ROAM');
    FETCH C_PARAMETRO
      INTO Lv_fecha_cobro_iva_roam;
    CLOSE C_PARAMETRO;
  --10182
  
    IF ln_band > 0 THEN
    
      FOR Ln_Contador IN Lt_ValoresRubros.FIRST .. Lt_ValoresRubros.LAST LOOP
      
        begin
        
          open c_obtiene_doc1(Ln_Contador);
          fetch c_obtiene_doc1
            into lc_obtiene_doc1;
          close c_obtiene_doc1;
        
        --INI 101812
         SELECT INSTR(upper(lc_obtiene_doc1.des),'ROAM') into ln_cont_roam FROM DUAL;
         if ln_cont_roam > 0 then
          if to_date(pv_fecha,'dd/mm/yyyy') < to_date(Lv_fecha_cobro_iva_roam,'dd/mm/yyyy') then
            LV_TIPO_WF:='004 - EXENTO';
          else
            LV_TIPO_WF:=lc_obtiene_doc1.tipo;
          END IF;
         else
            LV_TIPO_WF:=lc_obtiene_doc1.tipo;
         end if;
         
        --FIN 10182
        
          insert into WF_COMPLAIN_TEMPORAL
            (nombre2,
             valor,
             customer_id,
             custcode,
             ohrefnum,
             tipo,--
             nombre,
             servicio,
             descuento,
             cobrado,
             ctaclblep,
             cta_devol,
             tipo_iva)
          values
            (upper(lc_obtiene_doc1.des),
             Lt_ValoresRubros(Ln_Contador).valor,
             lv_customer_id,
             pv_cuenta,
             Lt_ValoresRubros(Ln_Contador).factura,
             LV_TIPO_WF,--10182
             lc_obtiene_doc1.des,
             -55,
             nvl(Lt_ValoresRubros(Ln_Contador).descuento, 0),
             Lt_ValoresRubros(Ln_Contador).valor - nvl(Lt_ValoresRubros(Ln_Contador).descuento, 0),
             lc_obtiene_doc1.ctaps,
             lc_obtiene_doc1.cta_devol,
             'I12');
        exception
          when others then
            null;
        end;
      END LOOP;
    
    END IF;
  
  exception
    when others then
      rollback;
      lv_error := substr(sqlerrm, 1, 100);
      pv_error := lv_error;
    
  end;

  /* Query que se actualmente se usa en la HC
      lv_query := 'SELECT X.NOMBRE2,
                         sum(X.VALOR) VALOR,
                         X.CUSTOMER_ID,
                         X.CUSTCODE,
                         X.OHREFNUM,
                         X.TIPO,
                         X.NOMBRE,
                         X.SERVICIO,
                         sum(X.DESCUENTO) DESCUENTO,
                         sum(X.COBRADO) COBRADO,
                         X.CTACLBLEP,
                         X.CTA_DEVOL,
                         X.SERVICIO SERVICE,
                         X.NOMBRE NAME,
                         X.NOMBRE2 SHORT_NAME,
                         X.TIPO TYPE
                    FROM (SELECT A.NOMBRE2,
                                 A.VALOR,
                                 A.CUSTOMER_ID,
                                 A.CUSTCODE,
                                 A.OHREFNUM,
                                 A.TIPO,
                                 TRIM(DECODE(A.SERVICIO,
                                             3,
                                             'I.V.A. Por servicios (12%)',
                                             B.DES)) NOMBRE,
                                 (select to_char(z.SERVICIO)
                                    from COB_SERVICIOS z
                                   where z.NOMBRE2 = a.NOMBRE2
                                     and rownum < 2) SERVICIO,
                                 A.DESCUENTO,
                                 A.VALOR - A.DESCUENTO COBRADO,
                                 A.CTACLBLEP,
                                 (select CTA_DEVOL
                                    from COB_SERVICIOS
                                   where SERVICIO = A.SERVICIO
                                     and CTACTBLE = A.CBLE) CTA_DEVOL
                            FROM SYSADM.co_fact_02012013 A, MPUSNTAB B
                           WHERE A.SERVICIO = B.SNCODE
                             AND A.CUSTCODE = '6.149074'
                             AND A.SERVICIO not in
                                 (select substr(otname,
                                                instr(otname, '.', 1, 3) + 1,
                                                instr(substr(otname,
                                                             instr(otname, '.', 1, 3) + 1),
                                                      '.') - 1)
                                    from ordertrailer a, orderhdr_all b, customer_all c
                                   where a.otshipdate = to_date('02012013', 'ddmmyyyy')
                                     and b.OHSTATUS IN ('IN', 'CM')
                                     and b.OHUSERID IS NULL
                                     and a.otshipdate = b.ohentdate
                                     and a.otxact = b.ohxact
                                     and b.customer_id = c.customer_id
                                     and c.custcode = '6.149074'
                                     and (otname like ('%.BASE.%') or otname like ('%.IC.%'))
                                   group by substr(otname,
                                                   instr(otname, '.', 1, 3) + 1,
                                                   instr(substr(otname,
                                                                instr(otname, '.', 1, 3) + 1),
                                                         '.') - 1))
                          UNION
                          SELECT UPPER(DESCRIPCION1),
                                 TO_NUMBER(VALOR, '999999999990.99') VALOR,
                                 (SELECT CUSTOMER_ID
                                    FROM CUSTOMER_ALL
                                   WHERE CUSTCODE = '6.149074'),
                                 CUENTA,
                                 (SELECT A.OHREFNUM
                                    FROM SYSADM.co_fact_02012013 A
                                   WHERE A.CUSTCODE = '6.149074'
                                     AND ROWNUM < 2),
                                 DECODE(UPPER(B.TAXINF),
                                        'EXCENTO',
                                        '004 - EXENTO',
                                        '002 - ADICIONALES'),
                                 TRIM(A.DESCRIPCION1),
                                 '-55',
                                 0,
                                 TO_NUMBER(VALOR, '999999999990.99'),
                                 B.CTAPS,
                                 B.CTA_DEVOL
                            FROM REGUN.bs_fact_ene@SMS A, DOC1.SERVICE_LABEL_DOC1 B   
                           WHERE UPPER(A.DESCRIPCION1) = UPPER(B.DES) 
                             AND A.CUENTA = '6.149074' 
                             AND A.LETRA = 'm') X 
                   GROUP BY X.NOMBRE2,
                            X.CUSTOMER_ID,
                            X.CUSTCODE,
                            X.OHREFNUM,
                            X.TIPO,
                            X.NOMBRE,
                            X.SERVICIO,
                            X.CTACLBLEP,
                            X.CTA_DEVOL,
                            X.SERVICIO,
                            X.NOMBRE,
                            X.NOMBRE2,
                            X.TIPO 
                   ORDER BY X.TIPO, X.SERVICIO;
    */

/* [10503]
Objetivo: Procedimiento para invocar nueva logica implementada para extraer datos adicionales de la co_fact
Invocado desde Java SqlComplainBean.
*/
PROCEDURE WFP_PRINCIPAL_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2) IS

  LV_ERROR VARCHAR2(4000);

BEGIN

  DELETE WF_COMPLAIN_TEMPORAL;
  COMMIT;

  WFK_COMPLAIN.WFP_OBTIENE_RUBROS_SERVICIOS_1(PV_FECHA, PV_CUENTA, LV_ERROR);
  IF LV_ERROR IS NOT NULL THEN 
    PV_ERROR := LV_ERROR;
    ROLLBACK;
    RETURN;
  END IF;
  
  WFK_COMPLAIN.WFP_OBTIENE_RUBROS_LLAMADAS_1(PV_FECHA, PV_CUENTA, LV_ERROR);
  IF LV_ERROR IS NOT NULL THEN 
    PV_ERROR := LV_ERROR;
    ROLLBACK;
    RETURN;
  END IF;
  
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := SUBSTR(SQLERRM,1,4000);
  
END WFP_PRINCIPAL_1;

/* [10503]
Objetivo: Procedimiento para obtener desde query principal datos adicionales de la co_fact.
*/
PROCEDURE WFP_OBTIENE_RUBROS_SERVICIOS_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2) IS

  -- cursor para traer bandera
  CURSOR C_BANDERA(CN_TIPO NUMBER, CV_PARAMETRO VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS
     WHERE ID_TIPO_PARAMETRO = CN_TIPO
       AND ID_PARAMETRO = CV_PARAMETRO;

  LV_QUERY VARCHAR2(32700);

  -- 10182 INI
  LV_FECHA_COBRO_IVA_ROAM GV_PARAMETROS.VALOR%TYPE;
  -- 10182 FIN

  LV_VALOR VARCHAR2(15); --10503
  LV_ERROR VARCHAR2(4000);
  LV_SENTENCIA_DEL VARCHAR2(4000);

BEGIN

  -- [10910] IVA 14 E ICE 15 NOTRAS DE CREDITO DE SDA
  IF NVL(WFK_COMPLAIN.WFF_OBTENER_VALOR_PARAMETRO(10910,'BAND_IVA'),'N')= 'S' THEN 
    -- SE OBTIENE LA SENTENCIA A INSERTAR
    LV_QUERY := WFK_COMPLAIN.WFF_OBTENER_VALOR_PARAMETRO(10910,'WF_SENTENCIA_INSERT');
    -- SE REEMPLAZA POR LA TABLA CORRESPONDIENTE
    LV_QUERY := REPLACE(LV_QUERY,'LV_TABLA_FACT','SYSADM.CO_FACT_'||PV_FECHA);
    -- EJECUCI�N DE LA SENTENCIA
    EXECUTE IMMEDIATE LV_QUERY USING PV_CUENTA,PV_FECHA,PV_CUENTA;
  ELSE 
  LV_QUERY := 'INSERT INTO WF_COMPLAIN_TEMPORAL(NOMBRE2,
                                                   VALOR,
                                                   CUSTOMER_ID,
                                                   CUSTCODE,
                                                   OHREFNUM,
                                                   TIPO,
                                                   NOMBRE,
                                                   SERVICIO,
                                                   DESCUENTO,
                                                   COBRADO,
                                                   CTACLBLEP,
                                                   CTA_DEVOL,
                                                   TIPO_IVA,
                                                   SERVICE_ID,
                                                   PRODUCT_ID,
                                                   ACCOUNT_ID,
                                                   BALANCE)
                 SELECT A.NOMBRE2,
                        A.VALOR,
                        A.CUSTOMER_ID,
                        A.CUSTCODE,
                        A.OHREFNUM,
                        A.TIPO,
                        TRIM(DECODE(A.SERVICIO, 3, ''I.V.A. Por servicios (12%)'',4,''ICE de Telecomunicaci�n (15%)'', B.DES)) NOMBRE,
                        (select to_char(z.SERVICIO)
                           from COB_SERVICIOS z
                          where z.servicio = a.servicio
                            and z.ctactble = a.cble) SERVICIO,
                        A.DESCUENTO,
                        A.VALOR - A.DESCUENTO COBRADO,
                        (select CTAPSOFT
                           from COB_SERVICIOS z
                          where z.servicio = a.servicio
                       and z.ctactble = a.cble) CTACLBLEP,
                        (select CTA_DEVOL
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) CTA_DEVOL,
                        (select TIPO_IVA
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) TIPO_IVA,
                          NVL(A.SERVICIO,1) SERVICE_ID, -- [10503] NUEVO ESQUEMA PARA PROCESOS MASIVO DE TRASPASO
                          (select ID_PRODUCTO
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) PRODUCT_ID,
                          A.CTACTBLE ACCOUNT_ID,
                          (select BALANCE
                           from COB_SERVICIOS
                          where SERVICIO = A.SERVICIO
                            and CTACTBLE = A.CBLE) BALANCE  -- [10503] NUEVO ESQUEMA PARA PROCESOS MASIVO DE TRASPASO  
                   FROM SYSADM.co_fact_' || PV_FECHA || ' A, MPUSNTAB B
                  WHERE A.SERVICIO = B.SNCODE
                    AND A.CUSTCODE = ''' || PV_CUENTA || '''
                    AND A.SERVICIO not in
              (select substr(otname,instr(otname, ''.'', 1, 3) + 1,instr(substr(otname, instr(otname, ''.'', 1, 3) + 1), ''.'') - 1)
                 from ordertrailer a, orderhdr_all b, customer_all c
                where a.otshipdate = to_date(''' || PV_FECHA || ''', ''ddmmyyyy'')
                  and b.OHSTATUS IN (''IN'', ''CM'')
                  and b.OHUSERID IS NULL
                  and a.otshipdate = b.ohentdate
                  and a.otxact = b.ohxact
                  and b.customer_id = c.customer_id
                  and c.custcode = ''' || PV_CUENTA || '''
                  and (otname like (''%.BASE.%'') or otname like (''%.IC.%''))
                group by substr(otname,instr(otname, ''.'', 1, 3) + 1,instr(substr(otname, instr(otname, ''.'', 1, 3) + 1),''.'') - 1))';

  EXECUTE IMMEDIATE LV_QUERY;

 END IF;
  -- [10910] IVA 14 E ICE 15 NOTRAS DE CREDITO DE SDA
  --10182 INI       
  OPEN C_PARAMETRO(10182, 'FECHA_INICIO_COBRO_IVA_ROAM');
  FETCH C_PARAMETRO
    INTO LV_FECHA_COBRO_IVA_ROAM;
  CLOSE C_PARAMETRO;

  IF TO_DATE(PV_FECHA, 'dd/mm/yyyy') < TO_DATE(LV_FECHA_COBRO_IVA_ROAM, 'dd/mm/yyyy') THEN
    UPDATE WF_COMPLAIN_TEMPORAL SET TIPO_IVA = 'EXP', TIPO = '004 - EXENTO' WHERE NOMBRE2 LIKE '%ROAM%';
  
  END IF;
  --10182 FIN

  UPDATE WF_COMPLAIN_TEMPORAL
     SET VALOR    =
         (SELECT SUM(VALOR)
            FROM WF_COMPLAIN_TEMPORAL
           WHERE TIPO = '003 - IMPUESTOS'
             AND NOMBRE2 LIKE 'IVA%'),
         DESCUENTO =
         (SELECT SUM(DESCUENTO)
            FROM WF_COMPLAIN_TEMPORAL
           WHERE TIPO = '003 - IMPUESTOS'
             AND NOMBRE2 LIKE 'IVA%'),
         COBRADO  =
         (SELECT SUM(COBRADO)
            FROM WF_COMPLAIN_TEMPORAL
           WHERE TIPO = '003 - IMPUESTOS'
             AND NOMBRE2 LIKE 'IVA%')
   WHERE TIPO = '003 - IMPUESTOS'
     AND NOMBRE2 = 'IVA';

   -- Se comenta para Parametrizar
  /*DELETE WF_COMPLAIN_TEMPORAL
   WHERE TIPO = '003 - IMPUESTOS'
     AND NOMBRE2 LIKE 'IVA_%';*/
  -- [10910] IVA 14 E ICE 15 NOTRAS DE CREDITO DE SDA
  IF NVL(WFK_COMPLAIN.WFF_OBTENER_VALOR_PARAMETRO(10910,'BAND_IVA'),'N')= 'S' THEN 
    -- SENTENCIA PARA ELIMINACI�N
    LV_SENTENCIA_DEL := WFK_COMPLAIN.WFF_OBTENER_VALOR_PARAMETRO(10910,'WF_SENTENCIA_DELETE');
    EXECUTE IMMEDIATE LV_SENTENCIA_DEL;
  END IF;     
  -- [10910] IVA 14 E ICE 15 NOTRAS DE CREDITO DE SDA
     
  --  10503
  WFP_VALIDA_RUBROS(LV_ERROR);
  IF LV_ERROR IS NOT NULL THEN 
    PV_ERROR := LV_ERROR;
  END IF;
  --  10503
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SQLERRM;
END WFP_OBTIENE_RUBROS_SERVICIOS_1;

-- 10503 :: Nuevo proceso para validar las facturas vs estados de cuentas
PROCEDURE WFP_VALIDA_PERIODOS(PV_PERIODOS         IN VARCHAR2,
                              PV_CUENTA           IN VARCHAR2,
                              PV_PERIODOS_VALIDOS OUT VARCHAR2,
                              PV_FACTURAS         OUT VARCHAR2,
                              PV_ERROR            OUT VARCHAR2) IS

  -- CURSOR PARA VERIFICAR PERIODOS VALIDOS
  LN_CANTIDAD      NUMBER;
  LN_CONTADOR      NUMBER := 0;
  LV_PERIODO       VARCHAR2(4000);
  LV_DATO          VARCHAR2(4000);
  LV_SENTENCIA     VARCHAR2(4000);
  LV_FACTURA       VARCHAR2(4000);
  LV_APLICACION    VARCHAR2(500) := 'WFK_COMPLAIN.WFP_VALIDA_PERIODOS';
  LN_CANTIDAD_FACT NUMBER := 0;
BEGIN
  LN_CANTIDAD := WFF_CONTARCADENA(PV_PERIODOS, ',');
  LV_DATO := PV_PERIODOS;

  WHILE LN_CONTADOR <= LN_CANTIDAD LOOP
    IF LV_DATO NOT LIKE '%,%' THEN
      LV_PERIODO := LV_DATO;
    ELSE
      LV_PERIODO := SUBSTR(LV_DATO, 1, INSTR(LV_DATO, ',') - 1);
      LV_DATO    := SUBSTR(LV_DATO, INSTR(LV_DATO, ',') + 1);
    END IF;
    LV_SENTENCIA := 'SELECT DISTINCT OHREFNUM FROM  CO_FACT_' || LV_PERIODO || ' WHERE CUSTCODE = :1';
    EXECUTE IMMEDIATE LV_SENTENCIA
      INTO LV_FACTURA
      USING PV_CUENTA;
    LN_CANTIDAD_FACT := WFF_CONTARCADENA(LV_FACTURA, '-');
    IF LN_CANTIDAD_FACT = 2 THEN
      IF PV_PERIODOS_VALIDOS IS NULL THEN
        PV_PERIODOS_VALIDOS := LV_PERIODO;
      ELSE
        PV_PERIODOS_VALIDOS := PV_PERIODOS_VALIDOS || ',' || LV_PERIODO;
      END IF;
    END IF;
    IF PV_FACTURAS IS NULL THEN
      PV_FACTURAS := LV_FACTURA;
    ELSE
      PV_FACTURAS := PV_FACTURAS || ',' || LV_FACTURA;
    END IF;
    LV_PERIODO  := NULL;
    LN_CONTADOR := LN_CONTADOR + 1;
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := LV_APLICACION || ' ' || SUBSTR(SQLERRM, 1, 100);
END WFP_VALIDA_PERIODOS;


/*
Proyecto    : [10503] Nuevo Esquema Para Proceso Masivo de Traspaso
Lider SIS   : Diana Chonillo
Lider PDS   : RGT Fernando Ortega
Recurso PDS : RGT Josue Rocafuerte
Fecha       : 01/02/2016
Objetivo    : Obtener Producto para Rubros de llamadas para las cuentas a devolver.
*/

PROCEDURE WFP_OBTIENE_RUBROS_LLAMADAS_1(PV_FECHA VARCHAR2, PV_CUENTA VARCHAR2, PV_ERROR OUT VARCHAR2) IS

  CURSOR C_OBTIENE_VALORES_LLAMADAS IS
    SELECT SUBSTR(OTNAME, 1, INSTR(OTNAME, '.', '1', '1') - 1) TMCODE,
           SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1, INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1), '.') - 1) SNCODE,
           SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 11) + 1, INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 11) + 1), '.') - 1) ZNCODE,
           SUM(A.OTAMT_REVENUE_GL) VALOR,
           SUM(A.OTAMT_DISC_DOC) DESCUENTO,
           B.OHREFNUM
      FROM ORDERTRAILER A, ORDERHDR_ALL B, CUSTOMER_ALL C
     WHERE A.OTSHIPDATE = TO_DATE(PV_FECHA, 'ddmmyyyy')
       AND B.OHSTATUS IN ('IN', 'CM')
       AND B.OHUSERID IS NULL
       AND A.OTSHIPDATE = B.OHENTDATE
       AND A.OTXACT = B.OHXACT
       AND B.CUSTOMER_ID = C.CUSTOMER_ID
       AND C.CUSTCODE = PV_CUENTA
       AND (OTNAME LIKE ('%.BASE.%') OR OTNAME LIKE ('%.IC.%'))
     GROUP BY SUBSTR(OTNAME, 1, INSTR(OTNAME, '.', '1', '1') - 1),
              SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1, INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1), '.') - 1),
              SUBSTR(OTNAME,
                     INSTR(OTNAME, '.', 1, 11) + 1,
                     INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 11) + 1), '.') - 1),
              B.OHREFNUM;

  CURSOR C_OBTIENE_MAPEO IS
    SELECT TMCODE, SNCODE, ZNCODE, CONDICION_TMCODE, CONDICION_SNCODE, CONDICION_ZNCODE, LABEL_ID
      FROM WF_MAPEO_RUBROS_LLAMADAS
     WHERE ESTADO = 'A'
     ORDER BY LABEL_ID;

  CURSOR C_OBTIENE_DOC1(CN_LABEL_ID NUMBER) IS
    SELECT DES,
           DECODE(UPPER(TAXINF), 'EXCENTO', '004 - EXENTO', '002 - ADICIONALES') TIPO,
           CTAPS,
           CTA_DEVOL,
           ID_PRODUCTO --- 10503 JRF
      FROM DOC1.SERVICE_LABEL_DOC1
     WHERE LABEL_ID = CN_LABEL_ID;

  CURSOR C_OBTIENE_CUSTOMER_ID IS
    SELECT CUSTOMER_ID FROM CUSTOMER_ALL WHERE CUSTCODE = PV_CUENTA;

  LV_ERROR         VARCHAR2(1000);
  LV_CUSTOMER_ID   VARCHAR2(100);
  LN_BAND          NUMBER := 0;
  LN_BAND1         NUMBER := 0;
  LN_BAND2         NUMBER := 0;
  LN_BAND3         NUMBER := 0;
  LC_OBTIENE_DOC1  C_OBTIENE_DOC1%ROWTYPE;
  LT_VALORESRUBROS GT_VALORESRUBROS;
  --10182 INICIO
  LV_FECHA_COBRO_IVA_ROAM GV_PARAMETROS.VALOR%TYPE;
  LV_TIPO_WF              VARCHAR2(25);
  LN_CONT_ROAM            NUMBER;
  --10182 FIN

BEGIN

  OPEN C_OBTIENE_CUSTOMER_ID;
  FETCH C_OBTIENE_CUSTOMER_ID
    INTO LV_CUSTOMER_ID;
  CLOSE C_OBTIENE_CUSTOMER_ID;

  FOR W IN C_OBTIENE_VALORES_LLAMADAS LOOP
    FOR I IN C_OBTIENE_MAPEO LOOP
      LN_BAND1 := 0;
      LN_BAND2 := 0;
      LN_BAND3 := 0;
      --An�lisis tmcode
      IF I.TMCODE IS NOT NULL AND I.CONDICION_TMCODE IS NOT NULL THEN
        IF I.CONDICION_TMCODE = '=' THEN
          IF I.TMCODE = W.TMCODE THEN
            LN_BAND1 := 1;
          ELSE
            LN_BAND1 := 0;
          END IF;
        ELSIF I.CONDICION_TMCODE = '<>' THEN
          IF I.TMCODE <> W.TMCODE THEN
            LN_BAND1 := 1;
          ELSE
            LN_BAND1 := 0;
          END IF;
        END IF;
      ELSE
        LN_BAND1 := 1;
      END IF;
      --An�lisis sncode
      IF I.SNCODE IS NOT NULL AND I.CONDICION_SNCODE IS NOT NULL THEN
        IF I.CONDICION_SNCODE = '=' THEN
          IF I.SNCODE = W.SNCODE THEN
            LN_BAND2 := 1;
          ELSE
            LN_BAND2 := 0;
          END IF;
        ELSIF I.CONDICION_SNCODE = '<>' THEN
          IF I.SNCODE <> W.SNCODE THEN
            LN_BAND2 := 1;
          ELSE
            LN_BAND2 := 0;
          END IF;
        END IF;
      ELSE
        LN_BAND2 := 1;
      END IF;
      --An�lisis zncode
      IF I.ZNCODE IS NOT NULL AND I.CONDICION_ZNCODE IS NOT NULL THEN
        IF I.CONDICION_ZNCODE = '=' THEN
          IF I.ZNCODE = W.ZNCODE THEN
            LN_BAND3 := 1;
          ELSE
            LN_BAND3 := 0;
          END IF;
        ELSIF I.CONDICION_ZNCODE = '<>' THEN
          IF I.ZNCODE <> W.ZNCODE THEN
            LN_BAND3 := 1;
          ELSE
            LN_BAND3 := 0;
          END IF;
        END IF;
      ELSE
        LN_BAND3 := 1;
      END IF;
    
      IF LN_BAND1 = 1 AND LN_BAND2 = 1 AND LN_BAND3 = 1 THEN
        BEGIN
          LT_VALORESRUBROS(I.LABEL_ID).VALOR := NVL(LT_VALORESRUBROS(I.LABEL_ID).VALOR, 0) + NVL(W.VALOR, 0);
          LT_VALORESRUBROS(I.LABEL_ID).DESCUENTO := NVL(LT_VALORESRUBROS(I.LABEL_ID).DESCUENTO, 0) +
                                                    NVL(W.DESCUENTO, 0);
          LT_VALORESRUBROS(I.LABEL_ID).FACTURA := W.OHREFNUM;
        EXCEPTION
          WHEN OTHERS THEN
            LT_VALORESRUBROS(I.LABEL_ID).VALOR := NVL(W.VALOR, 0);
            LT_VALORESRUBROS(I.LABEL_ID).DESCUENTO := NVL(W.DESCUENTO, 0);
            LT_VALORESRUBROS(I.LABEL_ID).FACTURA := W.OHREFNUM;
        END;
        LN_BAND := 1;
        EXIT;
      END IF;
    
    END LOOP;
  
  END LOOP;

  --10182
  OPEN C_PARAMETRO(10182, 'FECHA_INICIO_COBRO_IVA_ROAM');
  FETCH C_PARAMETRO
    INTO LV_FECHA_COBRO_IVA_ROAM;
  CLOSE C_PARAMETRO;
  --10182

  IF LN_BAND > 0 THEN
  
    FOR LN_CONTADOR IN LT_VALORESRUBROS.FIRST .. LT_VALORESRUBROS.LAST LOOP
    
      BEGIN
      
        OPEN C_OBTIENE_DOC1(LN_CONTADOR);
        FETCH C_OBTIENE_DOC1
          INTO LC_OBTIENE_DOC1;
        CLOSE C_OBTIENE_DOC1;
      
        --INI 101812
        SELECT INSTR(UPPER(LC_OBTIENE_DOC1.DES), 'ROAM') INTO LN_CONT_ROAM FROM DUAL;
        IF LN_CONT_ROAM > 0 THEN
          IF TO_DATE(PV_FECHA, 'dd/mm/yyyy') < TO_DATE(LV_FECHA_COBRO_IVA_ROAM, 'dd/mm/yyyy') THEN
            LV_TIPO_WF := '004 - EXENTO';
          ELSE
            LV_TIPO_WF := LC_OBTIENE_DOC1.TIPO;
          END IF;
        ELSE
          LV_TIPO_WF := LC_OBTIENE_DOC1.TIPO;
        END IF;
      
        --FIN 10182
      
        INSERT INTO WF_COMPLAIN_TEMPORAL
          (NOMBRE2,
           VALOR,
           CUSTOMER_ID,
           CUSTCODE,
           OHREFNUM,
           TIPO, --
           NOMBRE,
           SERVICIO,
           DESCUENTO,
           COBRADO,
           CTACLBLEP,
           CTA_DEVOL,
           TIPO_IVA,
           PRODUCT_ID,--10503
           ACCOUNT_ID)-- 11020
        VALUES
          (UPPER(LC_OBTIENE_DOC1.DES),
           LT_VALORESRUBROS(LN_CONTADOR).VALOR,
           LV_CUSTOMER_ID,
           PV_CUENTA,
           LT_VALORESRUBROS(LN_CONTADOR).FACTURA,
           LV_TIPO_WF, --10182
           LC_OBTIENE_DOC1.DES,
           -55,
           NVL(LT_VALORESRUBROS(LN_CONTADOR).DESCUENTO, 0),
           LT_VALORESRUBROS(LN_CONTADOR).VALOR - NVL(LT_VALORESRUBROS(LN_CONTADOR).DESCUENTO, 0),
           LC_OBTIENE_DOC1.CTAPS,
           LC_OBTIENE_DOC1.CTA_DEVOL,
           'I12',
           LC_OBTIENE_DOC1.ID_PRODUCTO, --10503
           '0'); -- 11020
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END LOOP;
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    LV_ERROR := SUBSTR(SQLERRM, 1, 100);
    PV_ERROR := LV_ERROR;
  
END WFP_OBTIENE_RUBROS_LLAMADAS_1;

PROCEDURE WFP_VALIDA_RUBROS(PV_ERROR OUT VARCHAR2) IS
  -- CURSOR PARA OBTENER LOS RUBROS CON MAS DE UNA COLUMNA
  CURSOR C_LINEA IS
    SELECT NOMBRE, SERVICE_ID, COUNT(*) CANTIDAD FROM WF_COMPLAIN_TEMPORAL GROUP BY NOMBRE, SERVICE_ID;
  -- CURSOR PARA OBTENER LAS CUENTA Y SERVICIO 
  CURSOR C_DATOS(CV_NOMBRE VARCHAR2) IS
    SELECT ACCOUNT_ID, SERVICE_ID , BALANCE -- [10808] SDA EN L�NEA
      FROM WF_COMPLAIN_TEMPORAL
     WHERE NOMBRE = CV_NOMBRE
       AND ROWNUM = 1;

  LC_DATOS      C_DATOS%ROWTYPE;
  LV_APLICACION VARCHAR2(500) := 'WFK_COMPLAIN.WFP_VALIDA_RUBROS';
BEGIN
  FOR I IN C_LINEA LOOP
    IF I.CANTIDAD > 1 THEN
      OPEN C_DATOS(I.NOMBRE);
      FETCH C_DATOS
        INTO LC_DATOS;
      CLOSE C_DATOS;    
      UPDATE WF_COMPLAIN_TEMPORAL
         SET SERVICIO = LC_DATOS.SERVICE_ID, SERVICE_ID = LC_DATOS.SERVICE_ID, ACCOUNT_ID = LC_DATOS.ACCOUNT_ID , BALANCE = LC_DATOS.BALANCE
       WHERE NOMBRE = I.NOMBRE
         AND SERVICE_ID = I.SERVICE_ID;--20180424 11749 Actualiza rubro por service_ID
    END IF;
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR AL ACTUALIZAR : ' || LV_APLICACION || ' ' || SUBSTR(SQLERRM, 1, 100);  
END WFP_VALIDA_RUBROS;
--- 10910 
FUNCTION WFF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO IN NUMBER,
                                      PV_ID_PARAMETRO IN VARCHAR2) RETURN VARCHAR2 IS

 CURSOR C_PARAMETRO (CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
 SELECT VALOR FROM WF_PARAMETROS 
  WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
    AND ID_PARAMETRO = CV_ID_PARAMETRO;

 LV_VALOR WF_PARAMETROS.VALOR%TYPE;

BEGIN

 OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
 FETCH C_PARAMETRO INTO LV_VALOR;
 CLOSE C_PARAMETRO;

 RETURN LV_VALOR;

END WFF_OBTENER_VALOR_PARAMETRO;

--- 10910
end WFK_COMPLAIN;
/
