CREATE OR REPLACE package PCK_OBTIENE_MUESTRA_CLIENTE is

  -- Author  : AAPOLOZ
  -- Created : 03/08/2007 9:05:11
  -- Purpose : Obtener los datos de la customer_all para desarrollo
 Function Cuenta_Cliente return number;
 Function Existe_Cliente_Padre (pn_customer_id in number) return varchar2;
 Function Existe_Cliente_Hijo  (pn_customer_id_high in number) return varchar2;
 Procedure Obtiene_data;
 Procedure Obtiene_data_vip;


end PCK_OBTIENE_MUESTRA_CLIENTE;
/
CREATE OR REPLACE package body PCK_OBTIENE_MUESTRA_CLIENTE is
 
 Function Cuenta_Cliente return number  is
     ln_cuantos  number;
    
 begin
  begin
    select count(*)
    into ln_cuantos
    from customer_all_aap;
    Exception when no_data_found then
        ln_cuantos:=0;
  end;   
     return ln_cuantos;
     
 end Cuenta_Cliente;
 
 Function Existe_Cliente_Padre (pn_customer_id in number) return varchar2  is
     lv_existe      varchar2(1);
    
 begin
  begin
    select 'S'
    into lv_existe
    from customer_all_aap
    where customer_id=pn_customer_id; 
    Exception when no_data_found then
        lv_existe:='N';
  end;   
     return lv_existe;
     
 end Existe_Cliente_padre;

 Function Existe_Cliente_Hijo (pn_customer_id_high in number) return varchar2  is
     lv_existe      varchar2(1);
    
 begin
  begin
    select 'S'
    into lv_existe
    from customer_all_aap
    where customer_id_high=pn_customer_id_high; 
    Exception when no_Data_found then
        lv_existe:='N';
  end;   
     return lv_existe;
     
 end Existe_Cliente_Hijo;
 
 Procedure Obtiene_data is

  Cursor cuenta is
  select a.campo1 customer_id,a.campo2 tmcode,campo3 estado
  from aap a
  where a.campo3 is null
  order by campo2;

  lv_inserto varchar2(1);
  ln_act  number;
  ln_ant  number;
  ln_cont number;
begin
  
  ln_act:=0;
  ln_ant:=0;
  ln_cont:=0;
  lv_inserto:=' ';
  for i in cuenta loop
    ln_cont:=ln_cont+1;

    if ln_cont=1 then
       ln_act:=i.tmcode;    
       ln_ant:=ln_act;
    else 
       if ln_ant<>i.tmcode then
          ln_cont:=0;
          ln_act:=i.tmcode;
          ln_ant:=ln_act;
       end if;

    end if;
    
     if (ln_cont<70)and(ln_act=ln_ant) and  cuenta_cliente<56000 then
     --hay q tomar el registro de la customer_all cta padre e hija de produccio
     --e insertarla en la customer_all de la bscsii
     --insertar o tomar muestra}
       if Existe_Cliente_Padre (i.customer_id)='N' then
           --begin
             insert into customer_all_aap
             select * from customer_all where customer_id=i.customer_id;
           --end;
           lv_inserto:='S';
       end if;
       
       if Existe_Cliente_hijo (i.customer_id)='N' then
           --begin
             insert into customer_all_aap
             select * from customer_all where customer_id_high=i.customer_id; 
           --end;
           lv_inserto:='S';
       end if;
       if lv_inserto='S' then
         update aap set campo3='P' where campo1=i.customer_id and campo2=i.tmcode;
         lv_inserto:='N';
       end if; 
     commit;  
     end if; 
  end loop;
    
end Obtiene_data;

Procedure Obtiene_data_vip is

Begin
  insert into customer_all_aap
  select * from customer_all where billcycle in (34,33,38,17,19,20,26,16)
  and customer_id_high is null and customer_id not in (select customer_id from customer_all_aap);
end Obtiene_data_vip;

end PCK_OBTIENE_MUESTRA_CLIENTE;
/

