CREATE OR REPLACE PACKAGE MFK_OBJ_MENSAJES
 /* 
 ||   Name       : MFK_OBJ_MENSAJES
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */ 
 IS 
 /*HELP 
   Overview of MFK_OBJ_MENSAJES
   HELP*/ 
 
 /*EXAMPLES 
   Examples of test 
   EXAMPLES*/ 
 
 /* Constants */ 
 
 /* Exceptions */ 
 
 /* Variables */ 
 
 /* Toggles */ 
 
 /* Windows */ 
 
 /* Programs */ 
 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL); 

PROCEDURE MFP_INSERTAR (PN_IDENVIO           IN MF_MENSAJES.IDENVIO%TYPE,
                        PV_MENSAJE           IN MF_MENSAJES.MENSAJE%TYPE,
                        PN_SECUENCIA_ENVIO   IN MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                        PV_REMITENTE         IN MF_MENSAJES.REMITENTE%TYPE,
                        PV_DESTINATARIO      IN MF_MENSAJES.DESTINATARIO%TYPE,
                        PV_COPIA             IN MF_MENSAJES.COPIA%TYPE,
                        PV_COPIA_OCULTA      IN MF_MENSAJES.COPIAOCULTA%TYPE,
                        PV_ASUNTO            IN MF_MENSAJES.ASUNTO%TYPE,
                        PN_HILO                IN MF_MENSAJES.HILO%TYPE,
                        PV_MSGERROR          IN OUT VARCHAR2
                       --
                        );
--
--
PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO           IN MF_MENSAJES.IDENVIO%TYPE,
                         PV_MENSAJE           IN MF_MENSAJES.MENSAJE%TYPE,
                         PN_SECUENCIA_ENVIO   IN MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                         PN_SECUENCIA_MENSAJE IN MF_MENSAJES.SECUENCIA_MENSAJE%TYPE,
                         PV_REMITENTE         IN MF_MENSAJES.REMITENTE%TYPE,
                         PV_DESTINATARIO      IN MF_MENSAJES.DESTINATARIO%TYPE,
                         PV_COPIA             IN MF_MENSAJES.COPIA%TYPE,
                         PV_COPIA_OCULTA      IN MF_MENSAJES.COPIAOCULTA%TYPE,
                         PV_ASUNTO            IN MF_MENSAJES.ASUNTO%TYPE,
                         PV_ESTADO            IN MF_MENSAJES.ESTADO%TYPE,
                         PV_MSGERROR          IN OUT  VARCHAR2
                       --
                        );


PROCEDURE MFP_OBJ_ELIMINA_MASIVO(PN_IDENVIO           MF_MENSAJES.IDENVIO%TYPE,
                                 PN_SECUENCIA_ENVIO   MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                                 PV_MSGERROR         IN OUT VARCHAR2);                       
                        
end MFK_OBJ_MENSAJES;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_MENSAJES

 /* 
 ||   Name       : MFK_OBJ_MENSAJES
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 IS 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL) is 
BEGIN
 NULL;
END MFP_HELP; 



PROCEDURE MFP_INSERTAR (PN_IDENVIO           IN MF_MENSAJES.IDENVIO%TYPE,
                        PV_MENSAJE           IN MF_MENSAJES.MENSAJE%TYPE,
                        PN_SECUENCIA_ENVIO   IN MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                        PV_REMITENTE         IN MF_MENSAJES.REMITENTE%TYPE,
                        PV_DESTINATARIO      IN MF_MENSAJES.DESTINATARIO%TYPE,
                        PV_COPIA             IN MF_MENSAJES.COPIA%TYPE,
                        PV_COPIA_OCULTA      IN MF_MENSAJES.COPIAOCULTA%TYPE,
                        PV_ASUNTO            IN MF_MENSAJES.ASUNTO%TYPE,
                        PN_HILO                IN MF_MENSAJES.HILO%TYPE,
                        PV_MSGERROR          IN OUT  Varchar2
                        
                       --
                        ) 
IS  
 /* 
 ||   Name       : MFP_INSERTAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_MENSAJES.MFP_INSERTAR';
 LN_SECUENCIA_MENSAJE       MF_MENSAJES.SECUENCIA_MENSAJE%TYPE;
 PV_ESTADO                  MF_MENSAJES.ESTADO%TYPE := 'A';
 
BEGIN

 SELECT MFS_ENVIOMENSAJE_SEC.NEXTVAL INTO LN_SECUENCIA_MENSAJE  FROM dual;

 INSERT INTO MF_MENSAJES(IDENVIO,MENSAJE,SECUENCIA_ENVIO,SECUENCIA_MENSAJE,REMITENTE,
                         DESTINATARIO,COPIA,COPIAOCULTA,ASUNTO,ESTADO,HILO)
 VALUES 
       (PN_IDENVIO,PV_MENSAJE,PN_SECUENCIA_ENVIO,
        LN_SECUENCIA_MENSAJE,PV_REMITENTE,
        PV_DESTINATARIO,PV_COPIA,PV_COPIA_OCULTA,
        PV_ASUNTO,PV_ESTADO,PN_HILO);

EXCEPTION -- Exception 
WHEN OTHERS THEN
   PV_MSGERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
END MFP_INSERTAR;

PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO           IN MF_MENSAJES.IDENVIO%TYPE,
                         PV_MENSAJE           IN MF_MENSAJES.MENSAJE%TYPE,
                         PN_SECUENCIA_ENVIO   IN MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                         PN_SECUENCIA_MENSAJE IN MF_MENSAJES.SECUENCIA_MENSAJE%TYPE,
                         PV_REMITENTE         IN MF_MENSAJES.REMITENTE%TYPE,
                         PV_DESTINATARIO      IN MF_MENSAJES.DESTINATARIO%TYPE,
                         PV_COPIA             IN MF_MENSAJES.COPIA%TYPE,
                         PV_COPIA_OCULTA      IN MF_MENSAJES.COPIAOCULTA%TYPE,
                         PV_ASUNTO            IN MF_MENSAJES.ASUNTO%TYPE,
                         PV_ESTADO            IN MF_MENSAJES.ESTADO%TYPE,
                         PV_MSGERROR          IN OUT  VARCHAR2
                       --
                        ) 
IS  
 /* 
 ||   Name       : MFP_ACTUALIZAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 CURSOR C_table_cur(CN_IDENVIO           NUMBER,
                    CN_SECUENCIA_ENVIO   NUMBER,
                    CN_SECUENCIA_MENSAJE NUMBER) IS 
    SELECT * FROM MF_MENSAJES M
    WHERE m.secuencia_mensaje = cn_secuencia_mensaje
    AND   m.secuencia_envio = cn_secuencia_envio
    AND   m.idenvio = cn_idEnvio;
 Lc_CurrentRow     MF_MENSAJES%ROWTYPE ;
 Le_NoDataUpdated  EXCEPTION;

 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_MENSAJES.MFP_ACTUALIZAR';
BEGIN
       PV_MSGERROR := NULL; 
       OPEN c_table_cur(PN_IDENVIO,PN_SECUENCIA_ENVIO,PN_SECUENCIA_MENSAJE);
       FETCH c_table_cur INTO Lc_CurrentRow;
       IF C_table_cur%NOTFOUND THEN 
          RAISE Le_NoDataUpdated ;
       END IF;
       
       UPDATE mf_mensajes 
       SET  mensaje      = nvl(PV_MENSAJE,mensaje)
            ,remitente    = nvl(PV_REMITENTE,remitente)
            ,destinatario = nvl(PV_DESTINATARIO,destinatario)
            ,copia        = nvl(PV_COPIA,copia)
            ,copiaoculta  = nvl(PV_COPIA_OCULTA,copiaoculta)
            ,asunto       = nvl(PV_ASUNTO,asunto)
            ,estado       = nvl(PV_ESTADO,estado)    
      WHERE idenvio           = PN_IDENVIO
      AND   secuencia_envio   = PN_SECUENCIA_ENVIO
      AND   secuencia_mensaje = PN_SECUENCIA_MENSAJE;

      CLOSE c_table_cur;            
                  
EXCEPTION -- Exception 
WHEN Le_NoDataUpdated THEN
    PV_MSGERROR := 'No se encontro el registro que desea actualizar. '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
WHEN OTHERS THEN
    PV_MSGERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END MFP_ACTUALIZAR;                  


PROCEDURE MFP_OBJ_ELIMINA_MASIVO(PN_IDENVIO           MF_MENSAJES.IDENVIO%TYPE,
                                 PN_SECUENCIA_ENVIO   MF_MENSAJES.SECUENCIA_ENVIO%TYPE,
                                 PV_MSGERROR          IN OUT VARCHAR2)IS

    CURSOR C_table_cur(CN_IDENVIO           NUMBER,
                       CN_SECUENCIA_ENVIO   NUMBER) IS 
        SELECT 1 FROM MF_MENSAJES M
        
        WHERE  m.secuencia_envio = CN_SECUENCIA_ENVIO
        AND    m.idenvio = CN_IDENVIO
        AND    ROWNUM = 1;
        
         
    ln_existe varchar2(1);
    Le_NoDataDeleted  EXCEPTION;
    LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_MENSAJES.MFP_OBJ_ELIMINA_MASIVO';                                 
                                 
BEGIN
 
     OPEN c_table_cur(PN_IDENVIO,PN_SECUENCIA_ENVIO);
     FETCH c_table_cur INTO ln_existe;
     IF C_table_cur%NOTFOUND THEN 
         RAISE Le_NoDataDeleted;
     END IF;

     DELETE FROM MF_MENSAJES M
     WHERE M.IDENVIO = PN_IDENVIO
     AND M.SECUENCIA_ENVIO = PN_SECUENCIA_ENVIO
     AND M.ESTADO = 'I';
    
     CLOSE C_table_cur;
     
EXCEPTION -- Exception 
WHEN Le_NoDataDeleted THEN
    PV_MSGERROR := 'El envio no Existe o no se ha Ejecutado. '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
WHEN OTHERS THEN
    PV_MSGERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END;


end MFK_OBJ_MENSAJES;
/
