CREATE OR REPLACE package COK_REPORT_CAPAS2 is

    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_lvMensErr       out  varchar2);
    PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                         pdFech_fin in date);
    PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                         pdFech_fin in date);
    PROCEDURE BALANCE_EN_CAPAS_PAGOS ( pdFechCierrePeriodo  date,
                                     pd_lvMensErr       out  varchar2  );
    PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date);
    FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                           pv_region      in varchar2,
                           pn_monto       out number,
                           pv_error       out varchar2)
                           RETURN NUMBER;
    PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date);
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER;
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER;
    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER;
    PROCEDURE MAIN (pdFech_ini in date,
                    pdFech_fin in date);
    PROCEDURE INDEX2;                    

end COK_REPORT_CAPAS2;
/

