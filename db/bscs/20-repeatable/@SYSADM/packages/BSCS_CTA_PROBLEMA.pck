CREATE OR REPLACE package        bscs_cta_problema is

PROCEDURE BSCS_CUENTAS_PRINCIPAL;
PROCEDURE BSCS_CUENTAS_PROBLEMA(pd_fecha IN DATE);

end bscs_cta_problema;
/
CREATE OR REPLACE package body        bscs_cta_problema  is
PROCEDURE BSCS_CUENTAS_PRINCIPAL is
--==========================================================================--
-- Versión:              1.0.0
-- Descripción: 
--==========================================================================--
-- Desarrollado por:  CLS-Darwin Rosero .
-- Fecha de actualización: 17/06/2005.
--==========================================================================--

BEGIN
   BSCS_CUENTAS_PROBLEMA(SYSDATE);
 END;
--------------------------------------
--------------------------------------
PROCEDURE BSCS_CUENTAS_PROBLEMA(pd_fecha IN DATE) is

CURSOR C_CICLOS_BSCS IS
SELECT t.billcycle FROM BILLCYCLES t;

CURSOR CUENTAS_PROBLEMA (cd_fecha_ini DATE,
                         cd_fecha_fin DATE,
                         cv_ciclo_bscs varchar2) IS
/*SELECT  \*+ RULE +*\ a.fp,a.des1,a.des2,a.termcode,
       b.CUSTOMER_ID,b.PAYMENT_TYPE
FROM   customer_all c,
       payment_all b,
       qc_fp a
WHERE  b.CUSTOMER_ID = c.CUSTOMER_ID
AND    b.ACT_USED    = 'X'
AND    a.FP          = b.BANK_ID
AND    a.id_ciclo    = c.billcycle
AND    a.TERMCODE    <> c.TERMCODE;
--AND   b.MODDATE BETWEEN cd_fecha_ini AND cd_fecha_fin; */  
 
SELECT  /*+ RULE +*/ a.fp,a.des1,a.des2,a.termcode,
       b.CUSTOMER_ID,b.PAYMENT_TYPE
FROM   customer_all c,
       payment_all b,
       qc_fp a       
WHERE  b.CUSTOMER_ID    = c.CUSTOMER_ID
AND    b.ACT_USED       = 'X'
AND    a.FP             = b.BANK_ID
AND    (a.id_ciclo, c.billcycle) = (select p.id_ciclo,p.id_ciclo_admin from fa_ciclos_axis_bscs p where  p.id_ciclo_admin = '26')
AND    a.TERMCODE       <> nvl(c.TERMCODE,0);

---------Variables-------------
lv_fecha_ini  VARCHAR2(30);
lv_fecha_fin  VARCHAR (30);
ld_fecha_ini  DATE;
ld_fecha_fin  DATE;
 
BEGIN
ld_fecha_ini:=pd_fecha;
lv_fecha_ini:=TO_CHAR(ld_fecha_ini);

SELECT TO_CHAR(SYSDATE) INTO lv_fecha_ini  FROM dual;
lv_fecha_ini:=lv_fecha_ini||' '||'00'||':'||'00'||':'||'00';
ld_fecha_ini:=TO_DATE(lv_fecha_ini,'dd-mm-yyyy hh24:mi:ss');

lv_fecha_fin:=TO_CHAR(pd_fecha);
lv_fecha_fin:=lv_fecha_fin||' '||'23'||':'||'59'||':'||'59';
ld_fecha_fin:=TO_DATE(lv_fecha_fin,'dd-mm-yyyy hh24:mi:ss');

FOR k IN C_CICLOS_BSCS LOOP 
    FOR i IN CUENTAS_PROBLEMA(ld_fecha_ini,ld_fecha_fin,k.billcycle) LOOP    
         UPDATE  CUSTOMER_ALL nologging
         SET termcode=i.termcode
         WHERE customer_id=i.customer_id;
         COMMIT;
    END LOOP;
END LOOP;    

END;
end  bscs_cta_problema ;
/

