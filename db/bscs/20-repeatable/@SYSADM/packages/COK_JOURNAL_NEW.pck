CREATE OR REPLACE PACKAGE COK_JOURNAL_NEW IS
  --===========================================================================================================================
  -- Author  : SUD Vanessa Herdoiza -- SUD Erick Poveda -- SIS Galo Jerez
  -- Created : 19/02/2009 14:42:33
  -- Purpose : [4219] Mejoras al Reporte Journal
  --===========================================================================================================================
  -- Idea de SIS GJE
  gv_key constant varchar2(8) := 'portasis';
  TYPE Tab_Journal IS TABLE OF VARCHAR2(400) INDEX BY BINARY_INTEGER;
  
  PROCEDURE Cop_Generar_MV(Pn_Customer IN NUMBER,
                           Pv_Region   IN VARCHAR2,
                           Pv_Plan     IN VARCHAR2,
                           Pv_FechaIni IN VARCHAR2,
                           Pv_FechaFin IN VARCHAR2,
                           Pv_Ciclo    IN VARCHAR2,
                           Pv_Clase    IN VARCHAR2,
                           Pv_FormaPa  IN VARCHAR2,
                           Pv_Usuario  IN VARCHAR2,
                           Pv_Credit   IN VARCHAR2,
                           Pt_Journal         OUT Tab_Journal,
                           Pv_Error OUT VARCHAR2);
                                                 

  procedure rcp_crea_archivo(pv_directorio    in varchar2, -- directorio donde se crear� el archivo (directorio rcc)
                             pv_nombrearchivo in varchar2, -- nombre del archivo
                             pn_bytes         out number, -- cantidad de bytes del archivo generado
                             pn_error         out number, -- si el proceso es exitoso pn_error=0 caso contrario pn_error=-1
                             pv_error         out varchar2);

  procedure rcp_ftp(pv_ip                in varchar2, -- ip remota
                    pv_usuario           in varchar2, -- usuario para conexion
                    pv_clave             in varchar2, -- clave para conexion
                    pv_ruta_remota       in varchar2, -- directorio remoto
                    pv_nombre_arch       in varchar2, -- nombre del archivo
                    pv_ruta_local        in varchar2, -- nombre ruta local
                    pv_borrar_arch_local in varchar2, -- S=borrar N=no borrar
                    pn_error             out number, -- codigo de error
                    pv_error             out varchar2);

  function rcf_desencripta(pv_cadena in varchar2, pv_error in out varchar2)
    return varchar2;

  procedure rcp_envia_archivo_adjunto(pv_clase             in varchar2,
                                      pv_remitente         in varchar2,
                                      pv_destinatarios     in varchar2,
                                      pv_asunto            in varchar2,
                                      pv_mensaje           in varchar2,
                                      pv_directorio_remoto in varchar2,
                                      pv_nombre_arch       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);

  PROCEDURE Cop_Generar_MV_RANGO(PD_FechaIni IN DATE,
                                 PD_FechaFin IN DATE,
                                 Pv_Error    OUT VARCHAR2);
                                 
  PROCEDURE Cop_Generar_MV_RANGO_SH(PN_secuencia number,
                                    Pv_Error    OUT VARCHAR2);
                                 
  PROCEDURE Cop_Generar_MV_3(Pn_Customer IN NUMBER,
                           Pv_Region   IN VARCHAR2,
                           Pv_Plan     IN VARCHAR2,
                           Pv_FechaIni IN VARCHAR2,
                           Pv_FechaFin IN VARCHAR2,
                           Pv_Ciclo    IN VARCHAR2,
                           Pv_Clase    IN VARCHAR2,
                           Pv_FormaPa  IN VARCHAR2,
                           Pv_Usuario  IN VARCHAR2,
                           Pv_Credit   IN VARCHAR2,
                           pv_directorio    in varchar2, -- directorio donde se crear� el archivo (directorio rcc)
                           pv_email         in varchar2 default null,
                             pv_nombrearchivo out varchar2, -- nombre del archivo
                             pn_bytes         out number, -- cantidad de bytes del archivo generado
                             pn_error         out number,
                           --Pt_Journal         OUT Tab_Journal,
                           Pv_Error OUT VARCHAR2);                                 
  PROCEDURE Cop_Inserta_Journal(PN_REGION      NUMBER,
                              PD_FECHA_DESDE   DATE,
                              PD_FECHA_HASTA   DATE,  
                              PV_email         varchar2,    
                              PN_SECUENCIA     OUT NUMBER,                    
                              PV_ERROR         OUT VARCHAR2);
                              
  PROCEDURE Cop_Actualiza_Journal(PN_SECUENCIA   NUMBER,
                                  PD_FECHA       DATE,
                                  PV_ESTADO      Varchar2,                                                           
                                  PV_ERROR       OUT VARCHAR2);                              
  
  PROCEDURE Cop_Inserta_Archivo(Pv_Region     NUMBER,
                                Pv_FechaIni   DATE,
                                Pv_FechaFin   DATE,     
                                Pt_Journal    IN Tab_Journal,
                                PV_ERROR      OUT VARCHAR2);
                                                            
  Procedure Cop_insert_util_sh(Pv_region varchar2,
                               pd_fecha_inicio date,
                               pd_fecha_fin date,
                               PV_email  varchar2,
                               pv_error out varchar2);
END COK_JOURNAL_NEW;
/
CREATE OR REPLACE PACKAGE BODY COK_JOURNAL_NEW IS
  --===========================================================================================================================
  -- Author  : SUD Vanessa Herdoiza -- SUD Erick Poveda -- SIS Galo Jerez
  -- Created : 19/02/2009 14:42:33
  -- Purpose : [4219] Mejoras al Reporte Journal
  --===========================================================================================================================
  PROCEDURE Cop_Generar_MV(Pn_Customer IN NUMBER,
                           Pv_Region   IN VARCHAR2,
                           Pv_Plan     IN VARCHAR2,
                           Pv_FechaIni IN VARCHAR2,
                           Pv_FechaFin IN VARCHAR2,
                           Pv_Ciclo    IN VARCHAR2,
                           Pv_Clase    IN VARCHAR2,
                           Pv_FormaPa  IN VARCHAR2,
                           Pv_Usuario  IN VARCHAR2,
                           Pv_Credit   IN VARCHAR2,
                           Pt_Journal         OUT Tab_Journal,
                           Pv_Error OUT VARCHAR2) IS
  
    CURSOR Cr_Data2 IS
    -------PAGOS
            select /*+ index(co_cashreceipts_all_mv IDX_CASHCSINDEX_MV)*/ --CUST.customer_id || ',' ||
       substr(CUST.custcode || ',' ||
              CASH.cachkdate || ',' ||
              nvl(CASH.cabankname, 'NO DEFINIDO') || ',' ||
              nvl(CASH.cabanksubacc, 'NDEF') || ',' ||
              CASH.cacuramt_pay || ',' ||
              CON.ccfname || ',' || CON.cclname || ',' ||
              CASH.carem || ',' ||
              decode(CASH.catype, 1, 'ACE', 3, 'CO') || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'DTH',9,
                     'Pymes') || ',' || 'PAGOS' || ',' || CASH.causername || ',' ||
              CUST.cstradecode || ',' || NVL(ci.dia_ini_ciclo,'000') || ',' ||(
              select sysadm.cok_trx_transaccion_pago.cof_journal_ticket(trunc(CASH.cachkdate),
              CUST.custcode,CASH.causername,CASH.carem,CASH.carem)id_pago_banco from dual),
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all  CUST,
             co_cashreceipts_all_mv CASH,
             ccontact_all CON,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where CASH.customer_id = CUST.customer_id
         and CUST.customer_id = CON.customer_id(+)
            --and CUST.customer_id = co_cuadre.customer_id (+)
            --and CUST.customer_id>0
         and CUST.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and CON.ccbill = 'X'
         and CON.ccseq <> 0
            --and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or prgcode >= decode(Pn_Customer,0,0,7) )                  
         and (prgcode in
             (TO_CHAR(Pn_Customer * 2), TO_CHAR(Pn_Customer * 2 - 1)) or
             prgcode >= TO_CHAR(decode(Pn_Customer, 0, 0, 11))) --15/08/2013 6172 SUD AGU
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                               costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
         and CASH.catype in ('1', '3', '9')
            --and caentdate >= to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss')
         and cachkdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and paymntresp = 'X'
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 1)
         and (ltrim(rtrim(Pv_Usuario)) = causername OR Pv_Usuario = 'TODOS')
         and (ltrim(rtrim(Pv_FormaPa)) = cabanksubacc OR
             Pv_FormaPa = 'TODOS')
         and (ltrim(rtrim(Pv_Credit)) = CUST.cstradecode OR
             Pv_Credit = 'TODOS')
      --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
      
      UNION ALL
      
      -------CREDITOS REGULARES
      select --cu.customer_id || ',' ||
       substr(cu.custcode || ',' || orderhdr_all.ohrefdate || ',' || 'CM' || ',' || 'CM' || ',' ||
              orderhdr_all.ohinvamt_doc || ',' || c.ccfname || ',' ||
              c.cclname || ',' || null || ',' || orderhdr_all.ohstatus || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,
                     1,
                     'Autocontrol',
                     2,
                     'Autocontrol',
                     3,
                     'Tarifario',
                     4,
                     'Tarifario',
                     5,
                     'Bulk',
                     6,
                     'Bulk',
                     7,
                     'DTH',
                     9,
                     'Pymes') || ',' || 'CREDITOS' || ',' || ohuserid || ',' ||
              cu.cstradecode || ',' || ci.dia_ini_ciclo,
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all cu,
             orderhdr_all,
             ccontact_all c,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where cu.customer_id = orderhdr_all.customer_id(+)
            --and cu.customer_id>0
         and c.ccbill = 'X'
         and c.ccseq <> 0
         and orderhdr_all.ohxact > 0
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                              costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
            --and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >= decode(to_number(substr(Pn_Customer,1,1)),0,0,7) ) 
         and (prgcode in (to_char(substr(Pn_Customer, 1, 1) * 2),
              to_char(substr(Pn_Customer, 1, 1) * 2 - 1)) or
             prgcode >=
             to_char(decode(substr(Pn_Customer, 1, 1), 0, 0, 11))) --15/08/2013 6172 SUD AGU
         and ohentdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and to_char(ohentdate, 'dd') <> ci.dia_ini_ciclo --'24' [2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Cambia dia 24 por dia de inicio del ciclo
         and c.customer_id = cu.customer_id
            --and cu.customer_id = co_cuadre.customer_id (+)
         and cu.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and paymntresp = 'X'
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 2)
         and (Pv_Usuario = ohuserid OR Pv_Usuario = 'TODOS')
         and (Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS')
      --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
      UNION ALL
      
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select --cu.customer_id || ',' ||
       substr(cu.custcode || ',' || f.entdate || ',' ||
              nvl(m.des, 'NO DEFINIDO') || ',' || to_char(f.sncode) || ',' ||
              f.amount || ',' || c.cclname || ',' || c.ccfname || ',' ||
              f.remark || ',' ||
              --'' carem,            
              '' || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,
                     1,
                     'Autocontrol',
                     2,
                     'Autocontrol',
                     3,
                     'Tarifario',
                     4,
                     'Tarifario',
                     5,
                     'Bulk',
                     6,
                     'Bulk',
                     7,
                     'DTH',
                     9,
                     'Pymes') || ',' || 'CREDITOS' || ',' || f.username || ',' ||
              --Pv_Usuario e_USUARIO,
              cu.cstradecode || ',' || ci.dia_ini_ciclo,
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all cu,
             CO_FEES_MV   f,
             mpusntab     m,
             ccontact_all c,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where cu.customer_id = f.customer_id(+)
            --and cu.customer_id>0
            --and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >=  decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )
         and (prgcode in (to_char(substr(Pn_Customer, 1, 1) * 2),
              to_char(substr(Pn_Customer, 1, 1) * 2 - 1)) or
             prgcode >=
             to_char(decode(substr(Pn_Customer, 1, 1), 0, 0, 11))) --15/08/2013 6172 SUD AGU
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
         and f.entdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and c.customer_id = cu.customer_id
         and c.ccseq <> 0
            --and cu.customer_id = co_cuadre.customer_id (+)
         and cu.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and c.ccbill = 'X'
         and f.sncode = m.sncode
         and m.sncode > 0
         and paymntresp = 'X'
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 2)
         and (ltrim(rtrim(Pv_Usuario)) = f.username OR Pv_Usuario = 'TODOS')
         and (Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS')
         and f.amount < 0
      --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
      UNION ALL
      
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select /*+ index(f IDX_FEES_MV_ENTD) */
       substr(cu.custcode || ',' || f.entdate || ',' ||
              nvl(m.des, 'NO DEFINIDO') || ',' || to_char(f.sncode) || ',' ||
              f.amount || ',' || c.cclname || ',' || c.ccfname || ',' ||
              f.remark || ',' ||
              --'' carem,            
              '' || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,
                     1,
                     'Autocontrol',
                     2,
                     'Autocontrol',
                     3,
                     'Tarifario',
                     4,
                     'Tarifario',
                     5,
                     'Bulk',
                     6,
                     'Bulk',
                     7,
                     'DTH',
                     9,
                     'Pymes') || ',' || 'CREDITOS' || ',' || f.username || ',' ||
              --Pv_Usuario e_USUARIO,
              cu.cstradecode || ',' || ci.dia_ini_ciclo,
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all cu,
             CO_FEES_MV   f,
             mpusntab     m,
             ccontact_all c,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where cu.customer_id = f.customer_id(+)
            --and cu.customer_id>0      
            --and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2, to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >= decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )                  
         and (prgcode in (to_char(substr(Pn_Customer, 1, 1) * 2),
              to_char(substr(Pn_Customer, 1, 1) * 2 - 1)) or
             prgcode >=
             to_char(decode(substr(Pn_Customer, 1, 1), 0, 0, 11))) --15082013 6172 SUD AGU
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,7) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
         and f.entdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and c.customer_id = cu.customer_id
         and c.ccseq <> 0
            --and cu.customer_id = co_cuadre.customer_id (+)
         and cu.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and c.ccbill = 'X'
         and f.sncode = m.sncode
         and m.sncode > 0
         and paymntresp is null
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 2)
         and (ltrim(rtrim(Pv_Usuario)) = f.username OR Pv_Usuario = 'TODOS')
         and (Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS')
         and f.amount < 0
      --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
      UNION ALL
      
      ---CARGOS cuentas padres
      select /*+ index(f IDX_FEES_MV_ENTD) */
       substr(cu.custcode || ',' || f.entdate || ',' ||
              nvl(m.des, 'NO DEFINIDO') || ',' || to_char(f.sncode) || ',' ||
              f.amount || ',' || c.cclname || ',' || c.ccfname || ',' ||
              f.remark || ',' ||
              --'' carem,            
              '' || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,
                     1,
                     'Autocontrol',
                     2,
                     'Autocontrol',
                     3,
                     'Tarifario',
                     4,
                     'Tarifario',
                     5,
                     'Bulk',
                     6,
                     'Bulk',
                     7,
                     'DTH',
                     9,
                     'Pymes') || ',' || 'CARGOS' || ',' || f.username || ',' ||
              --Pv_Usuario e_USUARIO,
              cu.cstradecode || ',' || ci.dia_ini_ciclo,
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all cu,
             CO_FEES_MV   f,
             mpusntab     m,
             ccontact_all c,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where cu.customer_id = f.customer_id(+)
            --and cu.customer_id>0
            --and ( prgcode in (Pn_Customer*2, Pn_Customer*2-1) or prgcode >=  decode(Pn_Customer,0,0,7) ) 
         and (prgcode in
             (to_char(Pn_Customer * 2), to_char(Pn_Customer * 2 - 1)) or
             prgcode >= to_char(decode(Pn_Customer, 0, 0, 11))) --15/08/2013 6172 SUD AGU
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma                             
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
         and f.entdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and c.customer_id = cu.customer_id
         and c.ccseq <> 0
            --and cu.customer_id = co_cuadre.customer_id (+)
         and f.sncode = m.sncode
         and m.sncode > 0
         and cu.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and c.ccbill = 'X'
         and paymntresp = 'X'
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 3)
         and (ltrim(rtrim(Pv_Usuario)) = f.username OR Pv_Usuario = 'TODOS')
         and (ltrim(rtrim(Pv_Credit)) = cu.cstradecode OR
             Pv_Credit = 'TODOS')
         and f.sncode <> 103
         and f.amount > 0
      --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
      UNION ALL
      
      --CARGOS para cuentas hijas
      select /*+ index(f IDX_FEES_MV_ENTD) */
       substr(cu.custcode || ',' || f.entdate || ',' ||
              nvl(m.des, 'NO DEFINIDO') || ',' || to_char(f.sncode) || ',' ||
              f.amount || ',' || c.cclname || ',' || c.ccfname || ',' ||
              f.remark || ',' ||
              --'' carem,           
              '' || ',' ||
              decode(costcenter_id, 1, 'Guayaquil', 2, 'Quito') || ',' ||
              decode(prgcode,
                     1,
                     'Autocontrol',
                     2,
                     'Autocontrol',
                     3,
                     'Tarifario',
                     4,
                     'Tarifario',
                     5,
                     'Bulk',
                     6,
                     'Bulk',
                     7,
                     'DTH',
                     9,
                     'Pymes') || ',' || 'CARGOS' || ',' || f.username || ',' ||
              --Pv_Usuario e_USUARIO,
              cu.cstradecode || ',' || ci.dia_ini_ciclo,
              1,
              400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from customer_all cu,
             CO_FEES_MV   f,
             mpusntab     m,
             ccontact_all c,
             --co_cuadre,
             fa_ciclos_bscs      ci,
             fa_ciclos_axis_bscs ma
       where cu.customer_id = f.customer_id(+)
            --and cu.customer_id>0
            --and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or prgcode >=  decode(Pn_Customer,0,0,7) )
         and (prgcode in
             (to_char(Pn_Customer * 2), to_char(Pn_Customer * 2 - 1)) or
             prgcode >= to_char(decode(Pn_Customer, 0, 0, 11))) --15/08/2013 6172 SUD AGU
            -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma                             
            /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                                                                                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
         and costcenter_id = to_number(substr(Pv_Region, 1, 1))
         and f.entdate between
             to_date(Pv_FechaIni, 'dd/mm/yyyy hh24:mi:ss') and
             to_date(Pv_FechaFin, 'dd/mm/yyyy hh24:mi:ss')
         and c.customer_id = cu.customer_id
         and c.ccseq <> 0
            --and cu.customer_id = co_cuadre.customer_id (+)
         and f.sncode = m.sncode
         and m.sncode > 0
         and cu.billcycle = ma.id_ciclo_admin
         and ci.id_ciclo = ma.id_ciclo
            --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
         and (ci.id_ciclo = Pv_Ciclo or
             ci.id_ciclo > decode(Pv_Ciclo, 'TODOS', '0'))
         and c.ccbill = 'X'
         and paymntresp is null
            -- SUD Erick Poveda -- Mejora para optimizacion de tiempo de respuesta en el reporte
            --and customer_id_high is not null
         and nvl(customer_id_high, '-1') <> '-1'
         and to_number(substr(Pv_Clase, 1, 1)) in (0, 3)
         and (ltrim(rtrim(Pv_Usuario)) = f.username OR Pv_Usuario = 'TODOS')
         and (ltrim(rtrim(Pv_Credit)) = cu.cstradecode OR
             Pv_Credit = 'TODOS')
         and f.sncode <> 103
         and f.amount > 0;
    --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan);
    lc_archivo CLOB;
    len        BINARY_INTEGER;
    LT_JOURNAL Tab_Journal;
    lv_datos   varchar2(4000);
    ln_bytes   number;
    lv_error   varchar2(2000);
    ln_error   number;
    LE_ERROR EXCEPTION;
  
  
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_GENERAR_MV';
    lv_id_proceso_scp           varchar2(100) := 'SCP_GENERA_MV';
    lv_unidad_registro_scp      varchar2(30) := 'Genera CSV';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
    --FIN BITACORA SCP    
  
  
  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
    
    EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_JOURNAL_FECHA_ARCHIVO');
    OPEN Cr_Data2;
    FETCH Cr_Data2 BULK COLLECT
      INTO LT_JOURNAL;
    CLOSE Cr_Data2;
    Pt_Journal := LT_JOURNAL;
  
    /*DBMS_LOB.createtemporary(lc_archivo, TRUE);
    dbms_lob.open(lc_archivo, dbms_lob.lob_readwrite);
    for i in LT_JOURNAL.first .. LT_JOURNAL.last loop
      lv_datos := LT_JOURNAL(I) || chr(13);
      LEN      := LENGTH(lv_datos);
      --exit when i = 5;
      dbms_lob.writeappend(lc_archivo, len, lv_datos);
      -- exit when i > 100;
    end loop;
    INSERT INTO CO_JOURNAL_FECHA_ARCHIVO
    VALUES
      (Pv_Region,
       TO_DATE(Pv_FechaIni, 'DD/MM/RRRR HH24:MI:SS'),
       LC_ARCHIVO,
       TO_DATE(Pv_FechaFin, 'DD/MM/RRRR HH24:MI:SS'));
    COMMIT;
  
    --PC_LOB := LC_ARCHIVO;
    LEN := DBMS_LOB.getlength(LC_ARCHIVO);
    DBMS_LOB.erase(LC_ARCHIVO, LEN);
    dbms_lob.trim(lc_archivo, 0);
  
    DBMS_LOB.CLOSE(LC_ARCHIVO);*/
  
/*    COK_JOURNAL_NEW.rcp_crea_archivo(pv_directorio    => 'JOURNAL',
                                     pv_nombrearchivo => 'postjrn_'|| Pv_Region ||'_' ||
                                                         TO_CHAR(TO_DATE(Pv_FechaIni,
                                                                         'DD/MM/RRRR HH24:MI:SS'),
                                                                 'DDMMRRRR') ||
                                                         '.CSV',
                                     pn_bytes         => Ln_bytes,
                                     pn_error         => Ln_error,
                                     pv_error         => Lv_error);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;*/
  
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se realiza un TRUNCATE TABLE CO_JOURNAL_FECHA_ARCHIVO';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := LV_ERROR;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    WHEN OTHERS THEN
      NULL;
      Pv_Error := SQLERRM;
      lv_mensaje_apl_scp := substr(SQLERRM,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
        -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
        scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Generar_MV;
  
  procedure rcp_crea_archivo(pv_directorio    in varchar2, -- directorio donde se crear� el archivo (directorio rcc)
                             pv_nombrearchivo in varchar2, -- nombre del archivo
                             pn_bytes         out number, -- cantidad de bytes del archivo generado
                             pn_error         out number, -- si el proceso es exitoso pn_error=0 caso contrario pn_error=-1
                             pv_error         out varchar2) is
    -- c�digo de error, si es NULL la ejecuci�n fue OK
  
    /*ini proyecto 6428  cursor que recoorre el numero de
    campos cuando sea visible y con el orden de presentacion*/
    file_handle  utl_file.file_type; -- file handle of os flat file
    lv_texto     varchar2(4000);
    lv_programa  varchar2(100) := 'COK_JOURNAL_NEW.RCP_CREA_ARCHIVO';
    lb_existe    boolean;
    ln_blocksize number;
  
    cursor c_journal is
      select archivo from co_journal_fecha_archivo j;
    lc_lob   clob;
    ln_car   number;
    ln_ini   number;
    ln_inter number;
    lv_error varchar2(2000);
    ln_error number;
    le_error exception;
  
  
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_id_proceso_scp           varchar2(100) := 'SCP_CREA_ARCIVO';
    lv_unidad_registro_scp      varchar2(30) := 'Crea CSV';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
    --FIN BITACORA SCP  
  
  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
  
  
    open c_journal;
    fetch c_journal
      into lc_lob;
    close c_journal;
    -- puntero al archivo
    ln_car   := 1;
    ln_ini   := 1;
    ln_inter := 1;
    if dbms_lob.getlength(lc_lob) > 0 then
      file_handle := utl_file.fopen(pv_directorio, pv_nombrearchivo, 'W');
      loop
        --dbms_output.put_line(dbms_lob.instr(lc_lob, chr(13), 1, ln_inter));
        ln_car   := dbms_lob.instr(lc_lob, chr(13), 1, ln_inter);
        lv_texto := dbms_lob.substr(lc_lob, ln_car - ln_ini, ln_ini);
        --dbms_output.put_line(lv_texto);
        exit when lv_texto is null;
        utl_file.put_line(file_handle, lv_texto);
        ln_ini   := ln_car + 1;
        ln_inter := ln_inter + 1;
      end loop;
      -- cierra archivos y cursores.
      utl_file.fclose(file_handle);
    
      -- C�digo para obtener el tama�o del archivo creado, s�lo para oracle 9i en adelante
      utl_file.fgetattr(pv_directorio,
                        pv_nombrearchivo,
                        lb_existe,
                        pn_bytes,
                        ln_blocksize);
    end if;
  
    rcp_ftp(pv_ip                => '130.2.18.27',
            pv_usuario           => 'gsioper',
            pv_clave             => 'y�����S��]�]����',
            pv_ruta_remota       => '/procesos/gsioper/notificaciones/archivos',
            pv_nombre_arch       => pv_nombrearchivo,
            pv_ruta_local        => pv_directorio,
            pv_borrar_arch_local => 'S',
            pn_error             => ln_error,
            pv_error             => lv_error);
  
    if lv_error is not null then
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := lv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      raise le_error;
    end if;
  
    rcp_envia_archivo_adjunto(pv_clase             => 'RCC',
                              pv_remitente         => 'rcc_notificaciones@claro.com.ec',
                              pv_destinatarios     => 'cespinoc@claro.com.ec;gycaza@claro.com.ec',
                              pv_asunto            => 'Reporte Journal',
                              pv_mensaje           => 'Reporte Journal',
                              pv_directorio_remoto => '/procesos/gsioper/notificaciones/archivos',
                              pv_nombre_arch       => pv_nombrearchivo,
                              pn_error             => ln_error,
                              pv_error             => lv_error);
  
    if ln_error <> 0 then
      pn_error := -1;
      pv_error := 'No se pudo ingresar notificacion de envio de correo con adjunto ' ||
                  lv_error;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := pv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      
      return;
    end if;
  
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se creo el archivo correctamente';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  
    /*en caso de que sea exitoso el pn_error va ahacer 0*/
    pn_error := 0;
    
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  exception
    when le_error then
      pv_error := lv_error;
      pn_error := -1;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := lv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    when others then
    
      if utl_file.is_open(file_handle) then
        utl_file.fclose(file_handle);
      end if;
      pv_error := lv_programa || 'sql_error - ' || sqlcode || sqlerrm;
      pn_error := -1;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  end rcp_crea_archivo;

  procedure rcp_ftp(pv_ip                in varchar2, -- ip remota
                    pv_usuario           in varchar2, -- usuario para conexion
                    pv_clave             in varchar2, -- clave para conexion
                    pv_ruta_remota       in varchar2, -- directorio remoto
                    pv_nombre_arch       in varchar2, -- nombre del archivo
                    pv_ruta_local        in varchar2, -- nombre ruta local
                    pv_borrar_arch_local in varchar2, -- S=borrar N=no borrar
                    pn_error             out number, -- codigo de error
                    pv_error             out varchar2) Is
  
    l_conn                 utl_tcp.connection; --utl_tcp
    lv_puerto_acceso       varchar2(4) := '21';
    lv_error               varchar2(2000);
    lv_clave_desencriptada varchar2(100);
    le_error exception;
  
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                varchar2(2000) := 'COK_JOURNAL_NEW.RCP_FTP';
    lv_id_proceso_scp           varchar2(100) := 'SCP_FTP';
    lv_unidad_registro_scp      varchar2(30) := 'Conexion ftp';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
    --FIN BITACORA SCP  
  
  begin
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
  
    -- Desencriptamos la clave
    lv_clave_desencriptada := rcf_desencripta(pv_cadena => pv_clave,
                                              pv_error  => lv_error);
  
    if lv_error is not null then
      lv_error := 'RCC: Error al desencriptar clave mediante rck_trx_utilitarios.rcf_desencripta dentro del proceso rck_api_programacion.rcp_ftp.';
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := lv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      raise le_error;
    end if;
  
    -- Abre conexion utl_tcp mediante FTP al servidor remoto
    l_conn := ftp_journal.login(pv_ip,
                                lv_puerto_acceso,
                                pv_usuario,
                                lv_clave_desencriptada);
  
    -- Envia el archivo al servidor remoto v�a FTP en modo binario
    ftp_journal.binary(p_conn => l_conn);
    ftp_journal.put(p_conn      => l_conn,
                    p_from_dir  => pv_ruta_local,
                    p_from_file => pv_nombre_arch,
                    p_to_file   => pv_ruta_remota || '/' || pv_nombre_arch);
    ftp_journal.send_command(p_conn    => l_conn,
                             p_command => 'SITE CHMOD 777 ' ||
                                          pv_ruta_remota || '/' ||
                                          pv_nombre_arch);
    ftp_journal.logout(l_conn);
    utl_tcp.close_connection(l_conn);
  
    if upper(pv_borrar_arch_local) = 'S' then
      utl_file.fremove(pv_ruta_local, pv_nombre_arch);
    end if;
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Conexion ftp correcta';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
    pn_error := 0;
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  exception
    when le_error then
      pv_error := lv_error;
      pn_error := -1;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := lv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    when others then
      pv_error := 'RCC: Error general en COK_JOURNAL_NEW.rcp_ftp : ' ||
                  sqlerrm;
      pn_error := -1;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin por error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END rcp_ftp;

  function rcf_desencripta(pv_cadena in varchar2, pv_error in out varchar2)
    return varchar2 is
    lv_cadena varchar2(32);
  
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                varchar2(2000) := 'COK_JOURNAL_NEW.RCF_DESENCRIPTA';
    lv_id_proceso_scp           varchar2(100) := 'SCP_DESENCRIPTA';
    lv_unidad_registro_scp      varchar2(30) := 'Desencripta pass';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
    --FIN BITACORA SCP  
  
  begin
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
  
    dbms_obfuscation_toolkit.desdecrypt(input_string     => pv_cadena,
                                        key_string       => COK_JOURNAL_NEW.gv_key,
                                        decrypted_string => lv_cadena);
  
    lv_cadena := substr(lv_cadena, 1, (instr(lv_cadena, '*', 1) - 1));
  
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se desencripto el pass correctamente';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    return(lv_cadena);
    pv_error := '';
  exception
  
    when others then
      pv_error  := 'error durante la desencriptacion de la clave. ' ||
                   sqlerrm;
      lv_cadena := null;
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      return(lv_cadena);
  end rcf_desencripta;

  procedure rcp_envia_archivo_adjunto(pv_clase             in varchar2,
                                      pv_remitente         in varchar2,
                                      pv_destinatarios     in varchar2,
                                      pv_asunto            in varchar2,
                                      pv_mensaje           in varchar2,
                                      pv_directorio_remoto in varchar2,
                                      pv_nombre_arch       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is
  
    respuesta              xmltype;
    ln_requerimiento       number := 1;
    ln_tiempo_expira       number := 3600;
    ln_max_intentos        number := 2;
    ln_offset_segundos     number := 0;
    lv_mensaje             varchar2(2000);
    lv_trama_parametros    varchar2(4000);
    lv_fault_code          varchar2(20);
    lv_fault_string        varchar2(2000);
    lv_programa            varchar2(100) := 'rck_api_programacion.rcp_envia_archivo_adjunto';

    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa1                varchar2(2000) := 'COK_JOURNAL_NEW.RCP_ENVIA_ARCHIVO_ADJUNTO';
    lv_id_proceso_scp           varchar2(100) := 'SCP_ENVIA_ARCHIVO';
    lv_unidad_registro_scp      varchar2(30) := 'Envia Archivo Adjunto';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
  
  BEGIN
    
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa1,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------

    pn_error := 0;
    pv_error := null;
  
    -- Lee el Id Requerimiento de Notificacion
    ln_requerimiento := 3;
  
    -- Lee el tiempo de vida en segundos
    ln_tiempo_expira := 3600;
  
    ln_max_intentos := 1;
  
    ln_offset_segundos := -30;
  
    lv_mensaje := pv_mensaje || ' </DIRECTORIO1=' || pv_directorio_remoto ||
                  '|ARCHIVO1=' || pv_nombre_arch || '|/>';
  
    -- Llena cadena de parametros
    lv_trama_parametros := 'pdFechaEnvio=' ||
                           to_char(sysdate + ln_offset_segundos / 86400,
                                   'yyyy-mm-dd') || 'T' ||
                           to_char(sysdate + ln_offset_segundos / 86400,
                                   'hh24:mi:ss') || '-05:00;' ||
                           'pdFechaExpiracion=' ||
                           to_char(sysdate + ln_tiempo_expira / 86400,
                                   'yyyy-mm-dd') || 'T' ||
                           to_char(sysdate + ln_tiempo_expira / 86400,
                                   'hh24:mi:ss') || '-05:00;' ||
                           'pvAsunto=' || pv_asunto || ';' || 'pvMensaje=' ||
                           lv_mensaje || ';' || 'pvDestinatario=' ||
                           pv_destinatarios || ';' || --Codigo Grupo
                           'pvRemitente=' || pv_remitente || ';' ||
                           'pvTipoRegistro=A;' || --A mail con adjunto, M solo mail
                           'pvClase=' || pv_clase || ';' ||
                           'pvMaxIntentos=' || to_char(ln_max_intentos) || ';' ||
                           'pvIdUsuario=' || USER || ';';
  
    /*Llamado soap para consumir servicio web*/
    --adicionarle @dblink si rcc esta en reporteria
    respuesta := scp_dat.sck_soap_gtw.scp_consume_servicio(pn_id_req                   => ln_requerimiento,
                                                           pv_parametros_requerimiento => lv_trama_parametros,
                                                           pv_fault_code               => lv_fault_code,
                                                           pv_fault_string             => lv_fault_string);
    if lv_fault_string is not null then
      pn_error := -1;
      pv_error := 'RCC: Error al Consumir Servicio Web Notificaciones-' ||
                  lv_fault_string;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      return;
    end if;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se envia archivo adjunto';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  exception
    when others then
      pv_error := 'RCC: Error general en ' || lv_programa || ' - ' ||
                  sqlerrm;
      pn_error := -1;
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  end rcp_envia_archivo_adjunto;

  PROCEDURE Cop_Generar_MV_RANGO(PD_FechaIni IN DATE,
                                 PD_FechaFin IN DATE,
                                 Pv_Error    OUT VARCHAR2) IS
  
    lv_error  varchar2(2000);
    ln_error  number;
    LV_REGION VARCHAR2(10);
    LE_ERROR EXCEPTION;
    LD_FECHAINI DATE;
    LD_FECHAFIN DATE;
    ln_nombre_arch varchar2(500);
    ln_bytes number;
    lv_email  varchar2(100):=null;
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_GENERAR_MV_RANGO';
    lv_id_proceso_scp           varchar2(100) := 'SCP_GENERA_RANGO';
    lv_unidad_registro_scp      varchar2(30) := 'Genera Rango';
    lv_proceso_scp              varchar2(100);
    lv_descripcion_scp          varchar2(500);
    lv_proceso_journal          varchar2(100);
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------

  BEGIN
    LD_FECHAINI := trunc(PD_FechaIni);
    LD_FECHAFIN := trunc(PD_FechaIni+1)-1/24/60/60;
    
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_PROCESO_AUX',
                                                lv_proceso_scp,
                                                lv_proceso_journal,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
    
    LOOP
      FOR I IN 1 .. 2 LOOP
        LV_REGION := TO_CHAR(I);
        scp_dat.sck_api.scp_parametros_procesos_lee(LV_REGION,
                                                    lv_id_proceso_scp,
                                                    lv_email,
                                                    lv_descripcion_scp,
                                                    ln_error_scp,
                                                    lv_error_scp);
        /*--Despues de las pruebas hay que borrar esto desde aqui
        if LV_REGION = '1' then--guayaquil
           lv_email:= 'wmendoza@corlasosa.com';
        elsif LV_REGION = '2' then--quito
           lv_email:= 'wmendoza@corlasosa.com';
        end if;
        --hasta aqui*/
        COK_JOURNAL_NEW.Cop_Generar_MV_3(pn_customer => 0,
                                         pv_region   => Lv_region,
                                         pv_plan     => NULL,
                                         pv_fechaini => TO_CHAR(LD_FechaIni,
                                                             'DD/MM/RRRR HH24:MI:SS'),
                                         pv_fechafin => TO_CHAR(LD_FECHAFIN,
                                                              'DD/MM/RRRR HH24:MI:SS'),
                                         pv_ciclo    => 'TODOS',
                                         pv_clase    => '0',
                                         pv_formapa  => 'TODOS',
                                         pv_usuario  => 'TODOS',
                                         pv_credit   => 'TODOS',
                                         pv_directorio => 'JOURNAL',
                                         pv_email => lv_email,
                                         pv_nombrearchivo => ln_nombre_arch,
                                         pn_bytes => ln_bytes,
                                         pn_error => ln_error,
                                         pv_error    => Lv_error);
     IF LV_ERROR IS NOT NULL THEN
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(LV_ERROR,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                Lv_region,
                                                lv_proceso_journal,--'JOURNAL',
                                                substr(lv_email,1,50),
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
          RAISE LE_ERROR;
        END IF;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Genera MV Rango';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;     
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                Lv_region,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      
      END LOOP;
      LD_FECHAINI := LD_FECHAINI +1;
      LD_FECHAFIN := LD_FECHAFIN +1;
      EXIT WHEN LD_FechaIni > PD_FechaFin;
      
    END LOOP;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Genera MV Rango con exito';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;      
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ---------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(LV_ERROR,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    WHEN OTHERS THEN
      NULL;
      Pv_Error := SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(Pv_Error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Generar_MV_RANGO;
  
  PROCEDURE Cop_Generar_MV_RANGO_SH(PN_secuencia number,                                    
                                    Pv_Error    OUT VARCHAR2) IS
  
      
    cursor c_obtiene_data is
    select region,fecha_desde,fecha_hasta,email
      from CO_JOURNAL_PROCESA_FECHAS
     where secuencia = PN_secuencia
       and estado = 'P';
       
    lv_error  varchar2(2000);
    ln_error  number;
    LV_REGION VARCHAR2(10);
    LE_ERROR EXCEPTION;
    LD_FECHAINI DATE;
    LD_FechaFin DATE;
    ln_nombre_arch varchar2(500);
    ln_bytes number;
    lc_obtiene_data c_obtiene_data%rowtype;
    lb_found  boolean;
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_GENERAR_MV_RANGO_SH';
    lv_id_proceso_scp           varchar2(100) := 'SCP_RANGO_SH';
    lv_unidad_registro_scp      varchar2(30) := 'Genera Rango SH';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
  
  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------

    open c_obtiene_data;
    fetch c_obtiene_data into lc_obtiene_data;
    lb_found := c_obtiene_data%found;
    close c_obtiene_data;
    
    LD_FECHAINI := trunc(lc_obtiene_data.fecha_desde);
    LD_FechaFin := trunc(lc_obtiene_data.fecha_desde+1)-1/24/60/60;
    LOOP      
        COK_JOURNAL_NEW.Cop_Generar_MV_3(pn_customer => 0,
                                         pv_region   => lc_obtiene_data.region,
                                         pv_plan     => NULL,
                                         pv_fechaini => TO_CHAR(LD_FechaIni,
                                                             'DD/MM/RRRR HH24:MI:SS'),
                                         pv_fechafin => TO_CHAR(LD_FechaFin,
                                                              'DD/MM/RRRR HH24:MI:SS'),
                                         pv_ciclo    => 'TODOS',
                                         pv_clase    => '0',
                                         pv_formapa  => 'TODOS',
                                         pv_usuario  => 'TODOS',
                                         pv_credit   => 'TODOS',
                                         pv_directorio => 'JOURNAL',
                                         pv_email => lc_obtiene_data.email,                                         
                                         pv_nombrearchivo => ln_nombre_arch,
                                         pn_bytes => ln_bytes,
                                         pn_error => ln_error,
                                         pv_error    => Lv_error);
        IF LV_ERROR IS NOT NULL THEN
          
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp := substr(LV_ERROR,1,4000);
          lv_mensaje_tec_scp := null;
          lv_mensaje_acc_scp := null;
          SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                    lv_mensaje_apl_scp,
                                                    lv_mensaje_tec_scp,
                                                    lv_mensaje_acc_scp,
                                                    0,
                                                    lc_obtiene_data.region,
                                                    'JOURNAL',
                                                    null,
                                                    null,
                                                    null,
                                                    'N',
                                                    ln_id_detalle_scp,
                                                    ln_error_scp,
                                                    lv_error_scp);
          ----------------------------------------------------------------------------  
        
          RAISE LE_ERROR;
        END IF;      

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Genera MV Rango SH LOOP';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
       
      LD_FECHAINI := LD_FECHAINI +1;
      LD_FechaFin := LD_FechaFin +1;
      
      EXIT WHEN LD_FechaIni > lc_obtiene_data.fecha_hasta;
      
    END LOOP;
    Cop_Actualiza_Journal(PN_SECUENCIA => PN_secuencia,
                          PD_FECHA => null,    
                          PV_ESTADO => 'F',
                          PV_ERROR => Lv_error);
    
    --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := nvl(Lv_error,'Genera MV Rango SH');
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(LV_ERROR,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    WHEN OTHERS THEN
      NULL;
      Pv_Error := SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(Pv_Error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Generar_MV_RANGO_SH;
  
  PROCEDURE Cop_Generar_MV_3(Pn_Customer IN NUMBER,
                           Pv_Region   IN VARCHAR2,
                           Pv_Plan     IN VARCHAR2,
                           Pv_FechaIni IN VARCHAR2,
                           Pv_FechaFin IN VARCHAR2,
                           Pv_Ciclo    IN VARCHAR2,
                           Pv_Clase    IN VARCHAR2,
                           Pv_FormaPa  IN VARCHAR2,
                           Pv_Usuario  IN VARCHAR2,
                           Pv_Credit   IN VARCHAR2,
                           pv_directorio    in varchar2, -- directorio donde se crear� el archivo (directorio rcc)
                           pv_email         in varchar2 default null,
                             pv_nombrearchivo out varchar2, -- nombre del archivo
                             pn_bytes         out number, -- cantidad de bytes del archivo generado
                             pn_error         out number,
                           --Pt_Journal         OUT Tab_Journal,
                           Pv_Error OUT VARCHAR2) IS
  
    LT_JOURNAL Tab_Journal;
    lv_error   varchar2(2000);
    lv_nombrearchivo varchar2(1000);
    lv_comando   varchar2(2000);
    lv_salida    varchar2(2000);
    ln_error   number;
    LE_ERROR EXCEPTION;
    file_handle  utl_file.file_type; -- file handle of os flat file
    lb_existe    boolean;
    ln_blocksize number;
    lv_mail varchar2(100):=null;
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_GENERAR_MV_3';
    lv_id_proceso_scp           varchar2(100) := 'SCP_GENERA_MV_3';
    lv_unidad_registro_scp      varchar2(30) := 'Genera MV 3';
    lv_proceso_journal          varchar2(100);
    lv_ip_parametrizada         varchar2(100);
    lv_user_parametrizado       varchar2(100);
    lv_clave_parametrizada      varchar2(100);
    lv_ruta_parametrizada       varchar2(100);
    lv_clase_parametrizada      varchar2(50);
    lv_remitente_parame         varchar2(100);
    lv_asun_mens_parame         varchar2(500);
    lv_borrar_archivo           varchar2(5);
    lv_descripcion_scp          varchar2(500);
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
  
  BEGIN
    
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_PROCESO_AUX',
                                                lv_id_proceso_scp,
                                                lv_proceso_journal,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------   
  
    COK_JOURNAL_NEW.cop_generar_mv(pn_customer => pn_customer,
                                 pv_region => pv_region,
                                 pv_plan => pv_plan,
                                 pv_fechaini => pv_fechaini,
                                 pv_fechafin => pv_fechafin,
                                 pv_ciclo => pv_ciclo,
                                 pv_clase => pv_clase,
                                 pv_formapa => pv_formapa,
                                 pv_usuario => pv_usuario,
                                 pv_credit => pv_credit,
                                 pt_journal => lt_journal,
                                 pv_error => pv_error);
    if pv_email is null then 
      scp_dat.sck_api.scp_parametros_procesos_lee(pv_region,
                                                  lv_id_proceso_scp,
                                                  lv_mail,
                                                  lv_descripcion_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
        /*--borrar despues de las pruebas desd aqui
        if pv_region = '1' then--guayaquil
           lv_mail:= 'wmendoza@corlasosa.com';
        elsif pv_region = '2' then--quito
           lv_mail:= 'wmendoza@corlasosa.com';
        end if;
        --hasta aqui*/
    else 
      lv_mail:=pv_email;
    end if;
    lv_nombrearchivo := 'postjrn_'|| Pv_Region ||'_' ||
                                                         TO_CHAR(TO_DATE(Pv_FechaIni,
                                                                         'DD/MM/RRRR HH24:MI:SS'),
                                                                 'DDMMRRRR') ||
                                                         '.CSV';
    file_handle := utl_file.fopen(pv_directorio, lv_nombrearchivo, 'W');
    for i in LT_JOURNAL.first .. LT_JOURNAL.last loop
        utl_file.put_line(file_handle, lt_journal(i));
    end loop;
    
      -- cierra archivos y cursores.
      utl_file.fclose(file_handle);
    --lv_nombrearchivo:='postjrn_1_22092014_prueba.CSV';
    lv_comando := '/bin/sh /procesos/gsioper/journal/comprime_archivo.sh ' || lv_nombrearchivo || ' '||'&'; 
    osutil.runoscmd(lv_comando, lv_salida);
    
    lv_nombrearchivo:=lv_nombrearchivo||'.Z';
    -- C�digo para obtener el tama�o del archivo creado, s�lo para oracle 9i en adelante
    utl_file.fgetattr(pv_directorio,
                      lv_nombrearchivo,
                      lb_existe,
                      pn_bytes,
                      ln_blocksize);
    
    if not lb_existe then
      Pv_Error:='No se encontro el archivo en el directorio'||pv_directorio;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(Pv_Error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      
      raise le_error;
    end if;
   
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_IP_PARAMETRO',
                                                lv_id_proceso_scp,
                                                lv_ip_parametrizada,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
                                                
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_USUARIO_PARAMETRO',
                                                lv_id_proceso_scp,
                                                lv_user_parametrizado,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
     
     scp_dat.sck_api.scp_parametros_procesos_lee('SCP_CLAVE_PARAMETRO',
                                                lv_id_proceso_scp,
                                                lv_clave_parametrizada,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      
     scp_dat.sck_api.scp_parametros_procesos_lee('SCP_RUTA_REMOTA',
                                                lv_id_proceso_scp,
                                                lv_ruta_parametrizada,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
     
     scp_dat.sck_api.scp_parametros_procesos_lee('SCP_BORRAR_ARCH',
                                                lv_id_proceso_scp,
                                                lv_borrar_archivo,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
                                                                                                                                                                                
    rcp_ftp(pv_ip                => lv_ip_parametrizada,--'130.2.18.27',
            pv_usuario           => lv_user_parametrizado,--'gsioper',
            pv_clave             => lv_clave_parametrizada,--'y�����S��]�]����',
            pv_ruta_remota       => lv_ruta_parametrizada,--'/procesos/gsioper/notificaciones/archivos',
            pv_nombre_arch       => lv_nombrearchivo,
            pv_ruta_local        => pv_directorio,
            pv_borrar_arch_local => lv_borrar_archivo,--'S',
            pn_error             => ln_error,
            pv_error             => lv_error);
  
    if lv_error is not null then
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(lv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      raise le_error;
    end if;
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_CLASE',
                                                lv_id_proceso_scp,
                                                lv_clase_parametrizada,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);

    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_REMITENTE',
                                                lv_id_proceso_scp,
                                                lv_remitente_parame,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);                                                
     
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_ASUNTO',
                                                lv_id_proceso_scp,
                                                lv_asun_mens_parame,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    scp_dat.sck_api.scp_parametros_procesos_lee('SCP_RUTA_REMOTA',
                                                lv_id_proceso_scp,
                                                lv_ruta_parametrizada,
                                                lv_descripcion_scp,
                                                ln_error_scp,
                                                lv_error_scp);
                                                                                           
    rcp_envia_archivo_adjunto(pv_clase             => lv_clase_parametrizada,--'RCC',
                              pv_remitente         => lv_remitente_parame,--'rcc_notificaciones@claro.com.ec',
                              pv_destinatarios     => lv_mail,--;Gycaza@claro.com.ec;smoreira@claro.com.ec',
                              pv_asunto            => lv_asun_mens_parame,--'Reporte Journal',
                              pv_mensaje           => lv_asun_mens_parame,--'Reporte Journal',
                              pv_directorio_remoto => lv_ruta_parametrizada,--'/procesos/gsioper/notificaciones/archivos',
                              pv_nombre_arch       => lv_nombrearchivo,
                              pn_error             => ln_error,
                              pv_error             => lv_error);
  
    if ln_error <> 0 then
      pn_error := -1;
      pv_error := 'No se pudo ingresar notificacion de envio de correo con adjunto ' ||
                  lv_error;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      return;
    end if;
  
    /*en caso de que sea exitoso el pn_error va ahacer 0*/
    pv_nombrearchivo := lv_nombrearchivo;
    pn_error := 0;
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se genera MV 3';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(LV_ERROR,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    WHEN OTHERS THEN
      NULL;
      Pv_Error := SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                lv_proceso_journal,--'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Generar_MV_3;
  
  PROCEDURE Cop_Inserta_Journal(PN_REGION      NUMBER,
                              PD_FECHA_DESDE   DATE,
                              PD_FECHA_HASTA   DATE, 
                              PV_email         varchar2,    
                              PN_SECUENCIA     OUT NUMBER,                    
                              PV_ERROR         OUT VARCHAR2) IS
  /*
  -- ECARFO 9493 FBE
  -- PROCESO QUE PERMITIRA REALIZAR UN INSERT EN LA TABLA CO_JOURNAL_PROCESA_FECHAS
  -- DESDE FORM
  -- CREADO 12/09/2014
  */
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                varchar2(2000) := 'COK_JOURNAL_NEW.COP_INSERTA_JOURNAL';
    lv_id_proceso_scp           varchar2(100) := 'SCP_INSERTA_JOURNAL';
    lv_unidad_registro_scp      varchar2(30) := 'Ins co_journal_procesa';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------

  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------  
  
  
    SELECT SEC_JOURNAL.NEXTVAL INTO PN_SECUENCIA FROM DUAL;
    
    INSERT INTO CO_JOURNAL_PROCESA_FECHAS
      (SECUENCIA,REGION,FECHA_INGRESO,FECHA_DESDE,FECHA_HASTA,ESTADO,EMAIL)
    VALUES
      (PN_SECUENCIA,
       TO_CHAR(PN_REGION),
       sysdate,
       PD_FECHA_DESDE,
       PD_FECHA_HASTA,
       'P',
       PV_email);

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se inserta registro';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:= 'ERROR: COK_JOURNAL_NEW.Cop_Inserta_Journal - '||SUBSTR(SQLERRM,1,500);
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Inserta_Journal;
  
  PROCEDURE Cop_Actualiza_Journal(PN_SECUENCIA   NUMBER,
                                  PD_FECHA       DATE,
                                  PV_ESTADO      Varchar2,                                                           
                                  PV_ERROR       OUT VARCHAR2) IS

    ---------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    lv_programa                varchar2(2000) := 'COK_JOURNAL_NEW.COP_ACTUALIZA_JOURNAL';
    lv_id_proceso_scp           varchar2(100) := 'SCP_ACTUALIZA_JOURNAL';
    lv_unidad_registro_scp      varchar2(30) := 'Act co_journal_procesa';
    lv_error_scp                varchar2(500);
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_error_scp                number := 0;
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ---------------------------------------------------------------------- 
    if PV_ESTADO is null then
      update CO_JOURNAL_PROCESA_FECHAS set fecha_proceso=PD_FECHA
      where secuencia = PN_SECUENCIA
      and estado = 'P';
    else
      update CO_JOURNAL_PROCESA_FECHAS 
      set fecha_proceso=PD_FECHA,estado='F'
      where secuencia = PN_SECUENCIA
      and estado = 'P';
    end if;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se actualiza registro';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:= 'ERROR: COK_JOURNAL_NEW.Cop_Actualiza_Journal - '||SUBSTR(SQLERRM,1,500);
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := substr(pv_error,1,4000);
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Actualiza_Journal;
  
  PROCEDURE Cop_Inserta_Archivo(Pv_Region     NUMBER,
                                Pv_FechaIni   DATE,
                                Pv_FechaFin   DATE,     
                                Pt_Journal    IN Tab_Journal,
                                PV_ERROR      OUT VARCHAR2) IS
  
  lc_archivo CLOB;
  lv_datos   varchar2(4000);
  len        BINARY_INTEGER;
  ---------------------------------------------------------------
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_INSERTA_ARCHIVO';
  lv_id_proceso_scp           varchar2(100) := 'SCP_INSERTA_ARCHIVO';
  lv_unidad_registro_scp      varchar2(30) := 'Inserta Archivo';
  lv_error_scp                varchar2(500);
  lv_proceso_par_scp          varchar2(30);
  lv_valor_par_scp            varchar2(4000);
  lv_descripcion_par_scp      varchar2(500);
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);
  ln_error_scp                number := 0;
  ln_id_bitacora_scp          number := 0;
  ln_total_registros_scp      number := 0;
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  ln_id_detalle_scp           number := 0;
  ---------------------------------------------------------------  
  
  BEGIN
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------     
  
    DBMS_LOB.createtemporary(lc_archivo, TRUE);
    dbms_lob.open(lc_archivo, dbms_lob.lob_readwrite);
    for i in Pt_Journal.first .. Pt_Journal.last loop
      lv_datos := Pt_Journal(I) || chr(13);
      LEN      := LENGTH(lv_datos);
      --exit when i = 5;
      dbms_lob.writeappend(lc_archivo, len, lv_datos);
      -- exit when i > 100;
    end loop;
    INSERT INTO CO_JOURNAL_FECHA_ARCHIVO
    VALUES
      (Pv_Region,
       TO_DATE(Pv_FechaIni, 'DD/MM/RRRR HH24:MI:SS'),
       LC_ARCHIVO,
       TO_DATE(Pv_FechaFin, 'DD/MM/RRRR HH24:MI:SS'));
    COMMIT;
  
    --PC_LOB := LC_ARCHIVO;
    LEN := DBMS_LOB.getlength(LC_ARCHIVO);
    DBMS_LOB.erase(LC_ARCHIVO, LEN);
    dbms_lob.trim(lc_archivo, 0);  
    DBMS_LOB.CLOSE(LC_ARCHIVO);
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
  ----------------------------------------------------------------------
  lv_mensaje_apl_scp := 'Se inserta archivo';
  lv_mensaje_tec_scp := null;
  lv_mensaje_acc_scp := null;
  SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            'JOURNAL',
                                            null,
                                            null,
                                            null,
                                            'N',
                                            ln_id_detalle_scp,
                                            ln_error_scp,
                                            lv_error_scp);
  ----------------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);   
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:= 'ERROR: COK_JOURNAL_NEW.Cop_Inserta_Journal - '||SUBSTR(SQLERRM,1,500);
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := pv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  END Cop_Inserta_Archivo;
  
  Procedure Cop_insert_util_sh(Pv_region varchar2,
                               pd_fecha_inicio date,
                               pd_fecha_fin date,
                               PV_email  varchar2,
                               pv_error out varchar2) is
  
  cursor c_verifi_fechas(cv_fecha date)is
  select fecha_ingreso
  from CO_JOURNAL_PROCESA_FECHAS t
  where trunc(fecha_ingreso) = trunc(sysdate)
   and region= Pv_region
   and estado = 'P' 
   and to_date(cv_fecha,'dd/mm/rrrr') >= trunc(fecha_desde)
   and to_date(cv_fecha,'dd/mm/rrrr') <= trunc(fecha_hasta);

  ln_secuencia number;
  ld_valor date;
  lv_comando   varchar2(2000);
  lv_salida    varchar2(2000);
  lv_error     varchar2(2000);
  LE_ERROR exception;
  ln_valor   number;
  ld_fecha_min date;
  ln_valor_dia number;
  
  ---------------------------------------------------------------
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  lv_programa                 varchar2(2000) := 'COK_JOURNAL_NEW.COP_INSERT_UTIL_SH';
  lv_id_proceso_scp           varchar2(100) := 'SCP_INSERTA_UTIL_SH';
  lv_unidad_registro_scp      varchar2(30) := 'Inserta Util SH';
  lv_error_scp                varchar2(500);
  lv_proceso_par_scp          varchar2(30);
  lv_valor_par_scp            varchar2(4000);
  lv_descripcion_par_scp      varchar2(500);
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);
  ln_error_scp                number := 0;
  ln_id_bitacora_scp          number := 0;
  ln_total_registros_scp      number := 0;
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  ln_id_detalle_scp           number := 0;
  ---------------------------------------------------------------
  
  begin
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------   
     
     open c_verifi_fechas(pd_fecha_inicio);
     fetch c_verifi_fechas into ld_valor;
     close c_verifi_fechas;
     
     select pd_fecha_fin - pd_fecha_inicio into ln_valor_dia from dual;
     
     if ln_valor_dia > 6 then
        lv_error:='Error: rango de fechas muy amplio usar maximo 5 d�as';
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := lv_error;
        lv_mensaje_tec_scp := null;
        lv_mensaje_acc_scp := null;
        SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  to_char(ln_valor_dia),
                                                  'JOURNAL',
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
        ----------------------------------------------------------------------------
        raise LE_ERROR;
      end if;
       
     
     if ld_valor is not null then
       select sysdate - 30/(60*24) into ld_fecha_min from dual;
       if ld_valor >= ld_fecha_min then
         lv_error:='Error: Se encuentra en proceso de ejecucion la fecha de inicio ingresada.';
       else         
         lv_error:='Error: comunicar a sistemas';
       end if;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := lv_error;
        lv_mensaje_tec_scp := null;
        lv_mensaje_acc_scp := null;
        SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  null,
                                                  'JOURNAL',
                                                  to_char(ld_valor),
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
        ----------------------------------------------------------------------------
       raise LE_ERROR;
     end if;
     
     open c_verifi_fechas(pd_fecha_fin);
     fetch c_verifi_fechas into ld_valor;
     close c_verifi_fechas;
     
     if ld_valor is not null then
       lv_error:='Error: Se encuentra en proceso de ejecucion la fecha Final ingresada.';
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := lv_error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                to_char(ld_valor),
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
       
       raise LE_ERROR;
     end if;
     
     Cop_Inserta_Journal(PN_REGION => Pv_region,
                         PD_FECHA_DESDE => trunc(pd_fecha_inicio),
                         PD_FECHA_HASTA => trunc(pd_fecha_fin),
                         PV_email =>  PV_email,  
                         PN_SECUENCIA => ln_secuencia,                                                
                         PV_ERROR => lv_error);
     commit;
     
     if lv_error is not null then
       lv_error:='Error: al registrar los datos Cop_Inserta_Journal'||lv_error;
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp := lv_error;
       lv_mensaje_tec_scp := null;
       lv_mensaje_acc_scp := null;
       SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                 lv_mensaje_apl_scp,
                                                 lv_mensaje_tec_scp,
                                                 lv_mensaje_acc_scp,
                                                 0,
                                                 null,
                                                 'JOURNAL',
                                                 to_char(ld_valor),
                                                 null,
                                                 null,
                                                 'N',
                                                 ln_id_detalle_scp,
                                                 ln_error_scp,
                                                 lv_error_scp);
       ----------------------------------------------------------------------------
       raise LE_ERROR;
     end if;
     lv_comando := '/bin/sh /procesos/gsioper/journal/cop_generar_mv_rango.sh ' || ln_secuencia || ' '||'&'; 
     osutil_noreturn.runoscmd(lv_comando, lv_salida);

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Insert Util SH Fin Exito';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
     
     pv_error:=null;
  -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
  scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  exception
     WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := LV_ERROR;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                'JOURNAL',
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    when others then
    pv_error:= 'ERROR: COK_JOURNAL_NEW.Cop_insert_util_sh - '||SUBSTR(SQLERRM,1,500);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de fin de ejecucion x error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := pv_error;
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              lv_mensaje_apl_scp,
                                              lv_mensaje_tec_scp,
                                              lv_mensaje_acc_scp,
                                              0,
                                              null,
                                              'JOURNAL',
                                              null,
                                              null,
                                              null,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  end Cop_insert_util_sh;
  
END COK_JOURNAL_NEW;
/
