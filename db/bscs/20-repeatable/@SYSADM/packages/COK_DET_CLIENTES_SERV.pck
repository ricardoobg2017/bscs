CREATE OR REPLACE package COK_DET_CLIENTES_SERV is

  -- Author  : RCABRERA
  -- Created : 21/04/2005 10:46:46
  -- Purpose : 

  FUNCTION SET_CAMPOS_SERV(pvError out varchar2) RETURN VARCHAR2;

  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;

  PROCEDURE CARGA_TABLA(pdFechaPeriodo in date);

end COK_DET_CLIENTES_SERV;
/
CREATE OR REPLACE package body COK_DET_CLIENTES_SERV is

  --------------------------------------------
  -- SET_CAMPOS_SERVICIOS
  --------------------------------------------
  FUNCTION SET_CAMPOS_SERV(pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia      varchar2(32000);
    lvNombreServicio varchar2(100);
  
    --SIS RCA: Agregado el "replace", para evitar problemas con nombres de servicio
    -- que contienen el caracter punto "."
  
    cursor c_servicios is
      select shdes from mpusntab;
  
    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.cob_servicios /*cob_servicios*/
       where cargo2 = 0;
  
    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.cob_servicios /*cob_servicios*/
       where cargo2 = 1;
  
  BEGIN
    lvSentencia := '';
    for s in c_servicios loop
      lvSentencia := lvSentencia || s.shdes || ' NUMBER default 0, ';
    end loop;
  
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_SERV;

  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    --
    lvSentencia VARCHAR2(32000);
    lvMensErr   VARCHAR2(1000);
    --
  BEGIN
    begin
      execute immediate 'drop table CO_DETCLISER_' ||
                        to_char(pdFechaPeriodo, 'ddMMyyyy');
    exception
      when others then
        null;
    end;
  
    lvSentencia := 'CREATE TABLE CO_DETCLISER_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( CUENTA               VARCHAR2(24),' ||
                   '  CO_ID            NUMBER,' ||
                   '  CUSTOMER_ID            NUMBER,' ||
                   '  DN_NUM            VARCHAR2(63),' ||
                   '  PLAN              VARCHAR2(30),' ||
                   '  PRODUCTO              VARCHAR2(30),' ||
                  --'  CANTON                VARCHAR2(40),' ||
                  --'  PROVINCIA             VARCHAR2(25),' ||
                   '  APELLIDOS             VARCHAR2(40),' ||
                   '  NOMBRES               VARCHAR2(40),' ||
                   '  RUC                   VARCHAR2(20),' ||
                  /*                   '  FORMA_PAGO            VARCHAR2(58),' ||
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           '  TARJETA_CUENTA        VARCHAR2(25),' ||
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           '  FECH_EXPIR_TARJETA    VARCHAR2(20),' ||
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           '  TIPO_CUENTA           VARCHAR2(40),' ||*/
                  --'  FECH_APER_CUENTA      DATE,' ||
                   '  TELEFONO1             VARCHAR2(25),' ||
                   '  TELEFONO2             VARCHAR2(25),' ||
                   '  DIRECCION             VARCHAR2(70),' ||
                   '  DIRECCION2            VARCHAR2(200),' ||
                  --'  GRUPO                 VARCHAR2(10),' ||
                   cok_det_clientes_serv.set_campos_serv(lvMensErr) ||
                  
                  --'  NUM_FACTURA           VARCHAR2(30),' ||
                   '  COMPANIA              NUMBER default 0)' ||
                  
                   ' tablespace DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K' || '   next 1040K' ||
                   '   minextents 1' || '   maxextents 505' ||
                   '   pctincrease 0)';
    --'  storage  (initial 256K    next 256K' ||
    --'    minextents 1    maxextents 505' ||
    --'    pctincrease 0)';
  
    null;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    /*    lvSentencia := 'create index IDX_DETCLISER_' ||
     to_char(pdFechaPeriodo, 'ddMMyyyy') ||
     ' on CO_DETCLISER_' ||
     to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (CUENTA)' ||
     'tablespace DATA ' || 'pctfree 10 ' || 'initrans 2 ' ||
     'maxtrans 255 ' || 'storage ' || '( initial 256K ' ||
     '  next 256K ' || '  minextents 1 ' ||
     '  maxextents unlimited ' ||
    --'  maxextents 505 '||
     '  pctincrease 0 )';*/
  
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'create public synonym CO_DETCLISER_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' for sysadm.CO_DETCLISER_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    Lvsentencia := 'grant all on CO_DETCLISER_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    return 1;
  
  EXCEPTION
    when others then
      return 0;
  END CREA_TABLA;

  PROCEDURE CARGA_TABLA(pdFechaPeriodo in date) IS
  
    cursor servicios is
      select co_id, snshdes, valor
        from rep_det_clie_serv_sum
       ORDER BY CO_ID;
  
    LV_COMANDO   VARCHAR2(3000);
    LV_CO_ID_ANT NUMBER;
  
  BEGIN
  
    execute immediate 'ALTER SESSION ENABLE PARALLEL DML';
  
    begin
      execute immediate 'drop table rep_det_clie_serv_sum';
    exception
      when others then
        null;
    end;
  
    execute immediate '  create table rep_det_clie_serv_sum as ' ||
                      'select /*+PARALLEL (rep_det_clie_serv,4) */ dn_num, custcode, co_id, fecha, snshdes, sum(valor) valor, costcenter_id, ' ||
                      'prgcode from rep_det_clie_serv group by dn_num, ' ||
                      'custcode, co_id, fecha, snshdes, costcenter_id, prgcode ';
  
    execute immediate 'create index IX_REP_DET_CLI2 on REP_DET_CLIE_SERV_SUM (CUSTCODE)';
    execute immediate 'create index IX_REP_DET_CLI3 on REP_DET_CLIE_SERV_SUM (CO_ID)';
  
    execute immediate 'INSERT /*+PARALLEL (CO_DETCLISER_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                      ',4) */ INTO CO_DETCLISER_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') || ' ' ||
                      '(DN_NUM, CUENTA, CO_ID, compania, producto) ' ||
                      'SELECT DISTINCT DN_NUM, CUSTCODE, CO_ID, COSTCENTER_ID, PRGCODE ' ||
                      'FROM rep_det_clie_serv_sum ';
  
    execute immediate 'create index ix_co_detcliser_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                      ' on co_detcliser_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') || '(co_id)';
    execute immediate 'create index ix_co_detcliser_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                      'B on co_detcliser_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') || '(CUENTA)';
  
    execute immediate 'UPDATE CO_DETCLISER_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                      ' R SET CUSTOMER_ID = ' ||
                      '(SELECT CUSTOMER_ID FROM CUSTOMER_ALL T WHERE T.CUSTCODE = R.CUENTA) ';
  
    LV_CO_ID_ANT := -1;
  
    FOR I IN SERVICIOS LOOP
      --      dbms_output.put_line(I.co_id || '|' || I.snshdes || '|' || I.valor);
    
      LV_COMANDO := 'UPDATE CO_DETCLISER_' ||
                    to_char(pdFechaPeriodo, 'ddMMyyyy') || ' SET ' ||
                    I.SNSHDES || ' = ' || I.VALOR || ' WHERE CO_ID = ' ||
                    I.CO_ID;
    
      EXECUTE IMMEDIATE LV_COMANDO;
    
      IF LV_CO_ID_ANT <> I.CO_ID THEN
      
        LV_COMANDO := 'UPDATE CO_DETCLISER_' ||
                      to_char(pdFechaPeriodo, 'ddMMyyyy') || ' e SET ' ||
                      ' (apellidos,nombres,ruc,telefono1,telefono2,direccion, ' ||
                      ' direccion2) =  (select cclname,ccfname, x.cssocialsecno, ' ||
                      ' cctn,cctn2,ccline3,ccline4 from ccontact_all x where ' ||
                      ' x.customer_id = e.customer_id and ccbill = ''X'' ) ' ||
                      ' WHERE CO_ID = ' || I.CO_ID;
      
        EXECUTE IMMEDIATE LV_COMANDO;
      
      END IF;
    
      LV_CO_ID_ANT := I.CO_ID;
    
    --  dbms_output.put_line(LV_COMANDO);
    
    END
    
     LOOP
    ;
  
    COMMIT;
  
    lv_comando := 'update CO_DETCLISER_' ||
                  to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                  ' p set plan = (select des ' ||
                  ' from rateplan a, contract_all b ' ||
                  ' where a.tmcode = b.tmcode and b.co_id = p.co_id) ';
  
    EXECUTE IMMEDIATE LV_COMANDO;
  
  END CARGA_TABLA;

end COK_DET_CLIENTES_SERV;
/

