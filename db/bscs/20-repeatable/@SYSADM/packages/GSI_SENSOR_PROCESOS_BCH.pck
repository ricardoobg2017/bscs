CREATE OR REPLACE package GSI_SENSOR_PROCESOS_BCH is

  -- Author  : Natividad del Rocio Mantilla L�pez
  -- Created : 19/06/2007 17:19:01
  -- Purpose : Determino los BCH que estan corriendo y los que habria que lanzar
  
PROCEDURE CENSA_PROCESOS;

end GSI_SENSOR_PROCESOS_BCH;
/
CREATE OR REPLACE package body GSI_SENSOR_PROCESOS_BCH is

PROCEDURE CENSA_PROCESOS IS

  --- Leo mi tabla maestra de los BCH,
  --- analizo los BCH que est�n corriendo (P= procesandose)
  --- para determinaron si Terminaron o no y cambiarles el estado.
  CURSOR C1 is 
     select ciclo from GSI_EJECUCION_PROCESOS_BCH
     where estado = 'P';
     

  CURSOR C2 is 
     select CICLO from GSI_EJECUCION_PROCESOS_BCH
     where estado in ('A','D') order by prioridad;
     
     
   
Var_Billseqno      number:=0;
Cuantos_Estados    number;
Estado             Varchar2(10);

Cuantos_Corriendo  number;
Var_Ciclo          Varchar2(10);
Reg_Actualizados   number;
     
     
Reg_C2      C2%Rowtype;

BEGIN

   -- Actualizo a estado P = Procesandose
   -- para los BCH que est�n corriendo.
   update GSI_EJECUCION_PROCESOS_BCH
   set    estado = 'P' -- Procesandose
   where  ciclo in (select billcycle from bch_monitor_table);
   commit;
   
   --- marco como Detenidos a los BCH que han bajado
   update GSI_EJECUCION_PROCESOS_BCH 
   set    ESTADO= 'D' 
   where  ciclo in (select billcycle from bch_monitor_table where STOPIND is null);
   commit;
   
   for i in C1 loop          
   
        -- De los ciclos corriendo o procesandose 
        -- voy a determinar el billseqno correspondiente
        select l.billseqno into Var_Billseqno from bch_process l
        where  billcycle = i.ciclo;               
                      
                      
        select COUNT(distinct j.customer_process_status) into Cuantos_Estados  from bch_process_cust j
        where j.billseqno =  Var_Billseqno;
        
        -- Verifico si ya termin� cada ciclo, al final en la tabla bch_process_cust
        -- ese ciclo (billseqno) debe desaparecer o quedar todos marcados como S.
        
        if Cuantos_Estados < 2 then
           
           Estado := 'X'; -- valor default, si no lo encuentra en la tabla,ya termino el ciclo.
           select distinct j.customer_process_status into Estado from bch_process_cust j
           where j.billseqno =  Var_Billseqno;
           
           if Estado in ('S','X') then
           
              update GSI_EJECUCION_PROCESOS_BCH 
              set    ESTADO= 'F' --Finalizado
              where  ciclo = i.ciclo;
              COMMIT;
              
           end if;
        
        end if;          
           
   end loop;
   commit;    
         

   --- Cuento para ver los cuantos BCH estan corriendo.
   select count(*) into Cuantos_Corriendo from GSI_EJECUCION_PROCESOS_BCH
   where estado = 'P';
  
  
  if Cuantos_Corriendo < 3 THEN
  
   open C2;
   fetch C2 into Reg_C2;

   Reg_Actualizados:= 0;
   LOOP 
     EXIT WHEN C2%NOTFOUND  or (Cuantos_Corriendo + Reg_Actualizados) = 3;
     
     
       update GSI_EJECUCION_PROCESOS_BCH 
       set    LANZAR = 'S' --Finalizado
       where  ciclo = Reg_C2.ciclo;
       COMMIT;   
     
     Reg_Actualizados:= Reg_Actualizados + 1 ;
     
     FETCH C2 INTO Reg_C2;
     
   END LOOP;
   close C2;
   
 end if;  


  
     
   
        
    
 
  
END CENSA_PROCESOS;

end GSI_SENSOR_PROCESOS_BCH;
/

