CREATE OR REPLACE PACKAGE fin_provision IS

  -- Author  : MJOFAT
  -- Created : 10/06/2008 11:40:11
  -- Purpose : 
  -- 10189
  -- SIS Luis Flores 
  -- PDS Fernando Ortega
  -- RGT Ney Miranda
  -- Ultimo Cambio: 30/07/2015
  -- Nueva logica para provisiones para SAP
  
  gv_aplicacion VARCHAR2(50);

  FUNCTION f_userso RETURN VARCHAR2;

  PROCEDURE p_descripcion_cta(pv_cta VARCHAR2, pv_descripcion OUT VARCHAR2);

  PROCEDURE p_gen_preasiento(pd_fecha DATE, pv_error OUT VARCHAR2);
  PROCEDURE crear_sumarizado_prov_sap(pd_fecha      IN DATE,
                                      pv_msj_trama2 IN OUT VARCHAR2);

  PROCEDURE p_carga_servicios(pv_error OUT VARCHAR2);

  PROCEDURE p_actualiza_ctas(pv_error OUT VARCHAR2);

  PROCEDURE p_genera_prorateo(pv_error OUT VARCHAR2);

  PROCEDURE replica_financiero(pd_fecha IN DATE, pv_error OUT VARCHAR2);

  FUNCTION crear_trama(PV_PRODUCTO      IN VARCHAR2,
                       PV_REGION        IN VARCHAR2,
                       PV_CICLO         IN VARCHAR2,
                       PV_IDENTIFICADOR OUT VARCHAR2,
                       ERROR            OUT VARCHAR2) RETURN CLOB;
  --
  FUNCTION obtener_valor_parametro_sap(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;
  --
  PROCEDURE envia_tramas_provision_sap(PD_FECHA     IN DATE, --FECHA DE CORTE
                                       PV_RESULTADO OUT VARCHAR2);
  --
  PROCEDURE reenvia_polizas(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2);
  --
  PROCEDURE anula_polizas(pd_fecha     IN DATE, --FECHA DE CORTE
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2);
  --
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2);

END fin_provision;
/
CREATE OR REPLACE PACKAGE BODY FIN_PROVISION IS
  /**********************************************************************/
  --PROCEDIMIENTO : Funcion que retorna el usuario del S.O. que se      --
  --                encuentra conectado                                 --
  --Nombre        : CLS Mariuxi Jofat.                                  --
  --Fecha         : 07/07/2008                                          --
  /**********************************************************************/
  pn_id_transaccion NUMBER := sc_id_transaccion_sap.nextval;

  FUNCTION f_userso RETURN VARCHAR2 IS
    pv_usuario VARCHAR2(60);
  BEGIN
    SELECT upper(SYS_CONTEXT('USERENV', 'OS_USER'))
      INTO pv_usuario
      FROM dual;
  
    RETURN pv_usuario;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN USER;
  END f_userso;

  PROCEDURE p_descripcion_cta(pv_cta VARCHAR2, pv_descripcion OUT VARCHAR2) IS
  
    /**********************************************************************/
    --PROCEDIMIENTO : Procedimiento que retorna la descripcion de una cta --
    --                especifica [3470] Provision de Proyeccion Postpago  --
    --Nombre        : CLS Rolando Herrera B.                              --
    --Fecha         : 06/06/2008                                          --
    /**********************************************************************/
  
    CURSOR c_cta IS
      SELECT descr
        FROM sysadm.PS_GL_ACCOUNT_TBL@finsys
       WHERE setid = 'PORTA'
         AND EFF_STATUS = 'A'
         AND account = pv_cta
       ORDER BY EFFDT DESC;
  
    lv_cta   VARCHAR2(200);
    lb_found BOOLEAN;
  
  BEGIN
  
    OPEN c_cta;
    FETCH c_cta
      INTO lv_cta;
    lb_found := c_cta%FOUND;
    CLOSE c_cta;
  
    IF NOT lb_found THEN
      lv_cta := 'Cta. no configurada';
    END IF;
  
    pv_descripcion := lv_cta;
  
  END p_descripcion_cta;

  /**********************************************************************/
  --PROCEDIMIENTO : Procedimiento que genera el preasiento en peoplesoft--
  --                [3470] Provision de Proyeccion Postpago             --
  --Nombre        : CLS Mariuxi Jofat Ch.                               --
  --Fecha         : 09/06/2008                                          --
  /**********************************************************************/

  --=====================================================================================--
  -- Modificado por: RGT Ney Miranda T.
  -- L�der proyecto: SIS Luis Flores
  -- Lider PDS:      RGT Fernando Ortega
  -- Fecha de modificaci�n: 24/04/2015
  -- Proyecto:  Proyecto: [10189] API BSCS SAP
  -- Proposito: Generaci�n de asiento contable  sumarizados por cuenta contable para el envio de polizas a SAP
  --=====================================================================================-- 
  --

  PROCEDURE p_gen_preasiento(pd_fecha DATE, pv_error OUT VARCHAR2) IS
  
    CURSOR c_reporte(cv_ciclo VARCHAR2) IS
      SELECT a.ciudad,
             a.servicio,
             MAX(a.CODIGO_CONTABLE) codigo_contable,
             decode(a.CODIGO_CONTABLE, '4201011001', '000003', ' ') producto_asi,
             SUM(round(a.valor_provisiona, 2)) Valor,
             id_ciclo
        FROM reporte_calc_prorrateo a
       WHERE a.id_ciclo = cv_ciclo
         AND a.valor_provisiona <> 0
       GROUP BY a.ciudad,
                a.servicio,
                decode(a.CODIGO_CONTABLE, '4201011001', '000003', ' '),
                id_ciclo
       ORDER BY a.ciudad, codigo_contable, a.servicio;
  
    CURSOR c_valida_ciclo IS
      SELECT DISTINCT id_ciclo FROM reporte_calc_prorrateo;
    pv_msj_trama2 VARCHAR2(2500);
    PV_APLICACION VARCHAR2(100) := 'FIN_PROVISION.P_GEN_PREASIENTO';
  
    i             NUMBER;
    ln_sec        NUMBER;
    ln_sumgye     NUMBER := 0;
    ln_sumuio     NUMBER := 0;
    lv_transid    VARCHAR2(30);
    ln_transline  NUMBER;
    ln_linea      NUMBER := 2;
    lc_ctacontab  VARCHAR2(200);
    lv_referencia VARCHAR2(300);
    lv_ciclo      VARCHAR2(100);
    ln_valor      NUMBER;
  
    -- INICIO 10189 RGT NEY MIRANDA T  
    LV_SENTENCIA VARCHAR2(50);
    -- FIN 10189 RGT NEY MIRANDA T        
    /*5359 JBA.- asignar tipo de IVA y ciclo al preasiento generado*/
  
    CURSOR C_VERIFICA_CUENTAS(CV_CUENTA VARCHAR2) IS
      SELECT A.USA_TIPO_IVA, A.USA_CICLO
        FROM GL_PARAMETROS_CTAS_CONTAB A
       WHERE A.CUENTA = CV_CUENTA
         AND A.SETID = 'PORTA';
  
    LC_VERIF_CTAS C_VERIFICA_CUENTAS%ROWTYPE;
    LB_FOUND_CTA  BOOLEAN;
    LV_CICLO_5359 VARCHAR2(5);
    LV_TIPO_IVA   VARCHAR2(5);
    --5359 FIN
    le_error EXCEPTION;
    lv_error VARCHAR2(3000);
    CURSOR c_valida_poliza_existente(cv_fecha DATE) IS
      SELECT COUNT(*)
        FROM gsib_tramas_sap d
       WHERE d.referencia LIKE '%PROVISION BSCS%'
         AND d.fecha_corte = cv_fecha
         AND d.estado NOT IN ('ERROR', 'ANULADA');
    lc_valida_poliza_existente NUMBER := 0;
  BEGIN
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           '**** INICIO GENERA ASIENTOS PROVISION BSCS ' ||
                           'FECHA DE CORTE :' || pd_fecha ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
    OPEN c_valida_poliza_existente(pd_fecha);
    FETCH c_valida_poliza_existente
      INTO lc_valida_poliza_existente;
    CLOSE c_valida_poliza_existente;
    IF lc_valida_poliza_existente > 0 THEN
      lv_error := 'Ya existen polizas Generadas para Provision BSCS con fecha de corte ' ||
                  pd_fecha;
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      RAISE le_error;
    END IF;
    LV_SENTENCIA := 'truncate table ps_jgen_acct_entry_prov';
    EXEC_SQL(LV_SENTENCIA);
  
    GSIB_SECURITY.dotracem(PV_APLICACION,
                           'Se elimino registros de la tabla temporal PS_JGEN_ACCT_ENTRY_PROV',
                           pn_id_transaccion);
    LV_SENTENCIA := 'truncate table ps_pr_jgen_acct_en_prov';
  
    EXEC_SQL(LV_SENTENCIA);
    GSIB_SECURITY.dotracem(PV_APLICACION,
                           'Se elimino registros de la tabla temporal PS_PR_JGEN_ACCT_EN_PROV',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Inicio de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV y PS_PR_JGEN_ACCT_EN_PROV ',
                           pn_id_transaccion);
  
    FOR j IN c_valida_ciclo LOOP
      --genera un asiento por ciclo
    
      ln_sumgye := 0;
      ln_sumuio := 0;
      ln_linea  := 2;
    
      SELECT nvl(MAX((to_number(substr(TRANSACTION_ID,
                                       5,
                                       length(TRANSACTION_ID) - 4)))),
                 0) + 1
        INTO ln_sec
        FROM ps_jgen_acct_entry@finsys
       WHERE BUSINESS_UNIT = 'CONEC'
         AND LEDGER_GROUP = 'REAL'
         AND LEDGER = 'LOCAL'
         AND transaction_id LIKE 'PBSC%';
    
      lv_transid := 'PBSC' || ln_sec;
    
      COMMIT;
    
      SELECT nvl(MAX(TRANSACTION_LINE), 2) + 1
        INTO ln_transline
        FROM ps_jgen_acct_entry@finsys --nm
       WHERE transaction_id = lv_transid;
    
      COMMIT;
    
      FOR i IN c_reporte(j.id_ciclo) LOOP
        ln_linea := ln_linea + 1;
      
        IF i.id_ciclo = '01' THEN
          lv_ciclo := 'Primer ciclo';
        ELSIF i.id_ciclo = '02' THEN
          lv_ciclo := 'Segundo ciclo';
        ELSIF i.id_ciclo = '03' THEN
          lv_ciclo := 'Tercer ciclo';
        ELSIF i.id_ciclo = '04' THEN
          lv_ciclo := 'Cuarto ciclo';
        ELSIF i.id_ciclo = '05' THEN
          lv_ciclo := 'Quinto ciclo';
        ELSIF i.id_ciclo = '06' THEN
          lv_ciclo := 'Sexto ciclo';
        ELSIF i.id_ciclo = '07' THEN
          lv_ciclo := 'Septimo ciclo';
        ELSIF i.id_ciclo = '08' THEN
          lv_ciclo := 'Octavo ciclo';
        ELSIF i.id_ciclo = '09' THEN
          lv_ciclo := 'Noveno ciclo';
        ELSIF i.id_ciclo = '10' THEN
          lv_ciclo := 'Decimo ciclo';
        ELSE
          lv_ciclo := 'Configurar ciclo';
        END IF;
      
        IF substr(i.codigo_contable, 1, 1) = '4' THEN
        
          FIN_PROVISION.p_descripcion_cta(I.CODIGO_CONTABLE, lc_ctacontab);
          lv_referencia := lc_ctacontab || ' Provisi�n mes ' ||
                           to_char(pd_fecha,
                                   'Month',
                                   'NLS_DATE_LANGUAGE=spanish') || ' del ' ||
                           to_char(pd_fecha, 'yyyy') || ' ' || lv_ciclo;
        
          IF i.ciudad = 'GYE' THEN
            ln_sumgye := ln_sumgye + i.valor;
          ELSE
            ln_sumuio := ln_sumuio + i.valor;
          END IF;
        
          ln_valor := i.valor * (-1);
        
          BEGIN
            INSERT INTO ps_jgen_acct_entry_prov
              (BUSINESS_UNIT,
               TRANSACTION_ID,
               TRANSACTION_LINE,
               LEDGER_GROUP,
               LEDGER,
               ACCOUNTING_DT,
               APPL_JRNL_ID,
               BUSINESS_UNIT_GL,
               FISCAL_YEAR,
               ACCOUNTING_PERIOD,
               JOURNAL_ID,
               JOURNAL_DATE,
               JOURNAL_LINE,
               ACCOUNT,
               OPERATING_UNIT,
               PRODUCT,
               CURRENCY_CD,
               FOREIGN_CURRENCY,
               RT_TYPE,
               RATE_MULT,
               RATE_DIV,
               MONETARY_AMOUNT,
               FOREIGN_AMOUNT,
               DOC_TYPE,
               DOC_SEQ_DATE,
               LINE_DESCR,
               GL_DISTRIB_STATUS,
               ALTACCT,
               DEPTID,
               FUND_CODE,
               CLASS_FLD,
               PROGRAM_CODE,
               BUDGET_REF,
               AFFILIATE,
               AFFILIATE_INTRA1,
               AFFILIATE_INTRA2,
               CHARTFIELD1,
               CHARTFIELD2,
               CHARTFIELD3,
               PROJECT_ID,
               STATISTICS_CODE,
               STATISTIC_AMOUNT,
               MOVEMENT_FLAG,
               DOC_SEQ_NBR,
               JRNL_LN_REF,
               IU_SYS_TRAN_CD,
               IU_TRAN_CD,
               IU_ANCHOR_FLG,
               PROCESS_INSTANCE)
            VALUES
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               'BSCS',
               'CONEC',
               to_char(pd_fecha, 'yyyy'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_linea,
               i.codigo_contable,
               i.ciudad || '_999',
               i.producto_asi,
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               ln_valor,
               ln_valor,
               'CPBSCS',
               pd_fecha,
               lv_referencia,
               'N',
               j.id_ciclo,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0);
          
            /*5359 JBA.- asignar tipo de IVA y ciclo al preasiento generado*/
            BEGIN
              --verifico si la cuenta aplica tipo de iva y ciclo
              OPEN C_VERIFICA_CUENTAS(i.codigo_contable);
              FETCH C_VERIFICA_CUENTAS
                INTO LC_VERIF_CTAS;
              LB_FOUND_CTA := C_VERIFICA_CUENTAS%FOUND;
              CLOSE C_VERIFICA_CUENTAS;
            
              IF LB_FOUND_CTA THEN
              
                LV_CICLO_5359 := ' ';
                LV_TIPO_IVA   := ' ';
              
                IF LC_VERIF_CTAS.USA_CICLO = 'Y' THEN
                  LV_CICLO_5359 := 'PROV';
                END IF;
                IF LC_VERIF_CTAS.USA_TIPO_IVA = 'Y' THEN
                  LV_TIPO_IVA := 'PROV';
                END IF;
              
                IF LV_TIPO_IVA <> ' ' OR LV_CICLO_5359 <> ' ' THEN
                  /* INSERT INTO ps_pr_jgen_acct_en_prov
                    (BUSINESS_UNIT,
                     TRANSACTION_ID,
                     TRANSACTION_LINE,
                     LEDGER_GROUP,
                     LEDGER,
                     PR_JRNL_LN_REF2,
                     PR_JRNL_TIPO_IVA,
                     PR_JRNL_CICLO)
                  VALUES
                    ('CONEC',
                     lv_transid,
                     ln_transline,
                     'REAL',
                     'LOCAL',
                     ' ',
                     LV_TIPO_IVA,
                     LV_CICLO_5359);*/
                
                  null;
                END IF;
              
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                pv_error := 'Error 1, al insertar el preasiento en la tabla ps_pr_jgen_acct_en_prov ' ||
                            SQLERRM;
                gsib_security.dotraceh(pv_aplicacion,
                                       pv_error,
                                       pn_id_transaccion);
              
                ROLLBACK;
            END;
            /*5359 - FIN*/
          
            ln_transline := ln_transline + 1;
          EXCEPTION
            WHEN OTHERS THEN
              pv_error := 'Error al insertar el preasiento' || SQLERRM;
              gsib_security.dotraceh(pv_aplicacion,
                                     pv_error,
                                     pn_id_transaccion);
              ROLLBACK;
          END;
        END IF;
      END LOOP;
    
      FIN_PROVISION.p_descripcion_cta('1102010150', lc_ctacontab);
      lv_referencia := lc_ctacontab || ' Provisi�n mes ' ||
                       to_char(pd_fecha,
                               'Month',
                               'NLS_DATE_LANGUAGE=spanish') || ' del ' ||
                       to_char(pd_fecha, 'yyyy') || ' ' || lv_ciclo;
    
      INSERT INTO ps_jgen_acct_entry_prov
        (BUSINESS_UNIT,
         TRANSACTION_ID,
         TRANSACTION_LINE,
         LEDGER_GROUP,
         LEDGER,
         ACCOUNTING_DT,
         APPL_JRNL_ID,
         BUSINESS_UNIT_GL,
         FISCAL_YEAR,
         ACCOUNTING_PERIOD,
         JOURNAL_ID,
         JOURNAL_DATE,
         JOURNAL_LINE,
         ACCOUNT,
         OPERATING_UNIT,
         PRODUCT,
         CURRENCY_CD,
         FOREIGN_CURRENCY,
         RT_TYPE,
         RATE_MULT,
         RATE_DIV,
         MONETARY_AMOUNT,
         FOREIGN_AMOUNT,
         DOC_TYPE,
         DOC_SEQ_DATE,
         LINE_DESCR,
         GL_DISTRIB_STATUS,
         ALTACCT,
         DEPTID,
         FUND_CODE,
         CLASS_FLD,
         PROGRAM_CODE,
         BUDGET_REF,
         AFFILIATE,
         AFFILIATE_INTRA1,
         AFFILIATE_INTRA2,
         CHARTFIELD1,
         CHARTFIELD2,
         CHARTFIELD3,
         PROJECT_ID,
         STATISTICS_CODE,
         STATISTIC_AMOUNT,
         MOVEMENT_FLAG,
         DOC_SEQ_NBR,
         JRNL_LN_REF,
         IU_SYS_TRAN_CD,
         IU_TRAN_CD,
         IU_ANCHOR_FLG,
         PROCESS_INSTANCE)
      VALUES
        ('CONEC',
         lv_transid,
         1,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'yyyy'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         1,
         '1102010150',
         'GYE_999C',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumgye,
         ln_sumgye,
         'CPBSCS',
         pd_fecha,
         lv_referencia,
         'N',
         j.id_ciclo,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      /*            5359 JBA.- asignar tipo de IVA y ciclo al preasiento generado*/
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN C_VERIFICA_CUENTAS('1102010150');
        FETCH C_VERIFICA_CUENTAS
          INTO LC_VERIF_CTAS;
        LB_FOUND_CTA := C_VERIFICA_CUENTAS%FOUND;
        CLOSE C_VERIFICA_CUENTAS;
      
        IF LB_FOUND_CTA THEN
        
          LV_CICLO_5359 := ' ';
          LV_TIPO_IVA   := ' ';
        
          IF LC_VERIF_CTAS.USA_CICLO = 'Y' THEN
            LV_CICLO_5359 := 'PROV';
          END IF;
          IF LC_VERIF_CTAS.USA_TIPO_IVA = 'Y' THEN
            LV_TIPO_IVA := 'PROV';
          END IF;
        
          INSERT INTO ps_pr_jgen_acct_en_prov
            (BUSINESS_UNIT,
             TRANSACTION_ID,
             TRANSACTION_LINE,
             LEDGER_GROUP,
             LEDGER,
             PR_JRNL_LN_REF2,
             PR_JRNL_TIPO_IVA,
             PR_JRNL_CICLO)
          VALUES
            ('CONEC',
             lv_transid,
             1,
             'REAL',
             'LOCAL',
             ' ',
             LV_TIPO_IVA,
             LV_CICLO_5359);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          pv_error := 'Error 2, al insertar el preasiento en la tabla ps_pr_jgen_acct_en_prov ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 pv_error,
                                 pn_id_transaccion);
          ROLLBACK;
      END;
      /*5359 - FIN*/
    
      FIN_PROVISION.p_descripcion_cta('1102010205', lc_ctacontab);
      lv_referencia := lc_ctacontab || ' Provisi�n mes ' ||
                       to_char(pd_fecha,
                               'Month',
                               'NLS_DATE_LANGUAGE=spanish') || ' del ' ||
                       to_char(pd_fecha, 'yyyy') || ' ' || lv_ciclo;
    
      INSERT INTO ps_jgen_acct_entry_prov
        (BUSINESS_UNIT,
         TRANSACTION_ID,
         TRANSACTION_LINE,
         LEDGER_GROUP,
         LEDGER,
         ACCOUNTING_DT,
         APPL_JRNL_ID,
         BUSINESS_UNIT_GL,
         FISCAL_YEAR,
         ACCOUNTING_PERIOD,
         JOURNAL_ID,
         JOURNAL_DATE,
         JOURNAL_LINE,
         ACCOUNT,
         OPERATING_UNIT,
         PRODUCT,
         CURRENCY_CD,
         FOREIGN_CURRENCY,
         RT_TYPE,
         RATE_MULT,
         RATE_DIV,
         MONETARY_AMOUNT,
         FOREIGN_AMOUNT,
         DOC_TYPE,
         DOC_SEQ_DATE,
         LINE_DESCR,
         GL_DISTRIB_STATUS,
         ALTACCT,
         DEPTID,
         FUND_CODE,
         CLASS_FLD,
         PROGRAM_CODE,
         BUDGET_REF,
         AFFILIATE,
         AFFILIATE_INTRA1,
         AFFILIATE_INTRA2,
         CHARTFIELD1,
         CHARTFIELD2,
         CHARTFIELD3,
         PROJECT_ID,
         STATISTICS_CODE,
         STATISTIC_AMOUNT,
         MOVEMENT_FLAG,
         DOC_SEQ_NBR,
         JRNL_LN_REF,
         IU_SYS_TRAN_CD,
         IU_TRAN_CD,
         IU_ANCHOR_FLG,
         PROCESS_INSTANCE)
      VALUES
        ('CONEC',
         lv_transid,
         2,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'yyyy'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         2,
         '1102010205',
         'UIO_999C',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumuio,
         ln_sumuio,
         'CPBSCS',
         pd_fecha,
         lv_referencia,
         'N',
         j.id_ciclo,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      /*5359 JBA.- asignar tipo de IVA y ciclo al preasiento generado*/
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN C_VERIFICA_CUENTAS('1102010205');
        FETCH C_VERIFICA_CUENTAS
          INTO LC_VERIF_CTAS;
        LB_FOUND_CTA := C_VERIFICA_CUENTAS%FOUND;
        CLOSE C_VERIFICA_CUENTAS;
      
        IF LB_FOUND_CTA THEN
        
          LV_CICLO_5359 := ' ';
          LV_TIPO_IVA   := ' ';
        
          IF LC_VERIF_CTAS.USA_CICLO = 'Y' THEN
            LV_CICLO_5359 := 'PROV';
          END IF;
          IF LC_VERIF_CTAS.USA_TIPO_IVA = 'Y' THEN
            LV_TIPO_IVA := 'PROV';
          END IF;
        
          INSERT INTO ps_pr_jgen_acct_en_prov
            (BUSINESS_UNIT,
             TRANSACTION_ID,
             TRANSACTION_LINE,
             LEDGER_GROUP,
             LEDGER,
             PR_JRNL_LN_REF2,
             PR_JRNL_TIPO_IVA,
             PR_JRNL_CICLO)
          VALUES
            ('CONEC',
             lv_transid,
             2,
             'REAL',
             'LOCAL',
             ' ',
             LV_TIPO_IVA,
             LV_CICLO_5359);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          pv_error := 'Error 3, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PROV ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 pv_error,
                                 pn_id_transaccion);
          ROLLBACK;
      END;
      /*5359 - FIN*/
      COMMIT;
    END LOOP;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Fin de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV y PS_PR_JGEN_ACCT_EN_PROV ',
                           pn_id_transaccion);
  
    --CREA LOS DIFERENTES ASIENTOS CONTABLES SUPARIZADOS POR CUENTA CONTABLE PARA UIO Y GYE
    crear_sumarizado_prov_sap(pd_fecha, pv_msj_trama2);
  
    gsib_security.dotraceh(pv_aplicacion, pv_msj_trama2, pn_id_transaccion);
    pv_error := pv_msj_trama2;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           '**** FIN GENERA ASIENTOS PROVISION BSCS ' ||
                           'FECHA DE CORTE :' || pd_fecha ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error;
    WHEN OTHERS THEN
      pv_error := 'Error al insertar el preasiento' || pv_msj_trama2 ||
                  SQLERRM;
      gsib_security.dotraceh(pv_aplicacion, pv_error, pn_id_transaccion);
    
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             '**** FIN GENERA ASIENTOS PROVISION BSCS ' ||
                             'FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '**** ',
                             pn_id_transaccion);
      ROLLBACK;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE crear_sumarizado_prov_sap(pd_fecha      IN DATE,
                                      pv_msj_trama2 IN OUT VARCHAR2) IS
  
    -- INICIO 10189 RGT NEY MIRANDA T  
    LV_SENTENCIA                  VARCHAR2(50);
    ID_POLIZA_COSTA_PROV_CICLO_1  NUMBER := 0;
    ID_POLIZA_SIERRA_PROV_CICLO_1 NUMBER := 0;
    ID_POLIZA_COSTA_PROV_CICLO_2  NUMBER := 0;
    ID_POLIZA_SIERRA_PROV_CICLO_2 NUMBER := 0;
    ID_POLIZA_COSTA_PROV_CICLO_3  NUMBER := 0;
    ID_POLIZA_SIERRA_PROV_CICLO_3 NUMBER := 0;
    T_COSTA_PROV_CICLO_1          CLOB := NULL;
    MY_ERROR EXCEPTION;
    T_SIERRA_PROV_CICLO_1 CLOB := NULL;
    T_COSTA_PROV_CICLO_2  CLOB := NULL;
    T_SIERRA_PROV_CICLO_2 CLOB := NULL;
    T_COSTA_PROV_CICLO_3  CLOB := NULL;
    T_SIERRA_PROV_CICLO_3 CLOB := NULL;
    LV_IDENTIFICADOR      VARCHAR2(15);
    LN_MESFINAL           NUMBER := TO_NUMBER(TO_CHAR(pd_fecha, 'MM'));
    LN_ANOFINAL           NUMBER := TO_NUMBER(TO_CHAR(pd_fecha, 'YYYY'));
    lv_fechaconta         DATE := to_date(to_char(to_date(pd_fecha),
                                                  'YYYYMMDD'),
                                          'YYYYMMDD');
    lv_html               VARCHAR2(2500);
    ERROR_TRAMA           VARCHAR2(1000);
    LNANOFINAL            NUMBER;
    LV_MSJ_CREA_TABLA     VARCHAR2(2500);
    MSJ_TRAMAS            VARCHAR2(2500);
    pv_error              VARCHAR2(3000);
    LV_ENVIA_TRAMA        VARCHAR2(10);
    LV_APLICACION         VARCHAR2(100) := 'FIN_PROVISION.CREAR_SUMARIZADO_PROV_SAP';
    LE_ERROR_TRAMA EXCEPTION;
    LV_ERROR_PREVIA VARCHAR2(3000);
    LV_FECHA_ACTUAL VARCHAR(100) := to_char(SYSDATE,
                                            'dd/mm/yyyy hh24:mi:ss');
    -- 10189 ASIGNA LOS CENTROS DE BENEFICIO
    CURSOR ASIGNA_CEBES IS
      SELECT R.CTA_SAP, R.CEBE_UIO, R.CEBE_GYE FROM GSI_FIN_CEBES R;
  
    CURSOR LC_ENVIA_TRAMA IS
      SELECT VALOR
        FROM GSIB_PARAMETROS_SAP
       WHERE ID_TIPO_PARAMETRO = '10189'
         AND ID_PARAMETRO = 'ENVIA_TRAMA';
  
    -- CREA LA VISUALIZACION DE LAS POLIZAS GENERADA                     
    CURSOR LC_CREA_HTML(LC_FECHA_CORTE DATE) IS
    
      SELECT G.ID_POLIZA
        FROM GSIB_TRAMAS_SAP G
       WHERE G.FECHA_CORTE = LC_FECHA_CORTE
         AND TRUNC(G.FECHA) = TRUNC(SYSDATE);
  
  BEGIN
  
    OPEN LC_ENVIA_TRAMA;
    FETCH LC_ENVIA_TRAMA
      INTO LV_ENVIA_TRAMA;
    CLOSE LC_ENVIA_TRAMA;
  
    BEGIN
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Inicio del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE',
                             pn_id_transaccion);
    
      gsib_api_sap.reinicia_secuencia(pn_id_transaccion); -- VALIDO SI YA PASO UN ANIO Y REINICIO LA SEQUENCIA
    
      --TRUNCO LOS DATOS DE LA TABLA TEMPORAL PARA ALMACENAR LOS NUEVOS VALORES
      LV_SENTENCIA := 'truncate table GSIB_PROVISION_SUMARIZADO_SAP';
      EXEC_SQL(LV_SENTENCIA);
    
      GSIB_SECURITY.dotracem(LV_APLICACION,
                             'Se elimino registros de la tabla temporal GSIB_PROVISION_SUMARIZADO_SAP ',
                             pn_id_transaccion);
    
      --ASIGNO LOS ID_POLIZA SEGUN EL CICLO Y LA REGION
      ID_POLIZA_COSTA_PROV_CICLO_1  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_PROV_CICLO_1 := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_COSTA_PROV_CICLO_2  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_PROV_CICLO_2 := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_COSTA_PROV_CICLO_3  := ID_POLIZA_SAP.NEXTVAL;
      ID_POLIZA_SIERRA_PROV_CICLO_3 := ID_POLIZA_SAP.NEXTVAL;
      LNANOFINAL                    := to_char(SYSDATE, 'YYYY');
    
      --CREO LA TABLA GSIB_PROVISION_SUMARIZADO_SAP
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Inicio carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP ',
                             pn_id_transaccion);
      INSERT INTO GSIB_PROVISION_SUMARIZADO_SAP
        SELECT P.ALTACCT,
               P.ACCOUNT,
               SUM(P.MONETARY_AMOUNT) MONETARY_AMOUNT,
               SUM(P.FOREIGN_AMOUNT) FOREIGN_AMOUNT,
               'BSCS' || LNANOFINAL ||
               DECODE(P.ALTACCT,
                      '01',
                      DECODE(SUBSTR(P.OPERATING_UNIT, 0, 7),
                             'GYE_999',
                             LPAD(ID_POLIZA_COSTA_PROV_CICLO_1, 3, 0),
                             'UIO_999',
                             LPAD(ID_POLIZA_SIERRA_PROV_CICLO_1, 3, 0)),
                      '02',
                      DECODE(SUBSTR(P.OPERATING_UNIT, 0, 7),
                             'GYE_999',
                             LPAD(ID_POLIZA_COSTA_PROV_CICLO_2, 3, 0),
                             'UIO_999',
                             LPAD(ID_POLIZA_SIERRA_PROV_CICLO_2, 3, 0)),
                      '03',
                      DECODE(SUBSTR(P.OPERATING_UNIT, 0, 7),
                             'GYE_999',
                             LPAD(ID_POLIZA_COSTA_PROV_CICLO_3, 3, 0),
                             'UIO_999',
                             LPAD(ID_POLIZA_SIERRA_PROV_CICLO_3, 3, 0))) IDENTIFICADOR,
               'EC02' SOCIEDAD,
               LV_FECHACONTA FECHA_CONTABILIZACION,
               'PROVISION ' || DECODE(SUBSTR(P.OPERATING_UNIT, 0, 7),
                                      'GYE_999',
                                      'COSTA',
                                      'UIO_999',
                                      'SIERRA') REFERENCIA,
               LV_FECHACONTA FECHA_DOC,
               'J4' CLASE_DOC,
               'PROVISION CONSUMO' || DECODE(SUBSTR(P.OPERATING_UNIT, 0, 7),
                                             'GYE_999',
                                             ' R2',
                                             'UIO_999',
                                             ' R1') TEXTO_CABECERA,
               LN_ANOFINAL ANIO_FINAL,
               LN_MESFINAL MES_FINAL,
               'USD' MONEDA,
               NULL IVA,
               NULL CLAVE_CONTA,
               NULL CENTRO_BENEFICIO,
               'PROV. INGRESO BSCS CICLO ' || P.ALTACCT || ' ' ||
               TRIM(TO_CHAR(pd_fecha, 'MONTH')) || ' DEL ' || LN_ANOFINAL TEXTO
        
          FROM ps_jgen_acct_entry_prov P
        
         GROUP BY P.ALTACCT, P.ACCOUNT, P.OPERATING_UNIT;
    
      --ASIGNA LOS CENTROS DE BENEFICIO
      FOR CE IN ASIGNA_CEBES LOOP
        UPDATE GSIB_PROVISION_SUMARIZADO_SAP F
           SET F.CENTRO_BENEFICIO = DECODE(SUBSTR(F.TEXTO_CABECERA,
                                                  LENGTH(F.TEXTO_CABECERA) - 1,
                                                  3),
                                           'R2',
                                           CE.CEBE_GYE,
                                           'R1',
                                           CE.CEBE_UIO) --ASIGNA CEBE
         WHERE F.ACCOUNT = CE.CTA_SAP;
      
      END LOOP;
    
      --ASIGNA LAS CLAVES DE CONTABILIZACION
      UPDATE GSIB_PROVISION_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 40
       WHERE F.MONETARY_AMOUNT > 0;
    
      UPDATE GSIB_PROVISION_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 50
       WHERE F.MONETARY_AMOUNT < 0;
    
      COMMIT;
    
      LV_MSJ_CREA_TABLA := 'Se creo correctamente la tabla GSIB_PROVISION_SUMARIZADO_SAP';
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Fin carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP ',
                             pn_id_transaccion);
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        LV_MSJ_CREA_TABLA := 'ERROR. Al crear la tabla GSIB_PROVISION_SUMARIZADO_SAP => ' ||
                             SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               lv_msj_crea_tabla,
                               pn_id_transaccion);
      
    END;
  
    BEGIN
      ---- CREO Y ENVIO TODAS TRAMAS 
      /* ===================================
      --CREO lA TRAMA DE LA COSTA CICLO 1
       =================================== */
      T_COSTA_PROV_CICLO_1 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          PV_CICLO         => '01',
                                          PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                          ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS := 'Error al crear la trama Provision COSTA CICLO 01 => ' ||
                      ERROR_TRAMA;
        RAISE LE_ERROR_TRAMA;
      
        ERROR_TRAMA := NULL;
      ELSE
      
        -- LA ALMACENEO EN LA TABLA  
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_COSTA_PROV_CICLO_1,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 01',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_COSTA_PROV_CICLO_1,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision COSTA CICLO 01 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      
      END IF;
    
      /*===================================
      --CREO A TRAMA DE LA SIERRA CICLO 1
       =================================== */
      T_SIERRA_PROV_CICLO_1 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           PV_CICLO         => '01',
                                           PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                           ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS  := MSJ_TRAMAS ||
                       ' Error al crear la trama Provision SIERRA CICLO 01=> ' ||
                       ERROR_TRAMA;
        ERROR_TRAMA := NULL;
        RAISE LE_ERROR_TRAMA;
      
      ELSE
      
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_SIERRA_PROV_CICLO_1,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 01',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
      
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_SIERRA_PROV_CICLO_1,
                                                 pv_error);
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision SIERRA CICLO 01 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      END IF;
      -- LA ALMACENE EN LA TABLA TEMPORAL  
    
      /*  ===================================
      --CREO A TRAMA DE LA COSTA CICLO 2
       =================================== */
      T_COSTA_PROV_CICLO_2 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          PV_CICLO         => '02',
                                          PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                          ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS  := MSJ_TRAMAS ||
                       ' Error al crear la trama Provision COSTA CICLO 02=> ' ||
                       ERROR_TRAMA;
        ERROR_TRAMA := NULL;
        RAISE LE_ERROR_TRAMA;
      
      ELSE
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_COSTA_PROV_CICLO_2,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 02',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_COSTA_PROV_CICLO_2,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision COSTA CICLO 02 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      
      END IF;
      /*     ===================================
      --CREO A TRAMA DE LA SIERRA CICLO 2
       =================================== */
      T_SIERRA_PROV_CICLO_2 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           PV_CICLO         => '02',
                                           PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                           ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS := MSJ_TRAMAS ||
                      ' Error al crear la trama Provision SIERRA CICLO 02=> ' ||
                      ERROR_TRAMA;
      
        ERROR_TRAMA := NULL;
        RAISE LE_ERROR_TRAMA;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_SIERRA_PROV_CICLO_2,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 02',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_SIERRA_PROV_CICLO_2,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision SIERRA CICLO 02 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      END IF;
      /*       ===================================
      --CREO A TRAMA DE LA COSTA CICLO 3
       =================================== */
      T_COSTA_PROV_CICLO_3 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          PV_CICLO         => '03',
                                          PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                          ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS  := MSJ_TRAMAS ||
                       ' Error al crear la trama Provision COSTA CICLO 03=> ' ||
                       ERROR_TRAMA;
        ERROR_TRAMA := NULL;
        RAISE LE_ERROR_TRAMA;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_COSTA_PROV_CICLO_3,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 03',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_COSTA_PROV_CICLO_3,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision COSTA CICLO 03 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        
        END IF;
      END IF;
    
      /*    
        ===================================
      --CREO LA TRAMA DE LA SIERRA CICLO 3
       =================================== */
      T_SIERRA_PROV_CICLO_3 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           PV_CICLO         => '03',
                                           PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                           ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS := MSJ_TRAMAS ||
                      ' Error al crear la trama Provision SIERRA CICLO 03=> ' ||
                      ERROR_TRAMA;
      
        RAISE LE_ERROR_TRAMA;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_SIERRA_PROV_CICLO_3,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 03',
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        GSIB_ENVIA_TRAMAS_SAP.CONSUME_WEBSERVICE(T_SIERRA_PROV_CICLO_3,
                                                 pv_error);
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP provision COSTA CICLO 03 ;' ||
                        pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
        
          COMMIT;
        END IF;
      END IF;
    
      --Generos los reportes en html
      genera_reporte_en_html(pd_fecha, lv_html);
      IF LV_ENVIA_TRAMA = 'S' THEN
        msj_tramas := lv_html || ' Se Crearon las tramas => ' || msj_tramas;
      ELSE
        msj_tramas := lv_html ||
                      ' Se Crearon las tramas , pero el envio a SAP no esta habilitado =>' ||
                      MSJ_TRAMAS;
      END IF;
    
      gsib_security.dotraceh(lv_aplicacion, msj_tramas, pn_id_transaccion);
    
    EXCEPTION
    
      WHEN LE_ERROR_TRAMA THEN
      
        MSJ_TRAMAS := 'ERROR al crear las tramas ' || MSJ_TRAMAS || SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      WHEN OTHERS THEN
      
        MSJ_TRAMAS := 'ERROR al crear las tramas ' || SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
    END;
  
    FOR D IN LC_CREA_HTML(pd_fecha) LOOP
      GSIB_API_SAP.GENERA_HTML(D.ID_POLIZA, LV_ERROR_PREVIA);
    END LOOP;
  
    PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                     LV_ERROR_PREVIA;
    gsib_security.dotraceh(lv_aplicacion, pv_msj_trama2, pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Fin del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN OTHERS THEN
    
      PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                       LV_ERROR_PREVIA;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_msj_trama2,
                             pn_id_transaccion);
  END;

  /************************************************/
  /*CLS RHE [3470] Carga servicio de cob_servicios*/
  -- [3470] Provision de Proyeccion Postpago      --   
  /*FECHA: 20/06/2008                             */
  /************************************************/
  PROCEDURE p_carga_servicios(pv_error OUT VARCHAR2) IS
  
    CURSOR C_servicios IS --4376 - mayo 2009 - Cambio en el cursor para barrerse la mpusntab y ya no la cob_servicios
      SELECT DISTINCT cb.shdes, TRIM(upper(des)) SERVICIO
        FROM mpusntab cb
       WHERE NOT EXISTS (SELECT cp.codigo_sh, cp.servicio
                FROM calculo_prorrateo cp
               WHERE cb.shdes = cp.codigo_sh);
  
    lv_procedimiento VARCHAR2(100);
  
  BEGIN
    pv_error         := NULL;
    gv_aplicacion    := 'FIN_PROVISION.';
    lv_procedimiento := 'P_CARGA_SERVICIO -';
    FOR I IN C_SERVICIOS LOOP
      BEGIN
        --4376 - mayo 2009 - Cambio para llenar la tabla calculo_prorrateo con el codigo_sh y ya no con el sncode
        INSERT INTO calculo_prorrateo
          (servicio, codigo_sh)
        VALUES
          (i.servicio, i.shdes);
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR := 'Error al insertar en calculo_prorrateo ' ||
                      gv_aplicacion || lv_procedimiento || SQLERRM;
      END;
    END LOOP;
    COMMIT;
  END;

  /***************************************************/
  /* CLS MJO [4376] Actualiza cuentas contables      */
  /*         de los rubros que no tienen             */
  /* FECHA: 23/06/2009                               */
  /***************************************************/
  PROCEDURE p_actualiza_ctas(pv_error OUT VARCHAR2) IS
  
    i                NUMBER;
    lv_procedimiento VARCHAR2(100);
  
    --obtiene las cuentas contables de los servicios no configurados
    CURSOR c_cuentas IS
      SELECT a.codigo_sh, a.servicio, b.ctapsoft
        FROM calculo_prorrateo a, mpusntab c, cob_servicios b
       WHERE a.codigo_contable IS NULL
         AND a.codigo_sh = c.shdes
         AND c.sncode = b.servicio;
  
  BEGIN
    pv_error         := NULL;
    gv_aplicacion    := 'FIN_PROVISION.';
    lv_procedimiento := 'p_actualiza_ctas -';
  
    --actualiza las cuentas contables en los servicios
    FOR i IN c_cuentas LOOP
      BEGIN
        UPDATE calculo_prorrateo x
           SET x.codigo_contable = i.ctapsoft
         WHERE x.codigo_sh = i.codigo_sh
           AND x.codigo_contable IS NULL;
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR := 'Error al actualizar cuentas en calculo_prorrateo ' ||
                      gv_aplicacion || lv_procedimiento || SQLERRM;
      END;
    END LOOP;
    COMMIT;
  END;

  /******************************************************************************
   Proyecto   : [4376] - Ingresos Pre-facturados
   Nombre     : CLS MMA - CLS SRA
   Lider      : CLS MJO - SIS WBU
   Fecha      : 15/05/2009 
   Descripci�n: Genera la pre facturaci�n de todos los rubros para todos los ciclos.
                Llama la carga del preasiento en Peoplesoft
  *******************************************************************************/
  PROCEDURE p_genera_prorateo(pv_error OUT VARCHAR2) IS
  
    CURSOR C_reportes(cv_ciclo VARCHAR2) IS
      SELECT /*+ rule +*/
       decode(b.costcenter_id, 1, 'GYE', 2, 'UIO') Ciudad,
       '',
       decode(b.prgcode,
              '1',
              'AUTOCONTROL',
              '2',
              'AUTOCONTROL',
              '3',
              'TARIFARIO',
              '4',
              'TARIFARIO',
              '5',
              'BULK',
              '6',
              'BULK',
              '9',
              'PYMES') Producto,
       a.campo_2 Servicio,
       SUM(to_number(a.campo_3) / 100) Valor
        FROM facturas_cargadas_tmp_fin a, customer_all b
       WHERE b.custcode = a.cuenta
         AND (upper(a.campo_2) NOT LIKE 'LLAMADA%')
         AND a.ciclo IN (SELECT id_ciclo_admin
                           FROM fa_ciclos_axis_bscs
                          WHERE id_ciclo = cv_ciclo)
       GROUP BY b.costcenter_id,
                decode(b.prgcode,
                       '1',
                       'AUTOCONTROL',
                       '2',
                       'AUTOCONTROL',
                       '3',
                       'TARIFARIO',
                       '4',
                       'TARIFARIO',
                       '5',
                       'BULK',
                       '6',
                       'BULK',
                       '9',
                       'PYMES'),
                campo_2
      UNION
      SELECT /*+ rule +*/
       decode(b.costcenter_id, 1, 'GYE', 2, 'UIO') Ciudad,
       p.codigo_contable,
       decode(b.prgcode,
              '1',
              'AUTOCONTROL',
              '2',
              'AUTOCONTROL',
              '3',
              'TARIFARIO',
              '4',
              'TARIFARIO',
              '5',
              'BULK',
              '6',
              'BULK',
              '9',
              'PYMES') Producto,
       p.nombre Servicio,
       SUM(to_number(a.campo_3) / 100) * (p.porcentaje / 100) Valor
        FROM facturas_cargadas_tmp_fin a,
             customer_all              b,
             prov_porc_llamada         p
       WHERE b.custcode = a.cuenta
         AND (upper(a.campo_2) LIKE 'LLAMADA%')
         AND a.ciclo IN (SELECT id_ciclo_admin
                           FROM fa_ciclos_axis_bscs
                          WHERE id_ciclo = cv_ciclo)
         AND p.id_ciclo = cv_ciclo
       GROUP BY b.costcenter_id,
                decode(b.prgcode,
                       '1',
                       'AUTOCONTROL',
                       '2',
                       'AUTOCONTROL',
                       '3',
                       'TARIFARIO',
                       '4',
                       'TARIFARIO',
                       '5',
                       'BULK',
                       '6',
                       'BULK',
                       '9',
                       'PYMES'),
                p.nombre,
                p.codigo_contable,
                p.porcentaje;
  
    CURSOR c_valida_ciclo IS
      SELECT DISTINCT id_ciclo
        FROM fa_ciclos_axis_bscs
       WHERE id_ciclo_admin IN
             (SELECT DISTINCT a.ciclo FROM facturas_cargadas_tmp_fin a);
  
    lv_valor_ciclo VARCHAR2(2);
    lv_sentencia   VARCHAR2(200);
    lv_error       VARCHAR2(200);
    ln_commit      NUMBER;
    Le_Error EXCEPTION;
    lv_cta_contable VARCHAR2(30);
    LV_APLICACION   VARCHAR2(100) := 'FIN_PROVISION.P_GENERA_PRORATEO';
  BEGIN
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** INICIO GENERA PRORRATEO PROVISION BSCS ' ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Inicio del procedimiento que genera la tabla reporte_calc_prorrateo.',
                           pn_id_transaccion);
  
    p_carga_servicios(pv_error => lv_error);
  
    IF lv_error IS NOT NULL THEN
      RAISE Le_Error;
    END IF;
  
    OPEN c_valida_ciclo;
    FETCH c_valida_ciclo
      INTO lv_valor_ciclo;
    CLOSE c_valida_ciclo;
  
    IF lv_valor_ciclo IS NOT NULL THEN
    
      lv_sentencia := 'truncate table reporte_calc_prorrateo';
      BEGIN
        EXECUTE IMMEDIATE lv_sentencia;
      
        GSIB_SECURITY.dotracem(LV_APLICACION,
                               'Se elimino los registros de la tabla temporal reporte_calc_prorrateo ',
                               pn_id_transaccion);
      
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := substr(SQLERRM, 1, 200);
          GSIB_SECURITY.dotracem(LV_APLICACION,
                                 'Error al tratar de eliminar registros la tabla temporal reporte_calc_prorrateo',
                                 pn_id_transaccion);
          RAISE Le_Error;
      END;
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Inicio de carga masiva de datos hacia la tabla reporte_calc_prorrateo ',
                             pn_id_transaccion);
    
      FOR j IN c_valida_ciclo LOOP
        ln_commit := 0;
        FOR i IN c_reportes(j.id_ciclo) LOOP
          ln_commit := ln_commit + 1;
          BEGIN
            SELECT DISTINCT a.codigo_contable
              INTO lv_cta_contable
              FROM calculo_prorrateo a
             WHERE a.servicio = upper(TRIM(i.servicio));
          EXCEPTION
            WHEN too_many_rows THEN
              lv_cta_contable := NULL;
            WHEN no_data_found THEN
              BEGIN
                SELECT t.codigo_contable
                  INTO lv_cta_contable
                  FROM prov_porc_llamada t
                 WHERE t.nombre = upper(TRIM(i.servicio))
                   AND t.id_ciclo = j.id_ciclo;
              EXCEPTION
                WHEN no_data_found THEN
                  lv_cta_contable := NULL;
              END;
          END;
        
          INSERT INTO reporte_calc_prorrateo
            (CIUDAD,
             CODIGO_CONTABLE,
             PRODUCTO,
             SERVICIO,
             VALOR,
             PRORRATEO,
             VALOR_PROVISIONA,
             PRODUCTO_ASI,
             CODIGO_SH,
             ID_CICLO)
          VALUES
            (i.ciudad,
             lv_cta_contable,
             i.producto,
             upper(TRIM(i.servicio)),
             i.valor,
             0,
             i.valor,
             '',
             '',
             j.id_ciclo);
          IF MOD(ln_commit, 500) = 0 THEN
            COMMIT;
          END IF;
        END LOOP;
        COMMIT;
      END LOOP;
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Fin de carga masiva de datos hacia la tabla reporte_calc_prorrateo ',
                             pn_id_transaccion);
    
    END IF;
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Fin del procedimiento que genera la tabla reporte_calc_prorrateo.',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** FIN GENERA PRORRATEO PROVISION BSCS ' ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN Le_Error THEN
      ROLLBACK;
      PV_ERROR := 'Error al generar el reporte_calc_prorrateo ' ||
                  'P_GENERA_PRORATEO' || lv_error;
    
      gsib_security.dotraceh(lv_aplicacion, pv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '**** FIN GENERA PRORRATEO PROVISION BSCS ' ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '**** ',
                             pn_id_transaccion);
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'Error al generar el reporte_calc_prorrateo ' ||
                  'P_GENERA_PRORATEO' || SQLERRM;
      gsib_security.dotraceh(lv_aplicacion, pv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '**** FIN GENERA PRORRATEO PROVISION BSCS ' ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '**** ',
                             pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  PROCEDURE replica_financiero(pd_fecha IN DATE, pv_error OUT VARCHAR2) IS
  
    lv_proceso VARCHAR2(60) := 'REPLICA_FINANCIERO';
    lv_error   VARCHAR2(1000) := NULL;
    le_error EXCEPTION;
  BEGIN
  
    GSIB_SECURITY.dotraceh(lv_proceso,
                           'Se ejecuto el procedimiento hace la replica hacia financiero',
                           pn_id_transaccion);
  
    IF pd_fecha IS NULL THEN
      lv_error := 'El parametro ingresado no es valido - ';
      gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
    
      RAISE le_error;
    END IF;
    BEGIN
      INSERT INTO ps_jgen_acct_entry_finsys
        SELECT *
          FROM ps_jgen_acct_entry_prov
         WHERE journal_date = pd_fecha;
    
      GSIB_SECURITY.dotraceh(lv_proceso,
                             'Se realizo un insert masivo a la tabla ps_jgen_acct_entry@finsys ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN dup_val_on_index THEN
        lv_error := 'Existen registros duplicados a cargar a la tabla PS_JGEN_ACCT_ENTRY - ';
        gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
      
        RAISE le_error;
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
    BEGIN
      INSERT INTO ps_pr_jgen_acct_en_finsys
        SELECT *
          FROM ps_pr_jgen_acct_en_prov
         WHERE pr_fec_prov_interc = pd_fecha;
    
      GSIB_SECURITY.dotraceh(lv_proceso,
                             'Se realizo un insert masivo a la tabla SYSADM.PS_PR_JGEN_ACCT_EN@FINSYS ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN dup_val_on_index THEN
        lv_error := 'Existen registros duplicados a cargar a la tabla PS_PR_JGEN_ACCT_EN - ';
        gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
      
        RAISE le_error;
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
      
        RAISE le_error;
    END;
    COMMIT;
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error || lv_proceso;
      gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
    
      ROLLBACK;
    WHEN OTHERS THEN
      pv_error := SQLERRM || ' - ' || lv_proceso;
      gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
    
      ROLLBACK;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION crear_trama(PV_PRODUCTO      IN VARCHAR2,
                       PV_REGION        IN VARCHAR2,
                       PV_CICLO         IN VARCHAR2,
                       PV_IDENTIFICADOR OUT VARCHAR2,
                       ERROR            OUT VARCHAR2) RETURN CLOB IS
  
    -- LN_MONTO      NUMBER := 0;
    PV_APLICACION VARCHAR2(100) := 'FIN_PROVISION.CREAR_TRAMA';
    LV_IVA        VARCHAR2(10);
    ACU           NUMBER := 0;
    --LV_MONTO DECIMAL;
    LC_CABECERA    CLOB;
    LC_CUERPO_TEMP CLOB;
    LC_CUERPO_FIN  CLOB;
    RESULTADO      VARCHAR2(100);
    LV_NODO_RAIZ   VARCHAR2(100);
    LV_SOCIEDAD    VARCHAR2(10);
    MY_ERROR EXCEPTION;
    CENTRO_BENEFICIO VARCHAR2(25);
    --OBTENGO TODOS LOS REGISTROS DEACUERDO AL PRODUCTO Y A LA REGION
    CURSOR C_PROD_REG IS
      SELECT *
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
       ORDER BY G.CLAVE_CONTA; --primero las cuentas contraprtidas (40)
  
    --OBTENGO EL IDENTIFICADOR UNICO POR CADA PRODUCTO Y  REGION
    CURSOR C_IDENTIFICADOR IS
      SELECT DISTINCT (G.IDENTIFICADOR)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_IDENTIFICADOR GSIB_PROVISION_SUMARIZADO_SAP.IDENTIFICADOR%TYPE;
  
    --OBTENGO LA FECHA DEL REGISTRO CONTABLE
    CURSOR C_REGISTRO_CONTABLE IS
      SELECT DISTINCT (G.FECHA_CONTABILIZACION)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_REGISTRO_CONTABLE GSIB_PROVISION_SUMARIZADO_SAP.FECHA_CONTABILIZACION%TYPE;
  
    --OBTENGO EL TIPO DE DOCUEMENTO
    CURSOR C_CLASE_DOC IS
      SELECT DISTINCT (G.CLASE_DOC)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_CLASE_DOC GSIB_PROVISION_SUMARIZADO_SAP.CLASE_DOC%TYPE;
  
    --OBTENGO LA REFERENCIA
    CURSOR C_REFERENCIA IS
      SELECT DISTINCT (G.REFERENCIA)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_REFERENCIA GSIB_PROVISION_SUMARIZADO_SAP.REFERENCIA%TYPE;
  
    --OBTENGO TEXTO CABECERA
    CURSOR C_TXT_CABECERA IS
      SELECT DISTINCT (G.TEXTO_CABECERA)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_TXT_CABECERA GSIB_PROVISION_SUMARIZADO_SAP.TEXTO_CABECERA%TYPE;
  
    --OBTENGO ANIO FISCAL
    CURSOR C_ANIO_FISCAL IS
      SELECT DISTINCT (G.ANIO_FINAL)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_ANIO_FISCAL GSIB_PROVISION_SUMARIZADO_SAP.ANIO_FINAL%TYPE;
  
    --OBTENGOEL MES FISCAL
    CURSOR C_MES_FISCAL IS
      SELECT DISTINCT (G.MES_FINAL)
        FROM GSIB_PROVISION_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND G.CICLO = PV_CICLO
         AND ROWNUM = 1;
    LV_MES_FISCAL GSIB_PROVISION_SUMARIZADO_SAP.MES_FINAL%TYPE;
  
  BEGIN
  
    IF PV_PRODUCTO IS NULL OR PV_PRODUCTO <> 'PROVISION' THEN
      RESULTADO := 'El parametro PV_PRODUCTO no es correcto. Debe ingresar PROVISION => ';
      RAISE MY_ERROR;
    END IF;
  
    IF PV_REGION IS NULL OR PV_REGION <> 'COSTA' AND PV_REGION <> 'SIERRA' THEN
      RESULTADO := 'El parametro PV_REGION no es correcto. Debe ingresar COSTA,SIERRA => ';
      RAISE MY_ERROR;
    END IF;
  
    IF PV_CICLO IS NULL THEN
      RESULTADO := 'El parametro PV_CICLO no es correcto. Debe ingresar el ciclo correspondiente ''01'',''02'',''03'' => ';
      RAISE MY_ERROR;
    END IF;
  
    LC_CABECERA := FIN_PROVISION.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                             'CABECERA');
    IF LC_CABECERA IS NULL THEN
      RESULTADO := 'PARAMETRO 10189 - CABECERA NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LV_NODO_RAIZ := FIN_PROVISION.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                              'NODO_RAIZ');
    IF LV_NODO_RAIZ IS NULL THEN
      RESULTADO := 'PARAMETRO 10189 - NODO_RAIZ NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LC_CABECERA := REPLACE(LC_CABECERA, '<%N_RAIZ%>', LV_NODO_RAIZ);
  
    --
    OPEN C_IDENTIFICADOR;
    FETCH C_IDENTIFICADOR
      INTO LV_IDENTIFICADOR;
    CLOSE C_IDENTIFICADOR;
  
    IF LV_IDENTIFICADOR IS NULL THEN
      RESULTADO := 'IDENTIFICADOR PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'DEL CICLO ' ||
                   PV_CICLO || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
  
    PV_IDENTIFICADOR := LV_IDENTIFICADOR;
    LC_CABECERA      := REPLACE(LC_CABECERA,
                                '<%ID_POLIZA%>',
                                REPLACE(LV_IDENTIFICADOR, ' ', ''));
  
    --
    LV_SOCIEDAD := FIN_PROVISION.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                             'SOCIEDAD');
    IF LV_SOCIEDAD IS NULL THEN
      RESULTADO := 'PARAMETRO 10189 - SOCIEDAD NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%SOCIEDAD%>', LV_SOCIEDAD);
  
    --
    OPEN C_REGISTRO_CONTABLE;
    FETCH C_REGISTRO_CONTABLE
      INTO LV_REGISTRO_CONTABLE;
    CLOSE C_REGISTRO_CONTABLE;
  
    IF LV_REGISTRO_CONTABLE IS NULL THEN
      RESULTADO := 'REGISTRO CONTABLE PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%FECHA%>',
                           TO_CHAR(LV_REGISTRO_CONTABLE, 'YYYYMMDD'));
    --TO_CHAR(ADD_MONTHS( LV_REGISTRO_CONTABLE,+1),'YYYYMMDD')); --SACAR EL ADD_MONTHS SOLO POR PRUBAS
  
    --
  
    OPEN C_CLASE_DOC;
    FETCH C_CLASE_DOC
      INTO LV_CLASE_DOC;
    CLOSE C_CLASE_DOC;
  
    IF LV_CLASE_DOC IS NULL THEN
      RESULTADO := 'LA CLASE DE DOCUMENTO PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%DOCUMENTO%>', LV_CLASE_DOC);
  
    --
  
    OPEN C_REFERENCIA;
    FETCH C_REFERENCIA
      INTO LV_REFERENCIA;
    CLOSE C_REFERENCIA;
  
    IF LV_REFERENCIA IS NULL THEN
      RESULTADO := 'LA REFERENCIA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%REFERENCIA_CIUDAD%>',
                           LV_REFERENCIA);
  
    --
  
    OPEN C_TXT_CABECERA;
    FETCH C_TXT_CABECERA
      INTO LV_TXT_CABECERA;
    CLOSE C_TXT_CABECERA;
  
    IF LV_TXT_CABECERA IS NULL THEN
      RESULTADO := 'EL TEXTO CABECERA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%CICLO%>', LV_TXT_CABECERA);
  
    --
  
    OPEN C_ANIO_FISCAL;
    FETCH C_ANIO_FISCAL
      INTO LV_ANIO_FISCAL;
    CLOSE C_ANIO_FISCAL;
  
    IF LV_ANIO_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%ANIO%>', LV_ANIO_FISCAL);
  
    --
  
    OPEN C_MES_FISCAL;
    FETCH C_MES_FISCAL
      INTO LV_MES_FISCAL;
    CLOSE C_MES_FISCAL;
  
    IF LV_MES_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%MES%>', LV_MES_FISCAL); --+1POR PRUEBAS
  
    -----
  
    FOR REC IN C_PROD_REG LOOP
      ACU            := ACU + 1;
      LC_CUERPO_TEMP := OBTENER_VALOR_PARAMETRO_SAP(10189, 'CUERPO');
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%POSICION%>', ACU);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CLAVE_CONTA%>',
                                REC.CLAVE_CONTA);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%FECHA%>',
                                TO_CHAR(REC.FECHA_CONTABILIZACION,
                                        'YYYYMMDD'));
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CUENTABSCS%>',
                                REC.ACCOUNT);
      IF REC.CENTRO_BENEFICIO IS NULL THEN
        CENTRO_BENEFICIO := '^';
      ELSE
        CENTRO_BENEFICIO := REC.CENTRO_BENEFICIO;
      END IF;
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CENTRO_BENEFICIO%>',
                                CENTRO_BENEFICIO);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%MONTO%>',
                                GSIB_API_SAP.ABS_NUMERO(REC.MONETARY_AMOUNT));
    
      IF REC.IVA = 'V2' THEN
        LV_IVA := REC.IVA;
      ELSIF REC.IVA = 'V1' THEN
        LV_IVA := REC.IVA;
      ELSE
        LV_IVA := '^';
      END IF;
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%IMPUESTO%>', LV_IVA);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%VALOR_IMPUESTO%>', '^'); -- EN PORVISION NO TIENE
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%ASIGNACION%>',
                                'PROVISION');
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%TEXTO%>', REC.TEXTO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<CICLO_FACTURACION>',
                                REC.CICLO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%MONEDA%>', REC.MONEDA);
    
      LC_CUERPO_FIN := LC_CUERPO_FIN || LC_CUERPO_TEMP;
    END LOOP;
  
    RETURN(LC_CABECERA || LC_CUERPO_FIN || ']');
  
  EXCEPTION
  
    WHEN MY_ERROR THEN
      ERROR := 'ERROR ' || RESULTADO || ' ' || PV_APLICACION;
    
      RETURN NULL;
    WHEN OTHERS THEN
      ERROR := 'ERROR ' || SQLERRM || ' ' || PV_APLICACION;
    
      RETURN NULL;
    
  END CREAR_TRAMA;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================

  FUNCTION obtener_valor_parametro_sap(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER,
                       CV_ID_PARAMETRO      VARCHAR2) IS
      SELECT VALOR
        FROM GSIB_PARAMETROS_SAP
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END OBTENER_VALOR_PARAMETRO_SAP;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE envia_tramas_provision_sap(PD_FECHA     IN DATE, --FECHA DE CORTE
                                       PV_RESULTADO OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(200) := 'FIN_PROVISION.ENVIA_TRAMAS_PROVISION_SAP';
  
  BEGIN
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** INICIO ENVIA TRAMAS PROVISION BSCS FECHA DE CORTE :' ||
                           PD_FECHA || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
    GSIB_API_SAP.envia_tramas(PD_FECHA,
                              'P', --P por provision 
                              'VOZ',
                              '',
                              pn_id_transaccion,
                              PV_RESULTADO);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** FIN ENVIA TRAMAS PROVISION BSCS FECHA DE CORTE :' ||
                           PD_FECHA || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reenvia_polizas(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2) IS
    LV_APLICACION VARCHAR2(200) := 'FIN_PROVISION.REENVIA_POLIZAS';
  
  BEGIN
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** INICIO RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  
    gsib_api_sap.reenvio_tramas(pv_id_polizas,
                                'PV',
                                pn_id_transaccion,
                                pv_resultado);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '**** FIN RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 26/06/2015 08:40:58
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE anula_polizas(pd_fecha     IN DATE, --FECHA DE CORTE
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2) IS
    lv_aplicacion VARCHAR2(200) := 'FIN_PROVISION.ANULA_POLIZAS';
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '**** INICIO ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
    gsib_api_sap.anula_polizas(pd_fecha,
                               'PROVISION',
                               'VOZ',
                               pn_id_transaccion,
                               pv_resultado,
                               pv_polizas);
    gsib_security.dotraceh(lv_aplicacion,
                           '**** FIN ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 26/06/2015 12:38:49
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2) IS
    --ciclos que tienen provision
    CURSOR c_ciclo IS
      SELECT DISTINCT ciclo,
                      decode(ciclo,
                             '01',
                             'Primer',
                             '02',
                             'Segundo',
                             '03',
                             'Tercer',
                             '04',
                             'Cuarto',
                             '05',
                             'Quinto',
                             '06',
                             'Sexto',
                             '07',
                             'S�ptimo',
                             '08',
                             'Octavo',
                             '09',
                             'Noveno',
                             '10',
                             'D�cimo',
                             'NUEVO CICLO A INGRESAR') des_ciclo
        FROM gsib_provision_sumarizado_sap;
    --ciudad GYE/UIO
    CURSOR c_referencia IS
      SELECT DISTINCT referencia
        FROM gsib_provision_sumarizado_sap
       ORDER BY referencia ASC;
    --datos para el reporte
    CURSOR c_datos(cv_referencia VARCHAR2, cv_ciclo VARCHAR2) IS
      SELECT s.texto,
             s.account,
             s.monetary_amount,
             s.identificador,
             s.fecha_contabilizacion,
             s.referencia,
             s.texto_cabecera,
             s.iva,
             s.clave_conta,
             s.centro_beneficio,
             s.ciclo
        FROM gsib_provision_sumarizado_sap s
       WHERE referencia = cv_referencia
         AND ciclo = cv_ciclo
       ORDER BY s.clave_conta ASC;
    CURSOR lc_id_reporte IS
      SELECT nvl(MAX(id_reporte), 0) + 1 FROM gsib_reportes_html;
    ln_id_reporte NUMBER := 0;
    lc_html           CLOB;
    lv_nombre_excel   VARCHAR2(1000);
    lv_ruta_escenario VARCHAR2(1000);
    lv_aplicacion     VARCHAR2(1000) := 'FIN_PROVISION.GENERA_REPORTE_EN_HTML';
  BEGIN
    lv_ruta_escenario := obtener_valor_parametro_sap(10189,
                                                     'RUTA_REPORTE_PROVISION');
    FOR c IN c_ciclo LOOP
      FOR r IN c_referencia LOOP
        lv_nombre_excel := lower(lv_ruta_escenario ||
                                 REPLACE(r.referencia, ' ', '_BSCS_') || '_' ||
                                 to_char(ld_fecha, 'ddmmyyyy') || '_' ||
                                 c.ciclo || '.xls');
        --cabecera
        lc_html := '<HTML>';
        lc_html := lc_html || '<HEAD>';
        lc_html := lc_html || '</HEAD>';
        lc_html := lc_html || '<BODY>';
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TABLE BORDER =1>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>ACCOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>MONETARY AMOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IDENTIFICADOR</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>FECHA CONTABILIZACION</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>REFERENCIA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO CABECERA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IVA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CLAVE CONTA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CENTRO BENEFICIO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CICLO</h4></STRONG></TD>';
        lc_html := lc_html || '</TR>';
        FOR d IN c_datos(r.referencia, c.ciclo) LOOP
          lc_html := lc_html || '<TR>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.account ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                     d.monetary_amount || '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.identificador ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.fecha_contabilizacion ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.referencia ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto_cabecera ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.iva ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.clave_conta ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.centro_beneficio ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.ciclo ||
                     '</STRONG></TD>';
          lc_html := lc_html || '</TR>';
        END LOOP;
        ln_id_reporte := NULL;
        lc_html := lc_html || '</table>';
        OPEN lc_id_reporte;
        FETCH lc_id_reporte
          INTO ln_id_reporte;
        CLOSE lc_id_reporte;
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD><STRONG>ID REPORTE : </STRONG></TD>';
        lc_html := lc_html || '<TD>' || ln_id_reporte || '</TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '</table></html>';
        INSERT INTO gsib_reportes_html
          (id_reporte,
           nombre_reporte,
           fecha_corte,
           fecha_generacion,
           reporte_html)
        VALUES
          (sc_id_reporte.nextval,
           lv_nombre_excel,
           ld_fecha,
           SYSDATE,
           lc_html);
        COMMIT;
      END LOOP;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error : ' || SQLERRM || lv_aplicacion;
  END;

END FIN_PROVISION;
/
