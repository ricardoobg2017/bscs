create or replace package GCK_TRX_REFACTURADOR_SVA IS

/*
   --=====================================================================================--
  -- Desarrollado por:  CLS Byron Rios Aviles
  -- Fecha de creaci�n: 26/10/2008
  -- Proyecto:          [3665] - Nuevo Esquema de Cobro de Datos
  -- Objetivo:          Paquete para el manejo del Nuevo Esquema de Cobro de Datos
  --=====================================================================================--
*/

PROCEDURE GCP_REFACTURA (PD_FECHA_INI_PERIODO IN DATE, 
                         PD_FECHA_FIN_PERIODO IN DATE,
                         PV_ERROR OUT VARCHAR2,
                         PV_BULK_SN VARCHAR2 DEFAULT NULL);
                         
PROCEDURE GCP_REFACTURA_NEW (PD_FECHA_INI_PERIODO IN DATE, 
                         PD_FECHA_FIN_PERIODO IN DATE,
                         PV_ERROR OUT VARCHAR2,
                         PV_BULK_SN VARCHAR2 DEFAULT NULL);
                         
FUNCTION GCF_GET_ESCALA (PV_PLAN  VARCHAR2, 
                         PN_VALOR NUMBER,
                         PV_ERROR OUT VARCHAR2
                         ) RETURN NUMBER;
  
FUNCTION GCF_GET_LIMITE_INFERIOR (PV_PLAN   VARCHAR2, 
                                  PN_ESCALA NUMBER,
                                  PV_ERROR  OUT VARCHAR2) RETURN NUMBER;
/*
PROCEDURE GCF_GET_TECHO_COSTO_KB (PV_PLAN          VARCHAR2, 
                                  PN_ESCALA        NUMBER,
                                  PN_TECHO_MB      OUT NUMBER,
                                  PN_COSTO_KB      OUT NUMBER,
                                  PN_TARIFA_BASICA OUT NUMBER,
                                  PV_ERROR         OUT VARCHAR2);*/
                                        
end GCK_TRX_REFACTURADOR_SVA;
/
create or replace package body GCK_TRX_REFACTURADOR_SVA is

   /*
   --=====================================================================================--
  -- Desarrollado por:  CLS Byron Rios Aviles
  -- Fecha de creaci�n: 26/10/2008
  -- Proyecto:          [3665] - Nuevo Esquema de Cobro de Datos
  -- Objetivo:          Paquete para el manejo del Nuevo Esquema de Cobro de Datos
  --=====================================================================================--
 */
 
   FUNCTION GCF_GET_LIMITE_superior (PV_PLAN   VARCHAR2, 
                                    PN_ESCALA NUMBER,
                                    PV_ERROR  OUT VARCHAR2) RETURN NUMBER IS
   
  --Cursor para obtener el limite inferior          
   CURSOR C_TECHO(CV_PLAN VARCHAR2, CN_ESCALA NUMBER) IS               
         SELECT TECHO_VALOR_max, tarifa_basica 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = PV_PLAN AND estado = 'A' 
            AND escala = CN_ESCALA;
   
   --Declaro variables
   LC_TECHO      C_TECHO%ROWTYPE;      
   LN_CONT       NUMBER:= 0;
   --LB_NOT_FOUND  BOOLEAN:=FALSE;
   
 BEGIN
    
    --Abro el cursor para saber el techo inferior
    OPEN  C_TECHO(PV_PLAN, PN_ESCALA);
    FETCH C_TECHO INTO LC_TECHO;
    CLOSE C_TECHO;
    
    LN_CONT := LC_TECHO.TECHO_VALOR_max;
 
    RETURN LN_CONT;
 
 EXCEPTION 
 
  WHEN OTHERS THEN
  
       PV_ERROR:= 'Error en GCF_OBTENER_LIMITE_INFERIOR: ' || SQLERRM;
       RETURN -1; 
       DBMS_OUTPUT.put_line(PV_ERROR);
     
 END GCF_GET_LIMITE_superior;
 
  FUNCTION GCF_GET_techo (PV_PLAN   VARCHAR2, 
                                    PN_ESCALA NUMBER,
                                    PV_ERROR  OUT VARCHAR2) RETURN NUMBER IS
   
  --Cursor para obtener el limite inferior          
   CURSOR C_TECHO(CV_PLAN VARCHAR2, CN_ESCALA NUMBER) IS               
         SELECT TECHO, tarifa_basica 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = PV_PLAN AND estado = 'A' 
            AND escala = CN_ESCALA;
   
   --Declaro variables
   LC_TECHO      C_TECHO%ROWTYPE;      
   LN_CONT       NUMBER:= 0;
   --LB_NOT_FOUND  BOOLEAN:=FALSE;
   
 BEGIN
    
    --Abro el cursor para saber el techo inferior
    OPEN  C_TECHO(PV_PLAN, PN_ESCALA);
    FETCH C_TECHO INTO LC_TECHO;
    CLOSE C_TECHO;
    
    LN_CONT := LC_TECHO.TECHO;
 
    RETURN LN_CONT;
 
 EXCEPTION 
 
  WHEN OTHERS THEN
  
       PV_ERROR:= 'Error en GCF_OBTENER_LIMITE_INFERIOR: ' || SQLERRM;
       DBMS_OUTPUT.put_line(PV_ERROR);
       RETURN -1; 
     
 END GCF_GET_techo;
 
 
 PROCEDURE GCP_REFACTURA (PD_FECHA_INI_PERIODO IN DATE, 
                          PD_FECHA_FIN_PERIODO IN DATE,
                          PV_ERROR OUT VARCHAR2,
                          PV_BULK_SN VARCHAR2 DEFAULT NULL)IS
           

  --Declaro Cursores                          
  CURSOR C_REG(c_co_id NUMBER) IS
  SELECT * FROM CL_SERVICIOS_GPRS_DIAS
  WHERE co_id=c_co_id;
  
  CURSOR C_FEES(c_co_id NUMBER) IS
  SELECT sum(m.AMOUNT) FROM fees m
  WHERE m.co_id=c_co_id;
  
  CURSOR C_val_config(c_planaxis varchar2) IS
   SELECT cupo_mb_tb, costo_kb_adicional 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = c_planaxis AND estado = 'A' 
            AND rownum = 1;
  
  --ciclo
  CURSOR C_CONTRATO_ciclo(c_co_id NUMBER) IS
  select c.id_ciclo,a.prgcode from customer_all a,contract_all b, fa_ciclos_axis_bscs c
  where b.customer_id = a.customer_id 
  and a.billcycle = c.id_ciclo_admin
  and b.co_id  = c_co_id;
  
/*  SELECT z.id_contrato 
    FROM customer_all y,contract_all x, cl_contratos@axis z
   WHERE x.customer_id = y.customer_id 
     AND x.co_id = c_co_id 
     AND y.custcode = z.codigo_doc;*/
  
/*  CURSOR C_CONTRATO_CICLO(c_idcontrato NUMBER) IS
  SELECT id_ciclo 
    FROM fa_contratos_ciclos_bscs@axis
   WHERE id_contrato = c_idcontrato;*/
  
  --Declaro Variables       
  LC_REG                    C_REG%ROWTYPE;
  LC_FEES                   C_FEES%ROWTYPE;
  LC_val_config             C_val_config%rowtype;
--  LC_CONTRATO               C_CONTRATO%ROWTYPE;
  LN_CONTRATO               NUMBER;
  LV_DIA_CICLO              VARCHAR2(100);
  LV_CICLO                  VARCHAR2(3);
  LV_CICLO_CONTRATO         VARCHAR2(3);
  LB_NOT_FOUND              BOOLEAN:=FALSE;
  LB_NOT_FOUND_CONTRATO     BOOLEAN:=FALSE;
  LN_NUEVO_VALOR_EN_FEES    NUMBER;
  LN_CANTIDAD_DIAS_EN_MES   NUMBER;
  LN_COSTO_TARIFA_BASICA    NUMBER;
  LN_VALOR_ANALIZAR         NUMBER;
  LN_ESCALA                 NUMBER;
  LN_NUEVO_VALOR_TOTAL      NUMBER;
  LV_Plan                   VARCHAR2(100);
  LV_ERROR                  VARCHAR2(2000);
  LV_FECHA                  VARCHAR2(20):= TO_CHAR(SYSDATE,'DDMMYYYYHH24MISS'); 
  LN_TECHO_MB               NUMBER;
  LN_COSTO_KB               NUMBER;
  LN_TARIFA_BASICA          NUMBER;
  LN_VALOR_CALCULADO        NUMBER;
  
  LN_MB_TARIFA_BASICA       number;
  LN_MB_exceso              number;
  
  LN_VALOR_ANALIZAR_MEGAS number;
  LV_PRGCODE_ESBULK       varchar2(1);
  LV_PRGCODE              customer_all.prgcode%type;
  
  --ECARFO PAN
  cursor c_gvparametro(cn_tipo_parametro number,cv_parametro varchar2) is
  select valor from gv_parametros where id_tipo_parametro=cn_tipo_parametro and id_parametro=cv_parametro;
  lv_ban_flujo varchar2(1);
 BEGIN
   
  begin
    OPEN c_gvparametro(10934,'CONTROL_FLUJO_TECHO'); 
    FETCH c_gvparametro INTO lv_ban_flujo; 
    CLOSE c_gvparametro;
    
    if nvl(lv_ban_flujo,'N') = 'S' then
       GCP_REFACTURA_NEW(pd_fecha_ini_periodo => PD_FECHA_INI_PERIODO,
                         pd_fecha_fin_periodo => PD_FECHA_FIN_PERIODO,
                         pv_error => PV_ERROR,
                         pv_bulk_sn => PV_BULK_SN);    
                                              
       return;
    end if;
  EXCEPTION
  WHEN OTHERS THEN          
        null;
  end;
    
 
  LV_DIA_CICLO := to_char(PD_FECHA_INI_PERIODO,'dd');
  
  SELECT t.id_ciclo 
    INTO LV_CICLO 
    FROM fa_ciclos_bscs@axis t 
   WHERE t.dia_ini_ciclo = LV_DIA_CICLO;

-- ITERAR
/*
  for j in (select co_id,sum(amount) amount From fees group by co_id
   where sncode = 129 And valid_from Between PD_FECHA_INI_PERIODO
      And PD_FECHA_FIN_PERIODO And period<>0 
     -- and co_id = 2594524--pruebas
  )

  LOOP */
  
  /*
    26/FEB/2014
    [9335] - Migracion Porting Bscs
    Se comento Loop ya que el Group By esta ubicado incorrectamente
    aunque la version anterior de BD lo soportaba la nueva version ya no  
  */

  FOR j in (select co_id,sum(amount) amount From fees 
   where sncode = 129 And valid_from Between PD_FECHA_INI_PERIODO
      And PD_FECHA_FIN_PERIODO And period<>0
      group by co_id
     -- and co_id = 2594524--pruebas
  )  
  
  LOOP

                

  
  
/*  IF J.CO_ID = 2594524 THEN
  LN_VALOR_ANALIZAR_MEGAS := 1;
  NULL;
  END IF;*/

   /*Determino si el cliente tiene un paquete*/ 
   OPEN C_REG(j.co_id); 
   FETCH C_REG INTO LC_REG;
   LB_NOT_FOUND := C_REG%NOTFOUND; 
   CLOSE C_REG;
   
  /* OPEN C_CONTRATO(j.co_id); 
   FETCH C_CONTRATO INTO LC_CONTRATO;
   --LB_NOT_FOUND_CONTRATO := C_REG%NOTFOUND; 
   CLOSE C_CONTRATO;
   */
   OPEN C_CONTRATO_CICLO(j.co_id); 
   FETCH C_CONTRATO_CICLO INTO LV_CICLO_CONTRATO,LV_PRGCODE;
   --LB_NOT_FOUND_CONTRATO := C_REG%NOTFOUND; 
   CLOSE C_CONTRATO_CICLO;
   
   if LV_PRGCODE in (5,6) then
   LV_PRGCODE_ESBULK := 'S';
   else
   LV_PRGCODE_ESBULK := 'N';
   end if;
   
   
   IF (LV_CICLO_CONTRATO = LV_CICLO) AND (nvl(PV_BULK_SN,'N') = LV_PRGCODE_ESBULK) THEN
   
   --Cuantos dias hay en el periodo
    LN_CANTIDAD_DIAS_EN_MES := PD_FECHA_FIN_PERIODO - PD_FECHA_INI_PERIODO+1; 
    
      IF LB_NOT_FOUND THEN
        
        LV_Plan := 'INT-EVENT';   /* Aplica Escala para Eventos*/
        LC_REG.costo_plan := 0;
        LC_REG.DIAS_ACTIVO:=LN_CANTIDAD_DIAS_EN_MES;
      
      ELSE  
      
        LV_Plan := LC_REG.ID_TIPO_DETALLE_SERV; /*Aplica Escala para Paquetes*/

      END IF;  

    --Prorratea
    OPEN C_FEES(j.co_id); 
    FETCH C_FEES INTO LC_FEES;
    LB_NOT_FOUND := C_FEES%NOTFOUND; 
    CLOSE C_FEES;
    
    OPEN C_val_config(LV_Plan); 
    FETCH C_val_config INTO LC_val_config;
    LB_NOT_FOUND := C_val_config%NOTFOUND; 
    CLOSE C_val_config;
      
    --Prorrateo tarifa basica
    /*LN_COSTO_TARIFA_BASICA := LC_REG.costo_plan;*/

    LN_COSTO_TARIFA_BASICA := (LC_REG.costo_plan * LC_REG.DIAS_ACTIVO )/ LN_CANTIDAD_DIAS_EN_MES;

    LN_MB_TARIFA_BASICA := (lc_val_config.cupo_mb_tb * LC_REG.DIAS_ACTIVO )/ LN_CANTIDAD_DIAS_EN_MES;
    
    LN_MB_EXCESO := j.amount/(1024*   lc_val_config.costo_kb_adicional); 

    --sumo lo que esta en la fees, mas el costo del paquete
    LN_VALOR_ANALIZAR := j.amount + LN_COSTO_TARIFA_BASICA;

    --Determino en que escala cae
    LN_ESCALA := GCF_GET_ESCALA(LV_Plan, LN_MB_TARIFA_BASICA+LN_MB_EXCESO, LV_ERROR);

    IF LN_ESCALA <= 1 OR j.amount = 0 THEN
    
      --Para la primera escala, no realiza ningun cambio   
      NULL;
    
    ELSE 
    
      BEGIN 
      
    --  GCF_GET_TECHO_COSTO_KB(LV_Plan, LN_ESCALA, LN_TECHO_MB, LN_COSTO_KB, LN_TARIFA_BASICA, LV_ERROR);
      
--      LN_VALOR_CALCULADO:= LN_TARIFA_BASICA + (LN_TECHO_MB*1024*(LN_COSTO_KB));
      
      --Obtengo el limite inferior (techo inferior)
      --LN_NUEVO_VALOR_TOTAL := GCF_GET_LIMITE_superior(LV_Plan, LN_ESCALA, LV_ERROR);
      LN_NUEVO_VALOR_TOTAL := GCF_GET_techo(LV_Plan, LN_ESCALA, LV_ERROR);
      
--      IF (LN_VALOR_ANALIZAR > LN_NUEVO_VALOR_TOTAL AND LN_VALOR_ANALIZAR < LN_VALOR_CALCULADO) THEN
      IF (LN_VALOR_ANALIZAR > LN_NUEVO_VALOR_TOTAL) THEN
      
      --Calculo el Nuevo Amount para la Fees
      LN_NUEVO_VALOR_EN_FEES := LN_NUEVO_VALOR_TOTAL - LN_COSTO_TARIFA_BASICA;

      
      -- RESPALDO REGISTROS DE LA FEES EN LA FEES_GPRS_HIST

      INSERT INTO FEES_GPRS_HIST (customer_id, seqno, fee_type, amount, remark, glcode, entdate, period, username, valid_from, jobcost, bill_fmt, servcat_code, serv_code, serv_type, 
                                  co_id, amount_gross, currency, glcode_disc, jobcost_id_disc, glcode_mincom, 
                                  jobcost_id_mincom, rec_version, cdr_id, cdr_sub_id, udr_basepart_id, udr_chargepart_id, 
                                  tmcode, vscode, spcode, sncode, evcode, fee_class, fu_pack_id, fup_version, 
                                  fup_seq, version, free_units_number, insertiondate, lastmoddate, Fecha_Proceso, Amount_New, Fecha_Fin_Periodo)
               SELECT customer_id, seqno, fee_type, amount, remark, 
                      glcode, entdate, period, username, valid_from, jobcost, bill_fmt, 
                      servcat_code, serv_code, serv_type, co_id, amount_gross, currency, 
                      glcode_disc, jobcost_id_disc, glcode_mincom, jobcost_id_mincom, 
                      rec_version, cdr_id, cdr_sub_id, udr_basepart_id, udr_chargepart_id, 
                      tmcode, vscode, spcode, sncode, evcode, fee_class, fu_pack_id, 
                      fup_version, fup_seq, version, free_units_number, insertiondate, lastmoddate, LV_FECHA, LN_NUEVO_VALOR_EN_FEES, PD_FECHA_FIN_PERIODO 
                      FROM fees WHERE co_id > 0 and co_id = J.CO_ID
      and sncode = 129 AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0;
      
    

      -- Elimino todos los registros de la FEES que cumplan esta condicion
       DELETE fees
       WHERE co_id > 0 and co_id = J.CO_ID
      and sncode = 129 And valid_from Between PD_FECHA_INI_PERIODO
      And PD_FECHA_FIN_PERIODO And period<>0 AND seqno NOT IN
       (SELECT MAX(seqno) FROM fees where co_id = J.co_id
       and sncode = 129  AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0 
       );
      --A EXCEPCION DEL QUE TENGA EL MAYOR SEQNO

      --A ese registro que qued�, se le hace un update del AMOUNT en la FEES
      UPDATE fees
      SET AMOUNT = LN_NUEVO_VALOR_EN_FEES
     -- WHERE co_id = J.CO_ID;
     WHERE co_id > 0 and co_id = J.CO_ID
      and sncode = 129 AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0;    
      
      
      END IF;
  
      COMMIT;    

      EXCEPTION
      
      WHEN OTHERS THEN          
        ROLLBACK;
        LV_ERROR := SQLERRM;
        DBMS_OUTPUT.put_line(lV_ERROR);
        
     --   INSERT INTO  GC_BITACORA_ERRORES_SVA@AXIS VALUES (LV_FECHA,j.co_id, LV_ERROR,j.AMOUNT, 'P', 'Error en GCK_TRX_REFACTURADOR_SVA'); 
        COMMIT;      
      END;
    END IF;
    END IF; --FIN IF CICLO
  END LOOP;  
  COMMIT;
  
 --******************************************---    
  EXCEPTION 
      
      WHEN OTHERS THEN
           ROLLBACK;
           PV_ERROR := 'ERROR EN GCP_REFACTURA: '||SQLERRM; 
           DBMS_OUTPUT.put_line(PV_ERROR);
    
 END GCP_REFACTURA;
 
 
  /*
   --=====================================================================================--
  -- Desarrollado por:  CLS Byron Rios Aviles
  -- Fecha de creaci�n: 26/10/2008
  -- Proyecto:          [3665] - Nuevo Esquema de Cobro de Datos
  -- Objetivo:          FUNCION PARA OBTENER LA ESCALA CON REFERENCIA A LA TABLA DE TECHOS
  --=====================================================================================--
  -- Modificado por: RGT. PAN
  -- Fecha:          17/08/2016
  -- Proyecto:       [10934] - Ecarfo
  -- Motivo:         Mejoras en procedimiento.
  --=====================================================================================--
 */
 PROCEDURE GCP_REFACTURA_NEW (PD_FECHA_INI_PERIODO IN DATE, 
                          PD_FECHA_FIN_PERIODO IN DATE,
                          PV_ERROR OUT VARCHAR2,
                          PV_BULK_SN VARCHAR2 DEFAULT NULL)IS
           

  --Declaro Cursores                          
  CURSOR C_REG(c_co_id NUMBER) IS
  SELECT * FROM CL_SERVICIOS_GPRS_DIAS
  WHERE co_id=c_co_id;
  
  CURSOR C_FEES(c_co_id NUMBER) IS
  SELECT sum(m.AMOUNT) FROM fees m
  WHERE m.co_id=c_co_id;
  
  CURSOR C_val_config(c_planaxis varchar2) IS
   SELECT cupo_mb_tb, costo_kb_adicional 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = c_planaxis AND estado = 'A' 
            AND rownum = 1;
  
  --ciclo
  CURSOR C_CONTRATO_ciclo(c_co_id NUMBER) IS
  select c.id_ciclo,a.prgcode from customer_all a,contract_all b, fa_ciclos_axis_bscs c
  where b.customer_id = a.customer_id 
  and a.billcycle = c.id_ciclo_admin
  and b.co_id  = c_co_id;
  
  
    ---INI 10934 RGT EAV
  
    cursor c_parametro(cn_tipo_parametro number, cv_parametro varchar2) is
      select valor
        from gv_parametros
       where id_tipo_parametro = cn_tipo_parametro
         and id_parametro = cv_parametro;
         
    LV_BAND_10934  VARCHAR2(1):= NULL;     
  
     ---FIN 10934 RGT EAV
 
  --Declaro Variables       
  LC_REG                    C_REG%ROWTYPE;
  LC_FEES                   C_FEES%ROWTYPE;
  LC_val_config             C_val_config%rowtype;
--  LC_CONTRATO               C_CONTRATO%ROWTYPE;
  LN_CONTRATO               NUMBER;
  LV_DIA_CICLO              VARCHAR2(100);
  LV_CICLO                  VARCHAR2(3);
  LV_CICLO_CONTRATO         VARCHAR2(3);
  LB_NOT_FOUND              BOOLEAN:=FALSE;
  LB_NOT_FOUND_CONTRATO     BOOLEAN:=FALSE;
  LN_NUEVO_VALOR_EN_FEES    NUMBER;
  LN_CANTIDAD_DIAS_EN_MES   NUMBER;
  LN_COSTO_TARIFA_BASICA    NUMBER;
  LN_VALOR_ANALIZAR         NUMBER;
  LN_ESCALA                 NUMBER;
  LN_NUEVO_VALOR_TOTAL      NUMBER;
  LV_Plan                   VARCHAR2(100);
  LV_ERROR                  VARCHAR2(2000);
  LV_FECHA                  VARCHAR2(20):= TO_CHAR(SYSDATE,'DDMMYYYYHH24MISS'); 
  LN_TECHO_MB               NUMBER;
  LN_COSTO_KB               NUMBER;
  LN_TARIFA_BASICA          NUMBER;
  LN_VALOR_CALCULADO        NUMBER;
  
  LN_MB_TARIFA_BASICA       number;
  LN_MB_exceso              number;
  
  LN_VALOR_ANALIZAR_MEGAS number;
  LV_PRGCODE_ESBULK       varchar2(1);
  LV_PRGCODE              customer_all.prgcode%type;
  
  --===================== [10934] ECARFO INI PAN ======================================--  
  -- Cursor que obtiene todos los valores a procesar. 
  Cursor C_Suma_Valores_Fees(C_Fecha_Ini_Periodo DATE, C_Fecha_Fin_Periodo DATE) is 
   select co_id,sum(amount) amount From fees 
   where sncode = 129 And valid_from Between C_Fecha_Ini_Periodo
      And C_Fecha_Fin_Periodo And period<>0
      group by co_id;
      
  TYPE LR_datos IS TABLE OF C_Suma_Valores_Fees%ROWTYPE;
  LT_datos   LR_datos;
  ln_id_bitacora_scp   Number;
  ln_error_scp         Number;
  lv_error_scp         VARCHAR2(2000):= null;
  lv_IdProceso         Varchar2(100):='BSCS_REFACTURADOR_SVA';
  lv_mensaje_apl_scp   varchar2(4000); 
  lv_mensaje_tec_scp   varchar2(4000); 
  lv_mensaje_acc_scp   varchar2(4000);
  -----Variables para SCP Trace
  lv_nombre_programa  varchar2(30) := 'GCP_REFACTURA';
  lv_sesion_trace     varchar2(60);
  ln_trace_key        number;
  ln_error_trace      number;
  lv_msg_error_trace  varchar2(250);
  lv_id_detalle_scp   varchar2(250);
  --===================== [10934] ECARFO FIN PAN ======================================--
  
  
 BEGIN
 
  LV_DIA_CICLO := to_char(PD_FECHA_INI_PERIODO,'dd');
  
  SELECT t.id_ciclo 
    INTO LV_CICLO 
    FROM fa_ciclos_bscs@axis t 
   WHERE t.dia_ini_ciclo = LV_DIA_CICLO;
   
  scp_dat.SCK_API.SCP_BITACORA_PROCESOS_INS( pv_id_proceso        => lv_IdProceso,
                                              pv_referencia        => 'INICIO_REFACTURADOR_SVA',
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => null,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => 0,
                                              pv_unidad_registro   => 'Registros',
                                              pn_id_bitacora       => ln_id_bitacora_scp,
                                              pn_error             => ln_error_scp,
                                              pv_error             => lv_error_scp);                                          
   -- Ini [10934] Ecarfo RGT. PAN                                         
   --Cuantos dias hay en el periodo
    LN_CANTIDAD_DIAS_EN_MES := PD_FECHA_FIN_PERIODO - PD_FECHA_INI_PERIODO+1; 
   -- Fin [10934] Ecarfo RGT. PAN
  open C_Suma_Valores_Fees(PD_FECHA_INI_PERIODO, PD_FECHA_FIN_PERIODO);
  fetch C_Suma_Valores_Fees bulk collect
  into lt_datos;
  close C_Suma_Valores_Fees;
  
  for i in lt_datos.first .. LT_datos.last loop 
   -- Ini [10934] Ecarfo RGT. PAN   
   -- Inicializaci�n de variables.
    LN_COSTO_TARIFA_BASICA := 0;
    LN_MB_TARIFA_BASICA    := 0;
    LN_MB_EXCESO           := 0;
    LN_VALOR_ANALIZAR      := 0;
    LN_ESCALA              := 0;
    LN_NUEVO_VALOR_TOTAL   := 0;
    LV_CICLO_CONTRATO      := NULL;
    LV_PRGCODE             := NULL;
    LC_REG                 := NULL;
    LC_FEES                := NULL;
    LC_val_config          := NULL;
    -- Fin [10934] Ecarfo RGT. PAN  
  
   lv_mensaje_apl_scp := 'INICIO PROCESO TECHO - CICLO:'||LV_CICLO||'-'||PD_FECHA_INI_PERIODO||'-'||PD_FECHA_FIN_PERIODO; 
   lv_mensaje_tec_scp := null; 
   lv_mensaje_acc_scp := null; 
   scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                            lv_mensaje_apl_scp, 
                                            lv_mensaje_tec_scp, 
                                            lv_mensaje_acc_scp, 
                                            0, 
                                            lt_datos(i).co_id, 
                                            null, 
                                            null, 
                                            NULL, 
                                            null, 
                                            'N', 
                                            lv_id_detalle_scp, 
                                            ln_error_scp, 
                                            lv_error_scp);
    /*Determino si el cliente tiene un paquete*/                                          
   OPEN C_REG(lt_datos(i).co_id); 
   FETCH C_REG INTO LC_REG;
   LB_NOT_FOUND := C_REG%NOTFOUND; 
   CLOSE C_REG;
   
  
   OPEN C_CONTRATO_CICLO(lt_datos(i).co_id); 
   FETCH C_CONTRATO_CICLO INTO LV_CICLO_CONTRATO,LV_PRGCODE;
   CLOSE C_CONTRATO_CICLO;
   
   if LV_PRGCODE in (5,6) then
   LV_PRGCODE_ESBULK := 'S';
   else
   LV_PRGCODE_ESBULK := 'N';
   end if;
   
   lv_mensaje_apl_scp := 'Obtengo LV_PRGCODE y CICLO'; 
   lv_mensaje_tec_scp := null; 
   lv_mensaje_acc_scp := 'LV_CICLO_CONTRATO:'||LV_CICLO_CONTRATO||'LV_PRGCODE:'||LV_PRGCODE; 
   scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                            lv_mensaje_apl_scp, 
                                            lv_mensaje_tec_scp, 
                                            lv_mensaje_acc_scp, 
                                            0, 
                                            lt_datos(i).co_id, 
                                            LV_CICLO_CONTRATO, 
                                            LV_PRGCODE_ESBULK, 
                                            LV_PRGCODE, 
                                            null, 
                                            'N', 
                                            lv_id_detalle_scp, 
                                            ln_error_scp, 
                                            lv_error_scp);
   
   IF (LV_CICLO_CONTRATO = LV_CICLO) AND (nvl(PV_BULK_SN,'N') = LV_PRGCODE_ESBULK) THEN
    
   lv_mensaje_apl_scp := 'Obtengo Cuantos dias hay en el periodo'; 
   lv_mensaje_tec_scp := null; 
   lv_mensaje_acc_scp := 'LN_CANTIDAD_DIAS_EN_MES:'||LN_CANTIDAD_DIAS_EN_MES; 
   scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                            lv_mensaje_apl_scp, 
                                            lv_mensaje_tec_scp, 
                                            lv_mensaje_acc_scp, 
                                            0, 
                                            lt_datos(i).co_id, 
                                            LN_CANTIDAD_DIAS_EN_MES, 
                                            null, 
                                            null, 
                                            null, 
                                            'N', 
                                            lv_id_detalle_scp, 
                                            ln_error_scp, 
                                            lv_error_scp);
                                            
      IF LB_NOT_FOUND THEN
        
        LV_Plan := 'INT-EVENT';   /* Aplica Escala para Eventos*/
        LC_REG.costo_plan := 0;
        LC_REG.DIAS_ACTIVO:=LN_CANTIDAD_DIAS_EN_MES;
        
        lv_mensaje_apl_scp := 'Aplica Escala para Eventos'; 
        lv_mensaje_tec_scp := null; 
        lv_mensaje_acc_scp := null; 
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                                 lv_mensaje_apl_scp, 
                                                 lv_mensaje_tec_scp, 
                                                 lv_mensaje_acc_scp, 
                                                 0, 
                                                 lt_datos(i).co_id, 
                                                 LC_REG.DIAS_ACTIVO, 
                                                 null, 
                                                 NULL, 
                                                 null, 
                                                 'N', 
                                                 lv_id_detalle_scp, 
                                                 ln_error_scp, 
                                                 lv_error_scp);
      
      ELSE  
      
        LV_Plan := LC_REG.ID_TIPO_DETALLE_SERV; /*Aplica Escala para Paquetes*/

      END IF;  

    --Prorratea
    OPEN C_FEES(lt_datos(i).co_id); 
    FETCH C_FEES INTO LC_FEES;
    LB_NOT_FOUND := C_FEES%NOTFOUND; 
    CLOSE C_FEES;
    
    OPEN C_val_config(LV_Plan); 
    FETCH C_val_config INTO LC_val_config;
    LB_NOT_FOUND := C_val_config%NOTFOUND; 
    CLOSE C_val_config;
    
      ---INI 10934 RGT EAV
         OPEN c_parametro(10934,'BAND_TECHOS');
        FETCH c_parametro
          INTO LV_BAND_10934;
        CLOSE c_parametro;
        
        IF NVL(LV_BAND_10934,'N')='S' THEN
          
          IF LB_NOT_FOUND  THEN
            LV_Plan            := 'INT-EVENT';
            LC_REG.costo_plan  := 0;
            LC_REG.DIAS_ACTIVO := LN_CANTIDAD_DIAS_EN_MES;
          
              OPEN C_val_config('INT-EVENT');
              FETCH C_val_config
                INTO LC_val_config;
              LB_NOT_FOUND := C_val_config%NOTFOUND;
              CLOSE C_val_config;
        
           END IF;
         END IF;
      --- FIN 10934 RGT EAV
      
    --Prorrateo tarifa basica
    LN_COSTO_TARIFA_BASICA := (LC_REG.costo_plan * LC_REG.DIAS_ACTIVO )/ LN_CANTIDAD_DIAS_EN_MES;

    LN_MB_TARIFA_BASICA := (lc_val_config.cupo_mb_tb * LC_REG.DIAS_ACTIVO )/ LN_CANTIDAD_DIAS_EN_MES;
    
    LN_MB_EXCESO := lt_datos(i).amount/(1024*   lc_val_config.costo_kb_adicional); 
    
    --sumo lo que esta en la fees, mas el costo del paquete
    LN_VALOR_ANALIZAR := lt_datos(i).amount + LN_COSTO_TARIFA_BASICA;

    --Determino en que escala cae
    LN_ESCALA := GCF_GET_ESCALA(LV_Plan, LN_MB_TARIFA_BASICA+LN_MB_EXCESO, LV_ERROR);
    
    lv_mensaje_apl_scp := 'Prorrateo tarifa basica, Determino en que escala cae'; 
    lv_mensaje_tec_scp := null; 
    lv_mensaje_acc_scp := 'LN_COSTO_TARIFA_BASICA:'||LN_COSTO_TARIFA_BASICA||
                          'LN_MB_TARIFA_BASICA:'||LN_MB_TARIFA_BASICA||
                          'LN_MB_EXCESO:'||LN_MB_EXCESO||
                          'LN_ESCALA:'||LN_ESCALA; 
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                             lv_mensaje_apl_scp, 
                                             lv_mensaje_tec_scp, 
                                             lv_mensaje_acc_scp, 
                                             0, 
                                             lt_datos(i).co_id, 
                                             null, 
                                             null, 
                                             NULL, 
                                             null, 
                                             'N', 
                                             lv_id_detalle_scp, 
                                             ln_error_scp, 
                                             lv_error_scp);
                                             
    IF LN_ESCALA <= 1 OR lt_datos(i).amount = 0 THEN
    
      --Para la primera escala, no realiza ningun cambio   
      NULL;
    
    ELSE 
    
      BEGIN 
      
      --Obtengo el limite inferior (techo inferior)
      LN_NUEVO_VALOR_TOTAL := GCF_GET_techo(LV_Plan, LN_ESCALA, LV_ERROR);
      
      lv_mensaje_apl_scp := 'Obtengo el limite inferior (techo inferior)'; 
      lv_mensaje_tec_scp := null; 
      lv_mensaje_acc_scp := 'LN_NUEVO_VALOR_TOTAL:'||LN_NUEVO_VALOR_TOTAL; 
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                               lv_mensaje_apl_scp, 
                                               lv_mensaje_tec_scp, 
                                               lv_mensaje_acc_scp, 
                                               0, 
                                               lt_datos(i).co_id, 
                                               null, 
                                               null, 
                                               NULL, 
                                               null, 
                                               'N', 
                                               lv_id_detalle_scp, 
                                               ln_error_scp, 
                                               lv_error_scp);
      
      IF (LN_VALOR_ANALIZAR > LN_NUEVO_VALOR_TOTAL) THEN
      
      --Calculo el Nuevo Amount para la Fees
      LN_NUEVO_VALOR_EN_FEES := LN_NUEVO_VALOR_TOTAL - LN_COSTO_TARIFA_BASICA;

      lv_mensaje_apl_scp := 'Calculo el Nuevo Amount para la Fees'; 
      lv_mensaje_tec_scp := null; 
      lv_mensaje_acc_scp := 'LN_NUEVO_VALOR_EN_FEES:'||LN_NUEVO_VALOR_EN_FEES; 
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                               lv_mensaje_apl_scp, 
                                               lv_mensaje_tec_scp, 
                                               lv_mensaje_acc_scp, 
                                               0, 
                                               lt_datos(i).co_id, 
                                               null, 
                                               null, 
                                               NULL, 
                                               null, 
                                               'N', 
                                               lv_id_detalle_scp, 
                                               ln_error_scp, 
                                               lv_error_scp);
      -- RESPALDO REGISTROS DE LA FEES EN LA FEES_GPRS_HIST

      
      INSERT INTO FEES_GPRS_HIST (customer_id, seqno, fee_type, amount, remark, glcode, entdate, period, username, valid_from, jobcost, bill_fmt, servcat_code, serv_code, serv_type, 
                                  co_id, amount_gross, currency, glcode_disc, jobcost_id_disc, glcode_mincom, 
                                  jobcost_id_mincom, rec_version, cdr_id, cdr_sub_id, udr_basepart_id, udr_chargepart_id, 
                                  tmcode, vscode, spcode, sncode, evcode, fee_class, fu_pack_id, fup_version, 
                                  fup_seq, version, free_units_number, insertiondate, lastmoddate, Fecha_Proceso, Amount_New, Fecha_Fin_Periodo)
               SELECT customer_id, seqno, fee_type, amount, remark, 
                      glcode, entdate, period, username, valid_from, jobcost, bill_fmt, 
                      servcat_code, serv_code, serv_type, co_id, amount_gross, currency, 
                      glcode_disc, jobcost_id_disc, glcode_mincom, jobcost_id_mincom, 
                      rec_version, cdr_id, cdr_sub_id, udr_basepart_id, udr_chargepart_id, 
                      tmcode, vscode, spcode, sncode, evcode, fee_class, fu_pack_id, 
                      fup_version, fup_seq, version, free_units_number, insertiondate, lastmoddate, LV_FECHA, LN_NUEVO_VALOR_EN_FEES, PD_FECHA_FIN_PERIODO 
                      FROM fees WHERE co_id > 0 and co_id = lt_datos(i).co_id
      and sncode = 129 AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0;
      
      lv_mensaje_apl_scp := 'RESPALDO REGISTROS DE LA FEES EN LA FEES_GPRS_HIST'; 
      lv_mensaje_tec_scp := null; 
      lv_mensaje_acc_scp := null; 
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                               lv_mensaje_apl_scp, 
                                               lv_mensaje_tec_scp, 
                                               lv_mensaje_acc_scp, 
                                               0, 
                                               lt_datos(i).co_id, 
                                               null, 
                                               null, 
                                               NULL, 
                                               null, 
                                               'N', 
                                               lv_id_detalle_scp, 
                                               ln_error_scp, 
                                               lv_error_scp);

      -- Elimino todos los registros de la FEES que cumplan esta condicion      
       DELETE fees
       WHERE co_id > 0 and co_id = lt_datos(i).co_id
      and sncode = 129 And valid_from Between PD_FECHA_INI_PERIODO
      And PD_FECHA_FIN_PERIODO And period<>0 AND seqno NOT IN
       (SELECT MAX(seqno) FROM fees where co_id = lt_datos(i).co_id
       and sncode = 129  AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0 
       );
      --A EXCEPCION DEL QUE TENGA EL MAYOR SEQNO
      lv_mensaje_apl_scp := 'Elimino todos los registros de la FEES que cumplan esta condicion'; 
      lv_mensaje_tec_scp := null; 
      lv_mensaje_acc_scp := null; 
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                               lv_mensaje_apl_scp, 
                                               lv_mensaje_tec_scp, 
                                               lv_mensaje_acc_scp, 
                                               0, 
                                               lt_datos(i).co_id, 
                                               null, 
                                               null, 
                                               NULL, 
                                               null, 
                                               'N', 
                                               lv_id_detalle_scp, 
                                               ln_error_scp, 
                                               lv_error_scp);
      --A ese registro que qued�, se le hace un update del AMOUNT en la FEES      
      UPDATE fees
      SET AMOUNT = LN_NUEVO_VALOR_EN_FEES
     -- WHERE co_id = lt_datos(i).co_id;
     WHERE co_id > 0 and co_id = lt_datos(i).co_id
      and sncode = 129 AND valid_from BETWEEN PD_FECHA_INI_PERIODO
      AND PD_FECHA_FIN_PERIODO And period<>0;
       
      lv_mensaje_apl_scp := 'Se le hace un update del AMOUNT en la FEES'; 
      lv_mensaje_tec_scp := null; 
      lv_mensaje_acc_scp := null; 
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                               lv_mensaje_apl_scp, 
                                               lv_mensaje_tec_scp, 
                                               lv_mensaje_acc_scp, 
                                               0, 
                                               lt_datos(i).co_id, 
                                               null, 
                                               null, 
                                               NULL, 
                                               null, 
                                               'N', 
                                               lv_id_detalle_scp, 
                                               ln_error_scp, 
                                               lv_error_scp);
      
      END IF;
  
      COMMIT;    

      EXCEPTION
      
      WHEN OTHERS THEN          
        ROLLBACK;
        LV_ERROR := SQLERRM;
        lv_mensaje_apl_scp := 'Error al momento de calcular techo'; 
        lv_mensaje_tec_scp := LV_ERROR; 
        lv_mensaje_acc_scp := null; 
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                                 lv_mensaje_apl_scp, 
                                                 lv_mensaje_tec_scp, 
                                                 lv_mensaje_acc_scp, 
                                                 0, 
                                                 lt_datos(i).co_id, 
                                                 null, 
                                                 null, 
                                                 NULL, 
                                                 null, 
                                                 'N', 
                                                 lv_id_detalle_scp, 
                                                 ln_error_scp, 
                                                 lv_error_scp);
        DBMS_OUTPUT.put_line(lV_ERROR);
        
     --   INSERT INTO  GC_BITACORA_ERRORES_SVA@AXIS VALUES (LV_FECHA,lt_datos(i).co_id, LV_ERROR,j.AMOUNT, 'P', 'Error en GCK_TRX_REFACTURADOR_SVA'); 
        COMMIT;      
      END;
    END IF;
    END IF; --FIN IF CICLO
    lv_mensaje_apl_scp := 'FINALIZA PROCESO TECHO CICLO'||LV_CICLO||'-'||PD_FECHA_INI_PERIODO||'-'||PD_FECHA_FIN_PERIODO; 
    lv_mensaje_tec_scp := null; 
    lv_mensaje_acc_scp := null; 
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                              lv_mensaje_apl_scp, 
                                              lv_mensaje_tec_scp, 
                                              lv_mensaje_acc_scp, 
                                              0, 
                                              lt_datos(i).co_id, 
                                              null, 
                                              null, 
                                              NULL, 
                                              null, 
                                              'N', 
                                              lv_id_detalle_scp, 
                                              ln_error_scp, 
                                              lv_error_scp);
  END LOOP;  
  COMMIT;
  
 --******************************************---    
  EXCEPTION 
      
      WHEN OTHERS THEN
           ROLLBACK;
           PV_ERROR := 'ERROR EN GCP_REFACTURA: '||SQLERRM; 
           lv_mensaje_apl_scp := 'Error en proceso'; 
           lv_mensaje_tec_scp := PV_ERROR; 
           lv_mensaje_acc_scp := null; 
           scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp, 
                                                    lv_mensaje_apl_scp, 
                                                    lv_mensaje_tec_scp, 
                                                    lv_mensaje_acc_scp, 
                                                    0, 
                                                    NULL, 
                                                    null, 
                                                    null, 
                                                    NULL, 
                                                    null, 
                                                    'N', 
                                                    lv_id_detalle_scp, 
                                                    ln_error_scp, 
                                                    lv_error_scp);
           DBMS_OUTPUT.put_line(PV_ERROR);
    
 END GCP_REFACTURA_NEW;
 FUNCTION GCF_GET_ESCALA (PV_PLAN  VARCHAR2, 
                          PN_VALOR NUMBER,
                          PV_ERROR OUT VARCHAR2) RETURN NUMBER IS
               
   LN_CONT      NUMBER:= 0;
   LV_ERROR     VARCHAR2(2000);
   LE_ERROR     EXCEPTION;
   
 BEGIN
 
--     LN_VALOR_ANALIZAR_MEGAS := LN_VALOR_ANALIZAR/(LC_REG.costo_plan*1024)  
 

    BEGIN
     SELECT ESCALA INTO LN_CONT
        FROM porta.PARAM_TECHOS_COBRO@AXIS
       WHERE plan_axis = PV_PLAN AND estado ='A'         
         AND TECHO_VALOR_MAX IN (SELECT MAX(TECHO_VALOR_MAX) 
                              FROM porta.PARAM_TECHOS_COBRO@AXIS
                              WHERE plan_axis = PV_PLAN
                              AND ESTADO= 'A'
                              AND PN_VALOR > TECHO_VALOR_MIN);
    EXCEPTION
    
      WHEN NO_DATA_FOUND THEN
         RETURN 0; 
         
      WHEN OTHERS THEN
         LV_ERROR := SQLERRM;
         RAISE LE_ERROR;
                                  
    END;

  RETURN LN_CONT;
 
 EXCEPTION 
 
  WHEN OTHERS THEN
  
       PV_ERROR:= 'Error en GCF_GET_ESCALA: ' || LV_ERROR;
       RETURN -1; 
     
 END GCF_GET_ESCALA;
 
 /*
   --=====================================================================================--
  -- Desarrollado por:  CLS Byron Rios Aviles
  -- Fecha de creaci�n: 26/10/2008
  -- Proyecto:          [3665] - Nuevo Esquema de Cobro de Datos
  -- Objetivo:          Funcion para poder obtener el limite inferior (techo inferior) 
  --=====================================================================================--
 */
 
  FUNCTION GCF_GET_LIMITE_INFERIOR (PV_PLAN   VARCHAR2, 
                                    PN_ESCALA NUMBER,
                                    PV_ERROR  OUT VARCHAR2) RETURN NUMBER IS
   
  --Cursor para obtener el limite inferior          
   CURSOR C_TECHO(CV_PLAN VARCHAR2, CN_ESCALA NUMBER) IS               
         SELECT TECHO_VALOR_MIN, tarifa_basica 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = PV_PLAN AND estado = 'A' 
            AND escala = CN_ESCALA;
   
   --Declaro variables
   LC_TECHO      C_TECHO%ROWTYPE;      
   LN_CONT       NUMBER:= 0;
   --LB_NOT_FOUND  BOOLEAN:=FALSE;
   
 BEGIN
    
    --Abro el cursor para saber el techo inferior
    OPEN  C_TECHO(PV_PLAN, PN_ESCALA);
    FETCH C_TECHO INTO LC_TECHO;
    CLOSE C_TECHO;
    
    LN_CONT := LC_TECHO.TECHO_VALOR_MIN;
 
    RETURN LN_CONT;
 
 EXCEPTION 
 
  WHEN OTHERS THEN
  
       PV_ERROR:= 'Error en GCF_OBTENER_LIMITE_INFERIOR: ' || SQLERRM;
       DBMS_OUTPUT.put_line(PV_ERROR);
       
       RETURN -1; 
     
 END GCF_GET_LIMITE_INFERIOR;
 
 
 

 
  /*
   --=====================================================================================--
  -- Desarrollado por:  CLS Byron Rios Aviles
  -- Fecha de creaci�n: 09/01/2009
  -- Proyecto:          [3665] - Nuevo Esquema de Cobro de Datos - Alcance
  -- Objetivo:          Procedimiento para el c�lculo del Techo en MB utilizado en
                        la condicion en el procedimiento GCP_REFACTURA
  --=====================================================================================--
 */
 
  /* PROCEDURE GCF_GET_TECHO_COSTO_KB (PV_PLAN         VARCHAR2, 
                                    PN_ESCALA        NUMBER,
                                    PN_TECHO_MB      OUT NUMBER,
                                    PN_COSTO_KB      OUT NUMBER,
                                    PN_TARIFA_BASICA OUT NUMBER,
                                    PV_ERROR         OUT VARCHAR2) IS
   
  --Cursor para obtener el limite inferior          
   CURSOR C_TECHO(CV_PLAN VARCHAR2, CN_ESCALA NUMBER) IS               
         SELECT TECHO_MB, COSTO_KB_ADICIONAL 
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = PV_PLAN AND estado = 'A' 
            AND escala = CN_ESCALA;
            
   CURSOR C_TECHO_MB(CV_PLAN VARCHAR2) IS               
         SELECT TECHO_MB, TARIFA_BASICA
           FROM porta.param_techos_cobro@axis 
          WHERE plan_axis = PV_PLAN AND estado = 'A' 
            AND escala = 1;
   --Declaro variables
   LC_TECHO           C_TECHO%ROWTYPE;  
   LC_TECHO_TARIFA    C_TECHO_MB%ROWTYPE;    
   LN_TECHO_MB_BASE   NUMBER;
   LN_TECHO_MB        NUMBER;
   --LB_NOT_FOUND  BOOLEAN:=FALSE;
   
 BEGIN
     
    --Abro el cursor para saber el techo inferior
    OPEN  C_TECHO(PV_PLAN, PN_ESCALA);
    FETCH C_TECHO INTO LC_TECHO;
    CLOSE C_TECHO;
    
    OPEN  C_TECHO_MB(PV_PLAN);
    FETCH C_TECHO_MB INTO LC_TECHO_TARIFA;
    CLOSE C_TECHO_MB;
    
    LN_TECHO_MB_BASE := LC_TECHO_TARIFA.TECHO_MB;
    
    PN_TARIFA_BASICA := LC_TECHO_TARIFA.TARIFA_BASICA;
    
    LN_TECHO_MB := LC_TECHO.TECHO_MB;
    
    PN_TECHO_MB := LN_TECHO_MB - LN_TECHO_MB_BASE;
    
    PN_COSTO_KB := LC_TECHO.COSTO_KB_ADICIONAL;
 
 EXCEPTION 
 
  WHEN OTHERS THEN
  
       PV_ERROR:= 'Error en GCF_GET_TECHO_COSTO_KB: ' || SQLERRM;
     
 END GCF_GET_TECHO_COSTO_KB;*/
 
end GCK_TRX_REFACTURADOR_SVA;
/
