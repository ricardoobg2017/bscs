CREATE OR REPLACE PACKAGE MFK_OBJ_MENSAJES_IVR IS

  PROCEDURE mfp_insertar(pn_idenvio         IN mf_mensajes_ivr.idenvio%TYPE,
                         pv_mensaje         IN mf_mensajes_ivr.mensaje%TYPE,
                         pn_secuencia_envio IN mf_mensajes_ivr.secuencia_envio%TYPE,
                         pv_remitente       IN mf_mensajes_ivr.remitente%TYPE,
                         pv_destinatario    IN mf_mensajes_ivr.destinatario%TYPE,
                         pv_copia           IN mf_mensajes_ivr.copia%TYPE,
                         pv_copia_oculta    IN mf_mensajes_ivr.copiaoculta%TYPE,
                         pv_asunto          IN mf_mensajes_ivr.asunto%TYPE,
                         pn_hilo            IN mf_mensajes_ivr.hilo%TYPE DEFAULT NULL,
                         pv_identificacion  IN mf_mensajes_ivr.identificacion%TYPE DEFAULT NULL,
                         pn_calificacion    IN mf_mensajes_ivr.calificacion%TYPE DEFAULT NULL,
                         pv_categoria       IN mf_mensajes_ivr.categoria_cli%TYPE DEFAULT NULL,
                         pv_cuenta          IN mf_mensajes_ivr.cuenta_bscs%TYPE DEFAULT NULL,
                         pv_msgerror        IN OUT VARCHAR2);

  PROCEDURE mfp_actualizar(pn_idenvio           IN mf_mensajes_ivr.idenvio%TYPE,
                           pv_mensaje           IN mf_mensajes_ivr.mensaje%TYPE,
                           pn_secuencia_envio   IN mf_mensajes_ivr.secuencia_envio%TYPE,
                           pn_secuencia_mensaje IN mf_mensajes_ivr.secuencia_mensaje%TYPE,
                           pv_remitente         IN mf_mensajes_ivr.remitente%TYPE,
                           pv_destinatario      IN mf_mensajes_ivr.destinatario%TYPE,
                           pv_copia             IN mf_mensajes_ivr.copia%TYPE,
                           pv_copia_oculta      IN mf_mensajes_ivr.copiaoculta%TYPE,
                           pv_asunto            IN mf_mensajes_ivr.asunto%TYPE,
                           pv_estado            IN mf_mensajes_ivr.estado%TYPE,
                           pv_msgerror          IN OUT VARCHAR2);

END MFK_OBJ_MENSAJES_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_MENSAJES_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar para el envio del reporte en la 
  **               tabla MF_MENSAJES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar(pn_idenvio         IN mf_mensajes_ivr.idenvio%TYPE,
                         pv_mensaje         IN mf_mensajes_ivr.mensaje%TYPE,
                         pn_secuencia_envio IN mf_mensajes_ivr.secuencia_envio%TYPE,
                         pv_remitente       IN mf_mensajes_ivr.remitente%TYPE,
                         pv_destinatario    IN mf_mensajes_ivr.destinatario%TYPE,
                         pv_copia           IN mf_mensajes_ivr.copia%TYPE,
                         pv_copia_oculta    IN mf_mensajes_ivr.copiaoculta%TYPE,
                         pv_asunto          IN mf_mensajes_ivr.asunto%TYPE,
                         pn_hilo            IN mf_mensajes_ivr.hilo%TYPE DEFAULT NULL,
                         pv_identificacion  IN mf_mensajes_ivr.identificacion%TYPE DEFAULT NULL,
                         pn_calificacion    IN mf_mensajes_ivr.calificacion%TYPE DEFAULT NULL,
                         pv_categoria       IN mf_mensajes_ivr.categoria_cli%TYPE DEFAULT NULL,
                         pv_cuenta          IN mf_mensajes_ivr.cuenta_bscs%TYPE DEFAULT NULL,
                         pv_msgerror        IN OUT VARCHAR2) IS
  
    lv_aplicacion        VARCHAR2(100) := 'MFK_OBJ_MENSAJES_IVR.MFP_INSERTAR';
    ln_secuencia_mensaje mf_mensajes_ivr.secuencia_mensaje%TYPE;
    pv_estado            mf_mensajes_ivr.estado%TYPE := 'A';
  
  BEGIN
  
    SELECT mfs_enviomensaje_sec_ivr.nextval
      INTO ln_secuencia_mensaje
      FROM dual;
  
    INSERT INTO mf_mensajes_ivr
      (idenvio,
       mensaje,
       secuencia_envio,
       secuencia_mensaje,
       remitente,
       destinatario,
       copia,
       copiaoculta,
       asunto,
       estado,
       hilo,
       identificacion,
       estado_archivo,
       calificacion,
       categoria_cli,
       cuenta_bscs,
       fecha_registro)
    VALUES
      (pn_idenvio,
       pv_mensaje,
       pn_secuencia_envio,
       ln_secuencia_mensaje,
       pv_remitente,
       pv_destinatario,
       pv_copia,
       pv_copia_oculta,
       pv_asunto,
       pv_estado,
       pn_hilo,
       pv_identificacion,
       'I',
       pn_calificacion,
       pv_categoria,
       pv_cuenta,
       SYSDATE);
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite actualizar para el envio del reporte en la 
  **               tabla MF_MENSAJES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_actualizar(pn_idenvio           IN mf_mensajes_ivr.idenvio%TYPE,
                           pv_mensaje           IN mf_mensajes_ivr.mensaje%TYPE,
                           pn_secuencia_envio   IN mf_mensajes_ivr.secuencia_envio%TYPE,
                           pn_secuencia_mensaje IN mf_mensajes_ivr.secuencia_mensaje%TYPE,
                           pv_remitente         IN mf_mensajes_ivr.remitente%TYPE,
                           pv_destinatario      IN mf_mensajes_ivr.destinatario%TYPE,
                           pv_copia             IN mf_mensajes_ivr.copia%TYPE,
                           pv_copia_oculta      IN mf_mensajes_ivr.copiaoculta%TYPE,
                           pv_asunto            IN mf_mensajes_ivr.asunto%TYPE,
                           pv_estado            IN mf_mensajes_ivr.estado%TYPE,
                           pv_msgerror          IN OUT VARCHAR2) IS
  
    CURSOR c_table_cur(cn_idenvio           NUMBER,
                       cn_secuencia_envio   NUMBER,
                       cn_secuencia_mensaje NUMBER) IS
      SELECT *
        FROM mf_mensajes_ivr m
       WHERE m.secuencia_mensaje = cn_secuencia_mensaje
         AND m.secuencia_envio = cn_secuencia_envio
         AND m.idenvio = cn_idenvio;
         
    lc_currentrow    mf_mensajes_ivr%ROWTYPE;
    le_nodataupdated EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_MENSAJES_IVR.MFP_ACTUALIZAR';
    
  BEGIN
    
    pv_msgerror := NULL;
    OPEN c_table_cur(pn_idenvio, pn_secuencia_envio, pn_secuencia_mensaje);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodataupdated;
    END IF;
  
    UPDATE mf_mensajes_ivr
       SET mensaje      = nvl(pv_mensaje, mensaje),
           remitente    = nvl(pv_remitente, remitente),
           destinatario = nvl(pv_destinatario, destinatario),
           copia        = nvl(pv_copia, copia),
           copiaoculta  = nvl(pv_copia_oculta, copiaoculta),
           asunto       = nvl(pv_asunto, asunto),
           estado       = nvl(pv_estado, estado)
     WHERE idenvio = pn_idenvio
       AND secuencia_envio = pn_secuencia_envio
       AND secuencia_mensaje = pn_secuencia_mensaje;
    CLOSE c_table_cur;
  
  EXCEPTION
    WHEN le_nodataupdated THEN
      pv_msgerror := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

END MFK_OBJ_MENSAJES_IVR;
/
