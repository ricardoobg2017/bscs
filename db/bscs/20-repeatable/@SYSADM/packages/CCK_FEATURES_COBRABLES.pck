CREATE OR REPLACE package CCK_FEATURES_COBRABLES is
  -- Version : 1.0.0
  -- Author  : DCHONILLO
  -- Created : 30/12/2004 10:38:02
  -- Purpose : DETECTAR FEATURES COBRABLES
  ------------------------------------------------------
  -- Version:  1.0.1
  -- Author  : DCHONILLO
  -- Created : 30/12/2004 10:38:02
  -- Purpose : Obtener n�mero de tel�fono
  
Procedure Ccp_Obtener_Features(Pn_Tmcode    In Number,
                                                   Pv_Resultado Out Varchar2);
Procedure Principal (pv_resultado Out Varchar2);                                                   

end CCK_FEATURES_COBRABLES;
/
CREATE OR REPLACE package body CCK_FEATURES_COBRABLES is

Procedure Ccp_Obtener_Features(Pn_Tmcode    In Number,
                                                 Pv_Resultado Out Varchar2) Is

  Cursor Features(Cn_Coid Number) Is
      Select  Pss.Sncode
      ---     pss.status
        From Pr_Serv_Status_Hist Pss, Profile_Service Ps, Cc_Tmp_Sncode_Fact Ctf
       Where Ps.Co_Id = Cn_Coid And Ps.Co_Id = Pss.Co_Id And
             Pss.Histno = Ps.Status_Histno And Ps.Sncode = Pss.Sncode And
             Ps.Profile_Id = 0 And Ps.Delete_Flag Is Null And
             Ctf.Sncode = Ps.Sncode And Pss.Status = 'A'
       Order By Pss.Sncode;


  Lv_Error         Varchar2(200);
  lv_sentencia    Varchar2(500);
  Le_Error Exception;
  Type cadena Is Table Of Varchar2(50);
  lr_cadena   cadena;
  Type co_id Is Table Of Number;
  lr_co_id co_id;
  
  

Begin


  If pn_tmcode Is Null Then
     lv_error:='Ingresar n�mero del plan o tmcode';     
     Raise le_error;
  End If;
  
  /*  [1468] JLI 11/05/2006. Se modifica la sentencia para que soporte
                           Prefijo 8 */
  --lv_sentencia:='Insert /*+ append */ Into cc_coid_features Nologging (co_id,tmcode,telefono) '; 
  --lv_sentencia:=lv_sentencia||'Select /*+ rule */  Co.Co_Id, Co.Tmcode,Substr(dn.Dn_Num, 5) ';
  --lv_sentencia:=lv_sentencia||'From Contract_All Co, Contract_History Ch, contr_services_cap csc, directory_number dn ';
  --lv_sentencia:=lv_sentencia||' Where  Co.Tmcode =:1 and Co.Co_Id = Ch.Co_Id and Ch.Ch_Status = ''a'' And  Ch.Ch_Seqno =';
  --lv_sentencia:=lv_sentencia||'(Select  /*+ rule */ Max(Ch_Seqno) From Contract_History Where Co_Id = Co.Co_Id) and ';
  --lv_sentencia:=lv_sentencia||'csc.co_id=co.co_id and ';
  --lv_sentencia:=lv_sentencia||'csc.cs_deactiv_date Is Null And dn.dn_id=csc.dn_id' ;


  lv_sentencia:='Insert /*+ append */ Into cc_coid_features Nologging (co_id,tmcode,telefono) '; 
  lv_sentencia:=lv_sentencia||'Select /*+ rule */  Co.Co_Id, Co.Tmcode,obtiene_telefono_dnc(dn.Dn_Num) ';  --[1468] JLI
  lv_sentencia:=lv_sentencia||'From Contract_All Co, Contract_History Ch, contr_services_cap csc, directory_number dn ';
  lv_sentencia:=lv_sentencia||' Where  Co.Tmcode =:1 and Co.Co_Id = Ch.Co_Id and Ch.Ch_Status = ''a'' And  Ch.Ch_Seqno =';
  lv_sentencia:=lv_sentencia||'(Select  /*+ rule */ Max(Ch_Seqno) From Contract_History Where Co_Id = Co.Co_Id) and ';
  lv_sentencia:=lv_sentencia||'csc.co_id=co.co_id and ';
  lv_sentencia:=lv_sentencia||'csc.cs_deactiv_date Is Null And dn.dn_id=csc.dn_id' ;
                          
  Begin
    Execute Immediate lv_sentencia Using pn_tmcode;
  End;
        
  Commit;       

           
  lr_cadena:=cadena();
  lr_co_id:=co_id();

  For i In (Select Co_Id From Cc_Coid_Features Where Cadena Is Null) Loop
    For j In features (i.co_id) Loop 
       lr_co_id.Extend;  
       lr_cadena.Extend;
       lr_co_id(lr_co_id.Last):=i.co_id;    
       lr_cadena(lr_cadena.Last):=j.sncode;
    End Loop;     
  End Loop;

  If lr_co_id.Count >0 Then
      Forall x In lr_co_id.First .. lr_co_id.Last 
         Update Cc_Coid_Features
            Set Cadena = cadena||'|'||lr_cadena(x),
                   tmcode=Pn_Tmcode    
          Where Co_Id = Lr_Co_Id(x);
   End If;   
      
  Commit;
  
  lv_sentencia:='Delete cc_coid_features where cadena is null';
  Begin
    Execute Immediate lv_sentencia;
  End;
  
  Commit;  
  
  Pv_Resultado := 'FINALIZADO';

Exception
  When Le_Error Then
    Pv_Resultado := 'CCP_OBTENER_FEATURES: ' || Lv_Error;
  When Others Then
    Pv_Resultado := 'CCP_OBTENER_FEATURES: ' || Substr(Sqlerrm, 1, 200);
  
End Ccp_Obtener_Features;
-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
Procedure Principal (pv_resultado Out Varchar2) Is


Cursor planes Is 
  Select Tmcode From Rateplan Where (Shdes Like 'GS%' Or Shdes Like 'TP%' ) ;---And tmcode=100 ;
lv_sentencia        Varchar2(200);
lv_error              Varchar2(250);
le_error              Exception;


Begin 
    ---Obtengo los features cobrables de AXIS.  
    lv_sentencia:='truncate table cc_tmp_sncode_fact';
    Begin 
      Execute Immediate lv_sentencia;
    Exception
      When Others Then 
        lv_error:=substr(Sqlerrm,1,200);      
        Raise Le_Error;        
    End;    
    
    Begin 
      Insert Into Cc_Tmp_Sncode_Fact
      Select Distinct Sncode From porta.Cc_Features_Cobrables@axis;
    Exception
      When Others Then 
        lv_error:=substr(Sqlerrm,1,200);      
        Raise Le_Error;            
    End;
    Commit;
    
    lv_sentencia:='truncate table cc_coid_features';
    Begin 
      Execute Immediate lv_sentencia;
    Exception
      When Others Then 
        lv_error:=substr(Sqlerrm,1,200);      
        Raise Le_Error;        
    End;        
    
    For i In planes Loop
        lv_error:=Null;
        cck_features_cobrables.Ccp_Obtener_Features (i.tmcode,lv_error);
        If lv_error<>'FINALIZADO' Then 
            lv_error:='TMCODE:'||i.tmcode||' '||lv_error;
            Raise le_error;
        End If;
    End Loop;

    lv_sentencia:='update cc_coid_features set cadena=cadena||''|''';
    Begin 
      Execute Immediate lv_sentencia;
    Exception
      When Others Then 
        lv_error:=substr(Sqlerrm,1,200);      
        Raise Le_Error;        
    End;     
    
    Commit;
    
    pv_resultado:='FINALIZADO';
    
Exception    
   When le_error Then
        pv_resultado:=lv_error;       
End;

end CCK_FEATURES_COBRABLES;
/

