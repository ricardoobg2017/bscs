create or replace package RPK_CARGO_FACTURA_FLEX is

/*
  TYPE RECORD_UDR IS RECORD(NUMBER_CALLING   VARCHAR2(20),
                            TIME_SUBMISSION  DATE, 
                            RATE_PLAN        VARCHAR2(15));
                            
  TYPE TNUMBER_CALLING IS TABLE OF VARCHAR2(20) INDEX BY BINARY_INTEGER;  
  TYPE TTIME_SUBMISSION IS TABLE OF DATE INDEX BY BINARY_INTEGER;  
  TYPE TRATE_PLAN IS TABLE OF VARCHAR2(15) INDEX BY BINARY_INTEGER;  */

  PROCEDURE FTU_GENERA_REPORTE_FLEX (PD_FECHA_INICIO      IN   DATE,
                                     PD_FECHA_FIN         IN   DATE,
                                     PV_TIPO_FLEX         IN   VARCHAR2,
                                     PV_USUARIO           IN   VARCHAR2,
                                     PV_FORZAR_EJECUCCION IN   VARCHAR2,                                    
                                     PV_ERROR             OUT  VARCHAR2 );
                                    
  
                               
  PROCEDURE RPP_REGISTRA_REPORTE(PV_ID_SERVICIO              VARCHAR2,
                                 PV_SNCODE                   VARCHAR2,
                                 PV_FEATURE                  VARCHAR2,
                                 PD_FECHA_OCC                DATE,
                                 PD_FECHA_NAVEGACION         DATE,
                                 PV_COSTO                    VARCHAR2,
--                                 PV_OBSERVACION              VARCHAR2,
                                 PV_OBSER_ACTUAL             VARCHAR2,
								                 PV_OBSER_DETAILS            VARCHAR2,
                                 PV_ESTADO                   VARCHAR2,
                                 PV_ERROR                OUT VARCHAR2);
                                 
  FUNCTION F_VALIDA_FECHA(PD_MES_INI   IN  DATE, 
                        PD_MES_FIN   IN  DATE, 
                        PV_TIPO_FLEX IN VARCHAR2,
                        PV_MENSAJE   OUT VARCHAR2) RETURN NUMBER;
                          
 
  FUNCTION F_VALIDA_FTU(PV_ID_SERVICIO    IN  VARCHAR2,
                      PN_CLIENTE        IN  NUMBER,
                      PD_FECHA_INI      IN   DATE,
                      PD_FECHA_FIN      IN   DATE,
                      PV_FEATURE        IN  VARCHAR2) RETURN NUMBER;
                        
                        
                      

  FUNCTION F_OBTIENE_FECHAS(PN_CLIENTE        IN  NUMBER,
                             PV_TIMESUBMISSION IN  VARCHAR2,
                             PD_FECHA_INI      OUT DATE,  
                             PD_FECHA_FIN      OUT DATE) RETURN NUMBER;                      
                      
  PROCEDURE P_GENERA_REPORTE (PV_TIPO_DATO     IN   VARCHAR2,
                            PV_FECHA_INI     IN   DATE,
                            PV_FECHA_FIN     IN   DATE,
                            PV_USUARIO       IN   VARCHAR2, --[9687] LPE - Mejora
                            PN_TOTAL_C       IN   NUMBER,
                            PN_TOTAL_F       IN   NUMBER, 
                            PN_ESTADO_SCP    IN   NUMBER,
                            PV_ERROR         OUT  VARCHAR2 );
                            
  FUNCTION F_BUSCAR_TEC_AXIS (PD_FECHA_CARGA        IN  DATE,
                            PV_ID_SERVICIO          IN  VARCHAR2,
                            PV_RATE_PLAN            IN  VARCHAR2,
                            PV_TIME_SUBMISSION      IN  VARCHAR2,
                            PN_CO_ID                IN  NUMBER,  
                            PN_CUSTOMER_ID          IN  NUMBER,
                            PN_SNCODE               IN  NUMBER,
                            PV_OBSER_TEC_DETAILS    OUT VARCHAR2,
                            PD_FECHA_INI            IN DATE, 
                            PD_FECHA_FIN            IN DATE,
                            PN_ID_DETALLE_PLAN      IN NUMBER,
                            PV_ID_SUBPRODUCTO       IN VARCHAR2) RETURN NUMBER;

                                                                                                                
end RPK_CARGO_FACTURA_FLEX;
/
CREATE OR REPLACE PACKAGE BODY RPK_CARGO_FACTURA_FLEX IS
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n:  GENERA REPORTE MENSUAL - DIARIO DE CONSUMO GPRS y por Adicionales
  --=====================================================================================--
  -- Desarrollado por:  SUD Laura Pe�a 
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa Carey
  -- Fecha de creaci�n: 05/05/2014
  -- Proyecto: [9687] SVA-FLEX Mejoras Procesos GSI - Generacion de Reporte GPRS
  --=====================================================================================--
  -- Modificado por:  SUD Laura Pe�a 
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa Carey
  -- Fecha de modificacion: 24/07/2014
  -- Proyecto: [9687] SVA-FLEX Mejoras Procesos GSI - Generacion de Reporte GPRS
  -- Objetivo: Se implementar� un parametro nuevo al Test de QC que ser� pv_forzar_ejecucion para permitir se procese el QC sin respetar previos cuadres. 
  -- Se implementa validacion de PV_TIPO_FLEX para generacion de reporte por este tipo 
  --=====================================================================================--
  -- Modificado por:  SUD Laura Pe�a 
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa Carey
  -- Fecha de modificacion: 31/07/2014
  -- Proyecto: [9687] Mejoras cierres de dblink
  --Objetivo: Mejoras cierres de dblink
  --=====================================================================================--
  -- Modificado por:  SUD Laura Pe�a 
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa Carey
  -- Fecha de modificacion: 05/08/2014
  -- Proyecto: [9687] SVA-FLEX Mejoras Procesos GSI - Generacion de Reporte GPRS
  --Objetivo: Descartar casos falsos por cambio de plan durante la fecha de corte del cliente y asi no generarle un doble cobro
  --=====================================================================================--

--Proceso principal para la generacion del qc cargos Flex

  GV_VALOR_PARAMETRO VARCHAR2(4):=NULL; --[9687] LPE - Definicion de variable para guardar el numero de dias para buscar en las tec_details historicas

PROCEDURE FTU_GENERA_REPORTE_FLEX (PD_FECHA_INICIO      IN   DATE,
                                   PD_FECHA_FIN         IN   DATE,
                                   PV_TIPO_FLEX         IN   VARCHAR2,
                                   PV_USUARIO           IN   VARCHAR2,
                                   PV_FORZAR_EJECUCCION IN   VARCHAR2, --[9687]LPE Nuevo parametro para reprocesos de reportes 
                                   PV_ERROR             OUT  VARCHAR2 ) IS
                                     
  --Declaracion de cursores
  CURSOR C_FEATURE_FLEX(CV_TIPO_FLEX VARCHAR2) IS
    SELECT ''''||T.ID_TIPO_DETALLE_SERV||'''' FEATURE
      FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXISREP T
     WHERE T.ID_TIPO_SVA_AMX = CV_TIPO_FLEX
       AND T.ID_CLASIFICACION_02 IS NOT NULL
       AND T.ID_CLASIFICACION_03 IS NOT NULL
       AND T.ESTADO = 'A';

  CURSOR C_OBTIENE_DATOS_AXIS (CV_IDSERVICIO VARCHAR2) IS
     SELECT 'X'
      FROM PORTA.CL_CONTRATOS@AXISREP T, PORTA.CL_SERVICIOS_CONTRATADOS@AXISREP P
     WHERE P.ID_CONTRATO = T.ID_CONTRATO
       AND P.ID_SERVICIO = CV_IDSERVICIO
       AND P.ESTADO = 'A'
       AND T.ESTADO='A';
   
  CURSOR C_OBTIENE_DATOS_BSCS (CV_CUENTA VARCHAR2) IS
     SELECT S.CUSTOMER_ID , C.CO_ID
       FROM SYSADM.CUSTOMER_ALL  S , SYSADM.CONTRACT_ALL C
      WHERE S.CUSTCODE =CV_CUENTA
        AND C.CUSTOMER_ID=S.CUSTOMER_ID;
       
 -- CURSOR PARA SACAR LA FECHA_OCC 
  CURSOR C_FECHA_OCC(CD_FECHA_INICIO DATE, 
                     CD_FECHA_FIN   DATE,
                     CV_RATE_PLAN   VARCHAR2,
                     CN_COID        NUMBER, 
                     CN_CUSTOMER_ID  NUMBER ) IS
    SELECT F.CUSTOMER_ID, F.VALID_FROM FECHA_OCC, F.SNCODE SNCODE, F.AMOUNT COSTO
      FROM FEES F--@BSCS.CONECEL.COM F
     WHERE F.VALID_FROM BETWEEN  CD_FECHA_INICIO AND CD_FECHA_FIN
       AND F.SNCODE=(SELECT T.ID_CLASIFICACION_02
                       FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXISREP T
                      WHERE T.ID_TIPO_DETALLE_SERV IN (CV_RATE_PLAN)
                        AND T.ESTADO = 'A')             
      AND F.CO_ID = NVL(CN_COID,F.CO_ID)
      AND F.CUSTOMER_ID= NVL(CN_CUSTOMER_ID,F.CUSTOMER_ID);
       
  CURSOR C_DETALLE (CV_FEATURE VARCHAR2) IS
     SELECT T.ID_CLASIFICACION_02 SNCODE,T.ID_CLASIFICACION_03 COSTO  
       FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXISREP T
      WHERE T.ID_TIPO_DETALLE_SERV =CV_FEATURE 
        AND T.ID_CLASIFICACION_02 IS NOT NULL
        AND T.ID_CLASIFICACION_03 IS NOT NULL;
  
  CURSOR C_TIPO_FLEX(CV_TIPO_FLEX VARCHAR2) IS               
   SELECT G.VALOR FROM PORTA.GV_PARAMETROS@AXISREP G
      WHERE G.ID_TIPO_PARAMETRO=9026
--        AND G.ID_PARAMETRO=CV_TIPO_FLEX
        AND G.NOMBRE = 'LN_FEATURES_FLEX'--'TIPO DE FLEX'
        AND G.VALOR= NVL(CV_TIPO_FLEX,G.VALOR); 
     
  CURSOR C_DATA_ORIGEN(CD_FECHA_INI         DATE,
                       CD_FECHA_FIN         DATE,
                       CN_MODO              NUMBER) IS
    SELECT to_char(CD_FECHA_INI,'YYYYMM') TABLA ,
           CD_FECHA_INI AS FECHA_INICIO, 
           CD_FECHA_FIN AS FECHA_FIN FROM DUAL
           WHERE CN_MODO=0
    UNION ALL        
    SELECT to_char(CD_FECHA_INI,'YYYYMM') TABLA ,
           CD_FECHA_INI AS FECHA_INICIO, 
           LAST_DAY(CD_FECHA_INI) AS FECHA_FIN FROM DUAL
           WHERE CN_MODO=1
    UNION ALL  
    SELECT TO_CHAR(CD_FECHA_FIN,'YYYYMM') TABLA , 
           TO_DATE('01/'||to_char(CD_FECHA_FIN,'mm/yyyy'),'DD/MM/YYYY') AS FECHA_INICIO , 
           CD_FECHA_FIN AS  FECHA_FIN  FROM DUAL
           WHERE CN_MODO=1;
       
  -- CURSOR SACA LOS REPORTE GENERADOS  
   CURSOR C_GENERADOS IS 
     SELECT * FROM RPT_REPORTE_CARGO_FLEX WHERE ESTADO='F';
   
   CURSOR C_OBTIENE_COID (CV_IDSERVICIO VARCHAR2, CD_TIME_SUBMISSION DATE) IS 
     SELECT T.ID_CONTRATO ,  P.CO_ID , T.CODIGO_DOC , P.ID_DETALLE_PLAN, P.ID_SUBPRODUCTO  --LPE obtengo el plan que al momento tenia el cliente durante su navegacion
      FROM PORTA.CL_CONTRATOS@AXISREP T, PORTA.CL_SERVICIOS_CONTRATADOS@AXISREP P
     WHERE P.ID_CONTRATO = T.ID_CONTRATO
       AND P.ID_SERVICIO = CV_IDSERVICIO
       AND CD_TIME_SUBMISSION BETWEEN P.FECHA_INICIO AND NVL(P.FECHA_FIN,CD_TIME_SUBMISSION+1);

   CURSOR C_VERIFICA_TECDETAILS (CV_IDSERVICIO VARCHAR2) IS 
     SELECT T.ID_CONTRATO ,  P.CO_ID , T.CODIGO_DOC 
      FROM PORTA.CL_CONTRATOS@AXISREP T, PORTA.CL_SERVICIOS_CONTRATADOS@AXISREP P
     WHERE P.ID_CONTRATO = T.ID_CONTRATO
       AND P.ID_SERVICIO = CV_IDSERVICIO
       AND P.FECHA_INICIO = (SELECT MAX(FECHA_INICIO) FROM CL_SERVICIOS_CONTRATADOS@AXISREP WHERE ID_SERVICIO = CV_IDSERVICIO);   

  LV_TRAMA_FEATURE      VARCHAR2(4000):=NULL;
  LE_ERROR				      EXCEPTION;
  LV_ERROR				      VARCHAR2(2000); --[9687] LPE - Mejora
  LD_FECHA_VALIDA		    DATE ;
  LD_FECHA_INICIO		    DATE := PD_FECHA_INICIO;
  LD_FECHA_FIN			    DATE := PD_FECHA_FIN;
  LV_USUARIO			      VARCHAR2(100):= userenv('terminal');
  LV_USUARIO_BASE		    VARCHAR2(100):= USER;
  LV_SENTENCIA			    VARCHAR2(2000):=NULL;  
  LV_TABLA				      VARCHAR2(50):= NULL;
  LV_TABLA_TECDETAILS      VARCHAR2(100):= NULL;
  TYPE ColaCurTyp       IS REF CURSOR;
  lc_cola               ColaCurTyp;
  LN_LONG_CAD           NUMBER;
  LV_FEATURE            VARCHAR2(500);
  LV_SQL				        VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LV_OBSERVACION		    VARCHAR2(2000); --[9687] LPE - Mejora
  LN_MODO				        NUMBER;
  LV_SNCODE				      VARCHAR2(15);
  LV_COSTO				      VARCHAR2(15);
  C_DETA_FEAT			      C_DETALLE%ROWTYPE;
 
  LC_FECHA_OCC			    C_FECHA_OCC%ROWTYPE;
 
  LN_CONT_CDR           NUMBER;
  LD_FECHA_OCC          DATE;
  LN_CODIGO             NUMBER;
  LV_ESTADO             VARCHAR2(1);
  --SCP BITACORA
  LV_ID_PROCESO         VARCHAR2(20):='FTU_CUADRES_GSIB';
  --LV_REFERENCIA_SCP       VARCHAR2(20):='CARGA_FEES';
  LV_REFERENCIA         VARCHAR2(30):='RPK_CARGO_FACTURA_FLEX';
  LN_ID_BITACORA_SCP    NUMBER;
  LN_ERROR_SCP          NUMBER;
  LV_ERROR_SCP          VARCHAR2(200);
  LN_ID_DETALLE_SCP     NUMBER;
  LT_NUMBER_CALLING     cot_string := cot_string();
  LT_TIME_SUBMISSION    cot_string := cot_string();
  LT_TRATE_PLAN         cot_string :=cot_string();
  LT_TFECHA_CARGA       cot_string :=cot_string();
  LV_ID_SERVICIO        VARCHAR2(12);  
  LC_OBTIENE_DATOS      C_OBTIENE_COID%ROWTYPE;
  LC_OBTIENE_DATOS_B    C_OBTIENE_DATOS_BSCS%ROWTYPE;
  LB_FOUND				      BOOLEAN;
  LN_EXISTE_FTU         NUMBER;
  ln_errores			      NUMBER:=0;
  LN_EXITOS				      NUMBER:=0;
  LN_CONCILIADOS		    NUMBER:=0;
  LN_PROCESADOS			    NUMBER:=0;
  LN_COMMIT				      NUMBER:=0;
  LV_TIPO_FLEX			    VARCHAR2(12):=UPPER(PV_TIPO_FLEX);--NULL;  
  LB_CONCILIAR			    BOOLEAN:= FALSE;    
  LV_PARAMETRO_COMMIT	  VARCHAR2(15):='QC_OCC_COMMIT';
  LN_VALOR_COMMIT		    NUMBER;
  LV_DESC_COMMIT		    VARCHAR2(100);
--  LN_TOTAL_C            NUMBER:=0;
--  LN_TOTAL_F            NUMBER:=0;
  LV_EXISTE             VARCHAR2(1);
  LV_OBSER_TEC_DETAILS  VARCHAR2(2000):=NULL; 
  LV_OBSER_ERROR        VARCHAR2(2000);
  LN_EXISTE_TEC_AXIS    NUMBER;
  LV_FLEX               VARCHAR2(200); 
  LV_VALOR              VARCHAR2(10):=NULL;  
  LN_EXISTE             NUMBER;
  LV_MENSAJE            VARCHAR2(100):=NULL;
  LN_RESULTADO          NUMBER:=0;
  LD_FECHA_CORTE_INI    DATE;
  LD_FECHA_CORTE_FIN    DATE;
  LD_FECHA_TIME_SUB     DATE;
  LN_CO_ID              NUMBER; 
  LV_FORZAR_EJECUCION  VARCHAR2(50):= UPPER(PV_FORZAR_EJECUCCION);
  LV_MENSAJE_FORZAR   VARCHAR2(20):=NULL;
  LV_PARAMETR_DIAS      VARCHAR2(50):='FTU_DIAS_TEC'; 
  LV_DESC_PARAMETRO     VARCHAR2(100); 
  
  BEGIN  
     
   
    BEGIN
        EXECUTE IMMEDIATE LV_SQL;
    EXCEPTION
        WHEN OTHERS THEN
           NULL;
    END;
   
     --[9687] INI LPE Obtiene el numero de dias maximos para recorrer las tec_details_trans_yyyymmdd
     SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => LV_PARAMETR_DIAS,
                                               pv_id_proceso   => LV_ID_PROCESO,
                                               pv_valor        => GV_VALOR_PARAMETRO,
                                               pv_descripcion  => LV_DESC_PARAMETRO,
                                               pn_error        => LN_ERROR_SCP,
                                               pv_error        => LV_ERROR_SCP);
     IF LN_ERROR_SCP = -1 THEN 
         GV_VALOR_PARAMETRO:=5;
     END IF;      
    --[9687] FIN LPE

    IF (PV_USUARIO IS NULL) THEN
         LV_ERROR:='Por favor, ingrese un nombre de Usuario';
         RAISE LE_ERROR;
    END IF;

    IF (LD_FECHA_INICIO IS NULL) THEN
         LV_ERROR:='No se ingreso el campo fecha inicio';
         RAISE LE_ERROR;
    ELSIF LD_FECHA_INICIO >= TRUNC(SYSDATE) THEN
         LV_ERROR:='La fecha de inicio ingresada es incorrecta. No se generara reportes con fechas de corte del dia actual';
         RAISE LE_ERROR;
    END IF;

    IF (LD_FECHA_FIN IS NULL) THEN
         LV_ERROR:='No se ingreso el campo fecha fin';
         RAISE LE_ERROR;
    ELSE
         LD_FECHA_VALIDA := ADD_MONTHS(LD_FECHA_INICIO,1);    
         IF LD_FECHA_FIN <= LD_FECHA_INICIO THEN   
             LV_ERROR:='Las fechas no pueden ser iguales. No se genero el reporte ('||LD_FECHA_INICIO||'-'||LD_FECHA_FIN||')';
             RAISE LE_ERROR;            
         ELSIF LD_FECHA_FIN NOT BETWEEN (LD_FECHA_INICIO + 1) AND LD_FECHA_VALIDA  THEN  
             LV_ERROR:='La fecha fin no es valida, no puede ser mayor a 30 dias ni menor a la fecha de inicio ';
             RAISE LE_ERROR;            
         END IF;         
    END IF;
    
    IF LV_TIPO_FLEX LIKE 'TODOS' THEN 
       LV_TIPO_FLEX:=NULL;
    ELSE 
      OPEN C_TIPO_FLEX (LV_TIPO_FLEX); --[9687] LPE - obtiene los diferentes tipo flex configurados
      LOOP 
      FETCH C_TIPO_FLEX INTO LV_VALOR;
      EXIT WHEN C_TIPO_FLEX%NOTFOUND;
      LV_FLEX := LV_FLEX||' , '||LV_VALOR;
      END LOOP;
      CLOSE C_TIPO_FLEX;       
      
      SELECT instr(LV_FLEX,LV_TIPO_FLEX) 
      INTO LN_EXISTE
      from dual;                
      
      IF LV_TIPO_FLEX IS NULL OR LN_EXISTE = 0 THEN
         LV_ERROR:='Debe ingresar uno de los siguientes TIPO FLEX : TODOS'||LV_FLEX;      
         RAISE LE_ERROR; 
      END IF;      
    END IF;
      
    SELECT DECODE(to_char(LD_FECHA_INICIO, 'YYYYMM'),
                  to_char(LD_FECHA_FIN, 'YYYYMM'),
                  0,
                  1)
                  INTO LN_MODO
    FROM DUAL;

    --[9687] INI LPE
    --Verifica si el proceso debe o no generar reportes ya ejecutados
    --'S' indica que se va ha forzar la ejecucion para realizar nuevamente un reporte
    --NULL indica valida si el reporte ya fue generado

    IF LV_FORZAR_EJECUCION <> 'S' THEN
         LV_ERROR:='Este parametro pv_forzar_ejecuccion debe ser S o NULL ' 
                   ||'(Si es S saltar� las reglas de control de QC ya realizados en fechas anteriores, '
                   ||'si es NULL las validaciones se mantienen)';
         RAISE LE_ERROR;
    ELSIF LV_FORZAR_EJECUCION = 'S' THEN
         LN_CODIGO:=1;
         LV_MENSAJE_FORZAR := 'FORZAR';
    ELSE
         --   Verificar si ya he realizado alguna ejecuccion  
         LN_CODIGO:=F_VALIDA_FECHA(Pd_MES_ini => LD_FECHA_INICIO, 
                                   Pd_MES_fin => LD_FECHA_FIN, 
                                   PV_TIPO_FLEX => PV_TIPO_FLEX, --[9687] LPE - Validacion reporte por tipo_flex
                                   PV_MENSAJE => LV_ERROR);          
    END IF;    
    --[9687] FIN LPE    

 --SI LN_CODIGO ES 0 NO SE GENERA REPORTE CON ESA FECHA
 --SI LN_CODIGO ES 1 SE GENERA EL REPORTE                            
    IF LN_CODIGO = 0 THEN 
--       PV_ERROR:=LV_ERROR; --[9687] LPE - Mejora
        RAISE LE_ERROR; 
    ELSE     
       DELETE FROM RPT_REPORTE_CARGO_FLEX;
       COMMIT;        
       
        FOR I IN C_TIPO_FLEX (LV_TIPO_FLEX) LOOP
                  FOR f IN C_FEATURE_FLEX(I.VALOR) LOOP
                    LV_TRAMA_FEATURE:= f.FEATURE||','||LV_TRAMA_FEATURE;
                  END LOOP;
        END LOOP;     

       
       IF LV_TRAMA_FEATURE IS NULL THEN
          LV_ERROR:='EL PARAMETRO TIPO FLEX NO ES EL CORRECTO';
          RAISE LE_ERROR; 
--          RETURN;
       END IF;
        
       LN_LONG_CAD:= LENGTH(lv_trama_feature);
       LV_FEATURE := SUBSTR(lv_trama_feature, 1, LN_LONG_CAD-1); 
          
	     SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO,
                                             LV_REFERENCIA,
                                             LV_USUARIO,
                                             LV_USUARIO_BASE,
                                             NULL,
                                             NULL,
                                             0,
                                             'SESIONES',
                                             LN_ID_BITACORA_SCP,
                                             LN_ERROR_SCP,
                                             LV_ERROR_SCP);   
					     
      --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS
       SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,  
                                             'INICIA PROCESO CUADRES ' ||LV_REFERENCIA||'- USUARIO_EJECUTA='||PV_USUARIO,  
                                             LV_MENSAJE_FORZAR,
                                             PV_TIPO_FLEX,
                                             0,              
                                             TO_CHAR(PD_FECHA_INICIO,'DD/MM/YYYY'),            
                                             TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY'),            
                                             TO_CHAR(LN_PROCESADOS),
                                             LN_EXITOS,
                                             LN_ERRORES,             
                                             LN_ID_DETALLE_SCP,            
                                             LN_ERROR_SCP,                   
                                             LV_ERROR_SCP);
            
       IF LN_ERROR_SCP <> 0 THEN
             LV_ERROR:=LV_ERROR_SCP;
             RAISE LE_ERROR; 
--           RETURN;
       END IF;  
      
       SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => LV_PARAMETRO_COMMIT,
                                               pv_id_proceso   => LV_ID_PROCESO,
                                               pv_valor        => LN_VALOR_COMMIT,
                                               pv_descripcion  => LV_DESC_COMMIT,
                                               pn_error        => LN_ERROR_SCP,
                                               pv_error        => LV_ERROR_SCP);
       IF LN_ERROR_SCP = -1 THEN 
           LN_VALOR_COMMIT:=1000;
       END IF;    

       --[9687]LPE Se extiende el campo de busqueda por fecha_carga y no por time_submission
       FOR N IN C_DATA_ORIGEN(LD_FECHA_INICIO,LD_FECHA_FIN,LN_MODO) LOOP
              LV_TABLA :='SMS.CDR_GPRS_OTROS_'||N.TABLA||'@COLECTOR.WORLD ';
              LV_SENTENCIA :='BEGIN ';
              LV_SENTENCIA:= LV_SENTENCIA ||' SELECT ';
              LV_SENTENCIA:= LV_SENTENCIA ||' DISTINCT TO_CHAR(I.TIME_SUBMISSION,''DD/MM/YYYY HH24:MI:SS''),';
              LV_SENTENCIA:= LV_SENTENCIA ||' NUMBER_CALLING, ';
              LV_SENTENCIA:= LV_SENTENCIA ||' I.RATE_PLAN, '; 
              LV_SENTENCIA:= LV_SENTENCIA ||' TO_CHAR(TRUNC(I.FECHA_CARGA),''DD/MM/YYYY'') BULK COLLECT INTO :1,:2,:3,:4 ';
              LV_SENTENCIA:= LV_SENTENCIA ||' FROM '|| LV_TABLA ||' I';
              LV_SENTENCIA:= LV_SENTENCIA ||' WHERE  I.RATE_PLAN IN ('||LV_FEATURE||')'; 
              LV_SENTENCIA:= LV_SENTENCIA ||' AND I.TRANSACTION_TYPE=598';
              LV_SENTENCIA:= LV_SENTENCIA ||' AND I.FECHA_CARGA BETWEEN TO_DATE('''||N.FECHA_INICIO||''',''DD/MM/YYYY HH24:MI:SS'')';
              LV_SENTENCIA:= LV_SENTENCIA ||'                           AND TO_DATE('''||N.FECHA_FIN||''',''DD/MM/YYYY HH24:MI:SS'') ';
              LV_SENTENCIA:= LV_SENTENCIA ||' AND FECHA_CARGA = ( SELECT MIN(FECHA_CARGA) ';
              LV_SENTENCIA:= LV_SENTENCIA ||'                       FROM '|| LV_TABLA ||' A';
              LV_SENTENCIA:= LV_SENTENCIA ||'   		             WHERE A.NUMBER_CALLING = I.NUMBER_CALLING ';
              LV_SENTENCIA:= LV_SENTENCIA ||' 		               AND A.RATE_PLAN = I.RATE_PLAN) ';
              LV_SENTENCIA:= LV_SENTENCIA ||'GROUP BY TIME_SUBMISSION , NUMBER_CALLING ,  RATE_PLAN, I.FECHA_CARGA;  END;';
              EXECUTE IMMEDIATE LV_SENTENCIA USING OUT LT_TIME_SUBMISSION , OUT  LT_NUMBER_CALLING, OUT LT_TRATE_PLAN, OUT LT_TFECHA_CARGA;             

             FOR A IN lt_NUMBER_CALLING.FIRST .. lt_NUMBER_CALLING.LAST LOOP              
                 LV_ID_SERVICIO:= OBTIENE_TELEFONO_DNC(LT_NUMBER_CALLING(A));
                 LN_COMMIT:= LN_COMMIT +1; 
                 LN_PROCESADOS:= LN_PROCESADOS +1; 
                 
                 OPEN C_OBTIENE_COID (LV_ID_SERVICIO,TO_DATE(LT_TIME_SUBMISSION(A),'DD/MM/YYYY HH24:MI:SS'));
                 FETCH C_OBTIENE_COID INTO LC_OBTIENE_DATOS;
                 LB_FOUND:=  C_OBTIENE_COID%NOTFOUND;
                 CLOSE C_OBTIENE_COID;
                 
                  IF LB_FOUND THEN
                       LV_OBSERVACION:='CLIENTE NO ESTA ACTIVO EN AXIS';   
                  END IF;                 
                 
                 -- SACA DATOS DE AXIS PARA CONCILIAR
                 OPEN C_DETALLE(lt_TRATE_PLAN(A));
                 FETCH C_DETALLE INTO C_DETA_FEAT;
                 CLOSE C_DETALLE;
                 
                  OPEN C_OBTIENE_DATOS_BSCS (LC_OBTIENE_DATOS.CODIGO_DOC);
                  FETCH C_OBTIENE_DATOS_BSCS INTO LC_OBTIENE_DATOS_B;
                  LB_FOUND:=  C_OBTIENE_DATOS_BSCS%NOTFOUND;
                  CLOSE C_OBTIENE_DATOS_BSCS;
                                 
                  IF LB_FOUND THEN
                      LV_OBSERVACION:=LV_OBSERVACION||' - CLIENTE NO EXISTE EN BSCS';  
                 
                  END IF;
                  IF NOT LB_FOUND AND LC_OBTIENE_DATOS_B.CUSTOMER_ID IS NOT NULL THEN
                  LN_RESULTADO:=  F_OBTIENE_FECHAS(LC_OBTIENE_DATOS_B.CUSTOMER_ID ,
  								                                lt_TIME_SUBMISSION(A),
 								                                  LD_FECHA_CORTE_INI,  
								                                  LD_FECHA_CORTE_FIN);
                  END IF;
                  
                 LD_FECHA_TIME_SUB:=TRUNC(TO_DATE(lt_TIME_SUBMISSION(A),'DD/MM/YYYY HH24:MI:SS'));                 

                --[9687] LPE Se cambia validacion para que busque por el co_id de axis y no de bscs 
                 LN_CO_ID:= LC_OBTIENE_DATOS.CO_ID;                               
                 IF LC_OBTIENE_DATOS.CO_ID <> LC_OBTIENE_DATOS_B.CO_ID THEN
                        LN_CO_ID:= LC_OBTIENE_DATOS.CO_ID;
                 ELSIF LC_OBTIENE_DATOS.CO_ID IS NULL AND LC_OBTIENE_DATOS_B.CO_ID IS NOT NULL THEN
                        LN_CO_ID:= LC_OBTIENE_DATOS_B.CO_ID;                 
                 END IF;  

             /*
                 IF LC_OBTIENE_DATOS.CO_ID IS NULL AND LC_OBTIENE_DATOS_B.CO_ID IS NOT NULL 
                    OR LC_OBTIENE_DATOS.CO_ID <> LC_OBTIENE_DATOS_B.CO_ID THEN 
                    LN_CO_ID:=LC_OBTIENE_DATOS_B.CO_ID;
                 END IF;
              */   
              
                IF  LN_CO_ID IS NOT NULL OR LC_OBTIENE_DATOS_B.CUSTOMER_ID IS NOT NULL THEN                 
                 OPEN  C_FECHA_OCC(NVL(LD_FECHA_CORTE_INI,LD_FECHA_TIME_SUB), 
                                   NVL(LD_FECHA_CORTE_FIN,LD_FECHA_TIME_SUB), 
                                   lt_TRATE_PLAN(A),LN_CO_ID,
                                   LC_OBTIENE_DATOS_B.CUSTOMER_ID);
                 FETCH C_FECHA_OCC INTO LC_FECHA_OCC;
                 LB_FOUND :=   C_FECHA_OCC%NOTFOUND;
                 CLOSE C_FECHA_OCC;               
                 
                ELSE
                   LB_FOUND := TRUE;
                END IF;
               
                 IF LB_FOUND THEN
                   
                    LV_ESTADO:='C'; 
                  
                    LN_EXISTE_TEC_AXIS:=RPK_CARGO_FACTURA_FLEX.F_BUSCAR_TEC_AXIS(PD_FECHA_CARGA        => TO_DATE(LT_TFECHA_CARGA(A),'dd/mm/yyyy'),
                                                                                  PV_ID_SERVICIO       => LV_ID_SERVICIO,
                                                                                  PV_RATE_PLAN         => lt_TRATE_PLAN(A),
                                                                                  PV_TIME_SUBMISSION   => lt_TIME_SUBMISSION(A),
                                                                                  PN_CO_ID             => LN_CO_ID,  
                                                                                  PN_CUSTOMER_ID       => LC_OBTIENE_DATOS_B.CUSTOMER_ID,
                                                                                  PN_SNCODE            => C_DETA_FEAT.SNCODE,
                                                                                  PV_OBSER_TEC_DETAILS => LV_OBSER_TEC_DETAILS,
                                                                                  PD_FECHA_INI         => LD_FECHA_CORTE_INI, --LPE para descartar casos falsos por cambio de plan
                                                                                  PD_FECHA_FIN         => LD_FECHA_CORTE_FIN,  --LPE
                                                                                  PN_ID_DETALLE_PLAN   => LC_OBTIENE_DATOS.ID_DETALLE_PLAN,
                                                                                  PV_ID_SUBPRODUCTO    => LC_OBTIENE_DATOS.ID_SUBPRODUCTO); 
                       --opcion2
                    IF LV_OBSER_TEC_DETAILS IS NOT NULL THEN
                      null;
                    END IF;                                                                                  
                                                                                  
                    LN_EXISTE_FTU:=RPK_CARGO_FACTURA_FLEX.F_VALIDA_FTU(PV_ID_SERVICIO => OBTIENE_TELEFONO_DNC_INT(LV_ID_SERVICIO),
                                                                    PN_CLIENTE => LC_OBTIENE_DATOS_B.CUSTOMER_ID,
                                                                    PD_FECHA_INI =>LD_FECHA_CORTE_INI,
                                                                    PD_FECHA_FIN =>LD_FECHA_CORTE_FIN,
                                                                    PV_FEATURE => LT_TRATE_PLAN(A)); 
                                          
                        IF LN_EXISTE_FTU = 1 THEN  
                           LV_ESTADO:='W';
                           
                        END IF;
                           LN_ERRORES:=LN_ERRORES +1 ; --[9687] LPE total descuadres 
                           LN_CONCILIADOS := LN_CONCILIADOS +1 ; 
                           LV_SNCODE:=C_DETA_FEAT.SNCODE;
                           LD_FECHA_OCC:=to_date(lt_TIME_SUBMISSION(A),'dd/mm/yyyy hh24:mi:ss'); --TAB_DET_Servi(A).TIME_SUBMISSION;
                           LV_COSTO:=C_DETA_FEAT.COSTO;
--                           LN_TOTAL_C:=LN_TOTAL_C+1;
                           LN_EXISTE_FTU :=NULL;                   
                    
                 ELSE
                         
                       LN_EXITOS:= LN_EXITOS +1 ;  
                       LV_SNCODE:=LC_FECHA_OCC.SNCODE;
                       LD_FECHA_OCC:=LC_FECHA_OCC.FECHA_OCC;
                       LV_COSTO:= LC_FECHA_OCC.COSTO;
                       LV_ESTADO:='F';
                       LV_OBSERVACION:='Cobro realizado con exito';    
--                       LN_TOTAL_F:=LN_TOTAL_F+1;
                   
                 END IF;                                
                 ---PROCEDIMIENTO DE INGRESOO
                 RPP_REGISTRA_REPORTE(PV_ID_SERVICIO       => OBTIENE_TELEFONO_DNC_INT(LV_ID_SERVICIO),       
                                      PV_SNCODE            => LV_SNCODE,
                                      PV_FEATURE           => lt_TRATE_PLAN(A),
                                      PD_FECHA_OCC         => LD_FECHA_OCC,
                                      PD_FECHA_NAVEGACION  => to_date(lt_TIME_SUBMISSION(A),'dd/mm/yyyy hh24:mi:ss'),
                                      PV_COSTO             => LV_COSTO,
                                      PV_OBSER_ACTUAL      => LV_OBSERVACION,        
                                      PV_OBSER_DETAILS     => LV_OBSER_TEC_DETAILS,  
                                      PV_ESTADO            => LV_ESTADO,
                                      PV_ERROR             => LV_ERROR);

                 IF LV_ERROR IS NOT NULL THEN
                     LV_ERROR:='Error al registrar en  la estructura:' ||LV_ERROR;
                     RAISE LE_ERROR; 
                 END IF; 
                   
                 IF LN_COMMIT = LN_VALOR_COMMIT  THEN
                    LN_COMMIT:=0;
                    COMMIT;
                 END IF;  
                 LV_OBSER_TEC_DETAILS:= null; 
                 LV_OBSERVACION:= NULL;
                 LV_ESTADO:=NULL; 
                 LV_COSTO:=NULL; 
                 LD_FECHA_OCC:=NULL;  
                 LV_SNCODE:=NULL;     
                 LV_OBSER_ERROR:=NULL;   
             END LOOP;
          
       END LOOP;
       COMMIT;  
       --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS       
       SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,  
                                             'CALCULA ' ||LV_REFERENCIA||'- USUARIO_EJECUTA='||PV_USUARIO,  
                                             LV_MENSAJE_FORZAR,
                                             PV_TIPO_FLEX,
                                             0,              
                                             TO_CHAR(PD_FECHA_INICIO,'DD/MM/YYYY'),            
                                             TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY'),            
                                             TO_CHAR(LN_PROCESADOS),
                                             LN_EXITOS,
                                             LN_ERRORES,             
                                             LN_ID_DETALLE_SCP,            
                                             LN_ERROR_SCP,                   
                                             LV_ERROR_SCP);
            
      IF LN_ERROR_SCP <> 0 THEN
           LV_ERROR:=LV_ERROR_SCP;
           RAISE LE_ERROR;        
--           RETURN;
      END IF;                                          
       
      RPK_CARGO_FACTURA_FLEX.P_GENERA_REPORTE(PV_TIPO_DATO   => LV_TIPO_FLEX,
                                                PV_FECHA_INI   => LD_FECHA_INICIO,
                                                PV_FECHA_FIN   => LD_FECHA_FIN,
                                                PV_USUARIO     => PV_USUARIO||' - '||LV_USUARIO||' - '||LV_USUARIO_BASE,
                                                PN_TOTAL_C     => LN_ERRORES,  --LN_TOTAL_C,
                                                PN_TOTAL_F     => LN_EXITOS,   --LN_TOTAL_F, 
                                                PN_ESTADO_SCP  => LN_CODIGO,
                                                PV_ERROR       => LV_ERROR);     
      IF LV_ERROR IS NOT NULL THEN
        LV_ERROR:= 'EMAIL NO ENVIADO CON EXITO '||LV_ERROR; --[9687] LPE Mejora presentacion de mensaje de error
        RAISE LE_ERROR;
      END IF;  

       --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS             
       SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,  
                                             'EMAIL ENVIADO CON EXITO'||'- USUARIO_EJECUTA='||PV_USUARIO,  
                                             LV_MENSAJE_FORZAR,                                                  
                                             PV_TIPO_FLEX,
                                             0,
                                             TO_CHAR(PD_FECHA_INICIO,'DD/MM/YYYY'),            
                                             TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY'),            
                                             TO_CHAR(LN_PROCESADOS),                                             
                                             LN_EXITOS,
                                             LN_ERRORES,             
                                             LN_ID_DETALLE_SCP,            
                                             LN_ERROR_SCP,                   
                                             LV_ERROR_SCP);
            
       IF LN_ERROR_SCP <> 0 THEN
           LV_ERROR:= LV_ERROR_SCP;
           RAISE LE_ERROR;         
--           RETURN;
       END IF; 
       
          --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS              
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,  
                                             'FIN PROCESO CUADRES ' ||LV_REFERENCIA||'- USUARIO_EJECUTA='||PV_USUARIO,  
                                             LV_MENSAJE_FORZAR,
                                             PV_TIPO_FLEX,
                                             0,              
                                             TO_CHAR(PD_FECHA_INICIO,'DD/MM/YYYY'),            
                                             TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY'),            
                                             TO_CHAR(LN_PROCESADOS),
                                             LN_EXITOS,
                                             LN_ERRORES,             
                                             LN_ID_DETALLE_SCP,            
                                             LN_ERROR_SCP,                   
                                             LV_ERROR_SCP);
            
       IF LN_ERROR_SCP <> 0 THEN
           LV_ERROR:= LV_ERROR_SCP;
           RAISE LE_ERROR;                  
--           RETURN;
       END IF;                                       
       

       --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS                            
       SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(PN_ID_BITACORA          => LN_ID_BITACORA_SCP,
                                             PN_REGISTROS_PROCESADOS => LN_PROCESADOS,
                                             PN_REGISTROS_ERROR      => 0,
                                             PN_ERROR                => LN_ERROR_SCP,
                                             PV_ERROR                => LV_ERROR_SCP);
   
       --------------------------------------------------------------------
                -- Termin� el proceso
       --------------------------------------------------------------------
       SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => LN_ID_BITACORA_SCP,
                                             pn_error       => ln_error_scp,
                                             pv_error       => lv_error_scp);
               --------------------------------------------------------------------
       
       
    END IF;      
    
    IF LD_FECHA_INICIO <= (SYSDATE - 40) THEN 
         LV_MENSAJE:='ALERTA USTED HA REALIZADO UN REPORTE DE MAS DE 40 D�AS ATR�S DE LA FECHA ACTUAL.';   
    END IF;
    
    pv_error:='QC TERMINADO EXITOSAMENTE. '||LV_MENSAJE;                 

    BEGIN
          DBMS_SESSION.CLOSE_DATABASE_LINK('AXISREP'); 
          DBMS_SESSION.CLOSE_DATABASE_LINK('COLECTOR.WORLD');     
          COMMIT; 
    EXCEPTION
       WHEN OTHERS THEN
           NULL;
    END;
    COMMIT;    

  EXCEPTION
     WHEN LE_ERROR THEN
        PV_ERROR := LV_ERROR;
           --[9687]LPE Mejoras en la bitacorizacion PARA SACAR ESTADISTICAS       
           SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,  
                                             'FIN ' ||LV_REFERENCIA || '- USUARIO_EJECUTA='||PV_USUARIO,  
                                             'Error: '||PV_ERROR,     
                                             PV_TIPO_FLEX,
                                             0,              
                                             TO_CHAR(PD_FECHA_INICIO,'DD/MM/YYYY'),            
                                             TO_CHAR(PD_FECHA_FIN,'DD/MM/YYYY'),            
                                             TO_CHAR(LN_PROCESADOS),
                                             LN_EXITOS,
                                             LN_ERRORES,                 
                                             LN_ID_DETALLE_SCP,            
                                             LN_ERROR_SCP,                   
                                             LV_ERROR_SCP);
        
          SCP.SCK_API.scp_bitacora_procesos_act(pn_id_bitacora          => LN_ID_BITACORA_SCP,
                                             pn_registros_procesados => ln_exitos,
                                             pn_registros_error      => 1,
                                             pn_error                => ln_error_scp,
                                             pv_error                => lv_error_scp);
      
       --------------------------------------------------------------------
                -- Termin� el proceso
       --------------------------------------------------------------------
       SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => LN_ID_BITACORA_SCP,
                                             pn_error       => ln_error_scp,
                                             pv_error       => lv_error_scp);
               --------------------------------------------------------------------

     BEGIN               
        DBMS_SESSION.CLOSE_DATABASE_LINK('COLECTOR.WORLD');                 
        DBMS_SESSION.CLOSE_DATABASE_LINK('AXISREP'); 
        COMMIT;  
     EXCEPTION
        WHEN OTHERS THEN
           NULL;
     END; 
     COMMIT;  
       
     WHEN OTHERS THEN     
          SCP.SCK_API.scp_bitacora_procesos_act(pn_id_bitacora          => LN_ID_BITACORA_SCP,
                                             pn_registros_procesados => ln_exitos,
                                             pn_registros_error      => 1,
                                             pn_error                => ln_error_scp,
                                             pv_error                => lv_error_scp);
        
       --------------------------------------------------------------------
                -- Termin� el proceso
       --------------------------------------------------------------------
       SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => LN_ID_BITACORA_SCP,
                                             pn_error       => ln_error_scp,
                                             pv_error       => lv_error_scp);
               --------------------------------------------------------------------
 
        BEGIN               
          DBMS_SESSION.CLOSE_DATABASE_LINK('COLECTOR.WORLD');                    
          DBMS_SESSION.CLOSE_DATABASE_LINK('AXISREP'); 
          COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
           NULL;
        END; 
        COMMIT;        
        pv_error:= 'error- '||LV_ID_SERVICIO||' - '||sqlerrm; 
          
  END FTU_GENERA_REPORTE_FLEX; 


  --Proceso se encarga de ingresar los datos procesados en la rpt_repote_cargo_flex
  --los datos serviran para formar el reporte de cargos FLEX
  
  PROCEDURE RPP_REGISTRA_REPORTE(PV_ID_SERVICIO              VARCHAR2,
                                 PV_SNCODE                   VARCHAR2,
                                 PV_FEATURE                  VARCHAR2,
                                 PD_FECHA_OCC                DATE,
                                 PD_FECHA_NAVEGACION         DATE,
                                 PV_COSTO                    VARCHAR2,
                                 PV_OBSER_ACTUAL             VARCHAR2,
								                 PV_OBSER_DETAILS            VARCHAR2,
                                 PV_ESTADO                   VARCHAR2,
                                 PV_ERROR                OUT VARCHAR2) IS
  LV_SENTENCIA_2         VARCHAR2(1000);
  
  BEGIN
    
  
        LV_SENTENCIA_2:= 'insert into RPT_REPORTE_CARGO_FLEX';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'(ID_SERVICIO,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'SNCODE,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'FEATURE,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'FECHA_OCC,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'FECHA_NAVEGACION,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'COSTO,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'ESTADO,';        
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'OBSERVACION_ACTUAL,';  
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'OBSERVACION_ANTERIOR)'; 
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'values ';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||'(:1,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :2,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :3,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :4,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :5,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :6,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :7,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :8,';
        LV_SENTENCIA_2:= LV_SENTENCIA_2 ||' :9)';
        
        EXECUTE IMMEDIATE LV_SENTENCIA_2 USING  IN PV_ID_SERVICIO ,                                    
                                                IN PV_SNCODE,          
                                                IN PV_FEATURE,         
                                                IN PD_FECHA_OCC,       
                                                IN PD_FECHA_NAVEGACION,
                                                IN PV_COSTO,           
                                                IN PV_ESTADO,                                                
												                        IN PV_OBSER_ACTUAL, 
											                         	IN PV_OBSER_DETAILS; 
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR:=PV_ID_SERVICIO ||' -- '|| SQLERRM;
  END RPP_REGISTRA_REPORTE;
  
--Funcion que se encarga de validar si existen reportes generados con los rangos de fecha ingresados

FUNCTION F_VALIDA_FECHA(PD_MES_INI   IN  DATE, 
                        PD_MES_FIN   IN  DATE, 
                        PV_TIPO_FLEX IN VARCHAR2, --[9687]LPE Buscar reportes por tipo flex
                        PV_MENSAJE   OUT VARCHAR2) RETURN NUMBER IS
 
CURSOR C_FECHA IS
--9687 LPE SE OBTIENEN DATOS DE BITACORA PARA PRESENTACION DE MENSAJE POR VALIDACION DE FECHAS
--SELECT N.COD_AUX_2, N.COD_AUX_3, N.ID_BITACORA, O.USUARIO_SO, O.USUARIO_BD ,N.COD_AUX_1, O.FECHA_ACTUALIZACION
SELECT N.COD_AUX_1, N.COD_AUX_2, N.ID_BITACORA, O.USUARIO_SO, O.USUARIO_BD , 
       substr(N.MENSAJE_APLICACION,instr(N.MENSAJE_APLICACION,'=')+1) USUARIO, O.FECHA_ACTUALIZACION
  FROM SCP.SCP_DETALLES_BITACORA N,  SCP.SCP_BITACORA_PROCESOS O
 WHERE N.ID_BITACORA=O.ID_BITACORA
   AND O.ID_PROCESO = 'FTU_CUADRES_GSIB' AND ESTADO='F'                      
   AND SUBSTR(N.COD_AUX_1,4,2)  =  SUBSTR(TO_CHAR(PD_MES_INI,'DD/MM/YYYY'),4,2)           
   AND SUBSTR(N.COD_AUX_2,4,2)  =  SUBSTR(TO_CHAR(PD_MES_FIN,'DD/MM/YYYY'),4,2)
   AND N.MENSAJE_ACCION = PV_TIPO_FLEX;

  LD_FECHA_INI            DATE;
  LD_FECHA_FIN            DATE;
  ln_exito                number:=1;
  LV_SQL                  VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';

BEGIN
     BEGIN
          EXECUTE IMMEDIATE LV_SQL;
     EXCEPTION
          WHEN OTHERS THEN
            NULL;
     END;
	 
     FOR N IN C_FECHA LOOP
        LD_FECHA_INI:= TO_DATE(N.COD_AUX_1,'DD/MM/YYYY');
        LD_FECHA_FIN:= TO_DATE(N.COD_AUX_2,'DD/MM/YYYY'); 
         
         IF Pd_MES_ini BETWEEN LD_FECHA_INI AND LD_FECHA_FIN or 
            Pd_MES_fin between LD_FECHA_INI and LD_FECHA_FIN THEN 
           --[9687]LPE Mejora en la presentacion de mensaje por error
           PV_MENSAJE:='YA HA SIDO GENERADO UN REPORTE TIPO_FLEX '|| PV_TIPO_FLEX ||'  POR '
                       ||N.USUARIO||' CON FECHA DE CORTE ('||N.COD_AUX_1||' - '||N.COD_AUX_2 ||') EL DIA '||N.FECHA_ACTUALIZACION || '. SELECCIONE OTRAS FECHAS' ;
           LN_EXITO:=0;
           return ln_exito;
         END IF; 
     END LOOP;
     
   return LN_EXITO;
  
END F_VALIDA_FECHA;

--Funcion que se encarga de controlar que exista un solo cliente ya procesado
FUNCTION F_VALIDA_FTU(PV_ID_SERVICIO    IN  VARCHAR2,
                      PN_CLIENTE        IN  NUMBER,
                      PD_FECHA_INI      IN   DATE,
                      PD_FECHA_FIN      IN   DATE,
                      PV_FEATURE        IN  VARCHAR2) RETURN NUMBER IS
 
   
 CURSOR C_VERIFICO_REPORTE(CV_ID_SERVICIO VARCHAR2,CV_FEATURE VARCHAR2 ) IS   
   SELECT A.FECHA_NAVEGACION
     FROM RPT_REPORTE_CARGO_FLEX A
    WHERE A.ID_SERVICIO = CV_ID_SERVICIO 
      AND A.FEATURE=CV_FEATURE
      AND A.ESTADO = 'C'
      AND FECHA_NAVEGACION = (SELECT MAX(B.FECHA_NAVEGACION)
                                FROM RPT_REPORTE_CARGO_FLEX B
                               WHERE B.ID_SERVICIO = CV_ID_SERVICIO 
                                 AND A.FEATURE=CV_FEATURE
                                 AND B.ESTADO = 'C');
   
  LD_FECHA_NAVEGA     DATE;   
  LD_FECHA_INI_ANT    DATE;
  LD_FECHA_FIN_ANT    DATE;  
  LN_EXITO            NUMBER:=0;
  LV_SQL              VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LB_FOUND            BOOLEAN;
  LN_EXISTE           NUMBER:=0;
  LN_EXISTE_ANT       NUMBER:=0;
  LN_RESULTADO        NUMBER:=0;

BEGIN

   BEGIN
        EXECUTE IMMEDIATE LV_SQL;
   EXCEPTION
        WHEN OTHERS THEN
          NULL;
   END;         
    
           OPEN C_VERIFICO_REPORTE (PV_ID_SERVICIO, PV_FEATURE);
           FETCH C_VERIFICO_REPORTE INTO LD_FECHA_NAVEGA;
           LB_FOUND:= C_VERIFICO_REPORTE%FOUND;
           CLOSE C_VERIFICO_REPORTE;                    

           IF LB_FOUND THEN 
               LN_EXISTE_ANT:=  F_OBTIENE_FECHAS(PN_CLIENTE ,
											                           LD_FECHA_NAVEGA,
												                         LD_FECHA_INI_ANT,  
												                         LD_FECHA_FIN_ANT);
                                          
               IF NVL(PD_FECHA_INI,LD_FECHA_INI_ANT)  = LD_FECHA_INI_ANT AND 
                    NVL(PD_FECHA_FIN,LD_FECHA_FIN_ANT) = LD_FECHA_FIN_ANT THEN
                    LN_EXITO:=1;
               END IF;
           END IF;  
      
   
                       
   return LN_EXITO;
   
   EXCEPTION 
      WHEN OTHERS THEN
          LN_EXITO:=0; 
          
  
end F_VALIDA_FTU;

--Funcion que obtine las fechas de inicio y fin segun el ciclo del cliente 
FUNCTION F_OBTIENE_FECHAS(PN_CLIENTE        IN  NUMBER,
                          PV_TIMESUBMISSION IN  VARCHAR2,
                          PD_FECHA_INI      OUT DATE,  
                          PD_FECHA_FIN      OUT DATE) RETURN NUMBER IS
 
   --OBTIENE DIA DE CICLO   
 CURSOR C_CICLO(CN_CUSTOMER_ID NUMBER) IS   
 SELECT A.ID_CICLO, A.DIA_INI_CICLO
   FROM FA_CICLOS_BSCS A
  WHERE ID_CICLO =( SELECT ID_CICLO
   FROM FA_CICLOS_AXIS_BSCS A
  WHERE A.ID_CICLO_ADMIN = (SELECT BILLCYCLE
							                FROM CUSTOMER_ALL
					                   WHERE CUSTOMER_ID = CN_CUSTOMER_ID));
          
  LC_CICLO                C_CICLO%ROWTYPE; 
  LV_FECHA_CICLO          VARCHAR2(15);
  LV_DIA                  VARCHAR2(2);
  LN_EXITO                NUMBER:=1;
  LV_SQL				          VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';

BEGIN
  
     BEGIN
          EXECUTE IMMEDIATE LV_SQL;
     EXCEPTION
          WHEN OTHERS THEN
            NULL;
     END;
    
     OPEN C_CICLO(PN_CLIENTE);
     FETCH C_CICLO INTO LC_CICLO;
     CLOSE C_CICLO; 
                       
     SELECT LC_CICLO.DIA_INI_CICLO || '/' ||
            TO_CHAR(TO_DATE(PV_TIMESUBMISSION,
                            'DD/MM/YYYY HH24:MI:SS'),
                    'MM/YYYY')
     INTO LV_FECHA_CICLO
     FROM DUAL;
                       
                       LV_DIA:=TO_CHAR(TO_DATE(PV_TIMESUBMISSION,'DD/MM/YYYY HH24:MI:SS'),'DD');

                       IF LV_DIA < LC_CICLO.DIA_INI_CICLO  THEN 
                              PD_FECHA_INI:=ADD_MONTHS(TO_DATE(LV_FECHA_CICLO,'DD/MM/YYYY'),-1);
                              PD_FECHA_FIN:=TO_DATE(LV_FECHA_CICLO,'DD/MM/YYYY') -1 ; 
                       ELSE
                              PD_FECHA_INI:=TO_dATE(LV_FECHA_CICLO,'DD/MM/YYYY');
                              PD_FECHA_FIN:=ADD_MONTHS(TO_DATE(LV_FECHA_CICLO,'DD/MM/YYYY'),1)-1;    
                       END IF;                       
                       
   return LN_EXITO;
   
  EXCEPTION 
    WHEN OTHERS THEN
      LN_EXITO:=0;   
  
end F_OBTIENE_FECHAS;

--Proceso que lanza la ejecucion del procesos que generara el reporte final y lo enviara por correo a GSI  
PROCEDURE P_GENERA_REPORTE (PV_TIPO_DATO     IN   VARCHAR2,
                            PV_FECHA_INI     IN   DATE,
                            PV_FECHA_FIN     IN   DATE,
                            PV_USUARIO       IN   VARCHAR2,
                            PN_TOTAL_C       IN   NUMBER,
                            PN_TOTAL_F       IN   NUMBER, 
                            PN_ESTADO_SCP    IN   NUMBER,
                            PV_ERROR         OUT  VARCHAR2 )IS
 
 CURSOR C_ESTADO IS
   SELECT 'F' ESTADO FROM DUAL
   UNION
   SELECT 'C' ESTADO FROM DUAL;

  LV_NOMBRE_ARCHIVO      VARCHAR2(60);
  LV_SQL				         VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LV_SUBJECT			       VARCHAR2(200);
  LV_COMANDO             VARCHAR2(1000);
  LV_SALIDA              VARCHAR2(1000):=NULL;
  LE_ERROR				       EXCEPTION;
  LV_ERROR               VARCHAR2(1000);
  LV_OBSERVACION         VARCHAR2(200);
  LN_TOTAL               NUMBER:=0;
  LV_TIPO_DATO           VARCHAR2(6);
  TOTAL_PROCESADOS       NUMBER;
BEGIN
  
   BEGIN
       EXECUTE IMMEDIATE LV_SQL;
   EXCEPTION
       WHEN OTHERS THEN
            NULL;
   END;
   LV_TIPO_DATO:= PV_TIPO_DATO;
   LV_SUBJECT := '=: REPORTE DE CARGOS FLEX:';
   TOTAL_PROCESADOS:=PN_TOTAL_F + PN_TOTAL_C;
   
  
   
   IF PV_TIPO_DATO IS NULL THEN
      LV_TIPO_DATO:='TODOS';
   END IF;
   
   FOR E IN C_ESTADO LOOP
          IF E.ESTADO = 'F' THEN
             LN_TOTAL:=PN_TOTAL_F;
             --[9687] LPE Cambio en el mensaje de presentacion del correo cuando se envia archivos por cuadres 
             --Cambio en presentacion de mensaje por correo 
--              LV_OBSERVACION:='''HAY CUADRES EN LOS CARGOS DE PLANES FLEX. EXISTEN '||PN_TOTAL_F||' QUE SE LES GENERO OCC'''; 
             LV_OBSERVACION:='''EXISTEN '|| PN_TOTAL_F || ' DE ' ||TOTAL_PROCESADOS||' GENERADO OCC CORRECTAMENTE (ADJUNTO ARCHIVO CON DETALLES)''';                
          ELSIF E.ESTADO = 'C' THEN
             LN_TOTAL:= PN_TOTAL_C; 
             --[9687] LPE cambio en el mensaje de presentacion del correo cuando se envia archivos por descuadres
--              LV_OBSERVACION:='''HAY DESCUADRES EN LOS CARGOS DE PLANES FLEX. EXISTEN '||PN_TOTAL_C||' QUE NO SE LES GENERO OCC'''; 
             LV_OBSERVACION:='''EXISTEN '|| PN_TOTAL_C ||' DE '||TOTAL_PROCESADOS||' CARGOS DE PLANES FLEX NO REALIZADOS. (ADJUNTO ARCHIVO CON DETALLES)''';
          END IF;
          
          LV_COMANDO := 'sh /bscs/bscsprod/cargosReportesFLex/flex_genera_archivo_cuadres.sh '
                        ||E.ESTADO|| ' ' ||LV_TIPO_DATO || ' ' || PV_FECHA_INI|| ' ' ||
                        PV_FECHA_FIN || ' ' || '''' ||PV_USUARIO||''''||' ' ||TOTAL_PROCESADOS|| ' ' || 
                        LV_OBSERVACION || ' ' ||PN_ESTADO_SCP;
          --Proceso de genero reporte y envio correo
          OSUTIL.RUNOSCMD(LV_COMANDO, LV_SALIDA);
          LV_COMANDO := '';  
          
          
             
          --Control de Error
--          IF LV_SALIDA LIKE '%Error%' THEN
          IF LV_SALIDA LIKE '%Error%' or LV_SALIDA LIKE '%ADVERTENCIA%' THEN
             LV_ERROR:=LV_ERROR||' | '||LV_SALIDA;
--             RAISE LE_ERROR;
          END IF;
          LV_SALIDA:=null;
   END LOOP;
   IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;

  EXCEPTION
      WHEN LE_ERROR THEN
        PV_ERROR := LV_ERROR; --[9687]LPE Mejora
     WHEN OTHERS THEN     
       pv_error:= 'error- '||sqlerrm; 
  
end P_GENERA_REPORTE;


FUNCTION F_BUSCAR_TEC_AXIS (PD_FECHA_CARGA          IN  DATE,
                            PV_ID_SERVICIO          IN  VARCHAR2,
                            PV_RATE_PLAN            IN  VARCHAR2,
                            PV_TIME_SUBMISSION      IN  VARCHAR2,
                            PN_CO_ID                IN  NUMBER, --[9687] LPE Usados para buscar errores bitacorizados en la OCC_CARGA_HIST  
                            PN_CUSTOMER_ID          IN  NUMBER, --[9687] LPE Usados para buscar errores bitacorizados en la OCC_CARGA_HIST
                            PN_SNCODE               IN  NUMBER, --[9687] LPE Usados para buscar errores bitacorizados en la OCC_CARGA_HIST
                            PV_OBSER_TEC_DETAILS    OUT VARCHAR2,
                            PD_FECHA_INI            IN DATE, 
                            PD_FECHA_FIN            IN DATE,
                            PN_ID_DETALLE_PLAN      IN NUMBER,
                            PV_ID_SUBPRODUCTO       IN VARCHAR2) RETURN NUMBER IS

 
  CURSOR C_DATA_ORIGEN(CD_FECHA_INI         DATE,
                       CD_FECHA_FIN         DATE,
                       CN_MODO              NUMBER) IS
    SELECT to_char(CD_FECHA_INI,'YYYYMM') TABLA ,
           TO_CHAR(CD_FECHA_INI,'DD') AS DIA_INICIO, 
           TO_CHAR(CD_FECHA_FIN,'DD') AS DIA_FIN FROM DUAL
           WHERE CN_MODO=0
    UNION ALL        
    SELECT to_char(CD_FECHA_INI,'YYYYMM') TABLA ,
           TO_CHAR(CD_FECHA_INI,'DD') AS DIA_INICIO, 
           TO_CHAR(LAST_DAY(CD_FECHA_INI),'DD') AS DIA_FIN FROM DUAL
           WHERE CN_MODO=1
    UNION ALL  
    SELECT TO_CHAR(CD_FECHA_FIN,'YYYYMM') TABLA , 
           TO_CHAR(TO_DATE('01/'||to_char(CD_FECHA_FIN,'mm/yyyy'),'DD/MM/YYYY'),'DD') AS DIA_INICIO, 
           TO_CHAR(CD_FECHA_FIN,'DD') AS  DIA_FIN  FROM DUAL
           WHERE CN_MODO=1;

   --[9687] LPE Obtiene el mensaje de error por lo cual no se cargo el cobro en la fees
  CURSOR OCC_CARGA (CV_ID_SERVICIO VARCHAR2,
                    CN_CO_ID NUMBER,
                    CN_CUSTOMER_ID NUMBER,
                    CN_SNCODE NUMBER, 
                    LD_TIME_SUBMISSION VARCHAR2) IS 
      SELECT A.TMCODE, NVL(A.ESTADO,'SIN ESTADO') || '-' || NVL(A.ERROR,'SIN DESCRIPCION DE ERROR')  OBSERVACION
        FROM OCC_CARGA_HIST A
       WHERE A.DN_NUM      = CV_ID_SERVICIO
         AND A.CO_ID       = CN_CO_ID
         AND A.CUSTOMER_ID = CN_CUSTOMER_ID
         AND A.SNCODE      = CN_SNCODE
         AND A.VALID_FROM  = TRUNC(TO_DATE(LD_TIME_SUBMISSION, 'DD/MM/YYYY HH24:MI:SS'))
         and rownum <2;
--          ORDER BY FECHA_REGISTRO DESC ;

   --[9687] Verificar si tiene un feature que no corresponde al plan
  CURSOR C_VERIFICO_CLIENTE (CN_TMCODE NUMBER,
                    CN_SNCODE NUMBER) IS 
      SELECT 'X'
        FROM BS_PLANES@AXISREP S, GE_DETALLES_PLANES@AXISREP P, 
             CL_SERVICIOS_PLANES@AXISREP PL, CL_TIPOS_DETALLES_SERVICIOS@AXISREP T
       WHERE S.COD_BSCS IN (SELECT TMCODE FROM MPULKTMB A WHERE  TMCODE = CN_TMCODE )
         AND P.ID_DETALLE_PLAN = S.ID_DETALLE_PLAN
         AND PL.ID_DETALLE_PLAN = P.ID_DETALLE_PLAN
         AND T.ID_TIPO_DETALLE_SERV = PL.ID_TIPO_DETALLE_SERV
         AND  T.ID_TIPO_DETALLE_SERV IN (SELECT X.ID_TIPO_DETALLE_SERV 
                                           FROM CL_TIPOS_DETALLES_SERVICIOS@AXISREP X 
                                          WHERE X.ID_CLASIFICACION_02 = CN_SNCODE);

  --[9687] INI LPE Verifico si hubo cambio de plan para identificar cobros falsos
  CURSOR C_VERIFICO_CAMBIOPLAN (CV_ID_SERVICIO VARCHAR2,CD_FECHA_INI DATE ,CD_FECHA_FIN DATE, 
                                CN_ID_DETALLE_PLAN NUMBER,CV_ID_SUBPRODUCTO VARCHAR2) IS   
      SELECT COUNT(*) CONT, A.ID_SUBPRODUCTO, A.ID_DETALLE_PLAN
        FROM CL_SERVICIOS_CONTRATADOS@AXISREP A
       WHERE A.ID_SERVICIO=CV_ID_SERVICIO
         AND TRUNC(A.FECHA_INICIO) BETWEEN CD_FECHA_INI AND CD_FECHA_FIN
         AND (A.ID_DETALLE_PLAN <> CN_ID_DETALLE_PLAN OR A.ID_SUBPRODUCTO  <> CV_ID_SUBPRODUCTO)
    GROUP BY ID_DETALLE_PLAN, ID_SUBPRODUCTO;
  LC_CAMBIO_PLAN      C_VERIFICO_CAMBIOPLAN%ROWTYPE;     
          
  LD_FECHA_INI            DATE;
  LD_FECHA_FIN            DATE;
  LV_FECHA_CICLO          VARCHAR2(15);
  LV_DIA                  VARCHAR2(2);
  LN_EXITO                NUMBER:=1;
  LV_SQL				          VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LN_MODO                 NUMBER;
  LV_TABLA_TECDETAILS     VARCHAR2(100):= NULL;
  LV_SENTENCIA			      VARCHAR2(2000):=NULL;  
  LV_OBSER_TEC_DETAILS    VARCHAR2(2000):=NULL; 
  LV_OBSER_ERROR          VARCHAR2(2000);
  LV_ESTADO               VARCHAR2(1);  
  LC_OBSERVACION_OCC      OCC_CARGA%ROWTYPE; 
  LV_ID_SERVICIO          VARCHAR2(12);
  LN_ERROR_SCP            NUMBER;
  LV_ID_PROCESO           VARCHAR2(50):='FTU_CUADRES_GSIB';
  LV_PARAMETRO            VARCHAR2(50):='FTU_DIAS_TEC'; 
  LV_VALOR_PARAMETRO      VARCHAR2(4);
  LV_DESC_PARAMETRO       VARCHAR2(100); 
  LB_FOUND				  BOOLEAN;
  LN_EXISTE               NUMBER;
  LV_EXISTE               VARCHAR2(1);
  
  
BEGIN

    BEGIN
        EXECUTE IMMEDIATE LV_SQL;
    EXCEPTION
        WHEN OTHERS THEN
           NULL;
    END;

   LV_VALOR_PARAMETRO:=GV_VALOR_PARAMETRO;
   LD_FECHA_INI:= PD_FECHA_CARGA;
   LD_FECHA_FIN:= LD_FECHA_INI + TO_NUMBER(LV_VALOR_PARAMETRO);
   LV_ID_SERVICIO:= OBTIENE_TELEFONO_DNC_INT(PV_ID_SERVICIO);
   
   SELECT DECODE(to_char(PD_FECHA_CARGA, 'YYYYMM'),
                  to_char(LD_FECHA_FIN, 'YYYYMM'),
                  0,
                  1)
                  INTO LN_MODO
    FROM DUAL;
                         
       FOR N IN C_DATA_ORIGEN(LD_FECHA_INI,LD_FECHA_FIN,LN_MODO) LOOP  
            FOR R IN N.DIA_INICIO .. N.DIA_FIN LOOP   
               IF R < 10 THEN
                  LV_DIA:='0'||R;
               ELSE    
                  LV_DIA:=R;
               END IF;                            
                                                             
                BEGIN                             
                     LV_TABLA_TECDETAILS:='PORTA.TEC_DETAILS_TRANS_'||N.TABLA||LV_DIA||'@AXISREP ';
                     LV_SENTENCIA:= ' SELECT ESTADO , NVL(ESTADO,''SIN ESTADO'') || ''-'' || NVL(ERROR,''SIN DESCRIPCION DE ERROR'')  LV_ERROR ';
                     LV_SENTENCIA:= LV_SENTENCIA ||' FROM '||LV_TABLA_TECDETAILS||' A';
                     LV_SENTENCIA:= LV_SENTENCIA ||' WHERE A.ID_SERVICIO = '''||PV_ID_SERVICIO||'''';   
                     LV_SENTENCIA:= LV_SENTENCIA ||' AND A.RATE_PLAN = '''||PV_RATE_PLAN||'''';     
                     LV_SENTENCIA:= LV_SENTENCIA ||' AND A.TIME_SUBMISSION =  TO_DATE('''||PV_TIME_SUBMISSION||''',''DD/MM/YYYY HH24:MI:SS'')';
                                      
                     EXECUTE IMMEDIATE LV_SENTENCIA 
                     INTO LV_ESTADO, LV_OBSER_ERROR;
                     LV_OBSER_TEC_DETAILS:=LV_OBSER_ERROR;   
                        
                     IF LV_OBSER_TEC_DETAILS IS NOT NULL THEN
	                 --[9687] INI LPE en el caso de encontrarse el servicio en las tec_details busco si hubo errores en la fees
                         IF LV_ESTADO = 'F' THEN 
                             OPEN  OCC_CARGA(LV_ID_SERVICIO,PN_CO_ID,PN_CUSTOMER_ID,PN_SNCODE,PV_TIME_SUBMISSION );
                             FETCH OCC_CARGA INTO LC_OBSERVACION_OCC;
                             LB_FOUND:=  OCC_CARGA%FOUND;
                             CLOSE OCC_CARGA;
                             LV_OBSER_TEC_DETAILS:= LV_OBSER_TEC_DETAILS||' - '||nvl(LC_OBSERVACION_OCC.OBSERVACION,' No existe en OCC_CARGA_HIST');
                             IF LB_FOUND THEN
                                    --Verificar si tiene un feature que no corresponde al plan
                                  OPEN C_VERIFICO_CLIENTE(LC_OBSERVACION_OCC.TMCODE,PN_SNCODE);
                                  FETCH C_VERIFICO_CLIENTE INTO LV_EXISTE;
                                  CLOSE C_VERIFICO_CLIENTE;
                                  IF LV_EXISTE IS NULL THEN 
                                  --[9687] INI LPE Verifico si tuvo cambio de plan o producto para descartar casos falsos y no generarle un doble cobro
                                  OPEN C_VERIFICO_CAMBIOPLAN(PV_ID_SERVICIO,PD_FECHA_INI,PD_FECHA_FIN,PN_ID_DETALLE_PLAN,PV_ID_SUBPRODUCTO);
                                  FETCH C_VERIFICO_CAMBIOPLAN INTO LC_CAMBIO_PLAN;
                                  CLOSE C_VERIFICO_CAMBIOPLAN;
                                  IF NVL(LC_CAMBIO_PLAN.CONT,0) >= 1 THEN 
                                        LV_OBSER_TEC_DETAILS:='CASO FALSO NO CONCILIAR - CAMBIO DE PLAN DENTRO DE SU FECHA_CORTE. PLAN ANTERIOR '||PN_ID_DETALLE_PLAN ||' PLAN NUEVO '||LC_CAMBIO_PLAN.ID_DETALLE_PLAN;
                                  END IF;                                       
                                  END IF;                                         
                                  --[9687] FIN LPE Verifico si tuvo cambio de plan                                  
                             END IF;
                         END IF; 
	                 --[9687] FIN LPE
                         PV_OBSER_TEC_DETAILS:=LV_OBSER_TEC_DETAILS;                         
                         RETURN LN_EXITO;
                     END IF;  
                                                   
               EXCEPTION
                   WHEN OTHERS THEN
                        LV_OBSER_TEC_DETAILS:='ERROR AL VERIFICAR EN TEC_DETAILS '||SUBSTR(SQLERRM,1,35); --(SQLERRM,1,35);                                                       
               END;  
            END LOOP;   
       END LOOP;
   
   PV_OBSER_TEC_DETAILS:=LV_OBSER_TEC_DETAILS;
   return LN_EXITO;
   
  EXCEPTION 
    WHEN OTHERS THEN
      PV_OBSER_TEC_DETAILS:='ERROR'||SUBSTR(SQLERRM,1,35);
      LN_EXITO:=0;   
  
end F_BUSCAR_TEC_AXIS;

end RPK_CARGO_FACTURA_FLEX;
/
