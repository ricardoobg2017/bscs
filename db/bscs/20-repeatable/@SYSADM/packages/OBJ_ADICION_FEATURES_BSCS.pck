CREATE OR REPLACE PACKAGE OBJ_ADICION_FEATURES_BSCS IS
--===============================================================================================================================================
--===================== Creado por                             :  CLS Rony Tamayo                                                              ==     
--===================== Lider CLS                              :  Ing. Sheyla Ramirez                                                          ==
--===================== Lider Claro SIS                        :  Jackeline Gomez                                                              ==
--===================== Fecha de creacion                      :  02-Marzo-2016                                                            ==
--Proyecto [10705] CREAR FORMA PARA ACTUALIZACION FEATURES(INGRESAR OCC Y MPULKMT1) : Adicion de features con planes en tabla MPULKMT1 y MPULKMB
--===============================================================================================================================================
   

PROCEDURE P_INSERTAR_PLAN_TMP(PV_ID_PLAN IN VARCHAR2,
                         PV_USUARIO IN VARCHAR2,
                         PV_ERROR OUT VARCHAR2);
                                   
PROCEDURE P_UPDATE_PLAN_TMP(PV_ID_PLAN IN VARCHAR2,
                       PV_ID_DETALLE_PLAN IN VARCHAR2,
                       PV_TMCODE IN VARCHAR2,
                       PV_FECHA_MAX IN VARCHAR2,
                       PV_VSCODE_MAX IN VARCHAR2,     
                       PV_ERROR OUT VARCHAR2);
PROCEDURE P_UPDATE_PLAN_PROCESADOS_TMP(PV_TMCODE IN VARCHAR2,PV_VALOR_PROCESADO IN VARCHAR2,PV_USUARIO IN VARCHAR2,PV_ERROR OUT VARCHAR2);
                               
                                 
PROCEDURE P_INSERTAR_FEAT_OCC_TMP(PV_SNCODE IN VARCHAR2,
                             PV_CUENTA_CONTABLE IN VARCHAR2,
                             PV_IMPUESTO IN VARCHAR2,
                             PV_USUARIO IN VARCHAR2,
                             PV_OBSERVACION IN VARCHAR2,
                             PV_CUENTA_CONTABLE_ING IN VARCHAR2,
                             PV_IMPUESTO_ING IN VARCHAR2,
                             PV_VALOR_OMISION IN VARCHAR2,
                             PV_ERROR OUT VARCHAR2);
                                       
PROCEDURE P_UPDATE_OBS_FEAT_OCC_TMP(PV_SNCODE IN VARCHAR2,
                             PV_OBSERVACION IN VARCHAR2,
                             PV_USUARIO IN VARCHAR2,                                     
                             PV_ERROR OUT VARCHAR2); 
                                        
PROCEDURE P_INSERTAR_FEAT_PLAN_MK1(pn_tmcode           integer,
                              pn_vscode            integer,
                              pd_vsdate          date,
                              pv_status          varchar2,
                              pn_spcode          integer,
                              pn_sncode          integer,
                              pf_subscript        float,
                              pf_accessfee        float,
                              pf_event            float,
                              pv_echind          varchar2,
                              pv_amtind          varchar2,
                              pv_frqind          varchar2,
                              pv_srvind          varchar2,
                              pv_proind          varchar2,
                              pv_advind          varchar2,
                              pv_susind          varchar2,
                              pn_ltcode          integer,
                              pn_plcode          integer,
                              pn_billfreq        integer,
                              pn_freedays        integer,
                              pv_accglcode       varchar2,
                              pv_subglcode        varchar2,
                              pv_usgglcode        varchar2,
                              pn_accjcid          integer,
                              pn_usgjcid          integer,
                              pn_subjcid          integer,
                              pv_csind            varchar2,
                              pn_clcode          integer,
                              pv_accserv_catcode  varchar2,
                              pv_accserv_code    varchar2,
                              pv_accserv_type    varchar2,
                              pv_usgserv_catcode  varchar2,
                              pv_usgserv_code    varchar2,
                              pv_usgserv_type    varchar2,
                              pv_subserv_catcode  varchar2,
                              pv_subserv_code    varchar2,
                              pv_subserv_type    varchar2,
                              pf_deposit          float,
                              pv_interval_type    varchar2,
                              pn_interval        integer,
                              pv_subglcode_disc  varchar2,
                              pv_accglcode_disc  varchar2,
                              pv_usgglcode_disc  varchar2,
                              pv_subglcode_mincom  varchar2,
                              pv_accglcode_mincom  varchar2,
                              pv_usgglcode_mincom  varchar2,
                              pn_subjcid_disc	    integer,
                              pn_accjcid_disc	    integer,
                              pn_usgjcid_disc	    integer,
                              pn_subjcid_mincom	  integer,
                              pn_accjcid_mincom	  integer,
                              pn_usgjcid_mincom	  integer,
                              pn_combi_id	        integer,
                              pv_prm_print_ind	    varchar2,
                              pv_printsubscrind	  varchar2,
                              pv_printaccessind	  varchar2,
                              pn_rec_version	      integer,
                              pv_prepaid_service_ind	varchar2,
                              PV_ERROR OUT VARCHAR2
                              );
                                        
PROCEDURE P_INSERTAR_FEAT_PLAN_MKTMB(PN_TMCODE                      integer,
                             PN_VSCODE                      integer ,
                             PD_VSDATE                      date,
                             PV_STATUS                      varchar2,             
                             PN_SPCODE                      integer,
                             PN_SNCODE                      integer,
                             PV_SRVIND                      varchar2,
                             PV_ECHIND                      varchar2,             
                             PV_AMTIND                      varchar2,
                             PV_FRQIND                      varchar2,
                             PF_SUBSCRIPT                   float,             
                             PF_ACCESSFEE                   float,
                             PF_EVENT                       float,
                             PV_PROIND                      varchar2,
                             PV_ADVIND                      varchar2,             
                             PV_SUSIND                      varchar2,
                             PV_SUBGLCODE                   varchar2,
                             PV_ACCGLCODE                   varchar2,
                             PV_USGGLCODE                   varchar2,             
                             PV_SUBGLCODE_DISC              varchar2,
                             PV_ACCGLCODE_DISC              varchar2,
                             PV_USGGLCODE_DISC              varchar2,
                             PV_SUBGLCODE_MINCOM            varchar2,             
                             PV_ACCGLCODE_MINCOM            varchar2,
                             PV_USGGLCODE_MINCOM            varchar2,
                             PN_SUBJCID                     integer,
                             PN_ACCJCID                     integer,             
                             PN_USGJCID                     integer,
                             PN_SUBJCID_DISC                integer,
                             PN_ACCJCID_DISC                integer,
                             PN_USGJCID_DISC                integer,             
                             PN_SUBJCID_MINCOM              integer,
                             PN_ACCJCID_MINCOM              integer,
                             PN_USGJCID_MINCOM              integer,
                             PV_SUBSERV_CATCODE             varchar2,             
                             PV_SUBSERV_CODE                varchar2,
                             PV_SUBSERV_TYPE                varchar2,
                             PV_ACCSERV_CATCODE             varchar2,
                             PV_ACCSERV_CODE                varchar2,             
                             PV_ACCSERV_TYPE                varchar2,
                             PV_USGSERV_CATCODE             varchar2,
                             PV_USGSERV_CODE                varchar2,
                             PV_USGSERV_TYPE                varchar2,             
                             PV_CSIND                       varchar2,
                             PN_CLCODE                      integer,
                             PF_DEPOSIT                     float,
                             PV_INTERVAL_TYPE               varchar2,             
                             PN_INTERVAL                    integer,
                             PN_COMBI_ID                    integer,
                             PV_PRM_PRINT_IND               varchar2,
                             PV_PRINTSUBSCRIND              varchar2,             
                             PV_PRINTACCESSIND              varchar2,
                             PC_PREPAID_SERVICE_IND         char,
                             PN_REC_VERSION                integer,
                             PV_ERROR                    OUT VARCHAR2);                                 
  /*                                                                       
---1M
PROCEDURE P_INSERTA_PLAN_1M_TMP(PV_id_plan	varchar2,
                        PV_nombre_axis	varchar2,
                        PV_nombre_bscs	varchar2,
                        PI_tmcode	integer,
                        PN_id_detalle_plan	number,
                        PV_usuario	varchar2,
                        PV_ERROR OUT VARCHAR2); */
   
PROCEDURE P_INSERTA_FEAT_1M_TMP(PI_sncode	           integer,
                          PF_accessfee	       float,--COSTO
                          PV_usuario	         varchar2,                                 
                          PV_PRORRATEBLES      varchar2, 
                          PV_OBSERVACION       varchar2,
                          PF_COSTO_ING            VARCHAR2, 
                          PV_PRORRATEABLE_ING     VARCHAR2,
                          PV_VALOR_OMISION        IN VARCHAR2,
                          PV_ERROR             OUT VARCHAR2);
                                    
PROCEDURE P_UPDATE_TMCODE_1M_TMP(PI_sncode	           integer,
                           PV_usuario	           varchar2,
                           PI_TMCODE_PLANTILLA   integer,
                           PV_ERROR              OUT VARCHAR2); 
                                    
                                    
PROCEDURE P_INSERTA_LOG_FEAT_BSCS(PV_sncode varchar2,
                            PV_tmcode varchar2,
                            PV_observacion varchar2,
                            PV_usuario varchar2,
                            PD_fecha_registro DATE,
                            PV_tipo_feature varchar2,
                            PV_ESTADO_OBSERVACION varchar2,
                            PV_ERROR OUT VARCHAR2);  
                                                                      
PROCEDURE P_UPDATE_LOG_OBSERVACION_FEAT(PV_sncode varchar2,
                            PV_tmcode varchar2,
                            PV_observacion varchar2,
                            PV_usuario varchar2,
                            PV_ESTADO_OBSERVACION varchar2,                                    
                            PV_ERROR OUT VARCHAR2); 


END OBJ_ADICION_FEATURES_BSCS;
/
CREATE OR REPLACE PACKAGE BODY OBJ_ADICION_FEATURES_BSCS IS
--===============================================================================================================================================
--===================== Creado por                             :  CLS Rony Tamayo                                                              ==     
--===================== Lider CLS                              :  Ing. Sheyla Ramirez                                                          ==
--===================== Lider Claro SIS                        :  Jackeline Gomez                                                              ==
--===================== Fecha de creacion                      :  02-Marzo-2016                                                            ==
--Proyecto [10705] CREAR FORMA PARA ACTUALIZACION FEATURES(INGRESAR OCC Y MPULKMT1) : Adicion de features con planes en tabla MPULKMT1 y MPULKMB
--===============================================================================================================================================
 
PROCEDURE P_INSERTAR_PLAN_TMP(PV_ID_PLAN IN VARCHAR2,
                      PV_USUARIO IN VARCHAR2,
                      PV_ERROR   OUT VARCHAR2) IS
BEGIN
INSERT INTO SYSADM.GSI_ASOCIA_PLANES
(ID_PLAN, USUARIO)
VALUES
(PV_ID_PLAN, PV_USUARIO);
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
      
END P_INSERTAR_PLAN_TMP;

    
----------------------
PROCEDURE P_UPDATE_PLAN_TMP(PV_ID_PLAN         IN VARCHAR2,
                      PV_ID_DETALLE_PLAN IN VARCHAR2,
                      PV_TMCODE          IN VARCHAR2,
                      PV_FECHA_MAX       IN VARCHAR2,
                      PV_VSCODE_MAX      IN VARCHAR2,
                      PV_ERROR           OUT VARCHAR2) IS
BEGIN
update SYSADM.GSI_ASOCIA_PLANES
set id_detalle_plan = PV_ID_DETALLE_PLAN,
   tmcode          = PV_TMCODE,
   fecha_max       = PV_FECHA_MAX,
   version_max     = PV_VSCODE_MAX
where id_plan = PV_ID_PLAN;
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
END P_UPDATE_PLAN_TMP;
          
-------------------------      
          
PROCEDURE P_UPDATE_PLAN_PROCESADOS_TMP(PV_TMCODE          IN VARCHAR2,
                               PV_VALOR_PROCESADO IN VARCHAR2,
                               PV_USUARIO         IN VARCHAR2,
                               PV_ERROR           OUT VARCHAR2) IS
  
BEGIN
update SYSADM.GSI_ASOCIA_PLANES
set procesados = PV_VALOR_PROCESADO
where tmcode = PV_TMCODE
and usuario = PV_USUARIO;
  
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
END P_UPDATE_PLAN_PROCESADOS_TMP;
    
 
    
PROCEDURE P_INSERTAR_FEAT_OCC_TMP(PV_SNCODE              IN VARCHAR2,
                          PV_CUENTA_CONTABLE     IN VARCHAR2,
                          PV_IMPUESTO            IN VARCHAR2,
                          PV_USUARIO             IN VARCHAR2,
                          PV_OBSERVACION         IN VARCHAR2,
                          PV_CUENTA_CONTABLE_ING IN VARCHAR2,
                          PV_IMPUESTO_ING        IN VARCHAR2,
                          PV_VALOR_OMISION       IN VARCHAR2,
                          PV_ERROR               OUT VARCHAR2) IS
BEGIN
  
INSERT INTO SYSADM.GSI_OCCS_A_AGREGAR
(SNCODE,
CUENTA_CONTABLE,
IMPUESTO,
USUARIO,
OBSERVACION,
CUENTA_CONTABLE_ING,
IMPUESTO_ING,
VALOR_OMISION)
VALUES
(PV_SNCODE,
PV_CUENTA_CONTABLE,
PV_IMPUESTO,
PV_USUARIO,
PV_OBSERVACION,
PV_CUENTA_CONTABLE_ING,
PV_IMPUESTO_ING,
PV_VALOR_OMISION);
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    
END P_INSERTAR_FEAT_OCC_TMP;




PROCEDURE P_UPDATE_OBS_FEAT_OCC_TMP(PV_SNCODE      IN VARCHAR2,
                            PV_OBSERVACION IN VARCHAR2,
                            PV_USUARIO     IN VARCHAR2,
                            PV_ERROR       OUT VARCHAR2) IS
BEGIN
  
-- INSERT INTO SYSADM.GSI_OCCS_A_AGREGAR(SNCODE,CUENTA_CONTABLE,IMPUESTO,USUARIO,OBSERVACION,CUENTA_CONTABLE_ING,IMPUESTO_ING) VALUES (PV_SNCODE,PV_CUENTA_CONTABLE,PV_IMPUESTO,PV_USUARIO,PV_OBSERVACION,PV_CUENTA_CONTABLE_ING,PV_IMPUESTO_ING);
  
UPDATE SYSADM.GSI_OCCS_A_AGREGAR
SET OBSERVACION = PV_OBSERVACION
WHERE SNCODE = PV_SNCODE
AND USUARIO = PV_USUARIO;
  
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    
END P_UPDATE_OBS_FEAT_OCC_TMP;

PROCEDURE P_INSERTAR_FEAT_PLAN_MK1(pn_tmcode              integer,
                           pn_vscode              integer,
                           pd_vsdate              date,
                           pv_status              varchar2,
                           pn_spcode              integer,
                           pn_sncode              integer,
                           pf_subscript           float,
                           pf_accessfee           float,
                           pf_event               float,
                           pv_echind              varchar2,
                           pv_amtind              varchar2,
                           pv_frqind              varchar2,
                           pv_srvind              varchar2,
                           pv_proind              varchar2,
                           pv_advind              varchar2,
                           pv_susind              varchar2,
                           pn_ltcode              integer,
                           pn_plcode              integer,
                           pn_billfreq            integer,
                           pn_freedays            integer,
                           pv_accglcode           varchar2,
                           pv_subglcode           varchar2,
                           pv_usgglcode           varchar2,
                           pn_accjcid             integer,
                           pn_usgjcid             integer,
                           pn_subjcid             integer,
                           pv_csind               varchar2,
                           pn_clcode              integer,
                           pv_accserv_catcode     varchar2,
                           pv_accserv_code        varchar2,
                           pv_accserv_type        varchar2,
                           pv_usgserv_catcode     varchar2,
                           pv_usgserv_code        varchar2,
                           pv_usgserv_type        varchar2,
                           pv_subserv_catcode     varchar2,
                           pv_subserv_code        varchar2,
                           pv_subserv_type        varchar2,
                           pf_deposit             float,
                           pv_interval_type       varchar2,
                           pn_interval            integer,
                           pv_subglcode_disc      varchar2,
                           pv_accglcode_disc      varchar2,
                           pv_usgglcode_disc      varchar2,
                           pv_subglcode_mincom    varchar2,
                           pv_accglcode_mincom    varchar2,
                           pv_usgglcode_mincom    varchar2,
                           pn_subjcid_disc        integer,
                           pn_accjcid_disc        integer,
                           pn_usgjcid_disc        integer,
                           pn_subjcid_mincom      integer,
                           pn_accjcid_mincom      integer,
                           pn_usgjcid_mincom      integer,
                           pn_combi_id            integer,
                           pv_prm_print_ind       varchar2,
                           pv_printsubscrind      varchar2,
                           pv_printaccessind      varchar2,
                           pn_rec_version         integer,
                           pv_prepaid_service_ind varchar2,
                           PV_ERROR               OUT VARCHAR2) is
begin
  
insert into SYSADM.mpulktm1
values
(pn_tmcode,
0,
pd_vsdate,
'W',
5,
pn_sncode,
null,
null,
0,
'A',
'C',
'O',
'E',
null,
null,
null,
null,
null,
null,
null,
pv_accglcode,
null,
null,
null,
null,
null,
null,
null,
pv_accserv_catcode,
pv_accserv_code,
'ACC',
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
'FIFDGS0000',
null,
null,
'FIFMGS0000',
null,
null,
null,
null,
null,
null,
null,
null,
'N',
'N',
'N',
'3',
'N');
  
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    
end P_INSERTAR_FEAT_PLAN_MK1;



PROCEDURE P_INSERTAR_FEAT_PLAN_MKTMB(PN_TMCODE              integer,
                             PN_VSCODE              integer,
                             PD_VSDATE              date,
                             PV_STATUS              varchar2,
                             PN_SPCODE              integer,
                             PN_SNCODE              integer,
                             PV_SRVIND              varchar2,
                             PV_ECHIND              varchar2,
                             PV_AMTIND              varchar2,
                             PV_FRQIND              varchar2,
                             PF_SUBSCRIPT           float,
                             PF_ACCESSFEE           float,
                             PF_EVENT               float,
                             PV_PROIND              varchar2,
                             PV_ADVIND              varchar2,
                             PV_SUSIND              varchar2,
                             PV_SUBGLCODE           varchar2,
                             PV_ACCGLCODE           varchar2,
                             PV_USGGLCODE           varchar2,
                             PV_SUBGLCODE_DISC      varchar2,
                             PV_ACCGLCODE_DISC      varchar2,
                             PV_USGGLCODE_DISC      varchar2,
                             PV_SUBGLCODE_MINCOM    varchar2,
                             PV_ACCGLCODE_MINCOM    varchar2,
                             PV_USGGLCODE_MINCOM    varchar2,
                             PN_SUBJCID             integer,
                             PN_ACCJCID             integer,
                             PN_USGJCID             integer,
                             PN_SUBJCID_DISC        integer,
                             PN_ACCJCID_DISC        integer,
                             PN_USGJCID_DISC        integer,
                             PN_SUBJCID_MINCOM      integer,
                             PN_ACCJCID_MINCOM      integer,
                             PN_USGJCID_MINCOM      integer,
                             PV_SUBSERV_CATCODE     varchar2,
                             PV_SUBSERV_CODE        varchar2,
                             PV_SUBSERV_TYPE        varchar2,
                             PV_ACCSERV_CATCODE     varchar2,
                             PV_ACCSERV_CODE        varchar2,
                             PV_ACCSERV_TYPE        varchar2,
                             PV_USGSERV_CATCODE     varchar2,
                             PV_USGSERV_CODE        varchar2,
                             PV_USGSERV_TYPE        varchar2,
                             PV_CSIND               varchar2,
                             PN_CLCODE              integer,
                             PF_DEPOSIT             float,
                             PV_INTERVAL_TYPE       varchar2,
                             PN_INTERVAL            integer,
                             PN_COMBI_ID            integer,
                             PV_PRM_PRINT_IND       varchar2,
                             PV_PRINTSUBSCRIND      varchar2,
                             PV_PRINTACCESSIND      varchar2,
                             PC_PREPAID_SERVICE_IND char,
                             PN_REC_VERSION         integer,
                             PV_ERROR               OUT VARCHAR2) is
begin

INSERT INTO SYSADM.MPULKTMB
(TMCODE,
VSCODE,
VSDATE,
STATUS,
SPCODE,
SNCODE,
SRVIND,
ECHIND,
AMTIND,
FRQIND,
SUBSCRIPT,
ACCESSFEE,
EVENT,
PROIND,
ADVIND,
SUSIND,
SUBGLCODE,
ACCGLCODE,
USGGLCODE,
SUBGLCODE_DISC,
ACCGLCODE_DISC,
USGGLCODE_DISC,
SUBGLCODE_MINCOM,
ACCGLCODE_MINCOM,
USGGLCODE_MINCOM,
SUBJCID,
ACCJCID,
USGJCID,
SUBJCID_DISC,
ACCJCID_DISC,
USGJCID_DISC,
SUBJCID_MINCOM,
ACCJCID_MINCOM,
USGJCID_MINCOM,
SUBSERV_CATCODE,
SUBSERV_CODE,
SUBSERV_TYPE,
ACCSERV_CATCODE,
ACCSERV_CODE,
ACCSERV_TYPE,
USGSERV_CATCODE,
USGSERV_CODE,
USGSERV_TYPE,
CSIND,
CLCODE,
DEPOSIT,
INTERVAL_TYPE,
"INTERVAL",
PV_COMBI_ID,
PRM_PRINT_IND,
PRINTSUBSCRIND,
PRINTACCESSIND,
PREPAID_SERVICE_IND,
REC_VERSION)
values
(PN_TMCODE,
PN_VSCODE,
PD_VSDATE,
PV_STATUS,
PN_SPCODE,
PN_SNCODE,
PV_SRVIND,
PV_ECHIND,
PV_AMTIND,
PV_FRQIND,
PF_SUBSCRIPT,
PF_ACCESSFEE,
PF_EVENT,
PV_PROIND,
PV_ADVIND,
PV_SUSIND,
PV_SUBGLCODE,
PV_ACCGLCODE,
PV_USGGLCODE,
PV_SUBGLCODE_DISC,
PV_ACCGLCODE_DISC,
PV_USGGLCODE_DISC,
PV_SUBGLCODE_MINCOM,
PV_ACCGLCODE_MINCOM,
PV_USGGLCODE_MINCOM,
PN_SUBJCID,
PN_ACCJCID,
PN_USGJCID,
PN_SUBJCID_DISC,
PN_ACCJCID_DISC,
PN_USGJCID_DISC,
PN_SUBJCID_MINCOM,
PN_ACCJCID_MINCOM,
PN_USGJCID_MINCOM,
PV_SUBSERV_CATCODE,
PV_SUBSERV_CODE,
PV_SUBSERV_TYPE,
PV_ACCSERV_CATCODE,
PV_ACCSERV_CODE,
PV_ACCSERV_TYPE,
PV_USGSERV_CATCODE,
PV_USGSERV_CODE,
PV_USGSERV_TYPE,
PV_CSIND,
PN_CLCODE,
PF_DEPOSIT,
PV_INTERVAL_TYPE,
PN_INTERVAL,
PN_COMBI_ID,
PV_PRM_PRINT_IND,
PV_PRINTSUBSCRIND,
PV_PRINTACCESSIND,
PC_PREPAID_SERVICE_IND,
PN_REC_VERSION);

EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
END P_INSERTAR_FEAT_PLAN_MKTMB;

/*
--RTA 
--Motivo: Insercion masiva de planes en paquete TRX_ADICION_FEATURES_BSCS no es necesario llamar a este PRC
--1M
PROCEDURE P_INSERTA_PLAN_1M_TMP(PV_id_plan	varchar2,
                        PV_nombre_axis	varchar2,
                        PV_nombre_bscs	varchar2,
                        PI_tmcode	integer,
                        PN_id_detalle_plan	number,
                        PV_usuario	varchar2,
                        PV_ERROR OUT VARCHAR2) is 
begin     
INSERT INTO GSI_PLANES_TMP_R(id_plan,nombre_axis,nombre_bscs,tmcode,id_detalle_plan,usuario)
VALUES(PV_id_plan,PV_nombre_axis,PV_nombre_bscs,PI_tmcode,PN_id_detalle_plan,PV_usuario);
    
EXCEPTION WHEN OTHERS THEN
PV_ERROR:='ERROR: '||SQLERRM||'CODIGO: '||SQLCODE;
    
END P_INSERTA_PLAN_1M_TMP; */
 
PROCEDURE P_INSERTA_FEAT_1M_TMP(PI_sncode           integer,
                       PF_accessfee        float, --COSTO
                       PV_usuario          varchar2,
                       PV_PRORRATEBLES     varchar2,
                       PV_OBSERVACION      varchar2,
                       PF_COSTO_ING        VARCHAR2,
                       PV_PRORRATEABLE_ING VARCHAR2,
                       PV_VALOR_OMISION    IN VARCHAR2,
                       PV_ERROR            OUT VARCHAR2) is
begin
 
INSERT INTO GSI_SNCODES_AGREGAR
(sncode,
accessfee,
usuario,
PRORRATEABLE,
OBSERVACION,
COSTO_ING,
PRORRATEABLE_ING,
VALOR_OMISION)
VALUES
(PI_sncode,
PF_accessfee,
PV_usuario,
PV_PRORRATEBLES,
PV_OBSERVACION,
PF_COSTO_ING,
PV_PRORRATEABLE_ING,
PV_VALOR_OMISION);
 
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
   
END P_INSERTA_FEAT_1M_TMP;
  
  
PROCEDURE P_UPDATE_TMCODE_1M_TMP(PI_sncode           integer,
                         PV_usuario          varchar2,
                         PI_TMCODE_PLANTILLA integer,
                         PV_ERROR            OUT VARCHAR2) IS
BEGIN
UPDATE SYSADM.GSI_SNCODES_AGREGAR
SET tmcode_plantilla = PI_TMCODE_PLANTILLA
WHERE SNCODE = PI_sncode
AND USUARIO = PV_usuario;
  
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    
END P_UPDATE_TMCODE_1M_TMP;
  
PROCEDURE P_INSERTA_LOG_FEAT_BSCS(PV_sncode             varchar2,
                         PV_tmcode             varchar2,
                         PV_observacion        varchar2,
                         PV_usuario            varchar2,
                         PD_fecha_registro     DATE,
                         PV_tipo_feature       varchar2,
                         PV_ESTADO_OBSERVACION varchar2,
                         PV_ERROR              OUT VARCHAR2) IS
BEGIN
NULL;
INSERT INTO SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
(sncode,
tmcode,
observacion,
usuario,
fecha_registro,
tipo_feature,
ESTADO)
VALUES
(PV_sncode,
PV_tmcode,
PV_observacion,
PV_usuario,
PD_fecha_registro,
PV_tipo_feature,
PV_ESTADO_OBSERVACION);
EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
END P_INSERTA_LOG_FEAT_BSCS;
     

PROCEDURE P_UPDATE_LOG_OBSERVACION_FEAT(PV_sncode             varchar2,
                                PV_tmcode             varchar2,
                                PV_observacion        varchar2,
                                PV_usuario            varchar2,
                                PV_ESTADO_OBSERVACION varchar2,
                                PV_ERROR              OUT VARCHAR2) IS
BEGIN
NULL;
UPDATE SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
SET observacion = PV_observacion, ESTADO = PV_ESTADO_OBSERVACION
WHERE sncode = PV_sncode
AND tmcode = PV_tmcode
AND USUARIO = PV_usuario;

EXCEPTION
WHEN OTHERS THEN
PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
END P_UPDATE_LOG_OBSERVACION_FEAT;
 
END OBJ_ADICION_FEATURES_BSCS;                                   
                                    
/
