CREATE OR REPLACE PACKAGE MMK_CALCULO_SCORE_CUENTA IS

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      AUTOR         : SUD JOSE SOTOMAYOR
      PROP�SITO     : C�lculo de Score Cuentas
      CREADO        : 30/05/2012
  ===========================================================================================================--*/

  --* VARIABLES SCP
  ---------------------------------------------------------------
  --SCP: C�DIGO GENERADO AUTOMATICAMENTE. DEFINICI�N DE VARIABLES
  ---------------------------------------------------------------
  GN_ID_BITACORA_SCP_RESULTA NUMBER;
  GN_ID_BITACORA_SCP_RESUMEN NUMBER;
  GB_ERROR                   BOOLEAN := FALSE;
  ---------------------------------------------------------------

  PROCEDURE MMP_PRINCIPAL(PN_HILO  IN NUMBER,
                          PD_FECHA IN DATE,
                          PV_ERROR OUT VARCHAR2);

  PROCEDURE MMP_PROCESA_CUENTAS(PV_CICLO    IN VARCHAR2,
                                PN_HILO     IN NUMBER,
                                PD_FECHA_EV IN DATE,
                                PV_ERROR    OUT VARCHAR2);

  PROCEDURE MMP_CALCULA_PROMEDIO(PV_CUENTA      IN VARCHAR2,
                                 PD_FECHA_CORTE IN DATE,
                                 PV_MORA        IN VARCHAR2,
                                 PN_PUNTAJE     OUT NUMBER,
                                 PN_PROMEDIO    OUT NUMBER,
                                 PV_ERROR       OUT VARCHAR2);

  PROCEDURE MMP_CONSULTA_MORA_ANTERIOR(PV_CUENTA      IN VARCHAR2,
                                       PD_FECHA_CORTE IN DATE,
                                       PV_RESULTADO   OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2);

  PROCEDURE MMP_INSERTA_SCORE_CTAS(PD_FECHA_CORTE IN DATE,
                                   PV_CICLO       IN VARCHAR2,
                                   PV_CUENTA      IN VARCHAR2,
                                   PN_PROMEDIO    IN NUMBER,
                                   PV_ERROR       OUT VARCHAR2);

  PROCEDURE MMP_OBTIENE_CICLO(PD_FECHA       IN DATE,
                              PV_CICLO_AXIS  IN VARCHAR2,
                              PV_CICLO_BSCS  OUT VARCHAR2,
                              PV_PER_INI     OUT VARCHAR2,
                              PV_PER_FIN     OUT VARCHAR2,
                              PV_DESC_CICLO  OUT VARCHAR2,
                              PV_CODIGOERROR OUT VARCHAR2);

  PROCEDURE MMP_PRINCIPAL_RESUMEN(PN_HILO  IN NUMBER,
                                  PD_FECHA IN DATE,
                                  PV_ERROR OUT VARCHAR2);

  PROCEDURE MMP_PROCESA_CUENTAS_RESUMEN(PV_CICLO    IN VARCHAR2,
                                        PN_HILO     IN NUMBER,
                                        PD_FECHA_EV IN DATE,
                                        PV_ERROR    OUT VARCHAR2);

  PROCEDURE MMP_INSERTA_RESUMEN_SCORE_CTAS(PD_FECHA_CORTE   IN  VARCHAR2,
                                           PV_CICLO         IN  VARCHAR2,
                                           PV_CUENTA        IN  VARCHAR2,
                                           PV_MAYOR_VENCIDO IN  VARCHAR2,
                                           PN_PROMEDIO      IN  NUMBER,
                                           PV_EXISTE        IN  VARCHAR2,                                           
                                           PV_ERROR         OUT VARCHAR2);

   PROCEDURE MMP_INSERT_CONTROL_CICLO(PV_CICLO       IN VARCHAR2,
                                      PD_FECHA_CORTE IN DATE,                                     
                                      PV_ESTADO      IN VARCHAR2,
                                      PN_HILO        IN NUMBER,
                                      PV_TABLA       IN VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2);

  PROCEDURE MMP_PRINCIPAL_RESULT(PN_HILO  IN NUMBER,
                                 PD_FECHA IN DATE,
                                 PV_ERROR OUT VARCHAR2);

  PROCEDURE MMP_INSERTA_RESULT_SCORE_CTAS(PN_HILO  IN NUMBER,
                                          PV_ERROR OUT VARCHAR2);
                                          
  PROCEDURE MMP_CALCULA_PROMEDIO_RESUMEN(PV_CUENTA      IN VARCHAR2,
                                         PD_FECHA_CORTE IN DATE,
                                         PV_MORA        IN VARCHAR2,
                                         PN_PROMEDIO    OUT NUMBER,
                                         PV_EXISTE      OUT VARCHAR2,
                                         PV_ERROR       OUT VARCHAR2);
                                 
  -- SUD DRO                               
  PROCEDURE MMP_OBTIENE_PUNTAJE_MORA(PV_MORA         IN VARCHAR2,
                                     PV_EQUIVALENCIA OUT NUMBER,
                                     PV_ERROR        OUT VARCHAR2);
                                     
 procedure MMP_VERIFICA_CASTIGO_CARTERA(pv_cuenta in varchar2,
                                       pn_saldo out varchar2,
                                       pv_error out varchar2);                                     
END MMK_CALCULO_SCORE_CUENTA;
/
CREATE OR REPLACE PACKAGE BODY MMK_CALCULO_SCORE_CUENTA IS

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Proceso Principal de C�lculo de Score Cuentas
      CREADO        : 30/05/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_PRINCIPAL(PN_HILO  IN NUMBER,
                          PD_FECHA IN DATE,
                          PV_ERROR OUT VARCHAR2) IS
  
    --* CICLOS DE FACTURACI�N EXISTENTES
    CURSOR C_CICLOS_FACTURA(CV_PARAMETRO VARCHAR2) IS
      SELECT F.ID_CICLO
        FROM FA_CICLOS_BSCS F
       WHERE F.ID_CICLO <> NVL(CV_PARAMETRO, 'N');
  
    --* CONSULTA CICLOS PENDIENTES DE PROCESAMIENTO
    CURSOR C_TRUNCA_ORIGEN IS
      SELECT COUNT(0)
        FROM MMAG_CONTROL_CICLOS T
       WHERE T.TABLA_PROCESO = 'RESULTADO'
         AND T.ESTADO = 'P';
  
    --* CONSULTO EL CICLO QUE NO SE USA PERO SE TIENE CONFIGURADO, PARA QUE NO LO TOME EN CONSIDERACION
    CURSOR C_PARAMETRO_CICLO(CD_PARAMETRO      VARCHAR2,
                             CD_TIPO_PARAMETRO VARCHAR2) IS
      SELECT I.VALOR
        FROM GV_PARAMETROS I
       WHERE I.ID_TIPO_PARAMETRO = CD_TIPO_PARAMETRO
         AND I.ID_PARAMETRO = CD_PARAMETRO;
  
    --* DECLARACI�N DE VARIABLES
    LV_ERROR     VARCHAR2(2000);
    LV_SENTENCIA VARCHAR2(2000);
    LE_ERROR EXCEPTION;
    LV_VALOR_PARAM VARCHAR2(5);
    LN_TRUNCA      NUMBER;
  
    --* VARIABLES SCP
    ---------------------------------------------------------------
    --* SCP: C�DIGO GENERADO AUTOMATICAMENTE. DEFINICI�N DE VARIABLES
    ---------------------------------------------------------------
    --LN_ID_BITACORA_SCP            NUMBER := 0;     
    LV_ID_PROCESO_SCP      VARCHAR2(100) := 'CALCULA_SCORE_CUENTAS';
    LV_REFERENCIA_SCP      VARCHAR2(100) := 'MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL';
    LN_TOTAL_REGISTROS_SCP NUMBER := 0;
    LV_UNIDAD_REGISTRO_SCP VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP           NUMBER := 0;
    LV_ERROR_SCP           VARCHAR2(500);
    ---------------------------------------------------------------    
  
  BEGIN
  
    --* SCP: INICIO DE PROCESO ------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          GN_ID_BITACORA_SCP_RESULTA,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP <> 0 THEN
      RETURN;
    END IF;
    
    -- SCP: BITACORA DE INICIO DE PROCESO --------------------------------------
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                          'INICIO C�LCULO SCORE CUENTAS PARA MMAG_RESUL_SCORE_CUENTA',
                                          NULL,
                                          NULL,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'S',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    --* VERIFICA SI EXISTEN CICLOS PENDIENTES DE PROCESAR PARA NO TRUNCAR LA TABLA DE RESULTADOS
    OPEN C_TRUNCA_ORIGEN;
    FETCH C_TRUNCA_ORIGEN
      INTO LN_TRUNCA;
    CLOSE C_TRUNCA_ORIGEN;
  
    --* VALIDA SI SE REALIZA TRUNCATE DE LA TABLA DE RESULTADOS     
    IF LN_TRUNCA = 0 THEN
      LV_SENTENCIA := 'TRUNCATE TABLE MMAG_RESUL_SCORE_CUENTA';
      EXECUTE IMMEDIATE LV_SENTENCIA;
    END IF;
  
    --* EXTRAIGO LOS CICLOS QUE SE TIENE CONFIGURADO PARA QUE NO LOS TOME EN CUENTA
    OPEN C_PARAMETRO_CICLO('CICLO_FACTURACION', '6172');
    FETCH C_PARAMETRO_CICLO
      INTO LV_VALOR_PARAM;
    CLOSE C_PARAMETRO_CICLO;
  
    --* SE OBTIENEN LOS CICLOS
    FOR DATOS IN C_CICLOS_FACTURA(LV_VALOR_PARAM) LOOP
    
      --* LLAMADA AL PROCESO QUE CALCULA SCORE DE CUENTAS DE ACUERDO AL CICLO
      MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS(PV_CICLO    => DATOS.ID_CICLO,
                                                   PN_HILO     => PN_HILO,
                                                   PD_FECHA_EV => PD_FECHA,
                                                   PV_ERROR    => LV_ERROR);
    
    END LOOP;
  
    -- SCP: BITACORA DE FINALIZACION DE PROCESO --------------------------------
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                          'FIN C�LCULO SCORE CUENTAS PARA MMAG_RESUL_SCORE_CUENTA',
                                          NULL,
                                          NULL,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'S',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    --* GRABA LO PENDIENTE
    COMMIT;
  
    IF GB_ERROR THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL ' ||
                  LV_ERROR;
    
      -- SCP: BITACORA DE ERRORES EN PROCESO ----------------------------------------- 
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                            'ERROR GENERAL EN PROCESO PRINCIPAL',
                                            PV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL ' ||
                  SQLERRM;
    
      -- SCP: BITACORA DE ERRORES EN PROCESO ----------------------------------------- 
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                            'ERROR GENERAL EN PROCESO PRINCIPAL',
                                            PV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    
  END MMP_PRINCIPAL;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Procesa las cuentas buscando en la tabla peri�dica de acuerdo al ciclo de facturaci�n
      CREADO        : 10/03/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_PROCESA_CUENTAS(PV_CICLO    IN VARCHAR2,
                                PN_HILO     IN NUMBER,
                                PD_FECHA_EV IN DATE,
                                PV_ERROR    OUT VARCHAR2) IS                                   
    --=====================================================================================================================--
    --    LIDER PDS     : SUD Alex Guaman
    --    AUTOR         : SUD Diego Rogel
    --    22/10/2012 - Se cambio el cursor de control C_CONTROL_CICLO para enviarle el hilo que se esta procesando    
    --=====================================================================================================================--
    --* VERIFICA EXISTENCIA DE TABLA PERI�DICA
    CURSOR C_VERIFICA_EXISTENCIA(CV_TABLA VARCHAR2) IS
      SELECT COUNT(0) FROM ALL_TABLES A WHERE TABLE_NAME = CV_TABLA;
  
    -- SUD DRO
    --* CONTROL DE CICLOS QUE SE HAN PROCESADO
    CURSOR C_CONTROL_CICLO(CV_CICLO IN VARCHAR2, CD_FECHA_CORTE IN DATE, CV_HILO IN NUMBER) IS
      SELECT I.ESTADO
        FROM MMAG_CONTROL_CICLOS I
       WHERE I.CICLO         = CV_CICLO
         AND I.FECHA_CORTE   = CD_FECHA_CORTE
         AND I.TABLA_PROCESO = 'RESULTADO'
         AND I.HILO          = CV_HILO;         
  
    --
    LT_CUENTA COT_STRING := COT_STRING();
    LT_MORA   COT_STRING := COT_STRING();
  
    --* DECLARACI�N DE VARIABLES
    LV_SENTENCIA   VARCHAR(2000);
    LV_CICLO_BSCS  VARCHAR2(2);
    LV_PER_INI     VARCHAR2(10);
    LV_PER_FIN     VARCHAR2(10);
    LV_PER_EVALUA  VARCHAR2(10);
    LV_TABLA       VARCHAR2(50);
    LV_DESCRIPCION VARCHAR2(100);
    LN_EXISTE      NUMBER;
    LN_PUNTAJE     NUMBER;
    LN_CONT        NUMBER := 0;
    LN_SCORE_CTA   FLOAT;
    LD_FECHA_PER   DATE;
    LV_ESTADO      VARCHAR2(2);
  
    --* VARIABLES DE ERROR
    LV_ERROR  VARCHAR(2000);
    LV_ERROR2 VARCHAR(2000);
    LE_ERROR EXCEPTION;
    LE_ERROR_SUB EXCEPTION;
  
    --* VARIABLES SCP
    ---------------------------------------------------------------
    --SCP: C�DIGO GENERADO AUTOMATICAMENTE. DEFINICI�N DE VARIABLES
    ---------------------------------------------------------------
    LN_ERROR_SCP NUMBER := 0;
    LV_ERROR_SCP VARCHAR2(500);
    ---------------------------------------------------------------
  
  BEGIN
  
    --* SE OBTIENE EL PER�ODO DE EVALUACI�N SEGUN EL CICLO
    MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_CICLO(PD_FECHA       => PD_FECHA_EV,
                                               PV_CICLO_AXIS  => PV_CICLO,
                                               PV_CICLO_BSCS  => LV_CICLO_BSCS,
                                               PV_PER_INI     => LV_PER_INI,
                                               PV_PER_FIN     => LV_PER_FIN,
                                               PV_DESC_CICLO  => LV_DESCRIPCION,
                                               PV_CODIGOERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    --* OBTIENE FORMATO CORRECTO
    LV_PER_EVALUA := REPLACE(LV_PER_INI, '/', '');
  
    --* OBTIENE EL NOMBRE DE LA TABLA
    LV_TABLA := 'CO_REPCARCLI_' || LV_PER_EVALUA;
    
    --* TRANSFORMA A FORMATO FECHA
    LD_FECHA_PER := TO_DATE(LV_PER_INI, 'DD/MM/YYYY');
  
    --* VERIFICA SI EL CICLO YA FUE PROCESADO
    OPEN C_CONTROL_CICLO(PV_CICLO, LD_FECHA_PER, PN_HILO);
    FETCH C_CONTROL_CICLO
      INTO LV_ESTADO;
    CLOSE C_CONTROL_CICLO;
  
    --* SI YA ESTA PROCESADO, NO CONTINUA
    IF NVL(LV_ESTADO, 'P') = 'F' THEN
      LV_ERROR := 'CICLO DE FACTURACION PARA LA TABLA DE RESULTADO YA ESTA PROCESADO';
    
      RETURN;    
    END IF;
  
    --* VERIFICA EXISTENCIA DE TABLA PERI�DICA
    OPEN C_VERIFICA_EXISTENCIA(LV_TABLA);
    FETCH C_VERIFICA_EXISTENCIA
      INTO LN_EXISTE;
    CLOSE C_VERIFICA_EXISTENCIA;
  
    --* VALIDA SI EXISTE LA TABLA PERI�DICA
    IF LN_EXISTE = 0 THEN
      LV_ERROR := 'LA TABLA PERI�DICA ' || LV_TABLA || ' NO EXISTE!!!';
    
      --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE ESTA PENDIENTE DE PROCESAR
      MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                        PD_FECHA_CORTE => LD_FECHA_PER,
                                                        PV_ESTADO      => 'P',
                                                        PN_HILO        => PN_HILO,
                                                        PV_TABLA       => 'RESULTADO',
                                                        PV_ERROR       => LV_ERROR2);
      IF LV_ERROR2 IS NOT NULL THEN
        LV_ERROR := LV_ERROR || ' Y ' || LV_ERROR2;
        RAISE LE_ERROR;
      END IF;
    
      RAISE LE_ERROR;
    END IF;
  
    --* SE ARMA LA SENTENCIA DIN�MICA                
    LV_SENTENCIA := ' BEGIN ' || CHR(10) || ' SELECT C.CUENTA,' || CHR(10) ||
                    '        C.MAYORVENCIDO ' || CHR(10) ||
                    '        BULK COLLECT INTO :1, :2' || CHR(10) ||
                    ' FROM ' || LV_TABLA || ' C' || CHR(10) ||
                    ' WHERE SUBSTR(C.CUENTA, -1) = ' || PN_HILO || CHR(10) ||
                    ' ;END;';
  
    --* SE REALIZA LA CONSULTA DIN�MICA  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING OUT LT_CUENTA, OUT LT_MORA;
  
    --* SI NO ENCUENTRA DATOS  
    IF LT_CUENTA.COUNT = 0 THEN
      LV_ERROR := 'NO SE ENCONTR� INFORMACI�N EN LA TABLA PERI�DICA ' ||
                  LV_TABLA || '!!!';
    
      --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE ESTA PENDIENTE DE PROCESAR
      MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                        PD_FECHA_CORTE => LD_FECHA_PER,
                                                        PV_ESTADO      => 'P',
                                                        PN_HILO        => PN_HILO,
                                                        PV_TABLA       => 'RESULTADO',
                                                        PV_ERROR       => LV_ERROR2);
      IF LV_ERROR2 IS NOT NULL THEN
        LV_ERROR := LV_ERROR || ' Y ' || LV_ERROR2;
        RAISE LE_ERROR;
      END IF;
    
      RAISE LE_ERROR;
    END IF;
  
    --* RECORRE TODOS LOS REGISTROS DE LA TABLA PERI�DICA
    FOR C IN LT_CUENTA.FIRST .. LT_CUENTA.LAST LOOP
    
      BEGIN
      
        --* CALCULA EL PROMEDIO DE LA CUENTA
        MMK_CALCULO_SCORE_CUENTA.MMP_CALCULA_PROMEDIO(PV_CUENTA      => LT_CUENTA(C),
                                                      PD_FECHA_CORTE => LD_FECHA_PER,
                                                      PV_MORA        => LT_MORA(C),
                                                      PN_PUNTAJE     => LN_PUNTAJE,
                                                      PN_PROMEDIO    => LN_SCORE_CTA,
                                                      PV_ERROR       => LV_ERROR);
      
        IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR_SUB;
        END IF;
      
        --* SE REGISTRA EN ESTRUCTURA DE CALIFICACIONES DE CUENTAS
        MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_SCORE_CTAS(PD_FECHA_CORTE => LD_FECHA_PER,
                                                        PV_CICLO       => PV_CICLO,
                                                        PV_CUENTA      => LT_CUENTA(C),
                                                        PN_PROMEDIO    => LN_SCORE_CTA,
                                                        PV_ERROR       => LV_ERROR);
      
        IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR_SUB;
        END IF;
      
        LN_CONT := LN_CONT + 1;
      
        --* GRABA CADA 500 REGISTROS Y ENCERA EL CONTADOR          
        IF (LN_CONT >= 500) THEN
          COMMIT;
          LN_CONT := 0;
        END IF;
      
      EXCEPTION
        WHEN LE_ERROR_SUB THEN        
          
           LV_ERROR := 'ERROR EN SUBBLOQUE GENERAL - MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS, MENSAJE: ' ||
                      LV_ERROR;
           RAISE LE_ERROR;  
                                   
        WHEN OTHERS THEN
        
          -- SCP: BITACORA DE ERRORES EN PROCESO --  
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                                'ERROR GENERAL BLOQUE DETALLE',
                                                LV_ERROR,
                                                NULL,
                                                1,
                                                LT_MORA(C),
                                                LT_CUENTA(C),
                                                NULL,
                                                NULL,
                                                NULL,
                                                'S',
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
        
          LV_ERROR := 'ERROR EN SUBBLOQUE GENERAL - MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS, MENSAJE: ' ||
                      SQLERRM;
          RAISE LE_ERROR;
      END;
    END LOOP;
  
    --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE YA FUE PROCESADO
    MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                      PD_FECHA_CORTE => LD_FECHA_PER,
                                                      PV_ESTADO      => 'F',
                                                      PN_HILO        => PN_HILO,
                                                      PV_TABLA       => 'RESULTADO',
                                                      PV_ERROR       => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    --* GRABA LO PENDIENTE
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      GB_ERROR := TRUE;
    
      -- SCP: BITACORA DE ERRORES EN PROCESO -- 
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                            'ERROR ESPEC�FICO BLOQUE PRINCIPAL',
                                            LV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR NO CONTROLADO EN MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS - NO SE PUDO PROCESAR EL C�LCULO DEL SCORE DE LAS CUENTAS' ||
                  SQLERRM;
      ROLLBACK;
      GB_ERROR := TRUE;
    
      -- SCP: BITACORA DE ERRORES EN PROCESO --  
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                            'ERROR GENERAL BLOQUE PRINCIPAL',
                                            LV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    
  END MMP_PROCESA_CUENTAS;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS    
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD Alex Guaman
      AUTOR         : SUD Cristian Silva
      PROP�SITO     : Realiza el c�lculo del promedio del Score Cuenta, buscando en la tabla de resumen
      CREADO        : 12/03/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_CALCULA_PROMEDIO(PV_CUENTA      IN VARCHAR2,
                                 PD_FECHA_CORTE IN DATE,
                                 PV_MORA        IN VARCHAR2,
                                 PN_PUNTAJE     OUT NUMBER,
                                 PN_PROMEDIO    OUT NUMBER,
                                 PV_ERROR       OUT VARCHAR2) IS
  
    --* DECLARACI�N DE CURSORES  
    --* CALIFICACI�N SEG�N LA MORA
    CURSOR C_PUNTAJES(CV_MORA VARCHAR2) IS
      SELECT P.PUNTAJE FROM MMAG_PUNTAJES_MORA P WHERE P.MORA = CV_MORA;
  
    --* DECLARACI�N DE VARIABLES
    LN_PUNTAJE         NUMBER;
    LN_PUNTAJE_RESUMEN NUMBER;
    LN_PROMEDIO        FLOAT;
    LV_ERROR           VARCHAR2(1000);
    LV_MORA_ANTERIOR   VARCHAR2(10);
    LV_SENTENCIA       VARCHAR2(2000);
    LV_MES             VARCHAR2(15);
    LB_NOTFOUND        BOOLEAN;
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    --* SE OBTIENE CALIFICACI�N
    OPEN C_PUNTAJES(PV_MORA);
    FETCH C_PUNTAJES
      INTO LN_PUNTAJE;
    LB_NOTFOUND := C_PUNTAJES%NOTFOUND;
    CLOSE C_PUNTAJES;
  
    IF LB_NOTFOUND THEN
      LV_ERROR := 'NO SE ENCONTR� CALIFICACI�N PARA LA MORA ' || PV_MORA ||
                  ', REVISE CONFIGURACION EN MMAG_PUNTAJES_MORA';
      RAISE LE_ERROR;
    END IF;
  
    --* SE EXTRAE EL MES ANTERIOR CORRESPONDIENTE AL CAMPO DE LA TABLA
    LV_MES := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -1),
                                   'DD/MM/YYYY'),
                           'MONTH',
                           'NLS_DATE_LANGUAGE=SPANISH'));
  
    BEGIN
      -- EXTRAE LA MORA ANTERIOR DE LA TABLA DE RESUMEN                     
      LV_SENTENCIA := 'BEGIN SELECT ' || 'I.' || LV_MES || ' ,I.PROMEDIO' ||
                      ' INTO :1, :2 FROM MMAG_SCORE_CUENTA_RESUMEN I WHERE I.CUENTA = ''' ||
                      PV_CUENTA || '''; END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING OUT LV_MORA_ANTERIOR, OUT LN_PUNTAJE_RESUMEN;
    
    EXCEPTION
      WHEN OTHERS THEN
        --CUANDO EL ERROR ES NO_DATA_FOUND
        LV_ERROR := SQLERRM;
      
        IF INSTR(LV_ERROR, 'ORA-01403') > 0 THEN
          NULL;
        ELSE
          RAISE LE_ERROR;
        END IF;
    END;
  
    IF LV_MORA_ANTERIOR IS NULL THEN
      -- SI NO SE ENCONTRO MORA EN EL MES ANTERIOR, COLOCA PUNTAJE DEL MES ACTUAL POR SER LA PRIMERA VEZ
      LN_PROMEDIO := LN_PUNTAJE;
    
    ELSE
    
      --* SE CALCULA EN BASE AL PROMEDIO ENTRE EL PUNTAJE ACTUAL Y PROMEDIO ANTERIOR
      LN_PROMEDIO := (LN_PUNTAJE + NVL(LN_PUNTAJE_RESUMEN, LN_PUNTAJE)) / 2;
    
    END IF;
  
    --Retorno promedio y puntaje 
    PN_PUNTAJE  := LN_PUNTAJE;
    PN_PROMEDIO := LN_PROMEDIO;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_CALCULA_PROMEDIO - ' ||
                  LV_ERROR;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_CONSULTA_MORA ' ||
                  SQLERRM;
    
  END MMP_CALCULA_PROMEDIO;
----------------------------------------------------------------------

----------------------------------------------------------------------
  PROCEDURE MMP_CALCULA_PROMEDIO_RESUMEN(PV_CUENTA      IN VARCHAR2,
                                         PD_FECHA_CORTE IN DATE,
                                         PV_MORA        IN VARCHAR2,
                                         PN_PROMEDIO    OUT NUMBER,
                                         PV_EXISTE      OUT VARCHAR2,
                                         PV_ERROR       OUT VARCHAR2) IS
  
    --=====================================================================================================================
        --    LIDER PDS     : SUD Alex Guaman
        --    AUTOR         : SUD Diego Rogel
        --    22/10/2012 - Se envia el estado de PV_EXISTE para los demas procedimientos
    --=====================================================================================================================
  
   --Consulto bandera para verificar el estado de cartera de una cuenta 
    cursor c_verifica_cuenta(cv_descripcion varchar2) is
     select f.valor
      from gv_parametros f
     where f.id_tipo_parametro = 6172
       and f.id_parametro = cv_descripcion;
  
    --* DECLARACI�N DE VARIABLES
    lc_verifica_cuenta c_verifica_cuenta%rowtype;
    LN_PROMEDIO        FLOAT;
    LV_ERROR           VARCHAR2(1000);
    LV_MORA_ANTERIOR   VARCHAR2(10);
    LV_SENTENCIA       VARCHAR2(2000);
    LV_MES             VARCHAR2(15);
    LV_BANDERA         varchar2(100) := 'VERIFICA_CTA_CASTIGADA';
    LE_ERROR           EXCEPTION;
  
    -- SUD DRO
    LV_MES_2           VARCHAR2(15);
    LV_MES_3           VARCHAR2(15);
    LV_MES_4           VARCHAR2(15);
    LV_MES_5           VARCHAR2(15);
    LV_MORA_ANTERIOR_2 VARCHAR2(10);
    LV_MORA_ANTERIOR_3 VARCHAR2(10);
    LV_MORA_ANTERIOR_4 VARCHAR2(10);
    LV_MORA_ANTERIOR_5 VARCHAR2(10);
    LN_PUNTAJE_A       NUMBER;
    LN_PUNTAJE_1       NUMBER;
    LN_PUNTAJE_2       NUMBER;
    LN_PUNTAJE_3       NUMBER;
    LN_PUNTAJE_4       NUMBER;
    LN_PUNTAJE_5       NUMBER;
    LN_SALDO           number;
    

  BEGIN
    -- DETERMINAR SI YA EXISTE EL REGISTRO
    PV_EXISTE := 'S';
  
    --* SE EXTRAE EL MES ANTERIOR CORRESPONDIENTE AL CAMPO DE LA TABLA
    LV_MES := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -1),
                                   'DD/MM/YYYY'),
                           'MONTH',
                           'NLS_DATE_LANGUAGE=SPANISH'));
    -- SUD DRO
    LV_MES_2 := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -2), 'DD/MM/YYYY'),
                                            'MONTH', 'NLS_DATE_LANGUAGE=SPANISH'));
    LV_MES_3 := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -3), 'DD/MM/YYYY'),
                                            'MONTH', 'NLS_DATE_LANGUAGE=SPANISH'));
    LV_MES_4 := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -4), 'DD/MM/YYYY'),
                                            'MONTH', 'NLS_DATE_LANGUAGE=SPANISH'));
    LV_MES_5 := TRIM(TO_CHAR(TO_DATE(ADD_MONTHS(PD_FECHA_CORTE, -5), 'DD/MM/YYYY'),
                                            'MONTH', 'NLS_DATE_LANGUAGE=SPANISH'));
  
    BEGIN      
      LV_SENTENCIA := 'BEGIN SELECT ' || 'I.' || LV_MES   ||
                                        ',I.' || LV_MES_2 ||
                                        ',I.' || LV_MES_3 ||
                                        ',I.' || LV_MES_4 ||
                                        ',I.' || LV_MES_5 ||
                      ' INTO :1, :2, :3, :4, :5 FROM MMAG_SCORE_CUENTA_RESUMEN I WHERE I.CUENTA = ''' ||
                      PV_CUENTA || '''; END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING OUT LV_MORA_ANTERIOR,   OUT LV_MORA_ANTERIOR_2, OUT LV_MORA_ANTERIOR_3,
              OUT LV_MORA_ANTERIOR_4, OUT LV_MORA_ANTERIOR_5;                       
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- DEVUELVE EL ESTADO N PARA INDICAR QUE NO EXISTE Y SE PROCEDA A REALIZAR EL INSERT
        PV_EXISTE := 'N';
      WHEN OTHERS THEN
        LV_ERROR := SQLERRM;
        raise LE_ERROR;
    END;
    
    ----------------------------------------------------------------------------
    --  SUD EFL - Se filtran las cuentas que tienen cartera vencida o castigada
    ----------------------------------------------------------------------------    
     open c_verifica_cuenta(LV_BANDERA);
    fetch c_verifica_cuenta
     into lc_verifica_cuenta;
    close c_verifica_cuenta;
   
    if nvl(lc_verifica_cuenta.valor,'N') = 'S' then
      
      MMK_CALCULO_SCORE_CUENTA.MMP_VERIFICA_CASTIGO_CARTERA(PV_CUENTA,
                                                            LN_SALDO,
                                                            LV_ERROR);                                                          
      if LV_ERROR is not null then        
        raise LE_ERROR;           
      end if; 
    
      --Realizo el promedio si no es una cuenta castigada o vencida
      if ln_saldo <= 0 then       
        
        -- RECUPERO LA EQUIVALENCIA DE CADA MES CON EL VALOR DE LA MORA PROCESADO EN LA TABLA
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(PV_MORA, LN_PUNTAJE_A, PV_ERROR);
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR, LN_PUNTAJE_1, PV_ERROR);
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_2, LN_PUNTAJE_2, PV_ERROR);
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_3, LN_PUNTAJE_3, PV_ERROR);
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_4, LN_PUNTAJE_4, PV_ERROR);
        MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_5, LN_PUNTAJE_5, PV_ERROR);
        
        -- SE CALCULA EN BASE AL PROMEDIO ENTRE EL PUNTAJE ACTUAL Y PROMEDIO ANTERIOR
        LN_PROMEDIO := (LN_PUNTAJE_A + LN_PUNTAJE_1 + LN_PUNTAJE_2 + LN_PUNTAJE_3 + LN_PUNTAJE_4 + LN_PUNTAJE_5) / 6;    
        PN_PROMEDIO := ROUND(LN_PROMEDIO,2);
        
      elsif ln_saldo > 0 then
        --Se setea el promedio con (0) para evitar que tenga un buen promedio
        PN_PROMEDIO := 0;
      end if;
    ----------------------------------------------------------------------------
    ----------------------------------------------------------------------------    
    else
      -- RECUPERO LA EQUIVALENCIA DE CADA MES CON EL VALOR DE LA MORA PROCESADO EN LA TABLA
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(PV_MORA, LN_PUNTAJE_A, PV_ERROR);
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR, LN_PUNTAJE_1, PV_ERROR);
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_2, LN_PUNTAJE_2, PV_ERROR);
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_3, LN_PUNTAJE_3, PV_ERROR);
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_4, LN_PUNTAJE_4, PV_ERROR);
      MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_PUNTAJE_MORA(LV_MORA_ANTERIOR_5, LN_PUNTAJE_5, PV_ERROR);
        
      -- SE CALCULA EN BASE AL PROMEDIO ENTRE EL PUNTAJE ACTUAL Y PROMEDIO ANTERIOR
      LN_PROMEDIO := (LN_PUNTAJE_A + LN_PUNTAJE_1 + LN_PUNTAJE_2 + LN_PUNTAJE_3 + LN_PUNTAJE_4 + LN_PUNTAJE_5) / 6;    
      PN_PROMEDIO := ROUND(LN_PROMEDIO,2);
        
    end if;
    
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_CALCULA_PROMEDIO - ' ||
                  LV_ERROR;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_CONSULTA_MORA ' ||
                  SQLERRM;
  END MMP_CALCULA_PROMEDIO_RESUMEN;


  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS    
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ 
      PROP�SITO     : Obtiene la mora de las cuentas consultando las tablas peri�dicas co_repcarcli_ddmmyyyy 
      CREADO        : 12/03/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_CONSULTA_MORA_ANTERIOR(PV_CUENTA      IN VARCHAR2,
                                       PD_FECHA_CORTE IN DATE,
                                       PV_RESULTADO   OUT VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2) IS
  
    --* DECLARACI�N DE VARIABLES
    LV_SENTENCIA   VARCHAR2(1000);
    LV_RESULTADO   VARCHAR2(10);
    LV_FECHA_CORTE VARCHAR2(10);
    LV_ERR         VARCHAR2(1000);
  
  BEGIN
  
    --* SE OBTIENE FORMATO CORRECTO
    LV_FECHA_CORTE := TO_CHAR(PD_FECHA_CORTE, 'DDMMYYYY');
  
    --* SE ARMA SENTENCIA
    LV_SENTENCIA := 'BEGIN SELECT C.MAYORVENCIDO INTO :1 FROM CO_REPCARCLI_' ||
                    LV_FECHA_CORTE || ' C WHERE C.CUENTA = :2; END;';
  
    --* OBTIENE EL NOMBRE DEL ARCHIVO AL EJECUTAR LA SENTENCIA
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING OUT LV_RESULTADO, IN PV_CUENTA;
  
    PV_RESULTADO := LV_RESULTADO;
  
  EXCEPTION
    WHEN OTHERS THEN
      --CUANDO EL ERROR ES NOT_DATA_FOUND
      LV_ERR := SQLERRM;
    
      IF INSTR(LV_ERR, 'ORA-01403') > 0 THEN
        PV_RESULTADO := 0;
      ELSE
        PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_CONSULTA_MORA_ANTERIOR ' ||
                    SQLERRM;
      END IF;
    
  END MMP_CONSULTA_MORA_ANTERIOR;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Realiza la inserci�n en la Estructura Principal de C�lculo de Score Cuenta
      CREADO        : 12/03/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_INSERTA_SCORE_CTAS(PD_FECHA_CORTE DATE,
                                   PV_CICLO       VARCHAR2,
                                   PV_CUENTA      VARCHAR2,
                                   PN_PROMEDIO    NUMBER,
                                   PV_ERROR       OUT VARCHAR2) IS
  
    --* OBIENE DATOS DE LA CUENTA
    CURSOR C_SCORE_CTAS(CV_CUENTA VARCHAR2, CD_FECHA DATE) IS
      SELECT F.*
        FROM MMAG_RESUL_SCORE_CUENTA F
       WHERE F.CUENTA = CV_CUENTA
         AND F.FECHA_CORTE = CD_FECHA;
  
    --* OBTIENE EL CUSTOMER_ID DE LA CUENTA
    CURSOR C_CUST_ID(CV_CUENTA VARCHAR2) IS
      SELECT I.CUSTOMER_ID
        FROM CUSTOMER_ALL I
       WHERE I.CUSTCODE = CV_CUENTA;
  
    --* DECLARACI�N DE VARIABLES   
    LE_ERROR EXCEPTION;
    LB_NOEXISTE    BOOLEAN := FALSE;
    PN_CUST_ID     VARCHAR2(20);
    LC_SCRORE_CTAS C_SCORE_CTAS%ROWTYPE;
    LV_ERROR       VARCHAR2(1000);
  
  BEGIN
  
    --* CONSULTO SI EXISTE ESA CUENTA REGISTRADA  
    OPEN C_SCORE_CTAS(PV_CUENTA, PD_FECHA_CORTE);
    FETCH C_SCORE_CTAS
      INTO LC_SCRORE_CTAS;
    LB_NOEXISTE := C_SCORE_CTAS%NOTFOUND;
    CLOSE C_SCORE_CTAS;
  
    --* CONSULTO EL CUSTOMER_ID DE LA CUENTA
    OPEN C_CUST_ID(PV_CUENTA);
    FETCH C_CUST_ID
      INTO PN_CUST_ID;
    CLOSE C_CUST_ID;
  
    IF LB_NOEXISTE THEN
    
      PN_CUST_ID := NVL(PN_CUST_ID, 0);
    
      --* SE INSERTA EL REGISTRO EN LA TABLA DE RESULTADOS
      INSERT INTO MMAG_RESUL_SCORE_CUENTA
        (CUENTA, CUSTOMER_ID, CICLO, PROMEDIO, FECHA_CORTE, FECHA_REGISTRO)
      
      VALUES
        (PV_CUENTA,
         PN_CUST_ID,
         PV_CICLO,
         PN_PROMEDIO,
         PD_FECHA_CORTE,
         SYSDATE);
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_SCORE_CTAS - ' ||
                  LV_ERROR;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_SCORE_CTAS ' ||
                  SQLERRM;
    
  END MMP_INSERTA_SCORE_CTAS;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Obtiene las fechas de corte actuales de acuerdo al ciclo de facturaci�n
      CREADO        : 12/03/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_OBTIENE_CICLO(PD_FECHA       IN DATE,
                              PV_CICLO_AXIS  IN VARCHAR2,
                              PV_CICLO_BSCS  OUT VARCHAR2,
                              PV_PER_INI     OUT VARCHAR2,
                              PV_PER_FIN     OUT VARCHAR2,
                              PV_DESC_CICLO  OUT VARCHAR2,
                              PV_CODIGOERROR OUT VARCHAR2) IS
  
    CURSOR C_FA_CICLOS(CV_CICLO VARCHAR2, CV_DEFAULT VARCHAR2) IS
      SELECT Q.ID_CICLO,
             Q.DIA_INI_CICLO,
             Q.DIA_FIN_CICLO,
             Q.DESCRIPCION,
             P.ID_CICLO_ADMIN
        FROM FA_CICLOS_BSCS Q, FA_CICLOS_AXIS_BSCS P
       WHERE Q.ID_CICLO = CV_CICLO
         AND P.ID_CICLO = Q.ID_CICLO
         AND P.CICLO_DEFAULT = CV_DEFAULT;
  
    CURSOR C_CICLOS_DEFAULT IS
      SELECT Q.ID_CICLO,
             Q.DIA_INI_CICLO,
             Q.DIA_FIN_CICLO,
             Q.DESCRIPCION,
             P.ID_CICLO_ADMIN
        FROM FA_CICLOS_BSCS Q, FA_CICLOS_AXIS_BSCS P
       WHERE Q.DEFAULTS = 'S'
         AND P.ID_CICLO = Q.ID_CICLO
         AND P.CICLO_DEFAULT = Q.DEFAULTS;
  
    LV_CICLO_AC     VARCHAR2(2);
    LV_CICLO_BC     VARCHAR2(2);
    LV_DIA_INI_C    VARCHAR2(2);
    LV_DIA_FIN_C    VARCHAR2(2);
    LV_DESC_CICLO_C FA_CICLOS_BSCS.DESCRIPCION%TYPE;
    LV_DIA_INI      VARCHAR2(2);
    LV_DIA_FIN      VARCHAR2(2);
    LV_MES_INI      VARCHAR2(2);
    LV_MES_FIN      VARCHAR2(2);
    LV_ANIO_INI     VARCHAR2(4);
    LV_ANIO_FIN     VARCHAR2(4);
    LD_FECHA        DATE;
    LV_APLICACION   VARCHAR2(70);
    LE_MIEXECPTION EXCEPTION;
    LB_FOUND   BOOLEAN;
    LV_DEFAULT VARCHAR2(1) := 'S';
  
  BEGIN
  
    LV_APLICACION := ' - MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_CICLOS';
  
    LD_FECHA := NVL(PD_FECHA, SYSDATE);
    --
    OPEN C_FA_CICLOS(PV_CICLO_AXIS, LV_DEFAULT);
    FETCH C_FA_CICLOS
      INTO LV_CICLO_AC,
           LV_DIA_INI_C,
           LV_DIA_FIN_C,
           LV_DESC_CICLO_C,
           LV_CICLO_BC;
    LB_FOUND := C_FA_CICLOS%FOUND;
    CLOSE C_FA_CICLOS;
  
    IF NOT LB_FOUND THEN
      PV_CODIGOERROR := 'NO EXISTE CICLO A CONSULTAR';
      RAISE LE_MIEXECPTION;
    END IF;
  
    IF PV_CICLO_AXIS IS NULL THEN
      OPEN C_CICLOS_DEFAULT;
      FETCH C_CICLOS_DEFAULT
        INTO LV_CICLO_AC,
             LV_DIA_INI_C,
             LV_DIA_FIN_C,
             LV_DESC_CICLO_C,
             LV_CICLO_BC;
      CLOSE C_CICLOS_DEFAULT;
      LV_DIA_INI := NVL(LV_DIA_INI_C, '24');
      LV_DIA_FIN := NVL(LV_DIA_FIN_C, '23');
    ELSE
      LV_DIA_INI := LV_DIA_INI_C;
      LV_DIA_FIN := LV_DIA_FIN_C;
    END IF;
  
    IF TO_NUMBER(TO_CHAR(LD_FECHA, 'DD')) < TO_NUMBER(LV_DIA_INI) THEN
      IF TO_NUMBER(TO_CHAR(LD_FECHA, 'MM')) = 1 THEN
        LV_MES_INI  := '12';
        LV_ANIO_INI := TO_CHAR(LD_FECHA, 'YYYY') - 1;
      ELSE
        LV_MES_INI  := LPAD(TO_CHAR(LD_FECHA, 'MM') - 1, 2, '0');
        LV_ANIO_INI := TO_CHAR(LD_FECHA, 'YYYY');
      END IF;
      LV_MES_FIN  := TO_CHAR(LD_FECHA, 'MM');
      LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'YYYY');
    ELSE
      IF TO_NUMBER(TO_CHAR(LD_FECHA, 'MM')) = 12 THEN
        LV_MES_FIN  := '01';
        LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'YYYY') + 1;
      ELSE
        LV_MES_FIN  := LPAD(TO_CHAR(LD_FECHA, 'MM') + 1, 2, '0');
        LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'YYYY');
      END IF;
      LV_MES_INI  := TO_CHAR(LD_FECHA, 'MM');
      LV_ANIO_INI := TO_CHAR(LD_FECHA, 'YYYY');
    END IF;
  
    PV_PER_INI    := LV_DIA_INI || '/' || LV_MES_INI || '/' || LV_ANIO_INI;
    PV_PER_FIN    := LV_DIA_FIN || '/' || LV_MES_FIN || '/' || LV_ANIO_FIN;
    PV_CICLO_BSCS := LV_CICLO_BC;
    PV_DESC_CICLO := LV_DESC_CICLO_C;
  
    --
    --
    --
  EXCEPTION
    WHEN LE_MIEXECPTION THEN
      PV_CODIGOERROR := PV_CODIGOERROR || LV_APLICACION;
      RETURN;
    WHEN OTHERS THEN
      PV_CODIGOERROR := SQLERRM || LV_APLICACION;
      RETURN;
  END MMP_OBTIENE_CICLO;

  /*=====================================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Proceso Principal de C�lculo de Score Cuentas para la tabla de resumen MMAG_SCORE_CUENTA_RESUMEN
      CREADO        : 11/06/2012
  ======================================================================================================================--*/
  PROCEDURE MMP_PRINCIPAL_RESUMEN(PN_HILO  IN NUMBER,
                                  PD_FECHA IN DATE,
                                  PV_ERROR OUT VARCHAR2) IS
  
    /*=====================================================================================================================--
    LIDER PDS     : SUD Alex Guaman
    AUTOR         : SUD Cristhian Silva
    18/09/2012 - Se cambio el proceso para que reciba y valide la fecha de corte a procesar
    ======================================================================================================================--*/
  
    --* DECLARACI�N DE VARIABLES
    LV_ERROR VARCHAR2(2000);
    LE_ERROR EXCEPTION;
    LV_CICLO          VARCHAR2(3);
    LV_PARAMETRO      VARCHAR2(20) := 'CICLO_FACTURACION';
    LV_TIPO_PARAMETRO VARCHAR2(20) := '6172';
    LV_VALOR_PARAM    VARCHAR2(5);
    LV_DIA_INI_CICLO  VARCHAR2(2);
    LD_FECHA          DATE;
    LV_DIAS_ESPERA    VARCHAR2(2);
    LV_DIA_EJEC       VARCHAR2(2);
    LV_APLICA_ESPERA  VARCHAR2(2);
    --* CONSULTO EL CICLO CORRESPONDIENTE DE ACUERDO AL DIA EN QUE SE EJECUTA EL PROCESO
    CURSOR C_VERIFICA_CICLO(CD_DIA VARCHAR2, CV_VALOR_PARAM VARCHAR2) IS
      SELECT CICLO, DIA_INI_CICLO
        FROM (SELECT F.ID_CICLO CICLO, F.DIA_INI_CICLO
                FROM FA_CICLOS_BSCS F
               WHERE (to_number(CD_DIA) = TO_NUMBER(F.DIA_INI_CICLO))
               --OR to_number(CD_DIA) = 1)
                 AND F.ID_CICLO <> NVL(CV_VALOR_PARAM, 'N')
               ORDER BY F.DIA_INI_CICLO DESC)
       WHERE ROWNUM <= 1;
  
    --* CONSULTO EL CICLO QUE NO SE USA PERO SE TIENE CONFIGURADO, PARA QUE NO LO TOME EN CONSIDERACION
    CURSOR C_OBTIENE_PARAMETRO(CD_PARAMETRO      VARCHAR2,
                               CD_TIPO_PARAMETRO VARCHAR2) IS
      SELECT I.VALOR
        FROM GV_PARAMETROS I
       WHERE I.ID_TIPO_PARAMETRO = CD_TIPO_PARAMETRO
         AND I.ID_PARAMETRO = CD_PARAMETRO;
  
    ---------------------------------------------------------------
  
  BEGIN
  
    IF PD_FECHA IS NULL THEN
      pv_error := 'Debe ingresar la fecha';
      Return;
    END IF;
  
    --Obtiene ciclo a excluir      
    OPEN C_OBTIENE_PARAMETRO(LV_PARAMETRO, LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_VALOR_PARAM;
    CLOSE C_OBTIENE_PARAMETRO;
  
    --Obtiene dias rango ejecucion      
    OPEN C_OBTIENE_PARAMETRO('DIAS_RANGO_EJECUCION', LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_DIAS_ESPERA;
    CLOSE C_OBTIENE_PARAMETRO;
  
    OPEN C_OBTIENE_PARAMETRO('APLICA_ESPERA', LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_APLICA_ESPERA;
    CLOSE C_OBTIENE_PARAMETRO;
    --* SACO LOS DIAS CORRECTOS DEL ULTIMO CORTE
    --LV_MES_ANIO :=  TO_CHAR(PD_FECHA,'MMYYYY');
    --LV_DIA_EJEC := TRIM(TO_CHAR(PD_FECHA - LV_DIAS_ESPERA + 1,'DD'));
    --LD_FECHA := PD_FECHA;
  
    IF LV_APLICA_ESPERA = 'S' THEN
      LD_FECHA    := PD_FECHA - LV_DIAS_ESPERA;
      LV_DIA_EJEC := TO_CHAR(LD_FECHA, 'DD');
    ELSE
      LD_FECHA    := PD_FECHA;
      LV_DIA_EJEC := TO_CHAR(LD_FECHA, 'DD');
    END IF;
  
    --Obtiene ciclo a procesar seg�n el d�a
    OPEN C_VERIFICA_CICLO(LV_DIA_EJEC, LV_VALOR_PARAM);
    FETCH C_VERIFICA_CICLO
      INTO LV_CICLO, LV_DIA_INI_CICLO;
    CLOSE C_VERIFICA_CICLO;
  
    --14/09/2012
    IF LV_CICLO IS NULL THEN
      --Pv_Error := 'La fecha receptada no corresponde a ning�n ciclo.';
      Return;
    END IF;
  
    IF TO_NUMBER(LV_DIA_EJEC) = TO_NUMBER(LV_DIA_INI_CICLO) THEN
          
      --============================================================================--
      --* LLAMADA AL PROCESO QUE CALCULA SCORE DE CUENTAS DE ACUERDO AL CICLO
      MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS_RESUMEN(PV_CICLO    => LV_CICLO,
                                                           PN_HILO     => PN_HILO,
                                                           PD_FECHA_EV => LD_FECHA,
                                                           PV_ERROR    => LV_ERROR);
      --* GRABA LO PENDIENTE
      COMMIT;
    
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;
    
      --============================================================================--
    
      COMMIT;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL_RESUMEN ' ||
                  LV_ERROR;
        
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL_RESUMEN ' ||
                  SQLERRM;
    
  END MMP_PRINCIPAL_RESUMEN;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR
      AUTOR         : SUD VANESSA GUTIERREZ
      PROP�SITO     : Procesa las cuentas buscando en la tabla peri�dica de acuerdo al ciclo de facturaci�n
      CREADO        : 11/06/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_PROCESA_CUENTAS_RESUMEN(PV_CICLO    IN VARCHAR2,
                                        PN_HILO     IN NUMBER,
                                        PD_FECHA_EV IN DATE,
                                        PV_ERROR    OUT VARCHAR2) IS
--=====================================================================================================================
    --    LIDER PDS     : SUD Alex Guaman
    --    AUTOR         : SUD Diego Rogel
    --    22/10/2012 - Se cambio el cursor de control C_CONTROL_CICLO para enviarle el hilo que se esta procesando
--=====================================================================================================================  
    --* VERIFICA EXISTENCIA DE TABLA PERI�DICA
    CURSOR C_VERIFICA_EXISTENCIA(CV_TABLA VARCHAR2) IS
      SELECT COUNT(0) FROM ALL_TABLES A WHERE TABLE_NAME = CV_TABLA;
  
    --* CONTROL DE CICLOS QUE SE HAN PROCESADO
    -- SUD DRO
    CURSOR C_CONTROL_CICLO(CV_CICLO IN VARCHAR2, CD_FECHA_CORTE IN DATE, CV_HILO IN NUMBER) IS
      SELECT I.ESTADO
        FROM MMAG_CONTROL_CICLOS I
       WHERE I.CICLO         = CV_CICLO
         AND I.FECHA_CORTE   = CD_FECHA_CORTE
         AND I.TABLA_PROCESO = 'RESUMEN'
         AND I.HILO          = CV_HILO;     
  
    --
    LT_CUENTA COT_STRING := COT_STRING();
    LT_MORA   COT_STRING := COT_STRING();
  
    --* DECLARACI�N DE VARIABLES
    LV_SENTENCIA   VARCHAR(2000);
    LV_CICLO_BSCS  VARCHAR2(2);
    LV_PER_INI     VARCHAR2(10);
    LV_PER_FIN     VARCHAR2(10);
    LV_PER_EVALUA  VARCHAR2(10);
    LV_TABLA       VARCHAR2(50);
    LV_DESCRIPCION VARCHAR2(100);
    LN_EXISTE      NUMBER;
    --LN_PUNTAJE     NUMBER;
    LN_CONT        NUMBER := 0;
    LN_SCORE_CTA   FLOAT;
    LD_FECHA_PER   DATE;
    LV_ESTADO      VARCHAR2(2);
  
    --* VARIABLES DE ERROR
    LV_ERROR  VARCHAR(2000);
    LV_ERROR2 VARCHAR(2000);
    LE_ERROR EXCEPTION;
    LE_ERROR_SUB EXCEPTION;
    
    -- SUD DRO
    LV_EXISTE      VARCHAR2(2):='S';
   
  BEGIN
  
    --* SE OBTIENE EL PER�ODO DE EVALUACI�N SEGUN EL CICLO
    MMK_CALCULO_SCORE_CUENTA.MMP_OBTIENE_CICLO(PD_FECHA       => PD_FECHA_EV,
                                               PV_CICLO_AXIS  => PV_CICLO,
                                               PV_CICLO_BSCS  => LV_CICLO_BSCS,
                                               PV_PER_INI     => LV_PER_INI,
                                               PV_PER_FIN     => LV_PER_FIN,
                                               PV_DESC_CICLO  => LV_DESCRIPCION,
                                               PV_CODIGOERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    --* OBTIENE FORMATO CORRECTO
    LV_PER_EVALUA := REPLACE(LV_PER_INI, '/', '');
  
    --* OBTIENE EL NOMBRE DE LA TABLA
    LV_TABLA := 'CO_REPCARCLI_' || LV_PER_EVALUA;
    
    --* TRANSFORMA A FORMATO FECHA
    LD_FECHA_PER := TO_DATE(LV_PER_INI, 'DD/MM/YYYY');
  
    --* VERIFICA SI EL CICLO YA FUE PROCESADO
    OPEN C_CONTROL_CICLO(PV_CICLO, LD_FECHA_PER, PN_HILO);
    FETCH C_CONTROL_CICLO
      INTO LV_ESTADO;
    CLOSE C_CONTROL_CICLO;
  
    --* SI YA ESTA PROCESADO, NO CONTINUA
    IF NVL(LV_ESTADO, 'P') = 'F' THEN
      LV_ERROR := 'CICLO DE FACTURACION PARA LA TABLA DE RESULTADO YA ESTA PROCESADO';
      RAISE LE_ERROR;
    END IF;
  
    --* VERIFICA EXISTENCIA DE TABLA PERI�DICA
    OPEN C_VERIFICA_EXISTENCIA(LV_TABLA);
    FETCH C_VERIFICA_EXISTENCIA
      INTO LN_EXISTE;
    CLOSE C_VERIFICA_EXISTENCIA;
  
    --* VALIDA SI EXISTE LA TABLA PERI�DICA
    IF LN_EXISTE = 0 THEN
      LV_ERROR := 'LA TABLA PER�DICA ' || LV_TABLA || ' NO EXISTE!!!';
    
      --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE ESTA PENDIENTE DE PROCESAR
      MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                        PD_FECHA_CORTE => LD_FECHA_PER,
                                                        PV_ESTADO      => 'P',
                                                        PN_HILO        => PN_HILO,
                                                        PV_TABLA       => 'RESUMEN',
                                                        PV_ERROR       => LV_ERROR2);
      IF LV_ERROR2 IS NOT NULL THEN
        LV_ERROR := LV_ERROR || ' Y ' || LV_ERROR2;
        RAISE LE_ERROR;
      END IF;
    
      RAISE LE_ERROR;
    END IF;
  
    --* SE ARMA LA SENTENCIA DIN�MICA                
    LV_SENTENCIA := ' BEGIN ' || ' SELECT C.CUENTA,' || 
                    '        C.MAYORVENCIDO ' || 
                    '        BULK COLLECT INTO :1, :2' ||
                    ' FROM ' || LV_TABLA || ' C' || 
                    ' WHERE SUBSTR(C.CUENTA, -1) = ' || PN_HILO || 
                    ' ;END;';
  
    --* SE REALIZA LA CONSULTA DIN�MICA  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING OUT LT_CUENTA, OUT LT_MORA;
  
    --* SI NO ENCUENTRA DATOS  
    IF LT_CUENTA.COUNT = 0 THEN
      LV_ERROR := 'NO SE ENCONTR� INFORMACI�N EN LA TABLA PERI�DICA ' ||
                  LV_TABLA || '!!!';
    
      --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE ESTA PENDIENTE DE PROCESAR
      MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                        PD_FECHA_CORTE => LD_FECHA_PER,
                                                        PV_ESTADO      => 'P',
                                                        PN_HILO        => PN_HILO,
                                                        PV_TABLA       => 'RESUMEN',
                                                        PV_ERROR       => LV_ERROR2);
    
      IF LV_ERROR2 IS NOT NULL THEN
        LV_ERROR := LV_ERROR || ' Y ' || LV_ERROR2;
        RAISE LE_ERROR;
      END IF;
    
      RAISE LE_ERROR;
    END IF;
  
    --* RECORRE TODOS LOS REGISTROS DE LA TABLA PERI�DICA
    FOR C IN LT_CUENTA.FIRST .. LT_CUENTA.LAST loop      
    
     BEGIN  
           
       --* CALCULA EL PROMEDIO DE LA CUENTA
       MMK_CALCULO_SCORE_CUENTA.MMP_CALCULA_PROMEDIO_RESUMEN(PV_CUENTA      => LT_CUENTA(C),
                                                             PD_FECHA_CORTE => LD_FECHA_PER,
                                                             PV_MORA        => LT_MORA(C),
                                                             --PN_PUNTAJE     => LN_PUNTAJE,
                                                             PN_PROMEDIO    => LN_SCORE_CTA,
                                                             PV_EXISTE      => LV_EXISTE,
                                                             PV_ERROR       => LV_ERROR);
            
       IF LV_ERROR IS NOT NULL THEN
         RAISE LE_ERROR_SUB;
       END IF;
              
      
       --* SE ENVIA A INSERTAR O ACTUALIZAR LA CUENTA EN LA TABLA DE RESUMEN
       MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_RESUMEN_SCORE_CTAS(PD_FECHA_CORTE   => LV_PER_EVALUA,
                                                               PV_CICLO         => PV_CICLO,
                                                               PV_CUENTA        => LT_CUENTA(C),
                                                               PV_MAYOR_VENCIDO => LT_MORA(C),
                                                               PN_PROMEDIO      => LN_SCORE_CTA,
                                                               PV_EXISTE        => LV_EXISTE,
                                                               PV_ERROR         => LV_ERROR);
         
       IF LV_ERROR IS NOT NULL THEN
         RAISE LE_ERROR_SUB;
       END IF;
          
       LN_CONT := LN_CONT + 1;
         
       --* GRABA CADA 500 Y ENCERA EL CONTADOR          
       IF (LN_CONT >= 500) THEN
         COMMIT;
         LN_CONT := 0;
       END IF;
        
     EXCEPTION
       WHEN LE_ERROR_SUB THEN              
         LV_ERROR := 'ERROR ESPEC�FICO MMP_PROCESA_CUENTAS_RESUMEN'||LV_ERROR;
         RAISE LE_ERROR;
        WHEN OTHERS THEN   
         LV_ERROR := 'ERROR EN SUBBLOQUE GENERAL - MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS_RESUMEN, MENSAJE: ' ||
                     SQLERRM;
         RAISE LE_ERROR;
     END;
      
    END LOOP;
  
    --* SE REGISTRA EN LA TABLA DE CONTROL DE CICLOS PARA INDICAR QUE YA FUE PROCESADO
    MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO(PV_CICLO       => PV_CICLO,
                                                      PD_FECHA_CORTE => LD_FECHA_PER,
                                                      PV_ESTADO      => 'F',
                                                      PN_HILO        => PN_HILO,
                                                      PV_TABLA       => 'RESUMEN',
                                                      PV_ERROR       => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    --* GRABA LO PENDIENTE
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;           
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR NO CONTROLADO EN MMK_CALCULO_SCORE_CUENTA.MMP_PROCESA_CUENTAS_RESUMEN - NO SE PUDO PROCESAR EL C�LCULO DEL SCORE DE LAS CUENTAS' ||
                  SQLERRM;
      ROLLBACK;
  END MMP_PROCESA_CUENTAS_RESUMEN;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS    
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR 
      AUTOR         : SUD VANESSA GUTIERREZ 
      PROP�SITO     : Inserta registros en la tabla de resumen de Score Cuentas MMAG_SCORE_CUENTA_RESUMEN
      CREADO        : 04/06/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_INSERTA_RESUMEN_SCORE_CTAS(PD_FECHA_CORTE   IN  VARCHAR2,
                                           PV_CICLO         IN  VARCHAR2,
                                           PV_CUENTA        IN  VARCHAR2,
                                           PV_MAYOR_VENCIDO IN  VARCHAR2,
                                           PN_PROMEDIO      IN  NUMBER,
                                           PV_EXISTE        IN  VARCHAR2,                                           
                                           PV_ERROR         OUT VARCHAR2) IS
    --=====================================================================================================================
        --    LIDER PDS     : SUD Alex Guaman
        --    AUTOR         : SUD Diego Rogel
        --    22/10/2012 - Se cambio el cursor C_CUST_ID para que me devuelva el RUC del cliente
    --===================================================================================================================== 
    -- SUD DRO
    CURSOR C_CUST_ID(CV_CUENTA VARCHAR2) IS
      SELECT I.CUSTOMER_ID, I.CSSOCIALSECNO
        FROM CUSTOMER_ALL I
       WHERE I.CUSTCODE = CV_CUENTA;       
  
    --* DECLARACION DE VARIABLES
    LV_SENTENCIA_INSERT VARCHAR2(5000);
    LV_SENTENCIA_UPDATE VARCHAR2(5000);
    LV_ANIO_TABLA       VARCHAR2(10);
    --LV_ANIO_TABLA_ANT         VARCHAR2(10);
    LV_MES   VARCHAR2(20);
    LV_ERROR VARCHAR2(1000);
    LE_ERROR EXCEPTION;
    --LN_EXISTE        NUMBER;
    LN_CUST_ID       VARCHAR2(20);
    LV_MAYOR_VENCIDO VARCHAR2(4); -- VARIABLE PARA MAYOR VENCIDO '-V
    -- SUD DRO
    LV_RUC CUSTOMER_ALL.CSSOCIALSECNO%TYPE;
  
  BEGIN
  
    --* GUARDO EL VALOR DE MAYOR VENCIDO
    LV_MAYOR_VENCIDO := PV_MAYOR_VENCIDO;
  
    --* VALIDO SI EL MAYOR VENCIDO ES '-V
    IF PV_MAYOR_VENCIDO = '''-V' THEN
      --* LE QUITO LA COMILLA Y LO DEJO SOLO PARA QUE LO INSERTE CON -V
      LV_MAYOR_VENCIDO := SUBSTR('''-V', -2);
    END IF;
  
    --* SE EXTRAE EL A�O AL QUE CORRESPONDE EL REGISTRO
    LV_ANIO_TABLA := TRIM(SUBSTR(PD_FECHA_CORTE, -4));
  
    --* SE EXTRAE EL MES CORRESPONDIENTE AL CAMPO DE LA TABLA
    LV_MES := TRIM(TO_CHAR(TO_DATE(PD_FECHA_CORTE, 'DD/MM/YYYY'),
                           'MONTH',
                           'NLS_DATE_LANGUAGE=SPANISH'));
  
    --* CONSULTO EL CUSTOMER_ID DE LA CUENTA
    -- SUD DRO
    OPEN C_CUST_ID(PV_CUENTA);
    FETCH C_CUST_ID
      INTO LN_CUST_ID, LV_RUC;
    CLOSE C_CUST_ID; 
  
    IF NVL(PV_EXISTE, 'N') <> 'S' THEN
      LN_CUST_ID := NVL(LN_CUST_ID, 0);
      -- SUD DRO
      --* SE ARMA LA SENTENCIA DINAMICA PARA INSERTAR LA CUENTA
      LV_SENTENCIA_INSERT := 'INSERT INTO MMAG_SCORE_CUENTA_RESUMEN nologging ' ||
                             '(ANIO,' || 'CUENTA,' || 'CUSTOMER_ID,' ||
                             LV_MES || ',' || 'PROMEDIO,' || 'ESTADO,' ||
                             'FECHA_CORTE,' || 'CICLO,' || 'FECHA_REGISTRO,' || 'RUC'|| ')' ||
                             'VALUES(''' || LV_ANIO_TABLA || ''',''' ||
                             PV_CUENTA || ''',''' || LN_CUST_ID || ''',''' ||
                             LV_MAYOR_VENCIDO || ''',''' || PN_PROMEDIO ||
                             ''',' || '''A''' || ',' || 'TO_DATE(''' ||
                             PD_FECHA_CORTE || ''',''DD/MM/RRRR''),''' ||
                             PV_CICLO || ''',' || 'SYSDATE,' || '''' || LV_RUC || '''' ||')';
                             
    
      EXECUTE IMMEDIATE LV_SENTENCIA_INSERT;
    
    ELSE
      --Actualiza el ciclo y omite 'where anio = lv_anio_tabla'
      LV_SENTENCIA_UPDATE := 'UPDATE MMAG_SCORE_CUENTA_RESUMEN ' || 'SET ' ||
                             LV_MES || ' = ''' || LV_MAYOR_VENCIDO ||
                             ''', ' || 'ANIO = ''' || LV_ANIO_TABLA ||
                             ''', ' ||' CUSTOMER_ID = NVL('|| LN_CUST_ID ||',CUSTOMER_ID)'||
                             ', ' || 'PROMEDIO = ''' || PN_PROMEDIO ||
                             ''', ' || 'CICLO    = ''' || PV_CICLO ||
                             ''', ' || 'FECHA_CORTE = TO_DATE(''' ||
                             PD_FECHA_CORTE || ''',''DD/MM/RRRR''), ' ||
                             ' FECHA_REGISTRO = SYSDATE ' ||
                             ', ' || 'RUC=' || '''' || LV_RUC || '''' ||
                             ' WHERE CUENTA = ''' || PV_CUENTA || '''';
      EXECUTE IMMEDIATE LV_SENTENCIA_UPDATE;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_RESUMEN_SCORE_CTAS - ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_RESUMEN_SCORE_CTAS ' ||
                  SQLERRM;
  END MMP_INSERTA_RESUMEN_SCORE_CTAS;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS    
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD JOSE SOTOMAYOR 
      AUTOR         : SUD VANESSA GUTIERREZ 
      PROP�SITO     : Inserta registros en la tabla de control MMAG_CONTROL_CICLOS
      CREADO        : 27/06/2012
  ===========================================================================================================--*/
  PROCEDURE MMP_INSERT_CONTROL_CICLO(PV_CICLO       IN VARCHAR2,
                                     PD_FECHA_CORTE IN DATE,                                     
                                     PV_ESTADO      IN VARCHAR2,
                                     PN_HILO        IN NUMBER,
                                     PV_TABLA       IN VARCHAR2,
                                     PV_ERROR       OUT VARCHAR2) IS
                                     
--=====================================================================================================================
    --    LIDER PDS     : SUD Alex Guaman
    --    AUTOR         : SUD Diego Rogel
    --    22/10/2012 - Se cambio el cursor C_CONTROL_CICLO para enviarle el hilo que se esta procesando
--=====================================================================================================================                                     
  
    --* CONTROL DE CICLOS QUE SE HAN PROCESADO
    -- SUD DRO
    CURSOR C_CONTROL_CICLO(CV_CICLO       IN VARCHAR2,
                           CD_FECHA_CORTE IN DATE,
                           CV_TABLA       IN VARCHAR2,
                           CN_HILO        IN NUMBER) IS
      SELECT 1
        FROM MMAG_CONTROL_CICLOS I
       WHERE I.CICLO         = CV_CICLO
         AND I.FECHA_CORTE   = CD_FECHA_CORTE
         AND I.TABLA_PROCESO = CV_TABLA
         AND I.HILO          = CN_HILO;         
  
    --* DECLARACION DE VARIABLES
    LN_EXISTE NUMBER;
  
  BEGIN

    -- SUD DRO  
    --* VERIFICO SI EXISTE REGISTRADO EL CICLO
    OPEN C_CONTROL_CICLO(PV_CICLO, PD_FECHA_CORTE, PV_TABLA, PN_HILO);
    FETCH C_CONTROL_CICLO
      INTO LN_EXISTE;
    CLOSE C_CONTROL_CICLO;
  
    IF NVL(LN_EXISTE, 0) <> 1 THEN
      --* EN CASO DE NO EXISTIR EL REGISTRO SE LO INSERTA
      INSERT INTO MMAG_CONTROL_CICLOS
        (CICLO, FECHA_CORTE, ESTADO, TABLA_PROCESO, HILO)
      VALUES
        (PV_CICLO, PD_FECHA_CORTE, PV_ESTADO, PV_TABLA, PN_HILO);
              
    ELSE
    
      --* SE ACTUALIZA EL REGISTRO CON EL NUEVO ESTADO
      UPDATE MMAG_CONTROL_CICLOS
         SET ESTADO = PV_ESTADO
       WHERE CICLO = PV_CICLO
         AND FECHA_CORTE = PD_FECHA_CORTE
         AND TABLA_PROCESO = PV_TABLA;
        
    END IF;
    
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERT_CONTROL_CICLO ' ||
                  SQLERRM;
    
  END MMP_INSERT_CONTROL_CICLO;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD ALEX GUAMAN
      AUTOR         : SUD CRISTHIAN SILVA
      PROP�SITO     : Proceso de C�lculo de RESULTADO Score Cuentas
      CREADO        : 14/09/2012
  ===========================================================================================================--*/

  PROCEDURE MMP_PRINCIPAL_RESULT(PN_HILO  IN NUMBER,
                                 PD_FECHA IN DATE,
                                 PV_ERROR OUT VARCHAR2) IS
  
    --* DECLARACI�N DE VARIABLES
    LE_ERROR EXCEPTION;
    LV_SENTENCIA VARCHAR2(2000);
  
    --* VARIABLES SCP
    ---------------------------------------------------------------
    --* SCP: C�DIGO GENERADO AUTOMATICAMENTE. DEFINICI�N DE VARIABLES
    ---------------------------------------------------------------
    --LN_ID_BITACORA_SCP            NUMBER := 0;     
    LV_ID_PROCESO_SCP      VARCHAR2(100) := 'CALCULA_SCORE_CUENTAS';
    LV_REFERENCIA_SCP      VARCHAR2(100) := 'MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL_RESULT';
    LN_TOTAL_REGISTROS_SCP NUMBER := 0;
    LV_UNIDAD_REGISTRO_SCP VARCHAR2(30) := 'CUENTAS';
    LN_ERROR_SCP           NUMBER := 0;
    LV_ERROR_SCP           VARCHAR2(500);
    ---------------------------------------------------------------    
    --* DECLARACI�N DE VARIABLES
    LE_ERROR EXCEPTION;
    LV_CICLO          VARCHAR2(3);
    LV_PARAMETRO      VARCHAR2(20) := 'CICLO_FACTURACION';
    LV_TIPO_PARAMETRO VARCHAR2(20) := '6172';
    LV_VALOR_PARAM    VARCHAR2(5);
    LV_DIA_INI_CICLO  VARCHAR2(2);
    LV_DIAS_ESPERA    VARCHAR2(2);
    LV_DIA_EJEC       VARCHAR2(2);
    LV_APLICA_ESPERA  VARCHAR2(2);
    --* CONSULTO EL CICLO CORRESPONDIENTE DE ACUERDO AL DIA EN QUE SE EJECUTA EL PROCESO
    CURSOR C_VERIFICA_CICLO(CD_DIA VARCHAR2, CV_VALOR_PARAM VARCHAR2) IS
      SELECT CICLO, DIA_INI_CICLO
        FROM (SELECT F.ID_CICLO CICLO, F.DIA_INI_CICLO
                FROM FA_CICLOS_BSCS F
               WHERE (to_number(CD_DIA) = TO_NUMBER(F.DIA_INI_CICLO)) --OR to_number(CD_DIA) = 1)
                 AND F.ID_CICLO <> NVL(CV_VALOR_PARAM, 'N')
               ORDER BY F.DIA_INI_CICLO DESC)
       WHERE ROWNUM <= 1;
  
    --* OBTIENE PARAMETROS
    CURSOR C_OBTIENE_PARAMETRO(CD_PARAMETRO      VARCHAR2,
                                CD_TIPO_PARAMETRO VARCHAR2) IS
      SELECT I.VALOR
        FROM GV_PARAMETROS I
       WHERE I.ID_TIPO_PARAMETRO = CD_TIPO_PARAMETRO
         AND I.ID_PARAMETRO = CD_PARAMETRO;
  
  BEGIN
  
    IF PD_FECHA IS NULL THEN
      pv_error := 'Debe ingresar la fecha';
      Return;
    END IF;
  
    --Obtiene ciclo a excluir      
    OPEN C_OBTIENE_PARAMETRO(LV_PARAMETRO, LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_VALOR_PARAM;
    CLOSE C_OBTIENE_PARAMETRO;
  
    --Obtiene dias rango ejecucion      
    OPEN C_OBTIENE_PARAMETRO('DIAS_RANGO_EJECUCION', LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_DIAS_ESPERA;
    CLOSE C_OBTIENE_PARAMETRO;

    --Obtiene bandera aplica espera s/n
    OPEN C_OBTIENE_PARAMETRO('APLICA_ESPERA', LV_TIPO_PARAMETRO);
    FETCH C_OBTIENE_PARAMETRO
      INTO LV_APLICA_ESPERA;
    CLOSE C_OBTIENE_PARAMETRO;
      
    IF LV_APLICA_ESPERA = 'S' THEN
      LV_DIA_EJEC := TRIM(TO_CHAR(PD_FECHA - LV_DIAS_ESPERA - 1, 'DD'));
    ELSE
      LV_DIA_EJEC := TRIM(TO_CHAR(PD_FECHA, 'DD'));
    END IF;
  
    --Obtiene ciclo a procesar seg�n el d�a
    OPEN C_VERIFICA_CICLO(LV_DIA_EJEC, LV_VALOR_PARAM);
    FETCH C_VERIFICA_CICLO
      INTO LV_CICLO, LV_DIA_INI_CICLO;
    CLOSE C_VERIFICA_CICLO;
  
    --14/09/2012       
    IF LV_CICLO IS NULL THEN
      --pv_error := 'No se encontro el ciclo correspondiente a la fecha';
      Return;
    END IF;
  
  IF TO_NUMBER(LV_DIA_EJEC) = TO_NUMBER(LV_DIA_INI_CICLO) THEN
    
    --* SCP: INICIO DE PROCESO ------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_ID_PROCESO_SCP,
                                          LV_REFERENCIA_SCP,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LN_TOTAL_REGISTROS_SCP,
                                          LV_UNIDAD_REGISTRO_SCP,
                                          GN_ID_BITACORA_SCP_RESULTA,
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
    IF LN_ERROR_SCP <> 0 THEN
      PV_ERROR := LV_ERROR_SCP;
      RETURN;
    END IF;
  
    -- SCP: BITACORA DE INICIO DE PROCESO --------------------------------------
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                          'INICIO C�LCULO SCORE CUENTAS PARA MMAG_RESUL_SCORE_CUENTA',
                                          NULL,
                                          NULL,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'S',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    --*TRUNCA LA TABLA
    --LV_SENTENCIA := 'TRUNCATE TABLE MMAG_RESUL_SCORE_CUENTA';
    LV_SENTENCIA := 'DELETE FROM MMAG_RESUL_SCORE_CUENTA WHERE SUBSTR(CUENTA,-1) = ' ||
                    PN_HILO;
  
    EXECUTE IMMEDIATE LV_SENTENCIA;
    COMMIT;
  
    --INSERTA SCORE CUENTAS 
    MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_RESULT_SCORE_CTAS(PN_HILO,
                                                           PV_ERROR);
  
    IF PV_ERROR IS NOT NULL THEN
      RETURN;
    END IF;
    -- SCP: BITACORA DE FINALIZACION DE PROCESO --------------------------------
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                          'FIN C�LCULO SCORE CUENTAS PARA MMAG_RESUL_SCORE_CUENTA',
                                          NULL,
                                          NULL,
                                          0,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'S',
                                          LN_ERROR_SCP,
                                          LV_ERROR_SCP);
  
    --* GRABA LO PENDIENTE
    COMMIT;
  END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL_RESULT ' ||
                  SQLERRM;
    
      -- SCP: BITACORA DE ERRORES EN PROCESO ----------------------------------------- 
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(GN_ID_BITACORA_SCP_RESULTA,
                                            'ERROR GENERAL EN PROCESO PRINCIPAL',
                                            PV_ERROR,
                                            NULL,
                                            2,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'S',
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
    
  END MMP_PRINCIPAL_RESULT;

  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD ALEX GUAMAN
      AUTOR         : SUD CRISTHIAN SILVA
      PROP�SITO     : Realiza la inserci�n en la Estructura Principal de C�lculo de Score Cuenta
      CREADO        : 17/09/2012
  ===========================================================================================================--*/

  PROCEDURE MMP_INSERTA_RESULT_SCORE_CTAS(PN_HILO  IN NUMBER,
                                          PV_ERROR OUT VARCHAR2) IS
    --=====================================================================================================================
        --    LIDER PDS     : SUD Alex Guaman
        --    AUTOR         : SUD Diego Rogel
        --    22/10/2012 - Se cambio el cursor C_SCORE_CUENTA_RESUMEN para que me devuelva el RUC del cliente
    --=====================================================================================================================
    -- SUD DRO
    --* OBTIENE DATOS DE TABLA RESUMEN POR HILOS
    CURSOR C_SCORE_CUENTA_RESUMEN(CN_HILO NUMBER) IS
      SELECT Cuenta, Customer_Id, ciclo, Promedio, Fecha_Corte, RUC
        FROM Mmag_Score_Cuenta_Resumen
       WHERE SUBSTR(Cuenta, -1) = CN_HILO;
  
    ---------------------------------------------
    --         VARIABLES TIPO ARREGLO          --
    --------------------------------------------
    LTV_CUENTA      COT_STRING := COT_STRING();
    LTN_CUSTOMER_ID COT_NUMBER := COT_NUMBER();
    LTV_CICLO       COT_STRING := COT_STRING();
    LTN_PROMEDIO    COT_NUMBER := COT_NUMBER();
    LTD_FECHA_CORTE COT_FECHA := COT_FECHA();
    -- SUD DRO
    LTV_RUC         COT_STRING := COT_STRING();
  
    --* DECLARACI�N DE VARIABLES  
    LE_ERROR EXCEPTION;
    LV_ERROR    VARCHAR2(1000);
    LN_CONTADOR NUMBER;
  BEGIN
    -- INSERTA EN TABLA RESULTADO
    LN_CONTADOR := 0;
  
    OPEN C_SCORE_CUENTA_RESUMEN(PN_HILO);
    FETCH C_SCORE_CUENTA_RESUMEN BULK COLLECT
      INTO LTV_CUENTA,
           LTN_CUSTOMER_ID,
           LTV_CICLO,
           LTN_PROMEDIO,
           LTD_FECHA_CORTE,
           LTV_RUC;
    CLOSE C_SCORE_CUENTA_RESUMEN;
  
    IF LTV_CUENTA.COUNT > 0 THEN
    
      FOR C IN LTV_CUENTA.FIRST .. LTV_CUENTA.LAST LOOP      
         -- SUD DRO  
         Insert into mmag_resul_score_cuenta
          (CUENTA,
           CUSTOMER_ID,
           CICLO,
           PROMEDIO,
           FECHA_CORTE,
           FECHA_REGISTRO,
           RUC)
        VALUES
          (LTV_CUENTA(C),
           LTN_CUSTOMER_ID(C),
           LTV_CICLO(C),
           LTN_PROMEDIO(C),
           LTD_FECHA_CORTE(C),
           SYSDATE,
           LTV_RUC(C));  

        LN_CONTADOR := LN_CONTADOR + 1;

        IF LN_CONTADOR >= 10000 THEN
          COMMIT;
          LN_CONTADOR := 0;
        END IF;
      END LOOP;
    
    ELSE
      LV_ERROR := 'NO SE ENCONTRO INFORMACION EN LA TABLA MMAG_SCORE_CUENTA_RESUMEN PARA EL HILO ' ||
                  PN_HILO;
      RAISE LE_ERROR;
    END IF;
    commit;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := 'ERROR EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_SCORE_CTAS - ' ||
                  LV_ERROR;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR GENERAL EN MMK_CALCULO_SCORE_CUENTA.MMP_INSERTA_SCORE_CTAS ' ||
                  SQLERRM;
    
  END MMP_INSERTA_RESULT_SCORE_CTAS;


  /*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD ALEX GUAMAN
      AUTOR         : SUD DIEGO ROGEL
      PROP�SITO     : Obtener la calificaci�n de la mora por cada mes ingresado al procedimiento.
      CREADO        : 22/10/2012
  ===========================================================================================================--*/
  -- SUD DRO
  PROCEDURE MMP_OBTIENE_PUNTAJE_MORA(PV_MORA         IN VARCHAR2,
                                     PV_EQUIVALENCIA OUT NUMBER,
                                     PV_ERROR        OUT VARCHAR2) IS
    -- CALIFICACI�N SEG�N LA MORA
    CURSOR C_PUNTAJES(CV_MORA VARCHAR2) IS
      SELECT P.PUNTAJE 
        FROM MMAG_PUNTAJES_MORA P 
       WHERE P.MORA = CV_MORA;

    -- VARIABLES LOCALES
    LB_NOTFOUND BOOLEAN;
    LE_ERROR    EXCEPTION;
  BEGIN
    -- SE OBTIENE CALIFICACI�N
    OPEN C_PUNTAJES(PV_MORA);
    FETCH C_PUNTAJES
      INTO PV_EQUIVALENCIA;
    LB_NOTFOUND := C_PUNTAJES%NOTFOUND;
    CLOSE C_PUNTAJES;
  
    IF LB_NOTFOUND THEN
      RAISE LE_ERROR;
    END IF;
    
  EXCEPTION
    WHEN LE_ERROR THEN
      --PV_ERROR := 'NO SE ENCONTR� CALIFICACI�N PARA LA MORA ' || PV_MORA || ', REVISE CONFIGURACION EN MMAG_PUNTAJES_MORA';
      PV_EQUIVALENCIA := 0;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
      PV_EQUIVALENCIA := 0;
  END MMP_OBTIENE_PUNTAJE_MORA;

/*=========================================================================================================--
      PROYECTO      : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
      LIDER SIS     : SIS ALAN PAB�
      LIDER PDS     : SUD Alex Guaman
      AUTOR         : SUD Ericka Flores
      PROP�SITO     : Procedimiento para consultar CARTERA VENCIDA O CASTIGADA de un Cliente
      CREADO        : 10/04/2013
===========================================================================================================--*/
  
procedure MMP_VERIFICA_CASTIGO_CARTERA(pv_cuenta in varchar2,
                                       pn_saldo out varchar2,
                                       pv_error out varchar2) is
  
 cursor c_cartera_vencida(cv_cuenta varchar2) is
   select t.cuen_sald saldo
     from cart_cuentas_dat@fingye_burocred t /*dblink prod*/
     --from cart_cuentas_dat@gye t /*dblink desa*/
    where cuen_nume_cuen = cv_cuenta;
    
 lc_cartera_vencida c_cartera_vencida%rowtype;
 lb_encuentra       boolean := false;
  
  begin
  
  --Consulto si un cliente tiene deuda castigada o vencida  
   open c_cartera_vencida(pv_cuenta);
  fetch c_cartera_vencida
   into lc_cartera_vencida;
        lb_encuentra := c_cartera_vencida%found;
  close c_cartera_vencida;
  
  if lb_encuentra then
    pn_saldo := lc_cartera_vencida.saldo;
  else
    pn_saldo := 0;
  end if;
  
 exception 
   when others then
     pv_error := 'Error General: MMP_VERIFICA_CASTIGO_CARTERA '||sqlerrm;
   
end;

END MMK_CALCULO_SCORE_CUENTA;
/
