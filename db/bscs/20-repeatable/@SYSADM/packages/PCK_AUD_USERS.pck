CREATE OR REPLACE PACKAGE PCK_AUD_USERS IS

--=============================================================================
--  Autor     : SIS Ronny Naranjo
--  Fecha     : 31/01/2007
--  Proyecto  : Evitar cambio de password desde KV - BSCS
--  Descripción :  Registro de datos en tabla sysadm.aud_users
--============================================================================= 

 PROCEDURE REGISTRANDO_DATOS(pv_username    varchar2,
                             pv_program     varchar2,
                             pv_module      varchar2,
                             pv_machine     varchar2,
                             pv_osuser       VARCHAR2,
                             pv_ip_address   varchar2                           
                             );

 PROCEDURE DEPURANDO_DATOS(pn_dias      IN NUMBER);

END PCK_AUD_USERS;
/
CREATE OR REPLACE PACKAGE BODY PCK_AUD_USERS IS

--=============================================================================
--  Autor     : SIS Ronny Naranjo
--  Fecha     : 31/01/2007
--  Proyecto  : Evitar cambio de password desde KV - BSCS
--  Descripción :  Registro de datos en tabla sysadm.aud_users
--=============================================================================  
  --
  
 PROCEDURE REGISTRANDO_DATOS(pv_username    varchar2,
                             pv_program     varchar2,
                             pv_module      varchar2,
                             pv_machine     varchar2,
                             pv_osuser       VARCHAR2,
                             pv_ip_address   varchar2)
                             IS
  --
  PRAGMA AUTONOMOUS_TRANSACTION;
  --
  BEGIN
    -----------------------------------------------------------------------------------------
    -- Inserta en la tabla de auditoria los registros a controlar
    -----------------------------------------------------------------------------------------
     insert into sysadm.aud_users(usuario,
                           fecha,
                           programa,
                           modulo,
                           machine,
                           osuser,
                           ipaddress)
                    values(pv_username,
                           sysdate,
                           pv_program,
                           pv_module,
                           pv_machine,
                           pv_osuser,
                           pv_ip_address);
    --
    COMMIT;
    --
  end;
  --==================================================================================================

 PROCEDURE DEPURANDO_DATOS(pn_dias      IN NUMBER) IS
     
  lv_sql      VARCHAR2(50);
  BEGIN  
      -----------------------------------------------------------------------------------------
      -- Eliminar datos
      -----------------------------------------------------------------------------------------
      DELETE aud_users WHERE fecha <= SYSDATE -pn_dias;
      --    
      COMMIT;
      --
      lv_sql := 'analyze table aud_users estimate statistics';
      --
      execute immediate lv_sql;
      --
      lv_sql:=SQLERRM;
      
  end;
  --==================================================================================================
END PCK_AUD_USERS;
/

