CREATE OR REPLACE PACKAGE MFK_OBJ_ENVIO_EJECUCIONES
 /* 
 ||   Name       : MFK_OBJ_ENVIO_EJECUCIONES
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by Sistem Department CONECEL S.A. 2003
 */ 
 IS 
 /*HELP 
   Overview of MFK_OBJ_ENVIO_EJECUCIONES
   HELP*/ 
 
 /*EXAMPLES 
   Examples of test 
   EXAMPLES*/ 
 
 /* Constants */ 
 
 /* Exceptions */ 
 
 /* Variables */ 
 
 /* Toggles */ 
 
 /* Windows */ 
 
 /* Programs */ 
 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL); 

PROCEDURE MFP_INSERTAR(PN_IDENVIO                    IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                       PD_FECHA_EJECUCION            IN  MF_ENVIO_EJECUCIONES.FECHA_EJECUCION%TYPE,
                       PD_FECHA_INICIO               IN  MF_ENVIO_EJECUCIONES.FECHA_INICIO%TYPE,
                       PD_FECHA_FIN                  IN  MF_ENVIO_EJECUCIONES.FECHA_FIN%TYPE,
                       PV_MENSAJE_PROCESO            IN  MF_ENVIO_EJECUCIONES.MENSAJE_PROCESO%TYPE,
                       PN_TOT_MENSAJES_ENVIADOS      IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_ENVIADOS%TYPE,
                       PN_TOT_MENSAJES_NO_ENVIADOS   IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_NO_ENVIADOS%TYPE DEFAULT NULL, --10798 SUD DPI 16-11-2016
                       PN_SECUENCIA                  IN OUT MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                       PV_MSGERROR                   IN OUT VARCHAR2 ,
                           PV_ESTADO_ENVIO In Varchar2 Default Null
--
) ; 
--
--
PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO                    IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                         PN_SECUENCIA                  IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                         PV_ESTADO_ENVIO               IN  MF_ENVIO_EJECUCIONES.ESTADO_ENVIO%TYPE,
                         PD_FECHA_EJECUCION            IN  MF_ENVIO_EJECUCIONES.FECHA_EJECUCION%TYPE,
                         PD_FECHA_INICIO               IN  MF_ENVIO_EJECUCIONES.FECHA_INICIO%TYPE,
                         PD_FECHA_FIN                  IN  MF_ENVIO_EJECUCIONES.FECHA_FIN%TYPE,
                         PV_MENSAJE_PROCESO            IN  MF_ENVIO_EJECUCIONES.MENSAJE_PROCESO%TYPE,
                         PN_TOT_MENSAJES_ENVIADOS      IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_ENVIADOS%TYPE,
                         PN_TOT_MENSAJES_NO_ENVIADOS   IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_NO_ENVIADOS%TYPE DEFAULT NULL, --10798 SUD DPI 16-11-2016
                         PV_MSGERROR                  IN  OUT VARCHAR2); 
 
--
PROCEDURE MFP_ELIMINAR(PN_IDENVIO        IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                       PN_SECUENCIA      IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE, 
                       PV_MSG_ERROR      IN  OUT VARCHAR2); 
--
--
PROCEDURE MFP_ELIMINACION_FISICA(PN_IDENVIO        IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                                 PN_SECUENCIA      IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE, 
                                 PV_MSG_ERROR      IN  OUT VARCHAR2);
END MFK_OBJ_ENVIO_EJECUCIONES ;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_ENVIO_EJECUCIONES
 /* 
 ||   Name       : MFK_OBJ_ENVIO_EJECUCIONES
 ||   Created on : 11-ABR-05
 ||   Comments   : Package Body automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 IS 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL) is 
BEGIN
 NULL;
END MFP_HELP; 

PROCEDURE MFP_INSERTAR(PN_IDENVIO                    IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                       PD_FECHA_EJECUCION            IN  MF_ENVIO_EJECUCIONES.FECHA_EJECUCION%TYPE,
                       PD_FECHA_INICIO               IN  MF_ENVIO_EJECUCIONES.FECHA_INICIO%TYPE,
                       PD_FECHA_FIN                  IN  MF_ENVIO_EJECUCIONES.FECHA_FIN%TYPE,
                       PV_MENSAJE_PROCESO            IN  MF_ENVIO_EJECUCIONES.MENSAJE_PROCESO%TYPE,
                       PN_TOT_MENSAJES_ENVIADOS      IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_ENVIADOS%TYPE,
                       PN_TOT_MENSAJES_NO_ENVIADOS   IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_NO_ENVIADOS%TYPE DEFAULT NULL, --10798 SUD DPI 16-11-2016
                       PN_SECUENCIA                  IN OUT MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                       PV_MSGERROR                  IN  OUT VARCHAR2,
                       PV_ESTADO_ENVIO In Varchar2 Default Null
--
) 
IS  
 /* 
 ||   Name       : MFP_INSERTAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES.MFP_INSERTAR';
 LN_SECUENCIA                MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE;
 LV_ESTADO_ENVIO             MF_ENVIO_EJECUCIONES.ESTADO_ENVIO%TYPE;
 lv_estado  MF_ENVIO_EJECUCIONES.ESTADO%Type;                    
BEGIN

  SELECT MFS_ENVIOEJECUCION_SEC.NEXTVAL INTO ln_secuencia FROM dual;
  LV_ESTADO_ENVIO := 'P'; --Significa Pendiente
  If nvl(PV_ESTADO_ENVIO,'P')='R' Then
    lv_estado:=Null;
  Else
    lv_estado:='A';  
  End If;

 INSERT INTO MF_ENVIO_EJECUCIONES(IDENVIO,SECUENCIA,ESTADO_ENVIO,FECHA_EJECUCION,
                                  FECHA_INICIO,FECHA_FIN,
                                  MENSAJE_PROCESO,TOT_MENSAJES_ENVIADOS,TOT_MENSAJES_NO_ENVIADOS,ESTADO)
 VALUES (PN_IDENVIO,
         LN_SECUENCIA,
         NVL(PV_ESTADO_ENVIO,LV_ESTADO_ENVIO),
         PD_FECHA_EJECUCION,
         PD_FECHA_INICIO,
         PD_FECHA_FIN,
         PV_MENSAJE_PROCESO,
         PN_TOT_MENSAJES_ENVIADOS,
         PN_TOT_MENSAJES_NO_ENVIADOS, --10798 SUD DPI 16-11-2016
         lv_estado);
         
 PN_SECUENCIA := ln_secuencia;        

EXCEPTION -- Exception 
WHEN OTHERS THEN
    PV_MSGERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
END MFP_INSERTAR;
 

PROCEDURE MFP_ACTUALIZAR(PN_IDENVIO                    IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                         PN_SECUENCIA                  IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                         PV_ESTADO_ENVIO               IN  MF_ENVIO_EJECUCIONES.ESTADO_ENVIO%TYPE,
                         PD_FECHA_EJECUCION            IN  MF_ENVIO_EJECUCIONES.FECHA_EJECUCION%TYPE,
                         PD_FECHA_INICIO               IN  MF_ENVIO_EJECUCIONES.FECHA_INICIO%TYPE,
                         PD_FECHA_FIN                  IN  MF_ENVIO_EJECUCIONES.FECHA_FIN%TYPE,
                         PV_MENSAJE_PROCESO            IN  MF_ENVIO_EJECUCIONES.MENSAJE_PROCESO%TYPE,
                         PN_TOT_MENSAJES_ENVIADOS      IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_ENVIADOS%TYPE,
                         PN_TOT_MENSAJES_NO_ENVIADOS   IN  MF_ENVIO_EJECUCIONES.TOT_MENSAJES_NO_ENVIADOS%TYPE DEFAULT NULL, --10798 SUD DPI 16-11-2016
                         PV_MSGERROR                  IN  OUT VARCHAR2) 
AS  
 /* 
 ||   Name       : MFP_ACTUALIZAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 CURSOR C_table_cur(Cn_idEnvio      number,
                    Cn_secuencia    number) IS 
    SELECT * FROM MF_ENVIO_EJECUCIONES EX
    WHERE EX.IDENVIO = cn_idEnvio
    AND EX.SECUENCIA = cn_secuencia;
     
 Lc_CurrentRow     MF_ENVIO_EJECUCIONES%ROWTYPE ;
 Le_NoDataUpdated  EXCEPTION;

 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR';
BEGIN

 OPEN C_table_cur(PN_IDENVIO,PN_SECUENCIA);
 FETCH C_table_cur INTO Lc_CurrentRow ;
 IF C_table_cur%NOTFOUND THEN 
    RAISE Le_NoDataUpdated ;
 END IF;

 UPDATE MF_ENVIO_EJECUCIONES SET -- Revisar los campos que desea actualizar
         ESTADO_ENVIO = NVL(PV_ESTADO_ENVIO, ESTADO_ENVIO)
        ,FECHA_INICIO = NVL(PD_FECHA_INICIO, FECHA_INICIO)
        ,FECHA_FIN = NVL(PD_FECHA_FIN, FECHA_FIN)
        ,MENSAJE_PROCESO = NVL(PV_MENSAJE_PROCESO,MENSAJE_PROCESO)
        ,TOT_MENSAJES_ENVIADOS = NVL(PN_TOT_MENSAJES_ENVIADOS,TOT_MENSAJES_ENVIADOS)
        ,TOT_MENSAJES_NO_ENVIADOS = NVL(PN_TOT_MENSAJES_NO_ENVIADOS,TOT_MENSAJES_NO_ENVIADOS) --10798 SUD DPI 16-11-2016
        ,FECHA_EJECUCION = NVL(PD_FECHA_EJECUCION,FECHA_EJECUCION)
 WHERE IDENVIO = PN_IDENVIO
 AND SECUENCIA = PN_SECUENCIA;

 CLOSE C_table_cur ;

EXCEPTION -- Exception 
WHEN Le_NoDataUpdated THEN
    PV_MSGERROR := 'No se encontro el registro que desea actualizar. '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
WHEN OTHERS THEN
    PV_MSGERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END MFP_ACTUALIZAR;
 
PROCEDURE MFP_ELIMINAR(PN_IDENVIO        IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                       PN_SECUENCIA      IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE, 
                       PV_MSG_ERROR      IN  OUT VARCHAR2) 
AS  
 /* 
 ||   Name       : MFP_ELIMINAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003
 */ 
 CURSOR C_table_cur(Cn_idEnvio    Number,
                    Cn_secuencia  Number) IS 
     SELECT * FROM MF_ENVIO_EJECUCIONES
     WHERE  IDENVIO = Cn_idEnvio 
     AND SECUENCIA = Cn_secuencia;
     
 Lc_CurrentRow     MF_ENVIO_EJECUCIONES%ROWTYPE ;
 Le_NoDataDeleted  EXCEPTION;
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES.MFP_ELIMINAR';
BEGIN

 PV_MSG_ERROR := NULL;
 OPEN C_table_cur(PN_IDENVIO,PN_SECUENCIA) ;
 FETCH C_table_cur INTO Lc_CurrentRow ;
 IF C_table_cur%NOTFOUND THEN 
    RAISE Le_NoDataDeleted ;
 END IF;

 DELETE FROM MF_ENVIO_EJECUCIONES
 WHERE IDENVIO = PN_IDENVIO
 AND SECUENCIA = PN_SECUENCIA; 

 CLOSE C_table_cur ;

EXCEPTION -- Exception 
 WHEN Le_NoDataDeleted  THEN
    PV_MSG_ERROR := 'El registro que desea borrar no existe. '||LV_APLICACION; 
    CLOSE C_table_cur ;
 WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;
END MFP_ELIMINAR;


PROCEDURE MFP_ELIMINACION_FISICA(PN_IDENVIO        IN  MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                                 PN_SECUENCIA      IN  MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE, 
                                 PV_MSG_ERROR      IN  OUT VARCHAR2) 
AS  
 /* 
 ||   Name       : MFP_ELIMINAR
 ||   Created on : 11-ABR-05
 ||   Comments   : Standalone Delete Procedure 
 ||   automatically generated using the PL/Vision building blocks 
 ||   http://www.stevenfeuerstein.com/puter/gencentral.htm 
 ||   Adjusted by System Department CONECEL S.A. 2003

 */ 
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_ENVIO_EJECUCIONES.MFP_ELIMINACION_FISICA';
BEGIN
 
 /*UPDATE MF_ENVIO_EJECUCIONES SET ESTADO = 'I'
 WHERE IDENVIO = PN_IDENVIO
 AND SECUENCIA = PN_SECUENCIA;*/
 
 DELETE FROM MF_ENVIO_EJECUCIONES
 WHERE IDENVIO = PN_IDENVIO
 AND SECUENCIA = PN_SECUENCIA; 

EXCEPTION -- Exception 
 WHEN OTHERS THEN
    PV_MSG_ERROR := 'Ocurrio el siguiente error '||SUBSTR(SQLERRM,1,250)||'. '||LV_APLICACION;
END MFP_ELIMINACION_FISICA;
--

END MFK_OBJ_ENVIO_EJECUCIONES ;
/
