create or replace package COK_DEPURACION_TABLAS is

  -- Author  : ARTURO GAMBOA
  -- Created : 08/06/2010 10:08:39
  -- Purpose : Procedimientos que permiten eliminar estructuras
  
  -- Public type declarations

  
  
PROCEDURE COP_DEPURA_TABLAS_CARGADAS(PV_TABLA       IN VARCHAR2,
                                     PD_FECHA       IN DATE,
                                     --PD_FECHA_DESDE IN DATE,
                                     PV_ERROR       OUT VARCHAR2);

end COK_DEPURACION_TABLAS;
/
create or replace package body COK_DEPURACION_TABLAS is

PROCEDURE COP_DEPURA_TABLAS_CARGADAS(PV_TABLA IN VARCHAR2,
                                     PD_FECHA IN DATE,
                                    --- PD_FECHA_DESDE IN DATE,
                                     Pv_Error OUT VARCHAR2) IS
    --===============================================================================================================================
    -- Author: SUD Arturo Gamboa Carey
    -- Proyecto: [4735] Modificaci�n al reporte de Detalle de Clientes
    -- Proposito: Procedimiento encargado de depurar las tablas que ya no se utilizan en los procedimientos co_reporte_adic
    --===============================================================================================================================
    -- Cursor encargado de obtener las CO_REPORTE_ADIC_ que fueron exportadas desde BSCS
    CURSOR C_REPORTE_ADIC(CV_TABLA varchar2,cd_fecha_desde date,Cd_Fecha_hasta DATE) IS
      SELECT object_name
        FROM all_objects
       WHERE object_type = 'TABLE'
         AND object_name LIKE ''||CV_TABLA||'%'
         AND TRUNC(CREATED) BETWEEN TRUNC(cd_fecha_desde) AND Cd_Fecha_hasta;
         --AND to_date(substr(object_name, 17, 24), 'dd/mm/yyyy') < Cd_Fecha_Anterior; --to_date('01/01/2008','dd/mm/yyyy')

    CURSOR C_REPORTE_ADIC2(CV_TABLA varchar2,cd_fecha_desde date,Cd_Fecha_hasta DATE) IS
      SELECT COUNT(object_name)
        FROM all_objects
       WHERE object_type = 'TABLE'
         AND object_name LIKE ''||CV_TABLA||'%'
         AND TRUNC(CREATED) BETWEEN TRUNC(cd_fecha_desde) AND Cd_Fecha_hasta;

    cursor c_obtiene_parametro(cv_parametro varchar2) is
       select to_number(valor) from gv_parametros where id_parametro=  cv_parametro;




  lv_proceso           varchar2(30) := 'DEPURACION_BSCS';
  lv_parametro_detener varchar2(30)  := 'DETENER_DEPURACION';
  lv_valor_detener     varchar2(1);
  lv_desc_detener      varchar2(5000);
  ln_contador          number:=0;
  ln_bitacora          number;
  ln_error             number;
  ln_errores           number;
  lv_MensajeAccion     varchar2(4000);
  lv_MensajeTecnico    varchar2(4000);
  lv_errorAplica     varchar2(100);
  ln_nivelError      varchar2(50) := '0';
  ln_exitos          number := 0;
    
    Ld_Fecha_hasta    DATE;
    Ld_fecha_desde    DATE;
    Ld_fecha_desde_axis  DATE;
    Lv_Sentencia      VARCHAR2(2000):=null;
    Ln_rango          number;
    Ln_rango_axis     number;
    Le_MiExecption    exception;
    mi_error          exception; 
    LV_ERROR VARCHAR2(2000):=NULL;
   
      
    --C_tablas C_REPORTE_ADIC%rowtype;
    lv_Programa          varchar2(2000) := 'DEPURACION ';
  BEGIN
    
    
    -- Obteniendo la fecha de hace 3 meses para depuracion de las tablas exportadas
     IF PV_TABLA IS NULL THEN 
     LV_ERROR:='Debe ingresar la tabla a borrar. ';
     RAISE Le_MiExecption;
     ELSIF  PD_FECHA IS NULL THEN
     LV_ERROR:='Debe ingresar la fecha del rango';
     RAISE mi_error;
     END IF;
    
    open c_obtiene_parametro('RANGO_DEPURACION_BSCS');
    fetch c_obtiene_parametro
      into ln_rango;
    close c_obtiene_parametro;
    
    open c_obtiene_parametro('RANGO_DEPURACION_AXIS');
    fetch c_obtiene_parametro
    into Ln_rango_axis;
    close c_obtiene_parametro;

    
     Ld_Fecha_hasta  := trunc(PD_FECHA);
     Ld_fecha_desde  := TRUNC(PD_FECHA - ln_rango);
     Ld_fecha_desde_axis  := TRUNC(PD_FECHA - Ln_rango_axis);
    -- Obteniendo las CO_REPORTE_ADIC_ para eliminarlas
    
    OPEN  C_REPORTE_ADIC2(PV_TABLA,Ld_fecha_desde,Ld_Fecha_hasta);
    FETCH C_REPORTE_ADIC2 INTO ln_contador;
    close C_REPORTE_ADIC2;
    
 
    lv_Programa:=lv_Programa  ||' DESDE: '||TO_CHAR(Ld_fecha_desde,'DD/MM/YYYY')||' HASTA :'||TO_CHAR(Ld_Fecha_hasta,'DD/MM/YYYY');
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso        => lv_proceso,
                                              pv_referencia        => lv_Programa,
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => null,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => ln_contador, --cuantos registros se procesar�n
                                              pv_unidad_registro   => 'registros',
                                              pn_id_bitacora       => ln_bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => LV_ERROR);

        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            LV_ERROR:='ERROR SCP';
            raise Le_MiExecption;
        end if;
  
    BEGIN  
    
          FOR REPORT IN C_REPORTE_ADIC(PV_TABLA,Ld_fecha_desde,Ld_Fecha_hasta) LOOP
            -- Preparando la sentencia para eliminar las tablas CO_REPORTE_ADIC_
            begin 
                
                 SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_detener,
                                                                                pv_id_proceso   => lv_proceso,
                                                                                pv_valor        => lv_valor_detener,
                                                                                pv_descripcion  => lv_desc_detener,
                                                                                pn_error        => ln_error,
                                                                                pv_error        => Pv_Error);
                                                                                
                                                                                
                  if lv_valor_detener = 'S' then
                      EXIT;
                      ---RAISE error_salida;
                  end if;
                Lv_Sentencia := 'DROP TABLE ' || REPORT.object_name;
                dbms_output.put_line(Lv_Sentencia);
                EXECUTE IMMEDIATE Lv_Sentencia;
            
               SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                    pv_mensaje_aplicacion => 'SE BORRO TABLA '||REPORT.object_name,
                                                    pv_mensaje_tecnico    => Lv_Sentencia,
                                                    pv_mensaje_accion     => lv_MensajeAccion,
                                                    pn_nivel_error        => 1,
                                                    pv_cod_aux_1          => null,
                                                    pv_cod_aux_2          => null,
                                                    pv_cod_aux_3          => null,
                                                    pn_cod_aux_4          => null,
                                                    pn_cod_aux_5          => null,
                                                    pv_notifica           => 'S',
                                                    pn_error              => ln_error,
                                                    pv_error              => Pv_Error);
              --   ln_procesados:=ln_procesados+1;
                 ln_exitos:=ln_exitos+1;  
                
                    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                           pn_registros_procesados => ln_exitos,
                                                           pn_registros_error      => ln_nivelError,
                                                           pn_error                => ln_error,
                                                           pv_error                => Pv_Error);                                            
                                                  
            exception
            when others then
             ln_nivelError := ln_nivelError+1;
             SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                    pv_mensaje_aplicacion => 'ERROR AL BORRAR TABLA '||REPORT.object_name,
                                                    pv_mensaje_tecnico    => Lv_Sentencia,
                                                    pv_mensaje_accion     => SQLERRM,
                                                    pn_nivel_error        => 3,
                                                    pv_cod_aux_1          => null,
                                                    pv_cod_aux_2          => null,
                                                    pv_cod_aux_3          => null,
                                                    pn_cod_aux_4          => null,
                                                    pn_cod_aux_5          => null,
                                                    pv_notifica           => 'S',
                                                    pn_error              => ln_error,
                                                    pv_error              => Pv_Error);
                                                    
             SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                           pn_registros_procesados => ln_exitos,
                                                           pn_registros_error      => ln_nivelError,
                                                           pn_error                => ln_error,
                                                           pv_error                => Pv_Error);                                               
            end;
          END LOOP;
    
    EXCEPTION
     WHEN OTHERS THEN
        LV_ERROR := 'Error al eliminar la estructura ingresada'||SQLERRM;
      
      
    END;
      
      if lv_valor_detener = 'S' then
         LV_ERROR:='PROGRAMA DETENIDO DESDE EL SCP';
         RAISE Le_MiExecption;
      end if;
      
      BEGIN 
          COK_DEPURACION_DETCLI_AXIS.COP_DEPURA_TABLAS_CARGADAS(PV_TABLA ,
                                                         PD_FECHA ,
                                                         Ld_fecha_desde_axis ,
                                                         Pv_Error );   
    
        IF Pv_Error IS NOT NULL THEN
         LV_ERROR:= 'ADVERTENCIA EN DEPURACION HACIA AXIS VERIFICAR SCP ID_PROCESO-DEPURACION_AXIS '|| Pv_Error;
         RAISE mi_error;
        END IF;  
     EXCEPTION 
        WHEN mi_error THEN
           RAISE mi_error;
      WHEN OTHERS THEN
          LV_ERROR:='ADVERTENCIA EN DEPURACION HACIA AXIS VERIFICAR SCP ID_PROCESO-DEPURACION_AXIS '||Pv_Error ||SQLERRM;
         RAISE Le_MiExecption;
       
     
     END;
     
     if Lv_Sentencia is null then
        raise NO_DATA_FOUND;
      end if;
     
      -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);

   
    
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
     ln_nivelError := ln_nivelError+1;
     LV_ERROR:='NO EXISTE LA ESTRUCTURA EN BSCS INGRESADA '||PV_TABLA;
     
     --- INSERTA DETALLE EN LA BITACORA
     
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => LV_ERROR,
                                            pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                            pv_mensaje_accion     => lv_MensajeAccion,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error,
                                            pv_error              => Pv_Error);
     
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_nivelError,
                                            pn_error                => ln_error,
                                            pv_error                => Pv_Error); 
                                            
                                               
     
     
       -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
      
     
     Pv_Error:=LV_ERROR;
     
     when mi_error then
      
       ln_nivelError := ln_nivelError+1;
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => LV_ERROR,
                                            pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                            pv_mensaje_accion     => lv_MensajeAccion,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error,
                                            pv_error              => Pv_Error);
     
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_nivelError,
                                            pn_error                => ln_error,
                                            pv_error                => Pv_Error); 
                                            
     
      
       -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
       
     pv_error:= LV_ERROR;
     
    WHEN Le_MiExecption THEN
         ln_nivelError := ln_nivelError+1;
  
        
      
     --- INSERTA DETALLE EN LA BITACORA
     
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => LV_ERROR,
                                            pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                            pv_mensaje_accion     => lv_MensajeAccion,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error,
                                            pv_error              => Pv_Error);
       
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_nivelError,
                                            pn_error                => ln_error,
                                            pv_error                => Pv_Error); 
                                            
     
     
       
     -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
     Pv_Error:=LV_ERROR;
    
    WHEN OTHERS THEN
      Lv_Error := 'Error al borrar las tablas - CO_REPORTE_ADIC_' || SQLERRM;
       ln_nivelError := ln_nivelError+1;
       --- INSERTA DETALLE EN LA BITACORA
     
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => Lv_Error,
                                            pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                            pv_mensaje_accion     => lv_MensajeAccion,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error,
                                            pv_error              => Pv_Error);
      
     SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_nivelError,
                                            pn_error                => ln_error,
                                            pv_error                => Pv_Error); 
                                            
      
     
     
      
       -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
        Pv_Error:=LV_ERROR;
      
  end COP_DEPURA_TABLAS_CARGADAS;
end COK_DEPURACION_TABLAS;
/
