create or replace package COK_REPORTE_CAPAS_NEW is

  -- Author  : CESPINOC
  -- Created : 07/04/2015 10:45:49
  -- Purpose : 

  gv_fecha     varchar2(50);
  gv_fecha_fin varchar2(50);

  gv_repcap                   varchar2(50) := 'CO_REPCAP_';
  gv_cabcap                   varchar2(50) := 'CO_CABCAP_';
  ln_id_bitacora_scp          number := 0;
  ln_total_registros_scp      number := 0;
  lv_id_proceso_scp           varchar2(100) := 'CAPAS_2';
  lv_referencia_scp           varchar2(100) := 'COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
  lv_unidad_registro_scp      varchar2(30) := '1';
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_proceso_par_scp          varchar2(30);
  lv_valor_par_scp            varchar2(4000);
  lv_descripcion_par_scp      varchar2(500);
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);
  
  PROCEDURE COP_CARGA_PRINCIPAL;
  
  PROCEDURE COP_CARGA_FACTURAS_PERIODO;

  PROCEDURE COP_CARGA_FACTURA(PD_FECHA DATE, PD_PERIODO_PADRE DATE);

  PROCEDURE COP_CARGA_ANTICIPOS(PV_ERROR OUT VARCHAR2);

  PROCEDURE COP_PAGO_ANTICIPADO(PN_HILO  NUMBER,
                                  PV_ERROR OUT VARCHAR2);

  PROCEDURE COP_PAGOS_SALDOS(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2);
  
  PROCEDURE COP_PAGOS(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2);

  PROCEDURE COP_SALDO(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2);

  PROCEDURE MAIN(PDFECH_INI IN DATE, PDFECH_FIN IN DATE);

  FUNCTION TOTAL_FACTURA(PD_PERIODO     IN DATE,
                         PN_COMPANIA    IN VARCHAR2,
                         PN_MONTO       OUT NUMBER,
                         PN_MONTO_FAVOR OUT NUMBER,
                         PN_CANTIDAD    OUT NUMBER,
                         PV_ERROR       OUT VARCHAR2) RETURN NUMBER;

  FUNCTION TOTAL_EFECTIVO(PD_FECHA    IN DATE,
                          PN_COMPANIA IN NUMBER,
                          PD_PERIODO  IN DATE,
                          PN_TOTAL    OUT NUMBER,
                          PN_CANTIDAD OUT NUMBER,
                          PV_ERROR    OUT VARCHAR2) RETURN NUMBER;

  FUNCTION PORCENTAJE(PN_TOTAL      IN NUMBER,
                      PN_AMORTIZADO IN NUMBER,
                      PN_PORC       OUT NUMBER,
                      PV_ERROR      OUT VARCHAR2) RETURN NUMBER;

  FUNCTION CREA_TABLA_CAB(PDFECHAPERIODO IN DATE, PV_ERROR OUT VARCHAR2)
    RETURN NUMBER;

  FUNCTION CREA_TABLA(PDFECHAPERIODO IN DATE, PV_ERROR OUT VARCHAR2)
    RETURN NUMBER;
 
  PROCEDURE COP_CAPAS_ESTADO(PV_ESTADO VARCHAR2, PD_FECHA OUT DATE, PN_HILOS OUT NUMBER, PN_MESES OUT NUMBER);
  
  PROCEDURE COP_ACTUALIZA_ESTADO(PV_ESTADO_INI VARCHAR2, PV_ESTADO_FIN VARCHAR2);
  
  PROCEDURE COP_CAPAS_MESES_ESTADO(PV_ESTADO VARCHAR2, PD_FECHA DATE, PN_HILOS OUT NUMBER, PD_PERIODO OUT DATE);
  PROCEDURE COP_ACTUALIZA_MESES_ESTADO(PV_ESTADO_INI VARCHAR2, PV_ESTADO_FIN VARCHAR2, PD_PERIODO DATE, PD_PERIODO_PADRE DATE);
end COK_REPORTE_CAPAS_NEW;
/
CREATE OR REPLACE PACKAGE BODY COK_REPORTE_CAPAS_NEW IS

  PROCEDURE COP_CARGA_PRINCIPAL IS
    LV_ERROR VARCHAR2(1000);
  
  BEGIN
    EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_CAPAS_PAGOS');
    EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_CAPAS_FACTURACION');
    EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_CAPAS_PERIODO_MESES');
    COP_CARGA_FACTURAS_PERIODO;
    
    COP_CARGA_ANTICIPOS(PV_ERROR => LV_ERROR);
   
  END COP_CARGA_PRINCIPAL;
  
  PROCEDURE COP_CARGA_FACTURAS_PERIODO IS
    CURSOR C_PERIODO IS
      SELECT PERIODO, MESES FROM CO_CAPAS_PERIODO WHERE ESTADO = 'I' AND ROWNUM = 1;
      
    LD_FECHA DATE;
  
  BEGIN
    FOR J IN C_PERIODO LOOP
      LD_FECHA := J.PERIODO;
      FOR I IN 1 .. J.MESES LOOP
        COK_REPORTE_CAPAS_NEW.COP_CARGA_FACTURA(PD_FECHA => LD_FECHA, PD_PERIODO_PADRE => J.PERIODO);
        LD_FECHA := ADD_MONTHS(LD_FECHA, -1);
      END LOOP;
      UPDATE CO_CAPAS_PERIODO SET ESTADO = 'C' WHERE PERIODO = J.PERIODO;
      COMMIT;
    END LOOP;
  END COP_CARGA_FACTURAS_PERIODO;

  PROCEDURE COP_CARGA_FACTURA(PD_FECHA DATE, PD_PERIODO_PADRE DATE) IS
    CURSOR C_PERIODO_MES IS
      SELECT PERIODO
        FROM CO_CAPAS_PERIODO_MESES G
       WHERE G.PERIODO = PD_FECHA
         AND ESTADO = 'I';
  
    LT_CUSTOMER_ID COT_NUMBER := COT_NUMBER();
    LT_VALOR       COT_NUMBER := COT_NUMBER();
    LT_DESCUENTO   COT_NUMBER := COT_NUMBER();
    LT_CREDITO     COT_NUMBER := COT_NUMBER();
    LT_COMPANIA    COT_NUMBER := COT_NUMBER();
    LV_FECHA       VARCHAR2(8);
    LD_FECHA       DATE;
    LB_FOUND       BOOLEAN;
    LVSENTENCIA    VARCHAR2(10000);
  BEGIN
    OPEN C_PERIODO_MES;
    FETCH C_PERIODO_MES
      INTO LD_FECHA;
    LB_FOUND := C_PERIODO_MES%FOUND;
    CLOSE C_PERIODO_MES;
  
    IF NOT LB_FOUND THEN
      
      LV_FECHA    := TO_CHAR(PD_FECHA, 'DDMMYYYY');
      LVSENTENCIA := 'BEGIN' ||
                     ' SELECT CUSTOMER_ID, SUM(DECODE(TIPO,''006 - CREDITOS'',0,VALOR)) VALOR, SUM(DESCUENTO) DESCUENTO, SUM(DECODE(TIPO,''006 - CREDITOS'',VALOR,0)) CREDITO, DECODE(COST_DESC,''Quito'',2,1) COMPANIA 
                     BULK COLLECT INTO :1, :2, :3, :4, :5' ||
                     ' FROM CO_FACT_' || LV_FECHA ||
                    --                   ' WHERE TIPO != ''006 - CREDITOS''
                     ' GROUP BY CUSTOMER_ID, DECODE(COST_DESC,''Quito'',2,1);
                     END;';
    
      EXECUTE IMMEDIATE LVSENTENCIA
        USING OUT LT_CUSTOMER_ID, OUT LT_VALOR, OUT LT_DESCUENTO, OUT LT_CREDITO, OUT LT_COMPANIA;
    
      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
        INSERT /*+ APPEND */
        INTO CO_CAPAS_FACTURACION NOLOGGING
          (COMPANIA,
           CUSTOMER_ID,
           VALOR,
           DESCUENTO,
           PERIODO,
           HILO,
           CREDITOS)
        VALUES
          (LT_COMPANIA(IND),
           LT_CUSTOMER_ID(IND),
           LT_VALOR(IND),
           LT_DESCUENTO(IND),
           PD_FECHA,
           trunc(DBMS_RANDOM.VALUE * 10),
           LT_CREDITO(IND));
      COMMIT;
      
      INSERT INTO CO_CAPAS_PERIODO_MESES VALUES (PD_FECHA, PD_PERIODO_PADRE, 'I',10);
      COMMIT;
    END IF;
  END COP_CARGA_FACTURA;

  PROCEDURE COP_CARGA_ANTICIPOS(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_PERIODO(CV_ESTADO VARCHAR2) IS
      SELECT PERIODO FROM CO_CAPAS_PERIODO WHERE ESTADO = CV_ESTADO;
  
    CURSOR C_PERIODO_MES IS
      SELECT CUSTOMER_ID
        FROM CO_CAPAS_FACTURACION
       WHERE PERIODO IN (SELECT PERIODO
                           FROM CO_CAPAS_PERIODO_MESES G
                          WHERE ESTADO = 'I');
  
    CURSOR C_ANTICIPOS IS
      SELECT A.CUSTOMER_ID,
             A.OHXACT FACTURA,
             (A.OHOPNAMT_GL) * -1 VALOR,
             A.OHENTDATE FECHA,
             A.INSERTIONDATE FECHA_INGRESO
        FROM ORDERHDR_ALL A
       WHERE A.OHSTATUS = 'CO'
         AND A.OHOPNAMT_GL < 0
         AND EXISTS (SELECT 1
                FROM ORDERHDR_ALL G
               WHERE G.CUSTOMER_ID = A.CUSTOMER_ID
                 AND G.OHENTDATE > A.OHENTDATE)
         AND EXISTS (SELECT CUSTOMER_ID
                FROM CO_CAPAS_CLIENTES W
               WHERE W.CUSTOMER_ID = A.CUSTOMER_ID);
  
    CURSOR C_CLIENTES_ANTICIPOS IS
      SELECT DISTINCT CUSTOMER_ID FROM CO_PAGOS_ANTICIPADOS;
  
    LT_CUSTOMER_ID   COT_NUMBER := COT_NUMBER();
    LT_FACTURA       COT_NUMBER := COT_NUMBER();
    LT_VALOR         COT_NUMBER := COT_NUMBER();
    LT_FECHA         COT_FECHA := COT_FECHA();
    LT_FECHA_INGRESO COT_FECHA := COT_FECHA();
  
    TYPE LR_ANTICIPOS IS TABLE OF C_ANTICIPOS%ROWTYPE;
    LT_ANTICIPOS LR_ANTICIPOS;
  
    LN_CANTIDAD NUMBER;
    LD_FECHA    DATE;
    LB_FOUND    BOOLEAN;
  BEGIN
  
    OPEN C_PERIODO('C');
    FETCH C_PERIODO
      INTO LD_FECHA;
    LB_FOUND := C_PERIODO%FOUND;
    CLOSE C_PERIODO;
  
    IF LB_FOUND THEN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_CAPAS_CLIENTES');
    
      OPEN C_PERIODO_MES;
      FETCH C_PERIODO_MES BULK COLLECT
        INTO LT_CUSTOMER_ID;
      CLOSE C_PERIODO_MES;
    
      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
        INSERT /*+ APPEND */
        INTO CO_CAPAS_CLIENTES NOLOGGING
          (CUSTOMER_ID)
        VALUES
          (LT_CUSTOMER_ID(IND));
      COMMIT;
    
      UPDATE CO_CAPAS_PERIODO SET ESTADO = 'CC' WHERE PERIODO = LD_FECHA;
      COMMIT;
    
    END IF;
  
    LB_FOUND := FALSE;
  
    OPEN C_PERIODO('CC');
    FETCH C_PERIODO
      INTO LD_FECHA;
    LB_FOUND := C_PERIODO%FOUND;
    CLOSE C_PERIODO;
  
    IF LB_FOUND THEN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_PAGOS_ANTICIPADOS');
      OPEN C_ANTICIPOS;
      FETCH C_ANTICIPOS BULK COLLECT
        INTO LT_ANTICIPOS;
      CLOSE C_ANTICIPOS;
    
      FORALL I IN LT_ANTICIPOS.FIRST .. LT_ANTICIPOS.LAST
        INSERT /*+ APPEND */
        INTO CO_PAGOS_ANTICIPADOS NOLOGGING
          (CUSTOMER_ID, FACTURA, VALOR, FECHA, FECHA_INGRESO)
        VALUES
          (LT_ANTICIPOS(I).CUSTOMER_ID,
           LT_ANTICIPOS(I).FACTURA,
           LT_ANTICIPOS(I).VALOR,
           LT_ANTICIPOS(I).FECHA,
           LT_ANTICIPOS(I).FECHA_INGRESO);
      COMMIT;
    
      UPDATE CO_CAPAS_PERIODO SET ESTADO = 'CP' WHERE PERIODO = LD_FECHA;
      COMMIT;
    END IF;
  
    LB_FOUND := FALSE;
  
    OPEN C_PERIODO('CP');
    FETCH C_PERIODO
      INTO LD_FECHA;
    LB_FOUND := C_PERIODO%FOUND;
    CLOSE C_PERIODO;
  
    IF LB_FOUND THEN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_CLIENTES_ANTICIPOS');
      OPEN C_CLIENTES_ANTICIPOS;
      FETCH C_CLIENTES_ANTICIPOS BULK COLLECT
        INTO LT_CUSTOMER_ID;
      CLOSE C_CLIENTES_ANTICIPOS;
    
      LN_CANTIDAD := LT_CUSTOMER_ID.COUNT;
    
      LN_CANTIDAD := LN_CANTIDAD / 5000;
    
      IF LN_CANTIDAD <= 5 THEN
        LN_CANTIDAD := 5;
      ELSE
        LN_CANTIDAD := CEIL(LN_CANTIDAD);
      END IF;
    
      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
        INSERT /*+ APPEND */
        INTO CO_CLIENTES_ANTICIPOS NOLOGGING
          (CUSTOMER_ID, HILO)
        VALUES
          (LT_CUSTOMER_ID(IND), trunc(DBMS_RANDOM.VALUE * LN_CANTIDAD));
      COMMIT;
    
      UPDATE CO_CAPAS_PERIODO SET ESTADO = 'CA', HILOS_ANTICIPO = LN_CANTIDAD WHERE PERIODO = LD_FECHA;
      COMMIT;
      
      EXECUTE IMMEDIATE ('TRUNCATE TABLE CO_PAGOS_VIRTUALES');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR(SQLERRM, 1, 1000);
  END COP_CARGA_ANTICIPOS;

  PROCEDURE COP_PAGO_ANTICIPADO(PN_HILO  NUMBER,
                                PV_ERROR OUT VARCHAR2) IS
    CURSOR C_CLIENTES(CN_HILO NUMBER) IS
      SELECT CUSTOMER_ID, ROWID
        FROM CO_CLIENTES_ANTICIPOS
       WHERE HILO = CN_HILO
         AND PROCESADO IS NULL /*
                                                                                             AND CUSTOMER_ID = 1591002*/
      ; --107853;
  
    TYPE LR_CLIENTES IS TABLE OF C_CLIENTES%ROWTYPE;
    LT_CLIENTES LR_CLIENTES;
  
    CURSOR C_PAGO_ANTICIPADO(CN_CUSTOMERID NUMBER) IS
      SELECT V.CUSTOMER_ID,
             SUM(V.VALOR) VALOR,
             V.FECHA,
             MAX(V.FACTURA) FACTURA,
             V.FECHA_INGRESO,
             MAX(ROWID) ROWID1 --V.CUSTOMER_ID, SUM(V.VALOR) VALOR, V.FECHA, V.FACTURA, V.FECHA_INGRESO, ROWID
        FROM CO_PAGOS_ANTICIPADOS V
       WHERE V.CUSTOMER_ID = CN_CUSTOMERID
       GROUP BY V.CUSTOMER_ID, V.FECHA, V.FECHA_INGRESO
       ORDER BY FECHA, FACTURA;
  
    CURSOR C_FACTURAS(CN_CUSTOMERID NUMBER,
                      CD_FECHA      DATE,
                      CN_FACTURA    NUMBER,
                      CD_FECHA_ING  DATE) IS
      SELECT F.OHENTDATE    FECHA,
             F.OHINVAMT_DOC VALOR,
             F.OHXACT,
             F.OHSTATUS,
             F.OHOPNAMT_DOC
        FROM ORDERHDR_ALL F
       WHERE F.CUSTOMER_ID = CN_CUSTOMERID
         AND F.OHSTATUS IN ('IN', 'CO')
            --AND F.OHOPNAMT_DOC > 0
         AND F.OHENTDATE >= CD_FECHA
         AND F.OHXACT <> CN_FACTURA
         AND F.INSERTIONDATE > CD_FECHA_ING
       ORDER BY F.OHENTDATE, F.OHXACT;
  
    LC_FACTURAS C_FACTURAS%ROWTYPE;
  
    CURSOR C_PAGOS(CN_OHXACT NUMBER) IS
      SELECT A.CADAMT_DOC VALOR, B.CAENTDATE FECHA, A.CADXACT
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE A.CADOXACT = CN_OHXACT
         AND A.CADXACT = B.CAXACT
         AND ((A.CADCURAMT_PAY > 0 AND B.CATYPE <> 2) OR
             (B.CATYPE = 2 AND A.CADCURAMT_PAY IS NULL))
       ORDER BY B.CAENTDATE;
  
    LC_PAGOS C_PAGOS%ROWTYPE;
  
    CURSOR C_ANTICIPO(CN_OHXACT NUMBER) IS
      SELECT SUM(A.CADAMT_DOC) VALOR,
             B.CAENTDATE FECHA,
             MAX(A.CADXACT) CADXACT
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE A.CADOXACT = CN_OHXACT
         AND A.CADXACT = B.CAXACT
         AND A.CADAMT_DOC < 0
       GROUP BY B.CAENTDATE;
  
    LC_ANTICIPO C_ANTICIPO%ROWTYPE;
  
    LN_CANT           NUMBER := 0;
    LN_VALOR_ANTICIPO NUMBER := 0;
    LN_VALOR          NUMBER := 0;
    LN_PAGO           NUMBER := 0;
    LB_PAGO           BOOLEAN := FALSE;
  
  BEGIN
  
    OPEN C_CLIENTES(PN_HILO);
    FETCH C_CLIENTES BULK COLLECT
      INTO LT_CLIENTES;
    CLOSE C_CLIENTES;
  
    IF LT_CLIENTES.COUNT > 0 THEN
      FOR I IN LT_CLIENTES.FIRST .. LT_CLIENTES.LAST LOOP
        LN_VALOR_ANTICIPO := 0;
        FOR O IN C_PAGO_ANTICIPADO(LT_CLIENTES(I).CUSTOMER_ID) LOOP
          BEGIN
            LC_ANTICIPO       := NULL;
            LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + O.VALOR;
          
            OPEN C_ANTICIPO(O.FACTURA);
            FETCH C_ANTICIPO
              INTO LC_ANTICIPO;
            CLOSE C_ANTICIPO;
          
            FOR J IN C_FACTURAS(O.CUSTOMER_ID,
                                O.FECHA,
                                O.FACTURA,
                                O.FECHA_INGRESO) LOOP
              EXIT WHEN J.OHSTATUS = 'CO' AND J.OHOPNAMT_DOC < 0;
            
              IF J.OHSTATUS = 'IN' THEN
                LN_VALOR := J.VALOR;
                /*IF LN_VALOR_ANTICIPO > LN_VALOR THEN
                  LN_VALOR_ANTICIPO := LN_VALOR;
                END IF;*/
                LB_PAGO := FALSE;
                FOR K IN C_PAGOS(J.OHXACT) LOOP
                  LB_PAGO := TRUE;
                  IF LN_VALOR > 0 THEN
                    IF K.CADXACT = LC_ANTICIPO.CADXACT THEN
                      IF K.VALOR >= LN_VALOR THEN
                        LN_PAGO           := LN_VALOR;
                        LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + K.VALOR -
                                             LN_VALOR;
                      ELSE
                        LN_PAGO := K.VALOR;
                      END IF;
                      INSERT INTO CO_PAGOS_VIRTUALES
                      VALUES
                        (O.CUSTOMER_ID, J.OHXACT, LN_PAGO, TRUNC(K.FECHA));
                    
                      LN_VALOR := LN_VALOR - K.VALOR;
                    ELSE
                      IF LN_VALOR_ANTICIPO > 0 THEN
                        IF LN_VALOR_ANTICIPO <= LN_VALOR THEN
                          LN_PAGO := LN_VALOR_ANTICIPO;
                        ELSIF LN_VALOR_ANTICIPO > LN_VALOR THEN
                          LN_PAGO := LN_VALOR;
                        END IF;
                      
                        INSERT INTO CO_PAGOS_VIRTUALES
                        VALUES
                          (O.CUSTOMER_ID,
                           J.OHXACT,
                           LN_PAGO,
                           TRUNC(J.FECHA));
                      
                        LN_VALOR          := LN_VALOR - LN_PAGO;
                        LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO - LN_PAGO;
                      END IF;
                      IF LN_VALOR > 0 THEN
                        IF LN_VALOR >= K.VALOR THEN
                          LN_PAGO := K.VALOR;
                        ELSIF LN_VALOR < K.VALOR THEN
                          LN_PAGO           := LN_VALOR;
                          LN_VALOR_ANTICIPO := K.VALOR - LN_VALOR;
                        END IF;
                      
                        INSERT INTO CO_PAGOS_VIRTUALES
                        VALUES
                          (O.CUSTOMER_ID,
                           J.OHXACT,
                           LN_PAGO,
                           TRUNC(K.FECHA));
                      
                        LN_VALOR := LN_VALOR - LN_PAGO;
                      ELSE
                        IF K.VALOR < 0 THEN
                          INSERT INTO CO_PAGOS_VIRTUALES
                          VALUES
                            (O.CUSTOMER_ID,
                             J.OHXACT,
                             LN_PAGO,
                             TRUNC(K.FECHA));
                        
                          LN_VALOR          := LN_VALOR - LN_PAGO;
                          LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + LN_PAGO;
                        ELSE
                        
                          LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + K.VALOR;
                        END IF;
                      END IF;
                    END IF;
                  ELSE
                    IF K.VALOR < 0 THEN
                      INSERT INTO CO_PAGOS_VIRTUALES
                      VALUES
                        (O.CUSTOMER_ID, J.OHXACT, K.VALOR, TRUNC(K.FECHA));
                    
                      LN_VALOR          := LN_VALOR - K.VALOR;
                      LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + K.VALOR;
                    ELSE
                      LN_VALOR := LN_VALOR - K.VALOR;
                      IF LN_VALOR_ANTICIPO < 0 THEN
                        LN_VALOR_ANTICIPO := 0;
                      END IF;
                      LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO + K.VALOR;
                    END IF;
                  END IF;
                END LOOP;
                IF NOT LB_PAGO THEN
                  IF LN_VALOR_ANTICIPO > 0 THEN
                    IF LN_VALOR_ANTICIPO <= LN_VALOR THEN
                      LN_PAGO := LN_VALOR_ANTICIPO;
                    ELSIF LN_VALOR_ANTICIPO > LN_VALOR THEN
                      LN_PAGO := LN_VALOR;
                    END IF;
                  
                    INSERT INTO CO_PAGOS_VIRTUALES
                    VALUES
                      (O.CUSTOMER_ID, J.OHXACT, LN_PAGO, TRUNC(J.FECHA));
                  
                    LN_VALOR          := LN_VALOR - LN_PAGO;
                    LN_VALOR_ANTICIPO := LN_VALOR_ANTICIPO - LN_PAGO;
                  END IF;
                END IF;
              
              END IF;
            END LOOP;
            UPDATE CO_PAGOS_ANTICIPADOS
               SET SALDO = LN_VALOR_ANTICIPO, PROCESADO = 'P'
             WHERE ROWID = O.ROWID1;
            LN_CANT := LN_CANT + 1;
            IF LN_CANT > 10 THEN
              COMMIT;
              LN_CANT := 0;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;
        END LOOP;
        UPDATE CO_CLIENTES_ANTICIPOS
           SET PROCESADO = 'S'
         WHERE ROWID = LT_CLIENTES(I).ROWID;
      END LOOP;
      COMMIT;
    END IF;
  END COP_PAGO_ANTICIPADO;
  
  PROCEDURE COP_PAGOS_SALDOS(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2) IS
  BEGIN 
    
     COK_REPORTE_CAPAS_NEW.COP_PAGOS(PD_FECHA => PD_FECHA,
                                  PN_HILO => PN_HILO,
                                  PV_ERROR => PV_ERROR);
                                  
     COK_REPORTE_CAPAS_NEW.COP_SALDO(PD_FECHA => PD_FECHA,
                                  PN_HILO => PN_HILO,
                                  PV_ERROR => PV_ERROR);
  END COP_PAGOS_SALDOS;
  
  PROCEDURE COP_PAGOS(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2) IS
    CURSOR C_FACTURADO(CD_FECHA DATE, CN_HILO NUMBER) IS
      SELECT V.COMPANIA, V.CUSTOMER_ID, V.ROWID
        FROM CO_CAPAS_FACTURACION V
       WHERE PERIODO = CD_FECHA
         AND HILO = CN_HILO
         AND PROCESADO IS NULL;
  
    TYPE LR_FACTURADO IS TABLE OF C_FACTURADO%ROWTYPE;
    LT_FACTURADO LR_FACTURADO;
  
    CURSOR C_FACTURA(CN_CUSTOMERID NUMBER, CD_FECHA DATE) IS
      SELECT A.OHXACT
        FROM ORDERHDR_ALL A
       WHERE CUSTOMER_ID = CN_CUSTOMERID
         AND A.OHENTDATE = CD_FECHA
         AND A.OHSTATUS = 'IN';
  
    LC_FACTURA C_FACTURA%ROWTYPE;
  
    CURSOR C_PAGOS(CN_OHXACT NUMBER) IS
      SELECT A.CADAMT_GL /*CADAMT_DOC*/ VALOR, B.CAENTDATE FECHA
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE A.CADOXACT = CN_OHXACT
         AND A.CADXACT = B.CAXACT;
  
    LC_PAGOS C_PAGOS%ROWTYPE;
  
    CURSOR C_PAGOS_ANTICIPADOS(CN_CUSTOMERID NUMBER, CN_OHXACT NUMBER) IS
      SELECT G.VALOR, G.FECHA
        FROM CO_PAGOS_VIRTUALES G
       WHERE G.CUSTOMER_ID = CN_CUSTOMERID
         AND G.FACTURA = CN_OHXACT;
  
    CURSOR C_ANTICIPOS(CN_CUSTOMERID NUMBER, CD_FECHA DATE) IS
      SELECT 1
        FROM CO_PAGOS_ANTICIPADOS
       WHERE CUSTOMER_ID = CN_CUSTOMERID
         AND FECHA < CD_FECHA;
  
    LC_ANTICIPOS C_ANTICIPOS%ROWTYPE;
  
    LN_CANT  NUMBER := 0;
    LB_FOUND BOOLEAN;
  
  BEGIN
  
    OPEN C_FACTURADO(PD_FECHA, PN_HILO);
    FETCH C_FACTURADO BULK COLLECT
      INTO LT_FACTURADO;
    CLOSE C_FACTURADO;
  
    IF LT_FACTURADO.COUNT > 0 THEN
      FOR I IN LT_FACTURADO.FIRST .. LT_FACTURADO.LAST LOOP
        BEGIN
        
          LC_FACTURA := NULL;
          LC_PAGOS   := NULL;
          OPEN C_FACTURA(LT_FACTURADO(I).CUSTOMER_ID, PD_FECHA);
          FETCH C_FACTURA
            INTO LC_FACTURA;
          CLOSE C_FACTURA;
          LB_FOUND := FALSE;
        
          /*OPEN C_PAGOS(LC_FACTURA.OHXACT);
          FETCH C_PAGOS
            INTO LC_PAGOS;
          LB_FOUND := C_PAGOS%FOUND;
          CLOSE C_PAGOS;*/
          OPEN C_ANTICIPOS(LT_FACTURADO(I).CUSTOMER_ID, PD_FECHA);
          FETCH C_ANTICIPOS
            INTO LC_ANTICIPOS;
          LB_FOUND := C_ANTICIPOS%FOUND;
          CLOSE C_ANTICIPOS;
        
          IF LB_FOUND THEN
            FOR J IN C_PAGOS_ANTICIPADOS(LT_FACTURADO(I).CUSTOMER_ID,
                                         LC_FACTURA.OHXACT) LOOP
            
              INSERT INTO CO_CAPAS_PAGOS
                (COMPANIA, CUSTOMER_ID, VALOR, FECHA, PERIODO, TIPO)
              VALUES
                (LT_FACTURADO(I).COMPANIA,
                 LT_FACTURADO(I).CUSTOMER_ID,
                 J.VALOR,
                 TRUNC(J.FECHA),
                 PD_FECHA,
                 'A');
            
              LN_CANT := LN_CANT + 1;
            
            END LOOP;
          ELSE
            FOR J IN C_PAGOS(LC_FACTURA.OHXACT) LOOP
            
              INSERT INTO CO_CAPAS_PAGOS
                (COMPANIA, CUSTOMER_ID, VALOR, FECHA, PERIODO, TIPO)
              VALUES
                (LT_FACTURADO(I).COMPANIA,
                 LT_FACTURADO(I).CUSTOMER_ID,
                 J.VALOR,
                 TRUNC(J.FECHA),
                 PD_FECHA,
                 'P');
            
              LN_CANT := LN_CANT + 1;
            
            END LOOP;
          END IF;
        
          UPDATE CO_CAPAS_FACTURACION
             SET PROCESADO = 'S'
           WHERE ROWID = LT_FACTURADO(I).ROWID;
          IF LN_CANT > 100 THEN
            COMMIT;
            LN_CANT := 0;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            PV_ERROR := LT_FACTURADO(I)
                        .CUSTOMER_ID || ' - ' || LC_PAGOS.VALOR;
            EXIT WHEN PV_ERROR IS NOT NULL;
        END;
      END LOOP;
      COMMIT;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR - ' || SQLERRM;
    
  END COP_PAGOS;

  PROCEDURE COP_SALDO(PD_FECHA DATE, PN_HILO NUMBER, PV_ERROR OUT VARCHAR2) IS
    CURSOR C_FACTURADO(CD_FECHA DATE, CN_HILO NUMBER) IS
      SELECT V.CUSTOMER_ID, V.ROWID, V.VALOR - V.DESCUENTO VALOR
        FROM CO_CAPAS_FACTURACION V
       WHERE PERIODO = CD_FECHA
         AND HILO = CN_HILO
         AND PROCESADO = 'S';
  
    TYPE LR_FACTURADO IS TABLE OF C_FACTURADO%ROWTYPE;
    LT_FACTURADO LR_FACTURADO;
  
    CURSOR C_PAGOS(CN_CUSTOMERID NUMBER, CD_FECHA DATE) IS
      SELECT SUM(VALOR) VALOR
        FROM CO_CAPAS_PAGOS F
       WHERE F.CUSTOMER_ID = CN_CUSTOMERID
         AND PERIODO = CD_FECHA;
  
    LC_PAGOS C_PAGOS%ROWTYPE;
  
    LN_CANT NUMBER := 0;
  
  BEGIN
  
    OPEN C_FACTURADO(PD_FECHA, PN_HILO);
    FETCH C_FACTURADO BULK COLLECT
      INTO LT_FACTURADO;
    CLOSE C_FACTURADO;
  
    IF LT_FACTURADO.COUNT > 0 THEN
      FOR I IN LT_FACTURADO.FIRST .. LT_FACTURADO.LAST LOOP
        BEGIN
          LC_PAGOS := NULL;
          OPEN C_PAGOS(LT_FACTURADO(I).CUSTOMER_ID, PD_FECHA);
          FETCH C_PAGOS
            INTO LC_PAGOS;
          CLOSE C_PAGOS;
          UPDATE CO_CAPAS_FACTURACION
             SET SALDO     = /*LT_FACTURADO(I).VALOR - */ NVL(LC_PAGOS.VALOR,
                                                              0),
                 PROCESADO = 'P'
           WHERE ROWID = LT_FACTURADO(I).ROWID;
          IF LN_CANT > 100 THEN
            COMMIT;
            LN_CANT := 0;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END LOOP;
      COMMIT;
    END IF;
  END COP_SALDO;

  PROCEDURE MAIN(pdFech_ini in date, pdFech_fin in DATE) IS
  
    -- variables
    lvSentencia    VARCHAR2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lvMensErr      VARCHAR2(2000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales facturados en el periodo
    lnTotalFavor   NUMBER; --variable para totales facturados en el periodo a favor
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de crédito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido día a día
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lnOc           NUMBER;
    lnValorFavor   NUMBER;
    ln_facturas    NUMBER;
    le_error EXCEPTION;
    le_error_main EXCEPTION;
  
    -- cursores
    cursor c_periodos is
      select distinct lrstart cierre_periodo
        from bch_history_table
       where lrstart = pdFech_ini
         and to_char(lrstart, 'dd') <> '01';
         
         
     CURSOR C_PERIODO_MES IS
      SELECT PERIODO
        FROM CO_CAPAS_PERIODO_MESES G
       WHERE G.PERIODO = pdFech_ini
         AND G.PERIODO_PADRE = pdFech_FIN
         AND ESTADO = 'P';
     LB_FOUND BOOLEAN;
     LD_FECHA DATE;
  BEGIN
    OPEN C_PERIODO_MES;
    FETCH C_PERIODO_MES
      INTO LD_FECHA;
    LB_FOUND := C_PERIODO_MES%FOUND;
    CLOSE C_PERIODO_MES;
  
    IF LB_FOUND THEN
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    lv_referencia_scp           := 'COK_REPORT_CAPAS_CALCULO.MAIN';
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      return;
    end if;
    ----------------------------------------------------------------------------
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CALCULO.MAIN ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
    for p in c_periodos loop
    
      --search the table, if it exists...
      lvSentencia   := ' select count(*) ' || ' from user_all_tables ' ||
                       ' where table_name = ''' || gv_repcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla is null or lnExisteTabla = 0 then
        --se crea la tabla
        lnExito := crea_tabla(p.cierre_periodo, lvMensErr);
      else
        --si no existe se trunca la tabla para colocar los datos nuevamente
        lvSentencia := 'truncate table ' || gv_repcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy');
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      end if;
    
      -- se respalda la informacion para auditoria de pagos
      lvSentencia := 'create table co_cap_' ||
                     to_char(pdFech_ini, 'ddMMyyyy') || '_' ||
                     to_char(sysdate, 'ddMMyyyy') ||
                     ' as select * from co_disponible2';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      -- 23/04/2008 LSE
      if lvMensErr is not null then
        raise le_error;
      end if;
    
      lvSentencia   := 'select count(*)  ' || ' from user_all_tables ' ||
                       ' where table_name = ''' || gv_cabcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla is null or lnExisteTabla = 0 then
        --se crea la tabla
        lnExito := crea_tabla_cab(p.cierre_periodo, lvMensErr);
      else
        -- se trunca tabla de cabecera
        lvSentencia := 'truncate table ' || gv_cabcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy');
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      end if;
      if lvMensErr is not null then
        raise le_error_main;
      end if;
    
      for c in 1 .. 2 loop
        if c = 1 then
          lvCostCode := 'GYE';
        else
          lvCostCode := 'UIO';
        end if;
      
        -- se calcula el total facturado hasta el cierre
        lnExito := total_factura(p.cierre_periodo,
                                 C,
                                 lnTotal,
                                 lnValorFavor,
                                 ln_facturas,
                                 lvMensErr);
        if lvMensErr is not null then
          raise le_error;
        end if;
      
        -- CBR: 25 Abril 2006
        -- solo se tomará como valor de cabecera el balance_12 del detalle de cliente y no los valores a favor
        -- se valida a partir de Agosto2004 se agrega a monto el valor adelantado obtenido en lnValorFavor
        --if p.cierre_periodo >= to_date('24/08/2004', 'dd/MM/yyyy') then
        if p.cierre_periodo >= to_date('24/10/2005', 'dd/MM/yyyy') then
        
          lvSentencia := 'begin
                insert into ' || gv_cabcap ||
                         to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)' ||
                         ' VALUES' || ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0),nvl(:4,0),nvl(:5,0));
                 end;';
        
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in lnTotal, in lnTotalFavor, in lnValorFavor;
        else
          lvSentencia := 'begin
                insert into ' || gv_cabcap ||
                         to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)' ||
                         ' VALUES' || ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0) + nvl(:4,0), nvl(:5,0),nvl(:4,0));
                end';
        
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in lnTotal, in lnValorFavor, in lnTotalFavor;
        
          lnTotal := lnTotal + lnValorFavor;
        end if;
        --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        commit;
      
        -- se inicializan variables acumuladoras de efectivo y credito
        lnAcuEfectivo := 0;
        lnAcuCredito  := 0;
      
        ldFech_dummy := pdFech_ini;
        lnDia        := 0;
      
        -- se inicializa el credito
        lnCredito := 0;
      
        -- Se calcula el total de las facturas dentro del periodo
      
        -- loop for each date from the begining
        while ldFech_dummy <= pdFech_fin loop
        
          lnDia := lnDia + 1;
          -- se calcula el total de pagos en efectivo
          lnExito := total_efectivo(ldFech_dummy,
                                    c,
                                    p.cierre_periodo,
                                    lnEfectivo,
                                    lnTotFact,
                                    lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo, 0);
        
          if ldFech_dummy = pdFech_ini then
            -- se calcula el total de creditos
            lnCredito := lnValorFavor;
          ELSE
            lnCredito := 0;
          end if;
          lnAcuCredito := lnAcuCredito + nvl(lnCredito, 0);
          -- se calcula el porcentaje recuperado del total
          lnExito := porcentaje(lnTotal,
                                lnAcuEfectivo + lnAcuCredito,
                                lnPorc,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnExito := porcentaje(lnTotal,
                                lnAcuEfectivo,
                                lnPorcEfectivo,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnExito := porcentaje(lnTotal,
                                lnAcuCredito,
                                lnPorcCredito,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
        
          -- se inserta a la tabla el primer registro
          lnMonto     := lnTotal - lnAcuEfectivo - lnAcuCredito;
          lnAcumulado := lnAcuEfectivo + lnAcuCredito;
        
          lvSentencia := 'begin
                                   insert into ' ||
                         gv_repcap || to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         '(region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) ' ||
                         'values (
                                  :1,
                                  to_date(:2,''yyyy/MM/dd''),
                                  to_date(:3,''yyyy/MM/dd''),
                                  :4,
                                  :5,
                                  :6,
                                  :7,
                                  :8,
                                  :9,
                                  :10,
                                  nvl(:11,0),
                                  :12,
                                  nvl(:13,0));
                                  end;';
        
          --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in to_char(ldFech_dummy, 'yyyy/MM/dd'), in lnTotFact, in lnMonto, in lnPorc, in lnDia, in lnAcuEfectivo, in lnPorcEfectivo, in lnAcuCredito, in lnPorcCredito, in lnAcumulado, in lnOc;
          commit;
        
          ldFech_dummy := ldFech_dummy + 1;
        end loop;
      
      end loop;
    
    end loop;
  
    COMMIT;
  
    ln_registros_procesados_scp := ln_registros_procesados_scp + 1;
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
  
    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          null,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------    
  
    cok_reporte_capas_new.cop_actualiza_meses_estado(pv_estado_ini => 'P',
                                                   pv_estado_fin => 'F',
                                                   pd_periodo => pdFech_ini,
                                                   pd_periodo_padre => pdFech_FIN);
    COMMIT;
    END IF;
  
  EXCEPTION
    WHEN le_error_main THEN
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    -- 22/04/2008 LSE
    WHEN le_error THEN
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    WHEN OTHERS THEN
      lvMensErr := SUBSTR(SQLERRM, 1, 500);
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
  END MAIN;

  FUNCTION TOTAL_FACTURA(PD_PERIODO     IN DATE,
                         PN_COMPANIA    IN VARCHAR2,
                         PN_MONTO       OUT NUMBER,
                         PN_MONTO_FAVOR OUT NUMBER,
                         PN_CANTIDAD    OUT NUMBER,
                         PV_ERROR       OUT VARCHAR2) RETURN NUMBER IS
  
    CURSOR C_FACTURADO(CD_FECHA DATE, CN_COMPANIA NUMBER) IS
      SELECT SUM(F.VALOR - F.DESCUENTO) FACTURADO,
             SUM(F.CREDITO_REAL) CREDITOS,
             COUNT(F.CUSTOMER_ID) CANTIDAD
        FROM (SELECT H.*,
                     case
                       WHEN H.VALOR - H.DESCUENTO < H.CREDITOS * -1 THEN
                        H.VALOR - H.DESCUENTO
                       ELSE
                        H.CREDITOS * -1
                     END CREDITO_REAL
                FROM CO_CAPAS_FACTURACION H
               WHERE H.PERIODO = CD_FECHA
                 AND H.COMPANIA = CN_COMPANIA
                 AND H.VALOR > 0) F;
  
    LN_RETORNO   NUMBER;
    LNMONTO      NUMBER;
    LNMONTOFAVOR NUMBER;
  
  BEGIN
  
    OPEN C_FACTURADO(PD_PERIODO, PN_COMPANIA);
    FETCH C_FACTURADO
      INTO PN_MONTO, PN_MONTO_FAVOR, PN_CANTIDAD;
    CLOSE C_FACTURADO;
  
    LN_RETORNO := 1;
  
    RETURN(LN_RETORNO);
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    
      PV_ERROR := 'FUNCION: TOTAL_FACTURA ON NO_DATA_FOUND: ' ||
                  SUBSTR(SQLERRM, 1, 500);
    
      RETURN(0);
    WHEN OTHERS THEN
    
      PV_ERROR := 'FUNCION: TOTAL_FACTURA: ' || SUBSTR(SQLERRM, 1, 500);
    
      RETURN(-1);
  END TOTAL_FACTURA;

  FUNCTION TOTAL_EFECTIVO(PD_FECHA    IN DATE,
                          PN_COMPANIA IN NUMBER,
                          PD_PERIODO  IN DATE,
                          PN_TOTAL    OUT NUMBER,
                          PN_CANTIDAD OUT NUMBER,
                          PV_ERROR    OUT VARCHAR2) RETURN NUMBER IS
    --LNPAGO1    NUMBER;
    --LNPAGO2    NUMBER;
    CURSOR C_PAGOS(CD_PERIODO DATE, CD_FECHA DATE, CN_COMPANIA NUMBER) IS
      SELECT SUM(VALOR) VALOR, COUNT(Y.CUSTOMER_ID) CANTIDAD
        FROM CO_CAPAS_PAGOS Y
       WHERE Y.COMPANIA = CN_COMPANIA
         AND Y.PERIODO = CD_PERIODO
         AND Y.FECHA = CD_FECHA;
  
  BEGIN
  
    OPEN C_PAGOS(PD_PERIODO, PD_FECHA, PN_COMPANIA);
    FETCH C_PAGOS
      INTO PN_TOTAL, PN_CANTIDAD;
    CLOSE C_PAGOS;
  
    RETURN 1;
    COMMIT;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    
      PV_ERROR := 'FUNCION: TOTAL_EFECTIVO ' || SUBSTR(SQLERRM, 1, 500);
      RETURN(0);
    WHEN OTHERS THEN
    
      PV_ERROR := 'FUNCION: TOTAL_EFECTIVO ' || SUBSTR(SQLERRM, 1, 500);
      RETURN(-1);
  END TOTAL_EFECTIVO;

  FUNCTION PORCENTAJE(PN_TOTAL      IN NUMBER,
                      PN_AMORTIZADO IN NUMBER,
                      PN_PORC       OUT NUMBER,
                      PV_ERROR      OUT VARCHAR2) RETURN NUMBER IS
    LN_RETORNO NUMBER;
  
  BEGIN
  
    PN_PORC    := ROUND((PN_AMORTIZADO * 100) / PN_TOTAL, 2);
    LN_RETORNO := 1;
    RETURN(LN_RETORNO);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'FUNCION: PORCENTAJE ' || SUBSTR(SQLERRM, 1, 500);
      RETURN(0);
  END PORCENTAJE;

  FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
    le_error exception;
  
  BEGIN
  
    lvSentencia := 'create table ' || gv_cabcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( REGION              VARCHAR2(3), ' ||
                   '  PERIODO_FACTURACION DATE, ' ||
                   '  MONTO_FACTURACION   NUMBER, ' ||
                   '  CANT_FACTURAS       NUMBER, ' ||
                   '  SALDO_FAVOR         NUMBER, ' ||
                   '  PAGOS_FAVOR         NUMBER, ' ||
                   '  VALOR_FAVOR         NUMBER) ' ||
                   'tablespace REP_COB_DAT ' || '  pctfree 10 ' ||
                   '  pctused 40 ' || '  initrans 1 ' || '  maxtrans 255 ' ||
                   '  storage ' || '  (initial 4K ' || '    next 1K ' ||
                   '    minextents 1 ' || '    maxextents 2 ' ||
                   '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'grant select, insert, update, delete, references, alter, index on ' ||
                   gv_cabcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' to PUBLIC';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
    --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    return 1;
  
    COMMIT;
  EXCEPTION
    when le_error then
      pv_error := 'FUNCION CREA_TABLA_CAB: ' || lvMensErr;
      return 0;
    when others then
      pv_error := 'FUNCION CREA_TABLA_CAB: ' || SUBSTR(SQLERRM, 1, 500);
      return 0;
  END CREA_TABLA_CAB;

  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(1000);
    le_error exception;
  
  BEGIN
  
    lvSentencia := 'create table ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( REGION              VARCHAR2(3),' ||
                   '  PERIODO_FACTURACION DATE,' ||
                   '  FECHA          DATE,' ||
                   '  TOTAL_FACTURA       NUMBER,' ||
                   '  MONTO               NUMBER,' ||
                   '  PORC_RECUPERADO     NUMBER(8,2),' ||
                   '  DIAS                NUMBER,' ||
                   '  EFECTIVO            NUMBER,' ||
                   '  PORC_EFECTIVO       NUMBER(8,2),' ||
                   '  CREDITOS            NUMBER,' ||
                   '  PORC_CREDITOS       NUMBER(8,2),' ||
                   '  ACUMULADO           NUMBER,' ||
                   '  PAGOS_CO            NUMBER)' ||
                   'tablespace REP_COB_DAT' || '  pctfree 10' ||
                   '  pctused 40' || '  initrans 1' || '  maxtrans 255' ||
                   '  storage' || '  ( initial 1M' || '    next 16K' ||
                   '    minextents 1' || '    maxextents 505' ||
                   '    pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.REGION is ''Centro de costo puede ser GYE o UIO.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PERIODO_FACTURACION is ''Cierre del periodo de facturación, siempre 24.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.MONTO is ''Monto amortizado de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.EFECTIVO is ''Todos los pagos en efectivo.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.CREDITOS is ''Todos los pagos en notas de crédito.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_CREDITOS is ''Porcentaje de las notas de crédito vs el total de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.ACUMULADO is ''Total de pagos y notas de crédito.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PAGOS_CO is ''Overpayments.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'alter table ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' add constraint PK' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' primary key (REGION,FECHA) ' || 'using index ' ||
                   'tablespace REP_COB_DAT ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 120K ' || '  next 104K ' || '  minextents 1 ' ||
                   '  maxextents unlimited ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'create index COX_REPCAP_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') || '_01 on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' (PERIODO_FACTURACION,REGION,FECHA) ' ||
                   'tablespace REP_COB_DAT ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 1M ' || '  next 1M ' || '  minextents 1 ' ||
                   '  maxextents 505 ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'create index COX_REPCAP_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') || '_02 on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' (PERIODO_FACTURACION,FECHA) ' ||
                   'tablespace REP_COB_DAT ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 1M ' || '  next 1M ' || '  minextents 1 ' ||
                   '  maxextents 505 ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'grant select, insert, update, delete, references, alter, index on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' to PUBLIC';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
    --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    return 1;
  
  EXCEPTION
    when le_error then
      pv_error := 'FUNCION CREA_TABLA: ' || lvMensErr;
      return 0;
    when others then
      pv_error := 'FUNCION CREA_TABLA: ' || SUBSTR(SQLERRM, 1, 500);
      return 0;
  END CREA_TABLA;
  
  
  PROCEDURE COP_CAPAS_ESTADO(PV_ESTADO VARCHAR2, PD_FECHA OUT DATE, PN_HILOS OUT NUMBER, PN_MESES OUT NUMBER) IS
    CURSOR C_PERIODO IS
      SELECT PERIODO, HILOS_ANTICIPO, MESES FROM CO_CAPAS_PERIODO WHERE ESTADO = PV_ESTADO;
    
    LD_FECHA DATE;
    LB_FOUND BOOLEAN;
    LN_RESULTADO NUMBER :=0;
    LN_MESES     NUMBER;
  BEGIN
    OPEN C_PERIODO;
    FETCH C_PERIODO INTO LD_FECHA, LN_RESULTADO, LN_MESES;
    LB_FOUND := C_PERIODO%FOUND;
    CLOSE C_PERIODO;
    
    PD_FECHA := LD_FECHA;
    PN_HILOS := LN_RESULTADO;
    PN_MESES := LN_MESES;
  END COP_CAPAS_ESTADO;
  
  PROCEDURE COP_ACTUALIZA_ESTADO(PV_ESTADO_INI VARCHAR2, PV_ESTADO_FIN VARCHAR2) IS
    
  BEGIN
    UPDATE CO_CAPAS_PERIODO SET ESTADO = PV_ESTADO_FIN WHERE ESTADO = PV_ESTADO_INI;
    COMMIT;
  END COP_ACTUALIZA_ESTADO;
  
  PROCEDURE COP_CAPAS_MESES_ESTADO(PV_ESTADO VARCHAR2, PD_FECHA DATE, PN_HILOS OUT NUMBER, PD_PERIODO OUT DATE) IS
    CURSOR C_PERIODO IS
      SELECT HILOS, PERIODO FROM CO_CAPAS_PERIODO_MESES WHERE ESTADO = PV_ESTADO
      AND PERIODO_PADRE = PD_FECHA;
    
    LD_FECHA DATE;
    LB_FOUND BOOLEAN;
    LN_RESULTADO NUMBER :=0;
  BEGIN
    OPEN C_PERIODO;
    FETCH C_PERIODO INTO LN_RESULTADO, PD_PERIODO;
    LB_FOUND := C_PERIODO%FOUND;
    CLOSE C_PERIODO;
    
    PN_HILOS := LN_RESULTADO;
  END COP_CAPAS_MESES_ESTADO;
  
  PROCEDURE COP_ACTUALIZA_MESES_ESTADO(PV_ESTADO_INI VARCHAR2, PV_ESTADO_FIN VARCHAR2, PD_PERIODO DATE, PD_PERIODO_PADRE DATE) IS
    
  BEGIN
    UPDATE CO_CAPAS_PERIODO_MESES SET ESTADO = PV_ESTADO_FIN WHERE PERIODO = PD_PERIODO AND ESTADO = PV_ESTADO_INI AND PERIODO_PADRE = PD_PERIODO_PADRE;
    COMMIT;
  END COP_ACTUALIZA_MESES_ESTADO;
  
end COK_REPORTE_CAPAS_NEW;
/
