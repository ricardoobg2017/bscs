create or replace package MFK_API_ENVIO_MASIVO is
 
 PROCEDURE MFP_ENVIO(PN_IDENVIO          MF_MENSAJES.IDENVIO%TYPE, 
                     PN_SECUENCIA_ENVIO  MF_MENSAJES.SECUENCIA_ENVIO%TYPE); 
 
                  
 
end MFK_API_ENVIO_MASIVO; 
/
create or replace package body MFK_API_ENVIO_MASIVO is
 
PROCEDURE MFP_ENVIO(PN_IDENVIO          MF_MENSAJES.IDENVIO%TYPE, 
                    PN_SECUENCIA_ENVIO  MF_MENSAJES.SECUENCIA_ENVIO%TYPE) IS 
 
  ln_secuencia   NUMBER; 
  lv_flag        VARCHAR2(1):=null; 
  
  Cursor C_PARAMETROS(Cn_id_tipo_param Number, Cv_id_param Varchar2) Is
  Select  to_number(valor) 
  From gv_parametros 
  Where id_tipo_parametro = Cn_id_tipo_param --6092 
  And id_parametro = Cv_id_param--'CANT_SMS_X_ENVIO'
  ; 
   
  CURSOR c_Mensajes_Enviar IS 
    SELECT M.DESTINATARIO, M.MENSAJE, M.Secuencia_Mensaje 
    FROM  MF_MENSAJES M 
    WHERE M.SECUENCIA_ENVIO = PN_SECUENCIA_ENVIO 
    AND   M.IDENVIO = PN_IDENVIO 
    AND   M.ESTADO  = 'A'   ; 
  --CIMA MAB 	[6092]-SMS Masivo	Inicio  
  CURSOR C_TIPO_ENVIO(CN_IDENVIO          MF_MENSAJES.IDENVIO%TYPE) IS
  SELECT TIPO_ENVIO
    FROM MF_ENVIOS
   WHERE IDENVIO = CN_IDENVIO
     AND ESTADO = 'A';
     
  CURSOR C_TIPO_NOVEDAD(CN_ID_TIPO        MF_TIPO_ENVIO.ID_TIPO%TYPE)IS
  SELECT ID_TIPO_NOVEDAD, ENVIO_NOVEDAD
    FROM MF_TIPO_ENVIO
   WHERE ID_TIPO = CN_ID_TIPO
     AND ESTADO = 'S'
     AND ENVIO_NOVEDAD = 'S';
     
  CURSOR C_DATOS_CLIENTE(CV_SERVICIO VARCHAR2) IS
  SELECT S.ID_CONTRATO,
         S.ID_SERVICIO,
         S.ID_SUBPRODUCTO,
         C.ID_PERSONA,
         S.ID_DETALLE_PLAN,
         S.CO_ID
         FROM Porta.CL_SERVICIOS_CONTRATADOS@Axis S, Porta.CL_CONTRATOS@Axis C
         WHERE S.ID_SERVICIO = obtiene_telefono_dnc@Axis(CV_SERVICIO)
         AND S.ID_CONTRATO = C.ID_CONTRATO
         AND S.ESTADO = 'A'
         AND C.ESTADO = S.ESTADO;
  --CIMA MAB 	[6092]-SMS Masivo	Fin
  lv_puerto varchar2(100); 
  lv_servidor varchar2(100); 
  lv_mensaje_retorno varchar2(100); 
  ln_contador_mensajes number:=0; 
  ld_fecha_envio      date; 
  ln_minuto number:=0;  
  --CIMA MAB 	[6092]-SMS Masivo	Inicio
  LB_TIPO_ENVIO_NOVEDAD  BOOLEAN := FALSE;
  LN_TIPO_ENVIO  MF_TIPO_ENVIO.ID_TIPO%TYPE;
  lv_error_noved varchar2(2000);  
  LC_DATOS_CLIENTE    C_DATOS_CLIENTE%ROWTYPE; 
  LB_DATOS_CLIENTE    BOOLEAN := FALSE;
  LV_OBSERVACION_NOVED    VARCHAR2(240);
  LV_ENVIO_NOVEDAD        MF_TIPO_ENVIO.ENVIO_NOVEDAD%TYPE;
  LV_ID_TIPO_NOVEDAD      MF_TIPO_ENVIO.ID_TIPO_NOVEDAD%TYPE;
  ln_cantidad             Number;
  lb_found                Boolean:=False;
  ln_commit            Number:= 500;
  ln_cont_commit       Number:=0;
  --CIMA MAB 	[6092]-SMS Masivo	Fin
BEGIN 
         --Obtenemos el tipo de esquema configurado para el envio de SMS Grupal 
         begin 
           select valor  
             into lv_flag  
             from co_parametros  
            where id_parametro = 'SMS_GRUPAL_SCO'; 
         exception 
           when others then 
             lv_flag:='A'; 
         end; 
         --CIMA MAB 	[6092]-SMS Masivo	Inicio
         Open C_PARAMETROS(6092,'CANT_SMS_X_ENVIO');
              Fetch C_PARAMETROS Into ln_cantidad;
              lb_found := C_PARAMETROS%Found;
         Close C_PARAMETROS;     
         If Not lb_found Then
            ln_cantidad := 5000;
         End If;
         OPEN C_TIPO_ENVIO(PN_IDENVIO);
             FETCH C_TIPO_ENVIO INTO LN_TIPO_ENVIO;
         CLOSE C_TIPO_ENVIO;
         
         IF LN_TIPO_ENVIO IS NULL THEN
            LB_TIPO_ENVIO_NOVEDAD := FALSE;
         ELSE
            OPEN C_TIPO_NOVEDAD(LN_TIPO_ENVIO);
                 FETCH C_TIPO_NOVEDAD INTO LV_ID_TIPO_NOVEDAD,LV_ENVIO_NOVEDAD;                 
            CLOSE C_TIPO_NOVEDAD;
            IF LV_ID_TIPO_NOVEDAD IS NOT NULL AND LV_ENVIO_NOVEDAD IS NOT NULL THEN
               LB_TIPO_ENVIO_NOVEDAD := TRUE;
            ELSE
               LB_TIPO_ENVIO_NOVEDAD := FALSE;
            END IF;
         END IF;
         --CIMA MAB 	[6092]-SMS Masivo	Fin

         if lv_flag = 'I' then 
            begin 
--              INSERT INTO SMG_ENVIO_SCO ---PRODUCCION
                INSERT Into sms2.SMG_ENVIO_SCO@sms_masivo--DESARROLLO
                (SELECT SUBSTR(M.DESTINATARIO,5) TELEFONO, --TELEFONO 
                        1,                                 --ID_THREAD 
                        0,                      --ID_SMG_CONF 
                        M.MENSAJE,                         --MENSAJE 
                        --TRUNC(SYSDATE)+(9/24), 
                        --Ya que el proceso de envio de mensajes tiene politicas y prioridades  
                        --en el envio se grabara con un a�o de retraso para que el proceso tome primero estos sms 
                        (trunc(sysdate)-365)+(9/24),       --FECHA_ENVIO 
                        1                                  --MEDIO_TRANS 
                 FROM MF_MENSAJES M 
                 WHERE M.SECUENCIA_ENVIO = PN_SECUENCIA_ENVIO 
                 AND M.IDENVIO = PN_IDENVIO 
                 AND M.ESTADO  = 'A'); 
                 null;
            end; 
         elsif lv_flag = 'A' then 
            --=============================================================================== 
            -- Se agrega logica para que apunte al gestor de mensajes (<<) 
            --=============================================================================== 
            begin 
               ld_fecha_envio:=(trunc(sysdate))+(9/24); 
               --si la fecha calculada ya paso, se deja para sysdate o para el siguiente dia  
               if (ld_fecha_envio<sysdate and (to_number(to_char(sysdate,'hh24'))<19)) then  
                   ld_fecha_envio:=sysdate; 
               else  
                   if (ld_fecha_envio<sysdate and (to_number(to_char(sysdate,'hh24'))>19)) then 
                       ld_fecha_envio:=ld_fecha_envio+1;  
                   end if; 
               end if; 
               for i in c_Mensajes_Enviar loop 
                   
                   notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio=> ld_fecha_envio, 
                                                                     pd_fecha_expiracion =>ld_fecha_envio+2/24, 
                                                                     pv_asunto => '.', 
                                                                     pv_mensaje => i.mensaje,                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                                                                     pv_destinatario => i.destinatario, 
                                                                     pv_remitente => 'CLARO', 
                                                                     pv_tipo_registro => 'S', 
                                                                     pv_clase => 'COB', 
                                                                     pv_puerto => lv_puerto, 
                                                                     pv_servidor => lv_servidor, 
                                                                     pv_max_intentos => '3', 
                                                                     pv_id_usuario => 'BSCS_COBRANZAS', 
                                                                     pv_mensaje_retorno => lv_mensaje_retorno); 
                     If lv_mensaje_retorno Is Null Then
                        MFK_OBJ_MENSAJES.MFP_ACTUALIZAR(PN_IDENVIO,
                                                                               Null,
                                                                               PN_SECUENCIA_ENVIO,
                                                                               i.Secuencia_Mensaje,
                                                                               Null,
                                                                               Null,
                                                                               Null,
                                                                               Null,
                                                                               Null,
                                                                               'I',
                                                                               lv_mensaje_retorno);
                     End If;
                     --CIMA MAB 	[6092]-SMS Masivo	Inicio 
                     IF LB_TIPO_ENVIO_NOVEDAD THEN
                       --DATOS DEL CLIENTE
                       OPEN C_DATOS_CLIENTE(i.destinatario);
                            FETCH C_DATOS_CLIENTE INTO LC_DATOS_CLIENTE;
                            LB_DATOS_CLIENTE := C_DATOS_CLIENTE%FOUND;
                       CLOSE C_DATOS_CLIENTE;
                       
                       IF LB_DATOS_CLIENTE THEN
                       -- CREAR LA NOVEDAD SI ES POR COBRANZAS
                       CLK_TRX_NOVEDADES.CLP_INGRESAR@Axis(SYSDATE,
                                                       	   i.mensaje,
                                                           LV_ID_TIPO_NOVEDAD,
                                                           'POR',
                                                           LC_DATOS_CLIENTE.ID_PERSONA,
                                                           LC_DATOS_CLIENTE.ID_SUBPRODUCTO,
                                                           LC_DATOS_CLIENTE.ID_SERVICIO,
                                                           'A',
                                                           USER,
                                                           SYSDATE,
                                                           LC_DATOS_CLIENTE.ID_CONTRATO,
                                                           'S',
                                                           lv_error_noved);
                       END IF;
                     END IF;
                     --CIMA MAB 	[6092]-SMS Masivo	Fin
                    ln_contador_mensajes:= ln_contador_mensajes+1; 
                    /*if (mod(ln_contador_mensajes,20000)=0) then 
                          --se programan cada 10 minutos 
                          ld_fecha_envio:=ld_fecha_envio+(600/86400); 
                    end if; 
                    if  ( mod(ln_contador_mensajes,1000)=0) then  
                          commit; 
                    end if;                    
                    */
                    
                    ln_cont_commit := ln_cont_commit + 1;
                    If ln_contador_mensajes >= ln_cantidad Then
                       Commit;
                       Exit;
                    End If;
                    If ln_cont_commit >= ln_commit Then
                       Commit;
                       ln_cont_commit:= 0;
                    End If;
                    
               end loop; 
               commit; 
               begin 
                   dbms_session.close_database_link(dblink => 'NOTIFICACIONES'); 
                   dbms_session.close_database_link(dblink => 'AXIS'); 
               exception when others then null; 
               end; 
 
            end; 
            --=============================================================================== 
            -- Se agrega logica para que apunte al gestor de mensajes  (>>) 
            --=============================================================================== 
         end if; 
          
         COMMIT; 
          
         UPDATE MF_MENSAJES M 
         SET M.ESTADO = 'I' 
         WHERE M.SECUENCIA_ENVIO =  PN_SECUENCIA_ENVIO 
         AND   M.IDENVIO         =  PN_IDENVIO 
         AND   M.ESTADO          = 'A'; 
          
         COMMIT; 
        
Exception
 When Others Then
             Rollback;
             dbms_session.close_database_link(dblink => 'NOTIFICACIONES'); 
             dbms_session.close_database_link(dblink => 'AXIS');             
END MFP_ENVIO; 
 
end MFK_API_ENVIO_MASIVO; 
/
