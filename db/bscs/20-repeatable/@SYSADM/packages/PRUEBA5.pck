CREATE OR REPLACE package PRUEBA5 is

  GN_COMMIT NUMBER:=5000;

    FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER;
                             
  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2;                             


end PRUEBA5;
/
CREATE OR REPLACE package body PRUEBA5 is

  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER IS

    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);

  BEGIN

    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura
                       prueba5.set_campos_mora(pdFechaPeriodo) ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0)'||
                       '  tablespace DATA' ||
                       '  pctfree 10' || '  pctused 40' || '  initrans 1' ||
                       '  maxtrans 255' || '  storage' || '  (initial 256K' ||
                       '    next 256K' || '    minextents 1' ||
                       '    maxextents unlimited' || '    pctincrease 0)'; --JHE 28-03-2007 se cambio Unlimited
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea clave primaria
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PKCUSTOMER_ID' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                       '  primary key (CUSTOMER_ID)' || '  using index' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create index CO_CUST_IDX on CO_CUADRE (CUSTCODE)' ||
                       'tablespace IND' || 'pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    --  Crea indice
    lvSentenciaCrea := 'create index ID_CUSTOMER_ID_' ||
                       to_char(pdFechaPeriodo, 'ddMMyyyy') || ' on ' ||
                       pvNombreTabla || ' (CUSTOMER_ID, tipo)' ||
                       '  tablespace DATA' || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    return 1;

  EXCEPTION
    when others then
      return 0;
  END CREA_TABLA_CUADRE;
  
  
    FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2 IS
    lvSentencia varchar2(2000);
    lnMesFinal  number;
  BEGIN
    lnMesFinal  := to_number(to_char(pdFecha, 'MM'));
    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  END SET_CAMPOS_MORA;

end PRUEBA5;
/

