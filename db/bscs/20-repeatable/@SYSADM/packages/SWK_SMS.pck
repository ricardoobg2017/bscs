create or replace package SWK_SMS is

  -- Author  : AGOMEZ
  -- Created : 11/10/2001 09:43:22
  -- Purpose : 

  FUNCTION GETDATOS ( NOMBRE_SERVIDOR     VARCHAR2,  
                      IP              OUT VARCHAR2, 
                      PUERTO          OUT NUMBER 
                    ) RETURN BOOLEAN;
                    
  PROCEDURE SEND ( NOMBRE_SERVIDOR In Varchar2, 
                   ID_SERVCIIO     In Varchar2, 
                   PV_MENSAJE      In Varchar2,
                   PV_MSG_ERR      Out Varchar2,
                   pv_SOURCE       In Varchar2 Default Null
                 );

end SWK_SMS;
/
CREATE OR REPLACE PACKAGE BODY SWK_SMS IS

  FUNCTION GETDATOS ( NOMBRE_SERVIDOR     VARCHAR2,  
                      IP              OUT VARCHAR2, 
                      PUERTO          OUT NUMBER  
                    ) RETURN BOOLEAN IS
                      
    CURSOR C_SERVICIOS_REMOTOS (C_NOMBRE_SERVIDOR VARCHAR2) IS
      SELECT * 
      FROM SW_SERVICIOS_REMOTOS@axis
      WHERE NOMBRE_SERVIDOR = C_NOMBRE_SERVIDOR --'imaget'
      AND SERVICIO = 'SMS';

    LC_SERVICIOS_REMOTOS  C_SERVICIOS_REMOTOS%ROWTYPE;
    LB_FOUND              BOOLEAN;
    
  BEGIN
  
    OPEN C_SERVICIOS_REMOTOS (NOMBRE_SERVIDOR);
    FETCH C_SERVICIOS_REMOTOS INTO LC_SERVICIOS_REMOTOS;
    LB_FOUND := C_SERVICIOS_REMOTOS%FOUND;
    CLOSE C_SERVICIOS_REMOTOS;
    
    IF LB_FOUND THEN
      IP        := LC_SERVICIOS_REMOTOS.IP;
      PUERTO    := LC_SERVICIOS_REMOTOS.PUERTO;
      RETURN TRUE;
    ELSE
      RETURN FALSE;    
    END IF;
    
  END;  
    
  --====================================================================================--                                             
  -- Susana Qui��nez Y-
  -- Sudamericana de Software para Conecel
  -- Julio 7 2004                                             
  --====================================================================================--
  
  -- INPUT:
  -- Pv_Mensaje:        Mensaje a enviar
  -- Pv_NumTelefono     N�mero de tel�fono, s�lo los 7 d�gitos     
  -- Pv_ip              IP deconexion con el proceso SERVER
  -- Pn_puerto          Puerto de conexi�n con el proceso SERVER
  -- Pn_seg_timeout     in number,
  -- Este proceso no espera respuesta 
  
  -- OUTPUT:
  -- pv_respuesta:       Cadena de Respuesta del Servidor
  -- pv_msg_err:         Mensaje de error
  -- pv_cod_err:         Codigo de error: 
  --                     -1 Other Exception,
  --                     0 OK
  --                     1 Network Error
  --                     2 End of Input
  --                     3 Timeout                                             

  PROCEDURE SEND ( NOMBRE_SERVIDOR In Varchar2, 
                   ID_SERVCIIO     In Varchar2, 
                   PV_MENSAJE      In Varchar2,
                   PV_MSG_ERR      Out Varchar2,
                   pv_SOURCE       In Varchar2 Default Null
                 ) IS

    Cv_PreTrama            Constant      varchar2(9):= 'ATF|GYEA|'; 
    Cv_PreTramaP           Constant      Varchar2(10):= 'ATFP|GYEA|';
    --Cv_PreFono             Constant      varchar2(4):='593'; 
    Lv_Trama               varchar2(500) := null; 
    --
    oObjConexion           utl_tcp.connection;          -- TCP/IP connection 
    Len                    PLS_INTEGER;
    Ln_timeout             number        :=  1;         --En segundos
    Lv_charset             varchar2(10)  :='US7ASCII';  --Character set for on-the-wire communication

    IP         VARCHAR2(20); 
    PUERTO     NUMBER;
    lv_telefono  varchar2(20);
    
  BEGIN
  
    pv_msg_err := null;
   
    IF GETDATOS ( NOMBRE_SERVIDOR,
                  IP,
                  PUERTO) THEN       
    
      oObjConexion := utl_tcp.open_connection ( remote_host => IP,
                                                remote_port => PUERTO,
                                                charset     => lv_charset
                                                );
                            
      dbms_lock.sleep(0.20); --dbms_lock.sleep(0.45);      
      lv_telefono:=obtiene_telefono_dnc_int (ID_SERVCIIO);                         
      
      --Cadena a enviar por socket al Server  
      If pv_SOURCE Is Null Then
         Lv_Trama := Cv_PreTrama ||Pv_Mensaje||'|'||lv_telefono;--Cv_PreFono||ID_SERVCIIO;
      Else
         lv_trama := Cv_PreTramaP||PV_MENSAJE||'|'||lv_telefono||'|'||pv_SOURCE;
      End If;
      
--      len := utl_tcp.write_line(oObjConexion,Lv_Trama); 
      len := utl_tcp.write_text(oObjConexion,Lv_Trama);       
        
      --Cerrar conexion con el Server
      utl_tcp.close_connection(oObjConexion);  
               
    END IF;
    
  EXCEPTION 
    when utl_tcp.network_error then
      pv_msg_err:=sqlerrm;
    when others then
      pv_msg_err:=sqlerrm;
  END;
                    
END SWK_SMS;
/
