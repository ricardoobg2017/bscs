CREATE OR REPLACE package DEPURA_TABLAS_CM Is
   PROCEDURE DEPURA_TABLAS(pv_tabla       varchar2,
                           pv_duenio       varchar2,
                           pv_campo_fecha Varchar2,
                           pn_dias_atras  Varchar2,
                           pn_reg_eli Out Number,
                           pn_error   Out Number,
                           pv_error   Out Varchar2);

   PROCEDURE DEPURA_X_CONDICION(pv_tabla       varchar2,
                                pv_duenio       varchar2,
                                pv_condicion   Varchar2,
                                pn_reg_eli Out Number,
                                pn_error   Out Number,
                                pv_error   Out Varchar2);

  PROCEDURE BORRA_REGISTROS(pv_tabla varchar2,
                              pv_duenio Varchar2,
                              pv_sentencia Varchar2,
                              pn_reg_eli Out Number,
                              pn_error Out Number,
                              pv_error Out Varchar2)  ;
end DEPURA_TABLAS_CM;
/

