CREATE OR REPLACE package CONCILIA_DETLLAM_TOTAL_1 is

  -- Author  : DCHONILLO
  -- Created : 21/04/2004 08:53:56
  -- Purpose : Identificar telefonos cuya factura  no presenta correctamente el TOTAL en las llamadas entrantes       

  PROCEDURE DETALLE_SIN_TOTAL (fecha_inicio varchar2,
                               fecha_fin varchar2);
  PROCEDURE TOTAL_LLAMADAS(contrato in number, 
                           custom   in number);
end CONCILIA_DETLLAM_TOTAL_1;
/
CREATE OR REPLACE package body CONCILIA_DETLLAM_TOTAL_1 is

  PROCEDURE DETALLE_SIN_TOTAL (fecha_inicio varchar2, fecha_fin varchar2) IS
  /* Formato de fecha es : dd/mm/yyyy hh24:mi:ss */
  
  /*Detalle: todos los telefonos con detalle de llamadas activo, y tambi�n los 
  suspendidos o deactivados entre un rango de fecha ingresado por el usuario*/
  cursor detalle is 
  select /*+ rule +*/ a.co_id,c.customer_id
  from profile_service a, pr_serv_status_hist b, contract_all c
  where c.co_id=b.co_id 
  and b.histno=a.status_histno
  and a.sncode in (13,31) 
  and b.co_id=a.co_id
  and b.status='A' 
  union all
  select /*+ rule +*/ a.co_id,c.customer_id
  from profile_service a, pr_serv_status_hist b, contract_all c
  where c.co_id=b.co_id 
  and b.histno=a.status_histno
  and a.sncode in (13,31) 
  and b.co_id=a.co_id
  and b.status ='S' and b.valid_from_date between to_date (fecha_inicio,'DD/MM/YYYY hh24:mi:ss') 
  and to_date (fecha_fin,'DD/MM/YYYY hh24:mi:ss')
  union all
  select /*+ rule +*/ a.co_id, c.customer_id
  from profile_service a, pr_serv_status_hist b,  contract_all c
  where  c.co_id=b.co_id 
  and b.histno=a.status_histno
  and a.sncode in (13,31) 
  and b.co_id=a.co_id
  and b.status ='D' and b.valid_from_date between to_date (fecha_inicio,'DD/MM/YYYY hh24:mi:ss') 
  and to_date (fecha_fin,'DD/MM/YYYY hh24:mi:ss');
 
  /*guarda los datos obtenidos del cursor detalle*/
  cursor temp is 
  select * from BS_TMP_COID where estado is null;
 

  BEGIN    

   Begin
--     tmp_monitor_diana para monitorear el proceso
     insert into tmp_monitor_diana VALUES ('INICIO CARGA TABLA',SYSDATE);
     COMMIT;
     for j in detalle loop 
     insert into BS_TMP_COID values (J.CO_ID,J.CUSTOMER_ID,NULL,NULL,NULL);
     end loop;
     commit;
     insert into tmp_monitor_diana VALUES ('FIN CARGA TABLA',SYSDATE);
     COMMIT;
   end;
  
--   begin
     insert into tmp_monitor_diana VALUES ('INICIA CONSULTA LLAMADAS',SYSDATE);
     commit;

   for i  IN temp LOOP
      concilia_detllam_total_1.total_llamadas (i.co_id, i.customer_id);
      update bs_tmp_coid set estado ='PROCESADO', fecha=sysdate 
      where co_id=i.co_id and customer_id=i.customer_id;
      commit;
   end loop;

   insert into tmp_monitor_diana VALUES ('FIN CONSULTA LLAMADAS',SYSDATE);
   commit;
  --- end;
     --end;
  
  END DETALLE_SIN_TOTAL;
  



  PROCEDURE TOTAL_LLAMADAS(contrato in number, custom in number) is
  /*procedimiento que verifica todas los tel�fonos que no tienen llamada saliente
  y al menos tienen un llamada entrante*/
  
  cursor numero (coid number) is /*para sacar el n�mero de telefono*/
  select f.dn_num from 
  contract_all d,
  directory_number f, 
  contr_services_cap g
  where d.co_id=coid
  and g.co_id=d.co_id 
  and f.dn_id=g.dn_id;
  
  cursor cuenta (contract_id number) is 
  select cust.custcode 
  from customer_all cust, contract_all cont
  where cont.customer_id=cust.customer_id 
  and cont.co_id=contract_id;

  mensaje varchar2(400);
  le_error exception;
  observacion varchar2(500);
  entrantes number;
  salientes number;
  telefono varchar2(15);
  dat_num numero%rowtype;
  existe boolean;
  cur_cuenta cuenta%rowtype;
  num_cuenta varchar2(50);
  --numero varchar2(15);
  --
  
  begin
  
  select count(*) into salientes
  from sysadm.udr_lt_diaria@BSCS_TO_RTX_LINK 
  where cust_info_customer_id=custom 
  and  cust_info_contract_id=contrato
  and  follow_up_call_type=1
  and rownum=1;
  
  if salientes=0 then
  
    select count(*) into entrantes
    from sysadm.udr_lt_diaria@BSCS_TO_RTX_LINK 
    where cust_info_customer_id=custom 
    and  cust_info_contract_id=contrato
    and follow_up_call_type=2
    and rownum=1;
  
    if entrantes>0 then
  
        open numero(contrato);
        fetch numero into dat_num;
        existe:=numero%found;    
        close numero;
        
        telefono:=to_number(dat_num.dn_num);      

        open cuenta(contrato);
        fetch cuenta into cur_cuenta;
        close cuenta;

       num_cuenta:=cur_cuenta.custcode;

      begin
        insert into es_registros values 
        (sysdate,5,custom,contrato,'A','REVISAR_DETALLE',sysdate,telefono,null,null,null,num_cuenta,'DCH');
        
       EXCEPTION 
       WHEN OTHERS THEN
       raise le_error;
       end;
  
     end if;
  end if;
   
  commit;      
  Exception 
  When  LE_ERROR Then
  MENSAJE:='ERROR AL INGRESAR DATOS' ||SQLERRM;
  DBMS_OUTPUT.PUT_LINE(MENSAJE);
  Rollback;
  
  end TOTAL_LLAMADAS;

end CONCILIA_DETLLAM_TOTAL_1;
/

