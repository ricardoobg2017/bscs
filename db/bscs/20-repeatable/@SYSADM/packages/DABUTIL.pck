CREATE OR REPLACE PACKAGE DabUtil
/*
**
**
** References:
**    HLD plsql/73998.rtf: Database Migration Concepts II
**
** Author: Peter Sorg, LH Specifications GmbH
**
** Filenames: dabutil.sph - Package header
**            dabutil.spb - Package body
**
** Overview:
**
** This package contains utility procedures Rfor database administration.
** It wrappes some Oracle DDL statements for better error handling.
**
**
** Major Modifications (when, who, what)
**
** 11/00 - PSO - initial version of DabUtil package
** 04/01 - PSO - enhancements for Dublin
*/
IS
   /*------------------------- CMVC version control------------------------*/
   -- constants can not be used for type anchoring. Therefore cosHeadScriptVersion
   -- and cosHeadScriptWhat are not defined to be CONSTANT here.
   cosHeadScriptVersion VARCHAR2(256)  := '/main/12';
   cosHeadScriptWhat    VARCHAR2(256) := 'but_bscs/bscs/database/migutil/orapack/dabutil.sph, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

   /*------------------------- Package Parameters -------------------------*/
   gobSecureInd BOOLEAN := TRUE;

   /*
   ** The following types should by used in all BSCS procedures,
   ** to declare the constants cosHeadScriptVersion and cosHeadScriptWhat.
   */
   SUBTYPE tosScriptVersion IS cosHeadScriptVersion%TYPE;
   SUBTYPE tosScriptWhat    IS cosHeadScriptWhat%TYPE;

   /*
   ** Global constants
   */
   cosNewline VARCHAR2(1) := CHR( 10 );

   /*
   ** Subtype for the database schema
   */
   cosSchema VARCHAR2(30) := 'SYSADM';
   SUBTYPE tosSchema IS cosSchema%TYPE;

   /*
   ** Subtype for the database object names
   */
   cosObjectName VARCHAR2(30) := 'Object Name';
   SUBTYPE tosObjectName IS cosObjectName%TYPE;

   /*
   ** Subtype for the database attribute and column names
   */
   cosAttributeName VARCHAR2(30) := 'Attribute or Column Name';
   SUBTYPE tosAttributeName IS cosAttributeName%TYPE;

   /*
   ** Subtype for a general key value of a column.
   ** Ancors logically to MIG_PROGRESS.KEYVALUE columns.
   */
   cosKeyValue VARCHAR2(256) := 'General Key Value';
   SUBTYPE tosKeyValue IS cosKeyValue%TYPE;

   /*
   ** This record is used to define the range partioning of a table.
   ** It is used for parallel migration.
   */
   TYPE torKeyValue IS RECORD
   (
       sKeyMaxValue       tosKeyValue
      ,nNumberOfKeys      INTEGER
      ,nNumberOfRows      INTEGER
   );
   TYPE tarKeyValues IS TABLE OF torKeyValue
   INDEX BY BINARY_INTEGER;

   /*
   ** Subtype for privilegs
   */
   cosPrivileg VARCHAR2( 10 ) := 'REFERENCES';
   SUBTYPE tosPrivileg IS cosPrivileg%TYPE;

   /*
   ** Subtype for the database object names
   */
   cosDBLink VARCHAR2(128) := 'Database Link';
   SUBTYPE tosDBLink IS cosDBLink%TYPE;

   /*
   ** Subtype for the storage clauses
   */
   cosStorageClause VARCHAR2(4000) := 'Storage Clause';
   SUBTYPE tosStorageClause IS cosStorageClause%TYPE;

   /*
   ** Subtype and constants for object types
   */
   cosObject VARCHAR2(1) := 'O';
   SUBTYPE       tosObjectType IS cosObject%TYPE;
   cosTable      CONSTANT tosObjectType := 'T';
   cosPkey       CONSTANT tosObjectType := 'P';
   cosUkey       CONSTANT tosObjectType := 'U';
   cosFkey       CONSTANT tosObjectType := 'R';
   cosIndex      CONSTANT tosObjectType := 'I';
   cosPartition  CONSTANT tosObjectType := 'Q';
   cosPackHeader CONSTANT tosObjectType := 'H';
   cosPackBody   CONSTANT tosObjectType := 'B';

   /*
   ** Subtypes for parameter handling
   */
   cosParaKey VARCHAR2( 30 ) := 'PARA_KEY';
   cosParaValue VARCHAR2( 256 ) := 'Parameter character value';
   conParaValue NUMBER := 0;
   SUBTYPE tosParaKey   IS cosParaKey%TYPE;
   SUBTYPE tosParaValue IS cosParaValue%TYPE;
   SUBTYPE tonParaValue IS conParaValue%TYPE;

   /*
   ** Subtype for the text of the SQL statement
   */
   cosSQLText VARCHAR2(2000) := 'SQL Text';
   SUBTYPE tosSQLText IS cosSQLText%TYPE;

   /*
   ** Subtype for one line of an SQL statement
   */
   cosTextLine VARCHAR2(255) := 'SQL Text Line';
   SUBTYPE tosTextLine IS cosTextLine%TYPE;

   /*
   ** Subtype for CASCADE word
   */
   cosTextCascade  VARCHAR2(9) := ' CASCADE ';
   SUBTYPE tosTextCascade IS cosTextCascade%TYPE;

   /*
   ** Array of SQL code lines
   ** Stores any segment of code.
   ** The line number coincides with the array index.
   */
   TYPE tasText IS TABLE OF tosTextLine;

 /*
   **=====================================================
   **
   ** Procedure: DropAllConstraints
   **
   ** Purpose:  Drop a constraint of a column
   **           or all constraints of the table.
   **
   **=====================================================
*/
   PROCEDURE DropAllConstraints
   (
	-- Table Name
	piosTableName IN  tosObjectName

	-- Column Name
	,piosColumnName IN  tosObjectName DEFAULT NULL
   );
 /*
   **=====================================================
   **
   ** Procedure: DropAllIndexes
   **
   ** Purpose:  Drop a index of a column
   **           or all indexes of the table.
   **
   **=====================================================
*/
   PROCEDURE DropAllIndexes
   (
	-- Table Name
	piosTableName IN  tosObjectName

	-- Column Name
	,piosColumnName IN  tosObjectName DEFAULT NULL
   );

   /*
   **=====================================================
   **
   ** Procedure: CreateSynonym
   **
   ** Purpose:  Drop and recreate a synonym.
   **
   **=====================================================
   */
   PROCEDURE CreateSynonym
   (
       -- Synonym Owner
       piosOwner IN tosSchema

       -- Synonym Name
      ,piosSynonymName IN  tosObjectName

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

       -- Database Link
      ,piosDBLink IN tosDBLink DEFAULT( NULL )

       -- Database Schema
      ,piosSchema IN tosSchema DEFAULT( NULL )

       -- PN 215070-1
       -- Replace existing Synonym
      ,piosReplace IN tosSchema DEFAULT( 'Y' )

   );

   /*
   **=====================================================
   **
   ** Procedure: DropSynonym
   **
   ** Purpose:  Drop synonym.
   **
   **=====================================================
   */
   PROCEDURE DropSynonym
   (
       -- Synonym Owner
       piosOwner IN tosSchema

       -- Synonym Name
      ,piosSynonymName IN  tosObjectName

   );



   /*
   **=====================================================
   **
   ** Procedure: CreateDBLink
   **
   ** Purpose:  Drop and recreate a database link.
   **           Only public database linkes are supported.
   **
   **=====================================================
   */

   PROCEDURE CreateDBLink
   (
       -- Database link name
       piosDBLink IN  tosDBLink

       -- Remote database name
      ,piosDBConnectString IN  tosDBLink

   );

   /*
   **=====================================================
   **
   ** Procedure: CreateRole
   **
   ** Purpose:  Create a role in the database
   **
   **=====================================================
   */
   PROCEDURE CreateRole
   (
       -- Synonym Name
       piosRoleName IN  tosObjectName

   );

   /*
   **=====================================================
   **
   ** Procedure: GrantPriv
   **
   ** Purpose:  Grant privilegs to an object for a role or user
   **
   **=====================================================
   */
   PROCEDURE GrantPriv
   (
       -- Object Name
       piosObjectName IN  tosObjectName

       -- Role Name
      ,piosRoleName IN  tosObjectName

       -- Privilegs
      ,piosPrivileg_1 IN tosPrivileg
      ,piosPrivileg_2 IN tosPrivileg DEFAULT NULL
      ,piosPrivileg_3 IN tosPrivileg DEFAULT NULL
      ,piosPrivileg_4 IN tosPrivileg DEFAULT NULL
   );

   /*
   **=====================================================
   **
   ** Procedure: DropTable
   **
   ** Purpose:  Drop a table.
   **
   **=====================================================
   */
   PROCEDURE DropTable
   (
       -- Table Name
       piosTableName IN  tosObjectName
   );

   /*
   **=====================================================
   **
   ** Procedure: DropConstraint
   **
   ** Purpose:  Drop a constraint of a table.
   **
   **=====================================================
   */
   PROCEDURE DropConstraint
   (
       -- Table Name
       piosTableName IN  tosObjectName

       -- Table Name
      ,piosConstraintName IN  tosObjectName
      ,piosConstraintType IN  tosObjectType DEFAULT(cosFkey)
   );

   /*
   **=====================================================
   **
   ** Procedure: DropView
   **
   ** Purpose:  Drop a view.
   **
   **=====================================================
   */
   PROCEDURE DropView
   (
       -- View Name
       piosViewName IN  tosObjectName

   );

   /*
   **=====================================================
   **
   ** Procedure: DropIndex
   **
   ** Purpose:  Drop a Index.
   **
   **=====================================================
   */
   PROCEDURE DropIndex
   (
       -- Index Name
       piosIndexName IN  tosObjectName
   );

   /*
   **=====================================================
   **
   ** Procedure: DropSequence
   **
   ** Purpose:  Drop a Sequence.
   **
   **=====================================================
   */
   PROCEDURE DropSequence
   (
       -- Index Name
       piosSequenceName IN  tosObjectName
   );

   /*
   **=====================================================
   **
   ** Procedure: CompileAll
   **
   ** Purpose:   Recompile all invalid views, packages and
   **            triggers in one step.
   **
   **=====================================================
   */
   PROCEDURE CompileAll
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)
   );

   /*
   **=====================================================
   **
   ** Procedure: CompileViews
   **
   ** Purpose:  Recompile all invalid views.
   **
   **=====================================================
   */
   PROCEDURE CompileViews
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   );

   /*
   **=====================================================
   **
   ** Procedure: CompilePackages
   **
   ** Purpose:  Recompile all invalid packages.
   **
   **=====================================================
   */
   PROCEDURE CompilePackages
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   );

   /*
   **=====================================================
   **
   ** Procedure: CompilePackageBodies
   **
   ** Purpose:  Recompile all invalid package bodies.
   **
   **=====================================================
   */
   PROCEDURE CompilePackageBodies
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   );

   /*
   **=====================================================
   **
   ** Procedure: CompileTriggers
   **
   ** Purpose:  Recompile all invalid triggers.
   **
   **=====================================================
   */
   PROCEDURE CompileTriggers
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   );

   /*
   **=====================================================
   **
   ** Procedure: ShowError
   **
   ** Purpose:  Check whether an Oracle object (procedure) is valid
   **
   **=====================================================
   */
   PROCEDURE ShowError
   (
       -- Object Name
       piosObjectName IN  tosObjectName

       -- Object Type
      ,piosObjectType IN  tosObjectType
   );

   /*
   **=====================================================
   **
   ** Procedure: DisableForeignKeys
   **
   ** Purpose:  Disable all foreign keys referencing a table.
   **
   **=====================================================
   */
   PROCEDURE DisableForeignKeys
   (
       -- Database Schema
       piosSchema IN tosSchema

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

   );

   /*
   **=====================================================
   **
   ** Procedure: EnableForeignKeys
   **
   ** Purpose:  Enable all foreign of a table.
   **
   **=====================================================
   */
   PROCEDURE EnableForeignKeys
   (
       -- Database Schema
       piosSchema IN tosSchema

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

   );

   /*
   **=====================================================
   **
   ** Procedure: AddText
   **
   ** Purpose:  Add a line to the text.
   **           Is possibly overloaded.
   **
   **=====================================================
   */
   PROCEDURE AddText
   (
       -- Text line
       piosTextLine IN tosTextLine

       -- Text (array)
      ,pmasText IN OUT  tasText
   );

   /*
   **=====================================================
   **
   ** Procedure: AddText
   **
   ** Purpose:  Append text to a text.
   **           Is possibly overloaded.
   **
   **=====================================================
   */
   PROCEDURE AddText
   (
       -- Text (array) to append
       piasSection IN tasText

       -- Text (array)
      ,pmasText IN OUT  tasText
   );

   /*
   **=====================================================
   **
   ** Procedure: CopySql
   **
   ** Purpose:  Copy a text array into an SQL array for DBMS_SQL
   **
   **=====================================================
   */
   PROCEDURE CopySql
   (
       -- Text (array)
       piasText IN tasText

       -- Sql array
      ,poasSqlText OUT  DBMS_SQL.VARCHAR2S
   );

   /*
   **=====================================================
   **
   ** Procedure: PrintText
   **
   ** Purpose:  Print a text using dbms_output.
   **
   **=====================================================
   */
   PROCEDURE PrintText
   (
       -- Text (array)
       pmasText IN tasText
   );

   /*
   **=====================================================
   **
   ** Procedure: DropConstraintsAndIndexes
   **
   ** Purpose:  Drops all  constraints and Indexes of table
   **           piosTableName and column piosColumnName .
   **           If piosColumnName is omitted drops all
   **	        constraints and indexes of the table piosTableName
   **
   **=====================================================
   */
   PROCEDURE DropConstraintsAndIndexes
   (
      	piosTableName IN   tosObjectName
      	,piosColumnName IN  tosObjectName DEFAULT NULL
   );

END DabUtil;
/
CREATE OR REPLACE PACKAGE BODY DabUtil
/*
**
**
** References:
**    HLD plsql/73998.rtf: Database Migration Concepts II
**
** Author: Peter Sorg, LH Specifications GmbH
**
** Filenames: dabutil.sph - Package header
**            dabutil.spb - Package body
**
** Overview:
**
** This package contains utility procedures Rfor database administration.
** It wrappes some Oracle DDL statements for better error handling.
**
**
** Major Modifications (when, who, what)
**
** 11/00 - PSO - Initial version of DabUtil package
**
** 06/02 - NKU - Drop All Constaints are added
**
** 07/02 - OLA - Recompile invalid Packages and Triggers
**
** 07/02 - OLA - Drop Sequence
*/
IS

   /*------------------------- CMVC version control------------------------*/
   cosBodyScriptVersion CONSTANT tosScriptVersion := '/main/11';
   cosBodyScriptWhat    CONSTANT tosScriptWhat    := 'but_bscs/bscs/database/migutil/orapack/dabutil.spb, , R_BSCS_7.00_CON01, L_BSCS_7.00_CON01_030327';

   /* ---------------------------------------------------------------------
   **  Static variables and exceptions
   */
   seoApplicationError EXCEPTION;
   seoNoPrivateSynonym EXCEPTION;
   seoNoPublicSynonym  EXCEPTION;
   seoNoDatabaseLink   EXCEPTION;
   seoNoTable          EXCEPTION;
   seoNoIndex          EXCEPTION;
   seoRoleConflict     EXCEPTION;
   seoNoConstraint     EXCEPTION;
   seoDupSynonym       EXCEPTION;
   seoNoSequence       EXCEPTION;
   sosErrorText VARCHAR2(2000) := NULL;
   sosSqlErrm VARCHAR2(2000) := NULL;

   PRAGMA EXCEPTION_INIT( seoApplicationError , -20002 );
   PRAGMA EXCEPTION_INIT( seoNoPrivateSynonym , -1434 );
   PRAGMA EXCEPTION_INIT( seoNoPublicSynonym , -1432 );
   PRAGMA EXCEPTION_INIT( seoNoDatabaseLink , -2024 );
   PRAGMA EXCEPTION_INIT( seoNoTable , -942 );
   PRAGMA EXCEPTION_INIT( seoNoIndex , -1418 );
   PRAGMA EXCEPTION_INIT( seoRoleConflict , -1921 );
   PRAGMA EXCEPTION_INIT( seoNoConstraint , -2443 );
   PRAGMA EXCEPTION_INIT( seoDupSynonym   , -955 );
   PRAGMA EXCEPTION_INIT( seoNoSequence   , -02289 );

   /*
   ** *****************************************************
   ** Cursor definitions
   */

   /*
   ** Get Synonyms
   */
   CURSOR socGetSynonym
   (
       pcosObjectName tosObjectName
      ,pcosSchema tosSchema
   )
   IS
      SELECT  OWNER
             ,SYNONYM_NAME
      FROM    DBA_SYNONYMS
      WHERE   TABLE_OWNER = pcosSchema
      AND     TABLE_NAME = pcosObjectName;

   /*
   ** Get invalid views
   */
   CURSOR socInvalidView
   (
       pcosSchema tosSchema
   )
   IS
      SELECT OBJECT_NAME
      FROM   DBA_OBJECTS
      WHERE  OWNER = pcosSchema
      AND    OBJECT_TYPE = 'VIEW'
      AND    STATUS = 'INVALID';

   /*
   ** Get invalid packages
   */
   CURSOR socInvalidPackage
   (
       pcosSchema tosSchema
   )
   IS
      SELECT OBJECT_NAME
      FROM   DBA_OBJECTS
      WHERE  OWNER = pcosSchema
      AND    OBJECT_TYPE = 'PACKAGE'
      AND    STATUS = 'INVALID';

   /*
   ** Get invalid package bodies
   */
   CURSOR socInvalidPackageBody
   (
       pcosSchema tosSchema
   )
   IS
      SELECT OBJECT_NAME
      FROM   DBA_OBJECTS
      WHERE  OWNER = pcosSchema
      AND    OBJECT_TYPE = 'PACKAGE BODY'
      AND    STATUS = 'INVALID';

   /*
   ** Get invalid triggers
   */
   CURSOR socInvalidTrigger
   (
       pcosSchema tosSchema
   )
   IS
      SELECT OBJECT_NAME
      FROM   DBA_OBJECTS
      WHERE  OWNER = pcosSchema
      AND    OBJECT_TYPE = 'TRIGGER'
      AND    STATUS = 'INVALID';

   /*
   ** Get invalid Object(s)
   */
   CURSOR socInvalidObject
   (
       pcosSchema tosSchema
      ,pcosObjectName tosObjectName
      ,pcosObjectLongType VARCHAR2
   )
   IS
      SELECT  OBJECT_NAME
             ,STATUS
      FROM    DBA_OBJECTS
      WHERE   OWNER = pcosSchema
      AND     OBJECT_NAME like pcosObjectName
      AND     OBJECT_TYPE = pcosObjectLongType ;

   /*
   ** Get referencing foreign key constraint
   */
   CURSOR socForeignKey
   (
       pcosSchema tosSchema
      ,pcosObjectName tosObjectName
   )
   IS
      SELECT  CONSTRAINT_NAME
             ,TABLE_NAME
             ,STATUS
      FROM   DBA_CONSTRAINTS
      WHERE  OWNER = pcosSchema
      AND    TABLE_NAME like pcosObjectName
      AND    CONSTRAINT_TYPE = 'R'
      ORDER BY TABLE_NAME;


   /*------------------------- Private Procedures --------------------------*/


   /*
   **=====================================================
   **
   ** Procedure: CallError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */

   PROCEDURE CallError( piosErrorText IN VARCHAR2 )
   IS
   BEGIN
      sosErrorText := piosErrorText;
      sosSqlErrm := SQLERRM;
      RAISE seoApplicationError;
   END CallError;

   /*
   **=====================================================
   **
   ** Procedure: RaiseError
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */

   PROCEDURE RaiseError( piosProcName IN VARCHAR2 )
   IS
      losErrorText VARCHAR2(2000) ;
   BEGIN
      losErrorText := 'DabUtil.' || piosProcName || ' : ';
      IF sosErrorText IS NULL
      THEN
         losErrorText := losErrorText || SQLERRM;
      ELSE
         losErrorText := losErrorText || sosErrorText || ' ' || sosSqlErrm ;
      END IF;
      sosErrorText := NULL;
      sosSqlErrm := NULL;
      RAISE_APPLICATION_ERROR( -20001 , losErrorText, TRUE );
   END RaiseError;

   /*
   **=====================================================
   **
   ** Procedure: CallInfo
   **
   ** Purpose:  Privat procedure for information print out (debug).
   **
   **=====================================================
   */

      PROCEDURE CallInfo( piosInfoText IN VARCHAR2 )
      IS
      BEGIN
         DBMS_OUTPUT.PUT_LINE( 'REM INFO: ' || piosInfoText );
      END CallInfo;

   /*
   **=====================================================
   **
   ** Procedure: CallWarning
   **
   ** Purpose:  Privat procedure for error handling.
   **
   **=====================================================
   */

   PROCEDURE CallWarning( piosInfoText IN VARCHAR2 )
   IS
   BEGIN
      DBMS_OUTPUT.PUT_LINE( 'REM WARNING: ' || piosInfoText || SQLERRM );
   END CallWarning;

   /*------------------------- Public Modules -----------------------------*/
   /*
   ** Version control
   */
   FUNCTION PrintBodyVersion RETURN VARCHAR2
   IS
   BEGIN
      RETURN cosBodyScriptVersion || '|' || cosBodyScriptWhat;
   END PrintBodyVersion;

   /*
   **=====================================================
   **
   ** Procedure: CreateSynonym
   **
   ** Purpose:  Drop and recreate a synonym.
   **
   **=====================================================
   */
   PROCEDURE CreateSynonym
   (
       -- Synonym Owner
       piosOwner IN tosSchema

       -- Synonym Name
      ,piosSynonymName IN  tosObjectName

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

       -- Database Link Name
      ,piosDBLink IN tosDBLink DEFAULT( NULL )

       -- Database Schema
      ,piosSchema IN tosSchema DEFAULT( NULL )

       -- PN 215070-1
       -- Replace existing Synonym
      ,piosReplace IN tosSchema DEFAULT( 'Y' )

   )
   IS
      losSQLText tosSQLText;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop (public) synonym
      IF upper( piosOwner ) = 'PUBLIC'
      THEN
         losSQLText := 'DROP PUBLIC SYNONYM ' || piosSynonymName;
      ELSIF piosOwner IS NOT NULL
      THEN
         losSQLText := 'DROP SYNONYM ' || piosOwner || '.' || piosSynonymName;
      ELSE
         losSQLText := 'DROP SYNONYM ' || piosSynonymName;
      END IF;

      -----------------------------------------------------------
      -- Execute SQL command: drop (public) synonym
      -- PN 215070-1
      IF piosReplace = 'Y' THEN
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoPrivateSynonym
         THEN
            CallInfo( 'Private synonym did not exists.' );
         WHEN seoNoPublicSynonym
         THEN
            CallInfo( 'Public synonym did not exist.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;
      END IF;

      -----------------------------------------------------------
      -- Build SQL command: create (public) synonym
      IF upper( piosOwner ) = 'PUBLIC'
      THEN
         losSQLText := 'CREATE PUBLIC SYNONYM ' || piosSynonymName ;
      ELSIF piosOwner IS NOT NULL
      THEN
         losSQLText := 'CREATE SYNONYM ' || piosOwner || '.' || piosSynonymName;
      ELSE
         losSQLText := 'CREATE SYNONYM ' || piosSynonymName;
      END IF;

      IF piosSchema IS NULL
      THEN
         losSQLText := losSQLText || ' FOR ';
      ELSE
         losSQLText := losSQLText || ' FOR ' || piosSchema || '.' ;
      END IF;

      IF piosObjectName IS NULL
      THEN
         losSQLText := losSQLText || piosSynonymName;
      ELSE
         losSQLText := losSQLText || piosObjectName ;
      END IF;

      IF piosDBLink IS NOT NULL
      THEN
         losSQLText := losSQLText || '@' || piosDBLink;
      END IF;

      -----------------------------------------------------------
      -- Execute SQL command: create (public) synonym
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoDupSynonym
         THEN
            -- PN 215070-1
            IF piosReplace = 'N' THEN
               CallInfo( 'Synonym did already exist.' || losSQLText || '".' );
            ELSE
               CallError( 'Synonym did already exist.' || losSQLText || '".' );
            END IF;
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CreateSynonym' );
   END CreateSynonym;

   /*
   **=====================================================
   **
   ** Procedure: DropSynonym
   **
   ** Purpose:  Drop synonym.
   **
   **=====================================================
   */
   PROCEDURE DropSynonym
   (
       -- Synonym Owner
       piosOwner IN tosSchema

       -- Synonym Name
      ,piosSynonymName IN  tosObjectName

   )
   IS
      losSQLText tosSQLText;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop (public) synonym
      IF upper( piosOwner ) = 'PUBLIC'
      THEN
         losSQLText := 'DROP PUBLIC SYNONYM ' || piosSynonymName;
      ELSIF piosOwner IS NOT NULL
      THEN
         losSQLText := 'DROP SYNONYM ' || piosOwner || '.' || piosSynonymName;
      ELSE
         losSQLText := 'DROP SYNONYM ' || piosSynonymName;
      END IF;

      -----------------------------------------------------------
      -- Execute SQL command: drop (public) synonym
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoPrivateSynonym
         THEN
            CallInfo( 'Private synonym did not exists.' );
         WHEN seoNoPublicSynonym
         THEN
            CallInfo( 'Public synonym did not exist.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropSynonym' );
   END DropSynonym;

   /*
   **=====================================================
   **
   ** Procedure: CreateDBLink
   **
   ** Purpose:  Drop and recreate a database link.
   **           Only public database linkes are supported.
   **
   **=====================================================
   */
   PROCEDURE CreateDBLink
   (
       -- Database link name
       piosDBLink IN  tosDBLink

       -- Remote database name
      ,piosDBConnectString IN  tosDBLink

   )
   IS
      losSQLText tosSQLText;
      losSQLCheck tosSQLText;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop (public) database link
      losSQLText := 'DROP PUBLIC DATABASE LINK ' || piosDBLink;

      -----------------------------------------------------------
      -- Execute SQL command: drop (public) database link
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoDatabaseLink
         THEN
            CallInfo( 'Database link did not exists.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      -----------------------------------------------------------
      -- Build SQL command: create (public) database link
      losSQLText := 'CREATE PUBLIC DATABASE LINK ' || piosDBLink ;

      -----------------------------------------------------------
      -- Continue SQL command
      losSQLText := losSQLText || ' USING ' || '''' || piosDBConnectString || '''';

      -----------------------------------------------------------
      -- Execute SQL command: create (public) synonym
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      -----------------------------------------------------------
      -- Check database link
      losSQLCheck := 'SELECT 1 FROM DUAL@' || piosDBlink ;
      BEGIN
         EXECUTE IMMEDIATE losSQLCheck;
         CallInfo( losSQLCheck );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallWarning( 'Can not execute: "' || losSQLCheck || '".' );
      END;


   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CreateDBLink' );
   END CreateDBLink;

   /*
   **=====================================================
   **
   ** Procedure: CreateRole
   **
   ** Purpose:  Create a role in the database
   **
   **=====================================================
   */
   PROCEDURE CreateRole
   (
       -- Role Name
       piosRoleName IN  tosObjectName

   )
   IS
      losSQLText tosSQLText;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: create role
      losSQLText := 'CREATE ROLE ' || piosRoleName;

      -----------------------------------------------------------
      -- Execute SQL command: create role
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoRoleConflict
         THEN
            CallInfo( 'Role conflicts with another user or role name.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CreateRole' );
   END CreateRole;

   /*
   **=====================================================
   **
   ** Procedure: GrantPriv
   **
   ** Purpose:  Grant privilegs to an object for a role or user
   **
   **=====================================================
   */
   PROCEDURE GrantPriv
   (
       -- Object Name
       piosObjectName IN  tosObjectName

       -- Role Name
      ,piosRoleName IN  tosObjectName

       -- Privilegs
      ,piosPrivileg_1 IN tosPrivileg
      ,piosPrivileg_2 IN tosPrivileg DEFAULT NULL
      ,piosPrivileg_3 IN tosPrivileg DEFAULT NULL
      ,piosPrivileg_4 IN tosPrivileg DEFAULT NULL
   )
   IS
      losSQLText tosSQLText;
   BEGIN

      -----------------------------------------------------------
      -- Build SQL command: create role
      losSQLText := 'GRANT ' || piosPrivileg_1 ;

      IF piosPrivileg_2 IS NOT NULL
      THEN
         losSQLText := losSQLText || ', ' || piosPrivileg_2;
      END IF;

      IF piosPrivileg_3 IS NOT NULL
      THEN
         losSQLText := losSQLText || ', ' || piosPrivileg_3;
      END IF;

      IF piosPrivileg_4 IS NOT NULL
      THEN
         losSQLText := losSQLText || ', ' || piosPrivileg_4;
      END IF;

      losSQLText := losSQLText || ' ON ' || piosObjectName || ' TO ' || piosRoleName;

      -----------------------------------------------------------
      -- Execute SQL command: create role
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'GrantPriv' );
   END GrantPriv;

   /*
   **=====================================================
   **
   ** Procedure: DropTable
   **
   ** Purpose:  Drop a table.
   **
   **=====================================================
   */
   PROCEDURE DropTable
   (
       -- Table Name
       piosTableName IN  tosObjectName
   )
   IS
      losSQLText tosSQLText;
      lcrGetSynonym socGetSynonym%ROWTYPE;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop table
      losSQLText := 'DROP TABLE ' || piosTableName;

      -----------------------------------------------------------
      -- Execute SQL command: drop table
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoTable
         THEN
            CallInfo( 'Table to be dropped does not exists.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      -- ----------------------------------------------------
      -- Open cursor on DBA_SYNONYMS
      BEGIN
         OPEN socGetSynonym
         (
             piosTableName
            ,USER
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_SYNONYMS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socGetSynonym
            INTO  lcrGetSynonym;
         EXCEPTION
            WHEN OTHERS
            THEN
               RaiseError( 'Fetch into cursor on DBA_SYNONYMS failed.' );
         END;

         EXIT WHEN socGetSynonym%NOTFOUND;

         -----------------------------------------------------------
         -- Drop synonyms
         DropSynonym
         (
             lcrGetSynonym.OWNER
            ,lcrGetSynonym.SYNONYM_NAME
         );
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socGetSynonym;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_SYNONYMS failed.') ;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropTable' );
   END DropTable;



   /*
   **=====================================================
   **
   ** Procedure: DropAllConstraints
   **
   ** Purpose:  Drop a constraint of a column
   **           or all constraints of the table.
   **
   **=====================================================
*/
PROCEDURE DropAllConstraints
(

	-- Table Name
   	piosTableName IN   tosObjectName

	-- Column Name
   	,piosColumnName IN  tosObjectName DEFAULT NULL
)
IS
	losTableName  tosObjectName;
   	losColumnName tosObjectName;

	CURSOR socColumnConstraints
	(
		pcosTableName		tosObjectName
		,pcosColumnName	tosObjectName
	)
	IS
		SELECT	UC.CONSTRAINT_NAME AS CONSTRAINT_NAME ,UC.CONSTRAINT_TYPE AS CONSTRAINT_TYPE
		FROM	USER_CONSTRAINTS UC,USER_CONS_COLUMNS UCC
		WHERE	UC.TABLE_NAME = pcosTableName
			AND UC.CONSTRAINT_NAME=UCC.CONSTRAINT_NAME
			AND UCC.COLUMN_NAME=pcosColumnName;
	CURSOR socTableConstraints
	(
		pcosTableName tosObjectName
	)
	IS
		SELECT	CONSTRAINT_NAME ,CONSTRAINT_TYPE
		FROM	USER_CONSTRAINTS
		WHERE	TABLE_NAME = pcosTableName;

	lcrColumnConstraints socColumnConstraints%ROWTYPE;
	lcrTableConstraints socTableConstraints%ROWTYPE;



BEGIN
   	losTableName := UPPER(LTRIM(RTRIM(piosTableName)));
   	losColumnName := UPPER(LTRIM(RTRIM(piosColumnName)));

	-- Check if all constraints have to be deleted or only
	-- constraints of one column piosColumnName
	IF losColumnName IS NOT NULL
	THEN

		OPEN socColumnConstraints
		(
			pcosTableName	=> losTableName
			,pcosColumnName	=> losColumnName
		);

		LOOP
		BEGIN
			FETCH socColumnConstraints
			INTO  lcrColumnConstraints;
		EXCEPTION
			WHEN OTHERS
			THEN
				CallError( 'Fetch into cursor on USER_CONSTRAINTS failed.' );
			END;

			EXIT WHEN socColumnConstraints%NOTFOUND;


			DropConstraint
			(
				piosTableName		=> losTableName
				,piosConstraintName	=> lcrColumnConstraints.CONSTRAINT_NAME
				,piosConstraintType	=> lcrColumnConstraints.CONSTRAINT_TYPE
			);

		END LOOP;

		CLOSE socColumnConstraints;

	ELSE
		OPEN socTableConstraints
		(
			pcosTableName	=> losTableName
		);

		LOOP
		BEGIN
			FETCH socTableConstraints
			INTO  lcrTableConstraints;
		EXCEPTION
			WHEN OTHERS
			THEN
				CallError( 'Fetch into cursor on USER_CONSTRAINTS failed.' );
		END;

			EXIT WHEN socTableConstraints%NOTFOUND;



			DropConstraint
			(
				piosTableName		=> losTableName
				,piosConstraintName	=> lcrTableConstraints.CONSTRAINT_NAME
				,piosConstraintType	=> lcrTableConstraints.CONSTRAINT_TYPE
			);

		END LOOP;

		CLOSE socTableConstraints;
	END IF;
EXCEPTION
		WHEN OTHERS
		THEN
			CallError( 'Open/Close cursor for USER_CONSTRAINTS failed.') ;


END DropAllConstraints;

   /*
   **=====================================================
   **
   ** Procedure: DropConstraint
   **
   ** Purpose:  Drop a constraint of a table.
   **
   **=====================================================
   */
   PROCEDURE DropConstraint
   (
       -- Table Name
       piosTableName IN  tosObjectName

       -- Constraint Name
      ,piosConstraintName IN  tosObjectName
      ,piosConstraintType IN  tosObjectType DEFAULT(cosFkey)
   )
   IS
      losSQLText tosSQLText;
      losTextCascade tosTextCascade :='';
   BEGIN
      -----------------------------------------------------------
      -- Check, if the constraint is primary key
      -- If yes, add cascade to losSQLText
      IF piosConstraintType = cosPkey OR piosConstraintType =  cosUkey
      THEN
          losTextCascade:=cosTextCascade;
      END IF;

      -----------------------------------------------------------
      -- Build SQL command: drop table
      losSQLText := 'ALTER TABLE ' || piosTableName
                     || ' DROP CONSTRAINT ' ||  piosConstraintName || losTextCascade;

      -----------------------------------------------------------
      -- Execute SQL command: drop table
      BEGIN

         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoConstraint
         THEN
            CallInfo( 'Constraint does not exist.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropConstraint' );
   END DropConstraint;

   /*
   **=====================================================
   **
   ** Procedure: DropView
   **
   ** Purpose:  Drop a view.
   **
   **=====================================================
   */
   PROCEDURE DropView
   (
       -- View Name
       piosViewName IN  tosObjectName

   )
   IS
      losSQLText tosSQLText;
      lcrGetSynonym socGetSynonym%ROWTYPE;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop table
      losSQLText := 'DROP VIEW ' || piosViewName;

      -----------------------------------------------------------
      -- Execute SQL command: drop view
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoTable
         THEN
            CallInfo( 'View to be dropped does not exists.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      -- ----------------------------------------------------
      -- Open cursor on DBA_SYNONYMS
      BEGIN
         OPEN socGetSynonym
         (
             piosViewname
            ,USER
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_SYNONYMS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socGetSynonym
            INTO  lcrGetSynonym;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor on DBA_SYNONYMS failed.' );
         END;

         EXIT WHEN socGetSynonym%NOTFOUND;

         -----------------------------------------------------------
         -- Drop synonyms
         DropSynonym
         (
             lcrGetSynonym.OWNER
            ,lcrGetSynonym.SYNONYM_NAME
         );
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socGetSynonym;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_SYNONYMS failed.') ;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropView' );
   END DropView;
   /*
   **=====================================================
   **
   ** Procedure: DropAllIndexes
   **
   ** Purpose:  Drop a index of a column
   **           or all indexes of the table.
   **
   **=====================================================
   */
   PROCEDURE DropAllIndexes
   (
   	-- Table Name
   	piosTableName IN   tosObjectName

	-- Column Name
   	,piosColumnName IN  tosObjectName DEFAULT NULL
   )
   IS
	losTableName  tosObjectName;
   	losColumnName tosObjectName;

   	CURSOR socColumnIndexes
	(
		pcosTableName		tosObjectName
		,pcosColumnName	tosObjectName
	)
	IS
		SELECT	UI.INDEX_NAME AS INDEX_NAME
		FROM	USER_INDEXES UI,USER_IND_COLUMNS UIC
		WHERE	UI.TABLE_NAME = pcosTableName
			AND UI.INDEX_NAME = UIC.INDEX_NAME
			AND UIC.COLUMN_NAME = pcosColumnName;

	CURSOR socTableIndexes
	(
		pcosTableName tosObjectName
	)
	IS
		SELECT	INDEX_NAME
		FROM	USER_INDEXES
		WHERE	TABLE_NAME = UPPER(pcosTableName);

	lcrColumnIndexes socColumnIndexes%ROWTYPE;
	lcrTableIndexes socTableIndexes%ROWTYPE;


   BEGIN
   	losTableName := UPPER(LTRIM(RTRIM(piosTableName)));
   	losColumnName := UPPER(LTRIM(RTRIM(piosColumnName)));

	-- Check if all Indexes have to be deleted or only
	-- Indexes of one column piosColumnName

	IF piosColumnName IS NOT NULL
	THEN

		OPEN socColumnIndexes
		(
			pcosTableName	=> losTableName
			,pcosColumnName	=> losColumnName
		);

		LOOP
		BEGIN
			FETCH socColumnIndexes
			INTO  lcrColumnIndexes;
		EXCEPTION
			WHEN OTHERS
			THEN
				CallError( 'Fetch into cursor on USER_INDEXES failed.' );
			END;

			EXIT WHEN socColumnIndexes%NOTFOUND;


			DropIndex
			(
				piosIndexName	=> lcrColumnIndexes.INDEX_NAME
			);

		END LOOP;

		CLOSE socColumnIndexes;

	ELSE
		OPEN socTableIndexes
		(
			pcosTableName	=> losTableName
		);

		LOOP
		BEGIN
			FETCH socTableIndexes
			INTO  lcrTableIndexes;
		EXCEPTION
			WHEN OTHERS
			THEN
				CallError( 'Fetch into cursor on USER_INDEXES failed.' );
		END;

			EXIT WHEN socTableIndexes%NOTFOUND;



			DropIndex
			(
				piosIndexName	=> lcrTableIndexes.INDEX_NAME
			);

		END LOOP;

		CLOSE socTableIndexes;
	END IF;
   EXCEPTION
		WHEN OTHERS
		THEN
			CallError( 'Open/Close cursor for USER_INDEXES failed.') ;


   END DropAllIndexes;
   /*
   **=====================================================
   **
   ** Procedure: DropIndex
   **
   ** Purpose:  Drop a Index.
   **
   **=====================================================
   */
   PROCEDURE DropIndex
   (
       -- Index Name
       piosIndexName IN  tosObjectName
   )
   IS
      losSQLText tosSQLText;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop table
      losSQLText := 'DROP INDEX ' || piosIndexName;

      -----------------------------------------------------------
      -- Execute SQL command: drop index
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoIndex
         THEN
            CallWarning( 'Index to be dropped does not exists.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropIndex' );
   END DropIndex;

   /*
   **=====================================================
   **
   ** Procedure: DropSequence
   **
   ** Purpose:   Drop a sequence.
   **
   **=====================================================
   */
   PROCEDURE DropSequence
   (
       -- Sequence Name
       piosSequenceName IN  tosObjectName

   )
   IS
      losSQLText tosSQLText;
      lcrGetSynonym socGetSynonym%ROWTYPE;
   BEGIN
      -----------------------------------------------------------
      -- Build SQL command: drop sequence
      losSQLText := 'DROP SEQUENCE ' || piosSequenceName;

      -----------------------------------------------------------
      -- Execute SQL command: drop sequence
      BEGIN
         EXECUTE IMMEDIATE losSQLText;
         CallInfo( losSQLText );
      EXCEPTION
         WHEN seoNoSequence
         THEN
            CallInfo( 'Sequence ' || piosSequenceName || ' does not exists.' );
         WHEN OTHERS
         THEN
            CallError( 'Can not execute: "' || losSQLText || '".' );
      END;

      -- ----------------------------------------------------
      -- Open cursor on DBA_SYNONYMS
      BEGIN
         OPEN socGetSynonym
         (
             piosSequenceName
            ,USER
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_SYNONYMS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socGetSynonym
            INTO  lcrGetSynonym;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor on DBA_SYNONYMS failed.' );
         END;

         EXIT WHEN socGetSynonym%NOTFOUND;

         -----------------------------------------------------------
         -- Drop synonyms
         DropSynonym
         (
             lcrGetSynonym.OWNER
            ,lcrGetSynonym.SYNONYM_NAME
         );
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socGetSynonym;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_SYNONYMS failed.') ;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DropSequence' );
 END DropSequence;

   /*
   **=====================================================
   **
   ** Procedure: CompileAll
   **
   ** Purpose:   Recompile all invalid views, packages and
   **            triggers in one step.
   **
   **=====================================================
   */
   PROCEDURE CompileAll
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   )
   IS
   BEGIN
      -- ----------------------------------------------------
     DabUtil.gobSecureInd := FALSE;

     CompileViews ( piosSchema );
     CompilePackages ( piosSchema );
     CompilePackageBodies ( piosSchema );
     CompileTriggers ( piosSchema );

   EXCEPTION
      WHEN OTHERS
      THEN
          null;
    --    RaiseError( 'CompileAll' );
   END CompileAll;

   /*
   **=====================================================
   **
   ** Procedure: CompileViews
   **
   ** Purpose:   Recompile all invalid views.
   **
   **=====================================================
   */
   PROCEDURE CompileViews
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   )
   IS
      losSQLText tosSQLText;
      lcrInvalidView socInvalidView%ROWTYPE;
      lobInvalidInd BOOLEAN := FALSE;
      counter INTEGER := 0;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socInvalidView
         (
             piosSchema
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor of invalid views for DBA_OBJECTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socInvalidView
            INTO  lcrInvalidView;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor of invalid views on DBA_OBJECTS failed.' );
         END;

         EXIT WHEN socInvalidView%NOTFOUND;

         counter := counter + 1;
         -----------------------------------------------------------
         -- Build SQL statement
         losSQLText := 'ALTER VIEW ' || piosSchema
                       || '.' || lcrInvalidView.OBJECT_NAME || ' COMPILE';

         -----------------------------------------------------------
         -- Execute SQL command: alter view compile
         BEGIN
            EXECUTE IMMEDIATE losSQLText;
            CallInfo( losSQLText );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallWarning( 'Can not execute: "' || losSQLText || '".' );
               lobInvalidInd := TRUE;
         END;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socInvalidView;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor of invalid views for DBA_OBJECTS failed.') ;
      END;

      -- ----------------------------------------------------
      -- Raise an Oracle error if one of the views can not be recompiled.
      IF lobInvalidInd
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Some views in schema ' || piosSchema || ' are invalid.' );
         ELSE
            CallWarning( 'Some views in schema ' || piosSchema || ' are invalid.' );
         END IF;
      ELSE
         IF counter > 0 THEN
            CallInfo('Invalid views (' || TO_CHAR(counter) || ') in schema '
                      || piosSchema || ' compiled with success.' );
         ELSE
            CallInfo( 'No invalid views in schema ' || piosSchema
                      || ' found.' );
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CompileViews' );
   END CompileViews;

   /*
   **=====================================================
   **
   ** Procedure: CompilePackages
   **
   ** Purpose:   Recompile all invalid packages.
   **
   **=====================================================
   */
   PROCEDURE CompilePackages
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   )
   IS
      losSQLText tosSQLText;
      lcrInvalidPackage socInvalidPackage%ROWTYPE;
      lobInvalidInd BOOLEAN := FALSE;
      counter INTEGER := 0;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socInvalidPackage
         (
             piosSchema
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor of invalid packages for DBA_OBJECTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socInvalidPackage
            INTO  lcrInvalidPackage;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor of invalid packages on DBA_OBJECTS failed.' );
         END;

         EXIT WHEN socInvalidPackage%NOTFOUND;

         counter := counter + 1;
         -----------------------------------------------------------
         -- Build SQL statement
         losSQLText := 'ALTER PACKAGE ' || piosSchema
                       || '.' || lcrInvalidPackage.OBJECT_NAME || ' COMPILE';

         -----------------------------------------------------------
         -- Execute SQL command: alter package compile
         BEGIN
            EXECUTE IMMEDIATE losSQLText;
            CallInfo( losSQLText );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallWarning( 'Can not execute: "' || losSQLText || '".' );
               lobInvalidInd := TRUE;
         END;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socInvalidPackage;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor of invalid packages for DBA_OBJECTS failed.') ;
      END;

      -- ----------------------------------------------------
      -- Raise an Oracle error if one of the packages can not be recompiled.
      IF lobInvalidInd
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Some packages in schema ' || piosSchema || ' are invalid.' );
         ELSE
            CallWarning( 'Some packages in schema ' || piosSchema || ' are invalid.' );
         END IF;
      ELSE
         IF counter > 0 THEN
            CallInfo('Invalid packages (' || TO_CHAR(counter) || ') in schema '
                      || piosSchema || ' compiled with success.' );
         ELSE
            CallInfo( 'No invalid packages in schema ' || piosSchema
                      || ' found.' );
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CompilePackages' );
   END CompilePackages;

   /*
   **=====================================================
   **
   ** Procedure: CompilePackageBodies
   **
   ** Purpose:   Recompile all invalid package bodies.
   **
   **=====================================================
   */
   PROCEDURE CompilePackageBodies
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   )
   IS
      losSQLText tosSQLText;
      lcrInvalidPackageBody socInvalidPackageBody%ROWTYPE;
      lobInvalidInd BOOLEAN := FALSE;
      counter INTEGER := 0;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socInvalidPackageBody
         (
             piosSchema
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor of invalid package bodies for DBA_OBJECTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socInvalidPackageBody
            INTO  lcrInvalidPackageBody;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor of invalid package bodies on DBA_OBJECTS failed.' );
         END;

         EXIT WHEN socInvalidPackageBody%NOTFOUND;

         counter := counter + 1;
         -----------------------------------------------------------
         -- Build SQL statement
         losSQLText := 'ALTER PACKAGE ' || piosSchema
                       || '.' || lcrInvalidPackageBody.OBJECT_NAME || ' COMPILE BODY';

         -----------------------------------------------------------
         -- Execute SQL command: alter package body compile
         BEGIN
            EXECUTE IMMEDIATE losSQLText;
            CallInfo( losSQLText );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallWarning( 'Can not execute: "' || losSQLText || '".' );
               lobInvalidInd := TRUE;
         END;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socInvalidPackageBody;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor of invalid package bodies for DBA_OBJECTS failed.') ;
      END;

      -- ----------------------------------------------------
      -- Raise an Oracle error if one of the package bodies can not be recompiled.
      IF lobInvalidInd
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Some package bodies in schema ' || piosSchema || ' are invalid.' );
         ELSE
            CallWarning( 'Some package bodies in schema ' || piosSchema || ' are invalid.' );
         END IF;
      ELSE
         IF counter > 0 THEN
            CallInfo('Invalid package bodies (' || TO_CHAR(counter) || ') in schema '
                      || piosSchema || ' compiled with success.' );
         ELSE
            CallInfo( 'No invalid package bodies in schema ' || piosSchema
                      || ' found.' );
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CompilePackageBodies' );
   END CompilePackageBodies;

   /*
   **=====================================================
   **
   ** Procedure: CompileTriggers
   **
   ** Purpose:   Recompile all invalid triggers.
   **
   **=====================================================
   */
   PROCEDURE CompileTriggers
   (
       -- Database Schema
       piosSchema IN tosSchema DEFAULT (user)

   )
   IS
      losSQLText tosSQLText;
      lcrInvalidTrigger socInvalidTrigger%ROWTYPE;
      lobInvalidInd BOOLEAN := FALSE;
      counter INTEGER := 0;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socInvalidTrigger
         (
             piosSchema
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor of invalid triggers for DBA_OBJECTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socInvalidTrigger
            INTO  lcrInvalidTrigger;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor of invalid triggers on DBA_OBJECTS failed.' );
         END;

         EXIT WHEN socInvalidTrigger%NOTFOUND;

         counter := counter + 1;
         -----------------------------------------------------------
         -- Build SQL statement
         losSQLText := 'ALTER TRIGGER ' || piosSchema
                       || '.' || lcrInvalidTrigger.OBJECT_NAME || ' COMPILE';

         -----------------------------------------------------------
         -- Execute SQL command: alter trigger compile
         BEGIN
            EXECUTE IMMEDIATE losSQLText;
            CallInfo( losSQLText );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallWarning( 'Can not execute: "' || losSQLText || '".' );
               lobInvalidInd := TRUE;
         END;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socInvalidTrigger;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor of invalid triggers for DBA_OBJECTS failed.') ;
      END;

      -- ----------------------------------------------------
      -- Raise an Oracle error if one of the triggers can not be recompiled.
      IF lobInvalidInd
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Some triggers in schema ' || piosSchema || ' are invalid.' );
         ELSE
            CallWarning( 'Some triggers in schema ' || piosSchema || ' are invalid.' );
         END IF;
      ELSE
         IF counter > 0 THEN
            CallInfo('Invalid triggers (' || TO_CHAR(counter) || ') in schema '
                      || piosSchema || ' compiled with success.' );
         ELSE
            CallInfo( 'No invalid triggers in schema ' || piosSchema
                      || ' found.' );
         END IF;
      END IF;

EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'CompileTriggers' );
   END CompileTriggers;

   /*
   **=====================================================
   **
   ** Procedure: ShowError
   **
   ** Purpose:  Check whether an Oracle object is valid
   **
   **=====================================================
   */
   PROCEDURE ShowError
   (
       -- Object Name
       piosObjectName IN  tosObjectName

       -- Object Type
      ,piosObjectType IN  tosObjectType
   )
   IS
      losSQLText tosSQLText;
      lcrInvalidObject socInvalidObject%ROWTYPE;
      lobInvalidInd BOOLEAN := FALSE;
      lobObjectFound BOOLEAN := FALSE;
      losObjectLongType VARCHAR2(30);
   BEGIN

      IF piosObjectType = cosPackBody
      THEN
         losObjectLongType := 'PACKAGE BODY';
      ELSIF piosObjectType = cosPackHeader
      THEN
         losObjectLongType := 'PACKAGE';
      ELSE
         CallError( 'Object type "' || piosObjectType || '" not supported.');
      END IF;

      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socInvalidObject
         (
             USER
            ,UPPER( piosObjectName )
            ,losObjectLongType
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_OBJECTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socInvalidObject
            INTO  lcrInvalidObject;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor on DBA_OBJECTS failed.' );
         END;

         EXIT WHEN socInvalidObject%NOTFOUND;

         lobObjectFound := TRUE;
         -- ----------------------------------------------------
         -- An invalid object has been found.
         IF lcrInvalidObject.STATUS = 'INVALID'
         THEN
            lobInvalidInd := TRUE;
            CallWarning( losObjectLongType || ' ' || lcrInvalidObject.OBJECT_NAME || ' IS INVALID' );
         ELSE
            CallInfo( losObjectLongType || ' ' || lcrInvalidObject.OBJECT_NAME || ' IS ' || lcrInvalidObject.STATUS );
         END IF;
      END LOOP;

      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socInvalidObject;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_OBJECTS failed.') ;
      END;

      -- ----------------------------------------------------
      -- Raise an Oracle error if no object is found.
      IF NOT lobObjectFound
      THEN
         CallError( 'Object ' || piosObjectName || ' not found.' );
      END IF;

      -- ----------------------------------------------------
      -- Raise an Oracle error if one of the objects is invalid.
      IF lobInvalidInd
      THEN
         IF gobSecureInd
         THEN
            CallError( 'Package(s) in schema ' || USER || ' is/are invalid.' );
         ELSE
            CallWarning( 'Package(s) in schema ' || USER || ' is/are invalid.' );
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'ShowError' );
   END ShowError;

   /*
   **=====================================================
   **
   ** Procedure: DisableForeignKeys
   **
   ** Purpose:  Disable all foreign keys of table.
   **
   **=====================================================
   */
   PROCEDURE DisableForeignKeys
   (
       -- Database Schema
       piosSchema IN tosSchema

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

   )
   IS
      losSQLText tosSQLText;
      lcrForeignKey socForeignKey%ROWTYPE;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socForeignKey
         (
             piosSchema
            ,piosObjectName
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_CONSTRAINTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socForeignKey
            INTO  lcrForeignKey;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor on DBA_CONSTRAINTS failed.' );
         END;

         EXIT WHEN socForeignKey%NOTFOUND;

         -----------------------------------------------------------
         -- Build SQL statement
         losSQLText := 'ALTER TABLE ' || piosSchema || '.' || lcrForeignKey.TABLE_NAME
                       || ' MODIFY CONSTRAINT ' || lcrForeignKey.CONSTRAINT_NAME || ' DISABLE';
         -----------------------------------------------------------
         -- Execute SQL command: alter view compile
         BEGIN
            EXECUTE IMMEDIATE losSQLText;
            CallInfo( losSQLText );
         EXCEPTION
            WHEN OTHERS
            THEN
               CallWarning( 'Can not execute: "' || losSQLText || '".' );
         END;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socForeignKey;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_CONSTRAINTS failed.') ;
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'DisableForeignKeys' );
   END DisableForeignKeys;

   /*
   **=====================================================
   **
   ** Procedure: EnableForeignKeys
   **
   ** Purpose:  Enable all foreign of a table.
   **
   **=====================================================
   */
   PROCEDURE EnableForeignKeys
   (
       -- Database Schema
       piosSchema IN tosSchema

       -- Object Name
      ,piosObjectName IN  tosObjectName DEFAULT( NULL )

   )
   IS
      losSQLText tosSQLText;
      lcrForeignKey socForeignKey%ROWTYPE;
   BEGIN
      -- ----------------------------------------------------
      -- Open cursor on DBA_OBJECTS
      BEGIN
         OPEN socForeignKey
         (
             piosSchema
            ,piosObjectName
         );
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Open cursor for DBA_CONSTRAINTS failed.') ;
      END;
      -- ----------------------------------------------------
      -- Fetch row(s)
      LOOP
         BEGIN
            FETCH socForeignKey
            INTO  lcrForeignKey;
         EXCEPTION
            WHEN OTHERS
            THEN
               CallError( 'Fetch into cursor on DBA_CONSTRAINTS failed.' );
         END;

         EXIT WHEN socForeignKey%NOTFOUND;

         IF lcrForeignKey.STATUS != 'ENABLE'
         THEN
            -----------------------------------------------------------
            -- Build SQL statement
            losSQLText := 'ALTER TABLE ' || piosSchema || '.' || lcrForeignKey.TABLE_NAME
                          || ' MODIFY CONSTRAINT ' || lcrForeignKey.CONSTRAINT_NAME || ' ENABLE';
            -----------------------------------------------------------
            -- Execute SQL command: alter view compile
            BEGIN
               EXECUTE IMMEDIATE losSQLText;
               CallInfo( losSQLText );
            EXCEPTION
               WHEN OTHERS
               THEN
                  CallWarning( 'Can not execute: "' || losSQLText || '".' );
            END;
         ELSE
            CallInfo( 'Constraint ' || lcrForeignKey.CONSTRAINT_NAME  || ' is enabled.' );
         END IF;
      END LOOP;
      -- ----------------------------------------------------
      -- Close cursor
      BEGIN
         CLOSE socForeignKey;
      EXCEPTION
         WHEN OTHERS
         THEN
            CallError( 'Close cursor for DBA_CONSTRAINTS failed.') ;
      END;

   EXCEPTION
      WHEN OTHERS
      THEN
          RaiseError( 'EnableForeignKeys' );
   END EnableForeignKeys;

   /*
   **=====================================================
   **
   ** Procedure: AddText
   **
   ** Purpose:  Add a line to the text.
   **           Is possibly overloaded.
   **           The case TextLine is NULL corresponds to an empty line.
   **=====================================================
   */
   PROCEDURE AddText
   (
       -- Text line
       piosTextLine IN tosTextLine

       -- Text (array)
      ,pmasText IN OUT  tasText
   )
   IS
      lonTextIndex PLS_INTEGER;
   BEGIN
      IF pmasText.COUNT > 0
      THEN
         lonTextIndex := pmasText.LAST;
      ELSE
         lonTextIndex := 0;
      END IF;

      pmasText.EXTEND;
      lonTextIndex := lonTextIndex + 1;
      pmasText( lonTextIndex ) := piosTextLine;

   END AddText;

   /*
   **=====================================================
   **
   ** Procedure: AddText
   **
   ** Purpose:  Append text to a text.
   **           Is possibly overloaded.
   **
   **=====================================================
   */
   PROCEDURE AddText
   (
       -- Text (array) to append
       piasSection IN tasText

       -- Text (array)
      ,pmasText IN OUT  tasText
   )
   IS
      lonSectionIndex PLS_INTEGER;
      lonTextIndex    PLS_INTEGER;
   BEGIN
      IF pmasText.COUNT > 0
      THEN
         lonTextIndex := pmasText.LAST;
      ELSE
         lonTextIndex := 0;
      END IF;

      IF piasSection.COUNT > 0
      THEN
         lonSectionIndex := piasSection.FIRST;
         pmasText.EXTEND( piasSection.COUNT);
         WHILE lonSectionIndex IS NOT NULL
         LOOP
            lonTextIndex := lonTextIndex + 1;
            pmasText( lonTextIndex ) := piasSection( lonSectionIndex );
            lonSectionIndex := piasSection.NEXT( lonSectionIndex );
         END LOOP;
      END IF;

   END AddText;

   /*
   **=====================================================
   **
   ** Procedure: CopySql
   **
   ** Purpose:  Copy a text array into an SQL array for DBMS_SQL
   **
   **=====================================================
   */
   PROCEDURE CopySql
   (
       -- Text (array)
       piasText IN tasText

       -- Sql array
      ,poasSqlText OUT  DBMS_SQL.VARCHAR2S
   )
   IS
      lonTextIndex    PLS_INTEGER;
      lonSqlTextIndex PLS_INTEGER;
   BEGIN
      lonSqlTextIndex := NVL( poasSqlText.LAST , 0 ) + 1;
      lonTextIndex := piasText.FIRST;
      WHILE lonTextIndex <= piasText.LAST
      LOOP
         poasSqlText( lonSqlTextIndex ) := piasText( lonTextIndex );
         lonSqlTextIndex := lonSqlTextIndex + 1;
         lonTextIndex := piasText.NEXT( lonTextIndex );
      END LOOP;

   END CopySql;

   /*
   **=====================================================
   **
   ** Procedure: PrintText
   **
   ** Purpose:  Print a text using dbms_output.
   **
   **=====================================================
   */
   PROCEDURE PrintText
   (
       -- Text (array)
       pmasText IN tasText
   )
   IS
      lonTextIndex PLS_INTEGER;
   BEGIN
      lonTextIndex := pmasText.FIRST;
      WHILE lonTextIndex <= pmasText.LAST
      LOOP
         DBMS_OUTPUT.PUT_LINE( pmasText( lonTextIndex ) );
         lonTextIndex := pmasText.NEXT( lonTextIndex );
      END LOOP;
   END PrintText;

   /*
   **=====================================================
   **
   ** Procedure: DropConstraintsAndIndexes
   **
   ** Purpose:  Drops all  constraints and Indexes of table
   **           piosTableName and column piosColumnName .
   **           If piosColumnName is omitted drops all
   **	        constraints and indexes of the table piosTableName
   **
   **=====================================================
   */
   PROCEDURE DropConstraintsAndIndexes
   (
      	piosTableName IN   tosObjectName
      	,piosColumnName IN  tosObjectName DEFAULT NULL
   )
   IS

   BEGIN

   	DabUtil.DropAllConstraints(piosTableName, piosColumnName);
	DabUtil.DropAllIndexes(piosTableName, piosColumnName);
   EXCEPTION
   	WHEN OTHERS
   	THEN
   		RaiseError( 'DropConstraintsAndIndexes' );

   END DropConstraintsAndIndexes;


END DabUtil;
/

