CREATE OR REPLACE PACKAGE GSI_PREPARA_AMBIENTE IS

  -- Author  : SRAMIRED
  -- Created : 31/05/2007 10:09:19
  -- Purpose : Generar el Ambiente de Facturacion de forma Automatica

  -- Public type declarations
  --type <TypeName> is <Datatype>;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --  <VariableName> <Datatype>;

  -- Public function and procedure declarations
  --  function <FunctionName>(<Parameter> <Datatype>) return <Datatype>;

  PROCEDURE MAN_FIX_FP(LWSCICLO INTEGER);
  PROCEDURE MAN_PRGCODE_COST;
  PROCEDURE MAN_PLANES;
  PROCEDURE CONF_TABLAS;
  PROCEDURE MAN_DOC1(LwsCiclo VARCHAR2, LwsTipoFact Varchar2);
  PROCEDURE MAN_PROMOCIONES(LwsCiclo varchar2, LwsPromo varchar2);
  PROCEDURE MAN_TABLAS_CTR;
  PROCEDURE MAN_CICLOS(LwsCiclo varchar2);
END GSI_PREPARA_AMBIENTE;
/
CREATE OR REPLACE PACKAGE BODY GSI_PREPARA_AMBIENTE IS

  -- Private type declarations
  --  type <TypeName> is <Datatype>;

  -- Private constant declarations
  --  <ConstantName> constant <Datatype> := <Value>;

  -- Private variable declarations
  --  <VariableName> <Datatype>;

  -- Function and procedure implementations
  --  function <FunctionName>(<Parameter> <Datatype>) return <Datatype> is
  --    <LocalVariable> <Datatype>;
  --  begin
  --    <Statement>;
  --    return(<Result>);
  --  end;

  --begin
  -- Initialization
  --  <Statement>;
  LwsFile          VARCHAR2(1);
  LWSTABLA_INI     VARCHAR2(50);
  LWSTABLA_FIN     VARCHAR2(50);
  LWINUMREG        INTEGER;
  LWICUTOMER_PADRE NUMBER;
  LWICUTOMER_HIJO  NUMBER;
  LWIPREG_PADRE    NUMBER;
  LWIPREG_HIJO     NUMBER;
  LWICOST_PADRE    NUMBER;
  LWICOST_HIJO     NUMBER;

  LWSCAMPO1        VARCHAR2(50);
  LWSCAMPO2        VARCHAR2(50);
  LWSCAMPO3        VARCHAR2(5000);

-----------------------------------------------------------------
-------------Se valida los nuevas formas de Pago-----------------
-----------------------------------------------------------------

PROCEDURE MAN_FIX_FP(LWSCICLO INTEGER) IS
  BEGIN
  SELECT COUNT(*)
    INTO LWINUMREG
    FROM (SELECT BANK_ID /*, BANKNAME */
            FROM BANK_ALL
           WHERE BANK_ID NOT IN
                 (77, 44, 70, 99, 100, 3, 7, 11, 14, 16, 17, 18, 19, 25, 27, 28, 29, 31, 73, 108, 109, 110, 117) --formas de pago no validas
          MINUS
          SELECT FP  FROM QC_FP WHERE ID_CICLO = LWSCICLO);

  IF LWINUMREG <> 0 THEN
  --   INSERT INTO GSI_AMBIENTE_LOG(1,'Configurar nuevos registros en QC_FP','ERR','');
  LWINUMREG :=0;
--  COMMIT;
  END IF;
END;
-----------------------------------------------------------------
----------Valida la diferencia de los COSTCENTER_ID--------------
-----------------------------------------------------------------
PROCEDURE MAN_PRGCODE_COST IS
CURSOR P_C IS
SELECT --a.custcode custcode_PADRE,
--B.custcode custcode_PADRE,
 A.CUSTOMER_ID   CUST_PADRE,
 B.CUSTOMER_ID   CUST_HIJO,
 A.PRGCODE       PRG_PADRE,
 B.PRGCODE       PRG_HIJO,
 A.COSTCENTER_ID COST_PADRE,
 B.COSTCENTER_ID COST_HIJO
  FROM CUSTOMER_ALL A, CUSTOMER_ALL B
 WHERE A.CUSTOMER_ID_HIGH IS NULL
   AND B.CUSTOMER_ID_HIGH = A.CUSTOMER_ID
   AND (A.PRGCODE <> B.PRGCODE OR A.COSTCENTER_ID <> B.COSTCENTER_ID);
BEGIN
--Se valida la diferencia de los prgcode entre padre e hijo

   FOR I in P_C loop 
      IF I.PRG_PADRE <> I.PRG_HIJO THEN 
      UPDATE CUSTOMER_ALL
         SET PRGCODE = (SELECT PRGCODE
                          FROM CUSTOMER_ALL A
                         WHERE A.CUSTOMER_ID_HIGH = I.CUST_PADRE)
       WHERE CUSTOMER_ID = I.CUST_HIJO;
       COMMIT;
      END IF;
      --Se valida la diferencia de los costcenter entre padre e hijo
      IF I.COST_PADRE <> I.COST_HIJO THEN 
      UPDATE CUSTOMER_ALL
         SET COSTCENTER_ID = (SELECT B.COSTCENTER_ID
                                FROM CUSTOMER_ALL B
                               WHERE B.CUSTOMER_ID_HIGH = I.CUST_PADRE)
       WHERE CUSTOMER_ID = I.CUST_HIJO;
       COMMIT;
      END IF; 
    END LOOP;
    COMMIT;
END;
-----------------------------------------------------------------
----------------Valida los Planes no existentes------------------
-----------------------------------------------------------------
PROCEDURE MAN_PLANES IS
BEGIN
  SELECT COUNT(*) INTO LWINUMREG FROM (select tmcode from rateplan 
  where  upper(des)  not like ('%MAESTRO%')
  and    tmcode not in (33,35,36,254,257,258,259,260,261,282,582,583,626)
  --aqu� se incluyen planes especiales de Roaming que no se faturan
  minus
  select distinct(tmcode) from doc1.bgh_tariff_plan_doc1);

  IF LWINUMREG <> 0 THEN
     LWINUMREG :=0;
  END IF;

END;

PROCEDURE CONF_TABLAS IS
BEGIN

--VALIDACION PARA BSCSPROD
  --� El sinonimo PUBLIC.BCH_LT debe apuntar a la vista BCH.UDR_LT_ST 
  select OWNER||'.'||SYNONYM_NAME CAMPO1,
         TABLE_OWNER||'.'||TABLE_NAME  CAMPO2
         INTO LWSCAMPO1, LWSCAMPO2
  from all_synonyms 
  WHERE  OWNER = 'PUBLIC'
  AND SYNONYM_NAME = 'BCH_LT';
  
  IF LWSCAMPO2 <> 'BCH.UDR_LT_ST' AND LWSCAMPO1 <> 'PUBLIC.BCH_LT' THEN
--     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo PUBLIC.BCH_LT no esta bien configurado en BSCSPROD','ERR','');
       LWSCAMPO2:='Error';
  END IF;
  --�	El sinonimo BCH.RTX debe apuntar a la vista BCH.UDR_LT_ST 
  select OWNER||'.'||SYNONYM_NAME CAMPO1,
         TABLE_OWNER||'.'||TABLE_NAME CAMPO2
         INTO LWSCAMPO1, LWSCAMPO2
  from all_synonyms 
  WHERE  OWNER = 'BCH'
  AND SYNONYM_NAME = 'RTX';
  
  IF LWSCAMPO2 <> 'BCH.UDR_LT_ST' AND LWSCAMPO1 <> 'BCH.RTX' THEN
--     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
       LWSCAMPO2:='Error';
  END IF;

 ---Correcto (solo para ver sobre que tabla el rating esta cargando llamadas)
/*
�	Cuando la tabla de free units se cree  (UDR_FU_YYYYMM)  --> no creada actualmente
se deben actualizar dos sinonimos estos son : 
PUBLIC.BCH_LT_APPEND
PUBLIC.RTX_LT_APPEND 
es decir que deben apuntar a la tabla UDR_FU_YYYYMM

�	La vista BCH.UDR_LT_ST debe crearse haciendo referencia al union de las 2 tablas involucradas en el cierre de facturaci�n 
*/
--VALIDACION DE RTXPROD
--�	El sinonimo PUBLIC.RTX_LT debe apuntar a la tabla de rateo del mes (UDR_LT_YYYYMM)    
SELECT OWNER || '.' || SYNONYM_NAME, TABLE_OWNER || '.' || TABLE_NAME
  INTO LWSCAMPO1, LWSCAMPO2
  FROM ALL_SYNONYMS@BSCS_TO_RTX_LINK
 WHERE OWNER = 'PUBLIC'
   AND SYNONYM_NAME = 'RTX_LT';

  IF LWSCAMPO2 <> 'SYSADM.UDR_LT_'||LWSTABLA_FIN OR LWSCAMPO1 <> 'PUBLIC.RTX_LT' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;

--�	El sinonimo PUBLIC.BCH_LT debe apuntar a la vista (UDR_LT_ST) con el union de las 2 tablas 
select OWNER||'.'||SYNONYM_NAME,
       TABLE_OWNER||'.'||TABLE_NAME
  INTO LWSCAMPO1, LWSCAMPO2
from all_synonyms@bscs_to_rtx_link
WHERE  OWNER = 'PUBLIC'
AND SYNONYM_NAME = 'BCH_LT';

  IF LWSCAMPO2 <> 'BCH.UDR_LT_ST' OR LWSCAMPO1 <> 'PUBLIC.BCH_LT' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;

--�	El sinonimo BCH.RTX debe apuntar a la vista BCH.UDR_LT_ST
select OWNER||'.'||SYNONYM_NAME,
       TABLE_OWNER||'.'||TABLE_NAME
  INTO LWSCAMPO1, LWSCAMPO2
from all_synonyms@bscs_to_rtx_link
WHERE  OWNER = 'BCH'
AND SYNONYM_NAME = 'RTX';

  IF LWSCAMPO2 <> 'BCH.UDR_LT_ST' OR LWSCAMPO1 <> 'BCH.RTX' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;

select OWNER||'.'||SYNONYM_NAME,
       TABLE_OWNER||'.'||TABLE_NAME
  INTO LWSCAMPO1, LWSCAMPO2
from all_synonyms@bscs_to_rtx_link
WHERE  OWNER = 'PUBLIC'
AND SYNONYM_NAME = 'BCH_LT_APPEND';

  IF LWSCAMPO2 <> 'SYSADM.UDR_FU_'||LWSTABLA_FIN OR LWSCAMPO1 <> 'PUBLIC.BCH_LT_APPEND' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;

select OWNER||'.'||SYNONYM_NAME,
       TABLE_OWNER||'.'||TABLE_NAME
  INTO LWSCAMPO1, LWSCAMPO2
from all_synonyms@bscs_to_rtx_link
WHERE  OWNER = 'PUBLIC'
AND SYNONYM_NAME = 'RTX_LT_APPEND';

  IF LWSCAMPO2 <> 'SYSADM.UDR_FU_'||LWSTABLA_FIN OR LWSCAMPO1 <> 'PUBLIC.RTX_LT_APPEND' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;

SELECT OWNER||'.'||VIEW_NAME,
       TEXT TEXTO
  INTO LWSCAMPO1, LWSCAMPO3
 FROM ALL_VIEWS@bscs_to_rtx_link
 WHERE OWNER = 'BCH'
 AND VIEW_NAME = 'UDR_LT_ST';

  IF LWSCAMPO2 <> '0' OR LWSCAMPO1 <> 'BCH.UDR_LT_ST' THEN
  --     INSERT INTO GSI_AMBIENTE_LOG(1,'Sinonymo BCH.RTX no esta bien configurado en BSCSPROD','ERR','');
         LWSCAMPO2:='Error';
  END IF;
END;

PROCEDURE MAN_DOC1(LwsCiclo VARCHAR2, LwsTipoFact Varchar2) IS
BEGIN

     IF LwsCiclo = 1 THEN
      SELECT COUNT(*)
        INTO LWINUMREG
        FROM DOC1.DOC1_PARAMETRO_INICIAL
       WHERE ESTADO = 'A'
         AND PERIODO_FINAL = TO_DATE('23/' || TO_CHAR(SYSDATE, 'mm') || '/' ||
                                     TO_CHAR(SYSDATE, 'YYYY'),
                                     'DD/MM/YYYY');
     
      IF LWINUMREG <> 0 THEN
        UPDATE DOC1.DOC1_PARAMETRO_INICIAL
           SET ESTADO = 'I'
         WHERE ESTADO = 'A';
      END IF;
     
        INSERT INTO DOC1.DOC1_PARAMETRO_INICIAL
          SELECT TRUNC(SYSDATE),
                 TO_DATE('24/' || TO_CHAR(TO_CHAR(SYSDATE, 'mm') - 1, '00') || '/' ||
                         TO_CHAR(SYSDATE, 'YYYY'),
                         'DD/MM/YYYY'),
                 TO_DATE('23/' || TO_CHAR(SYSDATE, 'mm') || '/' ||
                         TO_CHAR(SYSDATE, 'YYYY'),
                         'DD/MM/YYYY'),
                 'A',
                 LwsTipoFact
            FROM DUAL;
     END IF;

     IF LwsCiclo = 2 THEN
     
        SELECT COUNT(*)
          INTO LWINUMREG
          FROM DOC1.DOC1_PARAMETRO_INICIAL
         WHERE ESTADO = 'A'
           AND PERIODO_FINAL = TO_DATE('07/' || TO_CHAR(SYSDATE, 'mm') || '/' ||
                                       TO_CHAR(SYSDATE, 'YYYY'),
                                       'DD/MM/YYYY');
        
        IF LWINUMREG <> 0 THEN
          UPDATE DOC1.DOC1_PARAMETRO_INICIAL
             SET ESTADO = 'I'
           WHERE ESTADO = 'A';
        END IF;

        INSERT INTO DOC1.DOC1_PARAMETRO_INICIAL
        SELECT TRUNC(SYSDATE),
               TO_DATE('08/' || TO_CHAR(TO_CHAR(SYSDATE, 'mm') - 1, '00') || '/' ||
                       TO_CHAR(SYSDATE, 'YYYY'),
                       'DD/MM/YYYY'),
               TO_DATE('07/' || TO_CHAR(SYSDATE, 'mm') || '/' ||
                       TO_CHAR(SYSDATE, 'YYYY'),
                       'DD/MM/YYYY'),
               'A',
               LwsTipoFact
          FROM DUAL;
     END IF;
END;

PROCEDURE MAN_PROMOCIONES(LwsCiclo varchar2, LwsPromo varchar2) IS
BEGIN
    IF lwsCiclo = 1 THEN
      IF LwsPromo = 'A' OR LwsPromo = 'a' THEN
      -- CON PROMOCIONES ACTIVAS
      UPDATE MPSCFTAB
         SET CFVALUE = '-R -C -d -v -t ' || TO_CHAR(SYSDATE, 'YYYY') ||
                       TO_CHAR(SYSDATE, 'mm') || '24'
       WHERE CFCODE = 23;
      END IF;

      IF LwsPromo = 'I' OR LwsPromo = 'i' THEN
      -- SIN PROMOCIONES ACTIVAS
      UPDATE MPSCFTAB
         SET CFVALUE = '-R -C -d -p -v -t ' || TO_CHAR(SYSDATE, 'YYYY') ||
                       TO_CHAR(SYSDATE, 'mm') || '24'
       WHERE CFCODE = 23;
      END IF;
    END IF;
    
    IF lwsCiclo = 2 THEN
      IF LwsPromo = 'A' OR LwsPromo = 'a' THEN
      -- CON PROMOCIONES ACTIVAS
      UPDATE MPSCFTAB
         SET CFVALUE = '-R -C -d -v -t ' || TO_CHAR(SYSDATE, 'YYYY') ||
                       TO_CHAR(SYSDATE, 'mm') || '08'
       WHERE CFCODE = 23;
      END IF;
      
      IF LwsPromo = 'I' OR LwsPromo = 'i' THEN
      -- SIN PROMOCIONES ACTIVAS
      UPDATE MPSCFTAB
         SET CFVALUE = '-R -C -d -p -v -t ' || TO_CHAR(SYSDATE, 'YYYY') ||
                       TO_CHAR(SYSDATE, 'mm') || '08'
       WHERE CFCODE = 23;
      END IF;
    END IF;

END;

PROCEDURE MAN_TABLAS_CTR IS
BEGIN

	delete from bch_process_cust;
	delete from bch_process_contr;
	delete from bch_process_package;
	delete from bch_control;
	delete from bch_process;
	delete from bch_monitor_table;

END;

PROCEDURE MAN_CAMP_NULL IS
BEGIN
LwsFile:= NULL;
select bch_callrecord_to_file INTO LwsFile from bscsproject;
IF LwsFile IS NOT NULL THEN
   LwsFile:='E';
END IF;

END; 

PROCEDURE MAN_CICLOS(LwsCiclo varchar2) IS
CURSOR CYCLE IS
SELECT AA.billcycle,    AA.bch_run_date,   AA.last_run_date
  FROM BILLCYCLES AA, FA_CICLOS_AXIS_BSCS BB
 WHERE AA.BILLCYCLE = BB.ID_CICLO_ADMIN
   AND ID_CICLO = LwsCiclo;
   
CURSOR BCH_HISTORY IS
SELECT BILLCYCLE, MAX(LRSTART) LRSTART
  FROM BCH_HISTORY_TABLE AA, FA_CICLOS_AXIS_BSCS BB
 WHERE AA.BILLCYCLE = BB.ID_CICLO_ADMIN
   AND ID_CICLO = LwsCiclo
 GROUP BY BILLCYCLE;
 
 LwdFechaIni DATE;
 LwdFechaFin DATE;
BEGIN
IF LwsCiclo = 1 THEN
  LwdFechaIni:= to_date('25/'||to_char(to_char(SYSDATE,'mm')-1,'00')||'/'||to_char(SYSDATE,'yyyy'));
  LwdFechaFin:= to_date('25/'||to_char(SYSDATE,'mm')||'/'||to_char(SYSDATE,'yyyy'));
END IF;
IF LwsCiclo = 2 THEN
  LwdFechaIni:= to_date('09/'||to_char(to_char(SYSDATE,'mm')-1,'00')||'/'||to_char(SYSDATE,'yyyy'));
  LwdFechaFin:= to_date('09/'||to_char(SYSDATE,'mm')||'/'||to_char(SYSDATE,'yyyy'));
END IF;

   FOR I in CYCLE loop 
       IF I.bch_run_date <> LwdFechaFin THEN
          UPDATE BILLCYCLES
          SET bch_run_date = LwdFechaFin
          WHERE bch_run_date = I.bch_run_date
          AND billcycle = I.billcycle;
       END IF;

       IF I.last_run_date <> LwdFechaIni THEN
          UPDATE BILLCYCLES
          SET bch_run_date = LwdFechaIni
          WHERE bch_run_date = I.last_run_date
          AND billcycle = I.billcycle; 
       END IF;
   END LOOP;

   FOR J in BCH_HISTORY loop 
       IF J.LRSTART <> LwdFechaIni THEN
          UPDATE BCH_HISTORY_TABLE
          SET LRSTART = LwdFechaIni
          WHERE LRSTART = J.LRSTART
          AND BILLCYCLE = J.BILLCYCLE; 
       END IF;
   END LOOP;
END;

BEGIN
LWSTABLA_FIN:=TO_CHAR(SYSDATE,'yyyy')||TO_CHAR(SYSDATE,'mm');
LWSTABLA_INI:=TO_CHAR(SYSDATE,'yyyy')||TO_CHAR(TO_CHAR(SYSDATE,'mm')-1,'00');
END GSI_PREPARA_AMBIENTE;
/

