CREATE OR REPLACE PACKAGE PCK_VISTA_MATERIALIZADA IS

  /*--=========================================================================================================--
  PROYECTO               : [8718] vista materializada - validar ejecucion
  AUTOR                  : SUD Rosario Garcia
  CREADO                 : 23/02/2013
  CRM                    : ING. WILSON POZO
  PROPOSITO DEL PROYECTO : Mejoras y validar ejecución de la vista materializada.
  --===========================================================================================================--*/

  PROCEDURE PR_VISTA_MATIALIZADA (PV_MSG_ERROR IN OUT VARCHAR2 );

END;
/
CREATE OR REPLACE PACKAGE BODY PCK_VISTA_MATERIALIZADA IS

  /*--=========================================================================================================--
  PROYECTO               : [8718] vista materializada - validar ejecucion
  AUTOR                  : SUD Rosario Garcia
  CREADO                 : 23/01/2013
  CRM                    : ING. WILSON POZO
  PROPOSITO DEL PROYECTO :  Mejoras y validar ejecución de la vista materializada.
  --===========================================================================================================--*/
  
/*--=========================================================================================================--
PROYECTO               : [8718] vista materializada - validar ejecucion
MODIFICACION :  SUD Diana Murillo
CRM                    : ING. WILSON POZO
MOTIVO           :  SE COMENTA EL CALL DEL PAQUETE DE  SCP POR LO QUE NO SE ESTAN CREANDO LAS PARTICIONES EN LAS TABLA DE SCP.
--=========================================================================================================--*/
  
  
  PROCEDURE PR_VISTA_MATIALIZADA( PV_MSG_ERROR IN OUT VARCHAR2 )IS
    
   --- Obtiene el universo original 
   --- Se mantiene la Integridad de Datos
   CURSOR LC_TOTAL IS 
     SELECT COUNT(*)LINEAS 
     from fees 
     --where INSERTIONDATE >= SYSDATE-300  --- Varia la integridad de datos
     --where INSERTIONDATE >= TRUNC(SYSDATE)-300 ---Se mantiene la Integridad de datos
     where INSERTIONDATE >= TO_DATE(TO_CHAR(sysdate, 'DD/MM/YYYY') ||' 00:00:00', 'DD/MM/YYYY HH24:MI:SS')-300
     ;  
     
   --- Obtiene los datos de la Vista Materializada
   CURSOR LC_TOTAL_VM_OK IS 
     SELECT COUNT(*)LINEAS 
     FROM CO_FEES_MV_8718 ;  
     
   --- Enviar Alerta para los Usuarios.   
   CURSOR C_FROM_NAME IS
   SELECT T.VALOR
     FROM GV_PARAMETROS T
    WHERE T.ID_TIPO_PARAMETRO = 8718
      AND T.ID_PARAMETRO = 'FROM_MAIL';
      
   CURSOR C_TO_NAME IS 
    SELECT T.VALOR
     FROM GV_PARAMETROS T
    WHERE T.ID_TIPO_PARAMETRO = 8718
      AND T.ID_PARAMETRO LIKE 'RECEPTOR_%';
   
   CURSOR C_MAILSERVER(CV_EMPRESA VARCHAR2 ) IS
   SELECT G.SERVIDOR_CORREO DIRECCION_IP
     FROM GE_PARAMETROS_GENERALES@AXIS G
    WHERE G.EMPRESA = CV_EMPRESA;
   --- Enviar Alertas para los Usuarios.
     
     
    LV_MSG_ERR         VARCHAR2(2000);
    LE_SALIDA EXCEPTION;
    --LV_FILE       VARCHAR2(30) := 'PCK_VISTA_MATERIALIZADA_';
    LV_APLICACION VARCHAR2(100) := '>>PCK_VISTA_MATERIALIZADA.PR_VISTA_MATIALIZADA';
    LN_REGISTROS_OK             NUMBER:=0;
    LD_FECHA DATE; 
         
    --INICIO   
--    LV_PROCESO_SCP              VARCHAR2(50);
    LV_UNIDAD_REGISTRO          VARCHAR2(30):='CANTIDAD_REGISTROS';
    LV_PROGRAMA                 VARCHAR2(50):='PCK_VISTA_MATERIALIZADA.PR_VISTA_MATIALIZADA';
    LV_REGISTRO                 VARCHAR2(50):='CO_FEES_MV_8718';
   -- LV_ERROR_SCP                VARCHAR2(100);
    LV_MSG                      VARCHAR2(2000);
  -- LN_BITACORA                 NUMBER;
    --LN_ERROR_SCP                NUMBER;
    LN_INCORRECTOS              NUMBER:=0;
    LN_TOTAL_REGISTROS_SCP      NUMBER:=0;
    --LN_ID_DETALLE_SCP           NUMBER:=0;
--    LV_VALOR_SCP                VARCHAR2(1);-- SUD RGA
    LV_DESCRIPCION              VARCHAR2(100);--- SUD RGA    
    --FIN 
    
    --
    LV_ERROR                 VARCHAR2(30);
    LE_ERROR                 EXCEPTION;

    LV_FROM_NAME             VARCHAR2(200);
    LV_SERVIDOR_MAIL         VARCHAR2(60);
    LV_ASUNTO                VARCHAR2(2000);
    LV_CUERPO_MAIL           VARCHAR2(2000);
    LV_RESPCORREO            BOOLEAN;
    LV_RESP                  VARCHAR2(1000);
    LB_FOUND                  BOOLEAN;
    -------
    TO_NAME                  VARCHAR2(4000);--- RECEPTORES DE MAIL 
 
  BEGIN    
    
--    LV_PROCESO_SCP:='SCP_BITACORA_PROCESOS'; 
    LD_FECHA:= TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY') ||' 00:00:00', 'DD/MM/YYYY HH24:MI:SS');
      
    /*
    comi_graba_bitacora(lv_file,'-------------------- VISTAS MATERIALIZADAS -------------------');
    comi_graba_bitacora(lv_file,'Fech. Proc: '||to_char(LD_FECHA,'dd/mm/yyyy'));
    comi_graba_bitacora(lv_file,'MENSAJE: Inicio del proceso: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'));
    comi_graba_bitacora(lv_file,'--------------------------------------------------------------');
    */

    -- Obtiene  parametros correo
    OPEN C_MAILSERVER('POR');
    FETCH C_MAILSERVER
       INTO LV_SERVIDOR_MAIL; 
    LB_FOUND := C_MAILSERVER%FOUND;
    CLOSE C_MAILSERVER;

    IF LB_FOUND = FALSE THEN
       LV_ERROR := 'No se encuentra configurado servidor para envio de correo';
       RAISE LE_ERROR;
    END IF;

    OPEN C_FROM_NAME;
    FETCH C_FROM_NAME
       INTO LV_FROM_NAME;
    LB_FOUND := C_FROM_NAME%FOUND;
    CLOSE C_FROM_NAME;

    IF LB_FOUND = FALSE THEN
       LV_ERROR := 'No se encuentra configurado los parametros para envio de correo';
       RAISE LE_ERROR;
    END IF;
    --------------------------------
    OPEN C_TO_NAME;
    FETCH C_TO_NAME
    INTO TO_NAME; 
    LB_FOUND := C_TO_NAME%FOUND;
    CLOSE C_TO_NAME;
    IF LB_FOUND = FALSE THEN
      LV_ERROR :='No se encuentra configurado los parametros para la recepcion de correo';
      raise LE_ERROR;
    END IF; 
    ---------------------------------
  
    -- Obtengo universo de la tabla origen
    open  LC_TOTAL;
    fetch LC_TOTAL 
    into LN_TOTAL_REGISTROS_SCP;
    close LC_TOTAL;
    --comi_graba_bitacora(lv_file, 'Registros a Procesar: '|| to_char(ln_reg_procesar));
    
    /*SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_BITACORIZA_VM',
                                                LV_PROCESO_SCP,
                                                LV_VALOR_SCP,
                                                LV_DESCRIPCION,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
    IF NVL(LV_VALOR_SCP,'N') = 'S' THEN
        --SCP CABECERA:
        ----------------------------------------------------------------------------
        -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
        ----------------------------------------------------------------------------
        SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS(LV_PROCESO_SCP,LV_PROGRAMA,NULL,NULL,NULL,NULL,LN_TOTAL_REGISTROS_SCP,LV_REGISTRO,LN_BITACORA,LN_ERROR_SCP,LV_ERROR_SCP);
    END IF;   */ 
    
    --- INVOCACIÓN DE LA SENTENCIA QUE EJECUTA EL REFRESH DE LA VISTA MATERIALIZADA CO_FEES_MV_8718    
    --- comi_graba_bitacora(lv_file, 'Inicia el Refresh: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'));    
    DBMS_MVIEW.REFRESH('SYSADM.CO_FEES_MV_8718') ;  -- db Oracle v.10g
   -- DBMS_REFRESH.REFRESH('CO_FEES_MV_8718'); -- db Oracle v.8
    --- comi_graba_bitacora(lv_file, 'Finaliza el Refresh: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'));
    
    -- Obtengo universo  de la Vista Materializada despues del Refresh
    open  LC_TOTAL_VM_OK;
    fetch LC_TOTAL_VM_OK into LN_REGISTROS_OK;
    close LC_TOTAL_VM_OK;
    --comi_graba_bitacora(lv_file, 'Registros en VM: '|| to_char(LN_REGISTROS_OK));      

 
       /*IF NVL(LV_VALOR_SCP,'N') = 'S' THEN
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_BITACORA,LN_REGISTROS_OK,LN_INCORRECTOS,ln_error_scp,lv_error_scp);            
            ----------------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de finalización de proceso
            ----------------------------------------------------------------------------
            SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_BITACORA,ln_error_scp,lv_error_scp);
       END IF;*/
       
      ------- Alerta 
      IF LN_TOTAL_REGISTROS_SCP <> LN_REGISTROS_OK THEN 
          --* ENVIAMOS EL CORREO AL USUARIO
          LV_ASUNTO      := 'Inconsistencias en el Refrescamiento ' || 'PCK_VISTA_MATERIALIZADA' ||
                            ' - Problemas en el Refrescamiento ' || LV_REGISTRO;
          LV_CUERPO_MAIL := 'Ocurrieron Inconstencias en el refrescamiento de la vista materializada :  ' ||LV_REGISTRO||
                            Chr(13) || Chr(13) || ' - Problemas en el Refrescamiento:     ' || LV_REGISTRO || Chr(13) ||
                            ' - Fecha: ' || trunc(sysdate) || Chr(13) || Chr(13) ||
                            'Comunicarse :       ' || 'SIS Wilson Pozo' || Chr(13) ||
                            'Observación:                          Ejecutar nuevamente el Refrescamiento. ' || Chr(13) ||Chr(13) ||
                            'Cant. Vista :       ' || LN_REGISTROS_OK || Chr(13) || 
                            'Cant.  Tabla:       ' || LN_TOTAL_REGISTROS_SCP || Chr(13) ||                            
                            Chr(13) || Chr(13) || 'Saludos Cordiales';
                            
          porta.SWK_MAIL.ENVIA_MAIL_CC_FILES@AXIS(LV_SERVIDOR_MAIL,
                                            LV_FROM_NAME,
                                            TO_NAME,
                                            --LV_CORREO_TO || ',',
                                            --LV_CORREO_CC || ',',
                                            NULL,
                                            LV_ASUNTO,
                                            LV_CUERPO_MAIL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            LV_RESPCORREO,
                                            LV_RESP);
          IF LV_RESP IS NOT NULL THEN
             LV_ERROR := 'ERROR EN EL ENVIO DE CORREO ELECTRÓNICO';
             RAISE LE_ERROR;
          END IF; 
          --* ENVIAMOS EL CORREO AL USUARIO
      ELSE 
              
        LV_ASUNTO      := 'Refrescamiento ' || 'PCK_VISTA_MATERIALIZADA' ||
                            ' -  Refrescamiento con Exito ' || LV_REGISTRO;
          LV_CUERPO_MAIL := 'El refrescamiento de la vista materializada : ' ||LV_REGISTRO||
                            Chr(13) || Chr(13) || ' - El Refrescamiento de la vista fue exitoso:     ' || LV_REGISTRO || Chr(13) ||
                            ' - Fecha: ' || trunc(sysdate) || Chr(13) || Chr(13) ||
                           'Observación:                          No hay novedades. ' || Chr(13) ||Chr(13) ||
                           'Cant. Vista :       ' || LN_REGISTROS_OK || Chr(13) || 
                           'Cant.  Tabla:       ' || LN_TOTAL_REGISTROS_SCP || Chr(13) || 
                            Chr(13) || Chr(13) || 'Saludos Cordiales';
                            
          porta.SWK_MAIL.ENVIA_MAIL_CC_FILES@AXIS(LV_SERVIDOR_MAIL,
                                            LV_FROM_NAME,
                                            TO_NAME,
                                            NULL,
                                            LV_ASUNTO,
                                            LV_CUERPO_MAIL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            LV_RESPCORREO,
                                            LV_RESP);
          IF LV_RESP IS NOT NULL THEN
             LV_ERROR := 'ERROR EN EL ENVIO DE CORREO ELECTRÓNICO';
             RAISE LE_ERROR;
          END IF; 

        PV_MSG_ERROR := 'Refrescamiento Exitos' ;       
      END IF ; 
        
   EXCEPTION
     WHEN OTHERS THEN 
        PV_MSG_ERROR:= '> ERROR > General: No se puede continuar procesando.  '||LV_PROGRAMA||sqlerrm ; 
        
       /* IF NVL(LV_VALOR_SCP,'N') = 'S' THEN
            LN_INCORRECTOS:= LN_INCORRECTOS + 1;
            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_BITACORA,
                                                      LV_PROGRAMA,
                                                      'ERROR GENERAL: No se puede continuar procesando el PCK_VISTAS_MATERIALIZADAS '||' - '||SQLERRM,   
                                                      NULL,
                                                      3, -- niveles de Errores
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      'S', -- notifica el error si o no
                                                      LN_ID_DETALLE_SCP,
                                                      LN_ERROR_SCP,
                                                      LV_ERROR_SCP);
            -----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de avance de proceso
            -----------------------------------------------------------------------
            SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_BITACORA,LN_REGISTROS_OK,LN_INCORRECTOS,ln_error_scp,lv_error_scp);
            
            ----------------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de finalización de proceso
            ----------------------------------------------------------------------------
            SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_BITACORA,ln_error_scp,lv_error_scp);
        END IF;*/
                
  END PR_VISTA_MATIALIZADA; --- fin del Proceso 
  
END;--- fin del Package
/
