CREATE OR REPLACE PACKAGE COK_REPORTES_CAPAS IS
  -------------------------------------------------------------
  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                 --CLS PAUL PADILLA
  -- CREATED : 25/08/2014 15:37:09
  -- PURPOSE : MEJORA DEL REPORTE POR CAPAS [9824]
  -------------------------------------------------------------  

  PROCEDURE PR_REPORTE_CAPAS(PD_FECHA_FIN IN DATE, PV_SMS OUT VARCHAR2);

  PROCEDURE PR_REPORTE_CAPAS_MAIN(PD_FECHA_FIN_MAIN IN DATE,
                                  PV_SMS            OUT VARCHAR2);

  PROCEDURE PR_HORA_FINAL(PV_PARAMETRO IN VARCHAR2, PV_SMS OUT VARCHAR2);
  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                --CLS PAUL PADILLA
  -- MODIFY   : 27/10/2014 15:37:09
  -- PURPOSE  : MEJORA EN EL PROCESO DE REPORTE POR CAPAS [9824]
  --            SE AGREGO UN PARAMETRO DE ENTRADA(PV_OBSERVACION) AL PROCESO PR_INSERTA_BITACORA
  PROCEDURE PR_INSERTA_BITACORA(PV_NOMBRE_SH   IN VARCHAR2,
                                PN_BANDERA     IN NUMBER,
                                PD_FECHA_FIN   IN DATE,
                                PD_PERIODO     IN DATE,
                                PN_CUSTOMER    IN NUMBER,
                                PV_ESTADO      IN VARCHAR2,
                                PV_OBSERVACION IN VARCHAR2,
                                PV_SMS         OUT VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);
  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                --CLS PAUL PADILLA
  -- MODIFY   : 27/10/2014 15:37:09
  -- PURPOSE  : MEJORA EN EL PROCESO DE REPORTE POR CAPAS [9824]
  --            SE AGREGO UN PARAMETRO DE ENTRADA(PV_OBSERVACION) AL PROCESO PR_INSERTA_BITACORA_MAIN
  
  PROCEDURE PR_INSERTA_BITACORA_MAIN(PV_NOMBRE_SH   IN VARCHAR2,
                                     PN_BANDERA     IN NUMBER,
                                     PD_FECHA_FIN   IN DATE,
                                     PD_PERIODO     IN DATE,
                                     PN_CUSTOMER    IN NUMBER,
                                     PV_ESTADO      IN VARCHAR2,
                                     PV_OBSERVACION IN VARCHAR2,
                                     PV_SMS         OUT VARCHAR2,
                                     PV_ERROR       OUT VARCHAR2);
                                     
  PROCEDURE PR_VALIDA_COFACT(PN_FECHA IN DATE,
                             PN_RESP OUT NUMBER,
                             PV_ERROR OUT VARCHAR2);
   
  PROCEDURE PR_VALIDA_REPCARCLI(PN_FECHA IN DATE,
                                PN_RESP OUT NUMBER,
                                PV_ERROR OUT VARCHAR2);
                                
  PROCEDURE PR_VALIDA_CO_REPCAP(PN_FECHA IN DATE,
                                PN_RESP OUT NUMBER,
                                PV_ERROR OUT VARCHAR2);
                                
  PROCEDURE PR_CAPAS_ALARMA(PN_IDENTIFICADOR IN NUMBER,
                            PV_ERROR       OUT VARCHAR2);
                            
  PROCEDURE PR_BALANCEA_CAPAS(PN_HILO      IN NUMBER,
                              PV_TABLA     IN VARCHAR2,
                              PV_ROWID     IN VARCHAR2,
                              PV_ERROR     OUT VARCHAR2);

 PROCEDURE PR_BALANCEA_REPORTE_CAPAS(PN_PARAMETRO IN VARCHAR2,
                                    PN_BANDERA in varchar2,
                                    PV_ERROR   OUT VARCHAR2);
                                    
  PROCEDURE PR_INSERTA_VALIDA_PORCENTAJE(PD_FECHA_INI IN DATE,
                                        PD_FECHA_FIN IN DATE,
                                        PV_ERROR     OUT VARCHAR2);
                                                                 
 PROCEDURE PR_BALANCEA_REPORTE_CAPAS_MAIN(PN_PARAMETRO IN VARCHAR2,
                                          PN_BANDERA in varchar2,
                                          PV_ERROR   OUT VARCHAR2);                                  

END COK_REPORTES_CAPAS;
/
CREATE OR REPLACE PACKAGE BODY COK_REPORTES_CAPAS IS
  -------------------------------------------------------------
  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                 --CLS PAUL PADILLA
  -- CREATED :   25/08/2014 15:37:09
  -- PURPOSE :   MEJORA DEL REPORTE POR CAPAS [9824]
  ------------------------------------------------------------- 
  PROCEDURE PR_REPORTE_CAPAS(PD_FECHA_FIN IN DATE, PV_SMS OUT VARCHAR2) IS
  
    CURSOR C_FECHA(CD_FECHA_FIN DATE) IS
      SELECT F.FECHA_INICIO,
             F.FECHA_FIN,
             F.DIA,
             F.ESTADO_EJECUCION_LLB,
             F.CONTADOR_LLB,
             F.ESTADO
        FROM RPC_CAPAS_FECHAS F
       WHERE F.FECHA_FIN = CD_FECHA_FIN;
  
    CURSOR C_SECUENCIA_F IS
      SELECT SEQ_REPORTE_CAPAS.NEXTVAL FROM DUAL;
  
    LD_FECHA_INI    DATE;
    LN_SECUENCIA    NUMBER;
    LC_FECHA        C_FECHA%ROWTYPE;
    LN_DIA          VARCHAR2(2);
    LB_BOOLEAN      BOOLEAN;
    LN_CONTADOR_LLB NUMBER := 1;
    LN_ESTADO       NUMBER := 0;
  
  BEGIN
  
    OPEN C_FECHA(PD_FECHA_FIN);
    FETCH C_FECHA
      INTO LC_FECHA;
    LB_BOOLEAN := C_FECHA%NOTFOUND;
    CLOSE C_FECHA;
  
    IF LB_BOOLEAN = TRUE THEN
      OPEN C_SECUENCIA_F;
      FETCH C_SECUENCIA_F
        INTO LN_SECUENCIA;
      CLOSE C_SECUENCIA_F;
    
      SELECT ADD_MONTHS(TRUNC(TO_DATE(PD_FECHA_FIN, 'DD/MM/RR')), -11)
        INTO LD_FECHA_INI
        FROM DUAL;
      SELECT TO_CHAR(PD_FECHA_FIN, 'DD') INTO LN_DIA FROM DUAL;
    
      INSERT INTO RPC_CAPAS_FECHAS
        (ID_FECHAS,
         FECHA_INICIO,
         FECHA_FIN,
         DIA,
         ESTADO_EJECUCION_LLB,
         FECHA_INI_PRO_LLB,
         CONTADOR_LLB,
         ESTADO)
      VALUES
        (LN_SECUENCIA,
         LD_FECHA_INI,
         PD_FECHA_FIN,
         LN_DIA,
         'S',
         SYSDATE,
         LN_CONTADOR_LLB,
         LN_ESTADO);
      COMMIT;
    
      PV_SMS := TO_CHAR(LD_FECHA_INI, 'DD/MM/RRRR') || '|' ||
                TO_CHAR(PD_FECHA_FIN, 'DD/MM/RRRR') || '|' || LN_DIA || '|' ||
                LN_ESTADO || '|' || LN_CONTADOR_LLB || '|';
    
    ELSE
      IF LC_FECHA.ESTADO_EJECUCION_LLB = 'S' THEN
        LN_CONTADOR_LLB := LC_FECHA.CONTADOR_LLB + 1;
      
        UPDATE RPC_CAPAS_FECHAS A
           SET A.CONTADOR_LLB = LN_CONTADOR_LLB
         WHERE A.FECHA_FIN = LC_FECHA.FECHA_FIN;
        COMMIT;
      
        PV_SMS := TO_CHAR(LC_FECHA.FECHA_INICIO, 'DD/MM/RRRR') || '|' ||
                  TO_CHAR(LC_FECHA.FECHA_FIN, 'DD/MM/RRRR') || '|' ||
                  LC_FECHA.DIA || '|' || LC_FECHA.ESTADO || '|' ||
                  LN_CONTADOR_LLB || '|' || 'EJECUCION ' || '|';
      ELSE
        IF LC_FECHA.ESTADO_EJECUCION_LLB = 'P' THEN
          PV_SMS := 'EL REPORTE DE LA FECHA ' || PD_FECHA_FIN ||
                    ' YA HA SIDO PROCESADO';
        ELSE
          PV_SMS := 'REVISAR LA TABLA "FECHAS_CAPAS" ';
        END IF;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_SMS := 'ERROR: ' || SUBSTR(SQLERRM, 1, 500);
  END PR_REPORTE_CAPAS;

PROCEDURE PR_REPORTE_CAPAS_MAIN(PD_FECHA_FIN_MAIN IN DATE,
                                PV_SMS            OUT VARCHAR2) IS
  
    CURSOR C_FECHA_MAIN(CD_FECHA_FIN_MAIN DATE) IS
      SELECT F.FECHA_INICIO,
             F.FECHA_FIN,
             F.ESTADO_EJECUCION_LLB,
             F.ESTADO_EJECUCION_MAIN,
             F.CONTADOR_MAIN,
             F.ESTADO_MAIN
      FROM   RPC_CAPAS_FECHAS F 
      WHERE  F.FECHA_FIN = CD_FECHA_FIN_MAIN;
  
    LN_CONTADOR_MAIN NUMBER := 1;
    LC_FECHA_MAIN    C_FECHA_MAIN%ROWTYPE;
    LB_BOOLEAN       BOOLEAN;
    LN_ESTADO_MAIN   NUMBER := 0;
  
  BEGIN
  
      OPEN C_FECHA_MAIN(PD_FECHA_FIN_MAIN);
      FETCH C_FECHA_MAIN INTO LC_FECHA_MAIN;
      LB_BOOLEAN := C_FECHA_MAIN%FOUND;
      CLOSE C_FECHA_MAIN;
      
    IF LB_BOOLEAN = TRUE THEN
    
      IF LC_FECHA_MAIN.ESTADO_EJECUCION_LLB = 'P' AND LC_FECHA_MAIN.ESTADO_EJECUCION_MAIN IS NULL THEN
         
        UPDATE RPC_CAPAS_FECHAS
           SET ESTADO_EJECUCION_MAIN = 'S',
               FECHA_INI_PRO_MAIN = SYSDATE,
               CONTADOR_MAIN = LN_CONTADOR_MAIN,
               ESTADO_MAIN = LN_ESTADO_MAIN
         WHERE FECHA_FIN = PD_FECHA_FIN_MAIN
         AND   ESTADO_EJECUCION_LLB = 'P';
       COMMIT;
          
         PV_SMS := TO_CHAR(LC_FECHA_MAIN.FECHA_INICIO, 'DD/MM/RRRR') || '|' ||
                   TO_CHAR(PD_FECHA_FIN_MAIN, 'DD/MM/RRRR') || '|' ||
                   LN_ESTADO_MAIN || '|' || LN_CONTADOR_MAIN|| '|';

      ELSIF LC_FECHA_MAIN.ESTADO_EJECUCION_LLB = 'P' AND LC_FECHA_MAIN.ESTADO_EJECUCION_MAIN = 'S' THEN
          LN_CONTADOR_MAIN := LC_FECHA_MAIN.CONTADOR_MAIN + 1;
      
        UPDATE RPC_CAPAS_FECHAS
           SET CONTADOR_MAIN = LN_CONTADOR_MAIN 
           WHERE FECHA_FIN = PD_FECHA_FIN_MAIN; 
        COMMIT;
               PV_SMS := TO_CHAR(LC_FECHA_MAIN.FECHA_INICIO, 'DD/MM/RRRR') || '|' ||
                         TO_CHAR(PD_FECHA_FIN_MAIN, 'DD/MM/RRRR') || '|' ||
                         LN_ESTADO_MAIN || '|' || LN_CONTADOR_MAIN|| '|' ||
                         'EL REPORTE DE LA FECHA ' || PD_FECHA_FIN_MAIN ||
                         ' ESTA EN EJECUCIONM EN EL MAIN';
                   
      ELSIF LC_FECHA_MAIN.ESTADO_EJECUCION_LLB = 'S' AND LC_FECHA_MAIN.ESTADO_EJECUCION_MAIN IS NULL THEN
          LN_CONTADOR_MAIN := LC_FECHA_MAIN.CONTADOR_MAIN + 1;

          UPDATE RPC_CAPAS_FECHAS F
             SET F.CONTADOR_MAIN = LN_CONTADOR_MAIN
           WHERE F.FECHA_FIN_PRO_MAIN = PD_FECHA_FIN_MAIN;
          COMMIT;
          
          PV_SMS := 'EL REPORTE DE LA FECHA ' || PD_FECHA_FIN_MAIN ||
                    ' ESTA EN EJECUCIONL EN LLENA BALANCE';
        
          ELSE
          PV_SMS := 'EL PERIODO '|| PD_FECHA_FIN_MAIN ||' HA SIDO PROCESADO REVISAR LA TABLA FECHAS_CAPAS';
       END IF;
          

    ELSE
    
      PV_SMS := 'FECHA NO REVISADA EN PROCESO LLENA BALANCE' ||
                 PD_FECHA_FIN_MAIN || '|'; 
    END IF;
   
  EXCEPTION
  
      WHEN OTHERS THEN
      PV_SMS := 'ERROR: ' || SUBSTR(SQLERRM, 1, 500);
    
  END PR_REPORTE_CAPAS_MAIN;
  
  
  PROCEDURE PR_HORA_FINAL(PV_PARAMETRO IN VARCHAR2, PV_SMS OUT VARCHAR2) IS
  
    CURSOR C_HORA(CV_PARAMETRO VARCHAR2) IS
      SELECT (TO_CHAR((SYSDATE + (TO_NUMBER(NVL(VALOR2, '30'), 'FM999') * 1 /
                      (24 * 60))),
                      'DDMMRRRRHH24MI')) AS HORA_I,
             HILO AS C_HILO
        FROM RPC_CAPAS_PARAMETROS A
       WHERE A.CODIGO = CV_PARAMETRO
         AND ESTADO = 'A';
  
    LC_HORA C_HORA%ROWTYPE;
  
  BEGIN
    OPEN C_HORA(PV_PARAMETRO);
    FETCH C_HORA
      INTO LC_HORA;
    CLOSE C_HORA;
    PV_SMS := LC_HORA.HORA_I || '|' || LC_HORA.C_HILO || '|';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_SMS := 'ERROR: ' || SUBSTR(SQLERRM, 1, 500);
    
  END PR_HORA_FINAL;

  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                --CLS PAUL PADILLA
  -- MODIFY   : 27/10/2014 15:37:09
  -- PURPOSE  : MEJORA EN EL PROCESO DE REPORTE POR CAPAS [9824]
  --            SE AGREGO UN PARAMETRO DE ENTRADA(PV_OBSERVACION) AL PROCESO PR_INSERTA_BITACORA
  
  PROCEDURE PR_INSERTA_BITACORA(PV_NOMBRE_SH   IN VARCHAR2,
                                PN_BANDERA     IN NUMBER,
                                PD_FECHA_FIN   IN DATE,
                                PD_PERIODO     IN DATE,
                                PN_CUSTOMER    IN NUMBER,
                                PV_ESTADO      IN VARCHAR2,
                                PV_OBSERVACION IN VARCHAR2,
                                PV_SMS         OUT VARCHAR2,
                                PV_ERROR       OUT VARCHAR2) IS
                                
   --cursor para sacar los destinatarios de las alarmas 
     CURSOR C_ALARMA IS
      SELECT B.FROM_NAME, B.TO_NAME, B.CC, B.CCO
      FROM   RPC_CAPAS_PARAMETROS_ALARMA B
      WHERE  B.ESTADO = 'A';
  
   --cursor para sacar la secuencia de CAPAS_BITACORA
    CURSOR C_BITACORA IS
      SELECT SEQ_BITACORA.NEXTVAL FROM DUAL;
  
   --cursor para sacar el detalle de cada paso
    CURSOR C_DESCRIPCION(CN_BANDERA NUMBER) IS
      SELECT DESCRIPCION FROM RPC_CAPAS_DETALLE 
      WHERE ID_CAPAS = CN_BANDERA
      AND   ESTADO = 'A';
            
   --cursor para sacar el contador de FECHAS_CAPAS
    CURSOR C_CONTADOR(CD_FECHA_FIN DATE) IS
      SELECT CONTADOR_LLB 
      FROM RPC_CAPAS_FECHAS
      WHERE FECHA_FIN =CD_FECHA_FIN;
  
    LN_BITACORA    NUMBER;
    LV_DESCRIPCION VARCHAR2(60);
    LB_BOOLEAN     BOOLEAN;
    LN_CONTADOR    NUMBER;
    LC_ALARMA      C_ALARMA%ROWTYPE;
    
  BEGIN
  
    OPEN C_DESCRIPCION(PN_BANDERA);
    FETCH C_DESCRIPCION
    INTO LV_DESCRIPCION;
    LB_BOOLEAN := C_DESCRIPCION%FOUND;
    CLOSE C_DESCRIPCION;
    
    OPEN C_CONTADOR(PD_FECHA_FIN);
    FETCH C_CONTADOR INTO LN_CONTADOR;
    CLOSE C_CONTADOR;
  
    IF LB_BOOLEAN = TRUE THEN
      BEGIN
        OPEN C_BITACORA;
        FETCH C_BITACORA
          INTO LN_BITACORA;
        CLOSE C_BITACORA;
        INSERT INTO RPC_CAPAS_BITACORA
          (ID_BITACORA,
           IDENTIFICADOR,
           CONTADOR,
           FECHA_FIN,
           NOMBRE_DEL_SH,
           DESCRIPCION,
           PERIODO,
           CUSTOMER,
           FECHA,
           ESTADO,
           OBSERVACION)
        VALUES
          (LN_BITACORA,
           PN_BANDERA,
           LN_CONTADOR,
           PD_FECHA_FIN,
           PV_NOMBRE_SH,
           LV_DESCRIPCION,
           PD_PERIODO,
           PN_CUSTOMER,
           SYSDATE,
           PV_ESTADO,
           PV_OBSERVACION);
      
        UPDATE RPC_CAPAS_FECHAS
           SET ESTADO = PN_BANDERA
         WHERE FECHA_FIN = PD_FECHA_FIN;
        COMMIT;
      
        PV_SMS   := PN_BANDERA + 1;
        
      EXCEPTION
        WHEN OTHERS THEN
           PV_ERROR := SUBSTR(SQLERRM, 1, 500);
              IF  PV_ERROR IS NOT NULL THEN
                  OPEN C_ALARMA;
                  FETCH C_ALARMA INTO LC_ALARMA;
                  CLOSE C_ALARMA;
                        PORTA.SEND_MAIL.MAIL@AXIS(FROM_NAME => LC_ALARMA.FROM_NAME,
                                                  TO_NAME   => LC_ALARMA.TO_NAME,
                                                  CC        => LC_ALARMA.CC,
                                                  CCO       => LC_ALARMA.CCO,
                                                  SUBJECT   => LV_DESCRIPCION,
                                                  MESSAGE   => 'ERROR EN EL PERIODO O CUSTOMER: '|| PD_PERIODO ||PN_CUSTOMER||   ' LA OBSERVACION DEL ERROR ES: ' || PV_ERROR);
              END IF;
                  IF C_BITACORA%ISOPEN THEN
                     CLOSE C_BITACORA;
                     ROLLBACK;
                  END IF;
      END;
    ELSE
      PV_SMS := 'EL CURSOR NO DEVOLVIO NADA';
    END IF;
  COMMIT;
  
 IF  PV_OBSERVACION IS NOT NULL THEN
     OPEN C_ALARMA;
     FETCH C_ALARMA INTO LC_ALARMA;
     CLOSE C_ALARMA;
           PORTA.SEND_MAIL.MAIL@AXIS(FROM_NAME => LC_ALARMA.FROM_NAME,
                                     TO_NAME   => LC_ALARMA.TO_NAME,
                                     CC        => LC_ALARMA.CC,
                                     CCO       => LC_ALARMA.CCO,
                                     SUBJECT   => LV_DESCRIPCION,
                                     MESSAGE   => 'ERROR EN EL PERIODO O CUSTOMER: '|| PD_PERIODO || PN_CUSTOMER || ' LA OBSERVACION DEL ERROR ES: ' || PV_OBSERVACION);
 END IF;
 
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR(SQLERRM, 1, 500);
    
  END PR_INSERTA_BITACORA;

  -- AUTHORS  : --CLS JEAN CARLOS OYAGUE
                --CLS PAUL PADILLA
  -- MODIFY   : 27/10/2014 15:37:09
  -- PURPOSE  : MEJORA EN EL PROCESO DE REPORTE POR CAPAS [9824]
  --            SE AGREGO UN PARAMETRO DE ENTRADA(PV_OBSERVACION) AL PROCESO PR_INSERTA_BITACORA_MAIN
  PROCEDURE PR_INSERTA_BITACORA_MAIN(PV_NOMBRE_SH   IN VARCHAR2,
                                     PN_BANDERA     IN NUMBER,
                                     PD_FECHA_FIN   IN DATE,
                                     PD_PERIODO     IN DATE,
                                     PN_CUSTOMER    IN NUMBER,
                                     PV_ESTADO      IN VARCHAR2,
                                     PV_OBSERVACION IN VARCHAR2,
                                     PV_SMS         OUT VARCHAR2,
                                     PV_ERROR       OUT VARCHAR2) IS
 
   --cursor para sacar los destinatarios de las alarmas                             
      CURSOR C_ALARMA IS
      SELECT B.FROM_NAME, B.TO_NAME, B.CC, B.CCO
      FROM   RPC_CAPAS_PARAMETROS_ALARMA B
      WHERE  B.ESTADO = 'A';
       
    --cursor para sacar la secuencia de CAPAS_BITACORA
    CURSOR C_BITACORA_MAIN IS
      SELECT SEQ_BITACORA_MAIN.NEXTVAL FROM DUAL;
  
    --cursor para sacar el detalle de cada paso
    CURSOR C_DESCRIPCION_MAIN(CN_BANDERA NUMBER) IS
      SELECT DESCRIPCION
        FROM RPC_CAPAS_DETALLE_MAIN
       WHERE ID_CAPAS = CN_BANDERA;
       
    --cursor para sacar el contador de FECHAS_CAPAS
    CURSOR C_CONTADOR(CD_FECHA_FIN DATE) IS
     SELECT CONTADOR_LLB 
      FROM RPC_CAPAS_FECHAS
      WHERE FECHA_FIN =CD_FECHA_FIN;
  
    LN_BITACORA    NUMBER;
    LV_DESCRIPCION VARCHAR2(60);
    LB_BOOLEAN     BOOLEAN;
    LN_CONTADOR     NUMBER;
    LC_ALARMA      C_ALARMA%ROWTYPE;
    
  BEGIN
  
    OPEN C_DESCRIPCION_MAIN(PN_BANDERA);
    FETCH C_DESCRIPCION_MAIN
      INTO LV_DESCRIPCION;
    LB_BOOLEAN := C_DESCRIPCION_MAIN%FOUND;
    CLOSE C_DESCRIPCION_MAIN;
    
     OPEN C_CONTADOR(PD_FECHA_FIN);
    FETCH C_CONTADOR INTO LN_CONTADOR;
    CLOSE C_CONTADOR;
  
    IF LB_BOOLEAN = TRUE THEN
      BEGIN
        OPEN C_BITACORA_MAIN;
        FETCH C_BITACORA_MAIN
          INTO LN_BITACORA;
        CLOSE C_BITACORA_MAIN;
        INSERT INTO RPC_CAPAS_BITACORA_M
          (ID_BITACORA,
           IDENTIFICADOR,
           CONTADOR,
           FECHA_FIN,
           NOMBRE_DEL_SH,
           DESCRIPCION,
           PERIODO,
           CUSTOMER,
           FECHA,
           ESTADO,
           OBSERVACION)
        VALUES
          (LN_BITACORA,
           PN_BANDERA,
           LN_CONTADOR,
           PD_FECHA_FIN,
           PV_NOMBRE_SH,
           LV_DESCRIPCION,
           PD_PERIODO,
           PN_CUSTOMER,
           SYSDATE,
           PV_ESTADO,
           PV_OBSERVACION);
      
        UPDATE RPC_CAPAS_FECHAS
           SET ESTADO_MAIN = PN_BANDERA
         WHERE FECHA_FIN = PD_FECHA_FIN;
        COMMIT;
      
        PV_SMS := PN_BANDERA + 1;
      
      EXCEPTION
        WHEN OTHERS THEN
           PV_ERROR := SUBSTR(SQLERRM, 1, 500);
              IF  PV_ERROR IS NOT NULL THEN
                  OPEN C_ALARMA;
                  FETCH C_ALARMA INTO LC_ALARMA;
                  CLOSE C_ALARMA;
                        PORTA.SEND_MAIL.MAIL@AXIS(FROM_NAME => LC_ALARMA.FROM_NAME,
                                                  TO_NAME   => LC_ALARMA.TO_NAME,
                                                  CC        => LC_ALARMA.CC,
                                                  CCO       => LC_ALARMA.CCO,
                                                  SUBJECT   => LV_DESCRIPCION,
                                                  MESSAGE   => 'ERROR EN EL CUSTOMER : '|| PN_CUSTOMER || ' LA OBSERVACION DEL ERROR ES: ' || PV_ERROR);
              END IF;                  
          IF C_BITACORA_MAIN%ISOPEN THEN
            CLOSE C_BITACORA_MAIN;
            ROLLBACK;
          END IF;
      END;
    ELSE
      PV_SMS := 'EL CURSOR NO DEVOLVIO NADA';
    END IF;

IF  PV_OBSERVACION IS NOT NULL THEN
     OPEN C_ALARMA;
     FETCH C_ALARMA INTO LC_ALARMA;
     CLOSE C_ALARMA;
           PORTA.SEND_MAIL.MAIL@AXIS(FROM_NAME => LC_ALARMA.FROM_NAME,
                                     TO_NAME   => LC_ALARMA.TO_NAME,
                                     CC        => LC_ALARMA.CC,
                                     CCO       => LC_ALARMA.CCO,
                                     SUBJECT   => LV_DESCRIPCION,
                                     MESSAGE   => 'ERROR EN EL CUSTOMER : '|| PN_CUSTOMER || ' LA OBSERVACION DEL ERROR ES: ' || PV_OBSERVACION);
END IF;
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR(SQLERRM, 1, 500);
    
  END PR_INSERTA_BITACORA_MAIN;
  
PROCEDURE PR_VALIDA_COFACT(PN_FECHA IN DATE,
                          PN_RESP OUT NUMBER,
                          PV_ERROR OUT VARCHAR2)IS
                                           
      CURSOR C_TABLAS(CV_TABLA VARCHAR2) IS
        SELECT COUNT(*)
        FROM ALL_TABLES
        WHERE TABLE_NAME=UPPER(CV_TABLA);
        
      CURSOR C_INDICES(CV_TABLA VARCHAR2) IS
        SELECT count(*) 
        FROM ALL_INDEXES R 
        WHERE TABLE_NAME = CV_TABLA;
    
        
LV_TABLA           VARCHAR2(50):='CO_FACT_';
LV_DATOS_TABLA     VARCHAR2(50);
--LV_DATOS_INDICES   VARCHAR2(100);
LN_EXISTE_TA       NUMBER:=0;
LN_EXISTE_IN       NUMBER:=0;
LV_FECHA           VARCHAR2(20);


begin
    LV_FECHA := TO_CHAR(PN_FECHA,'DDMMYYYY');
    LV_DATOS_TABLA := LV_TABLA||LV_FECHA;
/*    LV_DATOS_INDICES := '''I_COFACT_'||LV_FECHA||''','||
                        '''I_COFACT2_'||LV_FECHA||''','||
                        '''I_COFACT3_'||LV_FECHA||'''';*/
  
       OPEN C_TABLAS(LV_DATOS_TABLA);
       FETCH C_TABLAS INTO LN_EXISTE_TA;
       CLOSE C_TABLAS;
       
       OPEN C_INDICES(LV_DATOS_TABLA);
       FETCH C_INDICES INTO LN_EXISTE_IN;
       CLOSE C_INDICES;
           
        IF (LN_EXISTE_TA > 0) THEN
            IF (LN_EXISTE_IN > 0) THEN
                PN_RESP:=1;
            ELSE
                PN_RESP:=0;
            END IF;
        ELSE
           PN_RESP:=0;        
        END IF;
               
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);

END PR_VALIDA_COFACT;

PROCEDURE PR_VALIDA_REPCARCLI(PN_FECHA IN DATE,
                              PN_RESP OUT NUMBER,
                              PV_ERROR OUT VARCHAR2)IS
                                           
      CURSOR C_TABLAS(CV_TABLA VARCHAR2) IS
        SELECT COUNT(*)
        FROM ALL_TABLES
        WHERE TABLE_NAME=UPPER(CV_TABLA);
        
      CURSOR C_INDICES(CV_TABLA VARCHAR2) IS
        SELECT count(*) 
        FROM ALL_INDEXES R 
        WHERE TABLE_NAME = CV_TABLA;
    
        
LV_TABLA           VARCHAR2(50):='CO_REPCARCLI_';
LV_DATOS_TABLA     VARCHAR2(50);
--LV_DATOS_INDICES   VARCHAR2(100);
LN_EXISTE_TA       NUMBER:=0;
LN_EXISTE_IN       NUMBER:=0;
LV_FECHA           VARCHAR2(20);


begin
    LV_FECHA := TO_CHAR(PN_FECHA,'DDMMYYYY');
    LV_DATOS_TABLA := LV_TABLA||LV_FECHA;
/*LV_DATOS_INDICES := '''I_COFACT_'||LV_FECHA||''','||
                      '''I_COFACT2_'||LV_FECHA||''','||
                      '''I_COFACT3_'||LV_FECHA||'''';  */
  
       OPEN C_TABLAS(LV_DATOS_TABLA);
       FETCH C_TABLAS INTO LN_EXISTE_TA;
       CLOSE C_TABLAS;
       
       OPEN C_INDICES(LV_DATOS_TABLA);
       FETCH C_INDICES INTO LN_EXISTE_IN;
       CLOSE C_INDICES;
           
        IF (LN_EXISTE_TA > 0) THEN
            IF (LN_EXISTE_IN > 0) THEN
                PN_RESP:=1;
            ELSE
                PN_RESP:=0;
            END IF;
        ELSE
          PN_RESP:=0;      
        END IF;      
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);

END PR_VALIDA_REPCARCLI;

PROCEDURE PR_VALIDA_CO_REPCAP(PN_FECHA IN DATE,
                              PN_RESP  OUT NUMBER,
                              PV_ERROR OUT VARCHAR2)IS
                                           
      CURSOR C_TABLAS(CV_TABLA VARCHAR2) IS
        SELECT COUNT(*)
        FROM ALL_TABLES
        WHERE TABLE_NAME=UPPER(CV_TABLA);
        
      CURSOR C_INDICES(CV_TABLA VARCHAR2) IS
        SELECT count(*) 
        FROM ALL_INDEXES R 
        WHERE TABLE_NAME = CV_TABLA;
    
        
LV_TABLA           VARCHAR2(50):='CO_REPCAP_';
LV_DATOS_TABLA     VARCHAR2(50);
--LV_DATOS_INDICES   VARCHAR2(100);
LN_EXISTE_TA       NUMBER:=0;
LN_EXISTE_IN       NUMBER:=0;
LV_FECHA           VARCHAR2(20);


begin
    LV_FECHA := TO_CHAR(PN_FECHA,'DDMMYYYY');
    LV_DATOS_TABLA := LV_TABLA||LV_FECHA;
/*    LV_DATOS_INDICES := '''I_COFACT_'||LV_FECHA||''','||
                        '''I_COFACT2_'||LV_FECHA||''','||
                        '''I_COFACT3_'||LV_FECHA||'''';*/
  
       OPEN C_TABLAS(LV_DATOS_TABLA);
       FETCH C_TABLAS INTO LN_EXISTE_TA;
       CLOSE C_TABLAS;
       
       OPEN C_INDICES(LV_DATOS_TABLA);
       FETCH C_INDICES INTO LN_EXISTE_IN;
       CLOSE C_INDICES;
           
        IF (LN_EXISTE_TA > 0) THEN
            IF (LN_EXISTE_IN > 0) THEN
                PN_RESP:=1;
            ELSE
                PN_RESP:=0;
            END IF;
        ELSE
           PN_RESP:=0;        
        END IF;
               
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);

END PR_VALIDA_CO_REPCAP;

 PROCEDURE PR_CAPAS_ALARMA( PN_IDENTIFICADOR IN NUMBER,
                            PV_ERROR       OUT VARCHAR2) IS
  
    CURSOR C_DESCRIPCION(CN_IDENTIFICADOR NUMBER) IS
      SELECT B.FROM_NAME, B.TO_NAME, B.CC, B.CCO, A.SUBJECT, A.DESCRIPCION
      FROM RPC_CAPAS_ALARMA A, RPC_CAPAS_PARAMETROS_ALARMA B
      WHERE A.ID_PARAMETRO = B.ID_PARAMETRO
      AND A.ID_ALARMA = CN_IDENTIFICADOR
      and a.estado = 'A'
      and b.estado = 'A';
  
      LB_BOOLEAN     BOOLEAN;
      LC_DESCRIPCION C_DESCRIPCION%ROWTYPE;
BEGIN
  
    OPEN C_DESCRIPCION(PN_IDENTIFICADOR);
    FETCH C_DESCRIPCION INTO LC_DESCRIPCION;
    LB_BOOLEAN := C_DESCRIPCION%FOUND;
    CLOSE C_DESCRIPCION;
  
    IF LB_BOOLEAN = TRUE THEN
           PORTA.SEND_MAIL.MAIL@AXIS(FROM_NAME => LC_DESCRIPCION.FROM_NAME,
                                     TO_NAME   => LC_DESCRIPCION.TO_NAME,
                                     CC        => LC_DESCRIPCION.CC,
                                     CCO       => LC_DESCRIPCION.CCO,
                                     SUBJECT   => LC_DESCRIPCION.SUBJECT,
                                     MESSAGE   => LC_DESCRIPCION.DESCRIPCION);
    ELSE
     PV_ERROR := 'EL CURSOR NO DEVOLVIO NADA';

    END IF;
  
EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR(SQLERRM, 1, 500);
    
END PR_CAPAS_ALARMA;

 PROCEDURE PR_BALANCEA_REPORTE_CAPAS(PN_PARAMETRO IN VARCHAR2,
                                     PN_BANDERA in varchar2,
                                     PV_ERROR   OUT VARCHAR2) IS
  CURSOR C_PARAMETRO(CV_PARAMETRO varchar2) IS
    select a.hilo
      from rpc_capas_parametros A
     where A.ESTADO = 'A'
       AND A.codigo = CV_PARAMETRO;
  ln_hilo        NUMBER := 0;
  ln_hilo_tmp    NUMBER := 1;
  ln_registro    NUMBER := 0;
  ln_reg_hilo    NUMBER := 0;
  LV_QUERY_DATOS varchar2(1000);
  LV_QUERY_REG   varchar2(1000);
  TYPE c_datos IS REF CURSOR;
  rc_datos    c_datos;
  id_registro varchar2(100);
  Lv_error    varchar2(100);
  Lv_tabla    VARCHAR2(100);
BEGIN
  open C_PARAMETRO(PN_PARAMETRO);
  fetch C_PARAMETRO
    into ln_hilo;
  close C_PARAMETRO;
  
  if PN_BANDERA = 2 then
    Lv_tabla       := 'CO_PERIODOS_REPCAR';
    LV_QUERY_DATOS := 'select rowid from CO_PERIODOS_REPCAR WHERE REVISADO IS NULL';
    LV_QUERY_REG   := 'select COUNT(*) from CO_PERIODOS_REPCAR WHERE REVISADO IS NULL';
  elsif PN_BANDERA = 4 then
    Lv_tabla       := 'RPC_PERIODOS_VAL_FACT';
    LV_QUERY_DATOS := 'select rowid from RPC_PERIODOS_VAL_FACT WHERE REVISADO IS NULL';
    LV_QUERY_REG   := 'select COUNT(*) from RPC_PERIODOS_VAL_FACT WHERE REVISADO IS NULL';
  elsif PN_BANDERA = 7 then
    Lv_tabla        := 'CO_CLIENTES';
    LV_QUERY_DATOS := 'select rowid from CO_CLIENTES WHERE CREDITOS IS NULL';
    LV_QUERY_REG   := 'select COUNT(*) from CO_CLIENTES WHERE CREDITOS IS NULL';
  elsif PN_BANDERA = 9 then
    Lv_tabla       := 'CO_PERIODOS_FACT'; 
    LV_QUERY_DATOS := 'select rowid from CO_PERIODOS_FACT WHERE REVISADO IS NULL';
    LV_QUERY_REG   := 'select COUNT(*) from CO_PERIODOS_FACT WHERE REVISADO IS NULL';
  end if;
  
  execute immediate LV_QUERY_REG
    into ln_registro;
  ln_reg_hilo := round(ln_registro / ln_hilo) + 1;
  open rc_datos for LV_QUERY_DATOS;
  loop
    id_registro := null;
    fetch rc_datos
      into id_registro;
    exit when rc_datos%notfound;
  
    cok_reportes_capas.pr_balancea_capas(pn_hilo  => ln_hilo_tmp,
                                         pv_tabla => Lv_tabla,
                                         pv_rowid => id_registro,
                                         pv_error => Lv_error);
    -- IF Lv_error IS NOT NULL THEN
       PV_ERROR := Lv_error;
     --END IF
     
    if ln_hilo = ln_hilo_tmp then
      ln_hilo_tmp := 1;
    else
      ln_hilo_tmp := ln_hilo_tmp + 1;
    end if;
    if (ln_hilo_tmp mod ln_reg_hilo = 0) then
      commit;
    end if;
  
  end loop;
  commit;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);
END PR_BALANCEA_REPORTE_CAPAS;

PROCEDURE PR_BALANCEA_REPORTE_CAPAS_MAIN(PN_PARAMETRO IN VARCHAR2,
                                     PN_BANDERA in varchar2,
                                     PV_ERROR   OUT VARCHAR2) IS
  CURSOR C_PARAMETRO(CV_PARAMETRO varchar2) IS
    select a.hilo
      from rpc_capas_parametros A
     where A.ESTADO = 'A'
       AND A.codigo = CV_PARAMETRO;
  ln_hilo        NUMBER := 0;
  ln_hilo_tmp    NUMBER := 1;
  ln_registro    NUMBER := 0;
  ln_reg_hilo    NUMBER := 0;
  LV_QUERY_DATOS varchar2(1000);
  LV_QUERY_REG   varchar2(1000);
  TYPE c_datos IS REF CURSOR;
  rc_datos    c_datos;
  id_registro varchar2(100);
  Lv_error    varchar2(100);
  Lv_tabla    VARCHAR2(100);
BEGIN
  open C_PARAMETRO(PN_PARAMETRO);
  fetch C_PARAMETRO
    into ln_hilo;
  close C_PARAMETRO;
  
  if PN_BANDERA = 2 then
    Lv_tabla       := 'CO_BALANCEO_CREDITOS_MAIN';
    LV_QUERY_DATOS := 'select rowid from CO_BALANCEO_CREDITOS_MAIN WHERE REVISADO IS NULL';
    LV_QUERY_REG   := 'select COUNT(*) from CO_BALANCEO_CREDITOS_MAIN WHERE REVISADO IS NULL';
  
  end if;
  
  execute immediate LV_QUERY_REG
    into ln_registro;
  ln_reg_hilo := round(ln_registro / ln_hilo) + 1;
  open rc_datos for LV_QUERY_DATOS;
  loop
    id_registro := null;
    fetch rc_datos
      into id_registro;
    exit when rc_datos%notfound;
  
    cok_reportes_capas.pr_balancea_capas(pn_hilo  => ln_hilo_tmp,
                                         pv_tabla => Lv_tabla,
                                         pv_rowid => id_registro,
                                         pv_error => Lv_error);
    -- IF Lv_error IS NOT NULL THEN
       PV_ERROR := Lv_error;
     --END IF
     
    if ln_hilo = ln_hilo_tmp then
      ln_hilo_tmp := 1;
    else
      ln_hilo_tmp := ln_hilo_tmp + 1;
    end if;
    if (ln_hilo_tmp mod ln_reg_hilo = 0) then
      commit;
    end if;
  
  end loop;
  commit;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := SUBSTR(SQLERRM, 1, 500);
END PR_BALANCEA_REPORTE_CAPAS_MAIN;


PROCEDURE PR_INSERTA_VALIDA_PORCENTAJE(PD_FECHA_INI IN DATE,
                                       PD_FECHA_FIN IN DATE,
                                       PV_ERROR     OUT VARCHAR2) IS      
                                                                                                          
CURSOR PERIODOS(CD_FECHA_FIN DATE) IS
SELECT  DISTINCT LRSTART
FROM BCH_HISTORY_TABLE         
WHERE LRSTART <= CD_FECHA_FIN  
AND LRSTART >= PD_FECHA_INI   
AND TO_CHAR(LRSTART, 'DD') = TO_CHAR(PD_FECHA_FIN,'DD')                       
ORDER BY LRSTART DESC; 

LT_PORCENTAJE   COT_NUMBER_POR  := COT_NUMBER_POR();
LT_REGION       COT_VARCHAR_PO  := COT_VARCHAR_PO();
LVSENTENCIA     VARCHAR2(1000);
LN_RESP         NUMBER;
LV_ERROR        VARCHAR2(500);
LE_EXCEPCION    EXCEPTION;

BEGIN
  


FOR I IN PERIODOS(PD_FECHA_FIN) LOOP
  
  COK_REPORTES_CAPAS.PR_VALIDA_CO_REPCAP(PN_FECHA => I.LRSTART,
                                         PN_RESP =>  LN_RESP,
                                         PV_ERROR => LV_ERROR);
  IF PV_ERROR IS NOT NULL THEN
    RAISE LE_EXCEPCION;
  END IF;
  
   IF LN_RESP > 0 THEN
LVSENTENCIA := 'BEGIN' || 
                ' SELECT REGION, PORC_RECUPERADO 
                     BULK COLLECT INTO :1, :2' ||
                     ' FROM CO_REPCAP_' ||
                     TO_CHAR(I.LRSTART, 'DDMMYYYY') ||
                     ' WHERE DIAS = (SELECT MAX(X.DIAS) FROM CO_REPCAP_'||
                      TO_CHAR(I.LRSTART, 'DDMMYYYY') ||' X'||') ;
                     END;';
                     
EXECUTE IMMEDIATE LVSENTENCIA
            USING OUT LT_REGION, OUT LT_PORCENTAJE;

      IF LT_PORCENTAJE.COUNT > 0 THEN
        FORALL IND IN LT_PORCENTAJE.FIRST .. LT_PORCENTAJE.LAST
          INSERT 
          INTO RPC_CAPAS_VALIDA_POR(FECHA_CORTE, 
                                    PERIODOS, 
                                    REGIONES, 
                                    USUARIO_INGRESO, 
                                    FECHA_INGRESO,
                                    POR_RECUPERADO)
          VALUES(PD_FECHA_FIN,
                 I.LRSTART,
                 LT_REGION(IND),
                 USER,
                 SYSDATE,
                 LT_PORCENTAJE(IND));
          COMMIT;        
      END IF;
    
END IF;  

END LOOP;
EXCEPTION
    WHEN LE_EXCEPCION THEN
    PV_ERROR := 'ERROR EN "COK_REPORTES_CAPAS.PR_VALIDA_CO_REPCAP" ' || LV_ERROR;
   
    WHEN OTHERS THEN
    PV_ERROR :='ERROR EN "COK_REPORTES_CAPAS.PR_INSERTA_VALIDA_PORCENTAJE" ' || SUBSTR(SQLERRM,1,500);
   
END;


PROCEDURE PR_BALANCEA_CAPAS(PN_HILO    IN NUMBER,
                            PV_TABLA   IN VARCHAR2,
                            PV_ROWID   IN VARCHAR2,
                            PV_ERROR   OUT VARCHAR2) IS

   LV_UPDATE VARCHAR2(1000);                   
BEGIN
  
  LV_UPDATE := 'UPDATE '|| PV_TABLA ||
               ' SET HILO = '|| PN_HILO ||
               ' WHERE ROWID =  ' ||''''||PV_ROWID||'''';             
  
  execute immediate LV_UPDATE;
  
  EXCEPTION
  WHEN OTHERS THEN
  PV_ERROR := SUBSTR(SQLERRM, 1, 500);
    
 END PR_BALANCEA_CAPAS;

 END COK_REPORTES_CAPAS;
/
