CREATE OR REPLACE PACKAGE Dunning
/*
**    @(#) but_bscs/bscs/database/scripts/oraproc/packages/dunning.sph, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/4, 24/04/02
**
**    References:
**    HLD ar/208315_ar_1 Combine separate dunning procedures
**                       to one dunning package
**
**    Author: Annegret Rauscher, LH Specifications GmbH
**
**    Filenames: dunning.sph - Package header
**               dunning.spb - Package body
**
**    Major Modifications (when, who, what)
**    04/02 - Horst Szillat
**            New function PrintBodyVersion added
**
**/
IS

   /**************************************************************************
   *        P R O C E D U R E  prepare_dunning                               *
   *                                                                         *
   * Description:                                                            *
   *   This procedure increases the dunning step of the documents to be      *
   *   dunned.                                                               *
   *                                                                         *
   * Parameters:                                                             *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   /**************************************************************************/
   PROCEDURE Prepare_Dunning
   (
       -- processing result
       success OUT NUMBER
   );

   /**************************************************************************
   *        P R O C E D U R E  do_dunning                                    *
   *                                                                         *
   * Description:                                                            *
   *   This procedure calculates dunning and late fees and creates           *
   *   dunning entries in table tmpcollection.                               *
   *                                                                         *
   * Parameters:                                                             *
   *   step        IN   NUMBER                                               *
   *                    Dunning step to be processed                         *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   /**************************************************************************/
   PROCEDURE Do_Dunning
   (
       -- dunning step
       step        NUMBER,

       -- processing result
       success OUT NUMBER
   );

   /**************************************************************************
   *        P R O C E D U R E  transfer_dunning                              *
   *                                                                         *
   * Description:                                                            *
   *   This procedure transfers flagged tmpcollection entries into table     *
   *   cashcollection. Additionally, it creates fees and ticklers if         *
   *   required.                                                             *
   *                                                                         *
   * Parameters:                                                             *
   *   user_name   IN   VARCHAR2                                             *
   *                    Name of the user who started the procedure           *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   /**************************************************************************/
   PROCEDURE Transfer_Dunning
   (
       -- user name
       user_name   VARCHAR2,

       -- processing result
       success OUT NUMBER
   );

   /**************************************************************************
   *        F U N C T I O N  PrintBodyVersion                                *
   *                                                                         *
   * Description:                                                            *
   *   This function returns version control information.                    *
   *                                                                         *
   * Parameters:                                                             *
   *   none                                                                  *
   *                                                                         *
   /**************************************************************************/
   FUNCTION PrintBodyVersion RETURN VARCHAR2;

END Dunning;
/
CREATE OR REPLACE PACKAGE BODY Dunning
/*
**    @(#) but_bscs/bscs/database/scripts/oraproc/packages/dunning.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/3, 24/04/02
**
**    References:
**    HLD ar/208315_ar_1 Combine separate dunning procedures
**                       to one dunning package
**
**    Author: Annegret Rauscher, LH Specifications GmbH
**
**    Filenames: dunning.sph - Package header
**               dunning.spb - Package body
**
**    Major Modifications (when, who, what)
**    04/02 - Horst Szillat
**            New function PrintBodyVersion added
**
**/
IS

   /**************************************************************************
   *        P R O C E D U R E  prepare_dunning                               *
   *                                                                         *
   * Description:                                                            *
   *   This procedure marks all ORDERHDR_ALL entries which are overdue and   *
   *   are ready to be dunned within a certain dunning step. The marking     *
   *   may be understood as a proposal for dunning.                          *
   *                                                                         *
   *   Marking will be done in the field OHDUNSTEP ofthe ORDERHDR_ALL entry. *
   *   This field contains the number of the dunning step.                   *
   *                                                                         *
   *   The following conditions must be given, if an ORDERHDR_ALL document   *
   *   shall be marked for a certain dunning step:                           *
   *   a) The document must be of type (OHSTATUS) 'IN' or 'FC'.              *
   *   b) The document must be overdue (OHDUEDATE is smaller than SYSDATE).  *
   *   c) The docuemnt hasn't been dunned yet (OHDUNSTEP is NULL)            *
   *      or the document has been selected for dunning before and the       *
   *      corresponding dunning letter has already been printed              *
   *      (an entry exists in table CASHCOLLECTION with this COLL_NO).       *
   *                                                                         *
   *   In this case, the OHDUNSTEP will be set up by one and the document    *
   *   is ready for the next dunning step.                                   *
   *   The flagging will be terminated for a document, if it has reached     *
   *   the collection step. This means the entry in OHDUNSTEP is higher than *
   *   the maximal possible dunning step as stated in APPMODULES             *
   *   feature code 1/2.                                                     *
   *                                                                         *
   *   Note: The whole process can work properly, if a flagged document,     *
   *   for which a full payment has been applied, will be unflagged by       *
   *   Accounts Receivable and Payment Input Handler.                        *
   *                                                                         *
   * Parameters:                                                             *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   * Author:                                                                 *
   *   S.Wunderlich/M.Kunz  Datum: 07.02.96     Version: 1.0                 *
   * Firma:                                                                  *
   *   LHS Specifications                                                    *
   *                                                                         *
   * Major Modifications (when, who, what)                                   *
   *   02/96 - SWU/MKU - initial version                                     *
   *   05/96 - JWE - use stored procedure for ticklers (HLD 04161_07)        *
   *                                                                         *
   /**************************************************************************/

   PROCEDURE Prepare_Dunning
   (
       success OUT NUMBER           -- processing result
   )
   IS

      -- Cursor to select all ORDERHDR_ALL entries which are due for dunning
      CURSOR c1 IS
         SELECT ohxact, ohdunstep
           FROM ORDERHDR_ALL ord
          WHERE     ord.ohduedate < SYSDATE
                AND ord.ohstatus IN ('IN', 'FC')
                AND ord.ohopnamt_doc > 0
                AND NVL(ord.complaint, 'Y') != 'X'
                AND ( ord.ohdunstep IS NULL OR
                      ord.ohdunstep =
                         (SELECT MAX(ccol.coll_no)
                            FROM CASHCOLLECTION ccol
                           WHERE ord.ohxact = ccol.ohxact AND
                                 NVL(ccol.printed, 'X') = 'Y'))
          FOR UPDATE;

      -- Maximum dunning step
      maxstep NUMBER;

   BEGIN

      -- Get maximal possible dunning step to determine the collection step
      SELECT TO_NUMBER(SUBSTR(params,2,2))
        INTO maxstep
        FROM APP_MODULES
       WHERE modulename = 'DB';

      -- Flag the ORDERHDRS with the dunning step, for which they are ready
      FOR dunord IN c1 LOOP
         IF dunord.ohdunstep IS NULL THEN
            dunord.ohdunstep := 1;
         ELSE
            dunord.ohdunstep := dunord.ohdunstep +1;
         END IF;

         IF dunord.ohdunstep <= maxstep + 1 THEN
            UPDATE ORDERHDR_ALL
               SET ohdunstep = dunord.ohdunstep
             WHERE CURRENT OF c1;
         END IF;
      END LOOP;

      COMMIT;

      success := 1;

      -- Handle exceptions
   EXCEPTION
      WHEN OTHERS THEN
           RAISE_APPLICATION_ERROR(-20000,SQLERRM);
           success := 0;

   END Prepare_Dunning;


   /**************************************************************************
   *        P R O C E D U R E  do_dunning                                    *
   *                                                                         *
   * Description:                                                            *
   *   This procedure serves to transfer the data given by the flagged       *
   *   ORDERDR_ALL entries into table TMPCOLLECTION. Prerequisite is,        *
   *   that the proc. prepare_dunning has been run.                          *
   *   It retrieves customer and tarif model data, calculates late and       *
   *   dunning fees, retrieves the appropriate income accounts and           *
   *   jobcost information and saves the entries into table TMPCOLLECTION.   *
   *                                                                         *
   * Parameters:                                                             *
   *   step        IN   NUMBER                                               *
   *                    Dunning step to be processed                         *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   * Author:                                                                 *
   *   S.Wunderlich/M.Kunz  Datum: 07.02.96     Version: 1.0                 *
   * Firma:                                                                  *
   *   LHS Specifications                                                    *
   *                                                                         *
   * Major Modifications (when, who, what)                                   *
   * 02/96 - SWU/MKU - initial version                                       *
   * 06/96 - RKL - correct fee inserts because of tmpcollection (HLD 10382)  *
   * 08/96 - AB  - Handling of column INCCODE in tmpcollectio added          *
   * 08/98 - SCA - Defect 44469                                              *
   * 09/99 - AF  - EURO Feature 52818                                        *
   * 02/00 - AF  - Defect 60871 Additional column OHDUEDATE in TMPCOLLECTION *
   * 06/01 - AK  - Feature 201957 Remove all ccinccode related.              *
   * 06/01 - AR  - a) Feature 82643                                          *
   *                  Additional columns tmcode, vscode, fee_spcode,         *
   *                  fee_sncode, fee_evcode, late_spcode, late_sncode,      *
   *                  late_evcode in TMPCOLLECTION                           *
   *               b) No insert of rec_version anymore                       *
   *                  -> usage of column default 0                           *
   * 07/01 - AR  - Defect 204177/d                                           *
   *               Rounding according to specified precision of the currency *
   *               instead of hard-coded precision 2                         *
   * 08/01 - AR  - Defect 208090                                             *
   *               Initialisation of late fee and dunning fee variables      *
   *               (of all columns, not only of the new columns)             *
   * 08/01 - AR  - Defect 204177/d-1                                         *
   *               usage of the currently active currency version            *
   *                                                                         *
   /**************************************************************************/

   PROCEDURE Do_Dunning
   (
       step        NUMBER,         -- dunning step
       success OUT NUMBER          -- processing result
   )
   IS

      -- Declare variables
      tmc               MPULKTMB.tmcode%TYPE;
      evc               MPULKEXN.evcode%TYPE;
      dunfee            NUMBER;
      dunfee_curr       NUMBER;
      latefee           NUMBER;
      latefee_curr      NUMBER;
      /* Defect 204177/d */
      dunfeeroundprec   NUMBER;
      latefeeroundprec  NUMBER;
      /* End Defect 204177/d */
      absflg                  MPULKTMB.echind%TYPE;
      editfee                 MPULKTMB.amtind%TYPE;
      editlate                MPULKTMB.amtind%TYPE;
      feeglcode               MPULKTMB.accglcode%TYPE;
      feeglcode_disc          MPULKTMB.accglcode_disc%TYPE;
      feeglcode_mincom        MPULKTMB.accglcode_mincom%TYPE;
      feejcid                 MPULKTMB.accjcid%TYPE;
      feejcid_disc            MPULKTMB.accjcid_disc%TYPE;
      feejcid_mincom          MPULKTMB.accjcid_mincom%TYPE;
      lateglcode              MPULKTMB.accglcode%TYPE;
      lateglcode_disc         MPULKTMB.accglcode_disc%TYPE;
      lateglcode_mincom       MPULKTMB.accglcode_mincom%TYPE;
      latejcid                MPULKTMB.accjcid%TYPE;
      latejcid_disc           MPULKTMB.accjcid_disc%TYPE;
      latejcid_mincom         MPULKTMB.accjcid_mincom%TYPE;
      serv_catcode_fee_v      MPULKTMB.accserv_catcode%TYPE;
      serv_code_fee_v         MPULKTMB.accserv_code%TYPE;
      serv_type_fee_v         MPULKTMB.accserv_type%TYPE;
      serv_catcode_late_v     MPULKTMB.accserv_catcode%TYPE;
      serv_code_late_v        MPULKTMB.accserv_code%TYPE;
      serv_type_late_v        MPULKTMB.accserv_type%TYPE;

      /* Feature 82643 */
      vsc                     MPULKTMB.vscode%TYPE;
      feespcode               MPULKTMB.spcode%TYPE;
      feesncode               MPULKTMB.sncode%TYPE;
      latespcode              MPULKTMB.spcode%TYPE;
      latesncode              MPULKTMB.sncode%TYPE;
      fee_evc                 MPULKEXN.evcode%TYPE;
      late_evc                MPULKEXN.evcode%TYPE;
      /* End Feature 82643 */

      days            NUMBER;
      custlang        NUMBER;
      --custinccode     ccontact.ccinccode%TYPE;
      feefreq         VARCHAR2(1);
      latefreq        VARCHAR2(1);
      calc_flag       VARCHAR2(1);
      cust_name       VARCHAR2(40);
      str_ohref       ORDERHDR_ALL.ohrefnum%TYPE;
      /* Defect 44469 */
      counter         NUMBER;

      /* Meaning of the variables
      tmc             customer's traiffmodel
      evc             eventcode for late,dunning, and collection-fees
      dunfee          calculated dunning fee
      dunfee_curr     currency of dunning fee
      latefee         calculated late fee
      latefee_curr    currency of late fee
      absflg          calculation based on relative or
                      absolute factor ('A' or 'R')
      editfee         dunning fee editable in AR or not ('C' or 'F')
      editlate        late fee editable in AR or not ('C' or 'F')
      feeglcode...    glcode of income/expense account (revenue, discount,
                      min.commitment) related to dunning fee
      feejcid...      jobcost code (according to GL account)
                      related to dunning fee
      lateglcode ...  glcode of income/expense account (revenue, discount,
                      min.commitment) related to late fee
      latejcid ...    jobcost code (according to GL account)
                      related to late fee
      days            number of days which is used to calculate the interest
                      (e.g. sysdate-ohduedate-entdate(last dunning))
      custlang        defines the customer's language
      latefreq        fee frequency for late fees
      feefreq         fee frequency for dunning fees
      calc_flag       determines whether interest and fee calculation
                      should be done or not (app_modules, fc. 7)
      cust_name       name of the customer
      str_ohref       just a aid string
      custinccode     flag, if City area is incorporated (US need)
      */


      /* Get ORDERHDR_ALL entries with the appropriate number in OHDUNSTEP
         for this dunning step.
         Method of payment may not be 'D' (Direct Debit), the customer may
         not be excepted from dunning ('X' in CUSTOMER_ALL.DUNNING_FLAG)
      */
      CURSOR c1 IS
         SELECT ord.ohxact, ord.ohdunstep, ord.customer_id,
                ord.ohopnamt_doc, ord.document_currency, ord.ohduedate,
                ord.ohrefnum, ord.ohrefdate,
                cudl.prgcode, cudl.custcode, cudl.tmcode,
                pay.paymentcode
           FROM CUSTOMER_ALL cudl,
                ORDERHDR_ALL ord,
                paymenttype_int pay,
                payment p
          WHERE ord.ohdunstep   = step
            AND NVL(ord.complaint,'Y') != 'X'
            AND p.customer_id   = cudl.customer_id
            AND p.customer_id   = ord.customer_id
            AND p.payment_type  = pay.payment_id
            AND p.act_used      = 'X'
            AND ord.customer_id = cudl.customer_id
            /*
            Defect 68825: Select all customers for dunning, even when not
                          active and cudl.cstype = 'a'
            End 68825
            */
            AND NVL(cudl.dunning_flag,'Y') != 'X'
            AND ord.ohopnamt_doc > 0
            AND ord.ohdunstep != (SELECT NVL(MAX(ccol.coll_no),0)
                                    FROM CASHCOLLECTION ccol
                                   WHERE ccol.ohxact = ord.ohxact);

      /* Getting the data of the tariffmodel of the customer related to the
         ORDERHDR.
         event           -> event fee
         echind          -> basis of calculation (absolute or relative)
         amtind          -> fee is editable or not in AR
         accjcid         -> jobcost_id for fee
         accglcode       -> income account for fee
         tmcode          -> code for tariffmodel
         evcode          -> eventcode in mpdevtab
         sncode          -> servive name code of the event driven
                            service for the fees
         vsdate          -> version date of the tariffmodel
         status          -> 'P' for version in production
         currency        -> rate plan currency of the tariffmodel
      */
      CURSOR c2 IS
         SELECT mpr.event, mpr.echind, mpr.amtind, mpr.frqind,
                mpr.accglcode, mpr.accglcode_disc, mpr.accglcode_mincom,
                mpr.accjcid, mpr.accjcid_disc, mpr.accjcid_mincom,
                mpr.accserv_catcode, mpr.accserv_code, accserv_type,
                mptab.currency
                /* Feature 82643 */
                , mpr.vscode, mpr.spcode, mpr.sncode
                /* End Feature 82643 */
           FROM MPULKTMB mpr,
                MPULKEXN mpe,
                MPUTMTAB mptab
          WHERE mpr.tmcode  = tmc
             AND mpe.evcode = evc
             AND mpr.sncode = mpe.sncode
             AND mpr.vsdate <= SYSDATE
             AND mpr.status = 'P'
             AND mpr.tmcode = mptab.tmcode
             AND mpr.vscode = mptab.vscode
        ORDER BY mpr.vsdate DESC;

      -- Maximum dunning step
      maxstep NUMBER;

   BEGIN

      /* Get the maximal possible dunning step from APP_MODULES, feature
         code 1/2. This is needed to determine ,whether parameter 'step'
         triggers a usual dunning or 'direct to collection'. With latter
         case the eventcode changes. Furthermore, fetch the calculation
         flag which determines whether fee and interest calc. are done
         or not. */

      SELECT TO_NUMBER(SUBSTR(PARAMS,2,2)), SUBSTR(PARAMS,8,1)
        INTO maxstep, calc_flag
        FROM APP_MODULES
       WHERE modulename = 'DB';

      -- Make sure a suitable content is provided
      IF calc_flag IS NULL THEN
         calc_flag := 'X';
      END IF;

      /* Defect 44469 */
      counter := 0;

      FOR dun IN c1 LOOP
      BEGIN
         /* Get default language and customer name. The awkward MAX...
            constructs are used to ensure that at least ONE row is found,
            otherwise a SQL-Error would occur! */
         /* SELECT NVL(MAX(cclanguage),0), MAX(cclname), MAX(ccinccode)
            INTO custlang, cust_name, custinccode */

         SELECT NVL(MAX(cclanguage),0), MAX(cclname)
           INTO custlang, cust_name
           FROM CCONTACT_ALL cc
          WHERE cc.customer_id = dun.customer_id
            AND cc.ccbill      = 'X';

         -- Calculate the number of days for the interest calculation
         SELECT SUM(NVL(due_days,0))
           INTO days
           FROM CASHCOLLECTION
          WHERE OHXACT = dun.ohxact;

         -- Make sure a suitable content is provided
         IF days IS NULL THEN
            days := 0;
         END IF;

         days := TO_NUMBER(TRUNC(SYSDATE)-TRUNC(dun.ohduedate)) - days;

         -- Calculation is only done when app_modules is parameterized accordingly
         IF calc_flag != '1' THEN

            -- initialize rate plan version number in case neither service for
            -- late fees nor service for dunning fees is set up and no record at all
            -- is retrieved
            tmc := dun.tmcode;
            vsc := NULL;

            --------------------------------------
            --       Calculate late fees        --
            --------------------------------------
            evc := 6;
            late_evc := 6;

            OPEN c2;

            FETCH c2
             INTO latefee, absflg, editlate, latefreq,
                  lateglcode, lateglcode_disc, lateglcode_mincom,
                  latejcid, latejcid_disc, latejcid_mincom,
                  serv_catcode_late_v, serv_code_late_v, serv_type_late_v,
                  latefee_curr,
                  /* Feature 82643 */
                  vsc, latespcode, latesncode;
                  /* End Feature 82643 */

            IF latefee IS NULL THEN
               latefee := 0;
            END IF;

            IF c2%NOTFOUND THEN
               -- no event driven service defined for this customer
               -- use document currency arbitrarily
               latefee := 0;
               latefee_curr := dun.document_currency;

               /* Defect 208090 */
               -- initialize late fee variables
               editlate          := NULL;
               latefreq          := NULL;
               lateglcode        := NULL;
               lateglcode_disc   := NULL;
               lateglcode_mincom := NULL;
               latejcid          := NULL;
               latejcid_disc     := NULL;
               latejcid_mincom   := NULL;
               serv_catcode_late_v := NULL;
               serv_code_late_v  := NULL;
               serv_type_late_v  := NULL;
               latefee_curr      := NULL;
               latespcode        := NULL;
               latesncode        := NULL;
               /* End Defect 208090 */

            ELSE

               IF absflg = 'A' THEN
                  -- absolute calculation basis days and latefee
                  -- use currency of the rate plan
                  latefee := days * latefee;
               ELSE
                  -- relative basis days and latefee (interest formula)
                  -- use document currency
                  latefee := (dun.ohopnamt_doc * days * latefee) / 360;
                  latefee_curr := dun.document_currency;
               END IF;

               /* Defect 204177/d */
               -- retrieve and use rounding precision of currency for late fee
               SELECT NVL(FCDECI,2)
                 INTO latefeeroundprec
                 FROM CURRENCY_VERSION
                WHERE FC_ID   = latefee_curr AND
                      version = (SELECT MAX(version)
                                   FROM CURRENCY_VERSION
                                  WHERE FC_ID       = latefee_curr AND
                                        valid_from <= SYSDATE);

               IF latefeeroundprec IS NULL THEN
                  latefeeroundprec := 2;
               END IF;

               latefee := ROUND(latefee, latefeeroundprec);
               /* End Defect 204177/d */

            END IF;

            CLOSE c2;

            --------------------------------------
            --      Calculate dunning fees      --
            --------------------------------------
            IF step > maxstep THEN
               -- Collection -> eventcode for collection must be used
               evc := 3;
            ELSE
               -- Dunning -> possible eventcodes are between 20 and 29.
               evc := 19 + step;
            END IF;

            fee_evc := evc;

            OPEN c2;

            FETCH c2
             INTO dunfee, absflg, editfee, feefreq,
                  feeglcode, feeglcode_disc, feeglcode_mincom,
                  feejcid, feejcid_disc, feejcid_mincom,
                  serv_catcode_fee_v, serv_code_fee_v, serv_type_fee_v,
                  dunfee_curr,
                  /* Feature 82643 */
                  vsc, feespcode, feesncode;
                  /* End Feature 82643 */

            IF dunfee IS NULL THEN
               dunfee := 0;
            END IF;

            IF c2%NOTFOUND THEN
               -- no event driven service defined for this customer
               -- use document currency arbitrarily
               dunfee := 0;
               dunfee_curr := dun.document_currency;
               /* Defect 208090 */
               -- initialize dunning fee variables
               editfee          := NULL;
               feefreq          := NULL;
               feeglcode        := NULL;
               feeglcode_disc   := NULL;
               feeglcode_mincom := NULL;
               feejcid          := NULL;
               feejcid_disc     := NULL;
               feejcid_mincom   := NULL;
               serv_catcode_fee_v := NULL;
               serv_code_fee_v  := NULL;
               serv_type_fee_v  := NULL;
               dunfee_curr      := NULL;
               feespcode        := NULL;
               feesncode        := NULL;
               /* End Defect 208090 */
            ELSE
               -- If basis is absolute use amount and currency of the rate plan
               -- therefore only relative calculation needs to be handled
               IF absflg = 'R' THEN
                  dunfee := dun.ohopnamt_doc * dunfee;
                  dunfee_curr := dun.document_currency;
               END IF;

               /* Defect 204177/d */
               -- retrieve and use rounding precision of currency for dunning fee
               SELECT NVL(FCDECI,2)
                 INTO dunfeeroundprec
                 FROM CURRENCY_VERSION
                WHERE FC_ID   = dunfee_curr AND
                      version = (SELECT MAX(version)
                                   FROM CURRENCY_VERSION
                                  WHERE FC_ID       = dunfee_curr AND
                                        valid_from <= SYSDATE);

               IF dunfeeroundprec IS NULL THEN
                  dunfeeroundprec := 2;
               END IF;
               dunfee  := ROUND(dunfee, dunfeeroundprec);
               /* End Defect 204177/d */

            END IF;

            CLOSE c2;

         END IF;

         -- Make sure a decent ohrefnum is stored
         IF dun.ohrefnum IS NULL THEN
            str_ohref := SUBSTR(TO_CHAR(dun.ohxact),1,10);
         ELSE
            str_ohref := dun.ohrefnum;
         END IF;

         -- Write tmpcollection
         INSERT INTO TMPCOLLECTION (
                ohxact, customer_id, coll_no, entdate, due_days,
                Fee, fee_curr, edit_fee, feefreq,
                fee_glcode, fee_glcode_disc, fee_glcode_mincom,
                fee_jcid, fee_jobcost_id_disc, fee_jobcost_id_mincom,
                interest, interest_curr, edit_late, latefreq,
                late_glcode, late_glcode_disc, late_glcode_mincom,
                late_jcid, late_jobcost_id_disc, late_jobcost_id_mincom,
                ohrefnum, ohrefdate, ohduedate,
                ohopnamt,ohopnamt_curr,
                custcode,custname,language_id,
                --inccode,paymnttype,prgcode,
                paymnttype,prgcode,
                servcat_code, serv_code, serv_type,
                servcat_code_late, serv_code_late, serv_type_late,
                -- rec_version,
                /* Feature 82643 */
                tmcode, vscode, fee_spcode, fee_sncode, fee_evcode,
                late_spcode, late_sncode, late_evcode
                /* End Feature 82643 */
                )
          VALUES (
                dun.ohxact, dun.customer_id, step, SYSDATE, days,
                /* Defect 204177/d */
                -- ROUND(dunfee,2), dunfee_curr, editfee, feefreq,
                dunfee, dunfee_curr, editfee, feefreq,
                /* End Defect 204177/d */
                feeglcode, feeglcode_disc, feeglcode_mincom,
                feejcid, feejcid_disc, feejcid_mincom,
                /* Defect 204177/d */
                -- ROUND(latefee,2), latefee_curr, editlate, latefreq,
                latefee, latefee_curr, editlate, latefreq,
                /* End Defect 204177/d */
                lateglcode, lateglcode_disc, lateglcode_mincom,
                latejcid, latejcid_disc, latejcid_mincom,
                str_ohref, dun.ohrefdate, dun.ohduedate,
                dun.ohopnamt_doc,dun.document_currency,
                dun.custcode,cust_name,custlang,
                --custinccode,dun.paymentcode,dun.prgcode,
                dun.paymentcode,dun.prgcode,
                serv_catcode_fee_v, serv_code_fee_v, serv_type_fee_v,
                serv_catcode_late_v, serv_code_late_v, serv_type_late_v,
                -- 1,
                /* Feature 82643 */
                tmc, vsc, feespcode, feesncode, fee_evc,
                latespcode, latesncode, late_evc
                /* End Feature 82643 */
                );

         /* If the row already exists update the row; this is necessary
            because the due_days may have changed */
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                 UPDATE TMPCOLLECTION
                    SET entdate  = SYSDATE,
                        due_days = days,
                        /* Defect 204177/d */
                        -- Fee                    = ROUND(dunfee,2),
                        Fee                    = dunfee,
                        /* End Defect 204177/d */
                        fee_curr               = dunfee_curr,
                        edit_fee               = editfee,
                        feefreq                = feefreq,
                        fee_glcode             = feeglcode,
                        fee_glcode_disc        = feeglcode_disc,
                        fee_glcode_mincom      = feeglcode_mincom,
                        fee_jcid               = feejcid,
                        fee_jobcost_id_disc    = feejcid_disc,
                        fee_jobcost_id_mincom  = feejcid_mincom,
                        /* Defect 204177/d */
                        -- interest               = ROUND(latefee,2),
                        interest               = latefee,
                        /* End Defect 204177/d */
                        interest_curr          = latefee_curr,
                        edit_late              = editlate,
                        latefreq               = latefreq,
                        late_glcode            = lateglcode,
                        late_glcode_disc       = lateglcode_disc,
                        late_glcode_mincom     = lateglcode_mincom,
                        late_jcid              = latejcid,
                        late_jobcost_id_disc   = latejcid_disc,
                        late_jobcost_id_mincom = latejcid_mincom,
                        ohduedate              = dun.ohduedate,
                        ohopnamt               = dun.ohopnamt_doc,
                        ohopnamt_curr          = dun.document_currency,
                        custcode               = dun.custcode,
                        custname               = cust_name,
                        language_id            = custlang,
                        --inccode                = custinccode,
                        paymnttype             = dun.paymentcode,
                        prgcode                = dun.prgcode,
                        servcat_code           = serv_catcode_fee_v,
                        serv_code              = serv_code_fee_v,
                        serv_type              = serv_type_fee_v,
                        servcat_code_late      = serv_catcode_late_v,
                        serv_code_late         = serv_code_late_v,
                        serv_type_late         = serv_type_late_v,
                        rec_version            = rec_version + 1,
                        /* Feature 82643 */
                        tmcode                 = tmc,
                        vscode                 = vsc,
                        fee_spcode             = feespcode,
                        fee_sncode             = feesncode,
                        fee_evcode             = fee_evc,
                        late_spcode            = latespcode,
                        late_sncode            = latesncode,
                        late_evcode            = late_evc
                        /* End Feature 82643 */
                  WHERE ohxact  = dun.ohxact
                    AND coll_no = step;

            -- Handle other exceptions
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(-20000,SQLERRM);
                 success := 0;

         END;

         /* Defect 44469 */
         /* Commit after every 500 transactions */
         counter := counter + 1 ;
         IF MOD (counter,500) = 0 THEN
            COMMIT;
         END IF;
         /* End Defect 44469 */

      END LOOP;

      COMMIT;
      success := 1;

   END Do_Dunning;


   /**************************************************************************
   *        P R O C E D U R E  transfer_dunning                              *
   *                                                                         *
   * Description:                                                            *
   *   This procedure transfers the flagged tmpcollection rows (trans_flag=X)*
   *   into the table cashcollection. Furthermore, ticklers are created      *
   *   (based on user_name). The corresponding fee information for late and  *
   *   dunning fees is transferred into the table fees. The remarks for fees *
   *   are translated according to the language flag (language_id).          *
   *                                                                         *
   * Parameters:                                                             *
   *   user_name   IN   VARCHAR2                                             *
   *                    Name of the user who started the procedure           *
   *   success     OUT  NUMBER                                               *
   *                    1: process finished without error                    *
   *                    0: process finished with an error                    *
   *                                                                         *
   * References:                                                             *
   *   HLD dab/04161_07 (use stored procedure for ticklers)                  *
   *   HLD db/10343_1 (remove lettertype support)                            *
   *   HLD 10382 (tax packages: geocode,servcat_code,serv_code,serv_type)    *
   *   HLD 31095 Configurable taxaction logic                                *
   *                                                                         *
   * Author:                                                                 *
   *   S.Wunderlich/M.Kunz  Datum: 07.02.96     Version: 1.0                 *
   * Firma:                                                                  *
   *   LHS Specifications                                                    *
   *                                                                         *
   * Major Modifications (when, who, what)                                   *
   * 02/96 - SWU/MKU - initial version                                       *
   * 05/96 - JWE - use stored procedure for ticklers (HLD 04161_07)          *
   *               remove field lettertype from insert (HLD 10343_4)         *
   * 06/96 - RKL - correct fee inserts because of tmpcollection (HLD 10382)  *
   * 07/96 - AB  - Entry of period for recurring/non-recurring fees          *
   * 08/96 - AB  - Handling of Entry INCCODE added                           *
   * 09/99 - AF  - EURO Feature 52818                                        *
   * 06/01 - AR  - a) Article String Feature 82643 (Release Dublin):         *
   *                  Usage of procedure fee.InsertFee instead of direct     *
   *                  insert into table Fees                                 *
   *                  -> parts commented out that are part of the procedure  *
   *                  -> determination of additional values that are         *
   *                     mandatory parameters for the procedure              *
   *               b) No insert of rec_version anymore                       *
   *                  -> usage of column default 0                           *
   *                                                                         *
   /**************************************************************************/

   PROCEDURE Transfer_Dunning
   (
       user_name   VARCHAR2,         -- user name,
       success OUT NUMBER            -- processing result
   )
   IS

      -- Select all marked rows in the tmpcollection
      /*
      ** defect 50391 HB Begin
      **
      ** CURSOR locTmpColl IS
      **    SELECT *
      **          FROM tmpcollection
      **         WHERE trans_flag = 'X' or trans_flag IS NULL;
      */
      CURSOR locTmpColl IS
         SELECT *
           FROM TMPCOLLECTION
          WHERE trans_flag = 'X';
      /*
      ** defect 50391 HB End
      */

      -- Declare variables
      def_lang      NUMBER;
      strtext1      NLS_DICT.TRANSLATION%TYPE;
      strtext2      NLS_DICT.TRANSLATION%TYPE;
      lang_id       NUMBER;
      oldlang_id    NUMBER;
      oldlang_id1   NUMBER;
      nls_exist     NUMBER;
      fee_remark    VARCHAR2(1000);
      calc_flag     VARCHAR2(1);
      maxstep       NUMBER;
      losLongDescription TICKLER_RECORDS.LONG_DESCRIPTION%TYPE;
      evc           MPULKEXN.evcode%TYPE;
      vsc           MPULKTMB.vscode%TYPE;
      spc           MPULKTMB.spcode%TYPE;
      snc           MPULKTMB.sncode%TYPE;
      seqno         FEES.SEQNO%TYPE := NULL;
      /* Feature 82643 (not necessary anymore)
      recurring     VARCHAR2(1);
      nrecurring    NUMBER;
      pricing_type  VARCHAR2(1);
      amount_net    NUMBER;
      amount_gross  NUMBER;
      seq_no        NUMBER;
      */

      /*    Explanation of the variables:
       def_lang:     default language id, used for translation,
                     if ccontact_all contains no info about
                     customer's language
       strtext1:     contains remark for dunning-fee entry in fees
       strtext2:     contains remark for interest entry in fees
       lang_id:      language id of customer
       oldlang_id:   aid variable to reduce fetches on the language
                     table(for the interest section)
       oldlang_id1:  aid variable to reduce fetches on the language
                     table(for the dunning-fee section)
       nls_exist:    indicator, whether translation for customer's
                     language exists
       fee_remark:   input buffer for fee.remark
       calc_flag:    determines whether interest and fee calculation
                     should be done or not (app_modules, fc. 7)
       maxstep:      maximum dunning step
       losLongDescription: Text for Tickler
       recurring:    indicator, whether dunning-fee or interest-fee
                     is recurring or non-recurring
       nrecurring:   Number of recurrencies for fees
       pricing_type: determines whether net or gross pricing is used
       amount_net:   is inserted into table FEES when net pricing
       amount_gross: is inserted into table FEES when gross pricing
       seq_no:       MAX. Fee ENTRY per customer
      */

   BEGIN
      /* Fetch the calculation flag which determines whether fee and interest
         calc. are done or not. */
      SELECT SUBSTR(PARAMS,8,1)
        INTO calc_flag
        FROM APP_MODULES WHERE modulename = 'DB';

      -- Make sure a suitable content is provided
      IF calc_flag IS NULL THEN
         calc_flag := 'X';
      END IF;

      /* Feature 82643 (not necessary anymore, pricing type is
         determined by procedure InsertFee)
      --- defect 88808, use view BSCSPROJECT
      -- Get pricing flag
      SELECT PRICING_TYPE
        INTO pricing_type
        FROM BSCSPROJECT;
      */

      -- Select default language
      SELECT lng_id
        INTO def_lang
        FROM language_table
       WHERE lng_def = 'X';

      -- Initalize variables
      oldlang_id  := -1;
      oldlang_id1 := -1;

      FOR dun IN locTmpColl
      LOOP

         -- Fill table cashcollection
         -- Use Entdate because this date was used to determine the rate plan
         INSERT INTO CASHCOLLECTION (
                ohxact,
                coll_no,
                entdate,
                Fee,
                fee_curr,
                interest,
                interest_curr,
                due_days
                --,rec_version
                )
             VALUES (
                dun.ohxact,
                dun.coll_no,
                dun.entdate,
                dun.Fee,
                dun.fee_curr,
                dun.interest,
                dun.interest_curr,
                dun.due_days
                --,1
                );

         losLongDescription := user_name || ' ' ||
             TO_CHAR(SYSDATE,'DD-MON-YY') ||
             ' DunStep: ' || TO_CHAR(dun.coll_no) ||
             ' RefNum: '  || dun.ohrefnum ||
             ' OpnAmt: '  || TO_CHAR(dun.ohopnamt) ||
             ' Fee: '     || TO_CHAR(dun.Fee) ||
             ' Int: '     || TO_CHAR(dun.interest);

         Tickler.AddNewTickler(
            dun.customer_id,
            NULL,         -- Contract ID
            'NOTE',       -- The status of this tickler
            5,            -- The priority of the tickler
            user_name,    -- Follow up user
            'AR DUNNING', -- Short description
            losLongDescription);

         /*
         ** Calculation has been done when app_modules is parameterized accordingly
         */
         IF calc_flag != '1' THEN

            -- Insert dunning fees and translated remark into fees, if an account
            -- entry exists in the tariff-modell of the customer
            IF (dun.fee_glcode        IS NOT NULL) AND
               (dun.fee_glcode_disc   IS NOT NULL) AND
               (dun.fee_glcode_mincom IS NOT NULL) AND
               (dun.Fee != 0)
            THEN

               -- No language specified -> use default language
               IF dun.language_id = 0 THEN
                  dun.language_id := def_lang;
               END IF;

               /* If previous customer has the same language_id, no fetch
                  from the database is neccessary */
               IF oldlang_id != dun.language_id THEN
                  oldlang_id := dun.language_id;

                  -- Look whether translation is available
                  SELECT COUNT(*)
                    INTO nls_exist
                    FROM NLS_RELEASE
                   WHERE base_tab = 'TEXT_CONSTANTS' AND
                         tran_col = 'TEXT'           AND
                         lng_id   = dun.language_id  AND
                         release_flag = 'Y';

                  -- get remark text, either translated or not
                  IF nls_exist > 0 THEN
                     SELECT translation
                       INTO strtext1
                       FROM NLS_DICT
                      WHERE lng_id = dun.language_id
                        AND base_tab  = 'TEXT_CONSTANTS'
                        AND key_value = 'DB0000000000000009';
                  ELSE
                     SELECT text
                       INTO strtext1
                       FROM TEXT_CONSTANTS
                      WHERE id = 'DB0000000000000009';
                  END IF;

               END IF;

               fee_remark := TO_CHAR(dun.coll_no) || RTRIM(strtext1,'  ') ||
                             ' ' || dun.ohrefnum;

               -- Feature 82643 (not necessary anymore, sequence number is
               -- determined by procedure InsertFee)
               -- Get max seq. number per customer and insert fee.
               --SELECT /*+ PKFEES */ MAX(seqno)
               --  INTO seq_no
               --  FROM FEES
               -- WHERE customer_id = dun.customer_id;

               /* Feature 82643 (not necessary anymore, done by procedure)
               IF seq_no IS NULL THEN
                  seq_no := 1;
               ELSE
                  seq_no := seq_no + 1;
               END IF;

               -- Determine whether fees are recurring or not
               IF dun.feefreq = 'O' THEN
                  recurring  := 'N';
                  nrecurring := 1;
               ELSE
                  recurring  := 'R';
                  nrecurring := 1;
               END IF;

               -- Use Entdate because this date was used to determine the rate plan
               -- and fill the amount according to the pricing flag
               IF pricing_type = 'N' THEN
                  amount_net   := dun.Fee;
                  amount_gross := NULL;
               ELSE
                  amount_net   := NULL;
                  amount_gross := dun.Fee;
               END IF;

               INSERT INTO FEES(
                  customer_id, seqno, amount, amount_gross, currency,
                  remark, entdate, valid_from,
                  fee_type, period, username,
                  glcode, JOBCOST,
                  glcode_disc, jobcost_id_disc,
                  glcode_mincom, jobcost_id_mincom,
                  servcat_code, serv_code, serv_type,
                  inccode, rec_version, fee_class)
               VALUES (
                  dun.customer_id, seq_no,
                  amount_net, amount_gross, dun.fee_curr,
                  fee_remark, dun.entdate, SYSDATE,
                  recurring, nrecurring, user_name,
                  dun.fee_glcode, dun.fee_jcid,
                  dun.fee_glcode_disc, dun.fee_jobcost_id_disc,
                  dun.fee_glcode_mincom, dun.fee_jobcost_id_mincom,
                  dun.servcat_code, dun.serv_code, dun.serv_type,
                  dun.inccode, 1, 4);
               */

               -- call procedure to create dunning fee
               Fee.InsertFee(
                   4, 0, dun.customer_id, user_name, dun.Fee, dun.fee_curr,
                   fee_remark, SYSDATE, 1, NULL,
                   dun.fee_glcode, dun.fee_glcode_disc, dun.fee_glcode_mincom,
                   dun.fee_jcid, dun.fee_jobcost_id_disc, dun.fee_jobcost_id_mincom,
                   dun.servcat_code, dun.serv_code, dun.serv_type,
                   NULL, NULL, NULL, NULL,
                   dun.tmcode, dun.vscode, dun.fee_spcode, dun.fee_sncode,
                   dun.fee_evcode, seqno);

               /* End Feature 82643 */

            END IF;

            -- Insert interest into fees. (see comments above for dunning section)
            IF (dun.late_glcode        IS NOT NULL) AND
               (dun.late_glcode_disc   IS NOT NULL) AND
               (dun.late_glcode_mincom IS NOT NULL) AND
               (dun.interest != 0)
            THEN

               IF dun.language_id = 0 THEN
                  dun.language_id := def_lang;
               END IF;

               IF oldlang_id1 != dun.language_id THEN
                  oldlang_id1 := dun.language_id;

                  SELECT COUNT(*)
                    INTO nls_exist
                    FROM NLS_RELEASE
                   WHERE base_tab = 'TEXT_CONSTANTS' AND
                         tran_col = 'TEXT'           AND
                         lng_id   = dun.language_id  AND
                         release_flag = 'Y';

                  IF nls_exist > 0 THEN
                     SELECT translation
                       INTO strtext2
                       FROM NLS_DICT
                      WHERE lng_id    = dun.language_id
                        AND base_tab  = 'TEXT_CONSTANTS'
                        AND key_value = 'DB0000000000000008';
                  ELSE
                     SELECT text
                       INTO strtext2
                       FROM TEXT_CONSTANTS
                      WHERE id = 'DB0000000000000008';
                  END IF;

               END IF;

               fee_remark := TO_CHAR(dun.coll_no) || RTRIM(strtext2,'  ') ||
                             ' ' || dun.ohrefnum;

               -- Feature 82643 (not necessary anymore)
               --SELECT /*+ PKFEES */ MAX(seqno)
               --  INTO seq_no
               --  FROM FEES
               -- WHERE customer_id=dun.customer_id;

               /* Feature 82643 (not necessary anymore)
               IF seq_no IS NULL THEN
                  seq_no := 1;
               ELSE
                  seq_no := seq_no + 1;
               END IF;

               IF dun.latefreq='O' THEN
                  recurring  := 'N';
                  nrecurring := 1;
               ELSE
                  recurring  := 'R';
                  nrecurring := 1;
               END IF;

               -- Use Entdate because this date was used to determine the rate plan
               -- and fill the amount according to the pricing flag
               IF pricing_type = 'N' THEN
                  amount_net   := dun.interest;
                  amount_gross := NULL;
               ELSE
                  amount_net   := NULL;
                  amount_gross := dun.interest;
               END IF;

               INSERT INTO FEES(
                  customer_id, seqno,
                  amount, amount_gross, currency,
                  remark, entdate, valid_from,
                  fee_type, period, username,
                  glcode, JOBCOST,
                  glcode_disc, jobcost_id_disc,
                  glcode_mincom, jobcost_id_mincom,
                  servcat_code, serv_code, serv_type,
                  inccode, rec_version, fee_class)
               VALUES (
                  dun.customer_id, seq_no,
                  amount_net, amount_gross, dun.interest_curr,
                  fee_remark, dun.entdate, SYSDATE,
                  recurring, nrecurring, user_name,
                  dun.late_glcode, dun.late_jcid,
                  dun.late_glcode_disc, dun.late_jobcost_id_disc,
                  dun.late_glcode_mincom, dun.late_jobcost_id_mincom,
                  dun.servcat_code_late, dun.serv_code_late, dun.serv_type_late,
                  dun.inccode, 1, 4);
               */

               -- call procedure to create late fee
               Fee.InsertFee(
                   4, 0, dun.customer_id, user_name, dun.interest, dun.interest_curr,
                   fee_remark, SYSDATE, 1, NULL,
                   dun.late_glcode, dun.late_glcode_disc, dun.late_glcode_mincom,
                   dun.late_jcid, dun.late_jobcost_id_disc, dun.late_jobcost_id_mincom,
                   dun.servcat_code_late, dun.serv_code_late, dun.serv_type_late,
                   NULL, NULL, NULL, NULL,
                   dun.tmcode, dun.vscode, dun.late_spcode, dun.late_sncode,
                   dun.late_evcode, seqno);

               /* End Feature 82643 */

            END IF;

         END IF;

         -- Delete the corresponding entry in tmpcollection
         DELETE FROM TMPCOLLECTION
          WHERE ohxact  = dun.ohxact AND
                coll_no = dun.coll_no;

         COMMIT;

      END LOOP;

      success := 1;

      -- Handle exceptions
   EXCEPTION
      WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR(-20000,SQLERRM);
         success := 0;

   END Transfer_Dunning;


   /**************************************************************************
   *        F U N C T I O N  PrintBodyVersion                                *
   *                                                                         *
   * Description:                                                            *
   *   This function returns version control information.                    *
   *   The placeholders (/main/3, 24/04/02, but_bscs/bscs/database/scripts/oraproc/packages/dunning.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605) are replaced when extracting the     *
   *   package.                                                              *
   *                                                                         *
   * Parameters:                                                             *
   *   none                                                                  *
   *                                                                         *
   * Major Modifications (when, who, what)                                   *
   *                                                                         *
   /**************************************************************************/

   FUNCTION PrintBodyVersion
            RETURN VARCHAR2
   IS
   BEGIN
      RETURN '/main/3' || ' | ' || '24/04/02' || ' | ' || 'but_bscs/bscs/database/scripts/oraproc/packages/dunning.spb, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605';
   END PrintBodyVersion;


END Dunning;
/

