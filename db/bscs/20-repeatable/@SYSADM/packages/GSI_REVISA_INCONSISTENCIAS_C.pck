create or replace package GSI_REVISA_INCONSISTENCIAS_C is

  -- Author  : FMIRANDA
  -- Created : 01/04/2008 15:39:16
  -- Purpose : REVISAR INCONSISTENICAS ENTRE AXIS Y BSCS

  -- Public type declarations
  --type <TypeName> is <Datatype>;
  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;
  -- Public variable declarations
  --<VariableName> <Datatype>;
  
  --SUD CAC--
  --MEJORAS PROCEDIMIENTO DE BUSQUEDA DE INCONSISTENCIAS ENTRE AXIS Y BSCS 
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: Definición de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_id_bitacora_scp number:=0;
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='GSI_REVISA_INCONSISTENCIAS';
  lv_referencia_scp varchar2(100):='GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA';
  lv_unidad_registro_scp varchar2(30):='conciliacion';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
  ---------------------------------------------------------------
  ---fin Definición de variables---------------------------------


  -- Procedimiento busca las inconsistencias entre AXIS y BSCS y las coloca en una tabla 
  Procedure GSI_BUSCA_INCONSISTENCIAS(/*PV_CICLO       Varchar2,*/
                                      --PV_FECHA_CORTE IN OUT DATE,  --SUD CAC
                                      PV_FECHA_INICIO DATE,
                                      PV_FECHA_FIN DATE);

  -- Procedimiento que barre la tabla de inconsistenicas para procesar uno a uno los telefonos 
  Procedure GSI_REVISA_TABLA;

  -- Procedimiento que actualiza las fechas en BSCS deacuerdo a la de AXIS
  Procedure GSI_CONCILIA_BSCS(FECHA_AXIS  in DATE,
                              V_CO_ID     in number,
                              FECHA_BSCS  IN DATE,
                              TELFONO     IN varchar2,
                              ESTADO_BSCS IN VARCHAR2,
                              COMENTARIOS OUT VARCHAR2,
                              ESTADO      OUT CHAR);


end GSI_REVISA_INCONSISTENCIAS_C;
/
create or replace package body GSI_REVISA_INCONSISTENCIAS_C is

  -- Procedimiento busca las inconsistencias entre AXIS y BSCS y las coloca en una tabla
  Procedure GSI_BUSCA_INCONSISTENCIAS(/*PV_CICLO  Varchar2,*/
                                      --- PV_FECHA_CORTE IN OUT DATE, ---SUD CAC---
                                      PV_FECHA_INICIO DATE,---SUD CAC se agrega variable
                                      PV_FECHA_FIN DATE) is ---CAC se agrega variable

    ----Saca los ciclos respectivos a cada ciclo de cierre
    
   /* cursor CICLOS is
      select id_ciclo_admin
      from fa_ciclos_axis_bscs
      where id_ciclo = nvl(PV_CICLO,id_ciclo); */  ---SUD CAC MODIFICACION---
    
--- SUD CAC                               
---DETERMINAR SERVICIOS QUE TIENEN MOVIMIENTOS EN CL_NOVEDADES EN UN RANGO DE FECHAS
           CURSOR SERVICIO_NOVEDADES( LV_FECHA_INICIO VARCHAR2, 
                                      LV_FECHA_FIN VARCHAR2)IS
                                       
                       select distinct '593'||d.id_servicio AS SERVICIO
                       from   cl_novedades@AXIS d
                       where  d.fecha between TO_DATE(LV_FECHA_INICIO,'DD/MM/YYYY') 
                                          and TO_DATE(LV_FECHA_FIN,'DD/MM/YYYY')
                       and    d.id_subproducto in ('AUT','TAR')
                       and    d.estado     = 'A'
                       and id_servicio = '90305835';
                                            
       
------------------------
------------------------   
      
      ---Cursor se utiliza para verificar si hubo un cambio de PPT A TAR
      
           cursor c_verifica (v_tel varchar2, fecha_verf varchar2)is
                 
                 SELECT /*+rule+*/ CO_ID
                 FROM CL_servicios_contratados@AXIS 
                 WHERE OBSERVACION LIKE 'CAMBIO DE NUMERO%'
                 AND ID_SERVICIO = v_tel
                 and TRUNC(fecha_inicio) = to_date(fecha_verf,'dd/mm/yyyy')
                 order by fecha_inicio;
           
            LC_C_verifica   c_verifica%ROWTYPE;
            LB_FOUND_ver BOOLEAN;
            
      ---Cursor se utiliza para ver si en el rango de analisis se encuentran Estado 31 y 91      
            cursor s_verifica (s_tel varchar2, fecha_s varchar2) is
                  SELECT /*+rule+*/ VALOR, FECHA_DESDE, FECHA_HASTA
                  FROM CL_DETALLES_SERVICIOS@AXIS
                  WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'                  
                  AND ID_SERVICIO = s_tel
                  AND valor IN ('31','91','25')---valor de cambio o reposicion
                  AND FECHA_DESDE >= to_date(fecha_s,'dd/mm/yyyy hh24:mi:ss')
                  ORDER BY FECHA_DESDE;

            LC_s_verifica   s_verifica%ROWTYPE;
            LB_FOUND_s_ver BOOLEAN;   
         
    
           cursor TELEFONO_SERVICIO(LV_TEL_SERV VARCHAR2, ---UTILIZAR LA FUNCION DE OBTENER NUMERO
                                    LV_FECHA1   VARCHAR2, 
                                    LV_FECHA2   VARCHAR2) IS
                 SELECT /*+rule+*/VALOR,FECHA_DESDE,FECHA_HASTA
                 FROM CL_DETALLES_SERVICIOS@AXIS
                 WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'
                 /*AND ESTADO = 'A'*/---SUB CAC 
                 AND ID_SERVICIO = LV_TEL_SERV
                 AND valor IN ('23', '29', '30', '32', '92','26','27','80','34','36'/*,'33'*/)---SUD CAC
                 AND FECHA_DESDE >= TO_DATE(LV_FECHA1, 'DD/MM/YYYY')                 
                 AND FECHA_DESDE <= TO_DATE(LV_FECHA2, 'DD/MM/YYYY')
                 ORDER BY FECHA_DESDE;
             
         /*LC_SERVICIO   TELEFONO_SERVICIO%ROWTYPE;*/
         /*LB_FOUND_SERV BOOLEAN;*/ 
         
            --23  ACTIVO NUEVO
            --29  REACTIVADO CON CARGO
            --30  REACTIVADO SIN CARGO
            --32  ACTIVO / CAMBIO
            --92  ACTIV.CAMBIO AUTOCONTROL
            
            --26 T/C SUSPENDIDO ROBO
            --27 SUSP.COBR.AUTOCONTROL
            --34 TC SUSPENSION PARCIAL
            --36 T/C SUSPENDIDO USUARIO
            --80 SUSPENSION PRE AVANZADA
            --33 SUSPENDIDO POR COBRANZA AVANZADA ---EN BSCS NO SE REFLEJA
            
           
    
     ---determinar CO_ID Y EL PERIODO DEL CICLO QUE PERTENECE EL CICLO
         cursor TELEFONOS(LV_DN_NUM VARCHAR2)IS  
    
                SELECT /*+rule+*/ A.DN_NUM AS SERVICIO,b.CO_ID,E.BILLCYCLE AS ID_CICLO_ADMIN
                FROM 
                       directory_number a,
                       contr_services_cap b,
                       contract_all d,
                       customer_all e
                where E.CUSTOMER_ID = D.CUSTOMER_ID
                and d.co_id = b.co_id
                AND B.CS_DEACTIV_DATE IS NULL
                AND B.DN_ID = A.DN_ID
                AND A.DN_NUM =LV_DN_NUM;
  
         
         LC_TELEFONOS   TELEFONOS%ROWTYPE;
         LB_FOUND_TEL BOOLEAN;
         
    
    
        ---Se scan los datos del servicio EN BSCS
            CURSOR SERVICIOS(LV_CO_ID_BSCS NUMBER,lv_fecha_1 date,lv_fecha_2 date) IS
                SELECT /*+rule+*/ /*c.entry_date , c.sncode, C.STATUS,H.STATUS_HISTNO*/ DISTINCT C.ENTRY_DATE,C.STATUS
                  FROM pr_serv_status_hist c, profile_service h
                 WHERE h.co_id = LV_CO_ID_BSCS
                   AND h.co_id = c.co_id
                      --AND  c.sncode NOT IN (select cod_sncode from SNCODE_CONCILIA)
                   and c.sncode IN (/*5,*/ 2)---SUD CAC SE TRABAJA SOLO CON SNCODE 2
                   AND C.STATUS NOT IN ('O')
                  /* and h.status_histno = c.histno*/
                   and c.entry_date > lv_fecha_1
                   AND c.entry_date <= lv_fecha_2 
                   ORDER BY C.ENTRY_DATE;
                   
            /*LB_FOUND_SERV BOOLEAN;*/
     

       cursor REVISADOS(CO_ID NUMBER, MES VARCHAR2, CICLO VARCHAR2,LV_FECHA_AXIS DATE,LV_FECHA_BSCS DATE) IS
             select /*+rule+*/ *
             from RESPALDO_CONCILIACION
             WHERE CO_ID_BSCS = CO_ID
             AND CICLO = CICLO
             AND MES_CIERRE = MES
             AND FECHA_AXIS=LV_FECHA_AXIS
             AND FECHA_BSCS=LV_FECHA_BSCS;

      LC_REV       REVISADOS%ROWTYPE;
      LB_FOUND_REV BOOLEAN;

    --- VARIABLES PARA DETERMINAR LOS RANGOS DE FECHA DEL PERIODO
    /*
    --- SUD CAC ----------------
    LV_dia_ini      varchar2(2);
    LV_dia_fin      varchar2(2);
    LV_mes_ini      varchar2(2);
    LV_anio_ini     varchar2(4);
    LV_anio_fin     varchar2(4);
    LV_fecha_ini_ci varchar2(10);
    LV_fecha_fin_ci varchar2(10);
    ----------------------------
    */
    
    LV_mes_fin      varchar2(2);
    
    -- VARIABLES PARA VALIDAR EL COMMIT
    LN_CONTADOR NUMBER := 0;
    LN_VALIDA   NUMBER := 500;
    ---VARIABLES PARA LA TABLA DE RESPALDO
    LV_FECHA_REVISA VARCHAR2(10);

   
    
    
    ---SUD CAC  VARIABLES PARA SCP------------
     lvMensErr VARCHAR2(2000) := null;
     lv_valor           VARCHAR2(1);
     lv_descripcion     VARCHAR2(500);
    ------------------------------------------
    ---SUD CAC--------------------------------
    ESTADO_AXIS VARCHAR2(2):='';
    LN_CONTADOR_SERVICIO23 NUMBER:=0;
    
    TYPE TT_SERVICIOS IS TABLE OF RESPALDO_CONCILIACION.SERVICIO_AXIS%TYPE INDEX BY BINARY_INTEGER;
    VTT_SERVICIOS TT_SERVICIOS;
                 
    ------------------------------------------
    
    /*BANDERA NUMBER:=0;*/
    cont number:=0;
    FECHA_1 DATE;
    FECHA_2 date;
    
    CONT_VER1 number:=0;
    CONT_VER2 number:=0;
    
    TELELEFONO VARCHAR2(10):='';
    
    BANDERA_SALTA BOOLEAN:=FALSE;
  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_BUSCA_INCONSISTENCIAS',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
                                               
     if nvl(lv_valor, 'N') = 'S' then
 
    ---SUD CAC----
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                           'GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS',
                                           null,
                                           null,
                                           null,
                                           null,
                                           ln_total_registros_scp,
                                           lv_unidad_registro_scp,
                                           ln_id_bitacora_scp,
                                           ln_error_scp,
                                           lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
    
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje inicio de Proceso
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          'Inicio de GSI_REVISA_INCONSISTENCIA.GSI_BUSCA_INCONSISTENCIAS',
                                          lvMensErr,
                                          '-',
                                          0,
                                          null,
                                          NULL/*'Ciclo: '|| PV_CICLO*/,
                                          'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
                                          
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
    
    --- ESTABLECER LA FECHA DE REVISION
    LV_FECHA_REVISA := to_date(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    ---1. Buscar la fecha de inicio y fin de ciclo
    
    ---SUD CAC
    ----------
    /* 
    SELECT a.dia_ini_ciclo, a.dia_fin_ciclo
      into LV_dia_ini, LV_dia_fin
      FROM fa_ciclos_bscs a
     where id_ciclo = PV_CICLO;
   
   
    -------------------------------------------------------------------------
    If to_NUMBER(to_char(PV_FECHA_CORTE, 'dd')) < to_NUMBER(lv_dia_ini) Then
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 1 Then
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy') - 1;
      Else
        ---LPad(char, length, pad_string)
        lv_mes_ini  := lpad(to_char(PV_FECHA_CORTE, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_fin  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
    Else
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 12 Then
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy') + 1;
      Else
        lv_mes_fin  := lpad(to_char(PV_FECHA_CORTE, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_ini  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
    End If;
    -------------------------------------------------------------------------
    LV_fecha_ini_ci := LV_dia_ini || '/' || lv_mes_ini || '/' ||
                       lv_anio_ini;
    LV_fecha_fin_ci := LV_dia_fin || '/' || lv_mes_fin || '/' ||
                       lv_anio_fin;
    -----------------------------------------------------------------------------
    */
    --- SUD CAC---
    lv_mes_fin:=to_char(PV_FECHA_FIN,'mm');
    --------------
    VTT_SERVICIOS.DELETE;
    
    
    FECHA_2:=PV_FECHA_INICIO;
        
    OPEN SERVICIO_NOVEDADES(TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),
                            TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')  );
    
               LOOP
                 
                EXIT WHEN SERVICIO_NOVEDADES%NOTFOUND;
               
                FETCH SERVICIO_NOVEDADES BULK COLLECT INTO VTT_SERVICIOS limit 10000;
                
                
                ---En busca de Inconsistencia de Fechas
                  for B in 1 .. VTT_SERVICIOS.COUNT LOOP
                       
                    LN_CONTADOR := 0;
                    LN_VALIDA   := 500;
                    
                     LN_CONTADOR := LN_CONTADOR + 1;
                     IF LN_VALIDA = LN_CONTADOR THEN
                       LN_VALIDA := LN_CONTADOR + 500;
                       COMMIT;
                     END IF;
                     
                    /*BANDERA:=0;*/
                    -------
                    ---PARA VERIFICAR EL SERVIIO TIENE PROBLEMAS
                    -------
                    CONT_VER1:=0;
                    CONT_VER2:=0;
                    ---------------
                    ---------------
                    cont:=0;
                    ---------------
                    ---------------
                    
                    FECHA_2:=PV_FECHA_INICIO;
                    select PV_FECHA_INICIO + 2 INTO FECHA_1 from dual;
                    
                    TELELEFONO:=obtiene_telefono_dnc@axis(VTT_SERVICIOS(B));
                  
                  OPEN s_verifica(TELELEFONO,
                                  to_char(PV_FECHA_INICIO,'dd/mm/yyyy hh24:mi:ss'));
                       FETCH s_verifica INTO lc_s_verifica;
                       LB_FOUND_s_ver:= s_verifica%FOUND;
                       CLOSE s_verifica;                    
                  
                  IF(LB_FOUND_s_ver = FALSE)THEN 
                  
                              OPEN TELEFONOS (VTT_SERVICIOS(B));
                                   FETCH TELEFONOS INTO LC_TELEFONOS;
                                   LB_FOUND_TEL:= TELEFONOS%FOUND;
                              CLOSE TELEFONOS;
                                   
                  IF LB_FOUND_TEL = TRUE THEN 
                  
                  FOR LC_SERVICIO IN TELEFONO_SERVICIO(TELELEFONO,
                                                      TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),  /*LV_fecha_ini_ci SUD CAC*/
                                                      TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')) LOOP
                         
                                  CONT_VER1:=CONT_VER1+1;
                                  IF LC_SERVICIO.VALOR IN ('23','29','30','32','92')THEN
                                    ESTADO_AXIS:='A';
                                  ELSIF LC_SERVICIO.VALOR IN ('26','27','34','36','80')THEN
                                    ESTADO_AXIS:='S';
                                  END IF;
                             
                                   ---------------------
                                   ---------------------
                                                   ---- SI ES UN CAMBIO DE PPT A TAR 
                                                   ---- ESTO NO SE REFLEJA EN BSCS
                                                  IF LC_SERVICIO.VALOR IN ('32')THEN
                                                   OPEN c_verifica(TELELEFONO,to_char(LC_SERVICIO.FECHA_DESDE,'dd/mm/yyyy'));
                                                         FETCH c_verifica INTO LC_C_verifica;
                                                         LB_FOUND_ver:= c_verifica%FOUND;
                                                   CLOSE c_verifica; 
                                                  END IF;
                                 ---------------------
                                 ---------------------
                                 
                            IF LB_FOUND_ver =TRUE THEN
                               -----
                               -----                               
                             
                               LB_FOUND_ver:=FALSE;
                               CONT_VER2:=CONT_VER2+1;
                               FECHA_2:=LC_SERVICIO.FECHA_DESDE;
                               select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                               
                            ELSE 
                            
                             FOR C IN SERVICIOS(LC_TELEFONOS.CO_ID,FECHA_2,FECHA_1) LOOP                 
                               
                                   IF(C.STATUS <> ESTADO_AXIS)THEN
                                         BANDERA_SALTA:=TRUE;
                                         EXIT; 
                                   END IF;
                                  
                                  
                                  CONT_VER2:=CONT_VER2+1;
                                  FECHA_2:=C.entry_date;
                                  select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                                  
                                  IF LC_SERVICIO.VALOR = 23 THEN  --VALIDA SERVICIO 23 --SUD CAC
                                     
                                     IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >    
                                        TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN
                                        
                                        --SUD CAC---
                                        --SCP Procedimiento de busquedas de inconsistencias
                                        --SCP:MENSAJE
                                        ----------------------------------------------------------------------
                                        -- SCP: Registro de mensaje DETALLE
                                        ----------------------------------------------------------------------
                                        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                              'SERVICIOS NUEVOS',
                                                                              lvMensErr,
                                                                              '-',
                                                                              0,
                                                                              null,
                                                                              VTT_SERVICIOS(B)||' CO_ID: '|| TO_CHAR(LC_TELEFONOS.CO_ID),
                                                                              'F. BSCS: '||TO_CHAR(C.entry_date,'dd/MM/yyyy')||' F. AXIS '||TO_CHAR(LC_SERVICIO.FECHA_DESDE,'dd/MM/yyyy'),
                                                                              LC_SERVICIO.VALOR,---VALOR EN AXIS 23
                                                                              null,
                                                                              'S',
                                                                              ln_error_scp,
                                                                              lv_error_scp);
                                                                                            
                                        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
                                        ---fin Registro de mensaje DETALLE------------------------------------------
                                        ----------------------------------------------------------------------------
                                        LN_CONTADOR_SERVICIO23:=LN_CONTADOR_SERVICIO23+1;
                                       
                                        exit;
                                    end if;---FIN IF COMPARA FECHAS SERVICIOS CON VALOR 23
                                       
                                    ELSE -- SUD CAC
                                  
                                        IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >---fecha bscs    
                                           TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN   ---fecha axis

                                            OPEN REVISADOS(LC_TELEFONOS.CO_ID, 
                                                           lv_mes_fin,
                                                           LC_TELEFONOS.ID_CICLO_ADMIN,
                                                           LC_SERVICIO.FECHA_DESDE,
                                                           C.ENTRY_DATE);
                                                           
                                            FETCH REVISADOS
                                              INTO LC_REV;
                                            LB_FOUND_REV := REVISADOS%FOUND;
                                            CLOSE REVISADOS;

                                                IF LB_FOUND_REV THEN
                                                    UPDATE RESPALDO_CONCILIACION
                                                       SET ESTADO = 'A', COMENTARIO = ''
                                                       WHERE ciclo = LC_TELEFONOS.ID_CICLO_ADMIN
                                                       AND mes_cierre = lv_mes_fin
                                                       and co_id_bscs = LC_TELEFONOS.co_id
                                                       and fecha_axis = LC_SERVICIO.FECHA_DESDE
                                                       and fecha_bscs = C.ENTRY_DATE;
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                 ELSE
                                                      cont:=cont+1;
                                                      insert into RESPALDO_CONCILIACION
                                                       values
                                                      (LV_FECHA_REVISA,
                                                       VTT_SERVICIOS(B),
                                                       LC_SERVICIO.FECHA_DESDE,
                                                       LC_TELEFONOS.CO_ID,
                                                       C.ENTRY_DATE,
                                                       'A',
                                                       null,
                                                       LC_TELEFONOS.ID_CICLO_ADMIN,
                                                       lv_mes_fin,
                                                       cont,
                                                       ESTADO_AXIS);
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                END IF;-- FIN IF CURSOR REVISADOS
                                                
                                                commit;
                                               
                                           
                                          end if;-- FIN IF DE COMPARACION DE FECHAS
                                          
                                    
                                       END IF; --VALIDA SERVICIO 23
                                   
                                    Exit;--- SALIR DEL LOOP
                                    
                                END LOOP; ---FIN LOOP CURSOR SERVICIOS
                                
                                IF(BANDERA_SALTA)THEN
                                   BANDERA_SALTA:=TRUE;
                                   EXIT;
                                END IF;
                                
                             END IF;
                  
                       END LOOP; --- FIN LOOP TELEFONO SERVICIO
                       
                       ----GUARDA SERVICIOS Q NO SE REFLEJEN EN BSCS
                       IF (CONT_VER1<>CONT_VER2)THEN 
                           --SUD CAC---
                            --SCP Procedimiento de busquedas de inconsistencias
                            --SCP:MENSAJE
                            ----------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------
                            
                            
                            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                  'Cuenta con Problemas Revisar: '||VTT_SERVICIOS(B),
                                                                  lvMensErr,
                                                                  '-',
                                                                  0,
                                                                  null,
                                                                  NULL,
                                                                  'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                                                  null,
                                                                  null,
                                                                  'S',
                                                                  ln_error_scp,
                                                                  lv_error_scp);
                             
                            --SCP:FIN
                            ----------------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------------                                                                  
                                                                  
                                             
                       END IF;
                       ----------------
                       ----------------
                   END IF; ---CURSOR TELEFONOS
                 END IF;
                
                end loop; -- FIN LOOP VARIABLE TIPO TABLA
               
             END LOOP;--FIN LOOP CURSOR BULK COLLECT
  
CLOSE SERVICIO_NOVEDADES;
   
   
    
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------
      
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se encontraron: '||to_char(LN_CONTADOR_SERVICIO23)||' Servicios Nuevos Activados INCONSISTENTES',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            NULL,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
                                            
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se ejecutó GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            null,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
      ----------------------------------------------------------------------------
      
      commit;
   ----CAC----
    
    EXCEPTION
      
      WHEN NO_DATA_FOUND THEN
      
      lvMensErr:= 'NO SE ENCONTRO DATOS EN LA TABLA fa_ciclos_bscs';
      
      IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_CONCILIA_BSCS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      ----------------------------------------------------------------------------

    WHEN OTHERS THEN      
    
     
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     
     IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      ----------------------------------------------------------------------------
    -------
    -------CAC
    

    ---Llamada al procedimiento de actualización
    --- GSI_REVISA_TABLA;

  end GSI_BUSCA_INCONSISTENCIAS; ---fin GSI_BUSCA_INCONSISTENCIAS
  
  

  -- Procedimiento que barre la tabla de inconsistenicas para procesar uno a uno los telefonos
 
  Procedure GSI_REVISA_TABLA is
    
    --CURSOR QUE BARRE LA TABLA CON LOS SERVICIOS PENDIENTES DE ACTUALIZACION
    
    CURSOR REVISIONES IS
      SELECT * FROM RESPALDO_CONCILIACION WHERE ESTADO = 'A';
      
      
    
    ---SUD CAC----
    --------------------------------------------------------------
    --Variables SCP
    --Para la Bitacora
   
    lv_valor           VARCHAR2(1);
    lv_descripcion     VARCHAR2(500);
    lvMensErr VARCHAR2(2000) := null;
    ---fin SUD CAC-------------------------------------------------
    ---------------------------------------------------------------  
      
    
    
    LC_COMENTARIOS VARCHAR2(1000);
    PROC_ESTADO    CHAR(1);

  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_CONCILIA_BSCS',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
    ---SUD CAC--------                                            
     if nvl(lv_valor, 'N') = 'S' then
      
     gv_fecha := to_char(Sysdate,'dd/MM/yyyy');
     gv_fecha_fin := NULL;
    -----------------
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ---SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,'GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA',null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
  
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP:  Registro Inicio del Proceso
    ----------------------------------------------------------------------------
    
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
  
  
    for A IN REVISIONES LOOP
      ---CONCILIA POR TELEFONO
      GSI_CONCILIA_BSCS(A.FECHA_AXIS,
                        A.CO_ID_BSCS,
                        A.FECHA_BSCS,
                        A.SERVICIO_AXIS,
                        A.ESTADO_BSCS,---SUD CAC
                        LC_COMENTARIOS,
                        PROC_ESTADO);

      --ACTUALIZA EL ESTADO Y COMENTARIO DEL REGISTRO
      UPDATE RESPALDO_CONCILIACION B
         SET ESTADO = PROC_ESTADO, COMENTARIO = LC_COMENTARIOS
       WHERE b.ciclo = a.ciclo
         AND b.mes_cierre = a.mes_cierre
         and b.co_id_bscs = a.co_id_bscs
         and b.id_respaldo=a.id_respaldo;
         
         -----
         -----SUD CAC
         ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
         -----
         -----

    END LOOP;

      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------------
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecutó GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----fin Registro de finalización de proceso---------------------------------
      ----------------------------------------------------------------------------
      
         
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
       commit;

      ----fin Registro de finalización de proceso---------------------------------
      ----------------------------------------------------------------------------
                                            
    EXCEPTION
  
    WHEN OTHERS THEN
      
      --SUD CAC---
      ------------
      
      lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE;
      
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ------------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en GSI_REVISA_TABLA',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      ----fin Registro de mensaje de error-----------------------------------
      -----------------------------------------------------------------------
   
    
  end GSI_REVISA_TABLA; ---fin GSI_REVISA_TABLA
  
  

  -- Procedimiento que actualiza las fechas en BSCS deacuerdo a la de AXIS
Procedure GSI_CONCILIA_BSCS(FECHA_AXIS  in DATE,
                              V_CO_ID     in number,
                              FECHA_BSCS  IN DATE,
                              TELFONO     IN varchar2,
                              ESTADO_BSCS IN VARCHAR2,
                              COMENTARIOS OUT VARCHAR2,
                              ESTADO      OUT CHAR) is
    --cursores por tabla para determinar las fechas ingresadas
    ---SUD CAC
  /*  cursor contract_al is
      select co_id, co_signed, co_installed, co_activated, co_entdate
        from CONTRACT_ALL
       where co_id = V_CO_ID
            --and trunc(co_activated) = to_date(FECHA_BSCS,'dd/mm/yyyy');
         and trunc(co_activated) = TRUNC(FECHA_BSCS);

    LC_contract contract_al%ROWTYPE;*/
    
    
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------

    /*

    cursor rateplan is
        select co_id, tmcode_date
           from rateplan_hist
        where co_id = V_CO_ID
          --and trunc(tmcode_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
          and trunc(tmcode_date) = TRUNC(FECHA_BSCS);

    LC_rate   rateplan%ROWTYPE;

     cursor contact_h is
        select co_id, ch_validfrom, entdate
           from contract_history
         where co_id = V_CO_ID
         --and  trunc(ch_validfrom) = to_date(FECHA_BSCS,'dd/mm/yyyy');
         and  trunc(ch_validfrom) = TRUNC(FECHA_BSCS);

    LC_contact   contact_h%ROWTYPE;


     cursor profile is
        select co_id, entry_date
         from profile_service
        where co_id = V_CO_ID
         --and trunc(entry_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
         and trunc(entry_date) = TRUNC(FECHA_BSCS);

    LC_prof   profile%ROWTYPE;*/
    
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    
    

    cursor pr_serv is
      select co_id, valid_from_date, entry_date, status
        from pr_serv_status_hist
       where co_id = V_CO_ID
            --and trunc(valid_from_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
         and entry_date = FECHA_BSCS
         
    -----SUD CAC MODIFICACION DE OBTENCION DE SERVICIOS----
    -------------------------------------------------------
         and STATUS=ESTADO_BSCS
         and SNCODE NOT IN   
             (select SNCODE
                from mpusntab
               where sncode in (select distinct sncode from mpulktmm))
    
        order by valid_from_date;
               
    -----FIN------------------------------------------------
    --------------------------------------------------------

    LC_serv pr_serv%ROWTYPE;

    cursor pr_spcode is
      select co_id, entry_date, valid_from_date
        from pr_serv_spcode_hist
       where co_id = V_CO_ID
            --and trunc(valid_from_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
         and valid_from_date = FECHA_BSCS;

    LC_spcode pr_spcode%ROWTYPE;
    
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    /*

      cursor directory is
         select dn_num, dn_status_mod_date,dn_moddate
           from directory_number
         where dn_num = TELFONO
          --and  trunc(dn_status_mod_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
          and  trunc(dn_status_mod_date) = TRUNC(FECHA_BSCS);

    LC_director   directory%ROWTYPE;


       cursor contr_cap (ln_dn number) is
         select dn_id, co_id, cs_activ_date
           from contr_services_cap
         where dn_id = ln_dn
           and co_id = V_CO_ID
           --and trunc(cs_activ_date)=to_date(FECHA_BSCS,'dd/mm/yyyy') ;
           and trunc(cs_activ_date)= TRUNC(FECHA_BSCS);

    LC_cap   contr_cap%ROWTYPE;


        cursor devices is
         select co_id, cd_activ_date, cd_validfrom cd_entdate
            from contr_devices
          where co_id = V_CO_ID
              --and trunc(cd_activ_date) = to_date(FECHA_BSCS,'dd/mm/yyyy')  ;
              and trunc(cd_activ_date) = TRUNC(FECHA_BSCS);

    LC_devices devices%ROWTYPE;


          cursor port_t (ln_port number) is
            select port_id, port_statusmoddat, port_activ_date
            from port
           where port_id = ln_port
           --and trunc(port_statusmoddat)= to_date(FECHA_BSCS,'dd/mm/yyyy') ;
           and trunc(port_statusmoddat)= TRUNC(FECHA_BSCS) ;

     LC_port_v   port_t%ROWTYPE;*/
    --------------------------------------------------------------------------- 
    ---------------------------------------------------------------------------
    
     
    LB_FOUND_C BOOLEAN;
    ------------------------------------------------------------
    ln_dn_id   number;
   -- ln_port_id number;---SUD CAC
    -----------------------------------------------------------
   /* lvSentencia varchar2(20000) := null;*/
    --lvMensErr   VARCHAR2(2000):= null;
    ----VARIABLES PARA SUMARIZAR LOS MINUTOS
    /*hora_c  CHAR(2);
    min_c   CHAR(2);
    seg_c   CHAR(2);
    fecha_c VARCHAR2(8);*/
    --------------------------------------------------------------
    
    
    ---SUD CAC----
    --------------------------------------------------------------
    --Variables DE ERROR     
    lvMensErr VARCHAR2(2000) := null;
    
    fecha_a date;
    ---fin-----------------------------------------------------------
    --------------------------------------------------------------

  begin
    
    --SUD CAC----
    --SCP Procedimiento de busquedas de inconsistencias
    -------------  
     gv_fecha := to_char(Sysdate,'dd/MM/yyyy');
     gv_fecha_fin := NULL;
     
    ----------------------------------------------------------------------
    -- SCP:  Registro Inicio del Proceso
    ----------------------------------------------------------------------
    
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de GSI_REVISA_INCONSISTENCIAS.GSI_CONCILIA_BSCS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ----fin Registro Inicio del Proceso------------------------------------
    -----------------------------------------------------------------------

   
    COMENTARIOS := '';
    
    /*lvSentencia := 'ALTER SESSION SET NLS_DATE_FORMAT = ' || '''' ||'dd/mm/yyyy' || '''';

    EJECUTA_SENTENCIA(lvSentencia, COMENTARIOS);*/

    COMENTARIOS := COMENTARIOS || ' ' || 'LAS TABLAS ACTUALIZADAS SON:';
  
   ---SUD CAC---  
    --------CONSULTAS DE DN_ID
    select dn_id
      into ln_dn_id
      from directory_number
     where dn_num = TELFONO;

/*
    ----CONSULTAS CONTR_DEVICES
    select port_id
      into ln_port_id
      from contr_devices
     where co_id = V_CO_ID
          --and  trunc(cd_activ_date) = to_date(FECHA_BSCS,'dd/mm/yyyy') ;
       and trunc(cd_activ_date) = TRUNC(FECHA_BSCS);*/
    --------------------------------------
    --------------------------------------
    
   
    /*OPEN contract_al;
    FETCH contract_al
      INTO LC_contract;
    LB_FOUND_C := contract_al%FOUND;
    CLOSE contract_al;

    ----ACTUALIZA LA CONTRACT ALL
    if LB_FOUND_C then

      if LC_contract.co_activated <> FECHA_AXIS then
        --verfecha := to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
        COMENTARIOS := COMENTARIOS || ' ' || 'contract_all se actualizaron';

        update contract_all
           set co_signed    = to_date(FECHA_AXIS, 'dd/mm/yyyy'),
               co_installed = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') ||to_char(co_installed, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
               co_activated = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') ||to_char(co_activated, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
               co_entdate   = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') ||to_char(co_entdate, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
         where co_id = V_CO_ID
              --and trunc(co_activated) = to_date(FECHA_BSCS,'dd/mm/yyyy') ;
           and trunc(co_activated) = TRUNC(FECHA_BSCS);

        --SUD CAC----
        --SCP Procedimiento de busquedas de inconsistencias
        ----------------------------------------------------------------------------
        -- SCP: Registro detalles de bitacora de ejecución
        ----------------------------------------------------------------------------
        
          lv_mensaje_apl_scp := '   ACTUALIZACION TABLA CONTRACT_ALL:   ' ||
                                '   FECHA AXIS:' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')) ||
                                '   FECHA BSCS:' ||
                                TO_CHAR(TO_DATE(LC_contract.co_activated,
                                                'dd/mm/yyyy ')) ||
                                '   FECHA ACTUALIZACION: ' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')); ---PREGUNTAR
          lv_mensaje_tec_scp := '';
          lv_mensaje_acc_scp := '';
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
       
        ---fin detalle bitacora -----------------------------------------
        -----------------------------------------------------------------

        COMENTARIOS := COMENTARIOS || ' ' ||
                       'co_signed, co_installed, co_activated, co_entdate';
        commit;
      else
        COMENTARIOS := COMENTARIOS || ' ' ||
                       'No se actualizo datos en contract_all';
      end if;
    else
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'No se actualizo datos en contract_all';
    end if;
    --------------------------------------*/
    
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    /*  OPEN rateplan ;
           FETCH rateplan INTO LC_rate;
           LB_FOUND_C := rateplan%FOUND;
       CLOSE rateplan;
    ----ACTUALIZA LA rateplan_hist
      if LB_FOUND_C then

        if LC_rate.tmcode_date <> FECHA_AXIS then
           COMENTARIOS := COMENTARIOS || ' ' || 'Tabla rateplan_hist se actualizaron';
           update rateplan_hist
               set tmcode_date = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(tmcode_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
           where co_id = V_CO_ID
            --and   trunc(tmcode_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
            and trunc(tmcode_date) = TRUNC(FECHA_BSCS);

             COMENTARIOS := COMENTARIOS || ' ' || 'tmcode_date';
            commit;

          else
         COMENTARIOS := COMENTARIOS || ' ' || 'No se actualizo datos en rateplan_hist';
         end if;

       else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en rateplan_hist';
       end if;*/
    --------------------------------------
    /* OPEN contact_h ;
           FETCH contact_h INTO LC_contact;
           LB_FOUND_C := contact_h%FOUND;
       CLOSE contact_h;
    ----ACTUALIZA LA CONTRACT HISTORY
       if LB_FOUND_C then

         if LC_contact.ch_validfrom <> FECHA_AXIS then
          COMENTARIOS := COMENTARIOS || ' ' || 'Tabla rateplan_hist se actualizaron';
          update contract_history
              set ch_validfrom = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ch_validfrom,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
                  entdate = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
            where co_id = V_CO_ID
               --and  trunc(ch_validfrom) = to_date(FECHA_BSCS,'dd/mm/yyyy');
               and  trunc(ch_validfrom) = TRUNC(FECHA_BSCS);

              COMENTARIOS := COMENTARIOS || ' ' || 'ch_validfrom, entdate';
            commit;
           else
             COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contract_history';
          end if;
        else
          COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contract_history';
        end if;*/
    --------------------------------------
    /*  OPEN profile;
          FETCH profile  INTO LC_prof;
          LB_FOUND_C := profile%FOUND;
      CLOSE profile;
    ----ACTUALIZA LA PROFILE SERVICE
     if LB_FOUND_C then

       if LC_prof.entry_date <> FECHA_AXIS then

         COMENTARIOS := COMENTARIOS || ' ' || 'Tabla profile_service se actualizaron';

         update profile_service a
            set a.entry_date = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(a.entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
         where a.co_id = V_CO_ID
           --and trunc(a.entry_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
           and trunc(a.entry_date) = TRUNC(FECHA_BSCS);

           COMENTARIOS := COMENTARIOS || ' ' || 'entry_date';
           commit;
         else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en profile_service';
         end if;
       else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en profile_service';

     end if;*/
    --------------------------------------
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
   
    
    OPEN pr_serv;
    FETCH pr_serv
      INTO LC_serv;
    LB_FOUND_C := pr_serv%FOUND;
    CLOSE pr_serv;
   
    
    ----ACTUALIZA LA PR_SERV_STATUS_HIST
    if LB_FOUND_C then
   
       fecha_a:=FECHA_AXIS;
     
       if LC_serv.Status IN ('S') then 
              
          if to_date(LC_serv.entry_date,'dd/mm/yyyy') <> to_date(FECHA_AXIS,'dd/mm/yyyy') then

            COMENTARIOS := COMENTARIOS || ' ' ||
                           'Tabla pr_serv_status_hist se actualizaron';
                           
            /*
           ---se le aumenta 1 minuto a los O
            hora_c  := to_char(fecha_axis, 'hh24');
            min_c   := lpad(to_char(fecha_axis, 'mi') + 1, 2, '0');
            seg_c   := to_char(fecha_axis, 'ss');
            fecha_c := hora_c || ':' || min_c || ':' || seg_c;

            update pr_serv_status_hist
               set valid_from_date = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss'),
                   entry_date      = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss')
               where co_id = V_CO_ID
               and trunc(valid_from_date) = TRUNC(FECHA_BSCS)
               and status = 'O';

           ---se le aumenta 2 minuto a los A con respecto a los O
            hora_c  := to_char(fecha_axis, 'hh24');
            min_c   := lpad(to_char(fecha_axis, 'mi') + 3, 2, '0');---aqui aumentar minutos
            seg_c   := to_char(fecha_axis, 'ss');
            fecha_c := hora_c || ':' || min_c || ':' || seg_c; 
          */
             
             select fecha_axis +(1/1400)*2 into fecha_a from dual;
                       
              update pr_serv_status_hist
                   set valid_from_date = to_date(to_char(fecha_a,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
                                       /*to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss'),*/
                       entry_date = to_date(to_char(fecha_a,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
                                    /*to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss')*/
               where co_id = V_CO_ID
               and entry_date = FECHA_BSCS
               and status = 'S'
               and SNCODE NOT IN   
                          (select SNCODE
                                  from mpusntab
                                  where sncode in (select distinct sncode from mpulktmm));

                    --SUD CAC----
                    --SCP Procedimiento de busquedas de inconsistencias
                    ----------------------------------------------------------------------------
                    -- SCP: Registro detalles de bitacora de ejecución
                    ----------------------------------------------------------------------------
                              
                      lv_mensaje_apl_scp := '   ACTUALIZACION TABLA PR_SERV_STATUS_HIST:   ' ||
                                            '   FECHA AXIS:' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')) ||
                                            '   FECHA BSCS:' ||
                                            TO_CHAR(TO_DATE(LC_serv.valid_from_date,
                                                            'dd/mm/yyyy ')) ||
                                            '   FECHA ACTUALIZACION: ' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')); 
                      lv_mensaje_tec_scp := '';
                      lv_mensaje_acc_scp := '';
                      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                            lv_mensaje_apl_scp,
                                                            lv_mensaje_tec_scp,
                                                            lv_mensaje_acc_scp,
                                                            0,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            'N',
                                                            ln_error_scp,
                                                            lv_error_scp);
                              
                    ---fin detalle bitacora -----------------------------------------
                    -----------------------------------------------------------------

            COMENTARIOS := COMENTARIOS || ' ' || 'entry_date , valid_from_date';
            commit;

          else
            COMENTARIOS := COMENTARIOS || ' ' ||
                           'No se actualizo datos en pr_serv_status_hist';
          end if;
      --------------------------------------------------------------------
      --------------------------------------------------------------------              
        elsif LC_serv.Status IN ('A') then 
        
         
                if to_date(LC_serv.entry_date,'dd/mm/yyyy') <> to_date(FECHA_AXIS,'dd/mm/yyyy') then
                 
                       COMENTARIOS := COMENTARIOS || ' ' ||'Tabla pr_serv_status_hist se actualizaron';
                 
                         select fecha_axis +(1/1400)*3 into fecha_a from dual;
                      
                         update pr_serv_status_hist
                         set valid_from_date = to_date(to_char(fecha_a,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
                                                 /*to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss'),*/
                             entry_date  = to_date(to_char(fecha_a,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
                                                 /*to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') || fecha_c,'dd/mm/yyyy hh24:mi:ss')*/
                         where co_id = V_CO_ID
                         and entry_date = FECHA_BSCS
                         and status = 'A'
                         and SNCODE NOT IN   
                               (select SNCODE
                                  from mpusntab
                                 where sncode in (select distinct sncode from mpulktmm));
                         
                    --SUD CAC----
                    --SCP Procedimiento de busquedas de inconsistencias
                    ----------------------------------------------------------------------------
                    -- SCP: Registro detalles de bitacora de ejecución
                    ----------------------------------------------------------------------------
                              
                      lv_mensaje_apl_scp := '   ACTUALIZACION TABLA PR_SERV_STATUS_HIST:   ' ||
                                            '   FECHA AXIS:' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')) ||
                                            '   FECHA BSCS:' ||
                                            TO_CHAR(TO_DATE(LC_serv.valid_from_date,
                                                            'dd/mm/yyyy ')) ||
                                            '   FECHA ACTUALIZACION: ' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')); 
                      lv_mensaje_tec_scp := '';
                      lv_mensaje_acc_scp := '';
                      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                            lv_mensaje_apl_scp,
                                                            lv_mensaje_tec_scp,
                                                            lv_mensaje_acc_scp,
                                                            0,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            'N',
                                                            ln_error_scp,
                                                            lv_error_scp);
                              
                    ---fin detalle bitacora -----------------------------------------
                    -----------------------------------------------------------------

                      COMENTARIOS := COMENTARIOS || ' ' || 'entry_date , valid_from_date';
                      commit;

                else
                  COMENTARIOS := COMENTARIOS || ' ' ||
                                 'No se actualizo datos en pr_serv_status_hist';
                end if;
          
       end if;
      ---------------------------------------------------------------------
      ---------------------------------------------------------------------
      
    else
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'No se actualizo datos en pr_serv_status_hist';
    end if;--- de la variable boolean 
    --------------------------------------
    
    
    
    OPEN pr_spcode;
    FETCH pr_spcode
      INTO LC_spcode;
    LB_FOUND_C := pr_spcode%FOUND;
    CLOSE pr_spcode;
    ----ACTUALIZA LA PR_SERV_SPCODE_HIST
    if LB_FOUND_C then

      if LC_spcode.valid_from_date <> FECHA_AXIS then

        COMENTARIOS := COMENTARIOS || ' ' ||'Tabla pr_serv_spcode_hist se actualizaron';

         update pr_serv_spcode_hist
           set valid_from_date = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') ||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
               entry_date      = to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy') ||to_char(entry_date, 'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
         where co_id = V_CO_ID
           and valid_from_date = FECHA_BSCS
           and SNCODE NOT IN   
                       (select SNCODE
                          from mpusntab
                         where sncode in (select distinct sncode from mpulktmm));
        --SUD CAC----
        --SCP Procedimiento de busquedas de inconsistencias
        ----------------------------------------------------------------------------
        -- SCP: Registro detalles de bitacora de ejecución
        ----------------------------------------------------------------------------
        
          lv_mensaje_apl_scp := '   ACTUALIZACION TABLA PR_SERV_SPCODE_HIST:   ' ||
                                '   FECHA AXIS:' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')) ||
                                '   FECHA BSCS:' ||
                                TO_CHAR(TO_DATE(LC_spcode.valid_from_date,
                                                'dd/mm/yyyy ')) ||
                                '   FECHA ACTUALIZACION: ' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'dd/mm/yyyy ')); ---PREGUNTAR
          lv_mensaje_tec_scp := '';
          lv_mensaje_acc_scp := '';
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_error_scp,
                                                lv_error_scp);
        
        ---fin detalle bitacora -----------------------------------------
        -----------------------------------------------------------------

        COMENTARIOS := COMENTARIOS || ' ' || 'entry_date , valid_from_date';
        commit;
      else
        COMENTARIOS := COMENTARIOS || ' ' ||
                       'No se actualizo datos en pr_serv_spcode_hist';
      end if;
    else
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'No se actualizo datos en pr_serv_spcode_hist';
    end if;
   
   ---------------------------------------------------------------------------
   ---------------------------------------------------------------------------
    ---SUD CAC-----------------------------------
    /* OPEN directory;
          FETCH directory  INTO LC_director;
          LB_FOUND_C := directory%FOUND;
      CLOSE directory;
    ----ACTUALIZA LA DIRECTORY NUMBER
     if LB_FOUND_C then

       if LC_director.dn_status_mod_date <> FECHA_AXIS then

        COMENTARIOS := COMENTARIOS || ' ' || 'Tabla directory_number se actualizaron';

        update directory_number
             set dn_status_mod_date=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(dn_status_mod_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
                 dn_moddate=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(dn_moddate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
          where dn_num = TELFONO
            --and  trunc(dn_status_mod_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
            and  trunc(dn_status_mod_date) = TRUNC(FECHA_BSCS);

          COMENTARIOS := COMENTARIOS || ' ' || 'dn_status_mod_date , dn_moddate';
           commit;
        else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en directory_number';
         end if;
       else
           COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en directory_number';
      end if;*/
    --------------------------------------
    /*  OPEN contr_cap(ln_dn_id) ;
        FETCH contr_cap   INTO LC_cap;
        LB_FOUND_C := contr_cap%FOUND;
     CLOSE contr_cap ;
    ----ACTUALIZA LA CONTR_SERVICES_CAP
    if LB_FOUND_C then

      if LC_cap.cs_activ_date <> FECHA_AXIS then

       COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contr_services_cap se actualizaron';

        update contr_services_cap
            set cs_activ_date=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cs_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
         where dn_id =ln_dn_id
            and co_id = V_CO_ID
            --and trunc(cs_activ_date)=to_date(FECHA_BSCS,'dd/mm/yyyy') ;
            and trunc(cs_activ_date)=TRUNC(FECHA_BSCS);

         COMENTARIOS := COMENTARIOS || ' ' || 'cs_activ_date';
         commit;
       else
          COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_services_cap';
       end if;
     else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_services_cap';
     end if;*/
    --------------------------------------
    /* OPEN devices;
       FETCH devices   INTO LC_devices;
       LB_FOUND_C := devices%FOUND;
     CLOSE devices ;
    ----ACTUALIZA LA CONTR_DEVICES
     if LB_FOUND_C then

       if LC_devices.cd_activ_date <> FECHA_AXIS then

        COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contr_devices se actualizaron';

        update contr_devices
          set cd_activ_date=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
              cd_validfrom=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_validfrom,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
              cd_entdate=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
           where co_id = V_CO_ID
              --and trunc(cd_activ_date) = to_date(FECHA_BSCS,'dd/mm/yyyy')  ;
              and trunc(cd_activ_date) = TRUNC(FECHA_BSCS);

           COMENTARIOS := COMENTARIOS || ' ' || 'cs_activ_date,. cd_validfrom, cd_entdate';
            commit;
        else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_devices';
      end if;
    else
        COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_devices';
    end if;
        */
    --------------------------------------
    /*    OPEN port_t (ln_port_id);
      FETCH port_t INTO LC_port_v;
      LB_FOUND_C := port_t%FOUND;
    CLOSE port_t ;
    ----ACTUALIZA LA PORT
    if LB_FOUND_C then

      if LC_port_v.port_statusmoddat <> FECHA_AXIS then

       COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contr_devices se actualizaron';

       update port
           set port_statusmoddat=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(port_statusmoddat,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
               port_activ_date=to_date(to_char(to_date(FECHA_AXIS,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(port_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
        where port_id = ln_port_id
          --and trunc(port_statusmoddat)= to_date(FECHA_BSCS,'dd/mm/yyyy') ;
          and trunc(port_statusmoddat)= TRUNC(FECHA_BSCS) ;

         COMENTARIOS := COMENTARIOS || ' ' || 'port_statusmoddat,. port_activ_date';
         commit;
        else
             COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en port';
        end if;
      else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en port';
      end if;*/
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    
       --SUD CAC----
        --SCP Procedimiento de busquedas de inconsistencias
       ----------------------------------------------------------------------
       -- SCP:  Registro fin del proceso
       ----------------------------------------------------------------------
       
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecutó GSI_REVISA_INCONSISTENCIAS.GSI_CONCILIA_BSCS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       -----fin registro fin de proceso--------------------------------------------
       
    ESTADO := 'P';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ESTADO      := 'E';
      COMENTARIOS := COMENTARIOS || ' ' || 'NO SE ENCONTRO DATOS';
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar GSI_CONCILIA_BSCS',COMENTARIOS,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     
      ----------------------------------------------------------------------------
      ------fin Registro mensaje de error-----------------------------------------

    WHEN OTHERS THEN
      ESTADO      := 'E';
      COMENTARIOS := COMENTARIOS || ' ' || SQLERRM;
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar GSI_CONCILIA_BSCS',COMENTARIOS,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
   
      ----------------------------------------------------------------------------
      ------fin Registro mensaje de error-----------------------------------------

  end GSI_CONCILIA_BSCS;
  -- fin GSI_CONCILIA_BSCS

end GSI_REVISA_INCONSISTENCIAS_C;
/
