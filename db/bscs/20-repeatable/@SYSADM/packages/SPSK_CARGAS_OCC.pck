CREATE OR REPLACE PACKAGE SPSK_CARGAS_OCC IS

  /*
    PROYECTO:       6042 PLANES POOL SVA
    CREADO POR:     CLS MANUEL VERA MVR
    LIDER:          CLS LUIS MU�OZ
                    SIS PAOLA CARVAJAL
    FECHA CREACION: 01/01/2012
    COMENTARIO:     PROCESOS NECESARIOS PARA REALIZAR LA CARGA OCC DESDE LA TABLA SPS_CARGA_OCC_TMP.
                    OBTIENE LOS PARAMETROS NECESARIOS PARA REALIZAR LA INSERCION DE CARGAS EN LA FEES.
  */
  /*******************************************************************************************
    PROYECTO:           [6042] PLANES POOL DE SVA/VOZ
    MODIFICADO POR:     CLS DANNY LAMILLA 
    LIDER CLS:          CLS LUIS MU�OZ
    LIDER SIS:          SIS PAOLA CARVAJAL
    FECHA MODIFICACION: 14/08/2012
    COMENTARIO:         Se modifica para verificar si el valor esta exento de impuesto
  ********************************************************************************************/

  PROCEDURE SPSP_INSERTA_FEES(PN_CUSTOMER_ID     IN NUMBER,
                              PN_SEQNO           IN NUMBER,
                              PV_FEETYPE         IN VARCHAR2,
                              PF_AMOUNT          IN FLOAT,
                              PV_REMARK          IN VARCHAR2,
                              PV_GLCODE          IN VARCHAR2,
                              PD_ENTDATE         IN DATE,
                              PN_PERIOD          IN NUMBER,
                              PV_USERNAME        IN VARCHAR2,
                              PD_VALIDFROM       DATE,
                              PN_JOBCOST         IN NUMBER,
                              PV_BILLFMT         IN VARCHAR2,
                              PV_SERVCATCODE     IN VARCHAR2,
                              PV_SERVCODE        IN VARCHAR2,
                              PV_SERVTYPE        IN VARCHAR2,
                              PN_COID            IN NUMBER,
                              PF_AMOUNTGROSS     IN FLOAT,
                              PN_CURRENCY        IN NUMBER,
                              PV_GLCODEDISC      IN VARCHAR2,
                              PN_JOBCOSTIDDISC   IN NUMBER,
                              PV_GLCODEMINCOM    IN VARCHAR2,
                              PN_JOBCOSTIDMINCOM IN NUMBER,
                              PN_RECVERSION      IN NUMBER,
                              PN_CDRID           IN NUMBER,
                              PN_CDRSUBID        IN NUMBER,
                              PN_UDRBASEPARTID   IN NUMBER,
                              PN_UDRCHARGEPARTID IN NUMBER,
                              PN_TMCODE          IN NUMBER,
                              PN_VSCODE          IN NUMBER,
                              PN_SPCODE          IN NUMBER,
                              PN_SNCODE          IN NUMBER,
                              PN_EVCODE          IN NUMBER,
                              PN_FEECLASS        IN NUMBER,
                              PN_FUPACKID        IN NUMBER,
                              PN_FUPVERSION      IN NUMBER,
                              PN_FUPSEQ          IN NUMBER,
                              PN_VERSION         IN NUMBER,
                              PN_FREEUNITSNUMBER IN NUMBER,
                              PV_ERROR           OUT VARCHAR2);

  PROCEDURE SPSP_OBTIENE_SECUENCIA_FEES(PN_CUSTOMERID IN NUMBER,
                                        PN_SEQNO      OUT NUMBER,
                                        PV_ERROR      OUT VARCHAR2);

  PROCEDURE SPSP_OBTIENE_EVCODE(PN_SNCODE IN NUMBER,
                                PN_EVCODE OUT NUMBER,
                                PV_ERROR  OUT VARCHAR2);

  PROCEDURE SPSP_DESPACHA_CARGA_OCC(PN_ID_EJECUCION NUMBER,
                                    PV_FECHA_INI    VARCHAR2,
                                    PV_FECHA_FIN    VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2);

  PROCEDURE SPSP_VALIDA_OCC_DUPLICADO(PN_CUSTOMERID IN NUMBER,
                                      PN_SNCODE     IN NUMBER,
                                      PN_TMCODE     IN NUMBER,
                                      PV_FEE_TYPE   IN VARCHAR2,
                                      PN_PERIOD     IN NUMBER,
                                      PV_REMARK     IN VARCHAR2,
                                      PV_ERROR      OUT VARCHAR2);

  PROCEDURE SPSP_ELIMINA_CARGA(PV_ROWID VARCHAR2);

  PROCEDURE SPSP_ACTUALIZA_EJECUCION(PN_ID_EJECUCION NUMBER);

  PROCEDURE SPSP_ACTUALIZA_CARGA_OCC(PV_ROWID        VARCHAR2 DEFAULT NULL,
                                     PN_ID_EJECUCION NUMBER DEFAULT NULL,
                                     PV_OBSERVACION  VARCHAR2);

  PROCEDURE SPSP_BIT_EJECUCION_CARGA_OCC(PD_FECHA_INI    DATE,
                                         PD_FECHA_FIN    DATE,
                                         PV_OBSERVACION  VARCHAR2,
                                         PV_ESTADO       VARCHAR2,
                                         PN_ID_EJECUCION NUMBER);
END SPSK_CARGAS_OCC;
/
CREATE OR REPLACE PACKAGE BODY SPSK_CARGAS_OCC IS

  /*
    PROYECTO:       6042 PLANES POOL SVA
    CREADO POR:     CLS MANUEL VERA MVR
    LIDER:          CLS LUIS MU�OZ
                    SIS PAOLA CARVAJAL
    FECHA CREACION: 20/01/2012
    COMENTARIO:     PROCESOS NECESARIOS PARA REALIZAR LA CARGA OCC DESDE LA TABLA SPS_CARGA_OCC_TMP.
                    OBTIENE LOS PARAMETROS NECESARIOS PARA REALIZAR LA INSERCION DE CARGAS EN LA FEES.
  */
  /*******************************************************************************************
    PROYECTO:           [6042] PLANES POOL DE SVA/VOZ
    MODIFICADO POR:     CLS DANNY LAMILLA 
    LIDER CLS:          CLS LUIS MU�OZ
    LIDER SIS:          SIS PAOLA CARVAJAL
    FECHA MODIFICACION: 14/08/2012
    COMENTARIO:         Se modifica para verificar si el valor esta exento de impuesto
  ********************************************************************************************/

  PROCEDURE SPSP_INSERTA_FEES(PN_CUSTOMER_ID     IN NUMBER,
                              PN_SEQNO           IN NUMBER,
                              PV_FEETYPE         IN VARCHAR2,
                              PF_AMOUNT          IN FLOAT,
                              PV_REMARK          IN VARCHAR2,
                              PV_GLCODE          IN VARCHAR2,
                              PD_ENTDATE         IN DATE,
                              PN_PERIOD          IN NUMBER,
                              PV_USERNAME        IN VARCHAR2,
                              PD_VALIDFROM       IN DATE,
                              PN_JOBCOST         IN NUMBER,
                              PV_BILLFMT         IN VARCHAR2,
                              PV_SERVCATCODE     IN VARCHAR2,
                              PV_SERVCODE        IN VARCHAR2,
                              PV_SERVTYPE        IN VARCHAR2,
                              PN_COID            IN NUMBER,
                              PF_AMOUNTGROSS     IN FLOAT,
                              PN_CURRENCY        IN NUMBER,
                              PV_GLCODEDISC      IN VARCHAR2,
                              PN_JOBCOSTIDDISC   IN NUMBER,
                              PV_GLCODEMINCOM    IN VARCHAR2,
                              PN_JOBCOSTIDMINCOM IN NUMBER,
                              PN_RECVERSION      IN NUMBER,
                              PN_CDRID           IN NUMBER,
                              PN_CDRSUBID        IN NUMBER,
                              PN_UDRBASEPARTID   IN NUMBER,
                              PN_UDRCHARGEPARTID IN NUMBER,
                              PN_TMCODE          IN NUMBER,
                              PN_VSCODE          IN NUMBER,
                              PN_SPCODE          IN NUMBER,
                              PN_SNCODE          IN NUMBER,
                              PN_EVCODE          IN NUMBER,
                              PN_FEECLASS        IN NUMBER,
                              PN_FUPACKID        IN NUMBER,
                              PN_FUPVERSION      IN NUMBER,
                              PN_FUPSEQ          IN NUMBER,
                              PN_VERSION         IN NUMBER,
                              PN_FREEUNITSNUMBER IN NUMBER,
                              PV_ERROR           OUT VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS MANUEL VERA MVR
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     PROCESO QUE REALIZA LA INSERCION DE UN REGISTRO EN LA TABLA FEES
    */
    LV_PROGRAMA VARCHAR2(65) := 'SPSK_CARGAS_OCC.SPSP_INSERTA_FEES';
  
  BEGIN
    INSERT INTO FEES
      (CUSTOMER_ID,
       SEQNO,
       FEE_TYPE,
       AMOUNT,
       REMARK,
       GLCODE,
       ENTDATE,
       PERIOD,
       USERNAME,
       VALID_FROM,
       JOBCOST,
       BILL_FMT,
       SERVCAT_CODE,
       SERV_CODE,
       SERV_TYPE,
       CO_ID,
       AMOUNT_GROSS,
       CURRENCY,
       GLCODE_DISC,
       JOBCOST_ID_DISC,
       GLCODE_MINCOM,
       JOBCOST_ID_MINCOM,
       REC_VERSION,
       CDR_ID,
       CDR_SUB_ID,
       UDR_BASEPART_ID,
       UDR_CHARGEPART_ID,
       TMCODE,
       VSCODE,
       SPCODE,
       SNCODE,
       EVCODE,
       FEE_CLASS,
       FU_PACK_ID,
       FUP_VERSION,
       FUP_SEQ,
       VERSION,
       FREE_UNITS_NUMBER)
    VALUES
      (PN_CUSTOMER_ID,
       PN_SEQNO,
       PV_FEETYPE,
       PF_AMOUNT,
       PV_REMARK,
       PV_GLCODE,
       PD_ENTDATE,
       PN_PERIOD,
       PV_USERNAME,
       PD_VALIDFROM,
       PN_JOBCOST,
       PV_BILLFMT,
       PV_SERVCATCODE,
       PV_SERVCODE,
       PV_SERVTYPE,
       PN_COID,
       PF_AMOUNTGROSS,
       PN_CURRENCY,
       PV_GLCODEDISC,
       PN_JOBCOSTIDDISC,
       PV_GLCODEMINCOM,
       PN_JOBCOSTIDMINCOM,
       PN_RECVERSION,
       PN_CDRID,
       PN_CDRSUBID,
       PN_UDRBASEPARTID,
       PN_UDRCHARGEPARTID,
       PN_TMCODE,
       PN_VSCODE,
       PN_SPCODE,
       PN_SNCODE,
       PN_EVCODE,
       PN_FEECLASS,
       PN_FUPACKID,
       PN_FUPVERSION,
       PN_FUPSEQ,
       PN_VERSION,
       PN_FREEUNITSNUMBER);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || SUBSTR(SQLERRM, 1, 500);
  END SPSP_INSERTA_FEES;

  PROCEDURE SPSP_OBTIENE_SECUENCIA_FEES(PN_CUSTOMERID IN NUMBER,
                                        PN_SEQNO      OUT NUMBER,
                                        PV_ERROR      OUT VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS MANUEL VERA MVR
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     OBTIENE LA SECUENCIA PARA INSERTAR EN LA FEES PARA EL CUSTOMER_ID INGRESADO
    */
    LV_PROGRAMA VARCHAR2(65) := 'SPSK_CARGAS_OCC.OBTIENE_SECUENCIA_FEES';
  
  BEGIN
    SELECT NVL(MAX(SEQNO), 0) + 1
      INTO PN_SEQNO
      FROM FEES
     WHERE CUSTOMER_ID = PN_CUSTOMERID;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || SUBSTR(SQLERRM, 1, 500);
  END SPSP_OBTIENE_SECUENCIA_FEES;

  PROCEDURE SPSP_OBTIENE_EVCODE(PN_SNCODE IN NUMBER,
                                PN_EVCODE OUT NUMBER,
                                PV_ERROR  OUT VARCHAR2) IS
  
    LV_PROGRAMA VARCHAR2(65) := 'SPSK_CARGAS_OCC.SPSP_OBTIENE_EVCODE';
  
  BEGIN
    SELECT EVCODE INTO PN_EVCODE FROM MPULKEXN WHERE SNCODE = PN_SNCODE;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' -> NO SE ENCONTRARON DATOS EN TABLA MPULKEXN PARA SNCODE: ' ||
                  PN_SNCODE;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || SUBSTR(SQLERRM, 1, 150);
  END SPSP_OBTIENE_EVCODE;

  PROCEDURE SPSP_DESPACHA_CARGA_OCC(PN_ID_EJECUCION NUMBER,
                                    PV_FECHA_INI    VARCHAR2,
                                    PV_FECHA_FIN    VARCHAR2,
                                    PV_ERROR        OUT VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS MANUEL VERA MVR
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     PROCESO QUE LEE DESDE LA TABLA SPS_CARGA_OCC_TMP Y OBTIENE LOS DATOS NECESARIOS
                      PARA INSERTAR EN LA FEES DESDE MPULKTMB, MPULKEXN Y CUSTOMER_ALL.
    */
    /*******************************************************************************************
      PROYECTO:           [6042] PLANES POOL DE SVA/VOZ
      MODIFICADO POR:     CLS DANNY LAMILLA 
      LIDER CLS:          CLS LUIS MU�OZ
      LIDER SIS:          SIS PAOLA CARVAJAL
      FECHA MODIFICACION: 14/08/2012
      COMENTARIO:         Se modifica para verificar si el valor debe insertarse con iva o no
                          en la tabla fees de bscs
    ********************************************************************************************/
    --TIPOS
    TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVARCHAR2000 IS TABLE OF VARCHAR2(2000) INDEX BY BINARY_INTEGER;
    TYPE TVARCHAR24 IS TABLE OF VARCHAR2(24) INDEX BY BINARY_INTEGER;
    TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TVARCHAR100 IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
    TYPE TVARCHAR1 IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER; -- AGREGADO 6042 CLS DLA 14/08/2012
    --CURSORES
    --Barre todos los registros insertados en la tabla SPS_CARGA_OCC_TMP
    CURSOR C_CUENTAS(CN_ID_EJECUCION NUMBER) IS
      SELECT T.ID_CONTRATO,
             T.CODIGO_DOC,
             T.AMOUNT,
             T.SNCODE,
             T.TMCODE,
             T.REMARK,
             T.ENTDATE,
             T.ID_EJECUCION,
             T.USUARIO,
             T.EXENTO, -- AGREGADO 6042 CLS DLA 14/08/2012
             T.ROWID
        FROM SPS_CARGA_OCC_TMP T
       WHERE T.ID_EJECUCION = CN_ID_EJECUCION;
  
    --Obtiene los datos de la tabla MPULKTMB   
    CURSOR C_MPULKTMB(CN_TMCODE NUMBER, CN_SNCODE NUMBER) IS
      SELECT *
        FROM MPULKTMB B
       WHERE TMCODE = CN_TMCODE
         AND SNCODE = CN_SNCODE
         AND VSCODE = (SELECT MAX(VSCODE)
                         FROM MPULKTMB
                        WHERE TMCODE = CN_TMCODE
                          AND SNCODE = CN_SNCODE);
  
    --Obtiene los datos de la customer_all
    CURSOR C_CUSTOMER(CV_CUSTCODE VARCHAR2) IS
      SELECT A.CUSTOMER_ID
        FROM CUSTOMER_ALL A
       WHERE A.CUSTCODE = CV_CUSTCODE;
  
    --VARIABLES
    LTN_CONTRATO     TNUMBER;
    LTV_CODIGO_DOC   TVARCHAR24;
    LTN_AMOUNT       TNUMBER;
    LTN_SNCODE       TNUMBER;
    LTN_TMCODE       TNUMBER;
    LTV_REMARK       TVARCHAR2000;
    LTV_ROWID        TVARCHAR2000;
    LTD_ENTDATE      TDATE;
    LTN_ID_EJECUCION TNUMBER;
    LTV_USUARIO      TVARCHAR100;
    LN_SEQNO         NUMBER;
    LN_EVCODE        NUMBER;
    LV_PROGRAMA      VARCHAR2(50) := 'SPSK_CARGAS_OCC.SPSP_DESPACHA_CARGA_OCC';
    LV_ERROR         VARCHAR2(2000);
    --LV_OBSERVACION   VARCHAR2(2000); -- Comentariado por no usarse 6042 CLS DLA 14/08/2012
    LC_MPULKTMB      C_MPULKTMB%ROWTYPE;
    LC_CUSTOMER      C_CUSTOMER%ROWTYPE;
    LTV_EXENTO       TVARCHAR1; -- AGREGADO 6042 CLS DLA 14/08/2012
    LE_ERROR EXCEPTION;
    LE_INGRESADO EXCEPTION;
    Ln_Iva        number;       --10920 SUD MNE
    Lv_Iva        varchar2(10); --10920 SUD MNE
  
  BEGIN
    --10920 INI SUD MNE
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number (1+Lv_Iva/100);
    --10920 FIN SUD MNE
    --Se obtiene los datos de la tabla temporal SPS_CARGA_OCC_TMP
    OPEN C_CUENTAS(PN_ID_EJECUCION);
    FETCH C_CUENTAS BULK COLLECT
      INTO LTN_CONTRATO,
           LTV_CODIGO_DOC,
           LTN_AMOUNT,
           LTN_SNCODE,
           LTN_TMCODE,
           LTV_REMARK,
           LTD_ENTDATE,
           LTN_ID_EJECUCION,
           LTV_USUARIO,
           LTV_EXENTO, -- AGREGADO 6042 CLS DLA 14/08/2012
           LTV_ROWID;
    --Si encuentra datos procesa estos registros para insertarlos en la FEES.
    IF LTN_CONTRATO.COUNT > 0 THEN
      FOR I IN LTN_CONTRATO.FIRST .. LTN_CONTRATO.LAST LOOP
        BEGIN
          --Obtiene los datos de la tabla MPULKTMB
          OPEN C_MPULKTMB(35, LTN_SNCODE(I));
          FETCH C_MPULKTMB
            INTO LC_MPULKTMB;
          CLOSE C_MPULKTMB;
          
          LC_CUSTOMER:= null;
          --Obtiene datos de la CUSTOMER_ALL
          OPEN C_CUSTOMER(LTV_CODIGO_DOC(I));
          FETCH C_CUSTOMER
            INTO LC_CUSTOMER;
          CLOSE C_CUSTOMER;
        
          --Verifica que ese OCC no se haya cargado ya a la FEES
          SPSP_Valida_Occ_Duplicado(Pn_Customerid => LC_CUSTOMER.CUSTOMER_ID,
                                    Pn_Sncode     => LTN_SNCODE(I),
                                    Pn_Tmcode     => LTN_TMCODE(I),
                                    Pv_Fee_Type   => 'N',
                                    Pn_Period     => 1,
                                    Pv_Remark     => LTV_REMARK(I),
                                    Pv_Error      => LV_ERROR);
        
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_INGRESADO;
          END IF;
        
          --Obtiene datos de la tabla MPULKEXN
          SPSP_OBTIENE_EVCODE(PN_SNCODE => LTN_SNCODE(I),
                              PN_EVCODE => LN_EVCODE,
                              PV_ERROR  => LV_ERROR);
        
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        
          --Obtiene la secuencia para el customer_id
          SPSP_OBTIENE_SECUENCIA_FEES(PN_CUSTOMERID => LC_CUSTOMER.CUSTOMER_ID,
                                      PN_SEQNO      => LN_SEQNO,
                                      PV_ERROR      => LV_ERROR);
        
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
          
          -- INICIO 6042 CLS DLA 14/08/2012
          -- Verificar si el valor debe agregarse el iva
          IF LTV_EXENTO(I) = 'N' THEN
            LTN_AMOUNT(I) := LTN_AMOUNT(I) * Ln_Iva; --10920 SUD MNE
          END IF;
        
          -- FIN 6042 CLS DLA 14/08/2012
          
          --Inserta en la fees
          SPSP_INSERTA_FEES(Pn_Customer_Id     => LC_CUSTOMER.CUSTOMER_ID,
                            Pn_Seqno           => LN_SEQNO,
                            Pv_Feetype         => 'N',
                            Pf_Amount          => LTN_AMOUNT(I), 
                            Pv_Remark          => LTV_REMARK(I),
                            Pv_Glcode          => LC_MPULKTMB.ACCGLCODE,
                            Pd_Entdate         => LTD_ENTDATE(I),
                            Pn_Period          => 1,
                            Pv_Username        => NVL(LTV_USUARIO(I), USER), --USER, --'RTXPOOL',--Usuario
                            Pd_Validfrom       => TO_DATE(PV_FECHA_FIN,
                                                          'DD/MM/YYYY'),
                            Pn_Jobcost         => NULL,
                            Pv_Billfmt         => NULL,
                            Pv_Servcatcode     => LC_MPULKTMB.ACCSERV_CATCODE,
                            Pv_Servcode        => LC_MPULKTMB.ACCSERV_CODE,
                            Pv_Servtype        => LC_MPULKTMB.ACCSERV_TYPE,
                            Pn_Coid            => NULL,
                            Pf_Amountgross     => NULL,
                            Pn_Currency        => 19, --USD
                            Pv_Glcodedisc      => lc_mpulktmb.accglcode_disc,
                            PN_JOBCOSTIDDISC   => lc_mpulktmb.Accjcid_Disc,
                            PV_GLCODEMINCOM    => lc_mpulktmb.Accglcode_Mincom,
                            Pn_Jobcostidmincom => lc_mpulktmb.accjcid_mincom,
                            Pn_Recversion      => 1,
                            Pn_Cdrid           => NULL,
                            Pn_Cdrsubid        => NULL,
                            Pn_Udrbasepartid   => NULL,
                            Pn_Udrchargepartid => NULL,
                            Pn_Tmcode          => 35,
                            Pn_Vscode          => LC_MPULKTMB.VSCODE,
                            Pn_Spcode          => LC_MPULKTMB.SPCODE,
                            Pn_Sncode          => LTN_SNCODE(I),
                            Pn_Evcode          => LN_EVCODE,
                            Pn_Feeclass        => 3,
                            Pn_Fupackid        => NULL,
                            Pn_Fupversion      => NULL,
                            Pn_Fupseq          => NULL,
                            PN_VERSION         => NULL,
                            Pn_Freeunitsnumber => NULL,
                            Pv_Error           => LV_ERROR);
        
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        
          --Si todo el proceso se realizo con exito se elimina de la tabla temporal de carga
          SPSP_ELIMINA_CARGA(LTV_ROWID(I));
        
        EXCEPTION
          WHEN LE_INGRESADO THEN
            --Si el registro ya fue ingresado se le cambia el estado y se ingresa una observacion
            SPSP_ACTUALIZA_CARGA_OCC(PV_ROWID       => LTV_ROWID(I),
                                     PV_OBSERVACION => 'CARGA OCC YA EXISTE EN LA FEES');
          
          --Si el registro ya fue ingresado se lo marca con I
          --SPSP_ELIMINA_CARGA(LTV_ROWID(I));
          WHEN LE_ERROR THEN
            --Si hubo un error al ingresar el registro en la FEES se lo marca con E y se ingresa una observacion
            SPSP_ACTUALIZA_CARGA_OCC(PV_ROWID       => LTV_ROWID(I),
                                     PV_OBSERVACION => 'ERROR AL REALIZAR LA CARGA OCC A LA FEES');
            RAISE LE_ERROR;
        END;
      END LOOP;
    END IF;
    CLOSE C_CUENTAS;
  
    -- Si no hubo errores en la carga occ se actualiza el estado de la ejecucion
    SPSP_ACTUALIZA_EJECUCION(PN_ID_EJECUCION => PN_ID_EJECUCION);
  
    --Se envia a bitacorizar la carga correcta
    SPSP_BIT_EJECUCION_CARGA_OCC(PD_FECHA_INI    => TO_DATE(PV_FECHA_INI,
                                                            'DD/MM/YYYY'),
                                 PD_FECHA_FIN    => TO_DATE(PV_FECHA_FIN,
                                                            'DD/MM/YYYY'),
                                 PV_OBSERVACION  => 'PROCESO EJECUTADO CON EXITO',
                                 PV_ESTADO       => 'T',
                                 PN_ID_EJECUCION => PN_ID_EJECUCION);
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || LV_ERROR;
      --Se envia a bitacorizar la carga por error
      SPSP_BIT_EJECUCION_CARGA_OCC(PD_FECHA_INI    => TO_DATE(PV_FECHA_INI,
                                                              'DD/MM/YYYY'),
                                   PD_FECHA_FIN    => TO_DATE(PV_FECHA_FIN,
                                                              'DD/MM/YYYY'),
                                   PV_OBSERVACION  => PV_ERROR,
                                   PV_ESTADO       => 'E',
                                   PN_ID_EJECUCION => PN_ID_EJECUCION);
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || SUBSTR(SQLERRM, 1, 500);
      --Si hubo un error se envia a actualizar los estados de la ejecucion de la tabla temporal de carga occ
      SPSP_ACTUALIZA_CARGA_OCC(PN_ID_EJECUCION => PN_ID_EJECUCION,
                               PV_OBSERVACION  => 'ERROR: ' ||
                                                  SUBSTR(SQLERRM, 1, 500));
      --Se envia a bitacorizar la carga por error
      SPSP_BIT_EJECUCION_CARGA_OCC(PD_FECHA_INI    => TO_DATE(PV_FECHA_INI,
                                                              'DD/MM/YYYY'),
                                   PD_FECHA_FIN    => TO_DATE(PV_FECHA_FIN,
                                                              'DD/MM/YYYY'),
                                   PV_OBSERVACION  => PV_ERROR,
                                   PV_ESTADO       => 'E',
                                   PN_ID_EJECUCION => PN_ID_EJECUCION);
  END SPSP_DESPACHA_CARGA_OCC;

  PROCEDURE SPSP_VALIDA_OCC_DUPLICADO(PN_CUSTOMERID IN NUMBER,
                                      PN_SNCODE     IN NUMBER,
                                      PN_TMCODE     IN NUMBER,
                                      PV_FEE_TYPE   IN VARCHAR2,
                                      PN_PERIOD     IN NUMBER,
                                      PV_REMARK     IN VARCHAR2,
                                      PV_ERROR      OUT VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS MANUEL VERA MVR
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     PROCESO QUE VERIFICA SI LA CARGA OCC QUE SE VA A REALIZAR YA FUE PREVIAMENTE REALIZADA.
    */
    LV_DUPLICADO VARCHAR2(1);
    LV_PROGRAMA  VARCHAR2(65) := 'SPSK_CARGAS_OCC.SPSP_VALIDA_OCC_DUPLICADO';
  BEGIN
    SELECT 'X'
      INTO LV_DUPLICADO
      FROM FEES
     WHERE CUSTOMER_ID = PN_CUSTOMERID
       AND SNCODE = PN_SNCODE
       AND TMCODE = PN_TMCODE
       AND FEE_TYPE = PV_FEE_TYPE
       AND PERIOD = PN_PERIOD
       AND UPPER(TRIM(REMARK)) = UPPER(TRIM(PV_REMARK));
    IF LV_DUPLICADO IS NOT NULL THEN
      PV_ERROR := LV_PROGRAMA ||
                  ': EL OCC YA SE ENCUENTRA CARGADO EN LA TABLA FEES';
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' -> ' || SUBSTR(SQLERRM, 1, 150);
  END SPSP_VALIDA_OCC_DUPLICADO;

  PROCEDURE SPSP_ELIMINA_CARGA(PV_ROWID VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS MANUEL VERA MVR
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     PROCESO QUE ELIMINA UN REGISTRO DE LA TABLA SPS_CARGA_OCC_TMP
    */
  BEGIN
    DELETE SPS_CARGA_OCC_TMP T WHERE T.ROWID = PV_ROWID;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END SPSP_ELIMINA_CARGA;

  PROCEDURE SPSP_ACTUALIZA_EJECUCION(PN_ID_EJECUCION NUMBER) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS DUSTIN RODRIGUEZ
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 01/01/2012
      COMENTARIO:     PROCESO QUE ACTUALIZA EL ESTADO DE LA EJECUCION EN LA TABLA SPS_CARGA_EJECUCION
    */
  BEGIN
    UPDATE SPS_CARGA_EJECUCION E
       SET E.ESTADO = 'P'
     WHERE E.ID_EJECUCION = PN_ID_EJECUCION;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END SPSP_ACTUALIZA_EJECUCION;

  PROCEDURE SPSP_ACTUALIZA_CARGA_OCC(PV_ROWID        VARCHAR2 DEFAULT NULL,
                                     PN_ID_EJECUCION NUMBER DEFAULT NULL,
                                     PV_OBSERVACION  VARCHAR2) IS
    /*
      PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS DUSTIN RODRIGUEZ
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 08/05/2012
      COMENTARIO:     PROCESO QUE ACTUALIZA EL ESTADO Y OBSERVACION DE LA TABLA SPS_CARGA_OCC_TMP
    */
  BEGIN
    UPDATE SPS_CARGA_OCC_TMP E
       SET E.ESTADO = 'E', E.OBSERVACION = PV_OBSERVACION
     WHERE E.ROWID = PV_ROWID
        OR E.ID_EJECUCION = PN_ID_EJECUCION;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END SPSP_ACTUALIZA_CARGA_OCC;

  PROCEDURE SPSP_BIT_EJECUCION_CARGA_OCC(PD_FECHA_INI    DATE,
                                         PD_FECHA_FIN    DATE,
                                         PV_OBSERVACION  VARCHAR2,
                                         PV_ESTADO       VARCHAR2,
                                         PN_ID_EJECUCION NUMBER) IS
    /*
    PROYECTO:       6042 PLANES POOL SVA
      CREADO POR:     CLS DUSTIN RODRIGUEZ
      LIDER:          CLS LUIS MU�OZ
                      SIS PAOLA CARVAJAL
      FECHA CREACION: 27/04/2012
      COMENTARIO:     PROCESO QUE BITACORIZA LA CARGA OCC CON ESTADOS:
                      T=TERMINADO
                      E=ERROR
    */
  
  BEGIN
    INSERT INTO SPS_BITACORA_CARGA_OCC
      (FECHA_CONSUMO_INI,
       FECHA_CONSUMO_FIN,
       OBSERVACION,
       ESTADO,
       ID_EJECUCION,
       FECHA_REGISTRO)
    VALUES
      (PD_FECHA_INI,
       PD_FECHA_FIN,
       PV_OBSERVACION,
       PV_ESTADO,
       PN_ID_EJECUCION,
       SYSDATE);
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END SPSP_BIT_EJECUCION_CARGA_OCC;
END SPSK_CARGAS_OCC;
/
