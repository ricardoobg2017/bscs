create or replace package CNK_PRO_CARGA_BONO_ASES is

  ------------------------------------------------
  -- Author  : PAOLA CARRASCO     
  -- Created : 08/02/2007 14:01:47
  -- Purpose : Carga los valores facturados
  ------------------------------------------------
  PROCEDURE CNP_CARGA_FACTURA(PV_ANIO      IN VARCHAR2,
                              PV_MES       IN VARCHAR2,
                              PV_MSG_ERROR IN OUT VARCHAR2);
                              
  PROCEDURE CNP_BUSCA_TABLA(PD_FECHA         IN DATE,
                            PV_SENTENCIA     IN OUT VARCHAR2,
                            PV_MSG_ERROR     IN OUT VARCHAR2);
                            
  PROCEDURE CNP_COPIA_FACTURADO(PV_ANIO IN VARCHAR2,
                                PV_MES  IN VARCHAR2,
                                PV_MSG_ERROR IN OUT VARCHAR2);

end CNK_PRO_CARGA_BONO_ASES;
/
CREATE OR REPLACE PACKAGE BODY CNK_PRO_CARGA_BONO_ASES IS

  ------------------------------------------------
  -- Author  : PAOLA CARRASCO
  -- Created : 08/02/2007 14:01:47
  -- Purpose : Carga los valores facturados
  ------------------------------------------------
  --=====================================================================================--
  -- Modificado por:   CIMA - PILAR BAQUERIZO M.
  -- Fecha :           01/04/2010
  -- Proyecto:         [4918] Implantaci�n de SCP en procesos Cr�ticos.
  --=====================================================================================--

   PROCEDURE CNP_CARGA_FACTURA(PV_ANIO IN VARCHAR2,
                              PV_MES  IN VARCHAR2,
                              PV_MSG_ERROR IN OUT VARCHAR2) IS

        CURSOR C_ASE_FACT IS
          SELECT CODIGO_DOC, --VARCHAR
                 ID_SECUENCIA,--NUMBER
                 ID_FUERZA_VENTA --NUMBER
          FROM CN_ASESORES_FACTURAS 
          WHERE ANIO= PV_ANIO
          AND MES= PV_MES;
         -- AND FACTURADO IS NULL;        

       CURSOR C_CONT_DATOS IS
              SELECT COUNT(*)
              FROM CN_ASESORES_FACTURAS AF
              WHERE AF.ANIO= PV_ANIO
              AND AF.MES= PV_MES;
          --AND AF.FACTURADO IS NULL;
              
       -- verifica esta declaracion, se debe hacer por el tipo de datos de base.
       TYPE GT_CODIGO_DOC IS TABLE OF CN_ASESORES_FACTURAS.CODIGO_DOC%TYPE INDEX BY BINARY_INTEGER;
       TYPE GT_SECUENCIA  IS TABLE OF CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE INDEX BY BINARY_INTEGER;
       TYPE GT_FUERZA_VENTA IS TABLE OF CN_ASESORES_FACTURAS.ID_FUERZA_VENTA%TYPE INDEX BY BINARY_INTEGER;
       
       LE_ERROR           EXCEPTION;
       LN_CONT            NUMBER;
       LV_SENTENCIA       VARCHAR2(30000);
       LD_FECHA           DATE;
       LN_CONSUMO_TOTAL   NUMBER  :=0;
       LV_MSG_ERROR       VARCHAR2(1000);
       LN_CURSOR          INTEGER;
       LN_VOID            INTEGER;
       LN_ROWS            INTEGER;
       LN_CONSUMO         NUMBER;
       
       -- jes - mira bien las declaraciones
       LT_CODIGO          GT_CODIGO_DOC;
       LT_SECUENCIA       GT_SECUENCIA;
       LT_FUERZA          GT_FUERZA_VENTA;
       LN_INDICE          NUMBER;
       LN_TRACE           NUMBER:=50;
       LN_CONTADOR        NUMBER:=0;
       LV_ACTUALIZAR      VARCHAR2(1000);
       
       --[4918] INICIO PBM
       LN_ID_PROCESO_EJECUCION     NUMBER;
       LV_ID_PROCESO               VARCHAR2(3) := 'C95'; /* C95 CARGA DE FACTURA */
       LV_REVERSO                  VARCHAR2(200) := ''; 
       LV_NIVEL_MENSAJE            VARCHAR2(1) := 'E';
       LN_PROCESADOS               NUMBER := 0;
       LV_ERROR_ALT                VARCHAR2(2000);      
       --[4918] FIN PBM
       
  BEGIN
  
        --[4918] INICIO PBM
        -- INICIA BITACORA
        SCT.CNK_TRX_PROCESO.CNP_INICIA_PROCESO2@OPER(LN_ID_PROCESO_EJECUCION,
                                            LV_ID_PROCESO,
                                            'PROCESO DE CARGA DE FACTURA',
                                            USER,
                                            0,
                                            LV_REVERSO,
                                            LV_MSG_ERROR);
        IF LV_MSG_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
        END IF;    
        
        -- INFORMACION DE LA SESION.
        SCT.CNK_API_SISTEMA.ACCION_SESSION@OPER('INICIO PROCESO',
                                            'CARGA DE FACTURA',
                                            LV_ID_PROCESO);         
        --[4918] FIN PBM
        
        
       OPEN  C_CONT_DATOS;
       FETCH C_CONT_DATOS INTO LN_CONT;
       CLOSE C_CONT_DATOS;

       IF LN_CONT = 0 THEN
          LV_MSG_ERROR := 'FACTURA PROCESADA';
          RAISE LE_ERROR;
          
       END IF;

       LD_FECHA:= TO_DATE('01/'||PV_MES||'/'||PV_ANIO,'DD/MM/YYYY');

       CNP_BUSCA_TABLA(LD_FECHA, LV_SENTENCIA, LV_MSG_ERROR);
       IF LV_MSG_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
       
       -- asi se debe recuperar los datos
       OPEN  C_ASE_FACT;
       FETCH C_ASE_FACT BULK COLLECT INTO LT_CODIGO, LT_SECUENCIA, LT_FUERZA;
       CLOSE C_ASE_FACT;
   
      LN_INDICE:= LT_CODIGO.FIRST;
      WHILE LN_INDICE IS NOT NULL LOOP
          
          LN_PROCESADOS:= LN_PROCESADOS + 1; --[4918]  PBM
      
          LN_CONSUMO_TOTAL:=0;
          LN_CURSOR := DBMS_SQL.OPEN_CURSOR;
          DBMS_SQL.PARSE(LN_CURSOR, LV_SENTENCIA, DBMS_SQL.V7);
          DBMS_SQL.BIND_VARIABLE(LN_CURSOR,':PV_CUENTA', LT_CODIGO(LN_INDICE));
          DBMS_SQL.DEFINE_COLUMN(LN_CURSOR, 1, LN_CONSUMO);
        
          LN_VOID := DBMS_SQL.EXECUTE(LN_CURSOR);
          LOOP
               LN_ROWS:= DBMS_SQL.FETCH_ROWS(LN_CURSOR);
               IF LN_ROWS > 0 THEN
                   BEGIN
                       DBMS_SQL.COLUMN_VALUE(LN_CURSOR, 1, LN_CONSUMO);
                       LN_CONSUMO_TOTAL:= LN_CONSUMO_TOTAL + LN_CONSUMO;
                   EXCEPTION
                       WHEN OTHERS THEN
                            NULL;
                   END;
               ELSE
                    DBMS_SQL.CLOSE_CURSOR(LN_CURSOR);
                    EXIT;
               END IF;
          END LOOP;

          CNK_OBJ_ASESORES_FACT.CNP_ACTUALIZAR(LT_SECUENCIA(LN_INDICE),
                                               NULL,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NULL,
                                               NVL(LN_CONSUMO_TOTAL, 0),
                                               LV_MSG_ERROR);
                                  
          IF LV_MSG_ERROR IS NOT NULL THEN
             RAISE LE_ERROR;
          END IF;

          LN_CONTADOR:= LN_CONTADOR +1;
          
          IF LN_CONTADOR= LN_TRACE THEN
             COMMIT;
             LN_CONTADOR:= 0;
          END IF; 
          LN_INDICE:= LT_CODIGO.NEXT(LN_INDICE);
      END LOOP;
       
       LT_CODIGO.DELETE; 
       LT_SECUENCIA.DELETE;
       LT_FUERZA.DELETE;
       
       CNP_COPIA_FACTURADO(PV_ANIO, PV_MES, LV_MSG_ERROR);
       IF LV_MSG_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
       END IF;
    
      --[4918] INICIO PBM   
      -- FINALIZA EL PROCESO.
      SCT.CNK_TRX_PROCESO.CNP_FINALIZA_PROCESO@OPER(LN_ID_PROCESO_EJECUCION,
                                                              LN_PROCESADOS,
                                                              LV_MSG_ERROR);
      IF LV_MSG_ERROR IS NOT NULL THEN
         RAISE LE_ERROR;
      END IF;
    
      -- INFORMACION DE LA SESION.
      SCT.CNK_API_SISTEMA.ACCION_SESSION@OPER('FIN PROCESO',
                                                        'CARGA DE FACTURA',
                                                        LV_ID_PROCESO);
      --[4918] FIN PBM
       
      COMMIT;
    
     --[4918] INICIO PBM 
     -- CIERRA DBLINKS.      
          BEGIN
              dbms_session.close_database_link('OPER');
          EXCEPTION
          WHEN OTHERS THEN
              NULL;
          END;     
          COMMIT;          
     --[4918] FIN PBM
    
    
   EXCEPTION
      WHEN LE_ERROR THEN
           PV_MSG_ERROR:= LV_MSG_ERROR;
           
           --[4918] INICIO PBM
           --MENSAJE BITACORA
          SCT.CNK_TRX_PROCESO.CNP_REGISTRA_MENSAJE@OPER(PN_ID_PROCESO_EJECUCION => LN_ID_PROCESO_EJECUCION,
                                               PV_MENSAJE              => PV_MSG_ERROR,
                                               PV_TIPO_MENSAJE         => 'E',
                                               PV_NIVEL_MENSAJE        => LV_NIVEL_MENSAJE,
                                               PV_MSG_ERROR            => LV_ERROR_ALT );
           COMMIT;
           -- CIERRA DBLINKS.      
           BEGIN
              dbms_session.close_database_link('OPER');
           EXCEPTION
           WHEN OTHERS THEN
              NULL;
           END;     
           COMMIT; 
           --[4918] FIN PBM
         
      WHEN OTHERS THEN
           PV_MSG_ERROR:= 'Error General al procesar los datos.  '|| SQLERRM ||'. ' || LV_MSG_ERROR;
         
           --[4918] INICIO PBM
           --MENSAJE BITACORA
           SCT.CNK_TRX_PROCESO.CNP_REGISTRA_MENSAJE@OPER(PN_ID_PROCESO_EJECUCION => LN_ID_PROCESO_EJECUCION,
                                               PV_MENSAJE              => SUBSTR(SQLERRM,1,1000),
                                               PV_TIPO_MENSAJE         => 'E',
                                               PV_NIVEL_MENSAJE        => LV_NIVEL_MENSAJE,
                                               PV_MSG_ERROR            => LV_ERROR_ALT );
           COMMIT;
           -- CIERRA DBLINKS.      
           BEGIN
              dbms_session.close_database_link('OPER');
           EXCEPTION
           WHEN OTHERS THEN
              NULL;
           END;     
           COMMIT; 
           --[4918] FIN PBM
           
  END;
  
  --*********************************************************************************
  -- DESARROLLADO POR: PAOLA CARRASCO IDROVO.
  --*********************************************************************************
  PROCEDURE CNP_BUSCA_TABLA(PD_FECHA         IN DATE,
                            PV_SENTENCIA     IN OUT VARCHAR2,
                            PV_MSG_ERROR     IN OUT VARCHAR2) AS

      CURSOR LC_TABLA( CV_TABLA VARCHAR2) IS
            SELECT COUNT(OBJECT_NAME) CONT
            FROM ALL_OBJECTS
            WHERE OBJECT_NAME = CV_TABLA
            AND OWNER = 'SYSADM'
            AND OBJECT_TYPE = 'TABLE';
      
      LN_CONT         NUMBER;
      LD_FECHA_FIN    DATE;
      LV_NOMBRE_TABLA VARCHAR2(25);
      LD_FECHA        DATE;
      LB_PRIMERO      BOOLEAN:= TRUE;
      LE_ERROR        EXCEPTION;
      LV_FECHA_TABLA  VARCHAR2(10);
      LV_SENTENCIA    VARCHAR2(30000);    
      
      
  BEGIN
       LD_FECHA_FIN := LAST_DAY(PD_FECHA);
       LD_FECHA     := PD_FECHA;
       
       --BUSCAR LAS TABLAS QUE SE HAN CREADO EN EL MES A PROCESAR
       WHILE LD_FECHA <= LD_FECHA_FIN LOOP
             LV_FECHA_TABLA  := TO_CHAR(LD_FECHA,'DDMMYYYY');
             LV_NOMBRE_TABLA := 'CO_REPCARCLI_' || LV_FECHA_TABLA;

             OPEN  LC_TABLA(LV_NOMBRE_TABLA);
             FETCH LC_TABLA INTO LN_CONT;
             CLOSE LC_TABLA;

             IF LN_CONT= 1 THEN
                IF LB_PRIMERO THEN
                   LB_PRIMERO := FALSE;
                ELSE
                   LV_SENTENCIA := LV_SENTENCIA || ' UNION ALL ';
                END IF;
                
                LV_SENTENCIA := LV_SENTENCIA ||
                                'SELECT BALANCE_12 VALOR_FINAL'||
                                ' FROM ' ||LV_NOMBRE_TABLA ||
                                ' WHERE CUENTA = :PV_CUENTA ';                    
             END IF;
             LD_FECHA:= LD_FECHA + 1;
       END LOOP;
       PV_SENTENCIA:= LV_SENTENCIA;

       IF PV_SENTENCIA IS NULL THEN
          RAISE LE_ERROR;
       END IF;

    EXCEPTION 
        WHEN LE_ERROR THEN
             PV_MSG_ERROR:= 'No existen tablas para la fecha: ' || PD_FECHA;
        WHEN OTHERS THEN
             PV_MSG_ERROR := 'ERROR GENERAL AL VERIFICAR TABLA '|| PD_FECHA ||': '||SQLERRM;
    END;
    
  --*********************************************************************************
  -- DESARROLLADO POR: PAOLA CARRASCO I.
  --*********************************************************************************
    PROCEDURE CNP_COPIA_FACTURADO(PV_ANIO IN VARCHAR2,
                                  PV_MES  IN VARCHAR2,
                                  PV_MSG_ERROR IN OUT VARCHAR2) IS
       CURSOR C_CONT_FACTURADO IS
              SELECT COUNT(*)
              FROM CN_ASESORES_FACTURAS AF
              WHERE AF.ANIO= PV_ANIO
              AND AF.MES= PV_MES
              AND AF.FACTURADO IS NOT NULL;
              
       CURSOR C_FACTURADO IS
              SELECT AF.ID_SECUENCIA, AF.FACTURADO
              FROM CN_ASESORES_FACTURAS AF
              WHERE AF.ANIO= PV_ANIO
              AND AF.MES= PV_MES
              AND AF.FACTURADO IS NOT NULL;
              
       TYPE GT_FACTURADO IS TABLE OF CN_ASESORES_FACTURAS.FACTURADO%TYPE INDEX BY BINARY_INTEGER;
       TYPE GT_SECUENCIA  IS TABLE OF CN_ASESORES_FACTURAS.ID_SECUENCIA%TYPE INDEX BY BINARY_INTEGER;
              
       LN_CONT      NUMBER:=0;
       LV_MSG_ERROR VARCHAR2(200);
       LE_ERROR     EXCEPTION;
       
       LT_FACTURADO       GT_FACTURADO;
       LT_SECUENCIA       GT_SECUENCIA;
       LV_SENTENCIA       VARCHAR2(10000);
       
       LN_INDICE          NUMBER:=0;
       
    BEGIN
       OPEN  C_CONT_FACTURADO;
       FETCH C_CONT_FACTURADO INTO LN_CONT;
       CLOSE C_CONT_FACTURADO;
       
       IF LN_CONT = 0 THEN
          LV_MSG_ERROR:= 'No esta procesado el valor facturado para el a�o: '|| PV_ANIO;
          RAISE LE_ERROR;
       END IF;
       
       OPEN  C_FACTURADO;
       FETCH C_FACTURADO BULK COLLECT INTO LT_SECUENCIA, LT_FACTURADO;
       CLOSE C_FACTURADO;
       
       LV_SENTENCIA := 'begin
                          UPDATE CN_ASESORES_FACTURAS@AXIS NOLOGGING SET FACTURADO = :1  WHERE ID_SECUENCIA= :2;
                        end;';
       
       LN_INDICE:= LT_SECUENCIA.FIRST;
       WHILE LN_INDICE IS NOT NULL LOOP
           -- verifica los permisos con la tabla en axis.  
           execute immediate LV_SENTENCIA USING IN LT_FACTURADO(LN_INDICE), IN LT_SECUENCIA(LN_INDICE);
           LN_INDICE:= LT_SECUENCIA.NEXT(LN_INDICE);
       END LOOP;
       
       LT_SECUENCIA.DELETE;
       LT_FACTURADO.DELETE;
       
    EXCEPTION
       WHEN LE_ERROR THEN
            PV_MSG_ERROR:= LV_MSG_ERROR;
       WHEN OTHERS THEN
            PV_MSG_ERROR:= 'Error general: '|| SQLERRM || '. ' || LV_MSG_ERROR;
    END;
END CNK_PRO_CARGA_BONO_ASES;
/
