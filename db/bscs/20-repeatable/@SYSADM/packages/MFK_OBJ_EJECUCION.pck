CREATE OR REPLACE PACKAGE MFK_OBJ_EJECUCION
 /* 
 ||   Name       : MFK_OBJ_EJECUCION
 ||   Created on : 28-JULIO-2011 Cima MAB
 ||   Comments   : Este paquete contiene los procedimientos para la 
                   ejecucion de envio automatico de envio masivo de SMS.
      Motivo     : [6092]Mejoras al M�dulo de Env�o de SMS Masivos
 */ 
 IS 
 /*HELP 
   Overview of MFK_OBJ_CRITERIO_X_ENVIOS
   HELP*/ 
 
 /*EXAMPLES 
   Examples of test 
   EXAMPLES*/ 
 
 /* Constants */ 
 
 /* Exceptions */ 
 
 /* Variables */ 
-- MFK_OBJ_CRITERIO_X_ENVIOS
 /* Toggles */ 
 
 /* Windows */ 
 
 /* Programs */ 

--
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL); 


PROCEDURE MFP_INSERTAR_EJECUCION(PN_IDENVIO             IN MF_TIME_EJECUCION.IDENVIO%TYPE,
                                 PD_DIAEJECUCION        IN MF_TIME_EJECUCION.DIAEJECUCION%TYPE,
                                 PN_HORAMAXEJECUCION    IN MF_TIME_EJECUCION.HORAMAXEJECUCION%TYPE,
                                 PN_MINUTOSMAXEJECUCION IN MF_TIME_EJECUCION.MINUTOSMAXEJECUCION%TYPE,
                                 PV_PERIODOEJECUCION    IN MF_TIME_EJECUCION.PERIODOEJECUCION%TYPE,
                                 PN_NUMEROSMSMAXENVIO   IN MF_TIME_EJECUCION.NUMEROSMSMAXENVIO%TYPE,
                                 PV_ESTADO              IN VARCHAR2,
                                 PV_ERROR               OUT VARCHAR2);
                                 
                                 
PROCEDURE MFP_ACTUALIZA_EJECUCION(PN_IDENVIO             IN MF_TIME_EJECUCION.IDENVIO%TYPE,
                                  PD_DIAEJECUCION        IN MF_TIME_EJECUCION.DIAEJECUCION%TYPE,
                                  PN_HORAMAXEJECUCION    IN MF_TIME_EJECUCION.HORAMAXEJECUCION%TYPE,
                                  PN_MINUTOSMAXEJECUCION IN MF_TIME_EJECUCION.MINUTOSMAXEJECUCION%TYPE,
                                  PV_PERIODOEJECUCION    IN MF_TIME_EJECUCION.PERIODOEJECUCION%TYPE,
                                  PN_NUMEROSMSMAXENVIO   IN MF_TIME_EJECUCION.NUMEROSMSMAXENVIO%TYPE,
                                  PV_ESTADO              IN VARCHAR2,
                                  PV_ERROR               IN OUT VARCHAR2);                                 
                      
   
PROCEDURE MFP_ELIMINA_EJECUCION(PN_IDENVIO    IN MF_ENVIOS.IDENVIO%TYPE,   
                                PN_IDCRITERIO IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                PN_SECUENCIA  IN MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                                PV_ERROR      IN OUT VARCHAR2);
                                 
                
PROCEDURE MFP_INSERTAR_DIAS(PN_IDENVIO     IN   MF_DIAS_SEMANA.IDENVIO%TYPE,
                            PV_DIA         IN  MF_DIAS_SEMANA.DIA%TYPE,
                            PV_MSG_ERROR   IN  OUT VARCHAR2);


END MFK_OBJ_EJECUCION;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_EJECUCION
 /* 
 ||   Name       : MFK_OBJ_EJECUCION
 ||   Created on : 28-JULIO-2011 Cima MAB
 ||   Comments   : Este paquete contiene los procedimientos para la 
                   ejecucion de envio automatico de envio masivo de SMS.
      Motivo     : [6092]Mejoras al M�dulo de Env�o de SMS Masivos
 */ 
 IS 
PROCEDURE MFP_HELP (context_in IN VARCHAR2 := NULL) is 
BEGIN
 NULL;
 END MFP_HELP; 
  /* 
 ||   Name       : MFP_INSERTAR_EJECUCION
 ||   Created on : 11-JULIO-2011
 ||   Creado por : Cima MARIUXI BUENAVENTURA
 ||   [6092] Envio Masivo SMS.
 */

PROCEDURE MFP_INSERTAR_EJECUCION(PN_IDENVIO             IN MF_TIME_EJECUCION.IDENVIO%TYPE,
                                 PD_DIAEJECUCION        IN MF_TIME_EJECUCION.DIAEJECUCION%TYPE,
                                 PN_HORAMAXEJECUCION    IN MF_TIME_EJECUCION.HORAMAXEJECUCION%TYPE,
                                 PN_MINUTOSMAXEJECUCION IN MF_TIME_EJECUCION.MINUTOSMAXEJECUCION%TYPE,
                                 PV_PERIODOEJECUCION    IN MF_TIME_EJECUCION.PERIODOEJECUCION%TYPE,
                                 PN_NUMEROSMSMAXENVIO   IN MF_TIME_EJECUCION.NUMEROSMSMAXENVIO%TYPE,
                                 PV_ESTADO              IN VARCHAR2,
                                 PV_ERROR               OUT VARCHAR2)
                                 
 IS
   LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_INSERTAR_EJECUCION';
   LV_ESTADO     MF_TIME_EJECUCION.ESTADO%TYPE := 'A';
   LE_ERROR      EXCEPTION;
   LV_ERROR      VARCHAR2(1000);
BEGIN

 PV_ERROR := NULL; 

 INSERT INTO MF_TIME_EJECUCION(IDENVIO,           
                               DIAEJECUCION,
                               HORAMAXEJECUCION, 
                               MINUTOSMAXEJECUCION,
                               PERIODOEJECUCION,
                               NUMEROSMSMAXENVIO,
                               ESTADO)
                       VALUES (PN_IDENVIO,           
                               PD_DIAEJECUCION,
                               PN_HORAMAXEJECUCION, 
                               PN_MINUTOSMAXEJECUCION,
                               PV_PERIODOEJECUCION,
                               PN_NUMEROSMSMAXENVIO,
                               LV_ESTADO);

EXCEPTION -- Exception 
WHEN LE_ERROR THEN
    PV_ERROR  := LV_ERROR;
WHEN OTHERS THEN
    PV_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    
END MFP_INSERTAR_EJECUCION;


---------ACTUALIZA EJECUCION-----------
  /* 
 ||   Name       : MFP_INSERTAR_EJECUCION
 ||   Created on : 11-JULIO-2011
 ||   Creado por : Cima MARIUXI BUENAVENTURA
 ||   [6092] Envio Masivo SMS.
 */ 
PROCEDURE MFP_ACTUALIZA_EJECUCION(PN_IDENVIO             IN MF_TIME_EJECUCION.IDENVIO%TYPE,
                                  PD_DIAEJECUCION        IN MF_TIME_EJECUCION.DIAEJECUCION%TYPE,
                                  PN_HORAMAXEJECUCION    IN MF_TIME_EJECUCION.HORAMAXEJECUCION%TYPE,
                                  PN_MINUTOSMAXEJECUCION IN MF_TIME_EJECUCION.MINUTOSMAXEJECUCION%TYPE,
                                  PV_PERIODOEJECUCION    IN MF_TIME_EJECUCION.PERIODOEJECUCION%TYPE,
                                  PN_NUMEROSMSMAXENVIO   IN MF_TIME_EJECUCION.NUMEROSMSMAXENVIO%TYPE,
                                  PV_ESTADO              IN VARCHAR2,
                                  PV_ERROR              IN OUT VARCHAR2)
                                  
AS  

 CURSOR C_table_cur(Cn_idEnvio     number) IS 
        
   SELECT *
     FROM MF_TIME_EJECUCION TE
    WHERE TE.IDENVIO=Cn_idEnvio
      AND TE.ESTADO='A';  
  
   Lc_CurrentRow     MF_TIME_EJECUCION%ROWTYPE ;
   Le_NoDataUpdated  EXCEPTION;
   LE_ERROR          EXCEPTION;
   LB_FOUND          BOOLEAN:= FALSE;
   LV_ERROR         VARCHAR2(1000);
   LV_APLICACION    VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_ACTUALIZA_EJECUCION';
BEGIN

 PV_ERROR := NULL; 
 OPEN C_table_cur(PN_IDENVIO) ;
 FETCH C_table_cur INTO Lc_CurrentRow ;
 LB_FOUND := C_table_cur%FOUND;
 CLOSE C_table_cur ;
 IF LB_FOUND THEN
   UPDATE MF_TIME_EJECUCION 
      SET DIAEJECUCION = NVL( PD_DIAEJECUCION, DIAEJECUCION),
          HORAMAXEJECUCION = NVL(PN_HORAMAXEJECUCION, HORAMAXEJECUCION),
          MINUTOSMAXEJECUCION= NVL(MINUTOSMAXEJECUCION, MINUTOSMAXEJECUCION),
          PERIODOEJECUCION = NVL(PV_PERIODOEJECUCION, PERIODOEJECUCION),
          NUMEROSMSMAXENVIO = NVL(PN_NUMEROSMSMAXENVIO, NUMEROSMSMAXENVIO)
    WHERE idEnvio = PN_IDENVIO
      AND estado = 'A';
 ELSE
   MFP_INSERTAR_EJECUCION(PN_IDENVIO,
                          PD_DIAEJECUCION,
                          PN_HORAMAXEJECUCION,
                          PN_MINUTOSMAXEJECUCION,
                          PV_PERIODOEJECUCION,
                          PN_NUMEROSMSMAXENVIO,
                          PV_ESTADO,
                          LV_ERROR);
   IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
   END IF;                       
 END IF;
  
  EXCEPTION 
  WHEN LE_ERROR THEN
      PV_ERROR  := LV_ERROR;
  WHEN Le_NoDataUpdated THEN
      PV_ERROR:= 'No se encontro el registro que desea actualizar. '||SQLERRM||'. '||LV_APLICACION; 
  WHEN OTHERS THEN
      PV_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
END MFP_ACTUALIZA_EJECUCION;
--_____________________________________________________

  /* 
 ||   Name       : MFP_INSERTAR_EJECUCION
 ||   Created on : 11-JULIO-2011
 ||   Creado por : Cima MARIUXI BUENAVENTURA
 ||   [6092] Envio Masivo SMS.
 */
PROCEDURE MFP_ELIMINA_EJECUCION(PN_IDENVIO    IN MF_ENVIOS.IDENVIO%TYPE,   
                                PN_IDCRITERIO IN MF_CRITERIOS_X_ENVIOS.IDCRITERIO%TYPE,
                                PN_SECUENCIA  IN MF_CRITERIOS_X_ENVIOS.SECUENCIA%TYPE,
                                PV_ERROR      IN OUT VARCHAR2)AS
    
    CURSOR C_table_cur(Cn_idEnvio number) IS 
     SELECT * FROM MF_TIME_EJECUCION TE
      WHERE TE.IDENVIO = Cn_idEnvio
        AND TE.ESTADO  = 'A';
     
 Lc_CurrentRow     MF_TIME_EJECUCION%ROWTYPE ;
 Le_NoDataDeleted  EXCEPTION;
 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_ELIMINAR';
BEGIN    
    UPDATE MF_TIME_EJECUCION E
       SET E.ESTADO='I'
     WHERE E.IDENVIO=PN_IDENVIO; 
    
    UPDATE MF_CRITERIOS_X_ENVIOS SET ESTADO = 'I'
     WHERE IDENVIO =PN_IDENVIO 
       AND idcriterio = PN_IDCRITERIO
       AND SECUENCIA = PN_SECUENCIA;
    
   EXCEPTION 
    WHEN OTHERS THEN
         PV_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 
    CLOSE C_table_cur ;                         
END  MFP_ELIMINA_EJECUCION;   
   
--Cima MAB
--[6092] Envio Masivo
--27/07/11
PROCEDURE MFP_INSERTAR_DIAS(PN_IDENVIO   IN   MF_DIAS_SEMANA.IDENVIO%TYPE,
                            PV_DIA       IN  MF_DIAS_SEMANA.DIA%TYPE,
                            PV_MSG_ERROR IN  OUT VARCHAR2) 
IS  



 LV_APLICACION VARCHAR2(100) := 'MFK_OBJ_CRITERIO_X_ENVIOS.MFP_INSERTAR_DIAS';              
 LE_ERROR              EXCEPTION;
 LV_ERROR         VARCHAR2(1000);
 
BEGIN
 PV_MSG_ERROR := NULL; 

   INSERT INTO MF_DIAS_SEMANA(IDENVIO,
                              DIA)
                      VALUES (PN_IDENVIO,
                              PV_DIA);                                                                                                                                
 
    EXCEPTION 
    WHEN LE_ERROR THEN
      PV_MSG_ERROR  := LV_ERROR;
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Ocurrio el siguiente error '||SQLERRM||'. '||LV_APLICACION; 

END MFP_INSERTAR_DIAS; 

                          
END MFK_OBJ_EJECUCION;
/
