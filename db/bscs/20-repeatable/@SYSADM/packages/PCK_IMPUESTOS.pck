CREATE OR REPLACE PACKAGE PCK_IMPUESTOS IS

 --=====================================================================================--
   -- Creador por    :  CLS David Solorzano (CLS DSR)
   -- Lider PDS      :  CLS Mariuxi Jofat
   -- Proposito      :  Reporte Facturación Servicios De Telecomunicaciones
   -- Solicitado por :  SIS Tanya Merchan
 --=====================================================================================--
 --
 TYPE GR_REP_SERVICIOSTEL_TMP IS RECORD (CUSTOMER_ID INTEGER,
                                         CUSTCODE    VARCHAR2(24),
                                         OHREFNUM    VARCHAR2(30),
                                         CCCITY      VARCHAR2(40),
                                         CCSTATE     VARCHAR2(25),
                                         BANK_ID     INTEGER,
                                         PRODUCTO    VARCHAR2(30),
                                         COST_DESC   VARCHAR2(30),
                                         CBLE        VARCHAR2(30),
                                         VALOR       NUMBER,
                                         DESCUENTO   NUMBER,
                                         TIPO        VARCHAR2(40),
                                         NOMBRE      VARCHAR2(40),
                                         NOMBRE2     VARCHAR2(40),
                                         SERVICIO    VARCHAR2(100),
                                         CTACTBLE    VARCHAR2(30),
                                         CTACLBLEP   VARCHAR2(30));
	 
 TYPE GT_REP_SERVICIOSTEL_TMP IS TABLE OF GR_REP_SERVICIOSTEL_TMP INDEX BY BINARY_INTEGER;
 TYPE CURSOR_TYPE IS REF CURSOR; 
 --
 FUNCTION PRC_OBTIENE_PARAMETROS(PN_ID_TIPO_PARAMETRO  NUMBER,
                                 PV_ID_PARAMETRO       VARCHAR2) RETURN VARCHAR2;
  
 PROCEDURE PRC_REPORTE_SERVICIOS_TEL(FECHA_CORTE VARCHAR2);

 /*
 PROYECTO       : [11091] - REPORTE IMPUESTOS IVA E ICE
 MODIFICACION   : CLS DAVID SOLORZANO
 FECHA          : 31/10/2016
 LIDER SIS      : TANYA MERCHAN
 LIDER CLS      : MARIUXI JOFAT
 PROPOSITO      : SE AGREGA EJECUCION DE UN SHELL
 */
 PROCEDURE PRC_REPORTE_SERVICIOS_TEL_TXT(PV_FECHA_CORTE VARCHAR2);

 /*
 PROYECTO       : [11091] - REPORTE IMPUESTOS IVA E ICE
 MODIFICACION   : CLS DAVID SOLORZANO
 FECHA          : 13/12/2016
 LIDER SIS      : TANYA MERCHAN
 LIDER CLS      : MARIUXI JOFAT
 PROPOSITO      : SE AGREGA MEJORAS PARA EL CALCULO DE LA BASE IMPONIBLE ICE
 */
 PROCEDURE PRC_FACT_SERV_TEL(PV_FECHA_CORTE  VARCHAR2,
                             PV_TMCODE       NUMBER);
 
END PCK_IMPUESTOS;
/
CREATE OR REPLACE PACKAGE BODY PCK_IMPUESTOS IS

 FUNCTION PRC_OBTIENE_PARAMETROS(PN_ID_TIPO_PARAMETRO  NUMBER,
                                 PV_ID_PARAMETRO       VARCHAR2) RETURN VARCHAR2 IS

  CURSOR OBT_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS S
     WHERE S.ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
       AND S.ID_PARAMETRO = CV_ID_PARAMETRO;
       
    LN_ID_TIPO_PARAMETRO  NUMBER;
    LV_ID_PARAMETRO       VARCHAR2(2000);
    LV_VALOR              VARCHAR2(2000);
    
 BEGIN
   LN_ID_TIPO_PARAMETRO := PN_ID_TIPO_PARAMETRO;
   LV_ID_PARAMETRO := PV_ID_PARAMETRO;
   
   OPEN OBT_PARAMETRO(LN_ID_TIPO_PARAMETRO,LV_ID_PARAMETRO);
   FETCH OBT_PARAMETRO INTO LV_VALOR;
   CLOSE OBT_PARAMETRO;
   
   IF LV_VALOR IS NOT NULL THEN
     RETURN LV_VALOR;
   ELSE
     RETURN NULL;
   END IF;
       
 END PRC_OBTIENE_PARAMETROS;

 --PROCEDIMIENTO QUE GENERA EL REPORTE ICE SERVICIOS TELECOMUNICACIONES 
 PROCEDURE PRC_REPORTE_SERVICIOS_TEL(FECHA_CORTE VARCHAR2) IS
  
    CURSOR EXISTE_TABLA IS
      SELECT 'X'
        FROM ALL_TABLES S
       WHERE S.TABLE_NAME = 'GSI_REP_SERVICIOSTEL_TMP'
         AND S.OWNER = 'SYSADM';
  
    LV_EXISTE_TABLA  VARCHAR2(10);
    SQL_TXT          VARCHAR2(5000):= NULL;
    SQL_TXT_GEN      VARCHAR2(5000):= NULL;
    SQL_TXT_INSERT   VARCHAR2(5000):= NULL;
  
 BEGIN
   
   OPEN EXISTE_TABLA;
   FETCH EXISTE_TABLA INTO LV_EXISTE_TABLA;
   CLOSE EXISTE_TABLA;
  
   IF LV_EXISTE_TABLA = 'X' THEN
     SQL_TXT:='truncate table GSI_REP_SERVICIOSTEL_TMP';
   ELSE       
     SQL_TXT:='create table GSI_REP_SERVICIOSTEL_TMP ';
     SQL_TXT:= SQL_TXT || '( ' ;
     SQL_TXT:= SQL_TXT || '  CUSTOMER_ID INTEGER, ';
     SQL_TXT:= SQL_TXT || '  CUSTCODE    VARCHAR2(24), ';
     SQL_TXT:= SQL_TXT || '  OHREFNUM    VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  CCCITY      VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  CCSTATE     VARCHAR2(25), ';
     SQL_TXT:= SQL_TXT || '  BANK_ID     INTEGER, ';
     SQL_TXT:= SQL_TXT || '  PRODUCTO    VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  COST_DESC   VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  CBLE        VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  VALOR       NUMBER, ';
     SQL_TXT:= SQL_TXT || '  DESCUENTO   NUMBER, ';
     SQL_TXT:= SQL_TXT || '  TIPO        VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  NOMBRE      VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  NOMBRE2     VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  SERVICIO    VARCHAR2(100), ';
     SQL_TXT:= SQL_TXT || '  CTACTBLE    VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  CTACLBLEP   VARCHAR2(30) ';
     SQL_TXT:= SQL_TXT || ') ';
     SQL_TXT:= SQL_TXT || 'tablespace DBX_DAT ';
     SQL_TXT:= SQL_TXT || '  pctfree 10 ';
     SQL_TXT:= SQL_TXT || '  pctused 40 ';
     SQL_TXT:= SQL_TXT || '  initrans 1 ';
     SQL_TXT:= SQL_TXT || '  maxtrans 255 ';
     SQL_TXT:= SQL_TXT || '  storage ';
     SQL_TXT:= SQL_TXT || '  ( ';
     SQL_TXT:= SQL_TXT || '    initial 64K ';
     SQL_TXT:= SQL_TXT || '    minextents 1 ';
     SQL_TXT:= SQL_TXT || '    maxextents unlimited ';
     SQL_TXT:= SQL_TXT || '  ) ';
   END IF;

 EXECUTE IMMEDIATE SQL_TXT;
 
 SQL_TXT_INSERT:='INSERT INTO  GSI_REP_SERVICIOSTEL_TMP   ';
 SQL_TXT_GEN:=  'select customer_id, custcode,	 ohrefnum,  cccity,  ccstate, bank_id, producto, cost_desc,	cble, valor, ';
 SQL_TXT_GEN:= SQL_TXT_GEN|| 'descuento, tipo, nombre, nombre2, servicio, ctactble,  ctaclblep  ';
 SQL_TXT_GEN:= SQL_TXT_GEN || 'from  co_fact_' || FECHA_CORTE;
 SQL_TXT_GEN:= SQL_TXT_GEN || ' where producto not in ''DTH'' order by custcode ';

 SQL_TXT:=SQL_TXT_INSERT||SQL_TXT_GEN;
 EXECUTE IMMEDIATE SQL_TXT;  
 END PRC_REPORTE_SERVICIOS_TEL;

 /*
 PROYECTO       : [11091] - REPORTE IMPUESTOS IVA E ICE
 MODIFICACION   : CLS DAVID SOLORZANO
 FECHA          : 31/10/2016
 LIDER SIS      : TANYA MERCHAN
 LIDER CLS      : MARIUXI JOFAT
 PROPOSITO      : SE AGREGA EJECUCION DE UN SHELL
 */
 PROCEDURE PRC_REPORTE_SERVICIOS_TEL_TXT(PV_FECHA_CORTE VARCHAR2) IS

    LV_DIRECTORIO          VARCHAR2(100);
    TYPE CURSOR_TYPE       IS REF CURSOR;
    LC_DATOS               CURSOR_TYPE;
    LF_FILE                UTL_FILE.FILE_TYPE;
    LV_NAME_ARCHIVO        VARCHAR2(1000);
    LV_FECHA_NOM_ARC       VARCHAR2(30);
    LT_TABLA               GT_REP_SERVICIOSTEL_TMP;
    SQL_TXT_GEN            VARCHAR2(4000);
    LV_LIMITE              NUMBER:=1000;
    LV_REGISTRO            VARCHAR2(4000);
    LN_DATA                NUMBER;
    LN_LIMITE_DIFERENCIA   NUMBER;
    LN_CONT                NUMBER :=0;
    TYPE CURSOR_DATA       IS REF CURSOR;
    LV_QUERY1              LONG;
    LC_CONT_DATA           CURSOR_DATA;  
    --INI CLS DSR 31102016
    LV_EJECUTA_SHELL       VARCHAR2(1000);
    W_SALIDA               VARCHAR2(1000);
    W_FILE                 VARCHAR2(1000);
    --FIN CLS DSR 31102016

 BEGIN
   --
   LV_NAME_ARCHIVO := PCK_IMPUESTOS.PRC_OBTIENE_PARAMETROS(11091,'REPORTE_PARAM_NOMBRE_ARC');
   LV_DIRECTORIO := PCK_IMPUESTOS.PRC_OBTIENE_PARAMETROS(11091,'REPORTE_PARAM_DIRECTORIO');
   LV_FECHA_NOM_ARC := PV_FECHA_CORTE;
   LV_NAME_ARCHIVO := LV_NAME_ARCHIVO||'_'||LV_FECHA_NOM_ARC||'.txt';
   LF_FILE := UTL_FILE.FOPEN(LV_DIRECTORIO, LV_NAME_ARCHIVO, 'W');
   LN_CONT:=0;
   LN_DATA:=0;
   LN_LIMITE_DIFERENCIA:=0;
     
   LV_QUERY1:='select COUNT(*) '||
              ' from  co_fact_' || PV_FECHA_CORTE||
              ' where producto not in ''DTH'' order by custcode';
                
   OPEN LC_CONT_DATA FOR LV_QUERY1;
   FETCH LC_CONT_DATA INTO LN_DATA;
   CLOSE LC_CONT_DATA;
     
   IF LN_DATA > 0 THEN
     --
     SQL_TXT_GEN:=  'select customer_id, custcode,	 ohrefnum,  cccity,  ccstate, bank_id, producto, cost_desc,	cble, valor, ';
     SQL_TXT_GEN:= SQL_TXT_GEN|| 'descuento, tipo, nombre, nombre2, servicio, ctactble,  ctaclblep  ';
     SQL_TXT_GEN:= SQL_TXT_GEN || 'from  co_fact_' || PV_FECHA_CORTE;
     SQL_TXT_GEN:= SQL_TXT_GEN || ' where producto not in ''DTH'' order by custcode ';
     --
     OPEN  LC_DATOS FOR SQL_TXT_GEN; 
      LOOP
        LT_TABLA.delete;
        --
        EXIT WHEN LC_DATOS%notfound or LN_CONT = LN_DATA;                                             
        LN_LIMITE_DIFERENCIA:= LN_DATA - LN_CONT;
        
        IF TO_NUMBER(LV_LIMITE) < LN_LIMITE_DIFERENCIA  then
          LV_LIMITE := to_char(LN_LIMITE_DIFERENCIA);
        END IF;                  
        FETCH LC_DATOS BULK COLLECT INTO LT_TABLA limit to_number(LV_LIMITE);--LIMITE PARAMETRIZADO
        EXIT WHEN (LC_DATOS%ROWCOUNT < 1);
        IF LT_TABLA.COUNT > 0 THEN
          FOR I IN LT_TABLA.FIRST .. LT_TABLA.LAST
            LOOP
              --
              LV_REGISTRO:= LT_TABLA(I).CUSTOMER_ID || '|' ||
                            LT_TABLA(I).CUSTCODE|| '|' ||
                            LT_TABLA(I).OHREFNUM|| '|' ||
                            LT_TABLA(I).CCCITY|| '|' ||
                            LT_TABLA(I).CCSTATE|| '|' ||
                            LT_TABLA(I).BANK_ID|| '|' ||
                            LT_TABLA(I).PRODUCTO|| '|' ||
                            LT_TABLA(I).COST_DESC|| '|' ||
                            LT_TABLA(I).CBLE|| '|' ||
                            LT_TABLA(I).VALOR|| '|' ||
                            LT_TABLA(I).DESCUENTO|| '|' ||
                            LT_TABLA(I).TIPO|| '|' ||
                            LT_TABLA(I).NOMBRE|| '|' ||
                            LT_TABLA(I).NOMBRE2|| '|' ||
                            LT_TABLA(I).SERVICIO|| '|' ||
                            LT_TABLA(I).CTACTBLE|| '|' ||
                            LT_TABLA(I).CTACLBLEP;
                         
                UTL_FILE.PUT_LINE(LF_FILE,LV_REGISTRO);     
                
          END LOOP;                 
        END IF;
        EXIT WHEN (LC_DATOS%NOTFOUND);
      END LOOP;
      CLOSE     LC_DATOS;
      
   --INI CLS DSR 31102106
   LV_EJECUTA_SHELL := PCK_IMPUESTOS.PRC_OBTIENE_PARAMETROS(11091,'SHELL_PARAM_EJECUCION');
   LV_EJECUTA_SHELL := REPLACE(LV_EJECUTA_SHELL,'%FECHA_CORTE%',PV_FECHA_CORTE);
   W_FILE := LV_EJECUTA_SHELL;
   OSUTIL.RUNOSCMD(W_FILE, W_SALIDA);   
   --FIN CLS DSR 31102016      
   
   END IF;    
 END PRC_REPORTE_SERVICIOS_TEL_TXT;

 /*
 PROYECTO       : [11091] - REPORTE IMPUESTOS IVA E ICE
 MODIFICACION   : CLS DAVID SOLORZANO
 FECHA          : 13/12/2016
 LIDER SIS      : TANYA MERCHAN
 LIDER CLS      : MARIUXI JOFAT
 PROPOSITO      : SE AGREGA MEJORAS PARA EL CALCULO DE LA BASE IMPONIBLE ICE
 */
 PROCEDURE PRC_FACT_SERV_TEL(PV_FECHA_CORTE  VARCHAR2,
                             PV_TMCODE       NUMBER) IS

    SQL_TXT                VARCHAR2(5000):=NULL;
    SQL_TXT_INSERT         VARCHAR2(5000):=NULL;
    SQL_TXT_PRINCIPAL      VARCHAR2(5000):=NULL;
    SQL_TXT_GROUP_BY       VARCHAR2(5000):=NULL;
    SQL_TXT_DIF            VARCHAR2(5000):=NULL;
    SQL_TXT_MAY            VARCHAR2(5000):=NULL;
    SQL_TXT_MEN            VARCHAR2(5000):=NULL;
   
    CURSOR EXISTE_TABLA IS
      SELECT 'X'
        FROM ALL_TABLES S
       WHERE S.TABLE_NAME = 'GSI_REP_FACTSERVTEL_TMP'
         AND S.OWNER = 'SYSADM';
   
    LV_EXISTE_TABLA VARCHAR2(10);   

 BEGIN
   OPEN EXISTE_TABLA;
   FETCH EXISTE_TABLA INTO LV_EXISTE_TABLA;
   CLOSE EXISTE_TABLA;
  
   IF LV_EXISTE_TABLA = 'X' THEN
     SQL_TXT:='truncate table gsi_rep_factservtel_tmp';
   ELSE
     SQL_TXT:='create table GSI_REP_FACTSERVTEL_TMP ';
     SQL_TXT:= SQL_TXT || '( ' ;
     SQL_TXT:= SQL_TXT || '  TIPO      VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  NOMBRE    VARCHAR2(40), ';
     SQL_TXT:= SQL_TXT || '  PRODUCTO  VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  VALOR     NUMBER, ';
     SQL_TXT:= SQL_TXT || '  DESCUENTO NUMBER, ';
     SQL_TXT:= SQL_TXT || '  COMPANIA  VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  CTBLEO    VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  SMA       VARCHAR2(30), ';
     SQL_TXT:= SQL_TXT || '  CTA_DSCTO CHAR(9), ';
     SQL_TXT:= SQL_TXT || '  SERVICIO  VARCHAR2(100), ';
     SQL_TXT:= SQL_TXT || '  BIMP_ICE  NUMBER ';
     SQL_TXT:= SQL_TXT || ') ';
     SQL_TXT:= SQL_TXT || 'tablespace DBX_DAT ';
     SQL_TXT:= SQL_TXT || '  pctfree 10 ';
     SQL_TXT:= SQL_TXT || '  pctused 40 ';
     SQL_TXT:= SQL_TXT || '  initrans 1 ';
     SQL_TXT:= SQL_TXT || '  maxtrans 255 ';
     SQL_TXT:= SQL_TXT || '  storage ';
     SQL_TXT:= SQL_TXT || '  ( ';
     SQL_TXT:= SQL_TXT || '    initial 64K ';
     SQL_TXT:= SQL_TXT || '    minextents 1 ';
     SQL_TXT:= SQL_TXT || '    maxextents unlimited ';
     SQL_TXT:= SQL_TXT || '  ) ';
   END IF;
   
 EXECUTE IMMEDIATE SQL_TXT;

 SQL_TXT_INSERT:='insert into  gsi_rep_factservtel_tmp ';
 SQL_TXT_PRINCIPAL := 'select x.tipo, x.nombre, x.producto, sum(x.valor) valor, sum(x.descuento) descuento,x.compania, ';
 SQL_TXT_PRINCIPAL := SQL_TXT_PRINCIPAL || ' x.ctbleo, x.sma , null, x.servicio, sum(x.base_ice) base_ice from ( ';
 SQL_TXT_GROUP_BY := ') x group by x.tipo, x.nombre, x.producto, x.compania, x.ctbleo, x.sma, x.servicio ';

 SQL_TXT_DIF := 'select a.custcode,a.tipo, a.nombre, a.producto, a.valor valor, a.descuento descuento,a.cost_desc compania, a.CTAclblep as ctbleo, b.sma , null, a.servicio, ';
 SQL_TXT_DIF := SQL_TXT_DIF || 'decode((select ''1'' from Co_Fact_'||PV_FECHA_CORTE||' s where s.tipo = ''003 - IMPUESTOS'' and s.nombre = ''ICE de Telecomunicación S(15%%)'' and s.custcode = a.custcode),''1'', ';
 --SQL_TXT_DIF := SQL_TXT_DIF || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 else (a.valor) end),0) base_ice ';
 SQL_TXT_DIF := SQL_TXT_DIF || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 when a.tipo in (''006 - CREDITOS'') then 0 when a.tipo in (''005 - CARGOS'') then 0 when a.nombre like (''Dscto.%'') then 0 else (a.valor) end),0) base_ice '; 
 SQL_TXT_DIF := SQL_TXT_DIF || 'from  Co_Fact_'||PV_FECHA_CORTE||' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
 SQL_TXT_DIF := SQL_TXT_DIF || 'where  a.servicio = b.servicio(+) and  a.tipo = B.TIPO(+) and producto != ''DTH'' ';

--INI CLS LCA 11258
/*
 SQL_TXT_MAY := 'select a.custcode,a.tipo, a.nombre, a.producto, a.valor valor, a.descuento descuento,a.cost_desc compania, a.CTAclblep as ctbleo, b.sma , null, a.servicio, ';
 SQL_TXT_MAY := SQL_TXT_MAY || 'decode((select ''1'' from Co_Fact_'||PV_FECHA_CORTE||' s where s.tipo = ''003 - IMPUESTOS'' and s.nombre = ''ICE de Telecomunicación S(15%%)'' and s.custcode = a.custcode),''1'', ';
 --SQL_TXT_MAY := SQL_TXT_MAY || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 else (a.valor) end),0) base_ice ';
 SQL_TXT_MAY := SQL_TXT_MAY || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 when a.tipo in (''006 - CREDITOS'') then 0 when a.tipo in (''005 - CARGOS'') then 0 when a.nombre like (''Dscto.%'') then 0 else (a.valor) end),0) base_ice '; 
 SQL_TXT_MAY := SQL_TXT_MAY || 'from  Co_Fact_'||PV_FECHA_CORTE||' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
 SQL_TXT_MAY := SQL_TXT_MAY || 'where  a.servicio = b.servicio(+) and  a.tipo = B.TIPO(+) and producto != ''DTH'' and valor > 0 ';

 SQL_TXT_MEN := 'select a.custcode,a.tipo, a.nombre, a.producto, a.valor valor, a.descuento descuento,a.cost_desc compania, a.CTAclblep as ctbleo, b.sma , null, a.servicio, ';
 SQL_TXT_MEN := SQL_TXT_MEN || 'decode((select ''1'' from Co_Fact_'||PV_FECHA_CORTE||' s where s.tipo = ''003 - IMPUESTOS'' and s.nombre = ''ICE de Telecomunicación S(15%%)'' and s.custcode = a.custcode),''1'', ';
 --SQL_TXT_MEN := SQL_TXT_MEN || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 else (a.valor) end),0) base_ice ';
 SQL_TXT_MEN := SQL_TXT_MEN || '(case when a.servicio in (''3'',''9'',''10'',''1057'') then 0 when a.tipo in (''006 - CREDITOS'') then 0 when a.tipo in (''005 - CARGOS'') then 0 when a.nombre like (''Dscto.%'') then 0 else (a.valor) end),0) base_ice '; 
 SQL_TXT_MEN := SQL_TXT_MEN || 'from  Co_Fact_'||PV_FECHA_CORTE||' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
 SQL_TXT_MEN := SQL_TXT_MEN || 'where  a.servicio = b.servicio(+) and  a.tipo = B.TIPO(+) and producto != ''DTH'' and valor < 0 and b.tipo=''006 - CREDITOS'' ';
*/
--FIN
 IF PV_TMCODE =0 THEN 
   SQL_TXT := SQL_TXT_INSERT||SQL_TXT_PRINCIPAL||SQL_TXT_DIF
              --||' union '||SQL_TXT_MAY
              --||' union '||SQL_TXT_MEN
              ||SQL_TXT_GROUP_BY;
 END IF; 

 --DBMS_OUTPUT.put_line(SQL_TXT);
 EXECUTE IMMEDIATE SQL_TXT;

 END PRC_FACT_SERV_TEL;
 
END PCK_IMPUESTOS;
/
