CREATE OR REPLACE PACKAGE BSK_CUENTAS_OCC_FACTURA IS

    PROCEDURE BSP_REGISTRA_OCC(PN_HILO      IN NUMBER,
                           PV_ERROR     OUT VARCHAR2);
  
END BSK_CUENTAS_OCC_FACTURA;
/
CREATE OR REPLACE PACKAGE BODY BSK_CUENTAS_OCC_FACTURA IS
  -- ============================================================================================
  -- CREADO  POR:       SUD EVELYN GONZALEZ G.
  -- LIDER CLARO:       SIS JULIA RODAS .
  -- LIDER PDS:         SUD ARTURO GAMBOA.
  -- FECHA DE CREACION: 24/04/2015 
  -- PROYECTO: [10203] ADICIONALES EN FACTURA 
  -- DESCRIPCION: ESTE PAQUETE SE ENCARGA DE EXTRAER LA ULTIMA FECHA DE ACTIVACION DE FEATURES 
  --              CUENTAS BULK
  -- ============================================================================================
 
  -- ============================================================================================
  -- Modificado  POR:   SUD Laura Pe�a
  -- LIDER CLARO:       SIS JULIA RODAS .
  -- LIDER PDS:         SUD ARTURO GAMBOA.
  -- FECHA DE CREACION: 24/07/2015 
  -- PROYECTO: [10203] ADICIONALES EN FACTURA 
  -- DESCRIPCION: Se cambian los nombres de los campos y algunos tipos de datos en la tabla bl_occ_facturar y bl_cuentas_facturar
  -- ============================================================================================
 ---
  --- Extrae OCC - hilo del 0 al 9
  PROCEDURE BSP_REGISTRA_OCC(PN_HILO      IN NUMBER,
                           PV_ERROR     OUT VARCHAR2) IS
    ---
    
     
    
    LV_APLICACION VARCHAR2(100) := 'BSK_CUENTAS_OCC_FACTURA.BSP_REGISTRA_OCC';
  
   cursor c_data (cn_hilo number)is 
       select CLIENTE,
              FECHA_ACTIVACION,
              CONTADOR,
              CO_ID,
              SNCODE,
              HILO,
              FECHA_CORTE,
              CICLO,
              TIPO_PROCESO
         from bl_occ_facturar
        where hilo = cn_hilo
        AND ESTADO='I';
        
    ---
    
    
    TYPE T_FEATURE_CUENTA IS TABLE OF c_data%ROWTYPE INDEX BY BINARY_INTEGER;
    LT_FEATURE_CUENTA T_FEATURE_CUENTA;
    
    cursor c_parametro (CV_PARAMETRO VARCHAR2) is
    select valor
    from gv_parametros a
    where id_tipo_parametro = 10203
    AND ID_PARAMETRO = CV_PARAMETRO;
 

    CURSOR C_OBTIENE_BILLCYCLE (CN_COID NUMBER, CV_CICLO VARCHAR2)
     IS 

    select a.billcycle , decode(A.customer_id_high, null, a.custcode, (select T.custcode from customer_all T where T.customer_id = a.customer_id_high)) CUENTA
      from customer_all a, fa_ciclos_axis_bscs f
     where customer_id =
           (select P.customer_id from contract_all P where P.co_id = CN_COID)
       and f.id_ciclo_admin = a.billcycle
       and f.id_ciclo = CV_CICLO;
   
   CURSOR C_MAPEA_SNCODE(CN_SNCODE NUMBER) IS 
    SELECT SHDES FROM MPUSNTAB WHERE SNCODE=CN_SNCODE;
         
    --
    LC_OBTIENE_BILLCYCLE C_OBTIENE_BILLCYCLE%ROWTYPE;
    LB_FOUND             BOOLEAN:= TRUE; 
    ---
    LV_CLIENTE      VARCHAR2(200);
    LV_CUENTA_BSCS  VARCHAR2(200) := NULL;
    LV_CICLO_ADMIN  VARCHAR2(200) := NULL;
    LD_FECHA_MAXIMA DATE;
    LN_COID         NUMBER;
    LV_SHDES        VARCHAR2(10);
    LV_ESTADO       VARCHAR2(1);
    LV_OBSERVACION  VARCHAR2(500) := NULL;
    -- 
    LN_COUNT NUMBER := 0;
    Ln_limit NUMBER;
    Ln_limit_commit number; 
    --
   
   
  BEGIN
    ---
    IF PN_HILO < 0 THEN
      PV_ERROR := 'El hilo debe ser 1 a 10';
      RETURN;
    END IF;
    
    open  c_parametro('LIMITE_BULK_COLLE');
    fetch c_parametro into Ln_limit;
    close c_parametro;
    
    open  c_parametro('LIMITE_COMMIT_COLLE');
    fetch c_parametro into Ln_limit_commit;
    close c_parametro;
    
    BEGIN 
    DELETE FROM BL_OCC_FACTURAR WHERE ESTADO='P' AND HILO=PN_HILO;
    DELETE FROM BL_OCC_FACTURAR WHERE ESTADO='E' AND HILO=PN_HILO; --SUD KBA 31/10/2016
    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
     NULL; 
    END ;   
       
    ---
    --- Cursor que Obtiene los registros de las diferentes tablas de cobro
    OPEN c_data(PN_HILO);
      
    LOOP
      FETCH c_data BULK COLLECT
        INTO LT_FEATURE_CUENTA LIMIT Ln_limit;
      ---        
      IF LT_FEATURE_CUENTA.COUNT > 0 THEN
        FOR I IN LT_FEATURE_CUENTA.FIRST .. LT_FEATURE_CUENTA.LAST LOOP
          
          LV_CLIENTE      := LT_FEATURE_CUENTA(I).CLIENTE;
          LD_FECHA_MAXIMA := to_Date(LT_FEATURE_CUENTA(I).FECHA_ACTIVACION,'dd/mm/yyyy hh24:mi:ss');
          LN_COID         := LT_FEATURE_CUENTA(I).CO_ID;
          
          LV_SHDES        := NULL;
          LV_CUENTA_BSCS  := NULL;
          LV_CICLO_ADMIN  := NULL;
          LB_FOUND := FALSE; 
          --- Funcion para extraer CTA. BSCS
         
       
         OPEN C_OBTIENE_BILLCYCLE(LN_COID,LT_FEATURE_CUENTA(I).CICLO);
         FETCH C_OBTIENE_BILLCYCLE INTO LC_OBTIENE_BILLCYCLE;
         LB_FOUND := C_OBTIENE_BILLCYCLE%FOUND;
         CLOSE C_OBTIENE_BILLCYCLE;

         IF LB_FOUND THEN

           OPEN C_MAPEA_SNCODE(LT_FEATURE_CUENTA(I).SNCODE);
           FETCH C_MAPEA_SNCODE INTO LV_SHDES;
           CLOSE C_MAPEA_SNCODE;
             
             LV_ESTADO:='P';
              
              BEGIN

                  INSERT INTO Bl_Cuentas_Facturar
                    (FECHA_CORTE,
                     CUENTA_BSCS,
                     CAMPO1,
                     CAMPO2,
                     FECHA_INGRESO,
                     CICLO,
                     HILO,
                     ESTADO,
                     TELEFONO,
                     TIPO_PROCESO,
                     ID_CICLO_ADMIN,
                     FEATURE_DESCRIPCION,
                     billcycle_final)


                VALUES
                  (to_Date(LT_FEATURE_CUENTA(I).FECHA_CORTE,'dd/mm/rrrr'),
                   LC_OBTIENE_BILLCYCLE.CUENTA,
                   LD_FECHA_MAXIMA,
                    TO_CHAR(to_date(LD_FECHA_MAXIMA, 'dd/mm/rrrr'),
                      'YYYYMMDD') || '|' || LV_SHDES,
                   SYSDATE,
                   LT_FEATURE_CUENTA(I).CICLO,
                   LT_FEATURE_CUENTA(I).HILO,
                   'A',
                   obtiene_telefono_dnc_int(LV_CLIENTE),
                   LT_FEATURE_CUENTA(I).TIPO_PROCESO,
                   LC_OBTIENE_BILLCYCLE.BILLCYCLE,
                   LT_FEATURE_CUENTA(I).CONTADOR,LC_OBTIENE_BILLCYCLE.BILLCYCLE);

              EXCEPTION
                WHEN OTHERS THEN
                  LV_OBSERVACION := 'ERROR AL INSERTAR DATOS - BL_OCC_FACTURAR '||SQLERRM;
                  
                  LV_ESTADO:='E';
                  
                  
                  --RAISE LE_ERROR;
              END;
              
              
              ---
           ELSE
             LV_ESTADO:='E';
--             LV_OBSERVACION:='Error al procesar, la informacion no existe en la base revisar la data  o depurar  => '||LT_FEATURE_CUENTA(I).CICLO;
             LV_OBSERVACION:='Error No se encontro informacion para el servicio '||LV_CLIENTE ||' - '||'revisar la data o depurar la bl_occ_facturar => '||LT_FEATURE_CUENTA(I).CICLO;
             LC_OBTIENE_BILLCYCLE.CUENTA:=NULL;
             LC_OBTIENE_BILLCYCLE.BILLCYCLE:= NULL;
             LV_SHDES:=NULL;  
              --
           END IF;

 
        UPDATE BL_OCC_FACTURAR  A
        SET ESTADO=LV_ESTADO,
         a.observacion = LV_OBSERVACION,
         A.SHDES =  LV_SHDES,
         A.CUENTA = LC_OBTIENE_BILLCYCLE.CUENTA,
         A.ID_CICLO_ADMIN = LC_OBTIENE_BILLCYCLE.BILLCYCLE 
         
        WHERE
        A.CLIENTE = LV_CLIENTE
        AND A.CO_ID =  LN_COID
        AND A.CICLO = LT_FEATURE_CUENTA(I).CICLO;
        
            IF LN_COUNT = Ln_limit_commit THEN
                LN_COUNT := 0;
                COMMIT;
            END IF;
              ---
           LN_COUNT := LN_COUNT + 1;  

        END LOOP;
      END IF;
      ---
      EXIT WHEN c_data%NOTFOUND;
      ---
    END LOOP;
    LT_FEATURE_CUENTA.DELETE;
    COMMIT;
    CLOSE c_data;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR(LV_APLICACION || ': ' || SQLERRM, 1, 1000);
  END BSP_REGISTRA_OCC;

 
  

END BSK_CUENTAS_OCC_FACTURA;
/
