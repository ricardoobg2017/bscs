CREATE OR REPLACE PACKAGE MFK_OBJ_MENSAJES_GES IS

  PROCEDURE mfp_insertar(pn_idenvio               IN mf_mensajes_ges.idenvio%TYPE,
                         pv_mensaje               IN mf_mensajes_ges.mensaje%TYPE,
                         pn_secuencia_envio       IN mf_mensajes_ges.secuencia_envio%TYPE,
                         pv_destinatario          IN mf_mensajes_ges.destinatario%TYPE,
                         pn_hilo                  IN mf_mensajes_ges.hilo%TYPE DEFAULT NULL,
                         pv_identificacion        IN mf_mensajes_ges.identificacion%TYPE DEFAULT NULL,
                         pn_calificacion          IN mf_mensajes_ges.calificacion%TYPE DEFAULT NULL,
                         pv_categoria             IN mf_mensajes_ges.categoria_cli%TYPE DEFAULT NULL,
                         pv_cuenta                IN mf_mensajes_ges.cuenta_bscs%TYPE DEFAULT NULL,
                         pn_id_cliente            IN mf_mensajes_ges.id_cliente%TYPE DEFAULT NULL,
                         pv_producto              IN mf_mensajes_ges.producto%TYPE DEFAULT NULL,
                         pv_canton                IN mf_mensajes_ges.canton%TYPE DEFAULT NULL,
                         pv_provincia             IN mf_mensajes_ges.provincia%TYPE DEFAULT NULL,
                         pv_nombres               IN mf_mensajes_ges.nombre_cliente%TYPE DEFAULT NULL,
                         pv_forma_pago            IN mf_mensajes_ges.forma_pago%TYPE DEFAULT NULL,
                         pv_forma_pago2           IN mf_mensajes_ges.forma_pago2%TYPE DEFAULT NULL,
                         pv_tarjeta_cuenta        IN mf_mensajes_ges.tarjeta_cuenta%TYPE DEFAULT NULL,
                         pv_fech_expir_tarjeta    IN mf_mensajes_ges.fech_expir_tarjeta%TYPE DEFAULT NULL,
                         pv_tipo_cuenta           IN mf_mensajes_ges.tipo_cuenta%TYPE DEFAULT NULL,
                         pd_fech_aper_cuenta      IN mf_mensajes_ges.fech_aper_cuenta%TYPE DEFAULT NULL,
                         pv_telefono1             IN mf_mensajes_ges.telefono1%TYPE DEFAULT NULL,
                         pv_telefono2             IN mf_mensajes_ges.telefono2%TYPE DEFAULT NULL,
                         pv_direccion             IN mf_mensajes_ges.direccion%TYPE DEFAULT NULL,
                         pv_direccion2            IN mf_mensajes_ges.direccion2%TYPE DEFAULT NULL,
                         pv_grupo                 IN mf_mensajes_ges.grupo%TYPE DEFAULT NULL,
                         pv_id_solicitud          IN mf_mensajes_ges.id_solicitud%TYPE DEFAULT NULL,
                         pv_carta_responsabilidad IN mf_mensajes_ges.carta_responsabilidad%TYPE DEFAULT NULL,
                         pv_verificacion          IN mf_mensajes_ges.verificacion%TYPE DEFAULT NULL,
                         pv_verificacion_c        IN mf_mensajes_ges.verificacion_c%TYPE DEFAULT NULL,
                         pv_verificacion_i        IN mf_mensajes_ges.verificacion_i%TYPE DEFAULT NULL,
                         pv_lineas_act            IN mf_mensajes_ges.lineas_act%TYPE DEFAULT NULL,
                         pv_lineas_inac           IN mf_mensajes_ges.lineas_inac%TYPE DEFAULT NULL,
                         pv_lineas_susp           IN mf_mensajes_ges.lineas_susp%TYPE DEFAULT NULL,
                         pv_regularizada          IN mf_mensajes_ges.regularizada%TYPE DEFAULT NULL,
                         pv_factura               IN mf_mensajes_ges.factura%TYPE DEFAULT NULL,
                         pv_vendedor              IN mf_mensajes_ges.vendedor%TYPE DEFAULT NULL,
                         pv_num_factura           IN mf_mensajes_ges.num_factura%TYPE DEFAULT NULL,
                         pn_balance_1             IN mf_mensajes_ges.balance_1%TYPE DEFAULT NULL,
                         pn_balance_2             IN mf_mensajes_ges.balance_2%TYPE DEFAULT NULL,
                         pn_balance_3             IN mf_mensajes_ges.balance_3%TYPE DEFAULT NULL,
                         pn_balance_4             IN mf_mensajes_ges.balance_4%TYPE DEFAULT NULL,
                         pn_balance_5             IN mf_mensajes_ges.balance_5%TYPE DEFAULT NULL,
                         pn_balance_6             IN mf_mensajes_ges.balance_6%TYPE DEFAULT NULL,
                         pn_balance_7             IN mf_mensajes_ges.balance_7%TYPE DEFAULT NULL,
                         pn_balance_8             IN mf_mensajes_ges.balance_8%TYPE DEFAULT NULL,
                         pn_balance_9             IN mf_mensajes_ges.balance_9%TYPE DEFAULT NULL,
                         pn_balance_10            IN mf_mensajes_ges.balance_10%TYPE DEFAULT NULL,
                         pn_balance_11            IN mf_mensajes_ges.balance_11%TYPE DEFAULT NULL,
                         pn_balance_12            IN mf_mensajes_ges.balance_12%TYPE DEFAULT NULL,
                         pn_totalvencida          IN mf_mensajes_ges.totalvencida%TYPE DEFAULT NULL,
                         pn_totaladeuda           IN mf_mensajes_ges.totaladeuda%TYPE DEFAULT NULL,
                         pv_mayorvencido          IN mf_mensajes_ges.mayorvencido%TYPE DEFAULT NULL,
                         pn_total_fact            IN mf_mensajes_ges.total_fact%TYPE DEFAULT NULL,
                         pn_saldoant              IN mf_mensajes_ges.saldoant%TYPE DEFAULT NULL,
                         pn_pagosper              IN mf_mensajes_ges.pagosper%TYPE DEFAULT NULL,
                         pn_credtper              IN mf_mensajes_ges.credtper%TYPE DEFAULT NULL,
                         pn_cmper                 IN mf_mensajes_ges.cmper%TYPE DEFAULT NULL,
                         pn_consmper              IN mf_mensajes_ges.consmper%TYPE DEFAULT NULL,
                         pn_compania              IN mf_mensajes_ges.compania%TYPE DEFAULT NULL,
                         pv_burocredito           IN mf_mensajes_ges.burocredito%TYPE DEFAULT NULL,
                         pd_fech_max_pago         IN mf_mensajes_ges.fech_max_pago%TYPE DEFAULT NULL,
                         pn_mora_real             IN mf_mensajes_ges.mora_real%TYPE DEFAULT NULL,
                         pn_mora_real_mig         IN mf_mensajes_ges.mora_real_mig%TYPE DEFAULT NULL,
                         pn_tipo_ingreso          IN mf_mensajes_ges.tipo_ingreso%TYPE DEFAULT NULL,
                         pv_id_plan               IN mf_mensajes_ges.id_plan%TYPE DEFAULT NULL,
                         pv_detalle_plan          IN mf_mensajes_ges.detalle_plan%TYPE DEFAULT NULL,
                         pv_msgerror              IN OUT VARCHAR2);

  PROCEDURE mfp_actualizar(pn_idenvio           IN mf_mensajes_ges.idenvio%TYPE,
                           pv_mensaje           IN mf_mensajes_ges.mensaje%TYPE,
                           pn_secuencia_envio   IN mf_mensajes_ges.secuencia_envio%TYPE,
                           pn_secuencia_mensaje IN mf_mensajes_ges.secuencia_mensaje%TYPE,
                           pv_destinatario      IN mf_mensajes_ges.destinatario%TYPE,
                           pv_estado            IN mf_mensajes_ges.estado%TYPE,
                           pv_msgerror          IN OUT VARCHAR2);

END MFK_OBJ_MENSAJES_GES;
/
CREATE OR REPLACE PACKAGE BODY MFK_OBJ_MENSAJES_GES IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite registrar para el envio del reporte en la 
  **               tabla MF_MENSAJES_GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_insertar(pn_idenvio               IN mf_mensajes_ges.idenvio%TYPE,
                         pv_mensaje               IN mf_mensajes_ges.mensaje%TYPE,
                         pn_secuencia_envio       IN mf_mensajes_ges.secuencia_envio%TYPE,
                         pv_destinatario          IN mf_mensajes_ges.destinatario%TYPE,
                         pn_hilo                  IN mf_mensajes_ges.hilo%TYPE DEFAULT NULL,
                         pv_identificacion        IN mf_mensajes_ges.identificacion%TYPE DEFAULT NULL,
                         pn_calificacion          IN mf_mensajes_ges.calificacion%TYPE DEFAULT NULL,
                         pv_categoria             IN mf_mensajes_ges.categoria_cli%TYPE DEFAULT NULL,
                         pv_cuenta                IN mf_mensajes_ges.cuenta_bscs%TYPE DEFAULT NULL,
                         pn_id_cliente            IN mf_mensajes_ges.id_cliente%TYPE DEFAULT NULL,
                         pv_producto              IN mf_mensajes_ges.producto%TYPE DEFAULT NULL,
                         pv_canton                IN mf_mensajes_ges.canton%TYPE DEFAULT NULL,
                         pv_provincia             IN mf_mensajes_ges.provincia%TYPE DEFAULT NULL,
                         pv_nombres               IN mf_mensajes_ges.nombre_cliente%TYPE DEFAULT NULL,
                         pv_forma_pago            IN mf_mensajes_ges.forma_pago%TYPE DEFAULT NULL,
                         pv_forma_pago2           IN mf_mensajes_ges.forma_pago2%TYPE DEFAULT NULL,
                         pv_tarjeta_cuenta        IN mf_mensajes_ges.tarjeta_cuenta%TYPE DEFAULT NULL,
                         pv_fech_expir_tarjeta    IN mf_mensajes_ges.fech_expir_tarjeta%TYPE DEFAULT NULL,
                         pv_tipo_cuenta           IN mf_mensajes_ges.tipo_cuenta%TYPE DEFAULT NULL,
                         pd_fech_aper_cuenta      IN mf_mensajes_ges.fech_aper_cuenta%TYPE DEFAULT NULL,
                         pv_telefono1             IN mf_mensajes_ges.telefono1%TYPE DEFAULT NULL,
                         pv_telefono2             IN mf_mensajes_ges.telefono2%TYPE DEFAULT NULL,
                         pv_direccion             IN mf_mensajes_ges.direccion%TYPE DEFAULT NULL,
                         pv_direccion2            IN mf_mensajes_ges.direccion2%TYPE DEFAULT NULL,
                         pv_grupo                 IN mf_mensajes_ges.grupo%TYPE DEFAULT NULL,
                         pv_id_solicitud          IN mf_mensajes_ges.id_solicitud%TYPE DEFAULT NULL,
                         pv_carta_responsabilidad IN mf_mensajes_ges.carta_responsabilidad%TYPE DEFAULT NULL,
                         pv_verificacion          IN mf_mensajes_ges.verificacion%TYPE DEFAULT NULL,
                         pv_verificacion_c        IN mf_mensajes_ges.verificacion_c%TYPE DEFAULT NULL,
                         pv_verificacion_i        IN mf_mensajes_ges.verificacion_i%TYPE DEFAULT NULL,
                         pv_lineas_act            IN mf_mensajes_ges.lineas_act%TYPE DEFAULT NULL,
                         pv_lineas_inac           IN mf_mensajes_ges.lineas_inac%TYPE DEFAULT NULL,
                         pv_lineas_susp           IN mf_mensajes_ges.lineas_susp%TYPE DEFAULT NULL,
                         pv_regularizada          IN mf_mensajes_ges.regularizada%TYPE DEFAULT NULL,
                         pv_factura               IN mf_mensajes_ges.factura%TYPE DEFAULT NULL,
                         pv_vendedor              IN mf_mensajes_ges.vendedor%TYPE DEFAULT NULL,
                         pv_num_factura           IN mf_mensajes_ges.num_factura%TYPE DEFAULT NULL,
                         pn_balance_1             IN mf_mensajes_ges.balance_1%TYPE DEFAULT NULL,
                         pn_balance_2             IN mf_mensajes_ges.balance_2%TYPE DEFAULT NULL,
                         pn_balance_3             IN mf_mensajes_ges.balance_3%TYPE DEFAULT NULL,
                         pn_balance_4             IN mf_mensajes_ges.balance_4%TYPE DEFAULT NULL,
                         pn_balance_5             IN mf_mensajes_ges.balance_5%TYPE DEFAULT NULL,
                         pn_balance_6             IN mf_mensajes_ges.balance_6%TYPE DEFAULT NULL,
                         pn_balance_7             IN mf_mensajes_ges.balance_7%TYPE DEFAULT NULL,
                         pn_balance_8             IN mf_mensajes_ges.balance_8%TYPE DEFAULT NULL,
                         pn_balance_9             IN mf_mensajes_ges.balance_9%TYPE DEFAULT NULL,
                         pn_balance_10            IN mf_mensajes_ges.balance_10%TYPE DEFAULT NULL,
                         pn_balance_11            IN mf_mensajes_ges.balance_11%TYPE DEFAULT NULL,
                         pn_balance_12            IN mf_mensajes_ges.balance_12%TYPE DEFAULT NULL,
                         pn_totalvencida          IN mf_mensajes_ges.totalvencida%TYPE DEFAULT NULL,
                         pn_totaladeuda           IN mf_mensajes_ges.totaladeuda%TYPE DEFAULT NULL,
                         pv_mayorvencido          IN mf_mensajes_ges.mayorvencido%TYPE DEFAULT NULL,
                         pn_total_fact            IN mf_mensajes_ges.total_fact%TYPE DEFAULT NULL,
                         pn_saldoant              IN mf_mensajes_ges.saldoant%TYPE DEFAULT NULL,
                         pn_pagosper              IN mf_mensajes_ges.pagosper%TYPE DEFAULT NULL,
                         pn_credtper              IN mf_mensajes_ges.credtper%TYPE DEFAULT NULL,
                         pn_cmper                 IN mf_mensajes_ges.cmper%TYPE DEFAULT NULL,
                         pn_consmper              IN mf_mensajes_ges.consmper%TYPE DEFAULT NULL,
                         pn_compania              IN mf_mensajes_ges.compania%TYPE DEFAULT NULL,
                         pv_burocredito           IN mf_mensajes_ges.burocredito%TYPE DEFAULT NULL,
                         pd_fech_max_pago         IN mf_mensajes_ges.fech_max_pago%TYPE DEFAULT NULL,
                         pn_mora_real             IN mf_mensajes_ges.mora_real%TYPE DEFAULT NULL,
                         pn_mora_real_mig         IN mf_mensajes_ges.mora_real_mig%TYPE DEFAULT NULL,
                         pn_tipo_ingreso          IN mf_mensajes_ges.tipo_ingreso%TYPE DEFAULT NULL,
                         pv_id_plan               IN mf_mensajes_ges.id_plan%TYPE DEFAULT NULL,
                         pv_detalle_plan          IN mf_mensajes_ges.detalle_plan%TYPE DEFAULT NULL,
                         pv_msgerror              IN OUT VARCHAR2) IS

    lv_aplicacion        VARCHAR2(100) := 'MFK_OBJ_MENSAJES_GES.MFP_INSERTAR';
    ln_secuencia_mensaje mf_mensajes_ges.secuencia_mensaje%TYPE;
    pv_estado            mf_mensajes_ges.estado%TYPE := 'A';
  
  BEGIN
  
    SELECT mfs_enviomensaje_sec_ges.nextval
      INTO ln_secuencia_mensaje
      FROM dual;
  
    INSERT INTO mf_mensajes_ges
      (idenvio,
       secuencia_envio,
       secuencia_mensaje,
       mensaje,
       destinatario,
       estado,
       hilo,
       estado_archivo,
       identificacion,
       cuenta_bscs,
       calificacion,
       categoria_cli,
       fecha_registro,
       id_cliente,
       producto,
       canton,
       provincia,
       nombre_cliente,
       forma_pago,
       forma_pago2,
       tarjeta_cuenta,
       fech_expir_tarjeta,
       tipo_cuenta,
       fech_aper_cuenta,
       telefono1,
       telefono2,
       direccion,
       direccion2,
       grupo,
       id_solicitud,
       carta_responsabilidad,
       verificacion,
       verificacion_c,
       verificacion_i,
       lineas_act,
       lineas_inac,
       lineas_susp,
       regularizada,
       factura,
       vendedor,
       num_factura,
       balance_1,
       balance_2,
       balance_3,
       balance_4,
       balance_5,
       balance_6,
       balance_7,
       balance_8,
       balance_9,
       balance_10,
       balance_11,
       balance_12,
       totalvencida,
       totaladeuda,
       mayorvencido,
       total_fact,
       saldoant,
       pagosper,
       credtper,
       cmper,
       consmper,
       compania,
       burocredito,
       fech_max_pago,
       mora_real,
       mora_real_mig,
       tipo_ingreso,
       id_plan,
       detalle_plan)
    VALUES
      (pn_idenvio,
       pn_secuencia_envio,
       ln_secuencia_mensaje,
       pv_mensaje,
       pv_destinatario,
       pv_estado,
       pn_hilo,
       'I',
       pv_identificacion,
       pv_cuenta,
       pn_calificacion,
       pv_categoria,
       SYSDATE,
       pn_id_cliente,
       pv_producto,
       pv_canton,
       pv_provincia,
       pv_nombres,
       pv_forma_pago,
       pv_forma_pago2,
       pv_tarjeta_cuenta,
       pv_fech_expir_tarjeta,
       pv_tipo_cuenta,
       pd_fech_aper_cuenta,
       pv_telefono1,
       pv_telefono2,
       pv_direccion,
       pv_direccion2,
       pv_grupo,
       pv_id_solicitud,
       pv_carta_responsabilidad,
       pv_verificacion,
       pv_verificacion_c,
       pv_verificacion_i,
       pv_lineas_act,
       pv_lineas_inac,
       pv_lineas_susp,
       pv_regularizada,
       pv_factura,
       pv_vendedor,
       pv_num_factura,
       pn_balance_1,
       pn_balance_2,
       pn_balance_3,
       pn_balance_4,
       pn_balance_5,
       pn_balance_6,
       pn_balance_7,
       pn_balance_8,
       pn_balance_9,
       pn_balance_10,
       pn_balance_11,
       pn_balance_12,
       pn_totalvencida,
       pn_totaladeuda,
       pv_mayorvencido,
       pn_total_fact,
       pn_saldoant,
       pn_pagosper,
       pn_credtper,
       pn_cmper,
       pn_consmper,
       pn_compania,
       pv_burocredito,
       pd_fech_max_pago,
       pn_mora_real,
       pn_mora_real_mig,
       pn_tipo_ingreso,
       pv_id_plan,
       pv_detalle_plan);
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite actualizar para el envio del reporte en la 
  **               tabla MF_MENSAJES_GES
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_actualizar(pn_idenvio           IN mf_mensajes_ges.idenvio%TYPE,
                           pv_mensaje           IN mf_mensajes_ges.mensaje%TYPE,
                           pn_secuencia_envio   IN mf_mensajes_ges.secuencia_envio%TYPE,
                           pn_secuencia_mensaje IN mf_mensajes_ges.secuencia_mensaje%TYPE,
                           pv_destinatario      IN mf_mensajes_ges.destinatario%TYPE,
                           pv_estado            IN mf_mensajes_ges.estado%TYPE,
                           pv_msgerror          IN OUT VARCHAR2) IS
  
    CURSOR c_table_cur(cn_idenvio           NUMBER,
                       cn_secuencia_envio   NUMBER,
                       cn_secuencia_mensaje NUMBER) IS
      SELECT *
        FROM mf_mensajes_ges m
       WHERE m.secuencia_mensaje = cn_secuencia_mensaje
         AND m.secuencia_envio = cn_secuencia_envio
         AND m.idenvio = cn_idenvio;
         
    lc_currentrow    mf_mensajes_ges%ROWTYPE;
    le_nodataupdated EXCEPTION;
    lv_aplicacion    VARCHAR2(100) := 'MFK_OBJ_MENSAJES_GES.MFP_ACTUALIZAR';
    
  BEGIN
    
    pv_msgerror := NULL;
    OPEN c_table_cur(pn_idenvio, pn_secuencia_envio, pn_secuencia_mensaje);
    FETCH c_table_cur
      INTO lc_currentrow;
    IF c_table_cur%NOTFOUND THEN
      RAISE le_nodataupdated;
    END IF;
  
    UPDATE mf_mensajes_ges
       SET mensaje      = nvl(pv_mensaje, mensaje),
           destinatario = nvl(pv_destinatario, destinatario),
           estado       = nvl(pv_estado, estado)
     WHERE idenvio = pn_idenvio
       AND secuencia_envio = pn_secuencia_envio
       AND secuencia_mensaje = pn_secuencia_mensaje;
    CLOSE c_table_cur;
  
  EXCEPTION
    WHEN le_nodataupdated THEN
      pv_msgerror := 'No se encontro el registro que desea actualizar. ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
    WHEN OTHERS THEN
      pv_msgerror := 'Ocurrio el siguiente error ' || SQLERRM || '. ' || lv_aplicacion;
      CLOSE c_table_cur;
  END;

END MFK_OBJ_MENSAJES_GES;
/
