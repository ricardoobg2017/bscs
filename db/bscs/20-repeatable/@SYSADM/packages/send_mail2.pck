create or replace package send_mail2 is

  -- Author  : HLOPEZ
  -- Created : 15/11/2006 09:38i:22
  -- Purpose : send_mail
  
procedure mail ( from_name  in varchar2,
                                    to_name    in varchar2,
                                    cc         in varchar2,
                                    cco        in varchar2,
                                    subject   in varchar2,
                                     message   in  varchar2);
                                     
function split(v_cadena          in varchar2,       -- la cadena a descomponer
                  v_nume_campo      in number,         -- el numero del campo a recuperar
                  v_separador       in varchar2,       -- el separador de campos de la cadena
                  v_mens_error      out varchar2) return varchar2;    

                    
end send_mail2;
/
create or replace package body send_mail2 is
lv_error varchar2(1000);
procedure mail ( from_name  in varchar2,
                  to_name   in varchar2,
                  cc        in varchar2,
                  cco       in varchar2,
                  subject   in varchar2,
                  message   in  varchar2)  is
  
    Lc_conexion         sys.utl_smtp.connection;
    Lr_reply            sys.utl_smtp.reply;
    crlf                varchar2(2):= chr(13) || chr(10);
    lv_ip               varchar2(20):='130.2.18.61';
    lv_email            varchar2(100);
    --
    Lv_emisor           varchar2(50);    
    ln_posi             number:=0;
    ln_cont_campo       number:=0;
    ln_cont_campo_cc    number:=0;
  begin  
   
    -- Abrir la conexion TELNET con SMPT
    lr_reply := sys.utl_smtp.open_connection ( lv_ip,
                                               25,   
                                               Lc_conexion);
                                               
    dbms_output.put_line ( lr_reply.text );     
  
    -- Comando HELO al SMPT
    lr_reply := sys.utl_smtp.helo ( Lc_conexion, --c
                                    lv_ip);    --domain                                    
                                    
    dbms_output.put_line ( lr_reply.text );    
 
    -- Quien envia el correo
    lr_reply := sys.utl_smtp.mail ( Lc_conexion,  --c
                                    from_name,    --sender
                                    null);        --parameters
  
    if to_name is not null then
      loop
              ln_posi := instr(to_name,';',ln_posi+1);
           exit when ln_posi = 0;
              ln_cont_campo := ln_cont_campo + 1;
      end loop;
    end if;
    
    if cc is not null then
      loop
              ln_posi := instr(cc,';',ln_posi+1);
           exit when ln_posi = 0;
              ln_cont_campo_cc := ln_cont_campo_cc + 1;
      end loop;
    end if;
    
 --   
 -- Receptor del correo
   ln_cont_campo:=ln_cont_campo + 1;
   if to_name is not null then
    for i in 1..ln_cont_campo loop
     lv_email:=send_mail2.split(to_name,i,';',lv_error);
     lr_reply:=sys.utl_smtp.rcpt ( Lc_conexion,    --c
                                  lv_email,    --recipient
                                  null);          --parameters
    end loop;
   end if;
   -- 
   if cc is not null then
    ln_cont_campo_cc:=ln_cont_campo_cc + 1;
    for i in 1..ln_cont_campo_cc loop
     lv_email:=send_mail2.split(cc,i,';',lv_error);
     lr_reply:=sys.utl_smtp.rcpt ( Lc_conexion,    --c
                                  lv_email,    --recipient
                                  null);          --parameters
    end loop;                               
   end if;
   
   if cco is not null then                                 
    lr_reply:=sys.utl_smtp.rcpt ( Lc_conexion,    --c
                                  cco,           --recipient
                                  null);          --parameters                                       
   end if;                               
    -- dbms_output.put_line(lr_reply.text);  
  
    -- Cuerpo del mail
    lr_reply := sys.utl_smtp.open_data (Lc_conexion);
         
    sys.utl_smtp.write_data ( Lc_conexion, 'From: '   ||from_name||utl_tcp.crlf);
    
    if to_name is not null then
     for i in 1..ln_cont_campo loop
      lv_email:=send_mail2.split(to_name,i,';',lv_error);
      sys.utl_smtp.write_data ( Lc_conexion, 'To: '||lv_email||utl_tcp.crlf);
     end loop;
    end if;
    --
    if cc is not null then
     for i in 1..ln_cont_campo_cc loop
      lv_email:=send_mail2.split(cc,i,';',lv_error);
      sys.utl_smtp.write_data ( Lc_conexion, 'Cc: '||lv_email||utl_tcp.crlf);
     end loop; 
    end if; 
   
    sys.utl_smtp.write_data ( Lc_conexion, 'Cco: '||cco||utl_tcp.crlf);
    
    sys.utl_smtp.write_data ( Lc_conexion, 'Subject: '||subject||utl_tcp.crlf);
    sys.utl_smtp.write_data ( Lc_conexion, utl_tcp.crlf||message||utl_tcp.crlf);   --body
  
    lr_reply := sys.utl_smtp.close_data (Lc_conexion);
    -- dbms_output.put_line(lr_reply.text);  
  
    -- Cerrar la conexion
    lr_reply := sys.utl_smtp.quit ( Lc_conexion);
    dbms_output.put_line(lr_reply.text); 
    
  exception
    when others then
      null;
  end;
 --=========================================================================================-- 
 --=========================================================================================-- 
function split(v_cadena          in varchar2,       -- la cadena a descomponer
                  v_nume_campo      in number,         -- el numero del campo a recuperar
                  v_separador       in varchar2,       -- el separador de campos de la cadena
                  v_mens_error      out varchar2) return varchar2 is

   /* para el correcto funcionamiento de este procedimiento      */
	 /* la cadena de caracteres a evaluar debe tener el separador  */
	 /* por cada campo de la cadena                                */
   /* ej:  guayaquil-quito-cuanca-loja-                          */

      ln_posi_ini    number  := 0;
      ln_posi_fin    number  := 0;
      ln_cont        number  := 0;
      ln_tama_camp   number  := 0;
      ln_cant_camp   number;
      lv_cadena      varchar2(3200);
      v_valo_campo   varchar2(50);
      ln_cont_campo  number := 0;
      ln_posi        number := 0;
  begin
      v_valo_campo := null;
      lv_cadena := v_cadena;
      if ( substr(v_cadena,length(v_cadena)-1,1) <> v_separador )
      then
       lv_cadena		 := v_cadena||v_separador; 
      end if;
      
      if lv_cadena is not null then
         loop
            ln_posi := instr(lv_cadena,v_separador,ln_posi+1);
         exit when ln_posi = 0;
            ln_cont_campo := ln_cont_campo + 1;
         end loop;
      end if;
      
      ln_cant_camp:=NVL(LN_CONT_CAMPO,0);
      
      if ln_cant_camp > ln_cant_camp then
         v_mens_error := 'get_campo - el numero de campo a recuperar excede el numero de campos existentes';
      elsif ln_cant_camp <= 0  then
         v_mens_error := 'get_campo - el numero de campo a recuperar debe ser mayor que cero';
      else
         if lv_cadena is not null then
            loop
               if ln_cont = 0 then
                  ln_posi_ini := 0;
                  ln_posi_fin := instr(lv_cadena,v_separador,1);
               else
                  ln_posi_ini := ln_posi_fin;
                  ln_posi_fin := instr(lv_cadena,v_separador,ln_posi_ini+1);
               end if;
            exit when ln_cont > ln_cant_camp;
               ln_cont := ln_cont + 1;
               if ((v_nume_campo = 1) and (ln_cont = 1 ))then
                  v_valo_campo := substr(lv_cadena,0,ln_posi_fin-1);
               elsif ((v_nume_campo = ln_cont) and (ln_cont <> 1)) then
                  ln_tama_camp  := (ln_posi_fin-1) - ln_posi_ini;
                  v_valo_campo := substr(lv_cadena,ln_posi_ini+1,ln_tama_camp);
               end if;
            end loop;
         end if;
      end if;
    return v_valo_campo; 
  end split;
  
  end send_mail2;
/
