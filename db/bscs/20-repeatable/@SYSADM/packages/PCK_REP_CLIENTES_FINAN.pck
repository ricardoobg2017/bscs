CREATE OR REPLACE PACKAGE PCK_REP_CLIENTES_FINAN IS

PROCEDURE P_CARGA_CTA(Pv_Corte       VARCHAR2,
                      Pn_Hilo        NUMBER,
                      Pv_Error   OUT VARCHAR2);

PROCEDURE P_ACTUALIZA_PAGOS (Pn_Hilo         NUMBER,
                             Pv_Error    OUT VARCHAR2);

PROCEDURE P_CAMBIA_ESTADO (Pv_Error    OUT VARCHAR2);

PROCEDURE P_CONFIRMAR_PROCESO(PV_NOMBRE_ARCHIVO         VARCHAR2,
                              PV_BITACORA_PROCESO       VARCHAR,
                              PV_ERROR              OUT VARCHAR2);

PROCEDURE P_TRASPASO_DATOS (Pv_Reproceso         VARCHAR2,
                            Pv_Error         OUT VARCHAR2);

END PCK_REP_CLIENTES_FINAN;
/
CREATE OR REPLACE PACKAGE BODY PCK_REP_CLIENTES_FINAN IS
--=====================================================================================--
-- CREADO POR :      Andres Balladares.
-- FECHA MOD  :      02/02/2018
-- PROYECTO   :      [11735] Equipo �gil Procesos Cobranzas.
-- LIDER IRO  :      IRO Juan Romero
-- LIDER      :      SIS Antonio Mayorga
-- MOTIVO     :      Reporte detallado de clientes con financiamiento y plazos vencidos.
--=====================================================================================--
PROCEDURE P_CARGA_CTA(Pv_Corte       VARCHAR2,
                      Pn_Hilo        NUMBER,
                      Pv_Error   OUT VARCHAR2) IS
  
  TYPE TYPE_DATOS_CLIENTE IS RECORD(
    CUENTA           VARCHAR2(24),
    ID_CLIENTE       NUMBER,
    PRODUCTO         VARCHAR2(30),
    CANTON           VARCHAR2(40),
    PROVINCIA        VARCHAR2(25),
    APELLIDOS        VARCHAR2(40),
    NOMBRES          VARCHAR2(40),
    RUC              VARCHAR2(20),
    ID_FORMA_PAGO    VARCHAR2(5),
    FORMA_PAGO       VARCHAR2(58),
    TIPO_CLIENTE     VARCHAR2(60),
    FECH_APER_CUENTA DATE,
    TELEFONO1        VARCHAR2(25),
    TELEFONO2        VARCHAR2(25),
    TELEFONO         VARCHAR2(63),
    DIRECCION        VARCHAR2(70),
    DIRECCION2       VARCHAR2(200),
    ID_SOLICITUD     VARCHAR2(10),
    LINEAS_ACT       VARCHAR2(5),
    LINEAS_INAC      VARCHAR2(5),
    LINEAS_SUSP      VARCHAR2(5),
    COD_VENDEDOR     VARCHAR2(10),
    VENDEDOR         VARCHAR2(2000),
    NUM_FACTURA      VARCHAR2(30),
    --COD_FACTURA      NUMBER,
    BALANCE_1        NUMBER,
    BALANCE_2        NUMBER,
    BALANCE_3        NUMBER,
    BALANCE_4        NUMBER,
    BALANCE_5        NUMBER,
    BALANCE_6        NUMBER,
    BALANCE_7        NUMBER,
    BALANCE_8        NUMBER,
    BALANCE_9        NUMBER,
    BALANCE_10       NUMBER,
    BALANCE_11       NUMBER,
    BALANCE_12       NUMBER,
    TOTALVENCIDA     NUMBER,
    TOTAL_DEUDA      NUMBER,
    SALDO_PENDIENTE  NUMBER,
    MAYORVENCIDO     VARCHAR2(10),
    COMPANIA         NUMBER,
    FECH_MAX_PAGO    DATE,
    ID_PLAN          VARCHAR2(10),
    DETALLE_PLAN     VARCHAR2(4000),
    TIPO_ING_SOLIC   NUMBER,
    DESC_APROBACION  VARCHAR2(4000));

  TYPE DATOS_CLIENTE IS TABLE OF TYPE_DATOS_CLIENTE INDEX BY BINARY_INTEGER;
  Tb_Datos           DATOS_CLIENTE;
  
  TYPE TC_OBTIENE_BASE_CLIENTES IS REF CURSOR;
  C_OBTIENE_BASE_CLIENTES TC_OBTIENE_BASE_CLIENTES;
  
  CURSOR C_TIPO_APROBACION(Cn_IdSolicitud NUMBER) IS
  SELECT T.DESCRIPCION
    FROM CR_SOLICITUDES@REPCAPAS_BSCS S, CR_TIPOS_APROBACIONES@REPCAPAS_BSCS T
   WHERE S.ID_SOLICITUD = Cn_IdSolicitud
     AND T.ID_TIPO_APROBACION = S.ID_TIPO_APROBACION;
  
  Ln_Pagos               NUMBER;
  Ln_Creditos            NUMBER;
  Lv_Sql                 VARCHAR2(6000);
  Lv_FechaTabla          VARCHAR2(100);
  Lv_FinanManual         VARCHAR2(30000);
  Lv_FinanAuto           VARCHAR2(4000);
  Lv_ErrorClient         VARCHAR2(4000);
  Lv_ErrorPagos          VARCHAR2(4000);
  LN_ID_BITACORA_SCP     NUMBER;
  LN_ERROR_SCP           NUMBER := 0;
  LN_BITACORA_ERROR      NUMBER := 0;
  LN_BITACORA_PROCESADOS NUMBER := 0;
  LN_ID_DETALLE          NUMBER;
  LV_PROGRAMA_SCP        VARCHAR2(200) := 'PCK_REP_CLIENTES_FINAN.P_CARGA_CTA';
  LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
  LV_ERROR_SCP           VARCHAR2(500);
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LV_MENSAJE             VARCHAR2(4000);
  
BEGIN
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('FIN_BIT_CLI_FINAN_EQUIP',
                                            LV_PROGRAMA_SCP||'_'||Pn_Hilo,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            0,
                                            LV_UNIDAD_REGISTRO,
                                            LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
  Lv_FinanAuto := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                       PV_ID_PARAMETRO      => 'OPE_FINAN_AUTOMATICO');
  
  Lv_FinanManual := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                         PV_ID_PARAMETRO      => 'OPE_FINAN_MANUAL');
  
  Lv_Sql := 'SELECT /*+ RULE */ B.CUENTA, A.ID_CLIENTE, A.PRODUCTO, A.CANTON, A.PROVINCIA, A.APELLIDOS, A.NOMBRES,
                    A.RUC, B.FORMA_PAGO ID_FORMA_PAGO, A.FORMA_PAGO, NULL TIPO_CLIENTE, A.FECH_APER_CUENTA, A.TELEFONO1,
                    A.TELEFONO2, A.TELEFONO, A.DIRECCION, A.DIRECCION2, B.ID_SOLICITUD, B.LINEAS_ACT, B.LINEAS_INAC,
                    B.LINEAS_SUSP, B.COD_VENDEDOR, B.VENDEDOR, A.NUM_FACTURA, A.BALANCE_1, A.BALANCE_2, A.BALANCE_3,
                    A.BALANCE_4, A.BALANCE_5, A.BALANCE_6, A.BALANCE_7, A.BALANCE_8, A.BALANCE_9, A.BALANCE_10, A.BALANCE_11,
                    A.BALANCE_12, A.TOTALVENCIDA, A.TOTALADEUDA, NULL SALDO_PENDIENTE, A.MAYORVENCIDO, A.COMPANIA, A.FECH_MAX_PAGO,
                    B.ID_PLAN, B.DETALLE_PLAN,B.TIPO_INGRESO, NULL DESC_APROBACION
             FROM SYSADM.CO_REPCARCLI_<FECHA> A, SYSADM.CO_REPORTE_ADIC_<FECHA> B,
                 (SELECT X.CUSTCODE, X.CUSTOMER_ID, X.VALOR
                    FROM SYSADM.CO_FACT_<FECHA> X
                   WHERE X.TIPO = ''005 - CARGOS''
                     AND (X.SERVICIO IN (SELECT TO_CHAR(SNCODE)
                                           FROM MPUSNTAB
                                          WHERE SNCODE IN (SELECT SNCODE
                                                             FROM PORTA.CO_MAPEO_FINAN_CARGOS@REPCAPAS_BSCS
                                                            WHERE ID_TRANSACCION IN (<FINAN_AUTOMATICO>)
                                                              AND NUMERO_CUOTA >= 1 AND TOTAL_CUOTAS >= 1)) 
                                                               OR X.SERVICIO IN (''<FINAN_MANUAL>''))
                   GROUP BY X.CUSTCODE, X.CUSTOMER_ID, X.VALOR) C
              WHERE B.HILO = <HILO>
              AND A.CUENTA = B.CUENTA
              AND C.CUSTOMER_ID = B.CLIENTE
              AND C.CUSTCODE = A.CUENTA';
  
  Lv_FechaTabla :=  REPLACE(Pv_Corte, '/', Lv_FechaTabla);
  
  Lv_Sql := REPLACE(Lv_Sql, '<FECHA>', Lv_FechaTabla);
  Lv_Sql := REPLACE(Lv_Sql, '<FINAN_AUTOMATICO>', Lv_FinanAuto);
  Lv_Sql := REPLACE(Lv_Sql, '<FINAN_MANUAL>', Lv_FinanManual);
  Lv_Sql := REPLACE(Lv_Sql, '<HILO>', Pn_Hilo);
  --Lv_Sql := REPLACE(Lv_Sql, '<FECHA_CORTE>', Pv_Corte);
  
  OPEN C_OBTIENE_BASE_CLIENTES FOR Lv_Sql;
  LOOP
    FETCH C_OBTIENE_BASE_CLIENTES BULK COLLECT INTO Tb_Datos LIMIT 1000;
    EXIT WHEN Tb_Datos.COUNT = 0;
      
      FOR I IN Tb_Datos.FIRST .. Tb_Datos.LAST LOOP
          
          PORTA.OP_REPORTEXCAPAS_SEGMENTADO.P_TIPO_CLIENTE@REPCAPAS_BSCS(PV_CUENTA      => Tb_Datos(I).CUENTA,
                                                                PV_RUC         => Tb_Datos(I).RUC,
                                                                PV_IDPLAN      => Tb_Datos(I).ID_PLAN,
                                                                PN_TIPOINGSOL  => Tb_Datos(I).TIPO_ING_SOLIC,
                                                                PV_TIPOCLIENTE => Tb_Datos(I).TIPO_CLIENTE,
                                                                PV_ERROR       => Lv_ErrorClient);
          
          IF Lv_ErrorClient IS NOT NULL THEN
             LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
             LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_REPORTEXCAPAS_SEGMENTADO.P_TIPO_CLIENTE';
             LV_MENSAJE_TEC_SCP := 'ERROR AL CONSULTAR EL TIPO DE CLIENTE CUENTA: '||Tb_Datos(I).CUENTA||' ERROR: '||Lv_ErrorClient;
             SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                       LV_MENSAJE_APL_SCP,
                                                       LV_MENSAJE_TEC_SCP,
                                                       NULL,
                                                       0,
                                                       0,
                                                       Tb_Datos(I).CUENTA,
                                                       NULL,
                                                       0,
                                                       0,
                                                       'N',
                                                       LN_ID_DETALLE,
                                                       LN_ERROR_SCP,
                                                       LV_MENSAJE);
          END IF;
          
          SYSADM.OP_RECAUDADOR_DAS.P_PAGOS_CREDITOS(PN_CUSTOMER_ID  => Tb_Datos(I).ID_CLIENTE,
                                                    PD_FECHACORTE   => TO_DATE(Pv_Corte,'DD/MM/YYYY'),
                                                    PN_DEUDA        => Tb_Datos(I).TOTAL_DEUDA,
                                                    PV_VERI_CREDITO => 'S',
                                                    PN_SALDO        => Tb_Datos(I).SALDO_PENDIENTE,
                                                    PN_PAGO         => Ln_Pagos,
                                                    PN_CREDITO      => Ln_Creditos,
                                                    PV_ERROR        => Lv_ErrorPagos);
          
          IF Lv_ErrorClient IS NOT NULL THEN
             LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
             LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_PAGOS_CREDITOS';
             LV_MENSAJE_TEC_SCP := 'ERROR AL ACTUALIZAR EL SALDO DEL CLIENTE CUENTA: '||Tb_Datos(I).CUENTA||' ERROR: '||Lv_ErrorPagos;
             SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                       LV_MENSAJE_APL_SCP,
                                                       LV_MENSAJE_TEC_SCP,
                                                       NULL,
                                                       0,
                                                       0,
                                                       Tb_Datos(I).CUENTA,
                                                       NULL,
                                                       0,
                                                       0,
                                                       'N',
                                                       LN_ID_DETALLE,
                                                       LN_ERROR_SCP,
                                                       LV_MENSAJE);
          END IF;
          
          OPEN C_TIPO_APROBACION(Tb_Datos(I).ID_SOLICITUD);
          FETCH C_TIPO_APROBACION INTO Tb_Datos(I).DESC_APROBACION;
          CLOSE C_TIPO_APROBACION;
          
      END LOOP;
      
      FORALL J IN Tb_Datos.FIRST .. Tb_Datos.LAST
      INSERT INTO FIN_DET_CLIENTE_FINANCIAMIENTO(CUENTA,ID_CLIENTE,PRODUCTO,CANTON,PROVINCIA,APELLIDOS,NOMBRES,RUC,ID_FORMA_PAGO,
                  FORMA_PAGO,TIPO_CLIENTE,FECH_APER_CUENTA,TELEFONO1,TELEFONO2,TELEFONO,DIRECCION,DIRECCION2,ID_SOLICITUD,LINEAS_ACT,
                  LINEAS_INAC,LINEAS_SUSP,COD_VENDEDOR,VENDEDOR,NUM_FACTURA,BALANCE_1,BALANCE_2,BALANCE_3,BALANCE_4,BALANCE_5,BALANCE_6,
                  BALANCE_7,BALANCE_8,BALANCE_9,BALANCE_10,BALANCE_11,BALANCE_12,TOTALVENCIDA,TOTAL_DEUDA,SALDO_PENDIENTE,MAYORVENCIDO,
                  COMPANIA,FECH_MAX_PAGO,ID_PLAN,DETALLE_PLAN,DESC_APROBACION,ESTADO,FECHA_CORTE,HILO,USUARIO_INGRESO,FECHA_INGRESO)
          VALUES (Tb_Datos(J).CUENTA,Tb_Datos(J).ID_CLIENTE,Tb_Datos(J).PRODUCTO,Tb_Datos(J).CANTON,Tb_Datos(J).PROVINCIA,
                  Tb_Datos(J).APELLIDOS,Tb_Datos(J).NOMBRES,Tb_Datos(J).RUC,Tb_Datos(J).ID_FORMA_PAGO,Tb_Datos(J).FORMA_PAGO,
                  Tb_Datos(J).TIPO_CLIENTE,Tb_Datos(J).FECH_APER_CUENTA,Tb_Datos(J).TELEFONO1,Tb_Datos(J).TELEFONO2,Tb_Datos(J).TELEFONO,
                  Tb_Datos(J).DIRECCION,Tb_Datos(J).DIRECCION2,Tb_Datos(J).ID_SOLICITUD,Tb_Datos(J).LINEAS_ACT,Tb_Datos(J).LINEAS_INAC,
                  Tb_Datos(J).LINEAS_SUSP,Tb_Datos(J).COD_VENDEDOR,Tb_Datos(J).VENDEDOR,Tb_Datos(J).NUM_FACTURA,Tb_Datos(J).BALANCE_1,
                  Tb_Datos(J).BALANCE_2,Tb_Datos(J).BALANCE_3,Tb_Datos(J).BALANCE_4,Tb_Datos(J).BALANCE_5,Tb_Datos(J).BALANCE_6,
                  Tb_Datos(J).BALANCE_7,Tb_Datos(J).BALANCE_8,Tb_Datos(J).BALANCE_9,Tb_Datos(J).BALANCE_10,Tb_Datos(J).BALANCE_11,
                  Tb_Datos(J).BALANCE_12,Tb_Datos(J).TOTALVENCIDA,Tb_Datos(J).TOTAL_DEUDA,Tb_Datos(J).SALDO_PENDIENTE,
                  Tb_Datos(J).MAYORVENCIDO,Tb_Datos(J).COMPANIA,Tb_Datos(J).FECH_MAX_PAGO,Tb_Datos(J).ID_PLAN,Tb_Datos(J).DETALLE_PLAN,
                  Tb_Datos(J).DESC_APROBACION, 'I',TO_DATE(Pv_Corte,'DD/MM/YYYY'),Pn_Hilo,USER,SYSDATE);
      
      COMMIT;
      
    EXIT WHEN C_OBTIENE_BASE_CLIENTES%NOTFOUND;
  END LOOP;
  CLOSE C_OBTIENE_BASE_CLIENTES;
  
  AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  COMMIT;
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                            LN_BITACORA_PROCESADOS,
                                            LN_BITACORA_ERROR,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
EXCEPTION
  WHEN OTHERS THEN
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    Pv_Error := 'P_CARGA_CTA => '|| SUBSTR(SQLERRM,1,150);
    ROLLBACK;
    LV_MENSAJE_APL_SCP := 'ERROR EN EL PROCESO PCK_REP_CLIENTES_FINAN.P_CARGA_CTA';
    LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. REVISAR LAS CUENTAS CORRESPONDIENTE A ESTE HILO EN LA TABLA FIN_DET_CLIENTE_FINANCIAMIENTO'||
                          ' '||SUBSTR(SQLERRM, 1, 100);
    
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              0,
                                              0,
                                              NULL,
                                              'ERROR_PRINCIPAL',
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);
    
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
    
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
END P_CARGA_CTA;

PROCEDURE P_ACTUALIZA_PAGOS (Pn_Hilo         NUMBER,
                             Pv_Error    OUT VARCHAR2)IS
  
  CURSOR C_CUENTAS (Cn_Hilo  NUMBER)IS
  SELECT X.*
    FROM FIN_DET_CLIENTE_FINANCIAMIENTO X
   WHERE X.HILO = Cn_Hilo
     AND X.ESTADO = 'A'
     AND X.SALDO_PENDIENTE > 0
     AND X.FECHA_ACTUALIZACION < TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY')||' 00:00:01', 'DD/MM/YYYY HH24:MI:SS');
  
  TYPE TAB_CUENTAS_FINAN IS TABLE OF FIN_DET_CLIENTE_FINANCIAMIENTO%ROWTYPE INDEX BY BINARY_INTEGER;
  Lr_CuentasFinan        TAB_CUENTAS_FINAN;
  
  Ln_Saldo               NUMBER;
  Ln_Pagos               NUMBER;
  Ln_Creditos            NUMBER;
  Lv_Error               VARCHAR2(4000);
  LN_ID_BITACORA_SCP     NUMBER;
  LN_BITACORA_ERROR      NUMBER := 0;
  LN_BITACORA_PROCESADOS NUMBER := 0;
  LN_ERROR_SCP           NUMBER := 0;
  LN_ID_DETALLE          NUMBER;
  LV_PROGRAMA_SCP        VARCHAR2(200) := 'PCK_REP_CLIENTES_FINAN.P_ACTUALIZA_PAGOS';
  LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
  LV_ERROR_SCP           VARCHAR2(500);
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LV_MENSAJE             VARCHAR2(4000);
  
BEGIN
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('FIN_BIT_CLI_FINAN_EQUIP',
                                            LV_PROGRAMA_SCP||'_'||Pn_Hilo,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            0,
                                            LV_UNIDAD_REGISTRO,
                                            LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
  OPEN C_CUENTAS(Pn_Hilo);
  LOOP
  FETCH C_CUENTAS BULK COLLECT INTO Lr_CuentasFinan LIMIT 1000;
    EXIT WHEN Lr_CuentasFinan.COUNT = 0;
    
    FOR I IN Lr_CuentasFinan.FIRST .. Lr_CuentasFinan.LAST LOOP
        
        Ln_Saldo := 0;
        Lv_Error := NULL;
        
        SYSADM.OP_RECAUDADOR_DAS.P_PAGOS_CREDITOS(PN_CUSTOMER_ID  => Lr_CuentasFinan(I).ID_CLIENTE,
                                                  PD_FECHACORTE   => NVL(TO_DATE(TO_CHAR(Lr_CuentasFinan(I).FECHA_ACTUALIZACION,'DD/MM/YYYY'),'DD/MM/YYYY'),
                                                                         TO_DATE(TO_CHAR(Lr_CuentasFinan(I).FECHA_INGRESO,'DD/MM/YYYY'),'DD/MM/YYYY')),
                                                  PN_DEUDA        => Lr_CuentasFinan(I).SALDO_PENDIENTE,
                                                  PV_VERI_CREDITO => 'N',
                                                  PN_SALDO        => Ln_Saldo,
                                                  PN_PAGO         => Ln_Pagos,
                                                  PN_CREDITO      => Ln_Creditos,
                                                  PV_ERROR        => Lv_Error);
        
        IF Lv_Error IS NOT NULL THEN
           LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
           LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO OP_RECAUDADOR_DAS.P_PAGOS_CREDITOS';
           LV_MENSAJE_TEC_SCP := 'ERROR AL ACTUALIZAR EL SALDO DEL CLIENTE CUENTA: '||Lr_CuentasFinan(I).CUENTA||' ERROR: '||Lv_Error;
           SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                     LV_MENSAJE_APL_SCP,
                                                     LV_MENSAJE_TEC_SCP,
                                                     NULL,
                                                     0,
                                                     0,
                                                     Lr_CuentasFinan(I).CUENTA,
                                                     NULL,
                                                     0,
                                                     0,
                                                     'N',
                                                     LN_ID_DETALLE,
                                                     LN_ERROR_SCP,
                                                     LV_MENSAJE);
        END IF;
        
        IF Ln_Saldo < 0 THEN
           Lr_CuentasFinan(I).SALDO_PENDIENTE := 0;
        ELSE
           Lr_CuentasFinan(I).SALDO_PENDIENTE := Ln_Saldo;
        END IF;
        
    END LOOP;
    
    FORALL J IN Lr_CuentasFinan.FIRST .. Lr_CuentasFinan.LAST
    UPDATE FIN_DET_CLIENTE_FINANCIAMIENTO X
       SET X.SALDO_PENDIENTE = Lr_CuentasFinan(J).SALDO_PENDIENTE,
           X.FECHA_ACTUALIZACION = SYSDATE
     WHERE X.CUENTA = Lr_CuentasFinan(J).CUENTA;
    
    COMMIT;
    
    EXIT WHEN C_CUENTAS%NOTFOUND;
  END LOOP;
  CLOSE C_CUENTAS;
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                            LN_BITACORA_PROCESADOS,
                                            LN_BITACORA_ERROR,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  
EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := 'P_ACTUALIZA_PAGOS => '|| SUBSTR(SQLERRM,1,150);
    ROLLBACK;
    LV_MENSAJE_APL_SCP := 'ERROR EN EL PROCESO PCK_REP_CLIENTES_FINAN.P_ACTUALIZA_PAGOS';
    LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. REVISAR LAS CUENTAS CORRESPONDIENTE A ESTE HILO EN LA TABLA FIN_DET_CLIENTE_FINANCIAMIENTO'||
                          ' '||SUBSTR(SQLERRM, 1, 100);
    
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              0,
                                              0,
                                              NULL,
                                              'ERROR_PRINCIPAL',
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);
    
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
    
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
END P_ACTUALIZA_PAGOS;

PROCEDURE P_CAMBIA_ESTADO (Pv_Error    OUT VARCHAR2)IS
  
  CURSOR C_CUENTAS_INGRESADAS IS
  SELECT COUNT(*)
    FROM FIN_DET_CLIENTE_FINANCIAMIENTO X
   WHERE X.ESTADO = 'I';
  
  Ln_Count  NUMBER;
  
BEGIN
  
  OPEN C_CUENTAS_INGRESADAS;
  FETCH C_CUENTAS_INGRESADAS INTO Ln_Count;
  CLOSE C_CUENTAS_INGRESADAS;
  
  IF Ln_Count > 0 THEN
     
     UPDATE FIN_DET_CLIENTE_FINANCIAMIENTO A
        SET A.ESTADO = 'A',
            A.FECHA_ACTUALIZACION = SYSDATE
      WHERE A.ESTADO = 'I';
     
     COMMIT;
     
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    Pv_Error := 'P_CAMBIA_ESTADO => '|| SUBSTR(SQLERRM,1,150);
END P_CAMBIA_ESTADO;

PROCEDURE P_CONFIRMAR_PROCESO(PV_NOMBRE_ARCHIVO         VARCHAR2,
                              PV_BITACORA_PROCESO       VARCHAR,
                              PV_ERROR              OUT VARCHAR2) IS
  
  CURSOR C_DETALLE_BITACORA(CN_ID_BITACORA NUMBER) IS
  SELECT A.COD_AUX_2, A.COD_AUX_3, A.MENSAJE_TECNICO
    FROM SCP_DAT.SCP_DETALLES_BITACORA A
   WHERE A.ID_BITACORA = CN_ID_BITACORA
     AND A.FECHA_EVENTO BETWEEN TRUNC(SYSDATE) AND SYSDATE;
  
  CURSOR C_BITACORA(CV_ID_PROCESO VARCHAR2) IS
  SELECT A.ID_BITACORA, A.REGISTROS_ERROR
    FROM SCP_DAT.SCP_BITACORA_PROCESOS A
   WHERE A.FECHA_ACTUALIZACION BETWEEN TRUNC(SYSDATE) AND SYSDATE
     AND A.ID_PROCESO = CV_ID_PROCESO
     AND A.REGISTROS_ERROR > 0;
  
  TYPE R_CUENTA IS RECORD(
    CUENTA          VARCHAR2(30),
    ERROR           VARCHAR2(50),
    MENSAJE_TECNICO VARCHAR2(3000));
  
  TYPE T_CUENTA IS TABLE OF R_CUENTA INDEX BY BINARY_INTEGER;
  
  LT_CUENTA T_CUENTA;
  
  LN_COUNT      NUMBER := 0;
  LF_OUTPUT     UTL_FILE.FILE_TYPE;
  LV_DIRECTORIO VARCHAR2(50);
  LN_ERROR      NUMBER;
  LV_USUFINAN27 VARCHAR2(50);
  LV_IPFINAN27  VARCHAR2(50);
  LV_RUTAREMOTA VARCHAR2(1000);
  LV_ERRORFTP   VARCHAR2(1000);
  LV_MENSAJE    VARCHAR2(4000) := NULL;
  LV_ASUNTO     VARCHAR2(100);
  Le_Error      EXCEPTION;
  
BEGIN
  
  LV_ASUNTO    := 'RELOJ DE COBRANZAS - INFORME DEL PROCESO DE CLIENTES CON FINANCIAMIENTO DE EQUIPO';
  
  LV_IPFINAN27 := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695, 'REP_IP_FIN', PV_ERROR);
  
  IF PV_ERROR IS NOT NULL THEN
     RAISE LE_ERROR;
  END IF;
  
  LV_USUFINAN27 := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695, 'REP_USU_FIN', PV_ERROR);
  IF PV_ERROR IS NOT NULL THEN
     RAISE LE_ERROR;
  END IF;
  
  LV_RUTAREMOTA := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695, 'REP_RUT_REPO', PV_ERROR);
  IF PV_ERROR IS NOT NULL THEN
     RAISE LE_ERROR;
  END IF;
  
  LV_DIRECTORIO := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(10695, 'REP_NOM_DIRE', PV_ERROR);
  IF PV_ERROR IS NOT NULL THEN
     RAISE LE_ERROR;
  END IF;
  
  FOR J IN C_BITACORA(PV_BITACORA_PROCESO) LOOP
      
      OPEN C_DETALLE_BITACORA(J.ID_BITACORA);
      FETCH C_DETALLE_BITACORA BULK COLLECT INTO LT_CUENTA;
      CLOSE C_DETALLE_BITACORA;
      
      IF LT_CUENTA.COUNT > 0 THEN
         
         FOR I IN LT_CUENTA.FIRST .. LT_CUENTA.LAST LOOP
             
             LN_COUNT := LN_COUNT + 1;
             IF LN_COUNT = 1 THEN
                LF_OUTPUT := UTL_FILE.FOPEN(LV_DIRECTORIO, PV_NOMBRE_ARCHIVO, 'W');
                UTL_FILE.PUT_LINE(LF_OUTPUT, 'CUENTA' || CHR(9) || 'ERROR');
             END IF;
             
             IF LT_CUENTA(I).ERROR = 'ERROR_PRINCIPAL' THEN
                UTL_FILE.PUT_LINE(LF_OUTPUT, LT_CUENTA(I).ERROR || CHR(9) || LT_CUENTA(I).MENSAJE_TECNICO);
             ELSIF LT_CUENTA(I).CUENTA IS NOT NULL THEN
                UTL_FILE.PUT_LINE(LF_OUTPUT, LT_CUENTA(I).CUENTA || CHR(9) || LT_CUENTA(I).MENSAJE_TECNICO);
             END IF;
         END LOOP;
      END IF;
      
  END LOOP;
  
  IF LN_COUNT > 0 THEN
     UTL_FILE.FCLOSE(LF_OUTPUT);
     LV_MENSAJE := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11735, 'REP_CLI_FINAN_EQ_MEN_ERROR', PV_ERROR);
     
     SCP_DAT.SCK_FTP_GTW.SCP_TRANSFIERE_ARCHIVO(PV_IP                => LV_IPFINAN27,
                                                PV_USUARIO           => LV_USUFINAN27,
                                                PV_RUTA_REMOTA       => LV_RUTAREMOTA,
                                                PV_NOMBRE_ARCH       => PV_NOMBRE_ARCHIVO,
                                                PV_RUTA_LOCAL        => LV_DIRECTORIO,
                                                PV_BORRAR_ARCH_LOCAL => 'N',
                                                PN_ERROR             => LN_ERROR,
                                                PV_ERROR             => LV_ERRORFTP);
     
     IF Lv_ErrorFtp IS NULL THEN
        Lv_RutaRemota := '</ARCHIVO1=' || PV_NOMBRE_ARCHIVO ||'|DIRECTORIO1=' || Lv_RutaRemota || '|/>';
     ELSE
        Lv_RutaRemota := NULL;
     END IF;
     
     SYSADM.RC_TRX_UTILITIES.RC_NOTIFICACIONES(PN_ID_PROCESO       => 0,
                                               PV_JOB              => NULL,
                                               PV_SHELL            => 'REP_CUENTAS_FINAN_EQUIP.SH',
                                               PV_OBJETO           => 'PCK_REP_CLIENTES_FINAN.P_CARGA_CTA',
                                               PV_MENSAJE_NOTIF    => LV_MENSAJE,
                                               PV_ASUNTO           => LV_ASUNTO,
                                               PV_RUTAREMOTA       => LV_RUTAREMOTA,
                                               PV_IDDESTINATARIO   => 'NCF',
                                               PV_TipoNotificacion => 'ERROR',
                                               PN_ERROR            => LN_ERROR,
                                               PV_ERROR            => PV_ERROR);
     
  ELSE
     LV_MENSAJE := sysadm.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(11735, 'REP_CLI_FINAN_EQ_MEN_OK', PV_ERROR);
     
     SYSADM.RC_TRX_UTILITIES.RC_NOTIFICACIONES(PN_ID_PROCESO       => 0,
                                               PV_JOB              => NULL,
                                               PV_SHELL            => 'REP_CUENTAS_FINAN_EQUIP.SH',
                                               PV_OBJETO           => 'PCK_REP_CLIENTES_FINAN.P_CARGA_CTA',
                                               PV_MENSAJE_NOTIF    => LV_MENSAJE,
                                               PV_ASUNTO           => LV_ASUNTO,
                                               PV_RUTAREMOTA       => NULL,
                                               PV_IDDESTINATARIO   => 'NCF',
                                               PV_TipoNotificacion => 'OK',
                                               PN_ERROR            => LN_ERROR,
                                               PV_ERROR            => PV_ERROR);
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    Pv_Error := 'P_CONFIRMAR_PROCESO => ' || Pv_Error;
  WHEN OTHERS THEN
    Pv_Error := 'P_CONFIRMAR_PROCESO => ' || SUBSTR(SQLERRM, 1, 300);
END P_CONFIRMAR_PROCESO;

PROCEDURE P_TRASPASO_DATOS (Pv_Reproceso         VARCHAR2,
                            Pv_Error         OUT VARCHAR2)IS
  
  CURSOR C_NUEVO_CORTE IS
  SELECT COUNT(*)
    FROM SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO X
   WHERE X.FECHA_INGRESO >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY')||' 00:00:01', 'DD/MM/YYYY HH24:MI:SS');
  
  CURSOR C_ACTUALIZA_REGISTRO IS
  SELECT *
    FROM SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO X
   WHERE X.ESTADO = 'A'
     AND X.FECHA_INGRESO < TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY')||' 00:00:01', 'DD/MM/YYYY HH24:MI:SS')
     AND X.FECHA_ACTUALIZACION >= TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY')||' 00:00:01', 'DD/MM/YYYY HH24:MI:SS');
  
  Ln_Existe      NUMBER;
  Ln_Commit      NUMBER;
  Lv_Consulta    VARCHAR2(1000);
  Lv_Insert      VARCHAR2(1000);
  Lv_Update      VARCHAR2(1000);
  Lv_Delete      VARCHAR2(1000);
  Lv_DbLinkBscs  VARCHAR2(100);
  
BEGIN
  
  Lv_DbLinkBscs := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11735,
                                                                        PV_ID_PARAMETRO      => 'REP_CLI_FINAN_EQ_DBLINK');
  
  IF Pv_Reproceso = 'R' THEN
     
     BEGIN
       
       Lv_Consulta := 'SELECT COUNT(1) FROM SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO@<DB_LINK> X
                       WHERE X.FECHA_INGRESO >= TO_DATE(''<FECHA> 00:00:01'', ''DD/MM/YYYY HH24:MI:SS'')';
       
       Lv_Consulta := REPLACE(Lv_Consulta,'<DB_LINK>',Lv_DbLinkBscs);
       Lv_Consulta := REPLACE(Lv_Consulta,'<FECHA>',TO_CHAR(SYSDATE,'DD/MM/YYYY'));
       
       EXECUTE IMMEDIATE Lv_Consulta INTO Ln_Existe;
       
     EXCEPTION
       WHEN OTHERS THEN
         Ln_Existe := 0;
     END;
     
     IF Ln_Existe > 0 THEN
        Lv_Delete := 'DELETE FROM SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO@<DB_LINK> X 
                      WHERE X.FECHA_INGRESO >= TO_DATE(''<FECHA> 00:00:01'', ''DD/MM/YYYY HH24:MI:SS'')';
        
        Lv_Delete := REPLACE(Lv_Delete,'<DB_LINK>',Lv_DbLinkBscs);
        Lv_Delete := REPLACE(Lv_Delete,'<FECHA>',TO_CHAR(SYSDATE,'DD/MM/YYYY'));
        
        EXECUTE IMMEDIATE Lv_Delete;
        
     END IF;
     
  END IF;
  
  OPEN C_NUEVO_CORTE;
  FETCH C_NUEVO_CORTE INTO Ln_Existe;
  CLOSE C_NUEVO_CORTE;
  
  IF Ln_Existe > 0 THEN
     Lv_Insert := 'INSERT INTO SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO@<DB_LINK>
                   SELECT * FROM SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO X
                   WHERE X.FECHA_INGRESO >= TO_DATE(TO_CHAR(SYSDATE,''DD/MM/YYYY'')||'' 00:00:01'', ''DD/MM/YYYY HH24:MI:SS'')';
     
     Lv_Insert := REPLACE(Lv_Insert,'<DB_LINK>',Lv_DbLinkBscs);
     EXECUTE IMMEDIATE Lv_Insert;
     
     COMMIT;
     
  END IF;
  
  FOR I IN C_ACTUALIZA_REGISTRO LOOP
      
      Lv_Update :='UPDATE SYSADM.FIN_DET_CLIENTE_FINANCIAMIENTO@<DB_LINK> X
                      SET X.SALDO_PENDIENTE = <SALDO>,
                          X.FECHA_ACTUALIZACION = TO_DATE(''<FECHA_MODIF>'',''DD/MM/YYYY HH24:MI:SS'')
                    WHERE X.CUENTA = <CUENTA>
                      AND X.FECHA_CORTE = TO_DATE(''<FECHA_CORTE>'',''DD/MM/YYYY'')';
      
      Lv_Update := REPLACE(Lv_Update,'<DB_LINK>',Lv_DbLinkBscs);
      Lv_Update := REPLACE(Lv_Update,'<SALDO>',I.SALDO_PENDIENTE);
      Lv_Update := REPLACE(Lv_Update,'<FECHA_MODIF>',I.FECHA_ACTUALIZACION);
      Lv_Update := REPLACE(Lv_Update,'<CUENTA>',I.CUENTA);
      Lv_Update := REPLACE(Lv_Update,'<FECHA_CORTE>',I.FECHA_CORTE);
      
      EXECUTE IMMEDIATE Lv_Update;
      
      IF Ln_Commit >= 1000 THEN
         Ln_Commit :=0;
         COMMIT;
      END IF;
      
  END LOOP
  
  COMMIT;
  
  AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    ROLLBACK;
    Pv_Error := 'P_TRASPASO_DATOS => ' || SUBSTR(SQLERRM, 1, 300);
END P_TRASPASO_DATOS;

END PCK_REP_CLIENTES_FINAN;
/
