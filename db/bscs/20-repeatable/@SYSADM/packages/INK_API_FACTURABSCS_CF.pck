CREATE OR REPLACE PACKAGE INK_API_FACTURABSCS_CF IS

  ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Sergio Concha.
  -- Modify:   CLS Cinthia Quinde , CLS F�lix Pazmi�o - FPA
  -- Created:  02/07/2015 14:55:37
  -- Proyecto: [9953]-Proyecto documento electronicos por reimpresion de servicios no inventariables
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Ernesto Idrovo
  -- Version:   1.0
  -- Motivo:    Paquete    que realiza consultas de las facturas de BSCS y retorna un XML
  ---------------------------------------------------------------------------------------------------
  TYPE Gr_ValoresRubros IS RECORD(
    valor     NUMBER,
    descuento NUMBER,
    factura   VARCHAR2(100),
    sncode     VARCHAR2(1000),
    id_sncode  VARCHAR2(1000)
    );

  TYPE Gt_ValoresRubros IS TABLE OF Gr_ValoresRubros INDEX BY BINARY_INTEGER;
  TYPE R_DATOS IS RECORD(
           nombre2          VARCHAR2(1000), 
           valor            NUMBER,
           customer_id      VARCHAR2(50),
           custcode         VARCHAR2(50),
           ohrefnum         VARCHAR2(100),
           tipo             VARCHAR2(50),
           nombre           VARCHAR2(1000),
           servicio         VARCHAR2(100),
           descuento        NUMBER,
           cobrado          NUMBER,
           ctaclblep        VARCHAR2(30),
           cta_devol        VARCHAR2(30),
           tipo_iva         VARCHAR2(30),
           id_producto      VARCHAR2(5));
  TYPE lt_datos IS TABLE OF R_DATOS INDEX BY BINARY_INTEGER;  
  FUNCTION GETPARAMETROS(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 ;
  
  FUNCTION GETDATOTRAMA(PV_TRAMA     IN VARCHAR2,
                        PV_DATO      IN VARCHAR2,
                        PV_SEPARATOR IN VARCHAR2 DEFAULT ';') RETURN VARCHAR2;
    ------------------------------------------------------------------------------
   --INI CQU 08/07/2016 10897 
  -- FUNCION QUE RETORNA EL DATO SEGUN LA POSICION QUE ENVIE
  ------------------------------------------------------------------------------
  FUNCTION GETDATOTRAMAPOS(PV_TRAMA     IN VARCHAR2,
                           PN_POSI      IN NUMBER) RETURN VARCHAR2 ;                        
  
  --Procedimiento que obtiene los rubros con la informacion de bscs y la inserta en una tabla temporal
  PROCEDURE PRC_OBTIENE_RUBROS(pv_fecha       IN  VARCHAR2,
                               pv_cuenta      IN  VARCHAR2,
                               pv_factura_sri IN  VARCHAR2,
                               pv_error       OUT VARCHAR2);
  
  --Procedimiento que obtiene los rubros con la informacion de bscs y la inserta en una tabla temporal
  procedure PRC_OBTIENE_RUBROS_LLAMADAS(pv_fecha       IN  VARCHAR2,
                                        pv_cuenta      IN  OUT VARCHAR2,
                                        pv_factura_sri IN  VARCHAR2,
                                        pv_error       OUT VARCHAR2);
  --PROCEDIMIENTO QUE RETORNA LA TRAMA DE LOS PRODUCTOS DE LA FACTURA DE BSCS CONSUMIDA DE LA TABLA TEMPORAL
  /*PROCEDURE PRC_TRAMA_FACTURA(PV_PARAMETROS    IN  VARCHAR2,
                              pv_mensaje_error OUT VARCHAR2,
                              pv_cod_error     OUT VARCHAR2,
                              pv_resultado     OUT LONG) ;*/
                             
  --PROCEDIMIENTO QUE INSERTA EN LA NUEVA TABLA DE CONTROL DE FACTURAS DE BSCS PROCESADAS CON SU NUEVA FACTURA SRI
  PROCEDURE PRC_INSERTA_CABECERA (pv_factura_bscs     IN  VARCHAR2,
                                  pv_factura_sri_caja IN  VARCHAR2,
                                  pd_fecha_factura    IN  DATE,
                                  pn_id_hist          IN NUMBER,--FPA
                                  pv_mensaje          OUT VARCHAR2,
																  pv_cod_error        OUT VARCHAR2);
 
   /*INI CLS FPA 9953*/                                 
  --PROCEDIMIENTO QUE ELIMINA LA INFORMACION DE LA TABLA TEMPORAL QUE CONTIENE LOS DATOS DE LOS RUBRO DE UNA FACTURA DE BSCS
  PROCEDURE PRC_ELIMINA_WF_COMPLAIN (PV_FACTURA_SRI     IN  VARCHAR2,
                                    PV_CTA             IN  VARCHAR2,
                                    PV_ERROR           OUT VARCHAR2);
  /*FIN CLS FPA 9953*/    
  
  --PROCEDIMIENTO QUE INSERTA EN TABLAS DE WF_COMPLAIN_TEMPORAL_CF
  --INI CQU PROY 10755 28/03/2015
  PROCEDURE PRC_TRAMA_FACTURA_NEW(PV_PARAMETROS    IN  VARCHAR2,
                                  PV_MENSAJE_ERROR OUT VARCHAR2,
                                  PV_COD_ERROR     OUT VARCHAR2) ;                              

    --INI 10987 ELIMINA VALORES DUPLICADOS EN LA TRAMA
   PROCEDURE PRC_ELIMINA_DUPLICADOS(PV_TRAMA       IN  VARCHAR2,
                                    PV_TRAMA_NUEVA     OUT  VARCHAR2);
END INK_API_FACTURABSCS_CF;
/
CREATE OR REPLACE PACKAGE BODY INK_API_FACTURABSCS_CF IS

 ---------------------------------------------------------------------------------------------------
  -- Author:   CLS Sergio Concha.
  -- Modify:   CLS Cinthia Quinde, CLS F�lix Pazmi�o
  -- Created:   02/07/2015 14:55:37
  -- Proyecto: [9953]-Proyecto documento electronicos por reimpresion de servicios no inventariables
  -- Lider cls: Ing. Mariuxi Dom�nguez
  -- Lider sis: Ing. Ernesto Idrovo
  -- Version:   1.0
  -- Motivo:    Paquete que realiza consultas de las facturas de BSCS y retorna un XML
  ---------------------------------------------------------------------------------------------------
 FUNCTION GETPARAMETROS(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS
    LV_VALOR_PAR   VARCHAR2(1000);
    LV_PROCESO     VARCHAR2(1000);
    LV_DESCRIPCION VARCHAR2(1000);
    LN_ERROR       NUMBER;
    LV_ERROR       VARCHAR2(1000);
  BEGIN
    SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(PV_COD_PARAMETRO,
                                                LV_PROCESO,
                                                LV_VALOR_PAR,
                                                LV_DESCRIPCION,
                                                LN_ERROR,
                                                LV_ERROR);
    RETURN LV_VALOR_PAR;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END GETPARAMETROS;
 

 FUNCTION GETDATOTRAMA(PV_TRAMA     IN VARCHAR2,
                        PV_DATO      IN VARCHAR2,
                        PV_SEPARATOR IN VARCHAR2 DEFAULT ';') RETURN VARCHAR2 IS
  
    LN_POSINI PLS_INTEGER;
    LN_POSFIN PLS_INTEGER;
    LV_DUMMY  VARCHAR2(4000);
    NTH       NUMBER := 1;
  
  BEGIN
  
    IF NTH < 0 THEN
      LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', NTH);
    ELSE
      LN_POSINI := INSTR(PV_TRAMA, PV_DATO || '=', 1, NTH);
    END IF;
  
    IF LN_POSINI > 0 THEN
      LV_DUMMY  := SUBSTR(PV_TRAMA, LN_POSINI);
      LN_POSFIN := INSTR(LV_DUMMY, PV_SEPARATOR);
      IF LN_POSFIN > 0 THEN
        LV_DUMMY := SUBSTR(LV_DUMMY, 1, LN_POSFIN - 1);
      END IF;
      LV_DUMMY := SUBSTR(LV_DUMMY, INSTR(LV_DUMMY, '=') + 1);
    END IF;
  
    RETURN LV_DUMMY;
  
  END GETDATOTRAMA;
    ------------------------------------------------------------------------------
   --INI CQU 08/07/2016 10897 
  -- FUNCION QUE RETORNA EL DATO SEGUN LA POSICION QUE ENVIE
  ------------------------------------------------------------------------------
  FUNCTION GETDATOTRAMAPOS(PV_TRAMA     IN VARCHAR2,
                           PN_POSI      IN NUMBER) RETURN VARCHAR2 IS

  CADENA_TOTAL     VARCHAR2(100):=NULL;
  LV_CAD           VARCHAR2(500):=NULL;
  POS        NUMBER;
  CONT     NUMBER:=0;
  LV_CADENA  VARCHAR2(500):=NULL;
  BEGIN
    LV_CADENA:=PV_TRAMA||';';
    LOOP 
            POS:=INSTR(LV_CADENA,'|',1); 
            IF POS>0 THEN
              CONT:=CONT+1;
             END IF;
            CADENA_TOTAL:=SUBSTR(LV_CADENA,1,POS-1); 
            LV_CADENA:=SUBSTR(LV_CADENA,POS+1); 
            IF(POS=0)THEN 
                CADENA_TOTAL:=LV_CADENA; 
            END IF;             
            IF CONT=PN_POSI AND POS >0 THEN
              LV_CAD:=CADENA_TOTAL;             
            END IF;
        EXIT WHEN POS=0;
        END LOOP;
        RETURN LV_CAD;
   EXCEPTION
   WHEN OTHERS THEN
    RETURN NULL;
   END GETDATOTRAMAPOS;                           
   --FIN CQU 08/07/2016 10897 
  --Procedimiento que obtiene los rubros con la informacion de bscs y la inserta en una tabla temporal
  PROCEDURE PRC_OBTIENE_RUBROS(pv_fecha       IN  VARCHAR2,
                               pv_cuenta      IN  VARCHAR2,
                               pv_factura_sri IN  VARCHAR2,
                               pv_error       OUT VARCHAR2) IS

    CURSOR c_parametro (gn_tipo_parametro NUMBER, gv_id_parametro   VARCHAR2) IS
      SELECT valor
        FROM gv_parametros g
       WHERE g.id_tipo_parametro = gn_tipo_parametro
         AND g.id_parametro = gv_id_parametro;
    
    lv_query                 VARCHAR2(32000);
    --ini cqu 13/09/2016 valida productos de colector y bscs
    TYPE RC_CONSULTA IS REF CURSOR;
    LRC_CONSULTA RC_CONSULTA; 
    --ini cqu 18/11/2016
    Type RC_RUBROS   IS REF CURSOR;
    LRC_RUBROS RC_RUBROS;
    TYPE R_RUBROS IS RECORD(
           nombre2          VARCHAR2(1000), 
           valor            NUMBER,
           customer_id      VARCHAR2(50),
           custcode         VARCHAR2(50),
           ohrefnum         VARCHAR2(100),
           tipo             VARCHAR2(50),
           nombre           VARCHAR2(1000),
           servicio         VARCHAR2(100),
           descuento        NUMBER,
           cobrado          NUMBER,
           ctaclblep        VARCHAR2(30),
           cta_devol        VARCHAR2(30),
           tipo_iva         VARCHAR2(30),
           id_producto      VARCHAR2(5)); 
    LR_RUBROS R_RUBROS;     
    lt_tabla_tmp        lt_datos;
    ln_ind              PLS_INTEGER := 0;
    ln_sum_valor        number:=0;
    ln_sum_desc         number:=0;--ini cqu 11397 15/06/2017
    --fin cqu 18/11/2016
    lv_query2                 VARCHAR2(1000);
    lv_nombre_pro             varchar2(100);
    li_servicio               integer;
    lv_codigo_prod            VARCHAR2(25); 
    lv_codigo_prod2           VARCHAR2(200);  
    lv_query_i                VARCHAR2(1500);   
    lv_trama_cod              VARCHAR2(1500);      
    lv_trama                  VARCHAR2(1500);
    lv_trama_cod2             VARCHAR2(1500); 
    ln_exite                  number:=0;
    lb_flag                   boolean:=false;
    lb_flag_aux               boolean:=false;
    lb_flag_aux2              boolean:=false;
    LV_CADENA                 VARCHAR2(1500);
    LN_POS                    NUMBER:=0;
    ln_valor                  NUMBER:=0;
    LV_RES_PROD               VARCHAR2(1000);
    --fin cqu 13/09/2016
    Lv_fecha_cobro_iva_roam  GV_PARAMETROS.VALOR%TYPE;
    ln_cobrado               NUMBER:=0;
    ln_cuenta_reg            NUMBER:=0;  
    ld_fecha_servcode        date;
    lv_query_ice_n          VARCHAR2(1000);
    lv_query_ice            VARCHAR2(1000);
  BEGIN
    

    lv_query := 'SELECT a.nombre2,
                        decode( SUBSTR(a.nombre2,1,5),''DSCTO'', 0,VALOR) VALOR ,
                        a.customer_id,
                        a.custcode,
                        a.ohrefnum,
                        a.tipo,
                        TRIM(DECODE(a.servicio, 3, ''I.V.A. Por servicios (12%)'',4,''ICE de Telecomunicaci�n (15%)'', b.des)) NOMBRE,
                        (SELECT TO_CHAR(z.servicio)
                           FROM cob_servicios z
                          WHERE z.servicio=a.servicio
                            AND ROWNUM < 2) SERVICIO,
                      decode( SUBSTR(a.nombre2,1,5),''DSCTO'', (VALOR*-1), a.descuento) descuento ,
                       decode( SUBSTR(a.nombre2,1,5),''DSCTO'', a.valor*-1 ,a.valor - a.descuento ) cobrado ,
                        (SELECT ctapsoft
                           FROM cob_servicios z
                          WHERE z.servicio=a.servicio
                            AND z.tipo in (''003 - IMPUESTOS'',''002 - ADICIONALES'')
                            AND ROWNUM < 2) CTACLBLEP,
                        (SELECT cta_devol
                           FROM cob_servicios
                          WHERE servicio = a.servicio
                            AND tipo in (''003 - IMPUESTOS'',''002 - ADICIONALES'')
                            AND ctactble = a.cble) CTA_DEVOL,
                        (SELECT tipo_iva
                           FROM cob_servicios
                          WHERE servicio = a.servicio                        
                            AND ctactble = a.cble) TIPO_IVA,
                        b.shdes
                   FROM sysadm.co_fact_' || pv_fecha || ' a,
                        mpusntab                          b
                  WHERE a.servicio = b.sncode 
                    AND a.custcode = :1
                    AND a.ohrefnum = :2
                    AND a.servicio NOT IN (SELECT SUBSTR(otname,INSTR(otname, ''.'', 1, 3) + 1,INSTR(SUBSTR(otname, INSTR(otname, ''.'', 1, 3) + 1), ''.'') - 1)
                                             FROM ordertrailer a,
                                                  orderhdr_all b,
                                                  customer_all c
                                            WHERE a.otshipdate = TO_DATE(:3, ''DDMMYYYY'')
                                              AND b.ohstatus IN (''IN'', ''CM'')
                                              AND b.ohuserid IS NULL
                                              AND a.otshipdate = b.ohentdate
                                              AND a.otxact = b.ohxact
                                              AND b.customer_id = c.customer_id 
                                              AND c.custcode = :4
                                              AND b.ohrefnum = :5
                                              AND (otname LIKE (''%.BASE.%'') OR otname LIKE (''%.IC.%''))
                                            GROUP BY SUBSTR(otname,INSTR(otname, ''.'', 1, 3) + 1,INSTR(SUBSTR(otname, INSTR(otname, ''.'', 1, 3) + 1),''.'') - 1))';
    
 
    lt_tabla_tmp.DELETE; 
    OPEN LRC_RUBROS FOR lv_query 
    USING IN pv_cuenta,pv_factura_sri,pv_fecha,pv_cuenta,pv_factura_sri; 
    
    LOOP
      FETCH LRC_RUBROS INTO LR_RUBROS;
      EXIT WHEN LRC_RUBROS%NOTFOUND;
      lt_tabla_tmp(ln_ind).nombre2      :=LR_RUBROS.nombre2;
      lt_tabla_tmp(ln_ind).valor        :=LR_RUBROS.valor;    
      lt_tabla_tmp(ln_ind).customer_id  :=LR_RUBROS.customer_id;  
      lt_tabla_tmp(ln_ind).custcode     :=LR_RUBROS.custcode; 
      lt_tabla_tmp(ln_ind).ohrefnum     :=LR_RUBROS.ohrefnum;  
      lt_tabla_tmp(ln_ind).tipo         :=LR_RUBROS.tipo;   
      lt_tabla_tmp(ln_ind).nombre       :=LR_RUBROS.nombre;    
      lt_tabla_tmp(ln_ind).servicio     :=LR_RUBROS.servicio;  
      lt_tabla_tmp(ln_ind).descuento    :=LR_RUBROS.descuento;   
      lt_tabla_tmp(ln_ind).cobrado      :=LR_RUBROS.cobrado;  
      lt_tabla_tmp(ln_ind).ctaclblep    :=LR_RUBROS.ctaclblep;  
      lt_tabla_tmp(ln_ind).cta_devol    :=LR_RUBROS.cta_devol; 
      lt_tabla_tmp(ln_ind).tipo_iva     :=LR_RUBROS.tipo_iva;  
      lt_tabla_tmp(ln_ind).id_producto  :=LR_RUBROS.id_producto;
      ln_ind:=ln_ind+1;   
   END LOOP;
   CLOSE LRC_RUBROS;
   IF lt_tabla_tmp.COUNT > 0 then
     For d in lt_tabla_tmp.FIRST .. lt_tabla_tmp.LAST loop
       lb_flag:=false;
       lv_codigo_prod:=null;
       lv_codigo_prod2:=null;
       ln_sum_valor:=0;
       ln_sum_desc:=0;
       ln_cobrado:=0;
       ln_sum_valor:=lt_tabla_tmp(d).valor;
       ln_sum_desc:=lt_tabla_tmp(d).descuento;
       lv_codigo_prod2:=lt_tabla_tmp(d).id_producto;
       ln_cobrado:=nvl(lt_tabla_tmp(d).cobrado,0);
       ln_exite:=0;
       lb_flag_aux:=false;
       lb_flag_aux2:=false;
       ln_cuenta_reg:=0;
       For x in lt_tabla_tmp.FIRST .. lt_tabla_tmp.LAST loop  
           lb_flag:=false;
           if lt_tabla_tmp(d).nombre = lt_tabla_tmp(x).nombre then            
                 if lt_tabla_tmp(d).servicio = lt_tabla_tmp(x).servicio then                  
                     ln_cuenta_reg:=ln_cuenta_reg+1;               
                     if ln_cuenta_reg>1 then
                       ln_sum_valor:=ln_sum_valor+nvl(lt_tabla_tmp(x).valor,0);
                       ln_sum_desc:=ln_sum_desc+nvl(lt_tabla_tmp(x).descuento,0);
                       ln_exite:=0;                       
                       if not lb_flag_aux2 then
                          lv_trama_cod2:=lv_trama_cod2||lt_tabla_tmp(d).nombre||'|';  
                       end if; 
                       ln_cobrado:=ln_cobrado+nvl(lt_tabla_tmp(x).cobrado,0);
                       lb_flag_aux2:=true;
                     end if;
                   
                 end if;     
                      
              if lt_tabla_tmp(d).servicio <> lt_tabla_tmp(x).servicio  then
                lb_flag:=true;
                lb_flag_aux:=true;
                ln_sum_valor:=ln_sum_valor+nvl(lt_tabla_tmp(x).valor,0);
                lv_codigo_prod2:=lv_codigo_prod2||';'||lt_tabla_tmp(x).id_producto;
                ln_cobrado:=ln_cobrado+nvl(lt_tabla_tmp(x).cobrado,0);
              end if;
           else
             ln_exite:=0;
           end if;
           if lb_flag then
             lv_trama_cod:=lv_trama_cod||lt_tabla_tmp(d).nombre||'|';           
           end if;  
       End Loop;  
       --loop que verifica cuantos hay repetidos
       if lv_trama_cod is not null or lv_trama_cod2 is not null then
         if  lb_flag_aux then
            LV_CADENA:=lv_trama_cod;
            lv_trama:=lv_trama_cod;
         elsif lb_flag_aux2 then
            LV_CADENA:=lv_trama_cod2;
            lv_trama:=lv_trama_cod2;
         end if;
            LN_POS:=0;
            ln_valor:=0;
            BEGIN
             LOOP 
                  LN_POS:=NVL(INSTR(LV_CADENA,'|',1),0);
                  IF LN_POS<>0 THEN
                      ln_valor:=ln_valor+1;  
                  END IF;
                  LV_CADENA:=SUBSTR(LV_CADENA,LN_POS+1);  
              EXIT WHEN LN_POS=0;
              END LOOP;  
          
          --for que recorrera para saber cuantos repetidos en la cadena habra
          for r in 1 ..  ln_valor loop
           LV_RES_PROD:=null;
          --LV_CADENA:=lv_trama_cod||'|'; --PAQ. 150 MIN EMP INCL MULT|PAQ. 150 MIN EMP INCL MULT|
          LV_RES_PROD:=getdatotramapos(pv_trama => lv_trama,
                                       pn_posi =>  r);
          if LV_RES_PROD is not null then            
            if LV_RES_PROD =lt_tabla_tmp(d).nombre then
              ln_exite:=ln_exite+1;
            end if;
          end if;
          end loop;
          
      exception
      when others then
        ln_exite:=0;
      end;
      end if;
       if  ln_exite =0  or ln_exite = 1 then

            lv_query_i := 'INSERT INTO wf_complain_temporal_cf(nombre2,
                                                         valor,
                                                         customer_id,
                                                         custcode,
                                                         ohrefnum,
                                                         tipo,
                                                         nombre,
                                                         servicio,
                                                         descuento,
                                                         cobrado,
                                                         ctaclblep,
                                                         cta_devol,
                                                         tipo_iva,
                                                         id_producto,
                                                         id_producto2)
                           VALUES
                          (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15)';
                 
              EXECUTE IMMEDIATE lv_query_i USING IN lt_tabla_tmp(d).nombre2,
                                                       nvl(ln_sum_valor,0),
                                                       lt_tabla_tmp(d).customer_id,
                                                       lt_tabla_tmp(d).custcode,
                                                       lt_tabla_tmp(d).ohrefnum,
                                                       lt_tabla_tmp(d).tipo,
                                                       lt_tabla_tmp(d).nombre,
                                                       lt_tabla_tmp(d).servicio,
                                                       ln_sum_desc,--lt_tabla_tmp(d).descuento,
                                                       ln_cobrado,
                                                       lt_tabla_tmp(d).ctaclblep,
                                                       lt_tabla_tmp(d).cta_devol,
                                                       lt_tabla_tmp(d).tipo_iva,
                                                       lt_tabla_tmp(d).id_producto,
                                                       lv_codigo_prod2||';';
        end if;                                                     
                                                     
       
         
     End loop;
   end if;                                          

    OPEN c_parametro(10182,'FECHA_INICIO_COBRO_IVA_ROAM');
    FETCH c_parametro
      INTO lv_fecha_cobro_iva_roam;
    CLOSE c_parametro;
  
    IF TO_DATE(pv_fecha,'DD/MM/YYYY') < TO_DATE(lv_fecha_cobro_iva_roam,'DD/MM/YYYY') THEN
    
      lv_query := 'UPDATE wf_complain_temporal_cf 
                      SET tipo_iva = ''EXP'',
                          tipo = ''004 - EXENTO''
                    WHERE nombre2 LIKE ''%ROAM%''
                      AND ohrefnum = :1';  
    
      EXECUTE IMMEDIATE lv_query USING IN pv_factura_sri;
    
    END IF;
    
    lv_query := 'UPDATE wf_complain_temporal_cf
                    SET valor     = (SELECT SUM(valor)
                                       FROM wf_complain_temporal_cf
                                      WHERE tipo = ''003 - IMPUESTOS''
                                        AND nombre2 LIKE ''IVA_%''
                                        AND ohrefnum = :1),
                        descuento = (SELECT SUM(descuento)
                                       FROM wf_complain_temporal_cf
                                      WHERE tipo = ''003 - IMPUESTOS''
                                        AND nombre2 LIKE ''IVA_%''
                                        AND ohrefnum = :2),
                        cobrado   = (SELECT SUM(cobrado)
                                       FROM wf_complain_temporal_cf
                                      WHERE tipo = ''003 - IMPUESTOS''
                                        AND nombre2 LIKE ''IVA_%''
                                        AND ohrefnum = :3)
                  WHERE tipo = ''003 - IMPUESTOS''
                    AND nombre2 LIKE ''IVA_%''';
       
    EXECUTE IMMEDIATE lv_query USING IN pv_factura_sri,
                                        pv_factura_sri,
                                        pv_factura_sri;
     
     --ini cqu  02/05/2017 Setea excluye_ice a 2 cuando tiene encuentra *N
    lv_query_ice_n := 'UPDATE wf_complain_temporal_cf 
                      SET excl_ice = ''2''
                      WHERE id_producto in(select /*+rule+*/ distinct t.shdes from gsib_features_ice f ,mpusntab t
                                          where f.sncode=t.sncode(+)
                                          and f.sncode IS NOT NULL
                                          and f.sncode not like ''%-%''
                                          and f.aplica_ice=''NO'')                   
                    AND ohrefnum = :1
                    AND tipo <> ''003 - IMPUESTOS''';   
    
    EXECUTE IMMEDIATE lv_query_ice_n USING IN pv_factura_sri;
    
     --ini cqu  02/05/2017 Setea excluye_ice a 3 cuando tiene encuentra *S
    lv_query_ice := 'UPDATE wf_complain_temporal_cf 
                      SET excl_ice = ''3''
                      WHERE id_producto in(select /*+rule+*/ distinct t.shdes from gsib_features_ice f ,mpusntab t
                                          where f.sncode=t.sncode(+)
                                          and f.sncode IS NOT NULL
                                          and f.sncode not like ''%-%''
                                          and f.aplica_ice=''SI'')                   
                    AND ohrefnum = :1
                    AND tipo <> ''003 - IMPUESTOS''';   
    
    EXECUTE IMMEDIATE lv_query_ice USING IN pv_factura_sri;
    --fin cqu 02/05/2017
     lv_query := 'UPDATE wf_complain_temporal_cf 
                      SET excl_ice = ''1''
                    WHERE id_producto in(select shdes from IN_EXCL_PROD_BSCS t where t.estado=''A'')                   
                    AND ohrefnum = :1';   
    
    EXECUTE IMMEDIATE lv_query USING IN pv_factura_sri;
   --ini cqu 09/09/2016
   lv_query2:='SELECT c.nombre,c.servicio
              FROM WF_COMPLAIN_TEMPORAL_CF
              W,READ.COB_SERVICIOS C
              WHERE W.SERVICIO=C.SERVICIO
              AND  W.OHREFNUM=:1
              AND UPPER(W.NOMBRE)<>C.NOMBRE
              AND W.TIPO_IVA IS NOT NULL';
              
   OPEN LRC_CONSULTA FOR lv_query2 
   USING IN pv_factura_sri;
   FETCH LRC_CONSULTA INTO lv_nombre_pro,li_servicio;
   CLOSE LRC_CONSULTA; 
   --Validacion para facturas de Mayo Cuando no coinciden las descripcion con colector
   IF TO_CHAR(TO_DATE(pv_fecha,'DD/MM/YYYY'),'MM') = GETPARAMETROS('CF_FACT_PROC_004') THEN
      lv_query := 'UPDATE wf_complain_temporal_cf x
                  SET x.NOMBRE = nvl(:1,x.NOMBRE)
                  WHERE x.ohrefnum = :2
                  AND x.custcode=:3
                  AND x.servicio=:4';  
    
      EXECUTE IMMEDIATE lv_query USING IN lv_nombre_pro,pv_factura_sri,pv_cuenta,li_servicio;
    
    END IF;
   
   --fin cqu       
    lv_query := 'DELETE wf_complain_temporal_cf
                  WHERE tipo = ''003 - IMPUESTOS''
                    AND nombre2 LIKE ''IVA_%''
                    AND nombre2 NOT IN (''IVA'',''IVA_DTH'')
                    AND ohrefnum = :1'; 
    
    EXECUTE IMMEDIATE lv_query USING IN pv_factura_sri;
    
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := SUBSTR(SQLERRM, 1,500);
      ROLLBACK;

  END PRC_OBTIENE_RUBROS;
  
  --Procedimiento que obtiene los rubros para llamadas con la informacion de bscs y la inserta en una tabla temporal
  PROCEDURE PRC_OBTIENE_RUBROS_LLAMADAS(pv_fecha       IN  VARCHAR2,
                                        pv_cuenta      IN  OUT VARCHAR2,
                                        pv_factura_sri IN  VARCHAR2,
                                        pv_error       OUT VARCHAR2) IS
  
    cursor c_obtiene_valores_llamadas is
      select substr(otname, 1, instr(otname, '.', '1', '1') - 1) tmcode,
             substr(otname,
                    instr(otname, '.', 1, 3) + 1,
                    instr(substr(otname, instr(otname, '.', 1, 3) + 1), '.') - 1) sncode,
             substr(otname,
                    instr(otname, '.', 1, 11) + 1,
                    instr(substr(otname, instr(otname, '.', 1, 11) + 1), '.') - 1) zncode,
             sum(a.otamt_revenue_gl) valor,
             sum(a.otamt_disc_doc) descuento,
             b.ohrefnum
        from ordertrailer a, orderhdr_all b, customer_all c
       where a.otshipdate = to_date(pv_fecha, 'ddmmyyyy')
         and b.OHSTATUS IN ('IN', 'CM')
         and b.OHUSERID IS NULL
         and a.otshipdate = b.ohentdate
         and a.otxact = b.ohxact
         and b.customer_id = c.customer_id
         and c.custcode = pv_cuenta
         and b.ohrefnum = pv_factura_sri
         and (otname like ('%.BASE.%') or otname like ('%.IC.%'))
       group by substr(otname, 1, instr(otname, '.', '1', '1') - 1),
                substr(otname,
                       instr(otname, '.', 1, 3) + 1,
                       instr(substr(otname, instr(otname, '.', 1, 3) + 1),
                             '.') - 1),
                substr(otname,
                       instr(otname, '.', 1, 11) + 1,
                       instr(substr(otname, instr(otname, '.', 1, 11) + 1),
                             '.') - 1),
                b.ohrefnum;
  
    cursor c_obtiene_mapeo is
      select tmcode,
             sncode,
             zncode,
             condicion_tmcode,
             condicion_sncode,
             condicion_zncode,
             label_id
        from wf_mapeo_rubros_llamadas
       where estado = 'A'
       order by label_id;
  
    cursor c_obtiene_doc1(cn_label_id number) is
      select DES,
             DECODE(UPPER(TAXINF),
                    'EXCENTO',
                    '004 - EXENTO',
                    '002 - ADICIONALES') tipo,
             CTAPS,
             CTA_DEVOL,
             d.codigo_sri
        from doc1.service_label_doc1 d
       where label_id = cn_label_id;
  
    cursor c_obtiene_customer_id(cv_cuenta varchar2) is
      select customer_id from customer_all where custcode = cv_cuenta;
    
    CURSOR C_PARAMETRO(GN_TIPO_PARAMETRO NUMBER, GV_ID_PARAMETRO   VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS G
       WHERE G.ID_TIPO_PARAMETRO = GN_TIPO_PARAMETRO
         AND G.ID_PARAMETRO = GV_ID_PARAMETRO;
    
    cursor c_obtiene_mpu (ln_sncode number ) is 
           select d.shdes from mpusntab d where d.sncode = ln_sncode; 
    cursor c_obtiene_cuenta is
      select c.custcode,
             b.ohrefnum
        from ordertrailer a, orderhdr_all b, customer_all c
       where a.otshipdate = to_date(pv_fecha, 'ddmmyyyy')
         and b.OHSTATUS IN ('IN', 'CM')
         and b.OHUSERID IS NULL
         and a.otshipdate = b.ohentdate
         and a.otxact = b.ohxact
         and b.customer_id = c.customer_id
         --and c.custcode = pv_cuenta
         and b.ohrefnum = pv_factura_sri;
    -- V A R I A B L E S --
    lv_error          varchar2(1000);
    lv_customer_id    varchar2(100);
    ln_band           number := 0;
    ln_band1          number := 0;
    ln_band2          number := 0;
    ln_band3          number := 0;
    lc_obtiene_doc1   c_obtiene_doc1%rowtype;
    lc_obtiene_cuenta c_obtiene_cuenta%rowtype;
    Lt_ValoresRubros  Gt_ValoresRubros;
    Lv_fecha_cobro_iva_roam  gv_parametros.valor%type;
    LV_TIPO_WF        VARCHAR2(25);
    ln_cont_roam      Number;
    lv_query          VARCHAR2(32000);
    lv_etiqueta       VARCHAR2(5);
    LV_CADENA         VARCHAR2(1025);
    LV_CADENA2         VARCHAR2(1025);
    LN_POSI            Number;
    LV_CONTENIDO       VARCHAR2(1025);
    LV_CONTENIDO1      VARCHAR2(1025);
    LV_TRAMA_NEW       VARCHAR2(1025);
    ln_v1    number;
    lv_dato VARCHAR2(1025);
  begin
    open c_obtiene_cuenta;
    fetch c_obtiene_cuenta into lc_obtiene_cuenta;
    close c_obtiene_cuenta;
    
    pv_cuenta := lc_obtiene_cuenta.custcode;
    
    open c_obtiene_customer_id(lc_obtiene_cuenta.custcode);
    fetch c_obtiene_customer_id
      into lv_customer_id;
    close c_obtiene_customer_id;
  
    for w in c_obtiene_valores_llamadas loop
      for i in c_obtiene_mapeo loop
        ln_band1 := 0;
        ln_band2 := 0;
        ln_band3 := 0;
        --An�lisis tmcode
        if i.tmcode is not null and i.condicion_tmcode is not null then
          if i.condicion_tmcode = '=' then
            if i.tmcode = w.tmcode then
              ln_band1 := 1;
            else
              ln_band1 := 0;
            end if;
          elsif i.condicion_tmcode = '<>' then
            if i.tmcode <> w.tmcode then
              ln_band1 := 1;
            else
              ln_band1 := 0;
            end if;
          end if;
        else
          ln_band1 := 1;
        end if;
        --An�lisis sncode
        if i.sncode is not null and i.condicion_sncode is not null then
          if i.condicion_sncode = '=' then
            if i.sncode = w.sncode then
              ln_band2 := 1;
            else
              ln_band2 := 0;
            end if;
          elsif i.condicion_sncode = '<>' then
            if i.sncode <> w.sncode then
              ln_band2 := 1;
            else
              ln_band2 := 0;
            end if;
          end if;
        else
          ln_band2 := 1;
        end if;
        --An�lisis zncode
        if i.zncode is not null and i.condicion_zncode is not null then
          if i.condicion_zncode = '=' then
            if i.zncode = w.zncode then
              ln_band3 := 1;
            else
              ln_band3 := 0;
            end if;
          elsif i.condicion_zncode = '<>' then
            if i.zncode <> w.zncode then
              ln_band3 := 1;
            else
              ln_band3 := 0;
            end if;
          end if;
        else
          ln_band3 := 1;
        end if;
      
        if ln_band1 = 1 and ln_band2 = 1 and ln_band3 = 1 then
          begin
            Lt_ValoresRubros(i.label_id).valor := nvl(Lt_ValoresRubros(i.label_id)
                                                      .valor,
                                                      0) + nvl(w.valor,0);
            Lt_ValoresRubros(i.label_id).descuento := nvl(Lt_ValoresRubros(i.label_id)
                                                          .descuento,
                                                          0) + nvl(w.descuento,0);
            Lt_ValoresRubros(i.label_id).factura := w.ohrefnum;
            IF Lt_ValoresRubros(i.label_id).sncode IS NOT NULL and w.sncode is not null THEN 
            Lt_ValoresRubros(i.label_id).sncode:=Lt_ValoresRubros(i.label_id).sncode; --MDO
            Lt_ValoresRubros(i.label_id).id_sncode:=Lt_ValoresRubros(i.label_id).sncode || ';' || w.sncode|| ';'; --MDO  
            else 
              Lt_ValoresRubros(i.label_id).sncode:= w.sncode;
              Lt_ValoresRubros(i.label_id).id_sncode:= w.sncode;
            end if; 
          exception
            when others then
              Lt_ValoresRubros(i.label_id).valor := nvl(w.valor,0);
              Lt_ValoresRubros(i.label_id).descuento := nvl(w.descuento,0);
              Lt_ValoresRubros(i.label_id).factura := w.ohrefnum;
              IF Lt_ValoresRubros(i.label_id).sncode IS NOT NULL and w.sncode is not null THEN 
                Lt_ValoresRubros(i.label_id).sncode:=Lt_ValoresRubros(i.label_id).sncode; --MDO
                Lt_ValoresRubros(i.label_id).id_sncode:=Lt_ValoresRubros(i.label_id).sncode || ';' || w.sncode|| ';'; --MDO  

            
              ELSE 
                Lt_ValoresRubros(i.label_id).sncode:= w.sncode; 
                Lt_ValoresRubros(i.label_id).id_sncode:= w.sncode; 
              END IF; 
          end;
          ln_band := 1;
          exit;
        end if;
      
      end loop;
    
    end loop;
  
   OPEN C_PARAMETRO(10182,'FECHA_INICIO_COBRO_IVA_ROAM');
    FETCH C_PARAMETRO
      INTO Lv_fecha_cobro_iva_roam;
    CLOSE C_PARAMETRO;
  
    IF ln_band > 0 THEN
    
      FOR Ln_Contador IN Lt_ValoresRubros.FIRST .. Lt_ValoresRubros.LAST LOOP
      
        BEGIN
        
          OPEN c_obtiene_doc1(ln_contador);
          FETCH c_obtiene_doc1
            INTO lc_obtiene_doc1;
          CLOSE c_obtiene_doc1;
        
         SELECT INSTR(upper(lc_obtiene_doc1.des),'ROAM') into ln_cont_roam FROM DUAL;
          IF ln_cont_roam > 0 THEN
            IF TO_DATE(pv_fecha,'DD/MM/YYYY') < TO_DATE(lv_fecha_cobro_iva_roam,'DD/MM/YYYY') then
              lv_tipo_wf := '004 - EXENTO';
            ELSE
              lv_tipo_wf := lc_obtiene_doc1.tipo;
            END IF;
          ELSE
            lv_tipo_wf := lc_obtiene_doc1.tipo;
          END IF;
         --ini mdo 
         lv_etiqueta:=null ;
         LV_TRAMA_NEW:=null ;
         if lt_valoresrubros(ln_contador).sncode is not null then 
           
         --mdo 
         LV_CADENA2:=lt_valoresrubros(ln_contador).id_sncode;
          
          PRC_ELIMINA_DUPLICADOS(PV_TRAMA           => LV_CADENA2,
                                          PV_TRAMA_NUEVA     => LV_TRAMA_NEW);
                                          
       
        
        --ln_v1 := INSTR(lv_trama_new, '||', 1, 1);
        --lv_dato := SUBSTR(lv_trama_new, 1, ln_v1 - 2);
        
          
         LV_CADENA:=lv_trama_new;--kri
         LN_POSI:=INSTR(LV_CADENA,';',1); 
         LV_CONTENIDO1:=NULL; 
         
         IF LN_POSI = 0 THEN
            OPEN c_obtiene_mpu(LV_CADENA);
          FETCH c_obtiene_mpu
            INTO lv_etiqueta;
          CLOSE c_obtiene_mpu;
            
            LV_CONTENIDO1:=lv_etiqueta; 
           
         END IF;  
         
         WHILE LN_POSI > 0 LOOP 
           
         LV_CONTENIDO:= substr(LV_CADENA,1,LN_POSI-1); 
            OPEN c_obtiene_mpu(LV_CONTENIDO);
          FETCH c_obtiene_mpu
            INTO lv_etiqueta;
          CLOSE c_obtiene_mpu;
          
          if lv_etiqueta is not null 
           then 
              IF LV_CONTENIDO1 IS NOT NULL THEN 
                LV_CONTENIDO1:=LV_CONTENIDO1||';'|| lv_etiqueta; 
              ELSE 
                LV_CONTENIDO1:=lv_etiqueta; 
              END IF; 
           end if ;            
          LV_CADENA:=substr(LV_CADENA,LN_POSI+1); 
          LN_POSI:=INSTR(LV_CADENA,';',1);
            
         END LOOP ; 
          IF LV_CONTENIDO1 IS NOT NULL THEN 
            LV_CONTENIDO1:=LV_CONTENIDO1||';'; 
          END IF; 
         else
           
         
            LV_CONTENIDO1:=null; 
         end if;  
          
         --fin mdo 
          lv_query := 'INSERT INTO wf_complain_temporal_cf
                      (nombre2,
                       valor,
                       customer_id,
                       custcode,
                       ohrefnum,
                       tipo,
                       nombre,
                       servicio,
                       descuento,
                       cobrado,
                       ctaclblep,
                       cta_devol,
                       tipo_iva, 
                       id_producto,
                       id_producto2,
                       id_sncode)
                    VALUES
                      (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16)';
             
          EXECUTE IMMEDIATE lv_query USING IN UPPER(lc_obtiene_doc1.des),
                                              lt_valoresrubros(ln_contador).valor,
                                              lv_customer_id,
                                              pv_cuenta,
                                              lt_valoresrubros(ln_contador).factura,
                                              lv_tipo_wf,
                                              lc_obtiene_doc1.des,
                                              -55,
                                              NVL(lt_valoresrubros(ln_contador).descuento, 0),
                                              lt_valoresrubros(ln_contador).valor - NVL(lt_valoresrubros(ln_contador).descuento, 0),
                                              lc_obtiene_doc1.ctaps,-- 11163 kri
                                              lc_obtiene_doc1.cta_devol,-- 11163 kri
                                              'I12',
                                              lc_obtiene_doc1.codigo_sri,
                                              LV_CONTENIDO1,--mdo 
                                              NVL(LV_TRAMA_NEW,-55);
             
        EXCEPTION
          WHEN OTHERS THEN
             lv_error := SUBSTR('Error: '|| SQLERRM, 1, 100);
        END;
        
      END LOOP;
    
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      lv_error := SUBSTR(SQLERRM, 1, 100);
      pv_error := lv_error;
    
  END PRC_OBTIENE_RUBROS_LLAMADAS;
  

  
  --PROCEDIMIENTO QUE INSERTA EN LA NUEVA TABLA DE CONTROL DE FACTURAS DE BSCS PROCESADAS CON SU NUEVA FACTURA SRI
  PROCEDURE PRC_INSERTA_CABECERA (pv_factura_bscs     IN  VARCHAR2,
                                  pv_factura_sri_caja IN  VARCHAR2,
                                  pd_fecha_factura    IN  DATE,
                                  pn_id_hist          IN NUMBER,--FPA
                                  pv_mensaje          OUT VARCHAR2,
																  pv_cod_error        OUT VARCHAR2) IS
                                  
    lv_error    VARCHAR2(4000);
    le_error    EXCEPTION;
    LV_UPDATE    VARCHAR2(500);--FPA
    LV_CAMPO     VARCHAR2(35);
    LN_SECUENCIA  NUMBER;
    LV_EXISTE     VARCHAR2(1);
    lv_fact_bscs  varchar2(30);
    CURSOR C_HISTORICO IS
      SELECT NVL((MAX(CF.SECUENCIA)+1),0)
      FROM sysadm.bit_factura_bscs_cf CF
      WHERE CF.ID_HIST = pn_id_hist;
      
    CURSOR C_VERIFICA_FACTURA IS
      SELECT 'X'
      FROM sysadm.bit_factura_bscs_cf CF
      WHERE CF.FACTURA_BSCS = pv_factura_bscs;
    
  BEGIN
      
    pv_cod_error := 0;
  
    /*INI CLS FPA 9953*/
    OPEN C_VERIFICA_FACTURA;
    FETCH C_VERIFICA_FACTURA INTO LV_EXISTE;
    CLOSE C_VERIFICA_FACTURA;
    
    IF LV_EXISTE = 'X' THEN
      
      UPDATE sysadm.bit_factura_bscs_cf
      SET factura_sri_caja = pv_factura_sri_caja
      WHERE factura_bscs = pv_factura_bscs;
      
    ELSE
    /*FIN CLS FPA 9953*/  
          OPEN C_HISTORICO;
          FETCH C_HISTORICO INTO LN_SECUENCIA;
          CLOSE C_HISTORICO ;
          BEGIN
            INSERT INTO sysadm.bit_factura_bscs_cf (factura_bscs, factura_sri_caja, fecha_factura, id_hist, secuencia)
            VALUES (pv_factura_bscs, pv_factura_sri_caja, pd_fecha_factura,pn_id_hist, LN_SECUENCIA);   
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := 'Error insertando el registro: ' || SQLERRM;
              RAISE le_error;
          END;
          
          /*INI 9953 CLS FPA*/ 
          IF LN_SECUENCIA = 0 THEN
            BEGIN
              
            LV_CAMPO := GETPARAMETROS('CF_FACT_PROC_002');
            lv_fact_bscs:=substr(pv_factura_bscs,1,3) || '-' ||substr(pv_factura_bscs,4,3)||   '-' ||substr(pv_factura_bscs,7);   
            LV_UPDATE := ' UPDATE sysadm.orderhdr_all'||
                         ' SET '|| LV_CAMPO || ' = ''X'''|| 
                         ' WHERE ohrefnum = '|| '''' || lv_fact_bscs || '''';
            
            Execute Immediate LV_UPDATE;
             /*
              UPDATE sysadm.orderhdr_all
                 SET ohsaddr3 = 'X'
               WHERE ohrefnum = pv_factura_bscs;*/
            
         /*FIN 9953 CLS FPA*/  
             EXCEPTION
                  WHEN OTHERS THEN
                  lv_error := 'Error actualizando estado: ' || SQLERRM;
                  RAISE le_error;        
             END;     
           END IF;
    END IF;
    
    --COMMIT;--FPA
    
    pv_mensaje := 'PROCESO OK';
    
  EXCEPTION
    WHEN le_error THEN
      pv_cod_error := -1;
      pv_mensaje := 'Error en INK_API_FACTURABSCS_CF.PRC_INSERTA_CABECERA: ' || lv_error;
    WHEN OTHERS THEN
      pv_cod_error := -2;
      pv_mensaje := 'Error en INK_API_FACTURABSCS_CF.PRC_INSERTA_CABECERA: ' || SQLERRM;      
  
  END PRC_INSERTA_CABECERA;
  
  /*INI CLS FPA 9953*/
  --PROCEDIMIENTO QUE ELIMINA LA INFORMACION DE LA TABLA TEMPORAL QUE CONTIENE LOS DATOS DE LOS RUBRO DE UNA FACTURA DE BSCS
  PROCEDURE PRC_ELIMINA_WF_COMPLAIN (pv_factura_sri     IN  VARCHAR2,
                                     pv_cta             IN  VARCHAR2,
                                     pv_error           OUT VARCHAR2) IS
                                     
    lv_query     VARCHAR2(32000);
    
  BEGIN
      
    lv_query := 'DELETE wf_complain_temporal_cf c' ||
                ' WHERE c.ohrefnum = :1 ' || 
                '   AND c.custcode = :2 ';
                    
    EXECUTE IMMEDIATE lv_query USING IN pv_factura_sri,
                                        pv_cta ;
    
    COMMIT;
    
  EXCEPTION    
    WHEN OTHERS THEN
      pv_error := 'Error en INK_API_FACTURABSCS_CF.PRC_ELIMINA_WF_COMPLAIN: ' || SQLERRM; 
  END PRC_ELIMINA_WF_COMPLAIN;
  /*FIN CLS FPA 9953*/
  
  --PROCEDIMIENTO QUE INSERTA EN TABLAS DE WF_COMPLAIN_TEMPORAL_CF
  --INI CQU PROY 10755 28/03/2015
  PROCEDURE PRC_TRAMA_FACTURA_NEW(PV_PARAMETROS    IN  VARCHAR2,
                                  PV_MENSAJE_ERROR OUT VARCHAR2,
                                  PV_COD_ERROR     OUT VARCHAR2) IS

    
    PV_FACTURA_SRI VARCHAR2(100):=null; 
    PV_FECHA       VARCHAR2(20):=null; 
    PV_CTA         VARCHAR2(20):=null; --INI CQU 9953 24/08/2015
    Lv_error       VARCHAR2(3000);
    LE_ERROR       EXCEPTION;   
    Lv_cuenta      VARCHAR2(200);
  BEGIN
 
    -- Devuelve la el codigo de la factura bscs
    PV_FACTURA_SRI := GETDATOTRAMA(PV_TRAMA => PV_PARAMETROS,
                                   PV_DATO  => 'PARAMETRO_BIND_1',
                                   PV_SEPARATOR => ';');
    
    
    -- obtiene la fecha de la factura
    PV_FECHA := GETDATOTRAMA(PV_TRAMA => PV_PARAMETROS,
                                   PV_DATO  => 'PARAMETRO_BIND_2',
                                   PV_SEPARATOR => ';');
                                   
   -- obtiene la fecha de la factura
    PV_CTA := GETDATOTRAMA(PV_TRAMA => PV_PARAMETROS,
                                   PV_DATO  => 'PARAMETRO_BIND_3',
                                   PV_SEPARATOR => ';');
     
    
    /*INI CLS FPA*/
     ink_api_facturabscs_cf.prc_elimina_wf_complain(pv_factura_sri => PV_FACTURA_SRI,
                                                    pv_cta => PV_CTA,
                                                    pv_error => Lv_error);
                                                    
     IF Lv_error IS NOT NULL THEN
       RAISE LE_ERROR;
     END IF;                                               
    /*FIN CLS FPA*/
    
    Lv_cuenta:=PV_CTA;
      
      ink_api_facturabscs_cf.prc_obtiene_rubros(pv_fecha  => pv_fecha,
                                                pv_cuenta => Lv_cuenta,
                                                pv_factura_sri => pv_factura_sri,
                                                pv_error => Lv_error);
   
    IF Lv_error IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
    ---
       
    ink_api_facturabscs_cf.prc_obtiene_rubros_llamadas(pv_fecha  => pv_fecha,
                                                         pv_cuenta => Lv_cuenta,
                                                         pv_factura_sri => pv_factura_sri,
                                                         pv_error => Lv_error);
    
    IF Lv_error IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
   
    
    pv_cod_error := 0; --ESTA TODO OK
    
  EXCEPTION
    WHEN LE_ERROR THEN
      
      pv_cod_error := -1;
      pv_mensaje_error := 'ERROR'||' ' ||Lv_error ; --ClS 10755 26/04/2016
      ROLLBACK;
      ink_api_facturabscs_cf.prc_elimina_wf_complain(pv_factura_sri => PV_FACTURA_SRI,
                                                     pv_cta         => PV_CTA,
                                                     pv_error       => Lv_error);
      
      
    WHEN OTHERS THEN
      pv_cod_error := -2;
      pv_mensaje_error := 'ERROR'||' ' ||TO_CHAR(SQLERRM); --ClS 10755 26/04/2016
      ROLLBACK;
      ink_api_facturabscs_cf.prc_elimina_wf_complain(pv_factura_sri => PV_FACTURA_SRI,
                                                     pv_cta         => PV_CTA,
                                                     pv_error       => Lv_error);
      
  END PRC_TRAMA_FACTURA_NEW;
  --FIN CQU 10755
  --INI KRI 10987 ELIMINA VALORES DUPLICADOS EN LA TRAMA
   PROCEDURE PRC_ELIMINA_DUPLICADOS(PV_TRAMA       IN  VARCHAR2,
                                    PV_TRAMA_NUEVA     OUT  VARCHAR2) IS
                               
                               
  lv_trama       VARCHAR2(4000):=null;
  lv_dato        VARCHAR2(100);
  ln_v1          PLS_INTEGER := 0;
  ln_ini         PLS_INTEGER := 1;
  ln_fin         PLS_INTEGER := 0;
  ln_valor       PLS_INTEGER := 0;
  ln_count       PLS_INTEGER := 0;
  lv_trama_new   VARCHAR2(4000);
 
 BEGIN
  
  ln_valor := INSTR(Pv_trama, ';', 1, 1);
  lv_trama:=Pv_trama;
  IF ln_valor = 0 then 
    lv_trama:=Pv_trama||';';
  end if;
      
  ln_fin := INSTR(lv_trama, ';', 1, 1);
  
  WHILE ln_fin > 0 LOOP
    ln_count := ln_count + 1;
    lv_dato := SUBSTR(lv_trama, ln_ini, ln_fin - ln_ini);
    ln_ini := ln_fin + 1;
    ln_fin := INSTR(lv_trama, ';', ln_ini, 1);
    IF lv_dato IS NOT NULL THEN
      IF ln_count = 1 THEN
        lv_trama_new := ';' || lv_dato || ';';
      ELSE
        IF INSTR(lv_trama_new,';' || lv_dato || ';') = 0 THEN
            lv_trama_new := lv_trama_new || lv_dato || ';';
       
        END IF;
      END IF;
    end if;
  END LOOP;
  PV_TRAMA_NUEVA:=lv_trama_new;
  --DBMS_OUTPUT.put_line('TRAMA NUEVA:' || lv_trama_new);
 

 END PRC_ELIMINA_DUPLICADOS; 
 --FIN 10987 ELIMINA VALORES DUPLICADOS EN LA TRAMA                          
END INK_API_FACTURABSCS_CF;
/
