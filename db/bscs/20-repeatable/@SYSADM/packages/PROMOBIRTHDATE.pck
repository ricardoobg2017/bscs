CREATE OR REPLACE PACKAGE        PROMOBIRTHDATE


IS

  vp_UseMaxContractActiveTime 		 		   NUMBER;
  vp_VirtualStartMonth 						   DATE;
  vp_last_billrun_start 					   DATE;
  vp_this_billrun_start 					   DATE;
  vp_Rateplans				 	  			   VARCHAR(200);
  vp_TraceOn								   VARCHAR(1);


PROCEDURE STARTUP (
	 startup  		 	   		  			IN VARCHAR,
	 v_UseMaxContractActiveTime				IN NUMBER,
	 v_VirtualStartMonth					IN DATE,
	 v_Rateplans							IN VARCHAR,
	 v_TraceOn								IN VARCHAR
	 -- 1 = evaluation
	 -- 0 = no eval
);

PROCEDURE FindCustomers;

PROCEDURE CopyBirthdate;

PROCEDURE FindContracts (
       v_customer_ID  	 			   		   		 IN customer_all.customer_id%TYPE	-- ID of the customer
);

PROCEDURE EvaluateServicesTime (
   --Loop through all state. Add all periods between activation and suspension
		--			or activation and deactivation until today.
   -- Determine the activation time of a service, either the usage service T11, GSM or TDMA.
   	v_co_ID  		 		   		     		    IN  contract_all.co_id%TYPE,      	-- ID of the contract
	v_Duration		 	 							OUT NUMBER,
	v_ResultBack								  	IN OUT VARCHAR2
);



PROCEDURE IsServiceNew  (
    v_co_ID  		   	 	   		   	 		    IN  contract_all.co_id%TYPE,      	 	 	-- ID of the contract
	v_Good		 	 	 						    OUT VARCHAR2,									-- Is 1 if this is a new service
	v_ResultBack								    IN OUT VARCHAR2--(200)
);

PROCEDURE SumLA;

END PROMOBIRTHDATE;
/
CREATE OR REPLACE PACKAGE BODY PROMOBIRTHDATE

IS
     v_rateplans		  	  				        VARCHAR2(1000);
     sv_customer_contract 	  		  				contract_all.co_id % TYPE;
     sv_customer 		 							contract_all.customer_id % TYPE;

CURSOR c_customer_contract IS
	 SELECT DISTINCT customer_id, co_id
	 FROM contract_all
	 ORDER BY customer_id, co_id;







PROCEDURE STARTUP (
	 startup  		 	   		  					IN VARCHAR,
	 -- copy 					moves promobirtdate to customer_all.birthdate
	 -- restore 				---
	 -- generate 				generates virtual birthdate
	 -- la 						sum for LA roots
	 v_UseMaxContractActiveTime				        IN NUMBER,
	 -- 1 = evaluation
	 -- 0 = no eval
	 v_VirtualStartMonth							IN DATE,
	 v_Rateplans									IN VARCHAR,
	 v_TraceOn										IN VARCHAR

)
IS
   	 x												NUMBER;

BEGIN

	 vp_UseMaxContractActiveTime := v_UseMaxContractActiveTime;
	 vp_Rateplans := v_Rateplans;
	 vp_TraceOn := v_TraceOn;

	 if vp_TraceOn = 1 then
	 	DBMS_output.put_line('PROMOBIRTHDATE');
	 	DBMS_output.put_line('Parameters:');
		DBMS_output.put_line('   Run mode			   	   			 :' || startup );
	 	DBMS_output.put_line('   Evaluate MaxContractActive 		 :' || v_UseMaxContractActiveTime );
	 	DBMS_output.put_line('   Date of Bill generation			 :' || v_VirtualStartMonth );
	 	DBMS_output.put_line('   List of rateplans					 :' || v_Rateplans );
	 	DBMS_output.put_line(' ');
	 end if ;

	 vp_VirtualStartMonth  := v_VirtualStartMonth;
	 if vp_VirtualStartMonth = NULL then vp_VirtualStartMonth := sysdate; end if;

	 SELECT to_DATE( '23.' || substr( to_char( add_months( vp_VirtualStartMonth, -1), 'dd.MM.YYYY') ,4) ,'dd.MM.YYYY')
	 INTO vp_last_billrun_start FROM dual;

	 SELECT to_DATE( '23.' || substr( to_char( vp_VirtualStartMonth, 'dd.MM.YYYY') ,4) ,'dd.MM.YYYY')
	 INTO vp_this_billrun_start FROM dual;



	 IF (startup = 'copy') THEN
 	 	   CopyBirthdate();
	 ELSIF (startup = 'restore') THEN
 	 	   x := 1;-- not supported, new birthdate should be restored from AXIS
	 ELSIF (startup = 'generate') THEN
	 	   FindCustomers();
	 ELSIF (startup = 'la') THEN
	       DBMS_output.put_line('los');
	 	   SumLA();
	 END IF;

END STARTUP;






PROCEDURE CopyBirthdate
IS
   v_errors 											 			 	  VARCHAR2(1000);

   CURSOR c_customer IS
      SELECT CUB.CUSTOMER_ID, CUB.PROMO_BIRTHDATE, CU.SETTLES_P_MONTH
      FROM CUSTOMER_ALL_BIRTHDATE CUB, CUSTOMER_ALL CU
	  WHERE CU.CUSTOMER_ID = CUB.CUSTOMER_ID
      ORDER BY CUB.CUSTOMER_ID;
   r_customer 			  			   		  					   	 	  c_customer%ROWTYPE;

BEGIN
	 UPDATE CUSTOMER_ALL SET BIRTHDATE = NULL;
	 FOR r_customer IN c_customer LOOP

		 UPDATE CUSTOMER_ALL SET BIRTHDATE = r_customer.PROMO_BIRTHDATE
		 WHERE CUSTOMER_ID = r_customer.CUSTOMER_ID;

		 UPDATE CUSTOMER_ALL_BIRTHDATE SET SETTLES_P_MONTH = r_customer.SETTLES_P_MONTH
		 WHERE CUSTOMER_ID = r_customer.CUSTOMER_ID;
	 	 COMMIT;
		 if vp_TraceOn = 1 then
		 	DBMS_output.put_line('  Copying customers birthdate		:' || r_customer.CUSTOMER_ID );
		 end if;
  	 END LOOP;

END CopyBirthdate;





PROCEDURE FindCustomers
   -- Find all customers with rateplans which have to be regarded in the promotion evaluation.
   	  -- Selection Criteria: Rateplan
	  -- filter: all not applicable customers
   --  customer_all, contract_all, rateplan,

IS
   v_errors 												  	  	 	   VARCHAR2(1000);

   CURSOR c_customer IS
      SELECT DISTINCT CU.CUSTOMER_ID "CUSTOMER"
      FROM CUSTOMER_ALL CU, CONTRACT_ALL CO
      WHERE CU.CUSTOMER_ID = CO.CUSTOMER_ID
      --AND CO.TMCODE IN (vp_Rateplans)-- and cu.customer_id = 95894
	  AND CO.TMCODE IN ( 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 14, 15, 13, 20, 17, 18, 19, 21, 22, 24, 28, 30, 35, 37, 38, 41, 42, 43, 44, 45, 46, 49, 51, 52, 54, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 73, 78, 80, 81, 82, 83, 84, 85, 87, 93, 90, 91, 92, 94, 95, 96, 97, 98, 112, 115, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 160, 156, 157, 158, 159, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 255, 256, 257, 258, 259, 260, 263, 261, 262, 264, 265, 266, 267, 268, 269, 270, 271, 272, 275, 276, 277, 278, 279, 281, 282, 284)-- and cu.cus
      ORDER BY CU.CUSTOMER_ID;
   r_customer 			  			   		  			  	  	 	 	   c_customer%ROWTYPE;

BEGIN

	 FOR r_customer IN c_customer LOOP
	 	 if vp_TraceOn = 1 then
		 	DBMS_output.put_line('Processing customer					 :' || r_customer.customer );
		 end if;
	 	 FindContracts(r_customer.customer);
  	 END LOOP;

END FindCustomers;







PROCEDURE FindContracts (
       v_customer_ID  	 			   		 							 	IN CUSTOMER_ALL.CUSTOMER_ID%TYPE
)
IS
   -- Find all contracts of a customers with a rateplans which have to be regarded in the promotion evaluation.
      -- filter: not applicable contracts because of rateplan
	  -- 		 	 					  or because contract not active
      v_RESULT 								 		 		  	  		  	VARCHAR2(1000);
   	  v_MAX_SERVICE_ACTIVE_TIME             								NUMBER;
	  v_MAX_SERVICE_ACTIVE_YEARS            								NUMBER;
	  v_NEW_CONTRACTS														NUMBER;
	  v_SUCCESS																NUMBER;
	  v_custcode															customer_all.custcode%TYPE;
	  v_birthdate															customer_all.birthdate%TYPE;
	  v_promo_birthdate														customer_all.birthdate%TYPE;
	  v_cslevel																customer_all.cslevel%TYPE;
	  v_activejn															NUMBER;
	  v_newjn																NUMBER;
	  v_Duration															NUMBER;

      CURSOR c_contracts IS
         SELECT DISTINCT CO_ID
         FROM CONTRACT_ALL
         WHERE CUSTOMER_ID = v_customer_ID
         --AND TMCODE IN (vp_Rateplans)
		 AND TMCODE IN ( 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 14, 15, 13, 20, 17, 18, 19, 21, 22, 24, 28, 30, 35, 37, 38, 41, 42, 43, 44, 45, 46, 49, 51, 52, 54, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 73, 78, 80, 81, 82, 83, 84, 85, 87, 93, 90, 91, 92, 94, 95, 96, 97, 98, 112, 115, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 160, 156, 157, 158, 159, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 255, 256, 257, 258, 259, 260, 263, 261, 262, 264, 265, 266, 267, 268, 269, 270, 271, 272, 275, 276, 277, 278, 279, 281, 282, 284)-- and cu.cus
         ORDER BY CO_ID;
      r_contract 			  			   		  			  	  	 	 	c_contracts%ROWTYPE;

BEGIN
	 v_MAX_SERVICE_ACTIVE_TIME := 0;
	 v_NEW_CONTRACTS := 0;
     v_RESULT := 'in progress ';

     SELECT custcode INTO v_custcode from customer_all where customer_id = v_customer_ID;
     SELECT birthdate INTO v_birthdate from customer_all where customer_id = v_customer_ID;
     SELECT cslevel INTO v_cslevel from customer_all where customer_id = v_customer_ID;


     if (v_cslevel <> '40') then v_RESULT := 'skip LA ' || v_cslevel || ' structure, please call script in mode "la", when still remaining: customer might not have a sublevel, or sublevel misses contract or contracct not active.'; end if;

     INSERT INTO CUSTOMER_ALL_BIRTHDATE ( CUSTOMER_ID, CUSTCODE, BIRTHDATE, PROMO_BIRTHDATE, MAX_SERVICE_ACTIVE_TIME, NEW_CONTRACTS, SUCCESS, ERR_DESCRIPTION )
     VALUES 	   						  ( v_customer_ID, v_custcode, NULL, NULL, NULL, NULL, 0, v_RESULT);
     COMMIT;

	 v_RESULT := '';

   IF (v_cslevel = '40') THEN
   	  --< dont do it for LA here


	 FOR r_contract IN c_contracts LOOP
	 	 if vp_TraceOn = 1 then
	 	   DBMS_output.put_line('   Contract					 :' || r_contract.co_id );
		 end if;

	    --> find deactivated contract by checking the last entry in this billcycle
        SELECT COUNT(*) INTO v_activejn FROM CONTRACT_HISTORY
        	WHERE CO_ID = r_contract.co_id
        	AND CH_SEQNO IN
        		  (SELECT DISTINCT max(CH_SEQNO)
              	   		  FROM CONTRACT_HISTORY
        	  			  WHERE CO_ID = r_contract.co_id AND CH_VALIDFROM < vp_this_billrun_start
        		  )
        	AND CH_STATUS in ('a');

	    IF v_activejn > 0 THEN
		   v_RESULT := v_RESULT || '|Co:' || r_contract.co_id || '=a';


	 	   if vp_TraceOn = 1 then
	 	      DBMS_output.put_line('           is active:');
		   end if;

		   -- process promotion: NEWCONTRACT:
		   IsServiceNew(r_contract.co_id, v_newjn, v_RESULT);
		   IF v_newjn = 1 THEN
		   	  v_NEW_CONTRACTS := v_NEW_CONTRACTS + 1;
		   END IF;



		   -- process promotion: MAXCONRACTACTIVETIME:
		   IF vp_UseMaxContractActiveTime = 1 THEN
		   	  EvaluateServicesTime(r_contract.co_id, v_Duration, v_RESULT);
			  IF v_MAX_SERVICE_ACTIVE_TIME < v_Duration THEN
			  	 v_MAX_SERVICE_ACTIVE_TIME := v_Duration;
			  END IF;
		   END IF;
		else
			v_RESULT := v_RESULT || '|Co:' || r_contract.co_id || '=d';
		 	if vp_TraceOn = 1 then
	 	        DBMS_output.put_line('           is deactive:');
		    end if;
		END IF;

        v_RESULT := substr(v_RESULT,1,290);
  	 END LOOP;


     v_MAX_SERVICE_ACTIVE_YEARS := 0;

     IF vp_UseMaxContractActiveTime = 1 THEN
   	    IF (v_MAX_SERVICE_ACTIVE_TIME / 365) <= 1 THEN
   	  	   v_MAX_SERVICE_ACTIVE_YEARS := 1;
   	    ELSIF (v_MAX_SERVICE_ACTIVE_TIME / 365) <= 2 THEN
   		   v_MAX_SERVICE_ACTIVE_YEARS := 2;
        ELSE
   		   v_MAX_SERVICE_ACTIVE_YEARS := 3;
        END IF;
     END IF;


     v_promo_birthdate := vp_this_billrun_start;
     v_promo_birthdate := add_months( v_promo_birthdate, (100 * v_MAX_SERVICE_ACTIVE_YEARS * -12));
     v_promo_birthdate := add_months( v_promo_birthdate, (v_NEW_CONTRACTS * -12));



     UPDATE CUSTOMER_ALL_BIRTHDATE
     SET BIRTHDATE = v_birthdate,
   	     PROMO_BIRTHDATE = v_promo_birthdate,
	     MAX_SERVICE_ACTIVE_TIME = v_MAX_SERVICE_ACTIVE_YEARS,
	     NEW_CONTRACTS = v_NEW_CONTRACTS,
	     SUCCESS = 1,
         ERR_DESCRIPTION = v_RESULT
     WHERE CUSTOMER_ID = v_customer_ID;
     COMMIT;
   END IF;
END FindContracts;








PROCEDURE EvaluateServicesTime (
   --Loop through all state. Add all periods between activation and suspension
		--			or activation and deactivation until today.
		-- Only give this value back in case the last state before billrun is active.
   -- Determine the activation time of a service, either the usage service T11, GSM or TDMA.
   	v_co_ID  		 		   		     		    	 	 	    IN  contract_all.co_id%TYPE,      	-- ID of the contract
	v_Duration		 	 										    OUT NUMBER,
	v_ResultBack								  					IN OUT VARCHAR2
)

IS
    CURSOR c_status IS
   		  select ph.valid_from_date, ph.status
		  from contract_all co, pr_serv_status_hist ph
		  where ph.co_id = co.co_id and ph.sncode in (91,55) and co.co_id = v_co_ID
		  order by co.customer_id, ph.sncode, ph.valid_from_date;
    rc_status number;
	r_status c_status%ROWTYPE;

	v_open							   			  					NUMBER; --a flag
    v_start 														pr_serv_status_hist.valid_from_date%TYPE;  --last activation
    v_stop  														pr_serv_status_hist.valid_from_date%TYPE;  --last deactivation /suspension
    v_time 	 		 												NUMBER; -- accummulated delta
	v_Result								  						VARCHAR2(1000);
	v_Timestamp														pr_serv_status_hist.valid_from_date%TYPE;



BEGIN


	v_open := 0;
    v_start := vp_this_billrun_start;
    v_stop := vp_this_billrun_start;
    v_time := 0;

	open c_status;
	v_Timestamp := sysdate;
	if vp_TraceOn = 1 then
	   DBMS_output.put_line('____Before Loop');
	   DBMS_output.put_line( 'vp_this_billrun_start='|| to_char(vp_this_billrun_start,'dd.mm.RRRR'));
	end if;

LOOP
	fetch c_status into r_status;

	rc_status := c_status%ROWCOUNT;
	v_Timestamp := to_date( r_status.valid_from_date,'dd.mm.RRRR');

	if vp_TraceOn = 1 then DBMS_output.put_line(' '); end if;

	if (c_status%NOTFOUND) then
	    --< this is the end:
		v_stop := vp_this_billrun_start;
		v_open := 0;  -- close any open period
		v_time := v_time + (v_stop - v_start);
		v_Result := v_Result || '-l' || v_time || '~';
		if vp_TraceOn = 1 then
		    DBMS_output.put_line('lineend');
		    DBMS_output.put_line( 'v_stop='|| to_date(v_stop,'dd.mm.RRRR'));
		    DBMS_output.put_line( 'v_time='|| to_char(v_time,'dd.mm.RRRR'));
		end if;
		exit when 1 = 1;
	end if;

	IF to_date(v_Timestamp,'dd.mm.RRRR') >= to_date(vp_this_billrun_start,'dd.mm.RRRR') THEN
	    --< this entry exceeds the end of the billcycle !!!
 		IF v_open = 1 THEN
		   v_stop := vp_this_billrun_start;
		   v_open := 0;  -- close any open period
		   v_time := v_time + (v_stop - v_start);
		END IF;
		v_Result := v_Result || '-b' || v_time || '~';
		if vp_TraceOn = 1 then
		   DBMS_output.put_line('bcend');
		   DBMS_output.put_line( 'v_stop='|| to_date(v_stop,'dd.mm.RRRR'));
		   DBMS_output.put_line( 'v_time='|| to_char(v_time,'dd.mm.RRRR'));
		end if;
	EXIT WHEN 1 = 1;
	END IF;

	if r_status.status ='A' and v_open = 0 then
	    --< open a new period if the change is to active
		v_start := v_Timestamp;
		v_open := 1;
		v_Result := v_Result || '-a' || v_start;
		if vp_TraceOn = 1 then
		   DBMS_output.put_line('activation');
		   DBMS_output.put_line( 'v_start='||to_date(v_start,'dd.mm.RRRR')) ;
		end if;
	end if;

	if r_status.status ='S' and v_open = 1 then
	    --> close the current period
		v_stop := v_Timestamp;
		v_open := 0;
		v_time := v_time + (v_stop - v_start);
		v_Result := v_Result || '-s' || v_time;
		if vp_TraceOn = 1 then
		   DBMS_output.put_line('suspension');
		   DBMS_output.put_line( 'v_stop='|| to_date(v_time,'dd.mm.RRRR'));
		   DBMS_output.put_line( 'v_time='|| to_char(v_time,'dd.mm.RRRR'));
		end if;
	end if;

	if r_status.status ='D' and v_open = 1 then
	    --> close the current period
		v_stop := v_Timestamp;
		v_open := 0;
		v_time := v_time + (v_stop - v_start);
		v_Result := v_Result || '-d' || v_time;
		if vp_TraceOn = 1 then
		   DBMS_output.put_line('deactivation');
		   DBMS_output.put_line( 'v_stop='|| to_date(v_time,'dd.mm.RRRR'));
		   DBMS_output.put_line( 'v_time='|| to_char(v_time,'dd.mm.RRRR'));
		end if;
	end if;

	if r_status.status ='O' then
		if vp_TraceOn = 1 then DBMS_output.put_line('on hold'); end if;
	end if;

end loop;

  	close c_status;
	v_Duration := v_time;
	v_ResultBack := substr( v_ResultBack || v_Result,1,990);
END EvaluateServicesTime;






PROCEDURE IsServiceNew  (
    v_co_ID  		   	 	   		   	 		  					   	   IN  contract_all.co_id%TYPE,      	 	 	-- ID of the contract
	v_Good		 	 	 						  						   OUT VARCHAR2,									-- Is 1 if this is a new service
	v_ResultBack								  						   IN OUT VARCHAR2
)

IS
    v_NOTONE									  						   NUMBER;
    v_SERVICE_ACTIVATION						  						   PR_SERV_STATUS_HIST.VALID_FROM_DATE%TYPE;
    v_LAST_STATE								  						   PR_SERV_STATUS_HIST.STATUS%TYPE;
	v_Result    								  						   VARCHAR2(1000);

BEGIN
DECLARE
	e_AnyOne EXCEPTION;

BEGIN

	v_Good := 0;
	/*
	SELECT COUNT(*) INTO V_NOTONE FROM PR_SERV_STATUS_HIST WHERE CO_ID = v_co_ID AND SNCODE IN (55,91) AND STATUS = 'O';
    if v_NOTONE < 1 then v_Result := v_Result || ' No telefony!'; RAISE e_AnyOne; end if;
    if v_NOTONE > 1 then v_Result := v_Result || ' More than one telefony!'; RAISE e_AnyOne; end if;

	--> Is the service active?
    SELECT COUNT(*) INTO V_NOTONE FROM PR_SERV_STATUS_HIST WHERE HISTNO IN(
    	   SELECT MIN(HISTNO) FROM PR_SERV_STATUS_HIST WHERE CO_ID = v_co_ID AND SNCODE IN (55,91) AND STATUS = 'A');

    if v_NOTONE < 1 then v_Result := v_Result || ' Telefony service never activated:' || v_co_ID; RAISE e_AnyOne; end if;
    if v_NOTONE > 1 then v_Result := v_Result || ' Database inconsistency, two activations on same date:' || v_co_ID; RAISE e_AnyOne; end if;
	*/
	-- Search the first activation date:
    SELECT MIN(VALID_FROM_DATE) INTO v_SERVICE_ACTIVATION FROM PR_SERV_STATUS_HIST
	WHERE CO_ID = v_co_ID AND SNCODE IN (55,91) AND STATUS = 'A';

	v_Result := v_Result || ' a;' || TO_CHAR(V_SERVICE_ACTIVATION,'dd.mm.RRRR') || ' ';


    IF to_date(vp_last_billrun_start,'dd.mm.RRRR') <= to_date(V_SERVICE_ACTIVATION,'dd.mm.RRRR') THEN

		v_Result := v_Result || '=new ';
	    --< yes, the service activation had happened last month

		--> test if the service is still active:

	    SELECT STATUS INTO v_LAST_STATE FROM PR_SERV_STATUS_HIST
	    WHERE HISTNO IN(
			  SELECT MAX(HISTNO) FROM PR_SERV_STATUS_HIST
			  WHERE CO_ID = v_co_ID AND SNCODE IN (55,91)
			  AND TO_DATE(VALID_FROM_DATE,'dd.mm.RRRR') < TO_DATE(vp_this_billrun_start,'dd.mm.RRRR'))
	    AND CO_ID = v_co_ID AND SNCODE IN (55,91);

			v_Result := v_Result || '-' || v_LAST_STATE;
     	    IF v_LAST_STATE = 'A' THEN
			   v_Result := v_Result || TO_CHAR(vp_this_billrun_start,'dd.mm.RRRR');
     	       -- ok, this service is still active, this contract gets promotions
     	       v_Good := 1;
			ELSE
			   v_Result := v_Result || 'd_in_month';
     	    END IF;

	else
		v_Result := v_Result || '=old ';
    END IF;

EXCEPTION

WHEN e_AnyOne THEN
	 v_Good := 0;
	 v_ResultBack := substr( v_ResultBack  || v_Result,1,990);
END;

	 v_ResultBack := substr( v_ResultBack  || v_Result,1,990);
END IsServiceNew;





PROCEDURE SumLA
IS
      v_RESULT 								 		 		  	  		  	VARCHAR2(1000);
	  v_promo_birthdate														customer_all.birthdate%TYPE;
	  v_custcode															customer_all.custcode%TYPE;
	  v_birthdate															customer_all.birthdate%TYPE;
	  v_notnull																NUMBER;

      CURSOR c_customer IS
   		  SELECT cu.customer_id_high CUSTOMER_ID_HIGH,
	   			 max(cub.max_service_active_time) MAX_SERVICE_ACTIVE_YEARS,
	   			 sum(cub.new_contracts) NEW_CONTRACTS
		  FROM CUSTOMER_ALL CU, CUSTOMER_ALL CU1, CUSTOMER_ALL_BIRTHDATE CUB
		  WHERE CU.CUSTOMER_ID = CUB.CUSTOMER_ID
		  		and CU.CUSTOMER_ID_high = CU1.CUSTOMER_ID
				GROUP BY CU.CUSTOMER_ID_high;
      r_customer 			  			   		  					   	 	  c_customer%ROWTYPE;

BEGIN

	 DBMS_output.put_line('hier');
	 FOR r_customer IN c_customer LOOP
	 	 --< in this select all subordinated LA subscribers will be summarized to the next level only !!!

        v_promo_birthdate := vp_this_billrun_start;
     	v_promo_birthdate := add_months( v_promo_birthdate, (100 * r_customer.MAX_SERVICE_ACTIVE_YEARS * -12));
     	v_promo_birthdate := add_months( v_promo_birthdate, (r_customer.NEW_CONTRACTS * -12));
		SELECT COUNT(*) INTO v_notnull FROM CUSTOMER_ALL_BIRTHDATE WHERE customer_id = r_customer.CUSTOMER_ID_HIGH;
	    SELECT custcode INTO v_custcode from customer_all where customer_id = r_customer.CUSTOMER_ID_HIGH;
        SELECT birthdate INTO v_birthdate from customer_all where customer_id = r_customer.CUSTOMER_ID_HIGH;

		if vp_TraceOn = 1 then DBMS_output.put_line(r_customer.CUSTOMER_ID_HIGH); END IF;

		if v_notnull = 0 then

		   v_RESULT := 'insert from subordinated LA subscriber';
		   INSERT INTO CUSTOMER_ALL_BIRTHDATE ( CUSTOMER_ID, CUSTCODE, BIRTHDATE, PROMO_BIRTHDATE,
		   		  	   						  	MAX_SERVICE_ACTIVE_TIME,
												NEW_CONTRACTS, SUCCESS, ERR_DESCRIPTION )
           VALUES 	   						  ( r_customer.CUSTOMER_ID_HIGH, v_custcode, v_birthdate, v_promo_birthdate,
		   									  	r_customer.MAX_SERVICE_ACTIVE_YEARS,
												r_customer.NEW_CONTRACTS, 2, v_RESULT);
           COMMIT;
		elsif v_notnull > 0 then

		   v_RESULT := 'update from subordinated LA subscriber';
     	   UPDATE CUSTOMER_ALL_BIRTHDATE
     	   SET PROMO_BIRTHDATE = v_promo_birthdate,
	          MAX_SERVICE_ACTIVE_TIME = r_customer.MAX_SERVICE_ACTIVE_YEARS,
	          NEW_CONTRACTS = r_customer.NEW_CONTRACTS,
	          SUCCESS = 2,
              ERR_DESCRIPTION = v_RESULT
           WHERE CUSTOMER_ID = r_customer.CUSTOMER_ID_HIGH;

		end if;

        COMMIT;
  	 END LOOP;

END SumLA;





END PROMOBIRTHDATE;
/

