CREATE OR REPLACE PACKAGE dth_devengamiento_pre_sap IS

  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Creado POr : SUD Norman Castro
   Fecha      : 31/05/2013
   Descripci�n: Paquete de procesos que devengan los valores pagados por anticipado
                para el producto DTH.
				
  *******************************************************************************/

  -- 10189
  -- SIS Luis Flores
  -- PDS Fernando Ortega
  -- RGT Ney Miranda
  -- Ultimo Cambio: 27/10/2015
  --[10183] RGT Ney Miranda T. 
  --Por el aumento de un nuevo ciclo en productos Dth se crea una trama por ciclos   
  -- Nueva logica de devengamiento para SAP

  
  gv_aplicacion VARCHAR2(50) := 'dth_devengamiento_pre_sap.';

  FUNCTION f_userso RETURN VARCHAR2;

  PROCEDURE p_descripcion_cta(pv_cta VARCHAR2, pv_descripcion OUT VARCHAR2);

  PROCEDURE p_gen_preasiento(pd_fecha DATE, pv_error OUT VARCHAR2);

  PROCEDURE crear_sumarizado_dev_sap(pd_fecha      IN DATE,
                                     pv_msj_trama2 IN OUT VARCHAR2);

  PROCEDURE p_carga_servicios(pv_error OUT VARCHAR2);

  PROCEDURE p_actualiza_ctas(pv_error OUT VARCHAR2);

  PROCEDURE replica_financiero(pd_fecha IN DATE, pv_error OUT VARCHAR2);

  PROCEDURE ajusta_prepago_dth(pd_fecha IN DATE, pv_error OUT VARCHAR2);
  FUNCTION crear_trama(PV_PRODUCTO      IN VARCHAR2, --DTH
                       PV_REGION        IN VARCHAR2,--COSTA/SIERRA
                       pv_ciclo         IN VARCHAR2, --03/04       [10183] RGT Ney Miranda T.   
                       PV_IDENTIFICADOR OUT VARCHAR2,
                       ERROR            IN OUT VARCHAR2) RETURN CLOB;
  --
  FUNCTION obtener_valor_parametro_sap(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2;
  --
  PROCEDURE envia_tramas_devengo_dth_sap(PD_FECHA     IN DATE, --FECHA DE CORTE
                                         PV_RESULTADO OUT VARCHAR2);
  --
  PROCEDURE reenvia_polizas(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2);
  -- 10189
  PROCEDURE Reverso_devengamiento_dth(pd_fecha IN date,
                                       pv_resultado  OUT VARCHAR2);                          
                            
  PROCEDURE anula_polizas(PD_FECHA     IN DATE, --FECHA DE CORTE
                          PV_RESULTADO OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2);                        
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2);
  -- Fin 10189
                          
END dth_devengamiento_pre_sap;
/
CREATE OR REPLACE PACKAGE BODY dth_devengamiento_pre_sap IS
  /******************************************************************************
   Proyecto   : [8693] Venta DTH Post Pago
   Lider Claro: SIS Paola Carvajal
   Lider PDS  : SUD Cristhian Acosta Chambers
   Creado POr : SUD Norman Castro
   Fecha      : 31/05/2013
   Descripci�n: Paquete de procesos que devengan los valores pagados por anticipado
                para el producto DTH.
  *******************************************************************************/
  pn_id_transaccion NUMBER := sc_id_transaccion_sap.nextval;
  FUNCTION f_userso RETURN VARCHAR2 IS
    /******************************************************************************
     Proyecto   : [8693] Venta DTH Post Pago
     Lider Claro: SIS Paola Carvajal
     Lider PDS  : SUD Cristhian Acosta Chambers
     Fecha      : 31/05/2013
     Descripci�n: Funcion que retorna el usuario del S.O. que se encuentra conectado
    *******************************************************************************/
    pv_usuario VARCHAR2(60);
  BEGIN
    SELECT upper(SYS_CONTEXT('USERENV', 'OS_USER'))
      INTO pv_usuario
      FROM dual;
  
    RETURN pv_usuario;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN USER;
  END f_userso;

  PROCEDURE p_descripcion_cta(pv_cta VARCHAR2, pv_descripcion OUT VARCHAR2) IS
  
    /******************************************************************************
     Proyecto   : [8693] Venta DTH Post Pago
     Lider Claro: SIS Paola Carvajal
     Lider PDS  : SUD Cristhian Acosta Chambers
     Fecha      : 31/05/2013
     Descripci�n: retorna la descripcion de una cta especifica
    *******************************************************************************/
  
    CURSOR c_cta IS
      SELECT descr
        FROM sysadm.PS_GL_ACCOUNT_TBL@finsys
       WHERE setid = 'PORTA'
         AND EFF_STATUS = 'A'
         AND account = pv_cta
       ORDER BY EFFDT DESC;
  
    lv_cta   VARCHAR2(200);
    lb_found BOOLEAN;
  
  BEGIN
  
    /* open c_cta;
    fetch c_cta into lv_cta;
       lb_found:=c_cta%found;
    close c_cta;
    
    if not lb_found then
     lv_cta:='Cta. no configurada';
    end if;*/
  
    --      pv_descripcion:=lv_cta;
    pv_descripcion := NULL;
  
  END p_descripcion_cta;

  PROCEDURE p_gen_preasiento(pd_fecha DATE, pv_error OUT VARCHAR2) IS
  
    /******************************************************************************
    Proyecto   : [8693] Venta DTH Post Pago
    Lider Claro: SIS Paola Carvajal
    Lider PDS  : SUD Cristhian Acosta Chambers
    Fecha      : 31/05/2013
    Descripci�n: Procedimiento que genera el preasiento en peoplesoft
    *******************************************************************************/
  
    CURSOR c_reporte(cv_ciclo VARCHAR2, cv_mes VARCHAR2, cv_anio VARCHAR2) IS
      SELECT a.ciudad,
             a.servicio,
             MAX(a.codigo_contable) codigo_contable,
             decode(a.codigo_contable, '4201011001', '000003', ' ') producto_asi,
             SUM(round(a.valor_provisiona, 2)) Valor,
             id_ciclo
        FROM reporte_calc_prorrateo_dth_pre a
       WHERE a.id_ciclo = cv_ciclo
         AND a.valor_provisiona <> 0
         AND a.mes = cv_mes
         AND a.anio = cv_anio
       GROUP BY a.ciudad,
                a.servicio,
                decode(a.codigo_contable, '4201011001', '000003', ' '),
                id_ciclo
       ORDER BY a.ciudad, codigo_contable, a.servicio;
  
    CURSOR c_valida_ciclo(cv_mes VARCHAR2, cv_anio VARCHAR2) IS
      SELECT DISTINCT id_ciclo
        FROM reporte_calc_prorrateo_dth_pre
       WHERE mes = cv_mes
         AND anio = cv_anio;
  
    --trunc(add_months(pd_fecha,-1),'mm')+14  || ' HASTA 23:59 ' || pd_fecha
    i             NUMBER;
    ln_sec        NUMBER;
    ln_sumgye     NUMBER := 0;
    ln_sumuio     NUMBER := 0;
    lv_transid    VARCHAR2(30);
    ln_transline  NUMBER;
    ln_linea      NUMBER := 2;
    lc_ctacontab  VARCHAR2(200);
    lv_referencia VARCHAR2(300);
    lv_ciclo      VARCHAR2(100);
    ln_valor      NUMBER;
    pv_msj_trama2 VARCHAR2(5000);
  -- INICIO [10183] RGT Ney Miranda T.   
    lv_resultado VARCHAR2(2000);
    le_error_respalda EXCEPTION;
   -- FIN [10183] RGT Ney Miranda T. 
    CURSOR c_verifica_cuentas(cv_cuenta VARCHAR2) IS
      SELECT a.usa_tipo_iva, a.usa_ciclo
        FROM gl_PARAMETROS_CTAS_CONTAB a
       WHERE a.cuenta = cv_cuenta
         AND a.setid = 'PORTA';
  
    lc_verif_ctas c_verifica_cuentas%ROWTYPE;
    lb_found_cta  BOOLEAN;
    lv_ciclo_5359 VARCHAR2(5);
    lv_tipo_iva   VARCHAR2(5);
  
    lv_dia_inicio VARCHAR2(2) := NULL;
    lv_mes        VARCHAR2(2) := NULL;
    lv_anio       VARCHAR2(4) := NULL;
    lv_mes_act    VARCHAR2(2) := NULL;
    lv_anio_act   VARCHAR2(4) := NULL;
    ld_fecha_ant  DATE;
    lv_error      VARCHAR2(2000) := NULL;
    PV_APLICACION VARCHAR2(200) := 'DTH_DEVENGAMIENTO_PRE_SAP.P_GEN_PREASIENTO';
    le_error EXCEPTION;
    -- 10189
    CURSOR c_valida_poliza_existente(cv_fecha DATE) IS
      SELECT COUNT(*)
        FROM gsib_tramas_sap d
       WHERE d.referencia LIKE '%DEVENGO DTH%'
         AND d.fecha_corte = cv_fecha
         AND d.estado <> 'ERROR';
    lc_valida_poliza_existente NUMBER := 0;
  BEGIN
  
    --10189
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           '****INICIO GENERA ASIENTOS DEVENGO DTH ' ||
                           'FECHA DE CORTE :' || pd_fecha ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    OPEN c_valida_poliza_existente(pd_fecha);
    FETCH c_valida_poliza_existente
      INTO lc_valida_poliza_existente;
    CLOSE c_valida_poliza_existente;
    IF lc_valida_poliza_existente > 0 THEN
      lv_error := 'Ya existen polizas Generadas para Provision DTH con fecha de corte ' ||
                  pd_fecha;
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      RAISE le_error;
    END IF;
    -- primero voy a generar del mes anterior
    ld_fecha_ant := add_months(pd_fecha, -1);
    lv_mes       := extract(MONTH FROM ld_fecha_ant);
    lv_anio      := extract(YEAR FROM ld_fecha_ant);
    ln_transline := 5;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Inicio de carga masiva de datos hacia la tabla ps_jgen_acct_entry_dth_pre_sap y ps_pr_jgen_acct_en_dth_pre_sap  del mes anterior',
                           pn_id_transaccion);
  
    FOR j IN c_valida_ciclo(lv_mes, lv_anio) LOOP
    
      --genera un asiento por ciclo
      ln_sumgye     := 0;
      ln_sumuio     := 0;
      ln_linea      := 2;
      lv_dia_inicio := NULL;
    
      -- saco el dia incicial del ciclo
      SELECT f.dia_ini_ciclo
        INTO lv_dia_inicio
        FROM fa_ciclos_bscs f
       WHERE f.id_ciclo = j.id_ciclo;
    
      FOR i IN c_reporte(j.id_ciclo, lv_mes, lv_anio) LOOP
        ln_linea := ln_linea + 1;
        IF substr(i.codigo_contable, 1, 1) = '4' THEN
          dth_devengamiento_pre_sap.p_descripcion_cta(I.codigo_contable,
                                                      lc_ctacontab);
          lv_referencia := lc_ctacontab || 'DEVENGADO DTH FACTURACION ' ||
                           i.servicio || ' DEL ' || '01/' ||
                           to_char(pd_fecha, 'mm/rrrr') ||
                           ' HASTA 00:00 DEL ' || lv_dia_inicio || '/' ||
                           to_char(pd_fecha, 'mm/rrrr');
          IF i.ciudad = 'GYE' THEN
            ln_sumgye  := ln_sumgye + i.valor;
            lv_transid := 'BG' || to_char(pd_fecha, 'rrrrmmdd');
          ELSE
            ln_sumuio  := ln_sumuio + i.valor;
            lv_transid := 'BU' || to_char(pd_fecha, 'rrrrmmdd');
          END IF;
          ln_valor := i.valor * (-1);
          BEGIN
            INSERT INTO ps_jgen_acct_entry_dth_pre_sap
              (business_unit,
               transaction_id,
               transaction_line,
               ledger_group,
               ledger,
               accounting_dt,
               appl_jrnl_id,
               business_unit_gl,
               fiscal_year,
               accounting_period,
               journal_id,
               journal_date,
               journal_line,
               account,
               operating_unit,
               product,
               currency_cd,
               foreign_currency,
               rt_type,
               rate_mult,
               rate_div,
               monetary_amount,
               foreign_amount,
               doc_type,
               doc_seq_date,
               line_descr,
               gl_distrib_status,
               altacct,
               deptid,
               fund_code,
               class_fld,
               program_code,
               budget_ref,
               affiliate,
               affiliate_intra1,
               affiliate_intra2,
               chartfield1,
               chartfield2,
               chartfield3,
               project_id,
               statistics_code,
               statistic_amount,
               movement_flag,
               doc_seq_nbr,
               jrnl_ln_ref,
               iu_sys_tran_cd,
               iu_tran_cd,
               iu_anchor_flg,
               process_instance)
            VALUES
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               'BSCS',
               'CONEC',
               to_char(pd_fecha, 'rrrr'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_linea,
               i.codigo_contable,
               i.ciudad || '_999',
               i.producto_asi,
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               ln_valor,
               ln_valor,
               'CBSCDDTH',
               pd_fecha,
               lv_referencia,
               'N',
               J.ID_CICLO,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0);
          
            BEGIN
              INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
                (business_unit,
                 transaction_id,
                 transaction_line,
                 ledger_group,
                 ledger,
                 pr_jrnl_ln_ref2,
                 pr_jrnl_tipo_iva,
                 pr_jrnl_ciclo,
                 Pr_id_cia_rel,
                 pr_jrnl_ln_ref3,
                 pr_line_desc2,
                 pr_jrnl_ln_ref_int,
                 pr_fec_prov_interc,
                 pr_ruc_prov_interc,
                 pr_servicio_interc,
                 pr_estado_interc,
                 pr_id_prov_interc,
                 pr_tipo_serv,
                 id_agrupacion,
                 pr_fec_ini_deven,
                 pr_fec_fin_deven)
              VALUES
                ('CONEC',
                 lv_transid,
                 ln_transline,
                 'REAL',
                 'LOCAL',
                 ' ',
                 'PROV',
                 j.id_ciclo,
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 pd_fecha,
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 pd_fecha,
                 pd_fecha);
            
            EXCEPTION
              WHEN OTHERS THEN
                lv_error := 'Error 1, al insertar el preasiento en la tabla ps_pr_jgen_acct_en_dth_pre_sap ' ||
                            SQLERRM;
                gsib_security.dotraceh(pv_aplicacion,
                                       lv_error,
                                       pn_id_transaccion);
              
                ROLLBACK;
                RAISE le_error;
            END;
          
            ln_transline := ln_transline + 1;
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := 'Error 1 al insertar el preasiento' || SQLERRM;
              gsib_security.dotraceh(pv_aplicacion,
                                     lv_error,
                                     pn_id_transaccion);
              ROLLBACK;
              RAISE le_error;
          END;
        END IF;
      END LOOP;
    
      dth_devengamiento_pre_sap.p_descripcion_cta('2205020010',
                                                  lc_ctacontab);
      lv_referencia := lc_ctacontab ||
                       'DEVENGADO PASIVO DIFERIDO DTH DEL 01/' ||
                       to_char(pd_fecha, 'mm/rrrr') || ' HASTA 00:00 DEL ' ||
                       lv_dia_inicio || '/' || to_char(pd_fecha, 'mm/rrrr');
      lv_transid    := 'BG' || to_char(pd_fecha, 'rrrrmmdd');
      INSERT INTO ps_jgen_acct_entry_dth_pre_sap
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         operating_unit,
         product,
         currency_cd,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         doc_type,
         doc_seq_date,
         line_descr,
         gl_distrib_status,
         altacct,
         deptid,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         statistics_code,
         statistic_amount,
         movement_flag,
         doc_seq_nbr,
         jrnl_ln_ref,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         process_instance)
      VALUES
        ('CONEC',
         lv_transid,
         1,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'rrrr'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         1,
         '2205020010',
         ' ',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumgye,
         ln_sumgye,
         'CBSCDDTH',
         pd_fecha,
         lv_referencia,
         'N',
         J.ID_CICLO,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN c_verifica_cuentas('2205020010');
        FETCH c_verifica_cuentas
          INTO lc_verif_ctas;
        lb_found_cta := c_verifica_cuentas%FOUND;
        CLOSE c_verifica_cuentas;
      
        IF lb_found_cta THEN
          lv_ciclo_5359 := ' ';
          lv_tipo_iva   := ' ';
          IF lc_verif_ctas.usa_ciclo = 'Y' THEN
            lv_ciclo_5359 := 'PROV';
          END IF;
          IF lc_verif_ctas.usa_tipo_iva = 'Y' THEN
            lv_tipo_iva := 'PROV';
          END IF;
          INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
            (business_unit,
             transaction_id,
             transaction_line,
             ledger_group,
             ledger,
             pr_jrnl_ln_ref2,
             pr_jrnl_tipo_iva,
             pr_jrnl_ciclo)
          VALUES
            ('CONEC',
             lv_transid,
             1,
             'REAL',
             'LOCAL',
             ' ',
             lv_tipo_iva,
             lv_ciclo_5359);
        
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error 2, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 lv_error,
                                 pn_id_transaccion);
          ROLLBACK;
          RAISE le_error;
      END;
      dth_devengamiento_pre_sap.p_descripcion_cta('2205020010',
                                                  lc_ctacontab);
      lv_referencia := lc_ctacontab ||
                       'DEVENGADO PASIVO DIFERIDO DTH DEL 01/' ||
                       to_char(pd_fecha, 'mm/rrrr') || ' HASTA 00:00 DEL ' ||
                       lv_dia_inicio || '/' || to_char(pd_fecha, 'mm/rrrr');
      lv_transid    := 'BU' || to_char(pd_fecha, 'rrrrmmdd');
      INSERT INTO ps_jgen_acct_entry_dth_pre_sap
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         operating_unit,
         product,
         currency_cd,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         doc_type,
         doc_seq_date,
         line_descr,
         gl_distrib_status,
         altacct,
         deptid,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         statistics_code,
         statistic_amount,
         movement_flag,
         doc_seq_nbr,
         jrnl_ln_ref,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         process_instance)
      VALUES
        ('CONEC',
         lv_transid,
         2,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'rrrr'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         2,
         '2205020010',
         ' ',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumuio,
         ln_sumuio,
         'CBSCDDTH',
         pd_fecha,
         lv_referencia,
         'N',
         J.ID_CICLO,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN c_verifica_cuentas('2205020010');
        FETCH c_verifica_cuentas
          INTO lc_verif_ctas;
        lb_found_cta := c_verifica_cuentas%FOUND;
        CLOSE c_verifica_cuentas;
      
        IF lb_found_cta THEN
          lv_ciclo_5359 := ' ';
          lv_tipo_iva   := ' ';
          IF lc_verif_ctas.usa_ciclo = 'Y' THEN
            lv_ciclo_5359 := 'PROV';
          END IF;
          IF lc_verif_ctas.usa_tipo_iva = 'Y' THEN
            lv_tipo_iva := 'PROV';
          END IF;
          INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
            (business_unit,
             transaction_id,
             transaction_line,
             ledger_group,
             ledger,
             pr_jrnl_ln_ref2,
             pr_jrnl_tipo_iva,
             pr_jrnl_ciclo)
          VALUES
            ('CONEC',
             lv_transid,
             2,
             'REAL',
             'LOCAL',
             ' ',
             lv_tipo_iva,
             lv_ciclo_5359);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error 3, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 lv_error,
                                 pn_id_transaccion);
          ROLLBACK;
          RAISE le_error;
      END;
    
      COMMIT;
    
    END LOOP;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Fin de carga masiva de datos hacia la tabla ps_jgen_acct_entry_dth_pre_sap y ps_pr_jgen_acct_en_dth_pre_sap  del mes anterior',
                           pn_id_transaccion);
  
    lv_mes_act  := extract(MONTH FROM pd_fecha);
    lv_anio_act := extract(YEAR FROM pd_fecha);
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Inicio de carga masiva de datos hacia la tabla ps_jgen_acct_entry_dth_pre_sap y ps_pr_jgen_acct_en_dth_pre_sap  del mes ACTUAL',
                           pn_id_transaccion);
  
    FOR j IN c_valida_ciclo(lv_mes_act, lv_anio_act) LOOP
      --genera un asiento por ciclo
      ln_sumgye     := 0;
      ln_sumuio     := 0;
      ln_linea      := 2;
      lv_dia_inicio := NULL;
    
      -- saco el dia incicial del ciclo
      SELECT f.dia_ini_ciclo
        INTO lv_dia_inicio
        FROM fa_ciclos_bscs f
       WHERE f.id_ciclo = j.id_ciclo;
    
      FOR i IN c_reporte(j.id_ciclo, lv_mes_act, lv_anio_act) LOOP
        ln_linea := ln_linea + 1;
        IF substr(i.codigo_contable, 1, 1) = '4' THEN
          dth_devengamiento_pre_sap.p_descripcion_cta(I.CODIGO_CONTABLE,
                                                      lc_ctacontab);
          lv_referencia := lc_ctacontab || 'DEVENGADO DTH FACTURACION ' ||
                           i.servicio || ' DEL ' || lv_dia_inicio || '/' ||
                           to_char(pd_fecha, 'mm/rrrr') ||
                           ' HASTA 23:59 DEL ' ||
                           to_char(pd_fecha, 'dd/mm/rrrr');
          IF i.ciudad = 'GYE' THEN
            ln_sumgye  := ln_sumgye + i.valor;
            lv_transid := 'BG' || to_char(pd_fecha, 'rrrrmmdd');
          ELSE
            ln_sumuio  := ln_sumuio + i.valor;
            lv_transid := 'BU' || to_char(pd_fecha, 'rrrrmmdd');
          END IF;
          ln_valor := i.valor * (-1);
          BEGIN
            INSERT INTO ps_jgen_acct_entry_dth_pre_sap
              (business_unit,
               transaction_id,
               transaction_line,
               ledger_group,
               ledger,
               accounting_dt,
               appl_jrnl_id,
               business_unit_gl,
               fiscal_year,
               accounting_period,
               journal_id,
               journal_date,
               journal_line,
               account,
               operating_unit,
               product,
               currency_cd,
               foreign_currency,
               rt_type,
               rate_mult,
               rate_div,
               monetary_amount,
               foreign_amount,
               doc_type,
               doc_seq_date,
               line_descr,
               gl_distrib_status,
               altacct,
               deptid,
               fund_code,
               class_fld,
               program_code,
               budget_ref,
               affiliate,
               affiliate_intra1,
               affiliate_intra2,
               chartfield1,
               chartfield2,
               chartfield3,
               project_id,
               statistics_code,
               statistic_amount,
               movement_flag,
               doc_seq_nbr,
               jrnl_ln_ref,
               iu_sys_tran_cd,
               iu_tran_cd,
               iu_anchor_flg,
               process_instance)
            VALUES
              ('CONEC',
               lv_transid,
               ln_transline,
               'REAL',
               'LOCAL',
               pd_fecha,
               'BSCS',
               'CONEC',
               to_char(pd_fecha, 'rrrr'),
               to_char(pd_fecha, 'mm'),
               'NEXT',
               pd_fecha,
               ln_linea,
               i.codigo_contable,
               i.ciudad || '_999',
               i.producto_asi,
               'USD',
               'USD',
               'CRRNT',
               1,
               1,
               ln_valor,
               ln_valor,
               'CBSCDDTH',
               pd_fecha,
               lv_referencia,
               'N',
               J.ID_CICLO,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0,
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               0);
          
            BEGIN
              INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
                (business_unit,
                 transaction_id,
                 transaction_line,
                 ledger_group,
                 ledger,
                 pr_jrnl_ln_ref2,
                 pr_jrnl_tipo_iva,
                 pr_jrnl_ciclo,
                 Pr_id_cia_rel,
                 pr_jrnl_ln_ref3,
                 pr_line_desc2,
                 pr_jrnl_ln_ref_int,
                 pr_fec_prov_interc,
                 pr_ruc_prov_interc,
                 pr_servicio_interc,
                 pr_estado_interc,
                 pr_id_prov_interc,
                 pr_tipo_serv,
                 id_agrupacion,
                 pr_fec_ini_deven,
                 pr_fec_fin_deven)
              VALUES
                ('CONEC',
                 lv_transid,
                 ln_transline,
                 'REAL',
                 'LOCAL',
                 ' ',
                 'PROV',
                 j.id_ciclo,
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 pd_fecha,
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 ' ',
                 pd_fecha,
                 pd_fecha);
            
            EXCEPTION
              WHEN OTHERS THEN
                lv_error := 'Error 1, al insertar el preasiento en la tabla ps_pr_jgen_acct_en_dth_pre_sap ' ||
                            SQLERRM;
                gsib_security.dotraceh(pv_aplicacion,
                                       lv_error,
                                       pn_id_transaccion);
                ROLLBACK;
                RAISE le_error;
            END;
            ln_transline := ln_transline + 1;
          EXCEPTION
            WHEN OTHERS THEN
              lv_error := 'Error 2 al insertar el preasiento' || SQLERRM;
              gsib_security.dotraceh(pv_aplicacion,
                                     lv_error,
                                     pn_id_transaccion);
              ROLLBACK;
              RAISE le_error;
          END;
        END IF;
      END LOOP;
      dth_devengamiento_pre_sap.p_descripcion_cta('2205020010',
                                                  lc_ctacontab);
      lv_referencia := lc_ctacontab || 'DEVENGADO PASIVO DIFERIDO DTH DEL ' ||
                       lv_dia_inicio || '/' || to_char(pd_fecha, 'mm/rrrr') ||
                       ' HASTA 23:59 DEL ' ||
                       to_char(pd_fecha, 'dd/mm/rrrr');
      lv_transid    := 'BG' || to_char(pd_fecha, 'rrrrmmdd');
      INSERT INTO ps_jgen_acct_entry_dth_pre_sap
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         operating_unit,
         product,
         currency_cd,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         doc_type,
         doc_seq_date,
         line_descr,
         gl_distrib_status,
         altacct,
         deptid,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         statistics_code,
         statistic_amount,
         movement_flag,
         doc_seq_nbr,
         jrnl_ln_ref,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         process_instance)
      VALUES
        ('CONEC',
         lv_transid,
         3,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'rrrr'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         1,
         '2205020010',
         ' ',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumgye,
         ln_sumgye,
         'CBSCDDTH',
         pd_fecha,
         lv_referencia,
         'N',
         J.ID_CICLO,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN c_verifica_cuentas('2205020010');
        FETCH c_verifica_cuentas
          INTO lc_verif_ctas;
        lb_found_cta := c_verifica_cuentas%FOUND;
        CLOSE c_verifica_cuentas;
      
        IF lb_found_cta THEN
          lv_ciclo_5359 := ' ';
          lv_tipo_iva   := ' ';
          IF lc_verif_ctas.usa_ciclo = 'Y' THEN
            lv_ciclo_5359 := 'PROV';
          END IF;
          IF lc_verif_ctas.usa_tipo_iva = 'Y' THEN
            lv_tipo_iva := 'PROV';
          END IF;
          INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
            (business_unit,
             transaction_id,
             transaction_line,
             ledger_group,
             ledger,
             pr_jrnl_ln_ref2,
             pr_jrnl_tipo_iva,
             pr_jrnl_ciclo)
          VALUES
            ('CONEC',
             lv_transid,
             1,
             'REAL',
             'LOCAL',
             ' ',
             lv_tipo_iva,
             lv_ciclo_5359);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error 2, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 lv_error,
                                 pn_id_transaccion);
          ROLLBACK;
          RAISE le_error;
      END;
      dth_devengamiento_pre_sap.p_descripcion_cta('2205020010',
                                                  lc_ctacontab);
      lv_referencia := lc_ctacontab || 'DEVENGADO PASIVO DIFERIDO DTH DEL ' ||
                       lv_dia_inicio || '/' || to_char(pd_fecha, 'mm/rrrr') ||
                       ' HASTA 23:59 DEL ' ||
                       to_char(pd_fecha, 'dd/mm/rrrr');
      lv_transid    := 'BU' || to_char(pd_fecha, 'rrrrmmdd');
      INSERT INTO ps_jgen_acct_entry_dth_pre_sap
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         operating_unit,
         product,
         currency_cd,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         doc_type,
         doc_seq_date,
         line_descr,
         gl_distrib_status,
         altacct,
         deptid,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         statistics_code,
         statistic_amount,
         movement_flag,
         doc_seq_nbr,
         jrnl_ln_ref,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         process_instance)
      VALUES
        ('CONEC',
         lv_transid,
         4,
         'REAL',
         'LOCAL',
         pd_fecha,
         'BSCS',
         'CONEC',
         to_char(pd_fecha, 'rrrr'),
         to_char(pd_fecha, 'mm'),
         'NEXT',
         pd_fecha,
         2,
         '2205020010',
         ' ',
         ' ',
         'USD',
         'USD',
         'CRRNT',
         1,
         1,
         ln_sumuio,
         ln_sumuio,
         'CBSCDDTH',
         pd_fecha,
         lv_referencia,
         'N',
         J.ID_CICLO,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0,
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         0);
    
      BEGIN
        --verifico si la cuenta aplica tipo de iva y ciclo
        OPEN c_verifica_cuentas('2205020010');
        FETCH c_verifica_cuentas
          INTO lc_verif_ctas;
        lb_found_cta := c_verifica_cuentas%FOUND;
        CLOSE c_verifica_cuentas;
      
        IF lb_found_cta THEN
          lv_ciclo_5359 := ' ';
          lv_tipo_iva   := ' ';
          IF lc_verif_ctas.usa_ciclo = 'Y' THEN
            lv_ciclo_5359 := 'PROV';
          END IF;
          IF lc_verif_ctas.usa_tipo_iva = 'Y' THEN
            lv_tipo_iva := 'PROV';
          END IF;
          INSERT INTO ps_pr_jgen_acct_en_dth_pre_sap
            (business_unit,
             transaction_id,
             transaction_line,
             ledger_group,
             ledger,
             pr_jrnl_ln_ref2,
             pr_jrnl_tipo_iva,
             pr_jrnl_ciclo)
          VALUES
            ('CONEC',
             lv_transid,
             2,
             'REAL',
             'LOCAL',
             ' ',
             lv_tipo_iva,
             lv_ciclo_5359);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error 3, al insertar el preasiento en la tabla PS_PR_JGEN_ACCT_EN_PRE ' ||
                      SQLERRM;
          gsib_security.dotraceh(pv_aplicacion,
                                 lv_error,
                                 pn_id_transaccion);
          RAISE le_error;
      END;
    
      COMMIT;
    END LOOP;
  
    GSIB_SECURITY.dotraceh(PV_APLICACION,
                           'Fin de carga masiva de datos hacia la tabla ps_jgen_acct_entry_dth_pre_sap y ps_pr_jgen_acct_en_dth_pre_sap  del mes ACTUAL',
                           pn_id_transaccion);
  
    crear_sumarizado_dev_sap(pd_fecha, pv_msj_trama2);
    pv_error := pv_msj_trama2;
  
    -- INICIO  [10183] RGT Ney Miranda T. 
    --RESPALDA LAS TABLA DE SERVICIOS_PREPAGADOS_DTH_SAP AL TERMINAR LA EJECUCION   
     
    gsib_api_sap.respaldo_servicios_dth(pd_fecha,
                                        'DEVENGO',
                                        pn_id_transaccion,
                                        lv_resultado);
    IF lv_resultado IS NOT NULL THEN
      pv_error := pv_error || ' ; ' || pv_msj_trama2;
      RAISE le_error_respalda;
    END IF;
    
    -- FIN  [10183] RGT Ney Miranda T.   
  
    gsib_security.dotraceh(pv_aplicacion,
                           '****FIN GENERA ASIENTOS DEVENGO DTH ' ||
                           'FECHA DE CORTE :' || pd_fecha ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error;
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             '****FIN GENERA ASIENTOS DEVENGO DTH ' ||
                             'FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
      ROLLBACK;
    -- INICIO [10183] RGT Ney Miranda T.  
    -- Detecta si hubo algun error al respaldar la tabla de Servicios_prepagados_sap
    WHEN le_error_respalda THEN
    
      gsib_security.dotraceh(pv_aplicacion,
                             lower(pv_error),
                             pn_id_transaccion);
      gsib_security.dotraceh(pv_aplicacion,
                             '****FIN GENERA ASIENTOS DEVENGO DTH ' ||
                             'FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
      ROLLBACK;
     -- FIN [10183] RGT Ney Miranda T. 
    WHEN OTHERS THEN
      pv_error := 'Error 3 al insertar el preasiento' || SQLERRM;
      gsib_security.dotraceh(pv_aplicacion, lv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(PV_APLICACION,
                             '****FIN GENERA ASIENTOS DEVENGO DTH ' ||
                             'FECHA DE CORTE :' || pd_fecha ||
                             ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
      ROLLBACK;
  END;

  PROCEDURE crear_sumarizado_dev_sap(pd_fecha      IN DATE,
                                     pv_msj_trama2 IN OUT VARCHAR2) IS
    LE_ERROR_TRAMA EXCEPTION;
    MSJ_TRAMAS            VARCHAR2(2500);
    pv_error              VARCHAR2(3000);
    ERROR_TRAMA           VARCHAR2(1000);
    LV_FECHA_ACTUAL       VARCHAR(100) := to_char(SYSDATE,
                                                  'dd/mm/yyyy hh24:mi:ss');
    T_COSTA_DEVENGAMIENTO CLOB := NULL;
  
    T_SIERRA_DEVENGAMIENTO CLOB := NULL;
    lv_html                       VARCHAR2(2500);
    LV_APLICACION                 VARCHAR2(200) := 'DTH_DEVENGAMIENTO_PRE_SAP.CREAR_SUMARIZADO_DEV_SAP';
    LV_MSJ_CREA_TABLA             VARCHAR2(1500);
     -- INICIO [10183] RGT Ney Miranda T. 
     -- Se genera nuevas polizas por aumento de un ciclo
    id_poliza_costa_dev_1  NUMBER := 0;
    id_poliza_sierra_dev_1 NUMBER := 0;
    id_poliza_costa_dev_2  NUMBER := 0;
    id_poliza_sierra_dev_2 NUMBER := 0;
     -- FIN [10183] RGT Ney Miranda T. 
  
    LV_FECHA_ANTERIOR VARCHAR2(100) := TO_CHAR(trunc(TO_DATE(TO_CHAR(pd_fecha,
                                                                     'MM/YYYY'),
                                                             'MM/YYYY'),
                                                     'mm') + 14,
                                               'MMYYYY');
  
    LV_IDENTIFICADOR VARCHAR2(15);
    LN_MESFINAL      NUMBER := TO_NUMBER(TO_CHAR(pd_fecha, 'MM'));
    LN_ANOFINAL      NUMBER := TO_NUMBER(TO_CHAR(pd_fecha, 'YYYY'));
    LV_FECHACONTA    DATE := TO_DATE(TO_CHAR(TO_DATE(pd_fecha), 'YYYYMMDD'),
                                     'YYYYMMDD');
    LNANOFINAL       NUMBER := to_char(pd_fecha, 'YYYY');
    LV_SENTENCIA     VARCHAR2(200);
    LV_ENVIA_TRAMA   VARCHAR2(10);
    LV_ERROR_PREVIA  VARCHAR2(3000);
    CURSOR LC_ENVIA_TRAMA IS
      SELECT VALOR
        FROM GSIB_PARAMETROS_SAP
       WHERE ID_TIPO_PARAMETRO = '10189'
         AND ID_PARAMETRO = 'ENVIA_TRAMA';
  
    --ASIGNA IVA
    CURSOR ASIGNA_IVA IS
      SELECT DISTINCT (p.cta_sap), P.TIPO_IVA FROM gsi_fin_cebes P;
  
    --ASIGNA LOS CENTROS DE BENEFICIO
    CURSOR ASIGNA_CEBES IS
      SELECT R.CTA_SAP, R.CEBE_UIO, R.CEBE_GYE FROM GSI_FIN_CEBES R;
  
    -- CREA LA VISUALIZACION DE LAS POLIZAS GENERADA                     
    CURSOR LC_CREA_HTML(LC_FECHA_CORTE DATE) IS
    
      SELECT G.ID_POLIZA
        FROM GSIB_TRAMAS_SAP G
       WHERE G.FECHA_CORTE = LC_FECHA_CORTE
         AND TRUNC(G.FECHA) = TRUNC(SYSDATE);
  
    -- INICIO  [10183] RGT Ney Miranda T.   
    -- obtengo los Ciclos procesados en el mes
    CURSOR c_ciclos IS
      SELECT ciclo FROM gsib_dev_dth_sumarizado_sap GROUP BY ciclo;
    -- FIN   [10183] RGT Ney Miranda T.   
  
  BEGIN
  
    gsib_api_sap.reinicia_secuencia(pn_id_transaccion); -- VALIDO SI YA PASO UN ANIO Y REINICIO LA SEQUENCIA
  
    -- INICIO  [10183] RGT Ney Miranda T.  
    --Asigno el valor de los id_polizas  
    id_poliza_costa_dev_2  := id_poliza_sap.nextval;
    id_poliza_sierra_dev_2 := id_poliza_sap.nextval;
    id_poliza_costa_dev_1  := id_poliza_sap.nextval;
    id_poliza_sierra_dev_1 := id_poliza_sap.nextval;
    -- FIN   [10183] RGT Ney Miranda T.  
    
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Inicio del procedimiento CREAR_SUMARIZADO_DEV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE',
                           pn_id_transaccion);
  
    --TRUNCO LOS DATOS DE LA TABLA TEMPORAL PARA ALMACENAR LOS NUEVOS VALORES
    LV_SENTENCIA := 'truncate table GSIB_DEV_DTH_SUMARIZADO_SAP';
    EXEC_SQL(LV_SENTENCIA);
  
    GSIB_SECURITY.dotracem(LV_APLICACION,
                           'Se elimino registros de la tabla temporal GSIB_DEV_DTH_SUMARIZADO_SAP',
                           pn_id_transaccion);
  
    OPEN LC_ENVIA_TRAMA;
    FETCH LC_ENVIA_TRAMA
      INTO LV_ENVIA_TRAMA;
    CLOSE LC_ENVIA_TRAMA;
    BEGIN
    
      --CREO LA TABLA GSIB_DEV_DTH_SUMARIZADO_SAP
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Inicio carga masiva de datos hacia la tabla GSIB_DEV_DTH_SUMARIZADO_SAP ',
                             pn_id_transaccion);
    
      INSERT INTO GSIB_DEV_DTH_SUMARIZADO_SAP
        SELECT p.altacct CICLO,
               P.ACCOUNT,
               SUM(P.MONETARY_AMOUNT) MONETARY_AMOUNT,
               SUM(P.FOREIGN_AMOUNT) FOREIGN_AMOUNT,
               'BSCS' || LNANOFINAL ||
               DECODE(SUBSTR(P.TRANSACTION_ID, 1, 2),
 -- INICIO [10183] RGT Ney Miranda T. por el aumento de productos Dth se agisna mas id_polizas	       
                      'BG',
                      decode(p.altacct,
                             '03',
                             lpad(id_poliza_costa_dev_1, 3, 0),
                             '04',
                             lpad(id_poliza_costa_dev_2, 3, 0)),
                      
                      'BU',
                      decode(p.altacct,
                             '03',
                             lpad(id_poliza_sierra_dev_1, 3, 0),
                             '04',
                             lpad(id_poliza_sierra_dev_2, 3, 0))) identificador,   
-- FIN [10183] RGT Ney Miranda T. 
               'EC02' sociedad,
               lv_fechaconta fecha_contabilizacion,
               'DTH' || decode(substr(p.transaction_id, 1, 2),
                               'BG',
                               ' COSTA',
                               'BU',
                               ' SIERRA') REFERENCIA,
               LV_FECHACONTA FECHA_DOC,
               'I1' CLASE_DOC,
               'DEVENGO DTH' || DECODE(SUBSTR(P.TRANSACTION_ID, 1, 2),
                                       'BG',
                                       ' R2',
                                       'BU',
                                       ' R1') TEXTO_CABECERA,
               LN_ANOFINAL ANIO_FINAL,
               LN_MESFINAL MES_FINAL,
               'USD' MONEDA,
               NULL IVA,
               NULL CLAVE_CONTA,
               NULL CENTRO_BENEFICIO,
	        -- INICIO [10183] RGT Ney Miranda T. Cambia la feha del texto de la poliza  
               
               'DEV DEL ' ||'<<<DIA_ANTERIOR>>>'||LV_FECHA_ANTERIOR ||
	       ' HASTA 23h59 DEL ' || to_char(pd_fecha, 'ddmmyyyy') TEXTO
	        -- FIN [10183] RGT Ney Miranda T. 
        
          FROM ps_jgen_acct_entry_dth_pre_sap P
        
         GROUP BY p.altacct, P.ACCOUNT, P.OPERATING_UNIT, P.TRANSACTION_ID;
    
    -- INICIO   [10183] RGT Ney Miranda T.   
    -- Se asigna la fecha correcta segun el ciclo

declare
  cursor c_ciclos_sumarizados is
    select distinct (s.ciclo) ciclo, f.dia_ini_ciclo
      from GSIB_DEV_DTH_SUMARIZADO_SAP s, fa_ciclos_bscs f
     where s.ciclo = f.id_ciclo;

begin

  for C IN c_ciclos_sumarizados loop
    update GSIB_DEV_DTH_SUMARIZADO_SAP G
    
       set G.texto = replace(G.texto,'<<<DIA_ANTERIOR>>>', C.dia_ini_ciclo)
     WHERE G.CICLO = C.ciclo;
  
  end loop;

        -- FIN  [10183] RGT Ney Miranda T.   


end;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    
    
      --ASIGNA LOS CENTROS DE BENEFICIO
      FOR CE IN ASIGNA_CEBES LOOP
        UPDATE GSIB_DEV_DTH_SUMARIZADO_SAP F
           SET F.CENTRO_BENEFICIO = DECODE(SUBSTR(F.TEXTO_CABECERA,
                                                  LENGTH(F.TEXTO_CABECERA) - 1,
                                                  3),
                                           'R2',
                                           CE.CEBE_GYE,
                                           'R1',
                                           CE.CEBE_UIO) --ASIGNA CEBE
         WHERE F.ACCOUNT = CE.CTA_SAP;
      
      END LOOP;
    
      --ASIGNA LAS CLAVES DE CONTABILIZACION
      UPDATE GSIB_DEV_DTH_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 40
       WHERE F.MONETARY_AMOUNT > 0;
    
      UPDATE GSIB_DEV_DTH_SUMARIZADO_SAP F
         SET F.CLAVE_CONTA = 50
       WHERE F.MONETARY_AMOUNT < 0;
    
      -- 10189 Comentado
      /*FOR IV IN ASIGNA_IVA LOOP
        UPDATE GSIB_DEV_DTH_SUMARIZADO_SAP S
           SET S.IVA = IV.TIPO_IVA
         WHERE S.ACCOUNT = IV.CTA_SAP;
      END LOOP;*/
    
      COMMIT;
    
      LV_MSJ_CREA_TABLA := 'Se creo correctamente la tabla GSIB_DEV_DTH_SUMARIZADO_SAP';
    
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             'Fin de carga masiva de datos hacia la tabla GSIB_DEV_DTH_SUMARIZADO_SAP ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        LV_MSJ_CREA_TABLA := 'ERROR. Al crear la tabla GSIB_DEV_DTH_SUMARIZADO_SAP => ' ||
                             SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               lv_msj_crea_tabla,
                               pn_id_transaccion);
      
    END;
  
    BEGIN
    
 FOR i IN c_ciclos LOOP
-- [10183] RGT Ney Miranda T.   
      T_COSTA_DEVENGAMIENTO := crear_trama(pv_producto      => 'DTH',
                                           pv_region        => 'COSTA',
					   pv_ciclo         => i.ciclo,
                                           PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                           ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS := 'Error al crear la trama DEVENGO COSTA => ' ||
                      ERROR_TRAMA;
        RAISE LE_ERROR_TRAMA;
      
        ERROR_TRAMA := NULL;
      ELSE
      
        -- LA ALMACENO EN LA TABLA LAS POLIZAS GENERADAS
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_COSTA_DEVENGAMIENTO,
           'CREADA',
          'DEVENGO DTH COSTA CICLO ' || i.ciclo,
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(T_COSTA_DEVENGAMIENTO,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP DEVENGO COSTA ;' || pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
          pv_error := NULL;
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      
      END IF;
    
      T_SIERRA_DEVENGAMIENTO := crear_trama(pv_producto      => 'DTH',
                                            pv_region        => 'SIERRA',
					    pv_ciclo         => i.ciclo,
                                            PV_IDENTIFICADOR => LV_IDENTIFICADOR,
                                            ERROR            => ERROR_TRAMA);
    
      IF ERROR_TRAMA IS NOT NULL THEN
        MSJ_TRAMAS := MSJ_TRAMAS ||
                      ' Error al crear la trama DEVENGO SIERRA => ' ||
                      ERROR_TRAMA;
        RAISE LE_ERROR_TRAMA;
      
      ELSE
        -- LA ALMACENO EN LA TABLA LAS POLIZAS GENERADAS
      
        INSERT INTO GSIB_TRAMAS_SAP
          (ID_POLIZA,
           FECHA,
           trama,
           estado,
           REFERENCIA,
           FECHA_CORTE,
           OBSERVACION)
        VALUES
          (LV_IDENTIFICADOR,
           SYSDATE,
           T_SIERRA_DEVENGAMIENTO,
           'CREADA',
           'DEVENGO DTH SIERRA CICLO ' || i.ciclo,
           pd_fecha,
           ' =>' || ' Creada el : ' || LV_FECHA_ACTUAL || chr(13));
      
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF LV_ENVIA_TRAMA = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(T_SIERRA_DEVENGAMIENTO,
                                                 pv_error);
      
        IF pv_error IS NOT NULL THEN
          MSJ_TRAMAS := MSJ_TRAMAS ||
                        'ERROR al enviar a SAP DEVENGO SIERRA;' || pv_error;
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO      = 'NO ENVIADA',
                 F.OBSERVACION = F.OBSERVACION || ' => ' || LV_FECHA_ACTUAL ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_error || chr(13)
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        
        ELSE
          UPDATE GSIB_TRAMAS_SAP F
             SET F.ESTADO = 'ENVIADA'
           WHERE F.ID_POLIZA = LV_IDENTIFICADOR;
          COMMIT;
        END IF;
      END IF;
 END LOOP;
    
      --10189 Generacion de Reportes
      genera_reporte_en_html(pd_fecha, lv_html);
      IF LV_ENVIA_TRAMA = 'S' THEN
        MSJ_TRAMAS := 'Se crearon las tramas ; Se envio a SAP';
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      ELSE
        MSJ_TRAMAS := 'Se crearon las tramas ;Pero el envio a SAP NO esta Habilitado';
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      
      END IF;
    
    EXCEPTION
    
      WHEN LE_ERROR_TRAMA THEN
      
        MSJ_TRAMAS := 'ERROR al crear las tramas ' || MSJ_TRAMAS || SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
      WHEN OTHERS THEN
      
        MSJ_TRAMAS := 'ERROR al crear las tramas ' || SQLERRM;
        gsib_security.dotraceh(lv_aplicacion,
                               msj_tramas,
                               pn_id_transaccion);
    END;
  
    FOR D IN LC_CREA_HTML(pd_fecha) LOOP
      GSIB_API_SAP.GENERA_HTML(D.ID_POLIZA, LV_ERROR_PREVIA);
    END LOOP;
  
    PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                     LV_ERROR_PREVIA;
    gsib_security.dotraceh(lv_aplicacion, pv_msj_trama2, pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Fin del procedimiento CREAR_SUMARIZADO_DEV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN OTHERS THEN
    
      PV_MSJ_TRAMA2 := LV_MSJ_CREA_TABLA || ' ' || MSJ_TRAMAS || ' ' ||
                       LV_ERROR_PREVIA;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_msj_trama2,
                             pn_id_transaccion);
    
  END;

  PROCEDURE p_carga_servicios(pv_error OUT VARCHAR2) IS
    /******************************************************************************
     Proyecto   : [8693] Venta DTH Post Pago
     Lider Claro: SIS Paola Carvajal
     Lider PDS  : SUD Cristhian Acosta Chambers
     Fecha      : 31/05/2013
     Descripci�n: Actualiza cuentas contables  de los rubros que no tienen
    *******************************************************************************/
    CURSOR C_servicios IS
      SELECT DISTINCT cb.shdes, TRIM(upper(des)) SERVICIO
        FROM mpusntab cb
       WHERE NOT EXISTS (SELECT cp.codigo_sh, cp.servicio
                FROM CALCULO_PRORRATEO cp
               WHERE cb.shdes = cp.codigo_sh);
  
    lv_procedimiento VARCHAR2(100);
  
  BEGIN
    pv_error         := NULL;
    gv_aplicacion    := 'dth_devengamiento_pre_sap.';
    lv_procedimiento := 'P_CARGA_SERVICIO -';
    FOR I IN C_SERVICIOS LOOP
      BEGIN
        --Llenar la tabla CALCULO_PRORRATEO con el codigo_sh
        INSERT INTO CALCULO_PRORRATEO
          (servicio, codigo_sh)
        VALUES
          (i.servicio, i.shdes);
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR := 'Error al insertar en CALCULO_PRORRATEO ' ||
                      gv_aplicacion || lv_procedimiento || SQLERRM;
      END;
    END LOOP;
    COMMIT;
  END;

  PROCEDURE p_actualiza_ctas(pv_error OUT VARCHAR2) IS
  
    /******************************************************************************
     Proyecto   : [8693] Venta DTH Post Pago
     Lider Claro: SIS Paola Carvajal
     Lider PDS  : SUD Cristhian Acosta Chambers
     Fecha      : 31/05/2013
     Descripci�n: Actualiza cuentas contables  de los rubros que no tienen
    *******************************************************************************/
    i                NUMBER;
    lv_procedimiento VARCHAR2(100);
  
    --obtiene las cuentas contables de los servicios no configurados
    CURSOR c_cuentas IS
      SELECT a.codigo_sh, a.servicio, b.ctapsoft
        FROM CALCULO_PRORRATEO a, mpusntab c, cob_servicios b
       WHERE a.codigo_contable IS NULL
         AND a.codigo_sh = c.shdes
         AND c.sncode = b.servicio;
  
  BEGIN
    pv_error         := NULL;
    gv_aplicacion    := 'dth_devengamiento_pre_sap.';
    lv_procedimiento := 'P_ACTUALIZA_CTAS -';
  
    --actualiza las cuentas contables en los servicios
    FOR i IN c_cuentas LOOP
      BEGIN
        UPDATE CALCULO_PRORRATEO x
           SET x.codigo_contable = i.ctapsoft
         WHERE x.codigo_sh = i.codigo_sh
           AND x.codigo_contable IS NULL;
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          PV_ERROR := 'Error al actualizar cuentas en CALCULO_PRORRATEO ' ||
                      gv_aplicacion || lv_procedimiento || SQLERRM;
      END;
    END LOOP;
    COMMIT;
  END;

  PROCEDURE replica_financiero(pd_fecha IN DATE, pv_error OUT VARCHAR2) IS
    /******************************************************************************
     Proyecto   : [8693] Venta DTH Post Pago
     Creado Por : SUD Norman Castro
     Lider Claro: SIS Paola Carvajal
     Lider PDS  : SUD Cristhian Acosta Chambers
     Fecha      : 06/06/2013
     Descripci�n: Proceso que replica la informacion de la provision de DTH al area de Financiero
    *******************************************************************************/
    lv_proceso VARCHAR2(60) := 'REPLICA_FINANCIERO';
    --ld_fecha_dep      date:=add_years(pd_fecha, -1);
    lv_anio  VARCHAR2(4) := NULL;
    lv_mes   VARCHAR2(2) := NULL;
    lv_error VARCHAR2(1000) := NULL;
    le_error EXCEPTION;
  
  BEGIN
    IF pd_fecha IS NULL THEN
      lv_error := 'El parametro ingresado no es valido - ';
      RAISE le_error;
    END IF;
    /*  begin
    
        insert into
        ps_jgen_acct_entry@finsys
        select * from ps_jgen_acct_entry_dth_pre
        where journal_date = pd_fecha;
    
    --    null;
      exception
        when dup_val_on_index then
         lv_error:='Existen registros duplicados a cargar a la tabla PS_JGEN_ACCT_ENTRY - ';
         raise le_error;
        when others then
          lv_error := sqlerrm;
         raise le_error;
      end;*/
  
    BEGIN
    
      INSERT INTO ps_pr_jgen_acct_en_finsys
        SELECT *
          FROM ps_pr_jgen_acct_en_dth_pre_sap
         WHERE pr_fec_prov_interc = pd_fecha;
    
      --    null;
    EXCEPTION
      WHEN dup_val_on_index THEN
        lv_error := 'Existen registros duplicados a cargar a la tabla PS_PR_JGEN_ACCT_EN - ';
        RAISE le_error;
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
    -- inserto los valor y servicios devengados a la tabla historica.
    BEGIN
      INSERT INTO dth_devengados_hist
        (ciclo,
         dia,
         mes,
         anio,
         tipo,
         nombre,
         valor,
         ctactble,
         cta_ctrap,
         compa�ia,
         devengado,
         shdes,
         fecha_devengado)
        SELECT a.ciclo,
               a.dia,
               a.mes,
               a.anio,
               a.tipo,
               a.nombre,
               a.valor,
               a.ctactble,
               a.cta_ctrap,
               a.compa�ia,
               b.valor_provisiona,
               a.shdes,
               SYSDATE
          FROM servicios_prepagados_dth_sap   a,
               reporte_calc_prorrateo_dth_pre b
         WHERE upper(a.nombre) = b.servicio
           AND decode(a.compa�ia, 'Guayaquil', 'GYE', 'Quito', 'UIO') =
               b.ciudad
           AND a.ciclo = b.id_ciclo
           AND a.mes = b.mes
           AND a.Ctactble = b.Codigo_Contable;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
    BEGIN
      lv_anio := extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      lv_mes  := extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      INSERT INTO dth_bitacora_finan
        (anio, mes, fecha_journal, estado, comentario, fecha_replicacion)
      VALUES
        (lv_anio,
         lv_mes,
         pd_fecha,
         'R', --replicado
         'Replicado a Financiero',
         SYSDATE);
      -- borro registros de 1 a�o atras del atabla de bitacora
    
      DELETE FROM dth_bitacora_finan
       WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
         AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      -- borro registros de 1 a�o atras de la tabla de servicios prepagados
      DELETE FROM servicios_prepagados_dth_sap
       WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
         AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
    COMMIT;
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error || gv_aplicacion || lv_proceso;
      ROLLBACK;
    WHEN OTHERS THEN
      pv_error := SQLERRM || gv_aplicacion || lv_proceso;
      ROLLBACK;
  END;

  PROCEDURE ajusta_prepago_dth(pd_fecha IN DATE, pv_error OUT VARCHAR2) IS
    ------------------------------------------------------------------------------------------------------------
    -- Proyecto: [8693] DTH Venta DTH Postpago
    -- Autor: SUD Norman Castro
    -- Creado: 29/05/2013
    -- Lider Conecel: SIS Paola Carvajal
    -- Lider SUD: SUD Cristhian Acosta Chambers
    -- Proposito:  Proceso que realiza el ajuste o la devengacion de los servicios pagados por adelantado para
    --             el producto DTH
    ------------------------------------------------------------------------------------------------------------
    CURSOR c_ciclos IS
      SELECT f.id_ciclo, f.dia_ini_ciclo, f.dia_fin_ciclo, f.descripcion
        FROM fa_ciclos_bscs f;
    CURSOR c_datos(cv_ciclo VARCHAR2, cn_mes NUMBER, cn_anio NUMBER) IS
      SELECT tipo,
             nombre,
             valor,
             nvl(por_devengar, 0) por_devengar,
             ctactble,
             cta_ctrap,
             compa�ia,
             decode(compa�ia, 'Guayaquil', 'GYE', 'Quito', 'UIO') ciudad,
             t.mes,
             t.anio,
             t.shdes,
             ROWID --sud cac - ajuste devengamiento 31/10/2014
        FROM servicios_prepagados_dth_sap t
       WHERE ciclo = cv_ciclo
         AND mes = cn_mes
         AND anio = cn_anio
      --and servicio = 1562
      ;
  
    CURSOR cta_post(cv_codigo VARCHAR2) IS
      SELECT a.ctapost
        FROM cob_servicios a
       WHERE upper(a.nombre) =
             (SELECT upper(des) FROM mpusntab WHERE SHDES = cv_codigo);
    -- cursor para ver si ha sido replicado el asiento a financiero
    CURSOR c_verifica(cn_anio NUMBER, cn_mes NUMBER) IS
      SELECT fecha_replicacion
        FROM dth_bitacora_finan
       WHERE anio = cn_anio
         AND mes = cn_mes
         AND estado = 'R';
    lv_proceso VARCHAR2(60) := 'DTH_DEVENGAMIENTO_PRE_SAP.AJUSTA_PREPAGO_DTH';
    --ld_fecha       date:=trunc(sysdate);
    ld_fecha_ejec   DATE := NULL;
    ln_ult_dia      NUMBER;
    ln_valor_dia    NUMBER := 0; -- valor x dia del servicio
    ln_pro_act      NUMBER := 0; -- valor prorrateado del servicio desde el ciclo hasta fin de mes.
    ln_pro_ant      NUMBER := 0; -- valor prorrateado del servicio desde el fin de mes hasta incio ciclo.
    ln_dias         NUMBER := 0;
    lv_sentencia    VARCHAR2(2000);
    lv_cta_contable VARCHAR2(30) := NULL;
    ld_fecha_ant    DATE;
    ln_valor        NUMBER;
    lv_error        VARCHAR2(2000) := NULL;
    ld_fecha_rep    DATE := NULL;
    lb_found        BOOLEAN := FALSE;
    ln_dias_ant     NUMBER := NULL;
    ln_valor_dia2   NUMBER := 0; -- valor x dia del servicio para reproceso
    ln_devengado    NUMBER := 0;
    ln_por_devengar NUMBER := 0;
    LV_APLICACION   VARCHAR2(100) := 'DTH_DEVENGAMIENTO_PRE_SAP.AJUSTA_PREPAGO_DTH';
    le_error EXCEPTION;
    le_siguiente EXCEPTION;
  BEGIN
  
    GSIB_SECURITY.dotraceh(lv_proceso,
                           '****INICIO GENERA PRORRATEO DEVENGO DTH FECHA DE CORTE : ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(lv_proceso,
                           'Inicio del procedimiento que genera la tabla reporte_calc_prorrateo_dth_pre.',
                           pn_id_transaccion);
  
    IF pd_fecha IS NULL THEN
      lv_error := 'Error, El parametro no puede ser nulo';
      gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
      RAISE le_error;
    ELSE
      IF extract(DAY FROM pd_fecha) <>
         extract(DAY FROM(last_day(pd_fecha))) THEN
        -- si no es el ultimo dia del mes
        lv_error := 'Error, Debe ingresar el ultimo dia del mes';
        gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
      
        RAISE le_error;
      ELSE
        ld_fecha_ejec := pd_fecha;
      END IF;
    END IF;
  
    ln_ult_dia := extract(DAY FROM ld_fecha_ejec);
    -- validacion de si ha sido replicado a financiero; de ser asi no se debe poder procesar nuevamente.
    OPEN c_verifica(extract(YEAR FROM ld_fecha_ejec),
                    extract(MONTH FROM ld_fecha_ejec));
    FETCH c_verifica
      INTO ld_fecha_rep;
    lb_found := c_verifica%FOUND;
    CLOSE c_verifica;
    IF lb_found THEN
      lv_error := 'No es posible generar el reporte nuevamente, ya ha sido replicado a SAP el dia ' ||
                  ld_fecha_rep;
      gsib_security.dotraceh(lv_proceso, lv_error, pn_id_transaccion);
    
      RAISE le_error;
    END IF;
  
    lv_sentencia := 'truncate table reporte_calc_prorrateo_dth_pre';
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
      GSIB_SECURITY.dotracem(LV_APLICACION,
                             'Se elimino los registros de la tabla temporal reporte_calc_prorrateo_dth_pre ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        GSIB_SECURITY.dotracem(LV_APLICACION,
                               'Error al tratar de eliminar registros la tabla temporal reporte_calc_prorrateo_dth_pre',
                               pn_id_transaccion);
      
        RAISE Le_Error;
    END;
    -- trunco las tablas de asientos contables locales
    lv_sentencia := 'truncate table ps_jgen_acct_entry_dth_pre_sap';
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
      GSIB_SECURITY.dotracem(LV_APLICACION,
                             'Se elimino los registros de la tabla temporal ps_jgen_acct_entry_dth_pre_sap ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        GSIB_SECURITY.dotracem(LV_APLICACION,
                               'Error al tratar de eliminar registros la tabla temporal ps_jgen_acct_entry_dth_pre_sap',
                               pn_id_transaccion);
      
        RAISE Le_Error;
    END;
  
    lv_sentencia := 'truncate table ps_pr_jgen_acct_en_dth_pre_sap';
  
    BEGIN
      EXECUTE IMMEDIATE lv_sentencia;
      GSIB_SECURITY.dotracem(LV_APLICACION,
                             'Se elimino los registros de la tabla temporal ps_pr_jgen_acct_en_dth_pre_sap ',
                             pn_id_transaccion);
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := substr(SQLERRM, 1, 200);
        GSIB_SECURITY.dotracem(LV_APLICACION,
                               'Error al tratar de eliminar registros la tabla temporal ps_pr_jgen_acct_en_dth_pre_sap',
                               pn_id_transaccion);
      
        RAISE Le_Error;
    END;
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Inicio de carga masiva de datos hacia la tabla reporte_calc_prorrateo_dth_pre ',
                           pn_id_transaccion);
  
    FOR i IN c_ciclos LOOP
      BEGIN
        ln_dias := (ln_ult_dia - i.dia_ini_ciclo) + 1;
        FOR j IN c_datos(i.id_ciclo,
                         extract(MONTH FROM ld_fecha_ejec),
                         extract(YEAR FROM ld_fecha_ejec)) LOOP
          -- sacando el valor prorrateado del mes en curso
          ln_valor_dia := j.valor / ln_ult_dia; -- sacando el valor x dia
          ln_pro_act   := ln_valor_dia * ln_dias;
          ln_valor     := round((j.valor - ln_pro_act), 3);
          -- actualizo el nuevo valor, le resto lo que ya se ha consumido
          UPDATE servicios_prepagados_dth_sap
             SET por_devengar = ln_valor, devengado = round(ln_pro_act, 3)
           WHERE tipo = j.tipo
             AND nombre = j.nombre
             AND ciclo = i.id_ciclo
             AND compa�ia = j.compa�ia
             AND mes = extract(MONTH FROM ld_fecha_ejec)
             AND anio = extract(YEAR FROM ld_fecha_ejec)
             AND ROWID = j.rowid; --sud cac - ajuste devengamiento 31/10/2014
          -- busco la cuenta contable de ese servicio
          lv_cta_contable := NULL;
          FOR k IN cta_post(j.shdes) LOOP
            BEGIN
              IF k.ctapost IS NOT NULL THEN
                lv_cta_contable := k.ctapost;
              ELSE
                RAISE le_siguiente;
              END IF;
            EXCEPTION
              WHEN le_siguiente THEN
                NULL;
              WHEN OTHERS THEN
                lv_error := SQLERRM;
                RAISE le_error;
            END;
          END LOOP;
          IF lv_cta_contable IS NULL THEN
            BEGIN
              SELECT DISTINCT a.codigo_contable
                INTO lv_cta_contable
                FROM CALCULO_PRORRATEO a
               WHERE a.servicio = upper(TRIM(j.nombre));
            EXCEPTION
              WHEN too_many_rows THEN
                lv_cta_contable := NULL;
              WHEN no_data_found THEN
                BEGIN
                  SELECT t.codigo_contable
                    INTO lv_cta_contable
                    FROM prov_porc_llamada t
                   WHERE t.nombre = upper(TRIM(j.nombre))
                     AND t.id_ciclo = i.id_ciclo;
                EXCEPTION
                  WHEN no_data_found THEN
                    lv_cta_contable := NULL;
                END;
            END;
          END IF;
          -- inserto lo devengado en la tabla reporte_calc_prorrateo_dth_pre
          INSERT INTO reporte_calc_prorrateo_dth_pre
            (ciudad,
             codigo_contable,
             producto,
             servicio,
             valor,
             prorrateo,
             valor_provisiona,
             producto_asi,
             codigo_sh,
             id_ciclo,
             mes,
             anio)
          VALUES
            (j.ciudad,
             lv_cta_contable,
             'DTH',
             upper(TRIM(j.nombre)),
             round(ln_pro_act, 3),
             0,
             round(ln_pro_act, 3),
             '',
             '',
             i.id_ciclo,
             j.mes,
             j.anio);
        
        END LOOP;
        -- procesando lo que quedo del mes anterior.
        ld_fecha_ant := add_months(ld_fecha_ejec, -1);
        FOR j IN c_datos(i.id_ciclo,
                         extract(MONTH FROM ld_fecha_ant),
                         extract(YEAR FROM ld_fecha_ant)) LOOP
          -- sacando el valor prorrateado del mes en curso
          IF j.por_devengar <> 0 THEN
            -- si por devengar no es 0 significa que es la primera vez que se ejecuta
            ln_valor_dia := j.por_devengar / i.dia_fin_ciclo; -- sacando el valor x dia
            ln_pro_ant   := ln_valor_dia * i.dia_fin_ciclo;
            ln_valor     := round((j.por_devengar - ln_pro_ant), 3);
            UPDATE servicios_prepagados_dth_sap
               SET por_devengar = ln_valor,
                   devengado    = round((devengado + ln_pro_ant), 3)
             WHERE tipo = j.tipo
               AND nombre = j.nombre
               AND ciclo = i.id_ciclo
               AND compa�ia = j.compa�ia
               AND mes = extract(MONTH FROM ld_fecha_ant)
               AND anio = extract(YEAR FROM ld_fecha_ant)
               AND ROWID = j.rowid; --sud cac - ajuste devengamiento 31/10/2014
          ELSE
            -- si es 0 ya fue ejecutado antes
            ln_ult_dia      := extract(DAY FROM(last_day(ld_fecha_ant))); -- saco el ultimo dia del mes anterior
            ln_dias_ant     := (ln_ult_dia - i.dia_ini_ciclo) + 1;
            ln_valor_dia2   := j.valor / ln_ult_dia;
            ln_devengado    := ln_valor_dia2 * ln_dias_ant; -- lo que se debio devengar el mes anterior
            ln_por_devengar := j.valor - ln_devengado;
            ln_valor_dia    := ln_por_devengar / i.dia_fin_ciclo;
            ln_pro_ant      := ln_valor_dia * i.dia_fin_ciclo;
            ln_valor        := round((ln_por_devengar - ln_pro_ant), 3);
            UPDATE servicios_prepagados_dth_sap
               SET por_devengar = ln_valor,
                   devengado    = round((ln_devengado + ln_pro_ant), 3)
             WHERE tipo = j.tipo
               AND nombre = j.nombre
               AND ciclo = i.id_ciclo
               AND compa�ia = j.compa�ia
               AND mes = extract(MONTH FROM ld_fecha_ant)
               AND anio = extract(YEAR FROM ld_fecha_ant)
               AND ROWID = j.rowid; --sud cac - ajuste devengamiento 31/10/2014
          END IF;
          -- busco la cuenta contable de ese servicio
          lv_cta_contable := NULL;
          FOR l IN cta_post(j.shdes) LOOP
            BEGIN
              IF l.ctapost IS NOT NULL THEN
                lv_cta_contable := l.ctapost;
              ELSE
                RAISE le_siguiente;
              END IF;
            EXCEPTION
              WHEN le_siguiente THEN
                NULL;
              WHEN OTHERS THEN
                lv_error := SQLERRM;
                RAISE le_error;
            END;
          END LOOP;
          IF lv_cta_contable IS NULL THEN
            BEGIN
              SELECT DISTINCT a.codigo_contable
                INTO lv_cta_contable
                FROM CALCULO_PRORRATEO a
               WHERE a.servicio = upper(TRIM(j.nombre));
            EXCEPTION
              WHEN too_many_rows THEN
                lv_cta_contable := NULL;
              WHEN no_data_found THEN
                BEGIN
                  SELECT t.codigo_contable
                    INTO lv_cta_contable
                    FROM prov_porc_llamada t
                   WHERE t.nombre = upper(TRIM(j.nombre))
                     AND t.id_ciclo = i.id_ciclo;
                EXCEPTION
                  WHEN no_data_found THEN
                    lv_cta_contable := NULL;
                END;
            END;
          END IF;
          -- inserto lo devengado en la tabla reporte_calc_prorrateo_dth_pre
          INSERT INTO reporte_calc_prorrateo_dth_pre
            (ciudad,
             codigo_contable,
             producto,
             servicio,
             valor,
             prorrateo,
             valor_provisiona,
             producto_asi,
             codigo_sh,
             id_ciclo,
             mes,
             anio)
          VALUES
            (j.ciudad,
             lv_cta_contable,
             'DTH',
             upper(TRIM(j.nombre)),
             round(ln_pro_ant, 3),
             0,
             round(ln_pro_ant, 3),
             '',
             '',
             i.id_ciclo,
             j.mes,
             j.anio);
        END LOOP;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := SQLERRM;
          RAISE le_error;
      END;
    END LOOP;
    COMMIT;
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           'Fin de carga masiva de datos hacia la tabla reporte_calc_prorrateo_dth_pre ',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(lv_proceso,
                           'Fin del procedimiento que genera la tabla reporte_calc_prorrateo_dth_pre.',
                           pn_id_transaccion);
  
    GSIB_SECURITY.dotraceh(lv_proceso,
                           '****FIN GENERA PRORRATEO DEVENGO DTH FECHA DE CORTE : ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error || ' ' || gv_aplicacion || lv_proceso;
      gsib_security.dotraceh(lv_proceso, pv_error, pn_id_transaccion);
    
      GSIB_SECURITY.dotraceh(lv_proceso,
                             '****FIN GENERA PRORRATEO DEVENGO DTH FECHA DE CORTE : ' ||
                             pd_fecha || ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
    
    WHEN OTHERS THEN
      pv_error := 'Error en el proceso ' || gv_aplicacion || lv_proceso || '-' ||
                  SQLERRM;
    
      gsib_security.dotraceh(lv_proceso, pv_error, pn_id_transaccion);
    
      GSIB_SECURITY.dotraceh(lv_proceso,
                             '****FIN GENERA PRORRATEO DEVENGO DTH FECHA DE CORTE : ' ||
                             pd_fecha || ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
    
  END;

  FUNCTION crear_trama(PV_PRODUCTO      IN VARCHAR2,
                       PV_REGION        IN VARCHAR2,
     		       pv_ciclo         IN VARCHAR2, --[10183] RGT Ney Miranda T.   
                       PV_IDENTIFICADOR OUT VARCHAR2,
                       ERROR            IN OUT VARCHAR2) RETURN CLOB IS
  
    LN_MONTO      NUMBER := 0;
    PV_APLICACION VARCHAR2(100) := 'dth_devengamiento_pre_sap.CREAR_TRAMA';
  
    ACU NUMBER := 0;
    --LV_MONTO DECIMAL;
    LC_CABECERA    CLOB;
    LC_CUERPO_TEMP CLOB;
    LC_CUERPO_FIN  CLOB;
    RESULTADO      VARCHAR2(1000);
    LV_NODO_RAIZ   VARCHAR2(100);
    LV_SOCIEDAD    VARCHAR2(10);
    MY_ERROR EXCEPTION;
    --OBTENGO TODOS LOS REGISTROS DEACUERDO AL PRODUCTO Y A LA REGION
    CURSOR C_PROD_REG IS
      SELECT *
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
        AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.   
       ORDER BY G.CLAVE_CONTA;
  
    --OBTENGO EL IDENTIFICADOR UNICO POR CADA PRODUCTO Y  REGION
    CURSOR C_IDENTIFICADOR IS
      SELECT DISTINCT (G.IDENTIFICADOR)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
  AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.   
         AND ROWNUM = 1;
    LV_IDENTIFICADOR GSIB_DEV_DTH_SUMARIZADO_SAP.IDENTIFICADOR%TYPE;
  
    --OBTENGO LA FECHA DEL REGISTRO CONTABLE
    CURSOR C_REGISTRO_CONTABLE IS
      SELECT DISTINCT (G.FECHA_CONTABILIZACION)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.   
         AND ROWNUM = 1;
    LV_REGISTRO_CONTABLE GSIB_DEV_DTH_SUMARIZADO_SAP.FECHA_CONTABILIZACION%TYPE;
  
    ----OBTENGO EL TIPO DE DOCUMENTO
    CURSOR C_CLASE_DOC IS
      SELECT DISTINCT (G.CLASE_DOC)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.   
         AND ROWNUM = 1;
    LV_CLASE_DOC GSIB_DEV_DTH_SUMARIZADO_SAP.CLASE_DOC%TYPE;
  
    --OBTENGO LA REFERENCIA
    CURSOR C_REFERENCIA IS
      SELECT DISTINCT (G.REFERENCIA)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
        AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.   
         AND ROWNUM = 1;
    LV_REFERENCIA GSIB_DEV_DTH_SUMARIZADO_SAP.REFERENCIA%TYPE;
  
    --OBTENGO TEXTO CABECERA
    CURSOR C_TXT_CABECERA IS
      SELECT DISTINCT (G.TEXTO_CABECERA)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T. 
         AND ROWNUM = 1;
    LV_TXT_CABECERA GSIB_DEV_DTH_SUMARIZADO_SAP.TEXTO_CABECERA%TYPE;
  
    --OBTENGO ANIO FISCAL
    CURSOR C_ANIO_FISCAL IS
      SELECT DISTINCT (G.ANIO_FINAL)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T.  
         AND ROWNUM = 1;
    LV_ANIO_FISCAL GSIB_DEV_DTH_SUMARIZADO_SAP.ANIO_FINAL%TYPE;
  
    --OBTENGOEL MES FISCAL
    CURSOR C_MES_FISCAL IS
      SELECT DISTINCT (G.MES_FINAL)
        FROM GSIB_DEV_DTH_SUMARIZADO_SAP G
       WHERE G.REFERENCIA LIKE '%' || PV_REGION || '%'
         AND G.REFERENCIA LIKE '%' || PV_PRODUCTO || '%'
         AND g.ciclo = pv_ciclo --  [10183] RGT Ney Miranda T. 
         AND ROWNUM = 1;
    LV_MES_FISCAL GSIB_DEV_DTH_SUMARIZADO_SAP.MES_FINAL%TYPE;
  
  BEGIN
  
    LC_CABECERA := dth_devengamiento_pre_sap.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                         'CABECERA');
    IF LC_CABECERA IS NULL THEN
      RESULTADO := 'PARAMETRO CABECERA  NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LV_NODO_RAIZ := dth_devengamiento_pre_sap.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                          'NODO_RAIZ');
    IF LV_NODO_RAIZ IS NULL THEN
      RESULTADO := 'PARAMETRO NODO_RAIZ NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
  
    LC_CABECERA := REPLACE(LC_CABECERA, '<%N_RAIZ%>', LV_NODO_RAIZ);
  
    --
    OPEN C_IDENTIFICADOR;
    FETCH C_IDENTIFICADOR
      INTO LV_IDENTIFICADOR;
    CLOSE C_IDENTIFICADOR;
  
    IF LV_IDENTIFICADOR IS NULL THEN
      RESULTADO := 'IDENTIFICADOR PARA EL PRODUCTO ' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || ' NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
  
    PV_IDENTIFICADOR := LV_IDENTIFICADOR;
    LC_CABECERA      := REPLACE(LC_CABECERA,
                                '<%ID_POLIZA%>',
                                REPLACE(LV_IDENTIFICADOR, ' ', ''));
  
    --
    LV_SOCIEDAD := dth_devengamiento_pre_sap.OBTENER_VALOR_PARAMETRO_SAP(10189,
                                                                         'SOCIEDAD');
    IF LV_SOCIEDAD IS NULL THEN
      RESULTADO := 'PARAMETRO SOCIEDAD NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%SOCIEDAD%>', LV_SOCIEDAD);
  
    --
    OPEN C_REGISTRO_CONTABLE;
    FETCH C_REGISTRO_CONTABLE
      INTO LV_REGISTRO_CONTABLE;
    CLOSE C_REGISTRO_CONTABLE;
  
    IF LV_REGISTRO_CONTABLE IS NULL THEN
      RESULTADO := 'REGISTRO CONTABLE PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%FECHA%>',
                           TO_CHAR(LV_REGISTRO_CONTABLE, 'YYYYMMDD'));
  
    --
  
    OPEN C_CLASE_DOC;
    FETCH C_CLASE_DOC
      INTO LV_CLASE_DOC;
    CLOSE C_CLASE_DOC;
  
    IF LV_CLASE_DOC IS NULL THEN
      RESULTADO := 'LA CLASE DE DOCUMENTO PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%DOCUMENTO%>', LV_CLASE_DOC);
  
    --
  
    OPEN C_REFERENCIA;
    FETCH C_REFERENCIA
      INTO LV_REFERENCIA;
    CLOSE C_REFERENCIA;
  
    IF LV_REFERENCIA IS NULL THEN
      RESULTADO := 'LA REFERENCIA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA,
                           '<%REFERENCIA_CIUDAD%>',
                           LV_REFERENCIA);
  
    --
  
    OPEN C_TXT_CABECERA;
    FETCH C_TXT_CABECERA
      INTO LV_TXT_CABECERA;
    CLOSE C_TXT_CABECERA;
  
    IF LV_TXT_CABECERA IS NULL THEN
      RESULTADO := 'EL TEXTO CABECERA PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%CICLO%>', LV_TXT_CABECERA);
  
    --
  
    OPEN C_ANIO_FISCAL;
    FETCH C_ANIO_FISCAL
      INTO LV_ANIO_FISCAL;
    CLOSE C_ANIO_FISCAL;
  
    IF LV_ANIO_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%ANIO%>', LV_ANIO_FISCAL);
  
    --
  
    OPEN C_MES_FISCAL;
    FETCH C_MES_FISCAL
      INTO LV_MES_FISCAL;
    CLOSE C_MES_FISCAL;
  
    IF LV_MES_FISCAL IS NULL THEN
      RESULTADO := 'EL ANIO FISCAL PARA EL PRODUCTO' || PV_PRODUCTO ||
                   ' DE LA REGION ' || PV_REGION || 'NO ENCONTRADO';
      RAISE MY_ERROR;
    END IF;
    LC_CABECERA := REPLACE(LC_CABECERA, '<%MES%>', LV_MES_FISCAL); --quitar el +1 solo para pruebas
  
    FOR REC IN C_PROD_REG LOOP
      ACU            := ACU + 1;
      LC_CUERPO_TEMP := OBTENER_VALOR_PARAMETRO_SAP(10189, 'CUERPO');
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%POSICION%>', ACU);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CLAVE_CONTA%>',
                                REC.CLAVE_CONTA);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%FECHA%>',
                                TO_CHAR(REC.FECHA_CONTABILIZACION,
                                        'YYYYMMDD'));
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CUENTABSCS%>',
                                REC.ACCOUNT);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%CENTRO_BENEFICIO%>',
                                REC.CENTRO_BENEFICIO);
    
      LN_MONTO := ABS(REC.MONETARY_AMOUNT);
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<%MONTO%>',
                                GSIB_API_SAP.ABS_NUMERO(REC.MONETARY_AMOUNT));
    
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%IMPUESTO%>', '^');
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%VALOR_IMPUESTO%>', '^'); -- EN DEVENAGCION NO TIENE SEGUN LA BAPI
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%ASIGNACION%>', 'DEVENGO');
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%TEXTO%>', REC.TEXTO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP,
                                '<CICLO_FACTURACION>',
                                REC.CICLO);
      LC_CUERPO_TEMP := REPLACE(LC_CUERPO_TEMP, '<%MONEDA%>', REC.MONEDA);
    
      LC_CUERPO_FIN := LC_CUERPO_FIN || LC_CUERPO_TEMP;
    END LOOP;
  
    RETURN(LC_CABECERA || LC_CUERPO_FIN || ']');
  
  EXCEPTION
  
    WHEN MY_ERROR THEN
      ERROR := RESULTADO || ' ' || PV_APLICACION;
      RETURN NULL;
    WHEN OTHERS THEN
      ERROR := 'ERROR ' || SQLERRM || ' ' || PV_APLICACION;
      RETURN NULL;
    
  END CREAR_TRAMA;

  FUNCTION obtener_valor_parametro_sap(PN_ID_TIPO_PARAMETRO IN NUMBER,
                                       PV_ID_PARAMETRO      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR C_PARAMETRO(CN_ID_TIPO_PARAMETRO NUMBER,
                       CV_ID_PARAMETRO      VARCHAR2) IS
      SELECT VALOR
        FROM GSIB_PARAMETROS_SAP
       WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
         AND ID_PARAMETRO = CV_ID_PARAMETRO;
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
    FETCH C_PARAMETRO
      INTO LV_VALOR;
    CLOSE C_PARAMETRO;
  
    RETURN LV_VALOR;
  
  END obtener_valor_parametro_sap;
  --
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE envia_tramas_devengo_dth_sap(PD_FECHA     IN DATE, --FECHA DE CORTE
                                         PV_RESULTADO OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(200) := 'FIN_PROVISION_DTH_SAP.ENVIA_TRAMAS_DEVENGO_DTH_SAP';
    lv_anio       VARCHAR2(4) := NULL;
    lv_mes        VARCHAR2(2) := NULL;
    lv_error      VARCHAR2(1000) := NULL;
    le_error EXCEPTION;
    pv_error VARCHAR2(1000) := NULL;
  BEGIN
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****INICIO ENVIA TRAMAS DEVENGO DTH FECHA DE CORTE :' ||
                           PD_FECHA || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    GSIB_API_SAP.envia_tramas(PD_FECHA,
                              'D', --D por DEVENGO 
                              'DTH',
                              '',
                              pn_id_transaccion,
                              PV_RESULTADO);
  
    -- inserto los valor y servicios devengados a la tabla historica.
    BEGIN
      INSERT INTO dth_devengados_hist
        (ciclo,
         dia,
         mes,
         anio,
         tipo,
         nombre,
         valor,
         ctactble,
         cta_ctrap,
         compa�ia,
         devengado,
         shdes,
         fecha_devengado)
        SELECT a.ciclo,
               a.dia,
               a.mes,
               a.anio,
               a.tipo,
               a.nombre,
               a.valor,
               a.ctactble,
               a.cta_ctrap,
               a.compa�ia,
               b.valor_provisiona,
               a.shdes,
               SYSDATE
          FROM servicios_prepagados_dth_sap   a,
               reporte_calc_prorrateo_dth_pre b
         WHERE upper(a.nombre) = b.servicio
           AND decode(a.compa�ia, 'Guayaquil', 'GYE', 'Quito', 'UIO') =
               b.ciudad
           AND a.ciclo = b.id_ciclo
           AND a.mes = b.mes
           AND a.Ctactble = b.Codigo_Contable;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
  
    BEGIN
      lv_anio := extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      lv_mes  := extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      INSERT INTO dth_bitacora_finan
        (anio, mes, fecha_journal, estado, comentario, fecha_replicacion)
      VALUES
        (lv_anio,
         lv_mes,
         pd_fecha,
         'R', --replicado
         'Replicado a SAP',
         SYSDATE);
      -- borro registros de 1 a�o atras del atabla de bitacora
    
      DELETE FROM dth_bitacora_finan
       WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
         AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
      -- borro registros de 1 a�o atras de la tabla de servicios prepagados
      DELETE FROM servicios_prepagados_dth_sap
       WHERE anio = extract(YEAR FROM to_date(pd_fecha, 'dd/mm/rrrr')) - 1
         AND mes = extract(MONTH FROM to_date(pd_fecha, 'dd/mm/rrrr'));
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := SQLERRM;
        RAISE le_error;
    END;
    COMMIT;
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****FIN ENVIA TRAMAS DEVENGO DTH FECHA DE CORTE :' ||
                           PD_FECHA || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error || LV_APLICACION;
    
      gsib_security.dotraceh(lv_aplicacion, pv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****FIN ENVIA TRAMAS DEVENGO DTH FECHA DE CORTE :' ||
                             PD_FECHA || ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
      ROLLBACK;
    WHEN OTHERS THEN
      pv_error := SQLERRM || LV_APLICACION;
      gsib_security.dotraceh(lv_aplicacion, pv_error, pn_id_transaccion);
      GSIB_SECURITY.dotraceh(LV_APLICACION,
                             '****FIN ENVIA TRAMAS DEVENGO DTH FECHA DE CORTE :' ||
                             PD_FECHA || ' FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
    
      ROLLBACK;
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T. 
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 24/04/20115 9:42:15
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reenvia_polizas(pv_id_polizas IN VARCHAR2, --id polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                            pv_resultado  OUT VARCHAR2) IS
    lv_aplicacion VARCHAR2(200) := 'DTH_DEVENGAMIENTO_PRE_SAP.REENVIA_POLIZAS';
  
  BEGIN
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****INICIO RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  
    gsib_api_sap.reenvio_tramas(pv_id_polizas,
                                'D',
                                pn_id_transaccion,
                                pv_resultado);
  
    GSIB_SECURITY.dotraceh(LV_APLICACION,
                           '****FIN RE-ENVIA TRAMAS ' || pv_id_polizas ||
                           ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 02/07/2015 15:16:08
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE reverso_devengamiento_dth(pd_fecha     IN DATE,
                                      pv_resultado OUT VARCHAR2) IS
    lv_aplicacion     VARCHAR2(150) := 'DTH_DEVENGAMIENTO_PRE_SAP.REVERSO_DEVENGAMIENTO_DTH';
    lv_truncate_table VARCHAR2(500) := '';
    lv_insert_table   VARCHAR2(500) := '';
    CURSOR lc_valida_table(cv_nombre VARCHAR2) IS
      SELECT COUNT(*)
        FROM all_all_tables
       WHERE upper(table_name) = upper(cv_nombre);
    lv_valida_table NUMBER := 0;
    lb_found        BOOLEAN;
    le_error EXCEPTION;
    lv_error        VARCHAR2(3000);
    lv_nombre_tabla VARCHAR2(300);
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '****INICIO REPROCESO DEVENGO DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
    lv_nombre_tabla := 'gsib_serv_prepagados_' ||
                       to_char(trunc(pd_fecha, 'MONTH') + 14, 'yyyymmdd');
    OPEN lc_valida_table(lv_nombre_tabla);
    FETCH lc_valida_table
      INTO lv_valida_table;
    CLOSE lc_valida_table;
    -- valida si la tabla temporal de ese corte existe
    IF lv_valida_table = 0 THEN
      lv_error := 'La tabla ' || lv_nombre_tabla || ' no existe.';
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
      RAISE le_error;
    END IF;
    BEGIN
      lv_truncate_table := 'truncate table servicios_prepagados_dth_SAP';
      EXECUTE IMMEDIATE lv_truncate_table;
      gsib_security.dotraceh(lv_aplicacion,
                             'Se elimino registros de la tabla servicios_prepagados_dth_SAP',
                             pn_id_transaccion);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'ERROR al tratar de ELIMINAR los registros en la tabla : servicios_prepagados_dth_SAP ' ||
                    SQLERRM;
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
        RAISE le_error;
    END;
    BEGIN
      lv_insert_table := 'insert into servicios_prepagados_dth_SAP  select * from ' ||
                         lv_nombre_tabla;
      EXECUTE IMMEDIATE lv_insert_table;
      gsib_security.dotraceh(lv_aplicacion,
                             'Se Inserto registros de la tabla servicios_prepagados_dth_SAP',
                             pn_id_transaccion);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'ERROR al tratar de INSERTAR los registros en la tabla : servicios_prepagados_dth_SAP ' ||
                    SQLERRM;
        gsib_security.dotraceh(lv_aplicacion, lv_error, pn_id_transaccion);
        RAISE le_error;
    END;
    pv_resultado := 'Datos actualizados al dia :' ||
                    to_char(pd_fecha, 'dd/mm/yyyy') || ' De la tabla ' ||
                    lv_nombre_tabla;
    gsib_security.dotraceh(lv_aplicacion, pv_resultado, pn_id_transaccion);
    COMMIT;
    gsib_security.dotraceh(lv_aplicacion,
                           '****FIN REPROCESO DEVENGO DTH FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '****',
                           pn_id_transaccion);
  EXCEPTION
    WHEN le_error THEN
      pv_resultado := 'ERROR: ' || lv_error || ' - ' || chr(13) ||
                      lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
      gsib_security.dotraceh(lv_aplicacion,
                             '****FIN REPROCESO DEVENGO DTH FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
    WHEN OTHERS THEN
      pv_resultado := 'ERROR: ' || SQLERRM || ' - ' || lv_aplicacion;
      gsib_security.dotraceh(lv_aplicacion,
                             pv_resultado,
                             pn_id_transaccion);
      gsib_security.dotraceh(lv_aplicacion,
                             '****FIN REPROCESO DEVENGO DTH FECHA EJECUCION : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                             '****',
                             pn_id_transaccion);
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 02/07/2015 09:08:17
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE anula_polizas(pd_fecha     IN DATE, --FECHA DE CORTE
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2) IS
    lv_aplicacion VARCHAR2(200) := 'DTH_DEVENGAMIENTO_PRE_SAP.ANULA_POLIZAS';
  BEGIN
    gsib_security.dotraceh(lv_aplicacion,
                           '**** INICIO ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
    gsib_api_sap.anula_polizas(pd_fecha,
                               'DEVENGO',
                               'DTH',
                               pn_id_transaccion,
                               pv_resultado,
                               pv_polizas);
    gsib_security.dotraceh(lv_aplicacion,
                           '**** FIN ANULA POLIZAS CON FECHA DE CORTE ' ||
                           pd_fecha || ' FECHA EJECUCION : ' ||
                           to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss') ||
                           '**** ',
                           pn_id_transaccion);
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Luis Flores
  -- PDS: RGT Fernando Ortega
  -- Fecha de creaci�n: 02/07/2015 13:08:17
  -- Proyecto: [10189] API BSCS SAP
  --===================================================================================
  PROCEDURE genera_reporte_en_html(ld_fecha IN DATE, pv_error OUT VARCHAR2) IS
    --ciclos que tienen provision
    CURSOR c_ciclo IS
      SELECT DISTINCT ciclo,
                      decode(ciclo,
                             '01',
                             'Primer',
                             '02',
                             'Segundo',
                             '03',
                             'Tercer',
                             '04',
                             'Cuarto',
                             '05',
                             'Quinto',
                             '06',
                             'Sexto',
                             '07',
                             'S�ptimo',
                             '08',
                             'Octavo',
                             '09',
                             'Noveno',
                             '10',
                             'D�cimo',
                             'NUEVO CICLO A INGRESAR') des_ciclo
        FROM gsib_dev_dth_sumarizado_sap;
    --ciudad GYE/UIO
    CURSOR c_referencia IS
      SELECT DISTINCT referencia
        FROM gsib_dev_dth_sumarizado_sap
       ORDER BY referencia ASC;
    --datos para el reporte
    CURSOR c_datos(cv_referencia VARCHAR2, cv_ciclo VARCHAR2) IS
      SELECT s.texto,
             s.account,
             s.monetary_amount,
             s.identificador,
             s.fecha_contabilizacion,
             s.referencia,
             s.texto_cabecera,
             s.iva,
             s.clave_conta,
             s.centro_beneficio,
             s.ciclo
        FROM gsib_dev_dth_sumarizado_sap s
       WHERE referencia = cv_referencia
         AND ciclo = cv_ciclo
       ORDER BY s.clave_conta ASC;
    CURSOR lc_id_reporte IS
      SELECT nvl(MAX(id_reporte), 0) + 1 FROM gsib_reportes_html;
    ln_id_reporte NUMBER := 0;
    lc_html           CLOB;
    lv_nombre_excel   VARCHAR2(1000);
    lv_ruta_escenario VARCHAR2(1000);
    lv_aplicacion     VARCHAR2(1000) := 'DTH_DEVENGAMIENTO_PRE_SAP.GENERA_REPORTE_EN_HTML';
  BEGIN
    lv_ruta_escenario := obtener_valor_parametro_sap(10189,
                                                     'RUTA_REPORTE_DEVENGO_DTH');
    FOR c IN c_ciclo LOOP
      FOR r IN c_referencia LOOP
        lv_nombre_excel := lower(lv_ruta_escenario || 'Devengo_' ||
                                 REPLACE(r.referencia, ' ', '_') || '_' ||
                                 to_char(ld_fecha, 'ddmmyyyy') || '_' ||
                                 c.ciclo || '.xls');
        --cabecera
        lc_html := '<HTML>';
        lc_html := lc_html || '<HEAD>';
        lc_html := lc_html || '</HEAD>';
        lc_html := lc_html || '<BODY>';
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TABLE BORDER =1>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>ACCOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>MONETARY AMOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IDENTIFICADOR</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>FECHA CONTABILIZACION</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>REFERENCIA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO CABECERA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IVA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CLAVE CONTA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CENTRO BENEFICIO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CICLO</h4></STRONG></TD>';
        lc_html := lc_html || '</TR>';
        FOR d IN c_datos(r.referencia, c.ciclo) LOOP
          lc_html := lc_html || '<TR>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.account ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                     d.monetary_amount || '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.identificador ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.fecha_contabilizacion ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.referencia ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto_cabecera ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.iva ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.clave_conta ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.centro_beneficio ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.ciclo ||
                     '</STRONG></TD>';
          lc_html := lc_html || '</TR>';
        END LOOP;
        ln_id_reporte := NULL;
        lc_html := lc_html || '</table>';
        OPEN lc_id_reporte;
        FETCH lc_id_reporte
          INTO ln_id_reporte;
        CLOSE lc_id_reporte;
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD><STRONG>ID REPORTE : </STRONG></TD>';
        lc_html := lc_html || '<TD>' || ln_id_reporte || '</TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '</table></html>';
        INSERT INTO gsib_reportes_html
          (id_reporte,
           nombre_reporte,
           fecha_corte,
           fecha_generacion,
           reporte_html)
        VALUES
          (sc_id_reporte.nextval,
           lv_nombre_excel,
           ld_fecha,
           SYSDATE,
           lc_html);
        COMMIT;
      END LOOP;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error : ' || SQLERRM || lv_aplicacion;
  END;

END dth_devengamiento_pre_sap;
/
