CREATE OR REPLACE package GSI_GENERA_INFORMACION_FIN is

  -- Author  : Natividad Mantilla L�pez.
  -- Created : 03/08/2007 10:49:50
  -- Purpose : Paquete que me permite generar la proyeccion mensual 
  ---          de lo que se facturar�a en el mes en cuerso para los 2 ciclos.

  Procedure Pr_Principal;
  Procedure Pr_Inserta_Datos;
  Procedure Pr_Borra_Subtotales;
  Procedure Pr_Borra_ServiciosNoValidos;
  Procedure Pt_Borra_Inconsistencias;



end GSI_GENERA_INFORMACION_FIN;
/

