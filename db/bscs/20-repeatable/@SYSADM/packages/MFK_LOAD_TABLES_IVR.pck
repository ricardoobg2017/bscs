CREATE OR REPLACE PACKAGE MFK_LOAD_TABLES_IVR IS

  PROCEDURE mfp_inserta_planes(pv_error OUT VARCHAR2);

  PROCEDURE mfp_inserta_formas_pagos(pv_error OUT VARCHAR2);

  PROCEDURE mfp_inserta_categ_clientes(pv_error OUT VARCHAR2);

  PROCEDURE mfp_inserta_linea_principal(pv_error OUT VARCHAR2);

END MFK_LOAD_TABLES_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_LOAD_TABLES_IVR IS

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que obtiene los planes que seran cargados en la tabla
  **               MF_PLANES_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_inserta_planes(pv_error OUT VARCHAR2) IS
  
    lv_cadena     VARCHAR2(5000);
    lv_aplicativo VARCHAR2(50) := 'MFK_LOAD_TABLES_IVR.MFP_INSERTA_PLANES';
    lv_tabla      VARCHAR2(100) := 'MF_PLANES_IVR';
    
  BEGIN
  
    lv_cadena := 'TRUNCATE TABLE ' || lv_tabla || chr(10);
    EXECUTE IMMEDIATE (lv_cadena);
  
    lv_cadena := 'insert /*+ append */ into ' || lv_tabla || chr(10);
    lv_cadena := lv_cadena || ' Select  y.Id_Plan, z.Descripcion, Cod_Bscs tmcode ' || chr(10);
    lv_cadena := lv_cadena || ' From Bs_Planes@axis x, Ge_Detalles_Planes@axis y, Ge_Planes@axis z ' || chr(10);
    lv_cadena := lv_cadena || ' where x.Tipo =' || chr(39) || 'O' || chr(39) || chr(10);
    lv_cadena := lv_cadena || ' And  x.Id_Clase =' || chr(39) || 'GSM' || chr(39) || chr(10);
    lv_cadena := lv_cadena || ' And x.Id_Detalle_Plan = y.Id_Detalle_Plan' || chr(10);
    lv_cadena := lv_cadena || ' And y.Estado = ' || chr(39) || 'A' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And z.Estado = ' || chr(39) || 'A' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And z.Id_Plan = y.Id_Plan' || chr(10);
    lv_cadena := lv_cadena || ' Order By 1, 2 ';
    EXECUTE IMMEDIATE (lv_cadena);
    COMMIT;
  
    BEGIN
      dbms_session.close_database_link('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR EN APLICATIVO: ' || lv_aplicativo || ' - ' || substr(SQLERRM, 1, 1000);
      ROLLBACK;
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      COMMIT;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que obtiene las formas de pago que seran cargados en la
  **               tabla MF_FORMAS_PAGOS_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_inserta_formas_pagos(pv_error OUT VARCHAR2) IS
  
    lv_cadena     VARCHAR2(5000);
    lv_aplicativo VARCHAR2(50) := 'MFK_LOAD_TABLES_IVR.MFP_INSERTA_FORMAS_PAGOS';
    lv_tabla      VARCHAR2(100) := 'MF_FORMAS_PAGOS_IVR';
    
  BEGIN
  
    lv_cadena := 'TRUNCATE TABLE ' || lv_tabla || chr(10);
    EXECUTE IMMEDIATE (lv_cadena);
  
    lv_cadena := 'insert /*+ append */ into ' || lv_tabla || chr(10);
    lv_cadena := lv_cadena || ' Select Bank_Id,Bankname,Nvl(Idgrupo, ' || chr(39) || 'NA' || chr(39) || ') Idgrupo,Nvl(Nombre_Grupo, ' || chr(39) || 'NO ASIGNADO' || chr(39) || ') Nombre_Grupo  ' || chr(10);
    lv_cadena := lv_cadena || ' From (Select z.Codigo_Bscs,Nvl(a.Id_Forma_Pago, ' || chr(39) || 'NA' || chr(39) || ') Idgrupo,Nvl(a.Descripcion, ' || chr(39) || 'NO ASIGNADO' || chr(39) || ') Nombre_Grupo ' || chr(10);
    lv_cadena := lv_cadena || ' From (Select Distinct x.Codigo_Bscs, ' || chr(10);
    lv_cadena := lv_cadena || ' Nvl(y.Id_Tipo_Fin, x.Id_Financiera) Grupo_Fp ' || chr(10);
    lv_cadena := lv_cadena || ' From Bs_Bank_All@Axis x, Ge_Financieras@Axis y ' || chr(10);
    lv_cadena := lv_cadena || ' Where x.Id_Financiera = y.Id_Financiera(+)) z, ' || chr(10);
    lv_cadena := lv_cadena || ' Ge_Forma_Pagos@Axis a ' || chr(10);
    lv_cadena := lv_cadena || ' Where a.Id_Forma_Pago(+) = z.Grupo_Fp) b, ' || chr(10);
    lv_cadena := lv_cadena || ' Bank_All c ' || chr(10);
    lv_cadena := lv_cadena || ' Where c.Bank_Id = b.Codigo_Bscs(+) ' || chr(10);
    lv_cadena := lv_cadena || ' Order By 4, 1 ';
    EXECUTE IMMEDIATE (lv_cadena);
    COMMIT;
    
    BEGIN
      dbms_session.close_database_link('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR EN APLICATIVO: ' || lv_aplicativo || ' - ' || substr(SQLERRM, 1, 1000);
      ROLLBACK;
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      COMMIT;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que obtiene las categorias de clientes que seran cargados
  **               en la tabla MF_CATEGORIAS_CLIENTE_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_inserta_categ_clientes(pv_error OUT VARCHAR2) IS
  
    lv_cadena     VARCHAR2(5000);
    lv_aplicativo VARCHAR2(50) := 'MFK_LOAD_TABLES_IVR.MFP_INSERTA_CATEG_CLIENTES';
    lv_tabla      VARCHAR2(100) := 'MF_CATEGORIAS_CLIENTE_IVR';
    lv_query      VARCHAR2(4000); -- 10798 SUD KBA
    lv_codigos    VARCHAR2(500);  -- 10798 SUD KBA
    
  BEGIN
  
    lv_cadena := 'TRUNCATE TABLE ' || lv_tabla || chr(10);
    EXECUTE IMMEDIATE (lv_cadena);
  
    lv_cadena := 'insert /*+ append */ into ' || lv_tabla || chr(10);
    lv_cadena := lv_cadena || ' Select Tradecode, Tradename, ' || chr(10);
    lv_cadena := lv_cadena || ' Nvl((Select Segmento ' || chr(10);
    lv_cadena := lv_cadena || ' From Cl_Categorias_Clientes@Axis ' || chr(10);
    lv_cadena := lv_cadena || ' Where Cod_Bscs = Tradecode ' || chr(10);
    lv_cadena := lv_cadena || ' And Rownum < 2), ' || chr(10);
    lv_cadena := lv_cadena || ' ' || chr(39) || 'NA' || chr(39) || ') ' || chr(10);
    lv_cadena := lv_cadena || ' From Trade_All ';
    EXECUTE IMMEDIATE (lv_cadena);
    COMMIT;
    
    -- 10798 INI SUD KBA
    -- Obtener y actualizar los codigos para cuentas categorizadas: VIP, PREMIUM, INSIGNIA
    BEGIN
      lv_query := gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'QUERY_CTAS_CATEGOR_I');
      EXECUTE IMMEDIATE lv_query INTO lv_codigos;
      UPDATE gv_parametros
         SET valor = nvl(lv_codigos, valor)
       WHERE id_tipo_parametro = 10798
         AND id_parametro = 'ID_CTAS_CATEGOR_I';
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;       
    -- 10798 FIN SUD KBA
      
    BEGIN
      dbms_session.close_database_link('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR EN APLICATIVO: ' || lv_aplicativo || ' - ' || substr(SQLERRM, 1, 1000);
      ROLLBACK;
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      COMMIT;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que obtiene las lineas principales que seran cargados
  **               en la tabla MF_LINEA_PRINCIPAL_IVR
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_inserta_linea_principal(pv_error OUT VARCHAR2) IS
    
    lv_cadena     VARCHAR2(5000);
    lv_aplicativo VARCHAR2(50) := 'MFK_LOAD_TABLES_IVR.MFP_INSERTA_LINEA_PRINCIPAL';
    lv_tabla      VARCHAR2(100) := 'MF_LINEA_PRINCIPAL_IVR';
    
  BEGIN
  
    lv_cadena := 'TRUNCATE TABLE ' || lv_tabla || chr(10);
    EXECUTE IMMEDIATE (lv_cadena);
  
    lv_cadena := 'insert /*+ append */ into ' || lv_tabla || chr(10);
    lv_cadena := lv_cadena || ' Select Cu.Id_Servicio, Co.Codigo_Doc, Gd.Id_Plan ' || chr(10);
    lv_cadena := lv_cadena || ' From Cl_Cupos_Servicios_Contratados@AXIS Cu, Cl_Contratos@AXIS Co, Ge_Detalles_Planes@AXIS Gd ' || chr(10);
    lv_cadena := lv_cadena || ' Where Cu.Tipo_Cupo = ' || chr(39) || 'P' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And Cu.Id_Contrato = Co.Id_Contrato ' || chr(10);
    lv_cadena := lv_cadena || ' And Co.Estado = ' || chr(39) || 'A' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And Cu.Estado = ' || chr(39) || 'A' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And Gd.Estado = ' || chr(39) || 'A' || chr(39) || ' ' || chr(10);
    lv_cadena := lv_cadena || ' And Gd.Id_Detalle_Plan = Cu.Id_Detalle_Plan ';
    EXECUTE IMMEDIATE (lv_cadena);
    COMMIT;
    
    BEGIN
      dbms_session.close_database_link('AXIS');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR EN APLICATIVO: ' || lv_aplicativo || ' - ' || substr(SQLERRM, 1, 1000);
      ROLLBACK;
      BEGIN
        dbms_session.close_database_link('AXIS');
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      COMMIT;
  END;

END MFK_LOAD_TABLES_IVR;
/
