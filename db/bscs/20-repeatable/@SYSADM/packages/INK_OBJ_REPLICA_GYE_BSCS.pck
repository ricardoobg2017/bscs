create or replace package INK_OBJ_REPLICA_GYE_BSCS is

  -- Author  : DHUAYAMAVE
  -- Created : 25/08/2016 16:40:11
  -- Purpose : Replicacion de informacion de GYE en  AXIS
  --INSERTA UNA FORMA DE PAGO LA CUAL ES INGRESADA EN INVENTARIO
  PROCEDURE INP_INS_FPHOMOLOGACION_SRI (PV_ID_FORMA_PAGO          IN NUMBER,
                                          PN_ID_FP_SRI            IN NUMBER,
                                          PN_ID_SISTEMA           IN NUMBER,
                                          PD_FECHA_CREACION       IN DATE,
                                          PV_USUARIO_CREACION     IN VARCHAR2,
                                          PV_ESTADO               IN VARCHAR2,
                                          PV_ERROR                OUT VARCHAR2,
                                          PN_COD_ERROR            OUT NUMBER);
  --ACTUALIZA LA FORMA DE PAGO  
  PROCEDURE INP_UPDATE_FPHOMOLOGACION_SRI(PV_ID_FORMA_PAGO     IN VARCHAR2,
                                        PN_ID_FP_SRI            IN NUMBER,
                                        PN_ID_SISTEMA           IN NUMBER,
                                        PD_FECHA_MODIFICACION   IN DATE,
                                        PV_USUARIO_MODIFICACION IN VARCHAR2,
                                        PV_ESTADO               IN VARCHAR2,
                                        PV_ERROR                OUT VARCHAR2,
                                        PN_COD_ERROR            OUT NUMBER);
                                       
end INK_OBJ_REPLICA_GYE_BSCS;
/
create or replace package body INK_OBJ_REPLICA_GYE_BSCS is

  GV_PROGRAMA VARCHAR2(30) := 'INK_OBJ_REPLICA_GYE_BSCS.';
  --INSERTA UNA FORMA DE PAGO LA CUAL ES INGRESADA EN INVENTARIO  
  PROCEDURE INP_INS_FPHOMOLOGACION_SRI(PV_ID_FORMA_PAGO    IN NUMBER,
                                       PN_ID_FP_SRI        IN NUMBER,
                                       PN_ID_SISTEMA       IN NUMBER,
                                       PD_FECHA_CREACION   IN DATE,
                                       PV_USUARIO_CREACION IN VARCHAR2,
                                       PV_ESTADO           IN VARCHAR2,
                                       PV_ERROR            OUT VARCHAR2,
                                       PN_COD_ERROR        OUT NUMBER) IS
    LV_ERROR     VARCHAR2(50) := 'OK';
    LN_COD_ERROR NUMBER := 0;
    LV_PROCESO   VARCHAR2(50) := 'INP_INS_FPHOMOLOGACION_SRI';
  BEGIN
    INSERT INTO bs_homologacion_fpsri
      (ID_FORMA_PAGO,
       ID_FP_SRI,
       ID_SISTEMA,
       FECHA_CREACION,
       USUARIO_CREACION,
       ESTADO)
    VALUES
      (PV_ID_FORMA_PAGO,
       PN_ID_FP_SRI,
       PN_ID_SISTEMA,
       PD_FECHA_CREACION,
       PV_USUARIO_CREACION,
       PV_ESTADO);
    PV_ERROR     := LV_ERROR;
    PN_COD_ERROR := LN_COD_ERROR;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR     := GV_PROGRAMA || LV_PROCESO ||
                      ' - ERROR AL INSERTAR GE_HOMOLOGACION_FPSRI ' ||
                      SUBSTR(SQLERRM, 1, 512);
      PN_COD_ERROR := 1;
  END INP_INS_FPHOMOLOGACION_SRI;

  --ACTUALIZA LA FORMA DE PAGO  
  PROCEDURE INP_UPDATE_FPHOMOLOGACION_SRI(PV_ID_FORMA_PAGO        IN VARCHAR2,
                                          PN_ID_FP_SRI            IN NUMBER,
                                          PN_ID_SISTEMA           IN NUMBER,
                                          PD_FECHA_MODIFICACION   IN DATE,
                                          PV_USUARIO_MODIFICACION IN VARCHAR2,
                                          PV_ESTADO               IN VARCHAR2,
                                          PV_ERROR                OUT VARCHAR2,
                                          PN_COD_ERROR            OUT NUMBER) IS
  
    LV_ERROR     VARCHAR2(50) := 'OK';
    LN_COD_ERROR NUMBER := 0;
    LV_PROCESO   VARCHAR2(50) := 'INP_UPDATE_FPHOMOLOGACION_SRI';
    

  BEGIN

      UPDATE bs_homologacion_fpsri HFP
       SET HFP.ID_FP_SRI            = PN_ID_FP_SRI,
           HFP.FECHA_MODIFICACION   = PD_FECHA_MODIFICACION,
           HFP.USUARIO_MODIFICACION = PV_USUARIO_MODIFICACION,
           HFP.ESTADO               = PV_ESTADO
     WHERE HFP.ID_SISTEMA = PN_ID_SISTEMA
       AND HFP.ID_FORMA_PAGO = PV_ID_FORMA_PAGO;
    
    PV_ERROR     := LV_ERROR;
    PN_COD_ERROR := LN_COD_ERROR;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR     := GV_PROGRAMA || LV_PROCESO ||
                      ' ERROR AL ACTUALIZAR bs_homologacion_fpsri' ||
                      SUBSTR(SQLERRM, 1, 512);
      PN_COD_ERROR := 1;
  END INP_UPDATE_FPHOMOLOGACION_SRI;


end INK_OBJ_REPLICA_GYE_BSCS;
/
